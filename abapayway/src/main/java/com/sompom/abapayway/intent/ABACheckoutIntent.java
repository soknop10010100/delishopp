package com.sompom.abapayway.intent;

import android.content.Context;
import android.content.Intent;

import com.sompom.abapayway.ui.ABACheckoutActivity;

public class ABACheckoutIntent extends Intent {

    public static final String IS_PAYMENT_SUCCESS = "IS_PAYMENT_SUCCESS";
    public static final String CHECKOUT_URL = "CHECKOUT_URL";
    public static final String CONTINUE_SUCCESS_URL = "CONTINUE_SUCCESS_URL";

    public ABACheckoutIntent(boolean isPaymentSuccess) {
        putExtra(IS_PAYMENT_SUCCESS, isPaymentSuccess);
    }

    public ABACheckoutIntent(Context packageContext, String checkOutUrl, String continueSuccessUrl) {
        super(packageContext, ABACheckoutActivity.class);
        putExtra(CHECKOUT_URL, checkOutUrl);
        putExtra(CONTINUE_SUCCESS_URL, continueSuccessUrl);
    }

    public ABACheckoutIntent(Intent o) {
        super(o);
    }

    public String getCheckoutUrl() {
        return getStringExtra(CHECKOUT_URL);
    }

    public String getContinueSuccessUrl() {
        return getStringExtra(CONTINUE_SUCCESS_URL);
    }

    public boolean isPaymentSuccess() {
        return getBooleanExtra(IS_PAYMENT_SUCCESS, false);
    }
}
