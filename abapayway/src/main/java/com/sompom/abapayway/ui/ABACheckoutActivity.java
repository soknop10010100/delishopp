package com.sompom.abapayway.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.sompom.abapayway.R;
import com.sompom.abapayway.databinding.ActivityAbacheckoutBinding;
import com.sompom.abapayway.helper.LogUtil;
import com.sompom.abapayway.intent.ABACheckoutIntent;

public class ABACheckoutActivity extends AppCompatActivity {

    private static final String TAG = ABACheckoutActivity.class.getSimpleName();
    private static final String CLOSE_CHECKOUT_REDIRECTION_URL = "closecheckout://ababank.com";

    private ActivityAbacheckoutBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_abacheckout);
        initActionBar();
        ABACheckoutIntent abaCheckoutIntent = new ABACheckoutIntent(getIntent());
        initWebView(abaCheckoutIntent.getCheckoutUrl(), abaCheckoutIntent.getContinueSuccessUrl());
    }

    private void initActionBar() {
        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initWebView(String checkoutUrl, String continueSuccessUrl) {
        LogUtil.log(TAG, "initWebView: " + checkoutUrl);
        mBinding.webView.getSettings().setJavaScriptEnabled(true);
        mBinding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                LogUtil.log(TAG, "shouldOverrideUrlLoading: url: " + request.getUrl());
                if (TextUtils.equals(request.getUrl().toString(), CLOSE_CHECKOUT_REDIRECTION_URL)) {
                    closeScreen(false);
                    return true;
                } else if (TextUtils.equals(request.getUrl().toString(), continueSuccessUrl)) {
                    closeScreen(true);
                    return true;
                }

                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                LogUtil.log(TAG, "onPageStarted");
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                LogUtil.log(TAG, "onPageFinished");
            }
        });
        mBinding.webView.setDownloadListener((url1, userAgent, contentDisposition, mimetype, contentLength) -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url1));
            startActivity(i);
        });
        mBinding.webView.loadUrl(checkoutUrl);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void closeScreen(boolean isPaymentSuccess) {
        setResult(isPaymentSuccess ? AppCompatActivity.RESULT_OK : AppCompatActivity.RESULT_CANCELED,
                new ABACheckoutIntent(isPaymentSuccess));
        finish();
    }

    @Override
    public void finish() {
        setResult(AppCompatActivity.RESULT_OK);
        super.finish();
    }
}