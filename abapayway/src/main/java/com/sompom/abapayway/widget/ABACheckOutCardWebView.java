package com.sompom.abapayway.widget;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.sompom.abapayway.helper.LogUtil;

public class ABACheckOutCardWebView extends WebView {

    private static final String TAG = ABACheckOutCardWebView.class.getSimpleName();
    private static final String CLOSE_CHECKOUT_REDIRECTION_URL = "closecheckout://ababank.com";

    private ABACheckOutCardWebViewListener mListener;

    public ABACheckOutCardWebView(@NonNull Context context) {
        super(context);
    }

    public ABACheckOutCardWebView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ABACheckOutCardWebView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ABACheckOutCardWebView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setListener(ABACheckOutCardWebViewListener listener) {
        mListener = listener;
    }

    public void loadCheckout(String checkoutUrl, String continueSuccessUrl) {
        LogUtil.log(TAG, "initWebView: " + checkoutUrl);
        getSettings().setJavaScriptEnabled(true);
        setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                LogUtil.log(TAG, "shouldOverrideUrlLoading: url: " + request.getUrl());
                if (TextUtils.equals(request.getUrl().toString(), CLOSE_CHECKOUT_REDIRECTION_URL)) {
                    fireWebViewCloseEvent(ABACheckOutCardWebViewCloseByEvent.CLOSED_BY_DRAGGING_DOWN_POP_UP);
                    return true;
                } else if (TextUtils.equals(request.getUrl().toString(), continueSuccessUrl)) {
                    fireWebViewCloseEvent(ABACheckOutCardWebViewCloseByEvent.CLOSED_BY_SUCCESS);
                    return true;
                }

                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                LogUtil.log(TAG, "onPageStarted");
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                LogUtil.log(TAG, "onPageFinished");
                if (mListener != null) {
                    mListener.onPageFinished();
                }
            }
        });
        setDownloadListener((url1, userAgent, contentDisposition, mimetype, contentLength) -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url1));
            getContext().startActivity(i);
        });
        loadUrl(checkoutUrl);
    }

    private void fireWebViewCloseEvent(ABACheckOutCardWebViewCloseByEvent event) {
        if (mListener != null) {
            mListener.onWebViewClosed(event);
        }
    }

    public interface ABACheckOutCardWebViewListener {
        void onWebViewClosed(ABACheckOutCardWebViewCloseByEvent event);

        void onPageFinished();
    }

    public enum ABACheckOutCardWebViewCloseByEvent {
        CLOSED_BY_SUCCESS,
        CLOSED_BY_DRAGGING_DOWN_POP_UP;
    }
}
