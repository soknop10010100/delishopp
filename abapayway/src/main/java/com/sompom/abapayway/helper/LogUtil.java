package com.sompom.abapayway.helper;

import android.util.Log;

public class LogUtil {

    public static void log(String tag, String message) {
        Log.d(tag, message);
    }
}
