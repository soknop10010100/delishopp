package com.sompom.abapayway.helper;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

public class ABAPayWayTestUrl {

    public static final String TEST_CONTINUE_SUCCESS_URL = "https://appgora.com/";
    public static final String TEST_CHECKOUT_URL = "https://checkout-sandbox.payway.com.kh/checkout-native/eyJzdGVwIjoiY2FyZF9wYXltZW50X2luaXQiLCJ0b2tlbl90aW1lIjoxNjQ2NjIxMjk1LCJ0b2tlbiI6ImEzV3BoYXVaTEliUWZxakNoZkF6RUpXNk5KNnllN04rU1lOOTZrdnRidEE3SU4zVStkT01SaXprOTRpM2VkNmhscGNZXC9pQWRTenJtelo1bUxtUVlaUT09IiwiY29mX2VuYWJsZSI6ZmFsc2UsImFiYV9kYXRhIjoiVjMrbklGSnRNMHVPektNNXNCcnV6S2kxemVWUmc2c1BYeGtSRjhNTVhMN0NJSkhKRDNYZTJUemsrbUV6NnBNNGhmM2pHVE94QTZ2c0E3NWd5NEZQa01LWkRoeFJjK2hQME5nNGtRdkF6eFE9IiwicnNhX21vZHVsZXMiOiI4QjgxM0M1NDkxN0I1RTBEQ0U0MzFGQjYxRDgzNzMwOTQwMDE4MUExRjExN0I1QzZDMUE3QjU1MjU5MkFGQURBRDRDRTlEQjUwRDNGNTlCOEJCRDhBMEE5NTkyMUVGRjU4RUY5NTJGRDhFQjhGRTM3Mjg3QzI0RTEzNjM1Q0Y2MzE0RjA5QTMyMjRDRjhGNkMxMTlCMTQ3NDZBQzRDMEMzQTZEQUQzMjA3OUFFMUE0RjU2OUVBNTFERUM1NzRCOEFCQ0E1RTlBQTdFOUE2Nzg4NDhDRjNCRUJFOTZENDA2RTNENjMxRTNCOTdEREEyQkFGREU0MkYzN0VDQzdBOTY1IiwicnNhX2V4cG9uZW50IjoiMDEwMDAxIiwiZXhwaXJlX2luIjoxNjQ2NjIxNDc1LCJ0cmFuc2FjdGlvbl9zdW1tYXJ5Ijp7Im9yZGVyX2RldGFpbHMiOnsic3VidG90YWwiOjMsInNoaXBwaW5nIjowLCJ2YXRfZW5hYmxlZCI6MCwidmF0IjowLCJ2YXRfYW1vdW50IjowLCJ0b3RhbCI6MywiY3VycmVuY3kiOiJVU0QifSwibWVyY2hhbnQiOnsibmFtZSI6IkRFTElTSE9QIiwibG9nbyI6Imh0dHBzOlwvXC9zYW5kYm94LnBheXdheS5jb20ua2hcL3VwbG9hZHNcL3R4X2FiYWVjb21tZXJjZVwvbWVyY2hhbnRfbG9nb1wvZGVsaXNob3AtcGF5d2F5LWxvZ28ucG5nIiwicHJpbWFyeV9jb2xvciI6IiMwRjkxMUIiLCJjYW5jZWxfdXJsIjoiIiwidGhlbWVzIjoiIiwiZm9udF9mYW1pbHkiOiIiLCJmb250X3NpemUiOiIiLCJiZ19jb2xvciI6IiIsImJvcmRlcl9yYWRpdXMiOiIifX0sInN0YXR1cyI6eyJjb2RlIjoiMDAiLCJtZXNzYWdlIjoiU3VjY2VzcyEifX0%3D";
    public static final String TEST_ABA_PAY_DEEP_LINK = "abamobilebank://ababank.com?type=payway&qrcode=ABA100411571101312038401302301408DELISHOP15046PW-2-019458b6eb7eda1870242adc5b2dc9ae9c032aebe";

    public static void openABAPayDeepLinking(Context context) {
        Intent intent = null;
        try {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(TEST_ABA_PAY_DEEP_LINK));
            context.startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
            if (intent != null) {
                intent.setData(Uri.parse("market://details?id=com.paygo24.ibank"));
            } else {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.paygo24.ibank"));
            }
            context.startActivity(intent);
        }
    }
}
