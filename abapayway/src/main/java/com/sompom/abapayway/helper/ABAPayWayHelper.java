package com.sompom.abapayway.helper;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class ABAPayWayHelper {

    private static final String ABA_APP_PACKAGE_NAME = "com.paygo24.ibank";
    private static final String TEST_ABA_APP_PACKAGE_NAME = "com.paygo24.ibank.test";
    private static final String ABA_MOBILE_APP_DETAIL_PACKAGE_URL = "market://details?id=" + ABA_APP_PACKAGE_NAME;

    public static void openABAPayDeepLinking(Context context, String deepLink) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(deepLink));
            context.startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(ABA_MOBILE_APP_DETAIL_PACKAGE_URL));
            context.startActivity(intent);
        }
    }

    public static boolean isABAMobileAppInstalledOnDevice(Context context) {
        Intent abaPackage = context.getPackageManager().getLaunchIntentForPackage(ABA_APP_PACKAGE_NAME);
//        Intent testAbaPackage = context.getPackageManager().getLaunchIntentForPackage(TEST_ABA_APP_PACKAGE_NAME);
//        return abaPackage != null || testAbaPackage != null;
        return abaPackage != null;
    }
}
