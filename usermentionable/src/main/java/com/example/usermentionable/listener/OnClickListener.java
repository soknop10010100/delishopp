package com.example.usermentionable.listener;

/**
 * Created by nuonveyo on 12/6/18.
 */

public interface OnClickListener<T> {
    void onClick(T t);
}
