package com.example.usermentionable.utils;

import android.content.Context;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.desmond.squarecamera.utils.ThemeManager;
import com.example.usermentionable.R;
import com.example.usermentionable.model.UserMentionable;
import com.resourcemanager.helper.FontHelper;
import com.resourcemanager.utils.AttributeConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by nuonveyo on 12/6/18.
 */

public final class GlideUtil {

    private GlideUtil() {
    }

    public static void loadImageProfile(ImageView imageView, UserMentionable userMentionable) {
        TextDrawable drawable = getDrawableProfileFromName(imageView.getContext(),
                userMentionable.getFirstName(),
                userMentionable.getLastName(), false);
        Glide.with(imageView.getContext())
                .load(userMentionable.getImageUrl())
                .apply(RequestOptions
                        .circleCropTransform()
                        .centerCrop()
                        .override(0, 400)
                        .placeholder(drawable))
                .into(imageView);
    }

    public static TextDrawable getDrawableProfileFromName(Context context,
                                                          @NonNull String firstName,
                                                          @Nullable String lastName,
                                                          boolean isBuildRect) {
        /*
        Will draw only with the first letter of first and last name.
        First name must be provided and last name can be ignored.
        If the last name is not given, then will check to take only max two first letter of
        the split of first name with space.
        If the first name is empty, will check to use app name.
         */
        String buildName = "";
        if (firstName != null) {
            firstName = firstName.trim();
        }
        if (lastName != null) {
            lastName = lastName.trim();
        }
        if (!TextUtils.isEmpty(firstName)) {
            if (TextUtils.isEmpty(lastName)) {
                buildName = getOnlyFirstLetterOfSeparateName(firstName);
            } else {
                buildName += firstName.charAt(0);
                buildName += Objects.requireNonNull(lastName).charAt(0);
            }
        }

        if (TextUtils.isEmpty(buildName)) {
            String defaultName = context.getResources().getString(R.string.app_name);
            buildName = getOnlyFirstLetterOfSeparateName(defaultName);
        }

        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getColor(buildName);

        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, ThemeManager.getAppTheme(context).getThemeRes());
        TextDrawable.IShapeBuilder iShapeBuilder = TextDrawable.builder()
                .beginConfig()
                .textColor(AttributeConverter.convertAttrToColor(contextThemeWrapper, R.attr.user_avatar_text))
                .useFont(FontHelper.getBoldFontStyle(context))
                .endConfig();

        if (!isBuildRect) {
            return iShapeBuilder.buildRound(buildName.toUpperCase(), color);
        }

        return iShapeBuilder.buildRect(buildName.toUpperCase(), color);
    }

    private static String getOnlyFirstLetterOfSeparateName(String name) {
        /*
        Will get only max two fist letter of split name by space.
        Ex: Chhom Veasna Dara => will return only "CV"
         */
        if (!TextUtils.isEmpty(name)) {
            String[] split = name.split(" ");
            List<String> validList = new ArrayList<>();
            for (String value : split) {
                if (!TextUtils.isEmpty(value)) {
                    validList.add(value);
                }
            }
            if (validList.size() > 1) {
                return validList.get(0).charAt(0) + String.valueOf(validList.get(1).charAt(0));
            } else {
                return String.valueOf(name.charAt(0));
            }
        }

        return "";
    }
}
