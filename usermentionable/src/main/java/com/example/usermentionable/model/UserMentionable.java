package com.example.usermentionable.model;

import android.os.Parcel;
import androidx.annotation.NonNull;

import com.example.usermentionable.mentions.Mentionable;
import com.example.usermentionable.utils.RenderTextAsMentionable;

/**
 * Created by nuonveyo on 11/16/18.
 */

public class UserMentionable implements Mentionable {
    public static final Creator<UserMentionable> CREATOR = new Creator<UserMentionable>() {
        @Override
        public UserMentionable createFromParcel(Parcel source) {
            return new UserMentionable(source);
        }

        @Override
        public UserMentionable[] newArray(int size) {
            return new UserMentionable[size];
        }
    };

    private String mUserId;
    private String mImageUrl;
    private String mUserName;
    private String mFirstName;
    private String mLastName;
    private boolean mIsStore;

    public UserMentionable() {
    }

    protected UserMentionable(Parcel in) {
        this.mUserId = in.readString();
        this.mImageUrl = in.readString();
        this.mUserName = in.readString();
        this.mFirstName = in.readString();
        this.mLastName = in.readString();
        this.mIsStore = in.readByte() != 0;
    }

    public UserMentionable(String userId, String firstName, String lastName, String userProfile, boolean isStore) {
        mUserId = userId;
        mFirstName = firstName;
        mLastName = lastName;
        mImageUrl = userProfile;
        mIsStore = isStore;
        mUserName = getFullName();
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getUserName() {
        //Old code of Tookitup
//        if (isStore()) {
//            return "@" + mUserName;
//        } else {
//            return mUserName;
//        }

        //No need to add @sign when post to server.
        return mUserName;
    }

    public String getNameWithAtSign() {
        return "@" + RenderTextAsMentionable.getValidDisplayName(mUserName);
    }

    public String getFullName() {
        return mFirstName + " " + mLastName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public boolean isStore() {
        return mIsStore;
    }

    @NonNull
    @Override
    public String getTextForDisplayMode(MentionDisplayMode mode) {
        switch (mode) {
            case FULL:
                //No need to display @sign before mention username
                return RenderTextAsMentionable.getValidDisplayName(mUserName);
            case PARTIAL:
            case NONE:
            default:
                return "";
        }
    }

    @Override
    public MentionDeleteStyle getDeleteStyle() {
        // Note: Cities do not support partial deletion
        // i.e. "San Francisco" -> DEL -> ""
        return MentionDeleteStyle.PARTIAL_NAME_DELETE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mUserId);
        dest.writeString(this.mImageUrl);
        dest.writeString(this.mUserName);
        dest.writeString(this.mFirstName);
        dest.writeString(this.mLastName);
        dest.writeByte(this.mIsStore ? (byte) 1 : (byte) 0);
    }
}
