
package com.tookitup.tenorgif.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Mediumgif {

    @SerializedName("dims")
    private List<Long> mDims;
    @SerializedName("preview")
    private String mPreview;
    @SerializedName("size")
    private Long mSize;
    @SerializedName("url")
    private String mUrl;

    public List<Long> getDims() {
        return mDims;
    }

    public void setDims(List<Long> dims) {
        mDims = dims;
    }

    public String getPreview() {
        return mPreview;
    }

    public void setPreview(String preview) {
        mPreview = preview;
    }

    public Long getSize() {
        return mSize;
    }

    public void setSize(Long size) {
        mSize = size;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

}
