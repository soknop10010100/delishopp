
package com.tookitup.tenorgif.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TenorResult {

    @SerializedName("next")
    private String mNext;
    @SerializedName("results")
    private List<Result> mResults;
    @SerializedName("weburl")
    private String mWeburl;

    public String getNext() {
        return mNext;
    }

    public void setNext(String next) {
        mNext = next;
    }

    public List<Result> getResults() {
        return mResults;
    }

    public void setResults(List<Result> results) {
        mResults = results;
    }

    public String getWeburl() {
        return mWeburl;
    }

    public void setWeburl(String weburl) {
        mWeburl = weburl;
    }

}
