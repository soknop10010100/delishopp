package com.tookitup.tenorgif.model;

/**
 * Created by nuonveyo on 10/17/18.
 */

public enum GifType {
    Tenor(1),
    GifPhy(2);

    private int mId;

    GifType(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public static GifType getType(int id) {
        for (GifType gifType : GifType.values()) {
            if (gifType.getId() == id) {
                return gifType;
            }
        }
        return GifType.Tenor;
    }
}
