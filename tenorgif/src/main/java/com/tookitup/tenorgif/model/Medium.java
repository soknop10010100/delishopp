
package com.tookitup.tenorgif.model;

import com.google.gson.annotations.SerializedName;

public class Medium {

    @SerializedName("gif")
    private Gif mGif;
    @SerializedName("mediumgif")
    private Mediumgif mMediumgif;
    @SerializedName("tinygif")
    private Gif mTinyGif;

    public Gif getGif() {
        return mGif;
    }

    public void setGif(Gif gif) {
        mGif = gif;
    }


    public Mediumgif getMediumgif() {
        return mMediumgif;
    }

    public void setMediumgif(Mediumgif mediumgif) {
        mMediumgif = mediumgif;
    }

    public Gif getTinyGif() {
        return mTinyGif;
    }

    public void setTinyGif(Gif tinyGif) {
        mTinyGif = tinyGif;
    }
}
