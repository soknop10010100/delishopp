package com.tookitup.tenorgif.listener;

/**
 * Created by nuonveyo on 10/16/18.
 */

public interface OnLoadCompletedListener {
    void onLoaded();
}
