package io.github.sac;

import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFrame;

import java.util.List;
import java.util.Map;

/**
 * Created by sachin on 13/11/16.
 */

/**
 * Must Read:
 * This class just the copy from @link{io.github.sac.BasicListener}  and just replace the use of @link{io.github.sac.Socket}
 * with CustomSocket that we created.
 * So in there is any update related to @link{io.github.sac.BasicListener}, this class must be updated too.
 */
public interface CustomBasicListener {

    void onConnected(CustomSocket socket, Map<String, List<String>> headers);

    void onDisconnected(CustomSocket socket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer);

    void onConnectError(CustomSocket socket, WebSocketException exception);

    void onAuthentication(CustomSocket socket, Boolean status);

    void onSetAuthToken(String token, CustomSocket socket);

    void onNoMoreFrameFromSocketServer();
}
