package com.proapp.sompom.decorataor;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by ChhomVeasna on 17/4/20.
 * <p>
 * This class can be only used for {@link LinearLayoutManager}
 */
public class GroupMediaSectionItemDecorator extends RecyclerView.ItemDecoration {

    private int mFirstItemMarginTop;
    private int mSecondItemMarginTop;

    public GroupMediaSectionItemDecorator(int firstItemMarginTop, int secondItemMarginTop) {
        mFirstItemMarginTop = firstItemMarginTop;
        mSecondItemMarginTop = secondItemMarginTop;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect,
                               @NonNull View view,
                               RecyclerView parent,
                               @NonNull RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        if (position == 0) {
            outRect.top = mFirstItemMarginTop;
        } else {
            outRect.top = mSecondItemMarginTop;
        }
    }
}
