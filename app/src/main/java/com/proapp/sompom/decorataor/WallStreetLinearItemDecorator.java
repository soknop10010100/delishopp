package com.proapp.sompom.decorataor;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.model.emun.ViewType;

import java.util.Objects;

/**
 * Created by ChhomVeasna on 11/8/17.
 * <p>
 * This class can be only used for {@link LinearLayoutManager}
 */
public class WallStreetLinearItemDecorator extends RecyclerView.ItemDecoration {

    private int mItemSpacing;
    private int mEdgeSpacing;
    private boolean mEnableTopBottomEdgePadding;
    private boolean mEnableLeftRightEdgePadding;
    private boolean mIncludePaddingForCreatePostItem = true;

    public WallStreetLinearItemDecorator(int itemSpacing,
                                         int edgeSpacing,
                                         boolean enableTopBottomEdgePadding,
                                         boolean enableLeftRightEdgePadding,
                                         boolean includePaddingForCreatePostItem) {
        mItemSpacing = itemSpacing;
        mEdgeSpacing = edgeSpacing;
        mEnableTopBottomEdgePadding = enableTopBottomEdgePadding;
        mEnableLeftRightEdgePadding = enableLeftRightEdgePadding;
        mIncludePaddingForCreatePostItem = includePaddingForCreatePostItem;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect,
                               @NonNull View view,
                               RecyclerView parent,
                               @NonNull RecyclerView.State state) {
        if (parent.getLayoutManager() instanceof LinearLayoutManager) {
            int position = parent.getChildAdapterPosition(view);
            int viewType = Objects.requireNonNull(parent.getAdapter()).getItemViewType(position);
            if ((viewType == ViewType.NewPost.getTimelineViewType() ||
                    viewType == ViewType.GREETING_ITEM.getTimelineViewType()) &&
                    !mIncludePaddingForCreatePostItem) {
                return;
            }

            if (position == 0) {
                if (mEnableTopBottomEdgePadding) {
                    outRect.top = mEdgeSpacing;
                }
                if (mEnableLeftRightEdgePadding) {
                    outRect.left = mEdgeSpacing;
                    outRect.right = mEdgeSpacing;
                }
                outRect.bottom = mItemSpacing;
            } else if (position == parent.getChildCount() - 1) {
                if (mEnableTopBottomEdgePadding) {
                    outRect.bottom = mEdgeSpacing;
                }
                if (mEnableLeftRightEdgePadding) {
                    outRect.left = mEdgeSpacing;
                    outRect.right = mEdgeSpacing;
                }
            } else {
                outRect.bottom = mItemSpacing;
                if (mEnableLeftRightEdgePadding) {
                    outRect.left = mEdgeSpacing;
                    outRect.right = mEdgeSpacing;
                }
            }
        }
    }
}
