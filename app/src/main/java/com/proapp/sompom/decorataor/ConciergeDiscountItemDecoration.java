package com.proapp.sompom.decorataor;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;

/**
 * Created by Or Vitovongsak on 18/1/22.
 */
public class ConciergeDiscountItemDecoration extends RecyclerView.ItemDecoration {

    private Context mContext;
    private int mSpace;

    public ConciergeDiscountItemDecoration(Context context) {
        mContext = context;
        mSpace = mContext.getResources().getDimensionPixelSize(R.dimen.space_medium_large);
    }

    public ConciergeDiscountItemDecoration(Context context, int space) {
        mContext = context;
        mSpace = space;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect,
                               @NonNull View view,
                               RecyclerView parent,
                               @NonNull RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        if (position == 0) {
            outRect.left = mSpace;
        }
        outRect.right = mSpace;
    }
}
