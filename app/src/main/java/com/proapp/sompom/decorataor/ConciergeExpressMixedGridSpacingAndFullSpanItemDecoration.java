package com.proapp.sompom.decorataor;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.AbsConciergeProductItemDisplayAdapter;
import com.proapp.sompom.databinding.LayoutLoadingBinding;
import com.proapp.sompom.databinding.ListConciergeExtraItemSectionTitleItemBinding;
import com.proapp.sompom.viewholder.BindingViewHolder;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class ConciergeExpressMixedGridSpacingAndFullSpanItemDecoration extends RecyclerView.ItemDecoration {

    private final int mSpanCount;
    private final int mSpacing;
    private final AbsConciergeProductItemDisplayAdapter mAbsConciergeProductItemDisplayAdapter;
    private final List<Integer> mIgnoreApplyingSpacePositionCountList = new ArrayList<>();
    private final List<Integer> mAppliedGridSpaceItemCount = new ArrayList<>();
    private int mSpannedItemCountBeforeDisplayingExtraItemSection;
    private int mExtraItemPosition = -1;

    public ConciergeExpressMixedGridSpacingAndFullSpanItemDecoration(Context context,
                                                                     AbsConciergeProductItemDisplayAdapter adapter,
                                                                     int spanCount) {
        this.mAbsConciergeProductItemDisplayAdapter = adapter;
        this.mSpanCount = spanCount;
        this.mSpacing = context.getResources().getDimensionPixelSize(R.dimen.space_large);
    }

    public ConciergeExpressMixedGridSpacingAndFullSpanItemDecoration(Context context,
                                                                     AbsConciergeProductItemDisplayAdapter adapter,
                                                                     int spacing,
                                                                     int spanCount) {
        this.mAbsConciergeProductItemDisplayAdapter = adapter;
        this.mSpanCount = spanCount;
        this.mSpacing = spacing;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, RecyclerView parent, @NonNull RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        if (position < 0) {
            return;
        }

        boolean shouldApplyGridSpacing = mAbsConciergeProductItemDisplayAdapter.shouldApplyGridSpacing(position);
        if (shouldApplyGridSpacing) {
            if (!mAppliedGridSpaceItemCount.contains(position)) {
                mAppliedGridSpaceItemCount.add(position);
            }

            int previousIgnoreGridSpacingItemCount = getPreviousIgnoreGridSpacingItemCount(position);
            int validPosition = Math.abs((position - previousIgnoreGridSpacingItemCount));
            if (position > mExtraItemPosition) {
                validPosition -= mSpannedItemCountBeforeDisplayingExtraItemSection;
            }
            int column = validPosition % mSpanCount; // item column
            Timber.i("validPosition: " + validPosition +
                    ", position: " + position +
                    ", column: " + column +
                    ", mExtraItemPosition: " + mExtraItemPosition +
                    ", mSpannedItemCountBeforeDisplayingExtraItemSection: " + mSpannedItemCountBeforeDisplayingExtraItemSection);

            if (validPosition < 0) {
                return;
            }

            outRect.left = mSpacing - column * mSpacing / mSpanCount; // spacing - column * ((1f / spanCount) * spacing)
            outRect.right = (column + 1) * mSpacing / mSpanCount; // (column + 1) * ((1f / spanCount) * spacing)
            outRect.bottom = mSpacing;
        } else {
            RecyclerView.ViewHolder childViewHolder = parent.getChildViewHolder(view);
            if (!(childViewHolder instanceof BindingViewHolder && ((BindingViewHolder) childViewHolder).getBinding() instanceof
                    ListConciergeExtraItemSectionTitleItemBinding)) {
                outRect.left = mSpacing;
                outRect.right = mSpacing;
            } else {
//                Timber.i("Reach extra item position at " + position);
                outRect.bottom = mSpacing;
            }

            if (!(childViewHolder instanceof BindingViewHolder && ((BindingViewHolder) childViewHolder).getBinding() instanceof LayoutLoadingBinding)) {
                if (!mIgnoreApplyingSpacePositionCountList.contains(position)) {
                    mIgnoreApplyingSpacePositionCountList.add(position);
                }
            }

            //Will check to get value only one time.
            if (mExtraItemPosition < 0 && mAbsConciergeProductItemDisplayAdapter.isExtraItemSectionTitleViewItem(position)) {
                mExtraItemPosition = position;
                mSpannedItemCountBeforeDisplayingExtraItemSection = mAppliedGridSpaceItemCount.size() % mSpanCount;
            }
        }
    }

    private int getPreviousIgnoreGridSpacingItemCount(int currentPosition) {
        int count = 0;
        for (Integer index : mIgnoreApplyingSpacePositionCountList) {
            if (index < currentPosition) {
                count++;
            }
        }

        return count;
    }
}