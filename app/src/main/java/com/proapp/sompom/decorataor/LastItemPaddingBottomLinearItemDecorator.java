package com.proapp.sompom.decorataor;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import timber.log.Timber;

/**
 * Created by ChhomVeasna on 17/4/20.
 * <p>
 * This class can be only used for {@link LinearLayoutManager}
 */
public class LastItemPaddingBottomLinearItemDecorator extends RecyclerView.ItemDecoration {

    private int mLastItemPaddingBottom;

    public LastItemPaddingBottomLinearItemDecorator(int lastItemPaddingBottom) {
        mLastItemPaddingBottom = lastItemPaddingBottom;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect,
                               @NonNull View view,
                               RecyclerView parent,
                               @NonNull RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
//        Timber.i("getItemOffsets: " + position + ", Item count: " + parent.getAdapter().getItemCount());
        if (parent.getAdapter() != null &&
                (position == parent.getAdapter().getItemCount() - 1)) {
            outRect.bottom = mLastItemPaddingBottom;
        } else {
            outRect.bottom = 0;
        }
    }
}
