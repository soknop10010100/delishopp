package com.proapp.sompom.decorataor;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.adapter.newadapter.AbsConciergeProductItemDisplayAdapter;

public class ConciergeGridSpacingItemDecoration extends RecyclerView.ItemDecoration {

    private int spanCount;
    private int spacing;
    private boolean includeEdge;
    private AbsConciergeProductItemDisplayAdapter mAbsConciergeProductItemDisplayAdapter;
    private int mSpaceForLastRow;

    public ConciergeGridSpacingItemDecoration(AbsConciergeProductItemDisplayAdapter adapter,
                                              int spanCount,
                                              int spacing,
                                              int spaceForLastRow,
                                              boolean includeEdge) {
        this.mAbsConciergeProductItemDisplayAdapter = adapter;
        this.spanCount = spanCount;
        this.mSpaceForLastRow = spaceForLastRow;
        this.spacing = spacing;
        this.includeEdge = includeEdge;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, RecyclerView parent, @NonNull RecyclerView.State state) {
        parent.getAdapter().getItemCount();
        int position = parent.getChildAdapterPosition(view); // item position
        boolean shouldApplyGridSpacing = mAbsConciergeProductItemDisplayAdapter.shouldApplyGridSpacing(position);
        if (shouldApplyGridSpacing) {
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }

//                Timber.i("position: " + position +
//                        ", parent.getAdapter().getItemCount(): " + parent.getAdapter().getItemCount() +
//                        ", spanCount: " + spanCount +
//                        ", has load more: " + mAbsConciergeProductItemDisplayAdapter.hasLoadMore());
                if (position > (parent.getAdapter().getItemCount() - spanCount) &&
                        !mAbsConciergeProductItemDisplayAdapter.hasLoadMore()) {
                    //Reach last last row and no load more. So we have to bottom space.
//                    Timber.i("mSpaceForLastRow: " + mSpaceForLastRow);
                    outRect.bottom = mSpaceForLastRow;
                }
            }
        }
    }
}