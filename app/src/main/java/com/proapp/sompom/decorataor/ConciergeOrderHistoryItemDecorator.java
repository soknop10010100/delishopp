package com.proapp.sompom.decorataor;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;

/**
 * Created by Veasna Chhom on 21/9/21.
 */
public class ConciergeOrderHistoryItemDecorator extends RecyclerView.ItemDecoration {

    private final int mFirstTopItemMarginTop;
    private final int mItemMarginBottom;

    public ConciergeOrderHistoryItemDecorator(Context context) {
        mFirstTopItemMarginTop = context.getResources().getDimensionPixelSize(R.dimen.space_xlarge);
        mItemMarginBottom = context.getResources().getDimensionPixelSize(R.dimen.space_large);
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect,
                               @NonNull View view,
                               RecyclerView parent,
                               @NonNull RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        if (position == 0) {
            outRect.top = mFirstTopItemMarginTop;
        }
        outRect.bottom = mItemMarginBottom;
    }
}
