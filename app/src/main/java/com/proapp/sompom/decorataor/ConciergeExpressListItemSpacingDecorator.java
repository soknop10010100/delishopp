package com.proapp.sompom.decorataor;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ListConciergeExtraItemSectionTitleItemBinding;
import com.proapp.sompom.viewholder.BindingViewHolder;

public class ConciergeExpressListItemSpacingDecorator extends RecyclerView.ItemDecoration {

    private final int mVerticalSpacing;
    private final int mHorizontalSpacing;
    private final int mExtraItemBottomSpace;

    public ConciergeExpressListItemSpacingDecorator(Context context) {
        mHorizontalSpacing = context.getResources().getDimensionPixelSize(R.dimen.space_large);
        mVerticalSpacing = 0;
        mExtraItemBottomSpace = context.getResources().getDimensionPixelSize(R.dimen.space_xlarge);
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, RecyclerView parent, @NonNull RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        if (position < 0) {
            return;
        }

        RecyclerView.ViewHolder childViewHolder = parent.getChildViewHolder(view);
        if ((childViewHolder instanceof BindingViewHolder)) {
            if (((BindingViewHolder) childViewHolder).getBinding() instanceof
                    ListConciergeExtraItemSectionTitleItemBinding) {
                outRect.bottom = mExtraItemBottomSpace;
            } else {
                if (mHorizontalSpacing > 0) {
                    outRect.left = mHorizontalSpacing;
                    outRect.right = mHorizontalSpacing;
                } else {
                    outRect.left = 0;
                    outRect.right = 0;
                }

                if (mVerticalSpacing > 0) {
                    outRect.top = mVerticalSpacing;
                    outRect.bottom = mVerticalSpacing;
                } else {
                    outRect.top = 0;
                    outRect.bottom = 0;
                }
            }
        } else {
            outRect.left = 0;
            outRect.right = 0;
            outRect.top = mVerticalSpacing;
            outRect.bottom = mVerticalSpacing;
        }
    }
}