package com.proapp.sompom.decorataor;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by ChhomVeasna on 17/4/20.
 * <p>
 * This class can be only used for {@link LinearLayoutManager}
 */
public class GridLeftAndEightEdgeSpaceItemDecorator extends RecyclerView.ItemDecoration {

    private int mSpace;

    public GridLeftAndEightEdgeSpaceItemDecorator(int space) {
        this.mSpace = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildLayoutPosition(view);
        if (position == 0) {
            outRect.left = mSpace;
        } else if (parent.getAdapter() != null && (position == parent.getAdapter().getItemCount() - 1)) {
            outRect.right = mSpace;
        }
    }
}
