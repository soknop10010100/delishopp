package com.proapp.sompom.decorataor;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.adapter.newadapter.AbsConciergeProductItemDisplayAdapter;
import com.proapp.sompom.databinding.LayoutLoadingBinding;
import com.proapp.sompom.viewholder.BindingViewHolder;

import java.util.ArrayList;
import java.util.List;

public class ConciergeMixedGridSpacingAndFullSpanItemDecoration extends RecyclerView.ItemDecoration {

    private int spanCount;
    private int spacing;
    private boolean includeEdge;
    private AbsConciergeProductItemDisplayAdapter mAbsConciergeProductItemDisplayAdapter;
    private List<Integer> mIgnoreApplyingSpacePositionCountList = new ArrayList<>();
    private List<Integer> mAppliedGridSpaceItemCount = new ArrayList<>();
    private int mSpannedItemCountBeforeDisplayingExtraItemSection;
    private boolean mShouldCheckToAddBottomSpaceForLastItem;
    private boolean mIsAlreadyFoundSpannedItemCountBeforeDisplayingExtraItemSection;
    private int mSpaceForLastRow;

    public ConciergeMixedGridSpacingAndFullSpanItemDecoration(AbsConciergeProductItemDisplayAdapter adapter,
                                                              int spanCount,
                                                              int spacing,
                                                              int spaceForLastRow,
                                                              boolean includeEdge) {
        this.mAbsConciergeProductItemDisplayAdapter = adapter;
        this.spanCount = spanCount;
        this.spacing = spacing;
        this.includeEdge = includeEdge;
        this.mShouldCheckToAddBottomSpaceForLastItem = true;
        this.mSpaceForLastRow = spaceForLastRow;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, RecyclerView parent, @NonNull RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
        if (position < 0) {
            return;
        }

        boolean shouldApplyGridSpacing = mAbsConciergeProductItemDisplayAdapter.shouldApplyGridSpacing(position);
//        Timber.i("Item position: " + position +
//                ", shouldApplyGridSpacing: " + shouldApplyGridSpacing +
//                ", spacing: " + spacing +
//                ", getChildLayoutPosition: " + parent.getChildLayoutPosition(view));
        if (shouldApplyGridSpacing) {
            if (!mAppliedGridSpaceItemCount.contains(position)) {
                mAppliedGridSpaceItemCount.add(position);
            }

            int previousIgnoreGridSpacingItemCount = getPreviousIgnoreGridSpacingItemCount(position);
            int validPosition = Math.abs((position - previousIgnoreGridSpacingItemCount) - mSpannedItemCountBeforeDisplayingExtraItemSection);
            int column = validPosition % spanCount; // item column

//            Timber.i("validPosition: " + validPosition
//                    + ", column: " + column +
//                    ", previousIgnoreGridSpacingItemCount: size: " + previousIgnoreGridSpacingItemCount +
//                    ", mSpannedItemCountBeforeDisplayingExtraItemSection: size: " + mSpannedItemCountBeforeDisplayingExtraItemSection);

            if (validPosition < 0) {
                return;
            }

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (validPosition < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (validPosition >= spanCount) {
                    outRect.top = spacing; // item top
                }

                if (mShouldCheckToAddBottomSpaceForLastItem) {
                    if (position >= (parent.getAdapter().getItemCount() - spanCount) &&
                            !mAbsConciergeProductItemDisplayAdapter.hasLoadMore()) {
                        //Reach last last row and no load more. So we have to bottom space.
//                    Timber.i("mSpaceForLastRow: " + mSpaceForLastRow);
                        outRect.bottom = mSpaceForLastRow;
                    }
                }
            }
        } else {
            RecyclerView.ViewHolder childViewHolder = parent.getChildViewHolder(view);
            if (!(childViewHolder instanceof BindingViewHolder && ((BindingViewHolder) childViewHolder).getBinding() instanceof LayoutLoadingBinding)) {
                if (!mIgnoreApplyingSpacePositionCountList.contains(position)) {
                    mIgnoreApplyingSpacePositionCountList.add(position);
                }
            }

            //Will check to get value only one time.
            if (!mIsAlreadyFoundSpannedItemCountBeforeDisplayingExtraItemSection &&
                    mAbsConciergeProductItemDisplayAdapter.isExtraItemSectionTitleViewItem(position)) {
                mIsAlreadyFoundSpannedItemCountBeforeDisplayingExtraItemSection = true;
                mSpannedItemCountBeforeDisplayingExtraItemSection = mAppliedGridSpaceItemCount.size() % spanCount;
            }
        }
    }

    private int getPreviousIgnoreGridSpacingItemCount(int currentPosition) {
        int count = 0;
        for (Integer index : mIgnoreApplyingSpacePositionCountList) {
            if (index < currentPosition) {
                count++;
            }
        }

        return count;
    }
}