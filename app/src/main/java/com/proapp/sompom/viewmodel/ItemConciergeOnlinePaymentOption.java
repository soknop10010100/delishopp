package com.proapp.sompom.viewmodel;

import android.widget.LinearLayout;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.R;
import com.proapp.sompom.model.concierge.ConciergeOnlinePaymentProvider;

public class ItemConciergeOnlinePaymentOption {

    private final ObservableBoolean mIsSelected = new ObservableBoolean();
    private ConciergeOnlinePaymentProvider mData;

    public ItemConciergeOnlinePaymentOption(ConciergeOnlinePaymentProvider data) {
        mData = data;
    }

    public ObservableBoolean getIsSelected() {
        return mIsSelected;
    }

    public void updateSelection(boolean isSelected) {
        mIsSelected.set(isSelected);
    }

    @BindingAdapter("setSelectedOptionBackground")
    public static void setSelectedOptionBackground(LinearLayout container, boolean isSelected) {
        if (isSelected) {
            container.setBackgroundResource(R.drawable.aba_selection_payment_option_background);
        } else {
            container.setBackground(null);
        }
    }
}
