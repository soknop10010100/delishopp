package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.content.Intent;
import android.util.TypedValue;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.intent.ConciergeReviewCheckoutIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.ConciergeCartModel;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;
import com.sompom.baseactivity.ResultCallback;

import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 6/9/21.
 */

public class ConciergeCartBottomSheetViewModel extends AbsLoadingViewModel {

    private ObservableField<String> mTotalPrice = new ObservableField<>();
    private ObservableInt mTotalItem = new ObservableInt();
    private ObservableBoolean mIsShowBottomCart = new ObservableBoolean();
    private ObservableBoolean mIsDisableCheckoutButton = new ObservableBoolean();

    private ObservableInt mCheckoutBackgroundColor = new ObservableInt();
    private ObservableInt mCheckoutTextColor = new ObservableInt();
    private ObservableInt mCheckoutCountColor = new ObservableInt();
    private ObservableInt mCheckoutCountBackgroundColor = new ObservableInt();

    private Context mContext;
    private ConciergeBottomSheetListener mListener;
    private ConciergeCartModel mCartModel;

    public ConciergeCartBottomSheetViewModel(Context context, ConciergeBottomSheetListener listener) {
        super(context);
        mContext = context;
        mListener = listener;
        getCartData();
    }

    public ObservableField<String> getTotalPrice() {
        return mTotalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.mTotalPrice.set(totalPrice);
    }

    public ObservableInt getTotalItem() {
        return mTotalItem;
    }

    public void setTotalItem(int totalItem) {
        this.mTotalItem.set(totalItem);
    }

    public ObservableBoolean getIsShowBottomCart() {
        return mIsShowBottomCart;
    }

    public void setIsShowBottomCart(boolean isShowBottomCart) {
        mIsShowBottomCart.set(isShowBottomCart);
    }

    public ObservableBoolean getIsDisableCheckoutButton() {
        return mIsDisableCheckoutButton;
    }

    public ObservableInt getCheckoutBackgroundColor() {
        return mCheckoutBackgroundColor;
    }

    public ObservableInt getCheckoutTextColor() {
        return mCheckoutTextColor;
    }

    public ObservableInt getCheckoutCountColor() {
        return mCheckoutCountColor;
    }

    public ObservableInt getCheckoutCountBackgroundColor() {
        return mCheckoutCountBackgroundColor;
    }

    public void onCheckoutClick() {
        if (!mIsDisableCheckoutButton.get()) {
            if (mContext instanceof AbsBaseActivity) {
                ((AbsBaseActivity) mContext).startActivityForResult(new ConciergeReviewCheckoutIntent(mContext),
                        new ResultCallback() {
                            @Override
                            public void onActivityResultSuccess(int resultCode, Intent data) {
                                if (resultCode == AppCompatActivity.RESULT_OK) {
                                    if (mContext instanceof AppCompatActivity) {
                                        ((AppCompatActivity) mContext).runOnUiThread(() -> {
                                            Timber.i("Finish clearing cart, refreshing cart");
                                            getCartData();
                                        });
                                    }

                                    if (mListener != null) {
                                        mListener.onDismiss();
                                        mListener.onCartCleared();
                                    }
                                }
                            }
                        });
            }
        }
    }

    public void updateCheckoutButtonStatus(boolean isDisable) {
        mIsDisableCheckoutButton.set(isDisable);

        mCheckoutTextColor.set(isDisable ?
                AttributeConverter.convertAttrToColor(mContext, R.attr.shop_list_checkout_button_disabled) :
                AttributeConverter.convertAttrToColor(mContext, R.attr.shop_list_checkout_button));

        mCheckoutBackgroundColor.set(isDisable ?
                AttributeConverter.convertAttrToColor(mContext, R.attr.shop_list_checkout_button_background_disabled) :
                AttributeConverter.convertAttrToColor(mContext, R.attr.shop_list_checkout_button_background));

        mCheckoutCountColor.set(isDisable ?
                AttributeConverter.convertAttrToColor(mContext, R.attr.shop_list_checkout_button_count_number_disabled) :
                AttributeConverter.convertAttrToColor(mContext, R.attr.shop_list_checkout_button_count_number));

        mCheckoutCountBackgroundColor.set(isDisable ?
                AttributeConverter.convertAttrToColor(mContext, R.attr.shop_list_checkout_button_count_background_disabled) :
                AttributeConverter.convertAttrToColor(mContext, R.attr.shop_list_checkout_button_count_background));
    }

    public void getCartData() {
        Timber.i("Getting cart data");
        Observable<ConciergeCartModel> ob = ConciergeCartHelper.getCart();
        BaseObserverHelper<ConciergeCartModel> helper = new BaseObserverHelper<>(mContext, ob);
        addDisposable(helper.execute(new OnCallbackListener<ConciergeCartModel>() {
            @Override
            public void onFail(ErrorThrowable ex) {
            }

            @Override
            public void onComplete(ConciergeCartModel result) {
                mCartModel = result;
                Timber.i("Get cart data complete: " + new Gson().toJson(mCartModel));
                int total = mCartModel.getTotalCartItem();
                if (mIsShowBottomCart.get()) {
                    if (total <= 0) {
                        if (mListener != null) {
                            mListener.onCheckOutViewVisibilityChanged(false);
                        }
                    }
                } else {
                    if (total > 0) {
                        if (mListener != null) {
                            mListener.onCheckOutViewVisibilityChanged(true);
                        }
                    }
                }

                mIsShowBottomCart.set(total != 0);
                if (mContext instanceof AppCompatActivity) {
                    ((AppCompatActivity) mContext).runOnUiThread(() -> {
                        Timber.i("Finish getting cart with items: " + total);
                        setTotalItem(total);
                        setTotalPrice(ConciergeHelper.getDisplayPrice(mContext, mCartModel.getTotalPrice()));
                    });
                }
            }
        }));
    }

    public interface ConciergeBottomSheetListener {
        void onDismiss();

        void onCartUpdate();

        void onCartCleared();

        void onItemAdded(ConciergeMenuItem item);

        void onItemRemoved(ConciergeMenuItem item);

        void onCheckOutViewVisibilityChanged(boolean isVisible);
    }

    @BindingAdapter("setCartItemCount")
    public static void setCartItemCount(TextView textView, int cartCount) {
        textView.setText(ConciergeHelper.getValidateProductCountForDisplay(cartCount));
        float textSize = cartCount > 99 ?
                textView.getContext().getResources().getDimensionPixelSize(R.dimen.text_small) :
                textView.getContext().getResources().getDimensionPixelSize(R.dimen.text_normal);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
    }
}
