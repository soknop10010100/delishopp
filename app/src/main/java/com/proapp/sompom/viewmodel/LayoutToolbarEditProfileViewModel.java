package com.proapp.sompom.viewmodel;

import android.content.Context;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

/**
 * Created by nuonveyo on 6/13/18.
 */

public class LayoutToolbarEditProfileViewModel extends AbsLoadingViewModel {

    //Set true unless user has changed the info in edit profile screen
    public ObservableBoolean mIsShowButtonSave = new ObservableBoolean();

    public ObservableField<String> mToolbarText = new ObservableField<>();

    public LayoutToolbarEditProfileViewModel(Context context) {
        super(context);
    }

    public void setToolbarText(String toolbarText) {
        mToolbarText.set(toolbarText);
    }

    public void onSaveButtonClick() {

    }
}
