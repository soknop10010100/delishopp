package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.ItemConciergeOrderHistoryAdaptive;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.ConciergeOrderTrackingDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 4/11/21.
 */
public class ConciergeOrderTrackingFragmentViewModel extends AbsLoadingViewModel {

    private final ConciergeOrderTrackingDataManager mDataManager;
    private final ObservableBoolean mIsRefreshing = new ObservableBoolean();

    private final ConciergeOrderTrackingFragmentViewModelListener mListener;
    private boolean mIsAlreadyLoadMore;
    private boolean mHasDataFromServer;

    public ConciergeOrderTrackingFragmentViewModel(Context context,
                                                   ConciergeOrderTrackingDataManager dataManager,
                                                   ConciergeOrderTrackingFragmentViewModelListener listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
    }

    public ObservableBoolean isRefreshing() {
        return mIsRefreshing;
    }

    public boolean isAlreadyLoadMore() {
        return mIsAlreadyLoadMore;
    }

    public void setIsAlreadyLoadMore(boolean mIsAlreadyLoadMore) {
        this.mIsAlreadyLoadMore = mIsAlreadyLoadMore;
    }

    public boolean isHasDataFromServer() {
        return mHasDataFromServer;
    }

    public void loadMore() {
        Timber.i("Load more tracking order");
        requestData(false, true);
    }

    public void requestData(boolean isRefresh, boolean isLoadMore) {
        if (isLoadMore) {
            if (!mIsAlreadyLoadMore) {
                mIsAlreadyLoadMore = true;
            } else {
                return;
            }
        }

        if (!isRefresh && !isLoadMore) {
            showLoading();
        }

        Observable<List<ConciergeOrder>> observable = mDataManager.getConciergeOrderTracking(isLoadMore);
        BaseObserverHelper<List<ConciergeOrder>> helper = new BaseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<List<ConciergeOrder>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                mIsRefreshing.set(false);
                if (ex.getCode() == 404) {
                    mHasDataFromServer = false;
                    showEmptyDataView();
                    if (mListener != null) {
                        mListener.onEmptyData();
                    }
                } else {
                    showError(ex.getMessage());
                }
            }

            @Override
            public void onComplete(List<ConciergeOrder> data) {
                hideLoading();
                mIsRefreshing.set(false);
                if (data.isEmpty() && !isLoadMore) {
                    mHasDataFromServer = false;
                    showEmptyDataView();
                    if (mListener != null) {
                        mListener.onEmptyData();
                    }
                } else {
                    mHasDataFromServer = true;
                    if (mListener != null) {
                        mListener.onLoadDataSuccess(data, isRefresh, isLoadMore, mDataManager.isCanLoadMore());
                    }
                }
            }
        }));
    }

    public void showEmptyDataView() {
        showEmptyDataError(getContext().getString(R.string.shop_order_tracking_empty_title),
                getContext().getString(R.string.shop_order_tracking_empty_message));
    }

    public void onOrderStatusClicked(ItemConciergeOrderHistoryAdaptive adaptive) {
        if (adaptive instanceof ConciergeOrder) {
            ConciergeOrder order = (ConciergeOrder) adaptive;
        }
    }

    @Override
    public void onRetryClick() {
        requestData(false, false);
    }

    public void onRefresh() {
        mIsRefreshing.set(true);
        mDataManager.resetPagination();
        requestData(true, false);
    }

    public interface ConciergeOrderTrackingFragmentViewModelListener {
        void onLoadDataSuccess(List<ConciergeOrder> data,
                               boolean isRefreshed,
                               boolean isFromLoadMore,
                               boolean isCanLoadMore);

        void onEmptyData();
    }
}
