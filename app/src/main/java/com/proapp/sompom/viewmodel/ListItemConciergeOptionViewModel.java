package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.intent.ConciergeProductDetailIntent;
import com.proapp.sompom.model.concierge.ConciergeComboChoiceSection;
import com.proapp.sompom.model.concierge.ConciergeComboItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.emun.ConciergeMenuItemOptionType;
import com.proapp.sompom.model.emun.ConciergeMenuItemType;
import com.proapp.sompom.model.result.ConciergeMenuItemOption;
import com.proapp.sompom.model.result.ConciergeMenuItemOptionSection;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.sompom.baseactivity.ResultCallback;

import timber.log.Timber;

public class ListItemConciergeOptionViewModel extends AbsLoadingViewModel {

    private final ObservableField<String> mTitle = new ObservableField<>();
    private final ObservableField<String> mDescription = new ObservableField<>();
    private final ObservableBoolean mDisplayRequiredLabel = new ObservableBoolean();

    private ConciergeMenuItemOptionSection mItemOption;
    private ConciergeComboChoiceSection mComboChoice;
    private ListItemConciergeVariationListener mListener;
    private final ConciergeMenuItemType mMenuItemType;

    public ListItemConciergeOptionViewModel(Context context,
                                            ConciergeMenuItemOptionSection itemOption,
                                            ListItemConciergeVariationListener listener) {
        super(context);
        mItemOption = itemOption;
        mListener = listener;
        mMenuItemType = ConciergeMenuItemType.ITEM;
        initData();
    }

    public ListItemConciergeOptionViewModel(Context context,
                                            ConciergeComboChoiceSection comboChoice,
                                            ListItemConciergeVariationListener listener) {
        super(context);
        mComboChoice = comboChoice;
        mListener = listener;
        mMenuItemType = ConciergeMenuItemType.COMBO;
        initData();
    }

    public String getRequiredLabel() {
        return "(" + mContext.getString(R.string.shop_item_detail_section_required_label) + ")";
    }

    public boolean isRequiredSectionOption() {
        //Mean that user has to select option as mandatory
        if (mMenuItemType == ConciergeMenuItemType.ITEM) {
            return ConciergeMenuItemOptionType.from(mItemOption.getType()) == ConciergeMenuItemOptionType.VARIATION;
        } else return mMenuItemType == ConciergeMenuItemType.COMBO;
    }

    public ObservableBoolean getDisplayRequiredLabel() {
        return mDisplayRequiredLabel;
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    public ObservableField<String> getDescription() {
        return mDescription;
    }

    private void initData() {
        if (mMenuItemType == ConciergeMenuItemType.ITEM) {
            mTitle.set(mItemOption.getName());
            mDescription.set(mItemOption.getDescription());
        } else if (mMenuItemType == ConciergeMenuItemType.COMBO) {
            mTitle.set(mComboChoice.getTitle());
        }
        mDisplayRequiredLabel.set(isRequiredSectionOption());
    }

    public void onItemOptionClick(ConciergeMenuItemOption selectedOption) {
        // Loop through all option in variation
        for (int index = 0; index < mItemOption.getOptionItems().size(); index++) {
            if (selectedOption.areItemsTheSame(mItemOption.getOptionItems().get(index))) {
                ConciergeMenuItemOptionType optionType = ConciergeMenuItemOptionType.from(mItemOption.getType());
                if (optionType == ConciergeMenuItemOptionType.VARIATION) {
                    for (int j = 0; j < mItemOption.getOptionItems().size(); j++) {
                        if (j == index) continue;
                        mItemOption.getOptionItems().get(j).setIsSelected(false);
                    }
                    mItemOption.getOptionItems().get(index).setIsSelected(selectedOption.isSelected());
                    break;
                } else if (optionType == ConciergeMenuItemOptionType.ADDON) {
                    mItemOption.getOptionItems().set(index, selectedOption);
                    break;
                }
                break;
            }
        }
        if (mListener != null) {
            mListener.onItemOptionUpdated(mItemOption);
        }
    }

    public void onComboChoiceClick(ConciergeComboItem comboItem, String preSelectedOptionId) {
        // Check if the combo item, which is a menu item, has options. If so, open the product detail
        // screen, else just update selection status of the combo item

        Integer selectedComboIndex = null;

        // Reset other combo item selection
        for (int index = 0; index < mComboChoice.getList().size(); index++) {
            if (comboItem.areItemsTheSame(mComboChoice.getList().get(index))) {
                for (int j = 0; j < mComboChoice.getList().size(); j++) {
                    if (j == index) continue;
                    mComboChoice.getList().get(j).setIsSelected(false);
                    mComboChoice.getList().get(j).clearComboChoiceSelection();
                }
                mComboChoice.getList().get(index).setIsSelected(comboItem.isSelected());
                selectedComboIndex = index;
                break;
            }
        }

        if (comboItem.getOptions() != null && !comboItem.getOptions().isEmpty()) {
            if (mContext instanceof AbsBaseActivity) {
                Integer finalSelectedComboIndex = selectedComboIndex;
                if (TextUtils.isEmpty(preSelectedOptionId)) {
                    ((AbsBaseActivity) mContext).startActivityForResult(
                            new ConciergeProductDetailIntent(mContext, comboItem, true),
                            new ResultCallback() {
                                @Override
                                public void onActivityResultSuccess(int resultCode, Intent data) {
                                    ConciergeMenuItem menuItem = data.getParcelableExtra(ConciergeProductDetailIntent.MENU_ITEM);
                                    Timber.i("What was returned : " + new Gson().toJson(menuItem) + "\n finalSelectedComboIndex: " + finalSelectedComboIndex);
                                    if (menuItem != null) {
                                        // Update the inner combo item selection
                                        if (finalSelectedComboIndex != null) {
                                            Timber.i(" mComboChoice.getList().get(finalSelectedComboIndex): " + new Gson().toJson(mComboChoice.getList().get(finalSelectedComboIndex)));
                                            mComboChoice.getList().get(finalSelectedComboIndex).setOptions(menuItem.getOptions());
                                            mComboChoice.getList().get(finalSelectedComboIndex).calculateComboItemPrice();
                                            mComboChoice.getList().get(finalSelectedComboIndex).setIsSelected(true);
                                        }

                                        if (mListener != null) {
                                            mListener.onComboItemUpdated(mComboChoice);
                                        }
                                    }
                                }

                                @Override
                                public void onActivityResultFail() {
                                    // This callback is called when the the activity finished without result ok,
                                    // like when user press the back button
                                    super.onActivityResultFail();
                                }
                            });
                } else {
                    //Make the selection for edit mode
                    if (finalSelectedComboIndex != null) {
                        for (ConciergeMenuItemOptionSection optionSection : mComboChoice.getList().get(finalSelectedComboIndex).getOptions()) {
                            if (optionSection.getOptionItems() != null) {
                                for (ConciergeMenuItemOption optionItem : optionSection.getOptionItems()) {
                                    if (TextUtils.equals(optionItem.getId(), preSelectedOptionId)) {
                                        optionItem.setIsSelected(true);
                                    }
                                }
                            }
                        }
                        mComboChoice.getList().get(finalSelectedComboIndex).calculateComboItemPrice();
                        mComboChoice.getList().get(finalSelectedComboIndex).setIsSelected(true);
                    }

                    if (mListener != null) {
                        mListener.onComboItemUpdated(mComboChoice);
                    }
                }
            }
        } else {
            if (mListener != null) {
                mListener.onComboItemUpdated(mComboChoice);
            }
        }
    }

    public interface ListItemConciergeVariationListener {
        void onItemOptionUpdated(ConciergeMenuItemOptionSection itemOption);

        void onComboItemUpdated(ConciergeComboChoiceSection comboSection);
    }
}
