package com.proapp.sompom.viewmodel;

import android.content.Context;

import androidx.databinding.ObservableField;

import com.proapp.sompom.model.concierge.ConciergeBrand;

/**
 * Created by Or Vitovongsak on 5/3/22.
 */
public class ListItemConciergeBrandViewModel extends AbsLoadingViewModel {

    private final ObservableField<String> mIcon = new ObservableField<>();
    private ConciergeBrand mBrand;

    public ListItemConciergeBrandViewModel(Context context, ConciergeBrand brand) {
        super(context);
        mBrand = brand;
        mIcon.set(mBrand.getImage());
    }

    public ObservableField<String> getIcon() {
        return mIcon;
    }
}
