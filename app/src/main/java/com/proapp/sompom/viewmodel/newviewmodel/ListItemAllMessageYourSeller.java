package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableField;

import com.proapp.sompom.model.result.User;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;
import com.proapp.sompom.listener.OnMessageItemClick;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class ListItemAllMessageYourSeller extends AbsBaseViewModel {
    public ObservableField<User> mUser = new ObservableField<>();
    private OnMessageItemClick mOnMessageItemClick;


    public ListItemAllMessageYourSeller(User user, OnMessageItemClick messageItemClick) {
        mUser.set(user);
        mOnMessageItemClick = messageItemClick;
    }

    public void onItemClick() {
        if (mOnMessageItemClick != null) {
            mOnMessageItemClick.onConversationUserClick(mUser.get());
        }
    }

    public User getUser() {
        return mUser.get();
    }
}
