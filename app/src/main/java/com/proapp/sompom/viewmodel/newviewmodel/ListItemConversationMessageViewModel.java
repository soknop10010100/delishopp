package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;

import androidx.core.content.ContextCompat;
import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.DateTimeUtils;
import com.proapp.sompom.utils.SpecialTextRenderUtils;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

/**
 * Created by Veasna Chhom on 8/7/20.
 */

public class ListItemConversationMessageViewModel extends AbsBaseViewModel {

    private ObservableField<User> mUser = new ObservableField<>();
    private ObservableField<String> mSenderName = new ObservableField<>();
    private Conversation mConversation;
    private Chat mChatMessage;
    private Context mContext;
    private String mSearchKeyWord;
    private Spannable mSpannableContent;

    public ListItemConversationMessageViewModel(Context context,
                                                Conversation conversation,
                                                String searchKeyWord,
                                                Chat chatMessage) {
        mContext = context;
        mSearchKeyWord = searchKeyWord;
        mConversation = conversation;
        mChatMessage = chatMessage;
        bindMessageData();
    }

    public ObservableField<String> getSenderName() {
        return mSenderName;
    }

    public ObservableField<User> getUser() {
        return mUser;
    }

    private void bindMessageData() {
        if (mChatMessage.getSender() != null) {
            mUser.set(mChatMessage.getSender());
            mSenderName.set(mChatMessage.getSender().getFullName());
        }
    }

    private Spannable getDescription() {
        if (TextUtils.isEmpty(mChatMessage.getContent())) {
            return SpecialTextRenderUtils.renderMentionUser(mContext,
                    mChatMessage.getActualContent(mContext,
                            mConversation,
                            true),
                    false,
                    false,
                    getMentionUserColor(),
                    -1, -1, true,
                    null);
        }

        return SpecialTextRenderUtils.renderMentionUser(mContext,
                mChatMessage.getContent(),
                false,
                false,
                getMentionUserColor(),
                -1, -1, false, null);
    }

    private int getMentionUserColor() {
        return ContextCompat.getColor(mContext, R.color.darkGrey);
    }

    public Spannable getValidDescription() {
        mSpannableContent = getDescription();
        if (mSpannableContent == null ||
                TextUtils.isEmpty(mSpannableContent) ||
                mSpannableContent.toString().equalsIgnoreCase("null")) {
            mSpannableContent = new SpannableString("");
        }
        injectHighLightSearchKeyword();
        return mSpannableContent;
    }

    private void injectHighLightSearchKeyword() {
        /*
        Add background high light to search keyword of chat content if the chat type was in search
        mode. And if it is in search mode, that their must be search keyword to pass along this chat.
        */
        if (!TextUtils.isEmpty(mSearchKeyWord)) {
            SpecialTextRenderUtils.buildHighLightBackgroundKeyWordSpan(mSpannableContent,
                    mSearchKeyWord,
                    AttributeConverter.convertAttrToColor(mContext, R.attr.search_high_light_word_background),
                    null);
        }
    }

    public String getFormatDate() {
        if (mChatMessage.getDate() != null) {
            return DateTimeUtils.parse(mContext, mChatMessage.getDate().getTime(), false, null);
        } else {
            return "";
        }
    }
}
