package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.text.TextUtils;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.model.concierge.ConciergeSupplier;

/**
 * Created by Chhom Veasna on 5/10/22.
 */
public class ListItemConciergeSupplierViewModel extends AbsLoadingViewModel {

    private final ObservableField<String> mIcon = new ObservableField<>();
    private final ObservableField<String> mTitle = new ObservableField<>();
    private final ObservableField<String> mAddress = new ObservableField<>();
    private final ObservableField<String> mEstimateDeliveryTime = new ObservableField<>();
    private final ObservableBoolean mIsSupplierClosed = new ObservableBoolean();
    private final ObservableField<String> mNextAvailableOpenSupplierTime = new ObservableField<>();

    private final ConciergeSupplier mSupplier;

    public ListItemConciergeSupplierViewModel(Context context, ConciergeSupplier supplier) {
        super(context);
        mSupplier = supplier;
        mIsSupplierClosed.set(!supplier.isShopOpen());
        mIcon.set(mSupplier.getLogo());
        mTitle.set(mSupplier.getSupplierName());
        mNextAvailableOpenSupplierTime.set(supplier.getAvailableDay());
        if (!TextUtils.isEmpty(mSupplier.getAddress())) {
            mAddress.set(mSupplier.getAddress());
        } else {
            mAddress.set(ConciergeHelper.N_A);
        }
        int estimateTimeDelivery = ConciergeHelper.getEstimateTimeDelivery(context, mSupplier);
        if (estimateTimeDelivery > 0) {
            if (estimateTimeDelivery > 1) {
                mEstimateDeliveryTime.set(context.getString(R.string.common_estimate_time_abbreviation_label) + " " +
                        estimateTimeDelivery +
                        context.getString(R.string.common_timestamp_plural_abbreviation_minute_label));
            } else {
                mEstimateDeliveryTime.set(context.getString(R.string.common_estimate_time_abbreviation_label) + " " +
                        estimateTimeDelivery +
                        context.getString(R.string.common_timestamp_single_abbreviation_minute_label));
            }
        } else {
            mEstimateDeliveryTime.set(ConciergeHelper.N_A);
        }
    }

    public ObservableField<String> getNextAvailableOpenSupplierTime() {
        return mNextAvailableOpenSupplierTime;
    }

    public ObservableBoolean getIsSupplierClosed() {
        return mIsSupplierClosed;
    }

    public ObservableField<String> getIcon() {
        return mIcon;
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    public ObservableField<String> getAddress() {
        return mAddress;
    }

    public ObservableField<String> getEstimateDeliveryTime() {
        return mEstimateDeliveryTime;
    }
}
