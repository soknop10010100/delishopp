package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.text.Spannable;
import android.view.View;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.utils.AttributeConverter;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 3/5/22.
 */
public class AbsConciergeProductViewModel extends AbsLoadingViewModel {

    protected final ConciergeMenuItem mMenuItem;
    protected final ObservableField<String> mProductName = new ObservableField<>();
    protected final ObservableField<String> mProductWeight = new ObservableField<>();
    protected final ObservableField<Spannable> mPrice = new ObservableField<>();
    protected final ObservableField<String> mProductPicture = new ObservableField<>();
    protected final ObservableField<String> mBrandLogo = new ObservableField<>();
    protected final ObservableBoolean mAddButtonVisibility = new ObservableBoolean(true);

    public AbsConciergeProductViewModel(Context context, ConciergeMenuItem menuItem) {
        super(context);
        mMenuItem = menuItem;
        Timber.i("isInStock: " + menuItem.isInStock() + ", isSameDay: " + menuItem.isSameDay());
        mAddButtonVisibility.set(ConciergeHelper.isAddProuctButtonVisible(mMenuItem));
    }

    public ObservableBoolean getAddButtonVisibility() {
        return mAddButtonVisibility;
    }

    public ConciergeMenuItem getMenuItem() {
        return mMenuItem;
    }

    public ObservableField<String> getProductName() {
        return mProductName;
    }

    public ObservableField<String> getProductWeight() {
        return mProductWeight;
    }

    public ObservableField<Spannable> getPrice() {
        return mPrice;
    }

    public ObservableField<String> getProductPicture() {
        return mProductPicture;
    }

    public ObservableField<String> getBrandLogo() {
        return mBrandLogo;
    }

    protected void initData(ConciergeMenuItem product) {
        mProductName.set(product.getName());
        mProductWeight.set(product.getWeight());
        mProductPicture.set(product.getPicture());
        mPrice.set(ConciergeHelper.buildShopItemNormalOrDiscountPriceDisplay(mContext,
                product,
                AttributeConverter.convertAttrToColor(mContext, R.attr.common_primary_color),
                AttributeConverter.convertAttrToColor(mContext, R.attr.shop_detail_menu_item_strikethroug_price),
                true));
        if (product.getConciergeBrand() != null) {
            mBrandLogo.set(product.getConciergeBrand().getLogo());
        }
    }

    @BindingAdapter({"setProduct"})
    public static void setProductInfoLabelMode(TextView textView, ConciergeMenuItem menuItem) {
        if (menuItem != null) {
            //Manage out of stock or get same day
            if (!menuItem.isInStock()) {
                textView.setVisibility(View.VISIBLE);
                textView.getBackground().setTint(AttributeConverter.convertAttrToColor(textView.getContext(), R.attr.shop_item_out_of_stock_background));
                textView.setTextColor(AttributeConverter.convertAttrToColor(textView.getContext(), R.attr.shop_item_out_of_stock));
                textView.setText(textView.getResources().getString(R.string.shop_item_out_of_stock));
            } else if (!MainApplication.isInExpressMode()) {
                textView.setTextColor(AttributeConverter.convertAttrToColor(textView.getContext(), R.attr.shop_checkout_item_get_same_day_text));
                if (menuItem.isSameDay()) {
                    textView.setVisibility(View.VISIBLE);
                    textView.getBackground().setTint(AttributeConverter.convertAttrToColor(textView.getContext(), R.attr.shop_checkout_item_get_same_day_background));
                    textView.setText(textView.getResources().getString(R.string.shop_item_get_same_day));
                } else if (menuItem.isGetTomorrow()) {
                    textView.setVisibility(View.VISIBLE);
                    textView.getBackground().setTint(AttributeConverter.convertAttrToColor(textView.getContext(), R.attr.shop_checkout_item_get_tomorrow_background));
                    textView.setText(textView.getResources().getString(R.string.shop_item_get_tomorrow));
                } else {
                    textView.setVisibility(View.INVISIBLE);
                }
            } else {
                textView.setVisibility(View.INVISIBLE);
            }
        } else {
            textView.setVisibility(View.INVISIBLE);
        }
    }
}
