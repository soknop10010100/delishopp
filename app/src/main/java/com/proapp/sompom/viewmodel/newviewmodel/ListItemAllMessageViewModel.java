package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.content.Intent;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.listener.OnMessageItemClick;
import com.proapp.sompom.model.emun.ConversationType;
import com.proapp.sompom.model.emun.SegmentedControlItem;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.Theme;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.DateTimeUtils;
import com.proapp.sompom.utils.SpecialTextRenderUtils;
import com.sompom.baseactivity.ResultCallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import timber.log.Timber;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class ListItemAllMessageViewModel extends ListItemMessageRelatedProduct {
    public final ObservableBoolean mIsRead = new ObservableBoolean();
    private ObservableBoolean mIsJoinAvailable = new ObservableBoolean();
    private ObservableBoolean mIsVideoJoinCall = new ObservableBoolean();
    private ObservableInt mNewMessageVisibility = new ObservableInt(View.GONE);
    private ObservableField<String> mNewMessageBadgeCount = new ObservableField<>();
    private ObservableField<String> mGroupImageUrl = new ObservableField<>();
    private ObservableField<String> mName = new ObservableField<>();
    private ObservableField<String> mGroupName = new ObservableField<>();
    private ObservableField<List<User>> mParticipants = new ObservableField<>();
    private ObservableBoolean mIsSupportConversation = new ObservableBoolean();

    private final Conversation mConversation;
    private final Context mContext;
    private final OnMessageItemClick mOnMessageItemClick;
    private ListItemAllMessageViewModelListener mListener;

    public ListItemAllMessageViewModel(Context context,
                                       Conversation conversation,
                                       OnMessageItemClick messageItemClick,
                                       SegmentedControlItem item) {
        super(conversation, item);
        mContext = context;
        mConversation = conversation;
        mIsSupportConversation.set(ConversationType.fromValue(mConversation.getType()) == ConversationType.SUPPORT
                || ConversationType.fromValue(mConversation.getType()) == ConversationType.SUPPORT_TELEGRAM);
        mOnMessageItemClick = messageItemClick;
        checkUpdateIsRedStatus();
        bindConversationName(conversation);
        checkJoinGroupCallStatus();
    }

    public Conversation getConversation() {
        return mConversation;
    }

    public void setListener(ListItemAllMessageViewModelListener listener) {
        mListener = listener;
    }

    private void checkJoinGroupCallStatus() {
        if (mConversation.isChannelOpen()) {
            setIsJoinAvailable(true, mConversation.isVideo());
        } else {
            setIsJoinAvailable(false, mConversation.isVideo());
        }
    }

    public void updateJoinGroupCallStatus(boolean isJoinAvailable, boolean isVideo) {
        mConversation.setChannelOpen(isJoinAvailable);
        mConversation.setVideo(isVideo);
        checkJoinGroupCallStatus();
    }

    public ObservableBoolean getIsVideoJoinCall() {
        return mIsVideoJoinCall;
    }

    public ObservableInt getNewMessageVisibility() {
        return mNewMessageVisibility;
    }

    public ObservableField<String> getNewMessageBadgeCount() {
        return mNewMessageBadgeCount;
    }

    public ObservableBoolean getIsJoinAvailable() {
        return mIsJoinAvailable;
    }

    public ObservableField<String> getGroupImageUrl() {
        return mGroupImageUrl;
    }

    public ObservableField<String> getMessageName() {
        return mName;
    }

    public ObservableField<String> getGroupName() {
        return mGroupName;
    }

    public ObservableField<List<User>> getParticipants() {
        return mParticipants;
    }

    public ObservableBoolean getIsSupportConversation() {
        return mIsSupportConversation;
    }

    private void setIsJoinAvailable(boolean isJoinAvailable, boolean isVideo) {
        mIsJoinAvailable.set(isJoinAvailable);
        mIsVideoJoinCall.set(isVideo);
        if (isJoinAvailable) {
            mNewMessageVisibility.set(View.GONE);
        } else {
            checkUpdateIsRedStatus();
        }
    }

    public AbsCallService.CallType getJoinGroupCallType() {
        if (mConversation.isVideo()) {
            return AbsCallService.CallType.GROUP_VIDEO;
        }
        return AbsCallService.CallType.GROUP_VOICE;
    }

    public final void onJoinCallClick() {
        CallingIntent callingIntent = CallingIntent.getJoinGroupCallIntent(mContext,
                getJoinGroupCallType(),
                mConversation,
                CallingIntent.StartFromType.CONVERSATION.getType());
        ((AbsBaseActivity) mContext).startActivityForResult(callingIntent, new ResultCallback() {
            @Override
            public void onActivityResultSuccess(int resultCode, Intent data) {
            }
        });
    }

    private void checkUpdateIsRedStatus() {
        if (mConversation != null) {
            mIsRead.set(mConversation.isRead());
            // Integrating unread badge into read logic
            if (!mIsRead.get()) {
                if (mConversation.getUnreadCount() == 0) {
                    mNewMessageVisibility.set(View.GONE);
                } else if (mConversation.getUnreadCount() == 1) {
                    mNewMessageBadgeCount.set(mContext.getString(R.string.chat_info_badge_new));
                    mNewMessageVisibility.set(View.VISIBLE);
                } else {
                    String messageCount = "";
                    Timber.e("Actual count: " + mConversation.getUnreadCount());
                    if (mConversation.getUnreadCount() > 99) {
                        messageCount = "99+";
                    } else {
                        messageCount = String.valueOf(mConversation.getUnreadCount());
                    }
                    mNewMessageBadgeCount.set(messageCount);
                    mNewMessageVisibility.set(View.VISIBLE);
                }
            } else {
                mNewMessageBadgeCount.set("");
                mNewMessageVisibility.set(View.GONE);
            }
        }
    }

    private int getNewBadgeCount() {
        if (mNewMessageVisibility.get() == View.VISIBLE) {
            return mConversation.getUnreadCount();
        }

        return 0;
    }

    private void bindConversationName(Conversation conversation) {
        if (conversation != null) {
            if (!isGroupConversation(conversation)) {
                User recipient = conversation.getOneToOneRecipient(mContext);
                if (recipient != null) {
                    checkUpdateOnlineStatusFromRemote(recipient);
                    mParticipants.set(Collections.singletonList(recipient));
                    mName.set(UserHelper.getValidDisplayName(recipient.getFullName()));
                }
            } else {
                if (mIsSupportConversation.get()) {
                    Theme theme = UserHelper.getSelectedThemeFromAppSetting(mContext);
                    if (theme != null && !TextUtils.isEmpty(theme.getCompanyAvatar())) {
                        mGroupImageUrl.set(theme.getCompanyAvatar());
                    }
                } else {
                    mGroupImageUrl.set(conversation.getGroupImageUrl());
                }
                mGroupName.set(conversation.getGroupName());
                mName.set(UserHelper.getValidDisplayName(conversation.getGroupName()));
                if (conversation.getParticipants() != null) {
                    mParticipants.set(new ArrayList<>(conversation.getParticipants()));
                }
            }
        }
    }

    private void checkUpdateOnlineStatusFromRemote(User localUser) {
        if (mConversation.getParticipants() != null) {
            for (User participant : mConversation.getParticipants()) {
                if (participant.getId().matches(localUser.getId())) {
                    localUser.setOnline(participant.isOnline());
                    break;
                }
            }
        }
    }

    private boolean isGroupConversation(Conversation conversation) {
        return conversation != null && !TextUtils.isEmpty(conversation.getGroupId());
    }

    private Spannable getDescription() {
        if (TextUtils.isEmpty(mConversation.getContent(mContext, false))) {
            if (mConversation.getLastMessage() != null) {
                return SpecialTextRenderUtils.renderMentionUser(mContext,
                        mConversation.getLastMessage().getActualContent(mContext, mConversation,
                                true),
                        false,
                        false,
                        getMentionUserColor(),
                        -1, -1, true,
                        null);
            } else {
                return new SpannableString("");
            }
        }

        return SpecialTextRenderUtils.renderMentionUser(mContext,
                mConversation.getContent(mContext,
                        true),
                false,
                false,
                getMentionUserColor(),
                -1, -1, false, null);
    }

    private int getMentionUserColor() {
        if (mIsRead.get()) {
            return ContextCompat.getColor(mContext, R.color.darkGrey);
        } else {
            return AttributeConverter.convertAttrToColor(mContext, R.attr.colorFirstText);
        }
    }

    private boolean isConsiderAsNewGroupConversation() {
        return mConversation != null && mConversation.isGroupHasInvalidLastMessage();
    }

    public Spannable getValidDescription() {
        if (isConsiderAsNewGroupConversation()) {
            return new SpannableString(mContext.getString(R.string.chat_info_create_new_group,
                    mConversation.getGroupName()));
        }

        Spannable description = getDescription();
        if (description == null ||
                TextUtils.isEmpty(description) ||
                description.toString().equalsIgnoreCase("null")) {
            return new SpannableString("");
        }

        return new SpannableString(description);
    }

    public String getFormatDate() {
        if (mConversation.getDate() != null) {
            return DateTimeUtils.parse(mContext, mConversation.getDate().getTime(), false, null);
        } else {
            return "";
        }
    }


    public void onItemClick() {
        //Must call this statement first before other else.
        int newBadgeCount = getNewBadgeCount();
        updateToReadStatusMessage();
        if (mListener != null) {
            mListener.onItemClicked(mConversation.getId());
        }
        mOnMessageItemClick.onConversationClick(mConversation, newBadgeCount);
    }

    public void updateToReadStatusMessage() {
        if (!mIsRead.get()) {
            mIsRead.set(true);
            mNewMessageVisibility.set(View.GONE);
            mConversation.setUnreadCount(0);
            ConversationDb.save(mContext, mConversation, "updateToReadStatusMessage");
        }
    }

    public View.OnLongClickListener onItemLongClick() {
        return view -> {
            mOnMessageItemClick.onConversationLongClick(mConversation);
            return false;
        };
    }

    public interface ListItemAllMessageViewModelListener {
        void onItemClicked(String conversationId);
    }
}
