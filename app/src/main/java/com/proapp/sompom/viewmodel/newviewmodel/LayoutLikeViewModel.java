package com.proapp.sompom.viewmodel.newviewmodel;

import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.listener.OnLayoutLikeListener;
import com.proapp.sompom.listener.OnLikeItemListener;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.LikeViewerDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class LayoutLikeViewModel extends AbsLoadingViewModel implements OnLikeItemListener {
    private final LikeViewerDataManager mDataManager;
    private final String mProductId;
    private final OnLayoutLikeListener mOnCallback;
    private LayoutLikeViewModelListener mListener;

    public LayoutLikeViewModel(String productID,
                               LikeViewerDataManager dataManager,
                               OnLayoutLikeListener onCallback,
                               LayoutLikeViewModelListener listener) {
        super(dataManager.getContext());
        mProductId = productID;
        mListener = listener;
        mDataManager = dataManager;
        mOnCallback = onCallback;
    }

    public final void onBackClick() {
        if (mListener != null) {
            mListener.onButtonBackClicked();
        }
    }

    public void getData() {
        showLoading();
        Observable<List<User>> callback = mDataManager.getUserWhoLikeProduct(mProductId);
        BaseObserverHelper<List<User>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<List<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    showNetworkError();
                } else {
                    showError(ex.toString());
                }
            }

            @Override
            public void onComplete(List<User> result) {
                hideLoading();
                mOnCallback.onComplete(result, mDataManager.isCanLoadMore());
            }
        }));
    }

    public void loadMore(OnCallbackListListener<List<User>> callbackListener) {
        Observable<List<User>> callback = mDataManager.getUserWhoLikeProduct(mProductId);
        BaseObserverHelper<List<User>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<List<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                callbackListener.onFail(ex);
            }

            @Override
            public void onComplete(List<User> result) {
                callbackListener.onComplete(result, mDataManager.isCanLoadMore());
            }
        }));
    }

    @Override
    public void onFollowClick(User user, boolean isFollow) {
        Observable<Response<Object>> callback;
        if (isFollow) {
            callback = mDataManager.postFollow(user.getId());
        } else {
            callback = mDataManager.unFollow(user.getId());
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute());
    }

    @Override
    public void onNotifyItem() {
        mOnCallback.onNotifyItem();
    }

    public OnClickListener onRetryButtonClick() {
        return this::getData;
    }

    public interface LayoutLikeViewModelListener {
        void onButtonBackClicked();
    }
}
