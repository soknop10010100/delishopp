package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.TextUtils;

import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.result.CustomMessageResponse;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ForgotPasswordDataManager;
import com.proapp.sompom.utils.HTTPResponse;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.SpecialTextRenderUtils;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import io.reactivex.Observable;
import retrofit2.Response;

public class ForgotPasswordViewModel extends AbsLoadingViewModel {

    private ObservableField<String> mEmail = new ObservableField<>();
    private ObservableField<String> mEmailError = new ObservableField<>();
    private ForgotPasswordDataManager mDataManager;
    private ForgotPasswordViewModelListener mListener;

    public ForgotPasswordViewModel(Context context,
                                   ForgotPasswordDataManager dataManager,
                                   ForgotPasswordViewModelListener listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
    }

    public ObservableField<String> getEmail() {
        return mEmail;
    }

    public ObservableField<String> getEmailError() {
        return mEmailError;
    }

    public boolean isAllFieldValid() {
        boolean valid = true;

        if (TextUtils.isEmpty(mEmail.get())) {
            mEmailError.set(mDataManager.getContext().getString(R.string.login_error_field_required));
            mEmailError.notifyChange();
            valid = false;
        }

        if (!TextUtils.isEmpty(mEmail.get()) && !SpecialTextRenderUtils.isEmailAddress(mEmail.get())) {
            mEmailError.set(mDataManager.getContext().getString(R.string.edit_profile_invalid_email_title));
            mEmailError.notifyChange();
            valid = false;
        }

        return valid;
    }

    public void requestForgotPassword() {
        //To make sure, there will be no access token applied to this API since it is the public one.
        SharedPrefUtils.setAccessToken(null, getContext());
        Observable<Response<CustomMessageResponse>> loginService = mDataManager.getForgotPasswordService(mEmail.get());
        ResponseObserverHelper<Response<CustomMessageResponse>> helper = new ResponseObserverHelper<>(mDataManager.mContext,
                loginService);
        addDisposable(helper.execute(new OnCallbackListener<Response<CustomMessageResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (ex.getCode() == HTTPResponse.NOT_FOUND) {
                    if (mListener != null) {
                        mListener.onFailed(getContext().getString(R.string.forget_password_error_email_not_found));
                    }
                } else {
                    if (mListener != null) {
                        mListener.onFailed(ex.getMessage());
                    }
                }
            }

            @Override
            public void onComplete(Response<CustomMessageResponse> result) {
                if (result.body().isSuccessful()) {
                    onForgotPasswordSuccess();
                } else {
                    if (mListener != null) {
                        mListener.onFailed(result.body().getMessage(getContext()));
                    }
                }
            }
        }));
    }

    private void onForgotPasswordSuccess() {
        if (mListener != null) {
            mListener.onSuccess();
        }
    }

    public interface ForgotPasswordViewModelListener {

        void onFailed(String error);

        void onSuccess();
    }
}
