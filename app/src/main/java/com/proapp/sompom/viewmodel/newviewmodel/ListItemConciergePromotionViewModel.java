package com.proapp.sompom.viewmodel.newviewmodel;

import com.proapp.sompom.model.concierge.ConciergeCarouselSectionResponse;

import java.util.List;

/**
 * Created by Or Vitovongsak on 31/8/21.
 */

public class ListItemConciergePromotionViewModel {

    private List<ConciergeCarouselSectionResponse.Data> mPromotions;

    public ListItemConciergePromotionViewModel(List<ConciergeCarouselSectionResponse.Data> promotions) {
        mPromotions = promotions;
    }

    public List<ConciergeCarouselSectionResponse.Data> getPromotions() {
        return mPromotions;
    }
}
