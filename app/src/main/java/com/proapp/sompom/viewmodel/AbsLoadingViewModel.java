package com.proapp.sompom.viewmodel;

import android.content.Context;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ObservableListenerBoolean;
import com.proapp.sompom.model.emun.ErrorLoadingType;
import com.proapp.sompom.newui.AbsBaseActivity;

import timber.log.Timber;

/**
 * Created by He Rotha on 11/6/17.
 */

public class AbsLoadingViewModel extends AbsBaseViewModel {

    public final ObservableField<ErrorLoadingType> mErrorLoadingType = new ObservableField<>();
    public final ObservableBoolean mIsShowButtonRetry = new ObservableBoolean(true);
    public final ObservableInt mButtonRetryText = new ObservableInt(R.string.error_retry_button);
    public final ObservableField<String> mErrorTitle = new ObservableField<>();
    public final ObservableField<String> mErrorMessage = new ObservableField<>();
    public final ObservableListenerBoolean mIsLoading = new ObservableListenerBoolean(false);
    public final ObservableListenerBoolean mIsActionLoading = new ObservableListenerBoolean(false);
    public final ObservableBoolean mIsError = new ObservableBoolean(false);
    private final ObservableBoolean mIsScreenShowing = new ObservableBoolean();
    private AbsLoadingViewModelListener mListener;

    protected final Context mContext;

    public AbsLoadingViewModel(Context context) {
        mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

    public void onRetryClick() {
        showLoading();
        if (mListener != null) {
            mListener.onRetryClicked();
        }
    }

    public void setListener(AbsLoadingViewModelListener listener) {
        mListener = listener;
    }

    protected void setShowKeyboardWhileLoadingScreen() {
        mIsLoading.setShowKeyboard(false);
    }

    public ObservableBoolean getIsScreenShowing() {
        return mIsScreenShowing;
    }

    public ObservableListenerBoolean getIsLoading() {
        return mIsLoading;
    }

    public void showError(String message, boolean showRetry) {
        showError(message);
        if (!showRetry) {
            mIsShowButtonRetry.set(false);
        }
    }

    public ObservableListenerBoolean getIsActionLoading() {
        return mIsActionLoading;
    }

    public void showNetworkError() {
        mErrorMessage.set(getContext().getString(R.string.error_internet_connection_description));
        mErrorLoadingType.set(ErrorLoadingType.GENERAL_ERROR);
        showError();
    }

    public void showError(String errorName) {
        mErrorMessage.set(errorName);
        mErrorLoadingType.set(ErrorLoadingType.GENERAL_ERROR);
        showError();
    }

    public void showNoItemError(String errorName) {
        mErrorMessage.set(errorName);
        mErrorLoadingType.set(ErrorLoadingType.NO_ITEM);
        showError();
    }

    public void showEmptyDataError(String errorTitle,
                                   String errorMessage) {
        mErrorTitle.set(errorTitle);
        mErrorMessage.set(errorMessage);
        mErrorLoadingType.set(ErrorLoadingType.EMPTY_DATA);
        showError();
    }

    public void showLoading() {
        mIsScreenShowing.set(true);
        mIsError.set(false);
        mIsLoading.set(true);
        mIsActionLoading.set(false);
        log("showLoading");
    }

    public void showLoading(boolean isAction) {
        mIsScreenShowing.set(true);
        mIsError.set(false);
        mIsActionLoading.set(isAction);
        mIsLoading.set(!isAction);
        log("showActionLoading");
    }

    public void interceptClick() {
        Timber.e("Hopefully background is unclickable");
    }

    public void hideLoading() {
        mIsError.set(false);
        mIsLoading.set(false);
        mIsActionLoading.set(false);
        mIsScreenShowing.set(false);
        log("hideLoading");
    }

    private void log(String s) {
        Timber.i(s + " mIsError " + mIsError.get());
        Timber.i(s + " mIsLoading " + mIsLoading.get());
        Timber.i(s + " mIsActionLoading " + mIsActionLoading.get());
    }

    private void showError() {
        mIsScreenShowing.set(true);
        mIsError.set(true);
        mIsLoading.set(false);
        mIsActionLoading.set(false);
        mIsShowButtonRetry.set(true);
    }

    public boolean isDisplayErrorInScreen() {
        return mIsError.get();
    }

    public void onBackButtonClick() {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).onBackPressed();
        }
    }

    public interface AbsLoadingViewModelListener {
        void onRetryClicked();
    }
}
