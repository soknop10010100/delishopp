package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableField;

/**
 * Created by nuonveyo on 8/30/18.
 */

public class LayoutChatVideoFullScreenViewModel {
    public ObservableField<String> mVideoUrl = new ObservableField<>();

    public LayoutChatVideoFullScreenViewModel(String videoUrl) {
        mVideoUrl.set(videoUrl);
    }
}
