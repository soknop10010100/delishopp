package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.amulyakhare.textdrawable.TextDrawable;
import com.github.vipulasri.timelineview.TimelineView;
import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.model.emun.ConciergeOrderStatus;
import com.proapp.sompom.model.emun.ConciergeOrderTrackingStatus;
import com.proapp.sompom.model.result.ConciergeOrderTrackingProgress;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 17/12/21.
 */
public class ItemConciergeDeliveryTrackingViewModel extends AbsBaseViewModel {

    private final ObservableInt mStartLineColor = new ObservableInt();
    private final ObservableInt mEndLineColor = new ObservableInt();
    private final ObservableInt mMarkerBackgroundColor = new ObservableInt();
    private final ObservableInt mMarkerIconColor = new ObservableInt();
    private final ObservableField<String> mMarkerIcon = new ObservableField<>();
    private final ObservableField<Drawable> mMarkerDrawable = new ObservableField<>();
    private final ObservableField<String> mStatusText = new ObservableField<>();
    private final ObservableInt mStatusTextColor = new ObservableInt();
    private final ObservableField<String> mStatusTimestamp = new ObservableField<>();
    private final ObservableInt mTimestampTextColor = new ObservableInt();
    private final ObservableBoolean mCancelButtonVisibility = new ObservableBoolean(false);

    private final Context mContext;
    private int mViewType;
    private ConciergeOrderTrackingProgress mOrderTrackingProgress;
    private ConciergeOrderTrackingStatus mTrackingStatus;
    private final ConciergeOrderStatus mOrderStatus;
    private final ItemDeliveryTrackingListener mListener;

    public ItemConciergeDeliveryTrackingViewModel(Context context,
                                                  ConciergeOrderTrackingProgress orderTrackingProgress,
                                                  ConciergeOrderStatus currentOrderStatus,
                                                  int viewType,
                                                  ItemDeliveryTrackingListener listener) {
        mContext = context;
        mOrderTrackingProgress = orderTrackingProgress;
        mOrderStatus = currentOrderStatus;
        mViewType = viewType;
        mListener = listener;

        checkToShowCancelButton();
        initTimelineItem();
    }

    public ObservableInt getStartLineColor() {
        return mStartLineColor;
    }

    public ObservableInt getEndLineColor() {
        return mEndLineColor;
    }

    public ObservableInt getMarkerBackgroundColor() {
        return mMarkerBackgroundColor;
    }

    public ObservableInt getMarkerIconColor() {
        return mMarkerIconColor;
    }

    public ObservableField<Drawable> getMarkerDrawable() {
        return mMarkerDrawable;
    }

    public ObservableField<String> getStatusText() {
        return mStatusText;
    }

    public ObservableInt getStatusTextColor() {
        return mStatusTextColor;
    }

    public ObservableField<String> getStatusTimestamp() {
        return mStatusTimestamp;
    }

    public void setViewType(int viewType) {
        this.mViewType = viewType;
    }

    public int getViewType() {
        return mViewType;
    }

    public void setOrderStatus(ConciergeOrderTrackingProgress orderTrackingProgress) {
        mOrderTrackingProgress = orderTrackingProgress;
    }

    public ConciergeOrderTrackingProgress getOrderTrackingProgress() {
        return mOrderTrackingProgress;
    }

    public ObservableInt getTimestampTextColor() {
        return mTimestampTextColor;
    }

    public ObservableBoolean getCancelButtonVisibility() {
        return mCancelButtonVisibility;
    }

    public void initTimelineItem() {
        Timber.i("Timeline item data: " + new Gson().toJson(mOrderTrackingProgress));

        mTrackingStatus = determineCurrentTrackingStatus(mOrderStatus);

        mMarkerIcon.set(mOrderTrackingProgress.getStatus().getIcon(mContext));
        mMarkerIconColor.set(mTrackingStatus.getStatusIconColor(mContext));
        mMarkerBackgroundColor.set(mTrackingStatus.getStatusIconBackgroundColor(mContext));
        mStatusTextColor.set(mTrackingStatus.getStatusTextColor(mContext));
        mTimestampTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_tracking_order_status_time));
        mStartLineColor.set(mTrackingStatus.getTopLineColor(mContext));
        mEndLineColor.set(mTrackingStatus.getBottomLineColor(mContext));

        int fontResource = R.font.uicons_regular_rounded;
        //Current order
        if (mOrderStatus == mOrderTrackingProgress.getStatus()) {
            fontResource = R.font.uicons_solid_rounded;
        }
        TextDrawable textDrawable = TextDrawable.builder()
                .beginConfig()
                .textColor(mMarkerIconColor.get())
                .useFont(ResourcesCompat.getFont(mContext, fontResource))
                .fontSize(mContext.getResources().getDimensionPixelSize(R.dimen.text_normal)) /* size in px */
                .bold()
                .toUpperCase()
                .endConfig()
                .buildRound(mMarkerIcon.get(), mMarkerBackgroundColor.get());

        mMarkerDrawable.set(textDrawable);

        mStatusText.set(mOrderTrackingProgress.getTitle());
    }

    public void checkToShowCancelButton() {
        mCancelButtonVisibility.set(mOrderStatus == ConciergeOrderStatus.PENDING &&
                mOrderTrackingProgress.getStatus() == ConciergeOrderStatus.PENDING);
    }

    public void onCancelButtonClick(View view) {
        if (mOrderStatus == ConciergeOrderStatus.PENDING &&
                mOrderTrackingProgress.getStatus() == ConciergeOrderStatus.PENDING) {
            if (mListener != null) {
                mListener.onCancelOrderClicked();
            }
        }
    }

    public ConciergeOrderTrackingStatus determineCurrentTrackingStatus(ConciergeOrderStatus currentStatus) {
        if (mOrderTrackingProgress.getDate() == null) {
            return ConciergeOrderTrackingStatus.NEXT;
        } else {
            if (mOrderTrackingProgress.getStatus() == currentStatus) {
                return ConciergeOrderTrackingStatus.CURRENT;
            } else {
                return ConciergeOrderTrackingStatus.PASSED;
            }
        }
    }

    public interface ItemDeliveryTrackingListener {
        void onCancelOrderClicked();
    }

    @BindingAdapter({"viewType", "startLineColor", "endLineColor"})
    public static void setupTimelineView(TimelineView timelineView,
                                         int viewType,
                                         int startLineColor,
                                         int endLineColor) {
        timelineView.setStartLineColor(startLineColor, viewType);
        timelineView.setEndLineColor(endLineColor, viewType);
    }
}
