package com.proapp.sompom.viewmodel;


import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

/**
 * Created by Veasna Chhom on 9/9/21.
 */
public class ItemStateSelectionViewModel {

    private final int mSelectedTextColor;
    private final int mUnSelectedTextColor;
    private final int mSelectedBackgroundColor;
    private final int mUnSelectedBackgroundColor;
    private final ObservableInt mTextColor = new ObservableInt();
    private final ObservableField<String> mDisplayValue = new ObservableField<>();
    private final ObservableInt mBackgroundColor = new ObservableInt();
    private final ObservableBoolean mIsSelected = new ObservableBoolean();

    public ItemStateSelectionViewModel(String displayText,
                                       int selectedTextColor,
                                       int unselectedTextColor,
                                       int selectedBackgroundColor,
                                       int unSelectedBackgroundColor) {
        mDisplayValue.set(displayText);
        mSelectedTextColor = selectedTextColor;
        mUnSelectedTextColor = unselectedTextColor;
        mSelectedBackgroundColor = selectedBackgroundColor;
        mUnSelectedBackgroundColor = unSelectedBackgroundColor;
    }

    public ObservableField<String> getDisplayValue() {
        return mDisplayValue;
    }

    public ObservableInt getTextColor() {
        return mTextColor;
    }

    public ObservableInt getBackgroundColor() {
        return mBackgroundColor;
    }

    public ObservableBoolean isSelected() {
        return mIsSelected;
    }

    public void updateState(boolean isSelected) {
        mIsSelected.set(isSelected);
        if (isSelected) {
            mBackgroundColor.set(mSelectedBackgroundColor);
            mTextColor.set(mSelectedTextColor);
        } else {
            mBackgroundColor.set(mUnSelectedBackgroundColor);
            mTextColor.set(mUnSelectedTextColor);
        }
    }
}
