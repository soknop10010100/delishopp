package com.proapp.sompom.viewmodel.newviewmodel;

import android.text.TextUtils;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.utils.MediaUtil;

/**
 * Created by Chhom Veasna on 7/27/2020.
 */
public class ListItemGroupPhotoViewModel {

    private ObservableField<String> mUrl = new ObservableField<>();
    private ObservableField<String> mVideoDuration = new ObservableField<>();
    protected ObservableBoolean mIsVideo = new ObservableBoolean();

    public ListItemGroupPhotoViewModel(Media media) {
        mUrl.set(getThumbnailUrl(media));
        mIsVideo.set(media.getType() == MediaType.VIDEO);
        if (mIsVideo.get()) {
            mVideoDuration.set(MediaUtil.getVideoDurationDisplayFormat(media.getDuration()));
        }
    }

    public ObservableBoolean getIsVideo() {
        return mIsVideo;
    }

    public ObservableField<String> getUrl() {
        return mUrl;
    }

    public ObservableField<String> getVideoDuration() {
        return mVideoDuration;
    }

    private String getThumbnailUrl(Media media) {
        if (!TextUtils.isEmpty(media.getThumbnail())) {
            return media.getThumbnail();
        } else {
            return media.getUrl();
        }
    }
}
