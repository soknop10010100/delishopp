package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.ShopDeliveryTimeSelection;
import com.proapp.sompom.model.concierge.ConciergeBasket;
import com.proapp.sompom.model.emun.ApplicationMode;
import com.proapp.sompom.model.emun.ConciergePaymentMethod;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ConciergeOrderHistoryDetailManager;
import com.proapp.sompom.utils.DateUtils;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;
import com.resourcemanager.helper.LocaleManager;

import java.time.format.DateTimeFormatter;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Veasna Chhom on 22/9/21.
 */
public class ConciergeOrderHistoryDetailFragmentViewModel extends AbsLoadingViewModel {

    private final ObservableField<String> mOrderTitle = new ObservableField<>();
    private final ObservableField<String> mOrderLogo = new ObservableField<>();
    private final ObservableField<String> mOrderAddress = new ObservableField<>();
    private final ObservableField<String> mOrderDate = new ObservableField<>();
    private final ObservableField<String> mOrderNumber = new ObservableField<>();
    private final ObservableField<String> mOrderStatus = new ObservableField<>();
    private final ObservableField<String> mPaymentMethod = new ObservableField<>();
    private final ObservableField<String> mDeliverySlot = new ObservableField<>();
    private final ObservableField<String> mCompanyName = new ObservableField<>();
    private final ObservableField<String> mUserName = new ObservableField<>();
    private final ObservableField<String> mDeliveryAddress = new ObservableField<>();
    private final ObservableField<String> mSubTotal = new ObservableField<>();
    private final ObservableField<String> mDeliveryCharge = new ObservableField<>();
    private final ObservableField<String> mContainerCharge = new ObservableField<>();
    private final ObservableField<String> mTotalDiscount = new ObservableField<>();
    private final ObservableField<String> mWallet = new ObservableField<>();
    private final ObservableField<String> mGrandTotal = new ObservableField<>();
    private final ObservableField<String> mDeliveryDetail = new ObservableField<>();
    private final ObservableField<String> mTime = new ObservableField<>();
    private final ObservableBoolean mIsEnableReOrder = new ObservableBoolean();
    private final ConciergeOrderHistoryDetailManager mDataManager;
    private ConciergeOrder mOrder;
    private final ConciergeOrderHistoryDetailFragmentViewModelListener mListener;
    private final ObservableInt mOrderStatusButtonBackground = new ObservableInt();
    private boolean mIsInExpressMode;

    public ConciergeOrderHistoryDetailFragmentViewModel(Context context,
                                                        ConciergeOrderHistoryDetailManager dataManager,
                                                        ConciergeOrderHistoryDetailFragmentViewModelListener listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
        mIsInExpressMode = ApplicationHelper.isInExpressMode(context);
    }

    public ObservableField<String> getDeliverySlot() {
        return mDeliverySlot;
    }

    public ObservableInt getOrderStatusButtonBackground() {
        return mOrderStatusButtonBackground;
    }

    public ObservableField<String> getCompanyName() {
        return mCompanyName;
    }

    public ObservableField<String> getUserName() {
        return mUserName;
    }

    public ObservableField<String> getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public ObservableField<String> getOrderTitle() {
        return mOrderTitle;
    }

    public ObservableField<String> getPaymentMethod() {
        return mPaymentMethod;
    }

    public ObservableField<String> getOrderLogo() {
        return mOrderLogo;
    }

    public ObservableField<String> getOrderAddress() {
        return mOrderAddress;
    }

    public ObservableField<String> getOrderDate() {
        return mOrderDate;
    }

    public ObservableField<String> getOrderNumber() {
        return mOrderNumber;
    }

    public ObservableField<String> getOrderStatus() {
        return mOrderStatus;
    }

    public ObservableField<String> getSubTotal() {
        return mSubTotal;
    }

    public ObservableField<String> getDeliveryCharge() {
        return mDeliveryCharge;
    }

    public ObservableField<String> getContainerCharge() {
        return mContainerCharge;
    }

    public ObservableField<String> getTotalDiscount() {
        return mTotalDiscount;
    }

    public ObservableField<String> getWallet() {
        return mWallet;
    }

    public ObservableField<String> getGrandTotal() {
        return mGrandTotal;
    }

    public ObservableField<String> getDeliveryDetail() {
        return mDeliveryDetail;
    }

    public ObservableField<String> getTime() {
        return mTime;
    }

    public ObservableBoolean isEnableReOrder() {
        return mIsEnableReOrder;
    }

    @Override
    public void onRetryClick() {
        requestData();
    }

    public void requestData() {
        showLoading();
        Observable<Response<ConciergeOrder>> service = mDataManager.getConciergeOrderHistoryService();
        ResponseObserverHelper<Response<ConciergeOrder>> helper = new ResponseObserverHelper<>(getContext(), service);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeOrder>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.i("onFail: " + ex.getMessage());
                hideLoading();
                showError(ex.getMessage());
            }

            @Override
            public void onComplete(Response<ConciergeOrder> result) {
                mOrder = result.body();
                Timber.i("onComplete: " + new Gson().toJson(result.body()));
                bindData();
                if (mListener != null) {
                    mListener.onLoadDataSuccess(result.body());
                }
            }
        }));
    }

    private void bindData() {
        mOrderTitle.set(mOrder.getOrderTitle());
        mOrderAddress.set(mOrder.getOrderDescription());
        mOrderLogo.set(mOrder.getOrderProfile());
        mOrderNumber.set("#" + mOrder.getOrderNumber());
        mOrderStatus.set(mOrder.getOrderStatus().getLabel(mContext));

        if (mOrder.getCreatedAt() != null) {
            String dateWithFormat = DateUtils.getDateWithFormat(mContext,
                    mOrder.getCreatedAt().getTime(),
                    DateUtils.DATE_FORMAT_23,
                    LocaleManager.getLocale(mContext.getResources()));
            mOrderDate.set(dateWithFormat);
        }

        mUserName.set(mOrder.getUserName());
        mTotalDiscount.set(ConciergeHelper.getDisplayPrice(mContext, mOrder.getTotalDiscount()));
        mWallet.set(ConciergeHelper.getDisplayPrice(mContext, mOrder.getWalletDiscount()));
        mDeliveryCharge.set(ConciergeHelper.getDisplayPrice(mContext, mOrder.getDeliveryCharge()));
        mContainerCharge.set(ConciergeHelper.getDisplayPrice(mContext, mOrder.getContainerFee()));
        mSubTotal.set(ConciergeHelper.getDisplayPrice(mContext, mOrder.getSubTotal()));
        mGrandTotal.set(ConciergeHelper.getDisplayPrice(mContext, mOrder.getGrandTotal()));

        //Bind time slot
        if (ConciergeHelper.isSelectedBasketTimeSlotValid(mOrder.getTimeSlot())) {
            ShopDeliveryTimeSelection timeSelection = ConciergeHelper.buildShopDeliveryTimeSelectionFromBasketTimeSlot(mOrder.getTimeSlot());
            String deliverySlot = timeSelection.getSelectedDate().format(DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_25)) +
                    " | " +
                    ConciergeHelper.buildTimeSlotDisplayFormat3(mContext, timeSelection.getStartTime().toLocalTime(),
                            timeSelection.getEndTime().toLocalTime()).toUpperCase();
            mDeliverySlot.set(deliverySlot);
        }

        if (!TextUtils.isEmpty(mOrder.getPaymentMethod())) {
            ConciergePaymentMethod paymentMethod = ConciergePaymentMethod.fromValue(mOrder.getPaymentMethod());
            if (paymentMethod == ConciergePaymentMethod.CASH_ON_DELIVERY) {
                mPaymentMethod.set(getContext().getString(R.string.shop_order_detail_payment_method_cash));
            } else if (mOrder.getPaymentProvider() != null) {
                mPaymentMethod.set(mOrder.getPaymentProvider().getDisplayName());
            }
        }
        mIsEnableReOrder.set(ConciergeHelper.checkShouldShowReOrderButton(mOrder.getOrderStatus()));
        mOrderStatusButtonBackground.set(ConciergeHelper.getOrderStatusBackground(mContext, mOrder));

        if (mOrder.getDeliveryAddress() != null) {
            mDeliveryAddress.set(mOrder.getDeliveryAddress().getAddress());
            mUserName.set(mOrder.getDeliveryAddress().getUserName());
        }
    }

    public void requestUserBasket(OnCallbackListener<Response<ConciergeBasket>> callbackListener) {
        Observable<Response<ConciergeBasket>> service = mDataManager.requestUserBasket();
        ResponseObserverHelper<Response<ConciergeBasket>> helper = new ResponseObserverHelper<>(getContext(), service);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeBasket>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showError(ex.getMessage(), false);
                if (callbackListener != null) {
                    callbackListener.onFail(ex);
                }
            }

            @Override
            public void onComplete(Response<ConciergeBasket> result) {
                if (callbackListener != null) {
                    callbackListener.onComplete(result);
                }
            }
        }));
    }

    private void clearLocalBasket() {
        showLoading(true);
        Observable<Response<Object>> observable = ConciergeHelper.getClearLocalBasketService(mContext);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mContext, observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<Object> result) {
                requestReorder(false);
            }
        }));
    }

    private void requestReorder(boolean isShowLoading) {
        if (isShowLoading) {
            showLoading(true);
        }
        Observable<Response<ConciergeBasket>> service = mDataManager.getReorderService();
        ResponseObserverHelper<Response<ConciergeBasket>> helper = new ResponseObserverHelper<>(getContext(), service);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeBasket>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<ConciergeBasket> result) {
                hideLoading();
                if (result.body() != null && result.body().isError()) {
                    mListener.showOrderErrorPopup(result.body().getErrorMessage(), view -> {
                        if (result.body().isBasketEmpty()) {
                            ((AppCompatActivity) mContext).finish();
                        } else {
                            mListener.onReorderSuccess();
                        }
                    });
                } else {
                    mListener.onReorderSuccess();
                }
            }
        }));
    }

    private void requestCheckBasket() {
        showLoading(true);
        Observable<Response<ConciergeBasket>> service = mDataManager.checkBasket();
        ResponseObserverHelper<Response<ConciergeBasket>> helper = new ResponseObserverHelper<>(getContext(), service);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeBasket>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<ConciergeBasket> result) {
                if (result.body() != null && !result.body().isBasketEmpty()) {
                    hideLoading();
                    //There is already basket, so will inform user to decide.
                    showConfirmationDialog(mContext,
                            mContext.getString(R.string.shop_checkout_alert_error),
                            mContext.getString(R.string.shop_order_detail_re_order_error),
                            mContext.getString(R.string.popup_ok_button),
                            mContext.getString(R.string.popup_cancel_button),
                            v -> clearLocalBasket());
                } else {
                    requestReorder(false);
                }
            }
        }));
    }

    public void updateOrderStatus(ConciergeOrder updatedOrder) {
        if (mOrder != null && mOrder.areItemsTheSame(updatedOrder)) {
            mOrder.setOrderStatus(updatedOrder.getOrderStatus());
            mOrderStatus.set(mOrder.getOrderStatus().getValue());
        }
    }

    public void onReOrderClicked(boolean isCheckLocalBasketFirst) {
        if (!isEnableReOrder().get()) {
            return;
        }

        if (mIsInExpressMode && mOrder.getType() == ApplicationMode.NORMAL_MODE) {
            mListener.showOrderErrorPopup(mContext.getString(R.string.shop_order_detail_switch_to_normal_mode_message_for_reorder), null);
        } else if (!mIsInExpressMode && mOrder.getType() == ApplicationMode.EXPRESS_MODE) {
            mListener.showOrderErrorPopup(mContext.getString(R.string.shop_order_detail_switch_to_express_mode_message_for_reorder), null);
        } else {
            requestCheckBasket();
        }
    }

    public interface ConciergeOrderHistoryDetailFragmentViewModelListener {

        void onLoadDataSuccess(ConciergeOrder data);

        void onReorderSuccess();

        void showOrderErrorPopup(String errorMessage, View.OnClickListener okClickListener);
    }
}
