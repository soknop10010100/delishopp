package com.proapp.sompom.viewmodel;

import android.content.Context;

import com.proapp.sompom.model.emun.ErrorLoadingType;

/**
 * Created by Or Vitovongsak on 5/1/22.
 */
public class ListItemEmptyDataErrorViewModel extends AbsLoadingViewModel {

    public ListItemEmptyDataErrorViewModel(Context context,
                                           ErrorLoadingType errorLoadingType,
                                           String errorTitle,
                                           String errorMessage) {
        super(context);
        mErrorLoadingType.set(errorLoadingType);
        showEmptyDataError(errorTitle, errorMessage);
    }
}
