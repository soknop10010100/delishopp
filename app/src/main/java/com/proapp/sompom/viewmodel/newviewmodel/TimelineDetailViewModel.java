package com.proapp.sompom.viewmodel.newviewmodel;

import android.os.Handler;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.NetworkBroadcastReceiver;
import com.proapp.sompom.helper.ContentTypeHelper;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.listener.LoadMoreCommentDirectionCallback;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.listener.OnEditTextCommentListener;
import com.proapp.sompom.listener.OnTimelineHeaderItemClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.notification.NotificationListAndRedirectionHelper;
import com.proapp.sompom.model.request.ReportRequest;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.model.result.JumpCommentResponse;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.TimelineDetailDataManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 7/12/18.
 */

public class TimelineDetailViewModel extends ListItemTimelineHeaderViewModel {

    public final ObservableBoolean mIsShowComment = new ObservableBoolean();
    public final ObservableField<LayoutEditTextCommentViewModel> mEditTextViewModel = new ObservableField<>();

    private final OnEditTextCommentListener mOnEditTextCommentListener;
    private final Callback onTimelineHeaderItemClickListener;
    private final TimelineDetailDataManager mDataManager;
    private final String mWallStreetId;
    private final NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    private WallStreetAdaptive mWallStreetAdaptive;
    private LayoutEditTextCommentViewModel mLayoutEditTextCommentViewModel;
    private final ObservableInt mcheckLikeCommentVisibility = new ObservableInt();
    private boolean mIsLoadedData;
    private boolean mIsSinglePostItem;
    private String mContentId;

    public void setCheckLikeCommentVisibility(int checkLikeCommentVisibility) {
        mcheckLikeCommentVisibility.set(checkLikeCommentVisibility);
    }

    public ObservableInt getCheckLikeCommentVisibility() {
        return mcheckLikeCommentVisibility;
    }

    public TimelineDetailViewModel(AbsBaseActivity activity,
                                   boolean checkLikeComment,
                                   String wallStreetId,
                                   TimelineDetailDataManager dataManager,
                                   WallStreetAdaptive adaptive,
                                   Callback listener,
                                   OnEditTextCommentListener onEditTextCommentListener) {

        super(activity, (Adaptive) adaptive, 0, listener);
        mWallStreetId = wallStreetId;
        mContentId = wallStreetId;
        mDataManager = dataManager;
        mOnEditTextCommentListener = onEditTextCommentListener;
        onTimelineHeaderItemClickListener = listener;

        //Check to visible comment option base on license configuration
        if (!checkLikeComment) {
            setCheckLikeCommentVisibility(View.GONE);
        } else {
            setCheckLikeCommentVisibility(View.VISIBLE);
        }

        if (!isRequestWallStreetDetail()) {
            checkToBuildPostItemDetail(adaptive);
        }

        mNetworkStateListener = networkState -> {
            mDataManager.setNetworkAvailable((networkState == NetworkBroadcastReceiver.NetworkState.Connected));
            if (!mIsLoadedData) {
                loadData();
            }
        };
        mContext.addNetworkStateChangeListener(mNetworkStateListener);
    }

    public TimelineDetailDataManager getDataManager() {
        return mDataManager;
    }

    public String getWallStreetId() {
        return mWallStreetId;
    }

    private boolean isRequestWallStreetDetail() {
        return !TextUtils.isEmpty(mWallStreetId);
    }

    private void loadData() {
        if (isRequestWallStreetDetail()) {
            requestWallStreetDetail();
        } else {
            if (mIsShowComment.get()) {
                getCommentList(null, false);
            }
        }
    }

    public void requestWallStreetDetail() {
        showLoading();
        Observable<Response<LifeStream>> callback = mDataManager.getWallStreetDetail(mWallStreetId);
        ResponseObserverHelper<Response<LifeStream>> helper = new ResponseObserverHelper<>(mDataManager.getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<LifeStream>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (mIsLoadedData) {
                    return;
                }
                showError(ex.getMessage());
            }

            @Override
            public void onComplete(Response<LifeStream> result) {
                hideLoading();
                if (result.body() != null && result.body().isDeleted()) {
                    showError(getContext().getString(R.string.post_detail_screen_post_deleted_message),
                            false);
                } else {
                    mIsLoadedData = true;
                    setPostData(result.body());
                }
            }
        }));
    }

    public void checkToLikeComment(Comment comment, OnCallbackListener<Response<Object>> listener) {
        Observable<Response<Object>> callback = mDataManager.postLikeComment(comment.getId(), comment.isLike(), mWallStreetId);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                showSnackBar(ex.getMessage(), true);
                if (listener != null) {
                    listener.onFail(ex);
                }
            }

            @Override
            public void onComplete(Response<Object> result) {
                if (listener != null) {
                    listener.onComplete(result);
                }
            }
        }));
    }

    public void setPostData(LifeStream lifeStream) {
        Timber.i("setPostData: mIsShowComment: " + mIsShowComment.get());
        setAdaptive(lifeStream);
        onTimelineHeaderItemClickListener.onGetPostDetailSuccess(lifeStream);
        checkToBuildPostItemDetail(lifeStream);
        if (mIsShowComment.get()) {
            getCommentList(null, false);
        }
        TimelineDetailViewModel.this.notifyChange();
        checkPreviewVisibility(lifeStream);
        bindCheckedInPlace(lifeStream);
    }

    @Override
    public void onRetryClick() {
        super.onRetryClick();
        if (isRequestWallStreetDetail()) {
            requestWallStreetDetail();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mContext.removeStateChangeListener(mNetworkStateListener);

    }

    public void checkToPostFollow(String userId, boolean isFollow) {
        Observable<Response<Object>> callback;
        if (isFollow) {
            callback = mDataManager.postFollow(userId);
        } else {
            callback = mDataManager.unFollow(userId);
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mDataManager.getContext(), callback);
        addDisposable(helper.execute());
    }

    public void reportPost(String id, ReportRequest reportRequest) {
        Observable<Response<Object>> request = mDataManager.reportPost(id, reportRequest);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mDataManager.getContext(), request);
        addDisposable(helper.execute());
    }

    public void unFollowShop(String id) {
        Observable<Response<Object>> observable = mDataManager.unFollowShop(id);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute());
    }

    public void onBackPress() {
        mContext.onBackPressed();
    }

    public void postComment(Comment comment) {
        if (mLayoutEditTextCommentViewModel != null) {
            mLayoutEditTextCommentViewModel.postComment(comment);
        }
    }

    private void getCommentList(OnCallbackListener<Response<List<Comment>>> listener, boolean isForResetJumpMode) {
        Timber.i("getCommentList");
        Observable<Response<List<Comment>>> callback = mDataManager.getCommentListWithJumpSupport(false,
                mContentId,
                JumpCommentResponse.REDIRECTION_NEXT);
        ResponseObserverHelper<Response<List<Comment>>> helper = new ResponseObserverHelper<>(mContext, callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Comment>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsLoadedData = false;
                if (listener != null) {
                    listener.onFail(ex);
                }
            }

            @Override
            public void onComplete(Response<List<Comment>> value) {
                mIsLoadedData = true;
                onTimelineHeaderItemClickListener.onLoadCommentSuccess(value.body(),
                        mDataManager.isInJumpCommentMode(),
                        mDataManager.isCanLoadPrevious(),
                        mDataManager.isCanLoadNext(),
                        isForResetJumpMode);
                if (listener != null) {
                    listener.onComplete(value);
                }
            }
        }));
    }

    public void loadMoreCommentList(String loadDirection, LoadMoreCommentDirectionCallback listListener) {
        Observable<Response<List<Comment>>> callback = mDataManager.getCommentListWithJumpSupport(true,
                mContentId,
                loadDirection);
        ResponseObserverHelper<Response<List<Comment>>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Comment>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                //Need to find where the load more direction came from previous or next.
                boolean canLoadPrevious = mDataManager.isCanLoadMore();
                boolean canLoadNext = mDataManager.isCanLoadNext();
                if (mDataManager.isInJumpCommentMode()) {
                    if (TextUtils.equals(loadDirection, JumpCommentResponse.REDIRECTION_NEXT)) {
                        canLoadNext = false;
                    } else {
                        canLoadPrevious = false;
                    }
                } else {
                    canLoadPrevious = false;
                }
                listListener.onFail(ex, mDataManager.isInJumpCommentMode(), canLoadPrevious, canLoadNext);
            }

            @Override
            public void onComplete(Response<List<Comment>> value) {
                listListener.onLoadCommentDirectionSuccess(value.body(),
                        mDataManager.isInJumpCommentMode(),
                        mDataManager.isCanLoadPrevious(),
                        mDataManager.isCanLoadNext());
            }
        }));
    }

    private void buildDefaultPostDetailScreen(WallStreetAdaptive adaptive) {
        List<Adaptive> adaptiveList = new ArrayList<>();
        /*
         * Note: All item added into @{adaptiveList} will render the items display in detail post screen.
         * So in case that there missing or invalid item render, you can check the condition added.
         */
        if (isFilePostTimeline(adaptive)) {
            /*
             For the file post, it will be considered as post contain single item too.
             *Note: Currently only from desktop that are allowing to post as file. And normally
             the post file will contain only file type and with or without text.
             */
            adaptiveList.add((Adaptive) adaptive);
            mIsSinglePostItem = true;
            showCommentLayout(adaptive.getId(), adaptive.getId(), ContentType.POST);
        } else {
            mWallStreetAdaptive = adaptive;
            /*
                Following are the conditions to show post section.
                1. If there is any post text
                2. Or if there is link or place preview
                3. Or if the media size is more than 1.
             */
            if (!TextUtils.isEmpty(adaptive.getDescription()) ||
                    WallStreetHelper.shouldRenderLinkPreview(adaptive) ||
                    shouldRenderPlacePreviewArea(adaptive) ||
                    (adaptive.getMedia() != null && adaptive.getMedia().size() > 1)) {
                adaptiveList.add((Adaptive) mWallStreetAdaptive);
            }

            //Add all media to display as post detail items for Media is the sub class of Adaptive.
            if (mWallStreetAdaptive.getMedia() != null && !mWallStreetAdaptive.getMedia().isEmpty()) {
                adaptiveList.addAll(mWallStreetAdaptive.getMedia());
            }

            /*
             To show the comment item directly for post contains single post item. The following condition
             describe the post that will considered as single post item post.
             1. Contain only one media and with or without post text.
             */
            if (adaptiveList.size() == 1 && (adaptiveList.get(0) instanceof LifeStream ||
                    adaptiveList.get(0) instanceof Media)) {
                /*
                For a post contains only single item either the item is text or media, the content state
                of that item should be the content state of the the whole post.
                 */
                mIsSinglePostItem = true;
                if (adaptiveList.get(0) instanceof Media) {
                    ((Media) adaptiveList.get(0)).setContentStat(mWallStreetAdaptive.getContentStat());
                    ((Media) adaptiveList.get(0)).setLike(mWallStreetAdaptive.isLike());
                }
                showCommentLayout(adaptive.getId(), adaptive.getId(), ContentType.POST);
            } else {
                //Hide comment layout
                mcheckLikeCommentVisibility.set(View.GONE);
            }
        }

        onTimelineHeaderItemClickListener.onShowAdapter(adaptiveList, mIsShowComment.get());
    }

    private void buildDPostPreviewScreenFromNotificationRedirection(WallStreetAdaptive post) {
        Timber.i("buildDPostPreviewScreenFromNotificationRedirection: mContentId: " + mContentId + ", Data" + new Gson().toJson(mDataManager.getRedirectionNotificationData()));
        List<Adaptive> adaptiveList = new ArrayList<>();
        ContentType commentContentType = ContentType.POST;
        if (!TextUtils.isEmpty(mDataManager.getNotificationExchangeData().getMediaId())) {
            Media redirectionMediaTarget = mDataManager.getRedirectionMediaTarget(post);
            if (redirectionMediaTarget != null) {
                adaptiveList.add(redirectionMediaTarget);
                commentContentType = ContentTypeHelper.getMediaContentType(redirectionMediaTarget);
                mContentId = redirectionMediaTarget.getId();
            } else {
                /*
                In this case, there was the redirection to the removed media item of post. So we
                will reset this redirection as normal post preview.
                 */
                adaptiveList.add((Adaptive) post);
                mDataManager.resetToNormalTimeLineDetailMode();
                new Handler().postDelayed(() -> NotificationListAndRedirectionHelper.showRedirectionNotificationContentNotFound(mContext),
                        NotificationListAndRedirectionHelper.SCROLL_DELAY + NotificationListAndRedirectionHelper.ANIMATION_DELAY);
            }
        } else {
            adaptiveList.add((Adaptive) post);
        }
        mIsSinglePostItem = true;
        Timber.i("mContentId: " + mContentId);
        showCommentLayout(post.getId(), mContentId, commentContentType);
        onTimelineHeaderItemClickListener.onShowAdapter(adaptiveList, mIsShowComment.get());
    }

    private Media getMediaFromPost(WallStreetAdaptive adaptive, String mediaId) {
        if (adaptive != null && adaptive.getMedia() != null && !adaptive.getMedia().isEmpty()) {
            for (Media media : adaptive.getMedia()) {
                if (TextUtils.equals(mediaId, media.getId())) {
                    return media;
                }
            }
        }

        return null;
    }

    private void checkToBuildPostItemDetail(WallStreetAdaptive adaptive) {
        if (mDataManager.isInJumpCommentMode()) {
            buildDPostPreviewScreenFromNotificationRedirection(adaptive);
        } else {
            buildDefaultPostDetailScreen(adaptive);
        }
    }

    public boolean isSinglePostItem() {
        return mIsSinglePostItem;
    }

    private boolean shouldRenderPlacePreviewArea(WallStreetAdaptive adaptive) {
        return WallStreetHelper.shouldRenderPlacePreview(adaptive) &&
                (!TextUtils.isEmpty(adaptive.getDescription()) ||
                        adaptive.getMedia() == null ||
                        adaptive.getMedia().isEmpty());
    }

    private boolean isFilePostTimeline(WallStreetAdaptive adaptive) {
        if (adaptive instanceof LifeStream) {
            return ((LifeStream) adaptive).isFilePostTimelineType();
        }

        return false;
    }

    private void showCommentLayout(String postId, String contentId, ContentType contentType) {
        mIsShowComment.set(true);
        mLayoutEditTextCommentViewModel = new LayoutEditTextCommentViewModel(mDataManager,
                postId,
                contentId,
                contentType,
                mOnEditTextCommentListener,
                (viewModel, newComment) -> {
                    if (mDataManager.isInJumpCommentMode()) {
                        showLoading(true);
                        mDataManager.resetToNormalTimeLineDetailMode();
                        getCommentList(new OnCallbackListener<Response<List<Comment>>>() {
                            @Override
                            public void onFail(ErrorThrowable ex) {
                                hideLoading();
                                viewModel.proceedSendNewComment(newComment);
                            }

                            @Override
                            public void onComplete(Response<List<Comment>> result) {
                                hideLoading();
                                new Handler().postDelayed(() ->
                                        viewModel.proceedSendNewComment(newComment), 500);

                            }
                        }, true);
                    } else {
                        viewModel.proceedSendNewComment(newComment);
                    }
                });
        mEditTextViewModel.set(mLayoutEditTextCommentViewModel);
    }

    public void setUpdateComment(Comment comment, int updateCommentPosition) {
        if (mLayoutEditTextCommentViewModel != null) {
            mLayoutEditTextCommentViewModel.mMessage.set(comment.getContent());
            mLayoutEditTextCommentViewModel.setUpdateComment(comment, updateCommentPosition);
        }
    }

    public void onDeleteComment(Comment comment, OnClickListener onClickListener) {
        showLoading(true);
        Observable<Response<Object>> callback = mDataManager.deleteComment(comment.getId());
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                String error = getContext().getString(R.string.comment_toast_delete_fail);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    error = ex.getMessage();
                }
                showSnackBar(error, true);
            }

            @Override
            public void onComplete(Response<Object> result) {
                hideLoading();
                onClickListener.onClick();
                if (onTimelineHeaderItemClickListener != null) {
                    int counter = 1;
                    if (comment.getTotalSubComment() > 0) {
                        counter += comment.getTotalSubComment();
                    }
                    onTimelineHeaderItemClickListener.onRemoveCommentSuccess(counter);
                }
            }
        }));
    }

    public void deletePost(WallStreetAdaptive adaptive) {
        Observable<Response<Object>> call = mDataManager.deletePost(adaptive.getId());
        ResponseObserverHelper<Response<Object>> helper
                = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute());
    }

    public interface Callback extends OnTimelineHeaderItemClickListener {

        void onShowAdapter(List<Adaptive> adaptiveList, boolean isShowEditText);

        void onLoadCommentSuccess(List<Comment> listComment,
                                  boolean isInJumpMode,
                                  boolean isCanLoadPrevious,
                                  boolean isCanLoadNext,
                                  boolean isFromResetJumpMode);

        void onGetPostDetailSuccess(WallStreetAdaptive adaptive);

        void onRemoveCommentSuccess(int removedCount);
    }
}
