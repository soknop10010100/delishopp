package com.proapp.sompom.viewmodel;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.NetworkBroadcastReceiver;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.listener.OnCompleteListener;
import com.proapp.sompom.model.ActiveUser;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.SearchGeneralDataManager;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by nuonveyo on 8/2/18.
 */

public class SearchGeneralFragmentViewModel extends AbsLoadingViewModel {
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private final SearchGeneralDataManager mSearchGeneralDataManager;
    private final OnCompleteListener<List<ActiveUser>> mCompleteListener;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    private boolean mIsDataRequested;
    private boolean mHasLocalSuggestedData;

    public SearchGeneralFragmentViewModel(SearchGeneralDataManager dataManager,
                                          OnCompleteListener<List<ActiveUser>> completeListener) {
        super(dataManager.getContext());
        mSearchGeneralDataManager = dataManager;
        mCompleteListener = completeListener;
        loadLocalSuggestedUsers();
        if (getContext() instanceof AbsBaseActivity) {
            mNetworkStateListener = networkState -> {
                if (!mIsDataRequested) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        getSearchGeneralUser(mHasLocalSuggestedData);
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected &&
                            !mHasLocalSuggestedData) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkStateListener);
        }
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        getSearchGeneralUser(mHasLocalSuggestedData);
    }

    public OnClickListener onButtonRetryClick() {
        return () -> getSearchGeneralUser(mHasLocalSuggestedData);
    }

    private void loadLocalSuggestedUsers() {
        List<User> suggestedUserList = UserHelper.getSuggestedUser(getContext());
        if (suggestedUserList != null && !suggestedUserList.isEmpty()) {
            mHasLocalSuggestedData = true;
            ActiveUser suggestedUser = new ActiveUser();
            suggestedUser.addAll(suggestedUserList);
            suggestedUser.setTitle(getContext().getString(R.string.search_home_screen_suggested_people_title));
            suggestedUser.setSellerItemType(ActiveUser.SellerItemType.SUGGESTED_PEOPLE);
            if (mCompleteListener != null) {
                mCompleteListener.onComplete(Collections.singletonList(suggestedUser));
            }
        }
    }

    private void getSearchGeneralUser(boolean isSilentRequest) {
        if (!mIsRefresh.get() && !isSilentRequest) {
            showLoading();
        }
        Observable<List<ActiveUser>> observable = mSearchGeneralDataManager.getSearchGeneralUser();
        BaseObserverHelper<List<ActiveUser>> helper = new BaseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<List<ActiveUser>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsRefresh.set(false);
                if (mIsDataRequested || mHasLocalSuggestedData) {
                    return;
                }

                mIsDataRequested = false;
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    showNetworkError();
                } else {
                    showError(ex.toString());
                }
            }

            @Override
            public void onComplete(List<ActiveUser> result) {
                mIsDataRequested = true;
                mIsRefresh.set(false);
                hideLoading();
                if (mCompleteListener != null) {
                    mCompleteListener.onComplete(result);
                }
            }
        }));
    }
}
