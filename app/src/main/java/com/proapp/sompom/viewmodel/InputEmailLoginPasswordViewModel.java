package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.services.datamanager.InputLoginPasswordDataManager;

/**
 * Created by Veasna Chhom on 4/10/22.
 */
public class InputEmailLoginPasswordViewModel extends AbsLoginViewModel<InputLoginPasswordDataManager,
        AbsLoginViewModel.AbsLoginViewModelListener> {


    public InputEmailLoginPasswordViewModel(Context context,
                                            InputLoginPasswordDataManager dataManager,
                                            AbsLoginViewModelListener listener) {
        super(context, dataManager, listener);
        mEmail.set(dataManager.getExchangeAuthData().getEmail());
    }

    @Override
    protected boolean shouldEnableActionButton() {
        return isPasswordValid();
    }

    @Override
    protected void onContinueButtonClicked() {
        requestLogin(mDataManager.getLoginService(mPassword.get()), mDataManager.isLogInWithPhone());
    }

    private boolean isPasswordValid() {
        return !TextUtils.isEmpty(mPassword.get());
    }

    public void onBackClick() {
        if (getContext() instanceof Activity) {
            ((Activity) getContext()).finish();
        }
    }

    public void onReceivedResetPasswordSuccessEvent(String email) {
        //Just reset the email and password.
        mEmail.set(email);
        mPassword.set("");
    }
}
