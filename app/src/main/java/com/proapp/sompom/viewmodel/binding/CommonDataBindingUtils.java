package com.proapp.sompom.viewmodel.binding;

import androidx.databinding.BindingAdapter;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

/**
 * Created by Chhom Veasna on 1/6/2020.
 */
public final class CommonDataBindingUtils {

    private CommonDataBindingUtils() {
    }

    @BindingAdapter("enableBackActionbarAction")
    public static void enableBackActionbarAction(View view, boolean enable) {
        if (enable) {
            view.setOnClickListener(v -> {
                if (view.getContext() instanceof AppCompatActivity) {
                    ((AppCompatActivity) view.getContext()).onBackPressed();
                }
            });
        }
    }
}
