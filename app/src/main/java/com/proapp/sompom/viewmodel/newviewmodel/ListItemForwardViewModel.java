package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.adapter.newadapter.ForwardAdapter;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.helper.ConversationHelper;
import com.proapp.sompom.listener.ConversationDataAdaptive;
import com.proapp.sompom.model.ForwardTitle;
import com.proapp.sompom.model.emun.ConversationType;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by He Rotha on 9/13/18.
 */
public class ListItemForwardViewModel {

    public final ObservableField<User> mUser = new ObservableField<>();
    public final ObservableBoolean mIsShowIndicator = new ObservableBoolean();
    public final ObservableBoolean mIsSent = new ObservableBoolean();
    public final ObservableBoolean mIsChangeBg = new ObservableBoolean();
    public final ObservableInt mTitle = new ObservableInt();
    private ForwardAdapter.OnForwardItemClickListener mListener;
    private ConversationDataAdaptive mAdaptive;

    private String mGroupImageUrl;
    private String mGroupName;
    private List<User> mParticipants;
    private String mName;

    public ListItemForwardViewModel(Context context,
                                    ConversationDataAdaptive adaptive,
                                    @Nullable MessageState state,
                                    ForwardAdapter.OnForwardItemClickListener listener) {
        mListener = listener;
        mAdaptive = adaptive;
        bindForwardProfile(context);
        if (state == MessageState.SENDING) {
            mIsSent.set(false);
            mIsShowIndicator.set(true);
            mIsChangeBg.set(true);
        } else if (state == MessageState.SENT) {
            mIsSent.set(true);
            mIsShowIndicator.set(false);
            mIsChangeBg.set(true);
        } else {
            mIsSent.set(false);
            mIsShowIndicator.set(false);
            mIsChangeBg.set(false);
        }
    }

    public String getName() {
        return mName;
    }

    public String getGroupImageUrl() {
        return mGroupImageUrl;
    }

    public String getGroupName() {
        return mGroupName;
    }

    public List<User> getParticipants() {
        return mParticipants;
    }

    private void bindForwardProfile(Context context) {
        if (mAdaptive instanceof Conversation) {
            //Group
            Conversation conversation = (Conversation) mAdaptive;
            if (ConversationType.fromValue(conversation.getType()) == ConversationType.SUPPORT) {
                mGroupImageUrl = ConversationHelper.getSupportConversationProfile(context);
            } else {
                mGroupImageUrl = conversation.getGroupImageUrl();
            }
            mGroupName = conversation.getGroupName();
            mName = getGroupName();
            if (conversation.getParticipants() != null) {
                mParticipants = new ArrayList<>(conversation.getParticipants());
            }
        } else {
            //Normal user
            mUser.set((User) mAdaptive);
            mParticipants = Collections.singletonList(mUser.get());
            mName = mUser.get().getFullName();
        }
    }

    public ListItemForwardViewModel(ForwardTitle title) {
        mTitle.set(title.getTitle());
    }

    public void onSendClick(View view) {
        AnimationHelper.animateBounce(view, null);
        mListener.onSendClick(mAdaptive);
        mIsSent.set(false);
        mIsChangeBg.set(true);
        mIsShowIndicator.set(true);
    }
}
