package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;

import com.proapp.sompom.BR;
import com.proapp.sompom.databinding.ListItemConciergeProductBinding;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeShop;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemConciergeProductViewModel;
import com.proapp.sompom.widget.CustomAddToCartButton;

public class ListConciergeShopItemViewModel extends AbsLoadingViewModel {

    private final ObservableField<ListItemConciergeProductViewModel> mItemViewModel = new ObservableField<>();
    private CustomAddToCartButton.OnAddDeleteListener mOnAddDeleteListener;
    private final ConciergeMenuItem mProduct;

    public ListConciergeShopItemViewModel(Context context,
                                          ConciergeShop shop,
                                          ConciergeMenuItem product,
                                          boolean isRequiredRootMarginAndPadding,
                                          ListItemConciergeProductViewModel.ListItemConciergeProductViewModelListener itemListener) {
        super(context);
        mProduct = product;
        if (shop != null) {
            mProduct.setShop(shop.cloneEmptyShop());
        }
        int itemAmountInBasket = ConciergeCartHelper.getExistingProductCountInCart(mProduct);
        ListItemConciergeProductViewModel viewModel = new ListItemConciergeProductViewModel(context,
                product,
                itemAmountInBasket,
                isRequiredRootMarginAndPadding,
                itemListener
        );
        mItemViewModel.set(viewModel);
    }

    public void assignOnAddDeleteListener(CustomAddToCartButton.OnAddDeleteListener onAddDeleteListener) {
        mOnAddDeleteListener = onAddDeleteListener;
    }

    public ObservableField<ListItemConciergeProductViewModel> getItemViewModel() {
        return mItemViewModel;
    }

    public CustomAddToCartButton.OnAddDeleteListener getOnAddDeleteListener() {
        return mOnAddDeleteListener;
    }

    @BindingAdapter({"setItemProductBinding", "setItemViewModel", "setListener"})
    public static void setItemViewModel(View view,
                                        ListItemConciergeProductBinding itemProductBinding,
                                        ListItemConciergeProductViewModel viewModel,
                                        CustomAddToCartButton.OnAddDeleteListener listener) {
        if (viewModel != null) {
//            itemProductBinding.customAddToCartButton.setOnAddDelListener(listener);
            itemProductBinding.setVariable(BR.viewModel, viewModel);
        }
    }
}
