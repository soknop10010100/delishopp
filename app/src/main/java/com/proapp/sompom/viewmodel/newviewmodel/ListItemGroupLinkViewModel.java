package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableField;

import com.proapp.sompom.model.result.LinkPreviewModel;

/**
 * Created by Chhom Veasna on 7/27/2020.
 */
public class ListItemGroupLinkViewModel {

    private ObservableField<String> mPreviewPhoto = new ObservableField<>();
    private ObservableField<String> mTitle = new ObservableField<>();
    private ObservableField<String> mDescription = new ObservableField<>();
    private ObservableField<String> mUrl = new ObservableField<>();

    public ListItemGroupLinkViewModel(LinkPreviewModel media) {
        mPreviewPhoto.set(media.getImage());
        mTitle.set(media.getTitle());
        mUrl.set(media.getLink());
        mDescription.set(media.getDescription());
    }

    public ObservableField<String> getPreviewPhoto() {
        return mPreviewPhoto;
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    public ObservableField<String> getUrl() {
        return mUrl;
    }

    public ObservableField<String> getDescription() {
        return mDescription;
    }
}
