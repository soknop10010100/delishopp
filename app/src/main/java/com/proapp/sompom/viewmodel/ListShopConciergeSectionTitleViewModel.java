package com.proapp.sompom.viewmodel;

import androidx.databinding.ObservableField;

public class ListShopConciergeSectionTitleViewModel extends AbsBaseViewModel {

    private final ObservableField<String> mTitle = new ObservableField<>();

    public ListShopConciergeSectionTitleViewModel(String title) {
        mTitle.set(title);
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }
}
