package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.listener.OnUserClickListener;
import com.proapp.sompom.model.emun.SuggestionTab;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.FormatNumber;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;
import com.proapp.sompom.R;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class ListItemSuggestionViewModel extends AbsBaseViewModel {
    public final ObservableBoolean mIsActive = new ObservableBoolean();
    public final ObservableBoolean mIsShowCover = new ObservableBoolean();
    public final User mUser;
    private OnUserClickListener mOnUserClickListener;

    public ListItemSuggestionViewModel(User user, SuggestionTab tab, OnUserClickListener listener) {
        mUser = user;
        mIsActive.set(mUser.isFollow());
        mIsShowCover.set(tab == SuggestionTab.Store);
        mOnUserClickListener = listener;
    }

    public String getFollowers(Context context) {
        long totalFollowers = mUser.getContentStat().getTotalFollowers();
        if (totalFollowers > 0) {
            String followers;
            if (totalFollowers == 1) {
                followers = context.getString(R.string.suggestion_follower);
            } else {
                followers = context.getString(R.string.seller_store_followers_title);
            }
            return FormatNumber.format(totalFollowers) + " " + followers;
        }
        return "";
    }

    public void onItemClick() {
        mUser.setFollow(!mUser.isFollow());
        mIsActive.set(mUser.isFollow());
        mOnUserClickListener.onUserClick(mUser);
    }

    public String getCoverUrl() {
        return mUser.getUserCoverProfile();
    }

    public int getCornerRadius(Context context) {
        return context.getResources().getDimensionPixelSize(R.dimen.medium_padding);
    }
}
