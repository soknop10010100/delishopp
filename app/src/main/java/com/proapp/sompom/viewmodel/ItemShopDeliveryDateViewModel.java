package com.proapp.sompom.viewmodel;


import android.content.Context;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.model.ShopDeliveryDate;
import com.resourcemanager.helper.LocaleManager;

import java.time.LocalDateTime;
import java.time.format.TextStyle;

/**
 * Created by Veasna Chhom on 9/9/21.
 */
public class ItemShopDeliveryDateViewModel extends ItemStateSelectionViewModel {

    private final ObservableField<String> mDateText = new ObservableField<>();
    private final ObservableField<String> mDateNumber = new ObservableField<>();
    private final ObservableInt mDateDotColor = new ObservableInt();
    private int mSelectedDateDotColor;
    private int mUnselectedDateDotColor;
    private ShopDeliveryDate mDeliveryDate;
    private Context mContext;
    private ItemShopDeliveryDateViewModelCallback mListener;

    public ItemShopDeliveryDateViewModel(Context context,
                                         ShopDeliveryDate deliveryDate,
                                         int selectedDateDotColor,
                                         int unselectedDateDotColor,
                                         int selectedTextColor,
                                         int unselectedTextColor,
                                         int selectedBackgroundColor,
                                         int unselectedBackgroundColor,
                                         ItemShopDeliveryDateViewModelCallback listener) {
        super(null, selectedTextColor, unselectedTextColor, selectedBackgroundColor, unselectedBackgroundColor);
        mContext = context;
        mListener = listener;
        mDeliveryDate = deliveryDate;
        mSelectedDateDotColor = selectedDateDotColor;
        mUnselectedDateDotColor = unselectedDateDotColor;
        bindDate();
    }

    private void bindDate() {
        if (mDeliveryDate != null) {
            updateState(mDeliveryDate.isSelected());
            binDateDotBackgroundFromModelState();

            LocalDateTime deliveryDate = mDeliveryDate.getDate();

            String dateText = deliveryDate.getDayOfWeek()
                    .getDisplayName(TextStyle.SHORT,
                            LocaleManager.getLocale(mContext.getResources()));
            int dayOfMonth = deliveryDate.getDayOfMonth();
            int month = deliveryDate.getMonthValue();

//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(mDeliveryDate.getDate());

//            String dateText = calendar.getDisplayName(Calendar.DAY_OF_WEEK,
//                    Calendar.SHORT,
//                    LocaleManager.getLocale(mContext.getResources()));
//            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

            mDateText.set(dateText);
//            Timber.i("dateText: " + dateText);
            mDateNumber.set(dayOfMonth + "/" + month); //Format 11/4
        }
    }

    private void binDateDotBackgroundFromModelState() {
        if (mDeliveryDate != null) {
            mDateDotColor.set(mDeliveryDate.isSelected() ? mSelectedDateDotColor : mUnselectedDateDotColor);
        }
    }

    public ObservableField<String> getDateText() {
        return mDateText;
    }

    public ObservableField<String> getDateNumber() {
        return mDateNumber;
    }

    public ObservableInt getDateDotColor() {
        return mDateDotColor;
    }

    @Override
    public void updateState(boolean isSelected) {
        mDeliveryDate.setSelected(isSelected);
        super.updateState(isSelected);
        if (isSelected) {
            mDateDotColor.set(mSelectedDateDotColor);
        } else {
            binDateDotBackgroundFromModelState();
        }
    }

    public void onDateClicked() {
        if (!mDeliveryDate.isSelected()) {
            updateState(true);
            if (mListener != null) {
                mListener.onDateClicked(mDeliveryDate);
            }
        }
    }

    public interface ItemShopDeliveryDateViewModelCallback {
        void onDateClicked(ShopDeliveryDate date);
    }
}
