package com.proapp.sompom.viewmodel;

import android.text.TextUtils;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;

import com.proapp.sompom.model.InBoardItem;
import com.proapp.sompom.utils.GenerateLinkPreviewUtil;
import com.proapp.sompom.utils.GlideLoadUtil;

public class InBoardingItemViewModel {

    private ObservableField<String> mTitle = new ObservableField<>();
    private ObservableField<String> mDescription = new ObservableField<>();
    private ObservableField<String> mImage = new ObservableField<>();

    public InBoardingItemViewModel(InBoardItem item) {
        mTitle.set(item.getTitle());
        mDescription.set(item.getDescription());
        mImage.set(item.getImage());
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    public ObservableField<String> getDescription() {
        return mDescription;
    }

    public ObservableField<String> getImage() {
        return mImage;
    }

    @BindingAdapter("image")
    public static void loadImage(ImageView imageView, String image) {
        if (!TextUtils.isEmpty(image)) {
            if (!GenerateLinkPreviewUtil.isValidUrl(image)) {
                //Local resource
                final int resourceId = imageView.getResources().getIdentifier(image,
                        "drawable",
                        imageView.getContext().getPackageName());
                GlideLoadUtil.loadFromResource(imageView, resourceId);
            } else {
                GlideLoadUtil.load(imageView, image);
            }
        }
    }
}
