package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;

public abstract class LayoutToolbarGroupDetailViewModel extends SupportCallViewModel {

    private ObservableInt mConfigOptionVisibility = new ObservableInt(View.GONE);

    public LayoutToolbarGroupDetailViewModel(Context context, SupportCallViewModelCallback supportCallViewModelCallback) {
        super(context, supportCallViewModelCallback);
        setCallActionVisibility();
    }

    public ObservableInt getConfigOptionVisibility() {
        return mConfigOptionVisibility;
    }

    public void setConfigOptionVisibility(int configOptionVisibility) {
        mConfigOptionVisibility.set(configOptionVisibility);
    }

    public void onOptionButtonClicked(View view) {
        LayoutInflater inflater =
                (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.layout_popup_setting_group_detail, null);
        View button = view.findViewById(R.id.groupOptionButton);

        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAsDropDown(button, 0, 0);

        View changePhoto = popupView.findViewById(R.id.changeGroupPhoto);
        changePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChangePhotoClick();
                popupWindow.dismiss();
            }
        });

        View changeName = popupView.findViewById(R.id.changeGroupName);
        changeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChangeNameClick();
                popupWindow.dismiss();
            }
        });
    }

    public void onChangePhotoClick() {

    }

    public void onChangeNameClick() {

    }
}
