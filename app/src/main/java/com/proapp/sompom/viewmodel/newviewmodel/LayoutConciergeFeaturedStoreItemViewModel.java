package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableField;

import com.proapp.sompom.model.concierge.ConciergeFeatureStoreSectionResponse;

/**
 * Created by Or Vitovongsak on 30/8/21.
 */
public class LayoutConciergeFeaturedStoreItemViewModel {

    private final ObservableField<String> mUrl = new ObservableField<>();
    private final ConciergeFeatureStoreSectionResponse.Data mData;

    public LayoutConciergeFeaturedStoreItemViewModel(ConciergeFeatureStoreSectionResponse.Data data) {
        mData = data;
        mUrl.set(mData.getShopLogo());
    }

    public ObservableField<String> getUrl() {
        return mUrl;
    }
}
