package com.proapp.sompom.viewmodel.newviewmodel;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;
import androidx.databinding.ViewDataBinding;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.FileDownloadHelper;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.helper.WallStreetIntentData;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;
import com.proapp.sompom.utils.DownloaderUtils;
import com.proapp.sompom.viewholder.BindingViewHolder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListItemTimelineFileViewModel extends ListItemMainTimelineViewModel {

    private static final int MAX_DISPLAY_FILE_ITEM = 3;

    private final ObservableField<List<Media>> mMediaList = new ObservableField<>();
    private Map<Media, ItemPostTimelineFileViewModel> mViewModelHashMap = new HashMap<>();

    public ListItemTimelineFileViewModel(AbsBaseActivity activity,
                                         BindingViewHolder bindingViewHolder,
                                         Adaptive adaptive,
                                         WallStreetDataManager dataManager,
                                         int position,
                                         OnTimelineItemButtonClickListener onItemClick,
                                         boolean checkLikeComment) {
        super(activity, bindingViewHolder, dataManager, adaptive, position, onItemClick, checkLikeComment);
        if (adaptive instanceof WallStreetAdaptive) {
            mMediaList.set(((WallStreetAdaptive) adaptive).getMedia());
        }
    }

    public Map<Media, ItemPostTimelineFileViewModel> getViewModelHashMap() {
        return mViewModelHashMap;
    }

    public ObservableField<List<Media>> getMediaList() {
        return mMediaList;
    }

    public ItemPostTimelineFileViewModel.ItemPostTimelineFileViewModelListener getFileClickListener() {
        return (isFromMoreFile, media, fileViewModel) -> {
            if (isFromMoreFile) {
                openDetailFileScreen();
            } else {
                if (!TextUtils.isEmpty(media.getDownloadedPath())) {
                    //File was previously downloaded.
                    DownloaderUtils.openDownloadedFile(mContext, media.getDownloadedPath());
                } else {
                    WallStreetHelper.downloadFile(mContext,
                            media,
                            fileViewModel,
                            new FileDownloadHelper.FileDownloadListener() {
                                @Override
                                public void onDownloadFailed(String requestId, String downloadId, Throwable ex) {
                                    if (mOnItemClickListener != null) {
                                        mOnItemClickListener.onShowSnackBar(true, ex.getMessage());
                                    }
                                }
                            });
                }
            }
        };
    }

    private void openDetailFileScreen() {
        mOnItemClickListener.getRecyclerView().pause();
        mOnItemClickListener.getRecyclerView().setAutoResume(false);
        mAdaptive.startActivityForResult(new WallStreetIntentData(mContext, mOnItemClickListener));
    }

    @BindingAdapter({"files", "viewModelHolder", "showAllFiles", "fileClickListener"})
    public static void bindFiles(LinearLayout linearLayout,
                                 List<Media> fileMedia,
                                 Map<Media, ItemPostTimelineFileViewModel> viewModelHashMap,
                                 boolean isShowAllFiles,
                                 ItemPostTimelineFileViewModel.ItemPostTimelineFileViewModelListener listener) {
        viewModelHashMap.clear();
        if (fileMedia != null && !fileMedia.isEmpty()) {
            linearLayout.removeAllViews();
            for (int i = 0; i < fileMedia.size(); i++) {
                if (!isShowAllFiles && i >= MAX_DISPLAY_FILE_ITEM) {
                    break;
                }

                Media media = fileMedia.get(i);
                ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(linearLayout.getContext()),
                        R.layout.list_item_post_timeline_file,
                        linearLayout,
                        false);
                ItemPostTimelineFileViewModel viewModel = new ItemPostTimelineFileViewModel(media,
                        listener);
                binding.setVariable(com.proapp.sompom.BR.viewModel, viewModel);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                params.topMargin = linearLayout.getContext().getResources().getDimensionPixelSize(R.dimen.space_medium);
                linearLayout.addView(binding.getRoot(), params);
                viewModelHashMap.put(media, viewModel);
            }

            if (!isShowAllFiles && fileMedia.size() > MAX_DISPLAY_FILE_ITEM) {
                //Display more file view at the last.
                ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(linearLayout.getContext()),
                        R.layout.list_item_show_more_post_files,
                        linearLayout,
                        false);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                params.topMargin = linearLayout.getContext().getResources().getDimensionPixelSize(R.dimen.space_medium);
                params.leftMargin = linearLayout.getContext().getResources().getDimensionPixelSize(R.dimen.medium_padding);
                linearLayout.addView(binding.getRoot(), params);
                binding.getRoot().setOnClickListener(v -> {
                    if (listener != null) {
                        listener.onFileClicked(true, null, null);
                    }
                });
            }
        }
    }
}
