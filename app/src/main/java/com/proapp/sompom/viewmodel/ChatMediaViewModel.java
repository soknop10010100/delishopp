package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.TextUtils;
import android.view.HapticFeedbackConstants;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.helper.ResizeAnimator;
import com.proapp.sompom.listener.OnChatItemListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.SelectedChat;
import com.proapp.sompom.model.emun.ChatBg;
import com.proapp.sompom.model.emun.ChatLinkBg;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.ReferencedChat;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.ChatUtility;
import com.proapp.sompom.utils.DateTimeUtils;
import com.proapp.sompom.utils.GenerateLinkPreviewUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.SpannableUtil;
import com.proapp.sompom.viewmodel.newviewmodel.AbsReplyMessageItemTypeViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ItemChatSeenAvatarViewModel;
import com.proapp.sompom.widget.ReplyChatHeaderView;
import com.proapp.sompom.widget.chat.MultiImageChatView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.RealmList;
import timber.log.Timber;

/**
 * Created by He Rotha on 9/14/17.
 */

public class ChatMediaViewModel extends ItemChatSeenAvatarViewModel {

    private static final int SWIPE_REACH_PERCENTAGE = 80; //80%
    private static final int IGNORE_SWIPE_REACH_PERCENTAGE = 10; //80%

    public final ObservableField<ChatBg> mChatBg = new ObservableField<>();
    public final ObservableBoolean mIsShowIconStatus = new ObservableBoolean();
    public final ObservableBoolean mIsShowSharingIcon = new ObservableBoolean();
    private final ObservableField<Drawable> mStatusIcon = new ObservableField<>();
    private final ObservableInt mReplyHeaderVisibility = new ObservableInt(View.GONE);
    private final ObservableField<ReferencedChat> mRepliedChat = new ObservableField<>();
    public final ObservableBoolean mIsLockSwipeReply = new ObservableBoolean(true);
    public final ObservableBoolean mWillVibrateReplyingView = new ObservableBoolean();
    public final ObservableBoolean mShouldCenterSecondaryViewToItsParent = new ObservableBoolean(true);
    private ObservableField<String> mStatus = new ObservableField<>();
    private ObservableInt mEditedIconVisibility = new ObservableInt(View.GONE);
    private ObservableField<String> mTimeStamp = new ObservableField<>();
    private final ObservableInt mSenderAvatarVisibility = new ObservableInt(View.INVISIBLE);
    private final ObservableField<Drawable> mChatItemBackground = new ObservableField<>();
    private final ObservableInt mStatusVisibility = new ObservableInt();

    public User mSender;
    public final Chat mChat;
    protected final OnChatItemListener mOnChatItemListener;
    private ChatUtility.GroupMessage mGroupMessage;
    private final SelectedChat mCurrentSelectChat;
    protected final Activity mContext;
    private List<User> mGroupParticipants;
    protected Conversation mConversation;
    protected User mRecipient;
    protected User mCurrentUser;
    private ChatBg mChatBackground;
    private int mSwipePercentage;
    private boolean mIsSwipeToReplyCallbackFired;
    private int mLastSwipeReplyViewKeyAction = -1;
    private String mSearchKeyWord;
    private boolean mIsDisplayingGroupMessageTimeStamp;
    private String mGroupSeenUserNameDisplay;
    private final String mCurrentUserId;
    private int mCalculatedTextViewStatusHeight;
    private int mDefaultTextViewStatusHeight;

    public ChatMediaViewModel(Activity context,
                              Conversation conversation,
                              Chat chat,
                              ChatUtility.GroupMessage groupMessage,
                              int position,
                              SelectedChat selectChat,
                              String searchKeyWord,
                              OnChatItemListener onItemClickListener) {
        super(position);
        mDefaultTextViewStatusHeight = context.getResources().getDimensionPixelSize(R.dimen.chat_height);
        mCalculatedTextViewStatusHeight = mDefaultTextViewStatusHeight;
        mCurrentUserId = SharedPrefUtils.getUserId(context);
        mShouldCenterSecondaryViewToItsParent.set(true);
        mContext = context;
        mConversation = conversation;
        mChat = chat;
        mCurrentSelectChat = selectChat;
        mSearchKeyWord = searchKeyWord;
        bindChatGroupMessageStatus(groupMessage);
        mChatBg.set(mGroupMessage.getChatBg());
        mOnChatItemListener = onItemClickListener;
        setMessageStatusIconVisibility();
        checkShareIconVisibility();
        checkToInitSender();
        checkUpdateStatusIcon();
        bindReplyChatDataIfAny();
        checkToUpdateDisplayTimeStamp();
        mIsLockSwipeReply.set(!isChatSent());
        loadEditedIconVisibility();
        updateChatStatusVisibilityFromChat();
        checkToDisplayGroupSeenUserName(false);
    }

    private void bindChatGroupMessageStatus(ChatUtility.GroupMessage groupMessage) {
        if (groupMessage != null) {
            mGroupMessage = groupMessage;
            mChatBackground = groupMessage.getChatBg();
            mIsDisplayingGroupMessageTimeStamp = groupMessage.isShowTime();
            mSenderAvatarVisibility.set(mChatBackground.shouldDisplaySenderAvatar() ? View.VISIBLE : View.INVISIBLE);
        }
    }

    protected boolean isCurrentUserSender() {
        return mChat != null && TextUtils.equals(mChat.getSenderId(), mCurrentUserId);
    }

    private void updateChatStatusVisibilityFromChat() {
        mStatusVisibility.set(mChat.isExpandHeight() ? View.VISIBLE : View.GONE);
    }

    public void updateMessageBackground(ChatUtility.GroupMessage groupMessage) {
        bindChatGroupMessageStatus(groupMessage);
        if (isChatBackgroundShouldUpdate()) {
            mChatItemBackground.set(ContextCompat.getDrawable(mContext,
                    mChatBackground.getDrawable(mChat.isExpandHeight())));
        }
    }

    protected boolean isChatBackgroundShouldUpdate() {
        return true;
    }

    public ObservableInt getSenderAvatarVisibility() {
        return mSenderAvatarVisibility;
    }

    public boolean isDisplayingGroupMessageTimeStamp() {
        return mIsDisplayingGroupMessageTimeStamp;
    }

    public void onChatContentEdited() {
        //Client that has text display must implement this method.
    }

    public String getDisplayChatContent() {
        //Client that has text display must implement this method.
        return null;
    }

    public ObservableInt getEditedIconVisibility() {
        return mEditedIconVisibility;
    }

    public void loadEditedIconVisibility() {
        mEditedIconVisibility.set(mChat.getEdited() ? View.VISIBLE : View.GONE);
        mEditedIconVisibility.notifyChange();
    }

    private boolean isSupportSharingOptionType() {
        Chat.Type type = mChat.getType();
        if (type == Chat.Type.TEXT) {
            if (TextUtils.isEmpty(mChat.getContent())) {
                return false;
            }

            String firstUrlIndex = GenerateLinkPreviewUtil.getPreviewLink(mChat.getContent());
            return GenerateLinkPreviewUtil.isValidUrl(firstUrlIndex);
        } else {
            return true;
        }
    }

    private boolean shouldShowSharingOption() {
        return isChatSent() && isSupportSharingOptionType();
    }

    protected void clearSearchKeyword() {
        mSearchKeyWord = null;
    }

    protected String getSearchKeyWord() {
        return mSearchKeyWord;
    }

    public ObservableBoolean getShouldCenterSecondaryViewToItsParent() {
        return mShouldCenterSecondaryViewToItsParent;
    }

    public ObservableBoolean getWillVibrateReplyingView() {
        return mWillVibrateReplyingView;
    }

    public void revalidateChatSwipeAbilityStatus() {
        mIsLockSwipeReply.set(!isChatSent());
    }

    private boolean isChatSent() {
        return mChat.getStatus() != MessageState.SENDING &&
                mChat.getStatus() != MessageState.FAIL;
    }

    public ObservableBoolean getIsLockSwipeReply() {
        return mIsLockSwipeReply;
    }

    public boolean isRepliedChatType() {
        return mChat.getChatReferenceWrapper() != null &&
                mChat.getChatReferenceWrapper().getReferencedChat() != null;
    }

    public Drawable getHeaderBackground() {
        ChatLinkBg chatLinkBg = ChatLinkBg.getChatLinkBg(getChatBackground());
        if (chatLinkBg != null) {
            return ContextCompat.getDrawable(getActivity(), chatLinkBg.getDrawableTitle());
        }

        return null;
    }

    protected void updateChatBackground(ChatBg chatBg) {
        mChatBackground = chatBg;
    }

    public ChatBg getChatBackground() {
        return mChatBackground;
    }

    public ObservableInt getReplyHeaderVisibility() {
        return mReplyHeaderVisibility;
    }

    public ObservableField<ReferencedChat> getRepliedChat() {
        return mRepliedChat;
    }

    private void bindReplyChatDataIfAny() {
        if (mChat != null &&
                mChat.getChatReferenceWrapper() != null &&
                mChat.getChatReferenceWrapper().getReferencedChat() != null) {
            mReplyHeaderVisibility.set(View.VISIBLE);
            mRepliedChat.set(mChat.getChatReferenceWrapper().getReferencedChat());
        } else {
            mReplyHeaderVisibility.set(View.GONE);
        }
    }

    public ObservableField<Drawable> getBackground(View view, Context context) {
        view.setTag(R.id.tagContainer, mChatBackground);
        mChatItemBackground.set(ContextCompat.getDrawable(context, mChatBackground.getDrawable(mChat.isExpandHeight())));
        return mChatItemBackground;
    }

    public AbsReplyMessageItemTypeViewModel.ReplyMessageItemTypeViewModelListener getRepliedViewListener(ViewGroup rootParent) {
        return new AbsReplyMessageItemTypeViewModel.ReplyMessageItemTypeViewModelListener() {
            @Override
            public void onRepliedChatClicked(Chat repliedChat, boolean isInInputReplyMode) {
                if (mOnChatItemListener != null) {
                    mOnChatItemListener.onRepliedChatClicked(repliedChat, isInInputReplyMode);
                }
            }

            @Override
            public void onLongClicked() {
                if (rootParent != null) {
                    rootParent.performLongClick();
                }
            }
        };
    }

    public MultiImageChatView.GifVideoPlayerAddListener getGifVideoPlayerAddListener() {
        return new MultiImageChatView.GifVideoPlayerAddListener() {
            @Override
            public void onGifVideoPlayerCreated(String mediaId, SimpleExoPlayer exoPlayer) {
                if (mOnChatItemListener != null) {
                    mOnChatItemListener.onGifVideoPlayerCreated(mediaId, exoPlayer);
                }
            }

            @Override
            public SimpleExoPlayer getGifVideoPlayer(String mediaId) {
                if (mOnChatItemListener != null) {
                    return mOnChatItemListener.getGifVideoPlayer(mediaId);
                }

                return null;
            }

            @Override
            public void onGifVideoPlayerReleased(String mediaId) {
                if (mOnChatItemListener != null) {
                    mOnChatItemListener.onGifVideoPlayerReleased(mediaId);
                }
            }
        };
    }

    public void checkShareIconVisibility() {
        mIsShowSharingIcon.set(shouldShowSharingOption());
    }

    public void checkToUpdateDisplayTimeStamp() {
        mTimeStamp.set(DateTimeUtils.parse(mContext, mChat.getDate().getTime(),
                true,
                mContext.getString(R.string.chat_message_call_at)));
    }

    public void setMessageStatusIconVisibility() {
        mIsShowIconStatus.set(!(mChat.getStatus() == MessageState.SEEN || hasSeenAvatar()));
    }

    public Chat getChat() {
        return mChat;
    }

    public User getRecipient() {
        return mRecipient;
    }

    public void setRecipient(User recipient) {
        mRecipient = recipient;
    }

    public User getCurrentUser() {
        return mCurrentUser;
    }

    public void setCurrentUser(User currentUser) {
        mCurrentUser = currentUser;
    }

    private boolean hasSeenAvatar() {
        if (mChat.getSeenParticipants() != null && !mChat.getSeenParticipants().isEmpty()) {
            boolean hasSeen = false;
            for (User seenParticipant : mChat.getSeenParticipants()) {
                if (seenParticipant.isSeenMessage()) {
                    return true;
                }
            }

            return hasSeen;
        }

        return false;
    }

    private void checkToInitSender() {
        if (mConversation == null) {
            //Individual chat
            if (mChat.getSender() == null) {
                mSender = mOnChatItemListener.getUser(mChat.getSenderId());
            } else {
                mSender = mChat.getSender();
            }
        } else {
            //Group or individual chat
            mGroupParticipants = mConversation.getParticipants();
            User senderIngroupChat = findSenderInGroupChat(mChat.getSenderId());
            if (senderIngroupChat != null) {
                mSender = senderIngroupChat;
            } else {
                if (mChat.getSender() == null) {
                    mSender = mOnChatItemListener.getUser(mChat.getSenderId());
                } else {
                    mSender = mChat.getSender();
                }
            }
        }
        bindSeenAvatar(false);
    }

    public void bindSeenAvatar(boolean isUpdate) {
        if (mChat.getSeenParticipants() != null) {
//            Timber.i("bindSeenAvatar: " + mChat.getSeenParticipants().size() + ", Position: " + mPosition);
            bindSeenStatus(new ArrayList<>(mChat.getSeenParticipants()), mChat);
            if (isUpdate) {
                checkToDisplayGroupSeenUserName(true);
            }
        }
    }

    private User findSenderInGroupChat(String senderId) {
        if (mGroupParticipants != null) {
            for (User groupParticipant : mGroupParticipants) {
                if (groupParticipant.getId().matches(senderId)) {
                    return groupParticipant;
                }
            }
        }

        return null;
    }

    public Activity getActivity() {
        return mContext;
    }

    public MultiImageChatView.OnLongClickListener getOnLongClickListener(View view) {
        return productMedia -> {
            if (ChatMediaViewModel.this.shouldDetectLongPress() && mOnChatItemListener != null) {
                view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY,
                        HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);
                mOnChatItemListener.onChatItemLongPressClick(mChat, productMedia);
            }
        };
    }

    public OnClickListener onAudioLongClickListener() {
        return () -> {
            if (shouldDetectLongPress() && mOnChatItemListener != null) {
                mOnChatItemListener.onChatItemLongPressClick(mChat, null);
            }
        };
    }

    private boolean shouldDetectLongPress() {
        return mSwipePercentage < IGNORE_SWIPE_REACH_PERCENTAGE;
    }

    public boolean onLongClick(View view) {
        Timber.i("onLongClick");
        if (shouldDetectLongPress() && mOnChatItemListener != null) {
            view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY,
                    HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);
            mOnChatItemListener.onChatItemLongPressClick(mChat, null);
        }
        return true;
    }

    public void checkUpdateStatusIcon() {
//        Timber.i(" mChat.isDeleted(): " + mChat.isDeleted() + ", Status: " + mChat.getStatus() + ", mChat.getSeenParticipants() " + mChat.getSeenParticipants() + ", id: " + mChat.getId());
        if (mChat.getIgnoreSeenStatus() ||
                shouldIgnoreDisplayStatusIconForRemovedIcon() ||
                isMessageSeenAndNoSeenAvatar()) {
            mStatusIcon.set(null);
        } else {
            mStatusIcon.set(ContextCompat.getDrawable(mContext, mChat.getStatus().getIcon()));
        }
    }

    private boolean isMessageSeenAndNoSeenAvatar() {
        return mChat.getStatus() == MessageState.SEEN &&
                (mChat.getSeenParticipants() == null || mChat.getSeenParticipants().isEmpty());
    }

    private boolean shouldIgnoreDisplayStatusIconForRemovedIcon() {
        return mChat.isDeleted() && ((mChat.getStatus() == MessageState.FAIL ||
                (mChat.getSeenParticipants() != null && !mChat.getSeenParticipants().isEmpty())));
    }

    public ObservableField<Drawable> getStatusIcon() {
        return mStatusIcon;
    }

    public ObservableField<String> getTime(Context context) {
        return mTimeStamp;
    }

    public void onForwardClick() {
        mOnChatItemListener.onForwardClick(mChat);
    }

    public ObservableField<String> getStatus(Context context) {
        return mStatus;
    }

    public void revalidateMessageStatusText() {
        checkToDisplayGroupSeenUserName(true);
        mStatus.notifyChange();
    }

    public int getVisibility() {
        if (mGroupMessage.isShowTime() || mChat.isExpandHeight()) {
            return View.VISIBLE;
        } else {
            return View.GONE;
        }
    }

    public ObservableInt getStatusVisibility() {
        return mStatusVisibility;
    }

    public void onRetryClick() {
        if (mChat.getStatus() == MessageState.FAIL) {
            mOnChatItemListener.onMessageRetryClick(mChat);
        }
    }

    public RealmList<String> updateSeenUserList(RealmList<String> seen) {
//        Timber.i("updateSeenUserList from: " + new Gson().toJson(mChat.getSeen()) + ", to: " + new Gson().toJson(seen));
        if (mChat.getSeen() == null || mChat.getSeen().isEmpty()) {
            mChat.setSeen(new RealmList<>());
            mChat.getSeen().addAll(seen);
        } else {
            for (String id : seen) {
                if (!mChat.getSeen().contains(id)) {
                    mChat.getSeen().add(id);
                }
            }
        }
//        Timber.i("updateSeenUserList: " + new Gson().toJson(mChat.getSeen()));
        mChat.setStatus(MessageState.SEEN);
        checkToDisplayGroupSeenUserName(true);

        return mChat.getSeen();
    }

    public List<Media> photoUrl() {
        return mChat.getMediaList();
    }

    public void onChatItemClicked(View rootView) {
        Timber.i("onChatItemClicked");
        checkToUpdateDisplayTimeStamp();
        if (mCurrentSelectChat.getSelectView() != null &&
                mCurrentSelectChat.getSelectView() != rootView &&
                mCurrentSelectChat.getSelectChat() != null) {
            View textViewStatus = mCurrentSelectChat.getSelectView().findViewById(R.id.textViewStatus);
            if (textViewStatus.getVisibility() == View.VISIBLE) {
                ChatMediaViewModel.this.animateLayout(mCurrentSelectChat.getSelectView(),
                        mCurrentSelectChat.getSelectChat(),
                        mCurrentSelectChat.getGroupMessage());
            } else {
                mCurrentSelectChat.getSelectChat().setExpandHeight(false);
            }
        }
        ChatMediaViewModel.this.animateLayout(rootView, mChat, mGroupMessage);
        updateChatStatusVisibilityFromChat();
    }

    public void onSeenAvatarClicked(View rootView) {
        onChatItemClicked(rootView);
    }

    private void checkToDisplayGroupSeenUserName(boolean forceBuild) {
        if (mConversation.isGroup()) {
            if (mChat.getStatus() == MessageState.SEEN) {
                if (TextUtils.isEmpty(mGroupSeenUserNameDisplay) || forceBuild) {
                    buildGroupSeenByUserName();
                }
                if (!TextUtils.isEmpty(mGroupSeenUserNameDisplay)) {
                    Timber.i("set status to: " + mGroupSeenUserNameDisplay);
                    mStatus.set(mGroupSeenUserNameDisplay);
                    mStatus.notifyChange();
                    mOnChatItemListener.onCalculateSeenDisplayNameHeight(mChat.getId(),
                            mGroupSeenUserNameDisplay,
                            !isCurrentUserSender(),
                            height -> {
                                Timber.i("mGroupSeenUserNameDisplay: " + mGroupSeenUserNameDisplay +
                                        ", calculated height: " + height
                                        + ", of message: " + mChat.getContent());
                                mCalculatedTextViewStatusHeight = height;
                            });
                } else {
                    mStatus.set(mContext.getString(mChat.getStatus().getTextStatus()));
                }
            } else {
                mStatus.set(mContext.getString(mChat.getStatus().getTextStatus()));
            }
        } else {
            mStatus.set(mContext.getString(mChat.getStatus().getTextStatus()));
        }
    }

    private void animateLayout(View view,
                               final Chat chat,
                               final ChatUtility.GroupMessage groupMessage) {
        final View textViewStatus = view.findViewById(R.id.textViewStatus);
        final View textViewTime = view.findViewById(R.id.textViewTime);
        final View textViewMessage = view.findViewById(R.id.message);
        ResizeAnimator statusAnimator;
        ResizeAnimator timeAnimator = null;
        if (chat.isExpandHeight()) {
            statusAnimator = new ResizeAnimator(mCalculatedTextViewStatusHeight, 0, textViewStatus);
            if (!groupMessage.isShowTime()) {
                timeAnimator = new ResizeAnimator(mDefaultTextViewStatusHeight, 0, textViewTime);
            }
            mCurrentSelectChat.setSelect(null, null, null);
        } else {
            statusAnimator = new ResizeAnimator(0, mCalculatedTextViewStatusHeight, textViewStatus);
            if (!groupMessage.isShowTime()) {
                timeAnimator = new ResizeAnimator(0, mDefaultTextViewStatusHeight, textViewTime);
            }
            mCurrentSelectChat.setSelect(view, chat, groupMessage);
        }
        statusAnimator.setAnimatorListener(new ResizeAnimator.AnimatorListener() {
            @Override
            public void onStart() {
                if (!chat.isExpandHeight()) {
                    textViewStatus.setVisibility(View.VISIBLE);
                    textViewTime.setVisibility(View.VISIBLE);
                }
                chat.setExpandHeight(!chat.isExpandHeight());
                if (textViewMessage != null) {
                    Object tag = textViewMessage.getTag(R.id.tagContainer);
                    if (tag instanceof ChatBg) {
                        ChatBg dr = (ChatBg) tag;
                        textViewMessage.setBackgroundResource(dr.getDrawable(chat.isExpandHeight()));
                    }
                }
            }

            @Override
            public void onEnd() {
                if (!chat.isExpandHeight()) {
                    textViewStatus.setVisibility(View.GONE);
                    if (!groupMessage.isShowTime()) {
                        textViewTime.setVisibility(View.GONE);
                    }
                }
            }
        });
        statusAnimator.startAnimation();
        if (timeAnimator != null) {
            timeAnimator.startAnimation();
        }
    }

    public OnItemClickListener<Integer> onImageClickListener() {
        return integer -> {
            if (mOnChatItemListener != null)
                mOnChatItemListener.onImageClick(photoUrl(), integer);
        };
    }

    public Media getGifMedia() {
        return mChat.getMediaList().get(0);
    }

    public String getViaGifText() {
        if (mChat.getMediaList().get(0).getType() == MediaType.TENOR_GIF) {
            return mContext.getString(R.string.chat_tenor_gif);
        } else {
            return "";
        }
    }

    private void buildGroupSeenByUserName() {
        Timber.i("Message Id: " + mChat.getId() + ", Seen: " + new Gson().toJson(mChat.getSeen()));
        if (mChat.getSeen() != null &&
                !mChat.getSeen().isEmpty() &&
                mConversation.getParticipants() != null &&
                !mConversation.getParticipants().isEmpty()) {
            List<String> seenList = new ArrayList<>();
            StringBuilder stringBuilder = new StringBuilder();
            for (String userId : mChat.getSeen()) {
                for (User participant : mConversation.getParticipants()) {
                    if (TextUtils.equals(participant.getId(), userId) && !seenList.contains(participant.getId())) {
                        seenList.add(participant.getId());
                        //Ignore render current user name
                        if (!TextUtils.equals(mCurrentUserId, participant.getId())) {
                            if (!TextUtils.isEmpty(stringBuilder)) {
                                stringBuilder.append(", ");
                            }
                            stringBuilder.append(SpannableUtil.capitaliseOnlyFirstLetter(participant.getFirstName()));
                        }
                        break;
                    }
                }
            }

            if (seenList.size() == mConversation.getParticipants().size()) {
                mGroupSeenUserNameDisplay = mContext.getString(R.string.chat_seen_by_status)
                        + " "
                        + mContext.getString(R.string.chat_seen_by_everyone);
            } else {
                if (!TextUtils.isEmpty(stringBuilder)) {
                    mGroupSeenUserNameDisplay = mContext.getString(R.string.chat_seen_by_status) + " " + stringBuilder.toString();
                }
            }
        }

        Timber.i("mGroupSeenUserNameDisplay: " + mGroupSeenUserNameDisplay);
    }

    protected User getUserFromParticipant(String userId) {
        /*
        First we try to get user from participant list of conversation so as to have the latest
        update of user data. If that user is not found, will take the message sender directly.
         */
        User checkParticipant = null;
        if (mConversation != null && mConversation.getParticipants() != null) {
            for (User participant : mConversation.getParticipants()) {
                if (TextUtils.equals(participant.getId(), userId)) {
                    checkParticipant = participant;
                    break;
                }
            }
        }

        if (checkParticipant == null) {
            checkParticipant = mChat.getSender();
        }

        return checkParticipant;
    }

    public SwipeRevealLayout.SimpleSwipeListener getSwipeListener() {
        return new SwipeRevealLayout.SimpleSwipeListener() {
            @Override
            public void onClosed(SwipeRevealLayout view) {
//                Timber.i("onClosed");
                mWillVibrateReplyingView.set(false);
                mIsSwipeToReplyCallbackFired = false;
                mLastSwipeReplyViewKeyAction = -1;
            }

            @Override
            public void onOpened(SwipeRevealLayout view) {
//                Timber.i("onOpened: mSwipePercentage: " + mSwipePercentage +
//                        ", mLastSwipeReplyViewKeyAction: " + mLastSwipeReplyViewKeyAction);
                if (mSwipePercentage < SWIPE_REACH_PERCENTAGE && !mWillVibrateReplyingView.get()) {
                    /*
                    Note: Sometime when use swipe the view fast, the callback onOpented() invoke
                    but the expected swipe reach percentage value does not get what we want and in
                    this case we consider as it is reached too.
                     */
                    mSwipePercentage = SWIPE_REACH_PERCENTAGE;
                    mWillVibrateReplyingView.set(true);
                }

                /*
                Check to invoke callback again if it was previous not been invoke properly.
                Note: We will check to invoke callback when user release shipping button.
                 */
                if ((mLastSwipeReplyViewKeyAction < 0 || mLastSwipeReplyViewKeyAction == MotionEvent.ACTION_UP) &&
                        !mIsSwipeToReplyCallbackFired) {
                    invokeSwipeToReplyCallback(view);
                }
            }

            @Override
            public void onSlide(SwipeRevealLayout view, float slideOffset) {
                mSwipePercentage = (int) (slideOffset * 100);
//                Timber.i("onSlide: " + mSwipePercentage);
                if (mSwipePercentage >= SWIPE_REACH_PERCENTAGE) {
                    if (!mWillVibrateReplyingView.get()) {
                        mWillVibrateReplyingView.set(true);
                    }
                } else {
                    mWillVibrateReplyingView.set(false);
                    mIsSwipeToReplyCallbackFired = false;
                }
            }
        };
    }

    public View.OnTouchListener getTouchListener(SwipeRevealLayout layout) {
        return (view, motionEvent) -> {
            mLastSwipeReplyViewKeyAction = motionEvent.getAction();
//            Timber.i("motionEvent: " + motionEvent.getAction());
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                if (mSwipePercentage >= SWIPE_REACH_PERCENTAGE) {
                    new Handler().postDelayed(() -> {
                        invokeSwipeToReplyCallback(layout);
                    }, 50);
                }
            } else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                view.performClick();
            }
            return false;
        };
    }

    private void invokeSwipeToReplyCallback(SwipeRevealLayout view) {
//        Timber.i("invokeSwipeToReplyCallback");
        mIsSwipeToReplyCallbackFired = true;
        view.close(true);
        if (mOnChatItemListener != null) {
            mOnChatItemListener.onSwipeToReply(mChat);
        }
    }

    public interface ChatMediaViewModelListener {
        void onGotStatusTextViewMeasuredHeight(int height);
    }

    public interface CalculateSeenNameDisplayCallback {
        void onCalculationFinished(int height);
    }

    @BindingAdapter(value = {"bindData", "repliedListener", "isCurrentUserSender", "mainChat"})
    public static void bindReplyChatHeaderViewData(ReplyChatHeaderView replyChatHeaderView,
                                                   ReferencedChat repliedChat,
                                                   AbsReplyMessageItemTypeViewModel.ReplyMessageItemTypeViewModelListener listener,
                                                   boolean isCurrentUserSender,
                                                   Chat mainChat) {
        if (repliedChat != null) {
            Chat newRepliedChat = new Chat();
            newRepliedChat.setId(repliedChat.getId());
            newRepliedChat.setContent(repliedChat.getContent());
            newRepliedChat.setDate(repliedChat.getDate());
            newRepliedChat.setSenderId(repliedChat.getSenderId());
            newRepliedChat.setSender(repliedChat.getSender());
            if (repliedChat.getMedia() != null) {
                newRepliedChat.setMediaList(Collections.singletonList(repliedChat.getMedia()));
            }
//            Timber.i("repliedChat: " + new Gson().toJson(repliedChat) + ", newRepliedChat: " + new Gson().toJson(newRepliedChat));
            replyChatHeaderView.buildReplayView(newRepliedChat,
                    mainChat,
                    false,
                    isCurrentUserSender,
                    listener);
        }
    }

    @BindingAdapter({"lockRevealLayout", "swipeListener", "willVibrateReplyingView", "touchListener"})
    public static void bindSwipeRevealLayout(SwipeRevealLayout layout,
                                             boolean lockRevealLayout,
                                             SwipeRevealLayout.SwipeListener swipeListener,
                                             boolean willVibrateReplyingView,
                                             View.OnTouchListener touchListener) {
        layout.setLockDrag(lockRevealLayout);
        layout.setOnTouchListener(touchListener);
        layout.setSwipeListener(swipeListener);
        if (willVibrateReplyingView) {
            layout.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY,
                    HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);
        }
    }

    @BindingAdapter({"setCenterParentVertical", "shouldMakeCenter"})
    public static void setSecondaryViewToCenterParent(View secondaryView,
                                                      SwipeRevealLayout parentView,
                                                      boolean shouldMakeCenter) {
        if (shouldMakeCenter) {
            parentView.post(() -> {
                int y = (parentView.getHeight() - secondaryView.getHeight()) / 2;
//                Timber.i("parentView: " + parentView.getHeight() + ", secondary view's height: " + secondaryView.getHeight() + ", Y: " + y);
                secondaryView.setY(y);
            });
        }
    }
}
