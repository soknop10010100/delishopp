package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.services.datamanager.LoginDataManager;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

/**
 * Created by Veasna Chhom on 4/21/21.
 */
public class LoginWithPhoneAndPasswordViewModel extends AbsLoginViewModel<LoginDataManager, LoginWithPhoneAndPasswordViewModel.LoginWithPhoneAndPasswordViewModelListener> {

    public LoginWithPhoneAndPasswordViewModel(Context context,
                                              LoginDataManager dataManager,
                                              LoginWithPhoneAndPasswordViewModelListener listener) {
        super(context, dataManager, listener);
    }

    @Override
    protected boolean shouldEnableActionButton() {
        return isValidPhoneNumber() && !TextUtils.isEmpty(mPassword.get());
    }

    @Override
    protected void onContinueButtonClicked() {
        requestLogin(mDataManager.getLoginWithPhoneService(String.valueOf(mListener.getCountryCodePicker()
                        .getPhoneNumber().getNationalNumber()),
                mPassword.get()),
                true);
    }

    private boolean isValidPhoneNumber() {
        return mListener.getCountryCodePicker().isValid();
    }

    public interface LoginWithPhoneAndPasswordViewModelListener extends AbsLoginViewModel.AbsLoginViewModelListener {
        CountryCodePicker getCountryCodePicker();
    }
}
