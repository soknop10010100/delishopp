package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;

import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeVerifyPaymentWithABAResponse;
import com.proapp.sompom.newui.fragment.AbsSupportABAPaymentFragment;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.PayWithCardDataManager;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Veasna Chhom on 3/17/22.
 */

public class PayWithCardFragmentViewModel extends AbsLoadingViewModel {

    private static final long INTERVAL = 5000L; //5 Seconds

    private final PayWithCardDataManager mDataManager;
    private CountDownTimer mCountDownTimer;
    private final List<Disposable> mDisposableList = new ArrayList<>();

    public PayWithCardFragmentViewModel(Context context, PayWithCardDataManager dataManager) {
        super(context);
        mDataManager = dataManager;
        initVerifyingABAPaymentStatusTimer();
    }

    private void initVerifyingABAPaymentStatusTimer() {
        mCountDownTimer = new CountDownTimer(Long.MAX_VALUE, INTERVAL) {
            @Override
            public void onTick(long l) {
                verifyOrderPaymentStatus();
            }

            @Override
            public void onFinish() {

            }
        };
        mCountDownTimer.start();
    }

    public void cancelTimer() {
        mCountDownTimer.cancel();
    }

    private void verifyOrderPaymentStatus() {
        Timber.i("verifyOrderPaymentStatus");
        Observable<Response<ConciergeVerifyPaymentWithABAResponse>> observable = mDataManager.verifyOrderPaymentStatus();
        ResponseObserverHelper<Response<ConciergeVerifyPaymentWithABAResponse>> helper = new ResponseObserverHelper<>(getContext(), observable);
        Disposable disposable = helper.execute(new OnCallbackListener<Response<ConciergeVerifyPaymentWithABAResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e(ex.getMessage());
            }

            @Override
            public void onComplete(Response<ConciergeVerifyPaymentWithABAResponse> result) {
                if (result.isSuccessful() && result.body().isPaid()) {
                    mCountDownTimer.cancel();
                    Intent intent = new Intent(AbsSupportABAPaymentFragment.RECEIVE_ABA_PAYMENT_PUSHBACK_EVENT);
                    intent.putExtra(AbsSupportABAPaymentFragment.BASKET_ID, mDataManager.getBasketId());
                    intent.putExtra(AbsSupportABAPaymentFragment.IS_ABA_PAYMENT_SUCCESS, true);
                    SendBroadCastHelper.verifyAndSendBroadCast(mContext, intent);
                    clearAllVerifyingPaymentRequest();
                }
            }
        });
        mDisposableList.add(disposable);
    }

    private void clearAllVerifyingPaymentRequest() {
        for (Disposable disposable : mDisposableList) {
            disposable.dispose();
        }
    }
}
