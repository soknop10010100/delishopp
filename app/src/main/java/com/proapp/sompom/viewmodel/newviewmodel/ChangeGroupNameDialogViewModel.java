package com.proapp.sompom.viewmodel.newviewmodel;

import android.view.View;

import androidx.databinding.ObservableField;

import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.Objects;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 7/5/20.
 */
public class ChangeGroupNameDialogViewModel extends AbsLoadingViewModel {

    private ChangeGroupNameViewModelListener mListener;
    private ObservableField<String> mGroupName = new ObservableField<>();
    private Conversation mConversation;
    private View mRoot;

    public ChangeGroupNameDialogViewModel(View view,
                                          Conversation conversation,
                                          ChangeGroupNameViewModelListener listener) {
        super(view.getContext());
        mRoot = view;
        mListener = listener;
        mConversation = conversation;
        mGroupName.set(mConversation.getGroupName());
    }

    public ObservableField<String> getGroupName() {
        return mGroupName;
    }

    public void updateGroupName() {
        //Noting has been changed.
        if (Objects.equals(mGroupName.get(), mConversation.getGroupName())) {
            return;
        }

        GroupDetail groupDetail = new GroupDetail();
        groupDetail.setName(mGroupName.get());
        Timber.e("New group name: " + mGroupName.get());

        if (mListener != null) {
            mListener.onChangeNameClick(groupDetail);
        }
    }

    public void clearInput() {
        mGroupName.set("");
    }

    public interface ChangeGroupNameViewModelListener {
        void onChangeNameClick(GroupDetail groupDetail);
    }
}
