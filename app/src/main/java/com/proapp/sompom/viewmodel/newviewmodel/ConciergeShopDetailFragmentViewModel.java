package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.view.View;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeShop;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ConciergeShopDetailDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Veasna Chhom on 9/9/21.
 */
public class ConciergeShopDetailFragmentViewModel extends AbsLoadingViewModel {

    private final ConciergeShopDetailDataManager mDataManager;
    private final ConciergeShopDetailFragmentViewModelListener mListener;
    private final ObservableBoolean mIsRefreshing = new ObservableBoolean();
    private ObservableInt mCheckOutViewMarginTopVisibility = new ObservableInt();
    private final ObservableField<String> mShopTitle = new ObservableField<>();
    private final ObservableField<String> mShopPhoto = new ObservableField<>();
    private final ObservableField<String> mShopDescription = new ObservableField<>();
    private final ObservableField<String> mShopLocation = new ObservableField<>();
    private final ConciergeCartBottomSheetViewModel mCartViewModel;
    private ConciergeShop mConciergeShop;

    public ConciergeShopDetailFragmentViewModel(Context context,
                                                ConciergeShopDetailDataManager dataManager,
                                                ConciergeCartBottomSheetViewModel cartViewModel,
                                                ConciergeShopDetailFragmentViewModelListener listener) {
        super(context);
        mCartViewModel = cartViewModel;
        mDataManager = dataManager;
        mListener = listener;
    }

    public ObservableInt getCheckOutViewMarginTopVisibility() {
        return mCheckOutViewMarginTopVisibility;
    }

    public ConciergeShop getConciergeShop() {
        return mConciergeShop;
    }

    public void setCheckOutViewMarginTopVisibility(boolean isVisible) {
        mCheckOutViewMarginTopVisibility.set(isVisible ? View.VISIBLE : View.GONE);
    }

    public ConciergeCartBottomSheetViewModel getCartViewModel() {
        return mCartViewModel;
    }

    public ObservableField<String> getShopTitle() {
        return mShopTitle;
    }

    public ObservableField<String> getShopPhoto() {
        return mShopPhoto;
    }

    public ObservableField<String> getShopDescription() {
        return mShopDescription;
    }

    public ObservableField<String> getShopLocation() {
        return mShopLocation;
    }

    public ObservableBoolean getIsRefreshing() {
        return mIsRefreshing;
    }

    public void onRefresh() {
        mIsRefreshing.set(true);
    }

    @Override
    public void onRetryClick() {
        getStoreDetail();
    }

    public void getStoreDetail() {
        if (!mIsRefreshing.get()) {
            showLoading();
        }
        Observable<Response<ConciergeShop>> service = mDataManager.getStoreDetailService();
        ResponseObserverHelper<Response<ConciergeShop>> helper = new ResponseObserverHelper<>(getContext(), service);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeShop>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.i("onFail: " + ex.getMessage());
                hideLoading();
                showError(ex.getMessage());
            }

            @Override
            public void onComplete(Response<ConciergeShop> result) {
                mConciergeShop = result.body();
                hideLoading();
                bindData(result.body());
                if (mListener != null) {
                    mListener.onLoadStoreDetailSuccess(result.body(), mDataManager.isShopClosed());
                }
            }
        }));
    }

    private void bindData(ConciergeShop shopDetail) {
        if (shopDetail != null) {
            mShopPhoto.set(shopDetail.getPicture());
            mShopTitle.set(shopDetail.getName());
            mShopDescription.set(shopDetail.getDescription());
            mShopLocation.set(shopDetail.getLocation());
        }
    }

    public interface ConciergeShopDetailFragmentViewModelListener {
        void onLoadStoreDetailSuccess(ConciergeShop store, boolean isShopClosed);
    }
}
