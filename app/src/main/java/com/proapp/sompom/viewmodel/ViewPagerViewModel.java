package com.proapp.sompom.viewmodel;

import androidx.fragment.app.FragmentManager;

import com.proapp.sompom.model.result.MoreGame;

import java.util.List;

/**
 * Created by He Rotha on 9/11/17.
 */

public class ViewPagerViewModel {

    public List<MoreGame> mMoreGames;
    public FragmentManager mFragmentManager;

    public ViewPagerViewModel(List<MoreGame> moreGames, FragmentManager fragmentManager) {
        mMoreGames = moreGames;
        mFragmentManager = fragmentManager;
    }

    public List<MoreGame> getMoreGames() {
        return mMoreGames;
    }

    public FragmentManager getFragmentManager() {
        return mFragmentManager;
    }
}
