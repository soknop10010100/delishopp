package com.proapp.sompom.viewmodel.newviewmodel;

import static com.proapp.sompom.newui.ChatActivity.REFRESH_NOTIFICATION_BADGE_ACTION;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ConciergeFragmentAdapter;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.intent.ConciergeDeliveryIntent;
import com.proapp.sompom.intent.newintent.ConciergeOrderHistoryContainerIntent;
import com.proapp.sompom.intent.newintent.ConciergeOrderTrackingIntent;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.ConciergeItemAdaptive;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeShopAnnouncementResponse;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.result.UserBadge;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.UserLoginRegisterBottomSheetDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ConciergeFragmentDataManager;
import com.proapp.sompom.utils.HTTPResponse;
import com.proapp.sompom.viewmodel.ConciergeSupportAddItemToCardViewModel;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 26/8/21.
 */
public class ConciergeFragmentViewModel extends ConciergeSupportAddItemToCardViewModel<ConciergeFragmentDataManager, ConciergeFragmentViewModel.ConciergeFragmentViewModelListener>
        implements ConciergeFragmentAdapter.ConciergeFragmentAdapterListener {

    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    public final ObservableBoolean mEnableRefresh = new ObservableBoolean(true);
    private final ObservableBoolean mIsHasPending = new ObservableBoolean();
    private final ObservableBoolean mCheckSotButtonVisibility = new ObservableBoolean();
    private final ObservableField<ConciergeFragmentCurrentLocationViewModel> mCurrentLocationViewModel = new ObservableField<>();
    private final ObservableInt mOrderBadgeCount = new ObservableInt();

    private Context mContext;
    private ConciergeCartBottomSheetViewModel mCartViewModel;

    public boolean isAlreadyLoadMore = false;

    public ConciergeFragmentViewModel(ConciergeFragmentDataManager dataManager,
                                      ConciergeCartBottomSheetViewModel cartViewModel,
                                      ConciergeFragmentViewModelListener listener) {
        super(dataManager.getContext(), listener, dataManager);
        mContext = dataManager.getContext();
        mDataManager = dataManager;
        mCartViewModel = cartViewModel;
        mListener = listener;
        mCheckSotButtonVisibility.set(!ApplicationHelper.isInExpressMode(mContext));
        bindNotificationBadge();
        initData();
    }

    public ObservableBoolean getCheckSotButtonVisibility() {
        return mCheckSotButtonVisibility;
    }

    public ObservableBoolean getEnableRefresh() {
        return mEnableRefresh;
    }

    public ConciergeCartBottomSheetViewModel getCartViewModel() {
        return mCartViewModel;
    }

    public ObservableBoolean getIsHasPending() {
        return mIsHasPending;
    }

    public void setIsHasPending(boolean isHasPending) {
        mIsHasPending.set(isHasPending);
    }

    public ObservableField<ConciergeFragmentCurrentLocationViewModel> getCurrentLocationViewModel() {
        return mCurrentLocationViewModel;
    }

    public ObservableInt getOrderBadgeCount() {
        return mOrderBadgeCount;
    }

    public void initData() {
        loadData(false);
        getUserSelectedLocation();
        if (mListener != null) {
            mListener.notifyShouldShowCartBottomSheet();
        }
    }

    public void onOrderTrackingButtonClick(View view) {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).startActivityForResult(new ConciergeOrderHistoryContainerIntent(mContext, false), new ResultCallback() {
                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    if (resultCode == AppCompatActivity.RESULT_OK) {
                        boolean hasDataFromServer = data.getBooleanExtra(ConciergeOrderTrackingIntent.HAS_DATA_FROM_SERVER, false);
                        if (!hasDataFromServer) {
                            //Will hide tracking order button
                            mIsHasPending.set(false);
                        }
                    }
                }
            });
        }
    }

    public void onCheckSlotClicked(Context context) {
        if (context instanceof AppCompatActivity) {
            if (ApplicationHelper.isInVisitorMode(context)) {
                UserLoginRegisterBottomSheetDialog dialog = UserLoginRegisterBottomSheetDialog.newInstance();
                dialog.show(((AppCompatActivity) context).getSupportFragmentManager(), dialog.getTag());
            } else {
                context.startActivity(ConciergeDeliveryIntent.getNewViewOnlyInstance(context));
            }
        }
    }

    private void bindNotificationBadge() {
        UserBadge userBadge = UserHelper.getUserBadge(getContext());
        if (userBadge != null) {
            mOrderBadgeCount.set(userBadge.getConciergeOrderBadge().intValue());
        }
    }

    public void loadUserCurrentLocation() {
        if (mCurrentLocationViewModel.get() != null) {
            mCurrentLocationViewModel.get().setDefaultLocationForUser();
        }
    }

    public void onUserAddressSelected(ConciergeUserAddress selectedAddress) {
        if (mCurrentLocationViewModel.get() != null) {
            mCurrentLocationViewModel.get().saveAndUpdatedUserSelectedLocation(selectedAddress);
        }
    }

    public void onUserAddressUpdated(ConciergeUserAddress updatedAddress) {
        if (mCurrentLocationViewModel.get() != null) {
            mCurrentLocationViewModel.get().checkIfCurrentAddressIsUpdated(updatedAddress);
        }
    }

    public void onUserAddressDeleted(ConciergeUserAddress deletedAddress) {
        if (mCurrentLocationViewModel.get() != null) {
            mCurrentLocationViewModel.get().checkIfCurrentAddressIsDeleted(deletedAddress);
        }
    }

    public void getUserSelectedLocation() {
        //No need to get current user location here.
    }

    public void updateOrderBadgeCount(long newCount) {
        Timber.i("Updated badge count " + newCount);
        mOrderBadgeCount.set((int) newCount);
    }

    public void onShopStatusUpdated(boolean isShopClosed) {
        if (mCartViewModel != null) {
            mCartViewModel.updateCheckoutButtonStatus(isShopClosed);
        }
    }

    @Override
    public void onRetryClick() {
        initData();
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        loadData(true);
        getUserSelectedLocation();
        SendBroadCastHelper.verifyAndSendBroadCast(mContext, new Intent(REFRESH_NOTIFICATION_BADGE_ACTION));
    }

    public void loadData(boolean isRefresh) {
        if (!isRefresh) {
            showLoading();
        }
        mEnableRefresh.set(true);
        Observable<Response<List<ConciergeItemAdaptive>>> ob = mDataManager.getShopFeature(false);
        ResponseObserverHelper<Response<List<ConciergeItemAdaptive>>> helper = new ResponseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<ConciergeItemAdaptive>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                if (!isRefresh) {
                    mEnableRefresh.set(false);
                }
                if (ex.getCode() == HTTPResponse.NOT_FOUND && !isRefresh) {
                    //There is no shop yet on server.
                    showError(mContext.getString(R.string.error_no_data));
                } else {
                    mIsRefresh.set(false);
                    if (!isRefresh) {
                        showError(ex.getMessage());
                    }
                }
            }

            @Override
            public void onComplete(Response<List<ConciergeItemAdaptive>> result) {
                hideLoading();
                mIsRefresh.set(false);
                mIsHasPending.set(mDataManager.isHasPending());

                ConciergeShopAnnouncementResponse shopAnnouncementResponse = mDataManager.getConciergeShopAnnouncement();
                if (shopAnnouncementResponse != null) {
                    onShopStatusUpdated(shopAnnouncementResponse.isShopClosed());
                } else {
                    // Shop status is null for whatever reason, should we default to close or open?
                    // Probably close right? If there are error, user must not be able to order things.
                    //
                    // If user cannot order stuff because of this use case, they should, and perhaps
                    // will, contact support if the really want to order stuff
                    onShopStatusUpdated(true);
                }

                List<ConciergeItemAdaptive> resultList = result.body();
                if (resultList == null || resultList.isEmpty()) {
                    if (shopAnnouncementResponse != null && shopAnnouncementResponse.isThereAnnouncement()) {
                        if (resultList == null) {
                            resultList = new ArrayList<>();
                        }
                        resultList.add(0, shopAnnouncementResponse.getShopAnnouncement());
                        if (mListener != null) {
                            mListener.onLoadData(resultList, isRefresh, mDataManager.isCanLoadMore());
                        }
                    } else {
                        showError(mContext.getString(R.string.error_no_data));
                    }
                } else {
                    if (shopAnnouncementResponse != null && shopAnnouncementResponse.isThereAnnouncement()) {
                        resultList.add(0, shopAnnouncementResponse.getShopAnnouncement());
                    }
                    if (mListener != null) {
                        mListener.onLoadData(result.body(), isRefresh, mDataManager.isCanLoadMore());
                    }
                }
            }
        }));
    }

    public void makeSilentRequestOfShopAnnouncement() {
        Observable<Response<ConciergeShopAnnouncementResponse>> ob = mDataManager.getShopAnnouncementService();
        ResponseObserverHelper<Response<ConciergeShopAnnouncementResponse>> helper = new ResponseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeShopAnnouncementResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
            }

            @Override
            public void onComplete(Response<ConciergeShopAnnouncementResponse> result) {
                if (mListener != null) {
                    mListener.onMakeSilentRequestShopSettingFinished(result.body());
                }
            }
        }));
    }

    public void loadMoreData(OnCallbackListListener<List<ConciergeItemAdaptive>> listener) {
        mEnableRefresh.set(true);
        if (!isAlreadyLoadMore) {
            isAlreadyLoadMore = true;
        } else {
            return;
        }

        if (mDataManager.isCanLoadMore()) {
            Observable<Response<List<ConciergeItemAdaptive>>> ob = mDataManager.getShopFeature(true);
            ResponseObserverHelper<Response<List<ConciergeItemAdaptive>>> helper = new ResponseObserverHelper<>(getContext(), ob);
            addDisposable(helper.execute(new OnCallbackListener<Response<List<ConciergeItemAdaptive>>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    listener.onFail(ex);
                }

                @Override
                public void onComplete(Response<List<ConciergeItemAdaptive>> result) {
                    if (listener != null) {
                        listener.onComplete(result.body(), mDataManager.isCanLoadMore());
                    }
                }
            }));
        } else {
            if (listener != null) {
                listener.onComplete(new ArrayList<>(), false);
            }
        }
    }

    @Override
    public void onProductAddedFromDetail() {
        if (mListener != null) {
            mListener.notifyShouldShowCartBottomSheet();
        }
    }

    @Override
    public void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {
        if (mListener != null) {
            mListener.onAddFirstItemToCart(conciergeMenuItem);
        }
    }

    public interface ConciergeFragmentViewModelListener extends ConciergeSupportAddItemToCardViewModelListener {
        void onLoadData(List<ConciergeItemAdaptive> data, boolean isRefresh, boolean isCanLoadMoreTrending);

        void notifyShouldShowCartBottomSheet();

        void onMakeSilentRequestShopSettingFinished(ConciergeShopAnnouncementResponse shopSetting);

        void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem);
    }
}
