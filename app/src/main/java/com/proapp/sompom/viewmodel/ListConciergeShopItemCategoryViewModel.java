package com.proapp.sompom.viewmodel;

import androidx.databinding.ObservableField;

public class ListConciergeShopItemCategoryViewModel extends AbsBaseViewModel {

    private final ObservableField<String> mTitle = new ObservableField<>();

    public ListConciergeShopItemCategoryViewModel(String title) {
        mTitle.set(title);
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }
}
