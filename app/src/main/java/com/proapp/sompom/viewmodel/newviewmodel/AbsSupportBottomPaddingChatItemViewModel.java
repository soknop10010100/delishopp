package com.proapp.sompom.viewmodel.newviewmodel;

import android.view.View;

import androidx.databinding.ObservableInt;

import com.proapp.sompom.viewmodel.AbsBaseViewModel;

import timber.log.Timber;

public class AbsSupportBottomPaddingChatItemViewModel extends AbsBaseViewModel {

    protected int mPosition;
    private ObservableInt mBottomLineVisibility = new ObservableInt(View.GONE);

    public AbsSupportBottomPaddingChatItemViewModel(int position) {
        mPosition = position;
    }

    public ObservableInt getBottomLineVisibility() {
        return mBottomLineVisibility;
    }

    public int getPosition() {
        return mPosition;
    }

    public void updateBottomLineVisibility(boolean isLastItem) {
//        Timber.i("updateBottomLineVisibility: isLastItem: " + isLastItem);
        mBottomLineVisibility.set(isLastItem ? View.VISIBLE : View.GONE);
    }

    public void updatePosition(int position) {
//        Timber.i("updatePosition: " + position);
        if (position >= 0) {
            mPosition = position;
        }
    }
}
