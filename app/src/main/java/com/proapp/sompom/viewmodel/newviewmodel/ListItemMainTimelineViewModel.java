package com.proapp.sompom.viewmodel.newviewmodel;

import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ListItemProductMediaBinding;
import com.proapp.sompom.databinding.ListItemTimelineMediaBinding;
import com.proapp.sompom.helper.WallStreetIntentData;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.ShareAds;
import com.proapp.sompom.model.SharedProduct;
import com.proapp.sompom.model.SharedTimeline;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;
import com.proapp.sompom.utils.TimelinePostItemUtil;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;
import com.proapp.sompom.widget.lifestream.CollageView;
import com.proapp.sompom.widget.lifestream.CollageViewBinder;

import java.util.List;

import timber.log.Timber;

/**
 * Created by nuonveyo on 11/8/18.
 */

public class ListItemMainTimelineViewModel extends AbsBaseViewModel {

    public final ObservableField<ListItemTimelineHeaderViewModel> mTimelineHeaderShareViewModel = new ObservableField<>();
    public final ObservableField<ListItemTimelineHeaderViewModel> mTimelineHeaderViewModel = new ObservableField<>();
    public final ObservableField<ListItemTimelineViewModel> mTimelineFooterViewModel = new ObservableField<>();
    public final ObservableField<ListItemTimelineViewModel> mProductFooterViewModel = new ObservableField<>();
    public ObservableInt mCheckFooterVisibility = new ObservableInt();
    public final ObservableBoolean mIsShowHeader = new ObservableBoolean();
    public final ObservableField<Product> mProduct = new ObservableField<>();
    protected final int mPosition;
    protected final AbsBaseActivity mContext;
    protected final OnTimelineItemButtonClickListener mOnItemClickListener;
    final BindingViewHolder mBindingViewHolder;
    final Adaptive mAdaptive;
    private final WallStreetIntentData mMyIntentData;
    private final WallStreetDataManager mWallStreetDataManager;
    private ObservableBoolean mHasPostText = new ObservableBoolean();

    public void setCheckFooterVisibility(int checkFooterVisibility) {
        mCheckFooterVisibility.set(checkFooterVisibility);
    }

    public ObservableInt getCheckFooterVisibility() {
        return mCheckFooterVisibility;
    }

    public ListItemMainTimelineViewModel(AbsBaseActivity activity,
                                         BindingViewHolder bindingViewHolder,
                                         WallStreetDataManager dataManager,
                                         Adaptive adaptive,
                                         int position,
                                         OnTimelineItemButtonClickListener onItemClickListener,
                                         boolean checkLikeComment) {
        mContext = activity;
        mHasPostText.set(!TextUtils.isEmpty(adaptive.getTitle()));
        mBindingViewHolder = bindingViewHolder;
        mWallStreetDataManager = dataManager;
        mAdaptive = adaptive;
        mPosition = position;
        mOnItemClickListener = onItemClickListener;

        mMyIntentData = new WallStreetIntentData(activity, mOnItemClickListener);

        if (!checkLikeComment || adaptive.isWelcomePostType()) {
            setCheckFooterVisibility(View.GONE);
        } else {
            setCheckFooterVisibility(View.VISIBLE);
        }

        if (mAdaptive instanceof SharedTimeline
                || mAdaptive instanceof SharedProduct
                || mAdaptive instanceof ShareAds) {
            mIsShowHeader.set(true);

            String title = activity.getString(R.string.home_shared_post_title);
            ListItemTimelineHeaderViewModel headerViewModel = new ListItemTimelineHeaderViewModel(mContext,
                    mAdaptive,
                    mPosition,
                    mOnItemClickListener);
            headerViewModel.setSharedTextTitle(title);
            mTimelineHeaderShareViewModel.set(headerViewModel);

            if (mAdaptive instanceof SharedProduct) {
                mProduct.set(((SharedProduct) mAdaptive).getProduct());
                initListProductFooter(mProduct.get());
            }
            initListItemTimelineFooter();
        } else if (mAdaptive instanceof Product) {
            mProduct.set((Product) mAdaptive);
            initListProductFooter(mProduct.get());
            //there is no footer for product
        } else {
            initListItemTimelineFooter();
        }
        initListItemTimelineHeader();
    }

    public ObservableBoolean getHasPostText() {
        return mHasPostText;
    }

    public void initListItemTimelineHeader() {
        Adaptive adaptive = TimelinePostItemUtil.getAdaptive(mAdaptive);
        ListItemTimelineHeaderViewModel viewModel = new ListItemTimelineHeaderViewModel(mContext,
                adaptive,
                mPosition,
                mOnItemClickListener);
        mTimelineHeaderViewModel.set(viewModel);

        CollageView collageView = getCollageView();
        if (collageView == null) {
            return;
        }

        final List<Media> medias = getMedia();
        if (medias == null) {
            return;
        }

        //Bind Timeline with media layout
        collageView.bindView(new CollageViewBinder(medias, mAdaptive, mOnItemClickListener, mMyIntentData, mBindingViewHolder));
        collageView.setBadge(medias.size());
    }

    private List<Media> getMedia() {
        final List<Media> medias;
        if (mAdaptive instanceof WallStreetAdaptive) {
            medias = ((WallStreetAdaptive) mAdaptive).getMedia();
        } else {
            medias = null;
        }
        return medias;
    }

    private void onVideoFullScreenClicked(Media media, int position, long time) {
        Timber.i("onVideoFullScreenClicked");
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onVideoFullScreenClicked(media, position, time);
        }
    }

    private CollageView getCollageView() {
        CollageView collageView = null;
        if (mBindingViewHolder.getBinding() instanceof ListItemTimelineMediaBinding) {
            collageView = ((ListItemTimelineMediaBinding) mBindingViewHolder.getBinding()).mediaLayouts;
        } else if (mBindingViewHolder.getBinding() instanceof ListItemProductMediaBinding) {
            collageView = ((ListItemProductMediaBinding) mBindingViewHolder.getBinding()).mediaLayouts;
        }
        return collageView;
    }

    private void initListItemTimelineFooter() {
        ListItemTimelineViewModel viewModel = new ListItemTimelineViewModel(mContext,
                mAdaptive,
                mWallStreetDataManager,
                mPosition,
                mOnItemClickListener);
        mTimelineFooterViewModel.set(viewModel);
    }

    private void initListProductFooter(Product product) {
        ListItemTimelineViewModel viewModel = new ListItemTimelineViewModel(mContext,
                product,
                mWallStreetDataManager,
                mPosition,
                mOnItemClickListener);
        mProductFooterViewModel.set(viewModel);
    }

    @BindingAdapter("enableMarginTop")
    public static void enableMarginTop(CollageView collageView, boolean shouldMarginTop) {
        if (collageView.getParent() instanceof LinearLayout) {
            if (shouldMarginTop) {
                ((LinearLayout.LayoutParams) collageView.getLayoutParams()).setMargins(0,
                        collageView.getResources().getDimensionPixelSize(R.dimen.medium_padding),
                        0,
                        0);
            } else {
                ((LinearLayout.LayoutParams) collageView.getLayoutParams()).setMargins(0,
                        0,
                        0,
                        0);
            }
        }
    }
}
