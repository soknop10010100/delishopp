package com.proapp.sompom.viewmodel.preview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;

import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.newui.dialog.BigPlayerFragment;
import com.proapp.sompom.widget.lifestream.MediaLayout;
import com.sompom.videomanager.helper.ExoPlayerBuilder;

/**
 * Created by Chhom Veasna on 05/11/2020.
 */
public class ItemVideoLinkPreviewViewModel extends AbsItemLinkPreviewViewModel {

    private ObservableField<Media> mMedia = new ObservableField<>();

    public ItemVideoLinkPreviewViewModel(LinkPreviewModel linkPreviewModel, PreviewContext previewContext) {
        super(linkPreviewModel, previewContext);
        setPreviewMediaUrl(linkPreviewModel.getLink());
        bindMediaFromUrl();
    }

    public ObservableField<Media> getMedia() {
        return mMedia;
    }

    private void bindMediaFromUrl() {
        Media media = new Media();
        media.setType(MediaType.VIDEO);
        media.setUrl(getPreviewMediaUrl().get());
        media.setThumbnail(getPreviewMediaUrl().get());
        mMedia.set(media);
    }

    public final ExoPlayerBuilder.OnPlayerListener getOnPlayerListener(MediaLayout videoLayout) {
        if (videoLayout != null && mListener != null) {
            videoLayout.setMediaLayoutType(MediaLayout.MediaLayoutType.LINK_PREVIEW);
            mListener.onBindVideoLayout(videoLayout);
        }

        return isPlaying -> updateCrossVisibility(!isPlaying);
    }

    @BindingAdapter({"media", "onPlayerListener"})
    public static void bindVideo(MediaLayout mediaLayout,
                                 Media media,
                                 ExoPlayerBuilder.OnPlayerListener onPlayerListener) {
        if (media != null) {
            mediaLayout.setOnFullScreenClickListener(v -> {
                if (mediaLayout.getContext() instanceof AppCompatActivity) {
                    mediaLayout.onMediaPause();
                    media.setTime(mediaLayout.getTime());
                    BigPlayerFragment playerFragment =
                            BigPlayerFragment.newInstance(0,
                                    media,
                                    false);
                    playerFragment.setListener((position, isAutoPlay, media1) -> mediaLayout.onMediaPlay(true));
                    playerFragment.show(((AppCompatActivity) mediaLayout.getContext()).getSupportFragmentManager(),
                            BigPlayerFragment.TAG);
                }
            });
            mediaLayout.setOnPlayerListener(onPlayerListener);
            mediaLayout.setMedia(media);
        }
    }
}
