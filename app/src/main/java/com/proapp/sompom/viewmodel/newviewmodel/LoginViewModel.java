package com.proapp.sompom.viewmodel.newviewmodel;

import android.text.TextUtils;

import com.proapp.sompom.model.request.LoginBody;
import com.proapp.sompom.services.datamanager.LoginDataManager;
import com.proapp.sompom.utils.SpecialTextRenderUtils;
import com.proapp.sompom.viewmodel.AbsLoginViewModel;

import androidx.databinding.ObservableField;

/**
 * Created by He Rotha on 6/4/18.
 */
public class LoginViewModel extends AbsLoginViewModel<LoginDataManager, AbsLoginViewModel.AbsLoginViewModelListener> {

    public final ObservableField<String> mEmail = new ObservableField<>();
    public final ObservableField<String> mPassword = new ObservableField<>();

    public LoginViewModel(LoginDataManager loginDataManager, AbsLoginViewModelListener absLoginViewModelListener) {
        super(loginDataManager.getContext(), loginDataManager, absLoginViewModelListener);
    }

    @Override
    protected boolean shouldEnableActionButton() {
        return !TextUtils.isEmpty(mEmail.get())
                && !TextUtils.isEmpty(mPassword.get())
                && SpecialTextRenderUtils.isEmailAddress(mEmail.get());
    }

    @Override
    protected void onContinueButtonClicked() {
        requestLogin(mDataManager.getLoginViaEmail(getLoginBody()), false);
    }

    private LoginBody getLoginBody() {
        String email = mEmail.get();
        if (email != null) {
            email = email.trim();
        }
        return new LoginBody(email, mPassword.get());
    }
}
