package com.proapp.sompom.viewmodel.newviewmodel;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.listener.OnChatItemListener;
import com.proapp.sompom.model.SelectedChat;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.ChatUtility;
import com.proapp.sompom.utils.DateTimeUtils;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.SpannableUtil;
import com.proapp.sompom.viewmodel.ChatMediaViewModel;

import timber.log.Timber;

public class CallEventChatViewModel extends ChatMediaViewModel {

    private ObservableBoolean mCurrentUserIsCaller = new ObservableBoolean();
    private ObservableField<String> mDescription = new ObservableField<>();
    private ObservableField<String> mCallDate = new ObservableField<>();
    private ObservableField<String> mCallActionLabel = new ObservableField<>();
    private ObservableBoolean mIsMissedCall = new ObservableBoolean();
    private ObservableField<Drawable> mCallIcon = new ObservableField<>();
    private User mDisplayUser;
    private AbsCallService.CallType mCallType = AbsCallService.CallType.UNKNOWN;

    public CallEventChatViewModel(Activity context,
                                  User currentUser,
                                  User recipient,
                                  Conversation conversation,
                                  int position,
                                  Chat chat,
                                  SelectedChat selectedChat,
                                  String searchKeyWord,
                                  ChatUtility.GroupMessage drawable,
                                  OnChatItemListener onItemClickListener) {
        super(context, conversation, chat, drawable, position, selectedChat, searchKeyWord, onItemClickListener);
        setCurrentUser(currentUser);
        setRecipient(recipient);
        bindData();
    }

    public ObservableField<String> getCallActionLabel() {
        return mCallActionLabel;
    }

    private void bindData() {
        Chat.ChatType type = Chat.ChatType.from(mChat.getChatType());
        boolean currentUserIsCaller = isCurrentUserIsCaller();
        mCurrentUserIsCaller.set(currentUserIsCaller);

        String description = "";
        if (!mConversation.isGroup()) {
            //One to one call
            mCallActionLabel.set(mCurrentUserIsCaller.get() ? mContext.getString(R.string.chat_message_call_again) :
                    mContext.getString(R.string.chat_message_call_back));
            if (type == Chat.ChatType.MISSED_CALL) {
                mCallType = AbsCallService.CallType.VOICE;
                mDisplayUser = getDisplayInChatUser(false);
                //Bind description
                description = currentUserIsCaller ? mContext.getString(R.string.chat_message_missed_your_call_message) :
                        mContext.getString(R.string.chat_message_you_missed_a_call_message);
                mIsMissedCall.set(true);
                mCallIcon.set(currentUserIsCaller ? mContext.getDrawable(R.drawable.ic_out_call) :
                        mContext.getDrawable(R.drawable.ic_incomming_call));
            } else if (type == Chat.ChatType.CALLED) {
                mCallType = AbsCallService.CallType.VOICE;
                //Bind user
                mDisplayUser = getDisplayInChatUser(false);
                //Bind description
                description = currentUserIsCaller ? mContext.getString(R.string.chat_message_you_called) :
                        mContext.getString(R.string.chat_message_called_you);
                mCallIcon.set(currentUserIsCaller ? mContext.getDrawable(R.drawable.ic_out_call) :
                        mContext.getDrawable(R.drawable.ic_incomming_call));
                mIsMissedCall.set(false);
            } else if (type == Chat.ChatType.MISSED_VIDEO_CALL) {
                mCallType = AbsCallService.CallType.VIDEO;
                mDisplayUser = getDisplayInChatUser(false);
                //Bind description
                description = currentUserIsCaller ? mContext.getString(R.string.chat_message_missed_your_video_call_message) :
                        mContext.getString(R.string.chat_message_you_missed_a_video_call_message);
                mIsMissedCall.set(true);
                mCallIcon.set(mContext.getDrawable(R.drawable.ic_small_video_camera));
            } else if (type == Chat.ChatType.VIDEO_CALLED) {
                mCallType = AbsCallService.CallType.VIDEO;
                //Bind user
                mDisplayUser = getDisplayInChatUser(false);
                //Bind description
                description = currentUserIsCaller ? mContext.getString(R.string.chat_message_you_video_called) :
                        mContext.getString(R.string.chat_message_video_called_you);
                mIsMissedCall.set(false);
                mCallIcon.set(mContext.getDrawable(R.drawable.ic_small_video_camera));
            }
        } else {
            //Group
            if (type == Chat.ChatType.START_GROUP_CALL) {
                mCallActionLabel.set(mContext.getString(R.string.chat_info_button_join));
                mCallType = AbsCallService.CallType.VOICE;
                //Bind user
                mDisplayUser = getUserFromParticipant(mChat.getSenderId());
                //Bind description
                if (mDisplayUser != null) {
                    String prefix = mDisplayUser.getFirstName();
                    if (isCurrentUserIsCaller()) {
                        description = mContext.getString(R.string.chat_message_you_start_group_audio);
                    } else {
                        description = prefix + " " + mContext.getString(R.string.chat_message_start_group_audio);
                    }
                }
                mIsMissedCall.set(false);
                mCallIcon.set(currentUserIsCaller ? mContext.getDrawable(R.drawable.ic_out_call) :
                        mContext.getDrawable(R.drawable.ic_incomming_call));
            } else if (type == Chat.ChatType.START_GROUP_VIDEO_CALL) {
                mCallActionLabel.set(mContext.getString(R.string.chat_info_button_join));
                mCallType = AbsCallService.CallType.VIDEO;
                //Bind user
                mDisplayUser = getUserFromParticipant(mChat.getSenderId());
                //Bind description
                if (mDisplayUser != null) {
                    String prefix = mDisplayUser.getFirstName();
                    if (isCurrentUserIsCaller()) {
                        description = mContext.getString(R.string.chat_message_you_start_group_video);
                    } else {
                        description = prefix + " " + mContext.getString(R.string.chat_message_start_group_video);
                    }
                }

                mIsMissedCall.set(false);
                mCallIcon.set(mContext.getDrawable(R.drawable.ic_small_video_camera));
            } else if (type == Chat.ChatType.END_GROUP_CALL) {
                mCallActionLabel.set(mCurrentUserIsCaller.get() ? mContext.getString(R.string.chat_message_call_again) :
                        mContext.getString(R.string.chat_message_call_back));
                mCallType = AbsCallService.CallType.VOICE;
                //Bind user
                mDisplayUser = getUserFromParticipant(mChat.getSenderId());
                //Bind description
                description = mContext.getString(R.string.chat_message_audio_call_end);
                mIsMissedCall.set(false);
                mCallIcon.set(currentUserIsCaller ? mContext.getDrawable(R.drawable.ic_out_call) :
                        mContext.getDrawable(R.drawable.ic_incomming_call));
            } else if (type == Chat.ChatType.END_GROUP_VIDEO_CALL) {
                mCallActionLabel.set(mCurrentUserIsCaller.get() ? mContext.getString(R.string.chat_message_call_again) :
                        mContext.getString(R.string.chat_message_call_back));
                mCallType = AbsCallService.CallType.VIDEO;
                //Bind user
                mDisplayUser = getUserFromParticipant(mChat.getSenderId());
                //Bind description
                description = mContext.getString(R.string.chat_message_video_call_end);
                mIsMissedCall.set(false);
                mCallIcon.set(mContext.getDrawable(R.drawable.ic_small_video_camera));
            } else if (type == Chat.ChatType.MISSED_CALL) {
                mCallActionLabel.set(mCurrentUserIsCaller.get() ? mContext.getString(R.string.chat_message_call_again) :
                        mContext.getString(R.string.chat_message_call_back));
                mCallType = AbsCallService.CallType.VOICE;
                mDisplayUser = getDisplayInChatUser(false);
                //Bind description
                description = currentUserIsCaller ? mContext.getString(R.string.chat_message_missed_your_call_message) :
                        mContext.getString(R.string.chat_message_you_missed_a_call_message);
                mIsMissedCall.set(true);
                mCallIcon.set(currentUserIsCaller ? mContext.getDrawable(R.drawable.ic_out_call) :
                        mContext.getDrawable(R.drawable.ic_incomming_call));
            }
        }

        mDescription.set(SpannableUtil.capitaliseOnlyFirstLetter(description));

        //Bind chat date and duration
        if (mChat.getDate() != null) {
            String chatDate = DateTimeUtils.getTimestampForMissedCallMessage(mContext,
                    mChat.getDate().getTime());
            if (CallHelper.isCalledHistoryMessageType(mChat) &&
                    mChat.getCallDuration() != null && mChat.getCallDuration() > 0) {
                //There is call duration and format of date will be: 20 mins 10 secs, Thu at 18:19
                String durationDisplay = CallHelper.formatCallDurationDisplay(mContext, mChat.getCallDuration());
                if (!TextUtils.isEmpty(durationDisplay)) {
                    mCallDate.set(durationDisplay + ", " + chatDate);
                } else {
                    mCallDate.set(chatDate);
                }
            } else {
                mCallDate.set(chatDate);
            }
        }
    }

    public ObservableField<String> getDescription() {
        return mDescription;
    }

    public User getDisplayUser() {
        return mDisplayUser;
    }

    public ObservableBoolean getCurrentUserIsCaller() {
        return mCurrentUserIsCaller;
    }

    public ObservableField<String> getCallDate() {
        return mCallDate;
    }

    private boolean isCurrentUserIsCaller() {
        String userId = SharedPrefUtils.getUserId(mContext);
        return userId.matches(mChat.getSenderId());
    }

    private User getDisplayInChatUser(boolean isGetCurrentUser) {
        if (isGetCurrentUser) {
            return mCurrentUser;
        } else {
            return mRecipient;
        }
    }

    public final void onCallActionClick() {
        if (mOnChatItemListener != null) {
            Timber.i("onCallActionClick");
            mOnChatItemListener.onCallAgainOrBack(mChat, mCallType);
        }
    }

    public ObservableBoolean getIsMissedCall() {
        return mIsMissedCall;
    }

    public void setIsMissedCall(boolean isMissedCall) {
        mIsMissedCall.set(isMissedCall);
    }

    public ObservableField<Drawable> getCallIcon() {
        return mCallIcon;
    }

    public void setCallIcon(Drawable callIcon) {
        mCallIcon.set(callIcon);
    }
}
