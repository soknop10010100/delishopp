package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.model.emun.SegmentedControlItem;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class ListItemMessageRelatedProduct extends AbsBaseViewModel {
    public final ObservableBoolean mIsShowProduct = new ObservableBoolean();
    public final Product mProduct;

    ListItemMessageRelatedProduct(Conversation conversation, SegmentedControlItem item) {
        Product product = null;
        if (item != SegmentedControlItem.Message) {
            product = conversation.getProduct();
            if (item == SegmentedControlItem.Buying || item == SegmentedControlItem.Selling) {
                if (product == null && conversation.getLastMessage() != null) {
                    product = conversation.getLastMessage().getProduct();
                }
            }
        }
        mIsShowProduct.set(product != null);
        mProduct = product;
    }

    public String getName() {
        if (mIsShowProduct.get()) {
            return mProduct.getName();
        }
        return null;
    }
}
