package com.proapp.sompom.viewmodel.binding;

import androidx.databinding.BindingAdapter;

import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.emun.ActionButtonItem;
import com.proapp.sompom.widget.actionbutton.ActionButtonContainer;

/**
 * Created by nuonveyo on 8/1/18.
 */

public final class ActionButtonContainerBindingUtil {
    private ActionButtonContainerBindingUtil() {
    }

    @BindingAdapter({"addActionButtonItem", "onActionButtonItemClick"})
    public static void addItem(ActionButtonContainer container,
                               ActionButtonItem[] items,
                               OnItemClickListener<ActionButtonItem> listener) {
        container.removeAllViews();
        for (ActionButtonItem item : items) {
            container.addView(item);
        }
        container.setOnItemClickListener(listener);
    }
}
