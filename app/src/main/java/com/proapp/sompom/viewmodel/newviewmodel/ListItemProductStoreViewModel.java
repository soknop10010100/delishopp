package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import android.text.TextUtils;
import android.view.View;

import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.helper.IntentData;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.listener.OnProductItemClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.emun.Status;
import com.proapp.sompom.model.result.ContentStat;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.utils.FormatNumber;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;
import com.proapp.sompom.newui.AbsBaseActivity;

/**
 * Created by nuonveyo on 6/5/18.
 */

public class ListItemProductStoreViewModel extends AbsBaseViewModel {
    public final ObservableBoolean mIsAddMarginBottom = new ObservableBoolean();
    public final ObservableInt mProductStatusId = new ObservableInt();
    public final ObservableBoolean mIsShowUserImageProfile = new ObservableBoolean();
    public final ObservableField<String> mLikeCount = new ObservableField<>();
    public final ObservableBoolean mIsEditable = new ObservableBoolean();
    public final ObservableBoolean mIsSetLikeBackground = new ObservableBoolean();
    public final ObservableBoolean mIsLike = new ObservableBoolean();
    private final OnProductItemClickListener mListener;
    private final String mUserId;
    private final ContentStat mContentStat;
    public Product mProduct;
    private OnItemClickListener<Product> mOnItemClickListener;
    private AbsBaseActivity mActivity;

    public ListItemProductStoreViewModel(AbsBaseActivity activity,
                                         Product product,
                                         String userId,
                                         boolean isMe,
                                         boolean isEditable,
                                         boolean isShowImageProfile,
                                         int productStatusId,
                                         OnProductItemClickListener onProductItemClickListener) {
        mActivity = activity;
        mProduct = product;
        mContentStat = mProduct.getContentStat();
        mUserId = userId;
        mIsShowUserImageProfile.set(isShowImageProfile);
        mProductStatusId.set(productStatusId);
        if (isEditable) {
            mIsEditable.set(isMe);
        } else {
            mIsEditable.set(false);
        }
        mListener = onProductItemClickListener;

        mIsLike.set(mProduct.isLike());
        mIsSetLikeBackground.set(mContentStat.getTotalLikes() > 0);
        if (mContentStat.getTotalLikes() > 1) {
            mLikeCount.set(FormatNumber.format(mContentStat.getTotalLikes()));
        }
    }

    public void setOnItemClickListener(OnItemClickListener<Product> onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public void onProductItemClick() {
        mProduct.startActivityForResult(new IntentData() {
            @Override
            public AbsBaseActivity getActivity() {
                return mActivity;
            }

            @Override
            public void onDataResultCallBack(Adaptive adaptive, boolean isRemove) {
                if (adaptive instanceof Product) {
                    mProduct = (Product) adaptive;
                    mIsLike.set(((Product) adaptive).isLike());
                }
            }
        });
    }

    public void onLikeButtonClick(View view, Context context) {
        if (mProductStatusId.get() == Status.ON_SALE.getStatusProduct()) {
            AnimationHelper.animateBounce(view, null);
            if (context instanceof AbsBaseActivity) {
                ((AbsBaseActivity) context).startLoginWizardActivity(new LoginActivityResultCallback() {
                    @Override
                    public void onActivityResultSuccess(boolean isAlreadyLogin) {
                        if (!TextUtils.equals(mUserId, SharedPrefUtils.getUserId(context))) {
                            long likeCount = mContentStat.getTotalLikes();
                            if (mIsLike.get()) {
                                mIsLike.set(false);
                                likeCount = likeCount - 1;
                            } else {
                                mIsLike.set(true);
                                likeCount = likeCount + 1;
                            }
                            mProduct.setLike(mIsLike.get());
                            mContentStat.setTotalLikes(likeCount);
                            mProduct.setContentStat(mContentStat);

                            mIsSetLikeBackground.set(mContentStat.getTotalLikes() > 0);
                            if (mContentStat.getTotalLikes() > 1) {
                                mLikeCount.set(FormatNumber.format(mContentStat.getTotalLikes()));
                            } else {
                                mLikeCount.set(null);
                            }
                            if (mOnItemClickListener != null) {
                                mOnItemClickListener.onClick(mProduct);
                            }
                        }
                    }
                });
            }
        }
    }

    public void onProductItemEdit() {
        if (mListener != null) {
            mListener.onProductItemEdit(mProduct);
        }
    }
}
