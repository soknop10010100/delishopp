package com.proapp.sompom.viewmodel;

import android.text.TextWatcher;

import androidx.databinding.ObservableField;

import com.proapp.sompom.helper.MyTextWatcher;

/**
 * Created by nuonveyo on 5/7/18.
 */

public class ListItemEditMediaFileViewModel extends AbsBaseViewModel {

    public ObservableField<String> mContent = new ObservableField<>();
    private ViewModelListener mListener;

    public ListItemEditMediaFileViewModel(String title, ViewModelListener listener) {
        mContent.set(title);
        mListener = listener;
    }

    public TextWatcher onTextChanged() {
        return new MyTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s) {
                if (mListener != null) {
                    mListener.onTextChanged(s.toString());
                }
            }
        };
    }

    public final void onCrossButtonClick() {
        if (mListener != null) {
            mListener.onCrossButtonClicked();
        }
    }

    public interface ViewModelListener {

        void onTextChanged(String text);

        void onCrossButtonClicked();
    }
}
