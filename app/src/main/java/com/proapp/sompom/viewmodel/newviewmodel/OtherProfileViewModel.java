package com.proapp.sompom.viewmodel.newviewmodel;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.NetworkBroadcastReceiver;
import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.listener.BaseAppSettingUI;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.result.AppSetting;
import com.proapp.sompom.model.result.Theme;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.OtherUserProfileDataManager;
import com.proapp.sompom.utils.NetworkStateUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

public class OtherProfileViewModel extends AbsLoadingViewModel implements BaseAppSettingUI {

    private final ObservableField<String> mCover = new ObservableField<>();
    private final ObservableField<String> mCompanyLogo = new ObservableField<>();
    private final ObservableInt mCallVisibility = new ObservableInt(View.GONE);
    private final ObservableInt mVideoCallVisibility = new ObservableInt(View.GONE);
    private final OtherUserProfileDataManager mDataManager;
    private final String mUserId;
    private final OnCallback mOnCallback;
    @Bindable
    public User mUser = new User();
    private boolean mIsReadUserDataReady;
    private NetworkBroadcastReceiver.NetworkListener mNetworkListener;
    private ObservableInt mActionButtonVisibility = new ObservableInt(View.INVISIBLE);
    private ObservableBoolean mShouldCheckLoadingViewHeight = new ObservableBoolean(true);

    public OtherProfileViewModel(OtherUserProfileDataManager dataManager,
                                 String userId,
                                 OnCallback onCallback) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mUserId = userId;
        mOnCallback = onCallback;
        mActionButtonVisibility.set(UserHelper.isInUserContact(getContext(), userId) ? View.VISIBLE : View.INVISIBLE);

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkListener = networkState -> {
                if (!mIsReadUserDataReady) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        requestUserProfile();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkListener);
        }
        setCallActionVisibility();
    }

    public ObservableBoolean getShouldCheckLoadingViewHeight() {
        return mShouldCheckLoadingViewHeight;
    }

    private void setCallActionVisibility() {
        UserHelper.checkUserCallFeatureSetting(getContext(), new UserHelper.UserHelperListener() {
            @Override
            protected void onCheckUserCallFeatureFinished(boolean isCallEnable, boolean isVideoCallEnable) {
                mCallVisibility.set(isCallEnable ? View.VISIBLE : View.GONE);
                mVideoCallVisibility.set(isVideoCallEnable ? View.VISIBLE : View.GONE);
            }
        });
    }

    public ObservableInt getVideoCallVisibility() {
        return mVideoCallVisibility;
    }

    public ObservableInt getCallVisibility() {
        return mCallVisibility;
    }

    public ObservableInt getActionButtonVisibility() {
        return mActionButtonVisibility;
    }

    public ObservableField<String> getCover() {
        return mCover;
    }

    public ObservableField<String> getCompanyLogo() {
        return mCompanyLogo;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkListener);
        }
    }

    private void requestUserProfile() {
        showLoading();
        Observable<Response<User>> call = mDataManager.getUserProfile(mUserId);
        ResponseObserverHelper<Response<User>> observerHelper = new ResponseObserverHelper<>(getContext(), call);
        observerHelper.execute(new OnCallbackListener<Response<User>>() {
            @Override
            public void onComplete(Response<User> data) {
                Timber.i("onComplete");
                mUser = data.body();
                updateData();
                mShouldCheckLoadingViewHeight.set(true);
                mShouldCheckLoadingViewHeight.notifyChange();
            }

            private void updateData() {
                hideLoading();
                mIsReadUserDataReady = true;
                notifyPropertyChanged(BR.user);
                bindUserCover();
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e("onFail: " + ex);
                if (ex.getCode() == 404) {
                    showError(getContext().getString(R.string.error_user_404));
                } else {
                    showError(ex.toString());
                }
            }
        });
    }

    private void bindUserCover() {
        AppSetting appSetting = SharedPrefUtils.getAppSetting(getContext());
        //Bind cover
        mCover.set(appSetting.getBackgroundCover());
        //Bind logo
        Theme theme = UserHelper.getSelectedThemeFromAppSetting(getContext());
        if (theme != null) {
            mCompanyLogo.set(theme.getCompanyLogo());
        }
    }

    @Override
    public void onRetryClick() {
        hideLoading();
        requestUserProfile();
    }

    public final View.OnClickListener onVideoCallClick() {
        return view -> {
            if (!NetworkStateUtil.isNetworkAvailable(getContext())) {
                showSnackBar(R.string.call_toast_no_internet, true);
                return;
            }

            if (CallHelper.isDeviceInPhoneCallProcess(getContext())) {
                showSnackBar(R.string.call_toast_already_in_call_description, true);
                return;
            }

            UserHelper.checkUserCallFeatureSetting(getContext(), new UserHelper.UserHelperListener() {
                @Override
                protected void onCheckUserCallFeatureFinished(boolean isCallEnable, boolean isVideoCallEnable) {
                    if (isVideoCallEnable) {
                        if (mOnCallback != null) {
                            mOnCallback.onVideoCallClicked(mUser);
                        }
                    } else {
                        showSnackBar(R.string.call_toast_no_call_feature, true);
                    }
                }
            });
        };
    }

    public final View.OnClickListener onCallClick() {
        return view -> {
            if (!NetworkStateUtil.isNetworkAvailable(getContext())) {
                showSnackBar(R.string.call_toast_no_internet, true);
                return;
            }

            if (CallHelper.isDeviceInPhoneCallProcess(getContext())) {
                showSnackBar(R.string.call_toast_already_in_call_description, true);
                return;
            }

            UserHelper.checkUserCallFeatureSetting(getContext(), new UserHelper.UserHelperListener() {
                @Override
                protected void onCheckUserCallFeatureFinished(boolean isCallEnable, boolean isVideoCallEnable) {
                    if (isCallEnable) {
                        if (mOnCallback != null) {
                            mOnCallback.onCallClicked(mUser);
                        }
                    } else {
                        showSnackBar(R.string.call_toast_no_call_feature, true);
                    }
                }
            });
        };
    }

    public final void onMessageClick() {
        if (mOnCallback != null) {
            mOnCallback.onMessageClicked(mUser);
        }
    }

    public final void onCloseClick() {
        if (mOnCallback != null) {
            mOnCallback.onCloseClicked();
        }
    }

    public final void onRootClick() {
        if (mOnCallback != null) {
            mOnCallback.onRootClicked();
        }
    }

    public interface OnCallback {
        void onCallClicked(User user);

        void onVideoCallClicked(User user);

        void onMessageClicked(User user);

        void onCloseClicked();

        void onRootClicked();
    }
}
