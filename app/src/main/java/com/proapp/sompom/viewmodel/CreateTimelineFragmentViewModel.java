package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.util.Linkify;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.inputmethod.InputContentInfoCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.desmond.squarecamera.intent.CameraIntent;
import com.example.usermentionable.model.UserMentionable;
import com.example.usermentionable.utils.RenderTextAsMentionable;
import com.example.usermentionable.widget.KeyboardInputEditor;
import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.upload.LifeStreamUploadService;
import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.helper.MetaPreviewHelper;
import com.proapp.sompom.helper.PostTextGenerateStyleHelper;
import com.proapp.sompom.helper.UrlDetectionWatcher;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.intent.CreateTimelineIntent;
import com.proapp.sompom.intent.EditMediaFileIntent;
import com.proapp.sompom.intent.SelectAddressIntent;
import com.proapp.sompom.intent.SelectAddressIntentResult;
import com.proapp.sompom.intent.newintent.ChoosePrivacyIntent;
import com.proapp.sompom.intent.newintent.MyCameraIntent;
import com.proapp.sompom.intent.newintent.MyCameraResultIntent;
import com.proapp.sompom.listener.AbsLinkPreviewCallback;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.ItemPrivacy;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.CreateWallStreetScreenType;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.emun.PublishItem;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.CreateWallStreetDataManager;
import com.proapp.sompom.utils.GenerateLinkPreviewUtil;
import com.proapp.sompom.utils.MediaUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.SpannableUtil;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemTimelineHeaderViewModel;
import com.proapp.sompom.widget.PlacePreviewLayout;
import com.sompom.baseactivity.ResultCallback;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 5/4/18.
 */

public class CreateTimelineFragmentViewModel extends ToolbarViewModel implements KeyboardInputEditor.KeyBoardInputCallbackListener {

    public final ObservableField<Spannable> mUserFullName = new ObservableField<>();
    public final ObservableBoolean mIsShowMediaLayout = new ObservableBoolean();
    public final ObservableBoolean mIsShareWallStreet = new ObservableBoolean();
    public final ObservableField<ListItemTimelineHeaderViewModel> mShareWallStreetItemViewModel = new ObservableField<>();
    public final ObservableField<WallStreetAdaptive> mWallStreetAdaptive = new ObservableField<>();
    public final ObservableField<CreateWallStreetScreenType> mScreenType = new ObservableField<>();
    public final ObservableField<String> mActionErrorMessage = new ObservableField<>();
    public final ObservableField<PublishItem> mPrivacy = new ObservableField<>();

    public final User mUser;
    private final List<Media> mMediaList = new ArrayList<>();
    private final AbsBaseActivity mContext;
    private final AbsLinkPreviewCallback mAbsLinkPreviewCallback;
    private final CreateWallStreetDataManager mStoreDataManager;

    private SearchAddressResult mSearchAddressResult = new SearchAddressResult();
    private Editable mEditable;

    private boolean mIsShowedLinkPreview;
    private boolean mIsMediaFromKeyboard;

    private CreateTimelineFragmentListener mListener;
    private boolean mIsFromOtherAppContent;
    private String mShareUrlFromOtherApp;
    private PublishItem mSelectedPrivacy;
    private LinkPreviewModel mLinkPreviewModel;
    private PostTextGenerateStyleHelper mPostTextGenerateStyleHelper;
    private boolean mShouldRenderPreviousPlacePreview; //For edit post only
    private boolean mShouldShowMapPreview;
    private String mPostId; //Use for edit post.
    private WallStreetAdaptive mPost;
    private boolean mIgnorePreviewUrlForEditPost;

    public CreateTimelineFragmentViewModel(AbsBaseActivity activity,
                                           CreateWallStreetDataManager dataManager,
                                           WallStreetAdaptive wallStreetAdaptive,
                                           CreateWallStreetScreenType screenType,
                                           boolean isFromOtherAppContent,
                                           AbsLinkPreviewCallback absLinkPreviewCallback,
                                           PostTextGenerateStyleHelper postTextGenerateStyleHelper,
                                           CreateTimelineFragmentListener listener) {
        super(dataManager.getContext());
        Timber.i("Screen type: " + screenType + "isFromOtherAppContent: " + isFromOtherAppContent + ", Post: " + new Gson().toJson(wallStreetAdaptive));
        mPostTextGenerateStyleHelper = postTextGenerateStyleHelper;
        mListener = listener;
        mStoreDataManager = dataManager;
        mContext = activity;
        backupOriginalPost(wallStreetAdaptive);
        if (wallStreetAdaptive != null) {
            mWallStreetAdaptive.set(wallStreetAdaptive);
            mSelectedPrivacy = wallStreetAdaptive.getPublish();
        } else {
            mSelectedPrivacy = PublishItem.getItem(UserHelper.getUserLastPostPrivacy(mContext));
        }
        mScreenType.set(screenType);
        mAbsLinkPreviewCallback = absLinkPreviewCallback;
        mUser = SharedPrefUtils.getUser(mContext);
        mIsFromOtherAppContent = isFromOtherAppContent;

        String actionButton = mContext.getString(R.string.create_wall_street_post_button);
        if (screenType == CreateWallStreetScreenType.CREATE_POST) {
            mPrivacy.set(PublishItem.getItem(UserHelper.getUserLastPostPrivacy(mContext)));
            if (wallStreetAdaptive != null
                    && wallStreetAdaptive.getMedia() != null
                    && !wallStreetAdaptive.getMedia().isEmpty()) {
                mIsShowMediaLayout.set(true);
                mMediaList.addAll(wallStreetAdaptive.getMedia());
            }
            checkToEnableActionButtonStatus(wallStreetAdaptive != null ? wallStreetAdaptive.getDescription() : null);
        } else if (isEditPostType(screenType)) {
            mIgnorePreviewUrlForEditPost = true;
            mIsShowMediaLayout.set(true);
            if (wallStreetAdaptive != null) {
                mPostId = wallStreetAdaptive.getId();
                if (wallStreetAdaptive.getMedia() != null && !wallStreetAdaptive.getMedia().isEmpty()) {
                    mMediaList.addAll(wallStreetAdaptive.getMedia());
                }
                mPrivacy.set(wallStreetAdaptive.getPublish());
                setSearchAddressResultData(wallStreetAdaptive);
                //Will ignore rendering text as quote type for the edit post share external link
                if (isShareExternalLinkPost() && getLinkPreviewModelFromEditPost() != null) {
                    mPostTextGenerateStyleHelper.setIgnoreQuoteStyle(true);
                }
                mAbsLinkPreviewCallback.onSetEditText(wallStreetAdaptive.getDescription(), true);
                if (!WallStreetHelper.isThereRenderLinkPreviewData(wallStreetAdaptive) &&
                        WallStreetHelper.shouldRenderPlacePreview(wallStreetAdaptive)) {
                    mShouldRenderPreviousPlacePreview = true;
                }
            } else {
                mPrivacy.set(PublishItem.Follower);
            }
            actionButton = mContext.getString(R.string.create_wall_street_edit_button);
            checkToEnableActionButtonStatus(wallStreetAdaptive != null ? wallStreetAdaptive.getDescription() : null);
        } else if (screenType == CreateWallStreetScreenType.SHARE_POST) {
            mPrivacy.set(PublishItem.getItem(UserHelper.getUserLastPostPrivacy(mContext)));
            /*
                The action should enable automatically for share post type and currently we don't have the
                share feature yet.
             */
            mIsEnableToolbarButton.set(true);
            if (wallStreetAdaptive != null && !TextUtils.isEmpty(wallStreetAdaptive.getShareUrl())) {
                mAbsLinkPreviewCallback.onSetEditText(wallStreetAdaptive.getShareUrl(), false);
                absLinkPreviewCallback.onStartDownloadLinkPreview(wallStreetAdaptive.getShareUrl());
                requestLinkPreview(wallStreetAdaptive.getShareUrl());
            }
        } else if (screenType == CreateWallStreetScreenType.SHARE_LINK) {
            mPrivacy.set(PublishItem.getItem(UserHelper.getUserLastPostPrivacy(mContext)));
            if (wallStreetAdaptive != null) {
                String previewLink = GenerateLinkPreviewUtil.getPreviewLink(wallStreetAdaptive.getDescription());
                Timber.i("previewLink: " + previewLink + ", Desc: " + wallStreetAdaptive.getDescription());
                if (GenerateLinkPreviewUtil.isValidUrl(previewLink)) {
                    Timber.i("Valid url:");
                    absLinkPreviewCallback.onStartDownloadLinkPreview(wallStreetAdaptive.getDescription());
                    requestLinkPreview(wallStreetAdaptive.getDescription());
                    mShareUrlFromOtherApp = wallStreetAdaptive.getDescription();
                    mAbsLinkPreviewCallback.shouldHidePreviewCloseButton();
                } else {
                    //If the shared content from other app content image
                    if (wallStreetAdaptive.getMedia() != null && !wallStreetAdaptive.getMedia().isEmpty()) {
                        mIsShowMediaLayout.set(true);
                        mMediaList.addAll(wallStreetAdaptive.getMedia());
                    }

                    if (!TextUtils.isEmpty(wallStreetAdaptive.getDescription())) {
                        mAbsLinkPreviewCallback.onSetEditText(wallStreetAdaptive.getDescription(), false);
                        if (!TextUtils.isEmpty(GenerateLinkPreviewUtil.getPreviewLink(wallStreetAdaptive.getDescription()))) {
                            absLinkPreviewCallback.onStartDownloadLinkPreview(wallStreetAdaptive.getDescription());
                            requestLinkPreview(wallStreetAdaptive.getDescription());
                        }
                    }
                }
            }
            checkToEnableActionButtonStatus(wallStreetAdaptive != null ? wallStreetAdaptive.getDescription() : null);
        }

        mIsShowAction.set(true);
        mActionText.set(actionButton);
        mUserFullName.set(getUserFullNameCheckIn());
        mPostTextGenerateStyleHelper.setPostMedia(mMediaList);
    }

    /**
     * This method must be called afer the view model was bound to view
     */
    public void checkToDisplayEditPostPreviewLink() {
        if (isEditPostType(mScreenType.get())) {
            mLinkPreviewModel = getLinkPreviewModelFromEditPost();
//            Timber.i("linkPreviewModel: " + mLinkPreviewModel);
            if (mLinkPreviewModel != null) {
                if (isShareExternalLinkPost()) {
                    mAbsLinkPreviewCallback.shouldHidePreviewCloseButton();
                }
                if (mListener != null) {
                    mListener.onRequestLinkPreviewFinished(getLinkPreviewModelFromEditPost());
                }
            }
            //Must reset this property to false to allow the url preview detection when input text change.
            mIgnorePreviewUrlForEditPost = false;
        }
    }

    private LinkPreviewModel getLinkPreviewModelFromEditPost() {
        LifeStream lifeStream = (LifeStream) mPost;
        return lifeStream.getLinkPreviewModel();
    }

    private void backupOriginalPost(WallStreetAdaptive post) {
        if (post instanceof LifeStream) {
            /*
                Note: Share external is similar to edit post which mean there are already some data
                along with post passed to create screen.
                Get the backup of edit/share external post for checking to enable the action button.
             */
            mPost = new LifeStream((LifeStream) post);
        }
    }

    private boolean isThereLinkPreviewDisablePreviouslyInEditMode() {
        return isEditPostType(mScreenType.get()) &&
                mPost != null &&
                !mPost.shouldShowLinkPreview();
    }

    private boolean isEditPostType(CreateWallStreetScreenType screenType) {
        return screenType == CreateWallStreetScreenType.EDIT_POST ||
                screenType == CreateWallStreetScreenType.EDIT_POST_FROM_POST_DETAIL;
    }

    private void checkToEnableActionButtonStatus(String text) {
        String newText = "";
        if (text != null) {
            newText = text.trim();
        }
        /*
          mPost should contain value if it is in edit post mode or create post from external sharing which
          mean there is already post's data pass into screen.
         */
        if (isEditPostType(mScreenType.get())) {
            mIsEnableToolbarButton.set(hasUserUpdatedPost(newText));
        } else if (mScreenType.get() == CreateWallStreetScreenType.SHARE_LINK) {
            if (!mMediaList.isEmpty()) {
                mIsEnableToolbarButton.set(true);
            } else if (!TextUtils.isEmpty(newText)) {
                mIsEnableToolbarButton.set(true);
            } else if (TextUtils.isEmpty(newText) && mLinkPreviewModel != null) {
                //No input, but there was already link preview display
                mIsEnableToolbarButton.set(true);
            } else if (isThereCheckedIn()) {
                mIsEnableToolbarButton.set(true);
            } else {
                mIsEnableToolbarButton.set(false);
            }
        } else {
            //Create new post
            mIsEnableToolbarButton.set((!TextUtils.isEmpty(newText) || !mMediaList.isEmpty()) ||
                    isThereCheckedIn());
        }
    }

    private boolean hasUserUpdatedPost(String input) {
        if (mPost != null) {
            boolean areMediaTheSame;
            if (!mMediaList.isEmpty()) {
                if (mPost.getMedia() == null || mPost.getMedia().isEmpty()) {
                    areMediaTheSame = false;
                } else if (mMediaList.size() == mPost.getMedia().size()) {
                    boolean areTheSame = true;
                    //Check if the media are still the same or edited
                    for (int i = 0; i < mPost.getMedia().size(); i++) {
                        if (!TextUtils.equals(mPost.getMedia().get(i).getId(),
                                mMediaList.get(i).getId())) {
                            areTheSame = false;
                            break;
                        }
                    }
                    areMediaTheSame = areTheSame;
                } else {
                    areMediaTheSame = false;
                }
            } else {
                areMediaTheSame = mPost.getMedia() == null || mPost.getMedia().isEmpty();
            }

            if (TextUtils.isEmpty(input) &&
                    mMediaList.isEmpty() &&
                    !isThereCheckedIn()) {
                //Should disable action button.
                return false;
            } else {
                return !TextUtils.equals(input, mPost.getDescription()) ||
                        !areMediaTheSame ||
                        isPrivacyUpdated();
            }
        }

        return false;
    }

    private boolean isPrivacyUpdated() {
        return mPost != null && mSelectedPrivacy != mPost.getPublish();
    }

    private List<Media> getMediaItemFromList(List<Media> mediaList, boolean isGetOnlyUploadedFile) {
        List<Media> newMediaList = new ArrayList<>();
        if (mediaList != null) {
            for (Media media : mediaList) {
                if (isGetOnlyUploadedFile) {
                    if (MediaUtil.isHttpUrl(media.getUrl())) {
                        //Get only uploaded to server files
                        newMediaList.add(media);
                    }
                } else {
                    if (!MediaUtil.isHttpUrl(media.getUrl())) {
                        //Get only local file
                        newMediaList.add(media);
                    }
                }
            }
        }

        return newMediaList;
    }

    public void checkToRenderPreviousEditPostPlacePreview() {
        if (mShouldRenderPreviousPlacePreview) {
            mShouldRenderPreviousPlacePreview = false;
            renderPlacePreview();
        }
    }

    public void onLinkPreviewClosed() {
        Timber.i("onLinkPreviewClosed");
        if (isEditPostType(mScreenType.get())) {
            mIsEnableToolbarButton.set(true);
        }
        if (mLinkPreviewModel != null) {
            mLinkPreviewModel.setShouldPreview(false);
        }
    }

    public void onMapPreviewClosed() {
        if (isEditPostType(mScreenType.get())) {
            mIsEnableToolbarButton.set(true);
        }
        mShouldShowMapPreview = false;
        mPostTextGenerateStyleHelper.revalidateTextStyle(false);
    }

    public ObservableField<PublishItem> getPrivacy() {
        return mPrivacy;
    }

    public final void onPrivacyClick() {
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).startActivityForResult(new ChoosePrivacyIntent(getContext(),
                    mSelectedPrivacy.getId()), new ResultCallback() {
                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    ItemPrivacy itemPrivacy = data.getParcelableExtra(ChoosePrivacyIntent.DATA);
                    if (itemPrivacy != null) {
                        mSelectedPrivacy = itemPrivacy.getPrivacy();
                        if (mWallStreetAdaptive.get() != null) {
                            mWallStreetAdaptive.get().setPublish(itemPrivacy.getPrivacy().getId());
                        }
                        mPrivacy.set(itemPrivacy.getPrivacy());
                        checkToEnableActionButtonStatus(getContent());
                    }
                }
            });
        }
    }

    private void setSearchAddressResultData(WallStreetAdaptive wallStreetAdaptive) {
        Locations locations = new Locations();
        locations.setLatitude(wallStreetAdaptive.getLatitude());
        locations.setLongitude(wallStreetAdaptive.getLongitude());
        mSearchAddressResult.setAddress(wallStreetAdaptive.getAddress());
        String fullAddress = wallStreetAdaptive.getAddress();
        if (!TextUtils.isEmpty(wallStreetAdaptive.getLocationName())) {
            fullAddress = wallStreetAdaptive.getLocationName() + ", " + fullAddress;
        }
        mSearchAddressResult.setFullAddress(fullAddress);
        mSearchAddressResult.setCity(wallStreetAdaptive.getCity());
        mSearchAddressResult.setLocations(locations);
        mSearchAddressResult.setPlaceTitle(wallStreetAdaptive.getLocationName());
        mSearchAddressResult.setCountry(wallStreetAdaptive.getLocationCountry());
    }

    private boolean isThereCheckedIn() {
        return mSearchAddressResult != null &&
                !TextUtils.isEmpty(mSearchAddressResult.getAddress()) &&
                !TextUtils.isEmpty(mSearchAddressResult.getPlaceTitle()) &&
                !TextUtils.isEmpty(mSearchAddressResult.getCountry()) &&
                mSearchAddressResult.getLocations() != null &&
                mSearchAddressResult.getLocations().getLatitude() != 0 &&
                mSearchAddressResult.getLocations().getLongitude() != 0;
    }

    private String getRenderedText() {
        String postContent = "";
        if (mEditable != null && mEditable.length() > 0) {
            postContent = RenderTextAsMentionable.render(mContext, mEditable);
        } else if (mListener != null) {
            postContent = RenderTextAsMentionable.render(mContext, mListener.getEditable());
        }
        if (postContent != null) {
            postContent = postContent.trim();
        }

        return postContent;
    }

    @Override
    public void onToolbarButtonClicked(Context context) {
        LifeStream lifeStream = new LifeStream();

        lifeStream.setTitle(getRenderedText());
        lifeStream.setAddress(mSearchAddressResult.getAddress());
        lifeStream.setLocationName(mSearchAddressResult.getPlaceTitle());
        lifeStream.setLocationCountry(mSearchAddressResult.getCountry());
        lifeStream.setCity(mSearchAddressResult.getCity());
        lifeStream.setCountry(mSearchAddressResult.getCountry());
        if (mSearchAddressResult.getLocations() != null) {
            lifeStream.setCountry(mSearchAddressResult.getLocations().getCountry());
            lifeStream.setLatitude(mSearchAddressResult.getLocations().getLatitude());
            lifeStream.setLongitude(mSearchAddressResult.getLocations().getLongitude());
            if (TextUtils.isEmpty(lifeStream.getLocationCountry()) && !TextUtils.isEmpty(lifeStream.getCountry())) {
                //Try to get country name from country code
                Locale locale = new Locale("", lifeStream.getCountry());
                lifeStream.setLocationCountry(locale.getDisplayCountry());
            }
        }

        lifeStream.setPublish(mPrivacy.get().getId());

        if (isEditPostType(mScreenType.get())) {
            lifeStream.setId(mPostId);
            lifeStream.setEditedMode(true);
            WallStreetAdaptive adaptive = mWallStreetAdaptive.get();
            if (adaptive != null) {
                //Backup meta preview state for edit.
                lifeStream.setRawLinkPreviewModelList(((LifeStream) adaptive).getRawLinkPreviewModelList());
                checkUpdateMetaPreviewState(lifeStream);
                if (mLinkPreviewModel != null) {
                    lifeStream.setRetrievedLinkPreviewResult(mLinkPreviewModel);
                }
                /*
                    Note: the current current does not allow to edit media, so we must maintain the previous
                    media if exist.
                 */
                lifeStream.setMedia(adaptive.getMedia());
            }
        } else {
            checkUpdateMetaPreviewState(lifeStream);
            User user = new User();
            user.setId(SharedPrefUtils.getUserId(mContext));
            lifeStream.setStoreUser(user);
            lifeStream.setMedia(mMediaList);
            lifeStream.setShareUrl(mShareUrlFromOtherApp);
            if (mLinkPreviewModel != null) {
                lifeStream.setRetrievedLinkPreviewResult(mLinkPreviewModel);
            }

            Timber.i("MetaPreview: " + new Gson().toJson(lifeStream.getRawLinkPreviewModelList()));
            Timber.i("MetaPreview backup: " + new Gson().toJson(lifeStream.getBackupMetaPreviewModelList()));
        }

        Timber.i("Lifestream: " + new Gson().toJson(lifeStream));
        if (isEditPostType(mScreenType.get())) {
            postWallInBackground(lifeStream, false);
        } else {
            if (mIsFromOtherAppContent) {
                postWallInBackground(lifeStream, false);
            } else {
                //Past result back to home screen
                Intent intent = new Intent();
                intent.putExtra(CreateTimelineIntent.DATA, lifeStream);
                mContext.setResult(AppCompatActivity.RESULT_OK, intent);
                mContext.finish();
            }
        }
    }

    private void postWallInBackground(LifeStream lifeStream, boolean isSetBackResult) {
        //Manage to post in background with notification and set back result to screen starter if necessary
        Intent uploadService = LifeStreamUploadService.getIntent(mContext,
                lifeStream,
                true);
        mContext.startService(uploadService);
        if (isSetBackResult) {
            Intent intent = new Intent();
            intent.putExtra(CreateTimelineIntent.DATA, lifeStream);
            mContext.setResult(AppCompatActivity.RESULT_OK, intent);
        }
        mContext.finish();
    }

    private void checkUpdateMetaPreviewState(LifeStream lifeStream) {
        //Update preview state
        if (!mMediaList.isEmpty()) {
            //Disable map preview
            lifeStream.setRawLinkPreviewModelList(MetaPreviewHelper
                    .addOrUpdateMapMetaPreviewState(lifeStream.getRawLinkPreviewModelList(),
                            false));
            //Disable link preview
            if (mLinkPreviewModel != null) {
                mLinkPreviewModel.setShouldPreview(false);
            }
        } else {
            //Update map preview
            if (isThereCheckedIn()) {
                lifeStream.setRawLinkPreviewModelList(MetaPreviewHelper
                        .addOrUpdateMapMetaPreviewState(lifeStream.getRawLinkPreviewModelList(),
                                mShouldShowMapPreview));
            }
        }
    }

    private void requestLinkPreview(String url) {
        mLinkPreviewModel = null;
        Observable<Response<LinkPreviewModel>> call = mStoreDataManager.getPreviewLink(url);
        ResponseObserverHelper<Response<LinkPreviewModel>> helper = new ResponseObserverHelper<>(mContext, call);
        addDisposable(helper.execute(new OnCallbackListener<Response<LinkPreviewModel>>() {
            @Override
            public void onComplete(Response<LinkPreviewModel> result) {
                Timber.i("onComplete: " + new Gson().toJson(result.body()));
                LinkPreviewModel linkPreviewModel = result.body();
                if (linkPreviewModel != null && linkPreviewModel.isValidPreviewData()) {
                    mLinkPreviewModel = linkPreviewModel;
                    mLinkPreviewModel.setShouldPreview(true);
                    LinkPreviewRetriever.preCachePreviewImage(getContext(), linkPreviewModel);
                    if (mListener != null) {
                        mListener.onRequestLinkPreviewFinished(linkPreviewModel);
                    }
                } else {
                    if (mListener != null) {
                        mListener.onRequestLinkPreviewFinished(null);
                    }
                }
                if (isThereLinkPreviewDisablePreviouslyInEditMode()) {
                    mIsEnableToolbarButton.set(true);
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                if (mListener != null) {
                    mListener.onRequestLinkPreviewFinished(null);
                }
            }
        }));
    }

    private String getContent() {
        if (mEditable != null) {
            return mEditable.toString();
        }
        return "";
    }


    public TextWatcher onTextChanged() {
        //Will not detect any text changed if we create a post from shared link from other app.
        //We can say shared link from other app is also a kind of post.
        if (!TextUtils.isEmpty(mShareUrlFromOtherApp)) {
            return null;
        }

        return new UrlDetectionWatcher() {
            @Override
            public void onTyping(String text) {
                checkToEnableActionButtonStatus(text);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                super.afterTextChanged(editable);
                mEditable = editable;
                Linkify.addLinks(editable, Linkify.ALL);
            }

            @Override
            public void onUrlInput(String url) {
                mAbsLinkPreviewCallback.onStartDownloadLinkPreview(url);
                requestLinkPreview(url);
            }

            @Override
            public void onTextChanged(String text) {
                super.onTextChanged(text);
                checkToToResetClearLinkPreviewOnTextClear(text);
            }

            @Override
            public boolean shouldDetectUrl() {
                return mMediaList.isEmpty() && !mIsShowedLinkPreview && !mIgnorePreviewUrlForEditPost;
            }
        };
    }

    private void checkToToResetClearLinkPreviewOnTextClear(String text) {
        if (text != null &&
                TextUtils.isEmpty(text.trim()) &&
                mIsShowedLinkPreview &&
                mLinkPreviewModel != null &&
                !isShareExternalLinkPost()) {
            mIsShowedLinkPreview = false;
            mLinkPreviewModel = null;
            if (mListener != null) {
                mListener.onHideLinkPreviewLayout();
            }
        }
    }

    private boolean isShareExternalLinkPost() {
        return mPost != null && !TextUtils.isEmpty(mPost.getShareUrl());
    }

    public void setShowedLinkPreview(boolean showedLinkPreview) {
        mIsShowedLinkPreview = showedLinkPreview;
    }

    private void removeLinkPreview() {
        if (mIsShowedLinkPreview) {
            mIsShowedLinkPreview = false;
            mAbsLinkPreviewCallback.onRemoveListener();
        }
    }

    private void removePlacePreviewLayout() {
        if (mListener != null) {
            mListener.onHidePlacePreviewLayout();
        }
    }

    private void validateThePreviousItemSelectionIndex(List<Media> localFiles) {
        /*
          To make sure the index that pass to the gallery builder will be the same as those displaying
          in the screen.
         */
        for (int i = 0; i < localFiles.size(); i++) {
            localFiles.get(i).setIndex(i);
        }
    }

    public void onImageVideoClick() {
        if (mListener != null) {
            mListener.onClearKeyboardFocus();
        }
        CameraIntent intent;
        WallStreetAdaptive adaptive = mWallStreetAdaptive.get();
        if (mIsMediaFromKeyboard || adaptive == null) {
            intent = MyCameraIntent.newTimelineInstance(mContext, null);
        } else {
            List<Media> localSelectedMedia = getMediaItemFromList(adaptive.getMedia(),
                    false);
            validateThePreviousItemSelectionIndex(localSelectedMedia);
            intent = MyCameraIntent.newTimelineInstance(mContext, localSelectedMedia);
        }

        mContext.startActivityForResult(intent, new ResultCallback() {
            @Override
            public void onActivityResultSuccess(int resultCode, Intent data) {
                if (resultCode == Activity.RESULT_OK) {
                    mIsMediaFromKeyboard = false;
                    MyCameraResultIntent cameraIntent1 = new MyCameraResultIntent(data);
                    if (cameraIntent1 != null &&
                            cameraIntent1.getMedia() != null &&
                            !cameraIntent1.getMedia().isEmpty()) {
                        mPostTextGenerateStyleHelper.resetToNormalStyle();

                        /*
                            Maintain the previous uploaded files after having selected some new media
                            from gallery or taking photo and this case is must for edit mode.
                         */
                        List<Media> uploadedFiles = getMediaItemFromList(mMediaList,
                                true);
                        Timber.i("Uploaded media: " + new Gson().toJson(uploadedFiles));
                        Timber.i("Gallery media: " + new Gson().toJson(cameraIntent1.getMedia()));
                        mMediaList.clear();
                        mMediaList.addAll(uploadedFiles);
                        mMediaList.addAll(cameraIntent1.getMedia());

                        final LifeStream lifeStream = new LifeStream();
                        lifeStream.setMedia(mMediaList);

                        mWallStreetAdaptive.set(lifeStream);
                        mIsShowMediaLayout.set(true);
                        removeLinkPreview();
                        removePlacePreviewLayout();
                        checkToEnableActionButtonStatus(getContent());
                    }
                }
            }
        });
    }

    public OnItemClickListener<Integer> onMediaItemClickListener() {
        return position -> {
            if (mScreenType.get() != CreateWallStreetScreenType.SHARE_POST) {
                EditMediaFileIntent intent = new EditMediaFileIntent(mContext,
                        (LifeStream) mWallStreetAdaptive.get(),
                        position,
                        true);
                mContext.startActivityForResult(intent, new ResultCallback() {
                    @Override
                    public void onActivityResultSuccess(int resultCode, Intent data) {
                        mMediaList.clear();
                        EditMediaFileIntent intent1 = new EditMediaFileIntent(data);
                        LifeStream lifeStream = intent1.getLifeStream();
                        if (lifeStream.getMedia() != null && !lifeStream.getMedia().isEmpty()) {
                            mMediaList.addAll(lifeStream.getMedia());
                            mWallStreetAdaptive.set(lifeStream);
                        } else {
                            mWallStreetAdaptive.set(null);
                            mIsShowMediaLayout.set(false);
                        }
                        checkToEnableActionButtonStatus(getContent());
                    }
                });

            }
        };
    }

    public void onCheckInClick() {
        if (mListener != null) {
            mListener.onClearKeyboardFocus();
        }
        SelectAddressIntent intent = new SelectAddressIntent(mContext, mSearchAddressResult);
        mContext.startActivityForResult(intent,
                new ResultCallback() {
                    @Override
                    public void onActivityResultSuccess(int resultCode, Intent data) {
                        SelectAddressIntentResult intentResult = new SelectAddressIntentResult(data);
                        mSearchAddressResult = intentResult.getResult();
                        mUserFullName.set(getUserFullNameCheckIn());
                        if (mMediaList.isEmpty()) {
                            renderPlacePreview();
                        }
                        mIsEnableToolbarButton.set(true); // Allow to post even if user only check-in.
                    }
                });
    }

    private void renderPlacePreview() {
//        Timber.i("renderPlacePreview: " + new Gson().toJson(mSearchAddressResult));
        if (!mIsShowedLinkPreview &&
                mListener != null &&
                mSearchAddressResult != null &&
                mSearchAddressResult.getLocations() != null) {
            PlacePreviewLayout.PlacePreviewData data = new PlacePreviewLayout.PlacePreviewData();
            data.setLatitude(mSearchAddressResult.getLocations().getLatitude());
            data.setLongitude(mSearchAddressResult.getLocations().getLongitude());
            data.setPaceTitle(mSearchAddressResult.getPlaceTitle());
            data.setPlaceDescription(mSearchAddressResult.getAddress());
            mListener.onRenderPlacePreview(data);
            mPostTextGenerateStyleHelper.revalidateTextStyle(true);
            mShouldShowMapPreview = true;
        }
    }

    private Spannable getUserFullNameCheckIn() {
        return SpannableUtil.getUserFullNameCheckIn(mContext,
                mUser,
                mSearchAddressResult,
                v -> onCheckInClick());
    }

    public int getMarginDimen() {
        if (mIsShareWallStreet.get()) {
            return mContext.getResources().getDimensionPixelSize(R.dimen.space_large);
        } else {
            return 0;
        }
    }

    @Override
    public void onCommitContent(InputContentInfoCompat inputContentInfo, int flags, Bundle opts) {
        mIsMediaFromKeyboard = true;
        //TODO: need to get width, height from image view
        Uri uri = inputContentInfo.getLinkUri();
        if (uri != null) {
            Media media = new Media();
            media.setUrl(String.valueOf(uri));
            media.setType(MediaType.IMAGE);

            mMediaList.clear();
            mMediaList.add(media);

            final LifeStream lifeStream = new LifeStream();
            lifeStream.setMedia(mMediaList);

            mWallStreetAdaptive.set(lifeStream);
            mIsShowMediaLayout.set(true);
            removeLinkPreview();
            removePlacePreviewLayout();

            if (!mIsEnableToolbarButton.get()) {
                mIsEnableToolbarButton.set(true);
            }
        }
    }

    public void getMentionUserList(String userName, OnCallbackListener<List<UserMentionable>> listOnCallbackListener) {
        Observable<List<UserMentionable>> observable = mStoreDataManager.getUserMentionList(userName);
        BaseObserverHelper<List<UserMentionable>> helper = new BaseObserverHelper<>(getContext(), observable);
        helper.execute(listOnCallbackListener);
    }

    public interface CreateTimelineFragmentListener {

        void onHidePlacePreviewLayout();

        void onHideLinkPreviewLayout();

        void onRenderPlacePreview(PlacePreviewLayout.PlacePreviewData data);

        void onClearKeyboardFocus();

        Editable getEditable();

        void onRequestLinkPreviewFinished(LinkPreviewModel linkPreviewModel);
    }

    @BindingAdapter({"privacyIcon", "privacyText", "privacy"})
    public static void updatePrivacy(ViewGroup root,
                                     ImageView privacyIcon,
                                     TextView privacyText,
                                     PublishItem privacy) {
        privacyIcon.setImageResource(privacy.getIcon());
        privacyText.setText(privacy.getTitleResource());
    }
}
