package com.proapp.sompom.viewmodel.newviewmodel;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ConciergeShopCategoryAdapter;
import com.proapp.sompom.databinding.ListItemConciergeShopCategoryBinding;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.DelayViewClickListener;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.intent.ConciergeBrandDetailIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeShopCategory;
import com.proapp.sompom.model.concierge.ConciergeShopSetting;
import com.proapp.sompom.model.concierge.ConciergeSubCategory;
import com.proapp.sompom.model.concierge.ConciergeSupplier;
import com.proapp.sompom.model.concierge.ConciergeSupportItemAndSubCategoryAdaptive;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.emun.ConciergeSortItemOption;
import com.proapp.sompom.model.emun.OpenBrandDetailType;
import com.proapp.sompom.model.result.UserBadge;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.ConciergeShopCategoryDataManager;
import com.proapp.sompom.viewmodel.ConciergeSupportAddItemToCardViewModel;
import com.proapp.sompom.viewmodel.ItemConciergeShopInfoViewModel;
import com.proapp.sompom.viewmodel.ListItemConciergeShopCategoryViewModel;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Veasna Chhom on 26/1/22.
 */
public class ConciergeShopCategoryFragmentViewModel extends ConciergeSupportAddItemToCardViewModel<ConciergeShopCategoryDataManager,
        ConciergeShopCategoryFragmentViewModel.ConciergeShopCategoryFragmentViewModelListener> {

    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    public final ObservableBoolean mEnableRefresh = new ObservableBoolean(true);
    private final ObservableBoolean mIsHasPending = new ObservableBoolean();
    private final ObservableField<ConciergeFragmentCurrentLocationViewModel> mCurrentLocationViewModel = new ObservableField<>();
    private final ObservableInt mOrderBadgeCount = new ObservableInt();
    private final ObservableField<ConciergeShopCategory> mDefaultShopCategory = new ObservableField<>();
    private final ObservableField<String> mSelectedCategoryTitle = new ObservableField<>();
    private final ObservableField<String> mRequestItemError = new ObservableField<>();
    private final ObservableBoolean mIsRequestingItem = new ObservableBoolean();
    private final ObservableBoolean mIsExpressMode = new ObservableBoolean();
    private final ObservableField<String> mSupplierLogo = new ObservableField<>();
    private ObservableBoolean mIsToolbarCollapsed = new ObservableBoolean();
    private final ObservableField<String> mSupplierTitle = new ObservableField<>();
    private final ObservableField<String> mSupplierAddress = new ObservableField<>();
    private final ObservableField<String> mSupplierOpenDeliveryTime = new ObservableField<>();
    private final ObservableField<String> mSupplierEstimateDeliveryTime = new ObservableField<>();
    private final ObservableBoolean mIsShopOpen = new ObservableBoolean();
    private final ObservableField<String> mNextAvailableOpenSupplierTime = new ObservableField<>();

    private ConciergeCartBottomSheetViewModel mCartViewModel;
    private ListItemConciergeShopCategoryViewModel mDefaultCategoryViewModel;
    private ConciergeShopCategory mSelectedCategory;
    private List<String> mFilterBrandId = new ArrayList<>();
    private String mSortBy;
    private boolean mIsDisplayOutOfStockItem;
    private ConciergeSortItemOption mConciergeSortItemOption;

    public ConciergeShopCategoryFragmentViewModel(ConciergeShopCategoryDataManager dataManager,
                                                  boolean isInExpressMode,
                                                  ConciergeCartBottomSheetViewModel cartViewModel,
                                                  ConciergeShopCategoryFragmentViewModelListener listener) {
        super(dataManager.getContext(), listener, dataManager);
        mIsExpressMode.set(isInExpressMode);
        mDataManager = dataManager;
        mCartViewModel = cartViewModel;
        mListener = listener;
        bindNotificationBadge();
    }

    public ObservableBoolean getIsShopOpen() {
        return mIsShopOpen;
    }

    public ObservableField<String> getSupplierTitle() {
        return mSupplierTitle;
    }

    public ObservableField<String> getSupplierAddress() {
        return mSupplierAddress;
    }

    public ObservableField<String> getSupplierOpenDeliveryTime() {
        return mSupplierOpenDeliveryTime;
    }

    public ObservableField<String> getSupplierEstimateDeliveryTime() {
        return mSupplierEstimateDeliveryTime;
    }

    public ObservableField<String> getNextAvailableOpenSupplierTime() {
        return mNextAvailableOpenSupplierTime;
    }

    public ObservableField<String> getSupplierLogo() {
        return mSupplierLogo;
    }

    public ObservableBoolean getIsToolbarCollapsed() {
        return mIsToolbarCollapsed;
    }

    public ObservableBoolean getIsExpressMode() {
        return mIsExpressMode;
    }

    public void requestBatchShopCategoryData(boolean isRefreshWholeScreen) {
        if (!isRefreshWholeScreen) {
            showLoading();
        }

        Observable<Response<Object>> observable = mDataManager.getBatchRequestOfShopCategoryService();
        BaseObserverHelper<Response<Object>> helper = new BaseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (isRefreshWholeScreen) {
                    mIsRefresh.set(false);
                    showSnackBar(ex.getMessage(), true);
                } else {
                    showError(ex.getMessage());
                }
            }

            @Override
            public void onComplete(Response<Object> result) {
                if (isRefreshWholeScreen) {
                    mIsRefresh.set(false);
                }
                bindSupplierData();
                mDefaultShopCategory.set(mDataManager.getDefaultShopCategory());
                mDefaultShopCategory.notifyChange();
                if (mListener != null) {
                    mListener.onSupplierLoadSuccess(mDataManager.getSupplier());
                    mListener.onLoadBatchDataSuccess(mDataManager.getRequestedConciergeShopCategories(),
                            isRefreshWholeScreen);
                }
            }
        }));
    }

    public void setIsToolbarCollapsed(boolean isToolbarCollapsed) {
        mIsToolbarCollapsed.set(isToolbarCollapsed);
    }

    private void bindSupplierData() {
        if (mDataManager.getSupplier() != null) {
            mIsShopOpen.set(mDataManager.getSupplier().isShopOpen());
            mNextAvailableOpenSupplierTime.set(mDataManager.getSupplier().getAvailableDay());
            mSupplierTitle.set(mDataManager.getSupplier().getSupplierName());
            mSupplierLogo.set(mDataManager.getSupplier().getLogo());
            //Bind start delivery time
            LocalTime startTime = null;
            LocalTime endTime = null;
            if (!TextUtils.isEmpty(mDataManager.getSupplier().getStartDeliveryTime())) {
                startTime = LocalTime.parse(mDataManager.getSupplier().getStartDeliveryTime());
            }
            if (!TextUtils.isEmpty(mDataManager.getSupplier().getEndDeliveryTime())) {
                endTime = LocalTime.parse(mDataManager.getSupplier().getEndDeliveryTime());
            }

            String deliveryTime = ConciergeHelper.buildTimeSlotDisplayFormat2(mContext, startTime, endTime);
            if (!TextUtils.isEmpty(deliveryTime)) {
                mSupplierOpenDeliveryTime.set(mContext.getString(R.string.shop_category_list_delivery_time, deliveryTime));
            } else {
                mSupplierOpenDeliveryTime.set(mContext.getString(R.string.shop_category_list_delivery_time, ConciergeHelper.N_A));
            }

            //Bind estimate delivery time
            if (mDataManager.getSupplier().getEstimateTimeDeliveryMinute() > 0) {
                int hour = mDataManager.getSupplier().getEstimateTimeDeliveryMinute() / 60;
                int minute = mDataManager.getSupplier().getEstimateTimeDeliveryMinute() % 60;

                StringBuilder time = new StringBuilder();
                if (hour > 1) {
                    time.append(hour);
                    time.append(mContext.getString(R.string.common_timestamp_plural_abbreviation_hour_label));
                } else if (hour == 1) {
                    time.append(hour);
                    time.append(mContext.getString(R.string.common_timestamp_single_abbreviation_hour_label));
                }

                if (minute > 0) {
                    if (!TextUtils.isEmpty(time)) {
                        time.append(" ");
                    }
                    time.append(minute);
                    if (minute > 1) {
                        time.append(mContext.getString(R.string.common_timestamp_plural_abbreviation_minute_label));
                    } else {
                        time.append(mContext.getString(R.string.common_timestamp_single_abbreviation_minute_label));
                    }
                }

                if (!TextUtils.isEmpty(time.toString())) {
                    mSupplierEstimateDeliveryTime.set(mContext.getString(R.string.shop_category_list_delivery_estimate_time, time.toString()));
                }
            } else {
                ConciergeShopSetting conciergeShopSetting = ConciergeHelper.getConciergeShopSetting(mContext);
                if (conciergeShopSetting != null) {
                    int minute = conciergeShopSetting.getDefaultEstimateTimeDeliveryMinute();
                    String displayMinute = String.valueOf(minute);
                    if (minute > 1) {
                        displayMinute += mContext.getString(R.string.common_timestamp_plural_abbreviation_minute_label);
                    } else {
                        displayMinute += mContext.getString(R.string.common_timestamp_single_abbreviation_minute_label);
                    }
                    mSupplierEstimateDeliveryTime.set(mContext.getString(R.string.shop_category_list_delivery_estimate_time, displayMinute));
                } else {
                    mSupplierEstimateDeliveryTime.set(mContext.getString(R.string.shop_category_list_delivery_estimate_time,
                            ConciergeHelper.N_A));
                }
            }

            //Bind address
            if (!TextUtils.isEmpty(mDataManager.getSupplier().getAddress())) {
                mSupplierAddress.set(mDataManager.getSupplier().getAddress());
            } else {
                mSupplierAddress.set(ConciergeHelper.N_A);
            }
        }
    }

    public void setSortBy(String sortBy) {
        mSortBy = sortBy;
    }

    public boolean isDisplayOutOfStockItem() {
        return mIsDisplayOutOfStockItem;
    }

    public List<String> getFilterBrandId() {
        return mFilterBrandId;
    }

    public void updateFilterSelectedCriteria(boolean isShowOutOfStockItem, List<String> filterBrandId) {
        mIsDisplayOutOfStockItem = isShowOutOfStockItem;
        mFilterBrandId = filterBrandId;
    }

    public void onApplyFilter(boolean isShowOutOfStockItem, List<String> filterBrandId) {
        updateFilterSelectedCriteria(isShowOutOfStockItem, filterBrandId);
        requestItem(mSelectedCategory, false, true, true, true);
    }

    public void onApplySort(ConciergeSortItemOption sortItemOption) {
        ConciergeHelper.setSortItemOption(sortItemOption);
        mConciergeSortItemOption = sortItemOption;
        setSortBy(sortItemOption.getValue());
        requestItem(mSelectedCategory, false, true, true, true);
    }

    public void setConciergeSortItemOption(ConciergeSortItemOption conciergeSortItemOption) {
        mConciergeSortItemOption = conciergeSortItemOption;
    }

    public ConciergeSortItemOption getConciergeSortItemOption() {
        return mConciergeSortItemOption;
    }

    public void loadMoreSubCategoryItem(ConciergeSubCategory existingItem, OnCallbackListener<Response<ConciergeSubCategory>> listener) {
        Observable<Response<ConciergeSubCategory>> observable = mDataManager.loadMoreSubCategoryItem(existingItem,
                mFilterBrandId);
        BaseObserverHelper<Response<ConciergeSubCategory>> helper = new BaseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(listener));
    }

    public ObservableBoolean getIsRequestingItem() {
        return mIsRequestingItem;
    }

    public ObservableField<String> getRequestItemError() {
        return mRequestItemError;
    }

    public ObservableField<ConciergeShopCategory> getDefaultShopCategory() {
        return mDefaultShopCategory;
    }

    public ConciergeShopCategoryAdapter.ConciergeShopCategoryAdapterListener getCategoryListener() {
        return mListener.getCategoryListener();
    }

    public void onCategorySelected(ConciergeShopCategory category) {
        mSelectedCategory = category;
    }

    public boolean isCategorySelected(ConciergeShopCategory category) {
        return mSelectedCategory != null && TextUtils.equals(category.getId(), mSelectedCategory.getId());
    }

    public void onSearchProductClicked() {
        if (mDataManager.getSupplier() != null) {
            mContext.startActivity(new ConciergeBrandDetailIntent(mContext,
                    mDataManager.getSupplier().getId(),
                    mDataManager.getSupplier().getSupplierName(),
                    OpenBrandDetailType.OPEN_AS_SEARCH_SUPPLIER_PRODUCT_MODE));
        }
    }

    public ConciergeShopCategory getSelectedCategory() {
        return mSelectedCategory;
    }

    public ViewCreationListener getViewCreationListener() {
        return new ViewCreationListener() {
            @Override
            public void onDefaultCategoryCreated(ListItemConciergeShopCategoryViewModel viewModel) {
                mDefaultCategoryViewModel = viewModel;
            }

            @Override
            public void onShopViewModelCreated(ItemConciergeShopInfoViewModel viewModel) {
            }
        };
    }

    public void checkToUnselectDefaultCategory() {
        if (mDefaultCategoryViewModel != null) {
            mDefaultCategoryViewModel.setSelection(false);
        }
    }

    public void requestItemOnCategorySelected(ConciergeShopCategory category) {
        requestItem(category, false, true, true, true);
    }

    public void requestMoreItemOfSelectedCategory(ConciergeShopCategory category) {
        requestItem(category, true, false, false, false);
    }

    private void requestItem(ConciergeShopCategory category,
                             boolean isLoadMore,
                             boolean isRefreshScreen,
                             boolean showLoading,
                             boolean isRefreshWholeData) {
        if (isRefreshWholeData) {
            mDataManager.resetReachExtraItemSection();
        }
        if (showLoading) {
            mIsRequestingItem.set(true);
            mRequestItemError.set(null);
        }
        mSelectedCategoryTitle.set(category.getTitle());
        Observable<List<ConciergeSupportItemAndSubCategoryAdaptive>> observable = mDataManager.getShopItemByCategory(category.getId(),
                isLoadMore,
                mFilterBrandId);
        BaseObserverHelper<List<ConciergeSupportItemAndSubCategoryAdaptive>> helper = new BaseObserverHelper<>(getContext(), observable);
        OnCallbackListener<List<ConciergeSupportItemAndSubCategoryAdaptive>> onCallbackListener = new OnCallbackListener<List<ConciergeSupportItemAndSubCategoryAdaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.i("onFail: " + ex.getMessage());
                if (isValidResponseOfSelectedCategory(this)) {
                    if (!isLoadMore) {
                        hideLoading();
                        mIsRequestingItem.set(false);
                        mRequestItemError.set(ex.getMessage());
                    } else {
                        showSnackBar(ex.getMessage(), true);
                        if (mListener != null) {
                            mListener.onLoadMoreItemFailed();
                        }
                    }
                }
            }

            @Override
            public void onComplete(List<ConciergeSupportItemAndSubCategoryAdaptive> result) {
                if (isValidResponseOfSelectedCategory(this)) {
                    if (!isLoadMore) {
                        hideLoading();
                        mIsRequestingItem.set(false);
                        if (result == null || result.isEmpty()) {
                            mRequestItemError.set(mContext.getResources().getString(R.string.shop_category_list_no_product_out_of_stock));
                        }
                    }

                    /*
                        If the app is in express mode and the selected category is ALL and it is loading
                        more action and there is no more data to be loaded more next, we will manage to
                        load extra item of that supplier.
                     */
                    boolean canLoadMore = mDataManager.isCanLoadMore();
                    if (category.isAllCategoryType() &&
                            mIsExpressMode.get() &&
                            (isLoadMore || isRefreshScreen) &&
                            !canLoadMore &&
                            !mDataManager.isReachedLoadExtraItemState()) {
                        //Will add display title of extra item of supplier.
                        Timber.i("Add extra item section title");
                        mRequestItemError.set(null);
                        mDataManager.onReachedLoadExtraItemState();
                        requestItem(category, true, false, false, false);
                    }

                    if (mListener != null) {
                        mListener.onLoadItemByCategorySuccess(result, isLoadMore, canLoadMore, isRefreshScreen);
                    }
                }
            }
        };
        mDataManager.addRequestItemByCategoryMap(onCallbackListener, category.getId());
        addDisposable(helper.execute(onCallbackListener));
    }

    private boolean isValidResponseOfSelectedCategory(OnCallbackListener<List<ConciergeSupportItemAndSubCategoryAdaptive>> callbackListener) {
        /*
            We will pass only request response of selected category so to prevent wrong item data display
            when user switch fast on the category selection and request item of that selected just come after
            user switch to another category.
         */
        String requestCategoryId = mDataManager.getRequestCategoryIdByCallback(callbackListener);
        mDataManager.removeRequestItemByCategoryMap(callbackListener);
//        Timber.i("requestCategoryId: " + requestCategoryId);
        return TextUtils.equals(requestCategoryId, mSelectedCategory.getId());
    }

    public ObservableBoolean getEnableRefresh() {
        return mEnableRefresh;
    }

    public ConciergeCartBottomSheetViewModel getCartViewModel() {
        return mCartViewModel;
    }

    public ObservableBoolean getIsHasPending() {
        return mIsHasPending;
    }

    public void setIsHasPending(boolean isHasPending) {
        mIsHasPending.set(isHasPending);
    }

    public ObservableField<ConciergeFragmentCurrentLocationViewModel> getCurrentLocationViewModel() {
        return mCurrentLocationViewModel;
    }

    public ObservableInt getOrderBadgeCount() {
        return mOrderBadgeCount;
    }

    private void bindNotificationBadge() {
        UserBadge userBadge = UserHelper.getUserBadge(getContext());
        if (userBadge != null) {
            mOrderBadgeCount.set(userBadge.getConciergeOrderBadge().intValue());
        }
    }

    public void loadUserCurrentLocation() {
        if (mCurrentLocationViewModel.get() != null) {
            mCurrentLocationViewModel.get().setDefaultLocationForUser();
        }
    }

    public void onUserAddressSelected(ConciergeUserAddress selectedAddress) {
        if (mCurrentLocationViewModel.get() != null) {
            mCurrentLocationViewModel.get().saveAndUpdatedUserSelectedLocation(selectedAddress);
        }
    }

    public void onUserAddressUpdated(ConciergeUserAddress updatedAddress) {
        if (mCurrentLocationViewModel.get() != null) {
            mCurrentLocationViewModel.get().checkIfCurrentAddressIsUpdated(updatedAddress);
        }
    }

    public void onUserAddressDeleted(ConciergeUserAddress deletedAddress) {
        if (mCurrentLocationViewModel.get() != null) {
            mCurrentLocationViewModel.get().checkIfCurrentAddressIsDeleted(deletedAddress);
        }
    }

    public void getUserSelectedLocation() {
        if (mCurrentLocationViewModel.get() == null) {
            mCurrentLocationViewModel.set(new ConciergeFragmentCurrentLocationViewModel(mContext));
        }
        mCurrentLocationViewModel.get().loadUserSelectedLocation();
    }

    public void updateOrderBadgeCount(long newCount) {
        Timber.i("Updated badge count " + newCount);
        mOrderBadgeCount.set((int) newCount);
    }

    public void onShopStatusUpdated(boolean isShopClosed) {
        if (mCartViewModel != null) {
            mCartViewModel.updateCheckoutButtonStatus(isShopClosed);
        }
    }

    @Override
    public void onRetryClick() {
        requestBatchShopCategoryData(false);
    }

    public void onRefresh() {
        //Reset the previous selected category.
        mSelectedCategory = null;
        mIsRefresh.set(true);
        requestBatchShopCategoryData(true);
    }

    public void onDirectlyAddProduct(ConciergeMenuItem trendingItem) {
        if (mListener != null) {
            mListener.onItemAddedToCart(trendingItem);
        }
    }

    public interface ViewCreationListener {
        void onDefaultCategoryCreated(ListItemConciergeShopCategoryViewModel viewModel);

        void onShopViewModelCreated(ItemConciergeShopInfoViewModel viewModel);
    }

    public interface ConciergeShopCategoryFragmentViewModelListener extends ConciergeSupportAddItemToCardViewModel.ConciergeSupportAddItemToCardViewModelListener {
        void notifyShouldShowCartBottomSheet();

        void onLoadBatchDataSuccess(List<ConciergeShopCategory> categories, boolean isRefreshWholeScreen);

        void onLoadItemByCategorySuccess(List<ConciergeSupportItemAndSubCategoryAdaptive> itemList,
                                         boolean isLoadMore,
                                         boolean isCanLoadMore,
                                         boolean isAppliedFilterResult);

        ConciergeShopCategoryAdapter.ConciergeShopCategoryAdapterListener getCategoryListener();

        void onLoadMoreItemFailed();

        default void onSupplierLoadSuccess(ConciergeSupplier supplier) {
        }
    }

    @BindingAdapter({"categoryView", "category", "categoryListener", "viewCreationListener"})
    public static void setDefaultCategoryData(ViewGroup viewContainer,
                                              ListItemConciergeShopCategoryBinding binding,
                                              ConciergeShopCategory category,
                                              ConciergeShopCategoryAdapter.ConciergeShopCategoryAdapterListener listener,
                                              ViewCreationListener viewCreationListener) {
        if (category != null && listener != null) {
            ListItemConciergeShopCategoryViewModel viewModel = new ListItemConciergeShopCategoryViewModel(viewContainer.getContext(), category);
            binding.setVariable(BR.viewModel, viewModel);
            binding.getRoot().setOnClickListener(new DelayViewClickListener() {
                @Override
                public void onDelayClick(@NonNull View widget) {
                    viewModel.setSelection(true);
                    listener.onCategoryClicked(category);
                }
            });
            //Make default selection
            if (viewCreationListener != null) {
                viewCreationListener.onDefaultCategoryCreated(viewModel);
            }

            //Make default selection
            binding.getRoot().post(() -> binding.getRoot().performClick());
        }
    }
}
