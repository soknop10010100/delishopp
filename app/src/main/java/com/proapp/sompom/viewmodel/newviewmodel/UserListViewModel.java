package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.listener.UserListAdaptive;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.UserContactDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import timber.log.Timber;

public class UserListViewModel extends AbsLoadingViewModel {

    private UserContactDataManager mDataManager;
    private UserListViewModelListener mListener;
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private boolean mIsLoadedData;
    private int mLocalDataSize;

    public UserListViewModel(Context context,
                             UserContactDataManager dataManager,
                             UserListViewModelListener listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
    }

    public void initializeViewModel() {
        requestUserGroupListFromDb();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public OnClickListener onRetryButtonClick() {
        return () -> requestUserGroupList(false, false);
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        requestUserGroupList(true, false);
    }

    @Override
    public void onRetryClick() {
        requestUserGroupList(false, false);
    }

    private void requestUserGroupListFromDb() {
        showLoading();
        List<UserListAdaptive> list = UserHelper.getUserListForContactScreen(getContext());
//        Timber.i("Contact from DB: " + new Gson().toJson(list));
        mLocalDataSize = list.size();
        onLoadFromDbSuccess(list);
        requestUserGroupList(false, false);
    }

    private void onLoadFromDbSuccess(List<UserListAdaptive> adaptiveList) {
        hideLoading();
        if (mListener != null) {
            //Consider as can load more.
            //No load more feature yet
            mListener.onLoadSuccess(adaptiveList,
                    false,
                    false,
                    false);
        }
    }

    public void requestUserGroupList(boolean isRefresh, boolean isLoadMore) {
        Observable<List<UserListAdaptive>> suggestCall = mDataManager.getUserGroupListService();
        BaseObserverHelper<List<UserListAdaptive>> suggestHelper =
                new BaseObserverHelper<>(getContext(), suggestCall);
        addDisposable(suggestHelper.execute(new OnCallbackListener<List<UserListAdaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e("onFail: " + ex.getCode());
                if (mIsLoadedData && !isLoadMore) {
                    mIsRefresh.set(false);
                    return;
                }
                mIsLoadedData = false;
                mIsRefresh.set(false);
                if (isLoadMore) {
                    showSnackBar(ex.getMessage(), true);
                    if (mListener != null) {
                        mListener.onLoadSuccess(new ArrayList<>(),
                                isRefresh,
                                isLoadMore,
                                false);
                    }
                } else {
                    if (!mListener.hasItemInList()) {
                        // If there are no previous data in contact list and the contact list failed
                        // to load, show empty data screen?
                        showEmptyDataError(getContext().getString(R.string.contact_empty_title),
                                getContext().getString(R.string.contact_empty_message));
                    } else if (isRefresh) {
                        showSnackBar(ex.getMessage(), true);
                    }
                }
            }

            @Override
            public void onComplete(List<UserListAdaptive> result) {
                Timber.i("onComplete");
                mIsLoadedData = true;
                mIsRefresh.set(false);
                if ((result == null || result.isEmpty()) && !isLoadMore && mLocalDataSize <= 0) {
                    showEmptyDataError(getContext().getString(R.string.contact_empty_title),
                            getContext().getString(R.string.contact_empty_message));
                } else if (result != null) {
                    if (mListener != null) {
                        // No load more feature yet
                        mListener.onLoadSuccess(result, isRefresh, isLoadMore, false);
                    }
                }
            }
        }));
    }

    public interface UserListViewModelListener {

        void onLoadSuccess(List<UserListAdaptive> userGroups,
                           boolean isRefresh,
                           boolean isLoadMore,
                           boolean isCanLoadMore);

        boolean hasItemInList();
    }
}
