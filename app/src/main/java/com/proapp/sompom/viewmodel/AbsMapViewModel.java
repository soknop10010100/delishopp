package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.text.TextUtils;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.CheckRequestLocationCallbackHelper;
import com.proapp.sompom.helper.RequestLocationManager;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.utils.BitmapConverter;
import com.proapp.sompom.utils.LocationStateUtil;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 20/12/21.
 */
public abstract class AbsMapViewModel extends AbsLoadingViewModel {

    protected static final int MAX_RESULT = 1;
    public static final float ZOOM_LEVEL = 18f;
    protected static final float ZOOM_LEVEL_CITY = 10f;

    protected final GoogleMap mGoogleMap;
    protected SearchAddressResult mSearchAddressResult;

    public AbsMapViewModel(Context context,
                           GoogleMap googleMap) {
        super(context);
        this.mGoogleMap = googleMap;
    }

    public GoogleMap getGoogleMap() {
        return mGoogleMap;
    }

    public SearchAddressResult getSearchAddressResult() {
        return mSearchAddressResult;
    }

    public void enableMapClick(boolean isEnable) {
        mGoogleMap.setOnMapClickListener(isEnable ?
                (latLng -> updateMapOnUserSelection(latLng, null, true)) :
                null);
    }

    public void enablePoiClick(boolean isEnable) {
        mGoogleMap.setOnPoiClickListener(isEnable ?
                (pointOfInterest -> updateMapOnUserSelection(pointOfInterest.latLng, getValidPOIsName(pointOfInterest.name), true)) :
                null);
    }

    public void checkToSelectPreviousLocation() {
        //Check to load the previous selected location if any
        if (mSearchAddressResult != null &&
                !TextUtils.isEmpty(mSearchAddressResult.getFullAddress()) &&
                mSearchAddressResult.getLocations() != null) {
            addMarker(mSearchAddressResult.getLocations().getLatitude(),
                    mSearchAddressResult.getLocations().getLongitude(),
                    false);
            onAddressChanged(mSearchAddressResult.getFullAddress());
            loadPreviousLocationIfNecessary(mSearchAddressResult.getLocations());
            findAddressCountry(mSearchAddressResult);
        } else {
            tryToLoadDefaultMapLocationOfCurrentDevice();
        }
    }

    public void onMoveToCurrentLocation(View view) {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).requestLocation(true,
                    new CheckRequestLocationCallbackHelper((Activity) mContext,
                            location -> {
                                Timber.i("onComplete request location: " + location.toString());
                                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),
                                                location.getLongitude()),
                                        ZOOM_LEVEL));
                            }));
        }
    }

    public void tryToLoadDefaultMapLocationOfCurrentDevice() {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).requestLocation(true,
                    new CheckRequestLocationCallbackHelper((Activity) mContext,
                            location -> {
                                zoomGoogleMapCamera(location.getLatitude(), location.getLongitude());
                                addMarker(location.getLatitude(), location.getLongitude(), true);
                            }));
        }
    }

    public void zoomToLocation(double latitude, double longitude, float zoom) {
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), zoom));
    }

    public void zoomGoogleMapCamera(double latitude, double longitude) {
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), ZOOM_LEVEL));
    }

    public void defaultZoomToCurrentCity() {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).requestLocation(true,
                    new CheckRequestLocationCallbackHelper((Activity) mContext,
                            location -> {
                                zoomToLocation(location.getLatitude(), location.getLongitude(), ZOOM_LEVEL_CITY);
                            }));
        }
    }

    public void findAddressCountry(SearchAddressResult addressResult) {
        if (addressResult.getLocations() == null) {
            return;
        }

        Observable<String> observable = Observable.just(new Object()).concatMap(o -> {
            Geocoder geocoder = new Geocoder(mContext);
            List<Address> addressList = geocoder.getFromLocation(addressResult.getLocations().getLatitude(),
                    addressResult.getLocations().getLongitude(),
                    MAX_RESULT);
            if (addressList != null && !addressList.isEmpty()) {
                return Observable.just(addressList.get(0).getCountryCode());
            } else {
                return Observable.just("");
            }
        });

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> addressResult.getLocations().setCountry(data), Timber::e);
    }

    public void updateMapOnUserSelection(LatLng point, String selectionName, boolean shouldAddMarker) {
        Timber.i("point: " + point.toString() + ", selectionName: " + selectionName);
        if (shouldAddMarker) {
            addMarker(point.latitude, point.longitude, false);
        }
        onAddressChanged(mContext.getString(R.string.search_address_looking_for_address_title));
        Observable<SearchAddressResult> observable = Observable.just(point).concatMap(new Function<LatLng, ObservableSource<? extends SearchAddressResult>>() {
            @Override
            public ObservableSource<? extends SearchAddressResult> apply(LatLng latLng) throws Exception {
                Geocoder geocoder = new Geocoder(mContext);
                List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, MAX_RESULT);
                Timber.i("addressList: " + new Gson().toJson(addressList));
                if (addressList != null && !addressList.isEmpty()) {
//                Timber.i("Address 1: " + new Gson().toJson(addressList.get(0)));
                    SearchAddressResult searchAddressResult = new SearchAddressResult();
                    searchAddressResult.setAddress(LocationStateUtil.getOnlyAddressFromFullPlace(addressList.get(0).getAddressLine(0)));
                    if (!TextUtils.isEmpty(selectionName)) {
                        //From POIs selection
                        searchAddressResult.setPlaceTitle(selectionName);
                        String fullAddress = selectionName;
                        if (!TextUtils.isEmpty(searchAddressResult.getAddress())) {
                            fullAddress += ", " + searchAddressResult.getAddress();
                        }
                        searchAddressResult.setFullAddress(fullAddress);
                    } else {
                        searchAddressResult.setPlaceTitle(LocationStateUtil.getPlaceNameFromFullAddress(addressList.get(0).getAddressLine(0)));
                        searchAddressResult.setFullAddress(addressList.get(0).getAddressLine(0));
                    }
                    searchAddressResult.setCity(addressList.get(0).getLocality());
                    searchAddressResult.setCountry(addressList.get(0).getCountryName());
                    Locations locations = new Locations();
                    locations.setLatitude(point.latitude);
                    locations.setLongitude(point.longitude);
                    locations.setCountry(addressList.get(0).getCountryCode());
                    searchAddressResult.setLocations(locations);

                    Timber.i("Country: " + locations.getCountry());

                    return Observable.just(searchAddressResult);
                }

                return Observable.error(new Throwable(mContext.getString(R.string.search_address_looking_for_address_title)));
            }
        });

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    mSearchAddressResult = data;
                    Timber.i("mSearchAddressResult: " + new Gson().toJson(mSearchAddressResult));
                    onAddressChanged(mSearchAddressResult.getFullAddress());
                }, throwable -> {
                    mSearchAddressResult = null;
                    onAddressChanged(mContext.getString(R.string.search_address_looking_for_address_title));
                });
    }

    public void addMarker(double lat, double lng, boolean shouldClickMarker) {
        MarkerOptions options = new MarkerOptions();
        options.icon(BitmapConverter.bitmapDescriptorFromVector(mContext, R.drawable.ic_home_icon));
        options.position(new LatLng(lat, lng));
        mGoogleMap.clear();
        Marker marker = mGoogleMap.addMarker(options);
        if (marker != null && shouldClickMarker) {
            zoomGoogleMapCamera(lat, lng);
            updateMapOnUserSelection(marker.getPosition(), marker.getTitle(), false);
        }
    }

    public void loadPreviousLocationIfNecessary(Locations previousLocation) {
        if (mGoogleMap != null && previousLocation != null) {
            Timber.i("Move map previous selected location.");
            zoomGoogleMapCamera(previousLocation.getLatitude(), previousLocation.getLongitude());
        }
    }

    public String getValidPOIsName(String checkName) {
        Timber.i(" " + (!TextUtils.isEmpty(checkName) ? checkName.length() : " null"));
        if (TextUtils.isEmpty(checkName)) {
            return "";
        } else {
            return LocationStateUtil.makeAddressValidation(checkName.trim());
        }
    }

    public void enableCurrentLocation(boolean isEnable) {
        if (RequestLocationManager.isLocationPermissionGranted(mContext)) {
            mGoogleMap.setMyLocationEnabled(isEnable);
        }
    }

    public abstract void onAddressChanged(String newAddress);
}
