package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.google.gson.Gson;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.DelayClickableSpan;
import com.proapp.sompom.helper.DelayWatcher;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.intent.ConciergeReviewCheckoutIntent;
import com.proapp.sompom.intent.CouponIntent;
import com.proapp.sompom.intent.PayWithCardIntent;
import com.proapp.sompom.intent.newintent.ConciergeCurrentLocationIntent;
import com.proapp.sompom.intent.newintent.ConciergeMyAddressIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.ConciergeCartModel;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.ShopDeliveryTimeSelection;
import com.proapp.sompom.model.concierge.ConciergeBasket;
import com.proapp.sompom.model.concierge.ConciergeCoupon;
import com.proapp.sompom.model.concierge.ConciergeGetDeliveryFeeResponse;
import com.proapp.sompom.model.concierge.ConciergeItemOutOfStockAlternativeOption;
import com.proapp.sompom.model.concierge.ConciergeOnlinePaymentProvider;
import com.proapp.sompom.model.concierge.ConciergeOrderResponseWithABAPayWay;
import com.proapp.sompom.model.concierge.ConciergeUpdateBasketRequest;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.concierge.ConciergeVerifyPaymentWithABAResponse;
import com.proapp.sompom.model.concierge.CouponDate;
import com.proapp.sompom.model.concierge.DeliveryRange;
import com.proapp.sompom.model.emun.CitySelectionType;
import com.proapp.sompom.model.emun.ConciergeOnlinePaymentOption;
import com.proapp.sompom.model.emun.ConciergePaymentMethod;
import com.proapp.sompom.model.emun.CustomErrorDataType;
import com.proapp.sompom.model.emun.ShopDeliveryType;
import com.proapp.sompom.model.result.AppSetting;
import com.proapp.sompom.model.result.ConciergeCheckoutData;
import com.proapp.sompom.model.result.ConciergeOrderResponse;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.fragment.AbsSupportABAPaymentFragment;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ConciergeCheckoutManager;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.utils.NetworkStateUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;
import com.sompom.abapayway.helper.ABAPayWayHelper;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 16/9/21.
 */
public class ConciergeCheckoutFragmentViewModel extends AbsLoadingViewModel {

    private final ObservableField<String> mCompanyName = new ObservableField<>();
    private final ObservableField<String> mUserName = new ObservableField<>();
    private final ObservableField<String> mDeliveryAddress = new ObservableField<>();
    private final ObservableField<String> mSubTotal = new ObservableField<>();
    private final ObservableField<String> mDeliveryCharge = new ObservableField<>();
    private final ObservableField<String> mContainerCharge = new ObservableField<>();
    private final ObservableField<String> mCouponValue = new ObservableField<>();
    private final ObservableField<String> mWalletValue = new ObservableField<>();
    private final ObservableField<String> mTotalDiscount = new ObservableField<>();
    private final ObservableField<String> mGrandTotal = new ObservableField<>();
    private final ObservableField<String> mInputCoupon = new ObservableField<>();
    private final ObservableField<String> mTime = new ObservableField<>();
    private final ObservableField<String> mDeliveryTime = new ObservableField<>();
    private final ObservableField<String> mDeliveryType = new ObservableField<>();
    private final ObservableBoolean mDeliveryTypeVisibility = new ObservableBoolean();
    private final ObservableField<String> mAccountNumber = new ObservableField<>();
    private final ObservableField<String> mAccountOwner = new ObservableField<>();
    private final ObservableBoolean mIsReceiptAdded = new ObservableBoolean(false);
    private final ObservableField<ConciergePaymentMethod> mPaymentType = new ObservableField<>(ConciergePaymentMethod.CASH_ON_DELIVERY);
    private final ObservableBoolean mIsDirectOnlinePaymentOptionSelected = new ObservableBoolean();
    private boolean mIsOnlinePaymentAvailable;
    private final ObservableBoolean mShouldDisplayOnlinePaymentSection = new ObservableBoolean();
    private final ObservableBoolean mCouponApplied = new ObservableBoolean();
    private final ObservableField<String> mDeliveryAddressType = new ObservableField<>();
    private final ObservableField<String> mDetailDeliveryAddress = new ObservableField<>();
    private final ObservableField<String> mAddressNameAndPhone = new ObservableField<>();
    private final ObservableBoolean mIsCancelOrderOptionSelected = new ObservableBoolean();
    private final ObservableBoolean mIsChangeOrderOptionSelected = new ObservableBoolean();
    private final ObservableBoolean mEnablePayOnDelivery = new ObservableBoolean(true);
    private final ObservableBoolean mEnablePayOnline = new ObservableBoolean(true);

    private final ObservableInt mCheckoutButtonTextColor = new ObservableInt();
    private final ObservableInt mCheckoutButtonBackground = new ObservableInt();
    private final ObservableInt mApplyCouponButtonTextColor = new ObservableInt();
    private final ObservableInt mApplyCouponButtonBackground = new ObservableInt();
    private final ObservableField<String> mCouponButtonTitle = new ObservableField<>();
    private final ObservableBoolean mIsWalletAvailable = new ObservableBoolean();
    private final ObservableBoolean mIsWalletApplied = new ObservableBoolean();
    private final ObservableField<String> mWalletBalance = new ObservableField<>();
    private final ObservableBoolean mAlreadyAppliedWalletBefore = new ObservableBoolean();
    private boolean mIsPayButtonEnabled = false;
    private boolean mIsApplyCouponButtonEnabled = false;

    private final ConciergeCheckoutManager mDataManager;
    private ConciergeCheckoutData mCheckoutData;
    private final ConciergeCheckoutFragmentViewModelCallback mListener;
    private ShopDeliveryTimeSelection mShopDeliveryTimeSelection;
    private ConciergeUserAddress mUserAddress;
    private Media mSelectedReceiptFile;
    private ConciergeOnlinePaymentProvider mSelectedOnlinePaymentAccount;
    private boolean mIsRequestedOpenABAAppOnDeviceForABAPay;
    private boolean mIsAlreadyReceivedABAPushbackResultForABAPay;
    private ConciergeUserAddress mConciergeUserAddress;
    private String mSelectedSlotId;
    private boolean mIsSlotProvince;
    private ConciergeItemOutOfStockAlternativeOption mOOSOption;
    private ConciergeOrderResponseWithABAPayWay mResumePaymentData;
    private String mPassedDeliveryInstruction;
    private ConciergeCoupon mConciergeCoupon;

    public ConciergeCheckoutFragmentViewModel(Context context,
                                              ConciergeUserAddress conciergeUserPrimaryAddress,
                                              String selectedSlotId,
                                              boolean isSlotProvince,
                                              ConciergeOrderResponseWithABAPayWay resumePaymentData,
                                              String passedDeliveryInstruction,
                                              ConciergeCheckoutManager dataManager,
                                              ConciergeCheckoutFragmentViewModelCallback listener) {
        super(context);
        mDataManager = dataManager;
        mSelectedSlotId = selectedSlotId;
        mIsSlotProvince = isSlotProvince;
        mResumePaymentData = resumePaymentData;
        mPassedDeliveryInstruction = passedDeliveryInstruction;
        mDataManager.setConciergeUserPrimaryAddress(conciergeUserPrimaryAddress);
        mConciergeUserAddress = conciergeUserPrimaryAddress;
        Timber.i("selectedSlotId: " + selectedSlotId + ", passedDeliveryInstruction: " + mPassedDeliveryInstruction);
        mListener = listener;
        initDeliveryDateSelectionData();
        updatePassedDeliveryInstructionToRemoteBasket();
    }

    public ObservableBoolean getEnablePayOnDelivery() {
        return mEnablePayOnDelivery;
    }

    public ObservableBoolean getEnablePayOnline() {
        return mEnablePayOnline;
    }

    public ObservableBoolean getAlreadyAppliedWalletBefore() {
        return mAlreadyAppliedWalletBefore;
    }

    public ObservableBoolean getIsWalletAvailable() {
        return mIsWalletAvailable;
    }

    public ObservableBoolean getIsWalletApplied() {
        return mIsWalletApplied;
    }

    public ObservableBoolean getIsCancelOrderOptionSelected() {
        return mIsCancelOrderOptionSelected;
    }

    public ObservableBoolean getIsChangeOrderOptionSelected() {
        return mIsChangeOrderOptionSelected;
    }

    public ObservableField<String> getWalletBalance() {
        return mWalletBalance;
    }

    public ConciergeCheckoutManager getDataManager() {
        return mDataManager;
    }

    public ObservableBoolean getIsDirectOnlinePaymentOptionSelected() {
        return mIsDirectOnlinePaymentOptionSelected;
    }

    public ObservableField<String> getDeliveryAddressType() {
        return mDeliveryAddressType;
    }

    public ObservableField<String> getDetailDeliveryAddress() {
        return mDetailDeliveryAddress;
    }

    public ObservableField<String> getAddressNameAndPhone() {
        return mAddressNameAndPhone;
    }

    public ObservableField<String> getCouponButtonTitle() {
        return mCouponButtonTitle;
    }

    public ObservableBoolean getCouponApplied() {
        return mCouponApplied;
    }

    public ObservableField<String> getCouponValue() {
        return mCouponValue;
    }

    public ObservableField<String> getWalletValue() {
        return mWalletValue;
    }

    public ObservableInt getApplyCouponButtonTextColor() {
        return mApplyCouponButtonTextColor;
    }

    public ObservableInt getApplyCouponButtonBackground() {
        return mApplyCouponButtonBackground;
    }

    public ObservableField<ConciergePaymentMethod> getPaymentType() {
        return mPaymentType;
    }

    public ObservableField<String> getUserName() {
        return mUserName;
    }

    public ObservableField<String> getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public ObservableField<String> getSubTotal() {
        return mSubTotal;
    }

    public ObservableField<String> getDeliveryCharge() {
        return mDeliveryCharge;
    }

    public ObservableField<String> getContainerCharge() {
        return mContainerCharge;
    }

    public ObservableField<String> getTotalDiscount() {
        return mTotalDiscount;
    }

    public ObservableField<String> getGrandTotal() {
        return mGrandTotal;
    }

    public ObservableField<String> getTime() {
        return mTime;
    }

    public ObservableField<String> getDeliveryTime() {
        return mDeliveryTime;
    }

    public ObservableField<String> getInputCoupon() {
        return mInputCoupon;
    }

    public ObservableInt getCheckoutButtonTextColor() {
        return mCheckoutButtonTextColor;
    }

    public ObservableInt getCheckoutButtonBackground() {
        return mCheckoutButtonBackground;
    }

    public ObservableBoolean getShouldDisplayOnlinePaymentSection() {
        return mShouldDisplayOnlinePaymentSection;
    }

    public ConciergeOnlinePaymentProvider getSelectedOnlinePaymentAccount() {
        return mSelectedOnlinePaymentAccount;
    }

    private void updatePassedDeliveryInstructionToRemoteBasket() {
        if (!TextUtils.isEmpty(mPassedDeliveryInstruction)) {
            updateOnlineBasket(new ConciergeUpdateBasketRequest(),false);
        }
    }

    public void updateOnlinePaymentSelectionAccount(ConciergeOnlinePaymentProvider paymentOption) {
        // Update online basket
        ConciergeUpdateBasketRequest request = new ConciergeUpdateBasketRequest();
        request.setPaymentProvider(paymentOption);
        updateOnlineBasket(request,false);

        mIsDirectOnlinePaymentOptionSelected.set(paymentOption.isDirectPayment());
        mSelectedOnlinePaymentAccount = paymentOption;
        mAccountNumber.set(paymentOption.getAccountNumber());
        mAccountOwner.set(paymentOption.getAccountOwner());
        checkToEnablePayButtonOnOnlinePaymentOptionSelected();
        ConciergeOnlinePaymentOption conciergeOnlinePaymentOption = ConciergeOnlinePaymentOption.fromValue(paymentOption.getProvider());
        if (conciergeOnlinePaymentOption == ConciergeOnlinePaymentOption.ABA_PAY) {
            FlurryHelper.logEvent(mContext, FlurryHelper.PAY_WITH_ABA_PAY);
        } else if (conciergeOnlinePaymentOption == ConciergeOnlinePaymentOption.CARD) {
            FlurryHelper.logEvent(mContext, FlurryHelper.PAY_WITH_CARD);
        } else if (conciergeOnlinePaymentOption == ConciergeOnlinePaymentOption.KHQR) {
            FlurryHelper.logEvent(mContext, FlurryHelper.PAY_WITH_KHQR);
        }
    }

    private void checkToEnablePayButtonOnOnlinePaymentOptionSelected() {
        enablePayButton(mUserAddress != null);
    }

    public void onReceiveABAPaymentPushbackEvent(String basketId, boolean isPaymentSuccess, String message) {
        Timber.i("onReceiveABAPaymentPushbackEvent: basketId: " + basketId + ", isPaymentSuccess: " + isPaymentSuccess);
        Timber.i("Will handle handleABAPushBackResult for ABA Pay.");
        mIsAlreadyReceivedABAPushbackResultForABAPay = true;
        handleABAPushBackResult(isPaymentSuccess, message, true);
    }

    @Override
    public void onRetryClick() {
        requestData(true, true, false);
    }

    public void initDeliveryDateSelectionData() {
        //Init with default value
        mShopDeliveryTimeSelection = new ShopDeliveryTimeSelection();
        mShopDeliveryTimeSelection.setTimeSlotId(mSelectedSlotId);
        mShopDeliveryTimeSelection.setIsProvince(mIsSlotProvince);
        mShopDeliveryTimeSelection.setSelectedDate(LocalDateTime.now());
        mShopDeliveryTimeSelection.setDeliveryType(ShopDeliveryType.DELIVERY);

        mDeliveryTime.set(mContext.getString(R.string.shop_checkout_asap_text));
        mDeliveryType.set(mShopDeliveryTimeSelection.getDeliveryType().getDisplayValue(mContext));
        mDeliveryTypeVisibility.set(true);
    }

    public void reloadData(boolean shouldRequestUserWallet) {
        requestData(false, false, shouldRequestUserWallet);
    }

    public void requestData(boolean isFirstLoadData, boolean isShowLoading, boolean shouldRequestUserWallet) {
        if (isShowLoading || shouldRequestUserWallet) {
            showLoading();
        }
        Observable<Response<ConciergeCheckoutData>> service = mDataManager.getCheckoutData(isFirstLoadData, shouldRequestUserWallet);
        ResponseObserverHelper<Response<ConciergeCheckoutData>> helper = new ResponseObserverHelper<>(getContext(), service);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeCheckoutData>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.i("onFail: " + ex.getMessage());
                hideLoading();
                showError(ex.getMessage());
            }

            @Override
            public void onComplete(Response<ConciergeCheckoutData> result) {
                Timber.i("onComplete: " + new Gson().toJson(result.body()));
                mCheckoutData = result.body();
                boolean shouldHideLoading = true;
                if (isFirstLoadData && mResumePaymentData != null) {
                    Timber.i("Will resume ABA payment directly");
                    shouldHideLoading = false;
                    handleABAPayment(ConciergeOnlinePaymentOption.fromValue(mResumePaymentData.getPaymentProvider().getProvider()),
                            mResumePaymentData,
                            true);
                    mResumePaymentData = null;
                    new Handler().postDelayed(() -> hideLoading(), 2000);
                }

                // Check to sync data from online basket with local basket
                if (isFirstLoadData && mResumePaymentData == null) {
                    if (mCheckoutData != null) {
                        checkToSyncDataWithOnlineBasket(mCheckoutData);
                    }
                }
                // Bind other data normally
                bindData();
                if (isFirstLoadData) {
                    if (mListener != null) {
                        mListener.onRequestDataSuccess(mCheckoutData);
                    }
                } else {
                    if (mListener != null) {
                        mListener.onRefreshData(mCheckoutData);
                    }
                }

                if (shouldHideLoading) {
                    hideLoading();
                }
            }
        }));
    }

    public void updateOnlineBasket(ConciergeUpdateBasketRequest request, boolean isReload) {
        /*
            Always to check to update delivery instruction which passed from review checkout screen.
            By doing this, we can guarantee that what user input for delivery instruction in previous will be saved and used
            properly in the order process.
         */
        if (!TextUtils.isEmpty(mPassedDeliveryInstruction)) {
            request.setDeliveryInstruction(mPassedDeliveryInstruction);
        }

        ConciergeHelper.updateOnlineBasket(mContext,
                mDataManager.mApiService,
                request,
                new OnCallbackListener<Response<ConciergeBasket>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        Timber.e("Failed to update cart with these data: " + new Gson().toJson(request));
                    }

                    @Override
                    public void onComplete(Response<ConciergeBasket> result) {
                        if(isReload) {
                            reloadData(false);
                        }
                        Timber.i("Cart updated successfully");
                    }
                });
    }

    public void checkToSyncDataWithOnlineBasket(ConciergeCheckoutData checkoutData) {
        ConciergeCartModel cartModel = checkoutData.getCartModel();
        // Update delivery address if it was saved previously
        if (cartModel.getDeliveryAddress() != null) {
            updateUserSelectedAddress(cartModel.getDeliveryAddress(), false);
        }

        // Update payment method if one was previously saved
        if (!TextUtils.isEmpty(cartModel.getPaymentMethod())) {
            mPaymentType.set(ConciergePaymentMethod.fromValue(cartModel.getPaymentMethod()));
        }

        // Update payment provider if one was previously saved
        if (cartModel.getPaymentProvider() != null) {
            // If there are payment method, we can assume that it's an online payment method?
            mSelectedOnlinePaymentAccount = cartModel.getPaymentProvider();
        }

        if (!TextUtils.isEmpty(cartModel.getReceiptUrl())) {
            mSelectedReceiptFile = new Media();
            mSelectedReceiptFile.setUrl(cartModel.getReceiptUrl());
            mIsReceiptAdded.set(true);
        }
    }

    public List<ConciergeOnlinePaymentProvider> getPaymentOptions() {
        List<ConciergeOnlinePaymentProvider> paymentProviders = new ArrayList<>(mDataManager.getPaymentOptions());
        mIsOnlinePaymentAvailable = !paymentProviders.isEmpty();
        mShouldDisplayOnlinePaymentSection.set(mIsOnlinePaymentAvailable && mPaymentType.get() == ConciergePaymentMethod.ONLINE_METHOD);
        return paymentProviders;
    }

    public void reloadUserAndCompanyInfo() {
        AppSetting appSetting = SharedPrefUtils.getAppSetting(getContext());
        if (appSetting != null) {
            mCheckoutData.setCompanyName(appSetting.getOrganizationName());
        }
        User user = SharedPrefUtils.getUser(getContext());
        mCheckoutData.setUserName(user.getFullName());
    }

    private void bindData() {
        /*
            Will check to take address from basket if it exists or take the primary address.
            Normally primary is already passed via open checkout screen.
         */
        if (mCheckoutData.getCartModel().getDeliveryAddress() != null) {
            mUserAddress = mCheckoutData.getCartModel().getDeliveryAddress();
            //Will update delivery address to the one from basket
            mConciergeUserAddress = mCheckoutData.getCartModel().getDeliveryAddress();
            if (mListener != null) {
                mListener.addMarker(mConciergeUserAddress.getLatitude(), mConciergeUserAddress.getLongitude());
            }
        }

        mOOSOption = ConciergeItemOutOfStockAlternativeOption.fromValue(mCheckoutData.getCartModel().getOOSOption());
        updateOOSOptionSelection();

        //Check user wallet option
        if (mDataManager.getWalletBalance() > 0) {
            mIsWalletAvailable.set(true);
            mAlreadyAppliedWalletBefore.set(mCheckoutData.getCartModel().isWalletApplied());
            updateWalletBalanceDisplay(mDataManager.getWalletBalance());
        } else {
            mIsWalletAvailable.set(false);
        }

        bindDeliveryAddress(mConciergeUserAddress);
        if (mCheckoutData.getCartModel().getConciergeCoupon() != null &&
                mCheckoutData.getCartModel().getConciergeCoupon().getmCouponCode() != null) {
            mInputCoupon.set(mCheckoutData.getCartModel().getConciergeCoupon().getmCouponCode());
            mCouponApplied.set(true);
            enableApplyCouponButton(true, true);
        } else {
            mCouponApplied.set(false);
            enableApplyCouponButton(false, false);
            mInputCoupon.set("");
        }

        if (mCheckoutData.getCartModel().getConciergeCoupon() != null) {
            mCouponValue.set(ConciergeHelper.getDisplayPrice(mContext, mCheckoutData.getCartModel().getConciergeCoupon().getDiscountPrice() ));
        } else {
            mCouponValue.set(ConciergeHelper.getDisplayPrice(mContext, 0f));
        }

        mCompanyName.set(mCheckoutData.getCompanyName());
        mUserName.set(mCheckoutData.getUserName());

        if (mCheckoutData.getCartModel().getConciergeCoupon() != null &&
                mCheckoutData.getCartModel().getConciergeCoupon().getDeliveryInfo() != null &&
                mCheckoutData.getCartModel().getConciergeCoupon().getDeliveryInfo().getDeliveryFee() == 0 ) {
            mDeliveryCharge.set(ConciergeHelper.getDisplayPrice(mContext, 0f));
        } else {
            mDeliveryCharge.set(ConciergeHelper.getDisplayPrice(mContext, mCheckoutData.getDeliveryCharge()));
        }

        mContainerCharge.set(ConciergeHelper.getDisplayPrice(mContext, mCheckoutData.getContainerCharge()));

        calculateTotalPrice();

        if (mPaymentType.get() == ConciergePaymentMethod.CASH_ON_DELIVERY) {
            enablePayButton(mConciergeUserAddress != null);
        }
    }

    private boolean shouldEnablePayOnDeliveryOption() {
        if (mUserAddress != null) {
            String city = mUserAddress.getCity();
            if (TextUtils.isEmpty(city)) {
                return false;
            }

            return TextUtils.equals(city.toLowerCase(), CitySelectionType.PHNOM_PENH.getKey().toLowerCase());
        }

        return true;
    }

    private void updateWalletBalanceDisplay(double balance) {
        mWalletBalance.set(mContext.getString(R.string.shop_checkout_payment_wallet_balance_title) +
                " " +
                ConciergeHelper.getDisplayPrice(mContext, balance));
    }

    private void updateOOSOptionSelection() {
        if (mOOSOption == ConciergeItemOutOfStockAlternativeOption.CANCEL_ITEM) {
            mIsCancelOrderOptionSelected.set(true);
            mIsChangeOrderOptionSelected.set(false);
        } else if (mOOSOption == ConciergeItemOutOfStockAlternativeOption.BE_CONTACTED) {
            mIsChangeOrderOptionSelected.set(true);
            mIsCancelOrderOptionSelected.set(false);
        } else {
            mIsChangeOrderOptionSelected.set(false);
            mIsCancelOrderOptionSelected.set(false);
        }
    }

    private void bindDeliveryAddress(ConciergeUserAddress conciergeUserAddress) {
        if (conciergeUserAddress != null) {
            mDeliveryAddressType.set(conciergeUserAddress.getLabel());
            String addressDetail = conciergeUserAddress.getHouseNumber() +
                    ", " +
                    conciergeUserAddress.getStreetDetail() +
                    ", " +
                    conciergeUserAddress.getCity() +
                    "\n" +
                    conciergeUserAddress.getAreaDetail();
            mDetailDeliveryAddress.set(addressDetail);
            mAddressNameAndPhone.set(conciergeUserAddress.getFirstName() + " "
                    + conciergeUserAddress.getLastName() +
                    "\n" +
                    "+"
                    + conciergeUserAddress.getCountryCode()
                    + conciergeUserAddress.getPhone());

            if (!MainApplication.isInExpressMode()) {
                checkToShowPaymentOptionBaseOnSelectedAddress(conciergeUserAddress);
            }
        }
    }

    private void checkToShowPaymentOptionBaseOnSelectedAddress(ConciergeUserAddress conciergeUserAddress) {
        DeliveryRange deliveryRange = conciergeUserAddress.getDeliveryRange();
        if (deliveryRange != null && deliveryRange.getAllowPaymentMethod() != null) {
            List<String> allowPaymentMethod = deliveryRange.getAllowPaymentMethod();
            boolean allowPayCash = false;
            boolean allowPayOnline = false;
            for (String option : allowPaymentMethod) {
                ConciergePaymentMethod paymentMethod = ConciergePaymentMethod.fromValue(option);
                if (paymentMethod == ConciergePaymentMethod.CASH_ON_DELIVERY) {
                    allowPayCash = true;
                }
                if (paymentMethod == ConciergePaymentMethod.ONLINE_METHOD) {
                    allowPayOnline = true;
                }
            }

            ConciergePaymentMethod currentOption = mPaymentType.get();
            mEnablePayOnDelivery.set(allowPayCash);
            mEnablePayOnline.set(allowPayOnline);
            if (!allowPayOnline) {
                mShouldDisplayOnlinePaymentSection.set(false);
            }
            if (allowPayCash && !allowPayOnline && currentOption == ConciergePaymentMethod.ONLINE_METHOD) {
                onPaymentOptionSelected(ConciergePaymentMethod.CASH_ON_DELIVERY);
            } else if (allowPayOnline && !allowPayCash && currentOption == ConciergePaymentMethod.CASH_ON_DELIVERY) {
                //Will force to select cash option instead.
                onPaymentOptionSelected(ConciergePaymentMethod.ONLINE_METHOD);
            }
        }
    }

    public void onCouponButtonClicked() {
        KeyboardUtil.hideKeyboard((AppCompatActivity) mContext);
        if (mIsApplyCouponButtonEnabled) {
            if (mCouponApplied.get()) {
                removeAppliedCoupon(true, true);
            } else {
                applyCoupon();
            }
        }
    }

    private void removeAppliedCoupon(boolean showLoading, boolean shouldReloadBasket) {
        if (showLoading) {
            showLoading(true);
        }
        Observable<Response<ConciergeCartModel>> observable = mDataManager.removeCouponToLocalBasket();
        ResponseObserverHelper<Response<ConciergeCartModel>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeCartModel>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                if (showLoading) {
                    showSnackBar(ex.getMessage(), true);
                }
            }

            @Override
            public void onComplete(Response<ConciergeCartModel> result) {
                hideLoading();
                if (shouldReloadBasket) {
                    reloadData(false);
                }
                //Remove applied coupon from remote basket
                updateAppliedCouponToRemoteBasket(null);
            }
        }));
    }

    private void requestDeliveryFee() {
        showLoading(true);
        Observable<Response<ConciergeGetDeliveryFeeResponse>> observable = mDataManager.getDeliveryFee(mConciergeUserAddress);
        ResponseObserverHelper<Response<ConciergeGetDeliveryFeeResponse>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeGetDeliveryFeeResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<ConciergeGetDeliveryFeeResponse> result) {
                hideLoading();
                if (result.body().isError()) {
                    mListener.showOrderErrorPopup(result.body().getErrorMessage(), null);

                }
                reloadData(false);
            }
        }));
    }

    private void updateAppliedCouponToRemoteBasket(String couponId) {
        ConciergeUpdateBasketRequest request = new ConciergeUpdateBasketRequest();
        //Need to set "" for remove applied coupon from basket.
        request.setCouponId(couponId == null ? "" : couponId);
        updateOnlineBasket(request,false);
    }

    private void applyCoupon() {
        FlurryHelper.logEvent(mContext, FlurryHelper.APPLY_COUPON);
        showLoading(true);
        Observable<Response<ConciergeCoupon>> observable = mDataManager.getApplyCouponService(mInputCoupon.get().trim() , mCheckoutData.getCartModel().getType(),mCheckoutData.getCartModel().getBasketId());
        ResponseObserverHelper<Response<ConciergeCoupon>> helper = new ResponseObserverHelper<>(getContext(), observable,true);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeCoupon>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<ConciergeCoupon> result) {
                hideLoading();
                Timber.i("Success: " + new Gson().toJson(result.body()));
                if (result.body() != null && result.body().getData() != null) {
                    CouponDate couponDate = result.body().getData();
                    ConciergeCoupon conciergeCoupon = new ConciergeCoupon();
                    conciergeCoupon.setCouponCodeId(couponDate.getCouponCodeId());
                    conciergeCoupon.setCouponCode(couponDate.getCouponCode());
                    conciergeCoupon.setMessage(couponDate.getMessage());
                    conciergeCoupon.setDeliveryInfo(couponDate.getDeliveryInfo());
                    conciergeCoupon.setDiscountPrice(couponDate.getDiscountPrice());
                    conciergeCoupon.setFinalPrice(couponDate.getFinalPrice());
                    conciergeCoupon.setSubTotal(couponDate.getSubTotal());
                    mCheckoutData.getCartModel().setConciergeCoupon(conciergeCoupon);
                    bindData();
                    updateAppliedCouponToRemoteBasket(result.body().getData().getCouponCodeId());
                } else {
                    mListener.showOrderErrorPopup(result.body().getMessage(), null);
                }
            }
        }));
    }

    public TextWatcher onInputCouponCodeTextChanged() {
        return new DelayWatcher(true) {
            @Override
            public void onTextChanged(String text) {

            }

            @Override
            public void onTextChangedNoDelay(String text) {
                enableApplyCouponButton(!TextUtils.isEmpty(text.trim()), mCouponApplied.get());
            }

            @Override
            public void onTyping(String text) {
            }
        };
    }

    private void enableApplyCouponButton(boolean isEnable, boolean isCouponApplied) {
        //Set apply button background
        if (isEnable) {
            mIsApplyCouponButtonEnabled = true;
            if (isCouponApplied) {
                mCouponButtonTitle.set(mContext.getString(R.string.shop_checkout_remove_coupon_button));
                mApplyCouponButtonTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_remove_coupon));
                mApplyCouponButtonBackground.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_remove_coupon_background));
            } else {
                mCouponButtonTitle.set(mContext.getString(R.string.shop_checkout_apply_coupon_button));
                mApplyCouponButtonTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_apply_coupon_enable));
                mApplyCouponButtonBackground.set(AttributeConverter.convertAttrToColor(mContext, R.attr.common_primary_color));
            }
        } else {
            mIsApplyCouponButtonEnabled = false;
            mCouponButtonTitle.set(mContext.getString(R.string.shop_checkout_apply_coupon_button));
            mApplyCouponButtonTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_apply_coupon_disable));
            mApplyCouponButtonBackground.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_apply_coupon_background_disable));
        }
    }

    public void onWalletOptionClicked(SwitchCompat switchCompat) {
        switchCompat.toggle();
    }

    public CompoundButton.OnCheckedChangeListener getApplyWalletSwitchListener() {
        return (compoundButton, checked) -> {
            mIsWalletApplied.set(checked);
            updateApplyWalletStatusToRemoteBasket();
            calculateTotalPrice();
            if (checked) {
                FlurryHelper.logEvent(mContext, FlurryHelper.APPLY_WALLET);
            }
        };
    }

    public final ClickableSpan getAccessPublicCouponClickableSpan() {
        return new DelayClickableSpan() {
            @Override
            public void onDelayClick(@NonNull View widget) {
                if (mContext instanceof AbsBaseActivity) {
                    ((AbsBaseActivity) mContext).startActivityForResult(new CouponIntent(mContext,
                            null), new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            CouponIntent couponIntent = new CouponIntent(data);
                            ConciergeCoupon selectedCoupon = couponIntent.getSelectedCoupon();
                            Timber.i("selectedCoupon: " + new Gson().toJson(selectedCoupon));
                            mInputCoupon.set(selectedCoupon.getCouponCode());
                            enableApplyCouponButton(true, false);
                            onCouponButtonClicked();
                        }
                    });
                }
            }
        };
    }

    public final void onPaymentOptionSelected(ConciergePaymentMethod paymentType) {
        mPaymentType.set(paymentType);
        ConciergeUpdateBasketRequest request = new ConciergeUpdateBasketRequest();
        request.setPaymentMethod(paymentType.getValue());
        updateOnlineBasket(request,false);

        if (paymentType == ConciergePaymentMethod.ONLINE_METHOD) {
            // Check if there are any available online payment returned from server.
            if (getPaymentOptions() != null && !getPaymentOptions().isEmpty()) {
                mIsOnlinePaymentAvailable = true;
                checkToEnablePayButtonOnOnlinePaymentOptionSelected();
            } else {
                // If not, don't enable pay button when user select online payment method and update
                // views accordingly
                mIsOnlinePaymentAvailable = false;
                enablePayButton(false);
            }
            mShouldDisplayOnlinePaymentSection.set(mIsOnlinePaymentAvailable);
        } else {
            FlurryHelper.logEvent(mContext, FlurryHelper.PAY_WITH_CASH);
            mShouldDisplayOnlinePaymentSection.set(false);
            if (mUserAddress != null && !mIsPayButtonEnabled) {
                enablePayButton(true);
            }
        }
    }

    private void enablePayButton(boolean isEnable) {
        if (isEnable) {
            mCheckoutButtonTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_pay_button));
            mCheckoutButtonBackground.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_pay_button_background));
            mIsPayButtonEnabled = true;
        } else {
            mCheckoutButtonTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_pay_button_disable));
            mCheckoutButtonBackground.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_pay_button_disable_background));
            mIsPayButtonEnabled = false;
        }
    }

    private void updateUserSelectedAddress(ConciergeUserAddress address, boolean shouldUpdateOnlineBasket) {
        mUserAddress = address;
        mDeliveryAddress.set(address.getAddress());
        if (TextUtils.isEmpty(mUserAddress.getCountryCode()) ||
                TextUtils.isEmpty(mUserAddress.getPhone())) {
            mUserAddress.setCountryCode(SharedPrefUtils.getString(mContext, SharedPrefUtils.COUNTRY_CODE));
            mUserAddress.setPhone(SharedPrefUtils.getPhone(mContext));
        }

        if (shouldUpdateOnlineBasket) {
            ConciergeUpdateBasketRequest request = new ConciergeUpdateBasketRequest();
            request.setDeliveryAddress(mUserAddress);
            updateOnlineBasket(request,false);
        }
    }

    private void calculateTotalPrice() {
        mSubTotal.set(ConciergeHelper.getDisplayPrice(mContext, mCheckoutData.getCartModel().getTotalPrice()));

        double grandTotal;

        if (mCheckoutData.getCartModel().getConciergeCoupon() != null) {
            grandTotal = ConciergeCartHelper.minusPrice(mCheckoutData.getCartModel().getTotalPrice(), mCheckoutData.getCartModel().getConciergeCoupon().getDiscountPrice() );
        } else {
            grandTotal = ConciergeCartHelper.minusPrice(mCheckoutData.getCartModel().getTotalPrice(), 0f );
        }

        if (grandTotal <= 0) {
            grandTotal = 0;
        }

        if (mCheckoutData.getCartModel().getConciergeCoupon() != null &&
                mCheckoutData.getCartModel().getConciergeCoupon().getDeliveryInfo() != null &&
            mCheckoutData.getCartModel().getConciergeCoupon().getDeliveryInfo().getDeliveryFee() == 0) {
            grandTotal = ConciergeCartHelper.addPrice(grandTotal, 0f);
        } else {
            grandTotal = ConciergeCartHelper.addPrice(grandTotal, mCheckoutData.getDeliveryCharge());
        }

        grandTotal = ConciergeCartHelper.addPrice(grandTotal, mCheckoutData.getContainerCharge());

        if (mIsWalletAvailable.get() && mDataManager.getWalletBalance() > 0) {
            if (mIsWalletApplied.get()) {
                double finalTotal = ConciergeCartHelper.minusPrice(grandTotal, mDataManager.getWalletBalance());
                double remainWalletBalance = ConciergeCartHelper.minusPrice(mDataManager.getWalletBalance(), grandTotal);
                if (finalTotal < 0) {
                    finalTotal = 0;
                }
                if (remainWalletBalance < 0) {
                    remainWalletBalance = 0;
                }
//            Timber.i("grandTotal: " + grandTotal + ", wallet: " + mDataManager.getWalletBalance() + ", finalTotal: " + finalTotal);
                mGrandTotal.set(ConciergeHelper.getDisplayPrice(mContext, finalTotal));
                updateWalletBalanceDisplay(remainWalletBalance);
                double appliedWallet = ConciergeCartHelper.minusPrice(mDataManager.getWalletBalance(), remainWalletBalance);
                if (appliedWallet < 0) {
                    appliedWallet = 0;
                }
                mWalletValue.set(ConciergeHelper.getDisplayPrice(mContext, appliedWallet));
            } else {
                mGrandTotal.set(ConciergeHelper.getDisplayPrice(mContext, grandTotal));
                updateWalletBalanceDisplay(mDataManager.getWalletBalance());
                mWalletValue.set(ConciergeHelper.getDisplayPrice(mContext, 0));
            }
        } else {
            mWalletValue.set(ConciergeHelper.getDisplayPrice(mContext, 0));
            mGrandTotal.set(ConciergeHelper.getDisplayPrice(mContext, grandTotal));
        }
    }

    public void onDeliveryAddressClicked() {
        ((AbsBaseActivity) mContext).startActivityForResult(new ConciergeMyAddressIntent(mContext,
                        true,
//                        mIsSlotProvince ? CitySelectionType.PROVINCE : CitySelectionType.PHNOM_PENH,
                        null,
                        mConciergeUserAddress.getId()),
                new ResultCallback() {
                    @Override
                    public void onActivityResultSuccess(int resultCode, Intent data) {
                        ConciergeUserAddress selectedAddress = data.getParcelableExtra(ConciergeCurrentLocationIntent.USER_CURRENT_ADDRESS);
                        Timber.i("Selected address: " + new Gson().toJson(selectedAddress));
                        if (selectedAddress != null) {
                            if (!TextUtils.equals(mConciergeUserAddress.getId(), selectedAddress.getId()) ||
                                    selectedAddress.getHasUpdateProperty()) {
                                mConciergeUserAddress = selectedAddress;
                                mUserAddress = selectedAddress;
                                bindDeliveryAddress(mConciergeUserAddress);
                                requestDeliveryFee();
                                //Request update selected address into basket silently
                                ConciergeUpdateBasketRequest request = new ConciergeUpdateBasketRequest();
                                request.setDeliveryAddress(mConciergeUserAddress);
                                updateOnlineBasket(request,false);
                                if (mListener != null) {
                                    mListener.addMarker(mConciergeUserAddress.getLatitude(), mConciergeUserAddress.getLongitude());
                                }
                            }
                        }
                    }
                });
    }

    public void onPayClicked() {
        if (!mIsPayButtonEnabled) {
            return;
        }

        if (!NetworkStateUtil.isNetworkAvailable(mContext)) {
            showSnackBar(mContext.getString(R.string.error_internet_connection_description), true);
            return;
        }

        if (ApplicationHelper.isInVisitorMode(getContext())) {
            mListener.onRequireLogin();
        } else {
            if (MainApplication.isInExpressMode()) {
                if (ConciergeHelper.isValidAddressForExpressOrder(mUserAddress)) {
                    showLoading(true);
                    processOrder(null);
                } else {
                    mListener.showOrderErrorPopup(mContext.getString(R.string.shop_checkout_alert_selected_address_out_of_range), null);
                }
            } else {
                // Validate slot selection and address selection before allowing user to proceed with
                // the order. Normally this condition should already be handled on server.
//                CitySelectionType slotSelectionType = mIsSlotProvince ? CitySelectionType.PROVINCE : CitySelectionType.PHNOM_PENH;
//                if (slotSelectionType != CitySelectionType.fromValue(mUserAddress.getCity())) {
//                    mListener.showOrderErrorPopup(mContext.getString(R.string.shop_checkout_error_slot_not_match_address), null);
//                } else {
                    showLoading(true);
                    processOrder(null);
//                }
            }
        }
    }

    private void processOrder(String receiptUrl) {
        Timber.i("processOrder: " + receiptUrl);
        if (isUserSelectToPayWithABAPayWay()) {
            processOrderWithABA();
        } else {
            Observable<Response<ConciergeOrderResponse>> observable = mDataManager.getRequestOrderWithCash(mShopDeliveryTimeSelection,
                    mOOSOption != null ? mOOSOption.getValue() : null,
                    mCheckoutData.getCartModel().getCouponId(),
                    mPassedDeliveryInstruction,
                    mUserAddress,
                    mIsWalletApplied.get());
            ResponseObserverHelper<Response<ConciergeOrderResponse>> helper = new ResponseObserverHelper<>(getContext(), observable);
            addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeOrderResponse>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    Timber.i("onFail: " + ex.getMessage());
                    hideLoading();
                    showSnackBar(ex.getMessage(), true);
                    FlurryHelper.logEvent(mContext, FlurryHelper.ORDER_FAILED);
                }

                @Override
                public void onComplete(Response<ConciergeOrderResponse> result) {
                    Timber.i("onComplete: " + new Gson().toJson(result.body()));
                    if (result.body() != null && result.body().isError()) {
                        FlurryHelper.logEvent(mContext, FlurryHelper.ORDER_FAILED);
                        handleOnOrderFailed(result.body());
                    } else {
                        clearCard(new OnCallbackListener<Response<Object>>() {
                            @Override
                            public void onFail(ErrorThrowable ex) {
                                onOrderWithCashSuccess();
                            }

                            @Override
                            public void onComplete(Response<Object> result) {
                                onOrderWithCashSuccess();
                            }
                        });
                    }
                }
            }));
        }
    }

    private void onOrderWithCashSuccess() {
        hideLoading();
        if (mListener != null) {
            mListener.onOrderSuccess();
        }
    }

    private void handleOnOrderFailed(ConciergeOrderResponse responseCustomErrorData) {
        //Check the error type
        List<CustomErrorDataType> errorTypes = responseCustomErrorData.getErrorTypes();
        Timber.i("errorTypes: " + new Gson().toJson(errorTypes));
        boolean hasTimeslotError = false;
        boolean hasItemError = false;
        boolean hasNoItemAvailableError = false;
        boolean hasWalletAmountChangedError = false;
        for (CustomErrorDataType errorType : errorTypes) {
            if (errorType == CustomErrorDataType.TIMESLOT_NOT_AVAILABLE) {
                hasTimeslotError = true;
            }

            if (errorType == CustomErrorDataType.NO_ITEM_AVAILABLE) {
                hasNoItemAvailableError = true;
            }

            if (errorType == CustomErrorDataType.ITEM_CHANGED ||
                    errorType == CustomErrorDataType.ITEM_REMOVED ||
                    errorType == CustomErrorDataType.NO_ITEM_AVAILABLE) {
                hasItemError = true;
            }

            if (errorType == CustomErrorDataType.WALLET_AMOUNT_CHANGED) {
                hasWalletAmountChangedError = true;
            }
        }

        if (hasNoItemAvailableError) {
            //Basket item has all been removed. So need to clear local basket
            Observable<Response<Object>> observable = ConciergeHelper.getClearLocalBasketService(mContext);
            ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mContext, observable);
            addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    hideLoading();
                    showSnackBar(ex.getMessage(), true);
                }

                @Override
                public void onComplete(Response<Object> result) {
                    hideLoading();
                    mListener.showOrderErrorPopup(responseCustomErrorData.getErrorMessage(), view -> {
                        //Broadcast as success order to review checkout screen so as to close itself.
                        Intent intent = new Intent(AbsSupportABAPaymentFragment.ORDER_SUCCESS_EVENT);
                        SendBroadCastHelper.verifyAndSendBroadCast(mContext, intent);
                        ((AppCompatActivity) mContext).finish();
                    });
                }
            }));
        } else {
            final boolean finalHasWalletAmountChangedError = hasWalletAmountChangedError;
            if (responseCustomErrorData.getBasket() != null) {
                final boolean shouldRedirectBackToReviewCheckoutScreen = hasTimeslotError || hasItemError;
                Observable<Response<ConciergeCartModel>> observable = mDataManager.updateLocalBasket(responseCustomErrorData.getBasket());
                ResponseObserverHelper<Response<ConciergeCartModel>> helper = new ResponseObserverHelper<>(getContext(), observable);
                addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeCartModel>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        hideLoading();
                        showSnackBar(ex.getMessage(), true);
                    }

                    @Override
                    public void onComplete(Response<ConciergeCartModel> result) {
                        reloadData(finalHasWalletAmountChangedError);
                        hideLoading();
                        mListener.showOrderErrorPopup(responseCustomErrorData.getErrorMessage(), view -> {
                            if (shouldRedirectBackToReviewCheckoutScreen) {
                                Intent intent = new Intent();
                                intent.putExtra(ConciergeReviewCheckoutIntent.SHOULD_RELOAD_DATA, true);
                                ((AppCompatActivity) mContext).setResult(AppCompatActivity.RESULT_OK, intent);
                                ((AppCompatActivity) mContext).finish();
                            }
                        });
                    }
                }));
            } else {
                hideLoading();
                mListener.showOrderErrorPopup(responseCustomErrorData.getErrorMessage(), null);
            }
        }
    }

    private boolean isUserSelectToPayWithABAPayWay() {
        if (mPaymentType.get() == ConciergePaymentMethod.ONLINE_METHOD) {
            ConciergeOnlinePaymentOption conciergeOnlinePaymentOption = ConciergeOnlinePaymentOption.fromValue(mSelectedOnlinePaymentAccount.getProvider());
            return conciergeOnlinePaymentOption == ConciergeOnlinePaymentOption.ABA_PAY ||
                    conciergeOnlinePaymentOption == ConciergeOnlinePaymentOption.CARD ||
                    conciergeOnlinePaymentOption == ConciergeOnlinePaymentOption.KHQR;
        }

        return false;
    }

    private void processOrderWithABA() {
        ConciergeOnlinePaymentOption paymentOption = ConciergeOnlinePaymentOption.fromValue(mSelectedOnlinePaymentAccount.getProvider());
        Timber.i("processOrderWithABA: " + paymentOption);
        if (paymentOption == ConciergeOnlinePaymentOption.ABA_PAY && !ABAPayWayHelper.isABAMobileAppInstalledOnDevice(mContext)) {
            hideLoading();
            mListener.showOrderErrorPopup(mContext.getString(R.string.shop_checkout_aba_app_not_install), null);
            return;
        }

        Observable<Response<ConciergeOrderResponseWithABAPayWay>> observable = mDataManager.getRequestOrderWithABA(mShopDeliveryTimeSelection,
                mOOSOption != null ? mOOSOption.getValue() : null,
                mCheckoutData.getCartModel().getCouponId(),
                mPassedDeliveryInstruction,
                mUserAddress,
                mSelectedOnlinePaymentAccount,
                mIsWalletApplied.get());
        ResponseObserverHelper<Response<ConciergeOrderResponseWithABAPayWay>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeOrderResponseWithABAPayWay>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.i("onFail: " + ex.getMessage());
                hideLoading();
                String errorMessage = ex.getMessage();
                if (ex.getCode() == ErrorThrowable.CONNECTION_TIME_OUT) {
                    errorMessage = mContext.getString(R.string.shop_checkout_error_aba);
                }
                showSnackBar(errorMessage, true);
                FlurryHelper.logEvent(mContext, FlurryHelper.ORDER_FAILED);
            }

            @Override
            public void onComplete(Response<ConciergeOrderResponseWithABAPayWay> result) {
                Timber.i("onComplete: " + new Gson().toJson(result.body()));
                if (result.body() != null && result.body().isError()) {
                    FlurryHelper.logEvent(mContext, FlurryHelper.ORDER_FAILED);
                    handleOnOrderFailed(result.body());
                } else {
                    hideLoading();
                    handleABAPayment(paymentOption, result.body(), false);
                }
            }
        }));
    }

    public void requestToClearBasket() {
        showLoading(true);
        Observable<Response<SupportConciergeCustomErrorResponse>> observable = mDataManager.clearBasket();
        ResponseObserverHelper<Response<SupportConciergeCustomErrorResponse>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<SupportConciergeCustomErrorResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<SupportConciergeCustomErrorResponse> result) {
                if (result.body() != null && result.body().isError()) {
                    hideLoading();
                    if (result.body().getDataType() == SupportConciergeCustomErrorResponse.ResponseDataType.PAYMENT_IN_PROCESS) {
                        if (mListener != null) {
                            mListener.onBasketModificationError(result.body(), result.body().getErrorMessage());
                        }
                    } else {
                        showSnackBar(result.body().getErrorMessage(), true);
                    }
                } else {
                    hideLoading();
                    //Consider as order success
                    if (mListener != null) {
                        mListener.onOrderSuccess();
                    }
                }
            }
        }));
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.i("onResume: mIsRequestOpenABAAppOnDeviceForABAPay: " + mIsRequestedOpenABAAppOnDeviceForABAPay +
                ", mIsAlreadyReceivedABAPushbackResultForABAPay: " + mIsAlreadyReceivedABAPushbackResultForABAPay);
        if (mIsRequestedOpenABAAppOnDeviceForABAPay && !mIsAlreadyReceivedABAPushbackResultForABAPay) {
            mIsRequestedOpenABAAppOnDeviceForABAPay = false;
            verifyOrderPaymentStatus(true);
        }
    }

    private void verifyOrderPaymentStatus(boolean isABAPayOption) {
        Timber.i("verifyOrderPaymentStatus for " + (isABAPayOption ? "ABA Pay" : "ABA Card"));
        showLoading(true);
        Observable<Response<ConciergeVerifyPaymentWithABAResponse>> observable = mDataManager.verifyOrderPaymentStatus();
        ResponseObserverHelper<Response<ConciergeVerifyPaymentWithABAResponse>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeVerifyPaymentWithABAResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showError(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<ConciergeVerifyPaymentWithABAResponse> result) {
                if (result.body() != null && result.body().isPaid()) {
                    //Mean the the payment is success.
                    clearCard(new OnCallbackListener<Response<Object>>() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                            hideLoading();
                            if (mListener != null) {
                                mListener.onABAPaymentIsPaidOnVerificationChecking();
                            }
                        }

                        @Override
                        public void onComplete(Response<Object> result) {
                            hideLoading();
                            if (mListener != null) {
                                mListener.onABAPaymentIsPaidOnVerificationChecking();
                            }
                        }
                    });
                } else {
                    if (mIsWalletAvailable.get() && mDataManager.getWalletBalance() > 0) {
                        calculateTotalPrice();
                    } else if (!mAlreadyAppliedWalletBefore.get() && mCheckoutData.getCartModel().isWalletApplied() && mDataManager.getWalletBalance() > 0) {
                        mAlreadyAppliedWalletBefore.set(true);
                        mIsWalletAvailable.set(true);
                        calculateTotalPrice();
                    }
                    hideLoading();
                }
            }
        }));
    }

    private void requestToEnableBasket(OnCallbackListener<Response<SupportConciergeCustomErrorResponse>> callbackListener) {
        Observable<Response<SupportConciergeCustomErrorResponse>> responseObservable = ConciergeHelper.enableBasket(mContext, mDataManager.getApiService());
        BaseObserverHelper<Response<SupportConciergeCustomErrorResponse>> helper = new BaseObserverHelper<>(mContext, responseObservable);
        helper.execute(callbackListener);
    }

    private void handleABAPayment(ConciergeOnlinePaymentOption paymentOption,
                                  ConciergeOrderResponseWithABAPayWay response,
                                  boolean isFromResumePayment) {
        if (!response.isNeedToPay()) {
            //No need to pay more. This case will happen if user choose wallet to pay and we will handle
            //the same as pay with cash success
            clearCard(new OnCallbackListener<Response<Object>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    onOrderWithCashSuccess();
                }

                @Override
                public void onComplete(Response<Object> result) {
                    onOrderWithCashSuccess();
                }
            });
            return;
        }

        if (paymentOption == ConciergeOnlinePaymentOption.ABA_PAY) {
            if (!isFromResumePayment || ABAPayWayHelper.isABAMobileAppInstalledOnDevice(mContext)) {
                ABAPayWayHelper.openABAPayDeepLinking(mContext, response.getPaymentForm().getABAPayDeepLink());
                new Handler().postDelayed(() -> mIsRequestedOpenABAAppOnDeviceForABAPay = true, 1000);
            } else {
                requestToEnableBasket(new OnCallbackListener<Response<SupportConciergeCustomErrorResponse>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        hideLoading();
                        showSnackBar(ex.getMessage(), true);
                    }

                    @Override
                    public void onComplete(Response<SupportConciergeCustomErrorResponse> result) {
                        hideLoading();
                        mListener.showOrderErrorPopup(mContext.getString(R.string.shop_checkout_aba_app_not_install), null);
                    }
                });
            }
        } else if (paymentOption == ConciergeOnlinePaymentOption.CARD || paymentOption == ConciergeOnlinePaymentOption.KHQR) {
            ((AbsBaseActivity) mContext).startActivityForResult(new PayWithCardIntent(mContext,
                            response.getPaymentForm().getCheckoutUrl(),
                            response.getPaymentForm().getSuccessUrl()),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            Timber.i("onActivityResultSuccess");
                            boolean isPaymentSuccess = data.getBooleanExtra(AbsSupportABAPaymentFragment.IS_ABA_PAYMENT_SUCCESS,
                                    false);
                            Timber.i("isPaymentSuccess: " + isPaymentSuccess);
                            String message = data.getStringExtra(AbsSupportABAPaymentFragment.ABA_PAYMENT_PUSHBACK_MESSAGE);
                            handleABAPushBackResult(isPaymentSuccess, message, false);
                        }

                        @Override
                        public void onActivityResultFail() {
                            Timber.i("onActivityResultFail");
                            verifyOrderPaymentStatus(false);
                        }
                    });
        }
    }

    private void handleABAPushBackResult(boolean isPaymentSuccess, String message, boolean isABAPayOption) {
        if (isPaymentSuccess) {
            showLoading(true);
            clearCard(new OnCallbackListener<Response<Object>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    Timber.e("Clear card failed: " + ex.getMessage());
                    if (isABAPayOption) {
                        mIsAlreadyReceivedABAPushbackResultForABAPay = false;
                    }
                    hideLoading();
                    ((AppCompatActivity) mContext).finish();
                }

                @Override
                public void onComplete(Response<Object> result) {
                    if (isABAPayOption) {
                        mIsAlreadyReceivedABAPushbackResultForABAPay = false;
                    }
                    hideLoading();
                    ((AppCompatActivity) mContext).finish();
                }
            });
        } else {
            if (isABAPayOption) {
                mIsAlreadyReceivedABAPushbackResultForABAPay = false;
            }
            if (mListener != null) {
                mListener.showOrderErrorPopup(message, null);
            }
        }
    }

    private void clearCard(OnCallbackListener<Response<Object>> callbackListener) {
        Observable<Response<Object>> observable = mDataManager.clearCartData();
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(callbackListener));
    }

    public void onItemOutOfStockOptionSelected(boolean isCancelItemClicked) {
        if (isCancelItemClicked && mOOSOption != ConciergeItemOutOfStockAlternativeOption.CANCEL_ITEM) {
            mOOSOption = ConciergeItemOutOfStockAlternativeOption.CANCEL_ITEM;
            updateOOSOptionSelection();
            updateSelectedOOSOptionToRemoteBasket();
        } else if (mOOSOption != ConciergeItemOutOfStockAlternativeOption.BE_CONTACTED) {
            mOOSOption = ConciergeItemOutOfStockAlternativeOption.BE_CONTACTED;
            updateOOSOptionSelection();
            updateSelectedOOSOptionToRemoteBasket();
        }
    }

    private void updateSelectedOOSOptionToRemoteBasket() {
        if (mOOSOption != null) {
            mCheckoutData.getCartModel().setOOSOption(mOOSOption.getValue());
            ConciergeUpdateBasketRequest request = new ConciergeUpdateBasketRequest();
            request.setOOSOption(mOOSOption.getValue());
            updateOnlineBasket(request,false);
        }
    }

    private void updateApplyWalletStatusToRemoteBasket() {
        mCheckoutData.getCartModel().setWalletApplied(mIsWalletApplied.get());
        ConciergeUpdateBasketRequest request = new ConciergeUpdateBasketRequest();
        request.setAppliedWallet(mIsWalletApplied.get());
        updateOnlineBasket(request,false);
    }

    public interface ConciergeCheckoutFragmentViewModelCallback {
        void onRequestDataSuccess(ConciergeCheckoutData data);

        void onOrderSuccess();

        void onRefreshData(ConciergeCheckoutData data);

        void onAllItemRemovedFromCart();

        void onRequireLogin();

        void onBasketModificationError(SupportConciergeCustomErrorResponse response, String errorMessage);

        void addMarker(double lat, double lng);

        void onABAPaymentIsPaidOnVerificationChecking();

        void showOrderErrorPopup(String errorMessage, View.OnClickListener okClickListener);
    }

    @BindingAdapter("setSelectionFontStyle")
    public static void setSelectionFontStyle(TextView textView, boolean isSelected) {
        if (isSelected) {
            textView.setTypeface(ResourcesCompat.getFont(textView.getContext(), R.font.uicons_solid_rounded));
        } else {
            textView.setTypeface(ResourcesCompat.getFont(textView.getContext(), R.font.uicons_regular_rounded));
        }
    }

    @BindingAdapter("setPaymentOptionBackground")
    public static void setPaymentOptionBackground(ViewGroup viewGroup, boolean isSelected) {
        viewGroup.setBackground(isSelected ? ContextCompat.getDrawable(viewGroup.getContext(),
                R.drawable.concierge_checkout_selected_payment_option_background) : null);
    }

    @BindingAdapter({"optionTitle", "optionDescription", "optionIcon", "isSelected"})
    public static void setItemOutOfStockAlternativeOption(ViewGroup optionContainer,
                                                          TextView optionTitle,
                                                          TextView optionDescription,
                                                          ImageView optionIcon,
                                                          boolean isSelected) {
        if (isSelected) {
            optionTitle.setTextColor(AttributeConverter.convertAttrToColor(optionTitle.getContext(), R.attr.shop_checkout_cancel_contacted_option_selected_title));
            optionDescription.setTextColor(AttributeConverter.convertAttrToColor(optionTitle.getContext(), R.attr.shop_checkout_cancel_contacted_option_selected_description));
            if (optionContainer.getBackground() != null) {
                DrawableCompat.setTint(optionContainer.getBackground(), AttributeConverter.convertAttrToColor(optionContainer.getContext(), R.attr.common_primary_color));
            }
            optionIcon.setImageDrawable(ContextCompat.getDrawable(optionContainer.getContext(), R.drawable.ic_baseline_radio_button_checked_24));
            DrawableCompat.setTint(optionIcon.getDrawable(), AttributeConverter.convertAttrToColor(optionContainer.getContext(), R.attr.shop_checkout_selected_time_slot_icon));
        } else {
            optionTitle.setTextColor(AttributeConverter.convertAttrToColor(optionTitle.getContext(), R.attr.shop_checkout_cancel_contacted_option_unselected_title));
            optionDescription.setTextColor(AttributeConverter.convertAttrToColor(optionTitle.getContext(), R.attr.shop_checkout_cancel_contacted_option_unselected_description));
            if (optionContainer.getBackground() != null) {
                DrawableCompat.setTint(optionContainer.getBackground(), AttributeConverter.convertAttrToColor(optionContainer.getContext(), R.attr.shop_checkout_not_select_time_slot_button_background));
            }
            optionIcon.setImageDrawable(ContextCompat.getDrawable(optionContainer.getContext(), R.drawable.ic_baseline_radio_button_unchecked_24));
            DrawableCompat.setTint(optionIcon.getDrawable(), AttributeConverter.convertAttrToColor(optionContainer.getContext(), R.attr.shop_checkout_not_select_time_slot_icon));
        }
    }
}
