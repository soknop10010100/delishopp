package com.proapp.sompom.viewmodel;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.emun.ConciergeSortItemOption;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.ConciergeSubCategoryDetailDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeCartBottomSheetViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 5/4522.
 */
public class ConciergeSubCategoryDetailFragmentViewModel extends AbsSupportViewItemByViewModel<ConciergeSubCategoryDetailDataManager, ConciergeSubCategoryDetailFragmentViewModel.ConciergeSubCategoryDetailFragmentViewModelListener> {

    public ConciergeSubCategoryDetailFragmentViewModel(Context context,
                                                       ConciergeSubCategoryDetailDataManager dataManager,
                                                       ConciergeCartBottomSheetViewModel cartViewModel,
                                                       ConciergeSubCategoryDetailFragmentViewModelListener listener) {
        super(context, cartViewModel, listener, dataManager);
        mDataManager = dataManager;
        if (getInternalAppliedSearchCriteriaLoadingViewModel() != null) {
            getInternalAppliedSearchCriteriaLoadingViewModel().setListener(() -> {
                //Item list retry
                getData(false, true, false);
            });
        }
    }

    public ConciergeCartBottomSheetViewModel getCartViewModel() {
        return mCartViewModel;
    }

    public void onBackPress() {
        if (getContext() instanceof AppCompatActivity) {
            ((AppCompatActivity) getContext()).onBackPressed();
        }
    }

    @Override
    public void onRetryClick() {
        //Main screen retry
        getData(true, false, false);
    }

    @Override
    public void onApplySort(ConciergeSortItemOption option) {
        Timber.i("onApplySort: " + option);
        super.onApplySort(option);
        getData(false, true, false);
    }

    public void onGetMainData() {
        getData(true, false, false);
    }

    public void loadMoreItem() {
        getData(false, false, true);
    }

    private void getData(boolean isFirstLoading,
                         boolean isAppliedSortCriteria,
                         boolean isLoadMore) {
        if (isFirstLoading) {
            showLoading();
        } else if (isAppliedSortCriteria) {
            showAppliedSearchCriteriaLoadingView();
        }

        Observable<Response<List<ConciergeMenuItem>>> observable = mDataManager.getSubcategoryItem(isLoadMore);
        BaseObserverHelper<Response<List<ConciergeMenuItem>>> helper = new BaseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<ConciergeMenuItem>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                if (isFirstLoading) {
                    showError(ex.getMessage(), true);
                } else {
                    if (isAppliedSortCriteria) {
                        getInternalAppliedSearchCriteriaLoadingViewModel().showError(ex.getMessage(), true);
                    } else {
                        if (!isLoadMore) {
                            getInternalAppliedSearchCriteriaLoadingViewModel().showError(ex.getMessage(), true);
                        } else {
                            showSnackBar(ex.getMessage(), true);
                            if (mListener != null) {
                                mListener.onLoadMoreItemFailed();
                            }
                        }
                    }
                }
            }

            @Override
            public void onComplete(Response<List<ConciergeMenuItem>> result) {
                hideLoading();
                if (!mIsFirstDataLoadSuccess) {
                    mIsFirstDataLoadSuccess = true;
                    if (mListener != null) {
                        mListener.onMainDataLoadSuccess(mDataManager.getSubCategory().getTitle());
                    }
                }

                if (!isLoadMore && result.body().isEmpty()) {
                    getInternalAppliedSearchCriteriaLoadingViewModel().showError(mContext.getString(R.string.error_product_not_found),
                            false);
                }

                if (mListener != null) {
                    mListener.onLoadItemSuccess(result.body(), !isLoadMore, isLoadMore, mDataManager.isCanLoadMore());
                }
            }
        }));
    }

    public interface ConciergeSubCategoryDetailFragmentViewModelListener extends AbsSupportViewItemByViewModelListener {

        void onLoadItemSuccess(List<ConciergeMenuItem> itemList, boolean isRefreshMode, boolean isLoadMore, boolean isCanLoadMore);

        void onLoadMoreItemFailed();

        void onMainDataLoadSuccess(String subcategoryTitle);
    }
}
