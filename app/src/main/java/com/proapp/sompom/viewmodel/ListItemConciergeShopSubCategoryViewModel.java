package com.proapp.sompom.viewmodel;

import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.model.concierge.ConciergeSubCategory;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 26/4/22.
 */
public class ListItemConciergeShopSubCategoryViewModel {

    private final ObservableField<String> mTitle = new ObservableField<>();
    private final ObservableBoolean mIsExpanded = new ObservableBoolean();
    private final ObservableBoolean mShouldApplySectionExpandAnimation = new ObservableBoolean();
    private final ConciergeSubCategory mSubCategory;
    private final ListItemConciergeShopSubCategoryViewModelListener mListener;
    private final ObservableField<String> mItemNote = new ObservableField<>();

    public ListItemConciergeShopSubCategoryViewModel(ConciergeSubCategory subCategory,
                                                     int position,
                                                     ListItemConciergeShopSubCategoryViewModelListener listener) {
        mSubCategory = subCategory;
        mItemNote.set(mSubCategory.getTitle() + " - " + position);
        mListener = listener;
        mTitle.set(subCategory.getTitle());
        mIsExpanded.set(subCategory.isSectionExpended());
    }

    public ObservableField<String> getItemNote() {
        return mItemNote;
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    public ObservableBoolean getIsExpanded() {
        return mIsExpanded;
    }

    public ObservableBoolean getShouldApplySectionExpandAnimation() {
        return mShouldApplySectionExpandAnimation;
    }

    public void onSubCategoryTitleClicked(boolean invokeCallback, boolean isViewClicked) {
        if (!mIsExpanded.get()) {
            mShouldApplySectionExpandAnimation.set(isViewClicked);
            mIsExpanded.set(!mIsExpanded.get());
            mSubCategory.setSectionExpended(mIsExpanded.get());
            if (invokeCallback) {
                mListener.expandedSubCategorySection(mSubCategory);
            }
        }
    }

    public void resetExpandSection() {
        mShouldApplySectionExpandAnimation.set(false);
        mIsExpanded.set(false);
        mSubCategory.setSectionExpended(mIsExpanded.get());
    }

    public ConciergeSubCategory getSubCategory() {
        return mSubCategory;
    }

    public interface ListItemConciergeShopSubCategoryViewModelListener {
        void expandedSubCategorySection(ConciergeSubCategory subCategory);
    }

    @BindingAdapter({"recyclerView", "isExpanded", "shouldApplySectionExpandAnimation", "itemNote"})
    public static void setExpandOrHideItemList(ViewGroup rootView,
                                               RecyclerView recyclerView,
                                               boolean isExpanded,
                                               boolean shouldApplySectionExpandAnimation,
                                               String itemNote) {
//        Timber.i("itemNote: " + itemNote +
//                ", isExpanded: " + isExpanded +
//                ", shouldApplySectionExpandAnimation: " + shouldApplySectionExpandAnimation +
//                ", root type: " + rootView.getClass().getSimpleName());
        recyclerView.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        if (shouldApplySectionExpandAnimation) {
            if (isExpanded) {
                AnimationHelper.expand(recyclerView);
            } else {
                recyclerView.setVisibility(View.GONE);
            }
        } else {
            recyclerView.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        }
    }
}
