package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.SupportCustomErrorResponse2;
import com.proapp.sompom.model.emun.ChangePasswordActionType;
import com.proapp.sompom.model.request.ChangePasswordRequestBody;
import com.proapp.sompom.model.result.BaseSupportCustomErrorResponse;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ChangePasswordDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.Objects;
import java.util.regex.Pattern;

import io.reactivex.Observable;
import retrofit2.Response;

public class ChangePasswordViewModel extends AbsLoadingViewModel {

    /*
    - at lease 1 lower letter
    - at lease 1 upper letter
    - at lease 8-16 characters
    - at lease 1 number
     */
    private static final String PASSWORD_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?!.*  )(?=.{8,16}).*";

    private ObservableField<String> mCurrentPassword = new ObservableField<>();
    private ObservableField<String> mPassword = new ObservableField<>();
    private ObservableField<String> mCurrentPasswordError = new ObservableField<>();
    private ObservableField<String> mPasswordError = new ObservableField<>();
    private ObservableField<String> mConfirmPassword = new ObservableField<>();
    private ObservableField<String> mConfirmPasswordError = new ObservableField<>();
    private ObservableInt mOldPasswordVisibility = new ObservableInt(View.GONE);
    private ChangePasswordDataManager mDataManager;
    private ChangePasswordViewModelListener mListener;
    private Pattern mPasswordPattern = Pattern.compile(PASSWORD_PATTERN);

    public ChangePasswordViewModel(Context context,
                                   ChangePasswordDataManager dataManager,
                                   ChangePasswordViewModelListener listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
        mOldPasswordVisibility.set(dataManager.getActionType() == ChangePasswordActionType.CHANGE_PASSWORD ? View.VISIBLE : View.GONE);
    }

    public ObservableInt getOldPasswordVisibility() {
        return mOldPasswordVisibility;
    }

    public ObservableField<String> getCurrentPassword() {
        return mCurrentPassword;
    }

    public ObservableField<String> getConfirmPassword() {
        return mConfirmPassword;
    }

    public ObservableField<String> getPassword() {
        return mPassword;
    }

    public ObservableField<String> getPasswordError() {
        return mPasswordError;
    }

    public ObservableField<String> getConfirmPasswordError() {
        return mConfirmPasswordError;
    }

    public ObservableField<String> getCurrentPasswordError() {
        return mCurrentPasswordError;
    }

    public boolean isReadyToChangePassword() {
        return isAllFieldValid() && isPasswordMatch() && isPasswordCorrect();
    }

    private boolean isPasswordCorrect() {
        if (!mPasswordPattern.matcher(mPassword.get()).matches()) {
            if (mListener != null) {
                mListener.onChangePasswordFailed(getContext().getString(R.string.change_password_error_password_format));
            }
            return false;
        }

        return true;
    }

    private boolean isAllFieldValid() {
        boolean valid = true;

        if (isOldPasswordRequire()) {
            if (TextUtils.isEmpty(mCurrentPassword.get())) {
                mCurrentPasswordError.set(mDataManager.getContext().getString(R.string.login_error_field_required));
                valid = false;
            } else {
                mCurrentPasswordError.set(null);
            }
        }

        if (TextUtils.isEmpty(mPassword.get())) {
            mPasswordError.set(mDataManager.getContext().getString(R.string.login_error_field_required));
            valid = false;
        } else {
            mPasswordError.set(null);
        }

        if (TextUtils.isEmpty(mConfirmPassword.get())) {
            mConfirmPasswordError.set(mDataManager.getContext().getString(R.string.login_error_field_required));
            valid = false;
        } else {
            mConfirmPasswordError.set(null);
        }

        return valid;
    }

    private boolean isOldPasswordRequire() {
        return mDataManager.getActionType() == ChangePasswordActionType.CHANGE_PASSWORD;
    }

    private boolean isPasswordMatch() {
        boolean match = true;

        if (!Objects.requireNonNull(mPassword.get()).matches(Objects.requireNonNull(mConfirmPassword.get()))) {
            match = false;
            mConfirmPasswordError.set(mDataManager.mContext.getString(R.string.change_password_and_confirm_didnt_match));
        } else {
            mConfirmPasswordError.set(null);
        }

        return match;
    }

    public void requestChangePassword() {
        if (mDataManager.getActionType() == ChangePasswordActionType.RESET_PASSWORD_VIA_PHONE) {
            requestResetPasswordViaPhoneNumber();
        } else {
            Observable<Response<SupportCustomErrorResponse2>> changeOrResetPasswordService;
            if (mDataManager.getActionType() == ChangePasswordActionType.RESET_PASSWORD) {
                changeOrResetPasswordService = mDataManager.getResetPasswordViaEmailService(mPassword.get());
            } else {
                changeOrResetPasswordService = mDataManager.getChangePasswordService(new ChangePasswordRequestBody(mCurrentPassword.get(),
                        mPassword.get()));
            }
            ResponseObserverHelper<Response<SupportCustomErrorResponse2>> helper = new ResponseObserverHelper<>(mDataManager.mContext,
                    changeOrResetPasswordService);
            addDisposable(helper.execute(new OnCallbackListener<Response<SupportCustomErrorResponse2>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    if (mListener != null) {
                        mListener.onChangePasswordFailed(ex.getMessage());
                    }
                }

                @Override
                public void onComplete(Response<SupportCustomErrorResponse2> result) {
                    if (!result.body().isError()) {
                        if (mListener != null) {
                            mListener.onChangePasswordSuccess(mDataManager.getActionType());
                        }
                    } else {
                        if (mListener != null) {
                            mListener.onChangePasswordFailed(result.body().getErrorMessage());
                        }
                    }
                }
            }));
        }
    }

    public void requestResetPasswordViaPhoneNumber() {
        Observable<Response<BaseSupportCustomErrorResponse>> changeOrResetPasswordService = mDataManager.getResetPasswordViaPhoneService(mPassword.get());
        ResponseObserverHelper<Response<BaseSupportCustomErrorResponse>> helper = new ResponseObserverHelper<>(mDataManager.mContext,
                changeOrResetPasswordService);
        addDisposable(helper.execute(new OnCallbackListener<Response<BaseSupportCustomErrorResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (mListener != null) {
                    mListener.onChangePasswordFailed(ex.getMessage());
                }
            }

            @Override
            public void onComplete(Response<BaseSupportCustomErrorResponse> result) {
                if (result.body().isSuccessful()) {
                    if (mListener != null) {
                        mListener.onChangePasswordSuccess(mDataManager.getActionType());
                    }
                } else {
                    if (mListener != null) {
                        mListener.onChangePasswordFailed(result.body().getMessage());
                    }
                }
            }
        }));
    }

    public interface ChangePasswordViewModelListener {

        void onChangePasswordFailed(String error);

        void onChangePasswordSuccess(ChangePasswordActionType type);
    }
}
