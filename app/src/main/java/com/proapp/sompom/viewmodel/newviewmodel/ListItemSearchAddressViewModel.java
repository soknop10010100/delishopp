package com.proapp.sompom.viewmodel.newviewmodel;

import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 6/11/18.
 */

public class ListItemSearchAddressViewModel extends AbsBaseViewModel {
    private SearchAddressResult mSearchAddressResult;
    private OnItemClickListener<SearchAddressResult> mListener;

    public ListItemSearchAddressViewModel(SearchAddressResult searchAddressResult,
                                          OnItemClickListener<SearchAddressResult> listener) {
        mSearchAddressResult = searchAddressResult;
        mListener = listener;
    }

    public String getPlaceTitle() {
        return mSearchAddressResult.getPlaceTitle();
    }

    public String getAddress() {
        return mSearchAddressResult.getAddress();
    }

    public void onItemClick() {
        if (mListener != null) {
            mListener.onClick(mSearchAddressResult);
        }
    }
}
