package com.proapp.sompom.viewmodel.newviewmodel;

import android.net.Uri;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.example.usermentionable.model.UserMentionable;
import com.example.usermentionable.tokenization.QueryToken;
import com.example.usermentionable.tokenization.interfaces.QueryTokenReceiver;
import com.example.usermentionable.utils.RenderTextAsMentionable;
import com.example.usermentionable.widget.KeyboardInputEditor;
import com.example.usermentionable.widget.RichEditorView;
import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.SoundEffectService;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnEditTextCommentListener;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.request.CommentBody;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.PopUpCommentDataManager;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 11/7/18.
 */

public class LayoutEditTextCommentViewModel extends AbsLoadingViewModel {

    protected final ObservableField<String> mSnackBarErrorMessage = new ObservableField<>();
    public final ObservableField<String> mHashTagToken = new ObservableField<>();
    public final ObservableField<String> mMessage = new ObservableField<>();
    public final ObservableBoolean mIsAddIconAnimation = new ObservableBoolean();
    public final ObservableBoolean mIsShowEmpty = new ObservableBoolean();
    public final ObservableBoolean mIsUpdate = new ObservableBoolean();
    public final ObservableField<List<UserMentionable>> mUserMentionableList = new ObservableField<>();
    public final ObservableBoolean mIsShowListPopupWindow = new ObservableBoolean();
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();

    final String mContentId;
    final PopUpCommentDataManager mDataManager;
    final OnEditTextCommentListener mEditTextCommentListener;
    private int mNumberOfCommentAdd = 0;
    int mUpdateCommentPosition;

    private Comment mUpdateComment;
    private Editable mEditable;
    protected String mPostId;
    private ContentType mContentType;
    private SendNewCommendInterceptorListener mSendNewCommendInterceptorListener;

    LayoutEditTextCommentViewModel(PopUpCommentDataManager dataManager,
                                   String postId,
                                   String id,
                                   ContentType contentType,
                                   OnEditTextCommentListener editTextCommentListener,
                                   SendNewCommendInterceptorListener sendNewCommendInterceptorListener) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mPostId = postId;
        mContentType = contentType;
        mContentId = id;
        mEditTextCommentListener = editTextCommentListener;
        mSendNewCommendInterceptorListener = sendNewCommendInterceptorListener;
        if (dataManager.isInJumpCommentMode()) {
            mPostId = mDataManager.getRedirectionNotificationData().getPostId();
        }
    }

    public ObservableField<String> getSnackBarErrorMessage() {
        return mSnackBarErrorMessage;
    }

    public ObservableField<String> getHashTagToken() {
        return mHashTagToken;
    }

    public int getNumberOfCommentAdd() {
        return mNumberOfCommentAdd;
    }

    public void setNumberOfCommentAdd(int numberOfCommentAdd) {
        mNumberOfCommentAdd = numberOfCommentAdd;
    }

    public void setUpdateComment(Comment updateComment, int position) {
        mUpdateComment = updateComment;
        mUpdateCommentPosition = position;
        mIsUpdate.set(mUpdateComment != null);
    }

    public void showOrHideEmptyCommentLabel(boolean isShow) {
        mIsShowEmpty.set(isShow);
    }

    public void onSendButtonClick() {
        if (mEditable != null && !TextUtils.isEmpty(mEditable.toString().trim())) {
            mMessage.set(mEditable.toString());
            if (getContext() instanceof AbsBaseActivity) {
                ((AbsBaseActivity) getContext()).startLoginWizardActivity(new LoginActivityResultCallback() {
                    @Override
                    public void onActivityResultSuccess(boolean isAlreadyLogin) {
                        String renderText = RenderTextAsMentionable.render(getContext(), mEditable);
                        if (mIsUpdate.get()) {
                            mUpdateComment.setContent(renderText);
                            updateComment(mUpdateComment);
                            mMessage.set("");
                            setUpdateComment(null, mUpdateCommentPosition);
                        } else {
                            Comment comment = new Comment();
                            comment.setContent(renderText);
                            comment.setUser(SharedPrefUtils.getUser(getContext()));
                            comment.setDate(new Date());
                            if (mContentType != null) {
                                comment.setContentType(mContentType.getValue());
                            }
                            comment.setPostId(mPostId);
                            onSendNewComment(comment);
                        }
                    }
                });
            }
        }
    }

    private void onSendNewComment(Comment newComment) {
        if (mSendNewCommendInterceptorListener == null) {
            proceedSendNewComment(newComment);
        } else {
            mSendNewCommendInterceptorListener.onNewCommentPrepareToBeSent(LayoutEditTextCommentViewModel.this,
                    newComment);
        }
    }

    public void proceedSendNewComment(Comment newComment) {
        postComment(newComment);
        if (mEditTextCommentListener != null) {
            mEditTextCommentListener.onSendText(newComment);
        }
    }

    public void updateComment(Comment comment) {
        CommentBody commentBody = new CommentBody();
        commentBody.setMessage(comment.getContent());
        Observable<Response<Comment>> callback = mDataManager.updateComment(comment.getId(), commentBody);
        ResponseObserverHelper<Response<Comment>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<Comment>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mSnackBarErrorMessage.set(null);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    mSnackBarErrorMessage.set(ex.toString());
                } else {
                    mSnackBarErrorMessage.set(getContext().getString(R.string.comment_toast_post_fail));
                }
            }

            @Override
            public void onComplete(Response<Comment> result) {
                if (mEditTextCommentListener != null) {
                    if (result.body() != null) {
                        comment.setMetaPreview(result.body().getMetaPreview());
                    }
                    mEditTextCommentListener.onUpdateCommentSuccess(comment, mUpdateCommentPosition);
                }
            }
        }));
    }

    public void postComment(Comment comment) {
        comment.setPosting(true);
        Observable<Response<Comment>> callback = mDataManager.postComment(mContentId, comment);
        ResponseObserverHelper<Response<Comment>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<Comment>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mSnackBarErrorMessage.set(null);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    mSnackBarErrorMessage.set(ex.toString());
                } else {
                    mSnackBarErrorMessage.set(getContext().getString(R.string.comment_toast_post_fail));
                }
                mEditTextCommentListener.onPostCommentFail();
            }

            @Override
            public void onComplete(Response<Comment> result) {
                showOrHideEmptyCommentLabel(false);
                SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.SEND_MESSAGE);
                if (mEditTextCommentListener != null) {
                    Comment comment1 = result.body();
                    if (comment1 != null) {
                        comment1.setUser(comment.getUser());
                    }
                    mEditTextCommentListener.onPostCommentSuccess(comment1);
                }
                mNumberOfCommentAdd++;
            }
        }));

        mMessage.set("");
        mEditable = null;
    }

    public void onCloseUpdateComment() {
        setUpdateComment(null, mUpdateCommentPosition);
        mMessage.set("");
    }

    public TextWatcher onTextChange() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                mEditable = s;
                if (!TextUtils.isEmpty(s)) {
                    if (!mIsAddIconAnimation.get()) {
                        mIsAddIconAnimation.set(true);
                    }
                } else {
                    if (mIsAddIconAnimation.get()) {
                        mIsAddIconAnimation.set(false);
                    }
                }
            }
        };
    }

    public KeyboardInputEditor.KeyBoardInputCallbackListener onKeyBoardInputCallbackListener() {
        return (inputContentInfo, flags, opts) -> {
            if (getContext() instanceof AbsBaseActivity) {
                ((AbsBaseActivity) getContext()).startLoginWizardActivity(new LoginActivityResultCallback() {
                    @Override
                    public void onActivityResultSuccess(boolean isAlreadyLogin) {
                        Uri uri = inputContentInfo.getLinkUri();
                        if (uri != null) {
                            Media media = new Media();
                            media.setUrl(String.valueOf(uri));
                            media.setType(MediaType.IMAGE);
                            Comment comment = new Comment();
                            comment.setMedia(media);
                            comment.setUser(SharedPrefUtils.getUser(getContext()));
                            if (mContentType != null) {
                                comment.setContentType(mContentType.getValue());
                            }
                            comment.setPostId(mPostId);
                            onSendNewComment(comment);
                        }
                    }
                });
            }
        };
    }

    public QueryTokenReceiver queryTokenReceiverListener() {
        return new QueryTokenReceiver() {
            @Override
            public void onQueryReceived(@NonNull QueryToken queryToken) {
                if (queryToken.getExplicitChar() == '@') {
                    mIsShowListPopupWindow.set(true);
                    getMentionUserList(queryToken.getKeywords());
                } else if (queryToken.getExplicitChar() == '#') {
                    mHashTagToken.set(queryToken.getTokenString());
                } else {
                    mIsShowListPopupWindow.set(false);
                }
            }

            @Override
            public void invalidToken() {
                mIsShowListPopupWindow.set(false);
            }
        };
    }

    private void getMentionUserList(String keyboard) {
        Observable<List<UserMentionable>> observable = mDataManager.getUserMentionList(keyboard);
        BaseObserverHelper<List<UserMentionable>> helper = new BaseObserverHelper<>(getContext(), observable);
        helper.execute(new OnCallbackListener<List<UserMentionable>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mUserMentionableList.set(null);
            }

            @Override
            public void onComplete(List<UserMentionable> result) {
                if (result != null && !result.isEmpty()) {
                    mUserMentionableList.set(result);
                } else {
                    mUserMentionableList.set(null);
                }
            }
        });
    }

    public interface SendNewCommendInterceptorListener {
        void onNewCommentPrepareToBeSent(LayoutEditTextCommentViewModel viewModel, Comment newComment);
    }

    public void onRefresh() {
        mIsRefresh.set(true);
    }

    public TextWatcher getTextWatcher(TextView actionButton) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    actionButton.setTextColor(AttributeConverter.convertAttrToColor(getContext(), R.attr.comment_send_activate_button));
                } else {
                    actionButton.setTextColor(AttributeConverter.convertAttrToColor(getContext(), R.attr.comment_send_button));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    @BindingAdapter("textChangeListener")
    public static void addTextChangeListener(RichEditorView richEditorView, TextWatcher listener) {
        richEditorView.getMentionsEditText().addTextChangedListener(listener);
    }
}
