package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.intent.newintent.ConciergeSelectLocationIntent;
import com.proapp.sompom.intent.newintent.MyCameraIntent;
import com.proapp.sompom.intent.newintent.MyCameraResultIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.emun.CitySelectionType;
import com.proapp.sompom.model.emun.ConciergeUserAddressLabel;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ConciergeNewAddressDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.sompom.baseactivity.ResultCallback;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Or Vitovongsak on 22/11/21.
 */
public class ConciergeNewAddressFragmentViewModel extends AbsLoadingViewModel {

    private ObservableField<String> mCustomerName = new ObservableField<>();
    private ObservableField<String> mFirstName = new ObservableField<>();
    private ObservableField<String> mLastName = new ObservableField<>();
    private ObservableField<String> mPhoneNumber = new ObservableField<>();
    private ObservableField<String> mAddress = new ObservableField<>();
    private ObservableField<String> mNumber = new ObservableField<>();
    private ObservableField<String> mPhotoUrl = new ObservableField<>();
    private ObservableInt mPhotoButtonVisibility = new ObservableInt(View.VISIBLE);
    private ObservableInt mPhotoVisibility = new ObservableInt(View.GONE);
    private ObservableBoolean mDeleteButtonVisibility = new ObservableBoolean();
    private ObservableInt mConfirmButtonText = new ObservableInt();

    private ObservableField<String> mLabel = new ObservableField<>();
    private ObservableField<String> mHouseNumber = new ObservableField<>();
    private ObservableField<String> mStreetDetail = new ObservableField<>();
    private ObservableField<String> mAreaDetail = new ObservableField<>();
    private ObservableField<String> mCity = new ObservableField<>();
    private ObservableField<String> mDeliveryInstruction = new ObservableField<>();
    private ObservableBoolean mIsPrimary = new ObservableBoolean();
    private ObservableBoolean mIsPrimaryOptionVisible = new ObservableBoolean();

    private ObservableField<String> mEditFirstNameError = new ObservableField<>();
    private ObservableField<String> mEditLastNameError = new ObservableField<>();
    private ObservableField<String> mEditHouseNumberError = new ObservableField<>();
    private ObservableField<String> mEditStreetDetailError = new ObservableField<>();
    private ObservableField<String> mEditAreaDetailError = new ObservableField<>();
    private ObservableField<String> mEditLabelError = new ObservableField<>();

    private ObservableField<String> mEditPhoneError = new ObservableField<>();
    private ObservableField<String> mEditNameError = new ObservableField<>();
    private ObservableField<String> mEditAddressError = new ObservableField<>();
    private ObservableField<String> mSelectCityError = new ObservableField<>();

    private ConciergeNewAddressDataManager mDataManager;
    private ConciergeNewAddressFragmentViewModelListener mListener;

    private boolean mIsEditMode;
    private ConciergeUserAddress mUserAddress;
    private ConciergeUserAddress mAddressBeforeEdit;

    public ConciergeNewAddressFragmentViewModel(Context context,
                                                ConciergeNewAddressDataManager dataManager,
                                                boolean shouldDisplayIsPrimaryOption,
                                                ConciergeNewAddressFragmentViewModelListener listener) {
        super(context);
        mIsPrimaryOptionVisible.set(shouldDisplayIsPrimaryOption);
        mDataManager = dataManager;
        mListener = listener;
        mUserAddress = new ConciergeUserAddress();
    }

    public ConciergeNewAddressFragmentViewModel(Context context,
                                                boolean isEditMode,
                                                ConciergeUserAddress addressToEdit,
                                                ConciergeNewAddressDataManager dataManager,
                                                boolean shouldDisplayIsPrimaryOption,
                                                ConciergeNewAddressFragmentViewModelListener listener) {
        super(context);
        mIsPrimaryOptionVisible.set(shouldDisplayIsPrimaryOption);
        mIsEditMode = isEditMode;
        mUserAddress = addressToEdit;
        mAddressBeforeEdit = addressToEdit;
        mDataManager = dataManager;
        mListener = listener;
    }

    public CitySelectionType getPreviousSelectedCity() {
        if (mAddressBeforeEdit != null) {
            return CitySelectionType.fromValue(mAddressBeforeEdit.getCity());
        }

        return null;
    }

    public ObservableField<String> getLabel() {
        return mLabel;
    }

    public ObservableField<String> getEditLabelError() {
        return mEditLabelError;
    }

    public ObservableBoolean getIsPrimaryOptionVisible() {
        return mIsPrimaryOptionVisible;
    }

    public ObservableField<String> getHouseNumber() {
        return mHouseNumber;
    }

    public ObservableField<String> getStreetDetail() {
        return mStreetDetail;
    }

    public ObservableField<String> getAreaDetail() {
        return mAreaDetail;
    }

    public ObservableField<String> getCity() {
        return mCity;
    }

    public ObservableField<String> getDeliveryInstruction() {
        return mDeliveryInstruction;
    }

    public ObservableBoolean getIsPrimary() {
        return mIsPrimary;
    }

    public ObservableField<String> getSelectCityError() {
        return mSelectCityError;
    }

    public ObservableField<String> getEditFirstNameError() {
        return mEditFirstNameError;
    }

    public ObservableField<String> getEditLastNameError() {
        return mEditLastNameError;
    }

    public ObservableField<String> getEditHouseNumberError() {
        return mEditHouseNumberError;
    }

    public ObservableField<String> getEditStreetDetailError() {
        return mEditStreetDetailError;
    }

    public ObservableField<String> getEditAreaDetailError() {
        return mEditAreaDetailError;
    }

    public ObservableField<String> getFirstName() {
        return mFirstName;
    }

    public ObservableField<String> getLastName() {
        return mLastName;
    }

    public ObservableField<String> getCustomerName() {
        return mCustomerName;
    }

    public ObservableField<String> getPhoneNumber() {
        return mPhoneNumber;
    }

    public ObservableField<String> getAddress() {
        return mAddress;
    }

    public ObservableField<String> getNumber() {
        return mNumber;
    }

    public ObservableInt getPhotoButtonVisibility() {
        return mPhotoButtonVisibility;
    }

    public ObservableInt getPhotoVisibility() {
        return mPhotoVisibility;
    }

    public ObservableBoolean getDeleteButtonVisibility() {
        return mDeleteButtonVisibility;
    }

    public ObservableInt getConfirmButtonText() {
        return mConfirmButtonText;
    }

    public ObservableField<String> getPhotoUrl() {
        return mPhotoUrl;
    }

    public ObservableField<String> getEditPhoneError() {
        return mEditPhoneError;
    }

    public ObservableField<String> getEditNameError() {
        return mEditNameError;
    }

    public ObservableField<String> getEditAddressError() {
        return mEditAddressError;
    }

    public void initData() {
        if (mIsEditMode) {
            mDeleteButtonVisibility.set(true);
            mConfirmButtonText.set(R.string.shop_new_address_update_button);

            mCustomerName.set(mUserAddress.getUserName());
            mPhoneNumber.set(mUserAddress.getPhone());
            mAddress.set(mUserAddress.getAddress());
            mNumber.set(mUserAddress.getNumber());
            mPhotoUrl.set(mUserAddress.getPhotoUrl());

            //Bind existing property for Delishop
            mLabel.set(mUserAddress.getLabel());
            mFirstName.set(mUserAddress.getFirstName());
            mLastName.set(mUserAddress.getLastName());
            mHouseNumber.set(mUserAddress.getHouseNumber());
            mStreetDetail.set(mUserAddress.getStreetDetail());
            mAreaDetail.set(mUserAddress.getAreaDetail());
            mDeliveryInstruction.set(mUserAddress.getDeliveryInstruction());
            if (mIsPrimaryOptionVisible.get()) {
                mIsPrimary.set(mUserAddress.isPrimary());
            }

            if (!TextUtils.isEmpty(mUserAddress.getPhotoUrl())) {
                mPhotoButtonVisibility.set(View.GONE);
                mPhotoVisibility.set(View.VISIBLE);
            }

            if (mListener != null) {
                if (mUserAddress.getCountryCode() != null) {
                    mListener.getCountryCodePicker().setCountryForPhoneCode(Integer.parseInt(mUserAddress.getCountryCode()));
                }
                if (!TextUtils.isEmpty(mUserAddress.getLabel())) {
                    mListener.selectLabel(ConciergeUserAddressLabel.fromValue(mUserAddress.getLabel()));
                }
            }
        } else {
            mDeleteButtonVisibility.set(false);
            mConfirmButtonText.set(R.string.shop_new_address_done_button);

            mFirstName.set(SharedPrefUtils.getUserFirstName(mContext));
            mLastName.set(SharedPrefUtils.getUserLastName(mContext));
            mPhoneNumber.set(SharedPrefUtils.getPhone(mContext));

            if (mListener != null) {
                String countryCode = SharedPrefUtils.getString(mContext, SharedPrefUtils.COUNTRY_CODE);

                if (!TextUtils.isEmpty(countryCode)) {
                    mListener.getCountryCodePicker().setCountryForPhoneCode(Integer.parseInt(countryCode));
                }
            }
        }
    }

    public void onSetLocationOnMapClick(View view) {
        if (mContext instanceof AbsBaseActivity) {

            ConciergeSelectLocationIntent intent = new ConciergeSelectLocationIntent(mContext);

            if (!TextUtils.isEmpty(mUserAddress.getAreaDetail())
                    && mUserAddress.getLatitude() != null
                    && mUserAddress.getLongitude() != null) {
                SearchAddressResult userAddress = new SearchAddressResult();
                userAddress.setFullAddress(mUserAddress.getAreaDetail());
                Locations locations = new Locations();
                locations.setLatitude(mUserAddress.getLatitude());
                locations.setLongitude(mUserAddress.getLongitude());
                userAddress.setLocations(locations);
                intent.putExtra(ConciergeSelectLocationIntent.ADDRESS_DATA, userAddress);
            }

            ((AbsBaseActivity) mContext).startActivityForResult(intent, new ResultCallback() {
                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    ConciergeSelectLocationIntent intent = new ConciergeSelectLocationIntent(data);
                    SearchAddressResult result = intent.getSearchAddressResult();

                    if (result != null) {
                        mAddress.set(result.getFullAddress());
                        mUserAddress.setAddress(mAddress.get());
                        mUserAddress.setAreaDetail(result.getFullAddress());
                        mUserAddress.setLatitude(result.getLocations().getLatitude());
                        mUserAddress.setLongitude(result.getLocations().getLongitude());
                        mAreaDetail.set(result.getFullAddress());
                        mEditAddressError.set(null);
                        mEditAreaDetailError.set(null);
                    }
                }
            });
        }
    }

    public void onLabelSelected(ConciergeUserAddressLabel label) {
        if (mUserAddress != null) {
            mUserAddress.setLabel(label.getValue());
        }
    }

    public void onPhotoClick(View view) {
        if (mContext instanceof AbsBaseActivity) {
            Intent intent = MyCameraIntent.newConciergeNewAddressInstance(mContext);
            ((AbsBaseActivity) mContext).startActivityForResult(intent, new ResultCallback() {
                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    MyCameraResultIntent resultIntent = new MyCameraResultIntent(data);
                    mPhotoUrl.set(resultIntent.getMedia().get(0).getUrl());
                    mPhotoButtonVisibility.set(View.GONE);
                    mPhotoVisibility.set(View.VISIBLE);
                }
            });
        }
    }

    public boolean validateUserInformation() {
        boolean isAllInfoValid = true;
        boolean isValidPhone = false;

        if (mListener != null) {
            isValidPhone = mListener.getCountryCodePicker().isValid();
        }

        if (!isValidPhone) {
            setError(mEditPhoneError, mContext.getString(R.string.shop_new_address_invalid_input_phone_number_format));
            isAllInfoValid = false;
        } else {
            mEditPhoneError.set(null);
            mUserAddress.setCountryCode(mListener.getCountryCodePicker().getSelectedCountryCode());
            mUserAddress.setPhone(String.valueOf(mListener.getCountryCodePicker().getPhoneNumber().getNationalNumber()));
        }

        //Start validating the field.
        if (TextUtils.isEmpty(getStringFieldValue(mLabel))) {
            setError(mEditLabelError, mContext.getString(R.string.shop_new_address_error_field_required));
            isAllInfoValid = false;
        } else {
            mEditLabelError.set(null);
            mUserAddress.setLabel(mLabel.get().trim());
        }

        if (TextUtils.isEmpty(getStringFieldValue(mFirstName))) {
            setError(mEditFirstNameError, mContext.getString(R.string.shop_new_address_error_field_required));
            isAllInfoValid = false;
        } else {
            mEditFirstNameError.set(null);
            mUserAddress.setFirstName(mFirstName.get().trim());
        }

        if (TextUtils.isEmpty(getStringFieldValue(mLastName))) {
            setError(mEditLastNameError, mContext.getString(R.string.shop_new_address_error_field_required));
            isAllInfoValid = false;
        } else {
            mEditLastNameError.set(null);
            mUserAddress.setLastName(mLastName.get().trim());
        }

        if (TextUtils.isEmpty(getStringFieldValue(mHouseNumber))) {
            setError(mEditHouseNumberError, mContext.getString(R.string.shop_new_address_error_field_required));
            isAllInfoValid = false;
        } else {
            mEditHouseNumberError.set(null);
            mUserAddress.setHouseNumber(mHouseNumber.get().trim());
        }

        if (TextUtils.isEmpty(getStringFieldValue(mStreetDetail))) {
            setError(mEditStreetDetailError, mContext.getString(R.string.shop_new_address_error_field_required));
            isAllInfoValid = false;
        } else {
            mEditStreetDetailError.set(null);
            mUserAddress.setStreetDetail(mStreetDetail.get().trim());
        }

        if (TextUtils.isEmpty(getStringFieldValue(mAreaDetail))) {
            setError(mEditAreaDetailError, mContext.getString(R.string.shop_new_address_error_field_required));
            isAllInfoValid = false;
        } else {
            mEditAreaDetailError.set(null);
            mUserAddress.setAreaDetail(mAreaDetail.get().trim());
        }

        if (mIsPrimaryOptionVisible.get()) {
            mUserAddress.setPrimary(mIsPrimary.get());
        }

        CitySelectionType selectedCity = mListener.getSelectedCity();
        if (selectedCity == CitySelectionType.SELECT_HINT) {
            setError(mSelectCityError, mContext.getString(R.string.shop_new_address_error_field_required));
            isAllInfoValid = false;
        } else {
            mSelectCityError.set(null);
            mUserAddress.setCity(selectedCity.getKey());
        }

        mUserAddress.setNumber(mNumber.get());
        mUserAddress.setDeliveryInstruction(getStringFieldValue(mDeliveryInstruction));

//        Timber.i("User address: " + new Gson().toJson(mUserAddress));
        return isAllInfoValid;
    }

    private void setError(ObservableField<String> errorObservable, String error) {
        errorObservable.set(error);
        errorObservable.notifyChange();
    }

    private String getStringFieldValue(ObservableField<String> value) {
        String checkValue = value.get();
        if (checkValue != null && !TextUtils.isEmpty(checkValue)) {
            return checkValue.trim();
        }

        return "";
    }

    public void onDoneButtonClick() {
        if (validateUserInformation()) {
            if (mIsEditMode) {
                showLoading(true);
                Observable<Response<ConciergeUserAddress>> ob = mDataManager.updateAddress(mUserAddress, mAddressBeforeEdit);
                ResponseObserverHelper<Response<ConciergeUserAddress>> helper = new ResponseObserverHelper<>(getContext(), ob);
                addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeUserAddress>>() {
                    @Override
                    public void onComplete(Response<ConciergeUserAddress> result) {
                        hideLoading();
                        if (result.isSuccessful() && !result.body().isError()) {
                            // Broadcast address updated event
                            ConciergeHelper.broadcastAddressUpdatedEvent(mContext, result.body());
                            if (mListener != null) {
                                mListener.onAddressUpdated(result.body());
                            }
                        } else {
                            showSnackBar(result.body().getErrorMessage(), true);
                        }
                    }

                    @Override
                    public void onFail(ErrorThrowable ex) {
                        hideLoading();
                        showSnackBar(ex.getMessage(), true);
                    }
                }));
            } else {
                showLoading(true);
                Observable<Response<ConciergeUserAddress>> ob = mDataManager.createNewAddress(mUserAddress);
                ResponseObserverHelper<Response<ConciergeUserAddress>> helper = new ResponseObserverHelper<>(getContext(), ob);
                addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeUserAddress>>() {
                    @Override
                    public void onComplete(Response<ConciergeUserAddress> result) {
                        hideLoading();
                        if (result.isSuccessful() && !result.body().isError()) {
                            // Broadcast address created event
                            ConciergeHelper.broadcastAddressCreatedEvent(mContext, result.body());
                            if (mListener != null) {
                                mListener.onAddNewAddress(result.body());
                            }
                        } else {
                            showSnackBar(result.body().getErrorMessage(), true);
                        }
                    }

                    @Override
                    public void onFail(ErrorThrowable ex) {
                        hideLoading();
                        showSnackBar(ex.getMessage(), true);
                    }
                }));
            }
        }
    }

    public void onDeleteButtonClick() {
        // Show confirmation first before deleting address
        showConfirmationDialog(mContext,
                mContext.getString(R.string.shop_checkout_alert_error),
                mContext.getString(R.string.shop_checkout_confirm_delete_address),
                mContext.getString(R.string.popup_ok_button),
                mContext.getString(R.string.popup_cancel_button),
                v -> deleteAddress());
    }

    private void deleteAddress() {
        showLoading(true);
        Observable<Response<SupportConciergeCustomErrorResponse>> ob = mDataManager.deleteAddress(mUserAddress);
        ResponseObserverHelper<Response<SupportConciergeCustomErrorResponse>> helper = new ResponseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(new OnCallbackListener<Response<SupportConciergeCustomErrorResponse>>() {
            @Override
            public void onComplete(Response<SupportConciergeCustomErrorResponse> result) {
                hideLoading();
                if (result.isSuccessful() && !result.body().isError()) {
                    ConciergeHelper.broadcastAddressDeletedEvent(mContext, mUserAddress);
                    if (mListener != null) {
                        mListener.onUserAddressDeleted(mUserAddress);
                    }
                } else {
                    showSnackBar(result.body().getErrorMessage(), true);
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }
        }));
    }

    @BindingAdapter("setInputEnglishOnlyNotice")
    public static void setInputEnglishOnlyNotice(TextView textView, boolean displayNote) {
        if (displayNote) {
            textView.post(() -> textView.setText(textView.getText().toString() + textView.getContext().getString(R.string.shop_new_address_english_only)));
        }
    }

    @BindingAdapter("setSpinnerError")
    public static void setSpinnerError(AppCompatSpinner spinner, String error) {
        if (spinner.getSelectedView() != null && spinner.getSelectedView() instanceof TextView) {
            ((TextView) spinner.getSelectedView()).setError(error);
        }
    }

    public interface ConciergeNewAddressFragmentViewModelListener {
        void onAddNewAddress(ConciergeUserAddress newAddress);

        void onAddressUpdated(ConciergeUserAddress editedAddress);

        void onUserAddressDeleted(ConciergeUserAddress deletedAddress);

        void selectLabel(ConciergeUserAddressLabel label);

        CountryCodePicker getCountryCodePicker();

        ConciergeUserAddressLabel getUserSelectedLabel();

        CitySelectionType getSelectedCity();
    }
}
