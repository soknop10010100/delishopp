package com.proapp.sompom.viewmodel.newviewmodel;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

/**
 * Created by Chhom Veasna on 8/6/2020.
 */
public abstract class AbsItemFileViewModel extends AbsBaseViewModel {

    protected ObservableInt mDownloadProgression = new ObservableInt();
    protected ObservableField<Media.FileStatusType> mFileStatus = new ObservableField<>(Media.FileStatusType.IDLE);
    protected Media mMedia;

    public AbsItemFileViewModel(Media media) {
        mMedia = media;
        mFileStatus.set(media.getFileStatusType());
    }

    public ObservableInt getDownloadProgression() {
        return mDownloadProgression;
    }

    public void setDownloadProgression(int downloadProgression) {
        mDownloadProgression.set(downloadProgression);
    }

    public ObservableField<Media.FileStatusType> getFileStatus() {
        return mFileStatus;
    }

    public void setFileStatus(Media.FileStatusType fileStatus) {
        mFileStatus.set(fileStatus);
    }

    public abstract void onFileClick();

    @BindingAdapter("fileStatusType")
    public static void bindFileStatus(ViewGroup viewGroup, Media.FileStatusType fileStatusType) {
        ProgressBar downloadProgressbar = viewGroup.findViewById(R.id.progressBarDownload);
        TextView textView = viewGroup.findViewById(R.id.textViewStatus);

        if (fileStatusType == Media.FileStatusType.IDLE) {
            downloadProgressbar.setProgress(0);
            downloadProgressbar.setVisibility(View.INVISIBLE);
            textView.setVisibility(View.VISIBLE);
            textView.setTextColor(AttributeConverter.convertAttrToColor(viewGroup.getContext(),
                    R.attr.post_file_download_indicator));
            textView.setText(R.string.code_check);
        } else if (fileStatusType == Media.FileStatusType.DOWNLOADING) {
            downloadProgressbar.setVisibility(View.VISIBLE);
            textView.setVisibility(View.INVISIBLE);
        } else {
            //Downloaded
            downloadProgressbar.setProgress(0);
            downloadProgressbar.setVisibility(View.INVISIBLE);
            textView.setVisibility(View.VISIBLE);
            textView.setTextColor(AttributeConverter.convertAttrToColor(viewGroup.getContext(),
                    R.attr.post_file_download_completed));
            textView.setText(R.string.code_check_filled);
        }
    }
}
