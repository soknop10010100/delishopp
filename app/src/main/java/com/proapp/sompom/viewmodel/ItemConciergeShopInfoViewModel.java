package com.proapp.sompom.viewmodel;

import androidx.databinding.ObservableField;

import com.proapp.sompom.model.concierge.ConciergeShop;

/**
 * Created by Veasna Chhom on 26/1/22.
 */
public class ItemConciergeShopInfoViewModel {

    private final ObservableField<String> mShopName = new ObservableField<>();
    private final ObservableField<String> mShopAddress = new ObservableField<>();
    private final ObservableField<String> mShopDescription = new ObservableField<>();
    private final ObservableField<String> mShopOpenHour = new ObservableField<>();

    public ItemConciergeShopInfoViewModel(ConciergeShop shop) {
        bindData(shop);
    }

    private void bindData(ConciergeShop shop) {
        mShopName.set(shop.getName());
        mShopAddress.set(shop.getLocation());
        mShopDescription.set(shop.getDescription());
        //TODO: Shop open hour will hide for now.
    }

    public ObservableField<String> getShopName() {
        return mShopName;
    }

    public ObservableField<String> getShopAddress() {
        return mShopAddress;
    }


    public ObservableField<String> getShopDescription() {
        return mShopDescription;
    }

    public ObservableField<String> getShopOpenHour() {
        return mShopOpenHour;
    }
}
