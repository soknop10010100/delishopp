package com.proapp.sompom.viewmodel;

import android.widget.LinearLayout;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.R;
import com.proapp.sompom.model.concierge.ConciergeSubCategoryLoadMoreDataModel;

/**
 * Created by Veasna Chhom on 26/4/22.
 */
public class ListLoadMoreConciergeSubCategoryItemViewModel extends AbsBaseViewModel {

    private final ObservableBoolean mIsPerformLoadingMore = new ObservableBoolean();
    private ConciergeSubCategoryLoadMoreDataModel mDataModel;
    private ListLoadMoreConciergeSubCategoryItemViewModeListener mListener;
    private ObservableBoolean mShouldAddMarginTop = new ObservableBoolean();

    public ListLoadMoreConciergeSubCategoryItemViewModel(ConciergeSubCategoryLoadMoreDataModel dataModel,
                                                         boolean shouldAddMarginTop,
                                                         ListLoadMoreConciergeSubCategoryItemViewModeListener listener) {
        mDataModel = dataModel;
        mListener = listener;
        mShouldAddMarginTop.set(shouldAddMarginTop);
        mIsPerformLoadingMore.set(dataModel.isPerformingLoadMore());
    }

    public ObservableBoolean getShouldAddMarginTop() {
        return mShouldAddMarginTop;
    }

    public ObservableBoolean getIsPerformLoadingMore() {
        return mIsPerformLoadingMore;
    }

    public void onItemClick() {
        if (!mIsPerformLoadingMore.get()) {
            mIsPerformLoadingMore.set(true);
            mDataModel.setPerformingLoadMore(true);
            if (mListener != null) {
                mListener.onLoadMoreClicked(mDataModel);
            }
        }
    }

    public void setPerformLoadingMoreStatus(boolean isLoadingMore) {
        mIsPerformLoadingMore.set(isLoadingMore);
        mDataModel.setPerformingLoadMore(isLoadingMore);
    }

    public interface ListLoadMoreConciergeSubCategoryItemViewModeListener {
        void onLoadMoreClicked(ConciergeSubCategoryLoadMoreDataModel loadMoreDataModel);
    }

    @BindingAdapter("shouldAddMarginTop")
    public static void setViewContainerMargin(LinearLayout linearLayout, boolean shouldAddMarginTop) {
        ((LinearLayout.LayoutParams) linearLayout.getLayoutParams()).bottomMargin = linearLayout.getResources().getDimensionPixelSize(R.dimen.space_12dp);
        if (shouldAddMarginTop) {
            ((LinearLayout.LayoutParams) linearLayout.getLayoutParams()).topMargin = linearLayout.getResources().getDimensionPixelSize(R.dimen.space_xlarge);
        }
        ((LinearLayout.LayoutParams) linearLayout.getLayoutParams()).leftMargin = 0;
        ((LinearLayout.LayoutParams) linearLayout.getLayoutParams()).rightMargin = 0;
    }
}
