package com.proapp.sompom.viewmodel;

import com.proapp.sompom.listener.OnMediaHandler;
import com.proapp.sompom.listener.OnMediaListener;
import com.proapp.sompom.listener.ViewModelLifeCycle;
import com.proapp.sompom.model.Media;

/**
 * Created by He Rotha on 9/1/17.
 */

public abstract class BaseMediaViewModel extends AbsBaseViewModel
        implements OnMediaListener, ViewModelLifeCycle {
    public OnMediaHandler mOnMediaHandler;
    private boolean mIsPlaying = false;
    private int mPosition = 0;

    public BaseMediaViewModel(OnMediaHandler onMediaHandler) {
        mOnMediaHandler = onMediaHandler;
    }

    public boolean isAutoScrollPlay() {
        return false;
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    @Override
    public void onMediaPlay(boolean isWait) {
        if (mOnMediaHandler != null) {
            mOnMediaHandler.onPlay(this);
        }
    }

    @Override
    public void onMediaPlay() {
        if (mOnMediaHandler != null) {
            mOnMediaHandler.onPlay(this);
        }
    }

    @Override
    public void onMediaPause() {

    }

    @Override
    public boolean isPlaying() {
        return false;
    }

    @Override
    public void onPause() {
        mIsPlaying = isPlaying();
        onMediaPause();
    }

    @Override
    public void onResume() {
        if (mIsPlaying) {
            onMediaPlay();
        }
    }

    @Override
    public void onResumePlaybackMedia(Media media) {

    }
}
