package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.SupportCustomErrorResponse2;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ResetPasswordDataManager;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Veasna Chhom on 5/18/22.
 */
public abstract class AbsResetPasswordViewModel extends AbsSignUpViewModel {

    protected ResetPasswordDataManager mDataManager;

    public AbsResetPasswordViewModel(Context context, ResetPasswordDataManager dataManager) {
        super(context);
        mDataManager = dataManager;
    }

    protected void requestForgotPassword(boolean isResend, String email) {
        showLoading(true);
        Observable<Response<SupportCustomErrorResponse2>> checkEmailService = mDataManager.getForgotPasswordService(email);
        ResponseObserverHelper<Response<SupportCustomErrorResponse2>> helper = new ResponseObserverHelper<>(getContext(), checkEmailService);
        addDisposable(helper.execute(new OnCallbackListener<Response<SupportCustomErrorResponse2>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<SupportCustomErrorResponse2> result) {
                hideLoading();
                if (result.body().isError()) {
                    showSnackBar(result.body().getErrorMessage(), true);
                } else {
                    AbsResetPasswordViewModel.this.onRequestForgotPasswordSuccess(isResend);
                }
            }
        }));
    }

    protected void onRequestForgotPasswordSuccess(boolean isResend) {
        //Client Impl...
    }

    public TextView.OnEditorActionListener getOnEditorActionListener() {
        return (textView, action, keyEvent) -> {
            if (action == EditorInfo.IME_ACTION_DONE) {
                onActionButtonClicked();
            }

            return false;
        };
    }

    public void onBackClick() {
        if (getContext() instanceof Activity) {
            ((Activity) getContext()).finish();
        }
    }
}
