package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.database.LinkPreviewDb;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnChatItemListener;
import com.proapp.sompom.model.BasePreviewModel;
import com.proapp.sompom.model.SelectedChat;
import com.proapp.sompom.model.emun.ChatLinkBg;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.model.result.ReferencedChat;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.ChatUtility;
import com.proapp.sompom.utils.GenerateLinkPreviewUtil;
import com.proapp.sompom.utils.GlideLoadUtil;
import com.proapp.sompom.widget.MaxWidthLinearLayout;

import java.util.Collections;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by He Rotha on 9/14/17.
 */

public class ListItemChatLInkPreviewViewModel extends ChatViewModel {

    public final ObservableField<String> mLinkLogoUrl = new ObservableField<>();
    public final ObservableField<String> mLinkTitle = new ObservableField<>();
    public final ObservableField<String> mLinkDescription = new ObservableField<>();
    public final ObservableBoolean mIsShowLogo = new ObservableBoolean(false);
    private final ObservableInt mPlayIconVisibility = new ObservableInt(View.GONE);

    private String mCurrentLinkPreview;
    private ChatLinkBg mChatLinkBg;
    private ApiService mApiService;

    public ListItemChatLInkPreviewViewModel(Activity context,
                                            ApiService apiService,
                                            Conversation conversation,
                                            View rootView,
                                            int position,
                                            Chat chat,
                                            SelectedChat selectedChat,
                                            String searchKeyWord,
                                            ChatUtility.GroupMessage drawable,
                                            boolean senderSide,
                                            OnChatItemListener onItemClickListener) {
        super(context,
                conversation,
                rootView,
                position,
                chat,
                selectedChat,
                searchKeyWord,
                drawable,
                senderSide,
                onItemClickListener);
        mApiService = apiService;
        mChatLinkBg = ChatLinkBg.getChatLinkBg(drawable.getChatBg());
        loadLinkPreview();
    }

    public ObservableInt getPlayIconVisibility() {
        return mPlayIconVisibility;
    }

    private void loadLinkPreview() {
        if (mChat.getLinkPreviewModel() != null
                && mChat.getLinkPreviewModel().isLinkDownloaded()) {
            bindData(mChat.getLinkPreviewModel());
        } else {
            mCurrentLinkPreview = GenerateLinkPreviewUtil.getFirstUrlIndex(mChat.getContent());
            if (!TextUtils.isEmpty(mCurrentLinkPreview)) {
                requestLinkPreview(mCurrentLinkPreview, mChat.getId(), mChat.getContent());
            }
        }
    }

    @Override
    public void onChatContentEdited() {
        super.onChatContentEdited();
        loadLinkPreview();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void onContainerLinkClick() {
        WallStreetHelper.openCustomTab(mContext, mCurrentLinkPreview);
    }

    public Drawable getLinkTitleBackground() {
        return ContextCompat.getDrawable(getActivity(), mChatLinkBg.getDrawableTitle());
    }

    public Drawable getLinkLayoutBackground(View view) {
        return ContextCompat.getDrawable(getActivity(), mChatLinkBg.getDrawableBody());
    }

    private void bindData(LinkPreviewModel linkPreviewModel) {
        mCurrentLinkPreview = linkPreviewModel.getLink();
        mLinkTitle.set(LinkPreviewRetriever.getPreviewTitle(linkPreviewModel));
        mLinkDescription.set(LinkPreviewRetriever.getPreviewDescription(linkPreviewModel));
        BasePreviewModel.PreviewType previewType = BasePreviewModel.PreviewType.getFromValue(linkPreviewModel.getType());
        if (previewType == BasePreviewModel.PreviewType.FILE) {
            BasePreviewModel.FilePreviewType fileType = BasePreviewModel.FilePreviewType.getFromValue(linkPreviewModel.getFileType());
            mPlayIconVisibility.set(fileType == BasePreviewModel.FilePreviewType.VIDEO ? View.VISIBLE : View.GONE);
            if (fileType == BasePreviewModel.FilePreviewType.IMAGE || fileType == BasePreviewModel.FilePreviewType.VIDEO) {
                mIsShowLogo.set(true);
                mLinkLogoUrl.set(linkPreviewModel.getLink());
            } else {
                mIsShowLogo.set(false);
            }
        } else {
            mIsShowLogo.set(!TextUtils.isEmpty(linkPreviewModel.getImage()));
            mPlayIconVisibility.set(View.GONE);
            mLinkLogoUrl.set(linkPreviewModel.getImage());
        }
    }

    private boolean isVisibleLogoPreview(LinkPreviewModel linkPreviewModel) {
        if (!TextUtils.isEmpty(linkPreviewModel.getImage())) {
            return true;
        } else {
            BasePreviewModel.PreviewType previewType = BasePreviewModel.PreviewType.getFromValue(linkPreviewModel.getType());
            if (previewType == BasePreviewModel.PreviewType.FILE) {
                BasePreviewModel.FilePreviewType fileType = BasePreviewModel.FilePreviewType.getFromValue(linkPreviewModel.getFileType());
                return fileType == BasePreviewModel.FilePreviewType.IMAGE || fileType == BasePreviewModel.FilePreviewType.VIDEO;
            }
        }

        return false;
    }


    private void bindDefaultData() {
        String firstUrlIndex = GenerateLinkPreviewUtil.getFirstUrlIndex(mChat.getContent());
        if (!TextUtils.isEmpty(firstUrlIndex)) {
            mLinkTitle.set(LinkPreviewRetriever.getHostFromUrl(firstUrlIndex));
            mLinkDescription.set(firstUrlIndex);
        }
    }

    private void requestLinkPreview(String url, String itemId, String resource) {
        Observable<Response<LinkPreviewModel>> call = LinkPreviewRetriever.getPreviewLink(getActivity(), mApiService, url);
        ResponseObserverHelper<Response<LinkPreviewModel>> helper = new ResponseObserverHelper<>(mContext, call);
        addDisposable(helper.execute(new OnCallbackListener<Response<LinkPreviewModel>>() {
            @Override
            public void onComplete(Response<LinkPreviewModel> result) {
                LinkPreviewModel linkPreviewModel = result.body();
                if (linkPreviewModel != null && linkPreviewModel.isValidPreviewData()) {
                    mChat.setLinkPreviewModel(linkPreviewModel);
                    mChat.setMetaPreview(Collections.singletonList(linkPreviewModel));
                    GlideLoadUtil.preCacheImage(mContext, linkPreviewModel.getImage(), null);
                    bindData(linkPreviewModel);
                    linkPreviewModel.setId(itemId);
                    linkPreviewModel.setResourceContent(resource);
                    linkPreviewModel.setLinkDownloaded(true);
                    LinkPreviewDb.save(mContext, linkPreviewModel, true);
                } else {
                    bindDefaultData();
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                bindDefaultData();
            }
        }));
    }

    @BindingAdapter({"referencedChat", "previewImage"})
    public static void validateLinkPreViewItemWidth(MaxWidthLinearLayout root,
                                                    ReferencedChat referencedChat,
                                                    ImageView previewImage) {
        if (referencedChat != null) {
            int desireWidth = ChatHelper.getDesireLinkPreviewWidthOfChatItemBaseOnReferenceChat(root.getContext(), referencedChat);
            if (desireWidth > 0) {
                root.getLayoutParams().width = desireWidth;
            }
        }
        //Set preview image height to 50% of root view width
        root.post(() -> previewImage.getLayoutParams().height = (int) ((root.getWidth() * 50) / 100f));
    }
}
