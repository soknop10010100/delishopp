package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableField;

import com.proapp.sompom.model.Media;

/**
 * Created by Chhom Veasna on 10/17/2019.
 */
public class ItemPostTimelineFileViewModel extends AbsItemFileViewModel {

    private ObservableField<String> mName = new ObservableField<>();
    private ObservableField<String> mExtension = new ObservableField<>();
    private ItemPostTimelineFileViewModelListener mListener;

    public ItemPostTimelineFileViewModel(Media media, ItemPostTimelineFileViewModelListener listener) {
        super(media);
        mListener = listener;
        mName.set(media.getFileName());
        mExtension.set(media.getFormat());
    }

    public ObservableField<String> getName() {
        return mName;
    }

    public ObservableField<String> getExtension() {
        return mExtension;
    }

    @Override
    public final void onFileClick() {
        if (mListener != null) {
            mListener.onFileClicked(false, mMedia, this);
        }
    }

    public interface ItemPostTimelineFileViewModelListener {
        void onFileClicked(boolean isFromMoreFile, Media media, ItemPostTimelineFileViewModel fileViewModel);
    }
}
