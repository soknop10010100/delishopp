package com.proapp.sompom.viewmodel.newviewmodel;

import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableInt;

import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;

/**
 * Created by nuonveyo on 7/12/18.
 */

public class ListItemTimelineMediaDetailViewModel extends AbsTimelineDetailItemViewModel {

    //    public final ObservableField<ListItemTimelineViewModel> mFooterViewModel = new ObservableField<>();
    private final ObservableInt mCheckLikeCommentVisibility = new ObservableInt();
    private ObservableInt mBottomLineVisibility = new ObservableInt(View.INVISIBLE);

    public void setCheckLikeCommentVisibility(int checkLikeCommentVisibility) {
        mCheckLikeCommentVisibility.set(checkLikeCommentVisibility);
    }

    public ObservableInt getBottomLineVisibility() {
        return mBottomLineVisibility;
    }

    public ObservableInt getCheckLikeCommentVisibility() {
        return mCheckLikeCommentVisibility;
    }

    public ListItemTimelineMediaDetailViewModel(AbsBaseActivity activity,
                                                WallStreetAdaptive post,
                                                boolean checkLikeComment,
                                                Adaptive adaptive,
                                                WallStreetDataManager dataManager,
                                                int position,
                                                OnTimelineItemButtonClickListener onItemClick) {
        super(activity, post, adaptive, dataManager, position, onItemClick);
        if (TextUtils.isEmpty(post.getDescription()) &&
                TextUtils.isEmpty(post.getShareUrl()) &&
                post.getMedia().size() == 1) {
            /*
            This post contains only one media and share no text or share url to preview. So we
            have to take user view from post for this media item and post are considered the same.
             */
            adaptive.setUserViewWithAdaptive(post.getUserView());
        }
        mBottomLineVisibility.set(View.INVISIBLE);
        if (!checkLikeComment) {
            setCheckLikeCommentVisibility(View.GONE);
        } else {
            setCheckLikeCommentVisibility(View.VISIBLE);
        }
    }

    public void setBottomLineVisibility(boolean visible) {
        mBottomLineVisibility.set(visible ? View.VISIBLE : View.INVISIBLE);
    }
}
