package com.proapp.sompom.viewmodel;

import android.graphics.drawable.Drawable;

import androidx.databinding.ObservableField;

public class OverlapImageContainerViewModel {

    private ObservableField<Drawable> mImageBorder = new ObservableField<>();

    public void setImageBorder(Drawable imageBorder) {
        mImageBorder.set(imageBorder);
    }

    public ObservableField<Drawable> getImageBorder() {
        return mImageBorder;
    }
}
