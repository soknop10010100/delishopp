package com.proapp.sompom.viewmodel;


import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.model.ShopDeliveryTime;
import com.proapp.sompom.utils.AttributeConverter;

/**
 * Created by Veasna Chhom on 9/9/21.
 */
public class ItemShopDeliveryTimeViewModel {

    private final ObservableField<String> mTime = new ObservableField<>();
    private final ObservableInt mTimeTextColor = new ObservableInt();
    private final ObservableField<Drawable> mOptionBackground = new ObservableField<>();
    private final ObservableField<String> mOptionTextIcon = new ObservableField<>();
    private final ObservableBoolean mIsSelected = new ObservableBoolean();
    private final ObservableInt mViewVisibility = new ObservableInt();
    private final ObservableBoolean mIsUnavailableTime = new ObservableBoolean();

    private final ShopDeliveryTime mShopDeliveryTime;
    private final Context mContext;
    private final ItemShopDeliveryTimeViewModelCallback mListener;
    private boolean mIsViewOnlyMode;

    public ItemShopDeliveryTimeViewModel(Context context,
                                         boolean isViewOnlyMode,
                                         ShopDeliveryTime shopDeliveryTime,
                                         ItemShopDeliveryTimeViewModelCallback listener) {
        mContext = context;
        mIsViewOnlyMode = isViewOnlyMode;
        mShopDeliveryTime = shopDeliveryTime;
        mListener = listener;
        bindData();
    }

    public ObservableBoolean getIsUnavailableTime() {
        return mIsUnavailableTime;
    }

    public ObservableField<String> getTime() {
        return mTime;
    }

    public ObservableInt getTimeTextColor() {
        return mTimeTextColor;
    }

    public ObservableField<Drawable> getOptionBackground() {
        return mOptionBackground;
    }

    public ObservableField<String> getOptionTextIcon() {
        return mOptionTextIcon;
    }

    public ObservableBoolean getIsSelected() {
        return mIsSelected;
    }

    public ObservableInt getViewVisibility() {
        return mViewVisibility;
    }

    private void bindData() {
        if (mShopDeliveryTime != null) {
            mIsUnavailableTime.set(mShopDeliveryTime.isDisable());
            mViewVisibility.set(mShopDeliveryTime.isVisible() ? View.VISIBLE : View.GONE);

            if (mShopDeliveryTime.isDisable()) {
                mTimeTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_delivery_time_unavailable_text));
            } else {
                if (mShopDeliveryTime.isSelected()) {
                    mTimeTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_delivery_time_text));
                    mOptionBackground.set(ContextCompat.getDrawable(mContext, R.drawable.shop_delivery_time_selected_option_background));
                    mOptionTextIcon.set(mContext.getString(R.string.fi_rr_check));
                    mIsSelected.set(true);
                } else {
                    mTimeTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_delivery_time_text));
                    mOptionBackground.set(ContextCompat.getDrawable(mContext, R.drawable.shop_delivery_time_default_option_background));
                    mOptionTextIcon.set("");
                    mIsSelected.set(false);
                }
            }

            if (mShopDeliveryTime.isProvinceTime()) {
                mTime.set("Select");
            } else {
                mTime.set(ConciergeHelper.buildTimeSlotDisplayFormat(mContext, mShopDeliveryTime.getStartTime(), mShopDeliveryTime.getEndTime()));
            }
        }
    }

    public void onItemClicked() {
        if (mIsViewOnlyMode) {
            return;
        }

        if (!mShopDeliveryTime.isDisable() &&
                mShopDeliveryTime.isVisible() &&
                !mShopDeliveryTime.isSelected()) {
            updateSelectionState(true);
            if (mListener != null) {
                mListener.onItemClicked(mShopDeliveryTime);
            }
        }
    }

    public void updateSelectionState(boolean isSelected) {
        mShopDeliveryTime.setSelected(isSelected);
        bindData();
    }

    public interface ItemShopDeliveryTimeViewModelCallback {
        void onItemClicked(ShopDeliveryTime time);
    }

    @BindingAdapter("isSelected")
    public static void setSelectedOption(TextView textView, boolean isSelected) {
        textView.setTypeface(null, isSelected ? Typeface.BOLD : Typeface.NORMAL);
    }
}
