package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;

import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.intent.ConciergeProductDetailIntent;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.utils.AttributeConverter;

/**
 * Created by Or Vitovongsak on 13/1/22.
 */
public class ListItemConciergePushItemViewModel {

    private ObservableField<String> mProductPicture = new ObservableField<>();
    private ObservableField<Spannable> mOriginalPrice = new ObservableField<>();
    private ObservableField<Spannable> mDiscountPrice = new ObservableField<>();

    private Context mContext;
    private ConciergeMenuItem mMenuItem;

    public ListItemConciergePushItemViewModel(Context context,
                                              ConciergeMenuItem menuItem) {
        mContext = context;
        mMenuItem = menuItem;
        initItem();
    }

    public void initItem() {
        mProductPicture.set(mMenuItem.getPicture());

        if (mMenuItem.getOriginalPrice() > 0) {
            mOriginalPrice.set(ConciergeHelper.getDisplayCrossedOutPrice(mContext,
                    mMenuItem.getOriginalPrice(),
                    AttributeConverter.convertAttrToColor(mContext, R.attr.shop_list_discount_item_strikethroug_price)));
        }
        SpannableStringBuilder discountPriceBuilder =
                new SpannableStringBuilder(ConciergeHelper.getDisplayPrice(mContext, mMenuItem.getPrice()));
        discountPriceBuilder.setSpan(
                new ForegroundColorSpan(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_list_discount_item_price)),
                0,
                discountPriceBuilder.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mDiscountPrice.set(discountPriceBuilder);
    }

    public void onItemClicked(View view) {
        // Open the item's detail when user clicked on the menu item
        mContext.startActivity(new ConciergeProductDetailIntent(mContext, mMenuItem, -1));
    }

    public ObservableField<String> getProductPicture() {
        return mProductPicture;
    }

    public ObservableField<Spannable> getOriginalPrice() {
        return mOriginalPrice;
    }

    public ObservableField<Spannable> getDiscountPrice() {
        return mDiscountPrice;
    }
}
