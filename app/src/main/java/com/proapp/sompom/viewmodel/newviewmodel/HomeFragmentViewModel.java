package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.call.CallTimerHelper;
import com.proapp.sompom.chat.call.CallingService;
import com.proapp.sompom.chat.call.IncomingCallDataHolder;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.NavigateSellerStoreHelper;
import com.proapp.sompom.helper.ObservableListenerBoolean;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.intent.ConciergeShopDetailIntent;
import com.proapp.sompom.intent.newintent.NotificationIntent;
import com.proapp.sompom.intent.newintent.SearchMessageIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnCompleteListener;
import com.proapp.sompom.model.result.AppFeature;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.model.result.UserBadge;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.fragment.AbsBaseFragment;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ProductListDataManager;
import com.proapp.sompom.utils.NetworkStateUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;
import com.sompom.baseactivity.ResultCallback;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/22/18.
 */

public class HomeFragmentViewModel extends AbsLoadingViewModel {

    private static final int REQUEST_BADGE_DELAY = 2000;

    public final ObservableField<User> mUser = new ObservableField<>();
    public final ObservableField<UserBadge> mUserBadge = new ObservableField<>();
    private final ObservableField<String> mSearchInputHint = new ObservableField<>();
    private final ObservableField<String> mErrorTitle = new ObservableField<>();
    private final ObservableField<String> mErrorMessage = new ObservableField<>();
    private final ObservableBoolean mShouldShowNotificationOption = new ObservableBoolean();
    public final ObservableListenerBoolean mIsShowingLoadingScreen = new ObservableListenerBoolean(false);
    private final ObservableField<AppFeature.NavigationBarMenu> mApplicationModeIcon = new ObservableField<>();

    private final ObservableBoolean mIsFirstLogin = new ObservableBoolean(true);
    private final ProductListDataManager mDataManager;
    private final OnQueryProductListener mOnQueryProductListener;
    private final HomeFragmentViewModelLister mLister;
    public final BadgeViewModel mNotificationBadge = new BadgeViewModel();
    private AppFeature.NavigationBarMenu mCurrentNavBarMenu;
    private OnBadgeUpdateListener mBadgeUpdateListener;
    private OnCompleteListener<CallingService.CallServiceBinder> mCallServiceBinderOnCompleteListener;

    public HomeFragmentViewModel(ProductListDataManager dataManager,
                                 OnQueryProductListener listener,
                                 HomeFragmentViewModelLister lister) {
        super(dataManager.getContext());
        mLister = lister;
        mOnQueryProductListener = listener;
        mDataManager = dataManager;
        mSearchInputHint.set(mContext.getString(R.string.search_home_screen_input_hint));
        initCallServiceBinderOnCompleteListener();
        checkToShowNotificationOption();
        if (SharedPrefUtils.isLogin(getContext())) {
            mUser.set(SharedPrefUtils.getUser(getContext()));
            mIsFirstLogin.set(UserHelper.isGuestUserExist(getContext()));
            bindNotificationBadge();
            if (getContext() instanceof AbsBaseActivity) {
                ((AbsBaseActivity) getContext()).bindCall(mCallServiceBinderOnCompleteListener, true);
            }
        }
    }

    public void loadApplicationModeIcon() {
        AppFeature appFeature = ApplicationHelper.getAppFeature(mContext);
        if (appFeature != null &&
                appFeature.getNavigationBarMenu() != null &&
                !appFeature.getNavigationBarMenu().isEmpty()) {
            /*
               We will get only one application icon which is either normal or express mode from API.
             */
            for (AppFeature.NavigationBarMenu navigationBarMenu : appFeature.getNavigationBarMenu()) {
                if (mApplicationModeIcon.get() != null) {
                    break;
                }

                if (navigationBarMenu.getNavMenuButton() == AppFeature.NavMenuButton.NormalMode ||
                        navigationBarMenu.getNavMenuButton() == AppFeature.NavMenuButton.ExpressMode) {
                    mApplicationModeIcon.set(navigationBarMenu);
                }
            }
        }
    }

    public ObservableField<AppFeature.NavigationBarMenu> getApplicationModeIcon() {
        return mApplicationModeIcon;
    }

    public ObservableListenerBoolean getIsShowingLoadingScreen() {
        return mIsShowingLoadingScreen;
    }

    public void showOrHideLoadingScreen(boolean isShow) {
        mIsShowingLoadingScreen.set(isShow);
    }

    private void initCallServiceBinderOnCompleteListener() {
        mCallServiceBinderOnCompleteListener = result -> result.addCallListener(new CallingService.CallingListener() {
            @Override
            public void onCallEnd(AbsCallService.CallType callType,
                                  String channelId,
                                  boolean isGroup,
                                  User recipient,
                                  boolean isMissedCall,
                                  int calledDuration,
                                  boolean isEndedByClickAction,
                                  boolean isCauseByTimeOut,
                                  boolean isCausedByParticipantOffline) {
            }

            @Override
            public void onIncomingCall(AbsCallService.CallType calType, Map<String, Object> data1, Map<String, Object> data2) {
                Timber.i("onIncomingCall: calType: " + calType.name() + ", data1: " + new Gson().toJson(data1) + "\n " +
                        "data2: " + new Gson().toJson(data2));
                IncomingCallDataHolder incomingCallDataHolder = new IncomingCallDataHolder();
                incomingCallDataHolder.setCallType(calType.getValue());
                incomingCallDataHolder.setCallData(data1);
                getContext().startActivity(new CallingIntent(getContext(), incomingCallDataHolder));
            }

            @Override
            public void onCallEstablished(AbsCallService.CallType callType, String channelId, boolean isGroup) {

            }

            @Override
            public void onCallInfoUpdated(AbsCallService.CallType callType, String info, boolean isCallProfileVisible, boolean showCallControls) {

            }

            @Override
            public void onCheckShowingRequestVideoCamera(boolean shouldShow,
                                                         AbsCallService.CallType callType,
                                                         User requester,
                                                         boolean isGroupCall) {

            }

            @Override
            public void onCallTimerUp(AbsCallService.CallType callType,
                                      CallTimerHelper.TimerType timerType,
                                      CallTimerHelper.StarterTimerType starterTimerType) {

            }
        });
    }

    public void onNavMenuChange(AppFeature.NavigationBarMenu menu) {
        mCurrentNavBarMenu = menu;
        switch (mCurrentNavBarMenu.getNavMenuScreen()) {
            case Wall:
            case Conversation:
            case Contact:
                // Search normal
                mSearchInputHint.set(mContext.getString(R.string.search_item_home_screen_input_hint));
                break;
            case Shop:
            case ShopCategory:
                // Search product
                mSearchInputHint.set(mContext.getString(R.string.search_item_home_screen_input_hint));
                break;
            case ShopDetail:
            case Chat:
                // Do nothing
                break;
        }
    }

    public ObservableField<String> getSearchInputHint() {
        return mSearchInputHint;
    }

    public void setSearchInputHint(String hint) {
        mSearchInputHint.set(hint);
    }

    public void setOnBadgeUpdateListener(OnBadgeUpdateListener badgeListener) {
        mBadgeUpdateListener = badgeListener;
    }

    public ObservableBoolean getShouldShowNotificationOption() {
        return mShouldShowNotificationOption;
    }

    private void checkToShowNotificationOption() {
        mShouldShowNotificationOption.set(!ApplicationHelper.isInVisitorMode(mContext));
    }

    public ObservableField<String> getErrorMessage() {
        return mErrorMessage;
    }

    public ObservableField<String> getErrorTitle() {
        return mErrorTitle;
    }

    public ObservableBoolean getIsFirstLogin() {
        return mIsFirstLogin;
    }

    public void reloadUserProfile() {
        mUser.set(SharedPrefUtils.getUser(getContext()));
        mUser.notifyChange();
        mIsFirstLogin.set(false);
    }

    private void bindNotificationBadge() {
        UserBadge userBadge = UserHelper.getUserBadge(getContext());
        if (userBadge != null) {
            mUserBadge.set(userBadge);
            mNotificationBadge.setBadgeValue(userBadge.getUnreadNotification());
        }
    }

    public void onImageProfileClick() {
        if (ApplicationHelper.isInVisitorMode(getContext())) {
            requestUserAuthentication(result -> new NavigateSellerStoreHelper(getContext(),
                    SharedPrefUtils.getUser(getContext()))
                    .openMyStore(true, mUser::set));
        } else {
            new NavigateSellerStoreHelper(getContext(),
                    SharedPrefUtils.getUser(getContext()))
                    .openMyStore(true, mUser::set);
        }
    }

    public void requestUserAuthentication(OnCompleteListener<Object> listener) {
        if (mLister != null) {
            mLister.showLoginSignupBottomSheet(result -> {
                mUser.set(SharedPrefUtils.getUser(HomeFragmentViewModel.this.getContext()));
                mIsFirstLogin.set(false);
                HomeFragmentViewModel.this.checkToShowNotificationOption();
                if (listener != null) {
                    listener.onComplete(result);
                }
            });
        }
    }

    public void onApplicationIconClicked() {
        if (NetworkStateUtil.isNetworkAvailable(mContext)) {
            AppFeature.NavigationBarMenu navigationBarMenu = mApplicationModeIcon.get();
            if (navigationBarMenu != null) {
                if (navigationBarMenu.getNavMenuButton() == AppFeature.NavMenuButton.NormalMode) {
                    if (mLister != null) {
                        mLister.onSwitchApplicationMode(false);
                    }
                } else if (navigationBarMenu.getNavMenuButton() == AppFeature.NavMenuButton.ExpressMode) {
                    if (mLister != null) {
                        mLister.onSwitchApplicationMode(true);
                    }
                }
            }
        } else {
            showSnackBar(mContext.getString(R.string.error_internet_connection_description), true);
        }
    }

    public final void onNotificationButtonClick() {
        if (ApplicationHelper.isInVisitorMode(getContext())) {
            if (mLister != null) {
                mLister.showLoginSignupBottomSheet(result -> {
                    mUser.set(SharedPrefUtils.getUser(getContext()));
                    mIsFirstLogin.set(false);
                    if (getContext() instanceof AbsBaseActivity) {
                        ((AbsBaseActivity) getContext()).startActivityForResult(new NotificationIntent(getContext()),
                                new ResultCallback() {
                                    @Override
                                    public void onActivityResultSuccess(int resultCode, Intent data) {
                                    }
                                });
                    }
                    new Handler().postDelayed(() -> mNotificationBadge.setBadgeValue(0), 100);
                });
            }
        } else {
            if (getContext() instanceof AbsBaseActivity) {
                ((AbsBaseActivity) getContext()).startActivityForResult(new NotificationIntent(getContext()),
                        new ResultCallback() {
                            @Override
                            public void onActivityResultSuccess(int resultCode, Intent data) {

                            }
                        });
            }
            new Handler().postDelayed(() -> mNotificationBadge.setBadgeValue(0), 100);
        }
    }

    public void onTitleClick() {
        if (mCurrentNavBarMenu != null) {
            if (mCurrentNavBarMenu.getNavMenuScreen() == AppFeature.NavMenuScreen.Shop ||
                    mCurrentNavBarMenu.getNavMenuScreen() == AppFeature.NavMenuScreen.ShopCategory) {
                // If user is currently on shop/multiShop screen, when user tap search bar, open search
                // related to product
                SearchMessageIntent intent = new SearchMessageIntent(getContext());
                intent.setSearchType(SearchMessageIntent.SearchType.Product);
                getContext().startActivity(intent);
            } else {
                // Else, we'll just search item normally
                getContext().startActivity(new SearchMessageIntent(getContext()));
            }
        }
    }

    public void onSupportConversationClicked(String conversationID) {
        UserHelper.openCrispChatSupportScreen(mContext);
    }

    public void onShopDetailClicked(String shopId,
                                    boolean isDiscountOnly) {
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).startActivityForResult(
                    new ConciergeShopDetailIntent(mContext, shopId, isDiscountOnly),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {

                        }
                    });
        }
    }

    public void getUserBadge(final AbsBaseFragment fragment) {
        if (fragment != null && fragment.isFragmentStillInValidState()) {
            fragment.requireActivity().runOnUiThread(() -> new Handler().postDelayed(() -> {
                Observable<Response<UserBadge>> observable = mDataManager.getUerBadge();
                ResponseObserverHelper<Response<UserBadge>> helper = new ResponseObserverHelper<>(getContext(), observable);
                addDisposable(helper.execute(result -> {
                    if (fragment.isFragmentStillInValidState()) {
                        Timber.i("update refresh badge data.");
                        if (result.body() != null) {
                            UserHelper.saveUserBadge(mDataManager.getContext(), result.body());
                        }
                        mUserBadge.set(result.body());

                        if (mBadgeUpdateListener != null) {
                            mBadgeUpdateListener.onBadgeUpdate(mUserBadge.get());
                        }

                        //Update Notification badge
                        if (result.body() != null) {
                            mNotificationBadge.setBadgeValue(result.body().getUnreadNotification());
                        }
                    }
                }));
            }, REQUEST_BADGE_DELAY));
        }
    }

    private void checkToRequestGuestUserIfNecessary(boolean shouldShowLoading) {
        if (ApplicationHelper.isInVisitorMode(mContext) &&
                ApplicationHelper.shouldRequestVisitorUserData(mContext) &&
                !UserHelper.isGuestUserExist(getContext())) {
            if (shouldShowLoading) {
                showLoading();
            }
            Timber.i("Will request visitor user data.");
            Observable<Response<Object>> observable = mDataManager.getGuestUser();
            ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), observable);
            addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    hideLoading();
                    mErrorTitle.set(getContext().getString(R.string.post_error_message));
                    mErrorMessage.set(getContext().getString(R.string.error_general_description));
                }

                @Override
                public void onComplete(Response<Object> result) {
                    hideLoading();
                    mErrorTitle.set(null);
                    mErrorMessage.set(null);
                    if (mLister != null) {
                        mLister.onShouldInitTab();
                    }
                }
            }));
        } else {
            hideLoading();
            if (mLister != null) {
                mLister.onShouldInitTab();
            }
        }
    }

    public void checkToRequestMandatoryData() {
        AppFeature appFeature = ApplicationHelper.getAppFeature(getContext());
        if (appFeature == null) {
            showLoading();
            Timber.i("Will request app feature");
            Observable<Response<Object>> service = mDataManager.getRequestAppFeatureService();
            ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), service);
            addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    checkToRequestGuestUserIfNecessary(false);
                }

                @Override
                public void onComplete(Response<Object> result) {
                    checkToRequestGuestUserIfNecessary(false);
                }
            }));
        } else {
            checkToRequestGuestUserIfNecessary(true);
        }
    }

    public boolean isDisplayingError() {
        return !TextUtils.isEmpty(mErrorTitle.get()) && !TextUtils.isEmpty(mErrorMessage.get());
    }

    public interface OnQueryProductListener {
        void onQueryProductOption(boolean isEnable);
    }

    public interface OnBadgeUpdateListener {
        void onBadgeUpdate(UserBadge userBadge);
    }

    public interface HomeFragmentViewModelLister {
        void onShouldInitTab();

        void showLoginSignupBottomSheet(OnCompleteListener<Object> listener);

        void onSwitchApplicationMode(boolean isSwitchToExpressMode);
    }

    @BindingAdapter({"setTabHorizontalLinePosition", "baseLineHeight"})
    public static void setTabHorizontalLinePosition(View view, boolean enable, int baseLine) {
        if (enable) {
            view.post(() -> {
                int margin = (baseLine - view.getHeight()) / 2;
                Timber.i("baseLine: " + baseLine + ", view.getHeight(): " + view.getHeight() +
                        "margin: " + margin);
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        view.getContext().getResources().getDimensionPixelSize(R.dimen.tab_bottom_line_size));
                params.bottomMargin = margin;
                params.gravity = Gravity.BOTTOM;
                view.setLayoutParams(params);
            });
        }
    }

    @BindingAdapter({"setApplicationModeIcon"})
    public static void setApplicationModeIcon(ImageView icon, AppFeature.NavigationBarMenu applicationModeIcon) {
        if (applicationModeIcon == null) {
            icon.setVisibility(View.GONE);
        } else {
            if (applicationModeIcon.getNavMenuButton() == AppFeature.NavMenuButton.NormalMode) {
                icon.setVisibility(View.VISIBLE);
                icon.setImageResource(R.drawable.ic_delishop_logo);
            } else if (applicationModeIcon.getNavMenuButton() == AppFeature.NavMenuButton.ExpressMode) {
                icon.setVisibility(View.VISIBLE);
                icon.setImageResource(R.drawable.ic_delixpress_logo);
            } else {
                icon.setVisibility(View.GONE);
            }
        }
    }
}
