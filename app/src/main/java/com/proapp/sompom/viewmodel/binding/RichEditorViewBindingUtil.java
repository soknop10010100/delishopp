package com.proapp.sompom.viewmodel.binding;

import android.text.TextUtils;
import android.text.TextWatcher;

import androidx.databinding.BindingAdapter;

import com.example.usermentionable.model.UserMentionable;
import com.example.usermentionable.tokenization.interfaces.QueryTokenReceiver;
import com.example.usermentionable.widget.KeyboardInputEditor;
import com.example.usermentionable.widget.RichEditorView;

import java.util.List;

/**
 * Created by nuonveyo on 12/14/18.
 */

public final class RichEditorViewBindingUtil {
    private RichEditorViewBindingUtil() {

    }

    @BindingAdapter({"setRichEditorView", "setRichEditorTextWatcher", "setRichEditorKeyboardInputListener", "setRichEditorQueryTokenListener"})
    public static void bindView(RichEditorView richEditorView,
                                String text,
                                TextWatcher textWatcher,
                                KeyboardInputEditor.KeyBoardInputCallbackListener keyBoardInputCallbackListener,
                                QueryTokenReceiver queryTokenReceiver) {
        richEditorView.setText(text, false);
        richEditorView.setOnKeyboardInputCallback(keyBoardInputCallbackListener);
        richEditorView.setQueryTokenReceiver(queryTokenReceiver);
        richEditorView.addOnTextChangeListener(textWatcher);
    }

    @BindingAdapter("setMaxLengths")
    public static void setMaxLengths(RichEditorView richEditorView, int maxLength) {
        richEditorView.getMentionsEditText().setMaxLength(maxLength);
    }

    @BindingAdapter({"setRichEditorViewList", "isShowListPopupWindow"})
    public static void setRichEditorViewList(RichEditorView richEditorView, List<UserMentionable> mentionableList, boolean isShowListPopupWindow) {
        richEditorView.setListPopupWindow(isShowListPopupWindow, mentionableList);
    }

    @BindingAdapter("setRenderHashTag")
    public static void setRichEditorViewList(RichEditorView richEditorView, String tokenString) {
        if (!TextUtils.isEmpty(tokenString)) {
            richEditorView.checkToRenderHashTag(tokenString);
        }
    }

    @BindingAdapter({"setHashtagTextColor",
            "setHashtagTextBackgroundColor",
            "setSelectedHashtagTextColor",
            "setSelectedHashtagTextBackgroundColor"})
    public static void setHasTagColor(RichEditorView richEditorView,
                                      int hashtagTextColor,
                                      int hashtagTextBackgroundColor,
                                      int selectedHashtagTextColor,
                                      int selectedHashtagTextBackgroundColor) {
//        richEditorView.setHashTagRenderColor(hashtagTextColor,
//                hashtagTextBackgroundColor,
//                selectedHashtagTextColor,
//                selectedHashtagTextBackgroundColor);
    }
}
