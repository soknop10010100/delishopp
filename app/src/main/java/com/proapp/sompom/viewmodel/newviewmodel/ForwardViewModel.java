package com.proapp.sompom.viewmodel.newviewmodel;

import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.AbsChatBinder;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.chat.listener.GlobalChatListener;
import com.proapp.sompom.database.LinkPreviewDb;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.DelayWatcher;
import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.listener.ConversationDataAdaptive;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.DefaultForwardResult;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.emun.RoundCornerType;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ForwardDataManager;
import com.proapp.sompom.utils.ConversationUtil;
import com.proapp.sompom.utils.GlideLoadUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.SpecialTextRenderUtils;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;
import com.proapp.sompom.widget.RoundedImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by He Rotha on 9/13/18.
 */
public class ForwardViewModel extends AbsLoadingViewModel implements GlobalChatListener {

    public final ObservableField<String> mForwardChat = new ObservableField<>();
    public final ObservableField<String> mImageCount = new ObservableField<>();
    public final ObservableField<String> mImageUrl = new ObservableField<>();
    public final ObservableBoolean mIsFromOutsideApp = new ObservableBoolean();
    public final ObservableBoolean mIsVisibleCounter = new ObservableBoolean(false);
    public final ObservableField<String> mLinkTitle = new ObservableField<>();
    public final ObservableField<String> mLink = new ObservableField<>();
    public final ObservableBoolean mIsDownloadingLinkPreview = new ObservableBoolean();
    public final ObservableBoolean mIsAudioMedia = new ObservableBoolean();
    public final ObservableBoolean mIsVideoMedia = new ObservableBoolean(false);
    private ObservableField<String> mFileName = new ObservableField<>();
    private ObservableField<String> mFileSize = new ObservableField<>();

    private final List<Media> mForwardProductMedia;
    private final ForwardDataManager mDataManager;
    private final OnViewModelCallback mCallback;
    private final List<ConversationDataAdaptive> mCachePeople = new ArrayList<>();
    private final List<ConversationDataAdaptive> mCacheSuggestion = new ArrayList<>();
    private final List<ConversationDataAdaptive> mCacheGroup = new ArrayList<>();
    private AbsChatBinder mAbsChatBinder;
    private String mUrl;
    private Chat mChat;

    public ForwardViewModel(AbsBaseActivity context,
                            Chat chat,
                            String forwardChat,
                            List<Media> forwardProductMedia,
                            boolean isFromOutSideApp,
                            ForwardDataManager manager,
                            OnViewModelCallback callback) {
        super(context);
        mChat = chat;
        Timber.i("Meta preview: " + new Gson().toJson(mChat.getMetaPreview()));
        setShowKeyboardWhileLoadingScreen();
        mIsFromOutsideApp.set(isFromOutSideApp);
        mForwardChat.set(forwardChat);
        mForwardProductMedia = forwardProductMedia;

        if (mForwardProductMedia != null && !mForwardProductMedia.isEmpty()) {
            MediaType mediaType = mForwardProductMedia.get(0).getType();
            if (mediaType == MediaType.IMAGE || mediaType == MediaType.GIF || mediaType == MediaType.VIDEO) {
                mImageUrl.set(getMediaUrl(mForwardProductMedia.get(0)));
                mImageCount.set(String.valueOf(mForwardProductMedia.size()));
                mIsVisibleCounter.set(mForwardProductMedia.size() > 1);
                if (mediaType == MediaType.VIDEO && mForwardProductMedia.size() == 1) {
                    mIsVideoMedia.set(true);
                }
            } else if (mediaType == MediaType.AUDIO) {
                mIsAudioMedia.set(true);

            } else if (mediaType == MediaType.TENOR_GIF) {
                mImageUrl.set(mForwardProductMedia.get(0).getUrl());
            } else if (mediaType == MediaType.FILE) {
                mFileName.set(mForwardProductMedia.get(0).getFileName());
                mFileSize.set(ChatHelper.getDisplaySizeWithFormat(mForwardProductMedia.get(0).getSize()));
            }
        }
        mDataManager = manager;
        mCallback = callback;
        onRetryClick();
        context.addOnServiceListener(binder -> {
            mAbsChatBinder = binder;
            mAbsChatBinder.addGlobalChatListener(this);
        });
    }

    private String getMediaUrl(Media media) {
        if (!TextUtils.isEmpty(media.getThumbnail())) {
            return media.getThumbnail();
        }

        return media.getUrl();
    }

    public ObservableField<String> getFileName() {
        return mFileName;
    }

    public ObservableField<String> getFileSize() {
        return mFileSize;
    }

    public void setDisplayLink(String url, LinkPreviewModel linkPreviewModel) {
        mIsFromOutsideApp.set(true);
        mIsDownloadingLinkPreview.set(false);
        mForwardChat.set("");
        mUrl = url;
        mImageUrl.set(linkPreviewModel.getImage());
        mLinkTitle.set(LinkPreviewRetriever.getPreviewTitle(linkPreviewModel));
        mLink.set(linkPreviewModel.getLink());
    }

    public TextWatcher getTextWatcher() {
        return new DelayWatcher(true) {
            @Override
            public void onTextChanged(String text) {
                if (TextUtils.isEmpty(text)) {
                    mCallback.onSearchSuccess(null, null);
                    mCallback.onLoadSuggestPeopleComplete(mCacheSuggestion);
                    mCallback.onLoadGroupComplete(mCacheGroup, mDataManager.isCanLoadMore());
                    mCallback.onLoadPeopleComplete(mCachePeople, mDataManager.isCanLoadMore());
                    hideLoading();
                } else {
                    clearDisposable();
                    Observable<Response<DefaultForwardResult>> call = mDataManager.searchPeople(text);
                    ResponseObserverHelper<Response<DefaultForwardResult>> helper = new ResponseObserverHelper<>(getContext(), call);
                    addDisposable(helper.execute(new OnCallbackListener<Response<DefaultForwardResult>>() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                            mCallback.onSearchSuccess(null, null);
                            hideLoading();
                        }

                        @Override
                        public void onComplete(Response<DefaultForwardResult> result) {
                            validateUserList(result.body().getSearchResultUser());
                            hideLoading();
                            if (isResultEmpty(result.body())) {
                                showError(getContext().getString(R.string.forward_empty_message), false);
                            } else {
                                mCallback.onSearchSuccess(new ArrayList<>(result.body().getSearchResultGroup()),
                                        new ArrayList<>(result.body().getSearchResultUser()));
                            }
                        }
                    }));
                }
            }

            @Override
            public void onTyping(String text) {
                showLoading();
            }
        };
    }

    private boolean isResultEmpty(DefaultForwardResult result) {
        return (result.getSearchResultGroup() == null ||
                result.getSearchResultGroup().isEmpty()) &&
                (result.getSearchResultUser() == null ||
                        result.getSearchResultUser().isEmpty());
    }

    public void requestLinkPreview(String url) {
        mIsDownloadingLinkPreview.set(true);
        mForwardChat.set(null);
        Observable<Response<LinkPreviewModel>> call = mDataManager.getPreviewLink(url);
        ResponseObserverHelper<Response<LinkPreviewModel>> helper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<Response<LinkPreviewModel>>() {
            @Override
            public void onComplete(Response<LinkPreviewModel> result) {
                LinkPreviewModel linkPreviewModel = result.body();
                if (linkPreviewModel != null && linkPreviewModel.isValidPreviewData()) {
                    GlideLoadUtil.preCacheImage(getContext(), linkPreviewModel.getImage(), null);
                    mChat.setLinkPreviewModel(linkPreviewModel);
                    setDisplayLink(mChat.getContent(), mChat.getLinkPreviewModel());
                    linkPreviewModel.setId(mChat.getId());
                    linkPreviewModel.setResourceContent(mChat.getContent());
                    linkPreviewModel.setLinkDownloaded(true);
                    LinkPreviewDb.save(getContext(), linkPreviewModel, true);
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {

            }
        }));
    }

    public void onBackPress() {
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).finish();
        }
    }

    @Override
    public void onRetryClick() {
        super.onRetryClick();

        Observable<Response<DefaultForwardResult>> suggestCall = mDataManager.getSuggestion();
        ResponseObserverHelper<Response<DefaultForwardResult>> suggestHelper =
                new ResponseObserverHelper<>(getContext(), suggestCall);
        addDisposable(suggestHelper.execute(new OnCallbackListener<Response<DefaultForwardResult>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                showError(ex.toString());
            }

            @Override
            public void onComplete(Response<DefaultForwardResult> result) {
                validateUserList(result.body().getSuggestedUserList());
                validateUserList(result.body().getPeople());

                //Suggested
                mCacheSuggestion.clear();
                mCacheSuggestion.addAll(result.body().getSuggestedUserList());
                mCallback.onLoadSuggestPeopleComplete(mCacheSuggestion);

                //Group
                mCacheGroup.clear();
                mCacheGroup.addAll(result.body().getGroupList());
                mCallback.onLoadGroupComplete(mCacheGroup, mDataManager.isCanLoadMore());

                //People
                mCachePeople.clear();
                mCachePeople.addAll(result.body().getPeople());
                mCallback.onLoadPeopleComplete(mCachePeople, mDataManager.isCanLoadMore());
                hideLoading();
            }
        }));
    }

    public String onForwardClick(ConversationDataAdaptive data) {
        final User user;

        if (data instanceof Conversation) {
            user = ((Conversation) data).getOneToOneRecipient(getContext());
        } else if (data instanceof User) {
            user = (User) data;
        } else {
            user = null;
        }

        if (data instanceof Conversation) {
            return performForwardMessage(data, user, ((Conversation) data).getGroupId(), null);
        } else {
            if (user != null) {
                String channelId = ConversationUtil
                        .getConversationId(user.getId(), SharedPrefUtils.getUserId(getContext()));
                return performForwardMessage(data, user, channelId, null);
            }
        }

        return null;
    }

    private String performForwardMessage(ConversationDataAdaptive adaptive,
                                         User user,
                                         String channelId,
                                         Product product) {
        String messageId = null;
        if (mForwardProductMedia != null && !mForwardProductMedia.isEmpty()) {
            Chat chat = Chat.getNewChat(getContext(), user, product);
            chat.setMediaList(mForwardProductMedia);
            setMetaPreview(chat);
            chat.setChannelId(channelId);
            chat.setGroup(adaptive instanceof Conversation);
            onSendForwardMessage(user, chat);
            messageId = chat.getId();
        }

        if (!TextUtils.isEmpty(mForwardChat.get())) {
            Chat chat = Chat.getNewChat(getContext(), user, product);
            chat.setContent(mForwardChat.get());
            setMetaPreview(chat);
            chat.setChannelId(channelId);
            chat.setGroup(adaptive instanceof Conversation);
            onSendForwardMessage(user, chat);

            if (TextUtils.isEmpty(messageId)) {
                messageId = chat.getId();
            }
        }
        if (!TextUtils.isEmpty(mUrl)) {
            Chat chat = Chat.getNewChat(getContext(), user, product);
            chat.setContent(mUrl);
            setMetaPreview(chat);
            chat.setChannelId(channelId);
            chat.setGroup(adaptive instanceof Conversation);
            onSendForwardMessage(user, chat);

            if (TextUtils.isEmpty(messageId)) {
                messageId = chat.getId();
            }
        }

        return messageId;
    }

    private void onSendForwardMessage(User user, Chat chat) {
        if (!chat.isGroup() && !TextUtils.isEmpty(chat.getContent())) {
            /*
            In case of sending forward message to one to one conversation, we will check to remove
            mention user name format from chat content since now there is no support of mention user
            for one to one conversation.
             */
            Spannable spannable = SpecialTextRenderUtils.renderMentionUser(getContext(),
                    chat.getContent(),
                    false,
                    false,
                    -1,
                    -1,
                    -1,
                    false,
                    null);
            if (spannable != null) {
                chat.setContent(spannable.toString());
            }
        }
//        Timber.i("onSendForwardMessage: " + new Gson().toJson(chat));
        mAbsChatBinder.sendMessage(user, chat);
    }

    private void setMetaPreview(Chat chat) {
        if (mChat.getLinkPreviewModel() != null) {
            chat.setMetaPreview(Collections.singletonList(mChat.getLinkPreviewModel()));
        }
    }

    @Override
    public void onMessageUpdated(Chat message, MessageState state) {
        mCallback.onForwardComplete(message.getId(), MessageState.SENT);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAbsChatBinder != null) {
            mAbsChatBinder.removeGlobalChatListener(this);
        }
    }

    private void validateUserList(List<User> userList) {
        String currentUserId = SharedPrefUtils.getUserId(getContext());
        for (int i = userList.size() - 1; i >= 0; i--) {
            if (userList.get(i).getId().matches(currentUserId)) {
                userList.remove(i);
                break;
            }
        }
    }

    @BindingAdapter("setRoundAllCorner")
    public static void setCornerPreviewImage(RoundedImageView imageView, int corner) {
        imageView.setCornerType(RoundCornerType.Single);
        imageView.setCircle(false);
        imageView.setMediumRadius(corner);
    }

    public interface OnViewModelCallback {

        void onLoadSuggestPeopleComplete(List<ConversationDataAdaptive> suggest);

        void onLoadPeopleComplete(List<ConversationDataAdaptive> people, boolean isLoadMore);

        void onLoadGroupComplete(List<ConversationDataAdaptive> group, boolean isLoadMore);

        void onSearchSuccess(List<ConversationDataAdaptive> groupResult, List<ConversationDataAdaptive> peopleResult);

        void onForwardComplete(String messageId, MessageState messageState);
    }
}
