package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.TextUtils;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.intent.ConciergeOrderHistoryDetailIntent;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.utils.DateUtils;
import com.resourcemanager.helper.LocaleManager;

/**
 * Created by Veasna Chhom on 20/9/21.
 */
public class ItemConciergeOrderTrackingViewModel extends AbsConciergeOrderHistoryViewModel {

    private final ObservableField<String> mOrderTitle = new ObservableField<>();
    private final ObservableField<String> mOrderLogo = new ObservableField<>();
    private final ObservableField<String> mOrderAddress = new ObservableField<>();

    private final ObservableBoolean mOrderInfoVisibility = new ObservableBoolean();
    private final ObservableField<String> mOrderDate = new ObservableField<>();
    private final ObservableField<String> mOrderNumber = new ObservableField<>();

    private final ObservableField<String> mOrderStatus = new ObservableField<>();

    private final ObservableField<String> mOrderTotalPrice = new ObservableField<>();

    public ItemConciergeOrderTrackingViewModel(Context context,
                                               ConciergeOrder order,
                                               AbsConciergeOrderHistoryViewModelListener absListener) {
        super(context, order, absListener);
        binData(context);
    }

    public ObservableField<String> getOrderLogo() {
        return mOrderLogo;
    }

    public ObservableField<String> getOrderTitle() {
        return mOrderTitle;
    }

    public ObservableField<String> getOrderAddress() {
        return mOrderAddress;
    }

    public ObservableField<String> getOrderDate() {
        return mOrderDate;
    }

    public ObservableBoolean getOrderInfoVisibility() {
        return mOrderInfoVisibility;
    }

    public ObservableField<String> getOrderNumber() {
        return mOrderNumber;
    }

    public ObservableField<String> getOrderStatus() {
        return mOrderStatus;
    }

    public ObservableField<String> getOrderTotalPrice() {
        return mOrderTotalPrice;
    }

    private void binData(Context context) {
        mOrderTitle.set(mOrder.getOrderTitle());
        mOrderAddress.set(mOrder.getOrderDescription());
        mOrderLogo.set(mOrder.getOrderProfile());

        mOrderInfoVisibility.set(!TextUtils.isEmpty(mOrder.getOrderNumber()) || !TextUtils.isEmpty(mOrder.getOrderStatus().getValue()));
        mOrderNumber.set("#" + mOrder.getOrderNumber());
        mOrderStatus.set(mOrder.getOrderStatus().getLabel(mContext));

        mOrderTotalPrice.set(ConciergeHelper.getDisplayPrice(mContext, mOrder.getGrandTotal()));
        if (mOrder.getCreatedAt() != null) {
            String dateWithFormat = DateUtils.getDateWithFormat(context,
                    mOrder.getCreatedAt().getTime(),
                    DateUtils.DATE_FORMAT_23,
                    LocaleManager.getLocale(context.getResources()));
            mOrderDate.set(dateWithFormat);
        }
    }

    public void onItemClicked() {
        mContext.startActivity(new ConciergeOrderHistoryDetailIntent(mContext,
                mOrder.getId(),
                mOrder.getOrderNumber()));
    }
}
