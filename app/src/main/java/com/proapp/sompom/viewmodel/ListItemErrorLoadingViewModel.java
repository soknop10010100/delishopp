package com.proapp.sompom.viewmodel;

import android.content.Context;

import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.model.emun.ErrorLoadingType;

/**
 * Created by nuonveyo on 8/27/18.
 */

public class ListItemErrorLoadingViewModel extends AbsLoadingViewModel {
    private OnClickListener mOnClickListener;

    public ListItemErrorLoadingViewModel(Context context,
                                         ErrorLoadingType errorLoadingType,
                                         String errorMessage,
                                         boolean showRetry,
                                         OnClickListener onClickListener) {
        super(context);
        mErrorLoadingType.set(errorLoadingType);
        mOnClickListener = onClickListener;
        showError(errorMessage);
        mIsShowButtonRetry.set(showRetry);
    }

    @Override
    public void onRetryClick() {
        if (mOnClickListener != null) {
            mOnClickListener.onClick();
        }
    }
}
