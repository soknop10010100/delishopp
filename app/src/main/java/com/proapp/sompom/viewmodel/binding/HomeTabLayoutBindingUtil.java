package com.proapp.sompom.viewmodel.binding;

import androidx.databinding.BindingAdapter;

import com.proapp.sompom.model.result.UserBadge;
import com.proapp.sompom.widget.HomeTabLayout;

/**
 * Created by nuonveyo
 * on 2/13/19.
 */

public final class HomeTabLayoutBindingUtil {
    private HomeTabLayoutBindingUtil() {

    }

    @BindingAdapter("setBadgeValue")
    public static void setBadgeValue(HomeTabLayout tabLayout, UserBadge userBadge) {
        if (userBadge != null) {
            tabLayout.setBadgeValue(userBadge);
        }
    }
}
