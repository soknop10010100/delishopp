package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.GroupMediaSection;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.GroupDetailMediaDataManager;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Veasna Chhom on 7/24/20.
 */
public class GroupDetailFileViewModel extends AbsGroupDetailMediaViewModel<Media> {


    public GroupDetailFileViewModel(Context context,
                                    GroupDetailMediaDataManager dataManager,
                                    GroupDetailMediaViewModelListener<Media> listener) {
        super(context, dataManager, listener);
    }

    @Override
    public void getMediaDetail(boolean isLoadMore) {
        if (!mIsRefresh.get() && !isLoadMore) {
            showLoading();
        }
        Observable<Response<List<GroupMediaSection<Media>>>> observable = mDataManager.getGroupFileDetailObservable(isLoadMore);
        ResponseObserverHelper<Response<List<GroupMediaSection<Media>>>> helper = new ResponseObserverHelper<>(mDataManager.mContext, observable);
        helper.execute(new OnCallbackListener<Response<List<GroupMediaSection<Media>>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsRefresh.set(false);
                hideLoading();
                showError(ex.getMessage());
                if (mListener != null) {
                    mListener.onLoadFailed(ex, isLoadMore, mDataManager.getGroupMediaDetailType());
                }
            }

            @Override
            public void onComplete(Response<List<GroupMediaSection<Media>>> result) {
                mIsRefresh.set(false);
                hideLoading();
                if (!isLoadMore && (result.body() == null || result.body().isEmpty())) {
                    showNoItemError(getContext().getString(R.string.error_no_data));
                } else {
                    if (mListener != null) {
                        mListener.onLoadDataSuccess(result.body(),
                                mDataManager.isCanLoadMore(),
                                isLoadMore,
                                mDataManager.getGroupMediaDetailType());
                    }
                }
            }
        });
    }
}
