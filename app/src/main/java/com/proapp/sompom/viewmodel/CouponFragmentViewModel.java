package com.proapp.sompom.viewmodel;

import android.content.Context;

import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeCoupon;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.CouponDataManager;
import com.proapp.sompom.utils.AttributeConverter;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Chhom Veasna on 7/21/22.
 */

public class CouponFragmentViewModel extends AbsLoadingViewModel {

    private final ObservableInt mApplyCouponButtonTextColor = new ObservableInt();
    private final ObservableInt mApplyCouponButtonBackground = new ObservableInt();
    private final CouponDataManager mDataManager;
    private final CouponFragmentViewModelListener mListener;
    private boolean mIsApplyButtonEnabled;

    public CouponFragmentViewModel(Context context, CouponDataManager dataManager, CouponFragmentViewModelListener listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
        setEnableApplyButton(false);
        getData();
    }

    public ObservableInt getApplyCouponButtonBackground() {
        return mApplyCouponButtonBackground;
    }

    public ObservableInt getApplyCouponButtonTextColor() {
        return mApplyCouponButtonTextColor;
    }

    public void onApplyClicked() {
        if (mIsApplyButtonEnabled) {
            if (mListener != null) {
                mListener.onApplyClicked();
            }
        }
    }

    @Override
    public void onRetryClick() {
        getData();
    }

    private void getData() {
        showLoading();
        Observable<Response<List<ConciergeCoupon>>> ob = mDataManager.getCouponListService();
        ResponseObserverHelper<Response<List<ConciergeCoupon>>> helper = new ResponseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<ConciergeCoupon>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showError(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<List<ConciergeCoupon>> result) {
                hideLoading();
                if (result.body() == null || result.body().isEmpty()) {
                    showError(mContext.getString(R.string.error_no_data), true);
                } else {
                    if (mDataManager.isHasFoundPreviousSelection()) {
                        checkToEnableApplyButton(true);
                    }
                    if (mListener != null) {
                        mListener.onDataLoadSuccess(result.body());
                    }
                }
            }
        }));
    }

    public void checkToEnableApplyButton(boolean isAnyOptionSelected) {
        if (mIsApplyButtonEnabled != isAnyOptionSelected) {
            mIsApplyButtonEnabled = isAnyOptionSelected;
            setEnableApplyButton(mIsApplyButtonEnabled);
        }
    }

    private void setEnableApplyButton(boolean enable) {
        if (enable) {
            mApplyCouponButtonTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_apply_coupon_enable));
            mApplyCouponButtonBackground.set(AttributeConverter.convertAttrToColor(mContext, R.attr.common_primary_color));
        } else {
            mApplyCouponButtonTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_apply_coupon_disable));
            mApplyCouponButtonBackground.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_apply_coupon_background_disable));
        }
    }

    public interface CouponFragmentViewModelListener {
        void onDataLoadSuccess(List<ConciergeCoupon> data);

        void onApplyClicked();
    }
}
