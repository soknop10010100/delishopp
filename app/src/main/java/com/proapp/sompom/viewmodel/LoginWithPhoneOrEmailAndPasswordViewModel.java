package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.model.request.LoginBody;
import com.proapp.sompom.services.datamanager.LoginDataManager;

import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;

/**
 * Created by Veasna Chhom on 4/21/21.
 */
public class LoginWithPhoneOrEmailAndPasswordViewModel extends AbsLoginViewModel<LoginDataManager, AbsLoginViewModel.AbsLoginViewModelListener> {

    public LoginWithPhoneOrEmailAndPasswordViewModel(Context context, LoginDataManager dataManager, AbsLoginViewModelListener listener) {
        super(context, dataManager, listener);
        mPhoneNumberUtil = PhoneNumberUtil.createInstance(getContext());
    }

    @Override
    protected boolean shouldLoadUserDefaultCountryCode() {
        return true;
    }

    @Override
    protected boolean shouldEnableActionButton() {
        return (isPhoneNumberValid(mIdentifier.get()) || isIdentifierValidEmail(mIdentifier.get())) &&
                !TextUtils.isEmpty(mPassword.get());
    }

    @Override
    protected void onContinueButtonClicked() {
        if (!TextUtils.isEmpty(mValidaPhoneCountryInput) && !TextUtils.isEmpty(mValidPhoneInput)) {
            //Valid phone input
            requestLogin(mDataManager.getLoginWithPhoneService(getFullPhoneNumberFromInput(), mPassword.get()),
                    true);
        } else if (!TextUtils.isEmpty(mValidEmailInput)) {
            //Valid email input
            requestLogin(mDataManager.getLoginViaEmail(new LoginBody(mValidEmailInput, mPassword.get())),
                    false);
        }
    }
}
