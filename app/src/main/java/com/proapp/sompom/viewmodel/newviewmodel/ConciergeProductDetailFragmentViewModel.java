package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.intent.ConciergeProductDetailIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeBasketMenuItem;
import com.proapp.sompom.model.concierge.ConciergeComboChoiceSection;
import com.proapp.sompom.model.concierge.ConciergeComboItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItemManipulationInfo;
import com.proapp.sompom.model.emun.ConciergeMenuItemOptionType;
import com.proapp.sompom.model.emun.ConciergeMenuItemType;
import com.proapp.sompom.model.result.ConciergeMenuItemOption;
import com.proapp.sompom.model.result.ConciergeMenuItemOptionSection;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.fragment.AbsSupportShopCheckOutFragment;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ConciergeProductDetailFragmentDataManager;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.viewmodel.ConciergeSupportAddItemToCardViewModel;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 13/9/21.
 */

public class ConciergeProductDetailFragmentViewModel extends ConciergeSupportAddItemToCardViewModel<ConciergeProductDetailFragmentDataManager,
        ConciergeProductDetailFragmentViewModel.ConciergeProductDetailViewModelListener> {

    private final ObservableField<String> mProductName = new ObservableField<>();
    private final ObservableField<String> mProductPhoto = new ObservableField<>();
    private final ObservableField<String> mProductDescription = new ObservableField<>();
    private final ObservableBoolean mIsDeliverToProvince = new ObservableBoolean();
    private final ObservableField<String> mProductWeight = new ObservableField<>();
    private final ObservableField<String> mCounterDisplay = new ObservableField<>();
    private final ObservableField<Spannable> mTopPrice = new ObservableField<>();
    private final ObservableField<String> mBottomPrice = new ObservableField<>();
    private final ObservableBoolean mIsToolbarCollapsed = new ObservableBoolean();
    private final ObservableBoolean mIsOpenFromCombo = new ObservableBoolean();
    private final ObservableBoolean mAddButtonVisibility = new ObservableBoolean();
    private final ObservableInt mAddButtonBackgroundColor = new ObservableInt();
    private final ObservableInt mAddButtonIconColor = new ObservableInt();
    private final ObservableInt mPriceVisibility = new ObservableInt(View.INVISIBLE);
    private final ObservableField<ConciergeMenuItem> mProduct = new ObservableField<>();

    private ConciergeMenuItem mMenuItem;
    private ConciergeMenuItemType mMenuItemType;
    private String mMenuId;
    private int mCounter = 1;
    private int mMinAmount = 1;
    private double mCurrentPrice = 0;
    private ConciergeMenuItem mPassedOrderItem;
    private int mPositionInBasket;
    private boolean mIsInExpressMode;
    private boolean mIsInEditMode;
    private boolean mHasModifiedProductCount;

    public ConciergeProductDetailFragmentViewModel(Context context,
                                                   boolean isOpenFromCombo,
                                                   String menuItemId,
                                                   ConciergeMenuItem passedOrderItem,
                                                   int positionInBasket,
                                                   ConciergeProductDetailFragmentDataManager dataManager,
                                                   ConciergeProductDetailViewModelListener listener) {
        super(context, listener, dataManager);
        mMenuId = menuItemId;
        mPassedOrderItem = passedOrderItem;
        mPositionInBasket = positionInBasket;
        mDataManager = dataManager;
        mListener = listener;
        mIsOpenFromCombo.set(isOpenFromCombo);
        mIsInExpressMode = ApplicationHelper.isInExpressMode(context);
        if (mPassedOrderItem != null) {
            ConciergeMenuItem existingProductInCart = ConciergeCartHelper.getExistingProductInCart(mPassedOrderItem);
            if (existingProductInCart != null) {
                mIsInEditMode = existingProductInCart.getProductCount() > 0 && !TextUtils.isEmpty(existingProductInCart.getBasketItemId());
                //Must set basket item id for update basket item with API.
                mPassedOrderItem.setBasketItemId(existingProductInCart.getBasketItemId());
                mPassedOrderItem.setProductCount(existingProductInCart.getProductCount());
            }
        }
    }

    public ObservableBoolean getAddButtonVisibility() {
        return mAddButtonVisibility;
    }

    public ObservableField<ConciergeMenuItem> getProduct() {
        return mProduct;
    }

    public void initComboData(ConciergeComboItem comboItem) {
        mMenuItem = comboItem.convertToMenuItem();
        initData();
        if (mListener != null) {
            mListener.onLoadDetailSuccess(mMenuItem);
        }
    }

    public ConciergeMenuItem getMenuItem() {
        return mMenuItem;
    }

    public void initData() {
        mProduct.set(mMenuItem);
        shouldEnableAddToCartButton();
        setMenuItemType(mMenuItem.getType());
        setProductName(mMenuItem.getName());
        setProductPhoto(mMenuItem.getPicture());
        setIsDeliverToProvince(mMenuItem.isDeliverToProvince());
        setProductDescription(mMenuItem.getDescription());
        mProductWeight.set(mMenuItem.getWeight());
        setCounterDisplay(String.valueOf(1));
        // Update bottom price view
        updatePriceDisplay();
        if (mMenuItem.getPrice() > 0 || mMenuItem.getOriginalPrice() > 0) {
            // Set the top price view, which will stay the same unlike the bottom display
            setTopPrice(ConciergeHelper.buildShopItemNormalOrDiscountPriceDisplay(mContext,
                    mMenuItem,
                    AttributeConverter.convertAttrToColor(mContext, R.attr.common_primary_color),
                    AttributeConverter.convertAttrToColor(mContext, R.attr.shop_item_detail_form_strikethroug_price),
                    false));
        }
        mAddButtonVisibility.set(ConciergeHelper.isAddProuctButtonVisible(mMenuItem));
        bindExistingItemCount();
    }

    @Override
    public void onRetryClick() {
        if (!mIsOpenFromCombo.get()) {
            getProductDetail(mMenuId);
        }
    }

    public void getProductDetail(String menuItemId) {
        showLoading();
        Observable<Response<ConciergeMenuItem>> ob = mDataManager.getData(menuItemId);
        ResponseObserverHelper<Response<ConciergeMenuItem>> helper = new ResponseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeMenuItem>>() {
            @Override
            public void onComplete(Response<ConciergeMenuItem> result) {
                hideLoading();
                if (result.isSuccessful()) {
                    mMenuItem = result.body();
                    initData();
                    if (mListener != null) {
                        mListener.onLoadDetailSuccess(result.body());
                    }
                } else {
                    showError(mContext.getString(R.string.error_general_description));
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showError(ex.getMessage());
            }
        }));
    }

    public ObservableField<String> getProductWeight() {
        return mProductWeight;
    }

    public String getMenuId() {
        return mMenuId;
    }

    public void setMenuId(String menuId) {
        this.mMenuId = menuId;
    }

    public ObservableField<String> getProductName() {
        return mProductName;
    }

    public void setProductName(String productName) {
        mProductName.set(productName);
    }

    public ConciergeMenuItemType getMenuItemType() {
        return mMenuItemType;
    }

    public void setMenuItemType(ConciergeMenuItemType menuItemType) {
        this.mMenuItemType = menuItemType;
    }

    public ObservableField<String> getProductPhoto() {
        return mProductPhoto;
    }

    public void setProductPhoto(String productPhoto) {
        mProductPhoto.set(productPhoto);
    }

    public ObservableField<String> getProductDescription() {
        return mProductDescription;
    }

    public void setProductDescription(String productDescription) {
        mProductDescription.set(productDescription);
    }

    public ObservableBoolean getIsDeliverToProvince() {
        return mIsDeliverToProvince;
    }

    public void setIsDeliverToProvince(boolean isDeliverToProvince) {
        mIsDeliverToProvince.set(isDeliverToProvince);
    }

    public ObservableField<String> getCounterDisplay() {
        return mCounterDisplay;
    }

    public void setCounterDisplay(String amount) {
        mCounterDisplay.set(amount);
    }

    public ObservableField<Spannable> getTopPrice() {
        return mTopPrice;
    }

    public void setTopPrice(Spannable topPrice) {
        mTopPrice.set(topPrice);
    }

    public ObservableField<String> getBottomPrice() {
        return mBottomPrice;
    }

    public void setBottomPrice(String price) {
        mBottomPrice.set(price);
    }

    public ObservableBoolean isOpenFromCombo() {
        return mIsOpenFromCombo;
    }

    public void setIsOpenFromCombo(boolean isOpenFromCombo) {
        mIsOpenFromCombo.set(isOpenFromCombo);
    }

    public ObservableBoolean isToolbarCollapsed() {
        return mIsToolbarCollapsed;
    }

    public void setIsToolbarCollapsed(boolean isToolbarCollapsed) {
        mIsToolbarCollapsed.set(isToolbarCollapsed);
    }

    public ConciergeMenuItem getShop() {
        return mMenuItem;
    }

    public void setProduct(ConciergeMenuItem menuItem) {
        this.mMenuItem = menuItem;
    }

    public ObservableInt getAddButtonBackgroundColor() {
        return mAddButtonBackgroundColor;
    }

    public ObservableInt getAddButtonIconColor() {
        return mAddButtonIconColor;
    }

    public ObservableInt getPriceVisibility() {
        return mPriceVisibility;
    }

    public void increaseItemCount() {
        mHasModifiedProductCount = true;
        mCounter++;
        mCounterDisplay.set(String.valueOf(mCounter));
        mMenuItem.setProductCount(mCounter);
        updatePriceDisplay();
    }

    private void bindExistingItemCount() {
        if (mPassedOrderItem != null && mPassedOrderItem.getProductCount() > 0) {
            mCounter = mPassedOrderItem.getProductCount();
            mCounterDisplay.set(String.valueOf(mCounter));
            mMenuItem.setProductCount(mCounter);
            updatePriceDisplay();
        }
    }

    public void decreaseItemCount() {
        if (mCounter > mMinAmount) {
            mHasModifiedProductCount = true;
            mCounter--;
            mCounterDisplay.set(String.valueOf(mCounter));
            mMenuItem.setProductCount(mCounter);
            updatePriceDisplay();
        }
    }

    public boolean isHasModifiedProductCount() {
        return mHasModifiedProductCount;
    }

    public void onAddToCartClicked() {
        if (!checkRequiredOptionSelected()) {
            // Show warning toast here if required option was not selected
            showSnackBar(mContext.getString(R.string.shop_item_detail_option_selection_required),
                    true);

            if (mListener != null) {
                mListener.onRequiredItemNotSelected();
            }
            return;
        }

        // If open from a combo item, return the result of the selection and close the screen
        if (isOpenFromCombo().get()) {
            if (mContext instanceof AbsBaseActivity) {
                Intent intent = new Intent();
                intent.putExtra(ConciergeProductDetailIntent.MENU_ITEM, mMenuItem);
                ((AbsBaseActivity) mContext).setResult(AppCompatActivity.RESULT_OK, intent);
                ((AbsBaseActivity) mContext).finish();
            }
            return;
        }

        if (ApplicationHelper.isInVisitorMode(getContext())) {
            mListener.onRequireLogin();
            return;
        }

        if (mIsInExpressMode) {
            if (ConciergeCartHelper.isItemFromTheSameSupplierAsItemInBasket(mMenuItem.getSupplier())) {
                proceedAnAddToCartClicked();
            } else {
                mListener.showAddItemFromDifferentShopWarningPopup(this,
                        new OnCallbackListener<Response<SupportConciergeCustomErrorResponse>>() {
                            @Override
                            public void onFail(ErrorThrowable ex) {

                            }

                            @Override
                            public void onComplete(Response<SupportConciergeCustomErrorResponse> result) {
                                onAddToCartClicked();
                            }
                        });
            }
        } else {
            proceedAnAddToCartClicked();
        }
    }

    private void proceedAnAddToCartClicked() {
        //If it is in edit mode, we need to remove old item first and then add new item to card.
        if (mIsInEditMode && mPassedOrderItem != null) {
            if (mMenuItem.getProductCount() == 0) {
                mMenuItem.setProductCount(mCounter);
            }
            requestToUpdateOrRemoveItemInBasket(mMenuItem, true);
        } else {
            requestAddItemToBasket(mMenuItem, true);
        }
    }

    private void updateLocalBasketItem(ConciergeMenuItem item) {
        Timber.i("updateLocalBasketItem: " + new Gson().toJson(item));
        Observable<ConciergeMenuItemManipulationInfo> ob = ConciergeCartHelper.modifyItemCount(item, item.getProductCount(), false);
        BaseObserverHelper<ConciergeMenuItemManipulationInfo> helper = new BaseObserverHelper<>(mContext, ob);
        addDisposable(helper.execute(new OnCallbackListener<ConciergeMenuItemManipulationInfo>() {
            @Override
            public void onComplete(ConciergeMenuItemManipulationInfo result) {
                hideLoading();
                Timber.i("UpdateLocalBasketItem success \n" + new Gson().toJson(result));
                Intent intent = new Intent(AbsSupportShopCheckOutFragment.CART_ITEM_ADDED_EVENT);
                intent.putExtra(AbsSupportShopCheckOutFragment.CART_EVENT_DATA, item);
                SendBroadCastHelper.verifyAndSendBroadCast(mContext, intent);

                if (mListener != null) {
                    mListener.onItemAddedToCart(item);
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(mContext.getString(R.string.error_general_description), true);
            }
        }));
    }

    public void requestAddItemToBasket(ConciergeMenuItem item, boolean showLoading) {
        //There must be one product count when add item to basket on server
        if (item.getProductCount() == 0) {
            item.setProductCount(1);
        }

        if (showLoading) {
            showLoading(true);
        }
        Observable<Response<ConciergeBasketMenuItem>> responseObservable = mDataManager.addItemToBasket(item);
        BaseObserverHelper<Response<ConciergeBasketMenuItem>> helper = new BaseObserverHelper<>(mContext, responseObservable);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeBasketMenuItem>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<ConciergeBasketMenuItem> result) {
                if (result.body() != null && result.body().isError()) {
                    hideLoading();
                    if (result.body().getDataType() == SupportConciergeCustomErrorResponse.ResponseDataType.PAYMENT_IN_PROCESS) {
                        if (mListener != null) {
                            mListener.onHandlePaymentInProgressError(ConciergeProductDetailFragmentViewModel.this,
                                    result.body().getErrorMessage(),
                                    item,
                                    false);
                        }
                    } else {
                        mListener.showOrderErrorPopup(mContext.getString(R.string.shop_request_add_item_to_basket_error, item.getName()),
                                null);
                    }
                } else {
                    //Must set basket item to menu item before saving to local basket for later use with update basket item
                    if (result.body() != null) {
                        mMenuItem.setBasketItemId(result.body().getId());
                    }
                    addItemToLocalBasket(mMenuItem, false);
                }
            }
        }));
    }

    public void requestToUpdateOrRemoveItemInBasket(ConciergeMenuItem item, boolean showLoading) {
        if (showLoading) {
            showLoading(true);
        }
        FlurryHelper.logEvent(mContext, FlurryHelper.UPDATE_PRODUCT_IN_CHECKOUT_CARD);
        Observable<Response<ConciergeBasketMenuItem>> responseObservable = mDataManager.updateOrRemoveItemInBasket(mPassedOrderItem.getBasketItemId(), item);
        BaseObserverHelper<Response<ConciergeBasketMenuItem>> helper = new BaseObserverHelper<>(mContext, responseObservable);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeBasketMenuItem>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<ConciergeBasketMenuItem> result) {
                if (result.body() != null && result.body().isError()) {
                    hideLoading();
                    if (result.body().getDataType() == SupportConciergeCustomErrorResponse.ResponseDataType.PAYMENT_IN_PROCESS) {
                        if (mListener != null) {
                            mListener.onHandlePaymentInProgressError(ConciergeProductDetailFragmentViewModel.this,
                                    result.body().getErrorMessage(),
                                    item,
                                    true);
                        }
                    } else {
                        showSnackBar(result.body().getErrorMessage(), true);
                    }
                } else {
                    updateLocalBasketItem(mMenuItem);
                }
            }
        }));
    }

    private void addItemToLocalBasket(ConciergeMenuItem item, boolean isFromUpdate) {
        Timber.i("addItemToLocalBasket: " + new Gson().toJson(item));
        Observable<ConciergeMenuItemManipulationInfo> ob = ConciergeCartHelper.addItemToCart(item, isFromUpdate ? mPositionInBasket : -1, false);
        BaseObserverHelper<ConciergeMenuItemManipulationInfo> helper = new BaseObserverHelper<>(mContext, ob);
        addDisposable(helper.execute(new OnCallbackListener<ConciergeMenuItemManipulationInfo>() {
            @Override
            public void onComplete(ConciergeMenuItemManipulationInfo result) {
                hideLoading();
                Timber.i("Item added to cart! \n" + new Gson().toJson(result));
                if (mListener != null) {
                    mListener.onItemAddedToCart(item);
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                mListener.showOrderErrorPopup(mContext.getString(R.string.shop_request_add_item_to_basket_error, item.getName()),
                        null);
            }
        }));
    }

    public void removeItemFromCart(ConciergeMenuItem item, OnCallbackListener<Object> listener) {
        Timber.i("Remove item to cart in edit mode.");
        Observable<Object> ob = ConciergeCartHelper.removeItem(item, true);
        BaseObserverHelper<Object> helper = new BaseObserverHelper<>(mContext, ob);
        addDisposable(helper.execute(listener));
    }

    private boolean checkRequiredOptionSelected() {
        if (mMenuItem.getType() == ConciergeMenuItemType.COMBO) {
            List<ConciergeComboChoiceSection> comboChoiceSections = mMenuItem.getChoices();
            List<Boolean> allOptionCheck = new ArrayList<>();
            if (comboChoiceSections != null && !comboChoiceSections.isEmpty()) {
                for (ConciergeComboChoiceSection comboChoiceSection : comboChoiceSections) {
                    if (comboChoiceSection != null) {
                        if (comboChoiceSection.getList() != null && !comboChoiceSection.getList().isEmpty()) {
                            boolean isAnOptionSelected = false;
                            for (ConciergeComboItem comboItem : comboChoiceSection.getList()) {
                                if (comboItem.isSelected()) {
                                    isAnOptionSelected = true;
                                    break;
                                }
                            }
                            allOptionCheck.add(isAnOptionSelected);
                        }
                    }
                }
            }

            return !allOptionCheck.contains(false);
        } else if (mMenuItem.getType() == ConciergeMenuItemType.ITEM) {
            List<ConciergeMenuItemOptionSection> options = mMenuItem.getOptions();
            List<Boolean> isAllRequiredOptionSelected = new ArrayList<>();
            if (options != null && !options.isEmpty()) {
                for (ConciergeMenuItemOptionSection option : options) {
                    ConciergeMenuItemOptionType optionType = ConciergeMenuItemOptionType.from(option.getType());
                    if (optionType == ConciergeMenuItemOptionType.VARIATION) {
                        boolean isAnOptionSelected = false;
                        for (ConciergeMenuItemOption optionItem : option.getOptionItems()) {
                            if (optionItem.isSelected()) {
                                isAnOptionSelected = true;
                            }
                        }
                        isAllRequiredOptionSelected.add(isAnOptionSelected);
                    }
                }

                return !isAllRequiredOptionSelected.contains(false);
            } else {
                // There is no variation, so return true to skip check
                return true;
            }
        }

        return false;
    }

    public void updatePriceDisplay() {
        mCurrentPrice = mMenuItem.getPrice();
        if (mMenuItem.getType() == ConciergeMenuItemType.COMBO) {
            List<ConciergeComboChoiceSection> comboSections = mMenuItem.getChoices();
            mPriceVisibility.set(View.VISIBLE);

            if (comboSections != null && !comboSections.isEmpty()) {
                for (ConciergeComboChoiceSection comboSection : comboSections) {
                    if (comboSection != null) {
                        if (comboSection.getList() != null && !comboSection.getList().isEmpty()) {
                            for (ConciergeComboItem comboItem : comboSection.getList()) {
                                if (comboItem != null && comboItem.isSelected()) {
                                    mCurrentPrice = ConciergeCartHelper.addPrice(mCurrentPrice, comboItem.getTotalPrice());
                                }
                            }
                        }
                    }
                }
            }
        } else if (mMenuItem.getType() == ConciergeMenuItemType.ITEM) {
            List<ConciergeMenuItemOptionSection> itemOptions = mMenuItem.getOptions();
            boolean isContainVariation = false;
            boolean isContainAddOn = false;

            if (itemOptions != null && !itemOptions.isEmpty()) {
                for (ConciergeMenuItemOptionSection option : itemOptions) {
                    if (option != null) {
                        ConciergeMenuItemOptionType optionType = ConciergeMenuItemOptionType.from(option.getType());
                        if (optionType == ConciergeMenuItemOptionType.VARIATION) {
                            isContainVariation = true;
                        } else if (optionType == ConciergeMenuItemOptionType.ADDON) {
                            isContainAddOn = true;
                        }

                        if (option.getOptionItems() != null && !option.getOptionItems().isEmpty()) {
                            for (ConciergeMenuItemOption optionItem : option.getOptionItems()) {
                                if (optionItem != null && optionItem.isSelected()) {
                                    if (optionType == ConciergeMenuItemOptionType.VARIATION) {
                                        // If a variation is selected, show price
                                        mPriceVisibility.set(View.VISIBLE);
                                    }
                                    mCurrentPrice = ConciergeCartHelper.addPrice(mCurrentPrice, optionItem.getPrice());
                                }
                            }
                        }
                    }
                }
            }

            if (!isContainVariation || isContainAddOn) {
                mPriceVisibility.set(View.VISIBLE);
            }
        }

        // Get the accumulated price of the item and multiply by the item count
        mCurrentPrice = ConciergeCartHelper.multiplyPrice(mCurrentPrice, mCounter);

        // Display the price at bottom of screen
        mBottomPrice.set(ConciergeHelper.getDisplayPrice(mContext, mCurrentPrice));
    }

    public void onOptionItemSelected(ConciergeMenuItemOptionSection optionItem) {
        for (int index = 0; index < mMenuItem.getOptions().size(); index++) {
            if (optionItem.areContentsTheSame(mMenuItem.getOptions().get(index))) {
                mMenuItem.getOptions().set(index, optionItem);
                break;
            }
        }
        updatePriceDisplay();
        shouldEnableAddToCartButton();
    }

    public void onComboItemSelected(ConciergeComboChoiceSection comboSection) {
        for (int index = 0; index < mMenuItem.getChoices().size(); index++) {
            if (comboSection.areContentsTheSame(mMenuItem.getChoices().get(index))) {
                mMenuItem.getChoices().set(index, comboSection);
                break;
            }
        }
        updatePriceDisplay();
        shouldEnableAddToCartButton();
    }

    public void shouldEnableAddToCartButton() {
        if (checkRequiredOptionSelected()) {
            mAddButtonIconColor.set(R.attr.shop_item_detail_cart_icon);
            mAddButtonBackgroundColor.set(R.attr.shop_item_detail_cart_icon_background);
        } else {
            mAddButtonIconColor.set(R.attr.shop_item_detail_cart_icon_disable);
            mAddButtonBackgroundColor.set(R.attr.shop_item_detail_cart_icon_disable_background);
        }
    }

    public void onSpecialInstructionChanged(String instruction) {
        mMenuItem.setSpecialInstruction(instruction);
    }

    public interface ConciergeProductDetailViewModelListener extends ConciergeSupportAddItemToCardViewModel.ConciergeSupportAddItemToCardViewModelListener {

        void onLoadDetailSuccess(ConciergeMenuItem item);

        void onRequiredItemNotSelected();

        void onRequireLogin();

        void showOrderErrorPopup(String errorMessage, View.OnClickListener okClickListener);
    }

    @BindingAdapter("setProductDescription")
    public static void setProductDescription(TextView textView, String description) {
        if (!TextUtils.isEmpty(description)) {
            Spanned htmlAsSpanned = Html.fromHtml(description, Html.FROM_HTML_MODE_LEGACY);
            textView.setText(htmlAsSpanned);
        }
    }
}
