package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.text.TextUtils;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.model.emun.ConciergeSortItemOption;
import com.proapp.sompom.model.emun.ConciergeViewItemByType;
import com.proapp.sompom.services.datamanager.AbsConciergeProductDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeCartBottomSheetViewModel;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 24/3/22.
 */
public class AbsSupportViewItemByViewModel<DM extends AbsConciergeProductDataManager, L extends AbsSupportViewItemByViewModel.AbsSupportViewItemByViewModelListener> extends ConciergeSupportAddItemToCardViewModel<DM, L> {

    private final ObservableField<String> mNextViewByIcon = new ObservableField<>();
    protected final ObservableBoolean mSortCriteriaSelected = new ObservableBoolean();
    protected final ObservableBoolean mFilterCriteriaSelected = new ObservableBoolean();
    protected final ObservableField<AbsLoadingViewModel> mAppliedSearchCriteriaLoadingViewModel = new ObservableField<>();
    private ConciergeViewItemByType mSelectedConciergeViewItemByType;
    private ConciergeViewItemByType mNextConciergeViewItemByType;
    protected ConciergeSortItemOption mConciergeSortItemOption;
    protected boolean mIsFirstDataLoadSuccess;
    protected ConciergeCartBottomSheetViewModel mCartViewModel;

    public AbsSupportViewItemByViewModel(Context context, L listener, DM dataManager) {
        super(context, listener, dataManager);
        mListener = listener;
        mSelectedConciergeViewItemByType = ConciergeHelper.getSelectedViewItemByOption(context);
        mNextConciergeViewItemByType = ConciergeViewItemByType.getNextViewByOption(mSelectedConciergeViewItemByType);
        mNextViewByIcon.set(context.getString(mNextConciergeViewItemByType.getIConResource()));
        mAppliedSearchCriteriaLoadingViewModel.set(new AbsLoadingViewModel(context));
        mSortCriteriaSelected.set(!TextUtils.isEmpty(ConciergeHelper.getSortOption()));
    }

    public AbsSupportViewItemByViewModel(Context context, ConciergeCartBottomSheetViewModel cartViewModel, L listener, DM dataManager) {
        super(context, listener, dataManager);
        mListener = listener;
        mSelectedConciergeViewItemByType = ConciergeHelper.getSelectedViewItemByOption(context);
        mNextConciergeViewItemByType = ConciergeViewItemByType.getNextViewByOption(mSelectedConciergeViewItemByType);
        mNextViewByIcon.set(context.getString(mNextConciergeViewItemByType.getIConResource()));
        mAppliedSearchCriteriaLoadingViewModel.set(new AbsLoadingViewModel(context));
        mCartViewModel = cartViewModel;
        mSortCriteriaSelected.set(!TextUtils.isEmpty(ConciergeHelper.getSortOption()));
    }

    public ObservableField<AbsLoadingViewModel> getAppliedSearchCriteriaLoadingViewModel() {
        return mAppliedSearchCriteriaLoadingViewModel;
    }

    public void onApplySort(ConciergeSortItemOption sortItemOption) {
        ConciergeHelper.setSortItemOption(sortItemOption);
        mSortCriteriaSelected.set(true);
        mConciergeSortItemOption = sortItemOption;
    }

    protected AbsLoadingViewModel getInternalAppliedSearchCriteriaLoadingViewModel() {
        return mAppliedSearchCriteriaLoadingViewModel.get();
    }

    public ObservableBoolean getFilterCriteriaSelected() {
        return mFilterCriteriaSelected;
    }

    public void showAppliedSearchCriteriaLoadingView() {
        mAppliedSearchCriteriaLoadingViewModel.get().showLoading();
    }

    public void hideAppliedSearchCriteriaLoadingView() {
        mAppliedSearchCriteriaLoadingViewModel.get().hideLoading();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        hideAppliedSearchCriteriaLoadingView();
    }

    public ObservableBoolean getSortCriteriaSelected() {
        return mSortCriteriaSelected;
    }

    public ObservableField<String> getNextViewByIcon() {
        return mNextViewByIcon;
    }

    public void onViewByOptionClicked(Context context) {
        mSelectedConciergeViewItemByType = mNextConciergeViewItemByType;
        ConciergeHelper.saveSelectedViewItemByOption(context, mSelectedConciergeViewItemByType);
        mNextConciergeViewItemByType = ConciergeViewItemByType.getNextViewByOption(mSelectedConciergeViewItemByType);
        mNextViewByIcon.set(context.getString(mNextConciergeViewItemByType.getIConResource()));
        if (mListener != null) {
            mListener.onViewItemByOptionChanged(mSelectedConciergeViewItemByType);
        }
    }

    public void onSortByOptionClicked(AppCompatSpinner spinner) {
        Timber.i("onSortByOptionClicked");
        spinner.performClick();
    }

    public void onFilterByOptionClicked() {
    }

    public interface AbsSupportViewItemByViewModelListener extends ConciergeSupportAddItemToCardViewModelListener {
        void onViewItemByOptionChanged(ConciergeViewItemByType selectedOption);
    }
}
