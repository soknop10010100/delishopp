package com.proapp.sompom.viewmodel.newviewmodel;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;

import com.proapp.sompom.utils.GlideLoadUtil;

/**
 * Created by nuonveyo on 8/30/18.
 */

public class LayoutChatImageFullScreenViewModel {

    private final ObservableField<String> mImageUrl = new ObservableField<>();
    private final ObservableField<String> mThumbnail = new ObservableField<>();

    public LayoutChatImageFullScreenViewModel(String imageUrl, String thumbnail) {
        mImageUrl.set(imageUrl);
        mThumbnail.set(thumbnail);
    }

    public ObservableField<String> getImageUrl() {
        return mImageUrl;
    }

    public ObservableField<String> getThumbnail() {
        return mThumbnail;
    }

    @BindingAdapter({"fullImageView", "thumbnailImageView", "fullUrl", "thumbnailUrl"})
    public static void setDetailChatImage(RelativeLayout rootView,
                                          ImageView fullImageView,
                                          ImageView thumbnailImageView,
                                          String fullUrl,
                                          String thumbnailUrl) {
        GlideLoadUtil.loadChatDetailImage(thumbnailImageView, thumbnailUrl, null);
        GlideLoadUtil.loadChatDetailImage(fullImageView,
                fullUrl,
                new GlideLoadUtil.GlideLoadUtilListener() {
                    @Override
                    public void onLoadSuccess(Drawable resource, boolean isSVG) {
                        fullImageView.setVisibility(View.VISIBLE);
                    }
                });
    }
}
