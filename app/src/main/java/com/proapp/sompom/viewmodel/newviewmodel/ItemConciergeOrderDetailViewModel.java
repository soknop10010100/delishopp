package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItemManipulationInfo;
import com.proapp.sompom.newui.fragment.AbsSupportShopCheckOutFragment;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.viewmodel.AbsConciergeProductViewModel;
import com.sompom.pushy.service.SendBroadCastHelper;

import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by Veasna Chhom on 20/9/21.
 */
public class ItemConciergeOrderDetailViewModel extends AbsConciergeProductViewModel {

    private final ObservableField<String> mNote = new ObservableField<>();
    private final ObservableField<String> mItemPrice = new ObservableField<>();
    private final ObservableField<String> mItemUnit = new ObservableField<>();
    private final ObservableInt mUpdateProductCountButtonVisibility = new ObservableInt();
    private int mItemCounter;
    private ItemConciergeOrderDetailViewModelListener mListener;

    public ItemConciergeOrderDetailViewModel(Context context, ConciergeMenuItem item) {
        super(context, item);
        initData(item);
    }

    public ObservableInt getUpdateProductCountButtonVisibility() {
        return mUpdateProductCountButtonVisibility;
    }

    public void addListener(ItemConciergeOrderDetailViewModelListener listener) {
        mListener = listener;
    }

    public ObservableField<String> getNote() {
        return mNote;
    }

    public ObservableField<String> getItemPrice() {
        return mItemPrice;
    }

    public ObservableField<String> getItemUnit() {
        return mItemUnit;
    }

    @Override
    protected void initData(ConciergeMenuItem product) {
        super.initData(product);
        mItemPrice.set(ConciergeHelper.getDisplayPrice(mContext, mMenuItem.getTotalPrice()));
        if (mMenuItem.getProductCount() > 0) {
            mItemCounter = mMenuItem.getProductCount();
            mItemUnit.set(String.valueOf(mItemCounter));
        } else {
            mItemUnit.set(String.valueOf(0));
        }
        if (MainApplication.isInExpressMode()) {
            mUpdateProductCountButtonVisibility.set(mMenuItem.isShopOpen() ? View.VISIBLE : View.INVISIBLE);
        } else {
            mUpdateProductCountButtonVisibility.set(View.VISIBLE);
        }
    }

    public void onRemoveItemClick() {
        if (mListener != null) {
            mListener.onRemoveWholeItemClicked(mMenuItem);
        }
    }

    public void onAddItemCountClicked() {
        mItemCounter++;
        mItemUnit.set(String.valueOf(mItemCounter));
        mMenuItem.setProductCount(mItemCounter);
        mItemPrice.set(ConciergeHelper.getDisplayPrice(mContext, mMenuItem.getTotalPrice()));

        Observable<ConciergeMenuItemManipulationInfo> ob = ConciergeCartHelper.modifyItemCount(mMenuItem, mItemCounter, true);
        BaseObserverHelper<ConciergeMenuItemManipulationInfo> helper = new BaseObserverHelper<>(mContext, ob);
        addDisposable(helper.execute(new OnCallbackListener<ConciergeMenuItemManipulationInfo>() {
            @Override
            public void onComplete(ConciergeMenuItemManipulationInfo result) {
                Timber.i("Add item into local success.");
                if (mListener != null) {
                    mListener.onAddItemClicked(mMenuItem);
                }
                Intent intent = new Intent(AbsSupportShopCheckOutFragment.CART_ITEM_ADDED_EVENT);
                intent.putExtra(AbsSupportShopCheckOutFragment.CART_EVENT_DATA, mMenuItem);
                SendBroadCastHelper.verifyAndSendBroadCast(mContext, intent);
                ConciergeHelper.manipulateItemToRemoteBasketInBackground(mContext, mListener.getAPIService(), result);
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                if (ConciergeHelper.shouldDisplayUpdateItemError(mMenuItem.getId())) {
                    ConciergeHelper.showUpdateItemInBasketErrorPopup((AppCompatActivity) mContext,
                            mContext.getString(R.string.shop_request_update_item_to_basket_error, mMenuItem.getName()),
                            mMenuItem.getId());
                }
                mItemCounter--;
                mItemUnit.set(String.valueOf(mItemCounter));
                mMenuItem.setProductCount(mItemCounter);
                mItemPrice.set(ConciergeHelper.getDisplayPrice(mContext, mMenuItem.getTotalPrice()));
            }
        }));
    }

    public void onMinusItemCountClicked() {
        if (mItemCounter > 0) {
            mItemCounter--;
            mMenuItem.setProductCount(mItemCounter);

            if (mItemCounter != 0) {
                mItemUnit.set(String.valueOf(mItemCounter));
                mItemPrice.set(ConciergeHelper.getDisplayPrice(mContext, mMenuItem.getTotalPrice()));
            } else {
                mItemPrice.set(ConciergeHelper.getDisplayPrice(mContext, 0f));
                mItemUnit.set(String.valueOf(0));
            }

            Observable<ConciergeMenuItemManipulationInfo> ob = ConciergeCartHelper.modifyItemCount(mMenuItem, mItemCounter, true);
            BaseObserverHelper<ConciergeMenuItemManipulationInfo> helper = new BaseObserverHelper<>(mContext, ob);
            addDisposable(helper.execute(new OnCallbackListener<ConciergeMenuItemManipulationInfo>() {
                @Override
                public void onComplete(ConciergeMenuItemManipulationInfo result) {
                    Timber.i("Remove item from local success.");
                    if (mListener != null) {
                        mListener.onRemoveItemClicked(mMenuItem);
                    }
                    Intent intent = new Intent(AbsSupportShopCheckOutFragment.CART_ITEM_REMOVED_EVENT);
                    intent.putExtra(AbsSupportShopCheckOutFragment.CART_EVENT_DATA, mMenuItem);
                    SendBroadCastHelper.verifyAndSendBroadCast(mContext, intent);
                    ConciergeHelper.manipulateItemToRemoteBasketInBackground(mContext, mListener.getAPIService(), result);
                }

                @Override
                public void onFail(ErrorThrowable ex) {
                    if (ConciergeHelper.shouldDisplayUpdateItemError(mMenuItem.getId())) {
                        ConciergeHelper.showUpdateItemInBasketErrorPopup((AppCompatActivity) mContext,
                                mContext.getString(R.string.shop_request_update_item_to_basket_error, mMenuItem.getName()),
                                mMenuItem.getId());
                    }
                    mItemCounter++;
                    mItemUnit.set(String.valueOf(mItemCounter));
                    mMenuItem.setProductCount(mItemCounter);
                    mItemPrice.set(ConciergeHelper.getDisplayPrice(mContext, mMenuItem.getTotalPrice()));
                }
            }));
        }
    }

    public void onItemClicked() {
        if (mListener != null) {
            mListener.onItemClicked(mMenuItem);
        }
    }

    public interface ItemConciergeOrderDetailViewModelListener {
        void onAddItemClicked(ConciergeMenuItem product);

        void onRemoveItemClicked(ConciergeMenuItem product);

        void onItemClicked(ConciergeMenuItem product);

        void onRemoveWholeItemClicked(ConciergeMenuItem product);

        ApiService getAPIService();
    }
}
