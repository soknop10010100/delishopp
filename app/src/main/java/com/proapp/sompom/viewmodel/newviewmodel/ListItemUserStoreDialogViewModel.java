package com.proapp.sompom.viewmodel.newviewmodel;

import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;
import com.proapp.sompom.listener.OnCompleteListener;

/**
 * Created by nuonveyo on 10/25/18.
 */

public class ListItemUserStoreDialogViewModel extends AbsBaseViewModel {
    public Product mProduct;
    private OnCompleteListener<Product> mOnItemClickListener;

    public ListItemUserStoreDialogViewModel(Product product, OnCompleteListener<Product> listener) {
        mProduct = product;
        mOnItemClickListener = listener;
    }

    public void onItemClick() {
        mOnItemClickListener.onComplete(mProduct);
    }
}
