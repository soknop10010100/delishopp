package com.proapp.sompom.viewmodel.newviewmodel;

import android.text.TextUtils;

import androidx.databinding.ObservableField;

import com.proapp.sompom.helper.FileDownloadHelper;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;
import com.proapp.sompom.utils.DownloaderUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListItemTimelineFileDetailViewModel extends AbsTimelineDetailItemViewModel {

    private final ObservableField<List<Media>> mMediaList = new ObservableField<>();
    private Map<Media, ItemPostTimelineFileViewModel> mViewModelHashMap = new HashMap<>();

    public ListItemTimelineFileDetailViewModel(AbsBaseActivity activity,
                                               WallStreetAdaptive post,
                                               Adaptive adaptive,
                                               WallStreetDataManager dataManager,
                                               int position,
                                               OnTimelineItemButtonClickListener onItemClick) {
        super(activity, post, adaptive, dataManager, position, onItemClick);
        if (adaptive instanceof WallStreetAdaptive) {
            mMediaList.set(((WallStreetAdaptive) adaptive).getMedia());
        }
    }

    public Map<Media, ItemPostTimelineFileViewModel> getViewModelHashMap() {
        return mViewModelHashMap;
    }

    public ObservableField<List<Media>> getMediaList() {
        return mMediaList;
    }

    public ItemPostTimelineFileViewModel.ItemPostTimelineFileViewModelListener getFileClickListener() {
        return (isFromMoreFile, media, fileViewModel) -> {
            if (!TextUtils.isEmpty(media.getDownloadedPath())) {
                //File was previously downloaded.
                DownloaderUtils.openDownloadedFile(mContext, media.getDownloadedPath());
            } else {
                WallStreetHelper.downloadFile(mContext,
                        media, fileViewModel,
                        new FileDownloadHelper.FileDownloadListener() {
                            @Override
                            public void onDownloadFailed(String requestId, String downloadId, Throwable ex) {
                                if (mOnItemClickListener != null) {
                                    mOnItemClickListener.onShowSnackBar(true, ex.getMessage());
                                }
                            }
                        });
            }
        };
    }
}
