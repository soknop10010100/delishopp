package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.text.TextUtils;

import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.model.concierge.WalletHistory;
import com.proapp.sompom.utils.DateUtils;
import com.proapp.sompom.utils.SpannableUtil;
import com.resourcemanager.helper.LocaleManager;

import java.util.Date;

/**
 * Created by Veasna Chhom on 8/23/22.
 */
public class WalletHistoryItemViewModel {

    private final ObservableField<String> mDate = new ObservableField<>();
    private final ObservableField<String> mInfo = new ObservableField<>();
    private final ObservableField<String> mOrderNumber = new ObservableField<>();
    private final ObservableField<String> mAmount = new ObservableField<>();
    private final ObservableField<String> mTransactionType = new ObservableField<>();
    private final ObservableField<String> mComment = new ObservableField<>();
    private final ObservableField<String> mOrderNumberTitle = new ObservableField<>();

    public WalletHistoryItemViewModel(Context context, WalletHistory history) {
        if (!TextUtils.isEmpty(history.getCreatedDateString())) {
            Date createdAt = history.getCreatedAt();
            if (createdAt == null) {
                createdAt = DateUtils.getDateFromStringDateWithSpecificFormat(history.getCreatedDateString(),
                        DateUtils.DATE_FORMAT_27);
            }

            if (createdAt != null) {
                history.setCreatedAt(createdAt);
                mDate.set(DateUtils.getDateWithFormat(context,
                        createdAt.getTime(),
                        DateUtils.DATE_FORMAT_26,
                        LocaleManager.getLocale(context.getResources())));
            }
        }
        mInfo.set(history.getInfo());
        mOrderNumberTitle.set(context.getString(R.string.shop_order_list_order_number_title) + ":");
        mOrderNumber.set(history.getOrderNumber());

        mAmount.set(ConciergeHelper.getDisplayPrice(context, history.getAmount()));
        if (!TextUtils.isEmpty(history.getTransactionType())) {
            mTransactionType.set(SpannableUtil.capitaliseOnlyFirstLetter(history.getTransactionType()));
        }
        mComment.set(history.getComment());
    }

    public ObservableField<String> getDate() {
        return mDate;
    }

    public ObservableField<String> getInfo() {
        return mInfo;
    }

    public ObservableField<String> getOrderNumber() {
        return mOrderNumber;
    }

    public ObservableField<String> getAmount() {
        return mAmount;
    }

    public ObservableField<String> getTransactionType() {
        return mTransactionType;
    }

    public ObservableField<String> getComment() {
        return mComment;
    }

    public ObservableField<String> getOrderNumberTitle() {
        return mOrderNumberTitle;
    }
}
