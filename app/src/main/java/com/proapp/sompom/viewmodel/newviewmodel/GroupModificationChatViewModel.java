package com.proapp.sompom.viewmodel.newviewmodel;

import android.app.Activity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnChatItemListener;
import com.proapp.sompom.model.MetaGroup;
import com.proapp.sompom.model.SelectedChat;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.ChatUtility;
import com.proapp.sompom.utils.SpannableUtil;
import com.proapp.sompom.viewmodel.ChatMediaViewModel;

import java.util.List;

import timber.log.Timber;

public class GroupModificationChatViewModel extends ChatMediaViewModel {

    private ObservableField<String> mIcon = new ObservableField<>();
    private ObservableField<Spanned> mFirstDescription = new ObservableField<>();
    private ObservableField<Spanned> mSecondDescription = new ObservableField<>();
    private ObservableField<String> mUserName = new ObservableField<>();
    private ObservableField<User> mUser = new ObservableField<>();
    private ObservableInt mIconVisibility = new ObservableInt();
    private ObservableInt mSecondDescriptionVisibility = new ObservableInt();
    private ObservableInt mFirstDescriptionVisibility = new ObservableInt();
    private ObservableInt mUserVisibility = new ObservableInt();
    private User mSender;

    public GroupModificationChatViewModel(Activity context,
                                          Conversation conversation,
                                          int position,
                                          Chat chat,
                                          SelectedChat selectedChat,
                                          String searchKeyWord,
                                          ChatUtility.GroupMessage drawable,
                                          OnChatItemListener onItemClickListener) {
        super(context, conversation, chat, drawable, position, selectedChat, searchKeyWord, onItemClickListener);
        bindData(chat);
    }

    public ObservableField<String> getIcon() {
        return mIcon;
    }

    public ObservableInt getFirstDescriptionVisibility() {
        return mFirstDescriptionVisibility;
    }

    public ObservableField<Spanned> getFirstDescription() {
        return mFirstDescription;
    }

    public ObservableField<Spanned> getSecondDescription() {
        return mSecondDescription;
    }

    public ObservableField<User> getUser() {
        return mUser;
    }

    public ObservableField<String> getUserName() {
        return mUserName;
    }

    public ObservableInt getIconVisibility() {
        return mIconVisibility;
    }

    public ObservableInt getSecondDescriptionVisibility() {
        return mSecondDescriptionVisibility;
    }

    public ObservableInt getUserVisibility() {
        return mUserVisibility;
    }

    private void bindData(Chat chat) {
        Timber.i("Group Media: " + chat.getMetaGroup());
        if (chat.getMetaGroup() != null) {
            loadSenderFromGroupParticipantIfNecessary(chat.getSender());
            Chat.ChatType chatType = Chat.ChatType.from(chat.getChatType());
            MetaGroup metaGroup = chat.getMetaGroup();
            if (chatType == Chat.ChatType.CHANGE_GROUP_NAME) {
                checkCommonViewStateForChangeGroupValue(chat);
                String text = mContext.getString(R.string.chat_message_change_group_name,
                        metaGroup.getName());
                mSecondDescription.set(SpannableUtil.setBoldText(mContext, text,
                        metaGroup.getName(),
                        AttributeConverter.convertAttrToColor(mContext, R.attr.chat_group_name)));
            } else if (chatType == Chat.ChatType.CHANGE_GROUP_PICTURE) {
                checkCommonViewStateForChangeGroupValue(chat);
                mSecondDescription.set(new SpannableString(mContext.getString(R.string.chat_message_change_group_picture)));
            } else if (chatType == Chat.ChatType.ADD_GROUP_MEMBER) {
                mIcon.set(mContext.getString(R.string.code_enter_right));
                checkCommonViewStateForAddOrRemoveMember(metaGroup.getParticipants(),
                        mContext.getString(R.string.chat_message_added_someone),
                        mContext.getString(R.string.chat_message_someone_to_group));
            } else if (chatType == Chat.ChatType.REMOVE_GROUP_MEMBER) {
                mIcon.set(mContext.getString(R.string.code_exit_left));
                checkCommonViewStateForAddOrRemoveMember(metaGroup.getParticipants(),
                        mContext.getString(R.string.chat_message_removed_someone),
                        mContext.getString(R.string.chat_message_someone_from_group));
            }
        }
    }

    private void checkCommonViewStateForChangeGroupValue(Chat chat) {
        mIconVisibility.set(View.GONE);
        mUserVisibility.set(mSender != null ? View.VISIBLE : View.GONE);
        if (mSender != null) {
            mUserVisibility.set(View.VISIBLE);
            mUser.set(mSender);
            mUserName.set(SpannableUtil.getMessageSubject(mContext, mSender));
        }
        mFirstDescriptionVisibility.set(View.GONE);
        mSecondDescriptionVisibility.set(View.VISIBLE);
    }

    private void checkCommonViewStateForAddOrRemoveMember(List<User> memberList,
                                                          String action,
                                                          String object) {
        if (mSender != null && memberList != null && !memberList.isEmpty()) {
            mIconVisibility.set(View.VISIBLE);
            mFirstDescriptionVisibility.set(View.VISIBLE);
            if (memberList.size() > 1) {
                //Multiple members
                mUserVisibility.set(View.GONE);
                mSecondDescriptionVisibility.set(View.GONE);
            } else {
                mSecondDescriptionVisibility.set(View.VISIBLE);
                mUserVisibility.set(View.VISIBLE);
                mUserVisibility.set(View.VISIBLE);
                User user = memberList.get(0);
                mUser.set(user);
                mUserName.set(SpannableUtil.buildGroupNotificationUserName(mContext, user, true));
            }

            String firstText = SpannableUtil.getMessageSubject(mContext, mSender) + " " + action;
            if (memberList.size() > 1) {
                String userNames = SpannableUtil.buildMultipleUserNameForGroupModificationMessage(mContext, memberList, false);
                firstText += " " + userNames + " " + object;
                mFirstDescription.set(SpannableUtil.setBoldText(mContext, firstText,
                        userNames,
                        AttributeConverter.convertAttrToColor(mContext, R.attr.chat_group_user)));
            } else {
                mFirstDescription.set(new SpannableString(firstText));
                mSecondDescription.set(new SpannableString(object));
            }
        }
    }

    private void loadSenderFromGroupParticipantIfNecessary(User checkingSender) {
        /*
        We just try to get the latest user info from participant instead of relying on the sender field
        since the user from participant is more frequently updated.
         */
        mSender = checkingSender;
        if (mConversation != null && mConversation.getParticipants() != null) {
            for (User participant : mConversation.getParticipants()) {
                if (TextUtils.equals(participant.getId(), checkingSender.getId())) {
                    mSender = checkingSender;
                    break;
                }
            }
        }
    }
}
