package com.proapp.sompom.viewmodel;

import android.content.Context;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

/**
 * Created by nuonveyo on 2/7/18.
 */

public class ToolbarViewModel extends AbsLoadingViewModel {

    public ObservableField<String> mActionText = new ObservableField<>();
    public ObservableBoolean mIsShowAction = new ObservableBoolean(false);
    public ObservableBoolean mIsEnableToolbarButton = new ObservableBoolean(false);

    public ToolbarViewModel(Context context) {
        super(context);
    }

    public void onToolbarButtonClicked(Context context) {
    }
}
