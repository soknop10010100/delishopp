package com.proapp.sompom.viewmodel;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.result.NotificationSettingModel;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.StoreDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 5/3/18.
 */

public class AdvanceNotificationFragmentViewModel extends AbsLoadingViewModel {
    public NotificationSettingModel mNotificationSettingModel;
    private StoreDataManager mDataManager;

    public AdvanceNotificationFragmentViewModel(StoreDataManager dataManager) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mNotificationSettingModel = SharedPrefUtils.getNotificationSetting(mDataManager.getContext());
        if (mNotificationSettingModel == null) {
            mNotificationSettingModel = new NotificationSettingModel();
        }
    }

    public void onButtonSaveClicked() {
        pushNotificationSetting();
    }

    private void pushNotificationSetting() {
        showLoading();
        Observable<Response<User>> call = mDataManager.updateNotification(mNotificationSettingModel);
        ResponseObserverHelper<Response<User>> helper =
                new ResponseObserverHelper<>(mDataManager.getContext(), call);

        addDisposable(helper.execute(new OnCallbackListener<Response<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(R.string.setting_popup_disable_notification_error_description, true);
            }

            @Override
            public void onComplete(Response<User> value) {
                if (value.body() != null) {
                    mNotificationSettingModel = value.body().getNotificationSettingModel();
                    SharedPrefUtils.setNotificationSetting(mDataManager.getContext(), mNotificationSettingModel);
                }

                hideLoading();
            }
        }));
    }
}
