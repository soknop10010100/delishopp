package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.databinding.ObservableLong;

import com.proapp.sompom.R;
import com.proapp.sompom.model.result.AppFeature;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 6/22/18.
 */

public class LayoutCustomHomeTabViewModel extends AbsBaseViewModel {

    private final ObservableBoolean mIsSelected = new ObservableBoolean();
    private final ObservableBoolean mIsIndicatorVisible = new ObservableBoolean(false);
    private final ObservableLong mBadgeCount = new ObservableLong();
    private final ObservableField<String> mTabIcon = new ObservableField<>();
    private final ObservableInt mTabIconSize = new ObservableInt();
    private final ObservableInt mTabIconColor = new ObservableInt();
    private final ObservableInt mTabFont = new ObservableInt();

    private Context mContext;
    private AppFeature.NavigationBarMenu mNavBarMenu;
    private int mPreviousBadgeCount;
    private boolean mIsHide;

    public LayoutCustomHomeTabViewModel(Context context,
                                        AppFeature.NavigationBarMenu navMenu) {
        mContext = context;
        mNavBarMenu = navMenu;
        setIsSelected(false);
    }

    public void setIsSelected(boolean isSelected) {
        mIsSelected.set(isSelected);
        mTabFont.set(isSelected ? mNavBarMenu.getNavMenuButton().getSelectedFont() : mNavBarMenu.getNavMenuButton().getNormalFont());
        mTabIcon.set(mContext.getString(isSelected ? mNavBarMenu.getNavMenuButton().getSelectedIcon() : mNavBarMenu.getNavMenuButton().getNormalIcon()));
        mTabIconColor.set(AttributeConverter.convertAttrToColor(mContext, isSelected ? R.attr.home_tab_selected : R.attr.home_tab_unselected));
        mTabIconSize.set(mNavBarMenu.getNavMenuButton().getIconSize(mContext));
    }

    public ObservableBoolean getIsSelected() {
        return mIsSelected;
    }

    public ObservableBoolean getIsIndicatorVisible() {
        return mIsIndicatorVisible;
    }

    public void setIsIndicatorVisible(boolean isVisible) {
        mIsIndicatorVisible.set(isVisible);
    }

    public ObservableLong getBadgeCount() {
        return mBadgeCount;
    }

    public AppFeature.NavigationBarMenu getNavBarMenu() {
        return mNavBarMenu;
    }

    public ObservableField<String> getTabIcon() {
        return mTabIcon;
    }

    public ObservableInt getTabIconSize() {
        return mTabIconSize;
    }

    public ObservableInt getTabIconColor() {
        return mTabIconColor;
    }

    public ObservableInt getTabFont() {
        return mTabFont;
    }

    public void setBadgeValue(long badgeValue) {
        if (!mIsHide) {
            mBadgeCount.set(badgeValue);
        }
        mPreviousBadgeCount = (int) badgeValue;
    }

    public void hideBadgeCount(boolean isHide) {
        mIsHide = isHide;
        if (isHide) {
            mBadgeCount.set(0);
        } else {
            mBadgeCount.set(mPreviousBadgeCount);
        }
    }

    @BindingAdapter("fontIcon")
    public static void setFontIconFamily(TextView textView, int fontIcon) {
        Typeface fontIconTypeFace = ResourcesCompat.getFont(textView.getContext(), fontIcon);
        textView.setTypeface(fontIconTypeFace);
    }
}
