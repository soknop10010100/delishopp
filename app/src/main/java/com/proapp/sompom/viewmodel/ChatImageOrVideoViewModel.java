package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnChatItemListener;
import com.proapp.sompom.model.SelectedChat;
import com.proapp.sompom.model.emun.ChatMediaBg;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.utils.ChatUtility;

import timber.log.Timber;

/**
 * Created Chhom Veasna on 3/9/21.
 */

public class ChatImageOrVideoViewModel extends ChatMediaViewModel {

    private ChatMediaBg mChatMediaBg;

    public ChatImageOrVideoViewModel(Activity context,
                                     Conversation conversation,
                                     Chat chat,
                                     ChatUtility.GroupMessage groupMessage,
                                     int position,
                                     SelectedChat selectChat,
                                     String searchKeyWord,
                                     OnChatItemListener onItemClickListener) {
        super(context,
                conversation,
                chat,
                groupMessage,
                position,
                selectChat,
                searchKeyWord,
                onItemClickListener);
        mChatMediaBg = ChatMediaBg.getChatLinkBg(groupMessage.getChatBg());
        Timber.i("mChatMediaBg: " + mChatMediaBg.name());
    }

    public Drawable getImageChatItemBackground(View view, Context context) {
        view.setTag(R.id.tagContainer, mChatMediaBg);
        return ContextCompat.getDrawable(context, mChatMediaBg.getDrawable(mChat.isExpandHeight()));
    }
}
