package com.proapp.sompom.viewmodel.newviewmodel;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ObservableField;

import com.google.android.gms.maps.GoogleMap;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.intent.newintent.ConciergeSelectLocationIntent;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.utils.NetworkStateUtil;
import com.proapp.sompom.viewmodel.AbsMapViewModel;

/**
 * Created by Or Vitovongsak on 25/11/21.
 */
public class ConciergeSelectLocationFragmentViewModel extends AbsMapViewModel {

    private static final String CAMBODIA_COUNTRY_CODE = "KH";

    public final ObservableField<String> mAddress = new ObservableField<>();
    private OnCallback mOnCallback;

    public ConciergeSelectLocationFragmentViewModel(Context context,
                                                    GoogleMap googleMap,
                                                    SearchAddressResult searchAddressResult,
                                                    OnCallback callback) {
        super(context, googleMap);
        mSearchAddressResult = searchAddressResult;
        mOnCallback = callback;

        enableCurrentLocation(true);
        enableMapClick(true);
        enablePoiClick(true);
        checkToSelectPreviousLocation();
    }

    public ObservableField<String> getAddress() {
        return mAddress;
    }

    @Override
    public void onAddressChanged(String newAddress) {
        mAddress.set(newAddress);
        if (mOnCallback != null) {
            mOnCallback.onAddressSelected();
        }
    }

    public void onUseAddressClick(View view) {
        if (mContext instanceof AbsBaseActivity) {
            if (mSearchAddressResult == null ||
                    !TextUtils.equals(mSearchAddressResult.getLocations().getCountry(), CAMBODIA_COUNTRY_CODE)) {
                ConciergeHelper.showCheckoutErrorPopup((AppCompatActivity) mContext,
                        NetworkStateUtil.isNetworkAvailable(mContext) ? mContext.getString(R.string.shop_select_location_outside_cambodia_error) :
                                mContext.getString(R.string.error_internet_connection_description),
                        null);
            } else {
                ConciergeSelectLocationIntent intent = new ConciergeSelectLocationIntent(mContext, mSearchAddressResult);
                ((AbsBaseActivity) mContext).setResult(Activity.RESULT_OK, intent);
                ((AbsBaseActivity) mContext).finish();
            }
        }
    }

    public interface OnCallback {
        void onAddressSelected();
    }
}
