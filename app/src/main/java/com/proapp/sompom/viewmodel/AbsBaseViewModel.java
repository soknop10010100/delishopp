package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.annotation.StringRes;
import androidx.databinding.BaseObservable;
import androidx.fragment.app.FragmentActivity;

import com.proapp.sompom.newui.AbsBindingActivity;
import com.proapp.sompom.newui.dialog.AbsBaseFragmentDialog;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.newui.fragment.AbsBaseFragment;
import com.proapp.sompom.utils.SnackBarUtil;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by He Rotha on 10/24/17.
 */

public abstract class AbsBaseViewModel extends BaseObservable {
    private CompositeDisposable mCompositeDisposable;
    private AbsBaseFragment mAbsBaseFragment;
    private AbsBaseFragmentDialog mAbsBaseFragmentDialog;

    public void setAbsBaseFragment(AbsBaseFragment absBaseFragment) {
        mAbsBaseFragment = absBaseFragment;
    }

    public void setAbsBaseFragmentDialog(AbsBaseFragmentDialog absBaseFragmentDialog) {
        mAbsBaseFragmentDialog = absBaseFragmentDialog;
    }


    public void onCreate() {

    }

    public void onDestroy() {
        clearDisposable();
    }

    public void onResume() {

    }

    public void onPause() {

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    public void clearDisposable() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
    }

    public void addDisposable(Disposable disposable) {
        if (mCompositeDisposable == null) {
            mCompositeDisposable = new CompositeDisposable();
        }
        mCompositeDisposable.add(disposable);
    }

    public void showConfirmationDialog(Context context,
                                       @StringRes int title,
                                       @StringRes int description,
                                       @StringRes int ok,
                                       @StringRes int cancel,
                                       View.OnClickListener onLeftClickListener) {
        if (context instanceof FragmentActivity) {
            MessageDialog dialog = new MessageDialog();
            dialog.setTitle(context.getString(title));
            dialog.setMessage(context.getString(description));
            dialog.setLeftText(context.getString(ok), onLeftClickListener);
            dialog.setRightText(context.getString(cancel), null);
            dialog.show(((FragmentActivity) context).getSupportFragmentManager(), MessageDialog.TAG);
        }
    }

    public void showConfirmationDialog(Context context,
                                       String title,
                                       String description,
                                       String ok,
                                       String cancel,
                                       View.OnClickListener onLeftClickListener) {
        if (context instanceof FragmentActivity) {
            MessageDialog dialog = new MessageDialog();
            dialog.setTitle(title);
            dialog.setMessage(description);
            dialog.setLeftText(ok, onLeftClickListener);
            dialog.setRightText(cancel, null);
            dialog.show(((FragmentActivity) context).getSupportFragmentManager(), MessageDialog.TAG);
        }
    }

    public void showSnackBar(String message, boolean isError) {
        if (mAbsBaseFragment != null) {
            mAbsBaseFragment.showSnackBar(message, isError);
        } else {
            if (mAbsBaseFragmentDialog != null) {
                mAbsBaseFragmentDialog.showSnackBar(message, isError);
            }
        }
    }

    public void showSnackBar(@StringRes int message, boolean isError) {
        if (mAbsBaseFragment != null) {
            mAbsBaseFragment.showSnackBar(message, isError);
        } else {
            if (mAbsBaseFragmentDialog != null) {
                mAbsBaseFragmentDialog.showSnackBar(message, isError);
            }
        }
    }

    public void showSnackBarFromContext(Context context, String error) {
        if (context instanceof AbsBindingActivity && ((AbsBindingActivity<?>) context).getRootView() != null) {
            SnackBarUtil.showSnackBar(((AbsBindingActivity<?>) context).getRootView(), error, true);
        }
    }
}
