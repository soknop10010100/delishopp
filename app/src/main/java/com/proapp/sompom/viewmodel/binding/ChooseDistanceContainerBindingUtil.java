package com.proapp.sompom.viewmodel.binding;

import androidx.databinding.BindingAdapter;

import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.emun.ChooseDistanceItem;
import com.proapp.sompom.widget.choosedistance.ChooseDistanceContainer;

/**
 * Created by nuonveyo on 7/5/18.
 */

public final class ChooseDistanceContainerBindingUtil {
    private ChooseDistanceContainerBindingUtil() {
    }

    @BindingAdapter({"addItem", "choosePosition", "onItemClick"})
    public static void addItem(ChooseDistanceContainer container,
                               ChooseDistanceItem[] items,
                               int choosePosition,
                               OnItemClickListener<ChooseDistanceItem> listener) {
        container.removeAllViews();
        for (ChooseDistanceItem item : items) {
            container.addView(item, choosePosition);
        }
        container.setOnItemClickListener(listener);
    }
}
