package com.proapp.sompom.viewmodel;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;

import com.google.gson.Gson;
import com.proapp.sompom.helper.upload.PhoneNumberHelper;
import com.proapp.sompom.intent.ConfirmCodeIntent;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.emun.AuthType;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.sompom.baseactivity.ResultCallback;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class ValidatePhoneNumberViewModel extends AbsSignUpViewModel {

    private final ObservableField<String> mPhoneNumber = new ObservableField<>();
    private ValidatePhoneNumberViewModelListener mListener;
    private AuthType mAuthType;
    private boolean mIsIgnoreFirebaseTokenValidation;

    public ValidatePhoneNumberViewModel(AppCompatActivity appCompatActivity,
                                        AuthType authType,
                                        boolean isIgnoreFirebaseTokenValidation,
                                        ValidatePhoneNumberViewModelListener listener) {
        super(appCompatActivity);
        mIsIgnoreFirebaseTokenValidation = isIgnoreFirebaseTokenValidation;
        mBackButtonVisibility.set(View.VISIBLE);
        mListener = listener;
        mAuthType = authType;
    }

    public ObservableField<String> getPhoneNumber() {
        return mPhoneNumber;
    }

    @Override
    protected boolean shouldEnableActionButton() {
        return isValidPhoneNumber();
    }

    @Override
    public void onActionButtonClicked() {
        if (isValidPhoneNumber()) {
            Timber.i("Code: " + mListener.getCountryCodePicker().getSelectedCountryCode() +
                    ", getNationalNumber: " + mListener.getCountryCodePicker().getPhoneNumber().getNationalNumber() +
                    "getFullNumber: " + mListener.getCountryCodePicker().getFullNumber());
            requestValidatePhoneNumber(mAuthType,
                    String.valueOf(mListener.getCountryCodePicker().getPhoneNumber().getNationalNumber()),
                    mListener.getCountryCodePicker().getSelectedCountryCode(),
                    mListener.getCountryCodePicker().getFullNumberWithPlus(),
                    mIsIgnoreFirebaseTokenValidation,
                    null,
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            if (resultCode == AppCompatActivity.RESULT_OK) {
                                ExchangeAuthData exchangeAuthData = data.getParcelableExtra(ConfirmCodeIntent.DATA);
                                Timber.i("onActivityResultSuccess: resultCode: " + resultCode + ", Data: " + new Gson().toJson(exchangeAuthData));
                                //Will pass result to the caller.
                                ((AbsBaseActivity) mContext).setResult(AppCompatActivity.RESULT_OK, data);
                                ((AbsBaseActivity) mContext).finish();
                            }
                        }
                    });
        }
    }

    private boolean isValidPhoneNumber() {
        return mListener.getCountryCodePicker().isValid();
    }

    public interface ValidatePhoneNumberViewModelListener {
        CountryCodePicker getCountryCodePicker();
    }

    @BindingAdapter({"setLoadDefaultCountryCode", "setEditText"})
    public static void setCountryCodePicker(CountryCodePicker picker, boolean shouldLoad, EditText editText) {
        picker.registerPhoneNumberTextView(editText);
        if (shouldLoad) {
            picker.setDefaultCountryUsingNameCodeAndApply(PhoneNumberHelper.getDefaultCountry(editText.getContext()));
        }
    }
}
