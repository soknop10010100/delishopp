package com.proapp.sompom.viewmodel.newviewmodel;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.text.TextWatcher;

import androidx.databinding.ObservableField;

import com.google.gson.Gson;
import com.proapp.sompom.helper.CheckRequestLocationCallbackHelper;
import com.proapp.sompom.helper.DelayWatcher;
import com.proapp.sompom.helper.SingleRequestLocation;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.SelectAddressActivity;
import com.proapp.sompom.services.datamanager.SelectAddressDataManager;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/11/18.
 */

public class SearchAddressFragmentViewModel extends AbsBaseViewModel {

    private static final int MAX_ADDRESS_RESULT = 30;
    public final ObservableField<String> mContent = new ObservableField<>();
    private final Context mContext;
    private final OnCallback mOnCallback;
    private final SelectAddressDataManager mSelectAddressDataManager;
    private SearchAddressResult mSearchAddressResult;

    public SearchAddressFragmentViewModel(SelectAddressDataManager selectAddressDataManager,
                                          SearchAddressResult searchAddressResult,
                                          OnCallback onCallback) {
        Timber.i("searchAddressResult: " + searchAddressResult);
        mSelectAddressDataManager = selectAddressDataManager;
        mSearchAddressResult = searchAddressResult;
        mContext = selectAddressDataManager.getContext();
        mOnCallback = onCallback;

        if (mSearchAddressResult != null && mSearchAddressResult.isValid()) {
            //Add current location for address result in case there is none location from the result screen.
//            if (mSearchAddressResult.getLocations() == null) {
//                requestLocation();
//            } else {
//                mContent.set(mSearchAddressResult.getValidFullPlaceAddress());
//                onSearchAddress(SearchType.GOOGLE_PLAY_API);
//            }
            mContent.set(mSearchAddressResult.getValidFullPlaceAddress());
            onSearchAddress(SearchType.GOOGLE_PLAY_API);
        }
    }

    public TextWatcher onSearchAddressTextChanged() {
        return new DelayWatcher(true) {
            @Override
            public void onTextChanged(String text) {
                //clear previous api
                clearDisposable();

                mContent.set(text);
                onSearchAddress(SearchType.GOOGLE_PLAY_API);
            }
        };
    }

    private void onSearchAddress(SearchType searchType) {
        if (searchType == SearchType.GEO_CODER) {
            Observable<List<SearchAddressResult>> observable = Observable.just(mContent.get()).concatMap(content -> {
                Geocoder geocoder = new Geocoder(mContext);
                List<Address> addressList = geocoder.getFromLocationName(content, MAX_ADDRESS_RESULT);
                Timber.e("addressList: %s", new Gson().toJson(addressList));
                if (addressList != null && !addressList.isEmpty()) {
                    List<SearchAddressResult> searchAddressResults = new ArrayList<>();
                    for (Address address : addressList) {
                        SearchAddressResult searchAddressResult = new SearchAddressResult();
                        searchAddressResult.setFullAddress(address.getAddressLine(0));
                        searchAddressResult.setCity(address.getLocality());
                        searchAddressResult.setCountry(address.getCountryName());

                        Locations locations = new Locations();
                        locations.setLatitude(address.getLatitude());
                        locations.setLongitude(address.getLongitude());
                        locations.setCountry(address.getCountryCode());
                        searchAddressResult.setLocations(locations);
                        searchAddressResults.add(searchAddressResult);
                    }
                    return Observable.just(searchAddressResults);
                } else {
                    return mSelectAddressDataManager.searchAddress(content);
                }
            });
            addDisposable(observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(addressResults -> {
                        if (mOnCallback != null) {
                            mOnCallback.onGetAddressSuccess(addressResults);
                        }
                    }, throwable -> {
                        Timber.e(throwable.toString());
                        if (mOnCallback != null) {
                            mOnCallback.onGetAddressSuccess(new ArrayList<>());
                        }
                    }));
        } else {
            searchAddressFromGooglePlace();
        }
    }

    private void searchAddressFromGooglePlace() {
        Observable<List<SearchAddressResult>> observable = mSelectAddressDataManager.searchAddress(mContent.get());
        addDisposable(observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(addressResults -> {
                    if (mOnCallback != null) {
                        mOnCallback.onGetAddressSuccess(addressResults);
                    }
                }, throwable -> {
                    if (mOnCallback != null) {
                        mOnCallback.onGetAddressSuccess(new ArrayList<>());
                    }
                }));
    }

    public void searchDetailAddress(SearchAddressResult searchAddressResult, OnSearchAddressDetailListener onSearchAddressDetailListener) {
        Observable<SearchAddressResult> observable = mSelectAddressDataManager.searchAddressDetail(searchAddressResult);
        addDisposable(observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onSearchAddressDetailListener::searchAddressSuccess, throwable -> Timber.e("search address detail fail!")));
    }

    public void onSetLocationOnMapClick() {
        SingleRequestLocation.reset();
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).requestLocation(true,
                    new CheckRequestLocationCallbackHelper((Activity) mContext,
                            location -> {
                        Timber.i("onComplete request location: " + location.toString());
                        if (mContext instanceof SelectAddressActivity) {
                            ((SelectAddressActivity) mContext).navigateToSetLocationOnMap();
                        }
                    }));
        }
    }

    public void onBackPress() {
        if (mContext instanceof SelectAddressActivity) {
            ((SelectAddressActivity) mContext).onBackPressed();
        }
    }

    public enum SearchType {
        GEO_CODER, GOOGLE_PLAY_API
    }

    public interface OnCallback {
        void onGetAddressSuccess(List<SearchAddressResult> list);
    }

    public interface OnSearchAddressDetailListener {
        void searchAddressSuccess(SearchAddressResult addressResult);
    }
}
