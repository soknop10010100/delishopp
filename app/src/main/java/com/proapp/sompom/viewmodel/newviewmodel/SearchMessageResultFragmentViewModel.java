package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.TextUtils;

import androidx.databinding.ObservableBoolean;

import com.google.gson.Gson;
import com.proapp.sompom.broadcast.NetworkBroadcastReceiver;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.helper.ConversationHelper;
import com.proapp.sompom.helper.UserSearchDataHolder;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.model.Search;
import com.proapp.sompom.model.SearchGeneralTypeResult;
import com.proapp.sompom.model.emun.GeneralSearchResultType;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.SearchGeneralTypeResultDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class SearchMessageResultFragmentViewModel extends AbsLoadingViewModel {

    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private SearchGeneralTypeResultDataManager mDataManager;
    private SearchMessageResultFragmentViewModelListener mOnCallback;
    private String mSearchText;
    private NetworkBroadcastReceiver.NetworkListener mNetworkListener;
    private boolean mIsLoadedData;
    private boolean mIsRefreshData;

    public SearchMessageResultFragmentViewModel(SearchGeneralTypeResultDataManager dataManager,
                                                SearchMessageResultFragmentViewModelListener listener) {
        super(dataManager.getContext());
        setShowKeyboardWhileLoadingScreen();
        mDataManager = dataManager;
        mOnCallback = listener;
        showLoading();
        if (getContext() instanceof AbsBaseActivity) {
            mNetworkListener = networkState -> {
                mDataManager.setNetworkAvailable((networkState ==
                        NetworkBroadcastReceiver.NetworkState.Connected));
                if (!mIsLoadedData) {
                    if (mIsRefreshData) {
                        if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                            getSearchData(mSearchText);
                        }
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkListener);
        }
    }

    public void setDataManager(SearchGeneralTypeResultDataManager dataManager) {
        mDataManager = dataManager;
    }

    public SearchGeneralTypeResultDataManager getDataManager() {
        return mDataManager;
    }

    public void setSearchText(String searchText) {
        mSearchText = searchText;
    }

    public String getSearchText() {
        return mSearchText;
    }

    public SearchMessageResultFragmentViewModel(Context context) {
        super(context);
    }

    public void requestGroupDetail(String conversationId,
                                   SearchMessageResultFragmentViewModel childViewModel,
                                   HomeNotificationFragmentViewModel.HomeNotificationFragmentViewModelListener listener) {
        childViewModel.showLoading(true);
        Observable<Response<Conversation>> ob = mDataManager.getConversationDetail(conversationId);
        ResponseObserverHelper<Response<Conversation>> call = new ResponseObserverHelper<>(getContext(), ob);
        addDisposable(call.execute(new OnCallbackListener<Response<Conversation>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                childViewModel.hideLoading();
                childViewModel.showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<Conversation> result) {
                childViewModel.hideLoading();
                if (result.body() != null && ConversationHelper.isConversationOfCurrentUser(getContext(), result.body())) {
                    ConversationDb.save(getContext(), result.body(), "requestGroupDetail");
                    if (listener != null) {
                        listener.onLoadGroupConversationSuccess(result.body());
                    }
                }
            }
        }));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity && mNetworkListener != null) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkListener);
        }
    }

    public void performLocalUserContactFilter(String query) {
        Timber.i("performLocalUserContactFilter: " + query);
        UserSearchDataHolder.getInstance().filerUserInList(query,
                result -> {
                    mDataManager.setCurrentLocalUserFilterResult(result);
                    SearchGeneralTypeResult generalTypeResult1 = buildLocalFilterResult(result);
                    Timber.i("generalTypeResult: " + new Gson().toJson(generalTypeResult1));
                    if (mOnCallback != null) {
                        mOnCallback.onComplete(generalTypeResult1, true, false);
                    }
                });
    }

    private SearchGeneralTypeResult buildLocalFilterResult(List<User> userList) {
        SearchGeneralTypeResult generalTypeResult = new SearchGeneralTypeResult();

        //User
        generalTypeResult.setUser(new LoadMoreWrapper<>(null, userList));

        //Set only default data for other types
        generalTypeResult.setChatList(new LoadMoreWrapper<>(null, new ArrayList<>()));
        generalTypeResult.setProduct(new LoadMoreWrapper<>(null, new ArrayList<>()));
        generalTypeResult.setBuyingConversation(new LoadMoreWrapper<>(null, new ArrayList<>()));
        generalTypeResult.setSellingConversation(new LoadMoreWrapper<>(null, new ArrayList<>()));

        return generalTypeResult;
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        getSearchData(mSearchText);
    }

    public void getData(String searchText) {
        if (!TextUtils.equals(searchText, mSearchText)) {
            Timber.i("getData: " + searchText);
            mSearchText = searchText;
            mIsRefreshData = true;
            getSearchData(searchText);
        }
    }

    public void onFollowClick(User user, boolean isFollow) {
        Observable<Response<Object>> callback;
        if (isFollow) {
            callback = mDataManager.postFollow(user.getId());
        } else {
            callback = mDataManager.unFollow(user.getId());
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute());
    }

    private void getSearchData(String searchText) {
        mSearchText = searchText;
        Observable<Response<SearchGeneralTypeResult>> callback = mDataManager.searchGeneralByType(mSearchText);
        ResponseObserverHelper<Response<SearchGeneralTypeResult>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<SearchGeneralTypeResult>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mOnCallback.onFail(ex, buildLocalFilterResult(new ArrayList<>()));
            }

            @Override
            public void onComplete(Response<SearchGeneralTypeResult> result) {
                mIsRefresh.set(false);
                mIsLoadedData = true;
                mOnCallback.onComplete(result.body(), false, mDataManager.isCanLoadMore());
            }
        }));
    }

    public void loadMore(GeneralSearchResultType type, OnCallbackListListener<List<Search>> listener) {
        Timber.i("loadMore: type: " + type + ", mSearchText: " + mSearchText);
        Observable<Response<List<Search>>> callback = mDataManager.loadMore(mSearchText, type);
        ResponseObserverHelper<Response<List<Search>>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Search>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (listener != null) {
                    listener.onFail(ex);
                }
            }

            @Override
            public void onComplete(Response<List<Search>> result) {
                if (listener != null) {
                    listener.onComplete(result.body(), mDataManager.isCanLoadMore(type));
                }
            }
        }));
    }

    public boolean isCanLoadMore(GeneralSearchResultType resultType) {
        return mDataManager.isCanLoadMore(resultType);
    }

    public OnClickListener onRetryButtonClick() {
        return () -> getSearchData(mSearchText);
    }

    public interface SearchMessageResultFragmentViewModelListener {

        void onFail(ErrorThrowable ex, SearchGeneralTypeResult defaultData);

        void onComplete(SearchGeneralTypeResult result, boolean isLocalData, boolean canLoadMore);
    }
}
