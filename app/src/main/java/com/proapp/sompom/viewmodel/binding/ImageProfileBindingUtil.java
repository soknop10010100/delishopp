package com.proapp.sompom.viewmodel.binding;

import android.text.TextUtils;

import androidx.databinding.BindingAdapter;

import com.proapp.sompom.R;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.widget.ConversationProfileView;
import com.proapp.sompom.widget.GroupDetailProfileView;
import com.proapp.sompom.widget.ImageProfileLayout;

import java.util.List;

/**
 * Created by nuonveyo on 8/22/18.
 */

public final class ImageProfileBindingUtil {
    private ImageProfileBindingUtil() {
    }

    @BindingAdapter(value = {"setUserStatusImageProfile", "simpleUrl", "itemPosition"}, requireAll = false)
    public static void setUserStatusImageProfile(ImageProfileLayout layout,
                                                 User user,
                                                 String simpleUrl,
                                                 Integer itemPosition) {
        if (TextUtils.isEmpty(simpleUrl)) {
            if (user == null) {
                user = new User();
            }
            if (itemPosition != null) {
                layout.setUser(user, false, itemPosition);
            } else {
                layout.setUser(user, false);
            }
        } else {
            layout.loadSimpleUrl(simpleUrl);
        }
    }

    @BindingAdapter({"setUserStatusImageProfileComment", "isReplyComment"})
    public static void setUserStatusImageProfileComment(ImageProfileLayout layout, User user, boolean isReplyComment) {
        if (user == null) {
            user = new User();
        }
        int dimension = layout.getResources().getDimensionPixelSize(R.dimen.user_profile_medium_size);
        if (isReplyComment) {
            dimension = layout.getResources().getDimensionPixelSize(R.dimen.user_profile_small_size);
        }
        layout.setUser(user, dimension);
    }

    @BindingAdapter(value = {"participants", "groupImgUrl", "groupName"}, requireAll = false)
    public static void setParticipantProfile(ConversationProfileView layout,
                                             List<User> participants,
                                             String groupImgUrl,
                                             String groupName) {
        if (participants != null && !participants.isEmpty()) {
            layout.bindData(layout.getContext(), participants, groupImgUrl, groupName);
        }
    }

    @BindingAdapter(value = {"addGroupItem", "addGroupImage", "addGroupName", "conversationId"}, requireAll = false)
    public static void setGroupProfile(GroupDetailProfileView layout,
                                       List<User> participants,
                                       String groupImgUrl,
                                       String groupName,
                                       String conversationId) {
        layout.addGroupItem(layout.getContext(), conversationId, participants, groupImgUrl, groupName);
    }
}
