package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.request.TelegramRedirectBody;
import com.proapp.sompom.model.result.TelegramRedirectUser;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

public class TelegramUserRedirectFragmentViewModel extends AbsLoadingViewModel {
    public ObservableBoolean mIsRefresh = new ObservableBoolean();

    private ApiService mApiService;
    private Listener mListener;
    private String mTelegramUserId;


    public TelegramUserRedirectFragmentViewModel(Context context, ApiService apiService, String telegramUserId, TelegramUserRedirectFragmentViewModel.Listener listener) {
        super(context);
        mApiService = apiService;
        mListener = listener;
        mTelegramUserId = telegramUserId;
        getRedirectUserList();
    }

    private void getRedirectUserList() {
        Observable<Response<List<TelegramRedirectUser>>> ob = mApiService.getTelegramRedirectUsers(mTelegramUserId);
        BaseObserverHelper<Response<List<TelegramRedirectUser>>> helper = new BaseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<TelegramRedirectUser>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsRefresh.set(false);
                Timber.e(ex.toString());
            }

            @Override
            public void onComplete(Response<List<TelegramRedirectUser>> result) {
                mIsRefresh.set(false);
                mListener.onGetSuccess(result.body());
            }
        }));
    }

    public void redirectTelegramUserTo(String telegramUserId, String appgoraUserId) {
        final TelegramRedirectBody body = new TelegramRedirectBody(telegramUserId, appgoraUserId);
        Observable<Response<Object>> ob = mApiService.redirectTelegramUser(body);
        BaseObserverHelper<Response<Object>> helper = new BaseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e(ex.toString());
            }

            @Override
            public void onComplete(Response<Object> result) {
                getRedirectUserList();
            }
        }));
    }

    public void onButtonRetryClick() {
        //  TODO: Not implemented yet
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        getRedirectUserList();
    }

    public interface Listener {
        void onGetSuccess(List<TelegramRedirectUser> users);
    }
}
