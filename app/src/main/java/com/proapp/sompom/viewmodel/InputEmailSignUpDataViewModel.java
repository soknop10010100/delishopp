package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ObservableField;

import com.google.gson.Gson;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.NotificationServiceHelper;
import com.proapp.sompom.intent.ConfirmCodeIntent;
import com.proapp.sompom.intent.ValidatePhoneNumberIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.emun.AuthType;
import com.proapp.sompom.model.request.SignUpUserBody;
import com.proapp.sompom.model.result.SignUpResponse2;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.InputEmailSignUpDataManager;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.sompom.baseactivity.ResultCallback;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class InputEmailSignUpDataViewModel extends AbsSignUpViewModel {

    private final ObservableField<String> mFirstName = new ObservableField<>();
    private final ObservableField<String> mLastName = new ObservableField<>();
    private final InputEmailSignUpDataManager mDataManager;

    public InputEmailSignUpDataViewModel(Context context, InputEmailSignUpDataManager dataManager) {
        super(context);
        mDataManager = dataManager;
        mEmail.set(dataManager.getEmail());
    }

    public ObservableField<String> getFirstName() {
        return mFirstName;
    }

    public ObservableField<String> getLastName() {
        return mLastName;
    }

    @Override
    protected boolean shouldEnableActionButton() {
        return isEmailValid() && isUserNameValid() && arePasswordValid();
    }

    @Override
    public void onActionButtonClicked() {
        if (mContext instanceof Activity) {
            KeyboardUtil.hideKeyboard((Activity) mContext);
        }

        if (isPasswordMatch() && isPasswordCorrect()) {
            ((AbsBaseActivity) mContext).startActivityForResult(new ValidatePhoneNumberIntent(mContext,
                            AuthType.SIGN_UP),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            if (resultCode == AppCompatActivity.RESULT_OK) {
                                ExchangeAuthData exchangeAuthData = data.getParcelableExtra(ConfirmCodeIntent.DATA);
                                Timber.i("onActivityResultSuccess: resultCode: " + resultCode + ", Data: " + new Gson().toJson(exchangeAuthData));
                                if (exchangeAuthData != null) {
                                    requestRegister(exchangeAuthData.getPhoneNumber(), exchangeAuthData.getCountyCode());
                                }
                            }
                        }
                    });
        }
    }

    private boolean isUserNameValid() {
        return mFirstName.get() != null &&
                mLastName.get() != null &&
                !TextUtils.isEmpty(mFirstName.get().trim()) && !TextUtils.isEmpty(mLastName.get().trim());
    }

    private SignUpUserBody getSignupBody(String phoneNumber, String countryCode) {
        SignUpUserBody body = new SignUpUserBody(mEmail.get(), mPassword.get());
        body.setFirstName(mFirstName.get());
        body.setLastName(mLastName.get());
        body.setPhone(phoneNumber);
        body.setCountryCode(countryCode);

        return body;
    }

    private void requestRegister(String phoneNumber, String countryCode) {
        showLoading(true);
        Observable<Response<SignUpResponse2>> loginService = mDataManager.getRegisterViaEmailService(getSignupBody(phoneNumber, countryCode));
        ResponseObserverHelper<Response<SignUpResponse2>> helper = new ResponseObserverHelper<>(mDataManager.mContext,
                loginService);
        addDisposable(helper.execute(new OnCallbackListener<Response<SignUpResponse2>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e("onFail: " + new Gson().toJson(ex));
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<SignUpResponse2> result) {
                Timber.i("onComplete");
                if (!result.body().isError()) {
                    requestRecordPlayerId();
                } else {
                    hideLoading();
                    showSnackBar(result.body().getErrorMessage(), true);
                }
            }
        }));
    }

    protected void requestRecordPlayerId() {
        NotificationServiceHelper.loadPlayerId(getContext(), (playerId, type) -> {
            if (!TextUtils.isEmpty(playerId)) {
                Observable<Response<Object>> loginService = mDataManager.addPlayerId(NotificationServiceHelper.getAddPlayerIdRequestBody(playerId, type));
                ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(),
                        loginService);
                addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        hideLoading();
                        //Clear logged in user value previously saved.
                        SharedPrefUtils.clearValue(mContext);
                        showSnackBar(ex.getMessage(), true);
                    }

                    @Override
                    public void onComplete(Response<Object> result) {
                        navigateToHomeScreen();
                    }
                }));
            } else {
                navigateToHomeScreen();
            }
        });
    }

    private void navigateToHomeScreen() {
        hideLoading();
        getContext().startActivity(ApplicationHelper.getFreshHomeIntent(getContext()));
    }
}
