package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.SingleRequestLocation;
import com.proapp.sompom.helper.upload.PhoneNumberHelper;
import com.proapp.sompom.intent.ConfirmCodeIntent;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.emun.AuthType;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.utils.NetworkStateUtil;
import com.proapp.sompom.utils.SpecialTextRenderUtils;
import com.sompom.baseactivity.ResultCallback;

import java.util.concurrent.TimeUnit;

import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;
import timber.log.Timber;

public class AbsAuthViewModel extends AbsLoadingViewModel {

    protected final ObservableField<String> mEmail = new ObservableField<>();
    protected PhoneNumberUtil mPhoneNumberUtil;
    protected String mValidPhoneInput;
    protected String mValidaPhoneCountryInput;
    protected String mValidEmailInput;
    protected String mDefaultUserCountryCode;

    public AbsAuthViewModel(Context context) {
        super(context);
        if (shouldLoadUserDefaultCountryCode()) {
            mDefaultUserCountryCode = PhoneNumberHelper.getDefaultCountry(context);
            Timber.i("mDefaultUserCountryCode: " + mDefaultUserCountryCode);
        }
    }

    protected boolean shouldLoadUserDefaultCountryCode() {
        return false;
    }

    public void checkToLoadUserDefaultCountyCodeFromLocationIfNecessary() {
        if (TextUtils.isEmpty(mDefaultUserCountryCode) && getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).requestLocation(true, new SingleRequestLocation.Callback() {
                @Override
                public void onComplete(Locations location, boolean isNeverAskAgain, boolean isPermissionEnabled, boolean isLocationEnabled) {
                    Timber.i("onComplete: " + new Gson().toJson(location));
                    if (location != null) {
                        mDefaultUserCountryCode = location.getCountry();
                    }
                    if (TextUtils.isEmpty(mDefaultUserCountryCode)) {
                        mDefaultUserCountryCode = PhoneNumberHelper.DEFAULT_COUNTY;
                    }
                    if (!TextUtils.isEmpty(mDefaultUserCountryCode)) {
                        mDefaultUserCountryCode = mDefaultUserCountryCode.toUpperCase();
                    }
                }
            });
        }
    }

    public ObservableField<String> getEmail() {
        return mEmail;
    }

    protected boolean isEmailValid() {
        return checkIfEmailValid(mEmail.get());
    }

    protected boolean checkIfEmailValid(String email) {
        return email != null &&
                !TextUtils.isEmpty(email.trim())
                && SpecialTextRenderUtils.isEmailAddress(email.trim());
    }

    protected String getFullPhoneNumberFromInput() {
        return "+" + mValidaPhoneCountryInput + mValidPhoneInput;
    }

    protected void requestValidatePhoneNumber(AuthType authType,
                                              String phoneNumber,
                                              String countryCode,
                                              String fullNumberWithPlus,
                                              boolean isIgnoreFirebaseTokenValidation,
                                              ValidatePhoneListener listener,
                                              ResultCallback resultCallback) {
        if (!NetworkStateUtil.isNetworkAvailable(mContext)) {
            showSnackBar(mContext.getString(R.string.error_internet_connection_description), true);
            return;
        }

        showLoading(true);
        PhoneAuthOptions options =
                PhoneAuthOptions.newBuilder(FirebaseAuth.getInstance())
                        .setPhoneNumber(fullNumberWithPlus)
                        .setTimeout(60L, TimeUnit.SECONDS)
                        .setActivity((Activity) mContext)
                        .setCallbacks(new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                            @Override
                            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                hideLoading();
                                Timber.i("onVerificationCompleted: " + phoneAuthCredential.getSmsCode());

                                // If we receive sms code here, meaning auto sms retrieval was
                                // triggered, do nothing and let the app's sms code retrieval handle
                                // like normal
                                //
                                // If sms code is null, meaning instant verification was triggered,
                                // use the auth here to sign in directly

                                if (phoneAuthCredential.getSmsCode() == null) {
                                    signInWithPhoneAuthCredential(phoneAuthCredential,
                                            authType,
                                            phoneNumber,
                                            countryCode,
                                            null,
                                            resultCallback);
                                }
                            }

                            @Override
                            public void onVerificationFailed(@NonNull FirebaseException e) {
//                                Timber.e("onVerificationFailed: " + e.getMessage() + ", Cause: " + e.getCause());
                                hideLoading();
                                showSnackBar(R.string.verify_error_firebase_token_invalid, true);
                            }

                            @Override
                            public void onCodeSent(@NonNull String verificationId, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                super.onCodeSent(verificationId, forceResendingToken);
                                Timber.i("onCodeSent: " + verificationId);
                                hideLoading();
                                if (listener != null) {
                                    listener.onSendCodeSuccess(verificationId);
                                }
                                if (resultCallback != null) {
                                    startConfirmCodeScreen(authType,
                                            phoneNumber,
                                            countryCode,
                                            verificationId,
                                            isIgnoreFirebaseTokenValidation,
                                            resultCallback);
                                }
                            }

                            @Override
                            public void onCodeAutoRetrievalTimeOut(@NonNull String verificationId) {
                                super.onCodeAutoRetrievalTimeOut(verificationId);
                                hideLoading();
                                Timber.e("onCodeAutoRetrievalTimeOut: verificationId: " + verificationId);
                                showSnackBar(R.string.error_general_description, true);
                            }
                        }).build();
        PhoneAuthProvider.verifyPhoneNumber(options);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential phoneAuthCredential,
                                               AuthType authType,
                                               String phoneNumber,
                                               String countryCode,
                                               String verificationId,
                                               ResultCallback resultCallback) {
        FirebaseAuth
                .getInstance()
                .signInWithCredential(phoneAuthCredential)
                .addOnCompleteListener((Activity) mContext, task -> {
                    if (task.isSuccessful()) {
                        Timber.i("signInWithCredential:success");
                        ExchangeAuthData data = createExchangeAuthData(authType, phoneNumber, countryCode, verificationId);

                        task.getResult().getUser().getIdToken(false).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                            @Override
                            public void onComplete(@NonNull Task<GetTokenResult> task) {
                                if (task.isSuccessful()) {
                                    hideLoading();
                                    startAutoConfirmCodeScreen(data, task.getResult().getToken(), resultCallback);
                                } else {
                                    hideLoading();
                                    showSnackBar(R.string.error_general_description, true);
                                }
                            }
                        });
                    } else {
                        hideLoading();
                        Timber.e("signInWithCredential:failure: " + task.getException());
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            showSnackBar(R.string.verify_phone_invalid_code, true);
                        } else {
                            showSnackBar(R.string.error_general_description, true);
                        }
                    }
                });
    }

    private ExchangeAuthData createExchangeAuthData(AuthType authType,
                                                    String phoneNumber,
                                                    String countryCode,
                                                    String verificationId) {
        ExchangeAuthData data = new ExchangeAuthData();
        data.setExchangeAuthType(authType.getValue());
        data.setPhoneNumber(String.valueOf(phoneNumber));
        data.setVerificationId(verificationId);
        data.setCountyCode(countryCode);

        return data;
    }

    private void startAutoConfirmCodeScreen(ExchangeAuthData data,
                                            String firebaseToken,
                                            ResultCallback resultCallback) {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).startActivityForResult(new ConfirmCodeIntent(mContext, data, firebaseToken), resultCallback);
        }
    }

    private void startConfirmCodeScreen(AuthType authType,
                                        String phoneNumber,
                                        String countryCode,
                                        String verificationId,
                                        boolean isIgnoreFirebaseTokenValidation,
                                        ResultCallback resultCallback) {
        if (mContext instanceof AbsBaseActivity) {
            ExchangeAuthData data = new ExchangeAuthData();
            data.setExchangeAuthType(authType.getValue());
            data.setPhoneNumber(String.valueOf(phoneNumber));
            data.setVerificationId(verificationId);
            data.setCountyCode(countryCode);
            data.setIgnoreFirebaseTokenValidation(isIgnoreFirebaseTokenValidation);
            ((AbsBaseActivity) mContext).startActivityForResult(new ConfirmCodeIntent(mContext, data), resultCallback);
        }
    }

    protected boolean isPhoneNumberValid(String identifier) {
        try {
            Timber.i("mDefaultUserCountryCode: " + mDefaultUserCountryCode);
            Phonenumber.PhoneNumber phoneNumber = mPhoneNumberUtil.parse(identifier, mDefaultUserCountryCode);
            boolean validNumber = mPhoneNumberUtil.isValidNumber(phoneNumber) && isPhoneNumberValid2(identifier);
            Timber.i("isValidNumber: " + validNumber);
            if (phoneNumber != null) {
                Timber.i("getCountryCode: " + phoneNumber.getCountryCode() + ", phoneNumber:" + phoneNumber.getNationalNumber());
            }
            if (validNumber && phoneNumber != null) {
                mValidaPhoneCountryInput = String.valueOf(phoneNumber.getCountryCode());
                mValidPhoneInput = String.valueOf(phoneNumber.getNationalNumber());
                mValidEmailInput = null;
            } else {
                mValidaPhoneCountryInput = null;
                mValidPhoneInput = null;
            }
            return validNumber;
        } catch (Exception e) {
            Timber.e(e.getMessage());
        }

        return false;
    }

    private boolean isPhoneNumberValid2(String phone) {
        if (phone != null && !TextUtils.isEmpty(phone)) {
            return Patterns.PHONE.matcher(phone).matches();
        }

        return false;
    }

    protected boolean isIdentifierValidEmail(String identifier) {
        boolean validEmail = checkIfEmailValid(identifier);
        if (validEmail) {
            mValidEmailInput = identifier.trim();
            mValidaPhoneCountryInput = null;
            mValidPhoneInput = null;
        } else {
            mValidEmailInput = null;
        }

        return validEmail;
    }

    public interface ValidatePhoneListener {
        void onSendCodeSuccess(String verificationId);
    }
}
