package com.proapp.sompom.viewmodel.newviewmodel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.CheckRequestLocationCallbackHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.DelayWatcher;
import com.proapp.sompom.intent.newintent.ConciergeNewAddressIntent;
import com.proapp.sompom.intent.newintent.ConciergeSelectLocationIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.ConciergeCurrentLocationDisplayAdaptive;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.ConciergeCurrentLocationDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;
import com.sompom.baseactivity.ResultCallback;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 16/11/21.
 */
public class ConciergeCurrentLocationFragmentViewModel extends AbsLoadingViewModel {

    private static final int MAX_ADDRESS_RESULT = 30;

    private final ObservableField<String> mSearchText = new ObservableField<>("");
    private final ObservableField<String> mCurrentAddress = new ObservableField<>();
    private final ObservableBoolean mSavedAddressLabelVisibility = new ObservableBoolean();
    private final ObservableField<String> mAddressListLabel = new ObservableField<>();

    private ConciergeUserAddress mUserCurrentAddress;
    private final ConciergeCurrentLocationDataManager mDataManager;
    private ConciergeCurrentLocationFragmentViewModelListener mListener;

    public ConciergeCurrentLocationFragmentViewModel(Context context,
                                                     ConciergeCurrentLocationDataManager dataManager,
                                                     ConciergeCurrentLocationFragmentViewModelListener listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
        initData();
    }

    public ObservableField<String> getSearchText() {
        return mSearchText;
    }

    public ObservableField<String> getCurrentAddress() {
        return mCurrentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        mCurrentAddress.set(currentAddress);
    }

    public ConciergeUserAddress getUserCurrentAddress() {
        return mUserCurrentAddress;
    }

    public ObservableBoolean getSavedAddressLabelVisibility() {
        return mSavedAddressLabelVisibility;
    }

    public ObservableField<String> getAddressListLabel() {
        return mAddressListLabel;
    }

    public void initData() {
        // Set current location label to locating for now, or if user has current location from
        // concierge home screen
        setCurrentAddress(mContext.getString(R.string.shop_current_location_locating_label) + "...");
        loadUserCurrentLocation();
        loadUserSavedAddress();
    }

    public void loadUserSavedAddress() {
        Observable<List<ConciergeUserAddress>> ob = mDataManager.getUserAddresses(false, false);
        BaseObserverHelper<List<ConciergeUserAddress>> helper = new BaseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(new OnCallbackListener<List<ConciergeUserAddress>>() {
            @Override
            public void onComplete(List<ConciergeUserAddress> result) {
                showSavedAddressListLabel(result != null && !result.isEmpty());
                if (mListener != null) {
                    mListener.onLoadSavedAddress(result);
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                showError(ex.getMessage());
            }
        }));
    }

    public void showSavedAddressListLabel(boolean isShow) {
        mSavedAddressLabelVisibility.set(isShow);
        mAddressListLabel.set(mContext.getResources().getString(R.string.shop_current_location_saved_address_label));
    }

    public void showSearchAddressResultListLabel() {
        mSavedAddressLabelVisibility.set(true);
        mAddressListLabel.set(mContext.getResources().getString(R.string.shop_current_location_search_address_label));
    }

    public void onSetLocationOnMapClick(View view) {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).startActivityForResult(new ConciergeSelectLocationIntent(mContext), new ResultCallback() {
                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    ConciergeSelectLocationIntent intent = new ConciergeSelectLocationIntent(data);
                    SearchAddressResult result = intent.getSearchAddressResult();

                    if (result != null) {
                        ConciergeUserAddress address = new ConciergeUserAddress();

                        address.setLabel((result).getPlaceTitle());
                        address.setAddress((result).getFullAddress());
                        address.setLatitude((result).getLocations().getLatitude());
                        address.setLongitude((result).getLocations().getLongitude());

                        if (mListener != null) {
                            mListener.onLocationSelected(address);
                        }
                    }
                }
            });
        }
    }

    public void onCurrentLocationSelected(View view) {
        if (mUserCurrentAddress != null && mListener != null) {
            mListener.onLocationSelected(mUserCurrentAddress);
        }
    }

    public void onAddNewAddressClick(View view) {
        if (ApplicationHelper.isInVisitorMode(getContext())) {
            mListener.onRequireLogin();
        } else {
            openAddNewAddressScreen();
        }
    }

    public void openAddNewAddressScreen() {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).startActivityForResult(new ConciergeNewAddressIntent(mContext), new ResultCallback() {
                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    ConciergeUserAddress newAddress = data.getParcelableExtra(ConciergeNewAddressIntent.ADDRESS_DATA);
                    if (newAddress != null) {
                        if (mListener != null) {
                            mListener.onNewAddressAdded(newAddress);
                        }
                    }
                }
            });
        }
    }

    public TextWatcher onSearchAddressTextChanged() {
        return new DelayWatcher(true) {
            @Override
            public void onTextChanged(String text) {
                //clear previous api
                clearDisposable();

                mSearchText.set(text);

                // If search text is not empty, search normally
                if (!TextUtils.isEmpty(mSearchText.get())) {
                    onSearchAddress(SearchAddressFragmentViewModel.SearchType.GOOGLE_PLAY_API);
                } else {
                    // Else if search text is empty or null, call out API to get user's saved address
                    // or perhaps getting a saved instance instead.
                    loadUserSavedAddress();
                }
            }
        };
    }

    public void loadUserCurrentLocation() {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).requestLocation(true,
                    new CheckRequestLocationCallbackHelper((Activity) mContext,
                            location -> {
                                Timber.i("onComplete request location: " + location.toString());
                                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                                addDisposable(ConciergeHelper.getUserAddressFromLatLng(mContext, latLng, "")
                                        .subscribe(data -> {
                                            mUserCurrentAddress = data;
                                            mCurrentAddress.set(mUserCurrentAddress.getAddress());
                                        }, Timber::e));
                            }));
        }
    }

    private void onSearchAddress(SearchAddressFragmentViewModel.SearchType searchType) {
        if (searchType == SearchAddressFragmentViewModel.SearchType.GEO_CODER) {
            Observable<List<SearchAddressResult>> observable = Observable.just(mSearchText.get()).concatMap(content -> {
                Geocoder geocoder = new Geocoder(mContext);
                List<Address> addressList = geocoder.getFromLocationName(content, MAX_ADDRESS_RESULT);
                Timber.e("addressList: %s", new Gson().toJson(addressList));
                if (addressList != null && !addressList.isEmpty()) {
                    List<SearchAddressResult> searchAddressResults = new ArrayList<>();
                    for (Address address : addressList) {
                        SearchAddressResult searchAddressResult = new SearchAddressResult();
                        searchAddressResult.setFullAddress(address.getAddressLine(0));
                        searchAddressResult.setCity(address.getLocality());
                        searchAddressResult.setCountry(address.getCountryName());

                        Locations locations = new Locations();
                        locations.setLatitude(address.getLatitude());
                        locations.setLongitude(address.getLongitude());
                        locations.setCountry(address.getCountryCode());
                        searchAddressResult.setLocations(locations);
                        searchAddressResults.add(searchAddressResult);
                    }
                    return Observable.just(searchAddressResults);
                } else {
                    return mDataManager.searchAddress(content);
                }
            });
            addDisposable(observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(addressResults -> {
                        showSearchAddressResultListLabel();
                        if (mListener != null) {
                            mListener.onLoadSearchAddressResult(addressResults);
                        }
                    }, throwable -> {
                        showSearchAddressResultListLabel();
                        Timber.e(throwable.toString());
                        if (mListener != null) {
                            mListener.onLoadSearchAddressResult(new ArrayList<>());
                        }
                    }));
        } else {
            searchAddressFromGooglePlace();
        }
    }

    private void searchAddressFromGooglePlace() {
        Observable<List<SearchAddressResult>> observable = mDataManager.searchAddress(mSearchText.get());
        addDisposable(observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(addressResults -> {
                    showSearchAddressResultListLabel();
                    if (mListener != null) {
                        mListener.onLoadSearchAddressResult(addressResults);
                    }
                }, throwable -> {
                    showSearchAddressResultListLabel();
                    if (mListener != null) {
                        mListener.onLoadSearchAddressResult(new ArrayList<>());
                    }
                }));
    }

    public void onAdapterItemClicked(ConciergeCurrentLocationDisplayAdaptive adaptive) {
        if (adaptive instanceof SearchAddressResult) {
            ConciergeUserAddress address = new ConciergeUserAddress();

            address.setLabel(((SearchAddressResult) adaptive).getPlaceTitle());
            address.setAddress(((SearchAddressResult) adaptive).getFullAddress());
            address.setLatitude(((SearchAddressResult) adaptive).getLocations().getLatitude());
            address.setLongitude(((SearchAddressResult) adaptive).getLocations().getLongitude());

            if (mListener != null) {
                mListener.onLocationSelected(address);
            }
        } else if (adaptive instanceof ConciergeUserAddress) {
            if (mListener != null) {
                mListener.onLocationSelected((ConciergeUserAddress) adaptive);
            }
        }
    }

    public interface ConciergeCurrentLocationFragmentViewModelListener {
        void onLocationSelected(ConciergeUserAddress address);

        void onLoadSavedAddress(List<ConciergeUserAddress> adaptive);

        void onLoadSearchAddressResult(List<SearchAddressResult> adaptive);

        void onNewAddressAdded(ConciergeUserAddress newAddress);

        void onRequireLogin();
    }
}
