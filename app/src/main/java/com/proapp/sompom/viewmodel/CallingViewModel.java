package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.desmond.squarecamera.utils.FormatTime;
import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.call.AbsCallClient;
import com.proapp.sompom.chat.call.CallClientService;
import com.proapp.sompom.chat.call.CallDurationHelper;
import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.chat.call.CallNotificationHelper;
import com.proapp.sompom.chat.call.CallServiceConnectionProvider;
import com.proapp.sompom.chat.call.CallTimerHelper;
import com.proapp.sompom.chat.call.CallingService;
import com.proapp.sompom.chat.call.IncomingCallDataHolder;
import com.proapp.sompom.chat.call.agora.VOIPCallMessageListener;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.helper.AudioPlayer;
import com.proapp.sompom.helper.BluetoothSCOHelper;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.call.CallData;
import com.proapp.sompom.model.emun.CallAudioOutputType;
import com.proapp.sompom.model.emun.GeneralCallBehaviorType;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.JoinOrLeaveGroupCallResponse;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.SocketError;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.fragment.CallAudioOutputOptionDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.CallDataManager;
import com.proapp.sompom.utils.NetworkStateUtil;
import com.proapp.sompom.utils.NotificationUtils;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.ToastUtil;
import com.proapp.sompom.viewmodel.newviewmodel.ItemCallViewModel;
import com.proapp.sompom.widget.call.CallViewContainer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

public class CallingViewModel extends AbsBaseViewModel implements CallingService.CallingListener,
        VOIPCallMessageListener, CallViewContainer.CallViewContainerListener {

    public final ObservableInt mIsAnswerVisible = new ObservableInt();
    private final ObservableInt mCallStatusVisibility = new ObservableInt(View.INVISIBLE);
    public final ObservableField<User> mUser = new ObservableField<>();
    public final ObservableField<Product> mProduct = new ObservableField<>();

    private final AudioPlayer mAudioPlayer;
    private int mCurrVideoRecTimer = 0;
    private CallingService.CallServiceBinder mBinder;
    private CallingService.CallingListener mListener;
    private Context mContext;
    private boolean mIsOutGoingCalling;
    private boolean mIsBackgroundCall;
    private boolean mIsHangUpFired;
    private CallDurationHelper mCallDurationHelper;

    private ObservableInt mCallViewComponentVisibility = new ObservableInt(View.VISIBLE);
    private ObservableBoolean mFloatingWindowIconVisibility = new ObservableBoolean();
    private ObservableField<String> mCallStatus = new ObservableField<>();
    private ObservableInt mSwapCameraVisibility = new ObservableInt();
    private ObservableInt mAudioOutputVisibility = new ObservableInt(View.GONE);
    private ObservableInt mCallBehavior = new ObservableInt(GeneralCallBehaviorType.UNKNOWN.ordinal());
    private ObservableBoolean mIsVideoOff = new ObservableBoolean();
    private ObservableBoolean mIsMicOff = new ObservableBoolean();
    private ObservableInt mCallProfileVisibility = new ObservableInt(View.VISIBLE);
    private ObservableField<CallAudioOutputType> mCallAudioOutputType = new ObservableField<>(CallAudioOutputType.PHONE);
    private AbsCallService.CallType mCallType;
    private boolean mIsCallInProgress;
    private boolean mCallOptionButtonHidden;

    //Profile picture and name
    private ObservableField<String> mName = new ObservableField<>();
    private ObservableField<String> mProfilePhoto = new ObservableField<>();
    private ObservableField<List<User>> mParticipants = new ObservableField<>();
    private boolean mShouldDisplayCallDuration;
    private boolean mIsAlreadyReceivedVideoRequest;
    private User mVideoCameraRequester;
    private CallingViewModelListener mViewModelListener;
    private ObservableBoolean mShouldHideVideoOption = new ObservableBoolean();
    private UserGroup mCurrentGroup;
    private User mCurrentRecipient;
    private CallDataManager mDataManager;
    private ChatIntent mChatIntent;
    private List<String> mAcceptedOrRejectedVideoShareUser = new ArrayList<>();
    private boolean mIsReceivedOrRequestShareVideoRequest;
    private boolean mIsRecipientOfOneToOneAcceptedShareVideo;
    private boolean mShouldAddOtherParticipantVideo;
    private boolean mIsCurrentUserFirstGroupVideoRequester;
    private AbsCallService.CallActionType mCallActionType;
    private boolean mIsOneToOneRecipientAcceptedCall;
    private boolean mIsAlreadyGotCallBusyStatus;
    private boolean mIsBlueToothHeadsetConnected;
    private BluetoothSCOHelper mBluetoothSCOHelper;
    private boolean mCallOffSoundDispatched;

    public CallingViewModel(Context context,
                            CallDataManager callDataManager,
                            AbsCallService.CallType callType,
                            AbsCallService.CallActionType callActionType,
                            IncomingCallDataHolder callVo,
                            User recipient,
                            UserGroup group,
                            boolean isIncomingCallFromNotification,
                            @Nullable Product product,
                            CallingService.CallServiceBinder binder,
                            CallingService.CallingListener listener,
                            CallingViewModelListener viewModelListener) {
        Timber.i("callActionType: " + callActionType + ", isIncomingCallFromNotification: " + isIncomingCallFromNotification);
        mCallActionType = callActionType;
        mShouldHideVideoOption.set(true);
        mCallType = callType;
        mDataManager = callDataManager;
        initCallTimer();
        mAudioPlayer = new AudioPlayer(context);
        mContext = context;
        this.mBinder = binder;
        mListener = listener;
        mViewModelListener = viewModelListener;
        bindCallOptionVisibility();
        mCallStatus.set(context.getString(R.string.call_screen_call_label));
        binder.addCallListener(this);
        binder.getChatBinder().addCallVOIPMessageListeners(this);
        initCallViewListener();
        getCallService().refreshSomeCallServiceData();
        mBluetoothSCOHelper = new BluetoothSCOHelper(mContext);
        mBluetoothSCOHelper.init();
        mBluetoothSCOHelper.startSco();

        //Update incoming call type to Calling service.
        if (mBinder != null && mBinder.getVoiceCallClientService() != null) {
            mBinder.getVoiceCallClientService().onCallTypeUpdated();
        }

        if (callActionType == AbsCallService.CallActionType.CALL) {
            CallHelper.clearRejectedCallList();
            if (mBinder != null) {
                mBinder.set1x1Participant(recipient);
            }
            mCallBehavior.set(GeneralCallBehaviorType.MAKING_CALL.ordinal());
            mIsOutGoingCalling = true;
            mIsAnswerVisible.set(View.GONE);
            mCallStatusVisibility.set(View.VISIBLE);
            binder.setRecipient(recipient);
            Map<String, Object> exchangeCallData = CallHelper.getExchangeCallData(mContext,
                    SharedPrefUtils.getUserId(context),
                    recipient.getId(),
                    group);
            binder.call(callType,
                    exchangeCallData,
                    recipient);
            mUser.set(recipient);
            mProduct.set(product);
            bindCallerInfo(CallHelper.getGroup(exchangeCallData), binder.getRecipient());
            initChatIntent(group, recipient);
            if (isGroupCall()) {
                new Handler().postDelayed(() -> viewModelListener.shouldDispatchStartGroupCall(getCallType()), 100);
            }
            getCallService().dispatchGroupCallNotification(mContext,
                    AbsCallService.CallActionType.CALL,
                    mBinder.get1x1Participant());
//            CallHelper.resetDeviceAudioStateAfterCallEnded(mContext);
            getCallService().setEnableLoudSpeaker(isVideoCall());
            new Handler().post(mAudioPlayer::playContactingTone);

            if (mViewModelListener != null) {
                mViewModelListener.onCallChannelIdIdGenerated(getCallService().getChannelId());
            }
        } else if (callActionType == AbsCallService.CallActionType.INCOMING) {
            Map<String, Object> exchangeCallData = callVo.getCallData();
            updateIncomingCallStatus(exchangeCallData, mCallType);
            mCallBehavior.set(GeneralCallBehaviorType.RECEIVING_CALL.ordinal());
            //Current user is recipient
            binder.setRecipient(SharedPrefUtils.getCurrentUser(mContext));
            if (callVo.isBackground()) {
                mIsBackgroundCall = true;
            }
            mIsOutGoingCalling = false;
            mUser.set(CallHelper.getCaller(exchangeCallData));
            mBinder.set1x1Participant(mUser.get());
            binder.getVoiceCallClientService().setIncomingCallData(callType, exchangeCallData);
            binder.getVoiceCallClientService().sendBackToCallerThatTheIncomingCallShowing();
            bindCallerInfo(CallHelper.getGroup(exchangeCallData), mUser.get());
            initChatIntent(group, mUser.get());
            if (!isIncomingCallFromNotification) {
                getCallService().dispatchGroupCallNotification(mContext,
                        AbsCallService.CallActionType.INCOMING,
                        mBinder.get1x1Participant());
//                CallHelper.resetDeviceAudioStateAfterCallEnded(mContext);
//                getCallService().setEnableLoudSpeaker(isVideoCall());
                CallHelper.setIncomingCallRingMode(mContext, isVideoCall());
                mAudioPlayer.playIncomingCallTone(true);
            }
            if (mViewModelListener != null) {
                mViewModelListener.onCallChannelIdIdGenerated(getCallService().getChannelId());
            }
        } else if (callActionType == AbsCallService.CallActionType.JOINED) {
            CallHelper.clearRejectedCallList();
            Timber.i("Join call.");
            initChatIntent(group, null);
            //For the exchange data builder of group, there is no participant attached, so we have to
            //load it from local.
            Conversation conversationById = ConversationDb.getConversationById(mContext, group.getId());
            Timber.i("conversationById: " + new Gson().toJson(conversationById));
            if (conversationById != null) {
                group.setParticipants(conversationById.getParticipants());
            }
            bindCallerInfo(group, null);
            binder.setRecipient(SharedPrefUtils.getUser(mContext));
            mIsOutGoingCalling = false;
            Map<String, Object> callData = CallHelper.generateGroupJoinCallData(mContext, group);
            mCallStatus.set(mContext.getString(R.string.call_screen_waiting_label));
            getHandler().postDelayed(() -> {
                getCallService().joinGroupCall(callType,
                        callData);
                mCallBehavior.set(GeneralCallBehaviorType.CALL_IN_PROGRESS.ordinal());
                if (mViewModelListener != null) {
                    mViewModelListener.onCallChannelIdIdGenerated(getCallService().getChannelId());
                }
            }, 100);
        }
        checkIfTheCallScreenShouldBeDismissed();
    }

    public ObservableInt getCallViewComponentVisibility() {
        return mCallViewComponentVisibility;
    }

    public ObservableBoolean getFloatingWindowIconVisibility() {
        return mFloatingWindowIconVisibility;
    }

    public Handler getHandler() {
        return new Handler(Looper.getMainLooper());
    }

    public ObservableField<CallAudioOutputType> getCallAudioOutputType() {
        return mCallAudioOutputType;
    }

    public boolean isShowingIncomingCall() {
        return mCallActionType == AbsCallService.CallActionType.INCOMING;
    }

    public void checkToStopIncomingToneWhenDeviceButtonPressed() {
        mAudioPlayer.checkToStopIncomingToneWhenDeviceButtonPressed();
    }

    public void playCallOnOrOffTone(boolean isCallOff) {
        if (mAudioPlayer != null) {
            if (isCallOff && !mCallOffSoundDispatched) {
                mCallOffSoundDispatched = true;
            }
            mAudioPlayer.playCallOnOrOffTone(isCallOff);
        }
    }

    public boolean isAlreadyGotCallBusyStatus() {
        return mIsAlreadyGotCallBusyStatus;
    }

    private void checkIfTheCallScreenShouldBeDismissed() {
        /*
            To prevent the issue as following:
            1. Current user receive incoming call and immediately the caller cancel the call fast,
            on current user does not receive any call reject callback since the it happen too fast.
            So we have to add a state to check of whether to dismiss the screen manually if the current
            user previously received the call rejected within the same conversation id.
            NOTE: Must be called in the start up of the call screen.
         */
        new Handler().postDelayed(() -> {
            if (getCallService() != null) {
                Map<String, Object> currentCallData = getCallService().getCurrentCallData();
                if (currentCallData != null) {
                    String channelId = getCallService().getChannelId();
                    //TODO: Need to check
//                    if (CallHelper.isStillInRejectedCallList(channelId) && !mActivity.isFinishing()) {
                    if (CallHelper.isStillInRejectedCallList(channelId)) {
                        Timber.i("Dismissed the call screen manually for there was a previous rejected this call status.");
                        handleOnCallRejected(currentCallData);
                    }
                }
            }
        }, 500);
    }

    public ItemCallViewModel.ItemCallViewModelCommunicator getItemCallViewModelCommunicator() {
        return () -> mViewModelListener.getCallViewContainer().getApplicationContext();
    }

    private void initChatIntent(UserGroup group, User recipient) {
        if (group != null) {
            mChatIntent = new ChatIntent(mContext,
                    ConversationDb.getConversationById(mContext, group.getId()));
        } else {
            mChatIntent = new ChatIntent(mContext, recipient);
        }
    }

    public void onFloatingWindowEnable(boolean isEnable) {
        mCallViewComponentVisibility.set(isEnable ? View.INVISIBLE : View.VISIBLE);
        mCallViewComponentVisibility.notifyChange();

        boolean shouldShowFloatingWindowIcon = getCallType() == AbsCallService.CallType.VOICE ||
                getCallType() == AbsCallService.CallType.GROUP_VOICE;

        mFloatingWindowIconVisibility.set(shouldShowFloatingWindowIcon && isEnable);
    }

    private void initCallViewListener() {
        //Init common listener
        mBinder.getVoiceCallClientService().setCallViewCommunicationListener(new AbsCallClient
                .CallViewCommunicationListener() {
            @Override
            public boolean isCurrentCallViewHasVideo() {
                return mViewModelListener.getCallViewContainer().isCurrentCallViewHasVideo();
            }

            @Override
            public boolean isShowingRequestCameraScreen() {
                return mViewModelListener.getCallViewContainer().isDisplayingRequestingVideo();
            }

            @Override
            public String getVideoRequesterId() {
                if (mVideoCameraRequester != null) {
                    return mVideoCameraRequester.getId();
                }

                return null;
            }

            @Override
            public CallData.CallAction getCurrentUserCallAction() {
                return mViewModelListener.getCallViewContainer().getCurrentUserCallAction();
            }

            @Override
            public boolean shouldAddOtherParticipantVideo() {
                return mShouldAddOtherParticipantVideo;
            }

            @Override
            public boolean isOneToOneRecipientAcceptedCall() {
                return mIsOneToOneRecipientAcceptedCall;
            }

            @Override
            public boolean isMicOff() {
                return mIsMicOff.get();
            }

            @Override
            public void onShouldHideAudioOption() {
                mAudioOutputVisibility.set(View.GONE);
            }

            @Override
            public boolean isParticipantCallViewAdded(String userId) {
                return mViewModelListener.getCallViewContainer().isParticipantCallViewAdded(userId);
            }
        });
    }

    private void notifyJoinOrLeaveGroupCall(String groupId,
                                            boolean isVideo,
                                            boolean isJoin,
                                            boolean isUpdateToVideoType,
                                            int callDuration) {
        Observable<Response<JoinOrLeaveGroupCallResponse>> loginService = mDataManager.getNotifyJoinOrLeaveGroupCall(groupId,
                isVideo,
                isJoin,
                isUpdateToVideoType,
                callDuration,
                isCurrentUserCaller());
        ResponseObserverHelper<Response<JoinOrLeaveGroupCallResponse>> helper = new ResponseObserverHelper<>(mDataManager.mContext,
                loginService);
//        Timber.i("groupId: " + groupId + ", isJoin: " + isJoin + "is Group: " + isGroupCall());
        helper.execute(new OnCallbackListener<Response<JoinOrLeaveGroupCallResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
            }

            @Override
            public void onComplete(Response<JoinOrLeaveGroupCallResponse> result) {
//                Timber.i("notifyJoinOrLeaveGroupCall onComplete: isJoin: " + isJoin + ",data: " + new Gson().toJson(result.body()));
                /*
                Now will check the join status only for leaving group action. For the join action, it
                is being handled directly inside call service.
                 */
                if (isGroupCall() && !isJoin && result.body() != null && result.body().isCallStillActive()) {
                    ChatHelper.broadcastGroupJoinStatusOnGroupCallEngaged("notifyJoinOrLeaveGroupCall", mContext, groupId, isVideo);
                }
            }
        });
    }

    public ObservableBoolean getShouldHideVideoOption() {
        return mShouldHideVideoOption;
    }

    private void updateIncomingCallStatus(Map<String, Object> callData, AbsCallService.CallType callType) {
        if (callData != null) {
            UserGroup group = CallHelper.getGroup(callData);
            User caller = CallHelper.getCaller(callData);
            if (caller != null) {
                if (group != null) {
                    //Incoming group call
                    if (CallHelper.isVideoCall(callType)) {
                        mCallStatus.set(mContext.getString(R.string.call_screen_group_incoming_video_call_label,
                                caller.getFirstName()));
                    } else {
                        mCallStatus.set(mContext.getString(R.string.call_screen_group_incoming_call_label,
                                caller.getFirstName()));
                    }
                } else {
                    //Incoming call
                    if (CallHelper.isVideoCall(callType)) {
                        mCallStatus.set(mContext.getString(R.string.call_screen_incoming_video_call_label));
                    } else {
                        mCallStatus.set(mContext.getString(R.string.call_screen_incoming_call_label));
                    }
                }
            }
        }
    }

    private void checkToUpdateToVideoCallTypeIfNecessary() {
        if (isGroupCall() && !CallHelper.isVideoCall(getCallService().getCallType())) {
            Timber.i("Update to group video call type.");
            getCallService().updateToVideoCallTye();
            mCallType = getCallService().getCallType();
        }
    }

    private void bindCallOptionVisibility() {
        if (CallHelper.isVideoCall(mCallType)) {
            mSwapCameraVisibility.set(View.VISIBLE);
            mIsVideoOff.set(false);
            mAudioOutputVisibility.set(View.GONE);
            mCallAudioOutputType.set(CallAudioOutputType.SPEAKER);
        } else {
            mSwapCameraVisibility.set(View.GONE);
            mIsVideoOff.set(true);
            mAudioOutputVisibility.set(View.VISIBLE);
            mCallAudioOutputType.set(CallAudioOutputType.PHONE);
        }
        mIsBlueToothHeadsetConnected = CallHelper.isBluetoothHeadsetDeviceConnected();
        if (mIsBlueToothHeadsetConnected) {
            mCallAudioOutputType.set(CallAudioOutputType.BLUETOOTH);
        }
    }

    private void bindCallerInfo(UserGroup group, User recipient) {
        Timber.i("bindCallerInfo: " + new Gson().toJson(group));
        mCurrentGroup = group;
        mCurrentRecipient = recipient;
        if (group != null) {
            //Group call
            mName.set(group.getName());
            mParticipants.set(group.getParticipants());
            mProfilePhoto.set(group.getProfilePhoto());
        } else if (recipient != null) {
            //Individual call
            mName.set(recipient.getFullName());
            mProfilePhoto.set(recipient.getUserProfileThumbnail());
            mParticipants.set(Collections.singletonList(recipient));
        }
    }

    public int getCurrVideoRecTimer() {
        return mCurrVideoRecTimer;
    }


    private void initCallTimer() {
        mCallDurationHelper = new CallDurationHelper(milliSecondCounter -> {
//            Timber.i("mShouldDisplayCallDuration: " + mShouldDisplayCallDuration + ", mCurrVideoRecTimer: " + mCurrVideoRecTimer);
            mCurrVideoRecTimer = milliSecondCounter;
            if (mShouldDisplayCallDuration) {
                mCallStatus.set(FormatTime.getFormattedDuration(milliSecondCounter));
            }
        });
    }

    public ObservableInt getCallStatusVisibility() {
        return mCallStatusVisibility;
    }

    public ObservableField<String> getCallStatus() {
        return mCallStatus;
    }

    public void setCallStatus(String status) {
        mCallStatus.set(status);
    }

    @Override
    public void onCallEnd(AbsCallService.CallType callType,
                          String channelId,
                          boolean isGroup,
                          User recipient,
                          boolean isMissedCall,
                          int calledDuration,
                          boolean isEndedByClickAction,
                          boolean isCauseByTimeOut,
                          boolean isCausedByParticipantOffline) {
        if (mIsReceivedOrRequestShareVideoRequest
                && isGroup
                && !isVideoCall()) {
            Timber.i("Update to video call mode for group on call ended.");
            getCallService().updateToVideoCallTye();
            callType = getCallService().getCallType();
            mCallType = callType;
        }

        boolean currentUserJoinedChannel = getCallService().isCurrentUserJoinedChannel();
        Timber.i("onCallEnd: callType: " + callType +
                ", channelId: " + channelId +
                ", isGroup: " + isGroup +
                ", currentUserJoinedChannel: " + currentUserJoinedChannel +
                ", isEndedByClickAction: " + isEndedByClickAction +
                ", isMissedCall: " + isMissedCall +
                ", isCauseByTimeOut: " + isCauseByTimeOut +
                ", isCausedByParticipantOffline: " + isCausedByParticipantOffline +
                ", isCurrentUserCaller: " + isCurrentUserCaller());
        mCallStatusVisibility.set(View.VISIBLE);
        mCallStatus.set(mContext.getString(R.string.call_screen_call_end_label));

        if (mIsOutGoingCalling) {
            mAudioPlayer.stopRingingTone();
        } else {
            Timber.i("stopRingtone");
            mAudioPlayer.stopContactingOrIncomingTone();
        }

        /*
        Will also end call screen if the following condition is true:
        1. Is miss called and current user is caller or
        2. There is no internet and user is not yet join call channel
         */
        if ((isMissedCall && !isCurrentUserCaller()) ||
                (!currentUserJoinedChannel && !NetworkStateUtil.isNetworkAvailable(mContext))) {
            //By call this method, it will cause calling screen to dismiss.
            mListener.onCallEnd(callType,
                    channelId,
                    isGroup,
                    recipient,
                    isMissedCall,
                    calledDuration,
                    isEndedByClickAction,
                    isCauseByTimeOut,
                    isCausedByParticipantOffline);
        }

        //Need to close the call screen too with following conditions:
        if ((isCauseByTimeOut && isMissedCall && isCurrentUserCaller()) ||
                (isCausedByParticipantOffline && !isGroupCall())) {
            performHangUp(false);
        }
    }

    @Override
    public void onIncomingCall(AbsCallService.CallType calType, Map<String, Object> data1, Map<String, Object> data2) {
        mListener.onIncomingCall(calType, data1, data2);
    }

    @Override
    public void onCallEstablished(AbsCallService.CallType callType,
                                  String channelId,
                                  boolean isGroup) {
        Timber.i("onCallEstablished: callType: " + callType + ", channelId: " + channelId + ", isGroup: " + isGroup);
        getCallService().setEnableLoudSpeaker(isVideoCall());
        mShouldHideVideoOption.set(false);
        getCallService().dispatchGroupCallNotification(mContext,
                AbsCallService.CallActionType.SPEAKING,
                mBinder.get1x1Participant());
        mCallBehavior.set(GeneralCallBehaviorType.CALL_IN_PROGRESS.ordinal());
        updateDisplayCallDurationStatus();
        mIsCallInProgress = true;
        if (!mCallDurationHelper.isInProgress()) {
            mCallDurationHelper.startTimer();
        }
        checkCallProfileVisibility("onCallEstablished");
        mListener.onCallEstablished(callType, channelId, isGroup);
        if (mIsOutGoingCalling) {
            mAudioPlayer.stopRingingTone();
            mAudioPlayer.checkToStopContactingOrIncomingTone();
        }

        if (mViewModelListener.getCallViewContainer().isCurrentUserRequestingCamera())
            getHandler().post(() -> {
                statRequestingCameraState(true);
            });
        if (isCurrentUserCaller()) {
            playCallOnOrOffTone(false);
        }
    }

    @Override
    public void onCurrentUserJoinOrLeaveGroupCall(AbsCallService.CallType callType,
                                                  String channelId,
                                                  boolean isLeft,
                                                  int callDuration) {
        Timber.i("onCurrentUserJoinOrLeaveGroupCall: callType: " + callType +
                ", channelId: " + channelId +
                ", isLeft: " + isLeft +
                ", Join: " + getCallService().getJoinCallUserId().size() +
                ", Call duration: " + callDuration);
        notifyJoinOrLeaveGroupCall(channelId, CallHelper.isVideoCall(callType), !isLeft, false, callDuration);
        if (isLeft) {
            mListener.onCallEnd(callType,
                    channelId,
                    isGroupCall(),
                    mBinder.getRecipient(),
                    isMissedCall(),
                    callDuration,
                    false,
                    false,
                    false);
        }
    }

    private void checkCallProfileVisibility(String tag) {
        checkToShowCallProfile(tag, mViewModelListener.getCallViewContainer().shouldDisplayMainCallProfile());
    }

    @Override
    public void onCallInfoUpdated(AbsCallService.CallType callType,
                                  String info,
                                  boolean isCallProfileVisible,
                                  boolean showCallControls) {
        Timber.i("onCallInfoUpdated: Info: " + info
                + ", isCallProfileVisible: " + isCallProfileVisible
                + ", showCallControls: " + showCallControls);
        updateDisplayCallDurationStatus();
        if (!TextUtils.isEmpty(info)) {
            mCallStatus.set(info);
        }
        if (showCallControls) {
            //Show all call controls back
            setCallControlVisibility(false);
        }
    }

    private void updateDisplayCallDurationStatus() {
        Timber.i("updateDisplayCallDurationStatus");
        if (isInRequestingVideoMode()) {
            mShouldDisplayCallDuration = false;
        } else {
            if (!isGroupCall() && !isVideoCall()) {
                mShouldDisplayCallDuration = true;
            } else {
                mShouldDisplayCallDuration = mViewModelListener.getCallViewContainer().shouldDisplayCallDuration();
            }
        }
    }

    private boolean isInRequestingVideoMode() {
        return (mViewModelListener.getCallViewContainer().isCurrentUserRequestingCamera() ||
                mViewModelListener.getCallViewContainer().isCurrentUserWaitingOtherToJoinGroupCall() ||
                GeneralCallBehaviorType.getFromValue(mCallBehavior.get()) ==
                        GeneralCallBehaviorType.SHOWING_REQUEST_CAMERA);
    }

    private void setCallControlVisibility(boolean shouldHide) {
        getHandler().post(() -> {
            mCallOptionButtonHidden = shouldHide;
            if (mViewModelListener.getTopCallOptionContainerView() != null) {
                mViewModelListener.getTopCallOptionContainerView().setVisibility(!shouldHide ? View.VISIBLE : View.GONE);
                mViewModelListener.getBottomCallOptionContainerView().setVisibility(!shouldHide ? View.VISIBLE : View.GONE);
            }
            mViewModelListener.getCallViewContainer().resetSingleCallViewMarginTop(shouldHide);
        });
    }

    @Override
    public void onCheckShowingRequestVideoCamera(boolean shouldShow,
                                                 AbsCallService.CallType callType,
                                                 User requester,
                                                 boolean isGroupCall) {
        Timber.i("onCheckShowingRequestVideoCamera: shouldShow: " + shouldShow
                + ", callType: " + callType
                + ", requester: " + (requester != null ? requester.getFullName() : null)
                + ", isGroupCall: " + isGroupCall + "," +
                "mIsAlreadyReceivedVideoRequest: " + mIsAlreadyReceivedVideoRequest);
        if (requester != null) {
            if (shouldShow && !mIsAlreadyReceivedVideoRequest) {
                mVideoCameraRequester = requester;
                mShouldDisplayCallDuration = false;
                mCallBehavior.set(GeneralCallBehaviorType.SHOWING_REQUEST_CAMERA.ordinal());
                mIsAlreadyReceivedVideoRequest = true;
                checkToShowCallProfile("onCheckShowingRequestVideoCamera received video request.", true);
                bindVideoRequesterProfile(requester);
                getCallService().startRequestVideoTimer(false);
                Timber.i("mCallViewComponentVisibility: " + mCallViewComponentVisibility.get());
            } else if (isVideoRequesterTheSameBefore(requester)) {
                mViewModelListener.getCallViewContainer().resetDisplayingRequestingVideo();
                bindCallerInfo(mCurrentGroup, mCurrentRecipient);
                mCallBehavior.set(GeneralCallBehaviorType.CALL_IN_PROGRESS.ordinal());
                updateDisplayCallDurationStatus();
                mCallStatus.set("");
                checkCallProfileVisibility("onCheckShowingRequestVideoCamera received video request same before.");
                if (!isGroupCall && mVideoCameraRequester != null && !isVideoCall()) {
                    mViewModelListener.getCallViewContainer().disableVideoViewForSpecificUser(mVideoCameraRequester.getId());
                }
                mViewModelListener.getCallViewContainer().resetReceivingCameraRequestStatusForGroup();
                mIsAlreadyReceivedVideoRequest = false;
                mVideoCameraRequester = null;
            }
        }
    }

    private boolean isVideoRequesterTheSameBefore(User requester) {
        return requester != null &&
                mVideoCameraRequester != null &&
                TextUtils.equals(requester.getId(), mVideoCameraRequester.getId());
    }

    private void bindVideoRequesterProfile(User user) {
        Timber.i("Bind video requester profile.");
        mName.set(user.getFullName());
        mParticipants.set(Collections.singletonList(user));
        mProfilePhoto.set(user.getUserProfileThumbnail());
        mCallStatus.set(mContext.getString(R.string.call_screen_receive_request_share_video_label));
    }

    @Override
    public void onCallTimerUp(AbsCallService.CallType callType,
                              CallTimerHelper.TimerType timerType,
                              CallTimerHelper.StarterTimerType starterTimerType) {
        Timber.i("onCallTimerUp: callType: " + callType +
                ", timerType: " + timerType +
                ", StarterTimerType: " + starterTimerType);
        getHandler().post(() -> {
            if (timerType == CallTimerHelper.TimerType.REQUEST_CAMERA) {
                if (starterTimerType == CallTimerHelper.StarterTimerType.RECIPIENT) {
                    //Current user is camera call recipient.
                    if (GeneralCallBehaviorType.getFromValue(mCallBehavior.get()) ==
                            GeneralCallBehaviorType.SHOWING_REQUEST_CAMERA) {
                        dismissShowRequestCameraScreen();
                    }
                } else if (!isGroupCall() && starterTimerType == CallTimerHelper.StarterTimerType.CALLER) {
                    onVideoToggleClick(null);
                }
            }
        });
    }

    private void dismissShowRequestCameraScreen() {
        Timber.i("dismissShowRequestCameraScreen");
        mViewModelListener.getCallViewContainer().resetDisplayingRequestingVideo();
        getCallService().cancelTimer();

//        if (isGroupCall() &&
//                mVideoCameraRequester != null &&
//                !isVideoCall()) {
//            mViewModelListener.getCallViewContainer().onRejectRequestingCamera(mVideoCameraRequester.getId());
//        }
        if (!isGroupCall()) {
            mViewModelListener.getCallViewContainer().disableVideoViewForSpecificUser(getCallService().get1x1CallParticipantId());
            if (mVideoCameraRequester != null && !isVideoCall()) {
                mViewModelListener.getCallViewContainer().onRejectRequestingCamera(mVideoCameraRequester.getId());
            }
        }
        mIsAlreadyReceivedVideoRequest = false;
        mCallBehavior.set(GeneralCallBehaviorType.CALL_IN_PROGRESS.ordinal());
        updateDisplayCallDurationStatus();
        mViewModelListener.getCallViewContainer().resetReceivingCameraRequestStatusForGroup();
    }

    public void onAnswerClick(View view) {
        Timber.i("mCallBehavior.get(): " + mCallBehavior.get());
        AnimationHelper.animateBounce(view, () -> {
            if (mBinder == null) {
                return;
            }
            if (GeneralCallBehaviorType.getFromValue(mCallBehavior.get()) == GeneralCallBehaviorType.SHOWING_REQUEST_CAMERA) {
                getCallService().getCallMessageHelper().sendAcceptedRequestVideo(getCallService().getCurrentCallData(),
                        getCallService().get1x1CallParticipantId());
                if (!isGroupCall()) {
                    mIsRecipientOfOneToOneAcceptedShareVideo = true;
                }
                getCallService().cancelTimer();
                mIsVideoOff.set(false);
                mSwapCameraVisibility.set(View.VISIBLE);
                checkToShowCallProfile("onAnswerClick", false);
                mCallBehavior.set(GeneralCallBehaviorType.CALL_IN_PROGRESS.ordinal());
                //Update call type to video must be called before other update call action.
                mCallType = getCallService().updateToVideoCallTye();
                getCallService().enableLocalVideo();
                getCallService().enableVideoCallViewForCurrentUserIfNecessary();
                mViewModelListener.getCallViewContainer().resetReceivingCameraRequestStatusForGroup();
            } else {
                mCallBehavior.set(GeneralCallBehaviorType.CALL_IN_PROGRESS.ordinal());
                mBinder.answer();
                mAudioPlayer.stopContactingOrIncomingTone();
            }
        });
    }

    public CallClientService getCallService() {
        return mBinder.getVoiceCallClientService();
    }

    public void onHangupClick(View view) {
        Timber.i("onHangupClick: mCallBehavior.get()" + mCallBehavior.get());
        AnimationHelper.animateBounce(view, () -> {
            if (GeneralCallBehaviorType.getFromValue(mCallBehavior.get()) == GeneralCallBehaviorType.SHOWING_REQUEST_CAMERA) {
                if (mVideoCameraRequester != null) {
                    getCallService().getCallMessageHelper().sendRejectRequestVideo(getCallService().getCurrentCallData(),
                            getCallService().get1x1CallParticipantId());
                }
                dismissShowRequestCameraScreen();
            } else {
                performHangUp(true);
            }
        });
    }

    public void performHangUp(boolean isEndedByClickAction) {
        mAudioPlayer.stopContactingOrIncomingTone();
        mIsHangUpFired = true;
        if (mBinder != null) {
            mBinder.hangup(isEndedByClickAction);
        }
        invokeCallEndedCallback(isEndedByClickAction);
    }

    private void invokeCallEndedCallback(boolean isEndedByClickAction) {
        onCallEnd(getCallType(),
                getCallService().getChannelId(),
                isGroupCall(),
                mBinder.get1x1Participant(),
                getCallService().isMissedCall(),
                0,
                isEndedByClickAction,
                false
                , false);
    }

    @Override
    public void onDestroy() {
        Timber.i("onDestroy: isCurrentUserJoinedChannel: " + getCallService().isCurrentUserJoinedChannel());
        mBluetoothSCOHelper.unregister();
        CallNotificationHelper.cancelCallInfoNotification(mContext);
        CallHelper.clearRejectedCallList();
        NotificationUtils.cancelNotificationByLocalId(mContext, getCallService().getChannelId());
        CallServiceConnectionProvider.getInstance().unbindAllServiceConnection();
        mAudioPlayer.stopAllTones();
        if (mBinder != null) {
            //Consider user close screen as hang up the call.
            if (!mIsHangUpFired) {
                onHangupClick(null);
            }
            mBinder.removeCallListener(this);
            mBinder.getChatBinder().removeCallVOIPMessageListeners(this);
            if (mIsBackgroundCall) {
                mBinder.stop();
            }
        }
        mCallDurationHelper.cancelTimer();
        super.onDestroy();
    }

    public void onMessageClick(View view) {
        AnimationHelper.animateBounce(view, () -> {
            if (mChatIntent != null) {
                if (mViewModelListener != null) {
                    mViewModelListener.onMessageClicked(mChatIntent);
                }
            }
        });
    }

    private boolean isMissedCall() {
        //Mean that call is not established.
        return getCallService().isMissedCall();
    }

    @Override
    public void onCallError(SocketError error) {
        mCallStatus.set(mContext.getString(R.string.call_screen_out_of_work_hour_label));
        getHandler().postDelayed(() -> {
            performHangUp(false);
        }, 2000);
    }

    @Override
    public void onReceiveVOIPCallMessage(Chat message) {
        Timber.i("onReceiveVOIPCallMessage: " + new Gson().toJson(message));
        if (mBinder.getVoiceCallClientService().isVOIPCallMessageFromCurrentCall(message)) {
            Timber.i("onReceiveVOIPCallMessage of current call.");
            if (CallHelper.isRingingVOIPCallMessageType(message) &&
                    CallHelper.isCurrentUserCaller(mContext, message.getCallData())) {
                //Update ringing status only from participant that has no send busy to current user before.
                if (!TextUtils.equals(getCallService().getGotUserBusyResponseFromOneToOneUser(),
                        message.getSenderId()) &&
                        mIsOutGoingCalling && isMissedCall()) {
                    mAudioPlayer.playRingingTone();
                    mCallStatusVisibility.set(View.VISIBLE);
                    mCallStatus.set(mContext.getString(R.string.call_screen_ring_label));
                    getCallService().dispatchGroupCallNotification(mContext,
                            AbsCallService.CallActionType.RINGING,
                            mBinder.get1x1Participant());
                }
            } else if (CallHelper.isRejectVOIPCallMessageType(message)) {
                checkToDisplayGroupCallMemberRejection(message);
                handleOnCallRejected(message.getCallData());
            } else if (CallHelper.isBusyVOIPCallMessageType(message)) {
                mIsAlreadyGotCallBusyStatus = true;
                String senderId = CallHelper.getSenderId(message.getCallData());
                User busyUser = mBinder.getVoiceCallClientService().getUserFromCurrentCalLData(senderId);
                if (busyUser != null) {
                    if (isGroupCall()) {
                        if (isCurrentUserCaller()) {
                            getHandler().post(() -> ToastUtil.showToastForCallScreen(mContext,
                                    mContext.getString(R.string.call_toast_reject_call,
                                            busyUser.getFirstName()), false));
                        } else if (getCallService().isCallInProgress()) {
                            //For recipient of call, this message should be displayed when call engaged only
                            getHandler().post(() -> ToastUtil.showToastForCallScreen(mContext,
                                    mContext.getString(R.string.call_toast_reject_call,
                                            busyUser.getFirstName()), false));
                        }
                    } else if (getCallService().isMissedCall()) {
                        getCallService().setGotUserBusyResponseFromOneToOneUser(message.getSenderId());
                        mCallStatus.set(mContext.getString(R.string.call_screen_busy_label,
                                busyUser.getFirstName()));
                        getHandler().post(() -> {
                            if (!isGroupCall()) {
                                new Handler().postDelayed(() -> {
                                            performHangUp(true);
                                        },
                                        1500);
                            }
                        });
                    }
                } else {
                    Timber.e("User busy is required for display user busy status.");
                }
            } else if (CallHelper.isCallAcceptedVOIPCallMessageType(message) &&
                    mBinder.getVoiceCallClientService().isMissedCall()) {
                if (!getCallService().isGroupCall() &&
                        CallHelper.isCurrentUserCaller(mContext, message.getCallData()) &&
                        !TextUtils.equals(SharedPrefUtils.getUserId(mContext), message.getSenderId())) {
                    //One to one recipient has accepted call.
                    Timber.i("One to one recipient has accepted call.");
                    mIsOneToOneRecipientAcceptedCall = true;
                    getCallService().checkToEngageOneToOneCallAfterRecipientAcceptedCall(message.getSenderId(), mIsMicOff.get());
                } else if (!isCurrentUserCaller()) {
                    /*
                      As call recipient, you receive call accepted from within the same account of another
                      device which indicated the incoming call to current account has been responded on
                      another device and on the current device, we have to show proper message to user.
                     */
                    Timber.i("Receive call accepted from within current user account on another device.");
                    mCallStatus.set(mContext.getString(R.string.call_screen_other_device_answered_label));
                    getHandler().postDelayed(() -> performHangUp(false), 1500);
                }
            } else if (CallHelper.getChatCallType(message) == AbsCallService.CallType.REQUEST_VIDEO &&
                    !CallHelper.isVideoCall(getCallType())) {
                if (!getCallService().isMissedCall()) {
                    if ((mCallActionType == AbsCallService.CallActionType.JOINED && CallHelper.isVideoCall(getCallService().getCallType()))) {
                        Timber.i("Ignore request share video type for joined group video call.");
                        return;
                    }

                    User caller = getCallService().getUserFromCurrentCalLData(message.getSenderId());
                    if (caller != null) {
                        //Consider the video requester as the one who already accepted the video request.
                        mAcceptedOrRejectedVideoShareUser.add(caller.getId());
                        int remoteCallViewIdOfUser = getCallService().getCallDataHolder().getRemoteCallViewIdOfUser(caller.getId());
                        if (remoteCallViewIdOfUser != -1) {
                            mIsReceivedOrRequestShareVideoRequest = true;
                            getHandler().post(() -> {
                                getCallService().onReceivingCameraRequestStateChanged(remoteCallViewIdOfUser,
                                        true);
                                onCheckShowingRequestVideoCamera(true,
                                        getCallService().getCallType(),
                                        caller,
                                        getCallService().isGroupCall());
                                checkToUpdateToVideoCallTypeIfNecessary();
                            });
                        }
                    }
                }
            } else if (CallHelper.getChatCallType(message) == AbsCallService.CallType.ACCEPTED_VIDEO) {
                mAcceptedOrRejectedVideoShareUser.add(message.getSenderId());
                if (!getCallService().isMissedCall()) {
                    if (mViewModelListener.getCallViewContainer().isCurrentUserRequestingCamera()) {
                        mIsRecipientOfOneToOneAcceptedShareVideo = true;
                    }
                    getHandler().post(() -> getCallService().onOtherUserAcceptedShareVideo(mViewModelListener
                                    .getCallViewContainer()
                                    .isCurrentUserRequestingCamera(),
                            message.getSenderId()));
                }
            } else if (CallHelper.getChatCallType(message) == AbsCallService.CallType.REJECT_VIDEO) {
                Timber.i("Got reject request video message.");
                mAcceptedOrRejectedVideoShareUser.add(message.getSenderId());
                if (!getCallService().isMissedCall()) {
                    getCallService().cancelTimer();
                    if (!isGroupCall()) {
                        if (mViewModelListener.getCallViewContainer().isCurrentUserRequestingCamera()) {
                            Timber.i("Participant of one to one call has refused to share back video.");
                            getHandler().post(() -> onVideoToggleClick(null));
                        } else {
                            Timber.i("Requester of one to one call has canceled request share video.");
                            getHandler().post(this::dismissShowRequestCameraScreen);
                        }
                    } else {
                        if (!mViewModelListener.getCallViewContainer().isCurrentUserRequestingCamera()) {
                            if (mVideoCameraRequester != null && TextUtils.equals(mVideoCameraRequester.getId(), message.getSenderId())) {
                                Timber.i("Video requester has canceled the request.");
                                if (GeneralCallBehaviorType.getFromValue(mCallBehavior.get()) ==
                                        GeneralCallBehaviorType.SHOWING_REQUEST_CAMERA) {
                                    getHandler().post(this::dismissShowRequestCameraScreen);
                                }
                            }
//                        else {
//                            //Got reject from other participants.
//                        }
                        }
                    }
                }
            }
        }
    }

    private void handleOnCallRejected(Map<String, Object> callData) {
        if (callData != null) {
            if (isGroupCall()) {
                if (!getCallService().isCallInProgress()) {
                    if (CallHelper.isCurrentUserCaller(mContext, callData)) {
                        //Current user is caller and receiving call rejected from other participants
                        getCallService().addRejectedCallUser(CallHelper.getSenderId(callData));
                        if (getCallService().isAllParticipantRejectGroupCall()) {
                            performHangUp(false);
                        }
                    } else {
                        //Current user is recipient of group call
                        User caller = CallHelper.getCaller(callData);
                        if (caller != null && TextUtils.equals(caller.getId(), CallHelper.getSenderId(callData))) {
                            //Caller declined call.
                            performHangUp(false);
                        }
                    }
                }
            } else {
                performHangUp(false);
            }
        }
    }

    private void checkToDisplayGroupCallMemberRejection(Chat callMessage) {
        /*
        This message will handle both cases:
        1. If user decline to answer, they will send call reject
        2. Or their incoming call time-out and they will send call reject too.
         */
        if (getCallService().isGroupCall() &&
                callMessage.getCallData() != null &&
                !TextUtils.equals(callMessage.getSenderId(), SharedPrefUtils.getUserId(mContext))) {
            if ((isCurrentUserCaller() || getCallService().isCallInProgress()) &&
                    !getCallService().isUserInCallingOrUsedToBeInCall(callMessage.getSenderId())) {
                User user = getCallService().getUserFromCurrentCalLData(callMessage.getSenderId());
                if (user != null) {
                    getHandler().post(() -> ToastUtil.showToastForCallScreen(mContext,
                            mContext.getString(R.string.call_toast_no_answer,
                                    user.getFirstName()), false));
                }
            }
        }
    }

    private void checkToDisplayGroupCallMemberJoinOrLeaveCall(User user, boolean isLeft) {
        if (user != null && getCallService().isGroupCall()) {
            if (isCurrentUserCaller() || getCallService().isCallInProgress()) {
                if (isLeft) {
                    getHandler().post(() ->
                            ToastUtil.showToastForCallScreen(mContext, mContext.getString(R.string.call_toast_left_call,
                                    user.getFirstName()), false));
                } else {
                    getHandler().post(() ->
                            ToastUtil.showToastForCallScreen(mContext, mContext.getString(R.string.call_toast_join_call,
                                    user.getFirstName()), false));
                }
            }
        }
    }

    @Override
    public void addCallView(AbsCallService.CallType callType,
                            boolean isGroupCall,
                            List<CallData> callDataList) {
        if (mViewModelListener.getCallViewContainer().getListener() == null) {
            mViewModelListener.getCallViewContainer().setListener(this);
        }
        getHandler().post(() -> {
            mViewModelListener.getCallViewContainer().addCallView(
                    callDataList);
            updateDisplayCallDurationStatus();
        });
    }

    @Override
    public void removeCallView(String userId) {
        getHandler().post(() -> {
            mViewModelListener.getCallViewContainer().removeCallView(userId);
            updateDisplayCallDurationStatus();
        });
    }

    @Override
    public void onUpdateMainCallProfileVisibilityChanged(boolean isVisible, boolean shouldDisplayDuration) {
        Timber.i("onUpdateMainCallProfileVisibilityChanged: " + isVisible +
                ", shouldDisplayDuration: " + shouldDisplayDuration +
                ", isInRequestingVideoMode: " + isInRequestingVideoMode());
        bindCallerInfo(mCurrentGroup, mCurrentRecipient);
        checkToShowCallProfile("onUpdateMainCallProfileVisibilityChanged", isVisible);
        if (!isVideoCall()) {
            setCallControlVisibility(false);
        } else {
            setCallControlVisibility(!isVisible);
        }

        if (!isInRequestingVideoMode() &&
                shouldDisplayDuration &&
                getCallService().isCallInProgress()) {
            mShouldDisplayCallDuration = true;
        }
    }

    private void checkToShowCallProfile(String tag, boolean isVisible) {
        Timber.i("checkToShowCallProfile from: " + tag + ", isVisible: " + isVisible);
        getHandler().post(() -> mCallProfileVisibility.set(isVisible ? View.VISIBLE : View.INVISIBLE));
    }

    @Override
    public void onWaitingOtherMemberToJoinCall() {
        mCallBehavior.set(GeneralCallBehaviorType.CALL_IN_PROGRESS.ordinal());
        mCallStatus.set(mContext.getString(R.string.call_screen_waiting_label));
        updateDisplayCallDurationStatus();
    }

    public ObservableInt getSwapCameraVisibility() {
        return mSwapCameraVisibility;
    }

    public ObservableInt getAudioOutputVisibility() {
        return mAudioOutputVisibility;
    }

    public ObservableInt getCallBehavior() {
        return mCallBehavior;
    }

    public ObservableBoolean getIsVideoOff() {
        return mIsVideoOff;
    }

    public ObservableBoolean getIsMicOff() {
        return mIsMicOff;
    }

    public ObservableInt getCallProfileVisibility() {
        return mCallProfileVisibility;
    }

    @Override
    public AbsCallService.CallType getCallType() {
        return mCallType;
    }

    @Override
    public void onCalLViewClicked() {
        if (isVideoCall() && mCallProfileVisibility.get() != View.VISIBLE) {
            Timber.i("mIsCallInProgress: " + mIsCallInProgress
                    + ", mCallOptionButtonHidden: " + mCallOptionButtonHidden);
            if (mIsCallInProgress && GeneralCallBehaviorType.getFromValue(mCallBehavior.get())
                    != GeneralCallBehaviorType.SHOWING_REQUEST_CAMERA) {
                if (!mCallOptionButtonHidden) {
                    setCallControlVisibility(true);
                } else {
                    setCallControlVisibility(false);
                }
            }
        }
    }

    @Override
    public int getJoinedGroupCallParticipantCounter() {
        return getCallService().getJoinCallUserId().size();
    }

    @Override
    public boolean isInFloatingMode() {
        return mBinder.isCallViewInFloatingMode();
    }

    @Override
    public void onVideoCameraStateChanged(String userId, boolean isOff, boolean isCurrentUser) {
        Timber.i("onVideoCameraStateChanged: userId: " + userId + ", isOff: " + isOff + ", isCurrentUser: " + isCurrentUser);
        getHandler().post(() -> {
            mViewModelListener.getCallViewContainer().resetCurrentUserRequestingCamera();
            if (isCurrentUser) {
                mViewModelListener.getCallViewContainer().onToggleVideoCameraOfCurrentUser(!isOff);
            } else {
                mViewModelListener.getCallViewContainer().onToggleVideoCameraOfParticipant(userId, !isOff);
                checkCallProfileVisibility("onVideoCameraStateChanged");
            }
            /*
                If it's a video call, when all user have disabled their video, we show the button
                for user to switch between speaker and phone. But when someone re-enable their
                video, we force speaker mode for the call again.
             */
            if (isVideoCall()) {
                if (mViewModelListener.getCallViewContainer().isAllParticipantHasNoVideoEnable()) {
                    mAudioOutputVisibility.set(View.VISIBLE);
                } else {
                    mAudioOutputVisibility.set(View.GONE);
                    checkToEnableCallAudioOutputViaPhoneOrSpeaker(CallAudioOutputType.SPEAKER, false);
                }
            }
        });
    }

    @Override
    public GeneralCallBehaviorType getCurrentCallBehavior() {
        return GeneralCallBehaviorType.getFromValue(mCallBehavior.get());
    }

    public void onVideoToggleClick(View view) {
        AnimationHelper.animateBounce(view, () -> {
            mIsVideoOff.set(!mIsVideoOff.get());
            mSwapCameraVisibility.set(mIsVideoOff.get() ? View.GONE : View.VISIBLE);
            if (mIsVideoOff.get()) {
                mBinder.getVoiceCallClientService().toggleVideoCamera(false);
            } else {
                mBinder.getVoiceCallClientService().toggleVideoCamera(true);
            }
        });
    }

    public void onSwapCameraClick(View view) {
        AnimationHelper.animateBounce(view, null);
        mBinder.getVoiceCallClientService().switchCamera();
    }

    public void onToggleMicClick(View view) {
        AnimationHelper.animateBounce(view, null);
        mIsMicOff.set(!mIsMicOff.get());
        /*
        For one to one call, as caller at the first of initialize call, we will mute the call client
        and will only un-mute back after receiving call accepted status from call's recipient. So if
        user make any change to call mics, we will manage to update only for display and after the call
        engage, we will take into account what user has previously set regarding to mic.
         */
        if (!(!getCallService().isGroupCall() && isCurrentUserCaller() && getCallService().isMissedCall())) {
            mBinder.getVoiceCallClientService().enableMics(!mIsMicOff.get());
        }
    }

    public void onAudioOptionClick(View view) {
        if (!mIsBlueToothHeadsetConnected) {
            CallAudioOutputType selectedType = mCallAudioOutputType.get() == CallAudioOutputType.SPEAKER ? CallAudioOutputType.PHONE : CallAudioOutputType.SPEAKER;
            mCallAudioOutputType.set(selectedType);
            AnimationHelper.animateBounce(view, () -> {
                mBluetoothSCOHelper.stopSco();
                CallHelper.enableCallAudioOutputOnPhoneOrSpeaker(mContext,
                        mCallAudioOutputType.get() == CallAudioOutputType.SPEAKER);
            });
        } else {
            AnimationHelper.animateBounce(view, () -> {
                CallAudioOutputOptionDialog dialog = CallAudioOutputOptionDialog.newInstance(mCallAudioOutputType.get());
                dialog.setListener(this::checkOnCallAudioOutputOptionSelected);
                dialog.show(((AppCompatActivity) mContext).getSupportFragmentManager(), null);
            });
        }
    }

    private void checkOnCallAudioOutputOptionSelected(CallAudioOutputType type) {
        if (type == CallAudioOutputType.BLUETOOTH) {
            if (mIsBlueToothHeadsetConnected) {
                mCallAudioOutputType.set(type);
                mBluetoothSCOHelper.startSco();
            } else {
                /*
                In case that BlueTooth get disconnected, we will switch to old audio output state.
                 */
                checkToEnableCallAudioOutputViaPhoneOrSpeaker(mCallAudioOutputType.get(), false);
            }
        } else {
            /*
            Need to disconnect output audio from BlueTooth if it was previously enabled.
             */
            if (mCallAudioOutputType.get() == CallAudioOutputType.BLUETOOTH) {
                mBluetoothSCOHelper.stopSco();
            }
            checkToEnableCallAudioOutputViaPhoneOrSpeaker(type, false);
        }
    }

    private void checkToEnableCallAudioOutputViaPhoneOrSpeaker(CallAudioOutputType newSelectedType, boolean isCheckingToggle) {
        if (isCheckingToggle) {
            //Will check to make toggle selection between phone and speaker
            CallAudioOutputType selectedType = mCallAudioOutputType.get() == CallAudioOutputType.SPEAKER ? CallAudioOutputType.PHONE : CallAudioOutputType.SPEAKER;
            mCallAudioOutputType.set(selectedType);
            mBluetoothSCOHelper.stopSco();
            CallHelper.enableCallAudioOutputOnPhoneOrSpeaker(mContext,
                    mCallAudioOutputType.get() == CallAudioOutputType.SPEAKER);
        } else if (newSelectedType != null) {
            //Will set the new selected type directly
            mCallAudioOutputType.set(newSelectedType);
            mBluetoothSCOHelper.stopSco();
            CallHelper.enableCallAudioOutputOnPhoneOrSpeaker(mContext,
                    mCallAudioOutputType.get() == CallAudioOutputType.SPEAKER);
        }
    }

    public ObservableField<String> getName() {
        return mName;
    }

    public ObservableField<String> getProfilePhoto() {
        return mProfilePhoto;
    }

    public ObservableField<List<User>> getParticipants() {
        return mParticipants;
    }

    @Override
    public boolean isVideoCall() {
        return CallHelper.isVideoCall(mBinder.getVoiceCallClientService().getCallType());
    }

    @Override
    public boolean isGroupCall() {
        return mBinder.getVoiceCallClientService().isGroupCall();
    }

    public boolean isCurrentUserCaller() {
        return CallHelper.isCurrentUserCaller(mContext, getCallService().getCurrentCallData());
    }

    public interface CallingViewModelListener {
        CallViewContainer getCallViewContainer();

        View getTopCallOptionContainerView();

        View getBottomCallOptionContainerView();

        void shouldDispatchStartGroupCall(AbsCallService.CallType callType);

        void onCallChannelIdIdGenerated(String channelId);

        default void onMessageClicked(ChatIntent chatIntent) {
        }

        default void onResizeScreenBackToFullAction() {
        }
    }

    @Override
    public void onRequestingCameraStateChanged(CallData callData, boolean isRequestingCamera) {
        Timber.i("onRequestingCameraStateChanged: " + isRequestingCamera + ", is call engaged: " + !getCallService().isMissedCall());
        if (!isGroupCall()) {
            if (!isVideoCall() && !mIsRecipientOfOneToOneAcceptedShareVideo) {
                if (!getCallService().isMissedCall() && !isRequestingCamera) {
                    getCallService().getCallMessageHelper().sendCancelRequestVideo(getCallService().getCurrentCallData(),
                            getCallService().get1x1CallParticipantId());
                }
                if (!getCallService().isMissedCall() && isRequestingCamera) {
                    statRequestingCameraState(true);
                }
                mViewModelListener.getCallViewContainer().onUpdateCallViewOnRequestingCamera(callData, isRequestingCamera);
            } else {
                updateDisplayCallDurationStatus();
            }
        } else {
            mViewModelListener.getCallViewContainer().onUpdateCallViewOnRequestingCamera(callData, isRequestingCamera);
            if (!getCallService().isMissedCall() && !isRequestingCamera) {
                getCallService().getCallMessageHelper().sendCancelRequestVideo(getCallService().getCurrentCallData(),
                        getCallService().get1x1CallParticipantId());
            }
            if (!getCallService().isMissedCall() && isRequestingCamera) {
                statRequestingCameraState(true);
            } else {
                updateDisplayCallDurationStatus();
            }
        }
    }

    private void statRequestingCameraState(boolean shouldSendRequestMessage) {
        Timber.i("statRequestingCameraState");
        if (!mIsReceivedOrRequestShareVideoRequest) {
            //Current user is the first one in the group that will request share video in call.
            mIsCurrentUserFirstGroupVideoRequester = true;
        }
        /*
        For requesting share video in group call, we won't display requesting screen, just show the
        share video.
         */
        if (shouldSendRequestMessage) {
            sendShareRequestVideo();
        }
        if (!isGroupCall()) {
            mShouldDisplayCallDuration = false;
            mCallStatus.set(mContext.getString(R.string.call_screen_wait_request_share_video_label));
        }
        getCallService().startRequestVideoTimer(true);
        if (isGroupCall() && !mIsReceivedOrRequestShareVideoRequest) {
            mIsReceivedOrRequestShareVideoRequest = true;
            notifyJoinOrLeaveGroupCall(getCallService().getChannelId(),
                    true,
                    true,
                    true,
                    0);
        }
        checkToUpdateToVideoCallTypeIfNecessary();
    }

    private void sendShareRequestVideo() {
        if (!isGroupCall() && !mIsRecipientOfOneToOneAcceptedShareVideo) {
            getCallService().getCallMessageHelper().sendRequestShareVideo(getCallService().getCurrentCallData(),
                    getCallService().get1x1CallParticipantId());
        } else {
            getCallService().getCallMessageHelper().sendRequestShareVideoForGroup(getCallService().getCurrentCallData(),
                    getUserIdForRequestVideoSharing());
        }
    }

    private List<String> getUserIdForRequestVideoSharing() {
        List<String> joinCallUserId = getCallService().getJoinCallUserId();
        List<String> newList = new ArrayList<>();
        for (String userId : joinCallUserId) {
            boolean isExisted = false;
            for (String userId2 : mAcceptedOrRejectedVideoShareUser) {
                if (TextUtils.equals(userId, userId2)) {
                    isExisted = true;
                    break;
                }
            }

            if (!isExisted) {
                newList.add(userId);
            }
        }

        return newList;
    }

    public void onUpdateBluetoothConnectionStatus(boolean isConnected) {
        if (isConnected) {
            mIsBlueToothHeadsetConnected = true;
            mCallAudioOutputType.set(CallAudioOutputType.BLUETOOTH);
        } else if (mCallAudioOutputType.get() == CallAudioOutputType.BLUETOOTH) {
            //Bluetooth just get disconnected, so connected back to Speaker by default.
            checkToEnableCallAudioOutputViaPhoneOrSpeaker(CallAudioOutputType.SPEAKER, false);
        }
        mIsBlueToothHeadsetConnected = isConnected;
    }

    @Override
    public void onReceivingCameraRequestStateChanged(CallData participantData) {
        Timber.i("onReceivingCameraRequestStateChanged");
        mViewModelListener.getCallViewContainer().onReceivingCameraRequestStateChanged(participantData);
    }

    @Override
    public void onOtherParticipantAcceptVideoRequest(CallData participantData) {
        Timber.i("onOtherParticipantAcceptVideoRequest");
        mViewModelListener.getCallViewContainer().onOtherParticipantAcceptVideoRequest(participantData);
    }

    @Override
    public void onAudioStateChanged(String userId, boolean isMute) {
        mViewModelListener.getCallViewContainer().onAudioStateChanged(userId, isMute);
    }

    @Override
    public boolean isReceivedOrRequestShareVideoRequest() {
        return mIsReceivedOrRequestShareVideoRequest;
    }

    @Override
    public void onGroupMemberJoinedCall(User user) {
        Timber.i("onGroupMemberJoinedCall: mIsCurrentUserFirstGroupVideoRequester: " + mIsCurrentUserFirstGroupVideoRequester);
        checkToDisplayGroupCallMemberJoinOrLeaveCall(user, false);
        if (mIsCurrentUserFirstGroupVideoRequester) {
            mAcceptedOrRejectedVideoShareUser.add(user.getId());
            getCallService().getCallMessageHelper().sendRequestShareVideoToUserDirectly(getCallService().getCurrentCallData(),
                    user.getId());
        }
    }

    @Override
    public void onGroupMemberLeftGroupCall(User user) {
        Timber.i("onGroupMemberLeftGroupCall");
        checkToDisplayGroupCallMemberJoinOrLeaveCall(user, true);
    }

    public void handleNotificationAction(AbsCallService.CallNotificationActionType type, String groupId) {
        Timber.i("handleNotificationAction: type: " + type + ", Group Id: " + groupId);
        if (getCallService() != null && getCallService().getCurrentCallData() != null) {
            if (type == AbsCallService.CallNotificationActionType.END ||
                    type == AbsCallService.CallNotificationActionType.DECLINE) {
                CallNotificationHelper.cancelCallInfoNotification(mContext);
                onHangupClick(null);
            } else if (type == AbsCallService.CallNotificationActionType.ACCEPT &&
                    !getCallService().isCallInProgress()) {
                onAnswerClick(null);
            }
        }
    }

    public void checkToUpdateToVideoCallTypeIfNecessary(String conversationId, boolean isJoinAvailable, boolean isVideo) {
        Timber.i("updateJoinGroupCallStatus: conversationId: " + conversationId + ", isVideo: " + isVideo);
        /*
        Check to update to group video call type in case the current user does not yet accept incoming
        call and that call has been updated to video call by other participants.
         */
        if (TextUtils.equals(conversationId, getCallService().getChannelId()) &&
                isJoinAvailable &&
                isVideo &&
                !isVideoCall() &&
                GeneralCallBehaviorType.getFromValue(mCallBehavior.get()) == GeneralCallBehaviorType.RECEIVING_CALL &&
                getCallService().isMissedCall()) {

            Timber.i("Will display video of other participant when call accepted.");
            mShouldAddOtherParticipantVideo = true;
        }
    }

    @BindingAdapter({"cancelButton", "acceptButton", "micCallViewContainer", "callBehaviour"})
    public static void bindCallActionContainerStatus(ViewGroup callActionContainer,
                                                     View cancelButton,
                                                     View acceptButton,
                                                     View micCallViewContainer,
                                                     int callBehaviour) {
        /*
            The parent of the passing view must be LinearLayout.
         */
        GeneralCallBehaviorType generalCallBehaviorType = GeneralCallBehaviorType.getFromValue(callBehaviour);
        if (generalCallBehaviorType == GeneralCallBehaviorType.MAKING_CALL ||
                generalCallBehaviorType == GeneralCallBehaviorType.CALL_IN_PROGRESS) {
            micCallViewContainer.setVisibility(View.VISIBLE);
            acceptButton.setVisibility(View.GONE);
            cancelButton.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams cancelButtonParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            cancelButtonParams.gravity = Gravity.CENTER;
            cancelButton.setLayoutParams(cancelButtonParams);
        } else if (generalCallBehaviorType == GeneralCallBehaviorType.RECEIVING_CALL ||
                generalCallBehaviorType == GeneralCallBehaviorType.SHOWING_REQUEST_CAMERA) {
            micCallViewContainer.setVisibility(View.GONE);
            acceptButton.setVisibility(View.VISIBLE);
            cancelButton.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams(0,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    1f);
            buttonParams.gravity = Gravity.CENTER;
            cancelButton.setLayoutParams(buttonParams);
            acceptButton.setLayoutParams(buttonParams);
        }
    }

    @BindingAdapter({"isActive", "activeIconText", "inactiveIconText"})
    public static void bindCallActionStatus(TextView textView,
                                            boolean isActive,
                                            String activeIconText,
                                            String inactiveIconText) {
        textView.setText(isActive ? activeIconText : inactiveIconText);
    }

    @BindingAdapter("callAudioOutputType")
    public static void bindCallOutputAudioType(TextView textView, CallAudioOutputType type) {
        if (type == CallAudioOutputType.BLUETOOTH) {
            textView.setText(textView.getContext().getString(R.string.code_bluetooth));
        } else if (type == CallAudioOutputType.SPEAKER) {
            textView.setText(textView.getContext().getString(R.string.code_sound_on));
        } else {
            textView.setText(textView.getContext().getString(R.string.code_volume_normal));
        }
    }

    @BindingAdapter("anchorView")
    public static void setCurrentUserCallViewMarginTop(CallViewContainer callViewContainer,
                                                       View anchorView) {
        anchorView.post(() -> {
            int[] location = new int[2];
            Timber.i("x: " + location[0] + ", y: " + location[1]);
            anchorView.getLocationOnScreen(location);
            int margin = location[1] + ((int) (anchorView.getHeight() / 1.5));
            Timber.i("marginTop: " + margin);
            callViewContainer.setCurrentUserProfileMarginTop(margin);
        });
    }
}
