package com.proapp.sompom.viewmodel.newviewmodel;

import com.proapp.sompom.model.concierge.ConciergeFeatureStoreSectionResponse;

import java.util.List;

/**
 * Created by Or Vitovongsak on 30/8/21.
 */
public class ConciergeFeaturedStoreGridFragmentViewModel {

    private List<ConciergeFeatureStoreSectionResponse.Data> mData;

    public ConciergeFeaturedStoreGridFragmentViewModel(List<ConciergeFeatureStoreSectionResponse.Data> data) {
        mData = data;
    }

    public List<ConciergeFeatureStoreSectionResponse.Data> getData() {
        return mData;
    }
}
