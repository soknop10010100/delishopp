package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.view.View;

import com.proapp.sompom.listener.OnChatItemListener;
import com.proapp.sompom.model.SelectedChat;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.utils.ChatUtility;

/**
 * Created by Chhom Veasna on 11/26/20.
 */

public class ChatEmojiTextViewModel extends ChatViewModel {

    public ChatEmojiTextViewModel(Activity context,
                                  Conversation conversation,
                                  View rootView,
                                  int position,
                                  Chat chat,
                                  SelectedChat selectedChat,
                                  String searchKeyWord,
                                  ChatUtility.GroupMessage drawable,
                                  boolean senderSide,
                                  OnChatItemListener onItemClickListener) {
        super(context,
                conversation,
                rootView,
                position,
                chat,
                selectedChat,
                searchKeyWord,
                drawable,
                senderSide,
                onItemClickListener);
        updateChatBackground(ChatUtility.getEmojiTextBackground());
    }

    @Override
    protected boolean isChatBackgroundShouldUpdate() {
        return false;
    }
}
