package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.model.request.ChangeOrResetPasswordBody;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.Objects;

import timber.log.Timber;

public class InputConfirmPasswordDialogViewModel extends AbsLoadingViewModel {

    private ObservableField<String> mPassword = new ObservableField<>();
    private ObservableField<String> mPasswordError = new ObservableField<>();
    private ObservableField<String> mConfirmPassword = new ObservableField<>();
    private ObservableField<String> mConfirmPasswordError = new ObservableField<>();
    private ObservableField<String> mWarningText = new ObservableField<>();
    private ObservableInt mWarningVisibility = new ObservableInt(View.GONE);
    private InputPasswordDialogListener mListener;

    public InputConfirmPasswordDialogViewModel(Context context,
                                               InputPasswordDialogListener listener) {
        super(context);
        mListener = listener;
    }

    public ObservableField<String> getPassword() {
        return mPassword;
    }

    public ObservableField<String> getPasswordError() {
        return mPasswordError;
    }

    public ObservableField<String> getConfirmPassword() {
        return mConfirmPassword;
    }

    public ObservableField<String> getConfirmPasswordError() {
        return mConfirmPasswordError;
    }

    public ObservableField<String> getWarningText() {
        return mWarningText;
    }

    public ObservableInt getWarningVisibility() {
        return mWarningVisibility;
    }

    public void setWarningText(String warning) {
        mWarningText.set(warning);
        if (warning != null && !warning.trim().isEmpty()) {
            mWarningVisibility.set(View.VISIBLE);
        }
    }

    public void requestPassword() {
        if (mListener != null) {
            mListener.onPasswordEnterSuccess(mPassword.get());
        }
    }

    public ChangeOrResetPasswordBody getBody() {
        return new ChangeOrResetPasswordBody(mPassword.get(), mPassword.get());
    }

    public boolean isReadyToProceed() {
        return isAllFieldValid() && isPasswordMatch();
    }

    private boolean isAllFieldValid() {
        boolean valid = true;

        if (TextUtils.isEmpty(mPassword.get())) {
            mPasswordError.set(getContext().getString(R.string.login_error_field_required));
            valid = false;
        } else {
            mPasswordError.set(null);
        }

        if (TextUtils.isEmpty(mConfirmPassword.get())) {
            mConfirmPasswordError.set(getContext().getString(R.string.login_error_field_required));
            valid = false;
        } else {
            mConfirmPasswordError.set(null);
        }

        return valid;
    }

    private boolean isPasswordMatch() {
        boolean match = true;

        if (!Objects.requireNonNull(mPassword.get()).matches(Objects.requireNonNull(mConfirmPassword.get()))) {
            match = false;
            mConfirmPasswordError.set(getContext().getString(R.string.change_password_and_confirm_didnt_match));
        } else {
            mConfirmPasswordError.set(null);
        }

        return match;
    }

    public void clearInput() {
        Timber.e("Clear input click");
        mPassword.set("");
    }

    public interface InputPasswordDialogListener {
        void onPasswordEnterSuccess(String password);
    }
}
