package com.proapp.sompom.viewmodel;

import android.view.View;

import androidx.databinding.Observable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.emun.ConciergeUserAddressLabel;

/**
 * Created by Or Vitovongsak on 23/11/21.
 */
public class ListItemConciergeAddressLabelViewModel {

    private final ObservableBoolean mIsSelected = new ObservableBoolean();
    private final ObservableInt mLabel = new ObservableInt();
    private final ObservableInt mLabelTextColor = new ObservableInt();
    private final ObservableInt mLabelBackgroundColor = new ObservableInt();

    private ConciergeUserAddressLabel mConciergeUserAddressLabel;
    private OnClickListener mOnClickListener;

    public ListItemConciergeAddressLabelViewModel(ConciergeUserAddressLabel conciergeUserAddressLabel) {
        mConciergeUserAddressLabel = conciergeUserAddressLabel;
        mLabel.set(conciergeUserAddressLabel.getLabel());
        shouldSelectOrUnselectLabel();
        mIsSelected.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                shouldSelectOrUnselectLabel();
            }
        });
    }

    public void setOnClickListener(OnClickListener listener) {
        mOnClickListener = listener;
    }

    public ConciergeUserAddressLabel getUserAddressLabel() {
        return mConciergeUserAddressLabel;
    }

    public ObservableBoolean getIsSelected() {
        return mIsSelected;
    }

    public void setIsSelected(boolean isSelected) {
        mIsSelected.set(isSelected);
    }

    public ObservableInt getLabel() {
        return mLabel;
    }

    public ObservableInt getLabelTextColor() {
        return mLabelTextColor;
    }

    public ObservableInt getLabelBackgroundColor() {
        return mLabelBackgroundColor;
    }

    public void shouldSelectOrUnselectLabel() {
        if (mIsSelected.get()) {
            mLabelTextColor.set(R.attr.shop_new_address_label_selected);
            mLabelBackgroundColor.set(R.attr.shop_new_address_label_selected_background);
        } else {
            mLabelTextColor.set(R.attr.shop_new_address_label_unselected);
            mLabelBackgroundColor.set(R.attr.shop_new_address_label_unselected_background);
        }
    }

    public void onClick(View view) {
        // Flip select status of the label
        mIsSelected.set(!mIsSelected.get());
        // Only if it's click to select the label should we unselect all the other label
        if (mOnClickListener != null && mIsSelected.get()) {
            mOnClickListener.onClick();
        }
    }
}
