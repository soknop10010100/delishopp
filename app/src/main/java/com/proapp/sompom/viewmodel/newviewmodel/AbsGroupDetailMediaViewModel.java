package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.model.GroupMediaAdaptive;
import com.proapp.sompom.model.GroupMediaSection;
import com.proapp.sompom.model.emun.GroupMediaDetailType;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.GroupDetailMediaDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.List;

/**
 * Created by Veasna Chhom on 7/24/20.
 */
public abstract class AbsGroupDetailMediaViewModel<T extends GroupMediaAdaptive> extends AbsLoadingViewModel {

    protected GroupDetailMediaViewModelListener<T> mListener;
    protected GroupDetailMediaDataManager mDataManager;
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();

    public AbsGroupDetailMediaViewModel(Context context,
                                        GroupDetailMediaDataManager dataManager,
                                        GroupDetailMediaViewModelListener<T> listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
    }

    public ObservableBoolean getIsRefresh() {
        return mIsRefresh;
    }

    @Override
    public void onRetryClick() {
        getMediaDetail(false);
    }

    public final void onRefresh() {
        mIsRefresh.set(true);
        getMediaDetail(false);
    }

    public abstract void getMediaDetail(boolean isLoadMore);

    public interface GroupDetailMediaViewModelListener<T extends GroupMediaAdaptive> {

        void onLoadFailed(ErrorThrowable ex, boolean isLoadMore, GroupMediaDetailType type);

        void onLoadDataSuccess(List<GroupMediaSection<T>> data,
                               boolean canLoadMore,
                               boolean isLoadMoreResult,
                               GroupMediaDetailType type);
    }
}
