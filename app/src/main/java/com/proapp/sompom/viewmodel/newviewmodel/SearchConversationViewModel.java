package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.DelayWatcher;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.SearchConversationDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Veasna Chhom on 8/6/2020.
 */

public class SearchConversationViewModel extends AbsLoadingViewModel {

    public final ObservableBoolean mIsShowClearIcon = new ObservableBoolean();
    public final ObservableField<String> mSearchText = new ObservableField<>();
    private final ObservableInt mLoadingVisibility = new ObservableInt(View.INVISIBLE);
    private SearchConversationDataManager mDataManager;
    private SearchConversationViewModelListener mListener;
    private String mQuery;

    public SearchConversationViewModel(Context context,
                                       ApiService apiService,
                                       GroupDetail groupDetail,
                                       SearchConversationViewModelListener listener) {
        super(context);
        mListener = listener;
        mDataManager = new SearchConversationDataManager(context, groupDetail, apiService);
    }

    public ObservableInt getLoadingVisibility() {
        return mLoadingVisibility;
    }

    private void showCustomLoading() {
        mLoadingVisibility.set(View.VISIBLE);
    }

    private void hideCustomLoading() {
        mLoadingVisibility.set(View.INVISIBLE);
    }

    private void onQuery(String query, boolean isLoadMore) {
        mQuery = query;
        searchMessage(query, isLoadMore);
    }

    public String getQuery() {
        return mQuery;
    }

    @Override
    public void onRetryClick() {
        searchMessage(mQuery, false);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        hideCustomLoading();
    }

    private void searchMessage(String query, boolean isLoadMore) {
        Observable<List<Chat>> observable = mDataManager.searchMessageFromServer(query, isLoadMore);
        BaseObserverHelper<List<Chat>> helper = new BaseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<List<Chat>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (!isLoadMore) {
                    hideLoading();
                    showError(ex.getMessage());
                }
                if (mListener != null) {
                    mListener.onQueryFailed(isLoadMore, ex);
                }
            }

            @Override
            public void onComplete(List<Chat> data) {
                if (!isLoadMore) {
                    hideLoading();
                }
                if (!isLoadMore && (data == null || data.isEmpty()) && !TextUtils.isEmpty(mQuery)) {
                    showNoItemError(getContext().getResources().getString(R.string.search_group_empty_message));
                    mIsShowButtonRetry.set(false);
                } else {
                    if (mListener != null) {
                        mListener.onQuerySuccess(data, isLoadMore, mDataManager.isCanLoadMore());
                    }
                }
            }
        }));
    }

    public void loadMore() {
        onQuery(mQuery, true);
    }

    public TextWatcher onSearchTextChange() {
        return new DelayWatcher(true) {
            @Override
            public void onTextChanged(String text) {
                if (!TextUtils.isEmpty(text)) {
                    onQuery(text, false);
                }
            }

            @Override
            public void onTextChangedNoDelay(String text) {
                if (!TextUtils.isEmpty(text)) {
                    showCustomLoading();
                } else {
                    hideLoading();
                    if (mListener != null) {
                        mListener.onShouldClearResultList();
                    }
                }
            }

            @Override
            public void onTyping(String text) {
                if (!TextUtils.isEmpty(text)) {
                    if (!mIsShowClearIcon.get()) {
                        mIsShowClearIcon.set(true);
                    }
                } else {
                    if (mIsShowClearIcon.get()) {
                        mIsShowClearIcon.set(false);
                    }
                }
            }
        };
    }

    public void onClearText() {
        if (!TextUtils.isEmpty(mSearchText.get())) {
            mSearchText.set("");
        }
    }

    public void onBackPress() {
        if (getContext() instanceof AppCompatActivity) {
            ((AppCompatActivity) getContext()).onBackPressed();

        }
    }

    public interface SearchConversationViewModelListener {

        void onQuerySuccess(List<Chat> data, boolean isLoadMore, boolean isCanLoadMore);

        void onQueryFailed(boolean isLoadMore, Throwable e);

        void onShouldClearResultList();
    }
}
