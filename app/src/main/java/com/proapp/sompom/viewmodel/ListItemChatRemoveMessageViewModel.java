package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.text.Spannable;
import android.text.TextUtils;

import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnChatItemListener;
import com.proapp.sompom.model.SelectedChat;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.ChatUtility;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.SpecialTextRenderUtils;

/**
 * Created by nuonveyo
 * on 3/6/19.
 */
public class ListItemChatRemoveMessageViewModel extends ChatMediaViewModel {

    public final ObservableField<Spannable> mRemoveMessage = new ObservableField<>();

    public ListItemChatRemoveMessageViewModel(Activity context,
                                              Conversation conversation,
                                              Chat chat,
                                              ChatUtility.GroupMessage groupMessage,
                                              int position,
                                              SelectedChat selectChat,
                                              String searchKeyWord,
                                              OnChatItemListener listener) {
        super(context, conversation, chat, groupMessage, position, selectChat, searchKeyWord, listener);
        mRemoveMessage.set(SpecialTextRenderUtils.renderMentionUser(mContext,
                getRemovedMessage(chat.getSenderId()),
                false,
                false,
                -1,
                -1,
                -1,
                false,
                null));
    }

    private String getRemovedMessage(String senderId) {
        if (TextUtils.equals(SharedPrefUtils.getUserId(mContext), senderId)) {
            return mContext.getString(R.string.chat_message_you_remove_message);
        } else {
            User userFromParticipant = getUserFromParticipant(senderId);
            String message = mContext.getString(R.string.chat_message_remove_message);
            if (userFromParticipant != null) {
                return userFromParticipant.getFullName() + " " + message;
            }

            return message;
        }
    }
}
