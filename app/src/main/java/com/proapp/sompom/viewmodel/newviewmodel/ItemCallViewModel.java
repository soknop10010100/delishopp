package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.amulyakhare.textdrawable.TextDrawable;
import com.example.usermentionable.utils.GlideUtil;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.call.CallViewAdapter;
import com.proapp.sompom.model.call.CallData;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.GlideLoadUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;
import com.proapp.sompom.widget.ConversationProfileView;

import java.util.Collections;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 3/16/2020.
 */
public class ItemCallViewModel extends AbsBaseViewModel {

    private ObservableField<String> mProfilePhoto = new ObservableField<>();
    private ObservableField<String> mDefaultBackgroundFirstName = new ObservableField<>();
    private ObservableField<String> mDefaultBackgroundLastName = new ObservableField<>();
    private ObservableField<String> mStatus = new ObservableField<>();
    private ObservableField<String> mName = new ObservableField<>();
    private ObservableField<List<User>> mParticipants = new ObservableField<>();
    private ObservableInt mVideoViewVisibility = new ObservableInt(View.INVISIBLE);
    private ObservableInt mSubCallInfoAreaViewVisibility = new ObservableInt(View.INVISIBLE);
    private ObservableInt mUserProfileVisibility = new ObservableInt(View.INVISIBLE);
    private ObservableBoolean mVideoDisabled = new ObservableBoolean();
    private ObservableBoolean mAudioDisabled = new ObservableBoolean();
    private ObservableBoolean mIsCenteredProfileInfo = new ObservableBoolean();
    private ObservableInt mCallSectionInfoVisibility = new ObservableInt(View.VISIBLE);
    private String mCurrentUserId;
    private CallData mCallData;
    private int mPosition;
    private CallViewAdapter.CallViewAdapterListener mCallViewAdapterListener;

    public ItemCallViewModel(CallViewAdapter.CallViewAdapterListener callViewAdapterListener,
                             CallData callData,
                             int callViewCount,
                             int position,
                             int totalParticipants) {
        /*
        There are two type of call view:
        1. Single view mode which is use for current user call view when there are only two member in call and
        normally the callViewCount of this type has only one.
        2. Grid call view, when a group call has more than two member join and normally "callViewCount" should
        match the count of total call member that participated in that group call.
         */
        mCallViewAdapterListener = callViewAdapterListener;
        mCallData = callData;
        mCurrentUserId = SharedPrefUtils.getUserId(mCallViewAdapterListener.getApplicationContext());
        mPosition = position;
        Timber.i("User: " + mCallData.getUser().getFullName() +
                ", Call Action: " + mCallData.getCallAction() +
                ", isVideo: " + mCallData.isVideoCall() +
                ", isHasVideo: " + mCallData.isHasVideo() +
                ", isCaller: " + mCallData.isCaller() +
                ", callViewCount: " + callViewCount +
                ", totalParticipants: " + totalParticipants);
        checkUpdateItemState(callViewCount);
        mName.set(callData.getUser().getFirstName());
        if (mCallData.isMuteAudio()) {
            onAudioOff(totalParticipants);
        }
    }

    public void setCallSectionInfoVisibility(int callSectionInfoVisibility) {
        mCallSectionInfoVisibility.set(callSectionInfoVisibility);
    }

    public ObservableInt getCallSectionInfoVisibility() {
        return mCallSectionInfoVisibility;
    }

    public ObservableField<String> getDefaultBackgroundFirstName() {
        return mDefaultBackgroundFirstName;
    }

    public ObservableField<String> getDefaultBackgroundLastName() {
        return mDefaultBackgroundLastName;
    }

    public ObservableBoolean getIsCenteredProfileInfo() {
        return mIsCenteredProfileInfo;
    }

    public ObservableInt getUserProfileVisibility() {
        return mUserProfileVisibility;
    }

    public ObservableField<String> getName() {
        return mName;
    }

    public ObservableBoolean getVideoDisabled() {
        return mVideoDisabled;
    }

    public ObservableBoolean getAudioDisabled() {
        return mAudioDisabled;
    }

    public void checkUpdateItemState(int subscriberSize) {
        mUserProfileVisibility.set(subscriberSize > 2 ? View.VISIBLE : View.INVISIBLE);
        mIsCenteredProfileInfo.set(mPosition == 0 && (subscriberSize % 2) != 0);
        setUpCallProfile();
        updateVoiceCallInfo(subscriberSize);
        if (mCallData.isVideoCall() &&
                mCallData.isHasVideo() &&
                ((mCallData.isCaller() && isCurrentUser()) ||
                        (mCallData.getCallAction() == CallData.CallAction.ENGAGED_IN_CALL ||
                                mCallData.getCallAction() == CallData.CallAction.WAITING_OTHER_TO_JOIN))) {
            mVideoViewVisibility.set(View.VISIBLE);
        } else {
            mVideoViewVisibility.set(View.INVISIBLE);
        }

        //Show the video mode disable if necessary
        if (mCallData.isVideoCall() && subscriberSize > 2 && !mCallData.isHasVideo()) {
            onVideoCameraOff(true, subscriberSize);
        }
    }

    public ItemCallViewModelCommunicator getItemCallViewModelCommunicator() {
        return () -> mCallViewAdapterListener.getApplicationContext();
    }

    public CallData getCallData() {
        return mCallData;
    }

    private void setUpCallProfile() {
        if (mCallData.getUserGroup() != null &&
                (mCallData.getCallAction() == CallData.CallAction.INITIALIZE_ViEW ||
                        mCallData.getCallAction() == CallData.CallAction.WAITING_OTHER_TO_JOIN)) {
            //Show group call profile.
            if (!TextUtils.isEmpty(mCallData.getUserGroup().getProfilePhoto())) {
                mDefaultBackgroundFirstName.set(mCallData.getUserGroup().getName());
                mProfilePhoto.set(mCallData.getUserGroup().getProfilePhoto());
                mParticipants.set(mCallData.getUserGroup().getParticipants());
            } else {
                mDefaultBackgroundFirstName.set(mCallData.getUserGroup().getName());
                mParticipants.set(mCallData.getUserGroup().getParticipants());
            }
        } else {
            //Show individual call profile
            mDefaultBackgroundFirstName.set(getUser().getFirstName());
            mDefaultBackgroundLastName.set(getUser().getLastName());
            mProfilePhoto.set(getUser().getUserProfileThumbnail());
            mParticipants.set(Collections.singletonList(getUser()));
        }
    }

    public void revalidateVideoCameraStatus(int participantSize) {
        if (mCallData.isVideoCall()) {
            if (mCallData.isHasVideo()) {
                onVideoCameraOn();
            } else onVideoCameraOff(true, participantSize);
        }
    }

    public boolean isVideoCallViewDisplay() {
        return mVideoViewVisibility.get() == View.VISIBLE;
    }

    public void hideSubInfo() {
        mSubCallInfoAreaViewVisibility.set(View.INVISIBLE);
    }

    public ObservableInt getSubCallInfoAreaViewVisibility() {
        return mSubCallInfoAreaViewVisibility;
    }

    public User getUser() {
        return mCallData.getUser();
    }

    public void updateVoiceCallInfo(int subscriberSize) {
        Timber.i("updateVoiceCallInfo: " + subscriberSize);
        if (!mCallData.isVideoCall()) {
            mSubCallInfoAreaViewVisibility.set(subscriberSize > 2 && !mCallData.isReceivingCameraRequest() ? View.VISIBLE : View.INVISIBLE);
            mVideoDisabled.set(true);
        } else {
            if (mCallData.getCallAction() == CallData.CallAction.ENGAGED_IN_CALL) {
                mSubCallInfoAreaViewVisibility.set(View.INVISIBLE);
            } else if (subscriberSize > 2) {
                mSubCallInfoAreaViewVisibility.set(View.VISIBLE);
            }
        }
    }

    private boolean isCurrentUser() {
        return TextUtils.equals(mCurrentUserId, getUser().getId());
    }

    public ObservableInt getVideoViewVisibility() {
        return mVideoViewVisibility;
    }

    public void setVideoViewVisibility(boolean visible) {
        mVideoViewVisibility.set(visible ? View.VISIBLE : View.INVISIBLE);
    }

    public ObservableField<String> getProfilePhoto() {
        return mProfilePhoto;
    }

    public ObservableField<String> getStatus() {
        return mStatus;
    }

    public ObservableField<List<User>> getParticipants() {
        return mParticipants;
    }

    public void setStatus(String status) {
        mStatus.set(status);
    }

    public void setParticipants(List<User> participants) {
        mParticipants.set(participants);
    }

    public void onVideoCameraOff(boolean showSubCallInfo, int participantSize) {
        Timber.i("onVideoCameraOff: showSubCallInfo: " + showSubCallInfo + ", call action:" + mCallData.getCallAction());
        if (participantSize == 1) {
            setUpCallProfile();
        }
        mVideoViewVisibility.set(View.INVISIBLE);
        //Sub info such as video or audio should be shown for group call view.
        if (showSubCallInfo && participantSize > 2) {
            mSubCallInfoAreaViewVisibility.set(View.VISIBLE);
        }
        mVideoDisabled.set(true);
    }

    public void onVideoCameraOn() {
        Timber.i("onVideoCameraOn");
        mVideoDisabled.set(false);
        mVideoViewVisibility.set(View.VISIBLE);
    }

    public void onAudioOn() {
        Timber.i("onAudioOn");
        mAudioDisabled.set(false);
    }

    public void onAudioOff(int joinedParticipantCounter) {
        Timber.i("onAudioOff");
        if (joinedParticipantCounter > 2) {
            mSubCallInfoAreaViewVisibility.set(View.VISIBLE);
        }
        mAudioDisabled.set(true);
    }

    @BindingAdapter({"profileView",
            "textViewName",
            "textViewDisabledVideo",
            "textViewDisabledAudio",
            "textViewDisabledLabel"})
    public static void setAdaptiveChildViewSize(ViewGroup root,
                                                ConversationProfileView profileView,
                                                TextView textViewName,
                                                TextView textViewDisabledVideo,
                                                TextView textViewDisabledAudio,
                                                TextView textViewDisabledLabel) {
        root.post(() -> {
            int iconSize = getMaxVideoCallWidth();
            Timber.i("icon size: " + iconSize);
            //Set call profile and name size
            int profileSize = (int) (iconSize * 0.35);
            int textSize = (int) (profileSize * 0.65);
            profileView.getLayoutParams().width = profileSize;
            profileView.getLayoutParams().height = profileSize;
            textViewName.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

            //Set disable views size
            setViewSize(textViewDisabledVideo, iconSize - 1, iconSize - 1);
            setViewSize(textViewDisabledAudio, iconSize, iconSize);
            textViewDisabledLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            int paddingTop = root.getResources().getDimensionPixelSize(R.dimen.space_tiny);
            int paddingEnd = root.getResources().getDimensionPixelSize(R.dimen.capture_button_space);
            textViewName.setPadding((int) (profileSize + (profileSize * 0.25)), paddingTop, paddingEnd, paddingTop);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            int marginTop = (int) (textSize * 0.5);
            Timber.i("Text size: " + textSize + ", marginTop: " + marginTop);
            params.topMargin = (int) (textSize * 0.5);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            textViewDisabledLabel.setLayoutParams(params);
        });
    }

    private static void setViewSize(View view, int width, int height) {
        view.setLayoutParams(new LinearLayout.LayoutParams(width, height));
    }

    private static int getMaxVideoCallWidth() {
        int widthPixels = Resources.getSystem().getDisplayMetrics().widthPixels;
//        Timber.i("Device width: " + widthPixels);
        /*
            Format of video render as following
            1|2
            3|4
            ...
            So we assume that the max width will define as following
         */
        return (int) ((widthPixels / 2) * 0.25);
    }

    @BindingAdapter({"disableLabel",
            "disableVideoView",
            "disableAudioView",
            "isDisabledVideo",
            "isDisabledAudio"})
    public static void setDisableCallAudioAndVideoState(ViewGroup container,
                                                        TextView disableLabel,
                                                        TextView disableVideoView,
                                                        TextView disableAudioView,
                                                        boolean isDisabledVideo,
                                                        boolean isDisabledAudio) {
        Timber.i("setDisableCallAudioAndVideoState");
        if (isDisabledVideo && isDisabledAudio) {
            disableAudioView.setVisibility(View.VISIBLE);
            disableVideoView.setVisibility(View.VISIBLE);
            disableLabel.setVisibility(View.VISIBLE);
            disableLabel.setText(R.string.call_screen_audio_video_disable_status);
            ((LinearLayout.LayoutParams) disableAudioView.getLayoutParams()).leftMargin =
                    container.getResources().getDimensionPixelSize(R.dimen.minus_call_margin);
        } else if (isDisabledVideo) {
            disableAudioView.setVisibility(View.GONE);
            disableVideoView.setVisibility(View.VISIBLE);
            disableLabel.setVisibility(View.VISIBLE);
            disableLabel.setText(R.string.call_screen_video_disable_status);
        } else if (isDisabledAudio) {
            ((LinearLayout.LayoutParams) disableAudioView.getLayoutParams()).leftMargin = 0;
            disableVideoView.setVisibility(View.GONE);
            disableAudioView.setVisibility(View.VISIBLE);
            disableLabel.setVisibility(View.VISIBLE);
            disableLabel.setText(R.string.call_screen_audio_mute_status);
        } else {
            disableAudioView.setVisibility(View.GONE);
            disableVideoView.setVisibility(View.GONE);
            disableLabel.setVisibility(View.GONE);
        }
    }

    public interface ItemCallViewModelCommunicator {
        //Must be the application context
        Context getValidContext();
    }

    @BindingAdapter("isCentered")
    public static void setCenterCallProfileInfo(ViewGroup container, boolean isCentered) {
        if (isCentered) {
            ((RelativeLayout.LayoutParams) container.getLayoutParams()).addRule(RelativeLayout.CENTER_HORIZONTAL);
        } else {
            ((RelativeLayout.LayoutParams) container.getLayoutParams()).removeRule(RelativeLayout.CENTER_HORIZONTAL);
        }
    }

    @BindingAdapter({"setCallProfileBackground", "setFirstName", "setLastName", "communicator"})
    public static void setCallProfileBackground(ImageView imageView,
                                                String url,
                                                String firstName,
                                                String lastName,
                                                ItemCallViewModelCommunicator communicator) {
        if (communicator != null) {
            TextDrawable textDrawable = GlideUtil.getDrawableProfileFromName(communicator.getValidContext(),
                    firstName, lastName, true);
            GlideLoadUtil.loadBlurImage(communicator.getValidContext(),
                    imageView,
                    url,
                    textDrawable);
        }
    }

    @BindingAdapter(value = {"participants",
            "groupImgUrl",
            "groupName",
            "communicator",
            "conversationProfileViewCallback"},
            requireAll = false)
    public static void setConversationProfile(ConversationProfileView layout,
                                              List<User> participants,
                                              String groupImgUrl,
                                              String groupName,
                                              ItemCallViewModelCommunicator communicator,
                                              ConversationProfileView.ConversationProfileViewCallback callback) {
        if (callback != null) {
            layout.setConversationProfileViewCallback(callback);
        }
        if (participants != null && !participants.isEmpty()) {
            if (communicator != null) {
                layout.setApplicationContext(communicator.getValidContext());
                layout.bindData(communicator.getValidContext(), participants, groupImgUrl, groupName);
            } else {
                layout.bindData(layout.getContext(), participants, groupImgUrl, groupName);
            }
        }
    }
}
