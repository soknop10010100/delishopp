package com.proapp.sompom.viewmodel.binding;

import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.FontRes;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.BindingAdapter;

import com.desmond.squarecamera.model.AppTheme;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.helper.LongClickLinkMovementMethod;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.listener.OnSpannableClickListener;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.model.emun.Range;
import com.proapp.sompom.model.result.MoreGame;
import com.proapp.sompom.model.result.NotificationData;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.CurrencyUtils;
import com.proapp.sompom.utils.FormatNumber;
import com.proapp.sompom.utils.GenerateLinkPreviewUtil;
import com.proapp.sompom.utils.SpannableUtil;
import com.proapp.sompom.widget.ImageProfileLayout;
import com.proapp.sompom.widget.MaxWidthLinearLayout;
import com.proapp.sompom.widget.WallStreetDescriptionTextView;
import com.resourcemanager.helper.FontHelper;
import com.resourcemanager.utils.ViewUtils;

import java.util.List;

import timber.log.Timber;

/**
 * Created by nuonveyo on 4/24/18.
 */

public final class TextViewBindingUtil {
    private TextViewBindingUtil() {
    }

    @BindingAdapter(value = {"setProductPrice", "isFormatPrice"}, requireAll = false)
    public static void setProductImage(TextView textView, Product product, boolean isFormatPrice) {
        if (product == null || TextUtils.isEmpty(product.getCurrency())) {
            return;
        }
        String price;
        if (isFormatPrice) {
            price = CurrencyUtils.formatPrice(product.getPrice(), product.getCurrency());
        } else {
            price = CurrencyUtils.formatMoney(product.getPrice(), product.getCurrency());
        }
        textView.setText(price);
    }

    @BindingAdapter("setProductFullPrice")
    public static void setProductFullPrice(TextView textView, Product product) {
        if (product == null || TextUtils.isEmpty(product.getCurrency())) {
            return;
        }
        String price = CurrencyUtils.formatFullCurrency(product.getPrice(), product.getCurrency());
        textView.setText(price);
    }

    @BindingAdapter("android:background")
    public static void background(TextView textView, int product) {
        textView.setBackgroundResource(product);
    }

    @BindingAdapter("android:text")
    public static void setProductImage(TextView textView, int product) {
        try {
            textView.setText(product);
        } catch (Exception ex) {
            Timber.e("setProductImage: %s", ex.getMessage());
        }
    }

    @BindingAdapter("setUserName")
    public static void getFullName(TextView textView, User user) {
        if (user == null) {
            return;
        }
        textView.setText(UserHelper.getValidDisplayName(user.getFullName()));
    }

    @BindingAdapter("setUserPosition")
    public static void setUserPosition(TextView textView, User user) {
        if (user == null) {
            return;
        }
        textView.setVisibility(TextUtils.isEmpty(user.getUserJob()) ? View.GONE : View.VISIBLE);
        textView.setText(user.getUserJob());
    }

    @BindingAdapter("isChangeCustomTabFont")
    public static void isChangeCustomTabFont(TextView textView, boolean isChange) {
        Typeface typeface = FontHelper.getRegularFontStyle(textView.getContext());
        if (isChange) {
            typeface = FontHelper.getBoldFontStyle(textView.getContext());
        }
        textView.setTypeface(typeface);
    }


    @BindingAdapter("setTextViewLabel")
    public static void setTextViewLabel(final TextView mTextView, final MoreGame moreGame) {
        mTextView.setText(moreGame.getSubtitle());
        mTextView.setOnClickListener(v -> {
            Context context = mTextView.getContext();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(moreGame.getLinkAndroid()));
            context.startActivity(browserIntent);
        });
    }


    @BindingAdapter("setFacebookButtonText")
    public static void setFacebookButtonText(TextView textSpannable, String value) {
        if (TextUtils.isEmpty(value)) {
            return;
        }
        String boldText = textSpannable.getContext().getString(R.string.seller_store_authenticated_by_facebook_only_height_light);
        String boldTextPhone = textSpannable.getContext().getString(R.string.seller_store_authenticated_by_phone_only_height_light);
        Spannable spannable = SpannableUtil.setFacebookButtonText(textSpannable.getContext(), value, boldText, boldTextPhone);
        textSpannable.setText(spannable);
    }

    @BindingAdapter(value = {"setNotificationDescriptionText", "setOnClickListener"})
    public static void setNotificationDescriptionText(TextView textView, String text, OnSpannableClickListener listener) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        Spannable spannable = SpannableUtil.setNotificationDescriptionText(textView.getContext(), text, listener);
        textView.setText(spannable);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }


    @BindingAdapter("setStoreName")
    public static void setStoreName(TextView textView, User user) {
        if (user == null) {
            return;
        }

        if (!TextUtils.isEmpty(user.getStoreName()))
            textView.setText(user.getStoreName());
        else {
            String userName = user.getFirstName() + " "
                    + textView.getContext().getString(R.string.store);
            textView.setText(userName);
        }
    }

    private static String getFullNameFromNotification(NotificationData notificationData) {
        if (notificationData != null && notificationData.getUser() != null) {
            return notificationData.getUser().getFullName();
        } else {
            return "";
        }
    }

    @BindingAdapter({"likes"})
    public static void setTimelineLikeCount(TextView textView, long value) {
        if (value == 0) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            String likeResource = FormatNumber.format(value) + " ";
            if (value == 1) {
                likeResource += textView.getContext().getString(R.string.post_count_like);
            } else {
                likeResource += textView.getContext().getString(R.string.post_count_multiple_like);
            }
            textView.setText(likeResource);
        }
    }

    @BindingAdapter({"shares"})
    public static void setTimelineShareCount(TextView textView, long value) {
        if (value == 0) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            String likeResource = FormatNumber.format(value) + " ";
            if (value == 1) {
                likeResource += textView.getContext().getString(R.string.share);
            } else {
                likeResource += textView.getContext().getString(R.string.home_button_shares);
            }
            textView.setText(likeResource);
        }
    }

    @BindingAdapter({"comments"})
    public static void setTimelineCommentCount(TextView textView, long value) {
        if (value == 0) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            String likeResource = FormatNumber.format(value) + " ";
            if (value == 1) {
                likeResource += textView.getContext().getString(R.string.post_count_comment);
            } else {
                likeResource += textView.getContext().getString(R.string.post_count_multiple_comment);
            }
            textView.setText(likeResource);
        }
    }

    @BindingAdapter(value = {"setUserNameTimeline",
            "checkInPlace",
            "shareTextTitle",
            "adsTitle",
            "renderType"},
            requireAll = false)
    public static void setUserNameTimeline(TextView textView,
                                           User user,
                                           SearchAddressResult checkInPlace,
                                           String shareTextTitle,
                                           String adsTitle,
                                           ImageProfileLayout.RenderType renderType) {
        if (user != null) {
            if (checkInPlace != null) {
                textView.setText(SpannableUtil.getUserFullNameCheckIn(textView.getContext(),
                        user,
                        checkInPlace,
                        v -> {
                            //Empty Implementation
                            Timber.i("on checked-in clicked.");
                            if (checkInPlace.getLocations() != null) {
                                openGoogleMapWithSpecificCoordinate(textView.getContext(),
                                        checkInPlace.getLocations());
                            }
                        }));
                textView.setMovementMethod(LongClickLinkMovementMethod.getInstance());
            } else {
                if (TextUtils.isEmpty(shareTextTitle)) {
                    textView.setText(SpannableUtil.getUserNameSharedTimeline(textView.getContext(),
                            user, renderType));
                } else {
                    textView.setText(SpannableUtil.getUserNameSharedTimeline(textView.getContext(), user, shareTextTitle, renderType));
                }
            }
        } else if (!TextUtils.isEmpty(adsTitle)) {
            textView.setText(adsTitle);
        }
    }

    private static void openGoogleMapWithSpecificCoordinate(Context context, Locations location) {
        String coordinate = String.format("geo:0,0?q=%1$s,%2$s",
                String.valueOf(location.getLatitude()),
                String.valueOf(location.getLongitude()));
        Uri gmmIntentUri = Uri.parse(coordinate);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        Timber.i("openGoogleMapWithSpecificCoordinate: " + mapIntent.resolveActivity(context.getPackageManager()));
        if (mapIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(mapIntent);
        }
    }

    @BindingAdapter("setSpannable")
    public static void setSpannable(TextView textView, Spannable spannable) {
        textView.setText(spannable);
        textView.setMovementMethod(LongClickLinkMovementMethod.getInstance());
    }

    @BindingAdapter(value = {"setChatContent", "isMe", "rootParent"})
    public static void setChatContent(TextView textView,
                                      Spannable content,
                                      boolean isMe,
                                      MaxWidthLinearLayout rootParent) {
        //Need to pass all click event to its parent
        textView.setOnLongClickListener(v -> {
            if (rootParent != null) {
                rootParent.performLongClick();
            }
            return false;
        });
        textView.setOnClickListener(v -> {
            //Will ignore the click even if it was from spannable event click
            if (textView.getSelectionStart() == -1 && textView.getSelectionEnd() == -1) {
                if (rootParent != null) {
                    rootParent.performClick();
                }
            }
        });
        if (content != null) {
            //Check to build email link first
            int emailColor = isMe ? AttributeConverter.convertAttrToColor(textView.getContext(),
                    R.attr.chat_email_me_color) : AttributeConverter.convertAttrToColor(textView.getContext(),
                    R.attr.chat_email_recipient_color);
            Spannable spannable = SpannableUtil.getEmailLink(textView.getContext(), content, emailColor);

            //Check to build normal lin
            if (!TextUtils.isEmpty(GenerateLinkPreviewUtil.getPreviewLink(content.toString()))) {
                int linkColor = isMe ? AttributeConverter.convertAttrToColor(textView.getContext(),
                        R.attr.chat_link_me_color) : AttributeConverter.convertAttrToColor(textView.getContext(),
                        R.attr.chat_link_recipient_color);
                spannable = SpannableUtil.getTextLink(textView.getContext(), content, linkColor);
            }
            textView.setText(spannable);
        } else {
            textView.setText(content);
        }
        textView.setMovementMethod(LongClickLinkMovementMethod.getInstance());
    }

    @BindingAdapter("changeFontIsMessageRead")
    public static void changeFontIsMessageRead(TextView textView, boolean isRead) {
        Timber.e("changeFontIsMessageRead " + isRead);
        Typeface typeface = FontHelper.getBoldFontStyle(textView.getContext());
        if (isRead) {
            typeface = FontHelper.getRegularFontStyle(textView.getContext());
        }
        textView.setTypeface(typeface);
    }

    @BindingAdapter(value = {"chatIconAnimation", "animatorListenerAdapter"}, requireAll = false)
    public static void chatIconAnimation(View view, boolean isStartAnimation, AnimatorListenerAdapter animatorListenerAdapter) {
        if (isStartAnimation) {
            AnimationHelper.translationToX(view, animatorListenerAdapter);
        } else {
            AnimationHelper.translationFromX(view, animatorListenerAdapter);
        }
    }

    @BindingAdapter("updateSendButton")
    public static void updateSendButton(View view, boolean isSent) {
        if (isSent) {
            view.setEnabled(false);
            view.setBackgroundResource(R.drawable.round_sent_forward_corner_backgrounnd);
        } else {
            view.setEnabled(true);
            view.setBackgroundResource(R.drawable.round_forward_corner_background);
        }
    }

    @BindingAdapter("updateSendText")
    public static void updateSendButton(TextView view, boolean isSent) {
        if (isSent) {
            view.setText(R.string.forward_sent_button);
            view.setTextColor(AttributeConverter.convertAttrToColor(view.getContext(), R.attr.colorAccent));
        } else {
            view.setText(R.string.forward_send_button);
            view.setTextColor(Color.WHITE);
        }
    }

    @BindingAdapter("setDialogUserStoreName")
    public static void setDialogUserStoreName(TextView textView, User user) {
        if (user == null) return;
        textView.setText(SpannableUtil.getUserStoreName(textView.getContext(), user));
    }

    @BindingAdapter("android:text")
    public static void text(TextView textView, AppTheme theme) {
        if (theme == null) return;
        textView.setText(theme.getStringRes());
    }

    @BindingAdapter({"setForwardLinkText", "isAudio"})
    public static void setForwardLinkText(TextView textView, String linkText, boolean isAudio) {
        Context context = textView.getContext();
        if (!TextUtils.isEmpty(linkText) || isAudio) {
            Typeface typefaceBold = FontHelper.getRegularFontStyle(context);
            int color = AttributeConverter.convertAttrToColor(context, R.attr.forward_link_url);
            String text = linkText;

            if (isAudio) {
                typefaceBold = FontHelper.getBoldFontStyle(textView.getContext());
                color = AttributeConverter.convertAttrToColor(context, R.attr.forward_audio_text);
                text = context.getString(R.string.forward_audio_recorded);
                Drawable drawable = ContextCompat.getDrawable(context, R.drawable.ic_small_mic_icon);
                if (drawable != null) {
                    drawable = ViewUtils.setColorFilter(drawable, AttributeConverter.convertAttrToColor(context, R.attr.forward_link_title));
                    textView.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable,
                            null,
                            null,
                            null);
                    textView.setCompoundDrawablePadding(context.getResources().getDimensionPixelSize(R.dimen.space_too_small));
                }
            }

            textView.setText(text);
            textView.setTypeface(typefaceBold);
            textView.setTextColor(color);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    @BindingAdapter(value = {"setWallStreetDescription",
            "postId",
            "setMaxLine",
            "setOnTextViewClickListener",
            "postMedia",
            "shouldShowLinkPreview",
            "shouldShowPlacePreview"},
            requireAll = false)
    public static void setWallStreetDescription(WallStreetDescriptionTextView textView,
                                                String text,
                                                String postId,
                                                boolean isSetMaxLine,
                                                OnClickListener onClickListener,
                                                List<Media> postMedia,
                                                boolean shouldShowLinkPreview,
                                                boolean shouldShowPlacePreview) {
        if (TextUtils.isEmpty(text)) {
            return;
        }

        /*
            Rule of display see more in post text content.
            1. If the post contain only text, will show 15 lines before showing see more text
            2. Else will display only 5 lines before see more text
         */
        int maxSeeMoreLine = 15;
        if ((postMedia != null && !postMedia.isEmpty()) ||
                shouldShowLinkPreview ||
                shouldShowPlacePreview) {
            maxSeeMoreLine = 5;
        }

        textView.setShouldShowLinkPreview(shouldShowLinkPreview);
        textView.setShouldShowPlacePreview(shouldShowPlacePreview);
        textView.setDescription(text, postId, isSetMaxLine, maxSeeMoreLine, postMedia, onClickListener);
    }

    @BindingAdapter("buttonLockEnable")
    public static void buttonLockEnable(TextView textView, boolean isEnable) {
        if (isEnable) {
            textView.setText(R.string.profile_user_change_button);
            textView.setTextColor(Color.WHITE);
            textView.setBackgroundResource(R.drawable.button_green_selector);
        } else {
            textView.setText(R.string.edit_profile_locked_button);
            textView.setTextColor(ContextCompat.getColor(textView.getContext(), R.color.darkGrey));
            textView.setBackgroundResource(R.drawable.round_outline_grey);
        }
    }

    @BindingAdapter(value = {"setStoreNameText"})
    public static void setStoreNameText(TextView textView, String text) {
        textView.setText(SpannableUtil.getStoreName(textView.getContext(), text));
    }

    @BindingAdapter("setTextSpannable")
    public static void setTextSpannable(TextView textView, Spannable spannable) {
        textView.setText(spannable);
        textView.setMovementMethod(LongClickLinkMovementMethod.getInstance());
    }

    @BindingAdapter("delegateLongClickEvenToParent")
    public static void setDelegateLongClickEvenToParent(View view, ViewGroup parentView) {
        view.setOnLongClickListener(v -> {
            if (parentView != null) {
                parentView.performLongClick();
            }
            return false;
        });
    }


    @BindingAdapter("slidePrice")
    public static void slidePrice(TextView textView, Range range) {
        textView.setText(CurrencyUtils.formatPrice(range.getValue()));
    }

    @BindingAdapter("customFontStyle")
    public static void customTextStyle(TextView textView, @FontRes int font) {
        textView.setTypeface(ResourcesCompat.getFont(textView.getContext(), font));
    }

    @BindingAdapter("isReadText")
    public static void isReadText(TextView textView, boolean isRead) {
        textView.setTypeface(Typeface.create(textView.getTypeface(), isRead ? Typeface.NORMAL : Typeface.BOLD));
    }

    @BindingAdapter("textVisibility")
    public static void textVisibility(TextView textView, String text) {
        if (!TextUtils.isEmpty(text)) {
            textView.setVisibility(View.VISIBLE);
            textView.setText(text);
        } else {
            textView.setVisibility(View.GONE);
        }
    }
}
