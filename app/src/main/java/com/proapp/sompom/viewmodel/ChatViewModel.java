package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.text.ParcelableSpan;
import android.text.Spannable;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnChatItemListener;
import com.proapp.sompom.model.SelectedChat;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.ChatUtility;
import com.proapp.sompom.utils.SpecialTextRenderUtils;

import java.util.List;

/**
 * Created by He Rotha on 9/14/17.
 */

public class ChatViewModel extends ChatMediaViewModel {

    public final ObservableField<Spannable> mContent = new ObservableField<>();
    private boolean mIsSenderSide;
    private Spannable mSpannableContent;
    private List<ParcelableSpan> mHighLightKeywordSpanList;

    public ChatViewModel(Activity context,
                         Conversation conversation,
                         View rootView,
                         int position,
                         Chat chat,
                         SelectedChat selectedChat,
                         String searchKeyWord,
                         ChatUtility.GroupMessage groupMessage,
                         boolean senderSide,
                         OnChatItemListener onItemClickListener) {
        super(context, conversation, chat, groupMessage, position, selectedChat, searchKeyWord, onItemClickListener);
        mIsSenderSide = senderSide;
        if (chat.isExpandHeight()) {
            selectedChat.setSelect(rootView, chat, groupMessage);
        }
        bindChatContent(true);
    }

    public void bindChatContent(boolean willInjectHighlyKeyWord) {
        String content = mChat.getContent();
        if (!TextUtils.isEmpty(content)) {
            mSpannableContent = SpecialTextRenderUtils.renderMentionUser(mContext, content,
                    false,
                    false,
                    AttributeConverter.convertAttrToColor(mContext, mIsSenderSide ? R.attr.chat_me_mention_color : R.attr.chat_recipient_mention_color),
                    -1,
                    -1,
                    false,
                    null);
            if (willInjectHighlyKeyWord) {
                injectHighLightSearchKeyword();
            }
            mContent.set(mSpannableContent);
        } else {
            mSpannableContent = null;
            mHighLightKeywordSpanList = null;
        }
    }

    @Override
    public String getDisplayChatContent() {
        Spannable spannable = mContent.get();
        if (spannable != null) {
            return spannable.toString();
        }
        return null;
    }

    @Override
    public void onChatContentEdited() {
        bindChatContent(false);
        loadEditedIconVisibility();
    }

    private void injectHighLightSearchKeyword() {
        /*
        Add background high light to search keyword of chat content if the chat type was in search
        mode. And if it is in search mode, that their must be search keyword to pass along this chat.
        */
        if (!TextUtils.isEmpty(getSearchKeyWord())) {
            mHighLightKeywordSpanList = SpecialTextRenderUtils.buildHighLightBackgroundKeyWordSpan(mSpannableContent,
                    getSearchKeyWord(),
                    AttributeConverter.convertAttrToColor(mContext, mIsSenderSide ? R.attr.chat_me_high_light_search_text_background : R.attr.chat_recipient_high_light_search_text_background),
                    null);
        }
    }

    public void removeHighLightSearchKeyword() {
        if (mHighLightKeywordSpanList != null && !mHighLightKeywordSpanList.isEmpty() && mSpannableContent != null) {
            clearSearchKeyword();
            for (ParcelableSpan parcelableSpan : mHighLightKeywordSpanList) {
                mSpannableContent.removeSpan(parcelableSpan);
            }
            mContent.set(mSpannableContent);
            mContent.notifyChange();
        }
    }
}
