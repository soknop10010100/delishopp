package com.proapp.sompom.viewmodel;

import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.InBoardItem;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.InboardingDataManager;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

public class InBoardingViewModel extends AbsLoadingViewModel {

    private InboardingDataManager mDataManager;
    private InBoardingViewModelListener mListener;
    private ObservableInt mPageIndicatorVisibility = new ObservableInt(View.GONE);

    public InBoardingViewModel(InboardingDataManager dataManager,
                               InBoardingViewModelListener listener) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mListener = listener;
    }

    public ObservableInt getPageIndicatorVisibility() {
        return mPageIndicatorVisibility;
    }

    public void requestData() {
        showLoading();
        Observable<Response<List<InBoardItem>>> call = mDataManager.getInBoarding();
        ResponseObserverHelper<Response<List<InBoardItem>>> helper = new ResponseObserverHelper<>(mDataManager.getContext(),
                call);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<InBoardItem>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                if (mListener != null) {
                    onLoadSuccess(mDataManager.getLocalData());
                }
            }

            @Override
            public void onComplete(Response<List<InBoardItem>> result) {
                if (result.body() != null) {
                    onLoadSuccess(result.body());
                } else {
                    hideLoading();
                    showError(mContext.getString(R.string.error_no_data));
                }
            }
        }));
    }

    private void onLoadSuccess(List<InBoardItem> result) {
        if (result.isEmpty() || result.size() == 1) {
            mPageIndicatorVisibility.set(View.GONE);
        } else {
            mPageIndicatorVisibility.set(View.VISIBLE);
        }

        hideLoading();
        if (mListener != null) {
            mListener.onDataLoadSuccess(result);
        }
    }

    @Override
    public void onRetryClick() {
        requestData();
    }

    public final void onNext() {
        if (mListener != null) {
            mListener.onNextPage();
        }
    }

    public final void onSkip() {
        if (mListener != null) {
            mListener.onActionDone();
        }
    }

    public interface InBoardingViewModelListener {

        void onActionDone();

        void onNextPage();

        void onDataLoadSuccess(List<InBoardItem> data);
    }

    @BindingAdapter("isWrapContentWidth")
    public static void setPageIndicatorWidthConstraint(CirclePageIndicator indicator, boolean isWrapContentWidth) {
        indicator.setWrapContent(isWrapContentWidth);
    }
}
