package com.proapp.sompom.viewmodel;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.model.concierge.ConciergeCoupon;

/**
 * Created by Veasna Chhom on 20/9/21.
 */
public class ItemConciergeCouponViewModel {

    private final ObservableField<String> mTitle = new ObservableField<>();
    private final ObservableField<String> mName = new ObservableField<>();
    private final ObservableField<String> mDescription = new ObservableField<>();
    private final ObservableBoolean mIsSelected = new ObservableBoolean();
    private final ConciergeCoupon mCoupon;

    public ItemConciergeCouponViewModel(ConciergeCoupon coupon) {
        mCoupon = coupon;
        bindData();
    }

    private void bindData() {
        mTitle.set(mCoupon.getCouponCode());
        mName.set(mCoupon.getName());
        mDescription.set(mCoupon.getDescription());
        mIsSelected.set(mCoupon.isSelected());
    }

    public void setSelection(boolean isSelected) {
        mIsSelected.set(isSelected);
        mCoupon.setSelected(isSelected);
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    public ObservableField<String> getName() {
        return mName;
    }

    public ObservableField<String> getDescription() {
        return mDescription;
    }

    public ObservableBoolean getIsSelected() {
        return mIsSelected;
    }
}
