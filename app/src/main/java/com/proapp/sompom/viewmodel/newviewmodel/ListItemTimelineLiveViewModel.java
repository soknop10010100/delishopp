package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableField;
import androidx.appcompat.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.ViewGroup;

import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.utils.TimelinePostItemUtil;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewholder.TimelineViewHolder;
import com.proapp.sompom.widget.lifestream.MediaLayout;
import com.proapp.sompom.databinding.ListItemTimelineSharedLiveVideoBinding;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;

/**
 * Created by nuonveyo on 8/6/18.
 */

public class ListItemTimelineLiveViewModel extends ListItemMainTimelineViewModel {
    public final ObservableField<String> mCountViewer = new ObservableField<>();

    public ListItemTimelineLiveViewModel(AbsBaseActivity activity,
                                         BindingViewHolder bindingViewHolder,
                                         Adaptive adaptive,
                                         WallStreetDataManager dataManager,
                                         int position,
                                         OnTimelineItemButtonClickListener onItemClick,
                                         boolean checkLikeComment) {
        super(activity, bindingViewHolder, dataManager, adaptive, position, onItemClick,checkLikeComment);

        //TODO: get viewer counter from server after done
        mCountViewer.set("100");
    }

    @Override
    public void initListItemTimelineHeader() {
        Adaptive adaptive = TimelinePostItemUtil.getAdaptive(mAdaptive);
        ListItemTimelineHeaderViewModel viewModel = new ListItemTimelineHeaderViewModel(mContext,
                adaptive,
                mPosition,
                mOnItemClickListener);
        viewModel.enableLiveHeader();

        Media media = ((WallStreetAdaptive) adaptive).getMedia().get(0);
        MediaLayout mediaLayout = ((ListItemTimelineSharedLiveVideoBinding) mBindingViewHolder.getBinding()).mediaLayout;
        DisplayMetrics metrics = new DisplayMetrics();
        ((AppCompatActivity) mediaLayout.getContext()).getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int screenwidth = metrics.widthPixels;
        ViewGroup.LayoutParams param = mediaLayout.getLayoutParams();
        int height;
        if (media.getWidth() == 0 || media.getHeight() == 0) {
            height = screenwidth;
        } else {
            height = media.getHeight() * screenwidth / media.getWidth();
        }
        param.width = screenwidth;
        param.height = height;

        mediaLayout.setLayoutParams(param);
        mediaLayout.setMedia(media);
        mediaLayout.setVisibleMuteButton(false);
        if (mBindingViewHolder instanceof TimelineViewHolder) {
            ((TimelineViewHolder) mBindingViewHolder).setMediaLayout(mediaLayout, null);
        }
        mTimelineHeaderViewModel.set(viewModel);
    }
}
