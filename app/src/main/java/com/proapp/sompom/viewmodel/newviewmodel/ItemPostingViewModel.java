package com.proapp.sompom.viewmodel.newviewmodel;

import android.app.NotificationManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.PostLifeStream;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.SpecialTextRenderUtils;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 11/7/2019.
 */
public class ItemPostingViewModel extends AbsBaseViewModel {

    private ObservableInt mProgression = new ObservableInt();
    private Spanned mContent;
    private String mPhoto;
    private ObservableField<Drawable> mActionIcon = new ObservableField<>();
    private int mPhotoVisibility = View.GONE;
    private int mContentVisibility = View.GONE;
    private ItemPostingViewModelListener mListener;
    private ObservableBoolean mIsPostError = new ObservableBoolean();
    private PostLifeStream mPostLifeStream;

    public ItemPostingViewModel(Context context, PostLifeStream postLifeStream, int progression) {
        mPostLifeStream = postLifeStream;
        bindData(context);
        updatePostProgression(progression);
    }

    public void setListener(ItemPostingViewModelListener listener) {
        mListener = listener;
    }

    private void bindData(Context context) {
        if (mPostLifeStream.getLifeStream() != null) {
            mActionIcon.set(ContextCompat.getDrawable(context, R.drawable.ic_posting_cross));
            mContentVisibility = TextUtils.isEmpty(mPostLifeStream.getLifeStream().getTitle()) ? View.GONE : View.VISIBLE;
            boolean hasMedia = mPostLifeStream.getLifeStream().getMedia() != null && !mPostLifeStream.getLifeStream().getMedia().isEmpty();
            mPhotoVisibility = !hasMedia ? View.GONE : View.VISIBLE;
            mContent = SpecialTextRenderUtils.renderMentionUser(context,
                    mPostLifeStream.getLifeStream().getTitle(),
                    true,
                    true,
                    AttributeConverter.convertAttrToColor(context, R.attr.colorFirstText), -1, -1, false, null);
            if (hasMedia) {
                mPhoto = mPostLifeStream.getLifeStream().getMedia().get(0).getUrl();
            }
        }
        if (mPostLifeStream.isError()) {
            onPostError(context, mPostLifeStream.getLifeStream(), mPostLifeStream.getErrorNotificationId());
        }
    }

    public ObservableBoolean getIsPostError() {
        return mIsPostError;
    }

    public void updatePostProgression(int progression) {
        Timber.i("updatePostProgression: " + progression);
        mProgression.set(progression);
    }

    public void onPostError(Context context, LifeStream lifeStream, int errorNotificationId) {
        mPostLifeStream.setErrorNotificationId(errorNotificationId);
        mPostLifeStream.setError(true);
        mPostLifeStream.setLifeStream(lifeStream); //Update to get the latest lifeStream from action
        mIsPostError.set(true);
        mActionIcon.set(ContextCompat.getDrawable(context, R.drawable.ic_post_retry));
    }

    public void onRetry(Context context) {
        mPostLifeStream.setError(false);
        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(mPostLifeStream.getErrorNotificationId());
        mIsPostError.set(false);
        mActionIcon.set(ContextCompat.getDrawable(context, R.drawable.ic_posting_cross));
    }

    public ObservableInt getProgression() {
        return mProgression;
    }

    public Spanned getContent() {
        return mContent;
    }

    public String getPhoto() {
        return mPhoto;
    }

    public ObservableField<Drawable> getActionIcon() {
        return mActionIcon;
    }

    public int getPhotoVisibility() {
        return mPhotoVisibility;
    }

    public int getContentVisibility() {
        return mContentVisibility;
    }

    public final void onActionButtonClick() {
        if (mIsPostError.get()) {
            if (mListener != null) {
                mListener.onRetryClicked(mPostLifeStream.getLifeStream());
            }
        } else {
            if (mListener != null) {
                mListener.onCancelClicked(mPostLifeStream.getLifeStream());
            }
        }
    }

    public interface ItemPostingViewModelListener {

        void onCancelClicked(LifeStream lifeStream);

        void onRetryClicked(LifeStream lifeStream);
    }

    @BindingAdapter({"postingLabel", "isError", "previewPhoto"})
    public static void displayError(LinearLayout root,
                                    TextView postingLabel,
                                    boolean isError,
                                    String previewPhoto) {
        LinearLayout.LayoutParams params;
        if (isError) {
            params = new LinearLayout.LayoutParams(0,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    1f);
        } else {
            params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        params.leftMargin = !TextUtils.isEmpty(previewPhoto) ? 0 : root.getResources().getDimensionPixelSize(R.dimen.big_padding);
        params.rightMargin = root.getResources().getDimensionPixelSize(R.dimen.space_large);
        postingLabel.setLayoutParams(params);
    }
}
