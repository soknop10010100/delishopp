package com.proapp.sompom.viewmodel.newviewmodel;

import com.proapp.sompom.model.concierge.ConciergeFeatureStoreSectionResponse;

import java.util.List;

/**
 * Created by Or Vitovongsak on 31/8/21.
 */

public class ListItemConciergeFeaturedStoreViewModel {

    private List<ConciergeFeatureStoreSectionResponse.Data> mFeaturedStores;

    public ListItemConciergeFeaturedStoreViewModel(List<ConciergeFeatureStoreSectionResponse.Data> featuredStores) {
        mFeaturedStores = featuredStores;
    }

    public List<ConciergeFeatureStoreSectionResponse.Data> getFeaturedStores() {
        return mFeaturedStores;
    }
}
