package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableField;

import com.proapp.sompom.model.Media;

/**
 * Created by Chhom Veasna on 7/27/2020.
 */
public class ListItemGroupVideoViewModel {

    private ObservableField<String> mUrl = new ObservableField<>();

    public ListItemGroupVideoViewModel(Media media) {
        mUrl.set(media.getUrl());
    }

    public ObservableField<String> getUrl() {
        return mUrl;
    }
}
