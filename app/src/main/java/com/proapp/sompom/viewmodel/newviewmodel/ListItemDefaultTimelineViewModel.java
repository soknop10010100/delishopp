package com.proapp.sompom.viewmodel.newviewmodel;

import android.view.View;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewholder.TimelineViewHolder;

import timber.log.Timber;

public class ListItemDefaultTimelineViewModel extends ListItemMainTimelineViewModel {

    private final ObservableField<ListItemMainLinkPreviewViewModel> mLinkPreviewViewModel = new ObservableField<>();
    private ObservableInt mLinkPreviewVisibility = new ObservableInt(View.GONE);
    private ObservableInt mPlacePreviewVisibility = new ObservableInt(View.GONE);
    private boolean mShouldRenderPlacePreview;

    public ListItemDefaultTimelineViewModel(AbsBaseActivity activity,
                                            BindingViewHolder bindingViewHolder,
                                            WallStreetDataManager dataManager,
                                            Adaptive adaptive,
                                            int position,
                                            OnTimelineItemButtonClickListener onItemClickListener,
                                            boolean checkLikeComment) {
        super(activity,
                bindingViewHolder,
                dataManager,
                adaptive,
                position,
                onItemClickListener,
                checkLikeComment);
        boolean shouldDisplayLinkPreview = WallStreetHelper.shouldRenderLinkPreview(adaptive);
        mLinkPreviewVisibility.set(shouldDisplayLinkPreview ? View.VISIBLE : View.GONE);
        checkToBindPreviewLink(shouldDisplayLinkPreview,
                mAdaptive.getLinkPreviewModel(),
                bindingViewHolder,
                position);

        if (!shouldDisplayLinkPreview) {
            mShouldRenderPlacePreview = WallStreetHelper.shouldRenderPlacePreview(adaptive);
            mPlacePreviewVisibility.set(mShouldRenderPlacePreview && WallStreetHelper.isEmptyMedia(adaptive) ? View.VISIBLE : View.GONE);
        }
    }

    public boolean isShouldRenderPlacePreview() {
        return mShouldRenderPlacePreview;
    }

    public ObservableInt getPlacePreviewVisibility() {
        return mPlacePreviewVisibility;
    }

    private void checkToBindPreviewLink(boolean shouldDisplayLinkPreview,
                                        LinkPreviewModel previewModel,
                                        BindingViewHolder bindingViewHolder,
                                        int position) {
        Timber.i("checkToBindPreviewLink: " + shouldDisplayLinkPreview +
                ", holder: " + bindingViewHolder.getClass().getSimpleName() +
                ", position: " + position);
        if (shouldDisplayLinkPreview) {
            ListItemMainLinkPreviewViewModel.ListItemMainLinkPreviewViewModelListener listener = null;
            if (LinkPreviewRetriever.isVideoType(previewModel.getLink())) {
                listener = videoLayout -> {
                    if (videoLayout != null && bindingViewHolder instanceof TimelineViewHolder) {
                        ((TimelineViewHolder) bindingViewHolder).setMediaLayout(videoLayout,
                                "Video Preview in post: " + position);
                    }
                };
            } else if (bindingViewHolder instanceof TimelineViewHolder) {
                ((TimelineViewHolder) bindingViewHolder).setMediaLayout(null,
                        "Reset to null in post: " + position);
            }
            mLinkPreviewViewModel.set(new ListItemMainLinkPreviewViewModel(previewModel,
                    false,
                    mOnItemClickListener,
                    listener));
        } else if (bindingViewHolder instanceof TimelineViewHolder) {
            //Reset the media layout to view holder
            ((TimelineViewHolder) bindingViewHolder).setMediaLayout(null, "Reset to null in post: " + position);
        }
    }

    public ObservableField<ListItemMainLinkPreviewViewModel> getLinkPreviewViewModel() {
        return mLinkPreviewViewModel;
    }

    public ObservableInt getLinkPreviewVisibility() {
        return mLinkPreviewVisibility;
    }
}
