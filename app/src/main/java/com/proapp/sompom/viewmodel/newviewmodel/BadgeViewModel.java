package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableLong;

import com.proapp.sompom.viewmodel.AbsBaseViewModel;

public class BadgeViewModel extends AbsBaseViewModel {

    private ObservableLong mBadgeCount = new ObservableLong();

    public ObservableLong getBadgeCount() {
        return mBadgeCount;
    }

    public void setBadgeValue(long badgeValue) {
        mBadgeCount.set(badgeValue);
    }
}
