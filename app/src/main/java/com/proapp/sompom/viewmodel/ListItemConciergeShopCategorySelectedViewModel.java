package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;

import com.google.gson.Gson;
import com.proapp.sompom.adapter.SortItemOptionArrayAdapter;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.model.concierge.ConciergeShopCategory;
import com.proapp.sompom.model.concierge.ConciergeSupplier;
import com.proapp.sompom.model.emun.ConciergeSortItemOption;
import com.proapp.sompom.newui.dialog.ConciergeFilterProductCriteriaBottomSheetDialog;
import com.proapp.sompom.services.datamanager.AbsConciergeProductDataManager;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 24/3/22.
 */
public class ListItemConciergeShopCategorySelectedViewModel extends AbsSupportViewItemByViewModel<AbsConciergeProductDataManager, ListItemConciergeShopCategorySelectedViewModel.ListItemConciergeShopCategorySelectedViewModelListener> {

    private final ObservableField<String> mTitle = new ObservableField<>();
    private ConciergeShopCategory mConciergeShopCategory;
    private ConciergeFilterProductCriteriaBottomSheetDialog.ConciergeFilterProductCriteriaBottomSheetDialogListener mFilterPopupListener;
    private boolean mIsShowOutOfStockItem;
    private List<String> mBrandId;
    protected ConciergeSortItemOption mConciergeSortItemOption;
    private ConciergeSupplier mSupplier;

    public ListItemConciergeShopCategorySelectedViewModel(Context context,
                                                          ConciergeShopCategory category,
                                                          ConciergeSupplier supplier,
                                                          boolean isShowOutOfStockItem,
                                                          List<String> brandId,
                                                          ConciergeFilterProductCriteriaBottomSheetDialog.ConciergeFilterProductCriteriaBottomSheetDialogListener filterPopupListener,
                                                          ListItemConciergeShopCategorySelectedViewModel.ListItemConciergeShopCategorySelectedViewModelListener listener) {
        super(context, listener, null);
        mSupplier = supplier;
        mConciergeSortItemOption = ConciergeHelper.getSortItemOption();
        Timber.i("brandId: " + new Gson().toJson(brandId) + ", mConciergeSortItemOption: " + mConciergeSortItemOption);
        mIsShowOutOfStockItem = isShowOutOfStockItem;
        mBrandId = new ArrayList<>(brandId);
        mFilterCriteriaSelected.set(!brandId.isEmpty() || ConciergeHelper.getShowOutOfStockProduct());
        mFilterPopupListener = filterPopupListener;
        mConciergeShopCategory = category;
        mTitle.set(category.getTitle());
        mListener = listener;
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    @Override
    public void onFilterByOptionClicked() {
        ConciergeFilterProductCriteriaBottomSheetDialog dialog = ConciergeFilterProductCriteriaBottomSheetDialog.newInstance(mConciergeShopCategory.getId(),
                new ArrayList<>(mBrandId),
                mSupplier);
        dialog.setListener(new ConciergeFilterProductCriteriaBottomSheetDialog.ConciergeFilterProductCriteriaBottomSheetDialogListener() {
            @Override
            public void onClearFilterCriteria() {
                mFilterCriteriaSelected.set(false);
                if (mFilterPopupListener != null) {
                    mFilterPopupListener.onClearFilterCriteria();
                }
            }

            @Override
            public void onApplyFilterCriteria(boolean isShowOutOfStockProduct, List<String> ids) {
                mFilterCriteriaSelected.set(true);
                if (mFilterPopupListener != null) {
                    mFilterPopupListener.onApplyFilterCriteria(isShowOutOfStockProduct, ids);
                }
            }
        });
        dialog.show(((AppCompatActivity) mContext).getSupportFragmentManager(),
                ConciergeFilterProductCriteriaBottomSheetDialog.class.getSimpleName());
    }

    public AdapterView.OnItemSelectedListener getSpinnerListener(AppCompatSpinner spinner) {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                SortItemOptionArrayAdapter adapter = (SortItemOptionArrayAdapter) spinner.getAdapter();
                ConciergeSortItemOption sortItemOption = adapter.getItem(position);
                Timber.i("onItemSelected position: " + position);
                if (mConciergeSortItemOption != sortItemOption) {
                    mConciergeSortItemOption = sortItemOption;
                    if (mListener != null) {
                        mListener.onItemSortOptionChanged(mConciergeSortItemOption);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        };
    }

    public interface ListItemConciergeShopCategorySelectedViewModelListener extends AbsSupportViewItemByViewModel.AbsSupportViewItemByViewModelListener {
        void onItemSortOptionChanged(ConciergeSortItemOption sortItemOption);
    }

    @BindingAdapter("sortSpinnerListener")
    public static void setSortSpinnerData(AppCompatSpinner spinner, AdapterView.OnItemSelectedListener listener) {
        if (spinner.getAdapter() == null) {
            SortItemOptionArrayAdapter adapter = new SortItemOptionArrayAdapter(spinner.getContext());
            spinner.setAdapter(adapter);
            ConciergeSortItemOption sortItemOption = ConciergeHelper.getSortItemOption();
            if (sortItemOption != null) {
                int foundPosition = 0;
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (adapter.getItem(i) == sortItemOption) {
                        foundPosition = i;
                        break;
                    }
                }
                spinner.setSelection(foundPosition);
            }
            spinner.postDelayed(() -> spinner.setOnItemSelectedListener(listener), 100);
        }
    }
}
