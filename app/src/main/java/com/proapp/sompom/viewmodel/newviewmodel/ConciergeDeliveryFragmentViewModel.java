package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ItemShopDeliveryTabLayoutBinding;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.ShopDeliveryTimeSelection;
import com.proapp.sompom.model.emun.CitySelectionType;
import com.proapp.sompom.model.result.ShopDeliveryData;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.DeliveryDataManager;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;
import com.proapp.sompom.viewmodel.ItemStateSelectionViewModel;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Veasna Chhom on 9/9/21.
 */
public class ConciergeDeliveryFragmentViewModel extends AbsLoadingViewModel {

    private final ObservableBoolean mIsEmptyTime = new ObservableBoolean(false);
    private final ObservableField<String> mConfirmButtonText = new ObservableField<>();
    private final ObservableInt mConfirmButtonTextColor = new ObservableInt();
    private final ObservableInt mConfirmButtonBackground = new ObservableInt();
    private final ObservableField<CitySelectionType> mSelectedDeliveryTab = new ObservableField<>();
    private final ObservableBoolean mShouldResetSelection = new ObservableBoolean();
    private final ObservableInt mTimeListVisibility = new ObservableInt(View.VISIBLE);

    private boolean mIsDateSelected;
    private boolean mIsTimeSelected;
    private boolean mIsASAP;
    private ObservableBoolean mIsConfirmButtonEnabled = new ObservableBoolean();

    private DeliveryDataManager mDataManager;
    private ConciergeDeliveryFragmentViewModelListener mListener;
    private ShopDeliveryTimeSelection mPreviousSelection;
    private boolean mIsViewOnlyMode;

    public ConciergeDeliveryFragmentViewModel(Context context,
                                              boolean isViewOnlyMode,
                                              DeliveryDataManager dataManager,
                                              ConciergeDeliveryFragmentViewModelListener listener) {
        super(context);
        mIsViewOnlyMode = isViewOnlyMode;
        mDataManager = dataManager;
        mListener = listener;
    }

    public ObservableBoolean getIsConfirmButtonEnabled() {
        return mIsConfirmButtonEnabled;
    }

    @Override
    public void onRetryClick() {
        requestData();
    }

    public ObservableBoolean isEmptyTime() {
        return mIsEmptyTime;
    }

    public void setPreviousSelection(ShopDeliveryTimeSelection previousSelection) {
        mPreviousSelection = previousSelection;
    }

    public ShopDeliveryTimeSelection getPreviousSelection() {
        return mPreviousSelection;
    }

    public ObservableField<String> getConfirmButtonText() {
        return mConfirmButtonText;
    }

    public ObservableInt getConfirmButtonTextColor() {
        return mConfirmButtonTextColor;
    }

    public ObservableInt getConfirmButtonBackground() {
        return mConfirmButtonBackground;
    }

    public ObservableField<CitySelectionType> getSelectedDeliveryTab() {
        return mSelectedDeliveryTab;
    }

    public ObservableInt getTimeListVisibility() {
        return mTimeListVisibility;
    }

    public void requestData() {
        showLoading();
        Observable<Response<ShopDeliveryData>> service = mDataManager.getTimeSlotService();
        ResponseObserverHelper<Response<ShopDeliveryData>> helper = new ResponseObserverHelper<>(getContext(), service);
        addDisposable(helper.execute(new OnCallbackListener<Response<ShopDeliveryData>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.i("onFail: " + ex.getMessage());
                hideLoading();
                showError(ex.getMessage());
            }

            @Override
            public void onComplete(Response<ShopDeliveryData> result) {
                Timber.i("result: " + new Gson().toJson(result.body()));
                if (mPreviousSelection != null) {
                    mShouldResetSelection.set(false);
                    mSelectedDeliveryTab.set(mPreviousSelection.isProvince() ? CitySelectionType.PROVINCE : CitySelectionType.PHNOM_PENH);
                }
                if (mListener != null) {
                    mListener.onLoadDataSuccess(result.body());
                }
            }
        }));
    }

    public void setErrorEmptyTimeSlotDisplay(boolean isEmptyTimeList) {
        mIsEmptyTime.set(isEmptyTimeList);
    }

    public void setConfirmationButtonVisibility(boolean isVisible) {
        if (isVisible && mIsViewOnlyMode) {
            return;
        }

        mIsConfirmButtonEnabled.set(isVisible);
        if (isVisible) {
            mConfirmButtonText.set(mContext.getResources().getString(R.string.shop_delivery_confirmation_button));
            mConfirmButtonTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_delivery_confirm_button));
            mConfirmButtonBackground.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_delivery_confirm_button_background));
        }
    }

    public void onConfirmationClick(View view) {
        if (mIsConfirmButtonEnabled.get()) {
            if (mListener != null) {
                mListener.onConfirmationButtonSelected();
            }
        }
    }

    public void onEmptyProvinceSlots() {
        setErrorEmptyTimeSlotDisplay(true);
    }

    public DeliverySlotTabCallbackListener getDeliverySlotTabListener() {
        return new DeliverySlotTabCallbackListener() {
            @Override
            public void onDeliverySlotTabSelected(CitySelectionType type) {
                Timber.i("onDeliveryTabOptionSelected: " + type);
                if (mListener != null) {
                    mTimeListVisibility.set(type == CitySelectionType.PHNOM_PENH ? View.VISIBLE : View.INVISIBLE);
                    boolean shouldResetSelection = mShouldResetSelection.get();
                    if (!mShouldResetSelection.get()) {
                        mShouldResetSelection.set(true);
                    }
                    mListener.onDeliveryTabOptionSelected(type, shouldResetSelection);
                }
            }
        };
    }

    public interface ConciergeDeliveryFragmentViewModelListener {

        void onLoadDataSuccess(ShopDeliveryData deliveryTimeResponse);

        void onDeliveryTabOptionSelected(CitySelectionType type, boolean shouldResetSelection);

        void onConfirmationButtonSelected();
    }

    public interface DeliverySlotTabCallbackListener {

        void onDeliverySlotTabSelected(CitySelectionType type);
    }

    @BindingAdapter({"phnomPenhOptionView", "provinceOptionView", "selectedDeliveryTab", "listener"})
    public static void setDeliverySlotTabSelection(View containerView,
                                                   ItemShopDeliveryTabLayoutBinding phnomPenhTabBinding,
                                                   ItemShopDeliveryTabLayoutBinding provinceTabBinding,
                                                   CitySelectionType selectedDeliveryTab,
                                                   DeliverySlotTabCallbackListener listener) {
        int selectedTextColor = AttributeConverter.convertAttrToColor(containerView.getContext(), R.attr.shop_delivery_selected_tab_text);
        int unselectedTextColor = AttributeConverter.convertAttrToColor(containerView.getContext(), R.attr.shop_delivery_unselected_tab_text);
        int selectedBackgroundColor = AttributeConverter.convertAttrToColor(containerView.getContext(), R.attr.shop_delivery_selected_tab_background);
        int unselectedBackgroundColor = AttributeConverter.convertAttrToColor(containerView.getContext(), R.attr.shop_delivery_unselected_tab_background);
        ItemStateSelectionViewModel phnomPenhViewModel = new ItemStateSelectionViewModel(CitySelectionType.PHNOM_PENH.getDisplayValue(containerView.getContext()),
                selectedTextColor,
                unselectedTextColor,
                selectedBackgroundColor,
                unselectedBackgroundColor);
        ItemStateSelectionViewModel provinceViewModel = new ItemStateSelectionViewModel(CitySelectionType.PROVINCE.getDisplayValue(containerView.getContext()),
                selectedTextColor,
                unselectedTextColor,
                selectedBackgroundColor,
                unselectedBackgroundColor);
        phnomPenhTabBinding.setVariable(BR.viewModel, phnomPenhViewModel);
        provinceTabBinding.setVariable(BR.viewModel, provinceViewModel);
        phnomPenhTabBinding.getRoot().setOnClickListener(view -> {
            phnomPenhViewModel.updateState(true);
            provinceViewModel.updateState(false);
            if (listener != null) {
                listener.onDeliverySlotTabSelected(CitySelectionType.PHNOM_PENH);
            }
        });
        provinceTabBinding.getRoot().setOnClickListener(view -> {
            phnomPenhViewModel.updateState(false);
            provinceViewModel.updateState(true);
            if (listener != null) {
                listener.onDeliverySlotTabSelected(CitySelectionType.PROVINCE);
            }
        });

        if (selectedDeliveryTab != null) {
            if (selectedDeliveryTab == CitySelectionType.PHNOM_PENH) {
                phnomPenhTabBinding.getRoot().performClick();
            } else if (selectedDeliveryTab == CitySelectionType.PROVINCE) {
                provinceTabBinding.getRoot().performClick();
            }
        } else {
            // Default to phnom penh selection if there are no previous selection
            phnomPenhTabBinding.getRoot().performClick();
        }
    }
}
