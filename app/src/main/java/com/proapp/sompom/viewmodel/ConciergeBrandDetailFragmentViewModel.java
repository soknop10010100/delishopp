package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.text.TextWatcher;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.DelayWatcher;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.emun.ConciergeSortItemOption;
import com.proapp.sompom.model.emun.OpenBrandDetailType;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.ConciergeBrandDetailDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeCartBottomSheetViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Chhom Veasna on 5/4/22.
 */
public class ConciergeBrandDetailFragmentViewModel extends AbsSupportViewItemByViewModel<ConciergeBrandDetailDataManager, ConciergeBrandDetailFragmentViewModel.ConciergeBrandDetailFragmentViewModelListener> {

    private final ObservableBoolean mIsShowClearIcon = new ObservableBoolean();
    private final ObservableField<String> mSearchText = new ObservableField<>();
    private final ObservableField<String> mTitle = new ObservableField<>();
    private final ObservableField<OpenBrandDetailType> mOpenType = new ObservableField<>();

    public ConciergeBrandDetailFragmentViewModel(Context context,
                                                 ConciergeBrandDetailDataManager dataManager,
                                                 ConciergeCartBottomSheetViewModel cartViewModel,
                                                 ConciergeBrandDetailFragmentViewModelListener listener) {
        super(context, cartViewModel, listener, dataManager);
        mDataManager = dataManager;
        mOpenType.set(dataManager.getOpenType());
        if (getInternalAppliedSearchCriteriaLoadingViewModel() != null) {
            getInternalAppliedSearchCriteriaLoadingViewModel().setListener(() -> getData(false, true, false));
        }
        if (mDataManager.isOpenAsSearchGeneralProductMode()) {
            showLoading();
            new Handler().postDelayed(() -> mSearchText.set(mDataManager.getPassParam()), 100);
        } else if (mDataManager.isOpenAsSearchSupplierProductMode()) {
            showLoading();
            new Handler().postDelayed(() -> {
                mSearchText.set("");
                onApplySearching();
            }, 100);
        } else {
            onGetMainData();
        }
    }

    public ObservableField<OpenBrandDetailType> getOpenType() {
        return mOpenType;
    }

    public ConciergeCartBottomSheetViewModel getCartViewModel() {
        return mCartViewModel;
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    public ObservableBoolean getIsShowClearIcon() {
        return mIsShowClearIcon;
    }

    public ObservableField<String> getSearchText() {
        return mSearchText;
    }

    public void initData() {
        if (mOpenType.get() == OpenBrandDetailType.OPEN_AS_NORMAL_MODE) {
            mTitle.set(mDataManager.getBrand().getName());
        } else if (mOpenType.get() == OpenBrandDetailType.OPEN_AS_SEARCH_SUPPLIER_PRODUCT_MODE) {
            mTitle.set(mDataManager.getTitleParam());
        }
    }

    public void onBackPress() {
        if (getContext() instanceof AppCompatActivity) {
            ((AppCompatActivity) getContext()).onBackPressed();
        }
    }

    public void onClearText() {
        if (!TextUtils.isEmpty(mSearchText.get())) {
            mSearchText.set("");
        }
    }

    @Override
    public void onRetryClick() {
        getData(true, false, false);
    }

    @Override
    public void onApplySort(ConciergeSortItemOption option) {
        super.onApplySort(option);
        getData(false, true, false);
    }

    private void onApplySearching() {
        if (mDataManager.isOpenAsSearchGeneralProductMode() || mDataManager.isOpenAsSearchSupplierProductMode()) {
            getData(!mIsFirstDataLoadSuccess, true, false);
        } else {
            getData(false, true, false);
        }
    }

    public void onGetMainData() {
        getData(true, false, false);
    }

    public void loadMoreItem() {
        getData(false, false, true);
    }

    private void getData(boolean isFirstLoading,
                         boolean isAppliedSearchCriteria,
                         boolean isLoadMore) {
        if (isFirstLoading) {
            showLoading();
        } else if (isAppliedSearchCriteria) {
            showAppliedSearchCriteriaLoadingView();
        }

        Observable<Response<List<ConciergeMenuItem>>> observable = mDataManager.getProductItem(isLoadMore,
                mSearchText.get());
        BaseObserverHelper<Response<List<ConciergeMenuItem>>> helper = new BaseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<ConciergeMenuItem>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                if (isFirstLoading) {
                    showError(ex.getMessage(), true);
                } else {
                    if (isAppliedSearchCriteria) {
                        getInternalAppliedSearchCriteriaLoadingViewModel().showError(ex.getMessage(), true);
                    } else {
                        if (!isLoadMore) {
                            getInternalAppliedSearchCriteriaLoadingViewModel().showError(ex.getMessage(), true);
                        } else {
                            showSnackBar(ex.getMessage(), true);
                            if (mListener != null) {
                                mListener.onLoadMoreItemFailed();
                            }
                        }
                    }
                }
            }

            @Override
            public void onComplete(Response<List<ConciergeMenuItem>> result) {
                hideLoading();
                if (!mIsFirstDataLoadSuccess) {
                    mIsFirstDataLoadSuccess = true;
                    initData();
                }

                if (!isLoadMore && result.body().isEmpty()) {
                    getInternalAppliedSearchCriteriaLoadingViewModel().showError(mContext.getString(R.string.error_product_not_found),
                            false);
                }

                if (mListener != null) {
                    mListener.onLoadItemSuccess(result.body(), !isLoadMore, isLoadMore, mDataManager.isCanLoadMore());
                }
            }
        }));
    }

    public TextWatcher onSearchTextChange() {
        return new DelayWatcher(true) {
            @Override
            public void onTextChanged(String text) {
                onApplySearching();
            }

            @Override
            public void onTextChangedNoDelay(String text) {
            }

            @Override
            public void onTyping(String text) {
                if (!TextUtils.isEmpty(text)) {
                    if (!mIsShowClearIcon.get()) {
                        mIsShowClearIcon.set(true);
                    }
                    if (mDataManager.isOpenAsSearchGeneralProductMode() || mDataManager.isOpenAsSearchSupplierProductMode()) {
                        if (mIsFirstDataLoadSuccess) {
                            getInternalAppliedSearchCriteriaLoadingViewModel().showLoading();
                        }
                    } else {
                        getInternalAppliedSearchCriteriaLoadingViewModel().showLoading();
                    }
                } else {
                    if (mIsShowClearIcon.get()) {
                        mIsShowClearIcon.set(false);
                    }
                }
            }
        };
    }

    public interface ConciergeBrandDetailFragmentViewModelListener extends AbsSupportViewItemByViewModelListener {

        void onLoadItemSuccess(List<ConciergeMenuItem> itemList, boolean isRefreshMode, boolean isLoadMore, boolean isCanLoadMore);

        void onLoadMoreItemFailed();
    }
}
