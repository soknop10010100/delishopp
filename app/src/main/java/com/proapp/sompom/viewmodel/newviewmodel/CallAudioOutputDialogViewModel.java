package com.proapp.sompom.viewmodel.newviewmodel;

import android.os.Handler;
import android.view.View;

import androidx.databinding.ObservableInt;

import com.proapp.sompom.model.emun.CallAudioOutputType;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

public class CallAudioOutputDialogViewModel extends AbsBaseViewModel {

    private ObservableInt mPhoneTickVisibility = new ObservableInt(View.GONE);
    private ObservableInt mSpeakerTickVisibility = new ObservableInt(View.GONE);
    private ObservableInt mBlueToothTickVisibility = new ObservableInt(View.GONE);
    private CallAudioOutputDialogViewModelListener mListener;

    public CallAudioOutputDialogViewModel(CallAudioOutputType type, CallAudioOutputDialogViewModelListener listener) {
        checkOnItemSelected(type);
        mListener = listener;
    }

    public ObservableInt getPhoneTickVisibility() {
        return mPhoneTickVisibility;
    }

    public ObservableInt getSpeakerTickVisibility() {
        return mSpeakerTickVisibility;
    }

    public ObservableInt getBlueToothTickVisibility() {
        return mBlueToothTickVisibility;
    }

    private void checkOnItemSelected(CallAudioOutputType type) {
        if (type == CallAudioOutputType.BLUETOOTH) {
            mPhoneTickVisibility.set(View.GONE);
            mSpeakerTickVisibility.set(View.GONE);
            mBlueToothTickVisibility.set(View.VISIBLE);
        } else if (type == CallAudioOutputType.SPEAKER) {
            mPhoneTickVisibility.set(View.GONE);
            mSpeakerTickVisibility.set(View.VISIBLE);
            mBlueToothTickVisibility.set(View.GONE);
        } else {
            mPhoneTickVisibility.set(View.VISIBLE);
            mSpeakerTickVisibility.set(View.GONE);
            mBlueToothTickVisibility.set(View.GONE);
        }
    }

    public void onOptionSelected(CallAudioOutputType type) {
        checkOnItemSelected(type);
        new Handler().postDelayed(() -> {
            if (mListener != null) {
                mListener.onItemSelected(type);
            }
        }, 100);
    }

    public interface CallAudioOutputDialogViewModelListener {

        void onItemSelected(CallAudioOutputType type);
    }
}
