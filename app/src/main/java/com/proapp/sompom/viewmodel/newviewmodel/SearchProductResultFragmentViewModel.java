package com.proapp.sompom.viewmodel.newviewmodel;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.SearchProductResultDataManager;
import com.proapp.sompom.viewmodel.ConciergeSupportAddItemToCardViewModel;

import java.util.List;

import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 25/1/22.
 */
public class SearchProductResultFragmentViewModel extends ConciergeSupportAddItemToCardViewModel<SearchProductResultDataManager, SearchProductResultFragmentViewModel.SearchProductResultFragmentViewModelCallback> {

    private String mSearchText;

    public SearchProductResultFragmentViewModel(SearchProductResultDataManager dataManager,
                                                SearchProductResultFragmentViewModelCallback listener) {
        super(dataManager.getContext(), listener, dataManager);
        setShowKeyboardWhileLoadingScreen();
        mDataManager = dataManager;
        mListener = listener;

    }

    public void searchProduct(String searchText, boolean isRefresh, boolean isLoadMore) {
        mSearchText = searchText;
        Observable<List<ConciergeMenuItem>> callback = mDataManager.searchProduct(mSearchText, isLoadMore);
        BaseObserverHelper<List<ConciergeMenuItem>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<List<ConciergeMenuItem>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                Timber.e(ex);
                showError(ex.getMessage());
            }

            @Override
            public void onComplete(List<ConciergeMenuItem> result) {
                hideLoading();
                if (result.isEmpty() && !isLoadMore) {
                    showError(getContext().getString(R.string.error_product_not_found), false);
                }
                if (mListener != null) {
                    mListener.onSearchResult(result, isRefresh, isLoadMore, mDataManager.isCanLoadMore());
                }
            }
        }));
    }

    @Override
    public void onRetryClick() {
        searchProduct(mSearchText, true, false);
    }

    public void loadMore() {
        searchProduct(mSearchText, false, true);
    }

    public interface SearchProductResultFragmentViewModelCallback extends ConciergeSupportAddItemToCardViewModelListener {

        void onSearchResult(List<ConciergeMenuItem> products,
                            boolean isRefresh,
                            boolean isFromLoadMore,
                            boolean isCanLoadMore);
    }
}
