package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.DelayWatcher;
import com.proapp.sompom.intent.newintent.SearchMessageIntent;
import com.proapp.sompom.newui.SearchMessageActivity;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class SearchGeneralContainerFragmentViewModel extends AbsLoadingViewModel {
    public final ObservableBoolean mIsShowClearIcon = new ObservableBoolean();
    public final ObservableField<String> mSearchText = new ObservableField<>();
    private final ObservableField<String> mSearchHint = new ObservableField<>();
    private final ObservableInt mSearchDividerVisibility = new ObservableInt();
    public final ObservableInt mElevation = new ObservableInt();
    private final Callback mCallback;
    private SearchMessageIntent.SearchType mSearchType;

    public SearchGeneralContainerFragmentViewModel(Context context,
                                                   String query,
                                                   SearchMessageIntent.SearchType searchType,
                                                   Callback callback) {
        super(context);
        mSearchText.set(query);
        setSearchType(searchType);
        mCallback = callback;
        mElevation.set(getContext().getResources().getDimensionPixelSize(R.dimen.space_small));
        performQueryAutomatically(query);
    }

    public ObservableField<String> getSearchHint() {
        return mSearchHint;
    }

    public ObservableInt getSearchDividerVisibility() {
        return mSearchDividerVisibility;
    }

    public void setSearchType(SearchMessageIntent.SearchType searchType) {
        mSearchType = searchType;
        switch (mSearchType) {
            case Normal:
                mSearchHint.set(getContext().getString(R.string.search_home_screen_active_title_hint));
                mSearchDividerVisibility.set(View.VISIBLE);
                break;
            case Product:
                mSearchHint.set(getContext().getString(R.string.search_item_home_screen_input_hint));
                mSearchDividerVisibility.set(View.GONE);
                break;
        }
    }

    private void performQueryAutomatically(String query) {
        if (!TextUtils.isEmpty(query)) {
            if (!mIsShowClearIcon.get()) {
                mIsShowClearIcon.set(true);
                mCallback.onReplaceFragment();
                mElevation.set(getContext().getResources().getDimensionPixelSize(R.dimen.space_tiny));
            } else {
                mCallback.showLoading();
            }
            new Handler().postDelayed(() -> {
                if (!TextUtils.isEmpty(query)) {
                    mCallback.onPerformLocalSearch(query);
                    mCallback.onSearch(query);
                }
            }, 100);
        }
    }

    public TextWatcher onSearchTextChange() {
        return new DelayWatcher(true) {
            @Override
            public void onTextChanged(String text) {
                mCallback.onSearch(text);
            }

            @Override
            public void onTextChangedNoDelay(String text) {
                mCallback.onPerformLocalSearch(text);
            }

            @Override
            public void onTyping(String text) {
                if (!TextUtils.isEmpty(text)) {
                    if (!mIsShowClearIcon.get()) {
                        mIsShowClearIcon.set(true);
                        mCallback.onReplaceFragment();
                        mElevation.set(getContext().getResources().getDimensionPixelSize(R.dimen.space_tiny));
                    }
                    mCallback.showLoading();
                } else {
                    if (mIsShowClearIcon.get()) {
                        mIsShowClearIcon.set(false);
                        mCallback.onRemoveFragment();
                        mElevation.set(getContext().getResources().getDimensionPixelSize(R.dimen.space_small));
                    }
                }
            }
        };
    }

    public void onClearText() {
        if (!TextUtils.isEmpty(mSearchText.get())) {
            mSearchText.set("");
        }
    }

    public void onBackPress() {
        if (getContext() instanceof SearchMessageActivity) {
            ((SearchMessageActivity) getContext()).onBackPressed();
        }
    }

    public interface Callback {
        void onReplaceFragment();

        void onRemoveFragment();

        void onSearch(String text);

        void onPerformLocalSearch(String text);

        void showLoading();
    }
}
