package com.proapp.sompom.viewmodel;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.fragment.app.FragmentManager;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.NetworkBroadcastReceiver;
import com.proapp.sompom.database.CurrencyDb;
import com.proapp.sompom.helper.CheckPermissionCallbackHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.FacebookSMSValidation;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.helper.upload.ProfileUploader;
import com.proapp.sompom.intent.ConfirmCodeIntent;
import com.proapp.sompom.intent.SelectAddressIntent;
import com.proapp.sompom.intent.SelectAddressIntentResult;
import com.proapp.sompom.intent.ValidatePhoneNumberIntent;
import com.proapp.sompom.intent.WalletHistoryIntent;
import com.proapp.sompom.intent.newintent.ConciergeMyAddressIntent;
import com.proapp.sompom.intent.newintent.ConciergeOrderHistoryContainerIntent;
import com.proapp.sompom.intent.newintent.MyCameraIntent;
import com.proapp.sompom.intent.newintent.MyCameraResultIntent;
import com.proapp.sompom.listener.BaseAppSettingUI;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnEditProfileListener;
import com.proapp.sompom.listener.OnSpannableClickListener;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.model.concierge.ConciergeShopSetting;
import com.proapp.sompom.model.emun.AuthType;
import com.proapp.sompom.model.emun.ChangePasswordActionType;
import com.proapp.sompom.model.emun.StoreStatus;
import com.proapp.sompom.model.result.AppSetting;
import com.proapp.sompom.model.result.CheckPhoneExistResponse;
import com.proapp.sompom.model.result.NotificationSettingModel;
import com.proapp.sompom.model.result.Theme;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.EditProfileActivity;
import com.proapp.sompom.newui.dialog.ChangeLanguageDialog;
import com.proapp.sompom.newui.dialog.ChangePasswordDialog;
import com.proapp.sompom.newui.dialog.CurrencyDialog;
import com.proapp.sompom.newui.dialog.EditUserInfoDialog;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.newui.dialog.ThemeDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.StoreDataManager;
import com.proapp.sompom.utils.IntentUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.widget.ImageProfileLayout;
import com.resourcemanager.helper.FontHelper;
import com.resourcemanager.helper.LocaleManager;
import com.sompom.baseactivity.ResultCallback;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/13/18.
 */

public class EditProfileFragmentViewModel extends LayoutToolbarEditProfileViewModel implements BaseAppSettingUI {

    private static final int ERROR_GET_DATA = 0X001;
    private final ObservableField<String> mWallet = new ObservableField<>("");
    private final ObservableField<String> mFullName = new ObservableField<>("");
    public final ObservableField<String> mStoreName = new ObservableField<>("");
    private final ObservableField<String> mUserId = new ObservableField<>("");
    public final ObservableField<String> mFirstName = new ObservableField<>("");
    public final ObservableField<String> mLastName = new ObservableField<>("");
    public final ObservableField<String> mEmail = new ObservableField<>("");
    public final ObservableBoolean mIsEnablePhoneClick = new ObservableBoolean();
    public final ObservableField<String> mPhone = new ObservableField<>();
    public final ObservableField<String> mCover = new ObservableField<>();
    public final ObservableField<String> mAddress = new ObservableField<>();
    public final ObservableField<String> mCurrency = new ObservableField<>();
    public final ObservableField<String> mLanguage = new ObservableField<>();
    public final ObservableField<String> mVersionName = new ObservableField<>();
    public final ObservableField<AppTheme> mTheme = new ObservableField<>();
    private final ObservableField<String> mCompanyLogo = new ObservableField<>();
    private ObservableField<String> mPreviewProfileAvatar = new ObservableField<>();
    private boolean mIsShopEnabled;
    private ObservableInt mShopFeatureVisibility = new ObservableInt(View.GONE);

    public final OnSpannableClickListener mSpannableClickListener;
    private final StoreDataManager mStoreDataManager;
    private final OnEditProfileListener mOnCallbackListener;
    private final FragmentManager mFragmentManager;
    private boolean mCheckPasswordVisibility;
    private boolean mCheckThemeVisibility;
    private final ObservableField<User> mUser = new ObservableField<>();
    private int mErrorType = ERROR_GET_DATA;
    private SearchAddressResult mSearchAddressResult = new SearchAddressResult();
    private FacebookSMSValidation mSMSValidationUtil;
    private int mLanguageSelectedPosition;
    private boolean mIsLoadedData;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    private OnCallbackListener mUpdateCallback = new OnCallbackListener<Response<User>>() {
        @Override
        public void onFail(ErrorThrowable ex) {
            hideLoading();
            if (ex.getCode() == ErrorThrowable.AdditionalError.UPDATE_EXIST_EMAIL.getCode()) {
                showSnackBar(R.string.error_email_is_already_used, true);
            } else if (ex.getCode() == ErrorThrowable.AdditionalError.UPDATE_EXIST_PHONE.getCode()) {
                showSnackBar(R.string.error_phone_social_already_used, true);
            } else if (ex.getCode() == ErrorThrowable.AdditionalError.UPDATE_EXIST_STORE_NAME.getCode()) {
                showSnackBar(R.string.error_store_already_used, true);
            } else if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                showSnackBar(R.string.error_internet_connection_description, true);
            } else {
                showSnackBar(R.string.error_cannot_update_user, true);
            }
        }

        @Override
        public void onComplete(Response<User> result) {
            Timber.i("onComplete: " + new Gson().toJson(result.body()));
            showSnackBar(R.string.profile_user_popup_update_profile_success_description, false);
//            CurrencyDb.updateSelectCurrency(getContext(), mCurrency.get());
            setUser(result.body());
            mIsShowButtonSave.set(false);
            hideLoading();
        }
    };

    public EditProfileFragmentViewModel(StoreDataManager storeDataManager,
                                        FragmentManager fragmentManager,
                                        OnEditProfileListener onCallbackListener,
                                        OnSpannableClickListener onSpannableClickListener,
                                        boolean checkPasswordVisibility,
                                        boolean checkThemeVisibility,
                                        boolean isEnableShop) {
        super(storeDataManager.getContext());
        mStoreDataManager = storeDataManager;
        mOnCallbackListener = onCallbackListener;
        mSpannableClickListener = onSpannableClickListener;
        mTheme.set(ThemeManager.getAppTheme(getContext()));
        mCheckPasswordVisibility = checkPasswordVisibility;
        mCheckThemeVisibility = checkThemeVisibility;
        mFragmentManager = fragmentManager;
        getMyUserProfile();
        mLanguageSelectedPosition = LocaleManager.selectLanguagePosition(getContext());
        setLanguage();
        mIsShopEnabled = isEnableShop;
        mShopFeatureVisibility.set(mIsShopEnabled ? View.VISIBLE : View.GONE);

        mIsEnablePhoneClick.set(CurrencyDb.getCurrencies(getContext()).size() > 1);
        setToolbarText(getContext().getString(R.string.profile_user_title));
        loadAppVersionInfo();
    }

    private void loadAppVersionInfo() {
        try {
            PackageInfo pInfo = getContext().getPackageManager().getPackageInfo(getContext().getOpPackageName(), 0);
            String builder = pInfo.versionName +
                    " " +
                    "(" +
                    pInfo.versionCode +
                    ")";
            mVersionName.set(builder);
        } catch (PackageManager.NameNotFoundException e) {
            Timber.e(e);
        }
    }

    public ObservableField<String> getFullName() {
        return mFullName;
    }

    public ObservableField<String> getWallet() {
        return mWallet;
    }

    public ObservableInt getShopFeatureVisibility() {
        return mShopFeatureVisibility;
    }

    public ObservableField<String> getPreviewProfileAvatar() {
        return mPreviewProfileAvatar;
    }

    public ObservableField<User> getUser() {
        return mUser;
    }

    public ObservableField<String> getVersionName() {
        return mVersionName;
    }

    public ObservableField<String> getUserId() {
        return mUserId;
    }

    public ObservableField<String> getCompanyLogo() {
        return mCompanyLogo;
    }

    private String getUserPhoneNumber() {
        String countryCode = SharedPrefUtils.getString(mContext, SharedPrefUtils.COUNTRY_CODE);
        String phone = SharedPrefUtils.getPhone(mContext);
        if (!TextUtils.isEmpty(countryCode) && !TextUtils.isEmpty(phone)) {
            return "+" + countryCode + phone;
        }

        return null;
    }

    public void updateUserWallet(double wallet) {
        mWallet.set(ConciergeHelper.getDisplayPrice(mContext, wallet));
        mStoreDataManager.setUserWallet(wallet);
    }

    @Override
    public void onSaveButtonClick() {
        showLoading(true);
        User user = mUser.get();
        user.setStoreName(mStoreName.get());
        user.setFirstName(mFirstName.get());
        user.setLastName(mLastName.get());
        user.setEmail(mEmail.get());
//        user.setPhone(mPhone.get());
        user.setCity(mAddress.get());
        user.setCurrency(mCurrency.get());
        if (mSearchAddressResult != null) {
            if (mSearchAddressResult.getLocations() != null && !mSearchAddressResult.getLocations().isEmpty()) {
                user.setLatitude(mSearchAddressResult.getLocations().getLatitude());
                user.setLongitude(mSearchAddressResult.getLocations().getLongitude());
                if (!TextUtils.isEmpty(mSearchAddressResult.getLocations().getCountry())) {
                    user.setCountry(mSearchAddressResult.getLocations().getCountry());
                }
            }
            user.setAddress(mSearchAddressResult.getFullAddress());
        }

        Timber.i("User profile: " + user.getUserProfileThumbnail());
        Observable<ProfileUploader.UploadResult> observable = null;
        if (!TextUtils.isEmpty(user.getUserProfileThumbnail()) && !user.getUserProfileThumbnail().startsWith("http")) {
            observable = ProfileUploader.uploadProfileImage(getContext(), user.getUserProfileThumbnail())
                    .concatMap(uploadResult -> {
                        if (uploadResult == null) {
                            uploadResult = new ProfileUploader.UploadResult();
                        }
                        user.setUserProfileThumbnail(uploadResult.getThumbnail());
                        user.setOriginalUserProfile(uploadResult.getUrl());
                        return Observable.just(uploadResult);
                    });
        }
        if (!TextUtils.isEmpty(user.getUserCoverProfile()) && !user.getUserCoverProfile().startsWith("http")) {
            if (observable == null) {
                observable = ProfileUploader.uploadCoverImage(getContext(), user.getUserCoverProfile())
                        .concatMap(uploadResult -> {
                            if (uploadResult == null) {
                                uploadResult = new ProfileUploader.UploadResult();
                            }

                            user.setUserCoverProfileThumbnail(uploadResult.getThumbnail());
                            user.setUserCoverProfile(uploadResult.getUrl());
                            return Observable.just(uploadResult);
                        });
            } else {
                observable = observable.concatMap(uploadResult -> ProfileUploader.uploadCoverImage(getContext(), user.getUserCoverProfile())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()));
            }
        }
        Observable<Response<User>> call;
        if (observable != null) {
            call = observable.concatMap(uploadResult -> mStoreDataManager.updateMyProfile(user)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()));
        } else {
            call = mStoreDataManager.updateMyProfile(user);
        }
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(getContext(), call, true);
        Disposable disposable = helper.execute(mUpdateCallback);
        addDisposable(disposable);
    }

    private void updateUser(User userToUpdate, OnCallbackListener<Response<User>> callbackListener) {
        Observable<Response<User>> observable = mStoreDataManager.updateMyProfile(userToUpdate);
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(getContext(),
                observable, true);
        addDisposable(helper.execute(callbackListener));
    }

    private void checkToUpdatePhoneNumber(User userToUpdate, OnCallbackListener<Response<User>> callbackListener) {
        Observable<Response<CheckPhoneExistResponse>> observable = mStoreDataManager.checkIfPhoneNumberExisted(userToUpdate.getCountryCode(),
                userToUpdate.getPhone());
        ResponseObserverHelper<Response<CheckPhoneExistResponse>> helper = new ResponseObserverHelper<>(getContext(),
                observable, true);
        addDisposable(helper.execute(new OnCallbackListener<Response<CheckPhoneExistResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<CheckPhoneExistResponse> result) {
                if (result.body().isError()) {
                    hideLoading();
                    showSnackBar(result.body().getErrorMessage(), true);
                } else {
                    if (result.body().isExited()) {
                        hideLoading();
                        showSnackBar(mContext.getString(R.string.profile_user_change_phone_error), true);
                    } else {
                        updateUser(userToUpdate, callbackListener);
                    }
                }
            }
        }));
    }


    public void checkShopSetting(CheckShopSettingListener listener) {
        Observable<Response<ConciergeShopSetting>> observable = mStoreDataManager.getShopSettingService();
        ResponseObserverHelper<Response<ConciergeShopSetting>> helper = new ResponseObserverHelper<>(getContext(),
                observable, true);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeShopSetting>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (listener != null) {
                    listener.onCheckFinished();
                }
            }

            @Override
            public void onComplete(Response<ConciergeShopSetting> result) {
                if (listener != null) {
                    listener.onCheckFinished();
                }
            }
        }));
    }

    public void onCountryClick() {
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).startActivityForResult(new SelectAddressIntent(getContext(), mSearchAddressResult),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            SelectAddressIntentResult result = new SelectAddressIntentResult(data);
                            SearchAddressResult value = result.getResult();
                            mIsShowButtonSave.set(!TextUtils.equals(value.getFullAddress(), mSearchAddressResult.getFullAddress()));
                            mSearchAddressResult = value;
                            String city = mSearchAddressResult.getCountry();
                            if (TextUtils.isEmpty(city)) {
                                city = mSearchAddressResult.getFullAddress();
                            }
                            mAddress.set(city);
                            mIsShowButtonSave.set(true);
                        }
                    });
        }
    }

    @Override
    public void onRetryClick() {
        getMyUserProfile();
    }

    public void onChangeCoverClick() {
        checkCameraPermission(PhotoType.COVER);
    }

    public void onChangeProfileClick() {
        checkCameraPermission(PhotoType.PROFILE);
    }

    private void checkCameraPermission(PhotoType photoType) {
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).checkPermission(
                    new CheckPermissionCallbackHelper((AbsBaseActivity) getContext(),
                            CheckPermissionCallbackHelper.Type.CAMERA) {
                        @Override
                        public void onPermissionGranted() {
                            launchCamera(photoType);
                        }

                    }, Manifest.permission.CAMERA);
        }
    }

    private void launchCamera(PhotoType photoType) {
        Intent intent = MyCameraIntent.newProfileInstance(getContext());
        ((AbsBaseActivity) getContext()).startActivityForResult(intent, new ResultCallback() {
            @Override
            public void onActivityResultSuccess(int resultCode, Intent data) {
                MyCameraResultIntent resultIntent = new MyCameraResultIntent(data);
                for (Media productMedia : resultIntent.getMedia()) {
                    if (photoType == PhotoType.PROFILE) {
                        mPreviewProfileAvatar.set(productMedia.getUrl());
                        mUser.get().setUserProfileThumbnail(productMedia.getUrl());
                    } else {
                        mCover.set(productMedia.getUrl());
                        mUser.get().setUserCoverProfile(productMedia.getUrl());
                    }
                }
                mIsShowButtonSave.set(true);
            }
        });
    }

    private void getMyUserProfile() {
        showLoading();
        Observable<Response<User>> call = mStoreDataManager.getMyUserProfile2();
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<Response<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                showError(ex.toString());
            }

            @Override
            public void onComplete(Response<User> result) {
                updateData(result.body());
            }

            private void updateData(User user) {
                mIsLoadedData = true;
                mWallet.set(ConciergeHelper.getDisplayPrice(mContext, mStoreDataManager.getUserWallet()));
                setUser(user);
                hideLoading();
                if (mOnCallbackListener != null) {
                    if (user.getNotificationSettingModel() != null) {
                        mOnCallbackListener.onComplete(user.getNotificationSettingModel());
                    } else {
                        //Try to get with local.
                        NotificationSettingModel notificationSetting = SharedPrefUtils.getNotificationSetting(getContext());
                        if (notificationSetting != null) {
                            mOnCallbackListener.onComplete(notificationSetting);
                        }
                    }
                }
            }
        }));
    }

    private void setUser(User user) {
        SharedPrefUtils.setUserValue(user, getContext());
        mFullName.set(user.getFullName());
        mStoreName.set(user.getStoreName());
        mUserId.set(user.getId());
        mEmail.set(user.getEmail());
        mPhone.set(getUserPhoneNumber());
        mAddress.set(user.getCountryName());
        mCurrency.set(user.getCurrency());
        if (TextUtils.isEmpty(mCurrency.get())) {
            mCurrency.set(CurrencyDb.getSelectedCurrency(getContext()).getName());
        }
        bindUserCover();
        Timber.i("setUser: " + new Gson().toJson(user));
        mUser.set(user);
    }

    public void reloadUserProfilePhoto() {
        User user = mUser.get();
        if (user != null) {
            Timber.i("reloadUserProfilePhoto");
            user.setUserProfileThumbnail(SharedPrefUtils.getImageProfileUrl(getContext()));
            mUser.set(user);
        }
    }

    private void bindUserCover() {
        AppSetting appSetting = SharedPrefUtils.getAppSetting(getContext());

        //Bind cover
//        SynchroniseData synchroniseData = LicenseSynchronizationDb.readSynchroniseData(getContext());

        //TODO: Need to remove this test
//        if (synchroniseData != null && synchroniseData.getProfile() != null) {
//            synchroniseData.getProfile().setEnableCustomCover(true);
//        }
//
//        if (synchroniseData != null &&
//                synchroniseData.getProfile() != null &&
//                synchroniseData.getProfile().isEnableCustomCover() &&
//                !TextUtils.isEmpty(mUser.getUserCoverProfile())) {
//            mCover.set(mUser.getUserCoverProfile());
//        } else if (appSetting != null) {
//            mCover.set(appSetting.getBackgroundCover());
//        }
        if (appSetting != null) {
            mCover.set(appSetting.getBackgroundCover());
        }
        //Bind logo
        Theme theme = UserHelper.getSelectedThemeFromAppSetting(getContext());
        if (theme != null) {
            mCompanyLogo.set(theme.getCompanyLogo());
        }
    }

    public void onEditPhoneNumberClicked() {
        ((AbsBaseActivity) mContext).startActivityForResult(new ValidatePhoneNumberIntent(mContext,
                        AuthType.CHANGE_PHONE),
                new ResultCallback() {
                    @Override
                    public void onActivityResultSuccess(int resultCode, Intent data) {
                        if (resultCode == AppCompatActivity.RESULT_OK) {
                            ExchangeAuthData exchangeAuthData = data.getParcelableExtra(ConfirmCodeIntent.DATA);
                            Timber.i("onActivityResultSuccess: resultCode: " + resultCode + ", Data: " + new Gson().toJson(exchangeAuthData));
                            if (exchangeAuthData != null) {
                                showLoading(true);
                                User user = new User();
                                user.setCountryCode(exchangeAuthData.getCountyCode());
                                user.setPhone(exchangeAuthData.getPhoneNumber());
                                checkToUpdatePhoneNumber(user, new OnCallbackListener<Response<User>>() {
                                    @Override
                                    public void onFail(ErrorThrowable ex) {
                                        hideLoading();
                                        showSnackBar(ex.getMessage(), true);
                                    }

                                    @Override
                                    public void onComplete(Response<User> result) {
                                        if (result.body().isResponseError()) {
                                            showSnackBar(result.body().getErrorMessage(), true);
                                        } else {
                                            mUser.get().setCountryCode(exchangeAuthData.getCountyCode());
                                            mUser.get().setPhone(exchangeAuthData.getPhoneNumber());
                                            SharedPrefUtils.setPhone(exchangeAuthData.getPhoneNumber(), mContext);
                                            SharedPrefUtils.setString(mContext, SharedPrefUtils.COUNTRY_CODE, exchangeAuthData.getCountyCode());
                                            mPhone.set(getUserPhoneNumber());
                                            hideLoading();
                                            showSnackBar(R.string.profile_user_popup_update_profile_success_description,
                                                    false);
                                        }
                                    }
                                });
                            }
                        }
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mSMSValidationUtil != null) {
            mSMSValidationUtil.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void onCurrencyClick() {
        if (!mIsEnablePhoneClick.get()) {
            return;
        }
        CurrencyDialog dialog = new CurrencyDialog();
        dialog.setCurrency(mCurrency.get());
        dialog.setOnCurrencyChooseListener(currency -> {
            if (!TextUtils.equals(currency, mCurrency.get())) {
                mCurrency.set(currency);
                mIsShowButtonSave.set(true);
            }
        });
        dialog.show(mFragmentManager);
    }

    public void onThemeClick() {
        if (mCheckThemeVisibility) {
            ThemeDialog dialog = new ThemeDialog();
            dialog.setOnThemeChooseListener(appTheme -> {
                if (mTheme.get() != appTheme) {
                    ThemeManager.setAppTheme(getContext(), appTheme);

                    mTheme.set(appTheme);
                    mIsShowButtonSave.set(true);

                    if (getContext() instanceof EditProfileActivity) {
                        ((EditProfileActivity) getContext()).recreate();
                    }
                }
            });
            dialog.show(mFragmentManager);
        }
    }

    public void onChangeLanguageClick() {
        ChangeLanguageDialog dialog = new ChangeLanguageDialog();
        dialog.setLanguages(getLanguage());
        dialog.setPosition(mLanguageSelectedPosition);
        dialog.setOnLanguageChooseListener(position -> {
            mLanguageSelectedPosition = position;
            setLanguage();
            String[] iso3SupportLanguage = getContext().getResources().getStringArray(R.array.support_lang_iso3);
            String localString = iso3SupportLanguage[mLanguageSelectedPosition];
            if (!TextUtils.equals(localString, SharedPrefUtils.getLanguage(getContext())) && getContext() instanceof EditProfileActivity) {
                ((EditProfileActivity) getContext()).recreateFragment(localString);
            }
        });
        dialog.show(mFragmentManager);
    }

    private String[] getLanguage() {
        return getContext().getResources().getStringArray(R.array.change_language_support_language);
    }

    private void setLanguage() {
        mLanguage.set(getLanguage()[mLanguageSelectedPosition]);
    }

    public void pushNotificationSetting(boolean disableAllNotification) {
        NotificationSettingModel notificationSettingModel = SharedPrefUtils.getNotificationSetting(getContext());
        notificationSettingModel.setDisableAllNotification(disableAllNotification);

        Observable<Response<User>> call = mStoreDataManager.updateNotification(notificationSettingModel);
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<Response<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                showSnackBar(R.string.setting_popup_disable_notification_error_description, true);
            }

            @Override
            public void onComplete(Response<User> result) {
                if (result.body() != null) {
                    SharedPrefUtils.setNotificationSetting(getContext(), result.body().getNotificationSettingModel());
                }
            }
        }));
    }

    public void changeMyStoreStatus(StoreStatus status, OnCallbackListener<Response<User>> listener) {
        Observable<Response<User>> call = mStoreDataManager.changeMyStoreStatus(status);
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(listener));
    }

    public void onStoreNameClick() {
//        showInputDialog(R.string.edit_profile_store_name,
//                R.string.edit_profile_enter_store_name_hint,
//                mStoreName.get(), InputMessageDialog.Validation.Store, mStoreName::set);
    }

    public void onEditFirstNameClicked() {
        showInputDialog();
    }


    public void onEditEmailClicked() {
        showInputDialog();
    }

    public void onEditLastNameClicked() {
        showInputDialog();
    }

    public void onEditPasswordClicked() {
        if (mCheckPasswordVisibility) {
            ChangePasswordDialog changePasswordDialog = ChangePasswordDialog.newInstance(ChangePasswordActionType.CHANGE_PASSWORD,
                    null);
            changePasswordDialog.setCancelable(false);
            changePasswordDialog.setListener(new ChangePasswordDialog.ChangePasswordDialogListener() {
                @Override
                public void onChangePasswordSuccess(ChangePasswordActionType type) {
                    MessageDialog messageDialog = MessageDialog.newInstance();
                    messageDialog.setCancelable(false);
                    messageDialog.setTitle(mContext.getString(R.string.change_password_title));
                    messageDialog.setMessage(mContext.getString(R.string.change_password_success_message));
                    messageDialog.setLeftText(mContext.getString(R.string.popup_ok_button), null);
                    messageDialog.show(((AbsBaseActivity) mContext).getSupportFragmentManager());
                }

                @Override
                public void onDismissed() {
                    hideLoading();
                }
            });
            changePasswordDialog.show(mFragmentManager);
        }
    }

    public void onLogoutClick() {
        mOnCallbackListener.onLogoutClick();
    }

    public void onAddressesClick() {
        if (mIsShopEnabled) {
            mContext.startActivity(new ConciergeMyAddressIntent(mContext));
        }
    }

    public void onWalletHistoryClick() {
        getContext().startActivity(new WalletHistoryIntent(getContext()));
    }

    public void onContactUsClick() {
        openExternalLink("https://delishop.asia/contact");
    }

    public void onAboutUsClick() {
        openExternalLink("https://delishop.asia/about");
    }

    public void onTermConditionClick() {
        openExternalLink("https://delishop.asia/terms");
    }

    public void onPaymentClick() {
        openExternalLink("https://delishop.asia/payments");
    }

    public void onDeliveryInformationClick() {
        openExternalLink("https://delishop.asia/delivery");
    }

    public void onFAQClick() {
        openExternalLink("https://delishop.asia/help");
    }

    public void onRefundPolicyClick() {
        openExternalLink("https://delishop.asia/refund-policies");
    }

    public void onOrderClick() {
        if (mIsShopEnabled) {
            mContext.startActivity(new ConciergeOrderHistoryContainerIntent(mContext, true));
        }
    }

    private void openExternalLink(String link) {
        IntentUtil.deepLinkIntent(getContext(), link + "?_locale=" + LocaleManager.getAppLanguage(getContext()));
    }

    private void showInputDialog() {
        EditUserInfoDialog editUserInfoDialog = EditUserInfoDialog.newInstance(() -> {
            showSnackBar(R.string.profile_user_popup_update_profile_success_description,
                    false);
            mUser.get().setFirstName(mFirstName.get());
            mUser.get().setLastName(mLastName.get());
            mUser.get().setEmail(mEmail.get());
            mEmail.set(SharedPrefUtils.getUserEmail(mContext));
            User user = SharedPrefUtils.getUser(mContext);
            mFullName.set(user.getFullName());
        });
        editUserInfoDialog.show(mFragmentManager, EditUserInfoDialog.class.getName());
    }

    public interface CheckShopSettingListener {
        void onCheckFinished();
    }

    public enum PhotoType {
        PROFILE, COVER
    }

    @BindingAdapter("enableBoldFont")
    public static void enableBoldFont(SwitchCompat switchCompat, boolean enable) {
        if (enable) {
            switchCompat.setTypeface(FontHelper.getBoldFontStyle(switchCompat.getContext()));
        }
    }

    @BindingAdapter("previewUrl")
    public static void enableBoldFont(ImageProfileLayout imageProfileLayout, String previewUrl) {
        if (!TextUtils.isEmpty(previewUrl)) {
            imageProfileLayout.loadPreviewUrl(previewUrl);
        }
    }
}
