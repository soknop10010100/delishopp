package com.proapp.sompom.viewmodel.newviewmodel;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.fragment.app.FragmentManager;

import com.proapp.sompom.R;
import com.proapp.sompom.chat.listener.LiveUserDataListener;
import com.proapp.sompom.chat.service.LiveUserDataService;
import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.helper.upload.AbsUploader;
import com.proapp.sompom.intent.newintent.CallIntent;
import com.proapp.sompom.intent.newintent.GroupDetailMediaContainerIntent;
import com.proapp.sompom.intent.newintent.MyCameraIntent;
import com.proapp.sompom.intent.newintent.MyCameraResultIntent;
import com.proapp.sompom.intent.newintent.SearchConversationIntent;
import com.proapp.sompom.listener.BaseAppSettingUI;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.Presence;
import com.proapp.sompom.model.result.AppSetting;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.Theme;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.ChangeGroupNameDialog;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.GroupDetailDataManager;
import com.proapp.sompom.utils.DateTimeUtils;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.LayoutToolbarGroupDetailViewModel;
import com.sompom.baseactivity.ResultCallback;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 4/29/20.
 */
public class GroupDetailFragmentViewModel extends LayoutToolbarGroupDetailViewModel implements BaseAppSettingUI {

    public final ObservableInt mVisibleActive = new ObservableInt(View.GONE);
    public final ObservableField<String> mLastActivity = new ObservableField<>();
    private final SocketService.SocketBinder mSocketBinder;
    public ObservableField<String> mCover = new ObservableField<>();
    public ObservableField<String> mGroupName = new ObservableField<>();
    public ObservableField<String> mGroupImageUrl = new ObservableField<>();
    public ObservableField<String> mMembersCount = new ObservableField<>();
    public ObservableField<String> mOwnerCount = new ObservableField<>();
    public ObservableField<String> mPhotoAndVideoCount = new ObservableField<>();
    public ObservableField<String> mFileCount = new ObservableField<>();
    public ObservableField<String> mLinkCount = new ObservableField<>();
    public ObservableField<String> mVoiceCount = new ObservableField<>();
    private ObservableField<List<User>> mParticipants = new ObservableField<>();
    private final ObservableField<String> mCompanyLogo = new ObservableField<>();
    private User mTrackActiveStatusUser;
    private boolean hasOnlineMember;
    private Conversation mConversation;
    private Activity mActivity;
    private FragmentManager mFragmentManager;
    private String mConversationId;
    private User mRecipient;
    private GroupDetailDataManager mDataManager;
    private OnCallback mCallBackListener;
    private LiveUserDataListener mLiveUserDataListener;
    private GroupDetail mGroupDetail;

    private ObservableInt mGroupOwnerCounter = new ObservableInt();
    private ObservableInt mGroupParticipantCounter = new ObservableInt();

    public GroupDetailFragmentViewModel(Activity activity,
                                        FragmentManager fragmentManager,
                                        Conversation conversation,
                                        User recipient,
                                        GroupDetailDataManager dataManager,
                                        SocketService.SocketBinder binder,
                                        OnCallback callback,
                                        SupportCallViewModelCallback supportCallViewModelCallback) {
        super(dataManager.getContext(), supportCallViewModelCallback);
        mActivity = activity;
        mFragmentManager = fragmentManager;
        mConversation = conversation;
        mConversationId = mConversation.getGroupId();
        mRecipient = recipient;
        mDataManager = dataManager;
        mCallBackListener = callback;
        showLoading();
        mSocketBinder = binder;
        getData();
        initPresenceListener();
        if (mSocketBinder != null) {
            mSocketBinder.addLiveUserDataListener(mLiveUserDataListener);
        }
    }

    public String getConversationId() {
        return mConversationId;
    }

    public ObservableField<String> getCompanyLogo() {
        return mCompanyLogo;
    }

    public ObservableField<List<User>> getParticipants() {
        return mParticipants;
    }

    public ObservableInt getGroupOwnerCounter() {
        return mGroupOwnerCounter;
    }

    public ObservableInt getGroupParticipantCounter() {
        return mGroupParticipantCounter;
    }

    public void bindGroupOwnerCounter(int counter) {
        mGroupOwnerCounter.set(counter);
        String ownerCount = String.valueOf(counter);
        if (mGroupOwnerCounter.get() > 1) {
            ownerCount = ownerCount + " " + getContext().getResources().getString(R.string.group_title_multiple_owner);
        } else {
            ownerCount = ownerCount + " " + getContext().getResources().getString(R.string.group_title_owner);
        }
        mOwnerCount.set(ownerCount);
    }

    public void bindGroupParticipantCounter(int counter) {
        mGroupParticipantCounter.set(counter);
        String memberCount = String.valueOf(counter);
        if (counter > 1) {
            memberCount = memberCount + " " + getContext().getResources().getString(R.string.group_title_multiple_member);
        } else {
            memberCount = memberCount + " " + getContext().getResources().getString(R.string.group_title_member);
        }
        mMembersCount.set(memberCount);
    }

    public void onGroupParticipantRemoved() {
        if (mGroupParticipantCounter.get() > 0) {
            int count = mGroupParticipantCounter.get() - 1;
            mGroupParticipantCounter.set(count);
        }
        bindGroupParticipantCounter(mGroupParticipantCounter.get());
    }

    public void onGroupOwnerRemoved() {
        if (mGroupOwnerCounter.get() > 0) {
            int count = mGroupOwnerCounter.get() - 1;
            mGroupOwnerCounter.set(count);
        }
        bindGroupOwnerCounter(mGroupOwnerCounter.get());
    }

    private void updateGroupMediaCounter(GroupDetail groupDetail) {
        if (groupDetail.getGroupMediaCount() != null) {
            mPhotoAndVideoCount.set(String.valueOf(groupDetail.getGroupMediaCount().getPhotoAndVideoCount()));
            mFileCount.set(String.valueOf(groupDetail.getGroupMediaCount().getFileCount()));
            mLinkCount.set(String.valueOf(groupDetail.getGroupMediaCount().getLinkCount()));
            mVoiceCount.set(String.valueOf(groupDetail.getGroupMediaCount().getAudioCount()));
        }
    }

    public ObservableField<String> getPhotoAndVideoCount() {
        return mPhotoAndVideoCount;
    }

    public ObservableField<String> getFileCount() {
        return mFileCount;
    }

    public ObservableField<String> getLinkCount() {
        return mLinkCount;
    }

    public ObservableField<String> getVoiceCount() {
        return mVoiceCount;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Timber.e("onDestroy");
        mSocketBinder.removeLiveUserDataListener(mLiveUserDataListener);
    }

    private void getData() {
        showLoading();
        getGroupDetail();
    }

    @Override
    public void onRetryClick() {
        getData();
    }

    @Override
    protected boolean isGroupConversation() {
        return mConversation != null && mConversation.isGroup();
    }

    @Override
    protected User getRecipient() {
        return mRecipient;
    }

    @Override
    protected Conversation getConversation() {
        return mConversation;
    }

    @Override
    protected CallIntent.StartFromType getStartCallScreenType() {
        return CallIntent.StartFromType.GROUP_DETAIL;
    }

    private void getGroupDetail() {
        showLoading();
        Observable<Response<GroupDetail>> observable = mDataManager.getGroupDetail();
        ResponseObserverHelper<Response<GroupDetail>> helper = new ResponseObserverHelper<>(mActivity, observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<GroupDetail>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.i("getGroupDetail Failed: " + ex.getMessage());
                hideLoading();
                showError(ex.getMessage());
            }

            @Override
            public void onComplete(Response<GroupDetail> result) {
                updateGroupMediaCounter(result.body());
                mGroupDetail = result.body();
                setConfigOptionVisibility((isCurrentUserIsGroupOwner() || isCurrentUserIsGroupCreator()) ? View.VISIBLE : View.GONE);
                bindActiveStatus();
                mCallBackListener.onLoadGroupDetailSuccess(result.body());
                hideLoading();
            }
        }));
    }

    public boolean isCurrentUserIsGroupOwner() {
        if (mGroupDetail != null && mGroupDetail.getGroupOwnerList() != null) {
            String userId = SharedPrefUtils.getUserId(getContext());
            for (User user : mGroupDetail.getGroupOwnerList()) {
                if (TextUtils.equals(user.getId(), userId)) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean isCurrentUserIsGroupCreator() {
        if (mGroupDetail != null) {
            return TextUtils.equals(SharedPrefUtils.getUserId(getContext()), mGroupDetail.getCreatorId());
        }

        return false;
    }

    private void bindActiveStatus() {
        AppSetting appSetting = SharedPrefUtils.getAppSetting(getContext());
        if (appSetting != null) {
            mCover.set(appSetting.getBackgroundCover());
        }

        Theme theme = UserHelper.getSelectedThemeFromAppSetting(getContext());
        if (theme != null) {
            mCompanyLogo.set(theme.getCompanyLogo());
        }

        if (mGroupDetail.getParticipant() != null) {
            bindGroupProfile();
            bindGroupParticipantCounter(mParticipants.get().size());
            bindGroupOwnerCounter(mGroupDetail.getGroupOwnerList().size());
            mSocketBinder.getGroupLastActiveInfo(mConversation.getParticipants(),
                    new LiveUserDataService.LiveUserDataServiceCallback() {
                        @Override
                        protected void onGotGroupLastActiveInfo(Date lastActiveDate, User lastActiveUser) {
                            mTrackActiveStatusUser = lastActiveUser;
                            if (mTrackActiveStatusUser != null) {
                                Timber.i("mTrackActiveStatusUser: " + mTrackActiveStatusUser.getFullName());
                                if (mTrackActiveStatusUser.isOnline() != null && mTrackActiveStatusUser.isOnline()) {
                                    mVisibleActive.set(View.VISIBLE);
                                    mLastActivity.set(getContext().getString(R.string.chat_header_active_status));
                                }
                            }
                        }
                    });
        }
    }

    public void bindGroupProfile() {
        Timber.i("mGroupDetail.getParticipant(): " + mGroupDetail.getParticipant().size());
        mParticipants.set(new ArrayList<>(mGroupDetail.getParticipant()));
        mGroupName.set(mGroupDetail.getName());
        mGroupImageUrl.set(mGroupDetail.getGroupPhoto());
    }

    private void initPresenceListener() {
        mLiveUserDataListener = new LiveUserDataListener() {
            @Override
            public void onPresenceUpdate(String userId, Presence presence, Date lastOnlineTime) {
                Timber.i("onPresenceUpdate: userId: " + userId +
                        ", presence: " + presence +
                        ", lastOnlineTime: " + lastOnlineTime);
                if (presence == null) {
                    return;
                }

                if (presence == Presence.Online) {
                    mVisibleActive.set(View.VISIBLE);
                    mLastActivity.set(getContext().getString(R.string.chat_header_active_status));
                    return;
                }

                if (lastOnlineTime == null) {
                    mVisibleActive.set(View.VISIBLE);
                    mLastActivity.set(getContext().getString(R.string.chat_header_offline_status));
                } else {
                    mVisibleActive.set(View.VISIBLE);
                    mLastActivity.set(DateTimeUtils.getLastActiveRelativeTimeSpanDisplay(getContext(),
                            lastOnlineTime.getTime()));
                }
            }

            @Override
            public String getUserId() {
                if (mTrackActiveStatusUser != null) {
                    return mTrackActiveStatusUser.getId();
                }

                return null;
            }
        };
    }

    public void onMessageClick() {
        onBackButtonClick();
    }

    public void onShareJoinLinkClick() {

    }

    public void onScheduleCallClick() {

    }

    public void onSearchConversationClick() {
        mActivity.startActivity(new SearchConversationIntent(mActivity,
                mConversation,
                mGroupDetail));
    }

    public void onPhotoVideoClick() {
        openGroupDetailMedia(0);
    }

    private void openGroupDetailMedia(int selectIndex) {
        mActivity.startActivity(new GroupDetailMediaContainerIntent(mActivity, mGroupDetail, selectIndex));
    }

    public void onFileClick() {
        openGroupDetailMedia(1);
    }

    public void onLinkClick() {
        openGroupDetailMedia(2);
    }

    public void onVoiceMessageClick() {
        openGroupDetailMedia(3);
    }

    @Override
    public void onChangePhotoClick() {
        Timber.e("OnChangePhotoClick is stub");
        //Open photo chooser

        if (getContext() instanceof AbsBaseActivity) {
            Intent intent = MyCameraIntent.newGroupProfileInstance(getContext());
            ((AbsBaseActivity) getContext()).startActivityForResult(intent, new ResultCallback() {

                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    showLoading(true);
                    MyCameraResultIntent resultIntent = new MyCameraResultIntent(data);
                    ArrayList<Media> media = resultIntent.getMedia();

                    AbsUploader uploader = new AbsUploader(getContext(), media.get(0).getUrl(), AbsUploader.UploadType.Profile) {
                        @Override
                        public void onUploadSucceeded(String imageUrl, String thumb) {
                            GroupDetail groupDetail = new GroupDetail();
                            groupDetail.setGroupPhoto(thumb);

                            Observable<Response<GroupDetail>> observable = mDataManager.updateGroupDetail(groupDetail);
                            BaseObserverHelper<Response<GroupDetail>> helper = new BaseObserverHelper<>(getContext(), observable);
                            helper.execute(new OnCallbackListener<Response<GroupDetail>>() {
                                @Override
                                public void onFail(ErrorThrowable ex) {
                                    Timber.e("Something went wrong \n" + ex);
                                    hideLoading();
                                    showSnackBar(R.string.error_general_description, true);
                                }

                                @Override
                                public void onComplete(Response<GroupDetail> result) {
                                    hideLoading();
                                    if (result.isSuccessful() && result.body() != null) {
                                        showSnackBar(R.string.group_detail_update_success, true);
                                        mGroupImageUrl.set(result.body().getGroupPhoto());
                                        mConversation.setGroupImageUrl(result.body().getGroupPhoto());
                                        ConversationDb.save(getContext(), mConversation, "onChangePhotoClick");
                                        if (mCallBackListener != null) {
                                            mCallBackListener.onPhotoChangeSuccess(mConversation);
                                        }
                                        //Update local DB then callback
                                    }
                                }
                            });
                        }

                        @Override
                        public void onUploadFail(String id, String ex) {
                            Timber.e("Something went wrong \n" + ex);
                            hideLoading();
                        }
                    };
                    uploader.doUpload(null);
                }
            });
        }
    }

    public void removeParticipant(boolean isRemoveGroupOwner, String userId, int position) {
        showLoading(true);
        Observable<Response<GroupDetail>> observable = isRemoveGroupOwner ? mDataManager.removeGroupOwner(userId) :
                mDataManager.removeGroupParticipant(userId);
        BaseObserverHelper<Response<GroupDetail>> helper = new BaseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<GroupDetail>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<GroupDetail> result) {
                mGroupDetail.setParticipant(result.body().getParticipant());
                mGroupDetail.setGroupOwnerList(result.body().getGroupOwnerList());
                hideLoading();
                if (mCallBackListener != null) {
                    mCallBackListener.onRemoveParticipantSuccess(isRemoveGroupOwner, position);
                }
                bindGroupProfile();
            }
        }));
    }

    @Override
    public void onChangeNameClick() {
        ChangeGroupNameDialog dialog = new ChangeGroupNameDialog(mConversation);
        dialog.setmListener(groupDetail -> {
            Observable<Response<GroupDetail>> observable = mDataManager.updateGroupDetail(groupDetail);
            BaseObserverHelper<Response<GroupDetail>> helper = new BaseObserverHelper<>(getContext(), observable);
            helper.execute(new OnCallbackListener<Response<GroupDetail>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    Timber.e("Something went wrong \n" + ex);
                    hideLoading();
                    showSnackBar(R.string.error_general_description, true);
                }

                @Override
                public void onComplete(Response<GroupDetail> result) {
                    hideLoading();
                    if (result.isSuccessful() && result.body() != null) {
                        mConversation.setGroupName(result.body().getName());
                        showSnackBar(R.string.group_detail_update_success, true);
                        //Update local DB then callback
                        mGroupName.set(mConversation.getGroupName());
                        ConversationDb.save(getContext(), mConversation, "onChangeNameClick");
                        if (mCallBackListener != null) {
                            mCallBackListener.onNameChangeSuccess(mConversation);
                        }

                    }
                }
            });
        });
        dialog.show(mFragmentManager);
    }

    //Callback handling
    public interface OnCallback extends OnCallbackListListener<Conversation> {
        //Stuff to do in GroupDetailFragment
        void onReceivedCallEvent(AbsCallService.CallType callType,
                                 User recipient,
                                 boolean isMissedCall,
                                 int calledDuration);

        void onNameChangeSuccess(Conversation conversation);

        void onPhotoChangeSuccess(Conversation conversation);

        void onLoadGroupDetailSuccess(GroupDetail groupDetail);

        void onRemoveParticipantSuccess(boolean isRemoveGroupOwner, int position);
    }
}
