package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.view.View;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.model.concierge.ConciergeShopCategory;
import com.proapp.sompom.utils.AttributeConverter;

/**
 * Created by Veasna Chhom on 25/1/22.
 */
public class ListItemConciergeShopCategoryViewModel {

    private final ObservableField<String> mTitle = new ObservableField<>();
    private final ObservableField<String> mIcon = new ObservableField<>();
    private final ObservableField<String> mCategoryIcon = new ObservableField<>();
    private final ObservableInt mVerticalBarVisibility = new ObservableInt(View.GONE);
    private final ObservableBoolean mShouldDisplayCategoryIcon = new ObservableBoolean();
    private final ObservableInt mTitleColor = new ObservableInt();
    private final ObservableInt mIconColor = new ObservableInt();
    private final ConciergeShopCategory mConciergeShopCategory;
    private final Context mContext;

    public ListItemConciergeShopCategoryViewModel(Context context, ConciergeShopCategory conciergeShopCategory) {
        mContext = context;
        mConciergeShopCategory = conciergeShopCategory;
        mTitle.set(conciergeShopCategory.getTitle());
        if (conciergeShopCategory.isAllCategoryType()) {
            mShouldDisplayCategoryIcon.set(false);
            mIcon.set(mContext.getString(R.string.fi_rr_apps));
        } else {
            mShouldDisplayCategoryIcon.set(true);
            mCategoryIcon.set(conciergeShopCategory.getLogo());
        }
        setSelection(conciergeShopCategory.isSelected());
    }

    public ObservableBoolean getShouldDisplayCategoryIcon() {
        return mShouldDisplayCategoryIcon;
    }

    public ObservableField<String> getCategoryIcon() {
        return mCategoryIcon;
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    public ObservableField<String> getIcon() {
        return mIcon;
    }

    public ObservableInt getVerticalBarVisibility() {
        return mVerticalBarVisibility;
    }

    public ObservableInt getTitleColor() {
        return mTitleColor;
    }

    public ObservableInt getIconColor() {
        return mIconColor;
    }

    public void setSelection(boolean isSelected) {
        mConciergeShopCategory.setSelected(isSelected);
        if (isSelected) {
            mTitleColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_category_list_selected_label));
            mIconColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_category_list_icon_selected_label));
            mVerticalBarVisibility.set(View.VISIBLE);
        } else {
            mTitleColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_category_list_label));
            mIconColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_category_list_icon_label));
            mVerticalBarVisibility.set(View.GONE);
        }
    }

    public boolean isSelected() {
        return mConciergeShopCategory.isSelected();
    }
}
