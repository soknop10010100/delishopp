package com.proapp.sompom.viewmodel.binding;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.databinding.BindingAdapter;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.example.usermentionable.utils.GlideUtil;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.GlideApp;
import com.proapp.sompom.helper.GlideRequest;
import com.proapp.sompom.helper.GlideRequests;
import com.proapp.sompom.model.AdsItem;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.ErrorLoadingType;
import com.proapp.sompom.model.notification.NotificationActor;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.GlideLoadUtil;

import java.io.File;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by He Rotha on 3/20/18.
 */

public final class ImageViewBindingUtil {

    public static final String SVG_EXTENSION = "svg";

    private ImageViewBindingUtil() {
    }

    @BindingAdapter(value = {"logoUrl", "logoPlaceHolder", "shouldLoad"})
    public static void loadCompanyLogo(ImageView imageView,
                                       String imageUrl,
                                       Drawable placeHolder,
                                       boolean shouldLoad) {
        if (shouldLoad) {
            imageView.setVisibility(View.VISIBLE);
            loadImageUrl(imageView, imageUrl, placeHolder, false);
        }
    }

    @BindingAdapter(value = {"imageUrl", "placeHolder", "isDirectLoad", "tint"}, requireAll = false)
    public static void loadImageUrl(ImageView imageView,
                                    String imageUrl,
                                    Drawable placeHolder,
                                    boolean isDirectLoad,
                                    Integer tint) {

        loadImageUrl(imageView, imageUrl, placeHolder, isDirectLoad);
        if (tint != null) {
            imageView.setColorFilter(tint);
        }
    }

    public static void loadImageUrl(ImageView imageView,
                                    String imageUrl,
                                    Drawable placeHolder,
                                    boolean isDirectLoad) {
        if (!TextUtils.isEmpty(imageUrl)) {
            if (!isDirectLoad) {
                //Download the resource and manually set to ImageView
                GlideLoadUtil.loadDefault(imageView, imageUrl, placeHolder, false, null);
            } else {
                //Let Glide manage the download and set to the view
                GlideLoadUtil.load(imageView, imageUrl, placeHolder);
            }
        } else if (placeHolder != null) {
            imageView.setImageDrawable(placeHolder);
        }
    }

    public static void loadImageUrl(ImageView imageView,
                                    String imageUrl,
                                    int resizeWidth,
                                    int resizeHeight,
                                    Drawable drawable,
                                    boolean isRound) {
        loadImageUrl(imageView, imageUrl, resizeWidth, resizeHeight, drawable, isRound, 0);
    }


    @BindingAdapter(value =
            {"loadImageUrl", "resizeWidth", "resizeHeight", "drawable", "isRound", "cornerRadius"},
            requireAll = false)
    public static void loadImageUrl(ImageView imageView,
                                    String imageUrl,
                                    int resizeWidth,
                                    int resizeHeight,
                                    Drawable drawable,
                                    boolean isRound,
                                    float cornerRadius) {
        if (!GlideLoadUtil.isValidContext(imageView.getContext())) {
            return;
        }

        if (drawable == null) {
            drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_400dp);
        }
        if (TextUtils.isEmpty(imageUrl)) {
            imageView.setImageDrawable(drawable);
            return;
        }
        GlideRequests glide = GlideApp.with(imageView);
        GlideRequest<?> glideRequest;

        if (imageUrl.startsWith("http")) {
            glideRequest = glide.load(imageUrl);
        } else {
            glideRequest = glide.load(new File(imageUrl));
        }

        if (resizeHeight != 0 || resizeWidth != 0) {
            if (isRound) {
                glideRequest = glideRequest.apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .centerCrop()
                        .dontAnimate()
                        .circleCrop()
                        .skipMemoryCache(false)
                        .override(resizeWidth, resizeHeight)
                        .placeholder(drawable));
            } else {
                if (cornerRadius != 0) {
                    MultiTransformation<android.graphics.Bitmap> multi = new MultiTransformation<>(
                            new CenterCrop(),
                            new RoundedCornersTransformation((int) cornerRadius, 0));
                    glideRequest = glideRequest.apply(RequestOptions.bitmapTransform(multi));
                } else {
                    glideRequest = glideRequest
                            .override(resizeWidth, resizeHeight);
                }
            }
        }
        glideRequest.placeholder(drawable).into(imageView);
    }

    @BindingAdapter("setUserImage")
    public static void setUserUrl(ImageView imageView, User user) {
        String name;
        String url;
        if (user == null) {
            name = null;
            url = null;
        } else {
            url = user.getUserProfileThumbnail();

            if (TextUtils.isEmpty(url)) {
                url = user.getUserProfileThumbnail();
            }
        }

        if (user != null) {
            TextDrawable drawable = getTextDrawable(imageView.getContext(),
                    user.getFirstName(),
                    user.getLastName());
            loadImageUrl(imageView, url, 150, 150, drawable, true);
        }
    }

    @BindingAdapter(value = {"setRoundImage", "setPlaceholder"}, requireAll = false)
    public static void setRoundImage(ImageView imageView, String url, Drawable placeholder) {
        GlideLoadUtil.loadCircle(imageView, url, placeholder);
    }

    @BindingAdapter("setRoundAvatar")
    public static void setRoundAvatar(ImageView imageView, User user) {
        TextDrawable drawable = ImageViewBindingUtil.getTextDrawable(imageView.getContext(),
                user.getFirstName(),
                user.getLastName());
        GlideLoadUtil.loadCircle(imageView, user.getUserProfileThumbnail(), drawable);
    }

    @BindingAdapter("setConciergeFeaturedPicture")
    public static void setConciergeFeaturedPicture(ImageView imageView, String url) {
        GlideLoadUtil.loadCircle(imageView, url, null);
    }

    @BindingAdapter("setUserImage")
    public static void setGroupImage(ImageView imageView, String url) {

        loadImageUrl(imageView, url, 150, 150, null, true);
    }

    @BindingAdapter("setUserImage")
    public static void setUserUrl(ImageView imageView, NotificationActor user) {
        String name;
        String url;
        if (user == null) {
            name = null;
            url = null;
        } else {
            url = user.getUserProfile();

            if (TextUtils.isEmpty(url)) {
                url = user.getUserProfile();
            }
        }

        if (user != null) {
            TextDrawable drawable = getTextDrawable(imageView.getContext(),
                    user.getFirstName(),
                    user.getLastName());
            loadImageUrl(imageView, url, 150, 150, drawable, true);
        }
    }

    public static TextDrawable getTextDrawable(Context context, String firstName, String lastName) {
        return GlideUtil.getDrawableProfileFromName(context, firstName, lastName, false);
    }

    @BindingAdapter("setProductImage")
    public static void setProductImage(ImageView imageView, Product product) {
        //TODO: need to get thumbnail from ProductMedia Object after server done
        if (product == null) {
            return;
        }
        String productPhotoUrl = null;
        if (product.getMedia() != null && !product.getMedia().isEmpty()) {
            productPhotoUrl = product.getMedia().get(0).getUrl();
        }
        if (product.getMedia() != null && !product.getMedia().isEmpty()) {
            Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_200dp);
            loadImageUrl(imageView, productPhotoUrl, 160, 160, drawable, false);
        } else {
            imageView.setImageResource(R.drawable.ic_photo_placeholder_200dp);
        }
    }

    @BindingAdapter("setAdsImage")
    public static void setAdsImage(ImageView imageView, AdsItem product) {
        if (!GlideLoadUtil.isValidContext(imageView.getContext())) {
            return;
        }

        boolean isPhone = imageView.getResources().getBoolean(R.bool.is_phone);
        String productPhotoUrl;
        if (isPhone) {
            productPhotoUrl = product.getCreativeMobile();
        } else {
            productPhotoUrl = product.getCreativeTablet();
        }
        GlideApp.with(imageView)
                .load(productPhotoUrl)
                .placeholder(R.drawable.ic_photo_placeholder_200dp)
                .into(imageView);
    }

    @BindingAdapter("setMoreGameImageView")
    public static void setMoreGameImageView(ImageView imageView, String url) {
        if (!GlideLoadUtil.isValidContext(imageView.getContext())) {
            return;
        }

        GlideApp.with(imageView)
                .load(url)
                .error(R.drawable.image_place_holder)
                .override(0, 300)
                .placeholder(R.drawable.image_place_holder)
                .into(imageView);
    }

    @BindingAdapter("setErrorImageLoading")
    public static void setErrorImageLoading(ImageView imageView, ErrorLoadingType type) {
        if (type == null) {
            return;
        }
        imageView.setImageResource(type.getIcon());
    }

    @BindingAdapter(value = {"loadImageUrlWithCorner", "resizeWidth", "resizeHeight", "radius"})
    public static void loadImageUrlWithCorner(ImageView imageView, String imageUrl, int resizeWidth, int resizeHeight, int radius) {
        if (!GlideLoadUtil.isValidContext(imageView.getContext())) {
            return;
        }

        Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_400dp);
        MultiTransformation<Bitmap> multi = new MultiTransformation<>(
                new CenterCrop(),
                new RoundedCornersTransformation(radius, 0));
        GlideApp.with(imageView)
                .load(imageUrl)
                .apply(RequestOptions.bitmapTransform(multi))
                .placeholder(drawable)
                .override(resizeWidth, resizeHeight)
                .into(imageView);
    }


    @BindingAdapter(value = {"setGifImage", "setIsComment"}, requireAll = false)
    public static void setGifImage(ImageView imageView, Media gifMedia, boolean isComment) {
        if (!GlideLoadUtil.isValidContext(imageView.getContext())) {
            return;
        }

        Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_400dp);
        int rounded = imageView.getContext().getResources().getDimensionPixelSize(R.dimen.space_small);
        GlideApp.with(imageView.getContext())
                .asGif()
                .load(gifMedia.getUrl())
                .transform(new RoundedCornersTransformation(rounded, 0))
                .placeholder(drawable)
                .into(imageView);

        int imageWidth;
        if (isComment) {
            imageWidth = imageView.getContext().getResources().getDimensionPixelOffset(R.dimen.comment_media_width);
        } else {
            imageWidth = imageView.getContext().getResources().getDimensionPixelOffset(R.dimen.chat_media_width);
        }

        imageView.getLayoutParams().width = imageWidth;
        imageView.getLayoutParams().height = imageWidth * gifMedia.getHeight() / gifMedia.getWidth();
    }

    @BindingAdapter(value = {"setLogoLinkPreview", "placeHolder"}, requireAll = false)
    public static void setLogoLinkPreview(ImageView imageView,
                                          String url,
                                          Drawable placeHolder) {
        GlideLoadUtil.loadDefaultLinkPreviewLogo(imageView,
                url,
                placeHolder,
                new GlideLoadUtil.GlideLoadUtilListener() {
                    @Override
                    public void onLoadSuccess(Drawable resource, boolean isSVG) {
                        if (imageView.getContext() instanceof Activity) {
                            ((Activity) imageView.getContext()).runOnUiThread(() -> GlideLoadUtil.loadLinkPreviewResource(imageView,
                                    url,
                                    resource,
                                    placeHolder,
                                    true));
                        }
                    }
                });
    }

    @BindingAdapter("setImageTintColor")
    public static void setImageTintColor(ImageView view, int color) {
        if (view.getDrawable() != null && color != Color.TRANSPARENT) {
            DrawableCompat.setTint(view.getDrawable(), color);
        }
    }
}
