package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import android.text.TextUtils;
import android.view.View;

import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.helper.NavigateSellerStoreHelper;
import com.proapp.sompom.model.emun.FollowItemType;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.FormatNumber;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;
import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnLikeItemListener;
import com.proapp.sompom.newui.AbsBaseActivity;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class ListItemLikeViewModel extends AbsBaseViewModel {
    public final ObservableField<String> mFollow = new ObservableField<>();
    public final ObservableBoolean mIsFollowing = new ObservableBoolean();
    public final ObservableBoolean mVisibleButtonFollow = new ObservableBoolean();
    private final ObservableField<String> mPosition = new ObservableField<>();

    public final User mUser;
    private final Context mContext;
    private final OnLikeItemListener mListener;
    private final FollowItemType mShowFollowType;

    public ListItemLikeViewModel(Context context,
                                 String myUserID,
                                 User likeViewerModel,
                                 FollowItemType showFollowType,
                                 OnLikeItemListener likeItemListener) {
        mContext = context;
        mUser = likeViewerModel;
        mShowFollowType = showFollowType;
        mListener = likeItemListener;
        mIsFollowing.set(mUser.isFollow());
        mVisibleButtonFollow.set(TextUtils.isEmpty(myUserID) || !TextUtils.equals(mUser.getId(), myUserID));
        //Old code
//        getFollow();

        //Bind position
        if (!TextUtils.isEmpty(likeViewerModel.getUserJob())) {
            mPosition.set(likeViewerModel.getUserJob());
        }
    }

    public ObservableField<String> getPosition() {
        return mPosition;
    }

    private void getFollow() {
        long totalFollower = mUser.getContentStat().getTotalFollowers();
        StringBuilder followerCount = new StringBuilder();
        followerCount
                .append(FormatNumber.format(totalFollower))
                .append(" ");

        if (totalFollower > 1) {
            followerCount.append(mContext.getString(R.string.seller_store_followers_title));
        } else {
            followerCount.append(mContext.getString(R.string.suggestion_follower));
        }

        StringBuilder followingCount = new StringBuilder();
        long following = mUser.getContentStat().getTotalFollowings();
        followingCount
                .append(FormatNumber.format(following))
                .append(" ")
                .append(mContext.getString(R.string.seller_store_following_title));

        if (mShowFollowType == FollowItemType.FOLLOWER) {
            mFollow.set(followerCount.toString());
        } else if (mShowFollowType == FollowItemType.FOLLOWING) {
            mFollow.set(followingCount.toString());
        } else {
            // Show both follower and following
            followerCount
                    .append(" - ")
                    .append(followingCount);
            mFollow.set(followerCount.toString());
        }
    }

    public void onFollowButtonClick(View view) {
        AnimationHelper.animateBounceFollowButton(view, 600, 0.15f, 10.0f);
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).startLoginWizardActivity(new LoginActivityResultCallback() {
                @Override
                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                    if (!isAlreadyLogin) {
                        if (!SharedPrefUtils.getUserId(mContext).equals(mUser.getId())) {
                            mIsFollowing.set(!mIsFollowing.get());
                            mUser.setFollow(mIsFollowing.get());
                            mUser.getContentStat().setTotalFollowers(getFollowingCounter(mUser.getContentStat().getTotalFollowers()));
                            getFollow();
                            if (mListener != null) {
                                mListener.onFollowClick(mUser, mIsFollowing.get());
                                mListener.onNotifyItem();
                            }
                        } else {
                            mVisibleButtonFollow.set(false);
                            if (mListener != null) {
                                mListener.onNotifyItem();
                            }
                        }
                    } else {
                        mIsFollowing.set(!mIsFollowing.get());
                        mUser.setFollow(mIsFollowing.get());
                        mUser.getContentStat().setTotalFollowers(getFollowingCounter(mUser.getContentStat().getTotalFollowers()));
                        getFollow();
                        if (mListener != null) {
                            mListener.onFollowClick(mUser, mIsFollowing.get());
                        }
                    }
                }
            });
        }
    }

    private long getFollowingCounter(long followingCounter) {
        if (mIsFollowing.get()) {
            return followingCounter + 1;
        } else {
            long count = followingCounter - 1;
            if (count < 0) {
                count = 0;
            }
            return count;
        }
    }

    public void onUserProfileClick() {
        new NavigateSellerStoreHelper(mContext, mUser).openSellerStore();
    }
}
