package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableField;

import com.proapp.sompom.model.concierge.ConciergeCarouselSectionResponse;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

/**
 * Created by Or Vitovongsak on 26/8/21.
 */
public class ConciergePromotionItemFragmentViewModel extends AbsBaseViewModel {

    private final ObservableField<String> mUrl = new ObservableField<>();
    private final ConciergePromotionItemCallback mCallback;
    private final ConciergeCarouselSectionResponse.Data mData;

    public ConciergePromotionItemFragmentViewModel(ConciergeCarouselSectionResponse.Data data,
                                                   ConciergePromotionItemCallback callback) {
        mCallback = callback;
        mData = data;
        initData();
    }

    public void initData() {
        if (mData != null) {
            mUrl.set(mData.getPreview());
        }
    }

    public String getUrl() {
        return mUrl.get();
    }

    public void setUrl(String url) {
        mUrl.set(url);
    }

    public void onPromotionClick() {
        if (mCallback != null) {
            mCallback.onPromotionClick(mData);
        }
    }

    public interface ConciergePromotionItemCallback {
        void onPromotionClick(ConciergeCarouselSectionResponse.Data data);
    }
}
