package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.view.View;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.TelegramChatRequest;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

public class TelegramChatRequestFragmentViewModel extends AbsLoadingViewModel {
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private ApiService mApiService;
    private Callback mCallback;

    public TelegramChatRequestFragmentViewModel(Context context, ApiService apiService, Callback callback) {
        super(context);
        mApiService = apiService;
        mCallback = callback;
        getTelegramChatRequest(false);
    }

    public void getTelegramChatRequest(boolean isRefresh) {
        Observable<Response<List<TelegramChatRequest>>> ob = mApiService.getTelegramChatRequest();
        BaseObserverHelper<Response<List<TelegramChatRequest>>> helper = new BaseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<TelegramChatRequest>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsRefresh.set(false);
                Timber.e(ex.toString());
            }

            @Override
            public void onComplete(Response<List<TelegramChatRequest>> result) {
                mIsRefresh.set(false);
                mCallback.onGetSuccess(result.body(), isRefresh);
            }
        }));
    }

    public void rejectTelegramChatRequest(String id) {
        Observable<Response<Object>> ob = mApiService.rejectTelegramChatRequest(id);
        BaseObserverHelper<Response<Object>> helper = new BaseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e(ex.toString());
            }

            @Override
            public void onComplete(Response<Object> result) {
                getTelegramChatRequest(true);
            }
        }));
    }

    public void acceptTelegramChatRequest(String id) {
        Observable<Response<Conversation>> ob = mApiService.acceptTelegramChatRequest(id);
        BaseObserverHelper<Response<Conversation>> helper = new BaseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(new OnCallbackListener<Response<Conversation>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e(ex.toString());
            }

            @Override
            public void onComplete(Response<Conversation> result) {
                getTelegramChatRequest(true);
                mCallback.onAcceptRequestSuccess(result.body());
            }
        }));
    }

    public interface Callback {
        void onGetSuccess(List<TelegramChatRequest> result, boolean isRefresh);

        void onAcceptRequestSuccess(Conversation conversation);
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        getTelegramChatRequest(true);
    }

    public OnClickListener onButtonRetryClick() {
        return () -> {
            Timber.i("I am clicked");
        };
    }


}
