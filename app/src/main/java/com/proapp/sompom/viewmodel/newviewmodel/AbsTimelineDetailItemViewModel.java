package com.proapp.sompom.viewmodel.newviewmodel;

import android.text.TextUtils;
import android.view.View;

import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.helper.ContentTypeHelper;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Chhom Veasna on 4/28/2020.
 */
public class AbsTimelineDetailItemViewModel extends ListItemTimelineViewModel {

    protected WallStreetAdaptive mPost;
    protected boolean mIsSingleItemPost;

    public AbsTimelineDetailItemViewModel(AbsBaseActivity activity,
                                          WallStreetAdaptive post,
                                          Adaptive dataItem,
                                          WallStreetDataManager dataManager,
                                          int position,
                                          OnTimelineItemButtonClickListener onItemClick) {
        super(activity, dataItem, dataManager, position, onItemClick);
        mPost = post;
        mIsSingleItemPost = isSingleItemPost();
    }

    private boolean isSingleItemPost() {
         /*
          A post contains only single item which can be either text or media, any action such as like
          and comment on that post's item must be considered as action on post its self.
         */
        return mPost != null &&
                TextUtils.isEmpty(mPost.getDescription()) &&
                mPost.getMedia() != null &&
                mPost.getMedia().size() == 1;
    }

    @Override
    public void onLikeClick(View view) {
        if (mAdaptive instanceof WallStreetAdaptive && mContentStat != null) {
            AnimationHelper.animateBounce(view, null);
            boolean currentIsLikedStatus = mIsLike.get();
            mContext.startLoginWizardActivity(new LoginActivityResultCallback() {
                @Override
                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                    ContentType contentType;
                    String submitId = mAdaptive.getId();
                    if (mIsSingleItemPost) {
                        contentType = ContentType.POST;
                        submitId = mPost.getId();
                    } else if (mAdaptive instanceof Media) {
                        contentType = ContentTypeHelper.getMediaContentType((Media) mAdaptive);
                    } else {
                        contentType = ContentType.POST;
                    }
                    Observable<Response<Object>> call = mWallStreetDataManager.likePost(mPost.getId(),
                            submitId,
                            contentType,
                            !currentIsLikedStatus);
                    ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mContext, call);
                    addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                            showSnackBarFromContext(getContext(), ex.getMessage());
                        }

                        @Override
                        public void onComplete(Response<Object> result) {
                            long likeCount = mContentStat.getTotalLikes();
                            if (currentIsLikedStatus) {
                                likeCount -= 1;
                                mIsLike.set(false);
                                if (mIsSingleItemPost) {
                                    mPost.setLike(false);
                                }
                            } else {
                                likeCount += 1;
                                mIsLike.set(true);
                                if (mIsSingleItemPost) {
                                    mPost.setLike(true);
                                }
                            }
                            mContentStat.setTotalLikes(likeCount);
                            checkLikeItem();
                        }
                    }));
                }
            });
        }
    }
}
