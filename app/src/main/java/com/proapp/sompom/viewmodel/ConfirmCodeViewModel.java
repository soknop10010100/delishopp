package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ObservableField;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.DelayClickableSpan;
import com.proapp.sompom.intent.ConfirmCodeIntent;
import com.proapp.sompom.intent.ProfileSignUpIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.emun.AuthConfigurationType;
import com.proapp.sompom.model.emun.AuthType;
import com.proapp.sompom.model.request.VerifyPhoneRequestBody;
import com.proapp.sompom.model.result.VerifyPhoneResponse;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ConfirmCodeDataManager;
import com.proapp.sompom.utils.NetworkStateUtil;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class ConfirmCodeViewModel extends AbsSignUpViewModel {

    private static final int CODE_LENGTH = 6;
    private static final int DELAY_MESSAGE_DURATION = 3000;

    private final ObservableField<String> mConfirmCode = new ObservableField<>();
    private ConfirmCodeDataManager mDataManager;
    private String mFirebaseToken;

    public ConfirmCodeViewModel(AppCompatActivity context, ConfirmCodeDataManager dataManager) {
        super(context);
        mDataManager = dataManager;
        mBackButtonVisibility.set(View.VISIBLE);

        // Start auto validating immediately when firebase token is parsed from phone input screen
        if (mDataManager.getFirebaseToken() != null) {
            requestAutomaticVerifyPhoneNumber();
        }
    }

    public ObservableField<String> getConfirmCode() {
        return mConfirmCode;
    }

    public final ClickableSpan getResendClickableSpan() {
        return new DelayClickableSpan() {
            @Override
            public void onDelayClick(@NonNull View widget) {
                mFirebaseToken = null;
                requestValidatePhoneNumber(AuthType.fromValue(mDataManager.getExchangeAuthData().getExchangeAuthType()),
                        mDataManager.getExchangeAuthData().getPhoneNumber(),
                        mDataManager.getExchangeAuthData().getCountyCode(),
                        getFullPhoneNumberWithPlus(),
                        false,
                        verificationId -> {
                            mDataManager.getExchangeAuthData().setVerificationId(verificationId);
                            showSnackBar(mContext.getString(R.string.verify_phone_resend_success_message,
                                    getFullPhoneNumberWithPlus()), false);
                        },
                        null);
            }
        };
    }

    public String getFullPhoneNumberWithPlus() {
        return "+" + mDataManager.getExchangeAuthData().getCountyCode() + mDataManager.getExchangeAuthData().getPhoneNumber();
    }

    @Override
    protected boolean shouldEnableActionButton() {
        String code = mConfirmCode.get();
        return code != null && !TextUtils.isEmpty(code) && code.length() >= CODE_LENGTH;
    }

    @Override
    public void onActionButtonClicked() {
        requestValidatePhoneNumber();
    }

    public void onAutoConfirmCode(String otp) {
        mConfirmCode.set(otp);
        new Handler().postDelayed(this::onActionButtonClicked, 1000);
    }

    private void requestValidatePhoneNumber() {
        if (!NetworkStateUtil.isNetworkAvailable(mContext)) {
            showSnackBar(mContext.getString(R.string.error_internet_connection_description), true);
            return;
        }

        showLoading(true);
        if (!TextUtils.isEmpty(mFirebaseToken)) {
            /*
                In this case, there might be retry to confirm code again after verify with Firebase APIs
                failed before. In this case, we will not confirm code with Firebase again.
             */
            requestVerifyPhoneNumber(mFirebaseToken,
                    mDataManager.getExchangeAuthData().getFullPhoneNumber());
        } else {
            FirebaseAuth
                    .getInstance()
                    .signInWithCredential(PhoneAuthProvider.getCredential(mDataManager.getExchangeAuthData().getVerificationId(),
                            mConfirmCode.get()))
                    .addOnCompleteListener((Activity) mContext, task -> {
                        if (task.isSuccessful()) {
                            Timber.i("signInWithCredential:success");
                            task.getResult().getUser().getIdToken(false).addOnCompleteListener(task1 -> {
                                if (task1.isSuccessful()) {
                                    mFirebaseToken = task1.getResult().getToken();
                                    requestVerifyPhoneNumber(task1.getResult().getToken(),
                                            mDataManager.getExchangeAuthData().getFullPhoneNumber());
                                } else {
                                    hideLoading();
                                    showSnackBar(R.string.error_general_description, true);
                                }
                            });
                        } else {
                            hideLoading();
                            Timber.e("signInWithCredential:failure: " + task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                showSnackBar(R.string.verify_phone_invalid_code, true);
                            } else {
                                showSnackBar(R.string.error_general_description, true);
                            }
                        }
                    });
        }
    }

    private void requestAutomaticVerifyPhoneNumber() {
        showLoading(true);
        requestVerifyPhoneNumber(mDataManager.getFirebaseToken(), mDataManager.getExchangeAuthData().getFullPhoneNumber());
    }

    private void requestVerifyPhoneNumber(String userFirebaseToken, String phoneNumber) {
        AuthType type = AuthType.fromValue(mDataManager.getExchangeAuthData().getExchangeAuthType());
        //For some types, we don't have to verify Firebase token with server.
        if (type == AuthType.RESET_PASSWORD_VIA_PHONE ||
                type == AuthType.CHANGE_PHONE ||
                mDataManager.getExchangeAuthData().isIgnoreFirebaseTokenValidation()) {
            passResultToCaller(userFirebaseToken);
        } else {
            requestVerifyPhoneNumberInternally(userFirebaseToken, phoneNumber);
        }
    }

    private void requestVerifyPhoneNumberInternally(String userFirebaseToken, String phoneNumber) {
        Observable<Response<VerifyPhoneResponse>> loginService = mDataManager.getVerifyPhoneNumber(new VerifyPhoneRequestBody(userFirebaseToken, phoneNumber));
        ResponseObserverHelper<Response<VerifyPhoneResponse>> helper = new ResponseObserverHelper<>(mDataManager.getContext(),
                loginService);
        addDisposable(helper.execute(new OnCallbackListener<Response<VerifyPhoneResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e("onFail: " + new Gson().toJson(ex));
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<VerifyPhoneResponse> result) {
                hideLoading();
                Timber.i("onComplete");
                if (result.body().isSuccessful()) {
                    mDataManager.getExchangeAuthData().setPhoneResponseAuthType(result.body().getAuthType().getValue());
                    passResultToCaller(userFirebaseToken);
                } else {
                    showSnackBar(result.body().getMessage(), true);
                }
            }
        }));
    }

    private void passResultToCaller(String firebaseToken) {
        mDataManager.getExchangeAuthData().setFirebaseToken(firebaseToken);
        AuthType exchangeAuthType = AuthType.fromValue(mDataManager.getExchangeAuthData().getExchangeAuthType());
        AuthType phoneResponseAuthType = AuthType.fromValue(mDataManager.getExchangeAuthData().getPhoneResponseAuthType());
        if (exchangeAuthType == AuthType.LOGIN && phoneResponseAuthType == AuthType.SIGN_UP) {
            // Try to log in  but phone does not existed.
            showSnackBar(R.string.verify_phone_login_non_exist, false);
            new Handler().postDelayed(() -> {
                ProfileSignUpIntent loginIntent = new ProfileSignUpIntent(getContext(), mDataManager.getExchangeAuthData());
                ((AppCompatActivity) mContext).startActivity(loginIntent);
                ((AppCompatActivity) mContext).finish();
            }, DELAY_MESSAGE_DURATION);
        } else if (exchangeAuthType == AuthType.SIGN_UP && phoneResponseAuthType == AuthType.LOGIN) {
            // Try to sign up but phone is already existed.
            boolean shouldRedirectToLogin = ApplicationHelper.getAuthConfigurationType(getContext()) != AuthConfigurationType.EMAIL_AND_PHONE;
            if (shouldRedirectToLogin) {
                showSnackBar(R.string.verify_phone_sign_up_exist, false);
            } else {
                showSnackBar(R.string.sign_up_error_phone_taken, true);
            }
            new Handler().postDelayed(() -> {
                if (shouldRedirectToLogin) {
                    Intent loginIntent = ApplicationHelper.getLoginIntent(getContext(), true);
                    loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(loginIntent);
                }
                ((AppCompatActivity) mContext).finish();
            }, DELAY_MESSAGE_DURATION);
        } else {
            Intent intent = new Intent();
            intent.putExtra(ConfirmCodeIntent.DATA, mDataManager.getExchangeAuthData());
            ((AppCompatActivity) mContext).setResult(AppCompatActivity.RESULT_OK, intent);
            ((AppCompatActivity) mContext).finish();
        }
    }
}
