package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.model.ActiveUser;

/**
 * Created by He Rotha on 7/6/18.
 */
public class ListItemChatSellerViewModel {
    public final ObservableField<String> mTitle = new ObservableField<>();
    public final ObservableBoolean mVisibleLoadMore = new ObservableBoolean();

    public ListItemChatSellerViewModel(ActiveUser seller) {
        mTitle.set(seller.getTitle());
        mVisibleLoadMore.set(seller.isShowMore());
    }
}
