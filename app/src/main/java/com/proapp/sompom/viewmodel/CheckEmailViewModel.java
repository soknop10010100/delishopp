package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.proapp.sompom.intent.InputEmailLoginPasswordIntent;
import com.proapp.sompom.intent.InputEmailSignUpDataIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.emun.AuthType;
import com.proapp.sompom.model.result.CheckEmailResponse;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.CheckEmailDataManager;
import com.sompom.baseactivity.ResultCallback;

import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Veasna Chhom on 2/10/22.
 */
public class CheckEmailViewModel extends AbsLoginViewModel<CheckEmailDataManager, AbsLoginViewModel.AbsLoginViewModelListener> {

    public CheckEmailViewModel(Context context, CheckEmailDataManager dataManager, AbsLoginViewModelListener listener) {
        super(context, dataManager, listener);
        mPhoneNumberUtil = PhoneNumberUtil.createInstance(getContext());
    }

    @Override
    protected boolean shouldLoadUserDefaultCountryCode() {
        return true;
    }

    @Override
    protected boolean shouldEnableActionButton() {
        return isPhoneNumberValid(mIdentifier.get()) || isIdentifierValidEmail(mIdentifier.get());
    }

    @Override
    protected void onContinueButtonClicked() {
        if (!TextUtils.isEmpty(mValidEmailInput)) {
            checkIfEmailExist();
        }
    }

    public void onBackClick() {
        if (getContext() instanceof Activity) {
            ((Activity) getContext()).finish();
        }
    }

    private void checkIfEmailExist() {
        showLoading(true);
        Observable<Response<CheckEmailResponse>> checkEmailService = mDataManager.getCheckEmailService(mValidEmailInput);
        ResponseObserverHelper<Response<CheckEmailResponse>> helper = new ResponseObserverHelper<>(getContext(), checkEmailService);
        addDisposable(helper.execute(new OnCallbackListener<Response<CheckEmailResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<CheckEmailResponse> result) {
                hideLoading();
                if (result.body().getAuthType() == AuthType.SIGN_UP) {
                    mContext.startActivity(new InputEmailSignUpDataIntent(getContext(), mValidEmailInput));
                } else {
                    ExchangeAuthData exchangeAuthData = new ExchangeAuthData();
                    exchangeAuthData.setEmail(mValidEmailInput);
                    ((AbsBaseActivity) mContext).startActivityForResult(new InputEmailLoginPasswordIntent(getContext(), exchangeAuthData),
                            new ResultCallback() {
                                @Override
                                public void onActivityResultSuccess(int resultCode, Intent data) {
                                    ((AbsBaseActivity) mContext).setResult(resultCode, data);
                                    ((AbsBaseActivity) mContext).finish();
                                }
                            });
                }
            }
        }));
    }

    public TextView.OnEditorActionListener getOnEditorActionListener() {
        return (textView, action, keyEvent) -> {
            if (action == EditorInfo.IME_ACTION_DONE) {
                onContinueButtonClicked();
            }

            return false;
        };
    }
}
