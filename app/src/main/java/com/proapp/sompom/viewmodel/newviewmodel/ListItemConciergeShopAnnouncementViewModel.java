package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableField;

import com.proapp.sompom.model.concierge.ConciergeShopAnnouncement;

/**
 * Created by Or Vitovongsak on 23/12/21.
 */
public class ListItemConciergeShopAnnouncementViewModel {

    private final ObservableField<String> mNoticeText = new ObservableField<>();
    private final ObservableField<String> mNoticeIcon = new ObservableField<>();
    private final ConciergeShopAnnouncement mAnnouncement;

    public ListItemConciergeShopAnnouncementViewModel(ConciergeShopAnnouncement announcement) {
        mAnnouncement = announcement;
        bindData();
    }

    public void bindData() {
        mNoticeText.set(mAnnouncement.getMessage());
    }

    public ObservableField<String> getNoticeText() {
        return mNoticeText;
    }

    public ObservableField<String> getNoticeIcon() {
        return mNoticeIcon;
    }
}
