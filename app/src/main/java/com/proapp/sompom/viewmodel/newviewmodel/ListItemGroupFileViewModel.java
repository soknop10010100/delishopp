package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.TextUtils;

import androidx.databinding.ObservableField;

import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.FileDownloadHelper;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.utils.DateTimeUtils;
import com.proapp.sompom.utils.DownloaderUtils;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 7/27/2020.
 */
public class ListItemGroupFileViewModel extends AbsItemFileViewModel {

    private ObservableField<String> mName = new ObservableField<>();
    private ObservableField<String> mDescription = new ObservableField<>();
    private ObservableField<String> mExtension = new ObservableField<>();
    private Context mContext;

    public ListItemGroupFileViewModel(Context context, Media media) {
        super(media);
        mContext = context;
        mName.set(media.getFileName());
        if (TextUtils.isEmpty(media.getFormat())) {
            String extension = getExtensionFromFileName(media.getFileName());
            Timber.i("extension: " + extension);
            if (!TextUtils.isEmpty(extension)) {
                mExtension.set(extension);
            }
        } else {
            mExtension.set(media.getFormat());
        }
        String description = ChatHelper.getDisplaySizeWithFormat(media.getSize());
        if (media.getCreateDate() != null) {
            description += ", " + DateTimeUtils.getGroupFileTimeStampDisplay(context,
                    media.getCreateDate().getTime());
        }
        mDescription.set(description);
    }

    private String getExtensionFromFileName(String fileName) {
        return fileName.substring(fileName.lastIndexOf('.') + 1);
    }

    public ObservableField<String> getName() {
        return mName;
    }

    public ObservableField<String> getExtension() {
        return mExtension;
    }

    public ObservableField<String> getDescription() {
        return mDescription;
    }

    @Override
    public final void onFileClick() {
        if (!TextUtils.isEmpty(mMedia.getDownloadedPath())) {
            //File was previously downloaded.
            DownloaderUtils.openDownloadedFile(mContext, mMedia.getDownloadedPath());
        } else {
            WallStreetHelper.downloadFile(mContext,
                    mMedia,
                    this,
                    new FileDownloadHelper.FileDownloadListener() {
                        @Override
                        public void onDownloadFailed(String requestId, String downloadId, Throwable ex) {
                            showSnackBar(ex.getMessage(), true);
                        }
                    });
        }
    }
}
