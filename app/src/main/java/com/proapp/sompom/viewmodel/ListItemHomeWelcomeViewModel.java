package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;

import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.model.WelcomeItem;
import com.proapp.sompom.model.result.AppSetting;
import com.proapp.sompom.model.result.Theme;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by Veasna Chhom on 07/17/20.
 */
public class ListItemHomeWelcomeViewModel extends AbsBaseViewModel {

    private static final String USER_NAME_TOKEN = "@username";

    private ObservableField<Spanned> mTitle = new ObservableField<>();
    private ObservableField<String> mDescription = new ObservableField<>();
    private ObservableField<User> mUserIcon = new ObservableField<>();
    private Context mContext;
    private WelcomeItem mWelcomeItem;

    public ListItemHomeWelcomeViewModel(Context context, WelcomeItem welcomeItem) {
        mContext = context;
        mWelcomeItem = welcomeItem;
        buildWelcomeTitle();
        mDescription.set(mWelcomeItem.getWelcomeDescription());
        bindWelcomeUserIcon();
    }

    private void bindWelcomeUserIcon() {
        //Welcome user icon need to be the name or the icon of organization.
        User user = new User();
        AppSetting appSetting = SharedPrefUtils.getAppSetting(mContext);
        if (appSetting != null && !TextUtils.isEmpty(appSetting.getOrganizationName())) {
            String companyName = appSetting.getOrganizationName().trim();
            String[] names = companyName.split(" ");
            if (names.length > 0) {
                user.setFirstName(names[0]);
                if (names.length > 1) {
                    String lastName = companyName.substring(companyName.indexOf(user.getFirstName()) +
                            user.getFirstName().length());
                    if (!TextUtils.isEmpty(lastName)) {
                        user.setLastName(lastName.trim());
                    }
                }
            } else {
                //There is only first name available.
                user.setFirstName(companyName);
            }
        }
        Theme theme = UserHelper.getSelectedThemeFromAppSetting(mContext);
        if (theme != null && !TextUtils.isEmpty(theme.getCompanyAvatar())) {
            user.setUserProfileThumbnail(theme.getCompanyAvatar());
        }
        mUserIcon.set(user);
    }

    public ObservableField<User> getUserIcon() {
        return mUserIcon;
    }

    private void buildWelcomeTitle() {
        String userName = TextUtils.isEmpty(mWelcomeItem.getUserName()) ? "" : mWelcomeItem.getUserName();
        String title = mWelcomeItem.getWelcomeTitle().replace(USER_NAME_TOKEN, userName);
        Spannable span = Spannable.Factory.getInstance().newSpannable(title);
        int index = title.indexOf(userName);
        span.setSpan(new ForegroundColorSpan(AttributeConverter.convertAttrToColor(mContext,
                R.attr.home_greeting_first_name)),
                index,
                index + userName.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTitle.set(span);
    }

    public ObservableField<Spanned> getTitle() {
        return mTitle;
    }

    public ObservableField<String> getDescription() {
        return mDescription;
    }
}
