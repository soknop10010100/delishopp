package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.proapp.sompom.intent.ResetPasswordIntent;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.datamanager.ResetPasswordDataManager;
import com.sompom.baseactivity.ResultCallback;

/**
 * Created by Veasna Chhom on 5/17/22.
 */
public class ForgotPasswordViewModel extends AbsResetPasswordViewModel {

    public ForgotPasswordViewModel(Context context, ResetPasswordDataManager dataManager) {
        super(context, dataManager);
    }

    @Override
    protected boolean shouldEnableActionButton() {
        return isPhoneNumberValid(mEmail.get()) || isIdentifierValidEmail(mEmail.get());
    }

    @Override
    public void onActionButtonClicked() {
        if (!TextUtils.isEmpty(mValidEmailInput)) {
            requestForgotPassword(false, mValidEmailInput);
        }
    }

    @Override
    protected void onRequestForgotPasswordSuccess(boolean isResend) {
        ((AbsBaseActivity) mContext).startActivityForResult(new ResetPasswordIntent(mContext, mEmail.get()),
                new ResultCallback() {
                    @Override
                    public void onActivityResultSuccess(int resultCode, Intent data) {
                        ((AbsBaseActivity) mContext).finish();
                    }
                });
    }
}
