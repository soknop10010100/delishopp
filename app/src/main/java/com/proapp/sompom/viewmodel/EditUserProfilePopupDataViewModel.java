package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.text.TextUtils;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.UpdateUserProfileDataManager;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.SharedPrefUtils;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Veasna Chhom on 6/17/22.
 */
public class EditUserProfilePopupDataViewModel extends AbsSignUpViewModel {

    private final ObservableField<String> mEditFirstNameError = new ObservableField<>();
    private final ObservableField<String> mEditLastNameError = new ObservableField<>();
    private final ObservableField<String> mEditEmailError = new ObservableField<>();
    private final ObservableField<String> mFirstName = new ObservableField<>();
    private final ObservableField<String> mLastName = new ObservableField<>();
    private UpdateUserProfileDataManager mDataManager;
    private final EditUserProfilePopupDataViewModelListener mListener;

    public EditUserProfilePopupDataViewModel(Context context,
                                             UpdateUserProfileDataManager dataManager,
                                             EditUserProfilePopupDataViewModelListener listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
        mEmail.set(SharedPrefUtils.getUserEmail(context));
        mFirstName.set(SharedPrefUtils.getUserFirstName(context));
        mLastName.set(SharedPrefUtils.getUserLastName(context));
    }

    public ObservableField<String> getEditFirstNameError() {
        return mEditFirstNameError;
    }

    public ObservableField<String> getEditLastNameError() {
        return mEditLastNameError;
    }

    public ObservableField<String> getEditEmailError() {
        return mEditEmailError;
    }

    public ObservableField<String> getFirstName() {
        return mFirstName;
    }

    public ObservableField<String> getLastName() {
        return mLastName;
    }

    @Override
    protected boolean shouldEnableActionButton() {
        return isEmailValid() && isUserNameValid();
    }

    @Override
    public void onActionButtonClicked() {
        //Do nothing
    }

    private boolean isUserNameValid() {
        return mFirstName.get() != null && !TextUtils.isEmpty(mFirstName.get()) &&
                mLastName.get() != null && !TextUtils.isEmpty(mLastName.get());
    }

    public void onCancelButtonClicked() {
        if (mListener != null) {
            mListener.onCancelButtonClicked();
        }
    }

    public void onUpdateButtonClicked() {
        if (mActionButtonEnable.get()) {
            requestUpdateUser();
        }
    }

    public void requestUpdateUser() {
        showLoading(true);
        Observable<Response<User>> call = mDataManager.updateUser(mFirstName.get().trim(), mLastName.get().trim(), mEmail.get().trim());
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<Response<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                if (mListener != null) {
                    mListener.onShowError(ex.getMessage());
                }
            }

            @Override
            public void onComplete(Response<User> result) {
                hideLoading();
                if (result.body().isResponseError()) {
                    if (mListener != null) {
                        mListener.onShowError(result.body().getErrorMessage());
                    }
                } else {
                    SharedPrefUtils.setUserFirstName(result.body().getFirstName(), mContext);
                    SharedPrefUtils.setUserLastName(result.body().getLastName(), mContext);
                    SharedPrefUtils.setUserEmail(result.body().getEmail(), mContext);
                    if (mListener != null) {
                        mListener.onUpdateProfileSuccess();
                    }
                }
            }
        }));
    }

    public interface EditUserProfilePopupDataViewModelListener {
        void onUpdateProfileSuccess();

        void onCancelButtonClicked();

        void onShowError(String error);
    }

    @BindingAdapter("setUpdateButtonStatus")
    public static void setUpdateButtonStatus(TextView updateButton, boolean isEnable) {
        if (isEnable) {
            updateButton.setTextColor(AttributeConverter.convertAttrToColor(updateButton.getContext(), R.attr.popup_button_ok_text));
            updateButton.setBackground(ContextCompat.getDrawable(updateButton.getContext(), R.drawable.pop_button_ok_selector));
        } else {
            updateButton.setTextColor(AttributeConverter.convertAttrToColor(updateButton.getContext(), R.attr.popup_button_cancel_text));
            updateButton.setBackground(ContextCompat.getDrawable(updateButton.getContext(), R.drawable.popup_cancel_button_background));
        }
    }
}
