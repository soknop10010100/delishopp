package com.proapp.sompom.viewmodel;

import com.proapp.sompom.listener.OnProductItemClickListener;
import com.proapp.sompom.model.AdsItem;

/**
 * Created by He Rotha on 9/4/17.
 */

public class AdsManagementViewModel {
    public AdsItem mProduct;
    private OnProductItemClickListener mListener;

    public AdsManagementViewModel(AdsItem product,
                                  OnProductItemClickListener listener) {
        mProduct = product;
        mListener = listener;
    }

    public void onAdsClick() {
        if (mListener != null) {
            mListener.onProductItemClick(mProduct);
        }
    }
}
