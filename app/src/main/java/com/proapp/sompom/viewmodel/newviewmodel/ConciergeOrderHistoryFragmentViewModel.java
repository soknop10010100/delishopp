package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.ConciergeOrderHistoryDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Veasna Chhom on 9/9/21.
 */
public class ConciergeOrderHistoryFragmentViewModel extends AbsLoadingViewModel {

    private final ConciergeOrderHistoryDataManager mDataManager;
    private final ObservableBoolean mIsRefreshing = new ObservableBoolean();
    private final ConciergeOrderHistoryFragmentViewModelListener mListener;

    public ConciergeOrderHistoryFragmentViewModel(Context context,
                                                  ConciergeOrderHistoryDataManager dataManager,
                                                  ConciergeOrderHistoryFragmentViewModelListener listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
    }

    public ObservableBoolean getIsRefreshing() {
        return mIsRefreshing;
    }

    public void loadMore() {
        requestData(false, true);
    }

    public void requestData(boolean isRefresh, boolean isLoadMore) {
        if (!isRefresh && !isLoadMore) {
            showLoading();
        }

        Observable<List<ConciergeOrder>> observable = mDataManager.getConciergeOrderHistoryService(isLoadMore);
        BaseObserverHelper<List<ConciergeOrder>> helper = new BaseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<List<ConciergeOrder>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                if (!isLoadMore) {
                    mIsRefreshing.set(false);
                    if (isRefresh) {
                        showSnackBar(ex.getMessage(), true);
                    } else {
                        showError(ex.getMessage());
                    }
                } else {
                    showSnackBar(ex.getMessage(), true);
                    if (mListener != null) {
                        mListener.onLoadMoreError();
                    }
                }
            }

            @Override
            public void onComplete(List<ConciergeOrder> data) {
                hideLoading();
                mIsRefreshing.set(false);
                if (data.isEmpty() && !isLoadMore) {
                    showError(getContext().getString(R.string.error_no_data));
                } else {
                    if (mListener != null) {
                        mListener.onLoadDataSuccess(data, isRefresh, isLoadMore, mDataManager.isCanLoadMore());
                    }
                }
            }
        }));
    }

    @Override
    public void onRetryClick() {
        requestData(false, false);
    }

    public void onRefresh() {
        mIsRefreshing.set(true);
        mDataManager.resetPagination();
        requestData(true, false);
    }

    public void makeSilentRefresh() {
        mDataManager.resetPagination();
        requestData(true, false);
    }

    public interface ConciergeOrderHistoryFragmentViewModelListener {
        void onLoadDataSuccess(List<ConciergeOrder> data,
                               boolean isRefreshed,
                               boolean isFromLoadMore,
                               boolean isCanLoadMore);

        void onLoadMoreError();
    }
}
