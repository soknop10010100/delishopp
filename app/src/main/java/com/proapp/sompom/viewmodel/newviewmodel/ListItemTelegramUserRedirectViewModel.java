package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.model.result.TelegramRedirectUser;
import com.proapp.sompom.model.result.User;

import java.util.Collections;
import java.util.List;

public class ListItemTelegramUserRedirectViewModel {
    private ObservableField<String> mName = new ObservableField<>();
    private ObservableField<String> mGroupImageUrl = new ObservableField<>();
    private ObservableField<String> mUserRequestName = new ObservableField<>();
    private ObservableField<List<User>> mParticipants = new ObservableField<>();
    private ObservableField<String> mConnectedLabel = new ObservableField<>();
    private TelegramRedirectUser mUser;
    private Listener mListener;

    public ListItemTelegramUserRedirectViewModel(TelegramRedirectUser user, ListItemTelegramUserRedirectViewModel.Listener listener) {
        mListener = listener;
        mUser = user;
        bindRequest(user);
    }

    public ObservableField<String> getGroupImageUrl() {
        return mGroupImageUrl;
    }

    public ObservableField<String> getName() {
        return mName;
    }

    public ObservableField<String> getUserRequestName() {
        return mUserRequestName;
    }

    public ObservableField<List<User>> getParticipants() {
        return mParticipants;
    }

    private void bindRequest(TelegramRedirectUser telegramRedirectUser) {
        mName.set(telegramRedirectUser.getFirstName() + ' ' + telegramRedirectUser.getLastName());
        User user = new User();
        user.setFirstName(telegramRedirectUser.getFirstName());
        user.setLastName(telegramRedirectUser.getLastName());
        mParticipants.set(Collections.singletonList(user));
        mConnectedLabel.set(getConnectedLabel());
    }

    public String getConnectedLabel() {
        return mUser.isIsCurrentlyConnected() ? "connected" : "";
    }

    public void onClick() {
        if (mListener != null) mListener.onClick(mUser);
    }

    public interface Listener {
        void onClick(TelegramRedirectUser user);
    }
}
