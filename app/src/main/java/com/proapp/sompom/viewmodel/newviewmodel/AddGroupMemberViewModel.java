package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.dialog.AddGroupParticipantDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.AddGroupMemberDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

public class AddGroupMemberViewModel extends AbsLoadingViewModel {

    private final AddGroupMemberDataManager mDataManager;
    private final OnCallback mOnCallback;
    private ObservableBoolean mIsAddOwner = new ObservableBoolean();

    public AddGroupMemberViewModel(AddGroupMemberDataManager dataManager, OnCallback onCallback) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mOnCallback = onCallback;
        mIsAddOwner.set(dataManager.getAddingType() == AddGroupParticipantDialog.AddingType.OWNER);
    }

    public ObservableBoolean getIsAddOwner() {
        return mIsAddOwner;
    }

    public void requestAuthorizedUserList() {
        showLoading(true);
        Observable<Response<List<User>>> call = mDataManager.getGroupAuthorizedUser();
        ResponseObserverHelper<Response<List<User>>> observerHelper = new ResponseObserverHelper<>(getContext(), call);
        observerHelper.execute(new OnCallbackListener<Response<List<User>>>() {
            @Override
            public void onComplete(Response<List<User>> data) {
                hideLoading();
                if (data.body() == null || data.body().isEmpty()) {
                    showNoItemError(getContext().getString(R.string.error_no_data));
                } else {
                    if (mOnCallback != null) {
                        mOnCallback.onLoadAuthorizedUserListSuccess(data.body());
                    }
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showError(ex.getMessage());
            }
        });
    }

    public final OnClickListener onRetryButtonClick() {
        return this::requestAuthorizedUserList;
    }

    private void addParticipant(List<String> userIds) {
        showLoading(true);
        Observable<Response<GroupDetail>> call = mDataManager.addGroupParticipants(userIds);
        ResponseObserverHelper<Response<GroupDetail>> observerHelper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(observerHelper.execute(new OnCallbackListener<Response<GroupDetail>>() {
            @Override
            public void onComplete(Response<GroupDetail> data) {
                hideLoading();
                if (mOnCallback != null) {
                    mOnCallback.onAddSuccess(mDataManager.getAddingType(), data.body());
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }
        }));
    }

    @Override
    public void onRetryClick() {
        super.onRetryClick();
        requestAuthorizedUserList();
    }

    public final void onRootClick() {
        if (mOnCallback != null) {
            mOnCallback.onRootClicked();
        }
    }

    public final void onAddClick() {
        List<String> selectedIds = mOnCallback.getSelectedIds();
        if (!selectedIds.isEmpty()) {
            addParticipant(selectedIds);
        }
    }

    public final void onCancelClick() {
        if (mOnCallback != null) {
            mOnCallback.onCancelClicked();
        }
    }

    public interface OnCallback {
        void onRootClicked();

        void onCancelClicked();

        void onAddSuccess(AddGroupParticipantDialog.AddingType type, GroupDetail groupDetail);

        void onLoadAuthorizedUserListSuccess(List<User> userList);

        List<String> getSelectedIds();
    }
}
