package com.proapp.sompom.viewmodel;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.intent.ConciergeDeliveryIntent;
import com.proapp.sompom.newui.dialog.UserLoginRegisterBottomSheetDialog;

public class ConciergeCheckSlotItemViewModel extends AbsBaseViewModel {

    public void onCheckSlotClicked(Context context) {
        if (context instanceof AppCompatActivity) {
            if (ApplicationHelper.isInVisitorMode(context)) {
                UserLoginRegisterBottomSheetDialog dialog = UserLoginRegisterBottomSheetDialog.newInstance();
                dialog.show(((AppCompatActivity) context).getSupportFragmentManager(), dialog.getTag());
            } else {
                context.startActivity(ConciergeDeliveryIntent.getNewViewOnlyInstance(context));
            }
        }
    }
}
