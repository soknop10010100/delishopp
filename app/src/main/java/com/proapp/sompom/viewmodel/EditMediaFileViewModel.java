package com.proapp.sompom.viewmodel;


import android.content.Context;

import com.proapp.sompom.R;

/**
 * Created by nuonveyo on 5/7/18.
 */

public class EditMediaFileViewModel extends ToolbarViewModel {
    private OnCallback mOnCallback;

    public EditMediaFileViewModel(Context context, OnCallback onCallback) {
        super(context);
        mOnCallback = onCallback;
        mIsShowAction.set(true);
        mIsEnableToolbarButton.set(true);
        mActionText.set(context.getString(R.string.create_wall_street_edit_media_done_button));
    }

    @Override
    public void onToolbarButtonClicked(Context context) {
        if (mOnCallback != null) {
            mOnCallback.onDone();
        }
    }

    public interface OnCallback {
        void onDone();
    }
}
