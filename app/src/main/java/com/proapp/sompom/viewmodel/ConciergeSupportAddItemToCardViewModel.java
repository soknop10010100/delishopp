package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeBasketMenuItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItemManipulationInfo;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.newui.fragment.AbsSupportShopCheckOutFragment;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.AbsConciergeProductDataManager;
import com.sompom.pushy.service.SendBroadCastHelper;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 13/9/21.
 */

public abstract class ConciergeSupportAddItemToCardViewModel<DM extends AbsConciergeProductDataManager,
        L extends ConciergeSupportAddItemToCardViewModel.ConciergeSupportAddItemToCardViewModelListener> extends AbsLoadingViewModel {

    protected DM mDataManager;
    protected L mListener;

    public ConciergeSupportAddItemToCardViewModel(Context context, L listener, DM dataManager) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
    }

    public void requestAddItemToBasket(ConciergeMenuItem item, AbsSupportShopCheckOutFragment fragment) {
        FlurryHelper.logEvent(mContext, FlurryHelper.ADD_PRODUCT_TO_CHECKOUT_CARD);
        //There must be one product count when add item to basket on server
        if (item.getProductCount() == 0) {
            item.setProductCount(1);
        }

        showLoading(true);
        Observable<Response<ConciergeBasketMenuItem>> responseObservable = mDataManager.addItemToBasket(item);
        BaseObserverHelper<Response<ConciergeBasketMenuItem>> helper = new BaseObserverHelper<>(mContext, responseObservable);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeBasketMenuItem>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    showSnackBar(ex.getMessage(), true);
                } else {
                    fragment.showOrderErrorPopup(mContext.getString(R.string.shop_request_add_item_to_basket_error, item.getName()), null);

                }
            }

            @Override
            public void onComplete(Response<ConciergeBasketMenuItem> result) {
                if (result.body() != null && result.body().isError()) {
                    hideLoading();
                    if (result.body().getDataType() == SupportConciergeCustomErrorResponse.ResponseDataType.PAYMENT_IN_PROCESS) {
                        if (mListener != null) {
                            mListener.onHandlePaymentInProgressError(ConciergeSupportAddItemToCardViewModel.this,
                                    result.body().getErrorMessage(),
                                    item,
                                    false);
                        }
                    } else {
                        fragment.showOrderErrorPopup(mContext.getString(R.string.shop_request_add_item_to_basket_error, item.getName()), null);
                    }
                } else {
                    //Must set basket item to menu item before saving to local basket for later use with update basket item
                    if (result.body() != null) {
                        item.setBasketItemId(result.body().getId());
                    }
                    addItemToLocalBasket(item, fragment);
                }
            }
        }));
    }

    private void addItemToLocalBasket(ConciergeMenuItem item, AbsSupportShopCheckOutFragment fragment) {
        Timber.i("addItemToLocalBasket: " + new Gson().toJson(item));
        Observable<ConciergeMenuItemManipulationInfo> ob = ConciergeCartHelper.addItemToCart(item, -1, false);
        BaseObserverHelper<ConciergeMenuItemManipulationInfo> helper = new BaseObserverHelper<>(mContext, ob);
        addDisposable(helper.execute(new OnCallbackListener<ConciergeMenuItemManipulationInfo>() {
            @Override
            public void onComplete(ConciergeMenuItemManipulationInfo result) {
                hideLoading();
                Timber.i("Item added to cart! \n" + new Gson().toJson(result));
                if (mListener != null) {
                    mListener.onItemAddedToCart(item);
                }
                Intent intent = new Intent(AbsSupportShopCheckOutFragment.CART_ITEM_ADDED_EVENT);
                intent.putExtra(AbsSupportShopCheckOutFragment.CART_EVENT_DATA, item);
                SendBroadCastHelper.verifyAndSendBroadCast(fragment.requireContext(), intent);
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                fragment.showOrderErrorPopup(mContext.getString(R.string.shop_request_add_item_to_basket_error, item.getName()), null);
            }
        }));
    }

    public void requestToClearBasket(OnCallbackListener<Response<SupportConciergeCustomErrorResponse>> clearBasketCallback) {
        showLoading(true);
        Observable<Response<SupportConciergeCustomErrorResponse>> observable = mDataManager.clearBasket();
        ResponseObserverHelper<Response<SupportConciergeCustomErrorResponse>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<SupportConciergeCustomErrorResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
                if (clearBasketCallback != null) {
                    clearBasketCallback.onFail(ex);
                }
            }

            @Override
            public void onComplete(Response<SupportConciergeCustomErrorResponse> result) {
                if (result.body() != null && result.body().isError()) {
                    hideLoading();
                    showSnackBar(result.body().getErrorMessage(), true);
                } else {
                    hideLoading();
                }
                if (clearBasketCallback != null) {
                    clearBasketCallback.onComplete(result);
                }
            }
        }));
    }

    public interface ConciergeSupportAddItemToCardViewModelListener {

        void onItemAddedToCart(ConciergeMenuItem item);

        void onHandlePaymentInProgressError(ConciergeSupportAddItemToCardViewModel viewModel,
                                            String errorMessage,
                                            ConciergeMenuItem menuItem,
                                            boolean isUpdateItemProcess);

        void showAddItemFromDifferentShopWarningPopup(ConciergeSupportAddItemToCardViewModel viewModel,
                                                      OnCallbackListener<Response<SupportConciergeCustomErrorResponse>> clearBasketCallback);
    }
}
