package com.proapp.sompom.viewmodel;

import android.text.TextWatcher;

import com.proapp.sompom.helper.DelayWatcher;

/**
 * Created by Or Vitovongsak on 15/9/21.
 */

public class ListItemSpecialInstructionViewModel {

    private ListItemSpecialInstructionViewModelListener mListener;

    public ListItemSpecialInstructionViewModel(ListItemSpecialInstructionViewModelListener listener) {
        mListener = listener;
    }

    public interface ListItemSpecialInstructionViewModelListener {
        void onInstructionTextChange(String instruction);
    }

    public final TextWatcher onTextChange() {
            return new DelayWatcher(false) {
                @Override
                public void onTextChanged(String text) {
                    if (mListener != null) {
                        mListener.onInstructionTextChange(text);
                    }
                }
            };
    }
}
