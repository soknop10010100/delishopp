package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.listener.OnEditTextCommentListener;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.PopUpCommentDataManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class PopUpCommentLayoutViewModel extends LayoutEditTextCommentViewModel {

    public final ObservableField<String> mEmptyMessage = new ObservableField<>();
    private final OnCallback mOnCallback;
    private boolean mIsLoadedData;

    public PopUpCommentLayoutViewModel(PopUpCommentDataManager dataManager,
                                       String postId,
                                       String itemId,
                                       ContentType contentType,
                                       OnCallback onCallback) {
        super(dataManager, postId, itemId, contentType, onCallback, null);
        mOnCallback = onCallback;
        mEmptyMessage.set(getContext().getString(R.string.comment_screen_empty_message));
        getData();
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        if (mOnCallback != null) {
            mOnCallback.onPullAndRefresh(mIsRefresh);
        }
    }

    @Override
    public void onRetryClick() {
        getData();
    }

    private void getData() {
        showLoading();
        Observable<Response<List<Comment>>> callback = mDataManager.getCommentList(mContentId, false);
        ResponseObserverHelper<Response<List<Comment>>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Comment>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
//                Timber.i("onFail: " + ex.getMessage());
                mIsRefresh.set(false);
                hideLoading();
                if (mIsLoadedData) {
                    return;
                }
                mIsLoadedData = false;
                showError(ex.getMessage());
            }

            @Override
            public void onComplete(Response<List<Comment>> value) {
                hideLoading();
                List<Comment> commentList = value.body();
                if (commentList == null || commentList.isEmpty()) {
                    showEmptyData();
                    showOrHideEmptyCommentLabel(true);
                    //Need to init empty list
                    mOnCallback.onLoadCommentSuccess(new ArrayList<>(), false);
                    return;
                }

                mIsRefresh.set(false);
                mIsLoadedData = true;
                showOrHideEmptyCommentLabel(false);
                mOnCallback.onLoadCommentSuccess(commentList, mDataManager.isCanLoadMore());
            }
        }));
    }

    public void loadMore(OnCallbackListListener<Response<List<Comment>>> listListener) {
        Observable<Response<List<Comment>>> callback = mDataManager.getCommentList(mContentId, true);
        ResponseObserverHelper<Response<List<Comment>>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Comment>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsRefresh.set(false);
                listListener.onFail(ex);
            }

            @Override
            public void onComplete(Response<List<Comment>> value) {
                mIsRefresh.set(false);
                listListener.onComplete(value, mDataManager.isCanLoadMore());
            }
        }));
    }

    public void onDeleteComment(Comment comment, OnClickListener onClickListener) {
        showLoading(true);
        Observable<Response<Object>> callback = mDataManager.deleteComment(comment.getId());
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                mSnackBarErrorMessage.set(null);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    mSnackBarErrorMessage.set(ex.toString());
                } else {
                    mSnackBarErrorMessage.set(getContext().getString(R.string.comment_toast_delete_fail));
                }
            }

            @Override
            public void onComplete(Response<Object> result) {
                hideLoading();
                onClickListener.onClick();
                int totalSubComment = comment.getTotalSubComment();
                if (totalSubComment < 0) {
                    totalSubComment = 0;
                }
                //Need to update comment counter base on its sub comment too.
                setNumberOfCommentAdd(getNumberOfCommentAdd() - (1 + totalSubComment));
            }
        }));
    }

    public void showEmptyData() {
        showOrHideEmptyCommentLabel(true);
        mEmptyMessage.set(getContext().getString(R.string.comment_screen_empty_message));
    }

    public void checkToLikeComment(Comment comment, OnCallbackListener<Response<Object>> listener) {
        Observable<Response<Object>> callback = mDataManager.postLikeComment(comment.getId(), comment.isLike(), mPostId);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mSnackBarErrorMessage.set(ex.getMessage());
                mSnackBarErrorMessage.notifyChange();
                if (listener != null) {
                    listener.onFail(ex);
                }
            }

            @Override
            public void onComplete(Response<Object> result) {
                if (listener != null) {
                    listener.onComplete(result);
                }
            }
        }));
    }

    public interface OnCallback extends OnEditTextCommentListener {
        void onLoadCommentSuccess(List<Comment> listComment, boolean isCanLoadMore);

        void onLoadCommentFail();

        void onPullAndRefresh(ObservableBoolean refresh);
    }
}
