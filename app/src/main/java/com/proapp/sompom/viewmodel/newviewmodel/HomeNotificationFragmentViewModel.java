package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableBoolean;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnCompleteListListener;
import com.proapp.sompom.model.notification.NotificationAdaptive;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.NotificationDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

/**
 * Created by nuonveyo on 7/4/18.
 */

public class HomeNotificationFragmentViewModel extends AbsLoadingViewModel {

    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private final NotificationDataManager mDataManager;
    private final OnCompleteListListener<List<NotificationAdaptive>> mListener;
    private boolean mIsLoadedData;

    public HomeNotificationFragmentViewModel(NotificationDataManager notificationDataManager,
                                             OnCompleteListListener<List<NotificationAdaptive>> listener) {
        super(notificationDataManager.getContext());
        mDataManager = notificationDataManager;
        mListener = listener;
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        getData();
    }

    @Override
    public void onRetryClick() {
        super.onRetryClick();
        getData();
    }

    public void getData() {
        if (!mIsRefresh.get()) {
            showLoading();
        } else {
            mDataManager.resetPagination();
        }
        Observable<List<NotificationAdaptive>> call = mDataManager.geNotification2(false);
        BaseObserverHelper<List<NotificationAdaptive>> helper = new BaseObserverHelper<>(mDataManager.getContext(), call);
        Disposable disposable = helper.execute(new OnCallbackListener<List<NotificationAdaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (mIsRefresh.get()) {
                    mIsRefresh.set(false);
                    showSnackBar(ex.getMessage(), true);
                } else {
                    showError(ex.toString(), true);
                }
            }

            @Override
            public void onComplete(List<NotificationAdaptive> result) {
                Timber.i("result: " + new Gson().toJson(result));
                hideLoading();
                if (mIsRefresh.get()) {
                    mIsRefresh.set(false);
                }
                if (result.isEmpty()) {
                    showError(getContext().getString(R.string.notification_empty_data));
                } else {
                    if (mListener != null) {
                        mListener.onComplete(result, mDataManager.isCanLoadMore());
                    }
                }
            }
        });
        addDisposable(disposable);
    }

    public void loadMore(OnCallbackListListener<List<NotificationAdaptive>> listListener) {
        Observable<List<NotificationAdaptive>> call = mDataManager.loadMore();
        BaseObserverHelper<List<NotificationAdaptive>> helper = new BaseObserverHelper<>(mDataManager.getContext(), call);
        Disposable disposable = helper.execute(new OnCallbackListener<List<NotificationAdaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                showSnackBar(ex.getMessage(), true);
                listListener.onFail(ex);
            }

            @Override
            public void onComplete(List<NotificationAdaptive> result) {
                listListener.onComplete(result, mDataManager.isCanLoadMore());
            }
        });
        addDisposable(disposable);
    }

    public interface HomeNotificationFragmentViewModelListener {
        void onLoadGroupConversationSuccess(Conversation conversation1);
    }
}
