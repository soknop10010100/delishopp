package com.proapp.sompom.viewmodel.newviewmodel;

import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.emun.ActionButtonItem;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

import timber.log.Timber;

/**
 * Created by nuonveyo on 8/1/18.
 */

public class ProductDetailActionButtonDialogViewModel extends AbsBaseViewModel {
    public ActionButtonItem[] getItems() {
        return ActionButtonItem.values();
    }

    public OnItemClickListener<ActionButtonItem> getListener() {
        return actionButtonItem -> Timber.e("actionButtonItem: %s", actionButtonItem.getId());
    }
}
