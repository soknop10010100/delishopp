package com.proapp.sompom.viewmodel.newviewmodel;

import android.view.View;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewholder.TimelineViewHolder;

import timber.log.Timber;

public class ListItemDefaultTimelineDetailViewModel extends AbsTimelineDetailItemViewModel {

    private OnClickListener onLikeButtonClickListener;
    private final ObservableInt mCheckLikeComment = new ObservableInt(View.GONE);
    private final ObservableField<ListItemMainLinkPreviewViewModel> mLinkPreviewViewModel = new ObservableField<>();
    private ObservableInt mLinkPreviewVisibility = new ObservableInt(View.GONE);
    private ObservableInt mPlacePreviewVisibility = new ObservableInt(View.GONE);
    private boolean mShouldRenderPlacePreview;

    public ObservableInt getCheckLikeComment() {
        return mCheckLikeComment;
    }

    public ListItemDefaultTimelineDetailViewModel(AbsBaseActivity activity,
                                                  BindingViewHolder holder,
                                                  WallStreetAdaptive post,
                                                  boolean checkLikeComment,
                                                  Adaptive adaptive,
                                                  WallStreetDataManager dataManager,
                                                  int position,
                                                  OnTimelineItemButtonClickListener onItemClick,
                                                  OnClickListener onClickListener) {
        super(activity, post, adaptive, dataManager, position, onItemClick);
        onLikeButtonClickListener = onClickListener;
        if (!checkLikeComment) {
            setCheckLikeComment(View.GONE);
        } else {
            setCheckLikeComment(View.VISIBLE);
        }
        boolean shouldDisplayLinkPreview = WallStreetHelper.shouldRenderLinkPreview(adaptive);
        mLinkPreviewVisibility.set(shouldDisplayLinkPreview ? View.VISIBLE : View.GONE);
        bindPreviewLink(shouldDisplayLinkPreview, mAdaptive.getLinkPreviewModel(), holder);

        if (!shouldDisplayLinkPreview) {
            boolean shouldBindPreviewPlaceData = WallStreetHelper.shouldRenderPlacePreview(adaptive);
            mShouldRenderPlacePreview = shouldBindPreviewPlaceData && WallStreetHelper.isEmptyMedia(adaptive);
            mPlacePreviewVisibility.set(mShouldRenderPlacePreview ? View.VISIBLE : View.GONE);
        }
    }

    public boolean isShouldRenderPlacePreview() {
        return mShouldRenderPlacePreview;
    }

    public ObservableInt getPlacePreviewVisibility() {
        return mPlacePreviewVisibility;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Timber.i("onDestroy");
    }

    private void bindPreviewLink(boolean shouldDisplayLinkPreview,
                                 LinkPreviewModel previewModel,
                                 BindingViewHolder holder) {
        if (shouldDisplayLinkPreview) {
            mLinkPreviewViewModel.set(new ListItemMainLinkPreviewViewModel(previewModel,
                    true,
                    mOnItemClickListener,
                    videoLayout -> {
                        Timber.i("onBindVideoLayout: " + videoLayout + ", Holder: " + holder.getClass().getSimpleName());
                        if (videoLayout != null && holder instanceof TimelineViewHolder) {
                            ((TimelineViewHolder) holder).setMediaLayout(videoLayout, "Timeline detail");
                        }
                    }));
        } else if (holder instanceof TimelineViewHolder) {
            ((TimelineViewHolder) holder).setMediaLayout(null, "Timeline detail");
        }
    }

    public ObservableField<ListItemMainLinkPreviewViewModel> getLinkPreviewViewModel() {
        return mLinkPreviewViewModel;
    }

    public ObservableInt getLinkPreviewVisibility() {
        return mLinkPreviewVisibility;
    }

    public void setCheckLikeComment(int checkLikeComment) {
        mCheckLikeComment.set(checkLikeComment);
    }

    @Override
    public void onLikeClick(View view) {
        super.onLikeClick(view);
        if (onLikeButtonClickListener != null) {
            onLikeButtonClickListener.onClick();
        }
    }
}
