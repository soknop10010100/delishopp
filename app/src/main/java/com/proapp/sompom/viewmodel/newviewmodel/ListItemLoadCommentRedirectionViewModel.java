package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnLoadPreviousCommentClickListener;
import com.proapp.sompom.model.LoadCommentDirection;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 12/17/18.
 */
public class ListItemLoadCommentRedirectionViewModel extends AbsBaseViewModel {

    private final ObservableField<String> mDirectionText = new ObservableField<>();
    private final ObservableBoolean mIsSubComment = new ObservableBoolean();
    private final ObservableBoolean mIsItemClick = new ObservableBoolean();
    private final OnLoadPreviousCommentClickListener mOnClickListener;
    private final LoadCommentDirection mLoadCommentDirection;
    private final int mPosition;
    private final Context mContext;

    public ListItemLoadCommentRedirectionViewModel(Context context,
                                                   boolean isSubComment,
                                                   int position,
                                                   OnLoadPreviousCommentClickListener onClickListener,
                                                   LoadCommentDirection loadCommentDirection) {
        mContext = context;
        mIsSubComment.set(isSubComment);
        mPosition = position;
        mOnClickListener = onClickListener;
        mLoadCommentDirection = loadCommentDirection;
        bindDirectionText();
    }

    public ObservableBoolean getIsItemClick() {
        return mIsItemClick;
    }

    public ObservableBoolean getIsSubComment() {
        return mIsSubComment;
    }

    public ObservableField<String> getDirectionText() {
        return mDirectionText;
    }

    private void bindDirectionText() {
        if (mIsSubComment.get()) {
            mDirectionText.set(mLoadCommentDirection.isLoadPreviousCommentMode() ? mContext.getString(R.string.comment_previous_reply_button) :
                    mContext.getString(R.string.comment_load_more_reply_button));
        } else {
            mDirectionText.set(mLoadCommentDirection.isLoadPreviousCommentMode() ? mContext.getString(R.string.comment_previous_comment_button) :
                    mContext.getString(R.string.comment_load_more_comment_button));
        }
    }

    public void onItemClick() {
        mIsItemClick.set(true);
        if (mOnClickListener != null) {
            mOnClickListener.onClick(mLoadCommentDirection, mPosition);
        }
    }
}
