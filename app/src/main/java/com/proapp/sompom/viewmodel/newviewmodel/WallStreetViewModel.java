package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.NetworkBroadcastReceiver;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.model.ActiveUser;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.MoreGameItem;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.request.ReportRequest;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.WallLoadMoreWrapper;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by He Rotha on 7/10/18.
 */
public class WallStreetViewModel extends AbsLoadingViewModel {

    public final ObservableBoolean mIsShowRecyclerView = new ObservableBoolean();
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private final WallStreetDataManager mWallStreetDataManager;
    private final OnCallback mCompleteListener;
    private final MoreGameItem mMoreGameList = new MoreGameItem();
    private boolean mIsLoadedData = false;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    private boolean mIsThereAnyWallItem;

    public WallStreetViewModel(Context context,
                               WallStreetDataManager manager,
                               OnCallback onCallback) {
        super(context);
        mWallStreetDataManager = manager;
        mCompleteListener = onCallback;

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkStateListener = networkState -> {
                mWallStreetDataManager.setNetworkAvailable(networkState == NetworkBroadcastReceiver
                        .NetworkState.Connected);
                if (!mIsLoadedData) {
                    getTimeline(false);
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);
        }
    }

    public OnClickListener onButtonRetryClick() {
        return () -> getTimeline(false);
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        getTimeline(true);
    }

    private void getTimeline(boolean isRefresh) {
        if (!mIsRefresh.get()) {
            showLoading();
            mIsShowRecyclerView.set(false);
        }
        mWallStreetDataManager.resetPagination();
        startGetData(isRefresh);
    }

    private void startGetData(boolean isRefresh) {
        Observable<WallLoadMoreWrapper<Adaptive>> callback;
        callback = mWallStreetDataManager.getData(false);
        BaseObserverHelper<WallLoadMoreWrapper<Adaptive>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<WallLoadMoreWrapper<Adaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsRefresh.set(false);
                if (mIsLoadedData) {
                    if (isRefresh && mCompleteListener != null) {
                        mCompleteListener.onRefreshFailed();
                    }
                    return;
                }

                mIsLoadedData = false;
                mIsShowRecyclerView.set(false);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    showNetworkError();
                } else {
                    showError(ex.toString());
                }
            }

            @Override
            public void onComplete(WallLoadMoreWrapper<Adaptive> result) {
                hideLoading();
                if (result.getData() != null && !result.getData().isEmpty() && !mIsThereAnyWallItem) {
                    mIsThereAnyWallItem = true;
                }
                if ((result.getData() == null || result.getData().isEmpty()) && (!isRefresh || !mIsThereAnyWallItem)) {
                    mIsRefresh.set(false);
                    showError(mContext.getString(R.string.error_no_data));
                } else {
                    mIsShowRecyclerView.set(true);
                    mIsLoadedData = true;
                    mIsRefresh.set(false);
                    mIsError.set(false);
                    mCompleteListener.onGetTimelineSuccess(result.getData(),
                            isRefresh,
                            mWallStreetDataManager.isCanLoadMore());
                }
            }
        }));
    }

    public void loadMoreData(OnCallbackListListener<LoadMoreWrapper<Adaptive>> listener) {
        if (mWallStreetDataManager.isCanLoadMore()) {
            Observable<WallLoadMoreWrapper<Adaptive>> callback = mWallStreetDataManager.loadMoreData();
            BaseObserverHelper<WallLoadMoreWrapper<Adaptive>> helper = new BaseObserverHelper<>(getContext(), callback);
            addDisposable(helper.execute(new OnCallbackListener<WallLoadMoreWrapper<Adaptive>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    listener.onFail(ex);
                }

                @Override
                public void onComplete(WallLoadMoreWrapper<Adaptive> result) {
                    listener.onComplete(result, mWallStreetDataManager.isCanLoadMore());
                }
            }));
        } else {
            LoadMoreWrapper<Adaptive> loadMoreWrapper = new LoadMoreWrapper<>();
            loadMoreWrapper.setData(new ArrayList<>());
            listener.onComplete(loadMoreWrapper, false);
        }
    }

    public void checkTopPostFollow(String id, boolean isFollow) {
        Observable<Response<Object>> callback;
        if (isFollow) {
            callback = mWallStreetDataManager.postFollow(id);
        } else {
            callback = mWallStreetDataManager.unFollow(id);
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute());
    }

    public void deletePost(WallStreetAdaptive lifeStream, OnCallbackListener<Response<Object>> callbackListener) {
        Observable<Response<Object>> call = mWallStreetDataManager.deletePost(lifeStream.getId());
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(callbackListener));
    }

    public void deleteProduct(Product product) {
        Observable<Response<Object>> call = mWallStreetDataManager.deleteProduct(product);
        ResponseObserverHelper<Response<Object>> helper
                = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute());
    }

    public void reportProduct(String id, ReportRequest reportRequest) {
        Observable<Response<Object>> request = mWallStreetDataManager.reportProduct(id, reportRequest);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), request);
        addDisposable(helper.execute());
    }

    public void unFollowShop(String id) {
        Observable<Response<Object>> observable = mWallStreetDataManager.unFollowShop(id);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute());
    }

    public void reportPost(String id, ReportRequest reportRequest) {
        Observable<Response<Object>> request = mWallStreetDataManager.reportPost(id, reportRequest);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), request);
        addDisposable(helper.execute());
    }

    public NetworkBroadcastReceiver.NetworkListener getNetworkStateListener() {
        return mNetworkStateListener;
    }

    public interface OnCallback {

        void onGetMoreGameSuccess(MoreGameItem moreGames);

        void onGetActiveUserSuccess(ActiveUser activeUser);

        void onGetTimelineSuccess(List<Adaptive> list, boolean isRefresh, boolean isCanLoadMore);

        void onRefreshFailed();
    }
}
