package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;

import androidx.annotation.DrawableRes;
import androidx.databinding.ObservableField;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeShop;
import com.proapp.sompom.model.emun.ConciergeOrderStatus;
import com.proapp.sompom.model.result.ConciergeDriverLocationResponse;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.model.result.ConciergeOrderTrackingProgress;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ConciergeOrderDeliveryTrackingDataManager;
import com.proapp.sompom.utils.BitmapConverter;
import com.proapp.sompom.utils.DateUtils;
import com.proapp.sompom.viewmodel.AbsMapViewModel;
import com.resourcemanager.helper.LocaleManager;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 16/12/21.
 */
public class ConciergeOrderDeliveryTrackingFragmentViewModel extends AbsMapViewModel {

    private static final long INTERVAL = 5000L; //10 Seconds

    private final ObservableField<String> mOrderDate = new ObservableField<>();
    private final ObservableField<String> mOrderTotalPrice = new ObservableField<>();
    private final ObservableField<String> mOrderNumber = new ObservableField<>();

    private ConciergeOrder mOrder;
    private ConciergeOrderDeliveryTrackingDataManager mDataManager;
    private ConciergeDeliveryTrackingListener mListener;
    private CountDownTimer mCountDownTimer;
    private final List<Disposable> mDisposableList = new ArrayList<>();
    private Marker mDriverMarker;
    private List<Marker> mMarkerList = new ArrayList<>();
    private boolean mIsTimerCanceled;
    private boolean mIsOrderDelivered;

    public ConciergeOrderDeliveryTrackingFragmentViewModel(Context context,
                                                           GoogleMap googleMap,
                                                           ConciergeOrderDeliveryTrackingDataManager dataManager,
                                                           ConciergeDeliveryTrackingListener listener) {
        super(context, googleMap);
        mOrder = dataManager.getOrder();
        mDataManager = dataManager;
        mListener = listener;
        enableMapClick(false);
    }

    public ConciergeOrder getOrder() {
        return mOrder;
    }

    public ObservableField<String> getOrderDate() {
        return mOrderDate;
    }

    public ObservableField<String> getOrderTotalPrice() {
        return mOrderTotalPrice;
    }

    public ObservableField<String> getOrderNumber() {
        return mOrderNumber;
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.i("onResume: " + mIsTimerCanceled + ", mIsOrderDelivered: " + mIsOrderDelivered);
        if (!mIsOrderDelivered && mIsTimerCanceled && mCountDownTimer != null) {
            mIsTimerCanceled = false;
            mCountDownTimer.start();
        }
    }

    public void updateOrderStatus(ConciergeOrderStatus status) {
        if (status == ConciergeOrderStatus.DELIVERED) {
            mIsOrderDelivered = true;
        }
        mOrder.setOrderStatus(status);
    }

    public void checkToUpdateOrderStatus(ConciergeOrderStatus status) {
        updateOrderStatus(status);
        if (status == ConciergeOrderStatus.DELIVERING) {
            Timber.i("Will add driver location.");
            requestNecessaryForDeliveringStatus();
        } else if (status == ConciergeOrderStatus.DELIVERED) {
            if (mCountDownTimer != null) {
                mCountDownTimer.cancel();
            }
            removeDriverLocation();
        }
    }

    @Override
    public void onRetryClick() {
        requestNecessaryData();
    }

    private void requestOrderDetail(OnCallbackListener<Response<ConciergeOrder>> callbackListener) {
        Observable<Response<ConciergeOrder>> observable = mDataManager.getOrderDetail();
        ResponseObserverHelper<Response<ConciergeOrder>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeOrder>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showError(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<ConciergeOrder> result) {
                if (callbackListener != null) {
                    callbackListener.onComplete(result);
                }
            }
        }));
    }

    private void requestDriverDataIfNecessary(boolean shouldShowLoading) {
        if (shouldShowLoading) {
            showLoading();
        }
        if (mDataManager.shouldRequestDriverLocation()) {
            requestDriverLocation(new OnCallbackListener<Response<ConciergeDriverLocationResponse>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    hideLoading();
                    showError(ex.getMessage(), true);
                }

                @Override
                public void onComplete(Response<ConciergeDriverLocationResponse> result) {
                    hideLoading();
                    new Handler().post(() -> {
                        initOrderInfoView();
                        addShopMarkerOnMap();
                        addReceivingAddressMarkerOnMap();
                        if (isValidDriverLocation(result.body())) {
                            addDriverMarker(result.body().getLatitude(), result.body().getLongitude(), true);
                        }
                        zoomToAllAddedMarker(false);
                        if (mListener != null) {
                            mListener.onDataSuccess(mOrder.getTrackingProgress(), mOrder.getOrderStatus());
                        }
                        if (mDataManager.shouldRequestDriverLocation()) {
                            initRequestDriverLocationTimer();
                        }
                        if (mListener != null) {
                            mListener.onShouldSetupBottomSheet();
                        }
                    });
                }
            });
        } else {
            initOrderInfoView();
            addShopMarkerOnMap();
            addReceivingAddressMarkerOnMap();
            zoomToAllAddedMarker(false);
            hideLoading();
            if (mListener != null) {
                mListener.onDataSuccess(mOrder.getTrackingProgress(), mOrder.getOrderStatus());
            }
            if (mListener != null) {
                mListener.onShouldSetupBottomSheet();
            }
        }
    }

    public void requestNecessaryData() {
        if (mDataManager.getOrder() == null) {
            showLoading();
            requestOrderDetail(new OnCallbackListener<Response<ConciergeOrder>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    //Do nothing
                }

                @Override
                public void onComplete(Response<ConciergeOrder> result) {
                    mOrder = mDataManager.getOrder();
                    requestDriverDataIfNecessary(false);
                }
            });
        } else {
            requestDriverDataIfNecessary(true);
        }
    }

    private void initRequestDriverLocationTimer() {
        mCountDownTimer = new CountDownTimer(Long.MAX_VALUE, INTERVAL) {
            @Override
            public void onTick(long l) {
                if (!MainApplication.isAppInBackground()) {
                    requestDriverLocation(new OnCallbackListener<Response<ConciergeDriverLocationResponse>>() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                            Timber.e(ex);
                        }

                        @Override
                        public void onComplete(Response<ConciergeDriverLocationResponse> result) {
                            if (isValidDriverLocation(result.body())) {
                                addDriverMarker(result.body().getLatitude(), result.body().getLongitude(), false);
                            }
                        }
                    });
                } else {
                    mIsTimerCanceled = true;
                    mCountDownTimer.cancel();
                }
            }

            @Override
            public void onFinish() {

            }
        };
        mCountDownTimer.start();
    }

    private boolean isValidDriverLocation(ConciergeDriverLocationResponse response) {
        return response != null && response.getLatitude() != null && response.getLongitude() != null;
    }

    public void checkToClearRequestDriverLocationTimer() {
        if (mDataManager.shouldRequestDriverLocation() && mCountDownTimer != null) {
            mCountDownTimer.cancel();
            clearAllGetDriverLocationRequests();
        }
    }

    private void clearAllGetDriverLocationRequests() {
        for (Disposable disposable : mDisposableList) {
            disposable.dispose();
        }
    }

    private Marker addMarker(String tag,
                             double lat,
                             double lng,
                             @DrawableRes int markerIcon,
                             boolean shouldAddInMarkerList) {
        Timber.i("Add marker of " + tag + ", lat: " + lat + ", lng: " + lng);
        MarkerOptions options = new MarkerOptions();
        options.icon(BitmapConverter.bitmapDescriptorFromVector(mContext, markerIcon));
        options.position(new LatLng(lat, lng));
        Marker marker = mGoogleMap.addMarker(options);
        if (shouldAddInMarkerList) {
            mMarkerList.add(marker);
        }

        return marker;
    }

    private void addShopMarkerOnMap() {
        if (mOrder.getShops() != null && !mOrder.getShops().isEmpty()) {
            ConciergeShop firstShop = mOrder.getShops().get(0);
            if (firstShop.getLatitude() != 0 && firstShop.getLongitude() != 0) {
                addMarker("addShopMarkerOnMap",
                        firstShop.getLatitude(),
                        firstShop.getLongitude(),
                        R.drawable.ic_shop_icon,
                        true);
            }
        }
    }

    private void addReceivingAddressMarkerOnMap() {
        if (mOrder.getDeliveryAddress() != null) {
            addMarker("addReceivingAddressMarkerOnMap",
                    mOrder.getDeliveryAddress().getLatitude(),
                    mOrder.getDeliveryAddress().getLongitude(),
                    R.drawable.ic_home_icon,
                    true);
        }
    }

    public void addDriverMarker(double lat, double lng, boolean shouldAddInMarkerList) {
        if (!mIsOrderDelivered) {
            //Remove previous driver marker on the map.
            if (mDriverMarker != null) {
                mDriverMarker.setPosition(new LatLng(lat, lng));
            } else {
                mDriverMarker = addMarker("addDriverMarker",
                        lat,
                        lng,
                        R.drawable.ic_driver_icon,
                        shouldAddInMarkerList);
            }
        }
    }

    private void zoomToAllAddedMarker(boolean isFromUpdateStatus) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : mMarkerList) {
            builder.include(marker.getPosition());
        }
        int width = mContext.getResources().getDisplayMetrics().widthPixels;
        int height = mContext.getResources().getDisplayMetrics().heightPixels;
        LatLngBounds bounds = builder.build();
        if (isFromUpdateStatus) {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } else {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, (int) (height * 0.3), 100));
        }
    }

    private void requestNecessaryForDeliveringStatus() {
        showLoading(true);
        requestDriverLocation(new OnCallbackListener<Response<ConciergeDriverLocationResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showError(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<ConciergeDriverLocationResponse> result) {
                hideLoading();
                if (isValidDriverLocation(result.body())) {
                    addDriverMarker(result.body().getLatitude(), result.body().getLongitude(), true);
                }
                zoomToAllAddedMarker(false);
                if (mListener != null) {
                    mListener.onShouldSetupBottomSheet();
                }
                initRequestDriverLocationTimer();
            }
        });
    }

    private void initOrderInfoView() {
        mOrderNumber.set(mContext.getString(R.string.shop_order_list_order_number_title) + " #" + mOrder.getOrderNumber());
        mOrderTotalPrice.set(mContext.getString(R.string.shop_order_list_total_title) + " " + ConciergeHelper.getDisplayPrice(mContext, mOrder.getGrandTotal()));
        OffsetDateTime offsetDateTime = OffsetDateTime.parse(mOrder.getCreatedAt().toInstant().toString());
        if (mOrder.getCreatedAt() != null) {
            String dateWithFormat = DateUtils.getDateWithFormat(mContext,
                    mOrder.getCreatedAt().getTime(),
                    DateUtils.DATE_FORMAT_23,
                    LocaleManager.getLocale(mContext.getResources()));
            mOrderDate.set(dateWithFormat);
        }
    }

    public void onCancelOrderClicked() {
        Timber.i("On cancel order clicked!");
    }

    private void requestDriverLocation(OnCallbackListener<Response<ConciergeDriverLocationResponse>> callbackListener) {
        Observable<Response<ConciergeDriverLocationResponse>> observable = mDataManager.getDriverLocation();
        ResponseObserverHelper<Response<ConciergeDriverLocationResponse>> helper = new ResponseObserverHelper<>(getContext(), observable);
        Disposable disposable = helper.execute(new OnCallbackListener<Response<ConciergeDriverLocationResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (callbackListener != null) {
                    callbackListener.onFail(ex);
                }
            }

            @Override
            public void onComplete(Response<ConciergeDriverLocationResponse> result) {
                //Will remove driver marker if the order status updated to delivered.
                if (result.body() != null &&
                        result.body().getCurrentOrderStatus() != null &&
                        result.body().getCurrentOrderStatus().getStatus() == ConciergeOrderStatus.DELIVERED &&
                        mDriverMarker != null) {
                    mIsOrderDelivered = true;
                    mCountDownTimer.cancel();
                    removeDriverLocation();
                }

                if (mListener != null) {
                    mListener.onRequestDriverLocationSuccess(result.body());
                }
                if (callbackListener != null) {
                    callbackListener.onComplete(result);
                }
            }
        });
        addDisposable(disposable);
        mDisposableList.add(disposable);
    }

    private void removeDriverLocation() {
        if (mDriverMarker != null) {
            mDriverMarker.remove();
            mDriverMarker = null;
        }
    }

    @Override
    public void onAddressChanged(String newAddress) {

    }

    public interface ConciergeDeliveryTrackingListener {
        void onDataSuccess(List<ConciergeOrderTrackingProgress> progressList,
                           ConciergeOrderStatus currentStatus);

        void onShouldSetupBottomSheet();

        void onRequestDriverLocationSuccess(ConciergeDriverLocationResponse response);
    }
}
