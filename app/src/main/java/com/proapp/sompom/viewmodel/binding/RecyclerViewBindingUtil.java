package com.proapp.sompom.viewmodel.binding;

import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.adapter.newadapter.ErrorAdapterAdapter;
import com.proapp.sompom.adapter.newadapter.PlaceHolderAdapter;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.model.emun.ErrorLoadingType;

import timber.log.Timber;

/**
 * Created by nuonveyo on 6/28/18.
 */

public final class RecyclerViewBindingUtil {
    private RecyclerViewBindingUtil() {

    }

    @BindingAdapter({"showPlaceHolderAdapter"})
    public static void showPlaceHolderAdapter(RecyclerView recyclerView, boolean isShow) {
        if (isShow) {
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
            PlaceHolderAdapter placeHolderAdapter = new PlaceHolderAdapter();
            recyclerView.setAdapter(placeHolderAdapter);
        } else {
            if (recyclerView.getAdapter() instanceof PlaceHolderAdapter) {
                recyclerView.setAdapter(null);
            }
        }
    }

    @BindingAdapter(value = {"showErrorType",
            "isShowErrorAdapter",
            "errorTitle",
            "errorMessage",
            "onRetryButtonClickListener",
            "isShowButtonRetry"},
            requireAll = false)
    public static void showErrorAdapter(RecyclerView recyclerView,
                                        ErrorLoadingType errorLoadingType,
                                        boolean isShow,
                                        String errorTitle,
                                        String errorMessage,
                                        OnClickListener onClickListener,
                                        boolean isShowButtonRetry) {
        if (isShow) {
            if (recyclerView.getVisibility() == View.GONE || recyclerView.getVisibility() == View.INVISIBLE) {
                recyclerView.setVisibility(View.VISIBLE);
            }
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
            ErrorAdapterAdapter adapter = new ErrorAdapterAdapter(errorLoadingType,
                    errorTitle,
                    errorMessage,
                    isShowButtonRetry,
                    onClickListener);
            recyclerView.setAdapter(adapter);
            Timber.e("showErrorAdapter show");
        } else {
            if (recyclerView.getAdapter() instanceof ErrorAdapterAdapter) {
                recyclerView.setAdapter(null);
                Timber.e("showErrorAdapter null");

            }
        }
    }

}
