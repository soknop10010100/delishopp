package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.services.datamanager.InputLoginPasswordDataManager;

/**
 * Created by Veasna Chhom on 4/21/21.
 */
public class InputLoginPasswordViewModel extends AbsLoginViewModel<InputLoginPasswordDataManager, AbsLoginViewModel.AbsLoginViewModelListener> {

    public InputLoginPasswordViewModel(Context context,
                                       InputLoginPasswordDataManager dataManager,
                                       AbsLoginViewModel.AbsLoginViewModelListener listener) {
        super(context, dataManager, listener);
    }

    @Override
    protected boolean shouldEnableActionButton() {
        return isPasswordValid();
    }

    @Override
    protected void onContinueButtonClicked() {
        requestLogin(mDataManager.getLoginService(mPassword.get()), mDataManager.isLogInWithPhone());
    }

    private boolean isPasswordValid() {
        return !TextUtils.isEmpty(mPassword.get());
    }
}
