package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.view.View;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeBrand;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.ConciergeFilterProductCriteriaDataManager;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Or Vitovongsak on 5/5/22.
 */
public class ConciergeFilterProductCriteriaBottomSheetDialogViewModel extends AbsLoadingViewModel {

    private final ObservableBoolean mIsShowOutOfStock = new ObservableBoolean();
    private ConciergeFilterProductCriteriaDataManager mDataManager;
    private ConciergeFilterProductCriteriaBottomSheetDialogViewModelCallBack mCallback;

    public ConciergeFilterProductCriteriaBottomSheetDialogViewModel(Context context,
                                                                    ConciergeFilterProductCriteriaDataManager dataManager,
                                                                    ConciergeFilterProductCriteriaBottomSheetDialogViewModelCallBack callback) {
        super(context);
        mDataManager = dataManager;
        mCallback = callback;
        mIsShowOutOfStock.set(ConciergeHelper.getShowOutOfStockProduct());
        requestCriteria();
    }

    public ObservableBoolean getIsShowOutOfStock() {
        return mIsShowOutOfStock;
    }

    public void onClearButtonClicked(View view) {
        ConciergeHelper.setShowOutOfStockProduct(false);
        if (mCallback != null) {
            mCallback.onClearFilterCriteria();
        }
    }

    public void onSaveButtonClicked(View view) {
        ConciergeHelper.setShowOutOfStockProduct(mIsShowOutOfStock.get());
        if (mCallback != null) {
            mCallback.onSaveFilterCriteria();
        }
    }

    @Override
    public void onRetryClick() {
        requestCriteria();
    }

    private void requestCriteria() {
        showLoading();
        Observable<Response<List<ConciergeBrand>>> observable = mDataManager.getConciergeBrand();
        BaseObserverHelper<Response<List<ConciergeBrand>>> helper = new BaseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<ConciergeBrand>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showError(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<List<ConciergeBrand>> result) {
                hideLoading();
                if (mCallback != null) {
                    mCallback.onLoadCriteriaSuccess(result.body());
                }
            }
        }));
    }

    public interface ConciergeFilterProductCriteriaBottomSheetDialogViewModelCallBack {

        void onClearFilterCriteria();

        void onSaveFilterCriteria();

        void onLoadCriteriaSuccess(List<ConciergeBrand> data);
    }
}
