package com.proapp.sompom.viewmodel.newviewmodel;

import static com.proapp.sompom.newui.ChatActivity.REFRESH_NOTIFICATION_BADGE_ACTION;

import android.content.Intent;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.ListResponseWrapper;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.AllMessageDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class AllMessageFragmentViewModel extends AbsLoadingViewModel {
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private final OnCallback mOnCallback;
    private final AllMessageDataManager mDataManager;
    private boolean mIsFirstStart;
    private boolean mIsSilentRefresh;

    public AllMessageFragmentViewModel(AllMessageDataManager dataManager, OnCallback onCallback) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mOnCallback = onCallback;
    }

    public OnClickListener onButtonRetryClick() {
        return () -> getRecentlyMessage(false);
    }

    public void onRefresh() {
        Timber.i("onRefresh");
        mIsRefresh.set(true);
        refreshMessage();
    }

    public void silentRefreshMessage() {
        Timber.i("silentRefreshMessage");
        mIsSilentRefresh = true;
        refreshMessage();
    }

    public int getDefaultRecentConversationDisplaySize() {
        return mDataManager.getDefaultRecentConversationDisplaySize();
    }

    private void refreshMessage() {
        SendBroadCastHelper.verifyAndSendBroadCast(mDataManager.getContext(), new Intent(REFRESH_NOTIFICATION_BADGE_ACTION));
        mDataManager.resetNextPage();
        int limitSize = mOnCallback.existItemCount();
        if (limitSize < AllMessageDataManager.DEFAULT_PAGINATION_SIZE) {
            limitSize = AllMessageDataManager.DEFAULT_PAGINATION_SIZE;
        }
        mDataManager.setPaginationSize(limitSize);
        getRecentlyMessage(true);
    }

    public void getRecentlyMessage(boolean isRefresh) {
        if (!mIsRefresh.get() && !mIsFirstStart && !mIsSilentRefresh) {
            Timber.d("getFromApi and is show loading");
            showLoading();
        }

        if (mIsFirstStart) {
            mIsFirstStart = false;
        }
        Timber.i("Getting chat from API");

        Observable<ListResponseWrapper<Conversation>> ob = mDataManager.getAllMessageList(isRefresh);
        BaseObserverHelper<ListResponseWrapper<Conversation>> helper = new BaseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(new Callback()));
    }

    public void getConversationDetail(String conversationId, OnCallbackListener<Response<Conversation>> listener) {
        Observable<Response<Conversation>> ob = mDataManager.getConversationDetail(conversationId);
        BaseObserverHelper<Response<Conversation>> helper = new BaseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(listener));
    }

    public void reloadRecentlyMessage() {
        mDataManager.resetNextPage();
        mIsRefresh.set(true);
        getRecentlyMessage(true);
    }

    public void getRecentlyMessageFromDb() {
        Timber.i("Getting data from DB for item type: " + mDataManager.getItemType().getValue());
        showLoading();
        mIsFirstStart = true;

        Observable<ListResponseWrapper<Conversation>> ob = mDataManager.getAllConversationListFromDb();
        BaseObserverHelper<ListResponseWrapper<Conversation>> helper = new BaseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(new OnCallbackListener<ListResponseWrapper<Conversation>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.i("No data from DB");
                getRecentlyMessage(false);
            }

            @Override
            public void onComplete(ListResponseWrapper<Conversation> result) {
                if (result.isEmpty()) {
                    Timber.i("No data from DB");
                    getRecentlyMessage(false);
                    return;
                }
                Timber.i("Adding data from DB");
//                Timber.i(new Gson().toJson(result.getData()));
                hideLoading();
                mOnCallback.getGetRecentMessageSuccess(result.getData(), mDataManager.isCanLoadMore());
                getRecentlyMessage(true);
            }
        }));
    }

    public void loadMore(OnCallbackListListener<List<Conversation>> listListener) {
        Observable<ListResponseWrapper<Conversation>> observable = mDataManager.loadMore();
        BaseObserverHelper<ListResponseWrapper<Conversation>> helper = new BaseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<ListResponseWrapper<Conversation>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                listListener.onFail(ex);
            }

            @Override
            public void onComplete(ListResponseWrapper<Conversation> result) {
                if (result.isError()) {
                    showSnackBar(result.isNoConnectionError() ? R.string.error_internet_connection_description :
                                    R.string.error_general_description,
                            true);
                } else {
                    listListener.onComplete(result.getData(), mDataManager.isCanLoadMore());
                }
            }
        }));
    }

    public interface OnCallback {
        void getGetRecentMessageSuccess(List<Conversation> conversations, boolean isCanLoadMore);

        boolean isHasData();

        int existItemCount();
    }

    private class Callback implements OnCallbackListener<ListResponseWrapper<Conversation>> {

        @Override
        public void onFail(ErrorThrowable ex) {
            mIsRefresh.set(false);
            if (mOnCallback.isHasData() || mIsSilentRefresh) {
                // Fail quietly if it's a silent refresh and reset silent refresh status
                mIsSilentRefresh = false;
                return;
            }
            mIsError.set(true);
            if (ex.getCode() == 404) {
                showEmptyDataError(getContext().getString(R.string.conversation_empty_title),
                        getContext().getString(R.string.conversation_empty_message));
            } else {
                showError(ex.toString());
            }
        }

        @Override
        public void onComplete(ListResponseWrapper<Conversation> result) {
            if (result.isError() && mIsRefresh.get()) {
                mIsRefresh.set(false);
                showSnackBar(result.isNoConnectionError() ? R.string.error_internet_connection_description :
                                R.string.error_general_description,
                        true);
            } else {
                if (result.isEmpty()) {
                    onFail(new ErrorThrowable(404, getContext().getString(R.string.error_no_data)));
                    return;
                }
                // Reset silent refresh status
                mIsSilentRefresh = false;
                mIsRefresh.set(false);
                hideLoading();
                Timber.i("Adding data from API");
//                Timber.i(new Gson().toJson(result));
                mOnCallback.getGetRecentMessageSuccess(result.getData(), mDataManager.isCanLoadMore());
            }
        }
    }
}
