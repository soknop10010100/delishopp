package com.proapp.sompom.viewmodel.newviewmodel;

import com.proapp.sompom.adapter.ProductPreviewAdapter;
import com.proapp.sompom.model.Media;

/**
 * Created by He Rotha on 9/26/17.
 */

public class ProductPreviewViewModel {
    private String mUrl;
    private ProductPreviewAdapter.OnProductItemClickListener mListener;

    public ProductPreviewViewModel(Media url,
                                   ProductPreviewAdapter.OnProductItemClickListener listener) {
        mUrl = url.getUrl();
        mListener = listener;
    }

    public String getUrl() {
        return mUrl;
    }

    public void onItemEditClick() {
        mListener.onItemEditClick();
    }
}
