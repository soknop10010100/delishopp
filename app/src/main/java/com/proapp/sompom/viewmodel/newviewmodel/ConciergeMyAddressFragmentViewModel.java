package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.intent.newintent.ConciergeNewAddressIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.emun.CitySelectionType;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.ConciergeMyAddressDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;
import com.sompom.baseactivity.ResultCallback;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Or Vitovongsak on 22/11/21.
 */
public class ConciergeMyAddressFragmentViewModel extends AbsLoadingViewModel {

    private final ObservableBoolean mIsRefreshing = new ObservableBoolean();
    private final ObservableBoolean mIsEmpty = new ObservableBoolean();

    private ConciergeMyAddressDataManager mDataManager;
    private ConciergeMyAddressFragmentViewModelListener mListener;
    private CitySelectionType mAddressTypeToShow;

    public ConciergeMyAddressFragmentViewModel(Context context,
                                               CitySelectionType addressTypeToShow,
                                               ConciergeMyAddressDataManager dataManager,
                                               ConciergeMyAddressFragmentViewModelListener listener) {
        super(context);
        mAddressTypeToShow = addressTypeToShow;
        mDataManager = dataManager;
        mListener = listener;
    }

    public ObservableBoolean getIsRefreshing() {
        return mIsRefreshing;
    }

    public ObservableBoolean getIsEmpty() {
        return mIsEmpty;
    }

    public void loadMore() {
        loadUserAddresses(false, true);
    }

    public void loadUserAddresses(boolean isRefresh, boolean isLoadMore) {
        if (!isLoadMore && !isRefresh) {
            showLoading();
        }
        Observable<List<ConciergeUserAddress>> ob = mDataManager.getUserAddresses(isRefresh, isLoadMore);
        BaseObserverHelper<List<ConciergeUserAddress>> helper = new BaseObserverHelper<>(getContext(), ob);
        addDisposable(helper.execute(new OnCallbackListener<List<ConciergeUserAddress>>() {
            @Override
            public void onComplete(List<ConciergeUserAddress> result) {
                hideLoading();
                if (isRefresh) {
                    mIsRefreshing.set(false);
                }

                if (!isLoadMore) {
                    showEmptyView(result.isEmpty());
                }

                if (mAddressTypeToShow != null) {
                    if (mAddressTypeToShow == CitySelectionType.PHNOM_PENH) {
                        List<ConciergeUserAddress> toReturn = new ArrayList<>();
                        result.forEach(address -> {
                            if (CitySelectionType.fromValue(address.getCity()) == CitySelectionType.PHNOM_PENH) {
                                toReturn.add(address);
                            }
                        });
                        if (mListener != null) {
                            mListener.onLoadUserAddress(toReturn, isLoadMore, mDataManager.isCanLoadMore());
                        }
                    } else if (mAddressTypeToShow == CitySelectionType.PROVINCE) {
                        List<ConciergeUserAddress> toReturn = new ArrayList<>();
                        result.forEach(address -> {
                            if (CitySelectionType.fromValue(address.getCity()) == CitySelectionType.PROVINCE) {
                                toReturn.add(address);
                            }
                        });
                        if (mListener != null) {
                            mListener.onLoadUserAddress(toReturn, isLoadMore, mDataManager.isCanLoadMore());
                        }
                    }
                } else {
                    if (mListener != null) {
                        mListener.onLoadUserAddress(result, isLoadMore, mDataManager.isCanLoadMore());
                    }
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showEmptyView(false);
                if (isRefresh) {
                    mIsRefreshing.set(false);
                }
                showError(ex.getMessage(), true);
            }
        }));
    }

    public void onRefresh() {
        mIsRefreshing.set(true);
        loadUserAddresses(true, false);
    }

    @Override
    public void onRetryClick() {
        loadUserAddresses(false, false);
    }

    public void showEmptyView(boolean isEmpty) {
        mIsEmpty.set(isEmpty);
    }

    public void onAddNewAddressClick(View view) {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).startActivityForResult(new ConciergeNewAddressIntent(mContext,
                            mListener.getAddressCount() > 0),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            ConciergeUserAddress newAddress = data.getParcelableExtra(ConciergeNewAddressIntent.ADDRESS_DATA);
                            if (newAddress != null) {
                                // If the address type to show is not null, only add the new address to list if it's the same type
                                if (mAddressTypeToShow != null) {
                                    if (mAddressTypeToShow == CitySelectionType.fromValue(newAddress.getCity())) {
                                        showEmptyView(false);
                                        if (mListener != null) {
                                            mListener.onNewAddressAdded(newAddress);
                                        }
                                    }
                                } else {
                                    showEmptyView(false);
                                    if (mListener != null) {
                                        mListener.onNewAddressAdded(newAddress);
                                    }
                                }
                            }
                        }
                    });
        }
    }

    public interface ConciergeMyAddressFragmentViewModelListener {
        void onLoadUserAddress(List<ConciergeUserAddress> addresses, boolean isFromLoadMore, boolean isCanLoadMore);

        void onNewAddressAdded(ConciergeUserAddress newAddress);

        int getAddressCount();
    }
}
