package com.proapp.sompom.viewmodel.preview;

import android.text.TextUtils;

import androidx.databinding.ObservableField;

import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;
import com.proapp.sompom.widget.lifestream.MediaLayout;

/**
 * Created by Chhom Veasna on 05/11/2020.
 */
public abstract class AbsItemLinkPreviewViewModel extends AbsBaseViewModel {

    private String mOriginalPreviewUrl;
    private ObservableField<String> mPreviewMediaUrl = new ObservableField<>();
    private ObservableField<String> mPreviewTitle = new ObservableField<>();
    private ObservableField<String> mPreviewDescription = new ObservableField<>();
    protected ItemLinkPreviewViewModelListener mListener;
    private PreviewContext mPreviewContext;
    protected LinkPreviewModel mLinkPreviewModel;

    public AbsItemLinkPreviewViewModel(LinkPreviewModel linkPreviewModel, PreviewContext previewContext) {
        mLinkPreviewModel = linkPreviewModel;
        mPreviewContext = previewContext;
        mOriginalPreviewUrl = linkPreviewModel.getLink();
        mPreviewTitle.set(LinkPreviewRetriever.getPreviewTitle(linkPreviewModel));
        mPreviewDescription.set(LinkPreviewRetriever.getPreviewDescription(linkPreviewModel));
        setPreviewMediaUrl(linkPreviewModel.getImage());
    }

    public PreviewContext getPreviewContext() {
        return mPreviewContext;
    }

    public String getOriginalPreviewUrl() {
        return mOriginalPreviewUrl;
    }

    protected void setPreviewMediaUrl(String url) {
        mPreviewMediaUrl.set(TextUtils.isEmpty(url) ? "" : url);
    }

    public ObservableField<String> getPreviewTitle() {
        return mPreviewTitle;
    }

    public void setListener(ItemLinkPreviewViewModelListener listener) {
        mListener = listener;
    }

    public ObservableField<String> getPreviewDescription() {
        return mPreviewDescription;
    }

    public ObservableField<String> getPreviewMediaUrl() {
        return mPreviewMediaUrl;
    }

    protected void updateCrossVisibility(boolean isVisible) {
        if (mListener != null) {
            mListener.updateCrossVisibility(isVisible);
        }
    }

    public interface ItemLinkPreviewViewModelListener {
        void updateCrossVisibility(boolean isVisible);

        void onBindVideoLayout(MediaLayout videoLayout);
    }

    public enum PreviewContext {
        CREATE_FEED,
        HOME_FEED
    }
}
