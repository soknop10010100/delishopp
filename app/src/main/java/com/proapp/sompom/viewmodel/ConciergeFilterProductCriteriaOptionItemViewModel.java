package com.proapp.sompom.viewmodel;

import android.content.Context;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.model.concierge.ConciergeBrand;

/**
 * Created by Veasna Chhom on 5/6/22.
 */
public class ConciergeFilterProductCriteriaOptionItemViewModel extends AbsLoadingViewModel {

    private final ObservableField<String> mTitle = new ObservableField<>();
    private final ObservableBoolean mIsSelected = new ObservableBoolean();
    private ConciergeBrand mConciergeBrand;

    public ConciergeFilterProductCriteriaOptionItemViewModel(Context context, ConciergeBrand conciergeBrand) {
        super(context);
        mConciergeBrand = conciergeBrand;
        mTitle.set(mConciergeBrand.getName());
        mIsSelected.set(conciergeBrand.isSelected());
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    public ObservableBoolean getIsSelected() {
        return mIsSelected;
    }

    public void onItemClicked() {
        mIsSelected.set(!mIsSelected.get());
    }

    public ConciergeBrand getConciergeBrand() {
        return mConciergeBrand;
    }
}
