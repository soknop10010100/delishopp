package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.UiThread;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ItemVideoLinkPreviewBinding;
import com.proapp.sompom.databinding.ListItemDefaultLinkPreviewBinding;
import com.proapp.sompom.databinding.ListItemFileLinkPreviewBinding;
import com.proapp.sompom.helper.DelayViewClickListener;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.BasePreviewModel;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.utils.CopyTextUtil;
import com.proapp.sompom.utils.GlideLoadUtil;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;
import com.proapp.sompom.viewmodel.preview.AbsItemLinkPreviewViewModel;
import com.proapp.sompom.viewmodel.preview.ItemDefaultLinkPreviewViewModel;
import com.proapp.sompom.viewmodel.preview.ItemFileLinkPreviewViewModel;
import com.proapp.sompom.viewmodel.preview.ItemVideoLinkPreviewViewModel;
import com.proapp.sompom.widget.lifestream.MediaLayout;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 10/30/2019.
 */
public class ListItemMainLinkPreviewViewModel extends AbsBaseViewModel {

    private static final float ADAPTIVE_HEIGHT_PERCENTAGE = 0.50f; //50%

    private ObservableField<LinkPreviewModel> mPreviewModel = new ObservableField<>();
    private ObservableField<String> mPreviewUrl = new ObservableField<>();
    private ListItemMainLinkPreviewViewModelListener mListener;
    private OnTimelineItemButtonClickListener mTimelineItemButtonClickListener;
    private ObservableBoolean mIsInDetail = new ObservableBoolean();
    private ObservableBoolean mIsRedirectOnClick = new ObservableBoolean();

    public ListItemMainLinkPreviewViewModel(LinkPreviewModel previewModel,
                                            boolean isInDetail,
                                            OnTimelineItemButtonClickListener timelineItemButtonClickListener,
                                            ListItemMainLinkPreviewViewModelListener listener) {
        mIsInDetail.set(isInDetail);
        mTimelineItemButtonClickListener = timelineItemButtonClickListener;
        mListener = listener;
        mPreviewModel.set(previewModel);
        if (previewModel != null) {
            mPreviewUrl.set(previewModel.getLink());
        }
        mIsRedirectOnClick.set(isDefaultLinkPreview());
    }

    private boolean isDefaultLinkPreview() {
        if (mPreviewModel.get() != null) {
            BasePreviewModel.PreviewType type = BasePreviewModel.PreviewType.getFromValue(mPreviewModel.get().getType());
            return (type == BasePreviewModel.PreviewType.LINK ||
                    (type == BasePreviewModel.PreviewType.FILE &&
                            BasePreviewModel.FilePreviewType.getFromValue(mPreviewModel.get().getFileType())
                                    == BasePreviewModel.FilePreviewType.IMAGE));
        }

        return false;
    }

    public ObservableBoolean getIsRedirectOnClick() {
        return mIsRedirectOnClick;
    }

    public ObservableField<String> getPreviewUrl() {
        return mPreviewUrl;
    }

    public ObservableBoolean getIsInDetail() {
        return mIsInDetail;
    }

    public ListItemMainLinkPreviewViewModelListener getListener() {
        return videoLayout -> {
            Timber.i("videoLayout: " + videoLayout);
            if (mListener != null) {
                mListener.onBindVideoLayout(videoLayout);
            }
        };
    }

    public OnTimelineItemButtonClickListener getTimelineItemButtonClickListener() {
        return mTimelineItemButtonClickListener;
    }

    public ObservableField<LinkPreviewModel> getPreviewModel() {
        return mPreviewModel;
    }

    public interface ListItemMainLinkPreviewViewModelListener {
        void onBindVideoLayout(MediaLayout videoLayout);
    }

    @BindingAdapter({"previewModel", "listener", "timelineListener", "isDetail"})
    public static void bindPreview(LinearLayout previewLayout,
                                   LinkPreviewModel previewModel,
                                   ListItemMainLinkPreviewViewModelListener listener,
                                   OnTimelineItemButtonClickListener onTimelineItemButtonClickListener,
                                   boolean isDetail) {
//        Timber.i("previewLayout: " + previewLayout.hashCode());
        if (previewModel != null) {
            removePreviousPreviewVideoLayout(previewLayout);
//            previewLayout.removeAllViews();
            BasePreviewModel.PreviewType type = BasePreviewModel.PreviewType.getFromValue(previewModel.getType());
            if (type == BasePreviewModel.PreviewType.LINK ||
                    (type == BasePreviewModel.PreviewType.FILE &&
                            BasePreviewModel.FilePreviewType.getFromValue(previewModel.getFileType())
                                    == BasePreviewModel.FilePreviewType.IMAGE)) {
                ViewDataBinding viewDataBinding = getViewDataBinding(previewLayout);
                if (viewDataBinding != null &&
                        !(viewDataBinding instanceof ListItemDefaultLinkPreviewBinding)) {
                    previewLayout.removeView(viewDataBinding.getRoot());
                    viewDataBinding = null;
                }

                if (viewDataBinding == null) {
                    viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(previewLayout.getContext()),
                            R.layout.list_item_default_link_preview,
                            null,
                            false);
                    previewLayout.addView(viewDataBinding.getRoot());
                }
                viewDataBinding.setVariable(BR.viewModel,
                        new ItemDefaultLinkPreviewViewModel(previewModel,
                                AbsItemLinkPreviewViewModel.PreviewContext.HOME_FEED));
            } else if (type == BasePreviewModel.PreviewType.FILE &&
                    BasePreviewModel.FilePreviewType.getFromValue(previewModel.getFileType()) == BasePreviewModel.FilePreviewType.VIDEO) {
                ViewDataBinding viewDataBinding = getViewDataBinding(previewLayout);
                if (viewDataBinding != null &&
                        !(viewDataBinding instanceof ItemVideoLinkPreviewBinding)) {
                    previewLayout.removeView(viewDataBinding.getRoot());
                    viewDataBinding = null;
                }

                if (viewDataBinding == null) {
                    viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(previewLayout.getContext()),
                            R.layout.item_video_link_preview,
                            null,
                            false);
                    previewLayout.addView(viewDataBinding.getRoot());
                }

                ItemVideoLinkPreviewViewModel viewModel = new ItemVideoLinkPreviewViewModel(previewModel,
                        AbsItemLinkPreviewViewModel.PreviewContext.HOME_FEED);
                viewModel.setListener(new AbsItemLinkPreviewViewModel.ItemLinkPreviewViewModelListener() {
                    @Override
                    public void updateCrossVisibility(boolean isVisible) {

                    }

                    @Override
                    public void onBindVideoLayout(MediaLayout videoLayout) {
//                        Timber.i("onBindVideoLayout: " + videoLayout.hashCode());
                        videoLayout.setInDetailPostScreen(isDetail);
                        if (listener != null) {
                            listener.onBindVideoLayout(videoLayout);
                        }
                    }
                });
                viewDataBinding.setVariable(BR.viewModel,
                        viewModel);
            } else if (type == BasePreviewModel.PreviewType.FILE &&
                    BasePreviewModel.FilePreviewType.getFromValue(previewModel.getFileType()) == BasePreviewModel.FilePreviewType.FILE) {
                //File link preview type
                ViewDataBinding viewDataBinding = getViewDataBinding(previewLayout);
                if (viewDataBinding != null &&
                        !(viewDataBinding instanceof ListItemFileLinkPreviewBinding)) {
                    previewLayout.removeView(viewDataBinding.getRoot());
                    viewDataBinding = null;
                }

                if (viewDataBinding == null) {
                    viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(previewLayout.getContext()),
                            R.layout.list_item_file_link_preview,
                            null,
                            false);
                    previewLayout.addView(viewDataBinding.getRoot());
                }
                viewDataBinding.setVariable(BR.viewModel,
                        new ItemFileLinkPreviewViewModel(previewModel,
                                AbsItemLinkPreviewViewModel.PreviewContext.HOME_FEED,
                                onTimelineItemButtonClickListener));
            }
        }
    }

    private static ViewDataBinding getViewDataBinding(ViewGroup containerView) {
//        Timber.i("containerView child size: " + containerView.getChildCount());
        if (containerView.getChildCount() > 0) {
            return DataBindingUtil.getBinding(containerView.getChildAt(0));
        }

        return null;
    }

    private static void removePreviousPreviewVideoLayout(LinearLayout previewLayout) {
        if (previewLayout != null) {
            for (int i = 0; i < previewLayout.getChildCount(); i++) {
                ViewDataBinding binding = DataBindingUtil.getBinding(previewLayout.getChildAt(i));
                if (binding instanceof ItemVideoLinkPreviewBinding) {
//                    Timber.i("Clear previous preview video layout.");
                    ((ItemVideoLinkPreviewBinding) binding).mediaLayoutView.release();
                }
            }
        }
    }

    @BindingAdapter("enableAdaptiveLinkPreviewHeight")
    public static void setEnableAdaptiveLinkPreviewHeight(View view, boolean enableAdaptiveHeight) {
        if (enableAdaptiveHeight) {
            view.getLayoutParams().height = (int) (Resources.getSystem().getDisplayMetrics().widthPixels *
                    ADAPTIVE_HEIGHT_PERCENTAGE);
        }
    }

    @UiThread
    @BindingAdapter("previewLinkLogo")
    public static void bindPreviewLinkLogo(ImageView imageView, String url) {
        Drawable placeHolder = ContextCompat.getDrawable(imageView.getContext(),
                R.drawable.ic_photo_placeholder_200dp);
        imageView.setImageDrawable(placeHolder);
        if (!TextUtils.isEmpty(url)) {
            GlideLoadUtil.loadDefaultLinkPreviewLogo(imageView,
                    url,
                    placeHolder,
                    new GlideLoadUtil.GlideLoadUtilListener() {
                        @Override
                        public void onLoadSuccess(Drawable resource, boolean isSVG) {
                            if (resource != null) {
                                WallStreetHelper.calculatePostHeight(new Point(resource.getIntrinsicWidth(),
                                                resource.getIntrinsicHeight()),
                                        (desireWidth, desiredHeight, scaleType) -> {
                                            Timber.i("scaleType: " + scaleType + ", desireWidth: "
                                                    + desireWidth + ", desiredHeight: " + desiredHeight);
                                            if (imageView.getContext() instanceof AppCompatActivity) {
                                                ((AppCompatActivity) imageView.getContext()).runOnUiThread(() -> {
                                                    imageView.getLayoutParams().width = desireWidth;
                                                    imageView.getLayoutParams().height = desiredHeight;
                                                    Timber.i("bindPreviewLinkLogo of url: " + url);
                                                    GlideLoadUtil.loadLinkPreviewResource(imageView,
                                                            url,
                                                            resource,
                                                            placeHolder,
                                                            scaleType == ImageView.ScaleType.CENTER_CROP);
                                                });
                                            }
                                        });
                            }
                        }
                    });
        }
    }

    @BindingAdapter({"previewLongPressUrl", "isRedirectOnClick"})
    public static void setLongPressOnContainerView(View containerView, String url, boolean isRedirectOnClick) {
        containerView.setOnLongClickListener(v -> {
            CopyTextUtil.copyTextToClipboard(containerView.getContext(), url);
            return true;
        });
        containerView.setOnClickListener(new DelayViewClickListener() {
            @Override
            public void onDelayClick(View v) {
                if (isRedirectOnClick) {
                    WallStreetHelper.openCustomTab(containerView.getContext(), url);
                }
            }
        });
    }
}
