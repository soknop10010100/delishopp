package com.proapp.sompom.viewmodel.newviewmodel;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.database.MessageDb;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.listener.OnChatItemListener;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.SelectedChat;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.utils.ChatUtility;
import com.proapp.sompom.utils.DownloaderUtils;
import com.proapp.sompom.utils.MediaUtil;
import com.proapp.sompom.viewmodel.ChatMediaViewModel;

import timber.log.Timber;

public class FileChatViewModel extends ChatMediaViewModel {

    private ObservableField<String> mName = new ObservableField<>();
    private ObservableField<String> mExtension = new ObservableField<>();
    private ObservableField<String> mDisplaySize = new ObservableField<>();
    private ObservableInt mProgression = new ObservableInt();
    private ObservableInt mFileStatus = new ObservableInt();
    private ObservableInt mFileExtensionVisibility = new ObservableInt(View.INVISIBLE);

    private Media mMedia;

    public FileChatViewModel(Activity context,
                             Conversation conversation,
                             Chat chat,
                             ChatUtility.GroupMessage groupMessage,
                             int position,
                             SelectedChat selectChat,
                             String searchKeyWord,
                             String filename,
                             OnChatItemListener onItemClickListener) {
        super(context, conversation, chat, groupMessage, position, selectChat, searchKeyWord, onItemClickListener);
        mName.set(filename);
        /*
          For now, the file message supports only one media.
         */
        bindFileStatus();
    }

    public ObservableInt getFileExtensionVisibility() {
        return mFileExtensionVisibility;
    }

    public void bindFileStatus() {
        if (mChat.getMediaList() != null && !mChat.getMediaList().isEmpty()) {
            mMedia = mChat.getMediaList().get(0);
            Timber.i("File size: " + mMedia.getSize());
            checkUpdateDownloadedFileStatus();
            if (TextUtils.isEmpty(mMedia.getFormat())) {
                mFileExtensionVisibility.set(View.INVISIBLE);
            } else {
                mFileExtensionVisibility.set(View.VISIBLE);
                mExtension.set(mMedia.getFormat());
            }
            mFileStatus.set(mMedia.getFileStatusType().ordinal());
            Media.FileStatusType fileStatusType = mMedia.getFileStatusType();
            Timber.i("fileStatusType: " + fileStatusType.name());
            if (fileStatusType == Media.FileStatusType.DOWNLOADED ||
                    fileStatusType == Media.FileStatusType.UPLOADED ||
                    fileStatusType == Media.FileStatusType.RETRY_UPLOAD ||
                    fileStatusType == Media.FileStatusType.IDLE) {
                mDisplaySize.set(formatSize(mMedia.getSize()));
            } else {
                //The file is being uploaded or downloaded
                //Init first progression display like: 0 of 10 KB.
                updateFileProgression(0, mMedia.getSize());
            }
        }
    }

    @Override
    public void onRetryClick() {
        //On retry uploading media
        mMedia.setFileStatusType(Media.FileStatusType.UPLOADING);
        super.onRetryClick();
    }

    private String getFileProgressionDisplay(long progressBytes, long totalBytes) {
        return mContext.getString(R.string.chat_message_file_upload_download_progress,
                ChatHelper.getDisplaySizeWithFormat(progressBytes),
                ChatHelper.getDisplaySizeWithFormat(totalBytes));
    }

    private void checkUpdateDownloadedFileStatus() {
        mMedia = MediaUtil.checkIfMediaExist(mMedia);
    }

    public void onUploadOrDownloadSuccess(boolean isUpload,
                                          String requestId,
                                          Chat chat,
                                          String mediaId,
                                          String imageUrl,
                                          String thumb) {
        Timber.i("onUploadOrDownloadSuccess: isUpload: " + isUpload);
        mProgression.set(100);
        if (isUpload) {
            updateFileStatus(Media.FileStatusType.UPLOADED);
            mDisplaySize.set(formatSize(mMedia.getSize()));
        } else {
            updateFileStatus(Media.FileStatusType.DOWNLOADED);
            mDisplaySize.set(formatSize(mMedia.getSize()));
            //Save downloaded path locally
            Timber.i("Downloaded path: " + imageUrl);
            updateLocallyOnFileDownloaded(imageUrl);
        }
    }

    private void updateLocallyOnFileDownloaded(String downloadPath) {
        for (int i = 0; i < mChat.getMediaList().size(); i++) {
            if (mChat.getMediaList().get(i).getId().matches(mMedia.getId())) {
                mMedia.setDownloadedPath(downloadPath);
                mChat.getMediaList().set(i, mMedia);
                break;
            }
        }

        MessageDb.save(mContext, mChat);
    }

    public void onUploadOrDownloadFailed(boolean isUpload,
                                         String requestId,
                                         Chat chat,
                                         String mediaId,
                                         Throwable ex) {
        Timber.i("onUploadOrDownloadFailed: " + ex);
        updateFileStatus(Media.FileStatusType.IDLE);
        //Set back the display size to normal.
        mDisplaySize.set(formatSize(mMedia.getSize()));
    }

    public void onDownloadGetStarted(String requestId, Chat chat, String mediaId) {
        updateFileStatus(Media.FileStatusType.DOWNLOADING);
        updateFileProgression(0, mMedia.getSize());
        updateFileStatus(Media.FileStatusType.DOWNLOADING);
    }

    private void updateFileStatus(Media.FileStatusType fileStatusType) {
        mMedia.setFileStatusType(fileStatusType);
        mFileStatus.set(fileStatusType.ordinal());
    }

    public void onUploadOrDownloadProgressing(boolean isUpload,
                                              String requestId,
                                              Chat chat,
                                              String mediaId,
                                              int percent,
                                              long percentBytes,
                                              long totalBytes) {
        Timber.i("onUploadProgressing: percent: " + percent);
        mProgression.set(percent);
        /*
          Since original file size and transformed file size is different. So we keep displaying
          original file size and manage the download progression with transformed file size.
         */
        updateFileProgression(percentBytes, mMedia.getSize());
    }

    private void updateFileProgression(long progressBytes, long totalBytes) {
        mDisplaySize.set(getFileProgressionDisplay(progressBytes, totalBytes));
    }

    private String formatSize(long size) {
        return ChatHelper.getDisplaySizeWithFormat(size);
    }

    public ObservableField<String> getName() {
        return mName;
    }

    public ObservableField<String> getDisplaySize() {
        return mDisplaySize;
    }

    public ObservableField<String> getExtension() {
        return mExtension;
    }

    public ObservableInt getProgression() {
        return mProgression;
    }

    public ObservableInt getFileStatus() {
        return mFileStatus;
    }

    public final void onFileClick() {
        if (mMedia.getFileStatusType() == Media.FileStatusType.DOWNLOADED) {
            DownloaderUtils.openDownloadedFile(mContext, mMedia.getDownloadedPath());
        } else {
            //To download file and store locally.
            if (mOnChatItemListener != null) {
                mOnChatItemListener.onFileClick(mChat, mMedia);
            }
        }
    }
}
