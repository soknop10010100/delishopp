package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Intent;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import android.text.TextUtils;
import android.view.View;

import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.intent.newintent.EditProfileIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.emun.FollowItemType;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.utils.FormatNumber;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.StoreStatusUtil;
import com.sompom.baseactivity.ResultCallback;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.NetworkBroadcastReceiver;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.datamanager.StoreDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by He Rotha on 10/9/17.
 */

public abstract class ListItemSellerStoreViewModel extends AbsLoadingViewModel {
    public final ObservableBoolean mIsFollow = new ObservableBoolean(false);
    public final ObservableBoolean mIsMe = new ObservableBoolean(false);
    public final ObservableField<String> mCover = new ObservableField<>();
    public final ObservableField<String> mStringStatus = new ObservableField<>();
    public final ObservableField<String> mFollower = new ObservableField<>("0");
    public final ObservableField<String> mFollowing = new ObservableField<>("0");
    private final StoreDataManager mDataManager;
    private final String mUserId;
    private final OnCallback mOnCallback;
    @Bindable
    public User mUser = new User();
    private boolean mIsReadUserDataReady;
    private NetworkBroadcastReceiver.NetworkListener mNetworkListener;

    ListItemSellerStoreViewModel(StoreDataManager dataManager,
                                 String userId,
                                 boolean isMe,
                                 OnCallback onCallback) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mUserId = userId;
        mIsMe.set(isMe);
        mOnCallback = onCallback;

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkListener = networkState -> {
                if (!mIsReadUserDataReady) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        getData();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkListener);
        }
    }

    public String getFollowButtonText(boolean isFollow) {
        if (isFollow) {
            return getContext().getString(R.string.following);
        } else {
            return getContext().getString(R.string.seller_store_follow_title);
        }
    }

    public void onFollowClick(View view) {
        AnimationHelper.animateBounceFollowButton(view, 600, 0.15f, 10.0f);
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).startLoginWizardActivity(new LoginActivityResultCallback() {
                @Override
                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                    if (!isAlreadyLogin) {
                        checkIsMe();
                    }
                    if (!mIsMe.get()) {
                        mIsFollow.set(!mIsFollow.get());
                        boolean isFollow = mIsFollow.get();
                        mUser.setFollow(isFollow);
                        checkToFollow(isFollow);
                    }
                }
            });
        }
    }

    private void checkToFollow(boolean isFollow) {
        Observable<Response<Object>> observable;
        long followerCount = mUser.getContentStat().getTotalFollowers();
        if (isFollow) {
            followerCount += 1;
            observable = mDataManager.postFollow(mUserId);
        } else {
            followerCount -= 1;
            observable = mDataManager.unFollow(mUserId);
        }
        mUser.getContentStat().setTotalFollowers(followerCount);
        mFollower.set(FormatNumber.format(followerCount));
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(value -> {
            mUser.setFollow(isFollow);
            notifyPropertyChanged(BR.user);
        }));
    }

    private void checkIsMe() {
        if (SharedPrefUtils.isLogin(getContext())) {
            if (mUser.getId().equals(SharedPrefUtils.getUserId(getContext()))) {
                SharedPrefUtils.setUserValue(mUser, getContext());
                mIsMe.set(true);
            } else {
                mIsMe.set(false);
            }
        } else {
            mIsMe.set(false);
        }
    }

    public void getData() {
        showLoading();
        Observable<Response<User>> call = mDataManager.getUserById(mUserId);
        ResponseObserverHelper<Response<User>> observerHelper = new ResponseObserverHelper<>(getContext(), call);
        observerHelper.execute(new OnCallbackListener<Response<User>>() {
            @Override
            public void onComplete(Response<User> data) {
                mUser = data.body();
                updateData();
            }

            private void updateData() {
                hideLoading();
                mIsReadUserDataReady = true;

                notifyPropertyChanged(BR.user);
                checkIsMe();
                mCover.set(mUser.getUserCoverProfile());
                mStringStatus.set(StoreStatusUtil.getUserStatus(getContext(), mUser));
                mFollower.set(FormatNumber.format(mUser.getContentStat().getTotalFollowers()));
                mFollowing.set(FormatNumber.format(mUser.getContentStat().getTotalFollowings()));
                mIsFollow.set(mUser.isFollow());
                if (mOnCallback != null) {
                    mOnCallback.onGetUserData(mUser);
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                String myUseID = SharedPrefUtils.getUserId(getContext());
                if (!TextUtils.isEmpty(myUseID) && myUseID.equals(mUserId)) {
                    mUser = SharedPrefUtils.getUser(getContext());
                    updateData();
                    return;
                }
                if (ex.getCode() == 404) {
                    showError(getContext().getString(R.string.error_user_404));
                } else {
                    showError(ex.toString());
                }
            }
        });
    }

    @Override
    public void onRetryClick() {
        super.onRetryClick();
        getData();
    }

    public void onEditProfileClick() {
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).startActivityForResult(new EditProfileIntent(getContext()),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            mUser = SharedPrefUtils.getUser(getContext());
                            notifyPropertyChanged(BR.user);
                            mCover.set(mUser.getUserCoverProfile());
                        }
                    });
        }
    }

    public void onShowListFollowUserClick(FollowItemType type) {
        if (mOnCallback != null) {
            mOnCallback.onShowListFollowUser(type);
        }
    }

    public interface OnCallback {
        void onGetUserData(User user);

        void onShowListFollowUser(FollowItemType type);
    }
}
