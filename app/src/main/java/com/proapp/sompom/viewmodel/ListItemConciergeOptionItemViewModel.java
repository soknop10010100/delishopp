package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.concierge.ConciergeComboItem;
import com.proapp.sompom.model.emun.ConciergeMenuItemOptionType;
import com.proapp.sompom.model.result.ConciergeMenuItemOption;
import com.proapp.sompom.utils.AttributeConverter;

/**
 * Created by Or Vitovongsak on 13/9/21.
 */

public class ListItemConciergeOptionItemViewModel extends AbsBaseViewModel {

    private ObservableBoolean mIsSelected = new ObservableBoolean();
    private ObservableBoolean mIsShowEditOption = new ObservableBoolean();
    private ObservableField<String> mOptionName = new ObservableField<>();
    private ObservableField<String> mPrice = new ObservableField<>();
    private ObservableField<String> mText = new ObservableField<>();
    private final ObservableInt mItemBackground = new ObservableInt(Color.TRANSPARENT);

    private Context mContext;

    private ConciergeMenuItemOption mOption;
    private ConciergeMenuItemOptionType mOptionType;
    private OnItemClickListener<ConciergeMenuItemOption> mOptionListener;

    private ConciergeComboItem mComboItem;
    private boolean mIsComboItem;
    private ListItemConciergeOptionItemViewModelListener mListener;

    private float mDefaultPrice;
    private boolean mIsDisplayFullPrice;

    public ListItemConciergeOptionItemViewModel(Context context,
                                                ConciergeMenuItemOption option,
                                                ConciergeMenuItemOptionType optionType,
                                                float defaultPrice,
                                                boolean isDisplayFullPrice,
                                                OnItemClickListener<ConciergeMenuItemOption> onClickListener) {
        mContext = context;
        mOption = option;
        mOptionType = optionType;
        mDefaultPrice = defaultPrice;
        mIsDisplayFullPrice = isDisplayFullPrice;
        mOptionListener = onClickListener;
        initData(option);
    }

    public ListItemConciergeOptionItemViewModel(Context context,
                                                ConciergeComboItem comboItem,
                                                float defaultPrice,
                                                boolean isDisplayFullPrice,
                                                ListItemConciergeOptionItemViewModelListener listener) {
        mContext = context;
        mIsComboItem = true;
        mComboItem = comboItem;
        mDefaultPrice = defaultPrice;
        mIsDisplayFullPrice = isDisplayFullPrice;
        mListener = listener;
        initData(comboItem);
    }

    public void initData(ConciergeMenuItemOption option) {
        mOption = option;
        mOptionName.set(option.getName());
        mIsSelected.set(option.isSelected());
        updatePriceDisplay();
    }

    public void initData(ConciergeComboItem comboItem) {
        mComboItem = comboItem;
        mComboItem.calculateComboItemPrice();
        mOptionName.set(comboItem.getName());
        mIsSelected.set(comboItem.isSelected());
        mIsShowEditOption.set(comboItem.isSelected() && comboItem.getOptions() != null && !comboItem.getOptions().isEmpty());
        updatePriceDisplay();
    }

    private void updatePriceDisplay() {
        if (mIsComboItem) {
            if (mIsDisplayFullPrice) {
                double comboPrice = mComboItem.getTotalPrice() + mDefaultPrice;
                mPrice.set(ConciergeHelper.getDisplayPrice(mContext, comboPrice));
            } else {
                mPrice.set("+ " + ConciergeHelper.getDisplayPrice(mContext, mComboItem.getTotalPrice()));
            }
        } else {
            if (mIsDisplayFullPrice) {
                double optionPrice = mOption.getPrice() + mDefaultPrice;
                mPrice.set(ConciergeHelper.getDisplayPrice(mContext, optionPrice));
            } else {
                mPrice.set("+ " + ConciergeHelper.getDisplayPrice(mContext, mOption.getPrice()));
            }
        }
    }

    public void notifyChange() {
        mOptionName.notifyChange();
        mPrice.notifyChange();
        mIsSelected.notifyChange();
        mIsShowEditOption.notifyChange();
    }

    public void shouldShowError(boolean isError) {
        if (isError) {
            mItemBackground.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_item_detail_option_error));
        } else {
            mItemBackground.set(Color.TRANSPARENT);
        }
    }

    public void onOptionClick(String preselectionChoiceVariationId) {
        if (mIsComboItem) {
            if (!mComboItem.isSelected()) {
                if (mComboItem.getOptions() == null || mComboItem.getOptions().isEmpty()) {
                    mComboItem.setIsSelected(true);
                }
                initData(mComboItem);

                if (mListener != null) {
                    mListener.onItemClicked(mComboItem, preselectionChoiceVariationId);
                }
            } else {
                if (mComboItem.getOptions() != null && !mComboItem.getOptions().isEmpty()) {
                    initData(mComboItem);
                    if (mListener != null) {
                        mListener.onItemClicked(mComboItem, preselectionChoiceVariationId);
                    }
                }
            }
        } else {
            if (mOptionType == ConciergeMenuItemOptionType.VARIATION) {
                if (!mOption.isSelected()) {
                    mOption.setIsSelected(true);
                    initData(mOption);

                    if (mOptionListener != null) {
                        mOptionListener.onClick(mOption);
                    }
                }
            } else if (mOptionType == ConciergeMenuItemOptionType.ADDON) {
                mOption.setIsSelected(!mOption.isSelected());
                initData(mOption);
                if (mOptionListener != null) {
                    mOptionListener.onClick(mOption);
                }
            }
        }
    }

    public String getDefaultPreselectionChoiceVariationId() {
        return "";
    }

    public ObservableBoolean getIsSelected() {
        return mIsSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.mIsSelected.set(isSelected);
    }

    public boolean isComboItem() {
        return mIsComboItem;
    }

    public ObservableBoolean getIsShowEditOption() {
        return mIsShowEditOption;
    }

    public void setIsShowEditOption(boolean isShowEditButton) {
        this.mIsShowEditOption.set(isShowEditButton);
    }

    public ObservableField<String> getOptionName() {
        return mOptionName;
    }

    public void setOptionName(ObservableField<String> optionName) {
        this.mOptionName = optionName;
    }

    public ObservableField<String> getPrice() {
        return mPrice;
    }

    public void setPrice(ObservableField<String> price) {
        this.mPrice = price;
    }

    public ObservableField<String> getText() {
        return mText;
    }

    public void setText(String text) {
        mText.set(text);
    }

    public ObservableInt getItemBackground() {
        return mItemBackground;
    }

    public interface ListItemConciergeOptionItemViewModelListener {
        void onItemClicked(ConciergeComboItem item, String preselectionChoiceVariationId);
    }

    @BindingAdapter("setCustomBackgroundColor")
    public static void setCustomBackgroundColor(View view, int backgroundColor) {
        view.setBackgroundColor(backgroundColor);
    }
}
