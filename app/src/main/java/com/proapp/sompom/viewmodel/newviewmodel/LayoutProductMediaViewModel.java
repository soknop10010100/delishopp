package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableField;

import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;
import com.proapp.sompom.model.SharedProduct;

/**
 * Created by nuonveyo on 7/13/18.
 */

public class LayoutProductMediaViewModel extends AbsBaseViewModel {
    public ObservableField<Product> mProduct = new ObservableField<>();
    private OnItemClickListener<Product> mOnItemClickListener;

    public LayoutProductMediaViewModel(Adaptive adaptive,
                                       OnItemClickListener<Product> buttonClickListener) {
        if (adaptive instanceof Product || adaptive instanceof SharedProduct) {
            if (adaptive instanceof Product) {
                mProduct.set((Product) adaptive);
            } else {
                mProduct.set(((SharedProduct) adaptive).getProduct());
            }
        }
        mOnItemClickListener = buttonClickListener;
    }

    public void onProductItemClick() {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onClick(mProduct.get());
        }
    }
}
