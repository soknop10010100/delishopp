package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.DelayWatcher;
import com.proapp.sompom.utils.InputUtils;
import com.proapp.sompom.utils.SpecialTextRenderUtils;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public abstract class AbsSignUpViewModel extends AbsAuthViewModel {

    protected final ObservableBoolean mActionButtonEnable = new ObservableBoolean();
    protected final ObservableField<String> mPassword = new ObservableField<>();
    protected final ObservableField<String> mConfirmPassword = new ObservableField<>();
    protected final ObservableInt mEmailVisibility = new ObservableInt(View.GONE);
    protected final ObservableInt mPasswordVisibility = new ObservableInt(View.GONE);
    protected final ObservableInt mBackButtonVisibility = new ObservableInt(View.INVISIBLE);
    protected final ObservableBoolean mEnableImeActionDone = new ObservableBoolean();

    public AbsSignUpViewModel(Context context) {
        super(context);
    }

    public ObservableBoolean getActionButtonEnable() {
        return mActionButtonEnable;
    }

    public ObservableField<String> getPassword() {
        return mPassword;
    }

    public ObservableField<String> getConfirmPassword() {
        return mConfirmPassword;
    }

    public ObservableInt getEmailVisibility() {
        return mEmailVisibility;
    }

    public ObservableInt getPasswordVisibility() {
        return mPasswordVisibility;
    }

    public ObservableInt getBackButtonVisibility() {
        return mBackButtonVisibility;
    }

    public ObservableBoolean getEnableImeActionDone() {
        return mEnableImeActionDone;
    }

    public final TextWatcher onInputChangeListener() {
        return new DelayWatcher(false) {
            @Override
            public void onTextChanged(String text) {
                checkToEnableActionButton();
            }
        };
    }

    private void checkToEnableActionButton() {
        if (shouldEnableActionButton()) {
            if (!mActionButtonEnable.get())
                mActionButtonEnable.set(true);
        } else {
            if (mActionButtonEnable.get())
                mActionButtonEnable.set(false);
        }
    }

    protected boolean isPasswordMatch() {
        boolean match = true;
        String password = mPassword.get();
        String confirmPassword = mConfirmPassword.get();
        if (!TextUtils.equals(password, confirmPassword)) {
            match = false;
            showSnackBar(R.string.change_password_and_confirm_didnt_match, true);
        }

        return match;
    }

    protected boolean isPasswordCorrect() {
        if (InputUtils.isPasswordFormatCorrect(mPassword.get())) {
            return true;
        } else {
            showSnackBar(R.string.change_password_error_password_format, true);
            return false;
        }
    }

    protected boolean arePasswordValid() {
        return !TextUtils.isEmpty(mPassword.get()) && !TextUtils.isEmpty(mConfirmPassword.get());
    }

    public void onBackClick() {
        if (getContext() instanceof Activity) {
            ((Activity) getContext()).finish();
        }
    }

    protected abstract boolean shouldEnableActionButton();

    public abstract void onActionButtonClicked();

    @BindingAdapter("setEnableImeActionDone")
    public static void setEnableImeActionDone(EditText editText, boolean enableImeActionDone) {
        editText.setImeOptions(enableImeActionDone ? EditorInfo.IME_ACTION_DONE : EditorInfo.IME_ACTION_NEXT);
    }
}
