package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCommentItemClickListener;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class ListItemMainCommentViewModel extends CommentViewModel {
    public final ObservableBoolean mIsShowReplayLayout = new ObservableBoolean();
    public final ObservableField<String> mReplyText = new ObservableField<>();
    public final ObservableField<User> mUserSubComment = new ObservableField<>();

    public ListItemMainCommentViewModel(Context context,
                                        int position,
                                        Comment comment,
                                        OnCommentItemClickListener onCommentItemClickListener) {
        super(context, position, comment, onCommentItemClickListener);
        if (comment.getReplyComment() != null && !comment.getReplyComment().isEmpty()) {
            int lastCommentIndex = comment.getReplyComment().size() - 1;
            mUserSubComment.set(comment.getReplyComment().get(lastCommentIndex).getUser());
            mIsShowReplayLayout.set(true);
            String content;
            int replyCommentCount = comment.getTotalSubComment();
            if (replyCommentCount == 1) {
                content = replyCommentCount + " " + context.getString(R.string.comment_reply_label);
            } else {
                content = replyCommentCount + " " + context.getString(R.string.comment_multiple_reply_label);
            }
            mReplyText.set(content);
        }
    }
}
