package com.proapp.sompom.viewmodel.newviewmodel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableField;

import com.google.android.gms.maps.model.LatLng;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.CheckRequestLocationCallbackHelperV2;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.RequestLocationManager;
import com.proapp.sompom.intent.newintent.ConciergeCurrentLocationIntent;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;
import com.sompom.baseactivity.ResultCallback;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 17/11/21.
 */
public class ConciergeFragmentCurrentLocationViewModel extends AbsLoadingViewModel {

    private ObservableField<String> mCurrentLocationString = new ObservableField<>();

    private ConciergeUserAddress mUserCurrentLocation = new ConciergeUserAddress();
    private boolean mIsLocationEnabled = false;

    public ConciergeFragmentCurrentLocationViewModel(Context context) {
        super(context);
    }

    public ObservableField<String> getCurrentLocationString() {
        return mCurrentLocationString;
    }

    public void setCurrentLocationString(String currentLocation) {
        mCurrentLocationString.set(currentLocation);
    }

    public boolean getIsLocationEnabled() {
        return mIsLocationEnabled;
    }

    public void loadUserSelectedLocation() {
        ConciergeUserAddress userSelectedAddress = SharedPrefUtils.getUserSelectedLocation(mContext);
        if (userSelectedAddress != null) {
            updateUserSelectedLocation(userSelectedAddress);
            mIsLocationEnabled = true;
        } else {
            setDefaultLocationForUser();
        }
    }

    public void saveAndUpdatedUserSelectedLocation(ConciergeUserAddress newAddress) {
        SharedPrefUtils.setUserSelectedLocation(mContext, newAddress);
        updateUserSelectedLocation(newAddress);
    }

    public void updateUserSelectedLocation(ConciergeUserAddress newAddress) {
        mUserCurrentLocation = newAddress;
        String label = "";
        String address = "";
        if (!TextUtils.isEmpty(mUserCurrentLocation.getLabel())) {
            label = mUserCurrentLocation.getLabel() + " ";
        } else {
            // Only when label is empty that we will show full address, in case of user current
            // location and user's saved address with no label
            if (!TextUtils.isEmpty(mUserCurrentLocation.getAddress())) {
                address = mUserCurrentLocation.getAddress();
            }
        }
        setCurrentLocationString(label + address);
    }

    public void checkIfCurrentAddressIsUpdated(ConciergeUserAddress toCheck) {
        if (TextUtils.equals(mUserCurrentLocation.getId(), toCheck.getId())) {
            saveAndUpdatedUserSelectedLocation(toCheck);
        }
    }

    public void checkIfCurrentAddressIsDeleted(ConciergeUserAddress toCheck) {
        if (TextUtils.equals(mUserCurrentLocation.getId(), toCheck.getId())) {
            setDefaultLocationForUser();
        }
    }

    public void showCurrentLocationLabel() {
        mCurrentLocationString.set(mContext.getString(R.string.shop_list_current_location_label));
    }

    public void setDefaultLocationForUser() {
        // Preserve previous permission request logic if the guest user has been prompted with
        // the first start permission request already.
        //
        // Use case is to prevent this location permission popup from appearing and conflicting the
        // default app permission request has been shown already.
        boolean isLocationPermissionEnabled = RequestLocationManager.isLocationPermissionGranted(mContext);
        // If guest user was already shown the initial permission request popup, we can keep the
        // previous behavior
        if (SharedPrefUtils.isGuestUserPermissionRequested(mContext)) {
            showRequestPermissionPopup();
        } else {
            // Else if guest user has not been shown the initial permission popup yet, but the location
            // permission was already granted, also preserve the previous behavior. If the permission
            // was already granted, the below function will not show any location permission popup as
            // this is standard behavior
            if (isLocationPermissionEnabled) {
                showRequestPermissionPopup();
            } else {
                // Else, guest user was not shown the initial permission popup yet, and location
                // permission was not granted either, so default to location denied text
                mCurrentLocationString.set(mContext.getString(R.string.shop_list_location_permission_error));
            }
        }
    }

    private void showRequestPermissionPopup() {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).requestLocation(true,
                    new CheckRequestLocationCallbackHelperV2((Activity) mContext, new CheckRequestLocationCallbackHelperV2.Callback() {
                        @Override
                        public void onComplete(Locations locations, boolean isLocationPermissionEnabled, boolean isLocationEnabled) {
                            mIsLocationEnabled = isLocationPermissionEnabled;
                            if (!isLocationPermissionEnabled) {
                                mCurrentLocationString.set(mContext.getString(R.string.shop_list_location_permission_error));
                            } else {
                                if (locations != null) {
                                    showCurrentLocationLabel();
                                    LatLng latLng = new LatLng(locations.getLatitude(), locations.getLongitude());
                                    addDisposable(ConciergeHelper.getUserAddressFromLatLng(mContext, latLng, "")
                                            .subscribe(data -> {
                                                mUserCurrentLocation = data;
                                            }, Timber::e));
                                }
                            }
                        }

                        @Override
                        public void onNeverAskAgainDialogClosed() {
                            mCurrentLocationString.set(mContext.getString(R.string.shop_list_location_permission_error));
                            mIsLocationEnabled = false;
                        }
                    }));
        }
    }

    public void onDeliveryAddressClick(View view) {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).requestLocation(true,
                    new CheckRequestLocationCallbackHelperV2((Activity) mContext, new CheckRequestLocationCallbackHelperV2.Callback() {
                        @Override
                        public void onComplete(Locations locations, boolean isLocationPermissionEnabled, boolean isLocationEnabled) {
                            mIsLocationEnabled = isLocationPermissionEnabled;

                            Timber.i("Is location enabled: " + mIsLocationEnabled);
                            if (mIsLocationEnabled) {
                                startCurrentLocationScreen();
                            }
                        }

                        @Override
                        public void onNeverAskAgainDialogClosed() {
                            mCurrentLocationString.set(mContext.getString(R.string.shop_list_location_permission_error));
                            mIsLocationEnabled = false;
                        }
                    }));
        }
    }

    public void startCurrentLocationScreen() {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).startActivityForResult(new ConciergeCurrentLocationIntent(mContext), new ResultCallback() {
                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    ConciergeUserAddress userAddress = data.getParcelableExtra(ConciergeCurrentLocationIntent.USER_CURRENT_ADDRESS);
                    if (userAddress != null) {
                        ConciergeHelper.broadcastAddressSelectedEvent(mContext, userAddress, false);
                        saveAndUpdatedUserSelectedLocation(userAddress);
                    }
                }
            });
        }
    }
}
