package com.proapp.sompom.viewmodel.newviewmodel;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.example.usermentionable.model.UserMentionable;
import com.example.usermentionable.tokenization.QueryToken;
import com.example.usermentionable.tokenization.interfaces.QueryTokenReceiver;
import com.example.usermentionable.utils.RenderTextAsMentionable;
import com.example.usermentionable.widget.KeyboardInputEditor;
import com.example.usermentionable.widget.RichEditorView;
import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.database.LinkPreviewDb;
import com.proapp.sompom.database.MessageDb;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.CheckPermissionCallbackHelper;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.helper.NavigateSellerStoreHelper;
import com.proapp.sompom.helper.SaveImageHelper;
import com.proapp.sompom.helper.SaveVideoHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.intent.newintent.CallIntent;
import com.proapp.sompom.intent.newintent.GroupDetailIntent;
import com.proapp.sompom.intent.newintent.MyCameraIntent;
import com.proapp.sompom.intent.newintent.MyCameraResultIntent;
import com.proapp.sompom.intent.newintent.MyFileIntent;
import com.proapp.sompom.intent.newintent.MyFileResultIntent;
import com.proapp.sompom.intent.newintent.TelegramUserRedirectIntent;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.BaseChatModel;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.ConversationType;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.emun.Presence;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.Theme;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.fragment.CallingFragment;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ChatDataManager;
import com.proapp.sompom.utils.DateTimeUtils;
import com.proapp.sompom.utils.NetworkStateUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.SnackBarUtil;
import com.proapp.sompom.utils.ToastUtil;
import com.proapp.sompom.viewmodel.SupportCallViewModel;
import com.proapp.sompom.viewmodel.binding.EditTextBindingUtil;
import com.proapp.sompom.widget.ConversationProfileView;
import com.proapp.sompom.widget.EditChatHeaderView;
import com.sompom.baseactivity.ResultCallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/25/18.
 */

public class ChatDialogViewModel extends SupportCallViewModel {

    public final ObservableBoolean mIsRecording = new ObservableBoolean();
    public final ObservableBoolean mIsAddIconAnimation = new ObservableBoolean();
    public final ObservableField<String> mContent = new ObservableField<>();
    public final ObservableField<String> mLastActivity = new ObservableField<>();
    public final ObservableInt mVisibleActive = new ObservableInt(View.GONE);
    private final ObservableBoolean mToolBtnVisibility = new ObservableBoolean(false);
    private final ObservableBoolean mToolVisibility = new ObservableBoolean(true);
    private ObservableBoolean mCameraVisibility = new ObservableBoolean();
    private ObservableBoolean mFileVisibility = new ObservableBoolean();
    private ObservableBoolean mPictureVisibility = new ObservableBoolean();
    private ObservableBoolean mGifVisibility = new ObservableBoolean();
    private ObservableBoolean mIsJoinAvailable = new ObservableBoolean();
    private ObservableBoolean mIsVideoJoinCall = new ObservableBoolean();
    private final ObservableBoolean mTelegramUserRedirectVisibility = new ObservableBoolean(false);

    private final ObservableBoolean mIsKeyboardInputSingleLine = new ObservableBoolean(false);
    private final ObservableInt mMsgInputMaxLine = new ObservableInt(6);
    public ObservableField<String> mDisplayName = new ObservableField<>();
    public ObservableField<String> mGroupImageUrl = new ObservableField<>();
    private ObservableInt mScrollDownButtonVisibility = new ObservableInt(View.GONE);
    private final ObservableInt mReplyOrEditHeaderVisibility = new ObservableInt(View.GONE);
    private final ObservableInt mInputReplyVisibility = new ObservableInt(View.GONE);
    private final ObservableInt mEditHeaderVisibility = new ObservableInt(View.GONE);
    private final ObservableInt mNormalInputOptionVisibility = new ObservableInt(View.VISIBLE);
    private final ObservableBoolean mIsReplyMessageMode = new ObservableBoolean();
    private final ObservableBoolean mIsShowNewGroupWelcomeMessage = new ObservableBoolean();
    private final ObservableBoolean mCallSectionVisibility = new ObservableBoolean();
    private final ObservableBoolean mIsSupportConversation = new ObservableBoolean();
    private final ObservableBoolean mIsWelcomeSupportGroupMessageVisible = new ObservableBoolean();

    public MyFileIntent mMyFileIntent;
    public User mRecipient;
    private final Product mProduct;
    private final String mConversationId;
    private final ChatDataManager mChatDataManager;
    private final OnCallback mCallbackListener;
    private final SaveImageHelper mSaveImageHelper;
    private final SaveVideoHelper mSaveVideoHelper;
    private final SocketService.SocketBinder mSocketBinder;
    private Conversation mConversation;
    public String mGroupName;

    private final ObservableField<List<User>> mParticipants = new ObservableField<>();
    private EditText mEditText;
    private int mMediaSaved = 0;

    public ChatDialogViewModel(ChatDataManager dataManager,
                               SocketService.SocketBinder binder,
                               Conversation conversation,
                               String conversationId,
                               User recipient,
                               Product product,
                               OnCallback callbackListener,
                               SupportCallViewModelCallback supportCallViewModelCallback) {
        super(dataManager.getContext(), supportCallViewModelCallback);
        mConversation = conversation;
        mSaveImageHelper = new SaveImageHelper(getContext());
        mSaveVideoHelper = new SaveVideoHelper(getContext());

        mChatDataManager = dataManager;
        mIsSupportConversation.set(
                ConversationType.fromValue(mConversation.getType()) == ConversationType.SUPPORT
                        || ConversationType.fromValue(mConversation.getType()) == ConversationType.SUPPORT_TELEGRAM);

        mConversationId = conversationId;
        mRecipient = recipient;
        mProduct = product;
        mCallbackListener = callbackListener;
        mSocketBinder = binder;
        bindProfileHeader();
        checkJoinGroupCallStatus();
        if (!mChatDataManager.isJumpSearch()) {
            getChatFromDb(false);
        } else {
            mChatDataManager.loadConversationFromLocal();
            jumpSearchMessage(false);
        }
        checkToRequestGroupDetailForOnlineStatusUpdate();

        /*
            Currently, for support conversation, we will not allow to make call. This setting can be changed
            with the future feature configuration, but for now we will check to disable it manually.
         */
        boolean isTelegramConversation = ConversationType.fromValue(mConversation.getType()) == ConversationType.TELEGRAM;
        mCallSectionVisibility.set((!mChatDataManager.isSupportConversation() && !mConversation.isSupportTelegram() && !isTelegramConversation));

        mTelegramUserRedirectVisibility.set(conversation.isSupportTelegram());
        if (mIsSupportConversation.get()) {
            setupSupportConversation();
        }
    }

    public ObservableBoolean getIsWelcomeSupportGroupMessageVisible() {
        return mIsWelcomeSupportGroupMessageVisible;
    }

    public void setWelcomeSupportGroupMessageVisible(boolean isVisible) {
        mIsWelcomeSupportGroupMessageVisible.set(isVisible);
    }

    private void setupSupportConversation() {
        mVisibleActive.set(View.VISIBLE);
        mLastActivity.set(mConversation.getReplyTextNotice());
    }

    private void checkToRequestGroupDetailForOnlineStatusUpdate() {
        addDisposable(mChatDataManager.requestGroupDetailForOnlineStatusUpdate(new OnCallbackListener<Conversation>() {
            @Override
            public void onFail(ErrorThrowable ex) {

            }

            @Override
            public void onComplete(Conversation result) {
//                Timber.i("requestGroupDetailForOnlineStatusUpdate onComplete: " + result);
                if (result != null && result.getParticipants() != null && !result.getParticipants().isEmpty()) {
                    //Update current participants of group
                    mConversation.setParticipants(result.getParticipants());
                    if (isGroupConversation()) {
                        mParticipants.set(result.getParticipants());
                    } else {
                        String currentUserId = SharedPrefUtils.getUserId(mContext);
                        //Find recipient
                        for (User participant : result.getParticipants()) {
                            if (!TextUtils.equals(currentUserId, participant.getId())) {
                                mRecipient = participant;
                                mParticipants.set(Collections.singletonList(mRecipient));
                                break;
                            }
                        }
                    }
                }
            }
        }));
    }

    public ObservableBoolean getIsSupportConversation() {
        return mIsSupportConversation;
    }

    public ObservableBoolean getCallSectionVisibility() {
        return mCallSectionVisibility;
    }

    public ObservableBoolean getIsShowNewGroupWelcomeMessage() {
        return mIsShowNewGroupWelcomeMessage;
    }

    public void setShowNewGroupWelcomeMessage(boolean show) {
        mIsShowNewGroupWelcomeMessage.set(show);
    }

    public ObservableBoolean getIsReplyMessageMode() {
        return mIsReplyMessageMode;
    }

    public void updateEditOrReplyMessageMode(boolean isReply) {
        mIsReplyMessageMode.set(isReply);
    }

    public void setCameraVisibility(boolean cameraVisibility) {
        mCameraVisibility.set(cameraVisibility);
    }

    public ObservableInt getInputReplyVisibility() {
        return mInputReplyVisibility;
    }

    public void setInputReplyVisibility(boolean isShow) {
        mReplyOrEditHeaderVisibility.set(isShow ? View.VISIBLE : View.GONE);
        mInputReplyVisibility.set(isShow ? View.VISIBLE : View.GONE);
        if (isShow) {
            mEditHeaderVisibility.set(View.GONE);
        }
    }

    public void setInputEditVisibility(boolean isShow) {
        mReplyOrEditHeaderVisibility.set(isShow ? View.VISIBLE : View.GONE);
        mEditHeaderVisibility.set(isShow ? View.VISIBLE : View.GONE);
        if (isShow) {
            mInputReplyVisibility.set(View.GONE);
        }
    }

    public void switchToEditMessageMode() {
        mNormalInputOptionVisibility.set(View.GONE);
    }

    public ObservableInt getReplyOrEditHeaderVisibility() {
        return mReplyOrEditHeaderVisibility;
    }

    public ObservableInt getEditHeaderVisibility() {
        return mEditHeaderVisibility;
    }

    public ObservableInt getScrollDownButtonVisibility() {
        return mScrollDownButtonVisibility;
    }

    public ObservableBoolean getIsVideoJoinCall() {
        return mIsVideoJoinCall;
    }

    public ObservableBoolean getIsJoinAvailable() {
        return mIsJoinAvailable;
    }

    public String getGroupName() {
        return mGroupName;
    }

    public void setFileVisibility(boolean fileVisibility) {
        mFileVisibility.set(fileVisibility);
    }

    public void setPictureVisibility(boolean pictureVisibility) {
        mPictureVisibility.set(pictureVisibility);
    }

    public boolean isInRepliedInput() {
        return mInputReplyVisibility.get() == View.VISIBLE && mEditHeaderVisibility.get() != View.VISIBLE;
    }

    public boolean isInEditMessageInput() {
        return mEditHeaderVisibility.get() == View.VISIBLE && mInputReplyVisibility.get() != View.VISIBLE;
    }

    public void setScrollDownButtonVisibility(int scrollDownButtonVisibility) {
        mScrollDownButtonVisibility.set(scrollDownButtonVisibility);
    }

    public void setGifVisibility(boolean gifVisibility) {
        mGifVisibility.set(gifVisibility);
    }

    public ObservableBoolean getCameraVisibility() {
        return mCameraVisibility;
    }

    public ObservableBoolean getFileVisibility() {
        return mFileVisibility;
    }

    public ObservableBoolean getPictureVisibility() {
        return mPictureVisibility;
    }

    public ObservableBoolean getGifVisibility() {
        return mGifVisibility;
    }

    public ObservableField<List<User>> getParticipants() {
        return mParticipants;
    }

    public ObservableInt getNormalInputOptionVisibility() {
        return mNormalInputOptionVisibility;
    }

    public ObservableBoolean getUserListVisibility() {
        return mTelegramUserRedirectVisibility;
    }

    public void resetToNormalMode() {
        mChatDataManager.resetToNormalModeChat();
        getChatFromDb(true);
    }

    public void checkJoinGroupCallStatus() {
        if (mConversation.isChannelOpen()) {
            mIsJoinAvailable.set(true);
            mIsVideoJoinCall.set(mConversation.isVideo());
            mCallVisibility.set(View.GONE);
            mVideoCallVisibility.set(View.GONE);
        } else {
            mIsJoinAvailable.set(false);
            setCallActionVisibility();
        }
    }

    public boolean isJumpSearch() {
        return mChatDataManager.isJumpSearch();
    }

    @Override
    protected boolean isGroupConversation() {
        return mConversation != null && mConversation.isGroup();
    }

    @Override
    protected User getRecipient() {
        return mRecipient;
    }

    @Override
    protected Conversation getConversation() {
        return mConversation;
    }

    @Override
    protected CallIntent.StartFromType getStartCallScreenType() {
        return CallIntent.StartFromType.CHAT_SCREEN;
    }

    public ConversationProfileView.ConversationProfileViewCallback getConversationProfileViewCallback() {
        return new ConversationProfileView.ConversationProfileViewCallback() {
            @Override
            public void onPresenceUpdate(String userId, Presence presence, Date lastOnlineTime) {
                checkUpdateOnlineStatus(userId, presence, lastOnlineTime);
            }

            @Override
            public void onGotDefaultUserOnlineStatus(String userId, Presence presence, Date lastOnlineTime) {
                checkUpdateOnlineStatus(userId, presence, lastOnlineTime);
            }
        };
    }

    private void checkUpdateOnlineStatus(String userId, Presence presence, Date lastOnlineTime) {
//        Timber.i("checkUpdateOnlineStatus of user " + userId + ", presence: " + presence + ", lastOnlineTime: " + lastOnlineTime);
        if (!mIsSupportConversation.get()) {
            if (presence == Presence.Online) {
                mVisibleActive.set(View.VISIBLE);
                mLastActivity.set(getContext().getString(R.string.chat_header_active_status));
            } else if (lastOnlineTime != null) {
                mVisibleActive.set(View.VISIBLE);
                mLastActivity.set(DateTimeUtils.getLastActiveRelativeTimeSpanDisplay(getContext(),
                        lastOnlineTime.getTime()));
            } else {
                mVisibleActive.set(View.VISIBLE);
                mLastActivity.set(getContext().getString(R.string.chat_header_offline_status));
            }
        }
    }

    public int getSelectedChatIndex(List<BaseChatModel> chatModelList) {
        if (mChatDataManager.getSelectedChat() != null) {
            for (int i = 0; i < chatModelList.size(); i++) {
                if (TextUtils.equals(chatModelList.get(i).getId(),
                        mChatDataManager.getSelectedChat().getId())) {
                    if (mChatDataManager.isCanLoadMore()) {
                        return i + 1;
                    } else {
                        return i;
                    }
                }
            }
        }

        return -1;
    }

    public Chat getSelectedChat() {
        return mChatDataManager.getSelectedChat();
    }

    public void updateJoinGroupCallStatus(boolean isJoinAvailable, boolean isVideo) {
        mConversation.setChannelOpen(isJoinAvailable);
        mConversation.setVideo(isVideo);
        checkJoinGroupCallStatus();
    }

    public boolean isAbleToSendMessage() {
        return mConversation == null ||
                !mConversation.isGroup() ||
                (mConversation.getParticipants() != null && mConversation.getParticipants().size() > 1);
    }

    private void bindProfileHeader() {
        if (mIsSupportConversation.get()) {
            Theme theme = UserHelper.getSelectedThemeFromAppSetting(mContext);
            if (theme != null && !TextUtils.isEmpty(theme.getCompanyAvatar())) {
                mGroupImageUrl.set(theme.getCompanyAvatar());
            }
            mDisplayName.set(mConversation.getGroupName());
        } else if (mConversation != null) {
            if (!isGroupConversation()) {
                setUpIndividualChatHeader();
            } else {
                mGroupImageUrl.set(mConversation.getGroupImageUrl());
                mDisplayName.set(mConversation.getGroupName());
                if (mConversation.getParticipants() != null) {
                    mParticipants.set(mConversation.getParticipants());
                }
            }
        } else {
            setUpIndividualChatHeader();
        }
        setCallActionVisibility();
    }

    public void updateGroupConversation(Conversation conversation) {
        mConversation = conversation;
        mIsSupportConversation.set(ConversationType.fromValue(mConversation.getType()) == ConversationType.SUPPORT);
        bindProfileHeader();
    }

    private void setUpIndividualChatHeader() {
        mParticipants.set(Collections.singletonList(mRecipient));
        mDisplayName.set(mRecipient.getFullName());
    }

    public ObservableBoolean getToolBtnVisibility() {
        return mToolBtnVisibility;
    }

    public void onShowBackToolClick() {
        mToolBtnVisibility.set(false);
        mToolVisibility.set(true);
        toggleAllToolBtn(false);
        setMaxLine(false);

        if (mCallbackListener != null) {
            mCallbackListener.onNeedResetFocus();
        }
    }

    public void onEditMessageClicked(EditChatHeaderView editChatHeaderView) {
        final String content = getInputText();
        String oldContent = editChatHeaderView.getChat().getContent();
        if (content != null &&
                !TextUtils.isEmpty(content.trim()) &&
                !oldContent.equalsIgnoreCase(content)) {
            if (mCallbackListener != null) {
                mCallbackListener.onEditedMessageSendClicked(content.trim());
            }
        }
    }

    public final void onReplyOrEditCloseClick() {
        if (isInRepliedInput()) {
            setInputReplyVisibility(false);
        } else if (isInEditMessageInput()) {
            resetFromEditModeToNormalMode();
        }
    }

    public void resetFromEditModeToNormalMode() {
        setInputEditVisibility(false);
        clearEditor();
        mNormalInputOptionVisibility.set(View.VISIBLE);
    }

    public ObservableInt getMsgInputMaxLine() {
        return mMsgInputMaxLine;
    }

    public ObservableBoolean getToolVisibility() {
        return mToolVisibility;
    }

    public ObservableBoolean getIsKeyboardInputSingleLine() {
        return mIsKeyboardInputSingleLine;
    }

    public void onInputClick() {
        Timber.i("onInputClick");
        if (mCallbackListener != null) {
            mCallbackListener.onNeedResetFocus();
        }

        if (!TextUtils.isEmpty(getInputText()) && !mIsAddIconAnimation.get()) {
            toggleAllToolBtn(true);
        }
    }

    public AbsCallService.CallType getJoinGroupCallType() {
        if (mConversation.isVideo()) {
            return AbsCallService.CallType.GROUP_VIDEO;
        }
        return AbsCallService.CallType.GROUP_VOICE;
    }

    public final View.OnClickListener onJoinCallClick() {
        return view -> {
            UserHelper.checkUserCallFeatureSetting(getContext(), new UserHelper.UserHelperListener() {
                @Override
                protected void onCheckUserCallFeatureFinished(boolean isCallEnable, boolean isVideoCallEnable) {
                    if (isCallEnable || isVideoCallEnable) {
                        CallingIntent callingIntent = CallingIntent.getJoinGroupCallIntent(getContext(),
                                getJoinGroupCallType(),
                                mConversation,
                                CallingIntent.StartFromType.CHAT_SCREEN.getType());
                        ((AbsBaseActivity) getContext()).startActivityForResult(callingIntent, new ResultCallback() {
                            @Override
                            public void onActivityResultSuccess(int resultCode, Intent data) {
                                if (data != null && data.getBooleanExtra(CallingFragment.SHOULD_BROADCAST_CALL_EVENT,
                                        false)) {
                                    User recipient = data.getParcelableExtra(CallingFragment.RECIPIENT);
                                    if (recipient != null && mCallbackListener != null) {
                                        onReceivedCallEvent(data,
                                                AbsCallService.CallType.getFromValue(data
                                                        .getStringExtra(CallingFragment.CALL_TYPE)),
                                                recipient,
                                                data.getBooleanExtra(CallingFragment.IS_MISSED_CALL,
                                                        false),
                                                data.getIntExtra(CallingFragment.CALLED_DURATION, 0));
                                    }
                                }
                            }
                        });
                    } else {
                        showSnackBar(R.string.call_toast_no_call_feature, true);
                    }
                }
            });
        };
    }

    public void onProfileClick() {
        if (!mIsSupportConversation.get()) {
            if (mConversation == null || TextUtils.isEmpty(mConversation.getGroupId())) {
                NavigateSellerStoreHelper.openSellerStore((AppCompatActivity) mChatDataManager.getContext(),
                        mRecipient.getId());
            } else {
                Timber.i("mConversation is " + mConversation + " with group id " + mConversation.getGroupId());
                GroupDetailIntent intent = new GroupDetailIntent(getContext(), mConversation);
                if (getContext() instanceof AbsBaseActivity) {
                    ((AbsBaseActivity) getContext()).startActivityForResult(intent, new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            if (resultCode == Activity.RESULT_OK) {
                                //Must fetch latest conversation from local db.
                                mConversation = ConversationDb.getConversationById(getContext(), mConversation.getId());
                                updateGroupConversation(mConversation);
                                mSocketBinder.onChannelCreateOrUpdate(mConversation, "onProfileClick");
                            }
                        }
                    });
                }
            }
        }
    }

    private void toggleAllToolBtn(boolean isHide) {
        mIsAddIconAnimation.set(isHide);
        mToolVisibility.set(true);
    }

    private void setMaxLine(boolean isFull) {
        if (isFull) {
            mMsgInputMaxLine.set(6);
        } else {
            mMsgInputMaxLine.set(1);
        }
    }

    public TextWatcher onTextChange() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) {
                    mCallbackListener.onStartTyping(start, count);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Ignore for edit message mode
                if (!isInEditMessageInput()) {
                    if (!TextUtils.isEmpty(s)) {
                        if (!mIsAddIconAnimation.get()) {
                            toggleAllToolBtn(true);
                            if (mCallbackListener != null) {
                                mCallbackListener.onReplaceSendIcon();
                            }
                        }
                    } else {
                        if (mIsAddIconAnimation.get()) {
                            mToolBtnVisibility.set(false);
                            mToolVisibility.set(true);
                            toggleAllToolBtn(false);
                            setMaxLine(true);

                            if (mCallbackListener != null) {
                                mCallbackListener.onReplaceRecordIcon();
                            }
                        }
                    }
                }
            }
        };
    }

    public void requestLinkPreview(Chat chat, LinkPreviewCallback listener) {
        Observable<Response<LinkPreviewModel>> call = mChatDataManager.getLinkPreview(chat);
        ResponseObserverHelper<Response<LinkPreviewModel>> helper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<Response<LinkPreviewModel>>() {
            @Override
            public void onComplete(Response<LinkPreviewModel> result) {
                Timber.i("requestLinkPreview: onComplete: " + new Gson().toJson(result.body()));
                LinkPreviewModel linkPreviewModel = result.body();
                if (linkPreviewModel != null && linkPreviewModel.isValidPreviewData()) {
                    linkPreviewModel.setId(chat.getId());
                    linkPreviewModel.setResourceContent(chat.getContent());
                    linkPreviewModel.setLinkDownloaded(true);
                    LinkPreviewDb.save(getContext(), linkPreviewModel, true);
                }
                if (listener != null) {
                    listener.onGetLinkPreviewFinished(linkPreviewModel);
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e("requestLinkPreview: onFail: " + ex.getMessage());
                if (listener != null) {
                    listener.onGetLinkPreviewFinished(null);
                }
            }
        }));
    }

    private void getChatFromDb(boolean isResetData) {
        if (MessageDb.getMessageCountForAllStatus(getContext(), mConversationId) > 0) {
            mIsLoading.set(true);
            Observable<List<BaseChatModel>> call = mChatDataManager.getLatestMessageFromDb(mConversationId);
            BaseObserverHelper<List<BaseChatModel>> helper = new BaseObserverHelper<>(getContext(), call);
            addDisposable(helper.execute(new OnCallbackListener<List<BaseChatModel>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    Timber.e("Load local chat failed: " + ex.toString());
                    mIsLoading.set(false);
                    insertEmptyLocalData();
                    loadChatHistoryFromServer(null, isResetData);
                }

                public void onComplete(List<BaseChatModel> result) {
                    mIsLoading.set(false);
                    // Add data from db first, then continue on with loading data from API
                    if (mCallbackListener != null) {
                        Timber.i("Load success from local. " + (result != null ? result.size() : null));
                        mCallbackListener.onLocalLoadSuccess(result, isResetData, mChatDataManager.isCanLoadMore());
                    }
                    Chat oldestChat = null;
                    if (result != null && !result.isEmpty()) {
                        oldestChat = getOldestChatFromList(result);
                    }
                    loadChatHistoryFromServer(oldestChat, isResetData);
                }
            }));
        } else {
            if (NetworkStateUtil.isNetworkAvailable(getContext())) {
                mIsLoading.set(true);
            } else {
                /*
                    Must make delay to make sure the view model is created successfully before any attempt
                    to call its method.
                 */
                new Handler().postDelayed(this::insertEmptyLocalData, 1000);
            }
            // No local chat in db, load from API
            Timber.d("No chat in local DB, loading history");
            loadChatHistoryFromServer(null, isResetData);
        }
    }

    private Chat getOldestChatFromList(List<BaseChatModel> result) {
        for (BaseChatModel baseChatModel : result) {
            if (baseChatModel instanceof Chat) {
                return (Chat) baseChatModel;
            }
        }

        return null;
    }

    private void insertEmptyLocalData() {
        /*
        We need to construct the default list event the load from local is failed or no data return.
         */
        if (mCallbackListener != null) {
            mCallbackListener.onLocalLoadSuccess(new ArrayList<>(), false, false);
        }
    }

    public void checkToReloadChatHistoryFromServer(Chat oldestChat) {
        Timber.i("checkToReloadChatHistoryFromServer");
        if (!mChatDataManager.isJumpSearch() && mChatDataManager.isHasLoadMessageFailed()) {
            loadChatHistoryFromServer(oldestChat, false);
        }
    }

    public void loadChatHistoryFromServer(Chat oldestChat, boolean isResetData) {
        if (mChatDataManager.getConversation() == null) {
            mIsLoading.set(false);
            if (mCallbackListener != null) {
                mCallbackListener.onLoadChatFromServerFailed(new ErrorThrowable(ErrorThrowable.GENERAL_ERROR,
                        getContext().getString(R.string.error_no_data)), isResetData);
            }
            return;
        }

        Disposable disposable = mChatDataManager.checkToRequestGroupConversationDetailIfNecessary(() -> {
            Observable<List<BaseChatModel>> callback = mChatDataManager.checkToLoadChatHistoryFromServer(oldestChat);
            BaseObserverHelper<List<BaseChatModel>> helper = new BaseObserverHelper<>(getContext(), callback);
            addDisposable(helper.execute(new OnCallbackListener<List<BaseChatModel>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    Timber.e("Load server chat failed: " + ex.toString());
                    if (mIsLoading.get()) {
                        mIsLoading.set(false);
                    }
                    if (mCallbackListener != null) {
                        mCallbackListener.onLoadChatFromServerFailed(ex, isResetData);
                    }
                }

                @Override
                public void onComplete(List<BaseChatModel> value) {
                    if (mIsLoading.get()) {
                        mIsLoading.set(false);
                    }
                    if (mCallbackListener != null) {
                        Timber.i("Load success from server. " + (value != null ? value.size() : null));
                        mCallbackListener.onChatFromServerSuccess(value,
                                isResetData,
                                mChatDataManager.isCanLoadMore(),
                                mChatDataManager.isRequestMessageHistory());
                    }
                }
            }));
        });
        if (disposable != null) {
            addDisposable(disposable);
        }
    }

    public void jumpSearchMessage(boolean isLoadMoreFromBottom) {
        if (!isLoadMoreFromBottom) {
            mIsLoading.set(true);
        }
        Observable<List<BaseChatModel>> callback = mChatDataManager.jumpSearchMessage(isLoadMoreFromBottom);
        BaseObserverHelper<List<BaseChatModel>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<List<BaseChatModel>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e("jumpSearchMessage failed: " + ex.toString());
                mIsLoading.set(false);
                if (mCallbackListener != null) {
                    mCallbackListener.onJumpSearchLoadFailed(ex, isLoadMoreFromBottom);
                }
            }

            @Override
            public void onComplete(List<BaseChatModel> value) {
                mIsLoading.set(false);
                if (mCallbackListener != null) {
                    Timber.i("jumpSearchMessage success:" + (value != null ? value.size() : null));
                    mCallbackListener.onJumpSearchLoadSuccess(value,
                            mChatDataManager.isCanLoadMore(),
                            mChatDataManager.isCanLoadMoreFromBottom(),
                            isLoadMoreFromBottom);
                }
            }
        }));
    }

    public void loadMoreData(OnLoadMoreCallback listener) {
        Observable<List<BaseChatModel>> callback = mChatDataManager.loadMore(listener.getOldestChat());
        BaseObserverHelper<List<BaseChatModel>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<List<BaseChatModel>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                listener.onFail(ex);
            }

            @Override
            public void onComplete(List<BaseChatModel> value) {
                listener.onComplete(value, mChatDataManager.isCanLoadMore());
            }
        }));
    }


    public AnimatorListenerAdapter getAnimatorListener() {
        return new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Timber.i("onAnimationEnd");
                if (mIsAddIconAnimation.get()) {
                    checkToolVisibility();
                    if (!checkToolVisibility()) {
                        mToolBtnVisibility.set(true);
                        mToolVisibility.set(false);
                        setMaxLine(true);
                    } else {
                        mToolBtnVisibility.set(false);
                        setMaxLine(true);
                    }
                }
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
            }
        };
    }

    public Boolean checkToolVisibility() {
        return !mCameraVisibility.get() &&
                !mFileVisibility.get() &&
                !mPictureVisibility.get() &&
                !mGifVisibility.get();
    }

    public void onSendButtonClick() {
        if (!isAbleToSendMessage()) {
            showSnackBar(R.string.chat_popup_no_recipient, true);
            return;
        }

        final String content = getInputText();
        if (!TextUtils.isEmpty(content)) {
            if (TextUtils.isEmpty(content.trim())) {
                return;
            }

            if (getContext() instanceof AbsBaseActivity) {
                ((AbsBaseActivity) getContext()).startLoginWizardActivity(new LoginActivityResultCallback() {
                    @Override
                    public void onActivityResultSuccess(boolean isAlreadyLogin) {
                        if (isGroupConversation()) {
                            clearEditor();
                            if (mCallbackListener != null) {
                                mCallbackListener.onSendButtonClick(content.trim(), mConversation);
                            }
                        } else {
                            Chat chat = Chat.getNewChat(getContext(), mRecipient, mProduct);
                            chat.setChannelId(mConversationId);
                            chat.setContent(content.trim());
                            clearEditor();
                            if (mCallbackListener != null) {
                                mCallbackListener.onSendButtonClick(chat);
                            }
                        }
                        mToolBtnVisibility.set(false);
                        toggleAllToolBtn(false);

                    }
                });
            }
        }
    }

    private void clearEditor() {
        mEditText.setText("");
    }

    public void onGalleryClick() {
        if (!isAbleToSendMessage()) {
            showSnackBar(R.string.chat_popup_no_recipient, true);
            return;
        }

        if (getContext() instanceof AbsBaseActivity) {
            Intent intent = MyCameraIntent.newChatGalleryInstance(getContext());
            ((AbsBaseActivity) getContext()).startActivityForResult(intent, new ResultCallback() {

                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    MyCameraResultIntent resultIntent = new MyCameraResultIntent(data);
                    ArrayList<Media> media = resultIntent.getMedia();
                    Chat chat = Chat.getNewChat(getContext(), mRecipient, mProduct);
                    chat.setChannelId(mConversationId);
                    chat.setMediaList(media);
                    Timber.i("MediaList: " + new Gson().toJson(media));

                    if (mCallbackListener != null) {
                        mCallbackListener.onSendButtonClick(chat);
                    }
                }
            });
        }
    }

    public void onFileClick() {
        if (!isAbleToSendMessage()) {
            showSnackBar(R.string.chat_popup_no_recipient, true);
            return;
        }
        mMyFileIntent = new MyFileIntent();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).checkPermission(new CheckPermissionCallbackHelper((Activity) getContext(), CheckPermissionCallbackHelper.Type.STORAGE) {
                @Override
                public void onPermissionGranted() {
                    mMyFileIntent.performFileSearch();
                    ((AbsBaseActivity) getContext()).startActivityForResult(mMyFileIntent, new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            MyFileResultIntent resultIntent = new MyFileResultIntent(data, getContext());
                            List<Media> media = resultIntent.getFile();
                            long maxFileSize = media.get(0).getSize() / (1024 * 1024);
                            //Limit size is max 10 MB.
                            if (maxFileSize <= 10) {
                                Chat chat = Chat.getNewChat(getContext(), mRecipient, mProduct);
                                chat.setChannelId(mConversationId);
                                chat.setMediaList(media);
                                if (mCallbackListener != null) {
                                    mCallbackListener.onSendButtonClick(chat);
                                }
                            } else {
                                View view = ((AbsBaseActivity) getContext()).findViewById(android.R.id.content);
                                SnackBarUtil.showSnackBar(view, R.string.toast_file_too_large, true);
                            }
                        }
                    });
                }
            }, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    public void onCameraClick() {
        if (!isAbleToSendMessage()) {
            showSnackBar(R.string.chat_popup_no_recipient, true);
            return;
        }

        if (getContext() instanceof AbsBaseActivity) {
            Intent intent = MyCameraIntent.newChatCameraInstance(getContext());

            ((AbsBaseActivity) getContext()).startActivityForResult(intent, new ResultCallback() {

                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    MyCameraResultIntent resultIntent = new MyCameraResultIntent(data);
                    ArrayList<Media> media = resultIntent.getMedia();
                    Chat chat = Chat.getNewChat(getContext(), mRecipient, mProduct);
                    chat.setChannelId(mConversationId);
                    chat.setMediaList(media);

                    if (mCallbackListener != null) {
                        mCallbackListener.onSendButtonClick(chat);
                    }
                }
            });
        }
    }

    public void saveImageVideoIntoLocal(List<Media> medias) {
        if (getContext() != null) {
            for (Media media : medias) {
                if (media.getType() == MediaType.IMAGE) {
                    mSaveImageHelper.startSave(media, new SaveImageHelper.OnCallback() {
                        @Override
                        public void onFail() {
                            handleCompleteFaildSavedMedia(medias.size(), true);
                        }

                        @Override
                        public void onSuccess(@NonNull Bitmap resource) {
                            handleCompleteFaildSavedMedia(medias.size(), false);


                        }
                    });
                } else {
                    // it is a video
                    mSaveVideoHelper.download(media, new SaveVideoHelper.OnVideoSaveCallBack() {
                        @Override
                        public void onFail(String message) {
                            handleCompleteFaildSavedMedia(medias.size(), true);
                        }

                        @Override
                        public void onSuccess() {
                            handleCompleteFaildSavedMedia(medias.size(), false);
                        }
                    });
                }
            }

        }
    }

    public boolean enableMentionUser() {
        return isGroupConversation();
    }

    private void handleCompleteFaildSavedMedia(int mediasSize, boolean alreadyFailSomeMedia) {
        Timber.e("mMediaSaved " + mMediaSaved);
        if (alreadyFailSomeMedia) {
            ToastUtil.showToast(getContext(), R.string.toast_save_file_fail, true);
            mMediaSaved = 0;
            return;
        }
        mMediaSaved++;

        Timber.e("mMediaSaved video" + mMediaSaved);

        if (mMediaSaved == mediasSize) {
            ToastUtil.showToast(getContext(), R.string.toast_file_save, false);
            mMediaSaved = 0;
        }
    }

    private String getInputText() {
        return RenderTextAsMentionable.render(getContext(), mEditText.getText());
    }

    public void onScrollDownClicked() {
        if (mCallbackListener != null) {
            mCallbackListener.onScrollDownToLastItem();
        }
    }

    public KeyboardInputEditor.KeyBoardInputCallbackListener onKeyBoardInputCallbackListener() {
        return (inputContentInfo, flags, opts) -> {
            if (!isInEditMessageInput()) {
                Uri uri = inputContentInfo.getLinkUri();
                if (uri != null) {
                    ArrayList<Media> mediaArrayList = new ArrayList<>();
                    Media media = new Media();
                    media.setUrl(String.valueOf(uri));
                    media.setType(MediaType.IMAGE);
                    //Set the default size
                    //The same size as tiny gift size of Tenor we are using
                    media.setWidth(220);
                    media.setHeight(160);
                    mediaArrayList.add(media);

                    Chat chat = Chat.getNewChat(ChatDialogViewModel.this.getContext(), mRecipient, null);
                    chat.setChannelId(mConversationId);
                    chat.setMediaList(mediaArrayList);
                    if (ChatDialogViewModel.this.isGroupConversation()) {
                        chat.setSendTo(mConversation.getGroupId());
                        chat.setGroup(true);
                    }
                    if (mCallbackListener != null) {
                        mCallbackListener.onKeyboardMediaSelected(chat, ChatDialogViewModel.this.isGroupConversation());
                    }
                }
            }
        };
    }

    public QueryTokenReceiver getQueryTokenReceiver(RichEditorView richEditorView) {
        return new QueryTokenReceiver() {
            @Override
            public void onQueryReceived(@NonNull QueryToken queryToken) {
                if (queryToken.getExplicitChar() == '@') {
                    getMentionUserList(queryToken.getKeywords(), new OnCallbackListener<List<UserMentionable>>() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                            richEditorView.hideListPopupWindow();
                        }

                        @Override
                        public void onComplete(List<UserMentionable> result) {
                            if (result != null && !result.isEmpty()) {
                                validateMentionableUser(result);
                                richEditorView.showListPopupWindow(result);
                            } else {
                                richEditorView.hideListPopupWindow();
                            }
                        }
                    });
                } else {
                    richEditorView.hideListPopupWindow();
                }
            }

            @Override
            public void invalidToken() {

            }
        };
    }

    private void validateMentionableUser(List<UserMentionable> result) {
        //Allow to mention user inside group chat only and exclude current user too.
        if (mConversation != null && mConversation.getParticipants() != null && result != null) {
            String currentUserId = SharedPrefUtils.getUserId(getContext());
            for (int size = result.size() - 1; size >= 0; size--) {
                boolean contain = false;
                for (User participant : mConversation.getParticipants()) {
                    if (TextUtils.equals(participant.getId(), result.get(size).getUserId()) &&
                            !TextUtils.equals(currentUserId, result.get(size).getUserId())) {
                        contain = true;
                        break;
                    }
                }

                if (!contain) {
                    result.remove(size);
                }
            }
        }
    }

    private void getMentionUserList(String userName, OnCallbackListener<List<UserMentionable>> listOnCallbackListener) {
        Observable<List<UserMentionable>> observable = mChatDataManager.getUserMentionList(userName);
        BaseObserverHelper<List<UserMentionable>> helper = new BaseObserverHelper<>(getContext(), observable);
        helper.execute(listOnCallbackListener);
    }

    public ViewCommunicatorListener getViewCommunicatorListener() {
        return editable -> mEditText = editable;
    }

    public interface OnCallback extends OnCallbackListListener<List<BaseChatModel>> {

        void onEditedMessageSendClicked(String editedContent);

        void onSendButtonClick(Chat chat);

        void onStartTyping(int start, int count);

        void onReplaceRecordIcon();

        void onReplaceSendIcon();

        void onSendButtonClick(String content, Conversation conversation);

        void onKeyboardMediaSelected(Chat message, boolean isGroup);

        void onNeedResetFocus();

        void onScrollDownToLastItem();

        void onLocalLoadSuccess(List<BaseChatModel> chatModelList, boolean isResetData, boolean isCanLoadMore);

        void onLocalLoadFailed(ErrorThrowable ex, boolean isResetData);

        void onChatFromServerSuccess(List<BaseChatModel> chatModelList,
                                     boolean isResetData,
                                     boolean isCanLoadMore,
                                     boolean isRequestChatHistory);

        void onLoadChatFromServerFailed(ErrorThrowable ex, boolean isResetData);

        void onJumpSearchLoadSuccess(List<BaseChatModel> chatModelList,
                                     boolean isCanLoadMoreFromTop,
                                     boolean isCanLoadMoreFromBottom,
                                     boolean isLoadMoreFromBottom);

        void onJumpSearchLoadFailed(ErrorThrowable ex, boolean isLoadMoreFromBottom);
    }

    public interface LinkPreviewCallback {
        void onGetLinkPreviewFinished(LinkPreviewModel previewModel);
    }

    public interface OnLoadMoreCallback extends OnCallbackListListener<List<BaseChatModel>> {
        Chat getOldestChat();
    }

    public interface ViewCommunicatorListener {

        void onObtainEditable(EditText editText);

    }

    @BindingAdapter(value = {"addTextChangeListener",
            "enableMention",
            "queryTokenReceiver",
            "setKeyBoardInputCallbackListener",
            "maxLines",
            "viewCommunicatorListener"}, requireAll = false)
    public static void bindMentionableEditText(RichEditorView editorView,
                                               TextWatcher textWatcher,
                                               boolean enableMention,
                                               QueryTokenReceiver queryTokenReceiver,
                                               KeyboardInputEditor.KeyBoardInputCallbackListener inputCallbackListener,
                                               int maxLine,
                                               ViewCommunicatorListener communicatorListener) {
        EditTextBindingUtil.addTextChangeListener(editorView.getMentionsEditText(), textWatcher);
        editorView.getMentionsEditText().setMaxLines(maxLine);
        if (inputCallbackListener != null) {
            EditTextBindingUtil.setKeyBoardInputCallbackListener(editorView.getMentionsEditText(), inputCallbackListener);
        }
        if (communicatorListener != null) {
            communicatorListener.onObtainEditable(editorView.getMentionsEditText());
        }
        if (enableMention) {
            editorView.setQueryTokenReceiver(queryTokenReceiver);
        }
    }

    public final View.OnClickListener onTelegramRedirectUserListClick() {
        return view -> {
            TelegramUserRedirectIntent intent = new TelegramUserRedirectIntent(getContext(), mConversation.getTelegramConnection().getUserId());
            if (getContext() instanceof AbsBaseActivity) {
                ((AbsBaseActivity) getContext()).startActivityForResult(intent, new ResultCallback() {
                    @Override
                    public void onActivityResultSuccess(int resultCode, Intent data) {
                        if (resultCode == Activity.RESULT_OK) {
                            //Must fetch latest conversation from local db.
                            mConversation = ConversationDb.getConversationById(getContext(), mConversation.getId());
                            updateGroupConversation(mConversation);
                            mSocketBinder.onChannelCreateOrUpdate(mConversation, "onProfileClick");
                        }
                    }
                });
            }
        };
    }

    @BindingAdapter("isVideoCall")
    public static void setJoinCallIcon(TextView textView, boolean isVideoCall) {
        textView.setCompoundDrawablesWithIntrinsicBounds(isVideoCall ? R.drawable.ic_x_small_video_camera
                        : R.drawable.ic_x_small_telephone,
                0,
                0,
                0);
    }
}
