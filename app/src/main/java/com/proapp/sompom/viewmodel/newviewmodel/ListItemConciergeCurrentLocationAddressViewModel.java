package com.proapp.sompom.viewmodel.newviewmodel;

import android.view.View;

import androidx.databinding.ObservableField;

import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.ConciergeCurrentLocationDisplayAdaptive;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

/**
 * Created by Or Vitovongsak on 17/11/21.
 */
public class ListItemConciergeCurrentLocationAddressViewModel extends AbsBaseViewModel {


    private ObservableField<String> mAddressLabel = new ObservableField<>();
    private ObservableField<String> mAddress = new ObservableField<>();

    private ConciergeCurrentLocationDisplayAdaptive mAdaptive;
    private final OnItemClickListener<ConciergeCurrentLocationDisplayAdaptive> mListener;

    public ListItemConciergeCurrentLocationAddressViewModel(ConciergeCurrentLocationDisplayAdaptive adaptive,
                                                            OnItemClickListener<ConciergeCurrentLocationDisplayAdaptive> listener) {
        setData(adaptive);
        mListener = listener;
    }

    public ObservableField<String> getAddressLabel() {
        return mAddressLabel;
    }

    public ObservableField<String> getAddress() {
        return mAddress;
    }

    public void setData(ConciergeCurrentLocationDisplayAdaptive adaptive) {
        mAdaptive = adaptive;
        if (mAdaptive instanceof ConciergeUserAddress) {
            ConciergeUserAddress userAddress = (ConciergeUserAddress) mAdaptive;

            mAddressLabel.set(userAddress.getLabel());
            mAddress.set(userAddress.getAddress());
        } else if (mAdaptive instanceof SearchAddressResult) {
            SearchAddressResult resultAddress = (SearchAddressResult) mAdaptive;

            mAddressLabel.set(resultAddress.getPlaceTitle());
            mAddress.set(resultAddress.getAddress());
        }
    }

    public void onClick(View view) {
        if (mListener != null) {
            mListener.onClick(mAdaptive);
        }
    }
}
