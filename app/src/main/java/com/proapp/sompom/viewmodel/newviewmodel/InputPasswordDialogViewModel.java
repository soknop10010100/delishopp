package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.model.request.ChangeOrResetPasswordBody;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import timber.log.Timber;

public class InputPasswordDialogViewModel extends AbsLoadingViewModel {

    private ObservableField<String> mPassword = new ObservableField<>();
    private ObservableField<String> mPasswordError = new ObservableField<>();
    private ObservableField<String> mWarningText = new ObservableField<>();
    private ObservableInt mWarningVisibility = new ObservableInt(View.GONE);
    private InputPasswordDialogListener mListener;

    public InputPasswordDialogViewModel(Context context,
                                        InputPasswordDialogListener listener) {
        super(context);
        mListener = listener;
    }

    public ObservableField<String> getPassword() {
        return mPassword;
    }

    public ObservableField<String> getPasswordError() {
        return mPasswordError;
    }

    public ObservableField<String> getWarningText() {
        return mWarningText;
    }

    public ObservableInt getWarningVisibility() {
        return mWarningVisibility;
    }

    public void setWarningText(String warning) {
        mWarningText.set(warning);
        if (warning != null && !warning.trim().isEmpty()) {
            mWarningVisibility.set(View.VISIBLE);
        }
    }

    public void requestPassword() {
        if (mListener != null) {
            mListener.onPasswordEnterSuccess(mPassword.get());
        }
    }

    public ChangeOrResetPasswordBody getBody() {
        return new ChangeOrResetPasswordBody(mPassword.get(), mPassword.get());
    }

    public boolean isReadyToProceed() {
        return isAllFieldValid();
    }

    private boolean isAllFieldValid() {
        boolean valid = true;

        if (TextUtils.isEmpty(mPassword.get())) {
            Timber.e("Password: " + mPassword.get());
            mPasswordError.set(getContext().getString(R.string.login_error_field_required));
            valid = false;
        } else {
            mPasswordError.set(null);
        }
        return valid;
    }

    public void clearInput() {
        Timber.e("Clear input click");
        mPassword.set("");
    }

    public interface InputPasswordDialogListener {
        void onPasswordEnterSuccess(String password);
    }
}
