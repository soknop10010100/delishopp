package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.intent.newintent.CallIntent;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.fragment.CallingFragment;
import com.proapp.sompom.utils.NetworkStateUtil;
import com.sompom.baseactivity.ResultCallback;

public abstract class SupportCallViewModel extends AbsLoadingViewModel {

    private SupportCallViewModelCallback mSupportCallViewModelCallback;
    protected ObservableInt mCallVisibility = new ObservableInt(View.GONE);
    protected ObservableInt mVideoCallVisibility = new ObservableInt(View.GONE);

    public SupportCallViewModel(Context context, SupportCallViewModelCallback supportCallViewModelCallback) {
        super(context);
        mSupportCallViewModelCallback = supportCallViewModelCallback;
    }

    public ObservableInt getCallVisibility() {
        return mCallVisibility;
    }

    public ObservableInt getVideoCallVisibility() {
        return mVideoCallVisibility;
    }

    protected void setCallActionVisibility() {
        UserHelper.checkUserCallFeatureSetting(getContext(), new UserHelper.UserHelperListener() {
            @Override
            protected void onCheckUserCallFeatureFinished(boolean isCallEnable, boolean isVideoCallEnable) {
                mCallVisibility.set(isCallEnable ? View.VISIBLE : View.GONE);
                mVideoCallVisibility.set(isVideoCallEnable ? View.VISIBLE : View.GONE);
            }
        });
    }

    protected abstract boolean isGroupConversation();

    protected abstract User getRecipient();

    protected abstract Conversation getConversation();

    protected abstract CallIntent.StartFromType getStartCallScreenType();

    protected void onReceivedCallEvent(Intent data,
                                       AbsCallService.CallType callType,
                                       User recipient,
                                       boolean isMissedCall,
                                       int calledDuration) {
        if (mSupportCallViewModelCallback != null) {
            mSupportCallViewModelCallback.onReceivedCallEvent(AbsCallService.CallType
                            .getFromValue(data.getStringExtra(CallingFragment.CALL_TYPE)),
                    recipient,
                    data.getBooleanExtra(CallingFragment.IS_MISSED_CALL,
                            false),
                    data.getIntExtra(CallingFragment.CALLED_DURATION, 0));
        }
    }

    public final View.OnClickListener onCallClick() {
        return view -> {
            if (!NetworkStateUtil.isNetworkAvailable(getContext())) {
                showSnackBar(R.string.call_toast_no_internet, true);
                return;
            }

            if (CallHelper.isDeviceInPhoneCallProcess(getContext())) {
                showSnackBar(R.string.call_toast_already_in_call_description, true);
                return;
            }

            UserHelper.checkUserCallFeatureSetting(getContext(), new UserHelper.UserHelperListener() {
                @Override
                protected void onCheckUserCallFeatureFinished(boolean isCallEnable, boolean isVideoCallEnable) {
                    if (isCallEnable) {
                        if (getContext() instanceof AbsBaseActivity) {
                            CallingIntent callingIntent = new CallingIntent(getContext(),
                                    isGroupConversation() ? AbsCallService.CallType.GROUP_VOICE : AbsCallService.CallType.VOICE,
                                    getRecipient(),
                                    isGroupConversation() ? getConversation() : null,
                                    getStartCallScreenType().getType());
                            ((AbsBaseActivity) getContext()).startActivityForResult(callingIntent, new ResultCallback() {
                                @Override
                                public void onActivityResultSuccess(int resultCode, Intent data) {
                                    if (data != null && data.getBooleanExtra(CallingFragment.SHOULD_BROADCAST_CALL_EVENT, false)) {
                                        User recipient = data.getParcelableExtra(CallingFragment.RECIPIENT);
                                        onReceivedCallEvent(data,
                                                AbsCallService.CallType
                                                        .getFromValue(data.getStringExtra(CallingFragment.CALL_TYPE)),
                                                recipient,
                                                data.getBooleanExtra(CallingFragment.IS_MISSED_CALL,
                                                        false),
                                                data.getIntExtra(CallingFragment.CALLED_DURATION, 0));
                                    }
                                }
                            });
                        }
                    } else {
                        showSnackBar(R.string.call_toast_no_call_feature, true);
                    }
                }
            });
        };
    }

    public final View.OnClickListener onVideoCallClick() {
        return view -> {
            if (!NetworkStateUtil.isNetworkAvailable(getContext())) {
                showSnackBar(R.string.call_toast_no_internet, true);
                return;
            }

            if (CallHelper.isDeviceInPhoneCallProcess(getContext())) {
                showSnackBar(R.string.call_toast_already_in_call_description, true);
                return;
            }
            UserHelper.checkUserCallFeatureSetting(getContext(), new UserHelper.UserHelperListener() {
                @Override
                protected void onCheckUserCallFeatureFinished(boolean isCallEnable, boolean isVideoCallEnable) {
                    if (isVideoCallEnable) {
                        CallingIntent callingIntent = new CallingIntent(getContext(),
                                isGroupConversation() ? AbsCallService.CallType.GROUP_VIDEO : AbsCallService.CallType.VIDEO,
                                getRecipient(),
                                isGroupConversation() ? getConversation() : null,
                                getStartCallScreenType().getType());
                        ((AbsBaseActivity) getContext()).startActivityForResult(callingIntent, new ResultCallback() {
                            @Override
                            public void onActivityResultSuccess(int resultCode, Intent data) {
                                if (data != null && data.getBooleanExtra(CallingFragment.SHOULD_BROADCAST_CALL_EVENT,
                                        false)) {
                                    User recipient = data.getParcelableExtra(CallingFragment.RECIPIENT);
                                    onReceivedCallEvent(data,
                                            AbsCallService.CallType
                                                    .getFromValue(data.getStringExtra(CallingFragment.CALL_TYPE)),
                                            recipient,
                                            data.getBooleanExtra(CallingFragment.IS_MISSED_CALL,
                                                    false),
                                            data.getIntExtra(CallingFragment.CALLED_DURATION, 0));
                                }
                            }
                        });
                    } else {
                        showSnackBar(R.string.call_toast_no_call_feature, true);
                    }
                }
            });
        };
    }

    public interface SupportCallViewModelCallback {

        void onReceivedCallEvent(AbsCallService.CallType callType,
                                 User recipient,
                                 boolean isMissedCall,
                                 int calledDuration);
    }
}
