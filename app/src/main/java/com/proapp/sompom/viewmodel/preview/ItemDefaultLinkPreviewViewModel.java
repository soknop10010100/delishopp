package com.proapp.sompom.viewmodel.preview;

import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.model.BasePreviewModel;
import com.proapp.sompom.model.result.LinkPreviewModel;

/**
 * Created by Chhom Veasna on 05/11/2020.
 */
public class ItemDefaultLinkPreviewViewModel extends AbsItemLinkPreviewViewModel {

    public ItemDefaultLinkPreviewViewModel(LinkPreviewModel linkPreviewModel, PreviewContext previewContext) {
        super(linkPreviewModel, previewContext);
         /*
          This ViewModel is being for both normal and image preview.
          The different is that image preview will render the link directly and put link as description
          preview too.
         */
        BasePreviewModel.PreviewType type = BasePreviewModel.PreviewType.getFromValue(linkPreviewModel.getType());
        if (type == BasePreviewModel.PreviewType.FILE && LinkPreviewRetriever.isImageType(linkPreviewModel.getLink())) {
            setPreviewMediaUrl(linkPreviewModel.getLink());
        }
    }
}
