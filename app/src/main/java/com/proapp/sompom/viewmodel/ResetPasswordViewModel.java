package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.DelayClickableSpan;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.SupportCustomErrorResponse2;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ResetPasswordDataManager;
import com.proapp.sompom.utils.KeyboardUtil;
import com.sompom.pushy.service.SendBroadCastHelper;

import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Veasna Chhom on 5/18/22.
 */
public class ResetPasswordViewModel extends AbsResetPasswordViewModel {

    public static final String RESET_PASSWORD_SUCCESS_EVENT = "RESET_PASSWORD_SUCCESS_EVENT";
    public static final String FIELD_EMAIL = "FIELD_EMAIL";

    protected final ObservableField<String> mOtp = new ObservableField<>();

    public ResetPasswordViewModel(Context context, String email, ResetPasswordDataManager dataManager) {
        super(context, dataManager);
        mDataManager = dataManager;
        mEmail.set(email);
        mPhoneNumberUtil = PhoneNumberUtil.createInstance(getContext());
    }

    public ObservableField<String> getOtp() {
        return mOtp;
    }

    @Override
    protected boolean shouldEnableActionButton() {
        return isEmailValid() &&
                !TextUtils.isEmpty(mPassword.get()) &&
                !TextUtils.isEmpty(mConfirmPassword.get()) &&
                !TextUtils.isEmpty(mOtp.get());
    }

    @Override
    public void onActionButtonClicked() {
        if (isPasswordMatch()) {
            if (isPasswordCorrect()) {
                if (getContext() instanceof Activity) {
                    KeyboardUtil.hideKeyboard((Activity) getContext());
                }
                requestResetPassword();
            }
        }
    }

    private void requestResetPassword() {
        showLoading(true);
        Observable<Response<SupportCustomErrorResponse2>> checkEmailService = mDataManager.getResetPasswordService(mEmail.get(), mPassword.get(), mOtp.get());
        ResponseObserverHelper<Response<SupportCustomErrorResponse2>> helper = new ResponseObserverHelper<>(getContext(), checkEmailService);
        addDisposable(helper.execute(new OnCallbackListener<Response<SupportCustomErrorResponse2>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<SupportCustomErrorResponse2> result) {
                hideLoading();
                if (!result.body().isError()) {
                    MessageDialog messageDialog = MessageDialog.newInstance();
                    messageDialog.setCancelable(false);
                    messageDialog.setTitle(mContext.getString(R.string.change_password_reset_title));
                    messageDialog.setMessage(mContext.getString(R.string.change_password_popup_success_reset_password_description));
                    messageDialog.setLeftText(mContext.getString(R.string.popup_ok_button), view -> {
                        Intent intent = new Intent(RESET_PASSWORD_SUCCESS_EVENT);
                        intent.putExtra(FIELD_EMAIL, mEmail.get());
                        SendBroadCastHelper.verifyAndSendBroadCast(mContext, intent);
                        ((AbsBaseActivity) mContext).setResult(AppCompatActivity.RESULT_OK);
                        ((AbsBaseActivity) mContext).finish();
                    });
                    messageDialog.show(((AbsBaseActivity) mContext).getSupportFragmentManager());
                } else {
                    showSnackBar(result.body().getErrorMessage(), true);
                }
            }
        }));
    }

    public final ClickableSpan getResentOTPClickableSpan() {
        return new DelayClickableSpan() {
            @Override
            public void onDelayClick(@NonNull View widget) {
                requestForgotPassword(true, mEmail.get());
            }
        };
    }

    @Override
    protected void onRequestForgotPasswordSuccess(boolean isResend) {
        showSnackBar(mContext.getString(R.string.change_password_reset_resend_otp_success), false);
    }
}
