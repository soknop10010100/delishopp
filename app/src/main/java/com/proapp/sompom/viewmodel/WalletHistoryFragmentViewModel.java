package com.proapp.sompom.viewmodel;

import android.content.Context;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.WalletHistory;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.WalletHistoryDataManager;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Chhom Veasna on 8/23/22.
 */
public class WalletHistoryFragmentViewModel extends AbsLoadingViewModel {

    private final WalletHistoryDataManager mDataManager;
    private final ObservableBoolean mIsRefreshing = new ObservableBoolean();
    private final WalletHistoryFragmentViewModelListener mListener;

    public WalletHistoryFragmentViewModel(Context context,
                                          WalletHistoryDataManager dataManager,
                                          WalletHistoryFragmentViewModelListener listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
        requestData(false, false);
    }

    public ObservableBoolean getIsRefreshing() {
        return mIsRefreshing;
    }

    public void loadMore() {
        requestData(false, true);
    }

    private void requestData(boolean isRefresh, boolean isLoadMore) {
        if (!isRefresh && !isLoadMore) {
            showLoading();
        }

        Observable<List<WalletHistory>> observable = mDataManager.getWalletHistoryListService(isLoadMore);
        BaseObserverHelper<List<WalletHistory>> helper = new BaseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<List<WalletHistory>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (!isRefresh && !isLoadMore) {
                    hideLoading();
                    showError(ex.getMessage(), true);
                } else {
                    mIsRefreshing.set(false);
                }
            }

            @Override
            public void onComplete(List<WalletHistory> data) {
                if (!isRefresh && !isLoadMore) {
                    hideLoading();
                    if (data.isEmpty()) {
                        showError(mContext.getString(R.string.error_no_data), true);
                    } else {
                        if (mListener != null) {
                            mListener.onLoadDataSuccess(data, false, false, mDataManager.isCanLoadMore());
                        }
                    }
                } else {
                    mIsRefreshing.set(false);
                    if (mListener != null) {
                        mListener.onLoadDataSuccess(data, isRefresh, isLoadMore, mDataManager.isCanLoadMore());
                    }
                }
            }
        }));
    }

    @Override
    public void onRetryClick() {
        requestData(false, false);
    }

    public void onRefresh() {
        mIsRefreshing.set(true);
        requestData(true, false);
    }

    public interface WalletHistoryFragmentViewModelListener {
        void onLoadDataSuccess(List<WalletHistory> data,
                               boolean isRefreshed,
                               boolean isFromLoadMore,
                               boolean isCanLoadMore);
    }
}
