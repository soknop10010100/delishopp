package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import android.view.ViewGroup;

public class ListItemUserGroupTitleViewModel {

    private ObservableInt mPaddingLefRight = new ObservableInt();
    private ObservableField<String> mTitle = new ObservableField<>();

    public ListItemUserGroupTitleViewModel(int paddingLefRight,
                                           String title) {
        mPaddingLefRight.set(paddingLefRight);
        mTitle.set(title);
    }

    public ObservableInt getPaddingLefRight() {
        return mPaddingLefRight;
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    @BindingAdapter("setPaddingLeftRight")
    public static void setViewGroupPaddingLeft(ViewGroup viewGroup, int padding) {
        viewGroup.setPadding(padding, 0, padding, 0);
    }
}
