package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.content.Intent;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.DelayClickableSpan;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.IntentUtil;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;
import com.resourcemanager.helper.LocaleManager;
import com.sompom.baseactivity.ResultCallback;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 26/1/22.
 */
public class UserLoginRegisterBottomSheetDialogViewModel extends AbsLoadingViewModel {

    private final ObservableField<String> mFirstButtonText = new ObservableField<>();
    private final ObservableInt mSecondLoginButtonVisibility = new ObservableInt(View.GONE);
    private final ObservableField<Spannable> mTermConditionPrivacyPolicySpan = new ObservableField<>();

    private DialogUserLoginRegisterBottomSheetViewModelCallback mCallback;

    public UserLoginRegisterBottomSheetDialogViewModel(Context context,
                                                       DialogUserLoginRegisterBottomSheetViewModelCallback callback) {
        super(context);
        mCallback = callback;
        initData();
    }

    public ObservableField<String> getFirstButtonText() {
        return mFirstButtonText;
    }

    public ObservableInt getSecondLoginButtonVisibility() {
        return mSecondLoginButtonVisibility;
    }

    public ObservableField<Spannable> getTermConditionPrivacyPolicySpan() {
        return mTermConditionPrivacyPolicySpan;
    }

    private void initData() {
        // For determining the appearance of the first and second login button. Currently, the second
        // button will always be an email button. They only one to change is the first button, which
        // could be a phone login only or email login only button
//        if (ApplicationHelper.isEmailLoginOnly(getContext())) {
//            mFirstButtonText.set(getContext().getString(R.string.register_login_popup_email_button));
//            mSecondLoginButtonVisibility.set(View.GONE);
//        } else if (ApplicationHelper.isPhoneLoginOnly(getContext())) {
//            mFirstButtonText.set(getContext().getString(R.string.register_login_popup_phone_button));
//            mSecondLoginButtonVisibility.set(View.GONE);
//        } else {
//            mFirstButtonText.set(getContext().getString(R.string.register_login_popup_phone_button));
//            mSecondLoginButtonVisibility.set(View.VISIBLE);
//        }

        //Allow to continue only with email.
        mFirstButtonText.set(getContext().getString(R.string.register_login_popup_email_button));
        mSecondLoginButtonVisibility.set(View.GONE);

        // Build the Term Condition and Privacy Policy span with the highlighted clickable text


        String tcText = getContext().getString(R.string.register_login_popup_term_condition);
        String ppText = getContext().getString(R.string.register_login_popup_privacy_policy);
        String fullText = getContext().getString(R.string.register_login_popup_term_condition_and_privacy_policy_description, tcText, ppText);

        SpannableStringBuilder builder = new SpannableStringBuilder(fullText);

        int tcStartIndex = fullText.indexOf(tcText);
        int ppStartIndex = fullText.indexOf(ppText);

        builder.setSpan(new ForegroundColorSpan(AttributeConverter.convertAttrToColor(getContext(),
                        R.attr.register_login_popup_term_condition_and_privacy_policy_description_highlight)),
                tcStartIndex,
                tcStartIndex + tcText.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(new DelayClickableSpan() {
                            @Override
                            public void onDelayClick(@NonNull View widget) {
                                IntentUtil.deepLinkIntent(getContext(), getTermOrPrivacyLink(true));
                            }
                        },
                tcStartIndex,
                tcStartIndex + tcText.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        builder.setSpan(new ForegroundColorSpan(AttributeConverter.convertAttrToColor(getContext(),
                        R.attr.register_login_popup_term_condition_and_privacy_policy_description_highlight)),
                ppStartIndex,
                ppStartIndex + ppText.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        builder.setSpan(new DelayClickableSpan() {
                            @Override
                            public void onDelayClick(@NonNull View widget) {
                                IntentUtil.deepLinkIntent(getContext(), getTermOrPrivacyLink(false));
                            }
                        },
                ppStartIndex,
                ppStartIndex + ppText.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mTermConditionPrivacyPolicySpan.set(builder);
    }

    private String getTermOrPrivacyLink(boolean isForTerm) {
        if (isForTerm) {
            return getContext().getString(R.string.term_and_condition) + "?_locale=" + LocaleManager.getAppLanguage(getContext());
        } else {
            return getContext().getString(R.string.privacy_policy) + "?_locale=" + LocaleManager.getAppLanguage(getContext());
        }
    }

    public void onAuthButtonClicked(View view) {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).startActivityForResult(ApplicationHelper.getLoginIntent(getContext(),
                            false),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            Timber.i("onActivityResultSuccess: " + resultCode);
                            if (mCallback != null) {
                                mCallback.onLoginSignUpSuccess();
                            }
                        }
                    });
        }
    }

    public void onCloseButtonClick(View view) {
        if (mCallback != null) {
            mCallback.onCloseButtonClick();
        }
    }

    public interface DialogUserLoginRegisterBottomSheetViewModelCallback {
        void onLoginSignUpSuccess();

        void onCloseButtonClick();
    }

    @BindingAdapter("setTermPrivacyText")
    public static void setTermPrivacyText(TextView textView, Spannable text) {
        textView.setText(text);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
