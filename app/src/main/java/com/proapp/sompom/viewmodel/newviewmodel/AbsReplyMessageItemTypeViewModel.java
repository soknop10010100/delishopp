package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.Spannable;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.EmojiDetectorUtil;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

/**
 * Created by Chhom Veasna 11/17/20.
 */
public abstract class AbsReplyMessageItemTypeViewModel extends AbsBaseViewModel {

    private final ObservableField<String> mUserName = new ObservableField<>();
    protected final ObservableField<Spannable> mSubTitleOne = new ObservableField<>();
    private ObservableInt mUserNameTextColor = new ObservableInt();
    private ObservableInt mContentColor = new ObservableInt();
    private ObservableInt mVerticalBorderColor = new ObservableInt();
    private ObservableInt mFileBackgroundColor = new ObservableInt();
    private ObservableInt mFileTextColor = new ObservableInt();

    protected boolean mIsInInputReplyMode;
    protected Chat mReferencedChat;
    protected Context mContext;
    protected boolean mIsMyChat;
    private ReplyMessageItemTypeViewModelListener mListener;
    private Chat mMainChat;

    public AbsReplyMessageItemTypeViewModel(Context context,
                                            Chat referencedChat,
                                            boolean isInInputReplyMode,
                                            boolean isMyChat,
                                            Chat mainChat) {
        mReferencedChat = referencedChat;
        mContext = context;
        mIsInInputReplyMode = isInInputReplyMode;
        mIsMyChat = isMyChat;
        mMainChat = mainChat;
        bindComponentColors();
    }

    public void setListener(ReplyMessageItemTypeViewModelListener listener) {
        mListener = listener;
    }

    public ObservableInt getUserNameTextColor() {
        return mUserNameTextColor;
    }

    public ObservableInt getContentColor() {
        return mContentColor;
    }

    public ObservableInt getVerticalBorderColor() {
        return mVerticalBorderColor;
    }

    public ObservableInt getFileBackgroundColor() {
        return mFileBackgroundColor;
    }

    public ObservableInt getFileTextColor() {
        return mFileTextColor;
    }

    public ObservableField<String> getUserName() {
        return mUserName;
    }

    public ObservableField<Spannable> getSubTitleOne() {
        return mSubTitleOne;
    }

    protected Media getMediaFromChat() {
        if (mReferencedChat.getMedia() != null && !mReferencedChat.getMedia().isEmpty()) {
            return mReferencedChat.getMedia().get(0);
        }

        return null;
    }

    public final void onRepliedChatClick() {
        if (mListener != null) {
            mListener.onRepliedChatClicked(mReferencedChat, mIsInInputReplyMode);
        }
    }

    public final void onLongClicked() {
        if (mListener != null) {
            mListener.onLongClicked();
        }
    }

    private void bindComponentColors() {
        /*
        This layout will be used for both input preview of reply and as reply item in chat list.
        So we have to differentiate its context with the color.
         */
        if (mIsInInputReplyMode) {
            mUserNameTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_input_username));
            mContentColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_input_text));
            mFileTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_input_file_text));
            mFileBackgroundColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_input_file_background));
            mVerticalBorderColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_input_vertical_border));
        } else {
            //As reply item in chat list
            //As main chat is Emoji only type
            if (mMainChat != null && EmojiDetectorUtil.isEmojiOnly(mMainChat.getContent())) {
                mUserNameTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_emoji_type_username));
                mFileTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_emoji_type_file_text));
                mFileBackgroundColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_emoji_type_file_background));
                mVerticalBorderColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_emoji_type_vertical_border));
                if (mIsMyChat) {
                    mContentColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_emoji_type_content_me));
                } else {
                    mContentColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_content_recipient));
                }
            } else {
                //Check other types
                if (mIsMyChat) {
                    //Current user side
                    //Specific for type file chat since it background conflict with theme color
                    if (mMainChat != null && mMainChat.getType() == Chat.Type.FILE) {
                        mContentColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_file_type_content));
                        mUserNameTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_file_type_username_me));
                        mFileTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_file_type_text_me));
                        mFileBackgroundColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_file_type_background_me));
                        mVerticalBorderColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_file_type_vertical_border_me));
                    } else {
                        mContentColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_content_me));
                        mUserNameTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_username_me));
                        mFileTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_file_text_me));
                        mFileBackgroundColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_file_background_me));
                        mVerticalBorderColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_vertical_border_me));
                    }
                } else {
                    //Recipient side
                    mUserNameTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_username_recipient));
                    mContentColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_content_recipient));
                    mFileTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_file_text_recipient));
                    mFileBackgroundColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_file_background_recipient));
                    mVerticalBorderColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.chat_reply_item_vertical_border_recipient));
                }
            }
        }
    }

    protected void bindContent() {
        if (mReferencedChat.getSender() != null) {
            mUserName.set(mReferencedChat.getSender().getFullName());
        }
    }

    public interface ReplyMessageItemTypeViewModelListener {
        void onRepliedChatClicked(Chat repliedChat, boolean isInInputReplyMode);

        default void onLongClicked() {
        }
    }
}
