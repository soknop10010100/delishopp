package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.TextUtils;

import androidx.databinding.ObservableField;

import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

/**
 * Created by Or Vitovongsak on 12/1/22.
 */
public class EmptyFragmentViewModel extends AbsLoadingViewModel {

    private ObservableField<String> mTitle = new ObservableField<>();
    private ObservableField<String> mMessage = new ObservableField<>();

    public EmptyFragmentViewModel(Context context) {
        super(context);
    }

    public EmptyFragmentViewModel(Context context,
                                  String title,
                                  String message) {
        super(context);
        mTitle.set(title);
        mMessage.set(message);
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    public ObservableField<String> getMessage() {
        return mMessage;
    }

    public void showEmptyMessage() {
        String title = !TextUtils.isEmpty(mTitle.get()) ? mTitle.get() : "";
        String message = !TextUtils.isEmpty(mMessage.get()) ? mMessage.get() : "";

        showEmptyDataError(title, message);
    }
}
