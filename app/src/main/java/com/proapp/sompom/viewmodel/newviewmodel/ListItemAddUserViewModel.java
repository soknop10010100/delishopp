package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableField;

import com.proapp.sompom.viewmodel.AbsBaseViewModel;

public class ListItemAddUserViewModel extends AbsBaseViewModel {

    private ObservableField<String> mTitle = new ObservableField<>();

    public ListItemAddUserViewModel(String title) {
        mTitle.set(title);
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }
}
