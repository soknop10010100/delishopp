package com.proapp.sompom.viewmodel.preview;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.widget.LinearLayout;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.FileDownloadHelper;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.DownloaderUtils;
import com.proapp.sompom.viewmodel.newviewmodel.ItemPostTimelineFileViewModel;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Chhom Veasna on 05/11/2020.
 */
public class ItemFileLinkPreviewViewModel extends AbsItemLinkPreviewViewModel {

    private final ObservableField<List<Media>> mMediaList = new ObservableField<>();
    private Map<Media, ItemPostTimelineFileViewModel> mViewModelHashMap = new HashMap<>();
    private OnTimelineItemButtonClickListener mTimelineItemButtonClickListener;

    public ItemFileLinkPreviewViewModel(LinkPreviewModel linkPreviewModel,
                                        PreviewContext previewContext,
                                        OnTimelineItemButtonClickListener timelineItemButtonClickListener) {
        super(linkPreviewModel, previewContext);
        mTimelineItemButtonClickListener = timelineItemButtonClickListener;
        mMediaList.set(buildMediaList());
    }

    private List<Media> buildMediaList() {
        Media media = new Media();
        media.setUrl(getOriginalPreviewUrl());
        media.setType(MediaType.VIDEO);
        media.setFileName(mLinkPreviewModel.getFileName());
        media.setFormat(mLinkPreviewModel.getFileExtension());
        return Collections.singletonList(media);
    }

    public Map<Media, ItemPostTimelineFileViewModel> getViewModelHashMap() {
        return mViewModelHashMap;
    }

    public ObservableField<List<Media>> getMediaList() {
        return mMediaList;
    }

    public ItemPostTimelineFileViewModel.ItemPostTimelineFileViewModelListener getFileClickListener(Context context) {
        return (isFromMoreFile, media, fileViewModel) -> {
            if (!TextUtils.isEmpty(media.getDownloadedPath())) {
                //File was previously downloaded.
                DownloaderUtils.openDownloadedFile(context, media.getDownloadedPath());
            } else {
                WallStreetHelper.downloadFile(context,
                        media,
                        fileViewModel,
                        new FileDownloadHelper.FileDownloadListener() {
                            @Override
                            public void onDownloadFailed(String requestId, String downloadId, Throwable ex) {
                                if (mTimelineItemButtonClickListener != null) {
                                    mTimelineItemButtonClickListener.onShowSnackBar(true, ex.getMessage());
                                }
                            }
                        });
            }
        };
    }

    @BindingAdapter("previewContext")
    public static void updateContainerState(LinearLayout container, PreviewContext previewContext) {
        if (previewContext == PreviewContext.CREATE_FEED) {
            int padding = container.getContext().getResources().getDimensionPixelSize(R.dimen.medium_padding);
            container.setPadding(0, padding, 0, padding);
            container.setBackgroundColor(AttributeConverter.convertAttrToColor(container.getContext(),
                    R.attr.colorRecyclerViewHome));
        } else {
            container.setPadding(0, 0, 0, 0);
            container.setBackgroundColor(Color.TRANSPARENT);
        }
    }
}
