package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;

import com.proapp.sompom.intent.ConfirmCodeIntent;
import com.proapp.sompom.intent.InputLoginPasswordIntent;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.emun.AuthType;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.datamanager.LoginDataManager;
import com.sompom.baseactivity.ResultCallback;

import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;

/**
 * Created by Veasna Chhom on 4/21/21.
 */
public class LoginWithPhoneOrEmailViewModel extends AbsLoginViewModel<LoginDataManager, AbsLoginViewModel.AbsLoginViewModelListener> {

    public LoginWithPhoneOrEmailViewModel(Context context, AbsLoginViewModel.AbsLoginViewModelListener listener) {
        super(context, listener);
        mPhoneNumberUtil = PhoneNumberUtil.createInstance(getContext());
    }

    @Override
    protected boolean shouldLoadUserDefaultCountryCode() {
        return true;
    }

    @Override
    protected boolean shouldEnableActionButton() {
        return isPhoneNumberValid(mIdentifier.get()) || isIdentifierValidEmail(mIdentifier.get());
    }

    @Override
    protected void onContinueButtonClicked() {
        if (!TextUtils.isEmpty(mValidaPhoneCountryInput) && !TextUtils.isEmpty(mValidPhoneInput)) {
            //Valid phone input
            ExchangeAuthData data = new ExchangeAuthData();
            data.setCountyCode(mValidaPhoneCountryInput);
            data.setPhoneNumber(mValidPhoneInput);
            data.setExchangeAuthType(AuthType.LOGIN.getValue());
            requestValidatePhoneNumber(AuthType.LOGIN,
                    mValidPhoneInput,
                    mValidaPhoneCountryInput,
                    getFullPhoneNumberFromInput(),
                    false,
                    null,
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            if (resultCode == AppCompatActivity.RESULT_OK) {
                                ExchangeAuthData exchangeAuthData = data.getParcelableExtra(ConfirmCodeIntent.DATA);
                                if (exchangeAuthData != null) {
                                    AuthType type = AuthType.fromValue(exchangeAuthData.getPhoneResponseAuthType());
                                    if (type == AuthType.LOGIN) {
                                        ((AbsBaseActivity) mContext).startActivityForResult(new InputLoginPasswordIntent(getContext(), exchangeAuthData),
                                                new ResultCallback() {
                                                    @Override
                                                    public void onActivityResultSuccess(int resultCode, Intent data) {
                                                        ((AbsBaseActivity) mContext).setResult(resultCode, data);
                                                        ((AbsBaseActivity) mContext).finish();
                                                    }
                                                });
                                    }
                                }
                            }
                        }
                    });
        } else if (!TextUtils.isEmpty(mValidEmailInput)) {
            //Valid email input
            ExchangeAuthData data = new ExchangeAuthData();
            data.setEmail(mValidEmailInput);
            data.setExchangeAuthType(AuthType.LOGIN.getValue());
            ((AbsBaseActivity) mContext).startActivityForResult(new InputLoginPasswordIntent(getContext(), data),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            ((AbsBaseActivity) mContext).setResult(resultCode, data);
                            ((AbsBaseActivity) mContext).finish();
                        }
                    });
        }
    }
}
