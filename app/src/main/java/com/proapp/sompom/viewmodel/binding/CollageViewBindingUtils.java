package com.proapp.sompom.viewmodel.binding;

import android.graphics.Point;
import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.proapp.sompom.databinding.LayoutProductMediaBinding;
import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.SharedProduct;
import com.proapp.sompom.model.SharedTimeline;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.CreateWallStreetScreenType;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.emun.ViewType;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.utils.VolumePlaying;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewholder.TimelineViewHolder;
import com.proapp.sompom.widget.lifestream.CollageView;
import com.proapp.sompom.widget.lifestream.OnBindItemListener;

import timber.log.Timber;

/**
 * Created by nuonveyo on 8/9/18.
 */

public final class CollageViewBindingUtils {
    private CollageViewBindingUtils() {
    }

    @BindingAdapter(value = {"setCollageView",
            "setOnItemClickListener",
            "setScreenType",
            "setBindingViewHolder"},
            requireAll = false)
    public static void setCollageView(CollageView collageView,
                                      WallStreetAdaptive adaptive,
                                      OnItemClickListener<Integer> onItemClickListener,
                                      CreateWallStreetScreenType streetScreenType,
                                      BindingViewHolder holder) {
        Timber.i("setCollageView: " + new Gson().toJson(adaptive));
        if (adaptive == null
                || adaptive.getMedia() == null
                || adaptive.getMedia().isEmpty()) {
            return;
        }

        int itemType = getItemType(adaptive);
        ViewType v = ViewType.fromValue(itemType);
        collageView.setOrientation(v.getOrientation());
        collageView.setNumberOfItem(v.getNumberOfItem());
        collageView.setFormat(v.getFormat());
        collageView.startGenerateView();

        collageView.bindView(new OnBindItemListener() {
            private boolean mIsMediaVideoAdded;

            @Override
            public void onBind(View view, int position) {
                LayoutProductMediaBinding bind = DataBindingUtil.bind(view);
                if (bind != null && position < adaptive.getMedia().size()) {
                    bind.mediaLayout.setMedia(adaptive.getMedia().get(position));
                    bind.mediaLayout.setOnMediaLayoutClickListener(mediaLayout -> {
                        if (onItemClickListener != null) {
                            onItemClickListener.onClick(position);
                        }
                    });
                    if (adaptive.getMedia().get(position).getType() == MediaType.VIDEO && !mIsMediaVideoAdded) {
                        mIsMediaVideoAdded = true;
                        bind.mediaLayout.setMute(VolumePlaying.isMute(bind.mediaLayout.getContext()));
                        bind.mediaLayout.setVisibleMuteButton(true);
                        if (holder instanceof TimelineViewHolder) {
                            ((TimelineViewHolder) holder).setMediaLayout(bind.mediaLayout, null);
                            bind.mediaLayout.setOnFullScreenClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });
                        }
                    }
                }
            }

            @Override
            public Point onRequiredWidthHeight() {
                Media media = adaptive.getMedia().get(0);
                return new Point(media.getWidth(), media.getHeight());
            }
        });
        collageView.setBadge(adaptive.getMedia().size());
        collageView.setVisibility(View.VISIBLE);
    }

    private static int getItemType(WallStreetAdaptive adaptive) {
        int itemType;
        if (adaptive instanceof Product || adaptive instanceof SharedProduct) {
            if (adaptive instanceof SharedProduct) {
                itemType = ((SharedProduct) adaptive).getTimelineViewType().getTimelineViewType();
            } else {
                itemType = ((Product) adaptive).getTimelineViewType().getTimelineViewType();
            }
        } else {
            if (adaptive instanceof SharedTimeline) {
                itemType = ((SharedTimeline) adaptive).getTimelineViewType().getTimelineViewType();
            } else {
                itemType = ((LifeStream) adaptive).getTimelineViewType().getTimelineViewType();
            }
            //For one item must be return ViewType.Timeline_FreeStyle_1
            if (itemType == ViewType.LiveVideoTimeline.getTimelineViewType()) {
                itemType = ViewType.FreeStyle.getTimelineViewType();
            }
        }
        return itemType;
    }
}
