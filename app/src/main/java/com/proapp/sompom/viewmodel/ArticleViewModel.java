package com.proapp.sompom.viewmodel;

import android.view.View;

import com.proapp.sompom.listener.OnArticleSelectListener;
import com.proapp.sompom.model.result.Category;
import com.proapp.sompom.R;

/**
 * Created by He Rotha on 9/15/17.
 */

public class ArticleViewModel {
    private Category mCategory;
    private OnArticleSelectListener mOnArticleSelectListener;

    public ArticleViewModel(Category category, OnArticleSelectListener onArticleSelectListener) {
        mCategory = category;
        mOnArticleSelectListener = onArticleSelectListener;
    }

    public String getName() {
        return mCategory.getName();
    }

    public String getCount() {
        return String.valueOf(mCategory.getCount());
    }

    public void onItemClick(View view) {
        if (mOnArticleSelectListener != null) {
            if (mCategory.getName() != null && mCategory.getName()
                    .equalsIgnoreCase(view.getContext().getString(R.string.all))) {
                mOnArticleSelectListener.onAllArticleSelected(view, mCategory);
            } else {
                mOnArticleSelectListener.onNormalArticleSelected(view, mCategory);
            }
        }
    }


}
