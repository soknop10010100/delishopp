package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class ListItemSearchMessageResultProductViewModel extends AbsBaseViewModel {
    public Product mProduct;

    public ListItemSearchMessageResultProductViewModel(Product product) {
        mProduct = product;
    }

    public String getProductName() {
        return mProduct.getName();
    }

    public void onItemClick(Context context) {
    }
}
