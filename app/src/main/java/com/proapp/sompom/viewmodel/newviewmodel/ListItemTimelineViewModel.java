package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.core.graphics.BlendModeColorFilterCompat;
import androidx.core.graphics.BlendModeCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;
import androidx.databinding.ObservableLong;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.helper.ContentTypeHelper;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.helper.WallStreetIntentData;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.emun.PublishItem;
import com.proapp.sompom.model.result.ContentStat;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.DateTimeUtils;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.SpannableUtil;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by He Rotha on 7/10/18.
 */
public class ListItemTimelineViewModel extends ListItemTimelineHeaderViewModel {

    public final ObservableLong mLikeCount = new ObservableLong();
    public final ObservableLong mShareCount = new ObservableLong();
    public final ObservableLong mUnreadCommentBadgeCounter = new ObservableLong();
    public final ObservableBoolean mIsLike = new ObservableBoolean();
    public final ObservableBoolean mIsShowShare = new ObservableBoolean();
    public final ObservableBoolean mIsHideCommentSharedButton = new ObservableBoolean();
    private final ObservableLong mUnreadCommentCount = new ObservableLong();
    private final ObservableLong mCommentCounter = new ObservableLong();

    protected Adaptive mAdaptive;
    protected WallStreetDataManager mWallStreetDataManager;
    protected OnTimelineItemButtonClickListener mOnItemClickListener;
    protected ContentStat mContentStat;
    protected ObservableInt mBottomLineVisibility = new ObservableInt(View.INVISIBLE);

    public ListItemTimelineViewModel(AbsBaseActivity activity,
                                     Adaptive adaptive,
                                     WallStreetDataManager dataManager,
                                     int position,
                                     OnTimelineItemButtonClickListener onItemClick) {
        super(activity, adaptive, position, onItemClick);
        mOnItemClickListener = onItemClick;
        mWallStreetDataManager = dataManager;
        mAdaptive = adaptive;
        if (mAdaptive instanceof WallStreetAdaptive) {
            mContentStat = ((WallStreetAdaptive) mAdaptive).getContentStat();
        }
        setData();
    }

    public long getTotalViewerCounter() {
        if (mAdaptive instanceof WallStreetAdaptive && ((WallStreetAdaptive) mAdaptive).getContentStat() != null) {
            return ((WallStreetAdaptive) mAdaptive).getContentStat().getTotalViews();
        }

        return 0;
    }

    public void onUpdateCommentCounter(long counter) {
        mContentStat.setTotalComments(counter);
        mCommentCounter.set(counter);
    }

    public void updateUnreadCommentCounter(int counter) {
        ((WallStreetAdaptive) mAdaptive).getContentStat().setUnreadCommentCount(counter);
        mUnreadCommentCount.set(counter);
        checkToUpdateUnreadCommentCounterBadge();
    }

    public ObservableLong getCommentCounter() {
        return mCommentCounter;
    }

    public ObservableLong getUnreadCommentCount() {
        return mUnreadCommentCount;
    }

    public ObservableLong getUnreadCommentBadgeCounter() {
        return mUnreadCommentBadgeCounter;
    }

    public void setBottomLineVisibility(boolean visible) {
        mBottomLineVisibility.set(visible ? View.VISIBLE : View.INVISIBLE);
    }

    public ObservableInt getBottomLineVisibility() {
        return mBottomLineVisibility;
    }

    public OnTimelineItemButtonClickListener getOnItemClickListener() {
        return mOnItemClickListener;
    }

    public void checkToUpdateUnreadCommentCounterBadge() {
        mUnreadCommentBadgeCounter.set(mUnreadCommentCount.get());
    }

    public SpannableString getTime(Context context) {
        if (mAdaptive instanceof WallStreetAdaptive) {
            Date createDate = ((WallStreetAdaptive) mAdaptive).getCreateDate();
            String address = ((WallStreetAdaptive) mAdaptive).getUser().getCity();
            PublishItem publish = ((WallStreetAdaptive) mAdaptive).getPublish();
            String date = DateTimeUtils.parse(context, createDate.getTime(), false, null);
            return SpannableUtil.getTimelineDate(context, date, address, publish, mAdaptive.isWelcomePostType());
        } else {
            return null;
        }
    }

    public boolean isHideProperty(List<User> user, long comment, long like, long share) {
        return comment == 0 && like == 0 && share == 0 && (user == null || user.isEmpty());
    }

    public void onCommentClick() {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onCommentButtonClick(mAdaptive, mPosition);
        }
    }

    public void onShareClick() {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onShareButtonClick(mAdaptive, mPosition);
        }
    }

    public void onLikeClick(View view) {
        if (mAdaptive instanceof WallStreetAdaptive && mContentStat != null) {
            AnimationHelper.animateBounce(view, null);
            boolean currentIsLikedStatus = mIsLike.get();
            mContext.startLoginWizardActivity(new LoginActivityResultCallback() {
                @Override
                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                    ContentType contentType;
                    if (mAdaptive instanceof Media) {
                        contentType = ContentTypeHelper.getMediaContentType((Media) mAdaptive);
                    } else {
                        contentType = ContentType.POST;
                    }
                    Observable<Response<Object>> call = mWallStreetDataManager.likePost(mAdaptive.getId(),
                            mAdaptive.getId(),
                            contentType,
                            !currentIsLikedStatus);
                    ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mContext, call);
                    addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                            showSnackBarFromContext(getContext(), ex.getMessage());
                        }

                        @Override
                        public void onComplete(Response<Object> result) {
                            long likeCount = mContentStat.getTotalLikes();
                            if (currentIsLikedStatus) {
                                likeCount -= 1;
                                mIsLike.set(false);

                            } else {
                                likeCount += 1;
                                mIsLike.set(true);
                            }
                            mContentStat.setTotalLikes(likeCount);
                            checkLikeItem();
                        }
                    }));
                }
            });
        }
    }

    protected void checkLikeItem() {
        if (mAdaptive instanceof WallStreetAdaptive && mContentStat != null) {
            ((WallStreetAdaptive) mAdaptive).setLike(mIsLike.get());
            ((WallStreetAdaptive) mAdaptive).setContentStat(mContentStat);
            mLikeCount.set(mContentStat.getTotalLikes());
            mIsLike.set(((WallStreetAdaptive) mAdaptive).isLike());
        }
    }

    private void setData() {
        if (mAdaptive instanceof WallStreetAdaptive && mContentStat != null) {
            mLikeCount.set(mContentStat.getTotalLikes());
            mShareCount.set(mContentStat.getTotalShares());
            mUnreadCommentCount.set(mContentStat.getUnreadCommentCount());
            mCommentCounter.set(mContentStat.getTotalComments());
            mIsLike.set(((WallStreetAdaptive) mAdaptive).isLike());
            checkToUpdateUnreadCommentCounterBadge();
        }
    }

    public List<User> getUserViewItem() {
        if (mAdaptive instanceof WallStreetAdaptive) {
            return ((WallStreetAdaptive) mAdaptive).getUserView();
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public Context getContext() {
        return mContext;
    }

    public WallStreetDataManager getWallStreetDataManager() {
        return mWallStreetDataManager;
    }

    public void onUserViewItemClick() {
        if (getUserViewItem() != null
                && !getUserViewItem().isEmpty()
                && mOnItemClickListener != null)
            mOnItemClickListener.onUserViewMediaClick(mAdaptive);
    }

    public void onSeeMoreClick() {
        mOnItemClickListener.getRecyclerView().pause();
        mOnItemClickListener.getRecyclerView().setAutoResume(false);
        mAdaptive.startActivityForResult(new WallStreetIntentData(mContext, mOnItemClickListener));
    }

    public void onMessageClick() {
        if (!(mAdaptive instanceof Product) || ((Product) mAdaptive).getUser() == null) {
            return;
        }
        mContext.startLoginWizardActivity(new LoginActivityResultCallback() {
            @Override
            public void onActivityResultSuccess(boolean isAlreadyLogin) {
                if (!isAlreadyLogin && TextUtils.equals(SharedPrefUtils.getUserId(getContext()), mAdaptive.getId())) {
                    return;
                }
                mContext.startActivity(new ChatIntent(mContext, ((Product) mAdaptive).getUser()));
            }
        });
    }

    public void onCallClick() {
        if (!(mAdaptive instanceof Product) || ((Product) mAdaptive).getUser() == null) {
            return;
        }
        mContext.startLoginWizardActivity(new LoginActivityResultCallback() {
            @Override
            public void onActivityResultSuccess(boolean isAlreadyLogin) {
                if (!isAlreadyLogin && TextUtils.equals(SharedPrefUtils.getUserId(getContext()), mAdaptive.getId())) {
                    return;
                }
                mContext.startActivity(new CallingIntent(mContext,
                        AbsCallService.CallType.VOICE,
                        ((Product) mAdaptive).getUser(),
                        (Product) mAdaptive));
            }
        });
    }

    public void onLikeClick() {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onLikeViewerClick(mAdaptive);
        }
    }

    @BindingAdapter("isLike")
    public static void bindLikeIcon(ImageView imageView, boolean isLike) {
        if (isLike) {
            imageView.setImageResource(R.drawable.ic_red_passion);
        } else {
            Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_heart_outline);
            if (drawable != null) {
                drawable = drawable.mutate();
                int color = AttributeConverter.convertAttrToColor(imageView.getContext(),
                        R.attr.post_unlike_button);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    drawable.setColorFilter(BlendModeColorFilterCompat.createBlendModeColorFilterCompat(color,
                            BlendModeCompat.SRC_ATOP));
                } else {
                    drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
                }
                imageView.setImageDrawable(drawable);
            }
        }
    }
}
