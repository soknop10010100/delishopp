package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;

import androidx.core.content.ContextCompat;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnChatItemClickListener;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.DateTimeUtils;
import com.proapp.sompom.utils.SpecialTextRenderUtils;

/**
 * Created by He Rotha on 9/14/17.
 */

public class ConversationViewModel implements OnChatItemClickListener {

    public Conversation mConversation;
    public User mRecipient;
    private OnChatItemClickListener mOnChatItemClickListener;

    public ConversationViewModel(Context context,
                                 Conversation conversation,
                                 OnChatItemClickListener onChatItemClickListener) {
        mConversation = conversation;
        mOnChatItemClickListener = onChatItemClickListener;
        mRecipient = conversation.getOneToOneRecipient(context);
    }

    public Spannable getMessage(Context context) {
        if (!TextUtils.isEmpty(mConversation.getSenderID())) {
            return SpecialTextRenderUtils.renderMentionUser(context,
                    mConversation.getLastMessage().getActualContent(context, mConversation, true),
                    false,
                    false,
                    ContextCompat.getColor(context, R.color.darkGrey),
                    -1, -1, false, null);
        } else {
            return new SpannableString("");
        }
    }

    public String getDate(Context context) {
        if (mConversation.getLastMessage().getDate().getTime() != 0) {
            return DateTimeUtils.parse(context, mConversation.getLastMessage().getDate().getTime(), false, null);
        } else {
            return "";
        }
    }

    @Override
    public void onChatItemClicked(Conversation chatItemModel) {
        mOnChatItemClickListener.onChatItemClicked(chatItemModel);
    }
}
