package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import androidx.databinding.ObservableField;

import com.proapp.sompom.helper.NavigateSellerStoreHelper;
import com.proapp.sompom.model.result.ContentStat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.FormatNumber;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;
import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCompleteListener;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class ListItemProductHistoryConversation extends AbsBaseViewModel {
    public final ObservableField<User> mUser = new ObservableField<>();
    private final Context mContext;
    private final OnCompleteListener<Conversation> mOnChatItemClickListener;
    private final Conversation mConversation;

    public ListItemProductHistoryConversation(Context context,
                                              Conversation conversation,
                                              OnCompleteListener<Conversation> listener) {
        mContext = context;
        mOnChatItemClickListener = listener;
        mConversation = conversation;
        mUser.set(getUser());

    }

    public String getFollow() {
        StringBuilder follow = new StringBuilder();
        User user = mUser.get();
        if (user != null) {
            ContentStat contentStat = user.getContentStat();
            follow
                    .append(FormatNumber.format(contentStat.getTotalFollowers()))
                    .append(" ");
            if (contentStat.getTotalFollowers() > 1) {
                follow.append(mContext.getString(R.string.seller_store_followers_title));
            } else {
                follow.append(mContext.getString(R.string.suggestion_follower));
            }

            follow
                    .append(" ")
                    .append(" ")
                    .append(" ")
                    .append(" ")
                    .append(" ")
                    .append(FormatNumber.format(contentStat.getTotalFollowings()))
                    .append(" ")
                    .append(mContext.getString(R.string.seller_store_following_title));
        }
        return follow.toString();
    }

    public void onItemClick() {
        if (mOnChatItemClickListener != null) {
            mOnChatItemClickListener.onComplete(mConversation);
        }
    }

    public void onUserProfileClick() {
        new NavigateSellerStoreHelper(mContext, mUser.get()).openSellerStore();
    }

    private User getUser() {
        for (User user : mConversation.getParticipants()) {
            if (!SharedPrefUtils.checkIsMe(mContext, user.getId())) {
                return user;
            }
        }
        return new User();
    }
}
