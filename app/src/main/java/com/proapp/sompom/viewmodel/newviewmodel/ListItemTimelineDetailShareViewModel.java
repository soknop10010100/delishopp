package com.proapp.sompom.viewmodel.newviewmodel;

import android.view.View;

import androidx.databinding.ObservableField;

import com.proapp.sompom.listener.OnDetailShareItemCallback;
import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.SharedProduct;
import com.proapp.sompom.model.SharedTimeline;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;
import com.proapp.sompom.viewholder.BindingViewHolder;

import java.util.List;

/**
 * Created by nuonveyo on 7/12/18.
 */

public class ListItemTimelineDetailShareViewModel extends AbsTimelineDetailItemViewModel {

    public final ObservableField<ListItemTimelineHeaderViewModel> mListItemTimelineHeaderViewModel = new ObservableField<>();
    public final ObservableField<WallStreetAdaptive> mAdaptive = new ObservableField<>();
    public final ObservableField<BindingViewHolder> mBindingViewHolder = new ObservableField<>();

    private final WallStreetAdaptive mLifeStream;
    private final OnDetailShareItemCallback mOnLikeButtonClickListener;

    public ListItemTimelineDetailShareViewModel(AbsBaseActivity activity,
                                                WallStreetAdaptive post,
                                                BindingViewHolder viewHolder,
                                                WallStreetDataManager wallStreetDataManager,
                                                WallStreetAdaptive adaptive,
                                                OnTimelineItemButtonClickListener listener,
                                                OnDetailShareItemCallback onLikeButtonClickListener) {
        super(activity, post, (Adaptive) adaptive, wallStreetDataManager, 0, listener);
        mAdaptive.set(adaptive);
        mOnLikeButtonClickListener = onLikeButtonClickListener;
        mBindingViewHolder.set(viewHolder);

        if (adaptive instanceof SharedProduct) {
            mLifeStream = ((SharedProduct) adaptive).getProduct();
        } else {
            mLifeStream = ((SharedTimeline) adaptive).getLifeStream();
        }
        ListItemTimelineHeaderViewModel headerViewModel = new ListItemTimelineHeaderViewModel((AbsBaseActivity) getContext(),
                (Adaptive) mLifeStream,
                0,
                getOnItemClickListener());
        headerViewModel.mIsSetMaxLine.set(false);
        mListItemTimelineHeaderViewModel.set(headerViewModel);
    }

    @Override
    public void onLikeClick(View view) {
        super.onLikeClick(view);
        if (mOnLikeButtonClickListener != null) mOnLikeButtonClickListener.onClick();
    }

    public OnItemClickListener<Integer> onItemClickListener() {
        return integer -> mOnLikeButtonClickListener.onMediaClick(mLifeStream, integer);
    }
}
