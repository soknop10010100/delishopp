package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.intent.newintent.ConciergeOrderDeliveryTrackingIntent;
import com.proapp.sompom.model.ShopDeliveryTimeSelection;
import com.proapp.sompom.model.emun.ApplicationMode;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.utils.DateUtils;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;
import com.sompom.baseactivity.ResultCallback;

import java.time.format.DateTimeFormatter;

public abstract class AbsConciergeOrderHistoryViewModel extends AbsBaseViewModel {

    private static final String ASAP = "ASAP";

    protected final Context mContext;
    protected final ConciergeOrder mOrder;
    private final ObservableInt mOrderStatusButtonBackground = new ObservableInt();
    private final AbsConciergeOrderHistoryViewModelListener mAbsListener;
    protected final ObservableField<String> mFullOrderStatus = new ObservableField<>();
    private final ObservableField<String> mDeliverySlot = new ObservableField<>();

    public AbsConciergeOrderHistoryViewModel(Context context, ConciergeOrder order, AbsConciergeOrderHistoryViewModelListener absListener) {
        mContext = context;
        mOrder = order;
        mAbsListener = absListener;
        mOrderStatusButtonBackground.set(ConciergeHelper.getOrderStatusBackground(mContext, mOrder));
        mFullOrderStatus.set(order.getStatusString());
        bindTimeSlot();
    }

    public ObservableField<String> getDeliverySlot() {
        return mDeliverySlot;
    }

    private void bindTimeSlot() {
        if (mOrder.getType() == ApplicationMode.EXPRESS_MODE) {
            mDeliverySlot.set(ASAP);
        } else {
            if (mOrder.getTimeSlot() != null) {
                if (ConciergeHelper.isSelectedBasketTimeSlotValid(mOrder.getTimeSlot())) {
                    ShopDeliveryTimeSelection timeSelection = ConciergeHelper.buildShopDeliveryTimeSelectionFromBasketTimeSlot(mOrder.getTimeSlot());
                    String deliverySlot = timeSelection.getSelectedDate().format(DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_5)) +
                            " " +
                            ConciergeHelper.buildTimeSlotDisplayFormat4(mContext, timeSelection.getStartTime().toLocalTime(),
                                    timeSelection.getEndTime().toLocalTime());
                    mDeliverySlot.set(deliverySlot);
                }
            } else {
                mDeliverySlot.set("—");
            }
        }
    }

    public ObservableField<String> getFullOrderStatus() {
        return mFullOrderStatus;
    }

    public ObservableInt getOrderStatusButtonBackground() {
        return mOrderStatusButtonBackground;
    }

    public void onCallClicked(View view) {
        if (mAbsListener != null) {
            mAbsListener.onCallClicked();
        }
    }

    public void onMessageClicked(View view) {
        if (mAbsListener != null) {
            mAbsListener.onMessageClicked();
        }
    }

    public void onOrderStatusClicked(View view) {
        if (mContext instanceof AbsBaseActivity) {
            ConciergeOrderDeliveryTrackingIntent intent = new ConciergeOrderDeliveryTrackingIntent(mContext, mOrder);
            ((AbsBaseActivity) mContext).startActivityForResult(intent, new ResultCallback() {
                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    // If the order is canceled, inform adapter that it was canceled
                    ConciergeOrderDeliveryTrackingIntent intent = new ConciergeOrderDeliveryTrackingIntent(data);
                    if (intent.getIsOrderCanceled()) {
                        if (mAbsListener != null) {
                            mAbsListener.onOrderCanceled(intent.getOrder());
                        }
                    }
                }
            });
        }
    }

    public interface AbsConciergeOrderHistoryViewModelListener {
        void onMessageClicked();

        void onCallClicked();

        void onOrderCanceled(ConciergeOrder canceledOrder);
    }
}
