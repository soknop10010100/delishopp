package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.BR;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

public class ListItemAddGroupParticipantViewModel extends AbsBaseViewModel {

    private ObservableBoolean mIsSelected = new ObservableBoolean();

    @Bindable
    public User mUser;

    public ListItemAddGroupParticipantViewModel(User user) {
        mUser = user;
        bindData();
    }

    public ObservableBoolean getIsSelected() {
        return mIsSelected;
    }

    private void bindData() {
        if (mUser != null) {
            notifyPropertyChanged(BR.user);
            mIsSelected.set(mUser.getSelected());
        }
    }

    public final void onItemClicked() {
        mIsSelected.set(!mIsSelected.get());
        mUser.setSelected(mIsSelected.get());
    }
}
