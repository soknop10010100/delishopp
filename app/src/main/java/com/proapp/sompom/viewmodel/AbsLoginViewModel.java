package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.DelayClickableSpan;
import com.proapp.sompom.helper.DelayWatcher;
import com.proapp.sompom.helper.NotificationServiceHelper;
import com.proapp.sompom.helper.SingleRequestLocation;
import com.proapp.sompom.intent.ConfirmCodeIntent;
import com.proapp.sompom.intent.EmailSignUpIntent;
import com.proapp.sompom.intent.ForgotPasswordIntent;
import com.proapp.sompom.intent.ProfileSignUpIntent;
import com.proapp.sompom.intent.ValidatePhoneNumberIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.emun.AuthConfigurationType;
import com.proapp.sompom.model.emun.AuthType;
import com.proapp.sompom.model.emun.ChangePasswordActionType;
import com.proapp.sompom.model.emun.EnvironmentType;
import com.proapp.sompom.model.request.SaveCustomerLocationRequestBody;
import com.proapp.sompom.model.result.AuthResponse;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.SplashActivity;
import com.proapp.sompom.newui.dialog.ChangePasswordDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.ServerUrl;
import com.proapp.sompom.services.datamanager.AbsLoginDataManager;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.sompom.baseactivity.ResultCallback;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

public abstract class AbsLoginViewModel<DM extends AbsLoginDataManager,
        L extends AbsLoginViewModel.AbsLoginViewModelListener> extends AbsAuthViewModel {

    protected final ObservableBoolean mActionButtonEnable = new ObservableBoolean();
    protected ObservableInt mDebugButtonVisibility = new ObservableInt(View.GONE);
    protected ObservableInt mSignUpVisibility = new ObservableInt(View.GONE);
    protected final ObservableField<String> mPassword = new ObservableField<>();
    private final ObservableField<String> mPhoneNumber = new ObservableField<>();
    protected final ObservableField<String> mIdentifier = new ObservableField<>();

    protected L mListener;
    protected DM mDataManager;

    public AbsLoginViewModel(Context context, L listener) {
        super(context);
        mListener = listener;
        checkToEnableDebugOption();
        mSignUpVisibility.set(shouldEnableResetPasswordOption() ? View.VISIBLE : View.GONE);
    }

    private void checkToEnableDebugOption() {
//        mDebugButtonVisibility.set(BuildConfig.DEBUG ? View.VISIBLE : View.GONE);
    }

    private boolean shouldEnableResetPasswordOption() {
        AuthConfigurationType authConfigurationType = ApplicationHelper.getAuthConfigurationType(getContext());
        return authConfigurationType == AuthConfigurationType.AGENCY ||
                authConfigurationType == AuthConfigurationType.PHONE_AND_PASSWORD;
    }

    public ObservableField<String> getIdentifier() {
        return mIdentifier;
    }

    public ObservableField<String> getPhoneNumber() {
        return mPhoneNumber;
    }

    public AbsLoginViewModel(Context context, DM dataManager, L listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
        checkToEnableDebugOption();
        AuthConfigurationType signUpType = ApplicationHelper.getAuthConfigurationType(getContext());
        mSignUpVisibility.set((signUpType != AuthConfigurationType.NONE && signUpType != AuthConfigurationType.AGENCY) ? View.VISIBLE : View.GONE);
    }

    public ObservableField<String> getPassword() {
        return mPassword;
    }

    @Override
    public ObservableField<String> getEmail() {
        return super.getEmail();
    }

    public ObservableBoolean getActionButtonEnable() {
        return mActionButtonEnable;
    }

    public ObservableInt getSignUpVisibility() {
        return mSignUpVisibility;
    }

    public ObservableInt getDebugButtonVisibility() {
        return mDebugButtonVisibility;
    }

    public final ClickableSpan getSignUpClickableSpan() {
        return new DelayClickableSpan() {
            @Override
            public void onDelayClick(@NonNull View widget) {
                AuthConfigurationType authConfigurationType = ApplicationHelper.getAuthConfigurationType(getContext());
                if (authConfigurationType == AuthConfigurationType.PHONE ||
                        authConfigurationType == AuthConfigurationType.PHONE_AND_EMAIL ||
                        authConfigurationType == AuthConfigurationType.PHONE_AND_PASSWORD ||
                        authConfigurationType == AuthConfigurationType.PHONE_AND_EMAIL_AND_PASSWORD) {
                    if (mContext instanceof AbsBaseActivity) {
                        ((AbsBaseActivity) mContext).startActivityForResult(new ValidatePhoneNumberIntent(mContext,
                                        AuthType.SIGN_UP),
                                new ResultCallback() {
                                    @Override
                                    public void onActivityResultSuccess(int resultCode, Intent data) {
                                        if (resultCode == AppCompatActivity.RESULT_OK) {
                                            ExchangeAuthData exchangeAuthData = data.getParcelableExtra(ConfirmCodeIntent.DATA);
                                            Timber.i("onActivityResultSuccess: resultCode: " + resultCode + ", Data: " + new Gson().toJson(exchangeAuthData));
                                            if (exchangeAuthData != null) {
                                                AuthType type = AuthType.fromValue(exchangeAuthData.getPhoneResponseAuthType());
                                                if (type == AuthType.SIGN_UP) {
                                                    mContext.startActivity(new ProfileSignUpIntent(mContext, exchangeAuthData));
                                                }
                                            }
                                        }
                                    }
                                });
                    }
                } else {
                    mContext.startActivity(new EmailSignUpIntent(mContext));
                }
            }
        };
    }

    public final ClickableSpan getForgotPasswordViaPhoneClickableSpan() {
        return new DelayClickableSpan() {
            @Override
            public void onDelayClick(@NonNull View widget) {
                if (mContext instanceof AbsBaseActivity) {
                    ((AbsBaseActivity) mContext).startActivityForResult(new ValidatePhoneNumberIntent(mContext,
                                    AuthType.RESET_PASSWORD_VIA_PHONE),
                            new ResultCallback() {
                                @Override
                                public void onActivityResultSuccess(int resultCode, Intent data) {
                                    if (resultCode == AppCompatActivity.RESULT_OK) {
                                        ExchangeAuthData exchangeAuthData = data.getParcelableExtra(ConfirmCodeIntent.DATA);
                                        Timber.i("onActivityResultSuccess: resultCode: " + resultCode + ", Data: " + new Gson().toJson(exchangeAuthData));
                                        ChangePasswordDialog dialog = ChangePasswordDialog.newInstance(ChangePasswordActionType.RESET_PASSWORD_VIA_PHONE,
                                                exchangeAuthData.getFirebaseToken(),
                                                exchangeAuthData.getFullPhoneNumber());
                                        dialog.setListener(new ChangePasswordDialog.ChangePasswordDialogListener() {
                                            @Override
                                            public void onChangePasswordSuccess(ChangePasswordActionType type) {
                                                hideLoading();
                                                new androidx.appcompat.app.AlertDialog.Builder(mContext)
                                                        .setTitle(mContext.getString(R.string.change_password_reset_title))
                                                        .setMessage((type == ChangePasswordActionType.RESET_PASSWORD ||
                                                                type == ChangePasswordActionType.RESET_PASSWORD_VIA_PHONE) ?
                                                                R.string.change_password_popup_success_reset_password_description : R.string.change_password_success_message)
                                                        .setPositiveButton(mContext.getString(R.string.popup_ok_button), null)
                                                        .show();
                                            }

                                            @Override
                                            public void onDismissed() {
                                                hideLoading();
                                            }
                                        });
                                        dialog.show(((AbsBaseActivity) mContext).getSupportFragmentManager());
                                    }
                                }
                            });
                }
            }
        };
    }

    public final ClickableSpan getForgotPasswordClickableSpan() {
        return new DelayClickableSpan() {
            @Override
            public void onDelayClick(@NonNull View widget) {
                if (mContext instanceof AppCompatActivity) {
                    ((AppCompatActivity) mContext).startActivity(new ForgotPasswordIntent(mContext));
                }
            }
        };
    }

    public final TextWatcher onInputChangeListener() {
        return new DelayWatcher(false) {
            @Override
            public void onTextChanged(String text) {
                checkToEnableActionButton();
            }
        };
    }

    private void checkToEnableActionButton() {
        if (shouldEnableActionButton()) {
            if (!mActionButtonEnable.get())
                mActionButtonEnable.set(true);
        } else {
            if (mActionButtonEnable.get())
                mActionButtonEnable.set(false);
        }
    }

    public final void onContinueClick() {
        if (mContext instanceof Activity) {
            KeyboardUtil.hideKeyboard((Activity) mContext);
        }
        onContinueButtonClicked();
    }

    protected void onLoginSuccess(AuthResponse authResponse) {
        hideLoading();
        if (mListener != null) {
            mListener.onLoginSuccess();
        }
    }

    protected void requestRecordPlayerId(AuthResponse authResponse) {
        NotificationServiceHelper.loadPlayerId(getContext(), (playerId, type) -> {
            if (!TextUtils.isEmpty(playerId)) {
                Observable<Response<Object>> loginService = mDataManager.addPlayerId(NotificationServiceHelper.getAddPlayerIdRequestBody(playerId, type));
                ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(),
                        loginService);
                addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        hideLoading();
                        //Clear logged in user value previously saved.
                        SharedPrefUtils.clearValue(mContext);
                        if (mListener != null) {
                            mListener.onLoginError(ex.getMessage());
                        }
                    }

                    @Override
                    public void onComplete(Response<Object> result) {
//                        requestSaveCustomerLastLocation(authResponse);
                        onLoginSuccess(authResponse);
                    }
                }));
            } else {
//                requestSaveCustomerLastLocation(authResponse);
                onLoginSuccess(authResponse);
            }
        });
    }

    protected void requestSaveCustomerLastLocation(AuthResponse authResponse) {
        if (mContext instanceof AbsBaseActivity) {
            SingleRequestLocation.reset();
            ((AbsBaseActivity) mContext).requestLocation(true,
                    (location, isNeverAskAgain, isPermissionEnabled, isLocationEnabled) -> {
                        if (location != null) {
                            Observable<Response<Object>> saveLocation = mDataManager.saveCustomerLastLocation(
                                    new SaveCustomerLocationRequestBody(location.getLatitude(), location.getLongitude()));
                            ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(),
                                    saveLocation);
                            addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
                                @Override
                                public void onComplete(Response<Object> result) {
                                    onLoginSuccess(authResponse);
                                }

                                @Override
                                public void onFail(ErrorThrowable ex) {
                                    onLoginSuccess(authResponse);
                                }
                            }));
                        } else {
                            onLoginSuccess(authResponse);
                        }
                    });
        } else {
            onLoginSuccess(authResponse);
        }
    }

    protected void requestLogin(Observable<Response<AuthResponse>> loginService, boolean isLoginViaPhone) {
        showLoading(true);
        ResponseObserverHelper<Response<AuthResponse>> helper = new ResponseObserverHelper<>(mDataManager.mContext, loginService);
        addDisposable(helper.execute(new OnCallbackListener<Response<AuthResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                if (mListener != null) {
                    mListener.onLoginError(ex.getMessage());
                }
            }

            @Override
            public void onComplete(Response<AuthResponse> result) {
                requestRecordPlayerId(result.body());
            }
        }));
    }

    public final void onDebugClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String[] options = new String[]{EnvironmentType.DEV.getValue(),
                EnvironmentType.DEMO.getValue(),
                EnvironmentType.LOCAL.getValue(),
                AuthConfigurationType.EMAIL.getValue(),
                AuthConfigurationType.PHONE.getValue(),
                AuthConfigurationType.PHONE_AND_EMAIL.getValue(),
                AuthConfigurationType.PHONE_AND_PASSWORD.getValue(),
                AuthConfigurationType.PHONE_AND_EMAIL_AND_PASSWORD.getValue()};
        builder.setTitle("Choose environment.");
        builder.setSingleChoiceItems(options,
                ServerUrl.getSelectedEnvironment(getContext()).ordinal(),
                (dialog, which) -> {
                    Timber.i("On environment selected: " + which);
                    if (which == 0) {
                        ServerUrl.switchToDevEnvironment(getContext());
                    } else if (which == 1) {
                        ServerUrl.switchToDemoEnvironment(getContext());
                    } else if (which == 2) {
                        ServerUrl.switchToLocalEnvironment(getContext());
                    } else if (which == 3) {
                        ApplicationHelper.setSignUpMethodToLocal(getContext(), AuthConfigurationType.EMAIL);
                        restartActivity();
                    } else if (which == 4) {
                        ApplicationHelper.setSignUpMethodToLocal(getContext(), AuthConfigurationType.PHONE);
                        restartActivity();
                    } else if (which == 5) {
                        ApplicationHelper.setSignUpMethodToLocal(getContext(), AuthConfigurationType.PHONE_AND_EMAIL);
                        restartActivity();
                    } else if (which == 6) {
                        ApplicationHelper.setSignUpMethodToLocal(getContext(), AuthConfigurationType.PHONE_AND_PASSWORD);
                        restartActivity();
                    } else if (which == 7) {
                        ApplicationHelper.setSignUpMethodToLocal(getContext(), AuthConfigurationType.PHONE_AND_EMAIL_AND_PASSWORD);
                        restartActivity();
                    }
                });
        builder.setPositiveButton(R.string.popup_ok_button, (dialog, which) -> {
            dialog.dismiss();
            if (mListener != null) {
                mListener.rebuildApplicationComponent();
            }
        });
        builder.create().show();
    }

    private void restartActivity() {
        Intent intent = new Intent(getContext(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
    }

    protected abstract boolean shouldEnableActionButton();

    protected abstract void onContinueButtonClicked();

    public interface AbsLoginViewModelListener {

        default void onLoginError(String message) {
        }

        default void onLoginSuccess() {
        }

        default void onChangePasswordRequire(AuthResponse authResponse) {
        }

        default void rebuildApplicationComponent() {
        }
    }
}
