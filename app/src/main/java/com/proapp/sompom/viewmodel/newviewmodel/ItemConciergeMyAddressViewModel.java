package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.intent.newintent.ConciergeNewAddressIntent;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;
import com.sompom.baseactivity.ResultCallback;

/**
 * Created by Or Vitovongsak on 22/11/21.
 */
public class ItemConciergeMyAddressViewModel extends AbsBaseViewModel {

    private ObservableField<String> mLabel = new ObservableField<>();
    private ObservableField<String> mName = new ObservableField<>();
    private ObservableField<String> mPhone = new ObservableField<>();
    private ObservableField<String> mAddress = new ObservableField<>();
    private ObservableBoolean mEditAddressOptionVisibility = new ObservableBoolean();
    private ObservableBoolean mIsPrimaryAddress = new ObservableBoolean();

    private Context mContext;
    private ConciergeUserAddress mConciergeUserAddress;
    private ItemConciergeMyAddressViewModelListener mListener;

    public ItemConciergeMyAddressViewModel(Context context,
                                           ConciergeUserAddress address,
                                           ItemConciergeMyAddressViewModelListener listener) {
        mContext = context;
        mConciergeUserAddress = address;
        mEditAddressOptionVisibility.set(true);
        mListener = listener;
        initData();
    }

    public ObservableBoolean getEditAddressOptionVisibility() {
        return mEditAddressOptionVisibility;
    }

    public ObservableField<String> getLabel() {
        return mLabel;
    }

    public ObservableField<String> getName() {
        return mName;
    }

    public ObservableField<String> getPhone() {
        return mPhone;
    }

    public ObservableField<String> getAddress() {
        return mAddress;
    }

    public ObservableBoolean getIsPrimaryAddress() {
        return mIsPrimaryAddress;
    }

    private void initData() {
        mLabel.set(mConciergeUserAddress.getLabel());
        mName.set(mConciergeUserAddress.getFirstName() + " " + mConciergeUserAddress.getLastName());
        mPhone.set("+" + mConciergeUserAddress.getCountryCode() + mConciergeUserAddress.getPhone());
        mAddress.set(mConciergeUserAddress.getAreaDetail());
        mIsPrimaryAddress.set(mConciergeUserAddress.isPrimary());
    }

    public void onEditAddressClick(View view) {
        if (mContext instanceof AbsBaseActivity) {
            ConciergeNewAddressIntent intent = ConciergeNewAddressIntent.getEditAddressIntent(mContext, mConciergeUserAddress);
            ((AbsBaseActivity) mContext).startActivityForResult(intent, new ResultCallback() {
                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    ConciergeNewAddressIntent resultIntent = new ConciergeNewAddressIntent(data);
                    if (resultIntent.getIsAddressDeleted()) {
                        if (mListener != null) {
                            mListener.onAddressDeleted();
                        }
                    } else {
                        mConciergeUserAddress = resultIntent.getAddressData();
                        initData();
                        if (mListener != null) {
                            mListener.onAddressUpdated(mConciergeUserAddress);
                        }
                    }
                }
            });
        }
    }

    public interface ItemConciergeMyAddressViewModelListener {
        void onAddressDeleted();

        void onAddressUpdated(ConciergeUserAddress address);
    }
}
