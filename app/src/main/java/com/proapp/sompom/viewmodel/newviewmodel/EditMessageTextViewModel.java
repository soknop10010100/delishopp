package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.Spannable;
import android.text.Spanned;

import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.SpecialTextRenderUtils;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

/**
 * Created by Chhom Veasna 11/17/20.
 */
public class EditMessageTextViewModel extends AbsBaseViewModel {

    private ObservableField<Spanned> mChatContent = new ObservableField<>();
    private Context mContext;
    private Chat mChat;

    public EditMessageTextViewModel(Context context, Chat chat) {
        mContext = context;
        mChat = chat;
        Spannable spannable = SpecialTextRenderUtils.renderMentionUser(context, chat.getContent(),
                false,
                false,
                AttributeConverter.convertAttrToColor(context, R.attr.chat_input_mention_user_color),
                -1,
                -1,
                false,
                null);
        mChatContent.set(spannable);
    }

    public ObservableField<Spanned> getChatContent() {
        return mChatContent;
    }
}
