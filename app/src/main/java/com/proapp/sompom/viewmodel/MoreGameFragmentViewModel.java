package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.ObservableField;
import android.net.Uri;

import com.proapp.sompom.model.result.MoreGame;
import com.proapp.sompom.R;

/**
 * Created by nuonveyo on 5/4/18.
 */

public class MoreGameFragmentViewModel extends AbsBaseViewModel {
    public ObservableField<String> mUrl = new ObservableField<>();
    private MoreGame mMoreGame;
    private Context mContext;

    public MoreGameFragmentViewModel(Context context, MoreGame moreGame) {
        mContext = context;
        mMoreGame = moreGame;
        if (mMoreGame == null) {
            mMoreGame = new MoreGame();
        }

        boolean isPhone = mContext.getResources().getBoolean(R.bool.is_phone);
        mUrl.set(mMoreGame.getLinkPreviewBaseUrl() + (isPhone ?
                mMoreGame.getPreview() : mMoreGame.getPreviewIPad()));
    }

    public void onImageClicked() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(mMoreGame.getLinkAndroid()));
        mContext.startActivity(browserIntent);
    }
}
