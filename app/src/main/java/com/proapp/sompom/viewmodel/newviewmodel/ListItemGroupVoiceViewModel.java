package com.proapp.sompom.viewmodel.newviewmodel;

import android.app.Activity;

import androidx.databinding.ObservableField;

import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.ChatBg;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.utils.DateTimeUtils;

/**
 * Created by Chhom Veasna on 7/27/2020.
 */
public class ListItemGroupVoiceViewModel {

    private ObservableField<String> mName = new ObservableField<>();
    private ObservableField<String> mDate = new ObservableField<>();
    private ObservableField<ChatBg> mChatBackground = new ObservableField<>(ChatBg.ChatYouSingle);
    private Media mMedia;
    private Chat mChat;
    private Activity mActivity;

    public ListItemGroupVoiceViewModel(Activity activity, Chat chat) {
        mActivity = activity;
        /*
        Since there is a constraint of using voice layout which was designed to work with only
        chat model. So we will create a dummy chat model which contain only voice media to work
        with voice layout we have.
         */
        mChat = chat;
        if (chat != null && chat.getMediaList() != null && !chat.getMediaList().isEmpty()) {
            mMedia = chat.getMediaList().get(0);

            //Bind date
            if (mMedia.getCreateDate() != null) {
                mDate.set(DateTimeUtils.getGroupFileTimeStampDisplay(activity,
                        mMedia.getCreateDate().getTime()));
            }

            //Bind owner
            if (mMedia.getOwner() != null) {
                mName.set(mMedia.getOwner().getFullName());
            }
        }
    }

    public ObservableField<ChatBg> getChatBackground() {
        return mChatBackground;
    }

    public Activity getActivity() {
        return mActivity;
    }

    public Chat getChat() {
        return mChat;
    }

    public ObservableField<String> getName() {
        return mName;
    }

    public ObservableField<String> getDate() {
        return mDate;
    }
}
