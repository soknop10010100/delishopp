package com.proapp.sompom.viewmodel.binding;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.GlideApp;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.ChatBg;
import com.proapp.sompom.model.result.ReferencedChat;
import com.proapp.sompom.utils.DrawChatImageRoundedUtil;
import com.proapp.sompom.utils.GlideLoadUtil;
import com.proapp.sompom.widget.RoundedImageView;

/**
 * Created by nuonveyo
 * on 1/7/19.
 */

public final class RoundedImageViewBindingUtil {

    private RoundedImageViewBindingUtil() {
    }

    @BindingAdapter({"setCenterParentVertical", "replyIconView", "setChatGifImage", "chatBg", "referencedChat"})
    public static void setChatGifImage(RoundedImageView imageView,
                                       SwipeRevealLayout parentView,
                                       View replyIconView,
                                       Media gifMedia,
                                       ChatBg chatBg,
                                       ReferencedChat referencedChat) {
//        Timber.i("setChatGifImage: getWidth: " + gifMedia.getWidth() + ", height: " + gifMedia.getWidth() + ", referencedChat: " + referencedChat);
        imageView.setRepliedChat(referencedChat != null);
        imageView.setCornerType(DrawChatImageRoundedUtil.getCornerType(chatBg));
        if (referencedChat != null) {
            /*
                We will have to check the reference chat in order to set the width of media which will
                guarantee that the reference chat user name will be not truncated unless it is bigger
                than max chat item width.
             */
            int desireWidth = ChatHelper.getDesireGiftMediaWidthOfChatItemBaseOnReferenceChat(imageView.getContext(), referencedChat);
            if (desireWidth <= 0) {
                //Follow the width and height of gift and just set max size.
                int imageWidth = imageView.getContext().getResources().getDimensionPixelOffset(R.dimen.chat_media_width);
                imageView.setMaxWidth(imageWidth);
                imageView.setMaxHeight(imageWidth * gifMedia.getHeight() / gifMedia.getWidth());
            } else {
                int desireHeight = desireWidth * gifMedia.getHeight() / gifMedia.getWidth();
                imageView.getLayoutParams().width = desireWidth;
                imageView.getLayoutParams().height = desireHeight;
                backupLoadedGiftSize(gifMedia, desireWidth, desireHeight);
            }
        } else {
            //Follow the width and height of gift and just set max size.
            int imageWidth = imageView.getContext().getResources().getDimensionPixelOffset(R.dimen.chat_media_width);
            imageView.setMaxWidth(imageWidth);
            imageView.setMaxHeight(imageWidth * gifMedia.getHeight() / gifMedia.getWidth());
        }

        //Maintain the previous size of gift view for some gift has changed their size during view recycle.
        if (gifMedia.getLoadedGiftWidth() != null && gifMedia.getLoadedGiftHeight() != null) {
            imageView.getLayoutParams().width = gifMedia.getLoadedGiftWidth();
            imageView.getLayoutParams().height = gifMedia.getLoadedGiftHeight();
        }
        Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_200dp);
        if (!GlideLoadUtil.isValidContext(imageView.getContext())) {
            return;
        }

        GlideApp.with(imageView.getContext())
                .asGif()
                .load(gifMedia.getUrl())
                .placeholder(drawable)
                .addListener(new RequestListener<GifDrawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
                        backupLoadedGiftSize(gifMedia, resource.getIntrinsicWidth(), resource.getIntrinsicHeight());
                        new Handler(Looper.getMainLooper()).post(() -> parentView.post(() -> {
//                            Timber.i("parentView: " + parentView.getHeight());
                            int y = (parentView.getHeight() - replyIconView.getHeight()) / 2;
                            replyIconView.setY(y);
                        }));

                        return false;
                    }
                })
                .into(imageView);
    }

    private static void backupLoadedGiftSize(Media gifMedia, int width, int height) {
        if (gifMedia.getLoadedGiftWidth() == null && gifMedia.getLoadedGiftHeight() == null) {
            gifMedia.setLoadedGiftWidth(width);
            gifMedia.setLoadedGiftHeight(height);
        }
    }
}
