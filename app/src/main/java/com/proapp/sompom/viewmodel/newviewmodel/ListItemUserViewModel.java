package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.NavigateSellerStoreHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.NetworkStateUtil;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

public class ListItemUserViewModel extends AbsBaseViewModel {

    private ObservableInt mVoiceCallVisibility = new ObservableInt(View.GONE);
    private ObservableInt mVideoCallVisibility = new ObservableInt(View.GONE);
    private ObservableInt mActionButtonVisibility = new ObservableInt(View.GONE);
    private ObservableInt mPositionVisibility = new ObservableInt(View.VISIBLE);
    private ObservableInt mDummyViewVisibility = new ObservableInt(View.INVISIBLE);
    private ObservableBoolean mLockSwipeRevealLayout = new ObservableBoolean();

    @Bindable
    public User mUser;
    private final Context mContext;
    private boolean mIsShowPosition;
    private ListItemUserViewModelListener mListener;

    public ListItemUserViewModel(Context context,
                                 User user,
                                 boolean showPosition,
                                 boolean showDummyView,
                                 boolean enableRevealLayout,
                                 ListItemUserViewModelListener listener) {
        mContext = context;
        mUser = user;
        mListener = listener;
        mIsShowPosition = showPosition;
        mDummyViewVisibility.set(showDummyView ? View.INVISIBLE : View.GONE);
        mLockSwipeRevealLayout.set(!enableRevealLayout);
        bindData();
    }

    public ObservableBoolean getLockSwipeRevealLayout() {
        return mLockSwipeRevealLayout;
    }

    public ObservableInt getDummyViewVisibility() {
        return mDummyViewVisibility;
    }

    public ObservableInt getVoiceCallVisibility() {
        return mVoiceCallVisibility;
    }

    public ObservableInt getVideoCallVisibility() {
        return mVideoCallVisibility;
    }

    public void setVideoCallVisibility(int videoCallVisibility) {
        mVideoCallVisibility.set(videoCallVisibility);
        mVideoCallVisibility.notifyChange();
    }

    public void setVoiceCallVisibility(int voiceCallVisibility) {
        this.mVoiceCallVisibility.set(voiceCallVisibility);
        mVoiceCallVisibility.notifyChange();
    }

    public ObservableInt getActionButtonVisibility() {
        return mActionButtonVisibility;
    }

    public ObservableInt getPositionVisibility() {
        return mPositionVisibility;
    }

    public void setPositionVisibility(ObservableInt mPositionVisibility) {
        this.mPositionVisibility = mPositionVisibility;
    }

    private void bindData() {

        if (mUser != null) {
            notifyPropertyChanged(BR.user);
        }
        mActionButtonVisibility.set((mUser.getIsAuthorizedUser() != null && mUser.getIsAuthorizedUser() ?
                View.VISIBLE : View.GONE));
        mPositionVisibility.set(mIsShowPosition ? View.VISIBLE : View.GONE);
    }

    public final View.OnClickListener onVideoCallClick() {
        return view -> {
            if (!NetworkStateUtil.isNetworkAvailable(mContext)) {
                if (mListener != null) {
                    mListener.onAttemptedCallWhenNoNetworkConnectedOrDeviceIsInAnotherCallProcess(false);
                }
                return;
            }

            if (CallHelper.isDeviceInPhoneCallProcess(mContext)) {
                if (mListener != null) {
                    mListener.onAttemptedCallWhenNoNetworkConnectedOrDeviceIsInAnotherCallProcess(true);
                }
                return;
            }

            UserHelper.checkUserCallFeatureSetting(mContext, new UserHelper.UserHelperListener() {
                @Override
                protected void onCheckUserCallFeatureFinished(boolean isCallEnable, boolean isVideoCallEnable) {
                    if (isVideoCallEnable) {
                        mContext.startActivity(new CallingIntent(mContext,
                                AbsCallService.CallType.VIDEO,
                                mUser,
                                null));
                    } else {
                        if (mListener != null) {
                            mListener.onMakeCallError(mContext.getString(R.string.call_toast_no_call_feature));
                        }
                    }
                }
            });
        };
    }

    public final void onDeleteClicked() {
        if (mListener != null) {
            mListener.onDeleteClicked();
        }
    }

    public final View.OnClickListener onCallClick() {
        return view -> {
            if (mUser == null) {
                return;
            }

            if (!NetworkStateUtil.isNetworkAvailable(mContext)) {
                if (mListener != null) {
                    mListener.onAttemptedCallWhenNoNetworkConnectedOrDeviceIsInAnotherCallProcess(false);
                }
                return;
            }

            if (CallHelper.isDeviceInPhoneCallProcess(mContext)) {
                if (mListener != null) {
                    mListener.onAttemptedCallWhenNoNetworkConnectedOrDeviceIsInAnotherCallProcess(true);
                }
                return;
            }

            UserHelper.checkUserCallFeatureSetting(mContext, new UserHelper.UserHelperListener() {
                @Override
                protected void onCheckUserCallFeatureFinished(boolean isCallEnable, boolean isVideoCallEnable) {
                    if (isCallEnable) {
                        mContext.startActivity(new CallingIntent(mContext,
                                AbsCallService.CallType.VOICE,
                                mUser,
                                null));
                    } else {
                        if (mListener != null) {
                            mListener.onMakeCallError(mContext.getString(R.string.call_toast_no_call_feature));
                        }
                    }
                }
            });
        };
    }

    public final void onMessageClick() {
        if (mUser == null) {
            return;
        }

        mContext.startActivity(new ChatIntent(mContext, mUser));
    }

    public final void onProfileClick() {
        if (mUser == null) {
            return;
        }
        new NavigateSellerStoreHelper(mContext, mUser).openSellerStore();
    }

    public interface ListItemUserViewModelListener {

        void onDeleteClicked();

        void onAttemptedCallWhenNoNetworkConnectedOrDeviceIsInAnotherCallProcess(boolean isInAnotherCall);

        void onMakeCallError(String error);
    }

    @BindingAdapter("lockRevealLayout")
    public static void setLockSwipeRevealLayout(SwipeRevealLayout layout, boolean lockRevealLayout) {
        layout.setLockDrag(lockRevealLayout);
    }
}
