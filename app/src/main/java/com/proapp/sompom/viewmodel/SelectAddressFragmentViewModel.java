package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.content.Context;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.google.android.gms.maps.GoogleMap;
import com.proapp.sompom.intent.SelectAddressIntentResult;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.SelectAddressActivity;

import timber.log.Timber;

/**
 * Created by nuonveyo on 5/10/18.
 */

public class SelectAddressFragmentViewModel extends AbsMapViewModel {

    public final ObservableField<String> mAddress = new ObservableField<>();
    public final ObservableBoolean mIsEnableButton = new ObservableBoolean();

    private final OnCallback mOnCallback;

    public SelectAddressFragmentViewModel(Context context,
                                          GoogleMap googleMap,
                                          SearchAddressResult addressResult,
                                          OnCallback onCallback) {
        super(context, googleMap);
        Timber.i("addressResult: " + addressResult);
        mSearchAddressResult = addressResult;
        mOnCallback = onCallback;
        enableCurrentLocation(true);
        enableMapClick(true);
        enablePoiClick(true);
        checkToSelectPreviousLocation();
    }

    public void onPickUpAddressClicked() {
        if (mContext instanceof AbsBaseActivity) {
            SelectAddressIntentResult intent = new SelectAddressIntentResult(mSearchAddressResult);
            ((AbsBaseActivity) mContext).setResult(Activity.RESULT_OK, intent);
            ((AbsBaseActivity) mContext).finish();
        }
    }

    public void onBackPress() {
        if (mContext instanceof SelectAddressActivity) {
            ((SelectAddressActivity) mContext).onBackPressed();
        }
    }

    @Override
    public void onAddressChanged(String newAddress) {
        mAddress.set(newAddress);
        mIsEnableButton.set(true);
        if (mOnCallback != null) {
            mOnCallback.slideUpButton();
        }
    }

    public interface OnCallback {
        void slideUpButton();
    }
}
