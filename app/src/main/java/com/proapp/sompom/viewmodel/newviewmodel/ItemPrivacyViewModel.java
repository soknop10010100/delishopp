package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.core.content.ContextCompat;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.model.ItemPrivacy;

/**
 * Created by Chhom Veasna on 12/11/2019.
 */
public class ItemPrivacyViewModel {

    private ObservableField<Drawable> mIcon = new ObservableField<>();
    private ObservableField<String> mTitle = new ObservableField<>();
    private ObservableField<String> mDescription = new ObservableField<>();
    private ObservableBoolean mIsSelected = new ObservableBoolean();

    public ItemPrivacyViewModel(Context context, ItemPrivacy privacy) {
        mIcon.set(ContextCompat.getDrawable(context, privacy.getPrivacy().getIcon()));
        mTitle.set(context.getString(privacy.getPrivacy().getTitleResource()));
        mDescription.set(context.getString(privacy.getPrivacy().getDescriptionResource()));
        mIsSelected.set(privacy.isSelected());
    }

    public ObservableField<Drawable> getIcon() {
        return mIcon;
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    public ObservableField<String> getDescription() {
        return mDescription;
    }

    public ObservableBoolean getIsSelected() {
        return mIsSelected;
    }
}
