package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import com.proapp.sompom.intent.ConfirmCodeIntent;
import com.proapp.sompom.intent.InputLoginPasswordIntent;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.emun.AuthType;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.datamanager.LoginDataManager;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.sompom.baseactivity.ResultCallback;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 4/21/21.
 */
public class LoginWithPhoneOnlyViewModel extends AbsLoginViewModel<LoginDataManager, LoginWithPhoneOnlyViewModel.LoginWithPhoneOnlyViewModelListener> {

    public LoginWithPhoneOnlyViewModel(Context context, LoginWithPhoneOnlyViewModelListener listener) {
        super(context, listener);
    }

    @Override
    protected boolean shouldEnableActionButton() {
        return isValidPhoneNumber();
    }

    @Override
    protected void onContinueButtonClicked() {
        requestValidatePhoneNumber(AuthType.LOGIN,
                String.valueOf(mListener.getCountryCodePicker().getPhoneNumber().getNationalNumber()),
                mListener.getCountryCodePicker().getSelectedCountryCode(),
                mListener.getCountryCodePicker().getFullNumberWithPlus(),
                false,
                null,
                new ResultCallback() {
                    @Override
                    public void onActivityResultSuccess(int resultCode, Intent data) {
                        Timber.i("onActivityResultSuccess: " + resultCode);
                        if (resultCode == AppCompatActivity.RESULT_OK) {
                            ExchangeAuthData exchangeAuthData = data.getParcelableExtra(ConfirmCodeIntent.DATA);
                            if (exchangeAuthData != null) {
                                AuthType type = AuthType.fromValue(exchangeAuthData.getPhoneResponseAuthType());
                                if (type == AuthType.LOGIN) {
                                    ((AbsBaseActivity) mContext).startActivityForResult(new InputLoginPasswordIntent(getContext(), exchangeAuthData),
                                            new ResultCallback() {
                                                @Override
                                                public void onActivityResultSuccess(int resultCode, Intent data) {
                                                    ((AbsBaseActivity) mContext).setResult(resultCode, data);
                                                    ((AbsBaseActivity) mContext).finish();
                                                }
                                            });
                                }
                            }
                        }
                    }
                });
    }

    private boolean isValidPhoneNumber() {
        return mListener.getCountryCodePicker().isValid();
    }

    public interface LoginWithPhoneOnlyViewModelListener extends AbsLoginViewModel.AbsLoginViewModelListener {
        CountryCodePicker getCountryCodePicker();
    }
}
