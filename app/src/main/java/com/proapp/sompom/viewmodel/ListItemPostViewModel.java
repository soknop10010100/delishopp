package com.proapp.sompom.viewmodel;

import androidx.databinding.ObservableBoolean;

import com.google.gson.Gson;
import com.proapp.sompom.helper.IntentData;
import com.proapp.sompom.listener.OnCreateTimelineListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.CreateTimelineItem;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.newui.AbsBaseActivity;

import timber.log.Timber;

/**
 * Created by He Rotha on 11/16/18.
 */
public class ListItemPostViewModel implements OnCreateTimelineListener {

    private AbsBaseActivity mActivity;
    private CreateTimelineItem mCreateTimelineItem;
    private ListItemPostViewModelListener mListener;
    private final ObservableBoolean mBottomLineVisibility = new ObservableBoolean();

    public ListItemPostViewModel(AbsBaseActivity activity,
                                 CreateTimelineItem createTimelineItem,
                                 ListItemPostViewModelListener listener) {
        mActivity = activity;
        mCreateTimelineItem = createTimelineItem;
        mListener = listener;
        mBottomLineVisibility.set(mCreateTimelineItem.isEnableBottomPostSeparator());
    }

    public ObservableBoolean getBottomLineVisibility() {
        return mBottomLineVisibility;
    }

    public void setBottomLineVisibility(boolean isVisible) {
        mBottomLineVisibility.set(isVisible);
    }

    @Override
    public void onPostWallStreetClick() {
        mCreateTimelineItem.startActivityForResult(new IntentData() {
            @Override
            public AbsBaseActivity getActivity() {
                return mActivity;
            }

            @Override
            public Action getAction() {
                return Action.CREATE_TIMELINE_ITEM;
            }

            @Override
            public void onDataResultCallBack(Adaptive adaptive, boolean isRemove) {
                Timber.i("onDataResultCallBack: " + new Gson().toJson(adaptive));
                if (mListener != null) {
                    mListener.onPrepareToPostWallItem((LifeStream) adaptive);
                }
            }
        });
    }

    @Override
    public void onCameraClick() {
        mCreateTimelineItem.startActivityForResult(new IntentData() {
            @Override
            public AbsBaseActivity getActivity() {
                return mActivity;
            }

            @Override
            public Action getAction() {
                return Action.CREATE_TIMELINE_CAMERA;
            }

            @Override
            public void onDataResultCallBack(Adaptive adaptive, boolean isRemove) {
                Timber.i("onDataResultCallBack: " + new Gson().toJson(adaptive));
                if (mListener != null) {
                    mListener.onPrepareToPostWallItem((LifeStream) adaptive);
                }
            }
        });
    }

    public interface ListItemPostViewModelListener {

        void onPrepareToPostWallItem(LifeStream lifeStream);
    }
}
