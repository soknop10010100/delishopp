package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;

import androidx.core.content.ContextCompat;
import androidx.databinding.ObservableField;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.proapp.sompom.R;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.utils.SpecialTextRenderUtils;

/**
 * Created by Chhom Veasna 11/17/20.
 */
public class ReplyMessageTypeTextViewModel extends AbsReplyMessageItemTypeViewModel {

    private final ObservableField<String> mFileExtension = new ObservableField<>();

    public ReplyMessageTypeTextViewModel(Context context,
                                         Chat chat,
                                         boolean isInInputReplyMode,
                                         boolean isMyChat,
                                         Chat mainChat) {
        super(context, chat, isInInputReplyMode, isMyChat, mainChat);
        bindContent();
    }

    public ObservableField<String> getFileExtension() {
        return mFileExtension;
    }

    @Override
    protected void bindContent() {
        super.bindContent();
        if (mReferencedChat.getType() == Chat.Type.AUDIO) {
            mSubTitleOne.set(new SpannableString(mContext.getString(R.string.chat_reply_voice_message)));
        } else if (mReferencedChat.getType() == Chat.Type.FILE) {
            if (mReferencedChat.getMedia() != null && !mReferencedChat.getMedia().isEmpty()) {
                mFileExtension.set(TextUtils.isEmpty(mReferencedChat.getMedia().get(0).getFormat()) ?
                        "" : mReferencedChat.getMedia().get(0).getFormat());
                mSubTitleOne.set(new SpannableString(mReferencedChat.getMedia().get(0).getFileName()));
            }
        } else {
            mSubTitleOne.set(SpecialTextRenderUtils.renderMentionUser(mContext,
                    mReferencedChat.getContent(),
                    false,
                    false,
                    getMentionColor(),
                    -1, -1, false,
                    null));
        }
    }

    private int getMentionColor() {
        if (ThemeManager.getAppTheme(mContext) == AppTheme.White) {
            if (mIsInInputReplyMode) {
                return ContextCompat.getColor(mContext, R.color.black);
            }

            if (mIsMyChat) {
                return ContextCompat.getColor(mContext, R.color.white);
            } else {
                return ContextCompat.getColor(mContext, R.color.black);
            }
        } else {
            return ContextCompat.getColor(mContext, R.color.white);
        }
    }
}
