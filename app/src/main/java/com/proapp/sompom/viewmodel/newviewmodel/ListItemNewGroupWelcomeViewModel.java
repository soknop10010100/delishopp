package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.DateTimeUtils;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Veasna Chhom 7/7/21.
 */

public class ListItemNewGroupWelcomeViewModel {

    private ObservableField<String> mTitle = new ObservableField<>();
    private ObservableField<String> mGroupName = new ObservableField<>();
    private ObservableField<String> mGroupImageUrl = new ObservableField<>();
    private ObservableField<List<User>> mParticipants = new ObservableField<>();
    private ObservableField<String> mCreatedDate = new ObservableField<>();
    private Context mContext;
    private Conversation mConversation;

    public ListItemNewGroupWelcomeViewModel(Context context, Conversation conversation) {
        mContext = context;
        mConversation = conversation;
        bindInfo();
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    public ObservableField<String> getGroupName() {
        return mGroupName;
    }

    public ObservableField<String> getCreatedDate() {
        return mCreatedDate;
    }

    public ObservableField<String> getGroupImageUrl() {
        return mGroupImageUrl;
    }

    public ObservableField<List<User>> getParticipants() {
        return mParticipants;
    }

    private void bindInfo() {
        if (mConversation != null) {
            User user = SharedPrefUtils.getUser(mContext);
            mTitle.set(mContext.getString(R.string.chat_message_welcome_group, user.getFirstName()));
            mGroupName.set(mConversation.getGroupName());
            if (mConversation.getDate() != null) {
                mCreatedDate.set(DateTimeUtils.parse(mContext,
                        mConversation.getDate().getTime(),
                        false,
                        null));
            }
            //Bind group avatar profile
            mGroupImageUrl.set(mConversation.getGroupImageUrl());
            mGroupName.set(UserHelper.getValidDisplayName(mConversation.getGroupName()));
            if (mConversation.getParticipants() != null) {
                mParticipants.set(new ArrayList<>(mConversation.getParticipants()));
            }
        }
    }
}
