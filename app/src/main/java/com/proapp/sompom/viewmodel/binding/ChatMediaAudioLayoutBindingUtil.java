package com.proapp.sompom.viewmodel.binding;

import android.app.Activity;
import android.text.TextUtils;

import androidx.databinding.BindingAdapter;

import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.model.emun.ChatBg;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.widget.chat.ChatMediaAudioLayout;

/**
 * Created by nuonveyo on 9/20/18.
 */

public final class ChatMediaAudioLayoutBindingUtil {

    private ChatMediaAudioLayoutBindingUtil() {
    }

    @BindingAdapter(value = {"setChatData",
            "chatBackground",
            "setActivity",
            "isRecipientSide",
            "setOnLongClickListener"},
            requireAll = false)
    public static void setChatData(ChatMediaAudioLayout layout,
                                   Chat chat,
                                   ChatBg chatBg,
                                   Activity activity,
                                   boolean isRecipientSide,
                                   OnClickListener listener) {
        layout.setActivity(activity);
        boolean isMe = TextUtils.equals(chat.getSenderId(), SharedPrefUtils.getUserId(activity));
        if (isRecipientSide) {
            //Will use the recipient layout directly.
            isMe = false;
        }
        layout.setChat(chat, chatBg, isMe, listener);
    }
}
