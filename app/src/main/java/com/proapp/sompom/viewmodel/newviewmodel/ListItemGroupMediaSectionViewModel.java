package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableField;

import com.proapp.sompom.model.GroupMediaAdaptive;
import com.proapp.sompom.model.GroupMediaSection;
import com.proapp.sompom.utils.DateTimeUtils;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 7/27/2020.
 */
public class ListItemGroupMediaSectionViewModel<T extends GroupMediaAdaptive> {

    private ObservableField<String> mDate = new ObservableField<>();

    public ListItemGroupMediaSectionViewModel(GroupMediaSection<T> section) {
        if (section.getCreatedAt() != null) {
            mDate.set(DateTimeUtils.getGroupSectionTimeFormat(section.getCreatedAt()));
        }
    }

    public ObservableField<String> getDate() {
        return mDate;
    }
}
