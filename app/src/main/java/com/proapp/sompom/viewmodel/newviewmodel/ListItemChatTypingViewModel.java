package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.TextUtils;

import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 7/16/18.
 */
public class ListItemChatTypingViewModel extends AbsSupportBottomPaddingChatItemViewModel {

    private static final String USER_SEPARATOR_TOKEN = ", ";

    private final ObservableField<String> mMultipleIndicatorText = new ObservableField<>();
    protected Conversation mConversation;
    private List<User> mTypingSenderList;
    private final ObservableField<User> mSenderAvatar = new ObservableField<>();
    private Context mContext;

    public ListItemChatTypingViewModel(Context context, int position, List<User> typingSenderList) {
        super(position);
        mContext = context;
        mTypingSenderList = new ArrayList<>(typingSenderList);
        bindUserDisplay();
    }

    public ObservableField<String> getMultipleIndicatorText() {
        return mMultipleIndicatorText;
    }

    public ObservableField<User> getSenderAvatar() {
        return mSenderAvatar;
    }

    public void addNewTypingSender(User user) {
        if (getTypingSenderIndex(user) < 0) {
            //New typing sender must be at the first index of the list.
            mTypingSenderList.add(0, user);
            bindUserDisplay();
        }
    }

    public void removeTypingSender(User user) {
        int typingSenderIndex = getTypingSenderIndex(user);
        if (typingSenderIndex >= 0) {
            mTypingSenderList.remove(typingSenderIndex);
            bindUserDisplay();
        }
    }

    public boolean shouldRemoveTypingIndicator() {
        return mTypingSenderList.isEmpty();
    }

    private int getTypingSenderIndex(User user) {
        for (int i = 0; i < mTypingSenderList.size(); i++) {
            if (TextUtils.equals(mTypingSenderList.get(i).getId(), user.getId())) {
                return i;
            }
        }

        return -1;
    }

    private void bindUserDisplay() {
        if (!mTypingSenderList.isEmpty()) {
            if (mTypingSenderList.size() > 1) {
                if (mTypingSenderList.size() == 2) {
                    mMultipleIndicatorText.set(getDisplayName(mTypingSenderList.get(0)) +
                            USER_SEPARATOR_TOKEN + getDisplayName(mTypingSenderList.get(1)));
                } else if (mTypingSenderList.size() == 3) {
                    mMultipleIndicatorText.set(getDisplayName(mTypingSenderList.get(0)) +
                            USER_SEPARATOR_TOKEN + getDisplayName(mTypingSenderList.get(1)) +
                            USER_SEPARATOR_TOKEN + getDisplayName(mTypingSenderList.get(2)));
                } else {
                    mMultipleIndicatorText.set(getDisplayName(mTypingSenderList.get(0)) +
                            USER_SEPARATOR_TOKEN + getDisplayName(mTypingSenderList.get(1)) +
                            USER_SEPARATOR_TOKEN + getDisplayName(mTypingSenderList.get(2)) +
                            USER_SEPARATOR_TOKEN + mContext.getString(R.string.chat_message_typing_more_user));
                }
            } else {
                mMultipleIndicatorText.set("");
            }
            mSenderAvatar.set(mTypingSenderList.get(0));
        }
    }

    private String getDisplayName(User user) {
        return user.getFirstName();
    }
}
