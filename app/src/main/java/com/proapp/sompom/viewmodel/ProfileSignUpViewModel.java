package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableField;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.emun.AuthConfigurationType;
import com.proapp.sompom.model.request.SignUpUserBody;
import com.proapp.sompom.model.result.SignUpResponse;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ProfileSignUpDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class ProfileSignUpViewModel extends AbsSignUpViewModel {

    private final ObservableField<String> mFirstName = new ObservableField<>();
    private final ObservableField<String> mLastName = new ObservableField<>();
    private final ProfileSignUpDataManager mDataManager;

    private ProfileSignUpViewModelCallback mCallback;

    public ProfileSignUpViewModel(Context context, ProfileSignUpDataManager dataManager) {
        super(context);
        mDataManager = dataManager;
        validateRegisterProfileType();
        checkToBindUserName();
    }

    private void checkToBindUserName() {
        if (!TextUtils.isEmpty(mDataManager.getExchangeAuthData().getFirstName())) {
            mFirstName.set(mDataManager.getExchangeAuthData().getFirstName());
        }
        if (!TextUtils.isEmpty(mDataManager.getExchangeAuthData().getLastName())) {
            mLastName.set(mDataManager.getExchangeAuthData().getLastName());
        }
    }

    private void validateRegisterProfileType() {
        AuthConfigurationType authConfigurationType = mDataManager.getSignUpType();
        if (authConfigurationType == AuthConfigurationType.EMAIL) {
            mEmailVisibility.set(View.GONE);
            mPasswordVisibility.set(View.GONE);
            mBackButtonVisibility.set(View.INVISIBLE);
            mEnableImeActionDone.set(true);
        } else if (authConfigurationType == AuthConfigurationType.PHONE) {
            mEmailVisibility.set(View.GONE);
            mPasswordVisibility.set(View.VISIBLE);
            mBackButtonVisibility.set(View.VISIBLE);
        } else if (authConfigurationType == AuthConfigurationType.PHONE_AND_PASSWORD) {
            mEmailVisibility.set(View.GONE);
            mPasswordVisibility.set(View.VISIBLE);
            mBackButtonVisibility.set(View.VISIBLE);
        } else {
            mEmailVisibility.set(View.VISIBLE);
            mPasswordVisibility.set(View.VISIBLE);
            mBackButtonVisibility.set(View.VISIBLE);
        }
    }

    public ObservableField<String> getFirstName() {
        return mFirstName;
    }

    public ObservableField<String> getLastName() {
        return mLastName;
    }

    public void setCallback(ProfileSignUpViewModelCallback callback) {
        mCallback = callback;
    }

    @Override
    protected boolean shouldEnableActionButton() {
        if (mDataManager.getSignUpType() == AuthConfigurationType.EMAIL) {
            return isUserNameValid();
        } else if (mDataManager.getSignUpType() == AuthConfigurationType.PHONE ||
                mDataManager.getSignUpType() == AuthConfigurationType.PHONE_AND_PASSWORD) {
            return isUserNameValid() && arePasswordValid();
        } else {
            return isEmailValid() && isUserNameValid() && arePasswordValid();
        }
    }

    @Override
    public void onActionButtonClicked() {
        if (mDataManager.getSignUpType() == AuthConfigurationType.EMAIL) {
            requestUpdateUserName();
        } else if (mDataManager.getSignUpType() == AuthConfigurationType.PHONE) {
            if (isPasswordMatch() && isPasswordCorrect()) {
                requestRegister();
            }
        } else if (mDataManager.getSignUpType() == AuthConfigurationType.PHONE_AND_EMAIL ||
                mDataManager.getSignUpType() == AuthConfigurationType.PHONE_AND_PASSWORD ||
                mDataManager.getSignUpType() == AuthConfigurationType.PHONE_AND_EMAIL_AND_PASSWORD) {
            if (isPasswordMatch() && isPasswordCorrect()) {
                requestRegister();
            }
        }
    }

    public SignUpUserBody getSignUpBody() {
        if (mDataManager.getSignUpType() == AuthConfigurationType.PHONE) {
            return new SignUpUserBody(mFirstName.get(),
                    mLastName.get(),
                    mDataManager.getExchangeAuthData().getCountyCode(),
                    mDataManager.getExchangeAuthData().getPhoneNumber(),
                    mPassword.get());
        } else if (mDataManager.getSignUpType() == AuthConfigurationType.PHONE_AND_EMAIL ||
                mDataManager.getSignUpType() == AuthConfigurationType.PHONE_AND_PASSWORD ||
                mDataManager.getSignUpType() == AuthConfigurationType.PHONE_AND_EMAIL_AND_PASSWORD) {
            return new SignUpUserBody(mFirstName.get(),
                    mLastName.get(),
                    mEmail.get(),
                    mDataManager.getExchangeAuthData().getCountyCode(),
                    mDataManager.getExchangeAuthData().getPhoneNumber(),
                    mPassword.get());
        }

        return null;
    }

    private void requestRegister() {
        showLoading(true);
        Observable<Response<SignUpResponse>> loginService = mDataManager.getRegisterViaPhoneService(getSignUpBody());
        ResponseObserverHelper<Response<SignUpResponse>> helper = new ResponseObserverHelper<>(mDataManager.getContext(),
                loginService);
        addDisposable(helper.execute(new OnCallbackListener<Response<SignUpResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e("onFail: " + new Gson().toJson(ex));
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<SignUpResponse> result) {
                Timber.i("onComplete");
                if (result.body().isSuccessful()) {
                    requestSynUserData(result.body().getAccessToken(), result.body().getUser());
                } else {
                    hideLoading();
                    showSnackBar(result.body().getLocalizationMessage(getContext()), true);
                }
            }
        }));
    }

    private void requestUpdateUserName() {
        //Must set access token since we will use the private API to update user.
        SharedPrefUtils.setAccessToken(mDataManager.getExchangeAuthData().getAccessToken(), getContext());
        showLoading(true);
        Observable<Response<User>> loginService = mDataManager.getUpdateUserProfileService(mFirstName.get(), mLastName.get());
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(mDataManager.mContext,
                loginService,
                false,
                false);
        addDisposable(helper.execute(new OnCallbackListener<Response<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e("onFail: " + new Gson().toJson(ex));
                hideLoading();
                showSnackBar(ex.getMessage(), true);
                SharedPrefUtils.setAccessToken(null, getContext());
            }

            @Override
            public void onComplete(Response<User> result) {
                Timber.i("onComplete");
                requestSynUserData(mDataManager.getExchangeAuthData().getAccessToken(), result.body());
            }
        }));
    }

    private void requestSynUserData(String accessToken, User user) {
        onUpdateOrRegisterSuccess(accessToken, user);
        Observable<Response<JsonObject>> loginService = mDataManager.getSynUserService();
        ResponseObserverHelper<Response<JsonObject>> helper = new ResponseObserverHelper<>(mDataManager.mContext,
                loginService,
                false,
                false);
        addDisposable(helper.execute(new OnCallbackListener<Response<JsonObject>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e("onFail: " + new Gson().toJson(ex));
                hideLoading();
                if (mCallback != null) {
                    mCallback.onNavigateToHomeScreen();
                }
            }

            @Override
            public void onComplete(Response<JsonObject> result) {
                Timber.i("onComplete");
                hideLoading();
                if (mCallback != null) {
                    mCallback.onNavigateToHomeScreen();
                }
            }
        }));
    }

    private void onUpdateOrRegisterSuccess(String accessToken, User user) {
        SharedPrefUtils.setAccessToken(accessToken, getContext());
        SharedPrefUtils.setUserValue(user, getContext());
        SentryHelper.setUser(user);
    }

    private boolean isUserNameValid() {
        return mFirstName.get() != null &&
                mLastName.get() != null &&
                !TextUtils.isEmpty(mFirstName.get().trim()) && !TextUtils.isEmpty(mLastName.get().trim());
    }

    public interface ProfileSignUpViewModelCallback {
        void onNavigateToHomeScreen();
    }
}
