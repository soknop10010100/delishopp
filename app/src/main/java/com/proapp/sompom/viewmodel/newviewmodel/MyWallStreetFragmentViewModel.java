package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.broadcast.NetworkBroadcastReceiver;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.listener.OnCompleteListListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.request.ReportRequest;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 8/1/18.
 */

public class MyWallStreetFragmentViewModel extends AbsLoadingViewModel {
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private final WallStreetDataManager mWallStreetDataManager;
    private final OnCompleteListListener<List<Adaptive>> mListOnCallbackListener;
    private boolean mIsLoadedData;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    private String mUserId;

    public MyWallStreetFragmentViewModel(String userId,
                                         WallStreetDataManager dataManager,
                                         OnCompleteListListener<List<Adaptive>> listOnCallbackListener) {
        super(dataManager.getContext());
        mUserId = userId;
        mWallStreetDataManager = dataManager;
        mListOnCallbackListener = listOnCallbackListener;

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkStateListener = networkState -> {
                if (!mIsLoadedData) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        getTimeline();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkStateListener);
        }
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        getTimeline();
    }

    private void getTimeline() {
        if (!mIsRefresh.get()) {
            showLoading();
        }
        mWallStreetDataManager.resetPagination();
        Observable<LoadMoreWrapper<Adaptive>> callback = mWallStreetDataManager.getMyWallStreet(mUserId);
        BaseObserverHelper<LoadMoreWrapper<Adaptive>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<LoadMoreWrapper<Adaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (mIsLoadedData) {
                    mIsRefresh.set(false);
                    return;
                }
                mIsLoadedData = false;
                mIsRefresh.set(false);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    showNetworkError();
                } else {
                    showError(ex.toString());
                }
            }

            @Override
            public void onComplete(LoadMoreWrapper<Adaptive> result) {
                mIsLoadedData = true;
                mIsRefresh.set(false);
                mIsError.set(false);
                mListOnCallbackListener.onComplete(result.getData(),
                        mWallStreetDataManager.isCanLoadMore());

                hideLoading();
            }
        }));
    }

    public void loadMoreData(OnCallbackListListener<LoadMoreWrapper<Adaptive>> listener) {
        Observable<LoadMoreWrapper<Adaptive>> callback = mWallStreetDataManager.loadMoreMyWallStreet(mUserId);
        BaseObserverHelper<LoadMoreWrapper<Adaptive>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<LoadMoreWrapper<Adaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                listener.onFail(ex);
            }

            @Override
            public void onComplete(LoadMoreWrapper<Adaptive> result) {
                listener.onComplete(result, mWallStreetDataManager.isCanLoadMore());
            }
        }));
    }

    public void checkToPostFollow(String id, boolean isFollow) {
        Observable<Response<Object>> callback;
        if (isFollow) {
            callback = mWallStreetDataManager.postFollow(id);
        } else {
            callback = mWallStreetDataManager.unFollow(id);
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute());
    }

    public void deletePost(WallStreetAdaptive lifeStream) {
        Observable<Response<Object>> call = mWallStreetDataManager.deletePost(lifeStream.getId());
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute());
    }

    public void reportPost(String id, ReportRequest reportRequest) {
        Observable<Response<Object>> request = mWallStreetDataManager.reportPost(id, reportRequest);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), request);
        addDisposable(helper.execute());
    }

    public void unFollowShop(String id) {
        Observable<Response<Object>> observable = mWallStreetDataManager.unFollowShop(id);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute());
    }

    public OnClickListener onRetryButtonClick() {
        return this::getTimeline;
    }
}
