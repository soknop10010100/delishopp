package com.proapp.sompom.viewmodel;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import com.google.gson.Gson;
import com.proapp.sompom.intent.ProfileSignUpIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.result.SignUpResponse2;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.EmailSignUpDataManager;
import com.proapp.sompom.utils.KeyboardUtil;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class EmailSignUpViewModel extends AbsSignUpViewModel {

    private final EmailSignUpDataManager mLoginDataManager;

    public EmailSignUpViewModel(EmailSignUpDataManager loginDataManager) {
        super(loginDataManager.getContext());
        mLoginDataManager = loginDataManager;
        mBackButtonVisibility.set(View.VISIBLE);
    }

    @Override
    protected boolean shouldEnableActionButton() {
        return isEmailValid() &&
                !TextUtils.isEmpty(mPassword.get()) &&
                !TextUtils.isEmpty(mConfirmPassword.get());
    }

    private void requestRegister() {
        showLoading(true);
        Observable<Response<SignUpResponse2>> loginService = mLoginDataManager.getRegisterViaEmailService(mEmail.get(), mPassword.get());
        ResponseObserverHelper<Response<SignUpResponse2>> helper = new ResponseObserverHelper<>(mLoginDataManager.mContext,
                loginService);
        addDisposable(helper.execute(new OnCallbackListener<Response<SignUpResponse2>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e("onFail: " + new Gson().toJson(ex));
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<SignUpResponse2> result) {
                Timber.i("onComplete");
                hideLoading();
                if (!result.body().isError()) {
                    ProfileSignUpIntent intent = new ProfileSignUpIntent(getContext(), getSignUpIntentData(result.body()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    getContext().startActivity(intent);
                } else {
                    showSnackBar(result.body().getErrorMessage(), true);
                }
            }
        }));
    }

    private ExchangeAuthData getSignUpIntentData(SignUpResponse2 response) {
        ExchangeAuthData exchangeAuthData = new ExchangeAuthData();
        exchangeAuthData.setAccessToken(response.getAccessToken());
        exchangeAuthData.setUserId(response.getUser().getId());
        exchangeAuthData.setEmail(response.getUser().getEmail());
        return exchangeAuthData;
    }

    @Override
    public void onActionButtonClicked() {
        if (isPasswordMatch()) {
            if (isPasswordCorrect()) {
                if (getContext() instanceof Activity) {
                    KeyboardUtil.hideKeyboard((Activity) getContext());
                }
                requestRegister();
            }
        }
    }
}
