package com.proapp.sompom.viewmodel.binding;

import androidx.databinding.BindingAdapter;

import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.ChatBg;
import com.proapp.sompom.widget.chat.MultiImageChatView;

import java.util.List;

/**
 * Created by nuonveyo on 8/30/18.
 */

public final class MultiImageChatViewBindingUtil {
    private MultiImageChatViewBindingUtil() {
    }

    @BindingAdapter(value = {"bindImageView",
            "onImageClickListener",
            "setLongClickListener",
            "backgroundType",
            "isRepliedChat",
            "setGifVideoPlayerAddListener"})
    public static void bindImageView(MultiImageChatView view,
                                     List<Media> list,
                                     OnItemClickListener<Integer> onItemClickListener,
                                     MultiImageChatView.OnLongClickListener onLongClickListener,
                                     ChatBg chatBg,
                                     boolean isRepliedChat,
                                     MultiImageChatView.GifVideoPlayerAddListener listener) {
        view.generateLayout(isRepliedChat,
                list,
                onItemClickListener,
                onLongClickListener,
                chatBg,
                listener);
    }
}
