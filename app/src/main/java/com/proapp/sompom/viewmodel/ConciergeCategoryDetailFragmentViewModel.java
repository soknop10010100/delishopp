package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeSubCategory;
import com.proapp.sompom.model.concierge.ConciergeSupportItemAndSubCategoryAdaptive;
import com.proapp.sompom.model.emun.ConciergeSortItemOption;
import com.proapp.sompom.newui.dialog.ConciergeFilterProductCriteriaBottomSheetDialog;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.datamanager.ConciergeShopCategoryDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeCartBottomSheetViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 6/8/22.
 */
public class ConciergeCategoryDetailFragmentViewModel extends AbsSupportViewItemByViewModel<ConciergeShopCategoryDataManager,
        ConciergeCategoryDetailFragmentViewModel.ConciergeCategoryDetailFragmentViewModelListener> {

    private final ObservableBoolean mIsExpressMode = new ObservableBoolean();
    private List<String> mFilterBrandId = new ArrayList<>();
    private String mSortBy;
    private boolean mIsDisplayOutOfStockItem;

    public ConciergeCategoryDetailFragmentViewModel(Context context,
                                                    ConciergeShopCategoryDataManager dataManager,
                                                    ConciergeCartBottomSheetViewModel cartViewModel,
                                                    ConciergeCategoryDetailFragmentViewModelListener listener) {
        super(context, cartViewModel, listener, dataManager);
        mIsExpressMode.set(ApplicationHelper.isInExpressMode(context));
        mDataManager = dataManager;
        if (getInternalAppliedSearchCriteriaLoadingViewModel() != null) {
            getInternalAppliedSearchCriteriaLoadingViewModel().setListener(() -> {
                //Item list retry
                requestItem(false, true, false);
            });
        }
    }

    public ConciergeCartBottomSheetViewModel getCartViewModel() {
        return mCartViewModel;
    }

    public void onBackPress() {
        if (getContext() instanceof AppCompatActivity) {
            ((AppCompatActivity) getContext()).onBackPressed();
        }
    }

    @Override
    public void onRetryClick() {
        //Main retry
        requestItem(true, false, false);
    }

    public void onApplyFilter(boolean isShowOutOfStockItem, List<String> filterBrandId) {
        updateFilterSelectedCriteria(isShowOutOfStockItem, filterBrandId);
        requestItem(false, true, false);
    }

    public void updateFilterSelectedCriteria(boolean isShowOutOfStockItem, List<String> filterBrandId) {
        mIsDisplayOutOfStockItem = isShowOutOfStockItem;
        mFilterBrandId = filterBrandId;
    }

    @Override
    public void onApplySort(ConciergeSortItemOption sortItemOption) {
        super.onApplySort(sortItemOption);
        setSortBy(sortItemOption.getValue());
        requestItem(false, true, false);
    }

    public void setSortBy(String sortBy) {
        mSortBy = sortBy;
    }

    public void onGetMainData() {
        requestItem(true, false, false);
    }

    public void loadMoreItem() {
        requestItem(false, false, true);
    }

    public void loadMoreSubCategoryItem(ConciergeSubCategory existingItem, OnCallbackListener<Response<ConciergeSubCategory>> listener) {
        Observable<Response<ConciergeSubCategory>> observable = mDataManager.loadMoreSubCategoryItem(existingItem,
                mFilterBrandId);
        BaseObserverHelper<Response<ConciergeSubCategory>> helper = new BaseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(listener));
    }

    private void requestItem(boolean isFirstLoading, boolean isAppliedFilterResult, boolean isLoadMore) {
        if (isFirstLoading) {
            showLoading();
        } else if (isAppliedFilterResult) {
            showAppliedSearchCriteriaLoadingView();
        }
        Observable<List<ConciergeSupportItemAndSubCategoryAdaptive>> observable = mDataManager.getShopItemByCategoryReferenceId(isLoadMore,
                mFilterBrandId);
        BaseObserverHelper<List<ConciergeSupportItemAndSubCategoryAdaptive>> helper = new BaseObserverHelper<>(getContext(), observable);
        OnCallbackListener<List<ConciergeSupportItemAndSubCategoryAdaptive>> onCallbackListener = new OnCallbackListener<List<ConciergeSupportItemAndSubCategoryAdaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.i("onFail: " + ex.getMessage());
                hideLoading();
                if (isFirstLoading) {
                    showError(ex.getMessage(), true);
                } else {
                    if (isAppliedFilterResult) {
                        getInternalAppliedSearchCriteriaLoadingViewModel().showError(ex.getMessage(), true);
                    } else {
                        if (!isLoadMore) {
                            getInternalAppliedSearchCriteriaLoadingViewModel().showError(ex.getMessage(), true);
                        } else {
                            showSnackBar(ex.getMessage(), true);
                            if (mListener != null) {
                                mListener.onLoadMoreItemFailed();
                            }
                        }
                    }
                }
            }

            @Override
            public void onComplete(List<ConciergeSupportItemAndSubCategoryAdaptive> result) {
                hideLoading();
                if (!mIsFirstDataLoadSuccess) {
                    mIsFirstDataLoadSuccess = true;
                    if (mListener != null && !TextUtils.isEmpty(mDataManager.getCategoryTitle())) {
                        mListener.onMainDataLoadSuccess(mDataManager.getCategoryTitle());
                    }
                }

                if (!isLoadMore && result.isEmpty()) {
                    getInternalAppliedSearchCriteriaLoadingViewModel().showError(mContext.getString(R.string.shop_category_list_no_product_out_of_stock),
                            false);
                }

                if (mListener != null) {
                    mListener.onLoadItemSuccess(result, isLoadMore, mDataManager.isCanLoadMore(), isAppliedFilterResult);
                }
            }
        };
        addDisposable(helper.execute(onCallbackListener));
    }

    @Override
    public void onFilterByOptionClicked() {
        ConciergeFilterProductCriteriaBottomSheetDialog dialog = ConciergeFilterProductCriteriaBottomSheetDialog.newInstance(mDataManager.getCategoryId(),
                new ArrayList<>(mFilterBrandId),
                null);
        dialog.setListener(new ConciergeFilterProductCriteriaBottomSheetDialog.ConciergeFilterProductCriteriaBottomSheetDialogListener() {
            @Override
            public void onClearFilterCriteria() {
                mFilterCriteriaSelected.set(false);
                if (mListener != null) {
                    mListener.onClearFilterCriteria();
                }
            }

            @Override
            public void onApplyFilterCriteria(boolean isShowOutOfStockProduct, List<String> ids) {
                mFilterCriteriaSelected.set(true);
                if (mListener != null) {
                    mListener.onApplyFilterCriteria(isShowOutOfStockProduct, ids);
                }
            }
        });
        dialog.show(((AppCompatActivity) mContext).getSupportFragmentManager(),
                ConciergeFilterProductCriteriaBottomSheetDialog.class.getSimpleName());
    }

    public interface ConciergeCategoryDetailFragmentViewModelListener extends AbsSupportViewItemByViewModelListener {

        void onLoadItemSuccess(List<ConciergeSupportItemAndSubCategoryAdaptive> itemList,
                               boolean isLoadMore,
                               boolean isCanLoadMore,
                               boolean isAppliedFilterResult);

        void onLoadMoreItemFailed();

        void onMainDataLoadSuccess(String categoryTitle);

        void onClearFilterCriteria();

        void onApplyFilterCriteria(boolean isShowOutOfStockProduct, List<String> ids);
    }
}
