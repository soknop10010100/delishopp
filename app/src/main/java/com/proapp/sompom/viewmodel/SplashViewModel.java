package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.graphics.Color;

import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.SingleRequestLocation;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.request.SaveCustomerLocationRequestBody;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.SplashScreenDataManager;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.SharedPrefUtils;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

public class SplashViewModel extends AbsBaseViewModel {

    private final SplashScreenDataManager mDataManager;
    private final SplashViewModelListener mListener;
    private final ObservableInt mSplashLogoColor = new ObservableInt(Color.TRANSPARENT);
    private final Context mContext;

    public SplashViewModel(Context context, SplashScreenDataManager dataManager, SplashViewModelListener listener) {
        mDataManager = dataManager;
        mListener = listener;
        mContext = context;
        checkToDisplaySplashLogoColor();
    }

    private void checkToDisplaySplashLogoColor() {
        //We need to check this configuration for apply the splash color theme to splash logo
        boolean shouldApplySplashLogoColor = mContext.getResources().getBoolean(R.bool.apply_splash_logo_color);
        if (shouldApplySplashLogoColor) {
            mSplashLogoColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.screen_splash_logo));
        }
    }

    public ObservableInt getSplashLogoColor() {
        return mSplashLogoColor;
    }

    public void getMyUserProfile() {
        Observable<Response<User>> call = mDataManager.getMyUserProfile();
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(mContext, call);
        addDisposable(helper.execute(new OnCallbackListener<Response<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                /*
                    If the screen is receiving command to show session dialog, we will not ignore this error.
                 */
                if (!(mContext instanceof AbsBaseActivity && ((AbsBaseActivity) mContext).isReceivedCommandToShowSessionDialogError())) {
                    if (mListener != null) {
                        mListener.onSynchronizeUserProfileFinished();
                    }
                }
            }

            @Override
            public void onComplete(Response<User> result) {
                SharedPrefUtils.setUserValue(result.body(), mContext);
                if (mListener != null) {
                    mListener.onSynchronizeUserProfileFinished();
                }
            }
        }));
    }

    /**
     * New business rule is to send customer latest location to server first before anything else,
     * so server can determine what feature to enable/disable based on location of customer
     */
    public void saveCustomerLocation() {
        if (mContext instanceof AbsBaseActivity) {
            SingleRequestLocation.reset();
            ((AbsBaseActivity) mContext).requestLocation(true, (location, isNeverAskAgain, isPermissionEnabled, isLocationEnabled) -> {
                if (location != null) {
                    Observable<Response<Object>> call = mDataManager.saveCustomerCurrentLocation(
                            new SaveCustomerLocationRequestBody(location.getLatitude(), location.getLongitude()));
                    ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mContext, call);
                    addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
                        @Override
                        public void onComplete(Response<Object> result) {
                            getMyUserProfile();
                        }

                        @Override
                        public void onFail(ErrorThrowable ex) {
                            getMyUserProfile();
                        }
                    }));
                } else {
                    getMyUserProfile();
                }
            });
        } else {
            getMyUserProfile();
        }
    }

    public void requestNecessaryDataForNoneLoginUser(RequestAppFeatureListener listener) {
        Observable<Response<Object>> call = mDataManager.requestNecessaryDataForNoneLoginUser();
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mContext, call);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.i("onFail: " + ex.getMessage());
                if (listener != null) {
                    listener.onRequestFinished();
                }
            }

            @Override
            public void onComplete(Response<Object> result) {
                if (listener != null) {
                    listener.onRequestFinished();
                }
            }
        }));
    }

    public interface RequestAppFeatureListener {
        void onRequestFinished();
    }

    public interface SplashViewModelListener {
        void onSynchronizeUserProfileFinished();
    }
}
