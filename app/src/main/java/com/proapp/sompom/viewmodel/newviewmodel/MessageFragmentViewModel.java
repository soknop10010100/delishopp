package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import com.proapp.sompom.listener.OnSegmentedClickListener;
import com.proapp.sompom.model.emun.SegmentedControlItem;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

/**
 * Created by nuonveyo on 6/25/18.
 */

public class MessageFragmentViewModel extends AbsLoadingViewModel {
    public OnSegmentedClickListener mOnSegmentedClickListener;

    public MessageFragmentViewModel(Context context, OnSegmentedClickListener onSegmentedClickListener) {
        super(context);
        mOnSegmentedClickListener = onSegmentedClickListener;
    }

    public SegmentedControlItem[] getItem() {
        boolean includeTelegram = !SharedPrefUtils.isSupportStaff(getContext());
        return SegmentedControlItem.getVisibleItems(includeTelegram);
    }

}
