package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.intent.ConciergeProductDetailIntent;
import com.proapp.sompom.intent.ConciergeReviewCheckoutIntent;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.emun.ConciergeViewItemByType;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.viewmodel.AbsConciergeProductViewModel;
import com.sompom.baseactivity.ResultCallback;

/**
 * Created by Or Vitovongsak on 30/8/21.
 */
public class ListItemConciergeProductViewModel extends AbsConciergeProductViewModel {

    private final ObservableBoolean mIsRequiredRootMarginAndPadding = new ObservableBoolean(true);

    private final ObservableBoolean mCountButtonVisibility = new ObservableBoolean(false);
    private final ObservableInt mInitialAmount = new ObservableInt();
    private final ObservableField<String> mProductCountDisplay = new ObservableField<>();
    private final ObservableField<ConciergeViewItemByType> mConciergeViewItemByType = new ObservableField<>();
    private final ObservableInt mItemPosition = new ObservableInt();

    private final ListItemConciergeProductViewModelListener mListener;
    private final int mProductCount;

    // Not used yet
    private final ObservableInt mAddedAmount = new ObservableInt();

    /**
     * Use this constructor when creating view model for shop detail screen
     *
     * @param product Product data to display
     */
    public ListItemConciergeProductViewModel(Context context,
                                             ConciergeMenuItem product,
                                             int itemAmount,
                                             boolean isRequiredRootMarginAndPadding,
                                             ListItemConciergeProductViewModelListener listener) {
        super(context, product);
        mInitialAmount.set(itemAmount);
        mProductCountDisplay.set(ConciergeHelper.getValidateProductCountForDisplay(itemAmount));
        mProductCount = itemAmount;
        mListener = listener;
        mIsRequiredRootMarginAndPadding.set(isRequiredRootMarginAndPadding);
        initData(product);
    }

    public ListItemConciergeProductViewModel(Context context,
                                             ConciergeMenuItem product,
                                             int itemAmount,
                                             boolean isRequiredRootMarginAndPadding,
                                             ConciergeViewItemByType viewItemByType,
                                             int itemPosition,
                                             ListItemConciergeProductViewModelListener listener) {
        super(context, product);
        mInitialAmount.set(itemAmount);
        mConciergeViewItemByType.set(viewItemByType);
        mItemPosition.set(itemPosition);
        mProductCountDisplay.set(ConciergeHelper.getValidateProductCountForDisplay(itemAmount));
        mProductCount = itemAmount;
        mListener = listener;
        mIsRequiredRootMarginAndPadding.set(isRequiredRootMarginAndPadding);
        initData(product);
    }

    public ObservableInt getItemPosition() {
        return mItemPosition;
    }

    public ObservableField<ConciergeViewItemByType> getConciergeViewItemByType() {
        return mConciergeViewItemByType;
    }

    @Override
    protected void initData(ConciergeMenuItem product) {
        super.initData(product);
        setupAddButtonListener();
    }

    public ObservableBoolean getIsRequiredRootMarginAndPadding() {
        return mIsRequiredRootMarginAndPadding;
    }

    public ObservableInt getAddedAmount() {
        return mAddedAmount;
    }

    public void setAddedAmount(int addedAmount) {
        mAddedAmount.set(addedAmount);
    }

    public ObservableInt getInitialAmount() {
        return mInitialAmount;
    }

    public void setInitialAmount(int amount) {
        mInitialAmount.set(amount);
    }

    public ObservableBoolean getCountButtonVisibility() {
        return mCountButtonVisibility;
    }

    public ObservableField<String> getProductCountDisplay() {
        return mProductCountDisplay;
    }

    public void setupAddButtonListener() {
        mCountButtonVisibility.set(mInitialAmount.get() > 0);
        mInitialAmount.addOnPropertyChangedCallback(new OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(androidx.databinding.Observable sender, int propertyId) {
                mCountButtonVisibility.set(mInitialAmount.get() > 0);
                mProductCountDisplay.set(ConciergeHelper.getValidateProductCountForDisplay((mInitialAmount.get())));
            }
        });
    }

    public void onAddButtonClicked(View view) {
        if (mInitialAmount.get() > 0) {
            mContext.startActivity(new ConciergeProductDetailIntent(mContext, mMenuItem, -1));
        } else {
            if (mListener != null) {
                mListener.onAddFirstItemToCart(mMenuItem);
            }
        }
    }

    public void openCheckoutScreen() {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).startActivityForResult(new ConciergeReviewCheckoutIntent(mContext),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            // Do nothing, since item update are handled using broadcast receiver
                        }
                    });
        }
    }

    public void openProductDetail() {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).startActivityForResult(
                    new ConciergeProductDetailIntent(mContext, mMenuItem, -1),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            ConciergeMenuItem menuItem = data.getParcelableExtra(ConciergeProductDetailIntent.MENU_ITEM);
                            if (menuItem != null) {
                                // If the item was added to cart successfully, get the item amount
                                // from cart directly
                                setInitialAmount(ConciergeCartHelper.getExistingProductCountInCart(menuItem));
                                if (mListener != null) {
                                    mListener.onProductAddedFromDetail();
                                }
                            }
                        }

                        @Override
                        public void onActivityResultFail() {
                            // This callback is called when the the activity finished without result ok,
                            // like when user press the back button
                            setInitialAmount(mProductCount);
                            if (mListener != null) {
                                mListener.onProductFailToAdd();
                            }
                            super.onActivityResultFail();
                        }
                    });
        }
    }

    public interface ListItemConciergeProductViewModelListener {

        void onProductAddedFromDetail();

        void onProductFailToAdd();

        void onItemCountChanged(int newAmount, boolean isRemove);

        void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem);
    }

    @BindingAdapter({"isRequireMarginPadding"})
    public static void setRootViewMarginAndPaddingMode(ConstraintLayout constraintLayout, boolean isRequireMarginPadding) {
        if (constraintLayout.getLayoutParams() instanceof LinearLayout.LayoutParams) {
            if (isRequireMarginPadding) {
                ((LinearLayout.LayoutParams) constraintLayout.getLayoutParams()).setMarginStart(constraintLayout.getResources().getDimensionPixelSize(R.dimen.space_large));
                ((LinearLayout.LayoutParams) constraintLayout.getLayoutParams()).setMarginEnd(constraintLayout.getResources().getDimensionPixelSize(R.dimen.space_large));
                constraintLayout.setPadding(constraintLayout.getResources().getDimensionPixelSize(R.dimen.medium_padding), 0, 0, 0);
            } else {
                ((LinearLayout.LayoutParams) constraintLayout.getLayoutParams()).setMarginEnd(0);
                ((LinearLayout.LayoutParams) constraintLayout.getLayoutParams()).setMarginStart(0);
                constraintLayout.setPadding(0, 0, 0, 0);
            }
        }
    }
}
