package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnEditDialogClickListener;
import com.proapp.sompom.model.emun.Status;
import com.proapp.sompom.model.result.Product;

/**
 * Created by He Rotha on 6/13/18.
 */
public class ReplyMessageHeaderViewModel {
    public ObservableField<Product> mProduct = new ObservableField<>();
    public ObservableField<String> mProductName = new ObservableField<>();
    public ObservableField<String> mEditButtonText = new ObservableField<>();
    private OnEditDialogClickListener mListener;

    public ReplyMessageHeaderViewModel(Context context,
                                       Product product,
                                       int productStatus,
                                       OnEditDialogClickListener listener) {
        mListener = listener;
        mProduct.set(product);
        mProductName.set(mProduct.get().getName());
        if (productStatus == Status.ON_SALE.getStatusProduct()) {
            mEditButtonText.set(context.getString(R.string.seller_store_button_edit));
        } else {
            mEditButtonText.set(context.getString(R.string.seller_store_button_clone));
        }
    }

    public void onEditClick() {
        mListener.onEditClick(mProduct.get());
    }

    public void onDeleteClick() {
        mListener.onDeleteClick(mProduct.get());
    }

}
