package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.widget.ImageProfileLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 7/10/2019.
 */
public class ItemChatSeenAvatarViewModel extends AbsSupportBottomPaddingChatItemViewModel {

    private static final int VISIBLE_AVATAR = 5;

    private ObservableField<List<User>> mMultipleSeenAvatars = new ObservableField<>(); //Multiple seen avatars
    private ObservableField<User> mSingleSeenAvatar = new ObservableField<>();
    private ObservableInt mSingleSeenAvatarVisibility = new ObservableInt(View.GONE);
    private ObservableInt mMultipleSeenAvatarVisibility = new ObservableInt(View.GONE);

    public ItemChatSeenAvatarViewModel(int position) {
        super(position);
    }

    public ObservableField<User> getSingleSeenAvatar() {
        return mSingleSeenAvatar;
    }

    public ObservableField<List<User>> getMultipleSeenAvatars() {
        return mMultipleSeenAvatars;
    }

    public void bindSeenStatus(List<User> seenUsers, Chat chat) {
        if (seenUsers != null) {
            List<User> users = validateSeenUser(seenUsers);
            Timber.i("Valid seen avatar list: " + users.size() + " chat: " + chat.getId() + ", content: " + chat.getContent() + ", seen: " + new Gson().toJson(users));
            if (!users.isEmpty()) {
                if (users.size() == 1) {
                    mSingleSeenAvatarVisibility.set(View.VISIBLE);
                    mMultipleSeenAvatarVisibility.set(View.GONE);
                    mSingleSeenAvatar.set(users.get(0));
                } else {
                    mSingleSeenAvatarVisibility.set(View.GONE);
                    mMultipleSeenAvatarVisibility.set(View.VISIBLE);
                    mMultipleSeenAvatars.set(users);
                }
            } else {
                mSingleSeenAvatarVisibility.set(View.GONE);
                mMultipleSeenAvatarVisibility.set(View.GONE);
            }
        }
    }

    private List<User> validateSeenUser(List<User> users) {
        List<User> users1 = new ArrayList<>();
        for (User user : users) {
            if (user.isSeenMessage()) {
                users1.add(user);
            }
        }

        return users1;
    }

    public ObservableInt getSingleSeenAvatarVisibility() {
        return mSingleSeenAvatarVisibility;
    }

    public ObservableInt getMultipleSeenAvatarVisibility() {
        return mMultipleSeenAvatarVisibility;
    }

    @BindingAdapter("multipleSeenAvatars")
    public static void setSeenAvatar(LinearLayout linearLayout, List<User> users) {
        if (users == null || users.isEmpty()) {
            if (linearLayout.getChildCount() > 0) {
                linearLayout.removeAllViews();
            }
            return;
        }

        List<User> validSeenList = new ArrayList<>(users);
        int existSeenCounter = 0;

        //Check to remove previous more text view if exist
        if (linearLayout.getChildCount() > 0) {
            for (int childCount = linearLayout.getChildCount() - 1; childCount >= 0; childCount--) {
                if (linearLayout.getChildAt(childCount) instanceof TextView) {
                    linearLayout.removeViewAt(childCount);
                    break;
                }
            }
        }

        if (linearLayout.getChildCount() > 0) {
            for (int size = validSeenList.size() - 1; size >= 0; size--) {
                for (int i = 0; i < linearLayout.getChildCount(); i++) {
                    View childAt = linearLayout.getChildAt(i);
                    if (childAt.getTag() != null && childAt.getTag() instanceof String) {
                        if (TextUtils.equals((CharSequence) childAt.getTag(), validSeenList.get(size).getId())) {
                            validSeenList.remove(size);
                            existSeenCounter++;
                            break;
                        }
                    }
                }
            }
        }

        if (!validSeenList.isEmpty()) {
            int seenAvatarSize = getSeenAvatarSize(linearLayout.getContext());
            int seenCounter = existSeenCounter;
            for (int i = 0; i < validSeenList.size(); i++) {
                if (seenCounter < VISIBLE_AVATAR) {
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(seenAvatarSize, seenAvatarSize);
                    if (i > 0) {
                        params.leftMargin = linearLayout.getResources().getDimensionPixelSize(R.dimen.space_tiny);
                    }

                    ImageProfileLayout imageView = new ImageProfileLayout(linearLayout.getContext());
                    imageView.setShowStatus(false);
                    imageView.setUser(validSeenList.get(i), seenAvatarSize);
                    imageView.setTag(validSeenList.get(i).getId());
                    linearLayout.addView(imageView, params);
                }

                seenCounter++;
            }

            int remain = seenCounter - VISIBLE_AVATAR;
            if (remain > 0) {
                TextView seenMoreView = (TextView) LayoutInflater.from(linearLayout.getContext()).inflate(R.layout.seen_more_view,
                        linearLayout,
                        false);
                seenMoreView.setText(String.format(Locale.getDefault(), "+%d", remain));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                params.leftMargin = linearLayout.getResources().getDimensionPixelSize(R.dimen.space_tiny);
                linearLayout.addView(seenMoreView, params);
            }
        }
    }

    @BindingAdapter("singleSeenAvatar")
    public static void bindSingleSeenAvatar(ImageProfileLayout imageView, User user) {
        Timber.i("setUserStatusImageProfile: " + user);
        if (user != null) {
            int seenAvatarSize = getSeenAvatarSize(imageView.getContext());
            imageView.setShowStatus(false);
            imageView.setUser(user, seenAvatarSize);
        }
    }

    private static int getSeenAvatarSize(Context context) {
        return context.getResources().getDimensionPixelSize(R.dimen.small_user_icon);
    }
}
