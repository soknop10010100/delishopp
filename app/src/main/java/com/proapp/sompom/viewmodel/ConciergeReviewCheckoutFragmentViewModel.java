package com.proapp.sompom.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.DelayWatcher;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.intent.ConciergeCheckoutIntent;
import com.proapp.sompom.intent.ConciergeDeliveryIntent;
import com.proapp.sompom.intent.ConciergeReviewCheckoutIntent;
import com.proapp.sompom.intent.newintent.ConciergeNewAddressIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.ConciergeCartModel;
import com.proapp.sompom.model.ShopDeliveryTimeSelection;
import com.proapp.sompom.model.concierge.ConciergeBasket;
import com.proapp.sompom.model.concierge.ConciergeBasketMenuItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeShopSetting;
import com.proapp.sompom.model.concierge.ConciergeUpdateBasketRequest;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.result.ConciergeCheckoutData;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.fragment.AbsSupportShopCheckOutFragment;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.ConciergeReviewCheckoutManager;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.DateUtils;
import com.proapp.sompom.utils.HTTPResponse;
import com.proapp.sompom.utils.NetworkStateUtil;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 16/9/21.
 */
public class ConciergeReviewCheckoutFragmentViewModel extends AbsLoadingViewModel {

    private final ObservableField<String> mUserName = new ObservableField<>();
    private final ObservableField<String> mDeliveryAddress = new ObservableField<>();
    private final ObservableField<String> mSubTotal = new ObservableField<>();
    private final ObservableField<String> mDeliveryCharge = new ObservableField<>();
    private final ObservableField<String> mDeliveryDetail = new ObservableField<>();
    private final ObservableField<String> mTime = new ObservableField<>();
    private final ObservableField<String> mDeliveryTime = new ObservableField<>();
    private final ObservableField<String> mSelectedSlotDate = new ObservableField<>();
    private final ObservableField<String> mSelectedSlotTime = new ObservableField<>();
    private final ObservableBoolean mIsSlotSelectionIconVisible = new ObservableBoolean();

    private final ObservableInt mCheckoutButtonTextColor = new ObservableInt();
    private final ObservableInt mCheckoutButtonBackground = new ObservableInt();
    private final ObservableInt mSelectSlotButtonBackground = new ObservableInt();
    private final ObservableBoolean mIsTimeSlotEnable = new ObservableBoolean();
    private boolean mIsContinueToPaymentButtonEnabled = false;

    private final ConciergeReviewCheckoutManager mDataManager;
    private ConciergeCheckoutData mCheckoutData;
    private final ConciergeReviewCheckoutFragmentViewModelCallback mListener;
    private ShopDeliveryTimeSelection mShopDeliveryTimeSelection;
    private String mSelectedTimeSlotId;
    private boolean mIsSlotProvince;
    private float mMinimumBasketPrice;
    private boolean mIsInExpressMode;

    public ConciergeReviewCheckoutFragmentViewModel(Context context,
                                                    ConciergeReviewCheckoutManager dataManager,
                                                    ConciergeReviewCheckoutFragmentViewModelCallback listener) {
        super(context);
        mIsInExpressMode = ApplicationHelper.isInExpressMode(context);
        mDataManager = dataManager;
        mListener = listener;
        mIsTimeSlotEnable.set(!mIsInExpressMode);
        enableContinueToPaymentButton(false);
        ConciergeShopSetting conciergeShopSetting = ConciergeHelper.getConciergeShopSetting(context);
        if (conciergeShopSetting != null) {
            mMinimumBasketPrice = conciergeShopSetting.getMinimumCheckoutPrice();
        }
    }

    public ObservableBoolean getIsTimeSlotEnable() {
        return mIsTimeSlotEnable;
    }

    public ObservableInt getSelectSlotButtonBackground() {
        return mSelectSlotButtonBackground;
    }

    public ObservableField<String> getSelectedSlotDate() {
        return mSelectedSlotDate;
    }

    public ObservableField<String> getSelectedSlotTime() {
        return mSelectedSlotTime;
    }

    public ObservableBoolean getIsSlotSelectionIconVisible() {
        return mIsSlotSelectionIconVisible;
    }

    public ConciergeReviewCheckoutManager getDataManager() {
        return mDataManager;
    }

    public ObservableField<String> getUserName() {
        return mUserName;
    }

    public ObservableField<String> getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public ObservableField<String> getSubTotal() {
        return mSubTotal;
    }

    public ObservableField<String> getDeliveryCharge() {
        return mDeliveryCharge;
    }

    public ObservableField<String> getDeliveryDetail() {
        return mDeliveryDetail;
    }

    public ObservableField<String> getTime() {
        return mTime;
    }

    public ObservableField<String> getDeliveryTime() {
        return mDeliveryTime;
    }

    public ObservableInt getCheckoutButtonTextColor() {
        return mCheckoutButtonTextColor;
    }

    public ObservableInt getCheckoutButtonBackground() {
        return mCheckoutButtonBackground;
    }

    @Override
    public void onRetryClick() {
        requestData(true, true, false);
    }

    public void reloadData() {
        requestData(false, false, false);
    }

    public void requestData(boolean isFirstLoadData, boolean isShowLoading, boolean isShowActionLoading) {
        if (isFirstLoadData && !NetworkStateUtil.isNetworkAvailable(mContext)) {
            showError(mContext.getString(R.string.error_internet_connection_description), true);
            return;
        }

        if (isShowLoading) {
            showLoading();
        } else if (isShowActionLoading) {
            showLoading(true);
        }

        Observable<Response<ConciergeCheckoutData>> service = mDataManager.getCartData(isFirstLoadData);
        ResponseObserverHelper<Response<ConciergeCheckoutData>> helper = new ResponseObserverHelper<>(getContext(), service);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeCheckoutData>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.i("onFail: " + ex.getMessage());
                hideLoading();
                String errorMessage = ex.getMessage();
                if (ex.getCode() == ErrorThrowable.CONNECTION_TIME_OUT) {
                    errorMessage = mContext.getString(R.string.shop_checkout_error_aba);
                }
                showError(errorMessage, true);
            }

            @Override
            public void onComplete(Response<ConciergeCheckoutData> result) {
                Timber.i("onComplete: " + new Gson().toJson(result.body()));
                if (isFirstLoadData && result.body() != null && result.body().getResumePaymentData() != null) {
                    mCheckoutData = result.body();
                    bindTimeSlot();
                    checkToSyncDeliveryInstructionFromOnlineBasket(mCheckoutData);
                    if (mListener != null) {
                        mListener.onRequestDataSuccess(mCheckoutData);
                    }

                    //Will resume to payment form directly
                    String timeSlotId = null;
                    boolean isSlotProvince = false;
                    if (ConciergeHelper.isSelectedBasketTimeSlotValid(mCheckoutData.getCartModel().getBasketTimeSlot())) {
                        timeSlotId = mCheckoutData.getCartModel().getBasketTimeSlot().getDeliveryTimes().get(0).getId();
                        isSlotProvince = mCheckoutData.getCartModel().getBasketTimeSlot().getDeliveryTimes().get(0).isAvailableProvince();
                    }
                    Timber.i("Will resume payment directly");
                    mContext.startActivity(new ConciergeCheckoutIntent(mContext,
                            mCheckoutData.getCartModel().getDeliveryAddress(),
                            timeSlotId,
                            isSlotProvince,
                            result.body().getResumePaymentData(),
                            mCheckoutData.getCartModel().getDeliveryInstruction()));
                    new Handler().postDelayed(() -> {
                        hideLoading();
                    }, 2000);
                } else {
                    mCheckoutData = result.body();
                    // Check to sync data from online basket with local basket
                    if (isFirstLoadData) {
                        if (mCheckoutData != null) {
                            checkToSyncDeliveryInstructionFromOnlineBasket(mCheckoutData);
                        }
                    }
                    // Bind other data normally
                    bindTimeSlot();
                    if (isFirstLoadData) {
                        hideLoading();
                        if (mListener != null) {
                            mListener.onRequestDataSuccess(mCheckoutData);
                        }
                    } else {
                        if (mListener != null) {
                            mListener.onRefreshData(mCheckoutData);
                        }
                        hideLoading();
                    }
                }
            }
        }));
    }

    public void updateOnlineBasket(ConciergeUpdateBasketRequest request) {
        // Update online basket
        ConciergeHelper.updateOnlineBasket(mContext,
                mDataManager.mApiService,
                request,
                new OnCallbackListener<Response<ConciergeBasket>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        Timber.e("Failed to update cart with these data: " + new Gson().toJson(request));
                    }

                    @Override
                    public void onComplete(Response<ConciergeBasket> result) {
                        Timber.i("Cart updated successfully");
                    }
                });
    }

    public void requestToRemoveItemInBasket(ConciergeMenuItem item) {
        showLoading(true);
        FlurryHelper.logEvent(mContext, FlurryHelper.REMOVE_PRODUCT_FROM_CHECKOUT_CARD);
        Observable<Response<ConciergeBasketMenuItem>> responseObservable = mDataManager.removeItemInBasket(item.getBasketItemId(), item);
        BaseObserverHelper<Response<ConciergeBasketMenuItem>> helper = new BaseObserverHelper<>(mContext, responseObservable);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeBasketMenuItem>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<ConciergeBasketMenuItem> result) {
                if (result.body() != null && result.body().isError()) {
                    hideLoading();
                    if (result.body().getDataType() == SupportConciergeCustomErrorResponse.ResponseDataType.PAYMENT_IN_PROCESS) {
                        if (mListener != null) {
                            mListener.onBasketModificationError(result.body(), result.body().getErrorMessage());
                        }
                    } else {
                        mListener.showOrderErrorPopup(mContext.getString(R.string.shop_request_remove_item_from_basket_error, item.getName()),
                                null);
                    }
                } else {
                    removeWholeItemFromLocalBasket(item);
                }
            }
        }));
    }

    public void requestUserPrimaryAddress(boolean isShowLoading) {
        if (isShowLoading) {
            showLoading(true);
        }
        Observable<Response<ConciergeUserAddress>> responseObservable = mDataManager.getPrimaryAddressService();
        BaseObserverHelper<Response<ConciergeUserAddress>> helper = new BaseObserverHelper<>(mContext, responseObservable);
        addDisposable(helper.execute(new OnCallbackListener<Response<ConciergeUserAddress>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<ConciergeUserAddress> result) {
                hideLoading();
                if (result.body().isError()) {
                    if (result.body().getCode() == HTTPResponse.NOT_FOUND) {
                        ((AbsBaseActivity) mContext).startActivityForResult(new ConciergeNewAddressIntent(mContext),
                                new ResultCallback() {
                                    @Override
                                    public void onActivityResultSuccess(int resultCode, Intent data) {
                                        ConciergeUserAddress newAddress = data.getParcelableExtra(ConciergeNewAddressIntent.ADDRESS_DATA);
                                        if (newAddress != null) {
                                            openConfirmCheckoutScreen(newAddress);
                                        }
                                    }
                                });
                    } else {
                        showSnackBar(result.body().getErrorMessage(), true);
                    }
                } else {
                    openConfirmCheckoutScreen(result.body());
                }
            }
        }));
    }

    private void openConfirmCheckoutScreen(ConciergeUserAddress address) {
        ((AbsBaseActivity) mContext).startActivityForResult(new ConciergeCheckoutIntent(mContext,
                        address,
                        mSelectedTimeSlotId,
                        mIsSlotProvince,
                        mDeliveryDetail.get()),
                new ResultCallback() {
                    @Override
                    public void onActivityResultSuccess(int resultCode, Intent data) {
                        if (data != null && data.getBooleanExtra(ConciergeReviewCheckoutIntent.SHOULD_RELOAD_DATA, false)) {
                            requestData(false, true, true);
                        }
                    }
                });
    }

    private void removeWholeItemFromLocalBasket(ConciergeMenuItem item) {
        Observable<Object> ob = ConciergeCartHelper.removeItem(item, true);
        BaseObserverHelper<Object> helper = new BaseObserverHelper<>(mContext, ob);
        addDisposable(helper.execute(new OnCallbackListener<Object>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                mListener.showOrderErrorPopup(mContext.getString(R.string.shop_request_remove_item_from_basket_error, item.getName()),
                        null);
            }

            @Override
            public void onComplete(Object result) {
                hideLoading();
                Intent intent = new Intent(AbsSupportShopCheckOutFragment.CART_ITEM_REMOVED_EVENT);
                intent.putExtra(AbsSupportShopCheckOutFragment.CART_EVENT_DATA, item);
                SendBroadCastHelper.verifyAndSendBroadCast(mContext, intent);
                if (mListener != null) {
                    mListener.onItemRemoved(item);
                }
            }
        }));
    }

    public void checkToSyncDeliveryInstructionFromOnlineBasket(ConciergeCheckoutData checkoutData) {
        ConciergeCartModel cartModel = checkoutData.getCartModel();
        // Update delivery instruction in case something was saved
        if (!TextUtils.isEmpty(cartModel.getDeliveryInstruction())) {
            mDeliveryDetail.set(cartModel.getDeliveryInstruction());
        }
    }

    private void bindTimeSlot() {
        if (!mIsInExpressMode) {
            /*
               For selected timeslot, we will clear the selection whenever use exit checkout screen.
               And we just maintain to display previous selection within screen at the time it is running
               for this method will be called when any data in checkout get updated.
             */
            if (mShopDeliveryTimeSelection != null &&
                    mShopDeliveryTimeSelection.getSelectedDate() != null &&
                    mShopDeliveryTimeSelection.getStartTime() != null &&
                    mShopDeliveryTimeSelection.getEndTime() != null &&
                    !TextUtils.isEmpty(mShopDeliveryTimeSelection.getTimeSlotId())) {
                updateTimeSlotData(mShopDeliveryTimeSelection, false);
                enableContinueToPaymentButton(true);
            } else {
                setSelectTimeSlotButtonStatus(false);
            }
        } else {
            enableContinueToPaymentButton(true);
        }
    }

    public TextWatcher onDeliveryInstructionTextChanged() {
        return new DelayWatcher(true) {
            @Override
            public void onTextChanged(String deliveryInstruction) {
                Timber.i("On delivery instruction changed: " + deliveryInstruction);
                ConciergeUpdateBasketRequest request = new ConciergeUpdateBasketRequest();
                request.setDeliveryInstruction(deliveryInstruction);
                updateOnlineBasket(request);
            }

            @Override
            public void onTextChangedNoDelay(String text) {
            }

            @Override
            public void onTyping(String text) {
            }
        };
    }

    private void enableContinueToPaymentButton(boolean isEnable) {
        if (isEnable) {
            mCheckoutButtonTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_pay_button));
            mCheckoutButtonBackground.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_pay_button_background));
            mIsContinueToPaymentButtonEnabled = true;
        } else {
            mCheckoutButtonTextColor.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_pay_button_disable));
            mCheckoutButtonBackground.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_pay_button_disable_background));
            mIsContinueToPaymentButtonEnabled = false;
        }
    }

    private void setSelectTimeSlotButtonStatus(boolean isSelected) {
        if (isSelected) {
            mSelectSlotButtonBackground.set(AttributeConverter.convertAttrToColor(mContext, R.attr.common_primary_color));
        } else {
            //Set select time slot title
            mSelectedSlotDate.set(mContext.getString(R.string.shop_checkout_select_schedule));
            mSelectedSlotTime.set("");
            mIsSlotSelectionIconVisible.set(true);
            mSelectSlotButtonBackground.set(AttributeConverter.convertAttrToColor(mContext, R.attr.shop_checkout_not_select_time_slot_button_background));
        }
    }

    public void onTimeClicked() {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).startActivityForResult(new ConciergeDeliveryIntent(mContext, mShopDeliveryTimeSelection),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            if (resultCode == AppCompatActivity.RESULT_OK) {
                                ShopDeliveryTimeSelection shopDeliveryTimeSelection = data.getParcelableExtra(ConciergeDeliveryIntent.DATA);
                                if (shopDeliveryTimeSelection != null) {
                                    Timber.i("shopDeliveryTimeSelection: " + shopDeliveryTimeSelection);
                                    updateTimeSlotData(shopDeliveryTimeSelection, false);
                                    enableContinueToPaymentButton(true);
                                }
                            }
                        }
                    });
        }
    }

    public void updateTimeSlotData(ShopDeliveryTimeSelection newData, boolean shouldUpdateOnlineBasket) {
        Timber.i("updateTimeSlotData: " + new Gson().toJson(newData));
        mSelectedTimeSlotId = newData.getTimeSlotId();
        mIsSlotProvince = newData.isProvince();
        //Since selected date is mandatory. We will supply the current date time if it is not selected.
        if (newData.getSelectedDate() == null) {
            newData.setSelectedDate(LocalDateTime.now());
        }
        if (mShopDeliveryTimeSelection == null) {
            mShopDeliveryTimeSelection = new ShopDeliveryTimeSelection();
        }

        mShopDeliveryTimeSelection.setTimeSlotId(newData.getTimeSlotId());
        mShopDeliveryTimeSelection.setSelectedDate(newData.getSelectedDate());
        mShopDeliveryTimeSelection.setStartTime(newData.getStartTime());
        mShopDeliveryTimeSelection.setEndTime(newData.getEndTime());
        mShopDeliveryTimeSelection.setIsProvince(newData.isProvince());

        mCheckoutData.setDeliveryData(mShopDeliveryTimeSelection);
        mSelectedSlotDate.set(newData.getSelectedDate().format(DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_24)));
        if (!newData.isProvince()) {
            mSelectedSlotTime.set(ConciergeHelper.buildTimeSlotDisplayFormat(mContext, newData.getStartTime().toLocalTime(), newData.getEndTime().toLocalTime()));
        }
        mIsSlotSelectionIconVisible.set(false);
        setSelectTimeSlotButtonStatus(true);

        if (shouldUpdateOnlineBasket) {
            // Update delivery preference selection
            ConciergeUpdateBasketRequest request = new ConciergeUpdateBasketRequest();
            request.setPreferenceDelivery(ConciergeHelper.buildOrderDeliveryFromTimeSelection(mShopDeliveryTimeSelection));
            updateOnlineBasket(request);
        }
    }

    public void onContinueClicked() {
        if (mIsContinueToPaymentButtonEnabled) {
            //Set the latest local basket data
            if (ConciergeCartHelper.getCartModel() != null) {
                mCheckoutData.setCartModel(ConciergeCartHelper.getCartModel());
            }

            if (mListener.getTotalPrice() < mMinimumBasketPrice) {
                mListener.onRequireMinBasketValue(mMinimumBasketPrice);
            } else {
                requestUserPrimaryAddress(true);
            }
        } else if (TextUtils.isEmpty(mSelectedTimeSlotId)) {
            //Let scroll screen to bottom to let user know what they have to select delivery slot
            if (mListener != null) {
                mListener.needToScrollToSelectDeliverySlotButton();
            }
        }
    }

    public void requestToClearBasket() {
        FlurryHelper.logEvent(mContext, FlurryHelper.CLEAR_CHECKOUT_CARD);
        showLoading(true);
        Observable<Response<SupportConciergeCustomErrorResponse>> observable = mDataManager.clearBasket();
        ResponseObserverHelper<Response<SupportConciergeCustomErrorResponse>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<SupportConciergeCustomErrorResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<SupportConciergeCustomErrorResponse> result) {
                if (result.body() != null && result.body().isError()) {
                    hideLoading();
                    mListener.showOrderErrorPopup(mContext.getString(R.string.shop_request_clear_basket_error),
                            null);
                } else {
                    hideLoading();
                    //Consider as order success
                    if (mListener != null) {
                        mListener.onOrderSuccess();
                    }
                }
            }
        }));
    }

    public interface ConciergeReviewCheckoutFragmentViewModelCallback {
        void onRequestDataSuccess(ConciergeCheckoutData data);

        void onOrderSuccess();

        void onRefreshData(ConciergeCheckoutData data);

        void onAllItemRemovedFromCart();

        void onBasketModificationError(SupportConciergeCustomErrorResponse response, String errorMessage);

        void onRequireMinBasketValue(double minBasketPrice);

        void showOrderErrorPopup(String errorMessage, View.OnClickListener okClickListener);

        void onItemRemoved(ConciergeMenuItem product);

        double getTotalPrice();

        void needToScrollToSelectDeliverySlotButton();
    }

    @BindingAdapter("setPaymentOptionBackground")
    public static void setPaymentOptionBackground(ViewGroup viewGroup, boolean isSelected) {
        viewGroup.setBackground(isSelected ? ContextCompat.getDrawable(viewGroup.getContext(),
                R.drawable.concierge_checkout_selected_payment_option_background) : null);
    }
}
