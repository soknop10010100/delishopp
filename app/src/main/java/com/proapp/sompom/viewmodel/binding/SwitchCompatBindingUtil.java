package com.proapp.sompom.viewmodel.binding;

import androidx.databinding.BindingAdapter;
import android.graphics.Typeface;
import androidx.appcompat.widget.SwitchCompat;

import com.resourcemanager.helper.FontHelper;

/**
 * Created by nuonveyo on 7/4/18.
 */

public final class SwitchCompatBindingUtil {
    private SwitchCompatBindingUtil() {
    }

    @BindingAdapter("setFont")
    public static void setFont(SwitchCompat switchCompat, boolean isSetFont) {
        if (isSetFont) {
            Typeface regularFont = FontHelper.getRegularFontStyle(switchCompat.getContext());
            switchCompat.setTypeface(regularFont);
        }
    }
}
