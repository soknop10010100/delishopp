package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.helper.NavigateSellerStoreHelper;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.helper.WallStreetIntentData;
import com.proapp.sompom.listener.OnTimelineHeaderItemClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.WallStreetAds;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.utils.DateTimeUtils;
import com.proapp.sompom.utils.GenerateLinkPreviewUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.SpannableUtil;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;
import com.proapp.sompom.widget.ImageProfileLayout;
import com.proapp.sompom.widget.WallStreetDescriptionTextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by He Rotha on 7/10/18.
 */
public class ListItemTimelineHeaderViewModel extends AbsLoadingViewModel {

    private static final int MAX_ADDRESS_CHARACTER_DISPLAY = 20;

    public static final int CLICK_ON_TEXT_POSITION = -1;
    public final ObservableBoolean mVisibleButtonFollow = new ObservableBoolean();
    public final ObservableField<User> mUser = new ObservableField<>();
    public final ObservableBoolean mIsFollowing = new ObservableBoolean();
    public final ObservableField<String> mTitle = new ObservableField<>();
    public final ObservableField<String> mShareTextTitle = new ObservableField<>();
    public final ObservableField<String> mAdsTitle = new ObservableField<>();
    public final ObservableField<String> mSimpleImageUrl = new ObservableField<>();
    public final ObservableBoolean mIsShowMoreButton = new ObservableBoolean(false);
    public final ObservableBoolean mIsSharedHeader = new ObservableBoolean();
    public final ObservableBoolean mIsProductItem = new ObservableBoolean();
    public final ObservableBoolean mIsSetMaxLine = new ObservableBoolean(true);
    public final ObservableInt mPostTextVisibility = new ObservableInt(View.VISIBLE);
    public final ObservableField<ImageProfileLayout.RenderType> mRenderType = new ObservableField<>(ImageProfileLayout.RenderType.USER);
    private ObservableField<SearchAddressResult> mCheckedInPlace = new ObservableField<>();
    private final ObservableBoolean mShowOnlineStatus = new ObservableBoolean(false);

    protected final int mPosition;
    protected final AbsBaseActivity mContext;
    private final OnTimelineHeaderItemClickListener mOnItemClickListener;
    protected Adaptive mAdaptive;
    private boolean mIsDisableClickOnUser;
    private boolean mIsLiveHeader = false;
    private SearchAddressResult mCheckedInPlaceData = null;
    @Bindable
    private String mPostId;
    @Bindable
    private List<Media> mPostMedia = new ArrayList<>();
    @Bindable
    private boolean mShouldShowLinkPreview;
    @Bindable
    private boolean mShouldShowPlacePreview;

    public ListItemTimelineHeaderViewModel(AbsBaseActivity context,
                                           Adaptive adaptive,
                                           int position,
                                           OnTimelineHeaderItemClickListener onItemClickListener) {
        super(context);
        mContext = context;
        mPosition = position;
        mOnItemClickListener = onItemClickListener;
        mAdaptive = adaptive;
        if (mAdaptive != null) {
            mPostId = adaptive.getId();
            mPostMedia = adaptive.getMedia();
        }
        init();
        bindPostTextVisibility();
        bindCheckedInPlace(adaptive);
        checkPreviewVisibility(adaptive);
    }

    private void bindPostTextVisibility() {
        if (mAdaptive != null) {
            if (TextUtils.isEmpty(mAdaptive.getTitle())) {
                mPostTextVisibility.set(View.GONE);
            } else {
                String firstUrlIndex = GenerateLinkPreviewUtil.getUrlFromText(mAdaptive.getTitle());
                if (!TextUtils.isEmpty(firstUrlIndex) &&
                        !TextUtils.isEmpty(mAdaptive.getTitle()) &&
                        TextUtils.equals(firstUrlIndex.trim(), mAdaptive.getTitle().trim()) &&
                        mAdaptive.getLinkPreviewModel() != null &&
                        mAdaptive.shouldShowLinkPreview() &&
                        (mAdaptive.getMedia() == null || mAdaptive.getMedia().isEmpty())) {
                    //There is only input link preview in post's text , so need to hide the display text.
                    mPostTextVisibility.set(View.GONE);
                } else {
                    mPostTextVisibility.set(View.VISIBLE);
                }
            }
        } else {
            mPostTextVisibility.set(View.GONE);
        }
    }

    public ObservableBoolean getShowOnlineStatus() {
        return mShowOnlineStatus;
    }

    public Integer getItemPosition() {
        return mPosition;
    }

    public ObservableInt getPostTextVisibility() {
        return mPostTextVisibility;
    }

    protected void checkPreviewVisibility(Adaptive adaptive) {
        if (adaptive != null) {
            mShouldShowLinkPreview = adaptive.shouldShowLinkPreview();
            mShouldShowPlacePreview = adaptive.shouldShowPlacePreview();
        }
    }

    public List<Media> getPostMedia() {
        return mPostMedia;
    }

    public boolean shouldShowLinkPreview() {
        return mShouldShowLinkPreview;
    }

    public boolean shouldShowPlacePreview() {
        return mShouldShowPlacePreview;
    }

    protected void bindCheckedInPlace(Adaptive adaptive) {
        if (WallStreetHelper.shouldRenderCheckedInLabel(adaptive)) {
            mCheckedInPlaceData = new SearchAddressResult();
            mCheckedInPlaceData.setPlaceTitle(((LifeStream) adaptive).getLocationName());
            mCheckedInPlaceData.setCity(((LifeStream) adaptive).getCity());
            mCheckedInPlaceData.setCountry(((LifeStream) adaptive).getCountry());
            Locations location = new Locations();
            location.setLatitude(((LifeStream) adaptive).getLatitude());
            location.setLongitude(((LifeStream) adaptive).getLongitude());
            mCheckedInPlaceData.setLocations(location);
            mCheckedInPlace.set(mCheckedInPlaceData);
        }
    }

    public String getPostId() {
        return mPostId;
    }

    public void setCheckedInPlace(SearchAddressResult checkedInPlace) {
        mCheckedInPlace.set(checkedInPlace);
    }

    public ObservableField<SearchAddressResult> getCheckedInPlace() {
        return mCheckedInPlace;
    }

    public void init() {
        if (mAdaptive != null) {
            mShowOnlineStatus.set(!mAdaptive.isWelcomePostType());
            setDisableClickOnUser(mAdaptive.isWelcomePostType());
        }

        if (mAdaptive instanceof Product) {
            mRenderType.set(ImageProfileLayout.RenderType.SHOP);
            mIsProductItem.set(true);
        } else {
            mRenderType.set(ImageProfileLayout.RenderType.USER);
            mIsProductItem.set(false);
        }

        User user;
        String description;
        if (mAdaptive instanceof WallStreetAds) {
            mVisibleButtonFollow.set(true);
            mIsFollowing.set(true);
            WallStreetAds wall = ((WallStreetAds) mAdaptive);
            mAdsTitle.set(wall.getTitle());
            mSimpleImageUrl.set(wall.getCreativeIconUrl());
//            if (wall.getNativeAd() != null) {
//                wall.getNativeAd().recordImpression();
//            }
        } else if (mAdaptive instanceof WallStreetAdaptive) {
            description = ((WallStreetAdaptive) mAdaptive).getDescription();
            mTitle.set(description);
            user = ((WallStreetAdaptive) mAdaptive).getUser();
            if (user != null) {
                //button follow visible for Product only
                if (mAdaptive instanceof Product) {
                    //button follow visible only if user not yet follow,
                    //or is not belong to user himself
                    boolean isFollow = true;
                    if (!SharedPrefUtils.checkIsMe(mContext, user.getId())) {
                        isFollow = user.isFollow();
                    }

                    mVisibleButtonFollow.set(!isFollow);
                    mIsFollowing.set(isFollow);
                } else {
                    mVisibleButtonFollow.set(false);
                }
                mUser.set(user);
                mIsShowMoreButton.set(SharedPrefUtils.checkIsMe(getContext(), user.getId()));
            }
        }
    }

    void setAdaptive(Adaptive adaptive) {
        mAdaptive = adaptive;
        init();
    }

    void enableLiveHeader() {
        mIsLiveHeader = true;
    }

    void setSharedTextTitle(String title) {
        mShareTextTitle.set(title);
        mIsSharedHeader.set(true);
    }

    public String getFollowText(boolean isFollow) {
        if (mAdaptive instanceof WallStreetAds) {
            return mContext.getString(R.string.home_sponsored);
        } else if (isFollow) {
            return mContext.getString(R.string.following);
        } else {
            return mContext.getString(R.string.seller_store_follow_title);
        }
    }

    @Bindable
    public SpannableString getSubtitle() {
        if (mAdaptive instanceof WallStreetAds) {
            String sub = ((WallStreetAds) mAdaptive).getSubTitle();
            if (TextUtils.isEmpty(sub)) {
                return null;
            }
            return new SpannableString(sub);
        } else if (mIsLiveHeader && mAdaptive instanceof LifeStream) {
            return new SpannableString(((LifeStream) mAdaptive).getUser().getStoreName());
        } else if (mAdaptive instanceof WallStreetAdaptive) {
            String address = getValidCheckInAddress(((WallStreetAdaptive) mAdaptive).getDisplayAddress());
//            PublishItem privacy = ((WallStreetAdaptive) mAdaptive).getPublish();
            //Display directly the world icon for post.
            if (mAdaptive instanceof Product) {
                return SpannableUtil.getProduct(mContext, ((Product) mAdaptive).getName(), address,
                        ((WallStreetAdaptive) mAdaptive).getPublish());
            } else {
                Date createDate = new Date();
                if (((WallStreetAdaptive) mAdaptive).getCreateDate() != null) {
                    createDate = ((WallStreetAdaptive) mAdaptive).getCreateDate();
                }
                String convertDate = DateTimeUtils.parse(getContext(), createDate.getTime(), false, null);
                return SpannableUtil.getTimelineDate(mContext,
                        convertDate,
                        address,
                        ((WallStreetAdaptive) mAdaptive).getPublish(),
                        mAdaptive.isWelcomePostType());
            }
        } else {
            return null;
        }
    }

    private String getValidCheckInAddress(String address) {
        if (!TextUtils.isEmpty(address)) {
            if (address.length() > MAX_ADDRESS_CHARACTER_DISPLAY) {
                return address.substring(0, MAX_ADDRESS_CHARACTER_DISPLAY) + "...";
            } else {
                return address;
            }
        }

        return "";
    }

    public void onFollowButtonClick(View view, Context context) {
        if (mAdaptive instanceof WallStreetAds) {
            return;
        }
        if (mOnItemClickListener != null) {
            ((AbsBaseActivity) context).startLoginWizardActivity(new LoginActivityResultCallback() {
                @Override
                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                    initFollow(isAlreadyLogin, context, view);
                }
            });
        }
    }

    private void initFollow(boolean isAlreadyLogin, Context context, View view) {
        User user = mUser.get();
        if (user == null) {
            return;
        }
        if (!isAlreadyLogin) {
            if (!SharedPrefUtils.getUserId(context).equals(user.getId())) {
                mIsFollowing.set(!mIsFollowing.get());
                mVisibleButtonFollow.set(!mIsFollowing.get());
                user.setFollow(mIsFollowing.get());
                mOnItemClickListener.onFollowButtonClick(user.getId(), mIsFollowing.get());
            } else {
                mVisibleButtonFollow.set(false);
            }
        } else {
            mIsFollowing.set(!mIsFollowing.get());
            mVisibleButtonFollow.set(!mIsFollowing.get());
            user.setFollow(mIsFollowing.get());
            mOnItemClickListener.onFollowButtonClick(user.getId(), mIsFollowing.get());
        }
    }

    public void onMoreButtonClick() {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onShowPostContextMenuClick(mAdaptive, mUser.get(), mPosition);
        }
    }

    public void setDisableClickOnUser(boolean disableClickOnUser) {
        mIsDisableClickOnUser = disableClickOnUser;
    }

    public void onUserProfileClick() {
        if (!mIsDisableClickOnUser) {
            new NavigateSellerStoreHelper(mContext, mUser.get()).openSellerStore();
        }
    }

    public void onHeaderItemClick(WallStreetDescriptionTextView textView) {
        if (mAdaptive.isWelcomePostType()) {
            return;
        }

//        Timber.i("onHeaderItemClick: ");
        boolean shouldOpenComment = true;
        if (textView.isSeeMore()) {
            shouldOpenComment = false;
        }

        if (shouldOpenComment) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onCommentClick(mAdaptive, false);
            }
        } else {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.getRecyclerView().pause();
                mOnItemClickListener.getRecyclerView().setAutoResume(false);
            }
            mAdaptive.startActivityForResult(new WallStreetIntentData(mContext, mOnItemClickListener));
        }
    }

    private boolean isPostContainOnlyText() {
        return !TextUtils.isEmpty(mAdaptive.getTitle()) && (mAdaptive.getMedia() == null || mAdaptive.getMedia().isEmpty());
    }

    public void onFooterCommentItemClick() {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onCommentClick(mAdaptive, false);
        }
    }
}
