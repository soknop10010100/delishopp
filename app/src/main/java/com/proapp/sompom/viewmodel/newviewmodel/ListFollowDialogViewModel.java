package com.proapp.sompom.viewmodel.newviewmodel;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.emun.FollowItemType;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.StoreDataManager;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 7/10/18.
 */

public class ListFollowDialogViewModel extends AbsLoadingViewModel {

    private final StoreDataManager mDataManager;
    private final User mUser;
    private final Adaptive mAdaptive;
    private final FollowItemType mType;
    private boolean mIsLoadedData;
    private ListFollowDialogViewModelListener mDialogViewModelListener;
    private ObservableBoolean mIsRefresh = new ObservableBoolean();
    private boolean mIsPostContainOnlyItem;
    private String mPostId;
    private ObservableField<String> mScreenTitle = new ObservableField<>();

    public ListFollowDialogViewModel(StoreDataManager dataManager,
                                     Adaptive adaptive,
                                     User user,
                                     boolean isPostContainOnlyItem,
                                     String postId,
                                     FollowItemType type,
                                     ListFollowDialogViewModelListener dialogViewModelListener) {
        super(dataManager.getContext());
        mPostId = postId;
        mDialogViewModelListener = dialogViewModelListener;
        mIsPostContainOnlyItem = isPostContainOnlyItem;
        mDataManager = dataManager;
        mUser = user;
        mType = type;
        mAdaptive = adaptive;
        mScreenTitle.set(mType == FollowItemType.LIKE_VIEWER ? getContext().getString(R.string.post_user_like_screen_title) :
                getContext().getString(R.string.post_user_view_screen_title));
    }

    public ObservableField<String> getScreenTitle() {
        return mScreenTitle;
    }

    public ObservableBoolean getIsRefresh() {
        return mIsRefresh;
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        mDataManager.resetPagination();
        getListFollow(true, false);
    }

    public final void onBackClick() {
        if (mDialogViewModelListener != null) {
            mDialogViewModelListener.onBackButtonClicked();
        }
    }

    public void getListFollow(boolean isRefresh, boolean isLoadMore) {
        if (!isRefresh && !isLoadMore) {
            showLoading();
        }
        Observable<List<User>> call;
        if (mType == FollowItemType.FOLLOWER) {
            call = mDataManager.getFollower(mUser.getId());
        } else if (mType == FollowItemType.FOLLOWING) {
            call = mDataManager.getFollowing(mUser.getId());
        } else if (mType == FollowItemType.LIKE_VIEWER) {
            call = mDataManager.getUserLike(mIsPostContainOnlyItem ? mPostId : mAdaptive.getId());
        } else {
            call = mDataManager.getUserView(mIsPostContainOnlyItem ? mPostId : mAdaptive.getId());
        }
        BaseObserverHelper<List<User>> helper = new BaseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<List<User>>() {
            @Override
            public void onComplete(List<User> result) {
                mIsRefresh.set(false);
                if (result == null || result.isEmpty()) {
                    onFail(new ErrorThrowable(404, null));
                } else {
                    hideLoading();
                    mIsLoadedData = true;
                    if (mDialogViewModelListener != null) {
                        mDialogViewModelListener.onLoadSuccess(result,
                                isRefresh,
                                isLoadMore,
                                mDataManager.isCanLoadMore());
                    }
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                mIsRefresh.set(false);
                if (mIsLoadedData) {
                    return;
                }

                if (ex.getCode() == 404) {
                    showError(getContext().getString(R.string.error_no_data));
                } else {
                    showError(ex.toString());
                }
            }
        }));
    }

    public void loadMore(ListFollowDialogViewModelListener listOnCallbackListener) {
        Observable<List<User>> call;
        if (mType == FollowItemType.FOLLOWER) {
            call = mDataManager.getFollower(mUser.getId());
        } else if (mType == FollowItemType.FOLLOWING) {
            call = mDataManager.getFollowing(mUser.getId());
        } else if (mType == FollowItemType.LIKE_VIEWER) {
            call = mDataManager.getUserLike(mAdaptive.getId());
        } else {
            call = mDataManager.getUserView(mAdaptive.getId());
        }

        BaseObserverHelper<List<User>> helper = new BaseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<List<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                listOnCallbackListener.onFail(ex);
            }

            @Override
            public void onComplete(List<User> result) {
                listOnCallbackListener.onLoadSuccess(result,
                        false,
                        true,
                        mDataManager.isCanLoadMore());
            }
        }));
    }

    public void onFollowClick(User user, boolean isFollow) {
        Observable<Response<Object>> callback;
        if (isFollow) {
            callback = mDataManager.postFollow(user.getId());
        } else {
            callback = mDataManager.unFollow(user.getId());
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute());
    }

    public OnClickListener onRetryButtonClick() {
        return () -> getListFollow(false, false);
    }

    public interface ListFollowDialogViewModelListener {
        void onBackButtonClicked();

        void onLoadSuccess(List<User> userList,
                           boolean isRefresh,
                           boolean isLoadMore,
                           boolean isCanLoadMore);

        void onFail(ErrorThrowable ex);
    }
}
