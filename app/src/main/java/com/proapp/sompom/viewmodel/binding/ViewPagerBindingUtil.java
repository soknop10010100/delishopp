package com.proapp.sompom.viewmodel.binding;

import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.MoreGameAdapter;
import com.proapp.sompom.helper.AlphaAnimation;
import com.proapp.sompom.helper.LoopAnimation;
import com.proapp.sompom.model.result.MoreGame;
import com.proapp.sompom.newui.fragment.MoreGameFragment;
import com.proapp.sompom.viewmodel.ViewPagerViewModel;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator3;

/**
 * Created by nuonveyo on 4/24/18.
 */

public final class ViewPagerBindingUtil {

    private ViewPagerBindingUtil() {
    }

    @BindingAdapter("setAdapter")
    public static void setAdapter(ViewPager viewPager,
                                  ViewPagerViewModel viewPagerViewModel) {
        List<MoreGame> games = viewPagerViewModel.getMoreGames();
        FragmentManager manager = viewPagerViewModel.getFragmentManager();

        if (games == null || games.isEmpty()) {
            return;
        }

        List<MoreGameFragment> fragments = new ArrayList<>();

        for (int i = 0; i < viewPagerViewModel.getMoreGames().size(); i++) {
            MoreGameFragment fragment = MoreGameFragment.getInstance(games.get(i));
            fragments.add(fragment);
        }
        final MoreGameAdapter adapter = new MoreGameAdapter(manager, fragments);
        viewPager.setAdapter(adapter);
    }

    @BindingAdapter(value = {"setAdapter", "setTextViewLabel", "setCirclePageIndicator"})
    public static void setViewPagerAdapter(final ViewPager viewPager,
                                           final ViewPagerViewModel viewPagerViewModel,
                                           final TextView textViewLabel,
                                           CirclePageIndicator circlePageIndicator) {

        setAdapter(viewPager, viewPagerViewModel);
        circlePageIndicator.setViewPager(viewPager);


        final List<MoreGame> games = viewPagerViewModel.getMoreGames();
        if (games == null || games.isEmpty()) {
            return;
        }

        final AlphaAnimation in = new AlphaAnimation(0f, 1f);
        final AlphaAnimation out = new AlphaAnimation(1f, 0f);

        if (viewPager.getTag() instanceof LoopAnimation) {
            LoopAnimation loopAnimation = (LoopAnimation) viewPager.getTag();
            loopAnimation.cancel();
        }
        LoopAnimation loopAnimation = new LoopAnimation(games.size());
        loopAnimation.setAnimation(position -> {
            final MoreGame game = games.get(position);
            out.setOnAnimationListener(() -> {
                viewPager.setCurrentItem(position, true);
                TextViewBindingUtil.setTextViewLabel(textViewLabel, game);
                textViewLabel.startAnimation(in);
            });
            textViewLabel.startAnimation(out);
        });
        viewPager.setTag(loopAnimation);
    }

    @BindingAdapter(value = {"setAutoScrollAdapter"})
    public static void setAutoScrollViewPager(final ViewPager2 viewPager,
                                              final int childCount) {
        if (childCount == 0) {
            return;
        }

        final int[] currentPage = {viewPager.getCurrentItem()};

        // These animation are just for driving the auto scroll of the viewpager
        final AlphaAnimation in = new AlphaAnimation(1f, 1f);
        final AlphaAnimation out = new AlphaAnimation(1f, 1f);

        // Cancel any previously running auto scroll animation on the view pager
        cancelAutoScrollViewPager(viewPager);

        ViewPager2.OnPageChangeCallback callback = new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                currentPage[0] = position;
            }
        };
        viewPager.registerOnPageChangeCallback(callback);

        LoopAnimation loopAnimation = new LoopAnimation(childCount);
        loopAnimation.setAnimation(position -> {
            out.setOnAnimationListener(() -> {
                currentPage[0] = position == 0 ? position : currentPage[0] + 1;
                viewPager.setCurrentItem(currentPage[0], true);
                viewPager.startAnimation(in);
            });
            viewPager.startAnimation(out);
        });
        viewPager.setTag(R.id.loop_animation_key, loopAnimation);
        viewPager.setTag(R.id.loop_scroll_listener_key, callback);
    }

    public static void cancelAutoScrollViewPager(final ViewPager2 viewPager) {
        if (viewPager.getTag(R.id.loop_animation_key) instanceof LoopAnimation) {
            LoopAnimation loopAnimation = (LoopAnimation) viewPager.getTag(R.id.loop_animation_key);
            loopAnimation.cancel();
        }
        if (viewPager.getTag(R.id.loop_scroll_listener_key) instanceof ViewPager2.OnPageChangeCallback) {
            ViewPager2.OnPageChangeCallback callback =
                    (ViewPager2.OnPageChangeCallback) viewPager.getTag(R.id.loop_scroll_listener_key);
            viewPager.unregisterOnPageChangeCallback(callback);
        }
    }

    @BindingAdapter(value = {"ci_drawable", "ci_drawable_unselected"}, requireAll = false)
    public static void setCircleIndicator3Drawable(CircleIndicator3 circleIndicator3,
                                                   int selectedColor,
                                                   int unselectedDrawable) {
        circleIndicator3.tintIndicator(selectedColor, unselectedDrawable);
    }
}
