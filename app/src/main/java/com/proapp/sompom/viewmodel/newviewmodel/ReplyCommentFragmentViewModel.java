package com.proapp.sompom.viewmodel.newviewmodel;

import android.text.TextUtils;

import androidx.databinding.ObservableBoolean;

import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.SoundEffectService;
import com.proapp.sompom.listener.LoadMoreCommentDirectionCallback;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.listener.OnEditTextCommentListener;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.request.CommentBody;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.model.result.JumpCommentResponse;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.datamanager.PopUpCommentDataManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class ReplyCommentFragmentViewModel extends LayoutEditTextCommentViewModel {

    private final PopUpCommentDataManager mCommentDataManager;
    private final OnCallback mListOnCallbackListListener;
    private Comment mMainComment;

    public ReplyCommentFragmentViewModel(String postId,
                                         String contentId,
                                         ContentType contentType,
                                         Comment mainComment,
                                         PopUpCommentDataManager dataManager,
                                         OnCallback onCallbackListListener,
                                         SendNewCommendInterceptorListener sendNewCommendInterceptorListener) {
        super(dataManager,
                postId,
                contentId,
                contentType,
                onCallbackListListener,
                sendNewCommendInterceptorListener);
        mMainComment = mainComment;
        mCommentDataManager = dataManager;
        mListOnCallbackListListener = onCallbackListListener;
        getData(null, false);
    }

    public PopUpCommentDataManager getDataManager() {
        return mCommentDataManager;
    }

    @Override
    public void onRetryClick() {
        getData(null, false);
    }

    @Override
    public void onRefresh() {
        if (!isDisplayErrorInScreen()) {
            super.onRefresh();
            if (mListOnCallbackListListener != null) {
                mListOnCallbackListListener.onPullAndRefresh(mIsRefresh);
            }
        }
    }

    private String getMainCommentId() {
        if (mMainComment != null) {
            return mMainComment.getId();
        }

        return null;
    }

    private List<Comment> getReplyCommentListOfMainComment() {
        if (mMainComment != null) {
            return mMainComment.getReplyComment();
        }

        return new ArrayList<>();
    }

    @Override
    public void postComment(Comment comment) {
        comment.setPosting(true);
        Observable<Response<Comment>> callback = mDataManager.postSubComment(getMainCommentId(), comment);
        ResponseObserverHelper<Response<Comment>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<Comment>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mSnackBarErrorMessage.set(null);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    mSnackBarErrorMessage.set(ex.toString());
                } else {
                    mSnackBarErrorMessage.set(getContext().getString(R.string.comment_toast_post_fail));
                }
                mEditTextCommentListener.onPostCommentFail();
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onComplete(Response<Comment> result) {
//                showOrHideEmptyCommentLabel(false);
                SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.SEND_MESSAGE);
                mListOnCallbackListListener.onPostSubCommentSuccess(result.body());
            }
        }));
        mMessage.set("");
    }

    @Override
    public void updateComment(Comment comment) {
        if (mUpdateCommentPosition == 0) {
            super.updateComment(comment);
        } else {
            CommentBody commentBody = new CommentBody();
            commentBody.setMessage(comment.getContent());
            Observable<Response<Comment>> callback = mDataManager.updateSubComment(comment.getId(), commentBody);
            ResponseObserverHelper<Response<Comment>> helper = new ResponseObserverHelper<>(getContext(), callback);
            addDisposable(helper.execute(new OnCallbackListener<Response<Comment>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    mSnackBarErrorMessage.set(null);
                    if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                        mSnackBarErrorMessage.set(ex.toString());
                    } else {
                        mSnackBarErrorMessage.set(getContext().getString(R.string.comment_toast_post_fail));
                    }
                }

                @Override
                public void onComplete(Response<Comment> result) {
                    Timber.i("Update sub comment success: mUpdateCommentPosition: " + mUpdateCommentPosition);
                    mListOnCallbackListListener.onSubCommentUpdated(result.body(), mUpdateCommentPosition);
                }
            }));
        }
    }

    public void getData(OnCallbackListener<Response<List<Comment>>> listener, boolean isActionLoading) {
        showLoading(isActionLoading);
        Observable<Response<List<Comment>>> observable = mCommentDataManager.getSubCommentList(false,
                getMainCommentId(),
                JumpCommentResponse.REDIRECTION_NEXT);
        ResponseObserverHelper<Response<List<Comment>>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Comment>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsRefresh.set(false);
                showError(ex.toString());
                if (listener != null) {
                    listener.onFail(ex);
                }
            }

            @Override
            public void onComplete(Response<List<Comment>> result) {
                if (mDataManager.isInJumpCommentMode() && mDataManager.getMainCommentOfJumpSubComment() == null) {
                    //Will show comment not found error for the main comment has been deleted.
                    showError(mContext.getString(R.string.comment_reply_main_comment_not_found), false);
                } else {
                    if (mDataManager.isInJumpCommentMode()) {
                        //Assign main comment from response
                        mMainComment = mDataManager.getMainCommentOfJumpSubComment();
                    }
                    hideLoading();
                    mIsRefresh.set(false);
                    List<Comment> commentList = result.body();
                    if (mListOnCallbackListListener != null) {
                        mListOnCallbackListListener.onLoadCommentSuccess(
                                mDataManager.getMainCommentOfJumpSubComment(),
                                commentList,
                                mDataManager.isInJumpCommentMode(),
                                mCommentDataManager.isCanLoadPrevious(),
                                mCommentDataManager.isCanLoadNext());
                    }
                    if (listener != null) {
                        listener.onComplete(result);
                    }
                }
            }
        }));
    }

    public void loadMoreCommentFromDirection(String loadDirection, LoadMoreCommentDirectionCallback listListener) {
        Observable<Response<List<Comment>>> observable = mCommentDataManager.getSubCommentList(true,
                getMainCommentId(),
                loadDirection);
        ResponseObserverHelper<Response<List<Comment>>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Comment>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsRefresh.set(false);
                //Need to find where the load more direction came from previous or next.
                boolean canLoadPrevious = mDataManager.isCanLoadMore();
                boolean canLoadNext = mDataManager.isCanLoadNext();
                if (mDataManager.isInJumpCommentMode()) {
                    if (TextUtils.equals(loadDirection, JumpCommentResponse.REDIRECTION_NEXT)) {
                        canLoadNext = false;
                    } else {
                        canLoadPrevious = false;
                    }
                } else {
                    canLoadPrevious = false;
                }
                listListener.onFail(ex, mDataManager.isInJumpCommentMode(), canLoadPrevious, canLoadNext);
            }

            @Override
            public void onComplete(Response<List<Comment>> result) {
                mIsRefresh.set(false);
                listListener.onLoadCommentDirectionSuccess(result.body(),
                        mDataManager.isInJumpCommentMode(),
                        mDataManager.isCanLoadPrevious(),
                        mDataManager.isCanLoadNext());
            }
        }));
    }

    public void onDeleteComment(Comment comment, int position, OnClickListener onClickListener) {
        showLoading(true);
        Observable<Response<Object>> callback;
        if (position == 0) {
            callback = mCommentDataManager.deleteComment(comment.getId());
        } else {
            callback = mCommentDataManager.deleteSubComment(comment.getId());
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                mSnackBarErrorMessage.set(null);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    mSnackBarErrorMessage.set(ex.toString());
                } else {
                    mSnackBarErrorMessage.set(getContext().getString(R.string.comment_toast_delete_fail));
                }
            }

            @Override
            public void onComplete(Response<Object> result) {
                hideLoading();
                onClickListener.onClick();
                //Position 0 is main comment and we will not fire the remove sub comment success callback.
                if (position > 0) {
                    mListOnCallbackListListener.onRemoveSubCommentSuccess();
                }
            }
        }));
    }

    public void checkToLikeComment(Comment comment, int position, OnCallbackListener<Response<Object>> listener) {
        Observable<Response<Object>> callback;
        if (position == 0) {
            callback = mDataManager.postLikeComment(comment.getId(), comment.isLike(), mPostId);
        } else {
            callback = mDataManager.postLikeSubComment(comment.getId(), comment.isLike(), mPostId);
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mSnackBarErrorMessage.set(ex.getMessage());
                mSnackBarErrorMessage.notifyChange();
                if (listener != null) {
                    listener.onFail(ex);
                }
            }

            @Override
            public void onComplete(Response<Object> result) {
                if (listener != null) {
                    listener.onComplete(result);
                }
            }
        }));
    }

    public interface OnCallback extends OnEditTextCommentListener {

        void onLoadCommentSuccess(Comment mainComment,
                                  List<Comment> listComment,
                                  boolean isInJumpMode,
                                  boolean isCanLoadPrevious,
                                  boolean isCanLoadNext);

        void onPostSubCommentSuccess(Comment comment);

        void onSubCommentUpdated(Comment comment, int position);

        void onPullAndRefresh(ObservableBoolean refresh);

        void onRemoveSubCommentSuccess();
    }
}
