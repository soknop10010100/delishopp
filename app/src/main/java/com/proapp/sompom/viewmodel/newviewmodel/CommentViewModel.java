package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.helper.NavigateSellerStoreHelper;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.listener.OnCommentItemClickListener;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.model.result.ContentStat;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.DateTimeUtils;
import com.proapp.sompom.utils.EmojiDetectorUtil;
import com.proapp.sompom.utils.FormatNumber;
import com.proapp.sompom.utils.GenerateLinkPreviewUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.SpecialTextRenderUtils;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

import retrofit2.Response;

/**
 * Created by He Rotha on 9/15/17.
 */

public class CommentViewModel extends AbsBaseViewModel {

    public final ObservableBoolean mIsShowReply = new ObservableBoolean(true);
    public final ObservableBoolean mIsLike = new ObservableBoolean();
    public final ObservableField<String> mLikeCount = new ObservableField<>();
    public final ObservableField<String> mHour = new ObservableField<>();
    public final ObservableBoolean mIsShowLikeCounter = new ObservableBoolean();
    public final ObservableBoolean mIsReplyCommentItem = new ObservableBoolean();
    public final ObservableField<User> mUser = new ObservableField<>();
    public final ObservableField<Drawable> mCommentBackground = new ObservableField<>();
    public final ObservableInt mTextSize = new ObservableInt();
    private ObservableField<String> mLinkTitle = new ObservableField<>();
    private ObservableField<String> mLinkDescription = new ObservableField<>();
    private ObservableField<String> mLink = new ObservableField<>();
    private ObservableField<String> mLinkIcon = new ObservableField<>();
    private ObservableInt mLinkIconVisibility = new ObservableInt(View.GONE);
    private ObservableInt mDescriptionVisibility = new ObservableInt(View.GONE);

    protected final Comment mComment;
    private final OnCommentItemClickListener mOnCommentItemClickListener;
    private final int mPosition;
    protected final Context mContext;
    private final ContentStat mContentStat;
    protected ObservableBoolean mShouldIgnoreLongPress = new ObservableBoolean();

    public CommentViewModel(Context context,
                            int position,
                            Comment comment,
                            OnCommentItemClickListener onCommentItemClickListener) {
        mContext = context;
        mComment = comment;
        mContentStat = mComment.getContentStat();
        mUser.set(comment.getUser());
        checkToEnableLongPressForOption();

        mIsLike.set(mComment.isLike());
        mPosition = position;
        mOnCommentItemClickListener = onCommentItemClickListener;

        if (!TextUtils.isEmpty(mComment.getContent()) && EmojiDetectorUtil.isEmojiOnly(mComment.getContent())) {
            mCommentBackground.set(mContext.getDrawable(R.drawable.layout_comment_emoji_only));
            mTextSize.set(mContext.getResources().getDimensionPixelSize(R.dimen.text_emoji_size));
        } else {
            mTextSize.set(mContext.getResources().getDimensionPixelSize(R.dimen.text_medium));
            mCommentBackground.set(mContext.getDrawable(R.drawable.layout_comment));
        }

        if (mComment.isPosting()) {
            mHour.set(mContext.getString(R.string.comment_posting_status));
        } else {
            mHour.set(DateTimeUtils.parse(mContext, mComment.getDate().getTime(), false, null));
        }

        if (mContentStat.getTotalLikes() > 0) {
            mIsShowLikeCounter.set(true);
            if (mContentStat.getTotalLikes() > 1) {
                mLikeCount.set(FormatNumber.format(mContentStat.getTotalLikes()));
            }
        }
        checkToBindLinkPreviewContent();
    }

    public CommentViewModel(Context context,
                            int position,
                            Comment comment,
                            OnCommentItemClickListener onCommentItemClickListener,
                            boolean isReplyComment) {
        this(context, position, comment, onCommentItemClickListener);
        mIsReplyCommentItem.set(isReplyComment);
        mIsShowReply.set(false);
        checkToBindLinkPreviewContent();
    }

    public Spannable getMessage() {
        if (!TextUtils.isEmpty(mComment.getContent())) {
            return SpecialTextRenderUtils.renderMentionUser(mContext,
                    mComment.getContent(),
                    true,
                    false,
                    AttributeConverter.convertAttrToColor(mContext, R.attr.comment_text_mention),
                    AttributeConverter.convertAttrToColor(mContext, R.attr.comment_text_url),
                    AttributeConverter.convertAttrToColor(mContext, R.attr.comment_text_hashtag),
                    false,
                    null);
        }
        return new SpannableString("");
    }

    public ObservableInt getDescriptionVisibility() {
        return mDescriptionVisibility;
    }

    public Spannable getMessageContainLink() {
        /*
        If there is only link in comment, we will display only preview data. Otherwise, we keep
        displaying the same as old normal comment text.
         */
        String content = mComment.getContent();
        if (content != null) {
            content = content.trim();
        }

        if (content != null && !TextUtils.isEmpty(content)) {
            String firstUrlIndex = GenerateLinkPreviewUtil.getFirstUrlIndex(content);
            if (firstUrlIndex != null && !TextUtils.isEmpty(firstUrlIndex)) {
                String[] split = content.split(" ");
                if (split.length > 1) {
                    mDescriptionVisibility.set(View.VISIBLE);
                    return SpecialTextRenderUtils.renderMentionUser(mContext,
                            content,
                            true,
                            true,
                            AttributeConverter.convertAttrToColor(mContext, R.attr.comment_text_mention),
                            AttributeConverter.convertAttrToColor(mContext, R.attr.comment_text_url),
                            AttributeConverter.convertAttrToColor(mContext, R.attr.comment_text_hashtag),
                            false,
                            null);
                }
            } else {
                mDescriptionVisibility.set(View.VISIBLE);
                return SpecialTextRenderUtils.renderMentionUser(mContext,
                        content,
                        true,
                        true,
                        AttributeConverter.convertAttrToColor(mContext, R.attr.post_description_mention),
                        AttributeConverter.convertAttrToColor(mContext, R.attr.comment_preview_link),
                        AttributeConverter.convertAttrToColor(mContext, R.attr.post_description_hashtag),
                        false,
                        null);
            }
        }

        mDescriptionVisibility.set(View.GONE);
        return new SpannableString("");
    }

    private void checkToBindLinkPreviewContent() {
        if (mComment.getMetaPreview() != null && !mComment.getMetaPreview().isEmpty()) {
            LinkPreviewModel linkPreviewModel = mComment.getMetaPreview().get(0);
            mLinkTitle.set(LinkPreviewRetriever.getPreviewTitle(linkPreviewModel));
            mLinkDescription.set(LinkPreviewRetriever.getPreviewDescription(linkPreviewModel));
            mLink.set(linkPreviewModel.getLink());
            if (!TextUtils.isEmpty(linkPreviewModel.getLogo())) {
                mLinkIconVisibility.set(View.VISIBLE);
                mLinkIcon.set(linkPreviewModel.getLogo());
            } else if (!TextUtils.isEmpty(linkPreviewModel.getImage())) {
                mLinkIconVisibility.set(View.VISIBLE);
                mLinkIcon.set(linkPreviewModel.getImage());
            } else {
                mLinkIconVisibility.set(View.GONE);
            }
        }
    }

    public ObservableField<String> getLinkTitle() {
        return mLinkTitle;
    }

    public ObservableField<String> getLinkDescription() {
        return mLinkDescription;
    }

    public ObservableField<String> getLink() {
        return mLink;
    }

    public ObservableField<String> getLinkIcon() {
        return mLinkIcon;
    }

    public ObservableInt getLinkIconVisibility() {
        return mLinkIconVisibility;
    }

    public void onPreviewClicked() {
        WallStreetHelper.openCustomTab(mContext, mLink.get());
    }

    private void checkToEnableLongPressForOption() {
        if (!TextUtils.isEmpty(mComment.getContent())) {
            mShouldIgnoreLongPress.set(false);
        } else if (mComment.getMedia() != null && !mComment.getMedia().isEmpty()) {
            mShouldIgnoreLongPress.set(!TextUtils.equals(SharedPrefUtils.getUserId(mContext), mComment.getUser().getId()));
        } else {
            mShouldIgnoreLongPress.set(true);
        }
    }

    public ObservableBoolean getShouldIgnoreLongPress() {
        return mShouldIgnoreLongPress;
    }

    public void onReplyCommentClick() {
        if (!mComment.isPosting() && mOnCommentItemClickListener != null) {
            mOnCommentItemClickListener.onReplyButtonClick(mComment, mPosition);
        }
    }

    public void onLikeClick(View view) {
        if (!mComment.isPosting()) {
            AnimationHelper.animateBounce(view, null);
            if (mOnCommentItemClickListener != null) {
                mOnCommentItemClickListener.onLikeButtonClick(mComment,
                        mPosition,
                        new OnCallbackListener<Response<Object>>() {
                            @Override
                            public void onFail(ErrorThrowable ex) {

                            }

                            @Override
                            public void onComplete(Response<Object> result) {
                                long like;
                                if (mIsLike.get()) {
                                    like = mContentStat.getTotalLikes() - 1;
                                    mIsLike.set(false);
                                } else {
                                    like = mContentStat.getTotalLikes() + 1;
                                    mIsLike.set(true);
                                }
                                mContentStat.setTotalLikes(like);
                                mComment.setContentStat(mContentStat);
                                mComment.setLike(mIsLike.get());
                                if (mContentStat.getTotalLikes() > 0) {
                                    mIsShowLikeCounter.set(true);
                                    if (mContentStat.getTotalLikes() > 1) {
                                        mLikeCount.set(FormatNumber.format(mContentStat.getTotalLikes()));
                                    } else {
                                        mLikeCount.set(null);
                                    }
                                } else {
                                    mIsShowLikeCounter.set(false);
                                }
                            }
                        });
            }
        }
    }

    public void onUserProfileClick() {
        new NavigateSellerStoreHelper(mContext, mUser.get()).openSellerStore();
    }

    public Media getGifMedia() {
        return mComment.getMedia().get(0);
    }

    public String getViaGifText() {
        if (mComment.getMedia().get(0).getType() == MediaType.TENOR_GIF) {
            return mContext.getString(R.string.chat_tenor_gif);
        } else {
            return "";
        }
    }

    public int getImageCornerRadius() {
        return mContext.getResources().getDimensionPixelSize(R.dimen.space_small);
    }

    public OnClickListener onLongPress() {
        return () -> {
            if (!mShouldIgnoreLongPress.get()) {
                if (!mComment.isPosting() && mOnCommentItemClickListener != null) {
                    mOnCommentItemClickListener.onLongPress(mComment, mPosition);
                }
            }
        };
    }
}
