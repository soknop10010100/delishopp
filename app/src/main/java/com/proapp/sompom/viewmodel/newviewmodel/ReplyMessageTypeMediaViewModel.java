package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.utils.GenerateLinkPreviewUtil;

/**
 * Created by Chhom Veasna 11/17
 * /20.
 */
public class ReplyMessageTypeMediaViewModel extends AbsReplyMessageItemTypeViewModel {

    private final ObservableField<String> mMediaLink = new ObservableField<>();
    private final ObservableInt mPlayIconVisibility = new ObservableInt(View.GONE);

    public ReplyMessageTypeMediaViewModel(Context context,
                                          Chat chat,
                                          boolean isInInputReplyMode,
                                          boolean isMyChat,
                                          Chat mainChat) {
        super(context, chat, isInInputReplyMode, isMyChat, mainChat);
        bindContent();
    }

    public ObservableField<String> getMediaLink() {
        return mMediaLink;
    }

    public ObservableInt getPlayIconVisibility() {
        return mPlayIconVisibility;
    }

    @Override
    protected void bindContent() {
        super.bindContent();
        if (mReferencedChat.getType() == Chat.Type.IMAGE ||
                mReferencedChat.getType() == Chat.Type.VIDEO ||
                mReferencedChat.getType() == Chat.Type.GIF ||
                mReferencedChat.getType() == Chat.Type.MIX_MEDIA ||
                mReferencedChat.getType() == Chat.Type.TENOR_GIF) {
            Media media = getMediaFromChat();
            String type = mContext.getString(R.string.chat_reply_image_message);
            mPlayIconVisibility.set(View.GONE);
            mMediaLink.set(media.getThumbnail());
            if (TextUtils.isEmpty(mMediaLink.get())) {
                mMediaLink.set(media.getUrl());
            }
            if (media.getType() == MediaType.VIDEO) {
                mPlayIconVisibility.set(View.VISIBLE);
                type = mContext.getString(R.string.chat_reply_video_message);
            } else if (media.getType() == MediaType.TENOR_GIF) {
                type = mContext.getString(R.string.chat_reply_gif_message);
                mMediaLink.set(media.getUrl());
            }
            mSubTitleOne.set(new SpannableString(type));
        } else if (WallStreetHelper.isContainLink(mReferencedChat.getContent())) {
            String firstUrlIndex = GenerateLinkPreviewUtil.getFirstUrlIndex(mReferencedChat.getContent());
            mSubTitleOne.set(new SpannableString(firstUrlIndex));
            if (mReferencedChat.getLinkPreviewModel() != null) {
                mMediaLink.set(mReferencedChat.getLinkPreviewModel().getImage());
            }
        }
    }
}
