package com.proapp.sompom.viewmodel.binding;

import androidx.databinding.BindingAdapter;

import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.emun.FilterSortByItem;
import com.proapp.sompom.widget.sortby.SortByFlowLayout;

/**
 * Created by nuonveyo on 7/6/18.
 */

public final class SortByFlowLayoutBindingUtil {
    @BindingAdapter({"addItem", "sortPosition", "onItemClickListener"})
    public static void addItem(SortByFlowLayout filterSortByGroup,
                               FilterSortByItem[] items,
                               int sortPosition,
                               OnItemClickListener<FilterSortByItem> listener) {
        filterSortByGroup.removeAllViews();
        for (FilterSortByItem item : items) {
            filterSortByGroup.addView(item, sortPosition);
        }
        filterSortByGroup.setOnItemClickListener(listener);
    }
}
