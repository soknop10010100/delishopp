package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;
import android.text.TextUtils;

import androidx.databinding.ObservableField;

import com.google.gson.Gson;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.result.ConciergeOrderItem;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

/**
 * Created by Veasna Chhom on 20/9/21.
 */
public class ItemConciergeOrderHistoryDetailItemViewModel extends AbsBaseViewModel {

    private final ObservableField<String> mName = new ObservableField<>();
    private final ObservableField<String> mDescription = new ObservableField<>();
    private final ObservableField<String> mNote = new ObservableField<>();
    private final ObservableField<String> mItemPrice = new ObservableField<>();
    private final ObservableField<String> mItemUnit = new ObservableField<>();
    private ConciergeOrderItem mOrderItem;
    private ConciergeMenuItem mItem;
    private int mItemCounter;

    public ItemConciergeOrderHistoryDetailItemViewModel(Context context, ConciergeOrderItem orderItem) {
        mOrderItem = orderItem;
        mItem = ConciergeHelper.parseFromConciergeMenuOrderHistoryItem(new Gson(), orderItem);
        bindData(context);
    }

    public ObservableField<String> getName() {
        return mName;
    }

    public ObservableField<String> getDescription() {
        return mDescription;
    }

    public ObservableField<String> getNote() {
        return mNote;
    }

    public ObservableField<String> getItemPrice() {
        return mItemPrice;
    }

    public ObservableField<String> getItemUnit() {
        return mItemUnit;
    }

    private void bindData(Context context) {
        mName.set(mItem.getName());
        mDescription.set(mItem.getWeight());
        mItemPrice.set(ConciergeHelper.getDisplayPrice(context, mOrderItem.getTotal()));
        if (mOrderItem.getQuantity() > 0) {
            mItemCounter = mOrderItem.getQuantity();
            mItemUnit.set("x" + mItemCounter);
        }
    }
}
