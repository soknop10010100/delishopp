package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import com.proapp.sompom.R;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

/**
 * Created by Chhom Veasna on 5/8/2020.
 */
public class SearchResultScreenViewModel extends AbsLoadingViewModel {

    public SearchResultScreenViewModel(Context context) {
        super(context);
    }

    public void showNoDataScreen() {
        showNoItemError(getContext().getString(R.string.search_home_screen_empty_message));
        mIsShowButtonRetry.set(false);
    }
}
