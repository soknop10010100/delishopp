package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.proapp.sompom.model.result.TelegramChatRequest;
import com.proapp.sompom.model.result.User;

import java.util.Collections;
import java.util.List;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class ListItemTelegramChatRequestViewModel {
    private ObservableField<String> mGroupImageUrl = new ObservableField<>();
    private ObservableField<String> mName = new ObservableField<>();
    private ObservableField<String> mUserRequestName = new ObservableField<>();
    private ObservableField<List<User>> mParticipants = new ObservableField<>();

    private final TelegramChatRequest mTelegramChatRequest;
    private final Context mContext;
    private final Listener mOnRequestItemClick;

    public ListItemTelegramChatRequestViewModel(Context context,
                                                TelegramChatRequest telegramChatRequest,
                                                Listener requestItemClick) {
        mContext = context;
        mTelegramChatRequest = telegramChatRequest;
        mOnRequestItemClick = requestItemClick;
        bindRequest(telegramChatRequest);
    }


    public ObservableField<String> getGroupImageUrl() {
        return mGroupImageUrl;
    }

    public ObservableField<String> getName() {
        return mName;
    }

    public ObservableField<String> getUserRequestName() {
        return mUserRequestName;
    }

    public ObservableField<List<User>> getParticipants() {
        return mParticipants;
    }

    private void bindRequest(TelegramChatRequest chatRequest) {
        mName.set(chatRequest.getUser().getFirstName() + ' ' + chatRequest.getUser().getLastName());
        mParticipants.set(Collections.singletonList(chatRequest.getUser()));
    }

    public void onAcceptClick() {
        if (mOnRequestItemClick != null) {
            mOnRequestItemClick.onAcceptClick(mTelegramChatRequest.getId());
        }
    }

    public void onRejectClick() {
        if (mOnRequestItemClick != null) {
            mOnRequestItemClick.onRejectClick(mTelegramChatRequest.getId());
        }
    }

    public interface Listener {
        void onAcceptClick(String id);
        void onRejectClick(String id);
    }
}
