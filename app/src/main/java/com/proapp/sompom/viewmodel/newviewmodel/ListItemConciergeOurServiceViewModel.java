package com.proapp.sompom.viewmodel.newviewmodel;

import android.content.Context;

import androidx.databinding.ObservableField;

import com.proapp.sompom.intent.ConciergeShopDetailIntent;
import com.proapp.sompom.model.concierge.ConciergeOurServiceSectionResponse;
import com.proapp.sompom.newui.AbsBaseActivity;

/**
 * Created by Or Vitovongsak on 31/8/21.
 */

public class ListItemConciergeOurServiceViewModel {

    private final ObservableField<String> mTitle = new ObservableField<>();
    private final ObservableField<String> mLogo = new ObservableField<>();
    private final ConciergeOurServiceSectionResponse mItem;
    private final Context mContext;

    public ListItemConciergeOurServiceViewModel(Context context, ConciergeOurServiceSectionResponse item) {
        mContext = context;
        mItem = item;
        bindData();
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    public ObservableField<String> getLogo() {
        return mLogo;
    }

    private void bindData() {
        if (mItem.getData() != null) {
            mTitle.set(mItem.getData().getLabel());
            mLogo.set(mItem.getData().getIcon());
        }
    }

    public void onClick() {
        if (mContext instanceof AbsBaseActivity && mItem.getData() != null) {
            mContext.startActivity(new ConciergeShopDetailIntent(mContext, mItem.getData().getShopId()));
        }
    }
}
