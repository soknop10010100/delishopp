package com.proapp.sompom.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhom Veasna on 7/17/2020.
 */
public class WelcomeDataItemWrapper {

    @SerializedName("morning")
    private WelcomeDataItem mMorningData;

    @SerializedName("day")
    private WelcomeDataItem mDayData;

    @SerializedName("night")
    private WelcomeDataItem mNightData;

    public WelcomeDataItem getMorningData() {
        return mMorningData;
    }

    public void setMorningData(WelcomeDataItem morningData) {
        mMorningData = morningData;
    }

    public WelcomeDataItem getDayData() {
        return mDayData;
    }

    public void setDayData(WelcomeDataItem dayData) {
        mDayData = dayData;
    }

    public WelcomeDataItem getNightData() {
        return mNightData;
    }

    public void setNightData(WelcomeDataItem nightData) {
        mNightData = nightData;
    }
}
