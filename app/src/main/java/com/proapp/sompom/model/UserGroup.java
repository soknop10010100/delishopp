package com.proapp.sompom.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.listener.UserListAdaptive;
import com.proapp.sompom.model.notification.NotificationProfileDisplayAdaptive;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;

import java.util.List;

public class UserGroup implements UserListAdaptive, Parcelable, NotificationProfileDisplayAdaptive {

    public static final String GROUP_PICTURE = "picture";

    @SerializedName("_id")
    private String mId;
    @SerializedName("name")
    private String mName;
    @SerializedName(GROUP_PICTURE)
    private String mProfilePhoto;
    @SerializedName("participants")
    private List<User> mParticipants;

    public static UserGroup newInstance(Conversation conversation) {
        if (conversation == null) {
            return null;
        }

        UserGroup userGroup = new UserGroup();
        userGroup.setId(conversation.getId());
        userGroup.setName(conversation.getGroupName());
        userGroup.setProfilePhoto(conversation.getGroupImageUrl());
        userGroup.setParticipants(UserHelper.getCommonUserList(conversation.getParticipants()));

        return userGroup;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public List<User> getParticipants() {
        return mParticipants;
    }

    public void setParticipants(List<User> participants) {
        mParticipants = participants;
    }

    public String getProfilePhoto() {
        return mProfilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        mProfilePhoto = profilePhoto;
    }

    @Override
    public String getDisplayProfileUrl() {
        return getProfilePhoto();
    }

    @Override
    public String getDisplayFirstName() {
        return getName();
    }

    @Override
    public String getDisplayLastName() {
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String getDisplayFullName() {
        return getName();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mName);
        dest.writeString(this.mProfilePhoto);
        dest.writeTypedList(this.mParticipants);
    }

    public UserGroup() {
    }

    protected UserGroup(Parcel in) {
        this.mId = in.readString();
        this.mName = in.readString();
        this.mProfilePhoto = in.readString();
        this.mParticipants = in.createTypedArrayList(User.CREATOR);
    }

    public static final Creator<UserGroup> CREATOR = new Creator<UserGroup>() {
        @Override
        public UserGroup createFromParcel(Parcel source) {
            return new UserGroup(source);
        }

        @Override
        public UserGroup[] newArray(int size) {
            return new UserGroup[size];
        }
    };

    public boolean isValidGroup() {
        return !TextUtils.isEmpty(mId);
    }
}
