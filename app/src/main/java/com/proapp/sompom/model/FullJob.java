package com.proapp.sompom.model;

import com.google.gson.annotations.SerializedName;

public class FullJob {

    @SerializedName("_id")
    private String mId;

    @SerializedName("label")
    private String mLabel;

    public String getId() {
        return mId;
    }

    public String getLabel() {
        return mLabel;
    }
}
