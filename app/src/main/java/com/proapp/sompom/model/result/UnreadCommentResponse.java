package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;

public class UnreadCommentResponse {

    @SerializedName("unreadCount")
    private int mUnreadCommentCounter;

    public int getUnreadCommentCounter() {
        return mUnreadCommentCounter;
    }
}
