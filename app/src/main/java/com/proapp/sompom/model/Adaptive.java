package com.proapp.sompom.model;

import com.proapp.sompom.helper.IntentData;
import com.proapp.sompom.model.emun.ViewType;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.model.result.User;

import java.util.List;

/**
 * Created by imac on 8/11/17.
 */

public interface Adaptive extends MediaContainerAdaptive {

    String getId();

    ViewType getTimelineViewType();

    void startActivityForResult(IntentData requiredIntentData);

    default String getTitle() {
        return "";
    }

    default String getShareUrl() {
        return null;
    }

    default LinkPreviewModel getLinkPreviewModel() {
        return null;
    }

    default String getPostId() {
        return getId();
    }

    default boolean shouldShowLinkPreview() {
        return false;
    }

    default boolean shouldShowPlacePreview() {
        return false;
    }

    default void setUserViewWithAdaptive(List<User> userView) {
    }

    default void setUnreadCommentCounter(int counter) {
    }

    default boolean isWelcomePostType() {
        return false;
    }
}
