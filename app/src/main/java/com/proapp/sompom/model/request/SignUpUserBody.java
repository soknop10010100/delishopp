package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Veasna Chhom on 9/4/21.
 */

public class SignUpUserBody {

    @SerializedName("email")
    private String mEmail;

    @SerializedName("password")
    private String mPassword;

    @SerializedName("firstName")
    private String mFirstName;

    @SerializedName("lastName")
    private String mLastName;

    @SerializedName("countryCode")
    private String mCountryCode;

    @SerializedName("phone")
    private String mPhone;

    @SerializedName("visitorUserId")
    private String mVisitorUserId;

    public SignUpUserBody(String email, String password) {
        mEmail = email;
        mPassword = password;
    }

    public SignUpUserBody(String firstName, String lastName, String countryCode, String phone, String password) {
        mPassword = password;
        mFirstName = firstName;
        mLastName = lastName;
        mCountryCode = countryCode;
        mPhone = phone;
    }

    public SignUpUserBody(String firstName, String lastName, String email, String countryCode, String phone, String password) {
        mPassword = password;
        mFirstName = firstName;
        mLastName = lastName;
        mCountryCode = countryCode;
        mPhone = phone;
        mEmail = email;
    }

    public void setVisitorUserId(String visitorUserId) {
        mVisitorUserId = visitorUserId;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public void setCountryCode(String countryCode) {
        mCountryCode = countryCode;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }
}
