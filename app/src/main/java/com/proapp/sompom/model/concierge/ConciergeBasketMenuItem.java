package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.result.ConciergeMenuItemOption;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;

import java.util.List;

public class ConciergeBasketMenuItem extends SupportConciergeCustomErrorResponse {

    @SerializedName(AbsConciergeModel.FIELD_ID)
    protected String mId;

    @SerializedName(AbsConciergeModel.FIELD_ITEM_ID)
    private String mItemId;

    @SerializedName(AbsConciergeModel.FIELD_NAME)
    private String mName;

    @SerializedName(AbsConciergeModel.FIELD_QUANTITY)
    private int mQuantity;

    @SerializedName(AbsConciergeModel.FIELD_TYPE)
    private String mType;

    @SerializedName(AbsConciergeModel.FIELD_SHOP_HASH)
    private Integer mShopHash;

    @SerializedName(AbsConciergeModel.FIELD_HASH)
    private Integer mItemHash;

    @SerializedName(AbsConciergeModel.FIELD_ORIGINAL_PRICE)
    private Double mOriginalPrice;

    @SerializedName(AbsConciergeModel.FIELD_DEFAULT_PRICE)
    private float mPrice;

    @SerializedName(AbsConciergeModel.FIELD_OPTIONS)
    private List<ConciergeMenuItemOption> mOptions;

    @SerializedName(AbsConciergeModel.FIELD_CHOICES)
    private List<ConciergeOrderHistoryComboItem> mChoices;

    @SerializedName(AbsConciergeModel.FIELD_INSTRUCTION)
    private String mInstruction;

    @SerializedName(AbsConciergeModel.FIELD_SHOP)
    private ConciergeShop mShop;

    @SerializedName(AbsConciergeModel.FIELD_DESCRIPTION)
    private String mDescription;

    @SerializedName(AbsConciergeModel.FIELD_PICTURE)
    private String mPicture;

    @SerializedName(AbsConciergeModel.FIELD_IS_SAME_DAY)
    private boolean mIsSameDay;

    @SerializedName(AbsConciergeModel.FIELD_WEIGHT)
    private String mWeight;

    @SerializedName(AbsConciergeModel.FIELD_IS_IN_STOCK)
    private boolean mIsInStock;

    @SerializedName(AbsConciergeModel.FIELD_SUPPLIER_ID)
    private String mSupplierId;

    @SerializedName(AbsConciergeModel.FIELD_SUPPLIER)
    private ConciergeSupplier mSupplier;

    @SerializedName(AbsConciergeModel.FIELD_BRAND)
    private ConciergeBrand mConciergeBrand;

    @SerializedName(AbsConciergeModel.FIELD_IS_SHOP_OPEN)
    private boolean mIsShopOpen;

    @SerializedName(AbsConciergeModel.FIELD_AVAILABLE_TOMORROW)
    private boolean mIsGetTomorrow;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getItemId() {
        return mItemId;
    }

    public void setItemId(String itemId) {
        mItemId = itemId;
    }

    public ConciergeSupplier getSupplier() {
        return mSupplier;
    }

    public void setSupplier(ConciergeSupplier supplier) {
        mSupplier = supplier;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public String getWeight() {
        return mWeight;
    }

    public void setQuantity(int quantity) {
        mQuantity = quantity;
    }

    public ConciergeBrand getConciergeBrand() {
        return mConciergeBrand;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        mPrice = price;
    }

    public String getSupplierId() {
        return mSupplierId;
    }

    public List<ConciergeMenuItemOption> getOptions() {
        return mOptions;
    }

    public void setOptions(List<ConciergeMenuItemOption> options) {
        mOptions = options;
    }

    public List<ConciergeOrderHistoryComboItem> getChoices() {
        return mChoices;
    }

    public void setChoices(List<ConciergeOrderHistoryComboItem> choices) {
        mChoices = choices;
    }

    public String getInstruction() {
        return mInstruction;
    }

    public void setInstruction(String instruction) {
        mInstruction = instruction;
    }

    public ConciergeShop getShop() {
        return mShop;
    }

    public void setShop(ConciergeShop shop) {
        mShop = shop;
    }

    public boolean isSameDay() {
        return mIsSameDay;
    }

    public void setSameDay(boolean sameDay) {
        mIsSameDay = sameDay;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getPicture() {
        return mPicture;
    }

    public void setPicture(String picture) {
        mPicture = picture;
    }

    public boolean isGetTomorrow() {
        return mIsGetTomorrow;
    }

    public int getShopHash() {
        if (mShopHash != null) {
            return mShopHash;
        }

        return 0;
    }

    public void setShopHash(int shopHash) {
        mShopHash = shopHash;
    }

    public int getItemHash() {
        if (mItemHash != null) {
            return mItemHash;
        }

        return 0;
    }

    public void setItemHash(int itemHash) {
        mItemHash = itemHash;
    }
}