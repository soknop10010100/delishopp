package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.DifferentAdaptive;

import java.util.List;

/**
 * Created by Chhom Veasna on 5/3/22.
 */
public class AbsListConciergeProductSectionResponse extends AbsConciergeSectionResponse
        implements DifferentAdaptive<AbsListConciergeProductSectionResponse> {

    @SerializedName(FIELD_DATA)
    private List<ConciergeMenuItem> mData;

    public List<ConciergeMenuItem> getData() {
        return mData;
    }

    public void setData(List<ConciergeMenuItem> data) {
        this.mData = data;
    }

    @Override
    public boolean areItemsTheSame(AbsListConciergeProductSectionResponse other) {
        return getData().size() == other.getData().size();
    }

    @Override
    public boolean areContentsTheSame(AbsListConciergeProductSectionResponse other) {
        return areDataTheSame(other.getData());
    }

    private boolean areDataTheSame(List<ConciergeMenuItem> other) {
        if ((getData() == null && other == null) ||
                (getData().isEmpty() && other.isEmpty())) {
            return true;
        } else if (getData() != null && other != null
                && getData().size() == other.size()) {

            for (int index = 0; index < getData().size(); index++) {
                if (!getData().get(index).areContentsTheSame(other.get(index))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
