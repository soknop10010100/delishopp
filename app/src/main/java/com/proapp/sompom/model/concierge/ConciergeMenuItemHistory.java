package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.result.ConciergeMenuItemOption;

import java.util.List;

/**
 * Created by Veasna Chhom on 18/10/21.
 */

public class ConciergeMenuItemHistory extends AbsConciergeModel {

    @SerializedName(FIELD_NAME)
    private String mName;

    @SerializedName(FIELD_DESCRIPTION)
    private String mDescription;

    @SerializedName(FIELD_PICTURE)
    private String mPicture;

    @SerializedName(FIELD_TYPE)
    private String mType;

    @SerializedName(FIELD_DEFAULT_PRICE)
    private float mPrice;

    @SerializedName(FIELD_SHOP_ID)
    private String mShopId;

    @SerializedName(FIELD_OPTIONS)
    private List<ConciergeMenuItemOption> mOptions;

    @SerializedName(FIELD_CHOICES)
    private List<ConciergeOrderHistoryComboItem> mChoices;

    @SerializedName(ConciergeMenuItem.FIELD_SHOP)
    private ConciergeShop mShop;

    @SerializedName(FIELD_WEIGHT)
    private String mWeight;

    public ConciergeMenuItemHistory() {
    }

    public List<ConciergeMenuItemOption> getOptions() {
        return mOptions;
    }

    public List<ConciergeOrderHistoryComboItem> getChoices() {
        return mChoices;
    }

    public ConciergeShop getShop() {
        return mShop;
    }

    public void setShop(ConciergeShop shop) {
        mShop = shop;
    }
}
