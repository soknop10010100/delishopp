package com.proapp.sompom.model.notification;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.utils.ConciergeNotificationUtil;

import java.util.Date;

public class ConciergeNotification extends AbsConciergeModel implements NotificationAdaptive {

    @SerializedName(AbsConciergeModel.FIELD_USER_ID)
    private String mUserId;

    @SerializedName(AbsConciergeModel.FIELD_NEW)
    private boolean mIsNew;

    @SerializedName(AbsConciergeModel.FIELD_SLOT_DATE)
    private Date mDate;

    @SerializedName(AbsConciergeModel.FIELD_TYPE)
    private String mType;

    @SerializedName(AbsConciergeModel.FIELD_DATA)
    private ConciergeNotificationData mData;

    @Override
    public Spannable getNotificationText(Context context) {
        String title = ConciergeNotificationUtil.getOrderStatusNotificationTitle(context, mData.getOrder().getOrderNumber());
        String content = mData.getOrder().getDescription();
        String fullText = title + "\n" + content;
        Spannable spannable = new SpannableString(fullText);
        NotificationListAndRedirectionHelper.bold(context, spannable, fullText, title);

        return spannable;
    }

    @Override
    public boolean isNewNotification() {
        return mIsNew;
    }

    @Override
    public Date getPublished() {
        return mDate;
    }

    @Override
    public boolean isManagedType() {
        return true;
    }

    public ConciergeOrder getOrder() {
        return mData.getOrder();
    }

    public ConciergeNotificationData getData() {
        return mData;
    }
}
