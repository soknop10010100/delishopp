package com.proapp.sompom.model;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;

import java.util.List;

public class DefaultForwardResult {

    @SerializedName("suggested")
    private List<User> mSuggestedUserList;

    @SerializedName("people")
    private List<User> mPeople;

    @SerializedName("group")
    private List<Conversation> mGroupList;

    public List<User> getSuggestedUserList() {
        return mSuggestedUserList;
    }

    public List<User> getPeople() {
        return mPeople;
    }

    public List<User> getSearchResultUser() {
        return getPeople();
    }

    public List<Conversation> getGroupList() {
        return mGroupList;
    }

    public List<Conversation> getSearchResultGroup() {
        return mGroupList;
    }
}
