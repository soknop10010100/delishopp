package com.proapp.sompom.model.concierge;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.result.ConciergeOrder;

public class ABAPaymentPushBack {

    @SerializedName("type")
    private String mType;

    @SerializedName("message")
    private String mMessage;

    @SerializedName("result")
    private ConciergeOrder mOrder;

    public ConciergeOrder getOrder() {
        return mOrder;
    }

    public ABAPaymentPushBackType getType() {
        return ABAPaymentPushBackType.fromValue(mType);
    }

    public String getMessage() {
        return mMessage;
    }

    public enum ABAPaymentPushBackType {

        ORDER_PAYMENT_SUCCESS("orderPaymentSuccess"),
        ORDER_PAYMENT_FAIL("orderPaymentFail"),
        UNKNOWN("UNKNOWN");

        private String mType;

        ABAPaymentPushBackType(String type) {
            mType = type;
        }

        public static ABAPaymentPushBackType fromValue(String value) {
            for (ABAPaymentPushBackType type : ABAPaymentPushBackType.values()) {
                if (TextUtils.equals(type.mType, value)) {
                    return type;
                }
            }

            return UNKNOWN;
        }
    }
}
