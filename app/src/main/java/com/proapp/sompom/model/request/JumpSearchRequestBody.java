package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhom Veasna on 18/12/20.
 */
public class JumpSearchRequestBody {

    @SerializedName("searchMessageId")
    private String mSearchMessageId;

    @SerializedName("oldestLocalMessageId")
    private String mOldestLocalMessageId;

    public JumpSearchRequestBody(String searchMessageId, String oldestLocalMessageId) {
        mSearchMessageId = searchMessageId;
        mOldestLocalMessageId = oldestLocalMessageId;
    }
}
