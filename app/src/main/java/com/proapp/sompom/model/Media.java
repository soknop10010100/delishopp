package com.proapp.sompom.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.helper.IntentData;
import com.proapp.sompom.helper.upload.FileType;
import com.proapp.sompom.model.emun.GroupMediaDetailType;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.emun.PublishItem;
import com.proapp.sompom.model.emun.ViewType;
import com.proapp.sompom.model.result.ContentStat;
import com.proapp.sompom.model.result.User;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Media implements Adaptive, Parcelable, Comparable<Media>, RealmModel, WallStreetAdaptive,
        DifferentAdaptive<Media>, GroupMediaAdaptive {

    public static final Creator<Media> CREATOR = new Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel source) {
            return new Media(source);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };
    private static final String FIELD_ID = "id";
    private static final String FIELD_TITLE = "title";
    private static final String FIELD_THUMBNAIL = "thumbnail";
    private static final String FIELD_URL = "url";
    private static final String FIELD_TYPE = "type";
    private static final String FILED_INDEX = "index";
    private static final String FILED_WIDTH = "width";
    private static final String FILED_HEIGHT = "height";
    private static final String FILED_DURATION = "duration";
    private static final String FIELD_CONTENT_STAT = "contentStat";
    private static final String MEDIA_NAME = "mediaName";
    private static final String MEDIA_FORMAT = "format";
    private static final String MEDIA_SIZE = "size";
    private static final String OWNER = "sender";

    @PrimaryKey
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_TITLE)
    private String mTitle;
    @SerializedName(FIELD_URL)
    private String mUrl;
    @SerializedName(FIELD_TYPE)
    private String mType;
    @SerializedName(FILED_INDEX)
    private Integer mIndex;
    @SerializedName(FILED_WIDTH)
    private Double mWidth;
    @SerializedName(FILED_HEIGHT)
    private Double mHeight;
    @SerializedName(FIELD_THUMBNAIL)
    private String mThumbnail;
    @SerializedName(FILED_DURATION)
    private Double mDuration;
    @Ignore
    private Boolean mIsPause;
    @Ignore
    private Long mTime;
    @Ignore
    @SerializedName(FIELD_CONTENT_STAT)
    private ContentStat mContentStat;
    @SerializedName(MEDIA_NAME)
    private String mFileName;
    @SerializedName(MEDIA_FORMAT)
    private String mFormat;
    @SerializedName(MEDIA_SIZE)
    private long mSize;
    private String mDownloadedPath;
    private Integer mFileStatusType = FileStatusType.IDLE.ordinal();
    @Ignore
    @SerializedName(WallStreetAdaptive.FIELD_CREATE_DATE)
    private Date mCreatedAt;
    @Ignore
    @SerializedName(OWNER)
    private User mOwner;

    private transient Integer mLoadedGiftWidth;
    private transient Integer mLoadedGiftHeight;

    public Media() {
        setId(UUID.randomUUID().toString());
    }

    public Media(Media media) {
        this.mId = media.getId();
        this.mTitle = media.mTitle;
        this.mUrl = media.mUrl;
        this.mType = media.mType;
        this.mIndex = media.mIndex;
        this.mWidth = media.mWidth;
        this.mHeight = media.mHeight;
        this.mThumbnail = media.mThumbnail;
        this.mDuration = media.mDuration;
        this.mTime = media.mTime;
        this.mFormat = media.getFormat();
        this.mSize = media.getSize();
        this.mDownloadedPath = media.getDownloadedPath();
    }

    protected Media(Parcel in) {
        this.mId = in.readString();
        this.mTitle = in.readString();
        this.mUrl = in.readString();
        this.mType = in.readString();
        this.mIndex = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mWidth = (Double) in.readValue(Double.class.getClassLoader());
        this.mHeight = (Double) in.readValue(Double.class.getClassLoader());
        this.mThumbnail = in.readString();
        this.mDuration = (Double) in.readValue(Double.class.getClassLoader());
        this.mIsPause = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mTime = (Long) in.readValue(Long.class.getClassLoader());
        this.mContentStat = in.readParcelable(ContentStat.class.getClassLoader());
        this.mFileName = in.readString();
        this.mSize = in.readLong();
        this.mFormat = in.readString();
        this.mDownloadedPath = in.readString();
        this.mFileStatusType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mOwner = in.readParcelable(User.class.getClassLoader());
    }

    public void setOwner(User owner) {
        mOwner = owner;
    }

    public User getOwner() {
        return mOwner;
    }

    public FileStatusType getFileStatusType() {
        if (mFileStatusType != null) {
            return FileStatusType.fromValue(mFileStatusType);
        }

        return FileStatusType.IDLE;
    }

    public void setFileStatusType(FileStatusType fileStatusType) {
        mFileStatusType = fileStatusType.ordinal();
    }

    public Integer getLoadedGiftWidth() {
        return mLoadedGiftWidth;
    }

    public void setLoadedGiftWidth(Integer loadedGiftWidth) {
        mLoadedGiftWidth = loadedGiftWidth;
    }

    public Integer getLoadedGiftHeight() {
        return mLoadedGiftHeight;
    }

    public void setLoadedGiftHeight(Integer loadedGiftHeight) {
        mLoadedGiftHeight = loadedGiftHeight;
    }

    public void resetFileStatusType() {
        mFileStatusType = null;
    }

    public String getDownloadedPath() {
        return mDownloadedPath;
    }

    public void setDownloadedPath(String downloadedPath) {
        mDownloadedPath = downloadedPath;
    }

    public String getId() {
        return mId;
    }

    @Override
    public GroupMediaDetailType getMediaDetailType() {
        return GroupMediaDetailType.PHOTO_VIDEO;
    }

    @Override
    public void setId(String id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getFileName() {
        return mFileName;
    }

    public void setFileName(String fileName) {
        this.mFileName = fileName;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public int getIndex() {
        if (mIndex == null) {
            return 0;
        }
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    public int getWidth() {
        if (mWidth == null) {
            return 0;
        }

        return (int) mWidth.longValue();
    }

    public void setWidth(int width) {
        mWidth = (double) width;
    }

    public int getHeight() {
        if (mHeight == null) {
            return 0;
        }
        return (int) mHeight.longValue();
    }

    public void setHeight(int height) {
        mHeight = (double) height;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(String thumbnail) {
        mThumbnail = thumbnail;
    }

    public MediaType getType() {
        return MediaType.fromValue(mType);
    }

    public void setType(MediaType type) {
        mType = type.getValue();
    }

    public long getDuration() {
        if (mDuration == null) {
            return 0;
        }
        return mDuration.longValue();
    }

    public void setDuration(long duration) {
        mDuration = (double) duration;
    }

    @Override
    public ContentStat getContentStat() {
        if (mContentStat == null) {
            mContentStat = new ContentStat();
        }
        return mContentStat;
    }

    @Override
    public void setUnreadCommentCounter(int counter) {
        if (getContentStat() != null) {
            getContentStat().setUnreadCommentCount(counter);
        }
    }

    public void setContentStat(ContentStat contentStat) {
        mContentStat = contentStat;
    }

    @Override
    public void setUserViewWithAdaptive(List<User> userView) {
        setUserView(userView);
    }

    @Override
    public PublishItem getPublish() {
        return null;
    }

    @Override
    public void setPublish(int publish) {

    }

    @Override
    public double getLatitude() {
        return 0;
    }

    @Override
    public void setLatitude(double latitude) {

    }

    @Override
    public String getAddress() {
        return null;
    }

    @Override
    public void setAddress(String address) {

    }

    @Override
    public String getCity() {
        return null;
    }

    @Override
    public void setCity(String city) {

    }

    @Override
    public String getCountry() {
        return null;
    }

    @Override
    public void setCountry(String country) {

    }

    @Override
    public double getLongitude() {
        return 0;
    }

    @Override
    public void setLongitude(double longitude) {

    }

    @Override
    public User getUser() {
        return null;
    }

    @Override
    public List<Media> getMedia() {
        return Collections.emptyList();
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getShareUrl() {
        return null;
    }

    @Override
    public Date getCreateDate() {
        return mCreatedAt;
    }

    @Override
    public void setCreateDate(Date createDate) {

    }

    public FileType getUploadType() {
        if (mType.equals(MediaType.IMAGE.getValue())) {
            return FileType.IMAGE;
        } else if (mType.equals(MediaType.GIF.getValue())) {
            return FileType.GIF;
        } else if (mType.equals(MediaType.VIDEO.getValue())) {
            return FileType.VIDEO;
        } else if (mType.equals(MediaType.FILE.getValue())) {
            return FileType.FILE;
        } else {
            return FileType.LiveVideo;
        }
    }


    @Override
    public boolean equals(Object obj) {
        return obj instanceof Media && Objects.equals(((Media) obj).getId(), mId);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public String toString() {
        return "id = " + mId + ", title = " + mTitle + ", url = " + mUrl;
    }

    public boolean isPause() {
        if (mIsPause == null) {
            return false;
        }
        return mIsPause;
    }

    public void setPause(boolean pause) {
        mIsPause = pause;
    }

    public long getTime() {
        if (mTime == null) {
            return 0;
        }
        return mTime;
    }

    public void setTime(long time) {
        mTime = time;
    }

    public String getFormat() {
        return mFormat;
    }

    public void setFormat(String format) {
        mFormat = format;
    }

    public long getSize() {
        return mSize;
    }

    public void setSize(long size) {
        mSize = size;
    }

    @Override
    public ViewType getTimelineViewType() {
        return null;
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting activity
    }

    @Override
    public int compareTo(@NonNull Media o) {
        return o.getIndex();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mTitle);
        dest.writeString(this.mUrl);
        dest.writeString(this.mType);
        dest.writeValue(this.mIndex);
        dest.writeValue(this.mWidth);
        dest.writeValue(this.mHeight);
        dest.writeString(this.mThumbnail);
        dest.writeValue(this.mDuration);
        dest.writeValue(this.mIsPause);
        dest.writeValue(this.mTime);
        dest.writeParcelable(this.mContentStat, flags);
        dest.writeString(this.mFileName);
        dest.writeLong(this.mSize);
        dest.writeString(this.mFormat);
        dest.writeString(this.mDownloadedPath);
        dest.writeValue(this.mFileStatusType);
        dest.writeParcelable(this.mOwner, flags);
    }

    @Override
    public boolean shouldShowPlacePreview() {
        return false;
    }

    @Override
    public boolean shouldShowLinkPreview() {
        return false;
    }

    @Override
    public boolean areItemsTheSame(Media other) {
        return getId().matches(other.getId());
    }

    @Override
    public boolean areContentsTheSame(Media other) {
        return areItemsTheSame(other) &&
                TextUtils.equals(getFileName(), other.getFileName()) &&
                TextUtils.equals(getUrl(), other.getUrl()) &&
                TextUtils.equals(getThumbnail(), other.getThumbnail()) &&
                TextUtils.equals(getFormat(), other.getFormat()) &&
                getType() == other.getType();
    }

    public enum FileStatusType {
        IDLE,
        DOWNLOADING,
        DOWNLOADED,
        UPLOADING,
        UPLOADED,
        RETRY_UPLOAD;

        public static FileStatusType fromValue(int value) {
            for (FileStatusType fileStatusType : FileStatusType.values()) {
                if (fileStatusType.ordinal() == value) {
                    return fileStatusType;
                }
            }

            return IDLE;
        }
    }
}