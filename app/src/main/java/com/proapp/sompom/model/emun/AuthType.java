package com.proapp.sompom.model.emun;

import android.text.TextUtils;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public enum AuthType {

    LOGIN("login"),
    SIGN_UP("signUp"),
    RESET_PASSWORD_VIA_PHONE("resetPasswordViaPhone"),
    CHANGE_PHONE("changePhone"),
    NONE("None");

    private String mValue;

    AuthType(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public static AuthType fromValue(String value) {
        for (AuthType type : AuthType.values()) {
            if (TextUtils.equals(type.mValue, value)) {
                return type;
            }
        }

        return NONE;
    }
}
