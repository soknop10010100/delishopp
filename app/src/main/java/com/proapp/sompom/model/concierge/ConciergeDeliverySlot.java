package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class ConciergeDeliverySlot {
    public static final String FIELD_DATE = "date";
    public static final String FIELD_PREFERRED_SLOTS = "preferredSlots";

    @SerializedName(FIELD_DATE)
    private Date mDate;

    @SerializedName(FIELD_PREFERRED_SLOTS)
    private List<ConciergePreferredSlot> mPreferredSlot;

    public void setDate(Date date) {
        this.mDate = date;
    }

    public Date getDate() {
        return mDate;
    }

    public List<ConciergePreferredSlot> getPreferredSlot() {
        return mPreferredSlot;
    }

    public void setPreferredSlot(List<ConciergePreferredSlot> preferredSlots) {
        this.mPreferredSlot = preferredSlots;
    }
}
