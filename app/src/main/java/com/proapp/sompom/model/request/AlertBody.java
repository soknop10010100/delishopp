package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Or Vitovongsak on 18/8/20.
 */
public class AlertBody {

    @SerializedName("id")
    private String mId;

    public AlertBody() {}

    public AlertBody(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }
}
