package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

public class LikeRequestBody {

    @SerializedName("postId")
    private String mPostId;

    public LikeRequestBody(String postId) {
        mPostId = postId;
    }
}
