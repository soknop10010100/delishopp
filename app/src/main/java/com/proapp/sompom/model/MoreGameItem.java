package com.proapp.sompom.model;

import com.proapp.sompom.helper.IntentData;
import com.proapp.sompom.model.emun.ViewType;
import com.proapp.sompom.model.result.MoreGame;

import java.util.ArrayList;

/**
 * Created by He Rotha on 9/5/17.
 */

public class MoreGameItem extends ArrayList<MoreGame> implements Adaptive {
    @Override
    public String getId() {
        return null;
    }

    @Override
    public ViewType getTimelineViewType() {
        return ViewType.ViewPagerAd;
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting activity
    }
}
