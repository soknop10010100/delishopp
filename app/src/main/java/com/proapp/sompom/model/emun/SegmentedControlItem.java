package com.proapp.sompom.model.emun;

import androidx.annotation.StringRes;

import com.proapp.sompom.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by nuonveyo on 6/26/18.
 */

public enum SegmentedControlItem {
    All(1, R.string.conversation_screen_all_title, "all"),
    Message(2, R.string.conversation_screen_message_title, "private"),
    Buying(3, R.string.conversation_screen_group_title, "group"),
    //    Buying(3, R.string.message_buying_title, "buyer"); => Change to Group as above
    Selling(4, R.string.message_selling_title, "seller"), // Hide this feature for now
    Telegram(5, R.string.conversation_screen_telegram_title, "Telegram");

    private int mId;
    @StringRes
    private int mTitle;
    private String mValue;

    SegmentedControlItem(int id, int title, String value) {
        mId = id;
        mTitle = title;
        mValue = value;
    }

    public static SegmentedControlItem getSegmentedControlItem(int id) {
        for (SegmentedControlItem segmentedControlItem : SegmentedControlItem.values()) {
            if (segmentedControlItem.getId() == id) {
                return segmentedControlItem;
            }
        }
        return SegmentedControlItem.All;
    }

    public int getId() {
        return mId;
    }

    public int getTitle() {
        return mTitle;
    }

    public String getValue() {
        return mValue;
    }

    public static SegmentedControlItem[] getVisibleItems(boolean includeTelegram) {
        List<SegmentedControlItem> itemList = new ArrayList<>(Arrays.asList(SegmentedControlItem.All,
                SegmentedControlItem.Message,
                SegmentedControlItem.Buying));

        if (includeTelegram) {
            itemList.add(SegmentedControlItem.Telegram);
        }
        return itemList.toArray(new SegmentedControlItem[0]);
    }
}
