package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Chhom Veasna on 7/8/2020.
 */
public class AddOrRemoveGroupOwnerBody {

    @SerializedName("ownerList")
    private List<String> mOwnerListIds;

    @SerializedName("groupId")
    private String mGroupId;

    public AddOrRemoveGroupOwnerBody(String groupId, List<String> ownerListIds) {
        mOwnerListIds = ownerListIds;
        mGroupId = groupId;
    }

    public List<String> getOwnerListIds() {
        return mOwnerListIds;
    }

    public void setOwnerListIds(List<String> ownerListIds) {
        mOwnerListIds = ownerListIds;
    }

    public String getGroupId() {
        return mGroupId;
    }

    public void setGroupId(String groupId) {
        mGroupId = groupId;
    }
}
