package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.result.LoadMoreWrapper;

/**
 * Created Chhom Veasna on 5/4/22.
 */

public class LoadMoreConciergeSubCategoryWrapper extends LoadMoreWrapper<ConciergeMenuItem> {

    @SerializedName("subCategory")
    private ConciergeSubCategory mConciergeSubCategory;

    public ConciergeSubCategory getConciergeSubCategory() {
        return mConciergeSubCategory;
    }
}
