package com.proapp.sompom.model;

/**
 * Created by Chhom Veasna on 11/1/2019.
 */
public interface DifferentAdaptive<T> {

    boolean areItemsTheSame(T other);

    boolean areContentsTheSame(T other);
}
