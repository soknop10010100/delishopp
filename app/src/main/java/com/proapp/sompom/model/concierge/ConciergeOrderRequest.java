package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.model.AbsConciergeModel;

import java.util.List;

public class ConciergeOrderRequest {

    @SerializedName(AbsConciergeModel.FIELD_ID)
    private String mId;

    @SerializedName(AbsConciergeModel.FIELD_BASKET_MENU_ITEMS)
    private List<ConciergeRequestOrderMenuItem> mMenuItems;

    @SerializedName(AbsConciergeModel.FIELD_PREFERENCE_DELIVERY)
    private ConciergePreferenceDelivery mPreferenceDelivery;

    @SerializedName(AbsConciergeModel.FIELD_IS_PROVINCE)
    private boolean mIsProvince;

    @SerializedName(AbsConciergeModel.FIELD_DELIVERY_INSTRUCTION)
    private String mDeliveryInstruction;

    @SerializedName(AbsConciergeModel.FIELD_DELIVERY_ADDRESS)
    private ConciergeUserAddress mDeliveryAddress;

    @SerializedName(AbsConciergeModel.FIELD_RECEIPT_URL)
    private String mReceiptUrl;

    @SerializedName(AbsConciergeModel.FIELD_PAYMENT_METHOD)
    private String mPaymentMethod;

    @SerializedName(AbsConciergeModel.FIELD_PAYMENT_PROVIDER)
    private ConciergeOnlinePaymentProvider mPaymentProvider;

    @SerializedName(AbsConciergeModel.FIELD_SLOT_ID)
    private String mTimeSlotId;

    @SerializedName(AbsConciergeModel.FIELD_ADDRESS_ID)
    private String mAddressId;

    @SerializedName(AbsConciergeModel.FIELD_COUPON_ID)
    private String mCouponId;

    @SerializedName(AbsConciergeModel.FIELD_OOS_OPTION)
    private String mOOSOption;

    @SerializedName(AbsConciergeModel.FIELD_PAYMENT_OPTION)
    private String mPaymentOption;

    @SerializedName(AbsConciergeModel.FIELD_DELIVERY_FEE)
    private float mDeliveryFee;

    @SerializedName(AbsConciergeModel.FIELD_CONTAINER_FEE)
    private float mContainerFee;

    @SerializedName(AbsConciergeModel.FIELD_TOTAL_DISCOUNT)
    private float mTotalDiscount;

    @SerializedName(AbsConciergeModel.FIELD_IS_APPLY_WALLET)
    private boolean mIsAppliedWallet;

    @SerializedName(AbsConciergeModel.FIELD_WALLET_AMOUNT)
    private Double mWalletAmount;

    public void setId(String id) {
        mId = id;
    }

    public List<ConciergeRequestOrderMenuItem> getMenuItems() {
        return mMenuItems;
    }

    public void setMenuItems(List<ConciergeRequestOrderMenuItem> menuItems) {
        mMenuItems = menuItems;
    }

    public ConciergePreferenceDelivery getPreferenceDelivery() {
        return mPreferenceDelivery;
    }

    public void setPreferenceDelivery(ConciergePreferenceDelivery preferenceDelivery) {
        this.mPreferenceDelivery = preferenceDelivery;
    }

    public boolean isProvince() {
        return mIsProvince;
    }

    public void setIsProvince(boolean isProvince) {
        this.mIsProvince = isProvince;
    }

    public String getDeliveryInstruction() {
        return mDeliveryInstruction;
    }

    public void setDeliveryInstruction(String deliveryInstruction) {
        mDeliveryInstruction = deliveryInstruction;
    }

    public ConciergeUserAddress getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public void setDeliveryAddress(ConciergeUserAddress deliveryAddress) {
        mDeliveryAddress = deliveryAddress;
    }

    public void setReceiptUrl(String receiptUrl) {
        mReceiptUrl = receiptUrl;
    }

    public void setPaymentMethod(String paymentMethod) {
        mPaymentMethod = paymentMethod;
    }

    public void setPaymentProvider(ConciergeOnlinePaymentProvider paymentProvider) {
        mPaymentProvider = paymentProvider;
    }

    public void setTimeSlotId(String timeSlotId) {
        mTimeSlotId = timeSlotId;
    }

    public void setAddressId(String addressId) {
        mAddressId = addressId;
    }

    public void setCouponId(String couponId) {
        mCouponId = couponId;
    }

    public void setOOSOption(String OOSOption) {
        mOOSOption = OOSOption;
    }

    public void setPaymentOption(String paymentOption) {
        mPaymentOption = paymentOption;
    }

    public void setDeliveryFee(float deliveryFee) {
        mDeliveryFee = deliveryFee;
    }

    public void setContainerFee(float containerFee) {
        mContainerFee = containerFee;
    }

    public void setTotalDiscount(float totalDiscount) {
        mTotalDiscount = ConciergeCartHelper.roundPriceValue(totalDiscount, 2);
    }

    public void setAppliedWallet(boolean appliedWallet) {
        mIsAppliedWallet = appliedWallet;
    }

    public void setWalletAmount(Double walletAmount) {
        mWalletAmount = walletAmount;
    }
}
