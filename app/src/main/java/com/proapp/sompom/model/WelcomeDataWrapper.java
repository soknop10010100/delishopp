package com.proapp.sompom.model;

import com.google.gson.annotations.SerializedName;
import com.resourcemanager.helper.LocaleManager;

/**
 * Created by Chhom Veasna on 7/17/2020.
 */
public class WelcomeDataWrapper {

    @SerializedName(LocaleManager.ENGLISH_LANGUAGE_KEY)
    private WelcomeDataItemWrapper mEnglishData;

    @SerializedName(LocaleManager.FRENCH_LANGUAGE_KEY)
    private WelcomeDataItemWrapper mFrenchData;

    @SerializedName(LocaleManager.KHMER_LANGUAGE_KEY)
    private WelcomeDataItemWrapper mKhmerData;

    public WelcomeDataItemWrapper getEnglishData() {
        return mEnglishData;
    }

    public void setEnglishData(WelcomeDataItemWrapper englishData) {
        mEnglishData = englishData;
    }

    public WelcomeDataItemWrapper getFrenchData() {
        return mFrenchData;
    }

    public void setFrenchData(WelcomeDataItemWrapper frenchData) {
        mFrenchData = frenchData;
    }

    public WelcomeDataItemWrapper getKhmerData() {
        return mKhmerData;
    }

    public void setKhmerData(WelcomeDataItemWrapper khmerData) {
        mKhmerData = khmerData;
    }
}
