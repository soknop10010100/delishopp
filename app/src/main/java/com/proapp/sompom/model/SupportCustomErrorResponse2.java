package com.proapp.sompom.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class SupportCustomErrorResponse2 {

    @SerializedName("statusCode")
    private int mCode;

    @SerializedName("message")
    private String mMessage;

    public boolean isError() {
        return !TextUtils.isEmpty(mMessage);
    }

    public String getErrorMessage() {
        return mMessage;
    }

    public int getCode() {
        return mCode;
    }

    public void setMessage(String message) {
        mMessage = message;
    }
}
