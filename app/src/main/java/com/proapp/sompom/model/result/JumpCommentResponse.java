package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JumpCommentResponse {

    public static final String REDIRECTION_NEXT = "next";
    public static final String REDIRECTION_PREVIOUS = "prev";

    @SerializedName("comment")
    private Comment mMainComment;
    @SerializedName("list")
    private List<Comment> mCommentList;
    @SerializedName(REDIRECTION_NEXT)
    private String mNextRedirection;
    @SerializedName(REDIRECTION_PREVIOUS)
    private String mPreviousRedirection;

    public List<Comment> getCommentList() {
        return mCommentList;
    }

    public String getNextRedirection() {
        return mNextRedirection;
    }

    public String getPreviousRedirection() {
        return mPreviousRedirection;
    }

    public Comment getMainComment() {
        return mMainComment;
    }
}
