package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ChhomVeasna on 5/17/22.
 */

public class ResetPasswordRequestBody2 {

    @SerializedName("email")
    private String mEmail;

    @SerializedName("password")
    private String mPassword;

    @SerializedName("otp")
    private String mOpt;

    public ResetPasswordRequestBody2(String email, String password, String opt) {
        mEmail = email;
        mPassword = password;
        mOpt = opt;
    }
}
