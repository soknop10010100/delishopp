package com.proapp.sompom.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by Or Vitovongsak on 5/4/20.
 */
public class GroupMediaSection<D extends GroupMediaAdaptive> {

    @SerializedName("media")
    private List<D> mMediaList;

    @SerializedName("createdAt")
    private Date mCreatedAt;

    public List<D> getMediaList() {
        return mMediaList;
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public void setMediaList(List<D> mediaList) {
        mMediaList = mediaList;
    }

    public void setCreatedAt(Date createdAt) {
        mCreatedAt = createdAt;
    }
}
