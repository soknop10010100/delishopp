package com.proapp.sompom.model.emun;

import android.text.TextUtils;

/**
 * Created by Veasna Chhom on 23/11/21.
 */
public enum ConversationType {

    SUPPORT("support"),
    SUPPORT_TELEGRAM("support_telegram"),
    TELEGRAM("telegram"),
    NONE("None");

    private String mValue;

    ConversationType(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public static ConversationType fromValue(String value) {
        for (ConversationType type : ConversationType.values()) {
            if (TextUtils.equals(type.mValue, value)) {
                return type;
            }
        }

        return NONE;
    }
}
