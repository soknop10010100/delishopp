package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;

public class CheckWorkingHourResponse {

    @SerializedName("inWorking")
    private boolean mIsInWorkingHour;

    public boolean isInWorkingHour() {
        return mIsInWorkingHour;
    }
}
