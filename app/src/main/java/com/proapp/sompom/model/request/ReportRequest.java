package com.proapp.sompom.model.request;

/**
 * Created by he.rotha on 5/10/16.
 */
public class ReportRequest {
    //CHECKSTYLE:OFF
    private String reason;
    //CHECKSTYLE:OFF

    public ReportRequest(String reason) {
        this.reason = reason;
    }
}
