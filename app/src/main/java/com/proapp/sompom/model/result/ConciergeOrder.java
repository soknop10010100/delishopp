package com.proapp.sompom.model.result;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.DifferentAdaptive;
import com.proapp.sompom.model.ItemConciergeOrderHistoryAdaptive;
import com.proapp.sompom.model.concierge.BasketTimeSlot;
import com.proapp.sompom.model.concierge.ConciergeOnlinePaymentProvider;
import com.proapp.sompom.model.concierge.ConciergeShop;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.emun.ApplicationMode;
import com.proapp.sompom.model.emun.ConciergeOrderStatus;

import java.util.Date;
import java.util.List;

/**
 * Created by Veasna Chhom on 20/9/21.
 */

public class ConciergeOrder extends AbsConciergeModel implements ItemConciergeOrderHistoryAdaptive,
        Parcelable,
        DifferentAdaptive<ConciergeOrder> {

    @SerializedName("number")
    private String mOrderNumber;

    @SerializedName("organisationName")
    private String mOrganizationName;

    @SerializedName("conversationId")
    private String mConversationId;

    @SerializedName("statusString")
    private String mStatusString;

    @SerializedName("userName")
    private String mUserName;

    @SerializedName("organisationAddress")
    private String mOrganizationAddress;

    @SerializedName(FIELD_SUB_TOTAL)
    private double mSubTotal;

    @SerializedName(FIELD_DELIVERY_FEE)
    private double mDeliveryCharge;

    @SerializedName(FIELD_CONTAINER_FEE)
    private double mContainerFee;

    @SerializedName(FIELD_TOTAL_DISCOUNT)
    private Double mTotalDiscount;

    @SerializedName(FIELD_TOTAL)
    private double mGrandTotal;

    @SerializedName("deliveryDetail")
    private String mDeliveryDetail;

    @SerializedName(FIELD_CREATED_AT)
    private Date mCreatedAt;

    @SerializedName("deliveryTime")
    private Date mDeliveryTime;

    @SerializedName("pickupTime")
    private Date mPickupTime;

    @SerializedName("orderItems")
    private List<ConciergeOrderItem> mOrderItems;

    @SerializedName("status")
    private String mOrderStatus;

    @SerializedName(FIELD_TITLE)
    private String mOrderTitle;

    @SerializedName("subTitle")
    private String mOrderDescription;

    @SerializedName("shopLogo")
    private String mOrderProfile;

    @SerializedName(FIELD_DELIVERY_ADDRESS)
    private ConciergeUserAddress mDeliveryAddress;

    @SerializedName("trackingProgress")
    private List<ConciergeOrderTrackingProgress> mTrackingProgress;

    @SerializedName(FIELD_SHOPS)
    private List<ConciergeShop> mShops;

    @SerializedName(AbsConciergeModel.FIELD_PAYMENT_PROVIDER)
    private ConciergeOnlinePaymentProvider mPaymentProvider;

    @SerializedName(AbsConciergeModel.FIELD_PAYMENT_METHOD)
    private String mPaymentMethod;

    @SerializedName(AbsConciergeModel.FIELD_DRIVER_ID)
    private String mDriverId;

    @SerializedName(FIELD_WALLET_DISCOUNT)
    private Double mWalletDiscount;

    @SerializedName(FIELD_TYPE)
    private int mType;

    @SerializedName(FIELD_DESCRIPTION)
    private String mDescription;

    @SerializedName(AbsConciergeModel.FIELD_PREFERENCE_DELIVERY)
    private BasketTimeSlot mTimeSlot;

    public ConciergeOrder() {
    }

    protected ConciergeOrder(Parcel in) {
        mId = in.readString();
        mOrderNumber = in.readString();
        mOrganizationName = in.readString();
        mConversationId = in.readString();
        mStatusString = in.readString();
        mUserName = in.readString();
        mOrganizationAddress = in.readString();
        mSubTotal = in.readDouble();
        mDeliveryCharge = in.readDouble();
        mContainerFee = in.readDouble();
        mTotalDiscount = (Double) in.readSerializable();
        mGrandTotal = in.readDouble();
        mDeliveryDetail = in.readString();
        mPaymentMethod = in.readString();
        mOrderStatus = in.readString();
        mOrderTitle = in.readString();
        mOrderDescription = in.readString();
        mOrderProfile = in.readString();
        mDeliveryAddress = in.readParcelable(ConciergeUserAddress.class.getClassLoader());
        mTrackingProgress = in.createTypedArrayList(ConciergeOrderTrackingProgress.CREATOR);
        mCreatedAt = (Date) in.readSerializable();
        mShops = in.createTypedArrayList(ConciergeShop.CREATOR);
        mDriverId = in.readString();
        mWalletDiscount = (Double) in.readSerializable();
        mType = in.readInt();
        mDescription = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mOrderNumber);
        dest.writeString(mOrganizationName);
        dest.writeString(mConversationId);
        dest.writeString(mStatusString);
        dest.writeString(mUserName);
        dest.writeString(mOrganizationAddress);
        dest.writeDouble(mSubTotal);
        dest.writeDouble(mDeliveryCharge);
        dest.writeDouble(mContainerFee);
        dest.writeSerializable(mTotalDiscount);
        dest.writeDouble(mGrandTotal);
        dest.writeString(mDeliveryDetail);
        dest.writeString(mPaymentMethod);
        dest.writeString(mOrderStatus);
        dest.writeString(mOrderTitle);
        dest.writeString(mOrderDescription);
        dest.writeString(mOrderProfile);
        dest.writeParcelable(mDeliveryAddress, flags);
        dest.writeTypedList(mTrackingProgress);
        dest.writeSerializable(mCreatedAt);
        dest.writeTypedList(mShops);
        dest.writeString(mDriverId);
        dest.writeSerializable(mWalletDiscount);
        dest.writeInt(mType);
        dest.writeString(mDescription);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConciergeOrder> CREATOR = new Creator<ConciergeOrder>() {
        @Override
        public ConciergeOrder createFromParcel(Parcel in) {
            return new ConciergeOrder(in);
        }

        @Override
        public ConciergeOrder[] newArray(int size) {
            return new ConciergeOrder[size];
        }
    };

    public String getOrderNumber() {
        return mOrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        mOrderNumber = orderNumber;
    }

    public String getOrganizationName() {
        return mOrganizationName;
    }

    public void setOrganizationName(String organizationName) {
        mOrganizationName = organizationName;
    }

    public BasketTimeSlot getTimeSlot() {
        return mTimeSlot;
    }

    public String getDescription() {
        return mDescription;
    }

    public Double getWalletDiscount() {
        return mWalletDiscount;
    }

    public ConciergeOnlinePaymentProvider getPaymentProvider() {
        return mPaymentProvider;
    }

    public void setPaymentProvider(ConciergeOnlinePaymentProvider paymentProvider) {
        mPaymentProvider = paymentProvider;
    }

    public ApplicationMode getType() {
        return ApplicationMode.from(mType);
    }

    public void setDriverId(String driverId) {
        mDriverId = driverId;
    }

    public String getDriverId() {
        return mDriverId;
    }

    public void setPaymentMethod(String paymentMethod) {
        mPaymentMethod = paymentMethod;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getOrganizationAddress() {
        return mOrganizationAddress;
    }

    public void setOrganizationAddress(String organizationAddress) {
        mOrganizationAddress = organizationAddress;
    }

    public double getSubTotal() {
        return mSubTotal;
    }

    public void setSubTotal(double subTotal) {
        mSubTotal = subTotal;
    }

    public double getDeliveryCharge() {
        return mDeliveryCharge;
    }

    public void setDeliveryCharge(double deliveryCharge) {
        mDeliveryCharge = deliveryCharge;
    }

    public double getContainerFee() {
        return mContainerFee;
    }

    public void setContainerFee(double mcontainerfee) {
        mContainerFee = mcontainerfee;
    }

    public double getTotalDiscount() {
        if (mTotalDiscount != null) {
            return mTotalDiscount;
        }

        return 0;
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        mCreatedAt = createdAt;
    }

    public double getGrandTotal() {
        return mGrandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        mGrandTotal = grandTotal;
    }

    public String getDeliveryDetail() {
        return mDeliveryDetail;
    }

    public void setDeliveryDetail(String deliveryDetail) {
        mDeliveryDetail = deliveryDetail;
    }

    @Override
    public String getSupportConversationId() {
        return getConversationId();
    }

    public String getPaymentMethod() {
        return mPaymentMethod;
    }

    public Date getDeliveryTime() {
        return mDeliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        mDeliveryTime = deliveryTime;
    }

    public Date getPickupTime() {
        return mPickupTime;
    }

    public void setPickupTime(Date pickupTime) {
        mPickupTime = pickupTime;
    }

    public List<ConciergeOrderItem> getOrderItems() {
        return mOrderItems;
    }

    public void setOrderItems(List<ConciergeOrderItem> conciergeShops) {
        mOrderItems = conciergeShops;
    }

    public ConciergeOrderStatus getOrderStatus() {
        return ConciergeOrderStatus.fromValue(mOrderStatus);
    }

    public void setOrderStatus(ConciergeOrderStatus status) {
        mOrderStatus = status.getValue();
    }

    public String getOrderTitle() {
        return mOrderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        mOrderTitle = orderTitle;
    }

    public String getOrderDescription() {
        return mOrderDescription;
    }

    public void setOrderDescription(String orderDescription) {
        mOrderDescription = orderDescription;
    }

    public String getConversationId() {
        return mConversationId;
    }

    public void setConversationId(String conversationId) {
        mConversationId = conversationId;
    }

    public String getStatusString() {
        return mStatusString;
    }

    public void setStatusString(String statusString) {
        mStatusString = statusString;
    }

    public String getOrderProfile() {
        return mOrderProfile;
    }

    public void setOrderProfile(String orderProfile) {
        mOrderProfile = orderProfile;
    }

    public ConciergeUserAddress getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public void setDeliveryAddress(ConciergeUserAddress deliveryAddress) {
        mDeliveryAddress = deliveryAddress;
    }

    public List<ConciergeOrderTrackingProgress> getTrackingProgress() {
        return mTrackingProgress;
    }

    public void setTrackingProgress(List<ConciergeOrderTrackingProgress> trackingProgresses) {
        this.mTrackingProgress = trackingProgresses;
    }

    public List<ConciergeShop> getShops() {
        return mShops;
    }

    public void setShops(List<ConciergeShop> shops) {
        this.mShops = shops;
    }

    @Override
    public boolean areItemsTheSame(ConciergeOrder other) {
        return TextUtils.equals(getId(), other.getId());
    }

    @Override
    public boolean areContentsTheSame(ConciergeOrder other) {
        return TextUtils.equals(getOrderNumber(), other.getOrderNumber()) &&
                TextUtils.equals(getOrganizationName(), other.getOrganizationName()) &&
                TextUtils.equals(getUserName(), other.getUserName()) &&
                TextUtils.equals(getOrganizationAddress(), other.getOrganizationAddress()) &&
                getSubTotal() == other.getSubTotal() &&
                getDeliveryCharge() == other.getDeliveryCharge() &&
                getContainerFee() == other.getContainerFee() &&
                getTotalDiscount() == other.getTotalDiscount() &&
                getGrandTotal() == other.getGrandTotal() &&
                TextUtils.equals(getDeliveryDetail(), other.getDeliveryDetail()) &&
                TextUtils.equals(getOrderStatus().getValue(), other.getOrderStatus().getValue()) &&
                TextUtils.equals(getOrderTitle(), other.getOrderTitle()) &&
                TextUtils.equals(getOrderDescription(), other.getOrderDescription()) &&
                TextUtils.equals(getOrderProfile(), other.getOrderProfile());
    }

}
