package com.proapp.sompom.model.concierge;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConciergeSubCategoryLoadMoreDataModel extends ConciergeSupportItemAndSubCategoryAdaptive {

    private ConciergeSubCategory mConciergeSubCategory;
    private boolean mIsPerformingLoadMore;

    public ConciergeSubCategory getConciergeSubCategory() {
        return mConciergeSubCategory;
    }

    public void setConciergeSubCategory(ConciergeSubCategory conciergeSubCategory) {
        mConciergeSubCategory = conciergeSubCategory;
    }

    public void setPerformingLoadMore(boolean performingLoadMore) {
        mIsPerformingLoadMore = performingLoadMore;
    }

    public boolean isPerformingLoadMore() {
        return mIsPerformingLoadMore;
    }
}
