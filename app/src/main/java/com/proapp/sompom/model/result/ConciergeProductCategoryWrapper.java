package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeMenuSection;

import java.util.List;

public class ConciergeProductCategoryWrapper {

    @SerializedName("category")
    private ConciergeMenuSection mConciergeProductCategory;

    @SerializedName("items")
    private List<ConciergeMenuItem> mItems;

    public ConciergeMenuSection getProductCategory() {
        return mConciergeProductCategory;
    }

    public void setProductCategory(ConciergeMenuSection conciergeProductCategory) {
        mConciergeProductCategory = conciergeProductCategory;
    }

    public List<ConciergeMenuItem> getItems() {
        return mItems;
    }

    public void setItems(List<ConciergeMenuItem> items) {
        mItems = items;
    }
}
