package com.proapp.sompom.model;

import android.content.Context;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhom Veasna on 6/2/22.
 */

public class WalletUpdateReceivePushNotificationModel extends ReceivePushNotificationModel {

    @SerializedName("description")
    private JsonElement mDescription;

    @SerializedName("actualWallet")
    private Double mBalance;

    public Double getBalance() {
        return mBalance;
    }

    public String getDescription(Context context) {
        return NotificationOrder.getStatusString(context, mDescription);
    }
}
