package com.proapp.sompom.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhom Veasna on 4/10/2020.
 */
public class BasePreviewModel {
    public static final String TYPE = "type";
    public static final String SHOULD_PREVIEW = "shouldPreview";

    @SerializedName(TYPE)
    private String mType;
    @SerializedName(SHOULD_PREVIEW)
    private Boolean mShouldPreview;

    public BasePreviewModel() {
    }

    public BasePreviewModel(String type, Boolean shouldPreview) {
        mType = type;
        mShouldPreview = shouldPreview;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public Boolean getShouldPreview() {
        return mShouldPreview;
    }

    public void setShouldPreview(Boolean shouldPreview) {
        mShouldPreview = shouldPreview;
    }

    public static enum PreviewType {

        LINK("link"),
        FILE("file"),
        MAP("map"),
        UNDEFINED("undefined");

        private String mType;

        PreviewType(String type) {
            mType = type;
        }

        public String getType() {
            return mType;
        }

        public static PreviewType getFromValue(String type) {
            for (PreviewType value : PreviewType.values()) {
                if (TextUtils.equals(value.mType, type)) {
                    return value;
                }
            }

            return UNDEFINED;
        }
    }

    public static enum FilePreviewType {

        IMAGE("image"),
        VIDEO("video"),
        FILE("file"),
        UNDEFINED("undefined");

        private String mType;

        FilePreviewType(String type) {
            mType = type;
        }

        public String getType() {
            return mType;
        }

        public static FilePreviewType getFromValue(String type) {
            for (FilePreviewType value : FilePreviewType.values()) {
                if (TextUtils.equals(value.mType, type)) {
                    return value;
                }
            }

            return UNDEFINED;
        }
    }
}
