package com.proapp.sompom.model.emun;

import android.text.TextUtils;

/**
 * Created by Or Vitovongsak on 29/9/21.
 */
public enum ConciergeMenuItemOptionType {
    VARIATION("variation"),
    ADDON("addon");

    private final String mValue;

    ConciergeMenuItemOptionType(String type) {
        mValue = type;
    }

    public static ConciergeMenuItemOptionType from(String value) {
        for (ConciergeMenuItemOptionType deeplinkType : ConciergeMenuItemOptionType.values()) {
            if (TextUtils.equals(value, deeplinkType.mValue)) {
                return deeplinkType;
            }
        }
        return ADDON;
    }

    public String getValue() {
        return mValue;
    }
}
