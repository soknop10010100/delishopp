package com.proapp.sompom.model.concierge;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;

import java.util.List;

public class ConciergeBasket extends SupportConciergeCustomErrorResponse {

    @SerializedName(AbsConciergeModel.FIELD_ID)
    protected String mId;

    @SerializedName(AbsConciergeModel.FIELD_TYPE)
    protected String mType;

    @SerializedName(AbsConciergeModel.FIELD_TRANSACTION_ID)
    protected String mTransactionId;

    @SerializedName(AbsConciergeModel.FIELD_BASKET_MENU_ITEMS)
    private List<ConciergeBasketMenuItem> mMenuItems;

    @SerializedName(AbsConciergeModel.FIELD_IS_DISABLED)
    private boolean mIsDisabled;

    @SerializedName(AbsConciergeModel.FIELD_PAYMENT_FORM)
    private PaymentForm mPaymentForm;

    @SerializedName(AbsConciergeModel.FIELD_TOTAL)
    private double mTotal;

    @SerializedName(AbsConciergeModel.FIELD_SUB_TOTAL)
    private double mSubTotal;

    @SerializedName(AbsConciergeModel.FIELD_TOTAL_DISCOUNT)
    private double mTotalDiscount;

    @SerializedName(AbsConciergeModel.FIELD_DELIVERY_FEE)
    private double mDeliveryFee;

    @SerializedName(AbsConciergeModel.FIELD_CONTAINER_FEE)
    private double mContainerFee;

    private ConciergePreferenceDelivery mPreferenceDelivery;

    @SerializedName(AbsConciergeModel.FIELD_PREFERENCE_DELIVERY)
    private BasketTimeSlot mBasketTimeSlot;

    @SerializedName(AbsConciergeModel.FIELD_DELIVERY_INSTRUCTION)
    private String mDeliveryInstruction;

    @SerializedName(AbsConciergeModel.FIELD_DELIVERY_ADDRESS)
    private ConciergeUserAddress mDeliveryAddress;

    @SerializedName(AbsConciergeModel.FIELD_PAYMENT_METHOD)
    private String mPaymentMethod;

    @SerializedName(AbsConciergeModel.FIELD_PAYMENT_PROVIDER)
    private ConciergeOnlinePaymentProvider mPaymentProvider;

    @SerializedName(AbsConciergeModel.FIELD_RECEIPT_URL)
    private String mReceiptUrl;

    @SerializedName(AbsConciergeModel.FIELD_COUPON_ID)
    private String mCouponId;

    @SerializedName(AbsConciergeModel.FIELD_COUPON_CODE)
    private ConciergeCoupon mConciergeCoupon;

    @SerializedName(AbsConciergeModel.FIELD_OOS_OPTION)
    private String mOOSOption;

    @SerializedName(AbsConciergeModel.FIELD_IS_APPLY_WALLET)
    private boolean mIsAppliedWallet;

    public String getCouponId() {
        return mCouponId;
    }

    public void setCouponId(String couponId) {
        mCouponId = couponId;
    }

    public ConciergeCoupon getConciergeCoupon() {
        return mConciergeCoupon;
    }

    public void setConciergeCoupon(ConciergeCoupon conciergeCoupon) {
        mConciergeCoupon = conciergeCoupon;
    }

    public String getOOSOption() {
        return mOOSOption;
    }

    public void setOOSOption(String OOSOption) {
        mOOSOption = OOSOption;
    }

    public BasketTimeSlot getBasketTimeSlot() {
        return mBasketTimeSlot;
    }

    public void setBasketTimeSlot(BasketTimeSlot basketTimeSlot) {
        mBasketTimeSlot = basketTimeSlot;
    }

    public String getTransactionId() {
        return mTransactionId;
    }

    public void setTransactionId(String transactionId) {
        mTransactionId = transactionId;
    }

    public boolean isLockedWithPayment() {
        return !TextUtils.isEmpty(mTransactionId);
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }

    public List<ConciergeBasketMenuItem> getMenuItems() {
        return mMenuItems;
    }

    public void setMenuItems(List<ConciergeBasketMenuItem> menuItems) {
        mMenuItems = menuItems;
    }

    public PaymentForm getPaymentForm() {
        return mPaymentForm;
    }

    public boolean isDisabled() {
        return mIsDisabled;
    }

    public void setDisabled(boolean disabled) {
        mIsDisabled = disabled;
    }

    public void setPaymentForm(PaymentForm paymentForm) {
        mPaymentForm = paymentForm;
    }

    public double getTotal() {
        return mTotal;
    }

    public void setTotal(double total) {
        mTotal = total;
    }

    public double getSubTotal() {
        return mSubTotal;
    }

    public void setSubTotal(double subTotal) {
        mSubTotal = subTotal;
    }

    public double getTotalDiscount() {
        return mTotalDiscount;
    }

    public void setTotalDiscount(double totalDiscount) {
        mTotalDiscount = totalDiscount;
    }

    public double getDeliveryFee() {
        return mDeliveryFee;
    }

    public void setDeliveryFee(double deliveryFee) {
        mDeliveryFee = deliveryFee;
    }

    public double getContainerFee() {
        return mContainerFee;
    }

    public void setContainerFee(double containerFee) {
        mContainerFee = containerFee;
    }

    public ConciergePreferenceDelivery getPreferenceDelivery() {
        return mPreferenceDelivery;
    }

    public void setPreferenceDelivery(ConciergePreferenceDelivery preferenceDelivery) {
        mPreferenceDelivery = preferenceDelivery;
    }

    public String getDeliveryInstruction() {
        return mDeliveryInstruction;
    }

    public void setDeliveryInstruction(String deliveryInstruction) {
        mDeliveryInstruction = deliveryInstruction;
    }

    public ConciergeUserAddress getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public void setDeliveryAddress(ConciergeUserAddress deliveryAddress) {
        mDeliveryAddress = deliveryAddress;
    }

    public String getPaymentMethod() {
        return mPaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        mPaymentMethod = paymentMethod;
    }

    public ConciergeOnlinePaymentProvider getPaymentProvider() {
        return mPaymentProvider;
    }

    public void setPaymentProvider(ConciergeOnlinePaymentProvider paymentProvider) {
        mPaymentProvider = paymentProvider;
    }

    public boolean isBasketEmpty() {
        return mMenuItems == null || mMenuItems.isEmpty();
    }

    public String getReceiptUrl() {
        return mReceiptUrl;
    }

    public void setReceiptUrl(String receiptUrl) {
        this.mReceiptUrl = receiptUrl;
    }

    public boolean isAppliedWallet() {
        return mIsAppliedWallet;
    }
}
