package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.ConciergeCurrentLocationDisplayAdaptive;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;

/**
 * Created by Or Vitovongsak on 17/11/21.
 */
public class ConciergeUserAddress extends SupportConciergeCustomErrorResponse
        implements ConciergeCurrentLocationDisplayAdaptive, Parcelable {


    public static final String FIELD_USERNAME = "username";
    public static final String FIELD_COUNTRY_CODE = "countryCode";
    public static final String FIELD_PHONE = "phone";
    public static final String FIELD_LABEL = "label";
    public static final String FIELD_ADDRESS = "address";
    public static final String FIELD_NUMBER = "detailAddress";
    public static final String FIELD_PHOTO_URL = "photoUrl";

    //  For Delishop
    public static final String FIELD_IS_PRIMARY = "isPrimary";
    public static final String FIELD_CITY = "city";
    public static final String FIELD_FIRST_NAME = "firstName";
    public static final String FIELD_LAST_NAME = "lastName";
    public static final String FIELD_APARTMENT_NAME = "apartmentName";
    public static final String FIELD_HOUSE_NUMBER = "houseNumber";
    public static final String FIELD_STREET_DETAIL = "streetDetail";
    public static final String FIELD_LAND_MARK = "landmark";
    public static final String FIELD_AREA_DETAIL = "areaDetail";
    public static final String FIELD_DELIVERY_INSTRUCTION = "deliveryInstruction";
    public static final String FIELD_IS_IN_DELIVERY_RANGE = "isInRange";
    public static final String FIELD_DELIVERY_RANGE = "deliveryRange";

    @SerializedName(AbsConciergeModel.FIELD_ID)
    protected String mId;

    @SerializedName(FIELD_USERNAME)
    private String mUserName;

    @SerializedName(FIELD_COUNTRY_CODE)
    private String mCountryCode;

    @SerializedName(FIELD_PHONE)
    private String mPhone;

    @SerializedName(FIELD_LABEL)
    private String mLabel;

    @SerializedName(FIELD_ADDRESS)
    private String mAddress;

    @SerializedName(FIELD_NUMBER)
    private String mNumber;

    @SerializedName(AbsConciergeModel.FIELD_LATITUDE)
    private Double latitude;

    @SerializedName(AbsConciergeModel.FIELD_LONGITUDE)
    private Double longitude;

    @SerializedName(FIELD_PHOTO_URL)
    private String mPhotoUrl;

    //  For Delishop
    @SerializedName(FIELD_FIRST_NAME)
    private String mFirstName;

    @SerializedName(FIELD_LAST_NAME)
    private String mLastName;

    @SerializedName(FIELD_CITY)
    private String mCity;

    @SerializedName(FIELD_APARTMENT_NAME)
    private String mApartmentName;

    @SerializedName(FIELD_HOUSE_NUMBER)
    private String mHouseNumber;

    @SerializedName(FIELD_STREET_DETAIL)
    private String mStreetDetail;

    @SerializedName(FIELD_LAND_MARK)
    private String mLandmark;

    @SerializedName(FIELD_DELIVERY_INSTRUCTION)
    private String mDeliveryInstruction;

    @SerializedName(FIELD_AREA_DETAIL)
    private String mAreaDetail;

    @SerializedName(FIELD_IS_PRIMARY)
    private boolean mIsPrimary;

    @SerializedName(FIELD_IS_IN_DELIVERY_RANGE)
    private boolean mIsInDeliveryRange;

    private Boolean mHasUpdateProperty;

    @SerializedName(FIELD_DELIVERY_RANGE)
    private DeliveryRange mDeliveryRange;

    @SerializedName(AbsConciergeModel.FIELD_IS_PROVINCE)
    private boolean mIsProvinceAddress;

    public ConciergeUserAddress() {
    }

    protected ConciergeUserAddress(Parcel in) {
        mId = in.readString();
        mUserName = in.readString();
        mCountryCode = in.readString();
        mPhone = in.readString();
        mLabel = in.readString();
        mAddress = in.readString();
        mNumber = in.readString();
        latitude = (Double) in.readSerializable();
        longitude = (Double) in.readSerializable();
        mPhotoUrl = in.readString();

        //For Delishop
        mFirstName = in.readString();
        mLastName = in.readString();
        mCity = in.readString();
        mApartmentName = in.readString();
        mHouseNumber = in.readString();
        mStreetDetail = in.readString();
        mLandmark = in.readString();
        mDeliveryInstruction = in.readString();
        mAreaDetail = in.readString();
        mIsPrimary = in.readInt() > 0;
        mHasUpdateProperty = in.readInt() > 0;
        mIsInDeliveryRange = in.readInt() > 0;
        mDeliveryRange = in.readParcelable(DeliveryRange.class.getClassLoader());
        mIsProvinceAddress = in.readByte() != 0;
    }

    public DeliveryRange getDeliveryRange() {
        return mDeliveryRange;
    }

    public boolean isInDeliveryRange() {
        return mIsInDeliveryRange;
    }

    public void setInDeliveryRange(boolean inDeliveryRange) {
        mIsInDeliveryRange = inDeliveryRange;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public boolean getHasUpdateProperty() {
        return mHasUpdateProperty != null && mHasUpdateProperty;
    }

    public void setHasUpdateProperty(boolean hasUpdateProperty) {
        mHasUpdateProperty = hasUpdateProperty;
    }

    public static final Creator<ConciergeUserAddress> CREATOR = new Creator<ConciergeUserAddress>() {
        @Override
        public ConciergeUserAddress createFromParcel(Parcel in) {
            return new ConciergeUserAddress(in);
        }

        @Override
        public ConciergeUserAddress[] newArray(int size) {
            return new ConciergeUserAddress[size];
        }
    };

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String mUserName) {
        this.mUserName = mUserName;
    }

    public String getCountryCode() {
        return mCountryCode;
    }

    public void setCountryCode(String mCountryCode) {
        this.mCountryCode = mCountryCode;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public String getLabel() {
        return mLabel;
    }

    public void setLabel(String mLabel) {
        this.mLabel = mLabel;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;

    }

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String mNumber) {
        this.mNumber = mNumber;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getPhotoUrl() {
        return mPhotoUrl;
    }

    public void setPhotoUrl(String mPhotoUrl) {
        this.mPhotoUrl = mPhotoUrl;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public boolean isPrimary() {
        return mIsPrimary;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }


    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getApartmentName() {
        return mApartmentName;
    }

    public void setApartmentName(String apartmentName) {
        mApartmentName = apartmentName;
    }

    public String getHouseNumber() {
        return mHouseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        mHouseNumber = houseNumber;
    }

    public String getStreetDetail() {
        return mStreetDetail;
    }

    public void setStreetDetail(String streetDetail) {
        mStreetDetail = streetDetail;
    }

    public String getLandmark() {
        return mLandmark;
    }

    public void setLandmark(String landmark) {
        mLandmark = landmark;
    }

    public String getDeliveryInstruction() {
        return mDeliveryInstruction;
    }

    public void setDeliveryInstruction(String deliveryInstruction) {
        mDeliveryInstruction = deliveryInstruction;
    }

    public String getAreaDetail() {
        return mAreaDetail;
    }

    public void setAreaDetail(String areaDetail) {
        mAreaDetail = areaDetail;
    }

    public void setPrimary(boolean primary) {
        mIsPrimary = primary;
    }

    public boolean isProvinceAddress() {
        return mIsProvinceAddress;
    }

    public void setIsProvinceAddress(boolean isProvinceAddress) {
        mIsProvinceAddress = isProvinceAddress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mUserName);
        dest.writeString(mCountryCode);
        dest.writeString(mPhone);
        dest.writeString(mLabel);
        dest.writeString(mAddress);
        dest.writeString(mNumber);
        dest.writeSerializable(latitude);
        dest.writeSerializable(longitude);
        dest.writeString(mPhotoUrl);

        //For Delishop
        dest.writeString(mFirstName);
        dest.writeString(mLastName);
        dest.writeString(mCity);
        dest.writeString(mApartmentName);
        dest.writeString(mHouseNumber);
        dest.writeString(mStreetDetail);
        dest.writeString(mLandmark);
        dest.writeString(mDeliveryInstruction);
        dest.writeString(mAreaDetail);
        dest.writeInt(mIsPrimary ? 1 : 0);
        dest.writeInt((mHasUpdateProperty != null && mHasUpdateProperty) ? 1 : 0);
        dest.writeInt(mIsInDeliveryRange ? 1 : 0);
        dest.writeParcelable(mDeliveryRange, flags);
        dest.writeByte((byte) (mIsProvinceAddress ? 1 : 0));
    }
}
