package com.proapp.sompom.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class ExchangeAuthData implements Parcelable {

    private String mUserId;
    private String mAccessToken;
    private String mEmail;
    private String mCountyCode;
    private String mPhoneNumber;
    private String mFirstName;
    private String mLastName;
    private String mVerificationId;
    private String mPhoneResponseAuthType;
    private String mExchangeAuthType;
    private String mFirebaseToken;
    private boolean mIsIgnoreFirebaseTokenValidation;

    public ExchangeAuthData() {
    }

    protected ExchangeAuthData(Parcel in) {
        mUserId = in.readString();
        mAccessToken = in.readString();
        mEmail = in.readString();
        mCountyCode = in.readString();
        mPhoneNumber = in.readString();
        mFirstName = in.readString();
        mLastName = in.readString();
        mVerificationId = in.readString();
        mPhoneResponseAuthType = in.readString();
        mExchangeAuthType = in.readString();
        mFirebaseToken = in.readString();
        mIsIgnoreFirebaseTokenValidation = in.readInt() > 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mUserId);
        dest.writeString(mAccessToken);
        dest.writeString(mEmail);
        dest.writeString(mCountyCode);
        dest.writeString(mPhoneNumber);
        dest.writeString(mFirstName);
        dest.writeString(mLastName);
        dest.writeString(mVerificationId);
        dest.writeString(mPhoneResponseAuthType);
        dest.writeString(mExchangeAuthType);
        dest.writeString(mFirebaseToken);
        dest.writeInt(mIsIgnoreFirebaseTokenValidation ? 1 : 0);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ExchangeAuthData> CREATOR = new Creator<ExchangeAuthData>() {
        @Override
        public ExchangeAuthData createFromParcel(Parcel in) {
            return new ExchangeAuthData(in);
        }

        @Override
        public ExchangeAuthData[] newArray(int size) {
            return new ExchangeAuthData[size];
        }
    };

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public void setAccessToken(String accessToken) {
        mAccessToken = accessToken;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getCountyCode() {
        return mCountyCode;
    }

    public void setCountyCode(String countyCode) {
        mCountyCode = countyCode;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public boolean isIgnoreFirebaseTokenValidation() {
        return mIsIgnoreFirebaseTokenValidation;
    }

    public void setIgnoreFirebaseTokenValidation(boolean ignoreFirebaseTokenValidation) {
        mIsIgnoreFirebaseTokenValidation = ignoreFirebaseTokenValidation;
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getVerificationId() {
        return mVerificationId;
    }

    public void setVerificationId(String verificationId) {
        mVerificationId = verificationId;
    }

    public String getPhoneResponseAuthType() {
        return mPhoneResponseAuthType;
    }

    public void setPhoneResponseAuthType(String phoneResponseAuthType) {
        mPhoneResponseAuthType = phoneResponseAuthType;
    }

    public String getExchangeAuthType() {
        return mExchangeAuthType;
    }

    public void setExchangeAuthType(String exchangeAuthType) {
        mExchangeAuthType = exchangeAuthType;
    }

    public String getFirebaseToken() {
        return mFirebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        mFirebaseToken = firebaseToken;
    }

    public String getFullPhoneNumber() {
        String countryCode = mCountyCode;
        if (!TextUtils.isEmpty(countryCode)) {
            countryCode = "+" + countryCode;
            return countryCode + mPhoneNumber;
        } else {
            return mPhoneNumber;
        }
    }
}
