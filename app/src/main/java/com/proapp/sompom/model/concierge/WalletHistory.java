package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class WalletHistory {

    @SerializedName("_id")
    private String mId;

    @SerializedName("createdAt")
    String mCreatedDateString;

    private Date mCreatedAt;

    @SerializedName("adminInfo")
    private String mInfo;

    @SerializedName("orderNumber")
    private String mOrderNumber;

    @SerializedName("amount")
    private double mAmount;

    @SerializedName("transactionType")
    private String mTransactionType;

    @SerializedName("comment")
    private String mComment;

    public String getId() {
        return mId;
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public String getInfo() {
        return mInfo;
    }

    public String getOrderNumber() {
        return mOrderNumber;
    }

    public double getAmount() {
        return mAmount;
    }

    public String getTransactionType() {
        return mTransactionType;
    }

    public String getComment() {
        return mComment;
    }

    public void setCreatedAt(Date createdAt) {
        mCreatedAt = createdAt;
    }

    public String getCreatedDateString() {
        return mCreatedDateString;
    }
}
