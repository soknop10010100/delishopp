package com.proapp.sompom.model.emun;

import android.text.TextUtils;

/**
 * Created by Veasna Chhom on 9/27/21.
 */
public enum ConciergeMainSectionType {

    BUTTON("button"),
    TREND_SHOP("trendShop"),
    TREND_ITEM("trendItem"),
    CAROUSEL("carousel"),
    PUSH_ITEM("pushItem"),
    NEW_ARRIVAL("newItem"),
    BRAND("brand"),
    SUPPLIER("supplier"),
    NONE("none");

    private final String mValue;

    ConciergeMainSectionType(String type) {
        mValue = type;
    }

    public static ConciergeMainSectionType from(String value) {
        for (ConciergeMainSectionType deeplinkType : ConciergeMainSectionType.values()) {
            if (TextUtils.equals(value, deeplinkType.mValue)) {
                return deeplinkType;
            }
        }
        return NONE;
    }

    public String getValue() {
        return mValue;
    }
}
