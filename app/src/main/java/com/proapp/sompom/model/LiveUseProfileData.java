package com.proapp.sompom.model;

import com.proapp.sompom.model.emun.Presence;
import com.proapp.sompom.model.result.User;

import java.util.Date;

public class LiveUseProfileData {

    private Presence mPresence;
    private Date mActivityDate;
    private int mUserHash;
    private String mUserProfileAvatar;

    public Presence getPresence() {
        return mPresence;
    }

    public void setPresence(Presence presence) {
        mPresence = presence;
    }

    public Date getActivityDate() {
        return mActivityDate;
    }

    public void setActivityDate(Date activityDate) {
        mActivityDate = activityDate;
    }

    public void updateUserHash(User userHash) {
        mUserHash = userHash.getHash();
        mUserProfileAvatar = userHash.getUserProfileThumbnail();
    }

    public int getUserHash() {
        return mUserHash;
    }

    public String getUserProfileAvatar() {
        return mUserProfileAvatar;
    }
}
