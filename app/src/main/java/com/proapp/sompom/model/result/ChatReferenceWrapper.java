package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class ChatReferenceWrapper extends RealmObject {

    @SerializedName("message")
    private ReferencedChat mReferencedChat;

    public ChatReferenceWrapper() {
    }

    public ChatReferenceWrapper(ReferencedChat referencedChat) {
        mReferencedChat = referencedChat;
    }

    public ReferencedChat getReferencedChat() {
        return mReferencedChat;
    }

    public void setReferencedChat(ReferencedChat referencedChat) {
        mReferencedChat = referencedChat;
    }
}
