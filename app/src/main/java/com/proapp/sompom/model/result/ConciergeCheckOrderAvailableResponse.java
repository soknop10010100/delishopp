package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;

public class ConciergeCheckOrderAvailableResponse {

    @SerializedName(AbsConciergeModel.FIELD_ORDER_AVAILABLE)
    private boolean mIsOrderAvailable;

    public boolean isOrderAvailable() {
        return mIsOrderAvailable;
    }
}
