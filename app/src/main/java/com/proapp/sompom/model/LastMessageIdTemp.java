package com.proapp.sompom.model;

import java.util.Date;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class LastMessageIdTemp implements RealmModel {

    public static final String FILED_ID = "mConversationId";
    public static final String FILED_LAST_MESSAGE_ID = "mLastMessageId";
    public static final String FILED_LAST_MESSAGE_DATE = "mLastMessageDate";

    @PrimaryKey
    private String mConversationId;
    private String mLastMessageId;
    private Date mLastMessageDate;
    private Boolean mShouldRequestHistoryFromServer;

    public String getConversationId() {
        return mConversationId;
    }

    public void setConversationId(String conversationId) {
        mConversationId = conversationId;
    }

    public String getLastMessageId() {
        return mLastMessageId;
    }

    public void setLastMessageId(String lastMessageId) {
        mLastMessageId = lastMessageId;
    }

    public Date getLastMessageDate() {
        return mLastMessageDate;
    }

    public void setLastMessageDate(Date lastMessageDate) {
        mLastMessageDate = lastMessageDate;
    }

    public Boolean getShouldRequestHistoryFromServer() {
        return mShouldRequestHistoryFromServer != null && mShouldRequestHistoryFromServer;
    }

    public void setShouldRequestHistoryFromServer(boolean shouldRequestHistoryFromServer) {
        mShouldRequestHistoryFromServer = shouldRequestHistoryFromServer;
    }
}