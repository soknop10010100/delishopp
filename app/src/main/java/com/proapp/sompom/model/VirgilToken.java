package com.proapp.sompom.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Or Vitovongsak on 10/7/20.
 */

public class VirgilToken {

    @SerializedName("virgilToken")
    String mVirgilToken;

    public String getVirgilToken() {
        return mVirgilToken;
    }

    public void setVirgilToken(String mVirgilToken) {
        this.mVirgilToken = mVirgilToken;
    }
}
