package com.proapp.sompom.model.emun;

import android.text.TextUtils;

/**
 * Created by Veasna Chhom on 8/4/21.
 */
public enum NotificationProviderType {

    ONESIGNAL("ONESIGNAL"),
    PUSHY("PUSHY");

    private String mValue;

    NotificationProviderType(String value) {
        mValue = value;
    }

    public static NotificationProviderType fromValue(String value) {
        for (NotificationProviderType type : NotificationProviderType.values()) {
            if (TextUtils.equals(type.mValue, value)) {
                return type;
            }
        }

        return ONESIGNAL;
    }
}
