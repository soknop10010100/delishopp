package com.proapp.sompom.model.notification;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;

import androidx.appcompat.app.AppCompatActivity;

import com.desmond.squarecamera.ui.dialog.MessageDialog;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.proapp.sompom.R;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.intent.ConciergeOrderHistoryDetailIntent;
import com.proapp.sompom.intent.newintent.ReplyCommentShellIntent;
import com.proapp.sompom.intent.newintent.TimelineDetailIntent;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.CustomTypefaceSpan;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.resourcemanager.helper.FontHelper;

import java.util.List;

public final class NotificationListAndRedirectionHelper {

    public static final int ANIMATION_DELAY = 200;
    public static final int SCROLL_DELAY = 300;

    //NotificationVerb
    public static final String FIELD_ID = "_id";
    public static final String FIELD_POST_ID = "post";
    public static final String FIELD_MEDIA_ID = "media";
    public static final String FIELD_COMMENT_ID = "comment";
    public static final String FIELD_SUB_COMMENT_ID = "subComment";
    public static final String FIELD_DATE = "date";
    public static final String FIELD_CREATED_AT = "createdAt";
    public static final String FIELD_ACTOR = "actor";
    public static final String FIELD_VERB = "verb";
    public static final String FIELD_OBJECT = "object";
    public static final String FIELD_IS_NEW = "new";
    public static final String FIELD_CONVERSATION_ID = "conversationId";
    public static final String FIELD_CONVERSATION_NAME = "groupName";
    public static final String FIELD_ORDER_ID = "order";
    public static final String FIELD_SHOP = "shop";
    public static final String NOTIFICATION_EXCHANGE_DATA = "NOTIFICATION_EXCHANGE_DATA";

    public static Spannable getNotificationText(Context context,
                                                Notification notification,
                                                String subjectText,
                                                String objectText) {
        String value = String.format("%s %s", subjectText, objectText);
        Spannable spannable = new SpannableString(value);

        if (!notification.getShops().isEmpty()) {
            bold(context, spannable, value, notification.getShops().get(0).getName());
        } else {
            String[] list = getHighLightName(notification.getActors());
            for (String s : list) {
                bold(context, spannable, value, s);
            }
        }
        return spannable;
    }

    public static String getSubjectText(Context context, List<NotificationActor> users) {
        String myUserId = SharedPrefUtils.getUserId(context);
        if (users.size() == 1) {
            return getUserName(myUserId, users.get(0));
        } else if (users.size() == 2) {
            String user1 = getUserName(myUserId, users.get(0));
            String user2 = getUserName(myUserId, users.get(1));
            return String.format("%s and %s", user1, user2);
        } else if (users.size() == 3) {
            String user1 = getUserName(myUserId, users.get(0));
            String user2 = getUserName(myUserId, users.get(1));
            String user3 = getUserName(myUserId, users.get(2));
            return String.format("%s, %s and %s", user1, user2, user3);
        } else {
            String user1 = getUserName(myUserId, users.get(0));
            String user2 = getUserName(myUserId, users.get(1));
            String other = String.format("%d others", users.size() - 2);
            return String.format("%s, %s and %s", user1, user2, other);
        }
    }

    private static String getUserName(String myUserId, NotificationActor user) {
        if (TextUtils.equals(myUserId, user.getId())) {
            return "You";
        } else {
            return user.getFullName();
        }
    }

    private static String[] getHighLightName(List<NotificationActor> userTemps) {
        if (userTemps.size() > 3) {
            String[] name = new String[3];

            for (int i = 0; i < 3; i++) {
                if (i == 2) {
                    name[i] = String.format("%d others", userTemps.size() - 2);
                } else {
                    name[i] = userTemps.get(i).getFullName();
                }
            }
            return name;
        } else {
            String[] name = new String[userTemps.size()];
            for (int i = 0; i < userTemps.size(); i++) {
                name[i] = userTemps.get(i).getFullName();
            }
            return name;
        }
    }

    public static void bold(Context context,
                            Spannable span,
                            String fullText,
                            String boldText) {

        int startIndex = fullText.indexOf(boldText);
        while (startIndex != -1) {
            int endIndex = startIndex + boldText.length();
            Typeface typeface = FontHelper.getBoldFontStyle(context);
            span.setSpan(new CustomTypefaceSpan("", typeface),
                    startIndex,
                    endIndex,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            span.setSpan(new ForegroundColorSpan(AttributeConverter.convertAttrToColor(context,
                            R.attr.notification_user_name)),
                    startIndex,
                    endIndex,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            startIndex = fullText.indexOf(boldText, startIndex + boldText.length());
        }
    }

    public static TimelineDetailIntent getCreateNewPostIntent(Context context, String postId) {
        RedirectionNotificationData data = new RedirectionNotificationData(NotificationVerb.VERB_POST,
                new NotificationObject(postId, null, null, null));
        return new TimelineDetailIntent(context, data);
    }

    public static String getNotificationActionText(Context context,
                                                   String verb,
                                                   NotificationObject object) {
        //Post Verbs
        if (TextUtils.equals(verb, NotificationVerb.VERB_POST)) {
            return context.getString(R.string.notification_create_new_post).toLowerCase();
        } else if (TextUtils.equals(verb, NotificationVerb.MENTION_POST)) {
            return context.getString(R.string.notification_mention_on_post).toLowerCase();
        } else if (TextUtils.equals(verb, NotificationVerb.LIKE_POST)) {
            return context.getString(R.string.notification_like_post).toLowerCase();
        } else if (TextUtils.equals(verb, NotificationVerb.LIKE_IMAGE)) {
            return context.getString(R.string.notification_like_image).toLowerCase();
        } else if (TextUtils.equals(verb, NotificationVerb.LIKE_VIDEO)) {
            return context.getString(R.string.notification_like_video).toLowerCase();
        } else if (TextUtils.equals(verb, NotificationVerb.LIKE_MEDIA)) {
            return context.getString(R.string.notification_like_media).toLowerCase();
        }
        //Comment Verbs
        else if (TextUtils.equals(verb, NotificationVerb.COMMENT_POST)) {
            return context.getString(R.string.notification_comment_on_post).toLowerCase();
        } else if (TextUtils.equals(verb, NotificationVerb.COMMENT_IMAGE)) {
            return context.getString(R.string.notification_comment_on_image).toLowerCase();
        } else if (TextUtils.equals(verb, NotificationVerb.COMMENT_VIDEO)) {
            return context.getString(R.string.notification_comment_on_video).toLowerCase();
        } else if (TextUtils.equals(verb, NotificationVerb.COMMENT_MEDIA)) {
            return context.getString(R.string.notification_comment_on_media).toLowerCase();
        } else if (TextUtils.equals(verb, NotificationVerb.MENTION_IN_COMMENT)) {
            return context.getString(R.string.notification_mention_on_comment).toLowerCase();
        } else if (TextUtils.equals(verb, NotificationVerb.LIKE_COMMENT)) {
            return context.getString(R.string.notification_like_comment).toLowerCase();
        }
        //Sub-comment Verbs
        else if (TextUtils.equals(verb, NotificationVerb.REPLY_SUB_COMMENT)) {
            return context.getString(R.string.notification_reply_on_comment).toLowerCase();
        } else if (TextUtils.equals(verb, NotificationVerb.LIKE_SUB_COMMENT)) {
            return context.getString(R.string.notification_like_reply_comment).toLowerCase();
        } else if (TextUtils.equals(verb, NotificationVerb.MENTION_SUB_COMMENT)) {
            return context.getString(R.string.notification_mention_on_reply_comment).toLowerCase();
        }

        //Group Creation
        else if (TextUtils.equals(verb, NotificationVerb.VERB_CREATE_GROUP)) {
            return context.getString(R.string.notification_create_new_group,
                    object.getConversionName()).toLowerCase();
        }

        //Default one
        else {
            return "";
        }
    }

    public static Intent getRedirectionIntent(Context context, NotificationAdaptive data) {
        if (data.shouldMakeNotificationRedirection(context)) {
            if (TextUtils.equals(data.getVerb(), NotificationVerb.VERB_CREATE_GROUP)) {
                if (data.getFirstObject() != null) {
                    Conversation conversationById = ConversationDb.getConversationById(context,
                            data.getFirstObject().getConversionId());
                    if (conversationById != null) {
                        return new ChatIntent(context, conversationById);
                    }
                }
                return null;
            } else if (!TextUtils.isEmpty(data.getNotificationExchangeData().getSubCommentId())) {
                return new ReplyCommentShellIntent(context, data.getNotificationExchangeData());
            } else if (TextUtils.equals(data.getVerb(), NotificationVerb.ORDER_PROCESSING) ||
                    TextUtils.equals(data.getVerb(), NotificationVerb.ORDER_DELIVERING)) {
                if (data.getFirstObject() != null) {
                    return new ConciergeOrderHistoryDetailIntent(context,
                            data.getFirstObject().getOrderId());
                }
                return null;
            } else {
                return new TimelineDetailIntent(context, data.getNotificationExchangeData());
            }
        }

        return null;
    }

    /**
     * Valid notification item data must include: verb, date, object, and actor as main factors.
     */
    public static boolean isValidNotificationData(JsonObject object) {
        if (object != null) {
            if (!object.has(NotificationListAndRedirectionHelper.FIELD_VERB)) {
                return false;
            }
            if (!object.has(NotificationListAndRedirectionHelper.FIELD_DATE)) {
                return false;
            }

            JsonElement actors = object.get(NotificationListAndRedirectionHelper.FIELD_ACTOR);
            JsonElement objects = object.get(NotificationListAndRedirectionHelper.FIELD_OBJECT);

            return !((actors == null || actors.getAsJsonArray().size() == 0) ||
                    (objects == null || objects.getAsJsonArray().size() == 0));
        }

        return false;
    }

    public static void showRedirectionNotificationContentNotFound(AppCompatActivity context) {
        MessageDialog dialog = new MessageDialog();
        dialog.setTitle(context.getString(R.string.popup_error_notification_redirection_not_found_title));
        dialog.setMessage(context.getString(R.string.popup_error_notification_redirection_not_found_message));
        dialog.setLeftText(context.getString(R.string.popup_ok_button), null);
        dialog.show(context.getSupportFragmentManager(), MessageDialog.TAG);
    }
}
