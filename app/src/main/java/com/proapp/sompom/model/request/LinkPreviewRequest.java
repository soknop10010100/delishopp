package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhom Veasna on 1/15/2020.
 */
public class LinkPreviewRequest {

    @SerializedName("url")
    private String mUrl;

    public LinkPreviewRequest(String url) {
        mUrl = url;
    }
}
