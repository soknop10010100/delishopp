package com.proapp.sompom.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserBadge implements Parcelable {

    @SerializedName("badgeAroundMe")
    private Long mNearBy;
    @SerializedName("badgeMessages")
    private Long mUnreadMessage;
    @SerializedName("badgeNotifications")
    private Long mUnreadNotification;
    @SerializedName("badgeShop")
    private Long mConciergeOrderBadge;

    public Long getNearBy() {
        if (mNearBy == null) {
            return 0L;
        }
        return mNearBy;
    }

    public void setNearBy(Long nearBy) {
        mNearBy = nearBy;
    }

    public Long getUnreadMessage() {
        if (mUnreadMessage == null) {
            return 0L;
        }
        return mUnreadMessage;
    }

    public void setUnreadMessage(Long unreadMessage) {
        mUnreadMessage = unreadMessage;
    }

    public Long getUnreadNotification() {
        if (mUnreadNotification == null) {
            return 0L;
        }
        return mUnreadNotification;
    }

    public void setUnreadNotification(Long unreadNotification) {
        mUnreadNotification = unreadNotification;
    }

    public Long getConciergeOrderBadge() {
        if (mConciergeOrderBadge == null) {
            return 0L;
        }
        return mConciergeOrderBadge;
    }

    public void setConciergeOrder(Long conciergeOrder) {
        mConciergeOrderBadge = conciergeOrder;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.mNearBy);
        dest.writeValue(this.mUnreadMessage);
        dest.writeValue(this.mUnreadNotification);
        dest.writeValue(this.mConciergeOrderBadge);
    }

    public UserBadge() {
    }

    protected UserBadge(Parcel in) {
        this.mNearBy = (Long) in.readValue(Long.class.getClassLoader());
        this.mUnreadMessage = (Long) in.readValue(Long.class.getClassLoader());
        this.mUnreadNotification = (Long) in.readValue(Long.class.getClassLoader());
        this.mConciergeOrderBadge = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Creator<UserBadge> CREATOR = new Creator<UserBadge>() {
        @Override
        public UserBadge createFromParcel(Parcel source) {
            return new UserBadge(source);
        }

        @Override
        public UserBadge[] newArray(int size) {
            return new UserBadge[size];
        }
    };
}
