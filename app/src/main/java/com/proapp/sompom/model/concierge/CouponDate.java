package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;

import java.io.Serializable;

public class CouponDate implements Serializable {

    @SerializedName("couponCode")
    @Expose
    private String couponCode;
    @SerializedName("couponCodeId")
    @Expose
    private String couponCodeId;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("discountPrice")
    @Expose
    private double discountPrice;
    @SerializedName("subTotal")
    @Expose
    private double subTotal;
    @SerializedName("finalPrice")
    @Expose
    private double finalPrice;
    @SerializedName("delivery_info")
    @Expose
    private DeliveryInfo deliveryInfo;

    public String getCouponCodeId() {
        return couponCodeId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public String getMessage() {
        return message;
    }

    public double getDiscountPrice() {
        return discountPrice;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public DeliveryInfo getDeliveryInfo() {
        return deliveryInfo;
    }

    public class DeliveryInfo implements Serializable {

        @SerializedName("deliveryFee")
        @Expose
        private double deliveryFee;
        @SerializedName("totalKm")
        @Expose
        private double totalKm;

        public double getDeliveryFee() {
            return deliveryFee;
        }

        public double getTotalKm() {
            return totalKm;
        }
    }
}
