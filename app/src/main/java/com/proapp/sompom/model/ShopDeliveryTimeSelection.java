package com.proapp.sompom.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.proapp.sompom.model.emun.ShopDeliveryType;

import java.time.LocalDateTime;

public class ShopDeliveryTimeSelection implements Parcelable {

    private LocalDateTime mStartTime;
    private LocalDateTime mEndTime;
    private LocalDateTime mSelectedDate;
    private boolean mIsASAP;
    private String mDeliveryType;
    private String mTimeSlotId;
    private boolean mIsProvince;

    public ShopDeliveryTimeSelection() {
    }

    public LocalDateTime getStartTime() {
        return mStartTime;
    }

    public void setStartTime(LocalDateTime start) {
        this.mStartTime = start;
    }

    public LocalDateTime getEndTime() {
        return mEndTime;
    }

    public void setEndTime(LocalDateTime end) {
        this.mEndTime = end;
    }

    public LocalDateTime getSelectedDate() {
        return mSelectedDate;
    }

    public void setSelectedDate(LocalDateTime selectedDate) {
        this.mSelectedDate = selectedDate;
    }

    public String getTimeSlotId() {
        return mTimeSlotId;
    }

    public void setTimeSlotId(String timeSlotId) {
        mTimeSlotId = timeSlotId;
    }

    public boolean isASAP() {
        return mIsASAP;
    }

    public void setIsASAP(boolean isASAP) {
        mIsASAP = isASAP;
    }

    public ShopDeliveryType getDeliveryType() {
        return ShopDeliveryType.fromValue(mDeliveryType);
    }

    public void setDeliveryType(ShopDeliveryType deliveryType) {
        mDeliveryType = deliveryType.getValue();
    }

    public boolean isProvince() {
        return mIsProvince;
    }

    public void setIsProvince(boolean isProvince) {
        mIsProvince = isProvince;
    }

    protected ShopDeliveryTimeSelection(Parcel in) {
        mDeliveryType = in.readString();
        mStartTime = (LocalDateTime) in.readSerializable();
        mEndTime = (LocalDateTime) in.readSerializable();
        mSelectedDate = (LocalDateTime) in.readSerializable();
        mIsASAP = in.readByte() != 0;
        mTimeSlotId = in.readString();
        mIsProvince = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDeliveryType);
        dest.writeSerializable(mStartTime);
        dest.writeSerializable(mEndTime);
        dest.writeSerializable(mSelectedDate);
        dest.writeByte((byte) (mIsASAP ? 1 : 0));
        dest.writeString(mTimeSlotId);
        dest.writeByte((byte) (mIsProvince ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ShopDeliveryTimeSelection> CREATOR = new Creator<ShopDeliveryTimeSelection>() {
        @Override
        public ShopDeliveryTimeSelection createFromParcel(Parcel in) {
            return new ShopDeliveryTimeSelection(in);
        }

        @Override
        public ShopDeliveryTimeSelection[] newArray(int size) {
            return new ShopDeliveryTimeSelection[size];
        }
    };

    @Override
    public String toString() {
        return "ShopDeliveryTimeSelection{" +
                "mStartTime=" + mStartTime +
                ", mEndTime=" + mEndTime +
                ", mSelectedDate=" + mSelectedDate +
                ", mIsASAP=" + mIsASAP +
                ", mDeliveryType='" + mDeliveryType + '\'' +
                ", mTimeSlotId='" + mTimeSlotId + '\'' +
                '}';
    }
}
