package com.proapp.sompom.model;

import androidx.annotation.StringRes;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.NotificationChannelHelper;


/**
 * Created by He Rotha on 3/28/18.
 */
public enum NotificationChannelSetting {

    Home(NotificationChannelHelper.CHANNEL_TYPE_FEED, R.string.feed),
    Chat(NotificationChannelHelper.CHANNEL_TYPE_CHAT, R.string.chat_notification_channel_name),
    FeatureSyn(NotificationChannelHelper.CHANNEL_TYPE_FEATURE_SYNCHRONIZATION, R.string.syn_data),
    Call(NotificationChannelHelper.CHANNEL_TYPE_CALL_INFORMATION, R.string.call_notification),
    IncomingCall(NotificationChannelHelper.CHANNEL_TYPE_INCOMING_CALL, R.string.incoming_call_notification_channel),
    Silent(NotificationChannelHelper.CHANNEL_TYPE_SILENT, R.string.silent_notification_channel),
    Vibrate(NotificationChannelHelper.CHANNEL_TYPE_VIBRATION, R.string.vibrate_notification_channel),
    OrderStatusUpdate(NotificationChannelHelper.CHANNEL_TYPE_ORDER_STATUS_UPDATE, R.string.order_status_update_notification_channel);

    private final String mBaseChannelId;
    @StringRes
    private final int mNotificationName;

    NotificationChannelSetting(String baseChannelId, @StringRes int notificationName) {
        mBaseChannelId = baseChannelId;
        mNotificationName = notificationName;
    }

    public String getLatestNotificationChannelId() {
        return NotificationChannelHelper.getLatestChannelId(mBaseChannelId);
    }

    public String getBaseNotificationChannelId() {
        return mBaseChannelId;
    }

    @StringRes
    public int getNotificationName() {
        return mNotificationName;
    }
}
