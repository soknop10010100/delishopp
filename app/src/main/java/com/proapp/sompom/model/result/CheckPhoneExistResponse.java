package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.SupportCustomErrorResponse2;

public class CheckPhoneExistResponse extends SupportCustomErrorResponse2 {

    @SerializedName("existed")
    private boolean mIsExited;

    public boolean isExited() {
        return mIsExited;
    }
}
