package com.proapp.sompom.model;

import android.text.TextUtils;

import com.proapp.sompom.helper.IntentData;
import com.proapp.sompom.model.emun.ViewType;
import com.proapp.sompom.model.result.LinkPreviewModel;

import java.util.List;

/**
 * Created by Chhom Veasna on 7/17/2020.
 */
public class WelcomeItem implements Adaptive {

    private String mWelcomeTitle;
    private String mWelcomeDescription;
    private String mUserName;

    public WelcomeItem() {
    }

    public void setWelcomeTitle(String welcomeTitle) {
        mWelcomeTitle = welcomeTitle;
    }

    public void setWelcomeDescription(String welcomeDescription) {
        mWelcomeDescription = welcomeDescription;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getWelcomeTitle() {
        return mWelcomeTitle;
    }

    public String getWelcomeDescription() {
        return mWelcomeDescription;
    }

    public String getUserName() {
        return mUserName;
    }

    @Override
    public String getId() {
        return "8fd58922-c803-11ea-87d0-0242ac130003"; //Unique id for all.
    }

    @Override
    public ViewType getTimelineViewType() {
        return ViewType.GREETING_ITEM;
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {

    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getShareUrl() {
        return null;
    }

    @Override
    public LinkPreviewModel getLinkPreviewModel() {
        return null;
    }

    @Override
    public List<Media> getMedia() {
        return null;
    }

    @Override
    public String getPostId() {
        return null;
    }

    @Override
    public boolean shouldShowLinkPreview() {
        return false;
    }

    @Override
    public boolean shouldShowPlacePreview() {
        return false;
    }

    public boolean areItemsTheSame(WelcomeItem other) {
        return true;
    }

    public boolean areContentsTheSame(WelcomeItem other) {
        return TextUtils.equals(getWelcomeTitle(), other.getWelcomeTitle()) &&
                TextUtils.equals(getWelcomeDescription(), other.getWelcomeDescription());
    }
}
