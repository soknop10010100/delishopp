package com.proapp.sompom.model;

import androidx.annotation.Nullable;
import androidx.core.util.Pair;

//import com.google.android.gms.ads.formats.NativeCustomTemplateAd;

/**
 * Created by He Rotha on 3/23/18.
 */
//public class PairAds extends Pair<AdsItem, NativeCustomTemplateAd> {
public class PairAds extends Pair<AdsItem, String> {

    /**
     * Constructor for a Pair.
     *
     * @param first  the first object in the Pair
     * @param second the second object in the pair
     */
//    public PairAds(@Nullable AdsItem first, @Nullable NativeCustomTemplateAd second) {
//        super(first, second);
//    }
    public PairAds(@Nullable AdsItem first, @Nullable String second) {
        super(first, second);
    }
}
