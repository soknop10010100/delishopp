package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.ShopDeliveryTime;

import java.util.List;

public class ConciergeSlot {

    @SerializedName("date")
    private String mDate;

    @SerializedName("list")
    private List<ShopDeliveryTime> mShopDeliveryTimes;

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public List<ShopDeliveryTime> getShopDeliveryTimes() {
        return mShopDeliveryTimes;
    }

    public void setShopDeliveryTimes(List<ShopDeliveryTime> shopDeliveryTimes) {
        mShopDeliveryTimes = shopDeliveryTimes;
    }
}
