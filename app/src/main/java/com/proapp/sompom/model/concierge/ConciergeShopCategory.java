package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;

public class ConciergeShopCategory extends AbsConciergeModel implements ConciergeShopDetailDisplayAdaptive {

    @SerializedName(FIELD_LABEL)
    private String mTitle;

    @SerializedName(FIELD_LOGO)
    private String mLogo;

    private boolean mIsAllCategoryType;
    private boolean mIsSelected;

    public ConciergeShopCategory() {
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    private ConciergeShopCategory(String id, String title, boolean isAllCategoryType) {
        setId(id);
        mTitle = title;
        mIsAllCategoryType = isAllCategoryType;
    }

    public String getLogo() {
        return mLogo;
    }

    public boolean isAllCategoryType() {
        return mIsAllCategoryType;
    }

    public void setSelected(boolean selected) {
        mIsSelected = selected;
    }

    public static ConciergeShopCategory newAllCategory(String title) {
        return new ConciergeShopCategory("ALL", title, true);
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    @Override
    public String toString() {
        return "ConciergeShopCategory{" +
                "mTitle='" + mTitle + '\'' +
                ", mIsAllCategoryType=" + mIsAllCategoryType +
                ", mIsSelected=" + mIsSelected +
                '}';
    }
}
