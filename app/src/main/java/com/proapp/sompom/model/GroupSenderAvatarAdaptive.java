package com.proapp.sompom.model;

public interface GroupSenderAvatarAdaptive {
    boolean shouldDisplaySenderAvatar();
}
