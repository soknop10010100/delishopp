package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.ShopDeliveryTime;

import java.util.List;

public class BasketTimeSlot {

    @SerializedName("slots")
    private List<ShopDeliveryTime> mDeliveryTimes;

    public List<ShopDeliveryTime> getDeliveryTimes() {
        return mDeliveryTimes;
    }

    public void setDeliveryTimes(List<ShopDeliveryTime> deliveryTimes) {
        mDeliveryTimes = deliveryTimes;
    }
}
