package com.proapp.sompom.model;

import android.content.Intent;

import com.proapp.sompom.helper.IntentData;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.intent.CreateTimelineIntent;
import com.proapp.sompom.intent.newintent.MyCameraIntent;
import com.proapp.sompom.intent.newintent.MyCameraResultIntent;
import com.proapp.sompom.model.emun.CreateWallStreetScreenType;
import com.proapp.sompom.model.emun.ViewType;
import com.proapp.sompom.model.result.MoreGame;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.sompom.baseactivity.ResultCallback;

import java.util.ArrayList;

/**
 * Created by He Rotha on 9/5/17.
 */

public class CreateTimelineItem extends ArrayList<MoreGame> implements Adaptive {

    private static final String STATIC_ID = "926fb0a0-dcdf-4ddd-9837-f7ac44926433"; //Unique id

    private boolean mEnableBottomPostSeparator;

    @Override
    public String getId() {
        return STATIC_ID;
    }

    @Override
    public ViewType getTimelineViewType() {
        return ViewType.NewPost;
    }

    public boolean isEnableBottomPostSeparator() {
        return mEnableBottomPostSeparator;
    }

    public void setEnableBottomPostSeparator(boolean enableBottomPostSeparator) {
        mEnableBottomPostSeparator = enableBottomPostSeparator;
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        AbsBaseActivity context = requiredIntentData.getActivity();
        if (context == null) {
            return;
        }

        context.startLoginWizardActivity(new LoginActivityResultCallback() {
            @Override
            public void onActivityResultSuccess(boolean isAlreadyLogin) {
                if (requiredIntentData.getAction() == IntentData.Action.CREATE_TIMELINE_ITEM) {
                    context.startActivityForResult(new CreateTimelineIntent(context, CreateWallStreetScreenType.CREATE_POST),
                            new ResultCallback() {
                                @Override
                                public void onActivityResultSuccess(int resultCode, Intent data) {
                                    requiredIntentData.onDataResultCallBack(data.getParcelableExtra(CreateTimelineIntent.DATA),
                                            false);
                                }
                            });
                } else if (requiredIntentData.getAction() == IntentData.Action.CREATE_TIMELINE_CAMERA) {
                    Intent cameraIntent = MyCameraIntent.newTimelineInstance(context, null);
                    context.startActivityForResult(cameraIntent, new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            MyCameraResultIntent cameraIntent1 = new MyCameraResultIntent(data);
                            final LifeStream lifeStream = new LifeStream();
                            lifeStream.setMedia(cameraIntent1.getMedia());
                            context.startActivityForResult(new CreateTimelineIntent(context,
                                            lifeStream,
                                            CreateWallStreetScreenType.CREATE_POST),
                                    new ResultCallback() {
                                        @Override
                                        public void onActivityResultSuccess(int resultCode, Intent data) {
                                            requiredIntentData.onDataResultCallBack(data.getParcelableExtra(CreateTimelineIntent.DATA),
                                                    false);
                                        }
                                    });
                        }
                    });
                }
            }
        });
    }
}
