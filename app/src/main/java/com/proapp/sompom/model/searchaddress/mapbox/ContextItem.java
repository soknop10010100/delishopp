package com.proapp.sompom.model.searchaddress.mapbox;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class ContextItem {

    @SerializedName("id")
    private String mId;

    @SerializedName("text")
    private String mText;


    public String getId() {
        return mId;
    }

    public String getText() {
        return mText;
    }

    public boolean isCountry() {
        //Sample:  "id": "country.12208545982530190"
        return !TextUtils.isEmpty(getId()) && getId().startsWith("country.");
    }

    public boolean isRegionOrCity() {
        //Sample:   "id": "place.14850115747415350" or  "id": "region.14850115747415350"
        return !TextUtils.isEmpty(getId()) && (getId().startsWith("place.") || getId().startsWith("region."));
    }
}
