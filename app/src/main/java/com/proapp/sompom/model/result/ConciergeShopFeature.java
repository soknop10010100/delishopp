package com.proapp.sompom.model.result;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;

import java.util.List;

/**
 * Created by Or Vitovongsak on 8/11/21.
 */
public class ConciergeShopFeature extends AbsConciergeModel {

    private static final String FIELD_SECTIONS = "sections";
    private static final String FIELD_NEXT = "next";

    @SerializedName(FIELD_SECTIONS)
    List<JsonObject> mSection;

    boolean mIsHasPendingOrder;

    @SerializedName(FIELD_NEXT)
    Integer mNext;

    public List<JsonObject> getSection() {
        return mSection;
    }

    public void setSection(List<JsonObject> mSection) {
        this.mSection = mSection;
    }

    public boolean isHasPendingOrder() {
        return mIsHasPendingOrder;
    }

    public void setIsHasPendingOrder(boolean mIsHasPendingOrder) {
        this.mIsHasPendingOrder = mIsHasPendingOrder;
    }

    public Integer getNext() {
        return mNext;
    }

    public void setNext(int next) {
        this.mNext = next;
    }
}
