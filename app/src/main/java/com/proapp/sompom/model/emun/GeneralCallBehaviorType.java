package com.proapp.sompom.model.emun;

public enum GeneralCallBehaviorType {

    MAKING_CALL,
    RECEIVING_CALL,
    CALL_IN_PROGRESS,
    SHOWING_REQUEST_CAMERA,
    UNKNOWN;

    public static GeneralCallBehaviorType getFromValue(int value) {
        for (GeneralCallBehaviorType generalCallBehaviorType : GeneralCallBehaviorType.values()) {
            if (generalCallBehaviorType.ordinal() == value) {
                return generalCallBehaviorType;
            }
        }

        return UNKNOWN;
    }
}
