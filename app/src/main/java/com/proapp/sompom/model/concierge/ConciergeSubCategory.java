package com.proapp.sompom.model.concierge;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConciergeSubCategory extends ConciergeSupportItemAndSubCategoryAdaptive {

    public static final String SUB_CATEGORY_TYPE = "subCategory";

    @SerializedName(FIELD_LABEL)
    private String mTitle;

    @SerializedName(FIELD_SHOP_ID)
    private String mShopId;

    @SerializedName(FIELD_LIST)
    private List<ConciergeMenuItem> mMenuItems;

    @SerializedName(FIELD_NEXT_PAGE)
    private String mNextPage;

    private boolean mIsSectionExpended;

    public String getTitle() {
        return mTitle;
    }

    public boolean isSubCategoryType() {
        return TextUtils.equals(mType, SUB_CATEGORY_TYPE);
    }

    public String getNextPage() {
        return mNextPage;
    }

    public String getShopId() {
        return mShopId;
    }

    public List<ConciergeMenuItem> getMenuItems() {
        return mMenuItems;
    }

    public boolean isSectionExpended() {
        return mIsSectionExpended;
    }

    public void setSectionExpended(boolean sectionExpended) {
        mIsSectionExpended = sectionExpended;
    }

    public void setMenuItems(List<ConciergeMenuItem> menuItems) {
        mMenuItems = menuItems;
    }

    public void setNextPage(String nextPage) {
        mNextPage = nextPage;
    }
}
