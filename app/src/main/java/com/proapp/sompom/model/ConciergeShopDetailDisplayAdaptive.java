package com.proapp.sompom.model;

/**
 * Created by Veasna Chhom on 10/9/21.
 */

public interface ConciergeShopDetailDisplayAdaptive {

    default boolean isProductCategory() {
        return false;
    }

    default String getId() {
        return null;
    }
}
