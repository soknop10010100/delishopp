package com.proapp.sompom.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.helper.IntentData;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.ViewType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


public class Comment implements Adaptive, Parcelable, Comparable<Comment> {

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
    private static final String FIELD_ID = "_id";
    private static final String FIELD_USER = "authorInfo";
    private static final String FIELD_DATE = "createdAt";
    private static final String FIELD_CONTENT = "message";
    private static final String FIELD_REPLAY_COMMENT = "subComments";
    private static final String FIELD_MEDIA = "media";
    private static final String FIELD_LAST_SUBCOMMENT_ID = "lastCommentId";
    private static final String FIELD_CONTENT_STAT = "contentStat";
    private static final String CONTENT_TYPE = "contentType";
    private static final String CONTENT_ID = "contentId";
    private static final String POST_ID = "postId";
    public static final String MAIN_COMMENT_ID = "mainCommentId";

    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_USER)
    private User mUser;
    @SerializedName(FIELD_DATE)
    private Date mDate;
    @SerializedName(FIELD_CONTENT)
    private String mContent;
    @SerializedName(FIELD_REPLAY_COMMENT)
    private List<Comment> mReplyComment;
    @SerializedName(FIELD_MEDIA)
    private List<Media> mMedia;
    private Boolean mIsPosting;
    @SerializedName(FIELD_CONTENT_STAT)
    private ContentStat mContentStat;
    @SerializedName(CONTENT_TYPE)
    transient private String mContentType;
    @SerializedName(CONTENT_ID)
    private String mContentId;
    @SerializedName(POST_ID)
    private String mPostId;
    @SerializedName(MAIN_COMMENT_ID)
    private String mMainCommentId;
    @SerializedName(LifeStream.META_PREVIEW)
    List<LinkPreviewModel> mMetaPreview;

    public Comment() {

    }

    protected Comment(Parcel in) {
        this.mId = in.readString();
        this.mUser = in.readParcelable(User.class.getClassLoader());
        long tmpMDate = in.readLong();
        this.mDate = tmpMDate == -1 ? null : new Date(tmpMDate);
        this.mContent = in.readString();
        this.mReplyComment = in.createTypedArrayList(Comment.CREATOR);
        this.mMedia = in.createTypedArrayList(Media.CREATOR);
        this.mIsPosting = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mContentStat = in.readParcelable(User.class.getClassLoader());
        this.mPostId = in.readString();
        this.mContentId = in.readString();
        this.mMainCommentId = in.readString();
        this.mMetaPreview = in.createTypedArrayList(LinkPreviewModel.CREATOR);
    }

    public List<LinkPreviewModel> getMetaPreview() {
        return mMetaPreview;
    }

    public void setMetaPreview(List<LinkPreviewModel> metaPreview) {
        mMetaPreview = metaPreview;
    }

    public String getContentId() {
        return mContentId;
    }

    public void setPostId(String postId) {
        mPostId = postId;
    }

    public void setContentId(String contentId) {
        mContentId = contentId;
    }

    public String getMainCommentId() {
        return mMainCommentId;
    }

    public void setMainCommentId(String mainCommentId) {
        mMainCommentId = mainCommentId;
    }

    public String getPostId() {
        return mPostId;
    }

    public static Comment getNewComment(Comment comment, boolean isSubComment) {
        Comment comment1 = new Comment();
        comment1.setUser(null);
        comment1.setUser(null);
        comment1.setPosting(null);
        comment1.setLike(null);
        comment1.setTotalSubComment(null);
        comment1.setDate(null);
        comment1.setContentStat(null);
        comment1.setContentType(comment.getContentType());
        comment1.setPostId(comment.getPostId());
        comment1.setMainCommentId(comment.getMainCommentId());

        if (comment.getMedia() == null || comment.getMedia().isEmpty()) {
            comment1.setContent(comment.getContent());
        } else {
            comment1.setMedia(comment.getMedia());
            comment1.setContent(null);
        }

        return comment1;
    }

    public String getContentType() {
        return mContentType;
    }

    public void setContentType(String contentType) {
        mContentType = contentType;
    }

    public List<Comment> getReplyComment() {
        return mReplyComment;
    }

    public void setReplyComment(List<Comment> replyComment) {
        mReplyComment = replyComment;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public Date getDate() {
        if (mDate == null) {
            mDate = new Date();
        }
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public boolean isLike() {
        if (getContentStat() != null) {
            return getContentStat().getLiked();
        }

        return false;
    }

    public void setLike(Boolean like) {
        if (getContentStat() != null && like != null) {
            getContentStat().setLiked(like);
        }
    }

    public List<Media> getMedia() {
        return mMedia;
    }

    public void setMedia(Media media) {
        List<Media> media1 = new ArrayList<>();
        media1.add(media);
        setMedia(media1);
    }

    public void setMedia(List<Media> media) {
        mMedia = media;
    }

    public boolean isPosting() {
        if (mIsPosting == null) {
            return false;
        }
        return mIsPosting;
    }

    public void setPosting(Boolean posting) {
        mIsPosting = posting;
    }

    public int getTotalSubComment() {
        if (getContentStat() != null) {
            return (int) getContentStat().getTotalComments();
        }

        return 0;
    }

    public void setTotalSubComment(Integer totalSubComment) {
        if (getContentStat() != null && totalSubComment != null) {
            getContentStat().setTotalComments((long) totalSubComment);
        }
    }

    public ContentStat getContentStat() {
        if (mContentStat == null) {
            mContentStat = new ContentStat();
        }
        return mContentStat;
    }

    public void setContentStat(ContentStat contentStat) {
        mContentStat = contentStat;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Comment && Objects.equals(((Comment) obj).getId(), mId);
    }

    @Override
    public int hashCode() {
        return mId.hashCode();
    }

    @Override
    public String toString() {
        return "id = " + mId + ", user = " + mUser + ", date = " + mDate + ", content = " + mContent;
    }

    @Override
    public int compareTo(@NonNull Comment another) {
        double lastContactMicroTime = another.getDate().getTime();

        return (int) (getDate().getTime() - lastContactMicroTime);
    }

    @Override
    public ViewType getTimelineViewType() {
        return null;
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting activity
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeParcelable(this.mUser, flags);
        dest.writeLong(this.mDate != null ? this.mDate.getTime() : -1);
        dest.writeString(this.mContent);
        dest.writeTypedList(this.mReplyComment);
        dest.writeTypedList(this.mMedia);
        dest.writeValue(this.mIsPosting);
        dest.writeParcelable(this.mContentStat, flags);
        dest.writeString(this.mPostId);
        dest.writeString(this.mContentId);
        dest.writeString(this.mMainCommentId);
        dest.writeTypedList(this.mMetaPreview);
    }
}