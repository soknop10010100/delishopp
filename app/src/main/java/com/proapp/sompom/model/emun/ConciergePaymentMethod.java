package com.proapp.sompom.model.emun;

import android.text.TextUtils;

/**
 * Created by Veasna Chhom on 3/1/22.
 */
public enum ConciergePaymentMethod {

    CASH_ON_DELIVERY("cash"),
    ONLINE_METHOD("online");

    private final String mValue;

    ConciergePaymentMethod(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public static ConciergePaymentMethod fromValue(String value) {
        for (ConciergePaymentMethod type : ConciergePaymentMethod.values()) {
            if (TextUtils.equals(type.mValue, value)) {
                return type;
            }
        }

        return CASH_ON_DELIVERY;
    }
}
