package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.ConciergeItemAdaptive;
import com.proapp.sompom.model.ConciergeOrderItemDisplayAdaptive;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;
import com.proapp.sompom.model.DifferentAdaptive;

/**
 * Created by Chhom Veasna on 5/10/22.
 */

public class ConciergeSupplier extends AbsConciergeModel
        implements DifferentAdaptive<ConciergeSupplier>,
        Parcelable,
        ConciergeOrderItemDisplayAdaptive,
        ConciergeShopDetailDisplayAdaptive,
        ConciergeItemAdaptive {

    @SerializedName(FIELD_SUPPLIER_CODE)
    private String mSupplierCode;

    @SerializedName(FIELD_NAME)
    private String mSupplierName;

    @SerializedName(FIELD_DESCRIPTION)
    private String mDescription;

    @SerializedName(FIELD_PHONE)
    private String mPhone;

    @SerializedName(FIELD_SUPPLIER_LOGO)
    private String mLogo;

    @SerializedName(FIELD_ALLOW_MULTIPLE_BASKET)
    private boolean mIsAllowMultiBasket;

    @SerializedName(FIELD_START_DELIVERY_TIME)
    private String mStartDeliveryTime;

    @SerializedName(FIELD_END_DELIVERY_TIME)
    private String mEndDeliveryTime;

    @SerializedName(FIELD_ESTIMATE_TIME_DELIVERY_MINUTE)
    private int mEstimateTimeDeliveryMinute;

    @SerializedName(FIELD_ADDRESS)
    private String mAddress;

    @SerializedName(FIELD_IS_SHOP_OPEN)
    private boolean mIsShopOpen;

    @SerializedName(FIELD_NEXT_AVAILABLE_DAY)
    private String mAvailableDay;

    public ConciergeSupplier() {
    }

    protected ConciergeSupplier(Parcel in) {
        mId = in.readString();
        mSupplierCode = in.readString();
        mSupplierName = in.readString();
        mDescription = in.readString();
        mPhone = in.readString();
        mLogo = in.readString();
        mIsAllowMultiBasket = in.readInt() > 0;
        mStartDeliveryTime = in.readString();
        mEndDeliveryTime = in.readString();
        mEstimateTimeDeliveryMinute = in.readInt();
        mAddress = in.readString();
        mIsShopOpen = in.readInt() > 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mSupplierCode);
        dest.writeString(mSupplierName);
        dest.writeString(mDescription);
        dest.writeString(mPhone);
        dest.writeString(mLogo);
        dest.writeInt(mIsAllowMultiBasket ? 1 : 0);
        dest.writeString(mStartDeliveryTime);
        dest.writeString(mEndDeliveryTime);
        dest.writeInt(mEstimateTimeDeliveryMinute);
        dest.writeString(mAddress);
        dest.writeInt(mIsShopOpen ? 1 : 0);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConciergeSupplier> CREATOR = new Creator<ConciergeSupplier>() {
        @Override
        public ConciergeSupplier createFromParcel(Parcel in) {
            return new ConciergeSupplier(in);
        }

        @Override
        public ConciergeSupplier[] newArray(int size) {
            return new ConciergeSupplier[size];
        }
    };

    public String getAvailableDay() {
        return mAvailableDay;
    }

    public boolean isShopOpen() {
        return mIsShopOpen;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getSupplierCode() {
        return mSupplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        mSupplierCode = supplierCode;
    }

    public String getSupplierName() {
        return mSupplierName;
    }

    public void setSupplierName(String supplierName) {
        mSupplierName = supplierName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getLogo() {
        return mLogo;
    }

    public void setLogo(String logo) {
        mLogo = logo;
    }

    public boolean isAllowMultiBasket() {
        return mIsAllowMultiBasket;
    }

    public void setAllowMultiBasket(boolean allowMultiBasket) {
        mIsAllowMultiBasket = allowMultiBasket;
    }

    public String getStartDeliveryTime() {
        return mStartDeliveryTime;
    }

    public String getEndDeliveryTime() {
        return mEndDeliveryTime;
    }

    public int getEstimateTimeDeliveryMinute() {
        return mEstimateTimeDeliveryMinute;
    }

    @Override
    public boolean isProductCategory() {
        return false;
    }

    @Override
    public boolean areItemsTheSame(ConciergeSupplier other) {
        return TextUtils.equals(getId(), other.getId());
    }

    @Override
    public boolean areContentsTheSame(ConciergeSupplier other) {
        return TextUtils.equals(getSupplierName(), other.getSupplierName()) &&
                TextUtils.equals(getDescription(), other.getDescription()) &&
                TextUtils.equals(getPhone(), other.getPhone()) &&
                TextUtils.equals(getLogo(), other.getLogo()) &&
                TextUtils.equals(getSupplierCode(), other.getSupplierCode());
    }
}
