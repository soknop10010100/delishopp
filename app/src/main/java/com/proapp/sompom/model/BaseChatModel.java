package com.proapp.sompom.model;

/**
 * Created by nuonveyo on 9/5/18.
 */

public interface BaseChatModel {

    long getTime();

    default String getId() {
        return null;
    }
}
