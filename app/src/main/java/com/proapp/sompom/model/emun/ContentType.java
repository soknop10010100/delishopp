package com.proapp.sompom.model.emun;

/**
 * Created by He Rotha on 3/18/19.
 */
public enum ContentType {

    PRODUCT("product"),
    POST("post"),
    COMMENT("comment"),
    SUB_COMMENT("subComment"),
    MEDIA("media"),
    VIDEO("video"),
    IMAGE("image");

    private String mValue;

    ContentType(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }
}
