package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.ConciergeItemAdaptive;
import com.proapp.sompom.model.ConciergeOrderItemDisplayAdaptive;
import com.proapp.sompom.model.DifferentAdaptive;
import com.proapp.sompom.model.emun.ConciergeMenuItemType;
import com.proapp.sompom.model.result.ConciergeMenuItemOptionSection;

import java.util.List;

/**
 * Created by Or Vitovongsak on 30/8/21.
 */

public class ConciergeMenuItem extends ConciergeSupportItemAndSubCategoryAdaptive implements DifferentAdaptive<ConciergeMenuItem>,
        Parcelable,
        ConciergeItemAdaptive,
        ConciergeOrderItemDisplayAdaptive {

    @SerializedName(FIELD_NAME)
    private String mName;

    @SerializedName(FIELD_DESCRIPTION)
    private String mDescription;

    @SerializedName(FIELD_PICTURE)
    private String mPicture;

    @SerializedName(FIELD_DEFAULT_PRICE)
    private float mPrice;

    @SerializedName(FIELD_ORIGINAL_PRICE)
    private Double mOriginalPrice;

    @SerializedName(FIELD_OPTIONS)
    private List<ConciergeMenuItemOptionSection> mOptions;

    @SerializedName(FIELD_CHOICES)
    private List<ConciergeComboChoiceSection> mChoices;

    @SerializedName(FIELD_SHOP)
    private ConciergeShop mShop;

    @SerializedName(FIELD_HASH)
    private Integer mHash;

    @SerializedName(FIELD_SHOP_ID)
    private String mShopId;

    @SerializedName(FIELD_IS_SAME_DAY)
    private boolean mIsSameDay;

    @SerializedName(FIELD_WEIGHT)
    private String mWeight;

    @SerializedName(FIELD_IS_IN_STOCK)
    private boolean mIsInStock;

    // For keeping track of number of item currently in cart
    @SerializedName("orderUnitCount")
    private int mCartCount = 0;

    // For keeping track of base price + selected options price
    @SerializedName("accumulatedPrice")
    private double mAccumulatedPrice;

    @SerializedName(FIELD_SUPPLIER_ID)
    private String mSupplierId;

    @SerializedName(FIELD_SUPPLIER)
    private ConciergeSupplier mSupplier;

    // For keeping track of total price, meaning accumulated price + count
    private double mTotalPrice;

    // Special instruction when user add product to cart
    private String mSpecialInstruction;

    private String mBasketItemId;

    @SerializedName("backupProductCount")
    private int mBackupProductCount;

    /*
        Soft delete item is the item that is considered removed from basket, but I don't get response
        back from server if the removal is success. We still keep this item in local basket in that
        the API response error and we can roll back the item into local basket.
     */
    @SerializedName("iSoftDeleted")
    private boolean mIsSoftDeleted;

    @SerializedName(FIELD_BRAND)
    private ConciergeBrand mConciergeBrand;

    @SerializedName(FIELD_IS_SHOP_OPEN)
    private boolean mIsShopOpen;

    @SerializedName(FIELD_AVAILABLE_TOMORROW)
    private boolean mIsGetTomorrow;

    @SerializedName(FIELD_IS_DELIVER_TO_PROVINCE)
    private boolean mIsDeliverToProvince;

    public ConciergeMenuItem() {
    }

    protected ConciergeMenuItem(Parcel in) {
        mId = in.readString();
        mName = in.readString();
        mDescription = in.readString();
        mPicture = in.readString();
        mType = in.readString();
        mPrice = in.readFloat();
        mOriginalPrice = (Double) in.readSerializable();
        mOptions = in.createTypedArrayList(ConciergeMenuItemOptionSection.CREATOR);
        mChoices = in.createTypedArrayList(ConciergeComboChoiceSection.CREATOR);
        mShop = in.readParcelable(ConciergeShop.class.getClassLoader());
        mShopId = in.readString();
        mCartCount = in.readInt();
        mAccumulatedPrice = in.readDouble();
        mTotalPrice = in.readDouble();
        mSpecialInstruction = in.readString();
        mHash = (Integer) in.readSerializable();
        mBasketItemId = in.readString();
        mWeight = in.readString();
        mSupplierId = in.readString();
        mSupplier = in.readParcelable(ConciergeSupplier.class.getClassLoader());
        mBackupProductCount = in.readInt();
        mIsSoftDeleted = in.readInt() > 0;
        mIsShopOpen = in.readInt() > 0;
        mIsGetTomorrow = in.readInt() > 0;
        mIsDeliverToProvince = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mName);
        dest.writeString(mDescription);
        dest.writeString(mPicture);
        dest.writeString(mType);
        dest.writeFloat(mPrice);
        dest.writeSerializable(mOriginalPrice);
        dest.writeTypedList(mOptions);
        dest.writeTypedList(mChoices);
        dest.writeParcelable(mShop, flags);
        dest.writeString(mShopId);
        dest.writeInt(mCartCount);
        dest.writeDouble(mAccumulatedPrice);
        dest.writeDouble(mTotalPrice);
        dest.writeString(mSpecialInstruction);
        dest.writeSerializable(mHash);
        dest.writeString(mBasketItemId);
        dest.writeString(mWeight);
        dest.writeString(mSupplierId);
        dest.writeParcelable(mSupplier, flags);
        dest.writeInt(mBackupProductCount);
        dest.writeInt(mIsSoftDeleted ? 1 : 0);
        dest.writeInt(mIsShopOpen ? 1 : 0);
        dest.writeInt(mIsGetTomorrow ? 1 : 0);
        dest.writeByte((byte) (mIsDeliverToProvince ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConciergeMenuItem> CREATOR = new Creator<ConciergeMenuItem>() {
        @Override
        public ConciergeMenuItem createFromParcel(Parcel in) {
            return new ConciergeMenuItem(in);
        }

        @Override
        public ConciergeMenuItem[] newArray(int size) {
            return new ConciergeMenuItem[size];
        }
    };

    public int getBackupProductCount() {
        return mBackupProductCount;
    }

    public void setBackupProductCount(int backupProductCount) {
        mBackupProductCount = backupProductCount;
    }

    public boolean isGetTomorrow() {
        return mIsGetTomorrow;
    }

    public ConciergeBrand getConciergeBrand() {
        return mConciergeBrand;
    }

    public boolean isSoftDeleted() {
        return mIsSoftDeleted;
    }

    public void setSoftDeleted(boolean softDeleted) {
        mIsSoftDeleted = softDeleted;
    }

    public ConciergeSupplier getSupplier() {
        return mSupplier;
    }

    public void setSupplier(ConciergeSupplier supplier) {
        mSupplier = supplier;
    }

    public String getSupplierId() {
        return mSupplierId;
    }

    public void setSupplierId(String supplierId) {
        mSupplierId = supplierId;
    }

    public boolean isShopOpen() {
        return mIsShopOpen;
    }

    @Override
    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getPicture() {
        return mPicture;
    }

    public double getOriginalPrice() {
        if (mOriginalPrice != null) {
            return mOriginalPrice;
        }

        return 0;
    }

    public String getWeight() {
        return mWeight;
    }

    public void setWeight(String weight) {
        mWeight = weight;
    }

    public void setPicture(String picture) {
        this.mPicture = picture;
    }

    public float getPrice() {
        return mPrice;
    }

    public int getHash() {
        if (mHash != null) {
            return mHash;
        }

        return 0;
    }

    public boolean isInStock() {
        return mIsInStock;
    }

    public void setInStock(boolean inStock) {
        mIsInStock = inStock;
    }

    public boolean isSameDay() {
        return mIsSameDay;
    }

    public String getBasketItemId() {
        return mBasketItemId;
    }

    public void setBasketItemId(String basketItemId) {
        mBasketItemId = basketItemId;
    }

    public void setHash(int hash) {
        mHash = hash;
    }

    public void setPrice(float price) {
        this.mPrice = price;
    }

    public double getAccumulatedPrice() {
        return mAccumulatedPrice;
    }

    public void setAccumulatedPrice(double accumulatedPrice) {
        mAccumulatedPrice = accumulatedPrice;
    }

    public ConciergeShop getShop() {
        return mShop;
    }

    public void setShop(ConciergeShop shop) {
        mShop = shop;
    }

    public List<ConciergeMenuItemOptionSection> getOptions() {
        return mOptions;
    }

    public void setOptions(List<ConciergeMenuItemOptionSection> options) {
        mOptions = options;
    }

    public List<ConciergeComboChoiceSection> getChoices() {
        return mChoices;
    }

    public void setChoices(List<ConciergeComboChoiceSection> choices) {
        this.mChoices = choices;
    }

    public int getProductCount() {
        return mCartCount;
    }

    public void setProductCount(int cartCount) {
        if (cartCount < 0) {
            cartCount = 0;
        }
        mCartCount = cartCount;
        updateProductPrice();
    }

    public void decreaseProductCount() {
        // Only decrease count if above 0
        if (mCartCount > 0) {
            mCartCount--;
            updateProductPrice();
        }
    }

    public void updateProductPrice() {
        mTotalPrice = mAccumulatedPrice * mCartCount;
    }

    public double getTotalPrice() {
        return mTotalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        mTotalPrice = totalPrice;
    }

    public String getSpecialInstruction() {
        return mSpecialInstruction;
    }

    public void setSpecialInstruction(String specialInstruction) {
        mSpecialInstruction = specialInstruction;
    }

    public String getShopId() {
        return mShopId;
    }

    public void setShopId(String shopId) {
        this.mShopId = shopId;
    }

    public boolean isDeliverToProvince() {
        return mIsDeliverToProvince;
    }

    public void setIsDeliverToProvince(boolean isDeliverToProvince) {
        mIsDeliverToProvince = isDeliverToProvince;
    }

    @Override
    public boolean areItemsTheSame(ConciergeMenuItem other) {
        return getId().equals(other.getId());
    }

    @Override
    public boolean areContentsTheSame(ConciergeMenuItem other) {
        return areItemsTheSame(other) &&
                TextUtils.equals(getName(), other.getName()) &&
                TextUtils.equals(getDescription(), other.getDescription()) &&
                TextUtils.equals(getPicture(), other.getPicture()) &&
                getType().equals(other.getType()) &&
                getPrice() == other.getPrice() &&
                areOriginalPriceTheSame(other) &&
                areMenuItemShopTheSame(other.getShop()) &&
                TextUtils.equals(getShopId(), other.getShopId()) &&
                TextUtils.equals(getSpecialInstruction(), other.getSpecialInstruction()) &&
                checkIfItemOrComboAreTheSame(other) &&
                getProductCount() == other.getProductCount() &&
                getHash() == other.getHash() &&
                isDeliverToProvince() == other.isDeliverToProvince();
    }

    public boolean areMenuItemShopTheSame(ConciergeShop otherShop) {
        if (getShop() == null && otherShop == null) {
            return true;
        } else if (getShop() != null && otherShop != null) {
            return getShop().areItemsTheSame(otherShop);
        } else {
            return false;
        }
    }

    public boolean areContentsTheSameForCartHelper(ConciergeMenuItem other) {
        return areItemsTheSame(other) &&
                TextUtils.equals(getName(), other.getName()) &&
                TextUtils.equals(getDescription(), other.getDescription()) &&
                TextUtils.equals(getPicture(), other.getPicture()) &&
                getType().equals(other.getType()) &&
                getPrice() == other.getPrice() &&
                areOriginalPriceTheSame(other) &&
                TextUtils.equals(getSpecialInstruction(), other.getSpecialInstruction()) &&
                checkIfItemOrComboAreTheSame(other);
    }

    private boolean areOriginalPriceTheSame(ConciergeMenuItem other) {
        return getOriginalPrice() == other.getOriginalPrice();
    }

    private boolean checkIfItemOrComboAreTheSame(ConciergeMenuItem other) {
        if (getType() == ConciergeMenuItemType.ITEM && other.getType() == ConciergeMenuItemType.ITEM) {
            return areOptionsTheSame(other.getOptions());
        } else if (getType() == ConciergeMenuItemType.COMBO && other.getType() == ConciergeMenuItemType.COMBO) {
            return areChoicesTheSame(other.getChoices());
        }
        return false;
    }

    private boolean areOptionsTheSame(List<ConciergeMenuItemOptionSection> other) {
        if ((getOptions() == null && other == null) ||
                (getOptions().isEmpty() && other.isEmpty())) {
            return true;
        } else if (getOptions() != null && other != null
                && getOptions().size() == other.size()) {

            for (int index = 0; index < getOptions().size(); index++) {
                if (!getOptions().get(index).areContentsTheSame(other.get(index))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private boolean areChoicesTheSame(List<ConciergeComboChoiceSection> other) {
        if ((getChoices() == null && other == null) ||
                (getChoices().isEmpty() && other.isEmpty())) {
            return true;
        } else if (getChoices() != null && other != null
                && getChoices().size() == other.size()) {

            for (int index = 0; index < getChoices().size(); index++) {
                if (!getChoices().get(index).areContentsTheSame(other.get(index))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
