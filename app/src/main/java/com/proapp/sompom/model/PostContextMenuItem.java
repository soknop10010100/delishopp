package com.proapp.sompom.model;

import androidx.annotation.FontRes;
import androidx.annotation.StringRes;

/**
 * Created by nuonveyo on 10/31/18.
 */

public class PostContextMenuItem {

    private int mId;
    @StringRes
    private int mIcon;
    @StringRes
    private int mTitle;
    @FontRes
    private int mFontStyle = -1;
    private int mIconColor = -1;
    private int mTextColor = -1;

    public PostContextMenuItem(int id, int icon, int title) {
        mId = id;
        mIcon = icon;
        mTitle = title;
    }

    public PostContextMenuItem(int id, int icon, int title, int iconColor, int fontStyle) {
        mId = id;
        mIcon = icon;
        mTitle = title;
        mIconColor = iconColor;
        mFontStyle = fontStyle;
    }

    public PostContextMenuItem(int id,
                               int icon,
                               int title,
                               int iconColor,
                               int textColor,
                               int fontStyle) {
        mId = id;
        mIcon = icon;
        mTitle = title;
        mIconColor = iconColor;
        mTextColor = textColor;
        mFontStyle = fontStyle;
    }

    public int getFontStyle() {
        return mFontStyle;
    }

    public int getIconColor() {
        return mIconColor;
    }

    public int getTextColor() {
        return mTextColor;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getIcon() {
        return mIcon;
    }

    public void setIcon(int icon) {
        mIcon = icon;
    }

    public int getTitle() {
        return mTitle;
    }

    public void setTitle(int title) {
        mTitle = title;
    }
}
