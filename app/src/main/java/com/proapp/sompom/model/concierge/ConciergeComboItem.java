package com.proapp.sompom.model.concierge;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.ConciergeItemAdaptive;
import com.proapp.sompom.model.ConciergeOrderItemDisplayAdaptive;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;
import com.proapp.sompom.model.DifferentAdaptive;
import com.proapp.sompom.model.ProductDetailAdaptive;
import com.proapp.sompom.model.emun.ConciergeMenuItemType;
import com.proapp.sompom.model.result.ConciergeMenuItemOptionSection;

import java.util.List;

/**
 * Created by Or Vitovongsak on 13/10/21.
 */

public class ConciergeComboItem extends AbsConciergeModel implements DifferentAdaptive<ConciergeComboItem>,
        Parcelable,
        ProductDetailAdaptive,
        ConciergeShopDetailDisplayAdaptive,
        ConciergeItemAdaptive,
        ConciergeOrderItemDisplayAdaptive {

    private static final String FIELD_NAME = "name";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIELD_PICTURE = "picture";
    private static final String FIELD_SHOP = "shop";
    private static final String FIELD_DEFAULT_PRICE = "defaultPrice";

    @SerializedName(FIELD_NAME)
    private String mName;

    @SerializedName(FIELD_DESCRIPTION)
    private String mDescription;

    @SerializedName(FIELD_PICTURE)
    private String mPicture;

    @SerializedName(FIELD_TYPE)
    private String mType;

    @SerializedName(FIELD_DEFAULT_PRICE)
    private float mDefaultPrice;

    @SerializedName(FIELD_PRICE)
    private float mPrice;

    @SerializedName(FIELD_SHOP_ID)
    private String mShopId;

    @SerializedName(FIELD_OPTIONS)
    private List<ConciergeMenuItemOptionSection> mOptions;

    @SerializedName(FIELD_SHOP)
    private ConciergeShop mShop;

    // For keeping track of base price + selected options price
    @SerializedName("accumulatedPrice")
    private double mAccumulatedPrice;

    // For keeping track of total price, meaning accumulated price + count
    private double mTotalPrice;

    private boolean mIsSelected;

    public ConciergeComboItem() {
    }

    protected ConciergeComboItem(Parcel in) {
        mId = in.readString();
        mName = in.readString();
        mDescription = in.readString();
        mPicture = in.readString();
        mType = in.readString();
        mDefaultPrice = in.readFloat();
        mPrice = in.readFloat();
        mShopId = in.readString();
        mOptions = in.createTypedArrayList(ConciergeMenuItemOptionSection.CREATOR);
        mShop = in.readParcelable(ConciergeShop.class.getClassLoader());
        mAccumulatedPrice = in.readDouble();
        mTotalPrice = in.readDouble();
        mIsSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mName);
        dest.writeString(mDescription);
        dest.writeString(mPicture);
        dest.writeString(mType);
        dest.writeFloat(mDefaultPrice);
        dest.writeFloat(mPrice);
        dest.writeString(mShopId);
        dest.writeTypedList(mOptions);
        dest.writeParcelable(mShop, flags);
        dest.writeDouble(mAccumulatedPrice);
        dest.writeDouble(mTotalPrice);
        dest.writeByte((byte) (mIsSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConciergeComboItem> CREATOR = new Creator<ConciergeComboItem>() {
        @Override
        public ConciergeComboItem createFromParcel(Parcel in) {
            return new ConciergeComboItem(in);
        }

        @Override
        public ConciergeComboItem[] newArray(int size) {
            return new ConciergeComboItem[size];
        }
    };

    @Override
    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getPicture() {
        return mPicture;
    }

    public void setPicture(String picture) {
        this.mPicture = picture;
    }

    public void setType(ConciergeMenuItemType type) {
        mType = type.getValue();
    }

    public ConciergeMenuItemType getType() {
        return ConciergeMenuItemType.from(mType);
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        this.mPrice = price;
    }

    public void setDefaultPrice(float defaultPrice) {
        mDefaultPrice = defaultPrice;
    }

    public float getDefaultPrice() {
        return mDefaultPrice;
    }

    public double getAccumulatedPrice() {
        return mAccumulatedPrice;
    }

    public void setAccumulatedPrice(double accumulatedPrice) {
        mAccumulatedPrice = accumulatedPrice;
    }

    public ConciergeShop getShop() {
        return mShop;
    }

    public void setShop(ConciergeShop shop) {
        mShop = shop;
    }

    public List<ConciergeMenuItemOptionSection> getOptions() {
        return mOptions;
    }

    public void setOptions(List<ConciergeMenuItemOptionSection> options) {
        mOptions = options;
    }

    public void calculateComboItemPrice() {
        double accumulatedPrice = ConciergeCartHelper.calculateItemAccumulatedPrice(getOptions());
        setAccumulatedPrice(accumulatedPrice);
        setTotalPrice(getPrice() + getAccumulatedPrice());
    }

    public double getTotalPrice() {
        return mTotalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        mTotalPrice = totalPrice;
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.mIsSelected = isSelected;
    }

    @SuppressLint("NewApi")
    public void clearComboChoiceSelection() {
        if (getOptions() != null && !getOptions().isEmpty()) {
            getOptions().forEach(conciergeMenuItemOptionSection -> {
                if (conciergeMenuItemOptionSection.getOptionItems() != null &&
                        !conciergeMenuItemOptionSection.getOptionItems().isEmpty()) {
                    conciergeMenuItemOptionSection.getOptionItems().forEach(option -> {
                        if (option != null) {
                            option.setIsSelected(false);
                        }
                    });
                }
            });
        }
    }

    public ConciergeMenuItem convertToMenuItem() {
        ConciergeMenuItem menuItem = new ConciergeMenuItem();

        menuItem.setId(getId());
        menuItem.setName(getName());
        menuItem.setDescription(getDescription());
        menuItem.setPicture(getPicture());
        menuItem.setType(getType());
        menuItem.setPrice(getPrice());
        menuItem.setShop(getShop());
        menuItem.setOptions(getOptions());
        menuItem.setAccumulatedPrice(getAccumulatedPrice());
        menuItem.setTotalPrice(getTotalPrice());

        return menuItem;
    }

    @Override
    public boolean areItemsTheSame(ConciergeComboItem other) {
        return getId().matches(other.getId());
    }

    @Override
    public boolean areContentsTheSame(ConciergeComboItem other) {
        return areItemsTheSame(other) &&
                TextUtils.equals(getName(), other.getName()) &&
                TextUtils.equals(getDescription(), other.getDescription()) &&
                TextUtils.equals(getPicture(), other.getPicture()) &&
                getPrice() == other.getPrice() &&
                areOptionTheSame(other.getOptions());
    }

    private boolean areOptionTheSame(List<ConciergeMenuItemOptionSection> other) {
        if ((getOptions() == null && other == null) ||
                (getOptions().isEmpty() && other.isEmpty())) {
            return true;
        } else if (getOptions() != null && other != null
                && getOptions().size() == other.size()) {

            for (int index = 0; index < getOptions().size(); index++) {
                if (!getOptions().get(index).areContentsTheSame(other.get(index))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
