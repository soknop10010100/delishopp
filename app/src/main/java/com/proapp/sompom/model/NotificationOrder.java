package com.proapp.sompom.model;

import android.content.Context;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.emun.ConciergeOrderStatus;
import com.resourcemanager.helper.LocaleManager;

public class NotificationOrder extends AbsConciergeModel {

    @SerializedName("number")
    private String mOrderNumber;

    @SerializedName("status")
    private String mOrderStatus;

    @SerializedName("statusString")
    private JsonElement mStatusString;

    @SerializedName("description")
    private JsonElement mDescription;

    public String getOrderNumber() {
        return mOrderNumber;
    }

    public ConciergeOrderStatus getOrderStatus() {
        return ConciergeOrderStatus.fromValue(mOrderStatus);
    }

    public String getStatusString(Context context) {
        return getStatusString(context, mStatusString);
    }

    public String getDescription(Context context) {
        return getStatusString(context, mDescription);
    }

    public static String getStatusString(Context context, JsonElement localizeStringElement) {
        if (localizeStringElement != null) {
            JsonObject asJsonObject = localizeStringElement.getAsJsonObject();
            if (asJsonObject != null) {
                JsonElement jsonElement = asJsonObject.get(LocaleManager.getAppLanguage(context));
                if (jsonElement != null) {
                    return jsonElement.getAsString();
                } else {
                    //Return default
                    JsonElement enKey = asJsonObject.get(LocaleManager.ENGLISH_LANGUAGE_KEY);
                    if (enKey != null) {
                        return enKey.getAsString();
                    }
                }
            }
        }

        return "";
    }
}
