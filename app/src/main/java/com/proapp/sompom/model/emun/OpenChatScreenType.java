package com.proapp.sompom.model.emun;

/**
 * Created by Chhom Veasna on 18/12/20.
 */

public enum OpenChatScreenType {

    DEFAULT(1),
    GENERAL_SEARCH(2),
    GROUP_SEARCH(3);

    private int mValue;

    OpenChatScreenType(int id) {
        mValue = id;
    }

    public static OpenChatScreenType getFromValue(int value) {
        for (OpenChatScreenType openChatScreenType : values()) {
            if (openChatScreenType.mValue == value) {
                return openChatScreenType;
            }
        }

        return OpenChatScreenType.DEFAULT;
    }

    public int getValue() {
        return mValue;
    }
}
