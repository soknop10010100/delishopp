package com.proapp.sompom.model;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.helper.IntentData;
import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.helper.MetaPreviewHelper;
import com.proapp.sompom.intent.newintent.TimelineDetailIntent;
import com.proapp.sompom.intent.newintent.TimelineDetailIntentResult;
import com.proapp.sompom.model.emun.ItemShape;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.emun.PublishItem;
import com.proapp.sompom.model.emun.TimelineDetailRedirectionType;
import com.proapp.sompom.model.emun.ViewType;
import com.proapp.sompom.model.result.ContentStat;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.widget.lifestream.Format;
import com.sompom.baseactivity.ResultCallback;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


public class LifeStream implements Adaptive, Parcelable, WallStreetAdaptive, DifferentAdaptive<LifeStream>,
        LinkPreviewAdaptive {

    public static final Creator<LifeStream> CREATOR = new Creator<LifeStream>() {
        @Override
        public LifeStream createFromParcel(Parcel source) {
            return new LifeStream(source);
        }

        @Override
        public LifeStream[] newArray(int size) {
            return new LifeStream[size];
        }
    };
    private static final String FIELD_MEDIA = "media";
    private static final String FIELD_TITLE = "text";
    private static final String FIELD_USER = "userstore";
    private static final String FIELD_VIDEO_SHAPE = "shape";
    public static final String META_PREVIEW = "metaPreview";
    public static final String CONTENT_ID = "contentId";
    public static final String IS_DELETED = "isDeleted";

    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_CREATE_DATE)
    private Date mCreateDate;
    @SerializedName(FIELD_PUBLISH)
    private int mPublish;
    @SerializedName(FIELD_LATITUDE)
    private Double mLatitude;
    @SerializedName(FIELD_ADDRESS)
    private String mAddress;
    @SerializedName(FIELD_LONGITUDE)
    private Double mLongtitude;
    @SerializedName(FIELD_MEDIA)
    private List<Media> mMedia;
    @SerializedName(FIELD_TITLE)
    private String mTitle;
    @SerializedName(FIELD_USER)
    private User mStoreUser;
    @SerializedName(FIELD_SHARE_URL)
    private String mShareUrl;
    @SerializedName(FIELD_VIDEO_SHAPE)
    private String mItemShape;
    @SerializedName(FIELD_CITY)
    private String mCity;
    @SerializedName(FIELD_COUNTRY)
    private String mCountry;
    @SerializedName(FIELD_CONTENT_STAT)
    private ContentStat mContentStat;
    @SerializedName(LOCATION_NAME)
    private String mLocationName;
    @SerializedName(LOCATION_COUNTRY)
    private String mLocationCountry;
    @SerializedName(META_PREVIEW)
    private JsonElement mMetaPreviewModelList = new JsonArray();
    private String mBackupMetaPreviewModelList;
    private List<LinkPreviewModel> mBackupLinkPreviewModelList;
    @SerializedName(CONTENT_ID)
    private String mContentId;
    private Boolean mIsEditedMode;
    @SerializedName(IS_DELETED)
    private Boolean mIsDeleted;
    private boolean mIsWelcomePost;

    public LifeStream() {
        mId = UUID.randomUUID().toString();
    }

    public static LifeStream getPostInstance(LifeStream lifeStream, boolean isEditedMode) {
        /*
            Can remove all unneeded properties to be posted to server.
         */
        LifeStream lifeStream1 = new LifeStream(lifeStream);
        lifeStream1.setId(null);
        lifeStream1.setEditedMode(null);
        if (isEditedMode) {
            lifeStream1.setId(null);
        }

        return lifeStream1;
    }

    public LifeStream(LifeStream lifeStream) {
        setId(lifeStream.getId());
        setAddress(lifeStream.getAddress());
        setCity(lifeStream.getCity());
        setCountry(lifeStream.getCountry());
        setTitle(lifeStream.getTitle());
        setPublish(lifeStream.mPublish);
        if (lifeStream.getMedia() != null && !lifeStream.getMedia().isEmpty()) {
            List<Media> medias = new ArrayList<>();
            for (Media media : lifeStream.getMedia()) {
                medias.add(new Media(media));
            }
            setMedia(medias);
        }
        setLatitude(lifeStream.getLatitude());
        setLongitude(lifeStream.getLongitude());
        setStoreUser(lifeStream.getStoreUser());
        setCountry(lifeStream.getCountry());
        setLocationName(lifeStream.getLocationName());
        setLocationCountry(lifeStream.getLocationCountry());
        setShareUrl(lifeStream.getShareUrl());
        setBackupLinkPreviewModelList(lifeStream.getBackupLinkPreviewModelList());
        setBackupMetaPreviewModelList(lifeStream.getBackupMetaPreviewModelList());
        setRawLinkPreviewModelList(lifeStream.getRawLinkPreviewModelList());
        setContentId(lifeStream.getContentId());
        setEditedMode(lifeStream.isEditedMode());
    }

    protected LifeStream(Parcel in) {
        this.mId = in.readString();
        long tmpMCreateDate = in.readLong();
        this.mCreateDate = tmpMCreateDate == -1 ? null : new Date(tmpMCreateDate);
        this.mPublish = in.readInt();
        this.mLatitude = (Double) in.readValue(Double.class.getClassLoader());
        this.mAddress = in.readString();
        this.mLongtitude = (Double) in.readValue(Double.class.getClassLoader());
        this.mMedia = in.createTypedArrayList(Media.CREATOR);
        this.mTitle = in.readString();
        this.mStoreUser = in.readParcelable(User.class.getClassLoader());
        this.mShareUrl = in.readString();
        this.mItemShape = in.readString();
        this.mCity = in.readString();
        this.mCountry = in.readString();
        this.mContentStat = in.readParcelable(ContentStat.class.getClassLoader());
        this.mLocationName = in.readString();
        this.mLocationCountry = in.readString();
        this.mBackupLinkPreviewModelList = in.createTypedArrayList(LinkPreviewModel.CREATOR);
        this.mContentId = in.readString();
        this.mBackupMetaPreviewModelList = in.readString();
        this.mIsEditedMode = (Boolean) in.readValue(Boolean.class.getClassLoader());
        /*
        Check to update meta preview from its string backup for JsonElement type can't be used in Parcel.
         */
        this.mMetaPreviewModelList = MetaPreviewHelper.getMetaPreviewListFromStringBackup(this.mBackupMetaPreviewModelList);
    }

    @Override
    public boolean isWelcomePostType() {
        return mIsWelcomePost;
    }

    public void setWelcomePost(boolean welcomePost) {
        mIsWelcomePost = welcomePost;
    }

    public boolean isEditedMode() {
        if (mIsEditedMode == null) {
            return false;
        }

        return mIsEditedMode;
    }

    public void setEditedMode(Boolean edited) {
        mIsEditedMode = edited;
    }

    public String getContentId() {
        return mContentId;
    }

    public void setContentId(String contentId) {
        mContentId = contentId;
    }


    public List<LinkPreviewModel> getBackupLinkPreviewModelList() {
        return mBackupLinkPreviewModelList;
    }

    @Override
    public boolean shouldShowPlacePreview() {
        return MetaPreviewHelper.shouldDisplayPreviewByType(BasePreviewModel.PreviewType.MAP,
                mMetaPreviewModelList);
    }

    public boolean isDeleted() {
        return mIsDeleted != null && mIsDeleted;
    }

    @Override
    public boolean shouldShowLinkPreview() {
        if (MetaPreviewHelper.shouldDisplayPreviewByType(BasePreviewModel.PreviewType.LINK,
                mMetaPreviewModelList)) {
            return true;
        }

        if (MetaPreviewHelper.shouldDisplayPreviewByType(BasePreviewModel.PreviewType.FILE,
                mMetaPreviewModelList)) {
            return true;
        }

        return false;
    }

    @Override
    public void setUserView(List<User> userView) {
        ((WallStreetAdaptive) this).setUserView(userView);
    }

    public void setBackupLinkPreviewModelList(List<LinkPreviewModel> backupLinkPreviewModelList) {
        mBackupLinkPreviewModelList = backupLinkPreviewModelList;
    }

    public void checkUpdateBackupOfMappingFields() {
        checkUpdateBackupMetaPreviewModelList();
    }

    /**
     * This method must be called once the meta preview element get updated or first mapping from
     * server response.
     */
    private void checkUpdateBackupMetaPreviewModelList() {
        mBackupMetaPreviewModelList = MetaPreviewHelper.getStringBackupOfMetaPreviewList(mMetaPreviewModelList);
        mBackupLinkPreviewModelList = MetaPreviewHelper.getMetaLinkPreview(mMetaPreviewModelList);
    }

    public List<LinkPreviewModel> getMetaPreviewModelList() {
        if (mBackupLinkPreviewModelList != null) {
            return mBackupLinkPreviewModelList;
        }

        if (mMetaPreviewModelList != null) {
            mBackupLinkPreviewModelList = MetaPreviewHelper.getMetaLinkPreview(mMetaPreviewModelList);
            return mBackupLinkPreviewModelList;
        }

        return new ArrayList<>();
    }

    public JsonElement getRawLinkPreviewModelList() {
        return mMetaPreviewModelList;
    }

    public String getBackupMetaPreviewModelList() {
        return mBackupMetaPreviewModelList;
    }

    public void setBackupMetaPreviewModelList(String backupMetaPreviewModelList) {
        mBackupMetaPreviewModelList = backupMetaPreviewModelList;
    }

    public void setRawLinkPreviewModelList(JsonElement jsonElement) {
        mMetaPreviewModelList = jsonElement;
        checkUpdateBackupMetaPreviewModelList();
    }

    @Override
    public LinkPreviewModel getLinkPreviewModel() {
        List<LinkPreviewModel> metaPreviewModelList = getMetaPreviewModelList();
        if (metaPreviewModelList != null && !metaPreviewModelList.isEmpty()) {
            /*
              Since in mobile app, the link preview will be rendered only one. So we can select from here.
             */
            return metaPreviewModelList.get(0);
        }

        return null;
    }

    @Override
    public String getLocationName() {
        return mLocationName;
    }

    @Override
    public void setLocationName(String locationName) {
        mLocationName = locationName;
    }

    @Override
    public String getLocationCountry() {
        return mLocationCountry;
    }

    @Override
    public void setLocationCountry(String locationCountry) {
        mLocationCountry = locationCountry;
    }

    @Override
    public String getPreviewContent() {
        return LinkPreviewRetriever.getLinkPreviewSource(this);
    }

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public void setId(String id) {
        mId = id;
    }

    @Override
    public Date getCreateDate() {
        return mCreateDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        mCreateDate = createDate;
    }

    @Override
    public PublishItem getPublish() {
        return PublishItem.getItem(mPublish);
    }

    @Override
    public void setPublish(int publish) {
        mPublish = publish;
    }

    @Override
    public ContentStat getContentStat() {
        if (mContentStat == null) {
            mContentStat = new ContentStat();
        }
        return mContentStat;
    }

    @Override
    public void setContentStat(ContentStat contentStat) {
        mContentStat = contentStat;
    }

    @Override
    public double getLatitude() {
        if (mLatitude == null) {
            return 0;
        }
        return mLatitude;
    }

    @Override
    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    @Override
    public String getAddress() {
        return mAddress;
    }

    @Override
    public void setAddress(String address) {
        mAddress = address;
    }

    @Override
    public String getCity() {
        return mCity;
    }

    @Override
    public void setCity(String city) {
        mCity = city;
    }

    @Override
    public double getLongitude() {
        if (mLongtitude == null) {
            return 0;
        }
        return mLongtitude;
    }

    @Override
    public void setLongitude(double longitude) {
        mLongtitude = longitude;
    }

    @Override
    public User getUser() {
        return mStoreUser;
    }

    @Override
    public List<Media> getMedia() {
        return mMedia;
    }

    public void setMedia(List<Media> media) {
        mMedia = media;
    }

    @Override
    public String getDescription() {
        return getTitle();
    }

    @Override
    public String getShareUrl() {
        return mShareUrl;
    }

    public void setShareUrl(String shareUrl) {
        mShareUrl = shareUrl;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public User getStoreUser() {
        return mStoreUser;
    }

    public void setStoreUser(User storeUser) {
        mStoreUser = storeUser;
    }

    public ItemShape getItemShape() {
        return ItemShape.fromValue(mItemShape);
    }

    public void setItemShape(ItemShape itemShape) {
        mItemShape = itemShape.getValue();
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof LifeStream && (obj == this || ((LifeStream) obj).getId().equals(getId()));
    }

    @Override
    public int hashCode() {
        return 0;
    }

    public boolean isFilePostTimelineType() {
        if (getMedia() != null && !getMedia().isEmpty()) {
            for (Media media1 : getMedia()) {
                if (media1.getType() == MediaType.FILE) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public ViewType getTimelineViewType() {
        if (getMedia() == null || getMedia().isEmpty() || isFilePostTimelineType()) {
            /*
                We have added one more view display in Wallstreet to display file post only and we
                consider this type as ViewType.Timeline and we will check to differentiate layout in
                the Wallstreet adapter.
             */
            return ViewType.Timeline;
        }
/*
    WallStreet Style 1
 */
//        if (getMedia().size() == 1) {
//            return ViewType.TimelineThreeOneItem;
//        } else if (getMedia().size() == 2) {
//            return ViewType.TimelineThreeTwoItem;
//        } else {
//            return ViewType.TimelineThreeThreeItem;
//        }

/*
    WallStreet Style 2
 */
        else {
            if (getMedia().size() == 1 && getMedia().get(0).getType() == MediaType.LIVE_VIDEO) {
                return ViewType.LiveVideoTimeline;
            } else {
                int firstWidth = getMedia().get(0).getWidth();
                int firstHeight = getMedia().get(0).getHeight();
                if (firstWidth != firstHeight) {
                    switch (getMedia().size()) {
                        case 1:
                            return ViewType.Timeline_FreeStyle_1;
                        case 2:
                            return firstWidth > firstHeight ?
                                    ViewType.Timeline_FreeStyle_2_Vertical :
                                    ViewType.Timeline_FreeStyle_2_Horizontal;
                        case 3:
                            return firstWidth > firstHeight ?
                                    ViewType.Timeline_FreeStyle_3_Vertical :
                                    ViewType.Timeline_FreeStyle_3_Horizontal;
                        case 4:
                            return firstWidth > firstHeight ?
                                    ViewType.Timeline_FreeStyle_4_Vertical :
                                    ViewType.Timeline_FreeStyle_4_Horizontal;
                        default:
                            return firstWidth > firstHeight ?
                                    ViewType.Timeline_FreeStyle_5_Vertical :
                                    ViewType.Timeline_FreeStyle_5_Horizontal;
                    }
                } else {
                    boolean isAllSquare = true;
                    for (int i = 1; i < getMedia().size(); i++) {
                        if (i > Format.SquareAll.getMaxDisplay() - 1) {
                            break;
                        }
                        if (getMedia().get(i).getWidth() != getMedia().get(i).getHeight()) {
                            isAllSquare = false;
                            break;
                        }
                    }
                    if (isAllSquare) {
                        switch (getMedia().size()) {
                            case 1:
                                return ViewType.Timeline_FreeStyle_1;
                            case 2:
                                return ViewType.Timeline_SquareAll_2;
                            case 3:
                                return ViewType.Timeline_SquareAll_3;
                            case 4:
                                return ViewType.Timeline_SquareAll_4;
                            default:
                                return ViewType.Timeline_SquareAll_5;
                        }
                    } else {
                        switch (getMedia().size()) {
                            case 1:
                                return ViewType.Timeline_FreeStyle_1;
                            case 2:
                                return ViewType.Timeline_SquareFirst_2;
                            case 3:
                                return ViewType.Timeline_SquareFirst_3;
                            case 4:
                                return ViewType.Timeline_SquareFirst_4;
                            default:
                                return ViewType.Timeline_SquareFirst_5;
                        }
                    }
                }
            }

        }
    }

    public void setRetrievedLinkPreviewResult(LinkPreviewModel previewResult) {
        mMetaPreviewModelList = MetaPreviewHelper.addOrUpdateLinkMetaPreviewItemState(mMetaPreviewModelList,
                LinkPreviewModel.newPostInstance(previewResult));
        //Update the backup data.
        checkUpdateBackupMetaPreviewModelList();
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting activity
        AbsBaseActivity activity = requiredIntentData.getActivity();
        TimelineDetailIntent intent = new TimelineDetailIntent(activity,
                this,
                shouldRequestCommentList() ? getId() : null,
                requiredIntentData.getMediaClickPosition(),
                TimelineDetailRedirectionType.FROM_WALL);

        requiredIntentData.getActivity().startActivityForResult(intent, new ResultCallback() {
            @Override
            public void onActivityResultSuccess(int resultCode, Intent data) {
                TimelineDetailIntentResult intentResult = new TimelineDetailIntentResult(data);
                requiredIntentData.onDataResultCallBack((Adaptive) intentResult.getLifeStream(),
                        intentResult.isRemoveItem());
            }
        });
    }

    @Override
    public void setUnreadCommentCounter(int counter) {
        if (getContentStat() != null) {
            getContentStat().setUnreadCommentCount(counter);
        }
    }

    private boolean shouldRequestCommentList() {
        //Will past the content id to the post detail for requesting comment list for a post that has
        //only text or one media
        return (getMedia() != null && getMedia().size() == 1) ||
                !TextUtils.isEmpty(getTitle()) ||
                isFilePostTimelineType();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeLong(this.mCreateDate != null ? this.mCreateDate.getTime() : -1);
        dest.writeInt(this.mPublish);
        dest.writeValue(this.mLatitude);
        dest.writeString(this.mAddress);
        dest.writeValue(this.mLongtitude);
        dest.writeTypedList(this.mMedia);
        dest.writeString(this.mTitle);
        dest.writeParcelable(this.mStoreUser, flags);
        dest.writeString(this.mShareUrl);
        dest.writeString(this.mItemShape);
        dest.writeString(this.mCity);
        dest.writeString(this.mCountry);
        dest.writeParcelable(this.mContentStat, flags);
        dest.writeString(this.mLocationName);
        dest.writeString(this.mLocationCountry);
        dest.writeTypedList(this.mBackupLinkPreviewModelList);
        dest.writeString(this.mContentId);
        dest.writeString(this.mBackupMetaPreviewModelList);
        dest.writeValue(this.mIsEditedMode);
    }

    @Override
    public boolean areItemsTheSame(LifeStream other) {
        return getId().matches(other.getId());
    }

    @Override
    public boolean areContentsTheSame(LifeStream other) {
        return areItemsTheSame(other) &&
                getStoreUser().areContentsTheSame(other.getStoreUser()) &&
                arePublishedDateTheSame(other) &&
                areUserViewTheSame(other) &&
                areContentStateTheSame(other) &&
                TextUtils.equals(getTitle(), other.getTitle()) &&
                TextUtils.equals(getShareUrl(), other.getShareUrl()) &&
                areMediaTheSame(other) &&
                isLinkPreviewTheSame(other) &&
                getPublish() == other.getPublish() &&
                areCheckLinkTheSame(other) &&
                (shouldShowLinkPreview() == other.shouldShowLinkPreview()) &&
                (shouldShowPlacePreview() == other.shouldShowPlacePreview());
    }

    private boolean areCheckLinkTheSame(LifeStream other) {
        return getLatitude() == other.getLatitude() && getLongitude() == other.getLongitude();
    }

    private boolean areUserViewTheSame(LifeStream other) {
        if (getUserView() == null && other.getUserView() == null) {
            return true;
        }

        if (getUserView() != null && other.getUserView() != null) {
            for (User user : getUserView()) {
                for (User user1 : other.getUserView()) {
                    if (user.getId().matches(user1.getId())) {
                        if (!user.areContentsTheSame(user1)) {
                            return false;
                        }

                        break;
                    }
                }
            }

            return true;
        }

        return false;
    }

    private boolean areContentStateTheSame(LifeStream other) {
        if (getContentStat() == null && other.getContentStat() == null) {
            return true;
        }

        return getContentStat().getTotalComments() == other.getContentStat().getTotalComments() &&
                getContentStat().getTotalLikes() == other.getContentStat().getTotalLikes();
    }

    private boolean arePublishedDateTheSame(LifeStream other) {
        if (getCreateDate() == null && other.getCreateDate() == null) {
            return true;
        }

        return getCreateDate() != null &&
                other.getCreateDate() != null &&
                getCreateDate().getTime() == other.getCreateDate().getTime();
    }

    private boolean isLinkPreviewTheSame(LifeStream other) {
        if (getLinkPreviewModel() == null && other.getLinkPreviewModel() == null) {
            return true;
        } else {
            return getLinkPreviewModel() != null &&
                    other.getLinkPreviewModel() != null &&
                    getLinkPreviewModel().areContentsTheSame(other.getLinkPreviewModel());
        }
    }

    private boolean areMediaTheSame(LifeStream other) {
        if ((getMedia() == null && other.getMedia() == null) ||
                (getMedia().isEmpty() && other.getMedia().isEmpty())) {
            return true;
        } else if (getMedia() != null && other.getMedia() != null && getMedia().size() == other.getMedia().size()) {
            for (Media media : getMedia()) {
                for (Media media1 : other.getMedia()) {
                    if (media.getId().matches(media1.getId())) {
                        if (!media.areContentsTheSame(media1)) {
                            return false;
                        }
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }
}