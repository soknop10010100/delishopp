package com.proapp.sompom.model.emun;

import android.content.Context;

import com.proapp.sompom.R;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.PostContextMenuItem;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.utils.AttributeConverter;
import com.resourcemanager.helper.FontHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuonveyo on 10/31/18.
 */

public enum PostContextMenuItemType {

    EDIT_POST_CLICK(1),
    EDIT_PRIVACY_CLICK(2),
    REPORT_CLICK(3),
    TURNOFF_NOTIFICATION_CLICK(4),
    DELETE_CLICK(5),

    FOLLOWER_CLICK(6),
    EVERYONE_CLICK(7),

    COPY_TEXT_CLICK(8),
    SAVE_IMAGE_CLICK(9),
    FORWARD_CLICK(11),
    SHARE_CLICK(12),

    WRITE_POST_CLICK(13),
    SENDTO_MESSENGER_CLICK(14),

    ARCHIVE_CONVERSATION(15),
    DELETE_CONVERSATION(16),

    UNFOLLOW_CLICK(17),

    REPLY_CLICK(18);

    private final int mId;

    PostContextMenuItemType(int id) {
        mId = id;
    }

    public static PostContextMenuItemType getItemType(int id) {
        for (PostContextMenuItemType postContextMenuItemType : PostContextMenuItemType.values()) {
            if (postContextMenuItemType.getId() == id) {
                return postContextMenuItemType;
            }
        }
        return EDIT_POST_CLICK;
    }

    public static List<PostContextMenuItem> getPostMenuItems(Context context,
                                                             boolean isOwner,
                                                             TimelinePostItem timelinePostItem) {
        List<PostContextMenuItem> items = new ArrayList<>();
        if (isOwner) {
            items.add(new PostContextMenuItem(PostContextMenuItemType.EDIT_POST_CLICK.getId(),
                    R.string.code_pencil,
                    R.string.post_menu_edit_post_title,
                    AttributeConverter.convertAttrToColor(context, R.attr.post_option_popup_edit_icon),
                    AttributeConverter.convertAttrToColor(context, R.attr.post_option_popup_text),
                    FontHelper.getMagicIconFontResource()));

            items.add(new PostContextMenuItem(PostContextMenuItemType.DELETE_CLICK.getId(),
                    R.string.code_trash,
                    R.string.post_menu_delete_post_title,
                    AttributeConverter.convertAttrToColor(context, R.attr.post_option_popup_delete_icon),
                    AttributeConverter.convertAttrToColor(context, R.attr.post_option_popup_text),
                    FontHelper.getMagicIconFontResource()));
        }

        return items;
    }

    public static List<PostContextMenuItem> getChatMenuItems(boolean isMultiImage,
                                                             boolean isShowDelete,
                                                             boolean isShowEdit,
                                                             Chat.Type type) {
        List<PostContextMenuItem> items = new ArrayList<>();
        if (isShowEdit) {
            items.add(new PostContextMenuItem(PostContextMenuItemType.EDIT_POST_CLICK.getId(),
                    R.string.code_pencil,
                    R.string.chat_popup_edit_title,
                    -1,
                    FontHelper.getMagicIconFontResource()));
        }

        items.add(new PostContextMenuItem(PostContextMenuItemType.REPLY_CLICK.getId(),
                R.string.code_reply_back,
                R.string.chat_popup_reply_title,
                -1,
                FontHelper.getMagicIconFontResource()));

        if (type == Chat.Type.TEXT) {
            items.add(new PostContextMenuItem(PostContextMenuItemType.COPY_TEXT_CLICK.getId(),
                    R.string.code_bookmark,
                    R.string.chat_popup_copy_text_title,
                    -1,
                    FontHelper.getMagicIconFontResource()));
        } else if (type == Chat.Type.IMAGE || type == Chat.Type.VIDEO || type == Chat.Type.MIX_MEDIA) {
            items.add(new PostContextMenuItem(PostContextMenuItemType.SAVE_IMAGE_CLICK.getId(),
                    R.string.code_bookmark,

                    isMultiImage ? R.string.chat_popup_save_all_image_title : R.string.chat_popup_save_title,
                    -1,
                    FontHelper.getMagicIconFontResource()));
        }
        items.add(new PostContextMenuItem(PostContextMenuItemType.FORWARD_CLICK.getId(),
                R.string.code_share,
                R.string.chat_popup_forward_title,
                -1,
                FontHelper.getFlatIconFontResource()));
        if (isShowDelete) {
            items.add(new PostContextMenuItem(PostContextMenuItemType.DELETE_CLICK.getId(),
                    R.string.code_trash,
                    R.string.chat_popup_remove_title,
                    -1,
                    FontHelper.getMagicIconFontResource()));
        }
        return items;
    }

    public static List<PostContextMenuItem> getChatOnlyRemoveMenuItem() {
        List<PostContextMenuItem> items = new ArrayList<>();
        items.add(new PostContextMenuItem(PostContextMenuItemType.DELETE_CLICK.getId(),
                R.string.code_trash,
                R.string.seller_store_button_delete,
                -1,
                FontHelper.getMagicIconFontResource()));

        return items;
    }

    public static List<PostContextMenuItem> getItemsShare(boolean isIncludeShare) {
        List<PostContextMenuItem> items = new ArrayList<>();
        if (isIncludeShare) {
            items.add(new PostContextMenuItem(PostContextMenuItemType.SENDTO_MESSENGER_CLICK.getId(),
                    R.string.code_comment,
                    R.string.post_menu_send_to_messenger));
            items.add(new PostContextMenuItem(PostContextMenuItemType.COPY_TEXT_CLICK.getId(),
                    R.string.code_bookmark,
                    R.string.post_menu_copy_link));
        }
        return items;
    }

    public static List<PostContextMenuItem> getCommentPopupMenuItem(Comment comment, boolean isOwner) {
        List<PostContextMenuItem> items = new ArrayList<>();
        boolean isCommentMedia = isCommentMedia(comment);
        if (!isCommentMedia) {
            items.add(new PostContextMenuItem(PostContextMenuItemType.COPY_TEXT_CLICK.getId(),
                    R.string.code_bookmark,
                    R.string.comment_popup_menu_copy,
                    -1,
                    FontHelper.getMagicIconFontResource()));
        }
        if (isOwner) {
            if (!isCommentMedia) {
                items.add(new PostContextMenuItem(PostContextMenuItemType.EDIT_POST_CLICK.getId(),
                        R.string.code_pencil,
                        R.string.comment_popup_menu_edit,
                        -1,
                        FontHelper.getMagicIconFontResource()));
            }
            items.add(new PostContextMenuItem(PostContextMenuItemType.DELETE_CLICK.getId(),
                    R.string.code_trash,
                    R.string.comment_popup_menu_delete,
                    -1,
                    FontHelper.getMagicIconFontResource()));
        }
        return items;
    }

    private static boolean isCommentMedia(Comment comment) {
        if (comment.getMedia() != null && !comment.getMedia().isEmpty()) {
            Media media = comment.getMedia().get(0);
            return media.getType() == MediaType.TENOR_GIF || media.getType() == MediaType.IMAGE;
        }

        return false;
    }

    public static List<PostContextMenuItem> getConversationPopupMenuItem(Context context) {
        List<PostContextMenuItem> items = new ArrayList<>();
        items.add(new PostContextMenuItem(PostContextMenuItemType.ARCHIVE_CONVERSATION.getId(),
                R.string.code_trash,
                R.string.conversation_screen_archive_menu_title,
                AttributeConverter.convertAttrToColor(context, R.attr.conversation_popup_archive_icon),
                AttributeConverter.convertAttrToColor(context, R.attr.conversation_popup_archive_text),
                FontHelper.getMagicIconFontResource()));

        //TODO: currently, Delete feature is postponed
//        items.add(new PostContextMenuItem(PostContextMenuItemType.DELETE_CONVERSATION.getId(),
//                R.string.code_trash,
//                R.string.seller_store_button_delete));

        return items;
    }


    public int getId() {
        return mId;
    }
}
