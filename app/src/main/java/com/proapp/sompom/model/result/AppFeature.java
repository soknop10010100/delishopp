package com.proapp.sompom.model.result;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.DimenRes;
import androidx.annotation.FontRes;
import androidx.annotation.StringRes;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.R;

import java.util.List;

/**
 * Created by Or Vitovongsak on 13/1/22.
 */
public class AppFeature {

    @SerializedName("navigationBar")
    List<NavigationBarMenu> mNavigationBarMenu;

    public List<NavigationBarMenu> getNavigationBarMenu() {
        return mNavigationBarMenu;
    }

    public void setNavigationBarMenu(List<NavigationBarMenu> menus) {
        mNavigationBarMenu = menus;
    }

    public static class NavigationBarMenu {

        @SerializedName("navButton")
        private String mNavButton;

        @SerializedName("navScreen")
        private String mNavScreen;

        @SerializedName("isEnabled")
        private boolean mIsEnabled;

        public NavigationBarMenu(String navButton,
                                 String navScreen,
                                 boolean isEnabled) {
            mNavButton = navButton;
            mNavScreen = navScreen;
            mIsEnabled = isEnabled;
        }

        public void setNavButton(String navButton) {
            mNavButton = navButton;
        }

        public NavMenuButton getNavMenuButton() {
            return NavMenuButton.fromValue(mNavButton);
        }

        public NavMenuScreen getNavMenuScreen() {
            return NavMenuScreen.fromValue(mNavScreen);
        }

        public boolean isEnabled() {
            return mIsEnabled;
        }
    }

    public enum NavMenuButton {
        Wall("wall", R.string.code_normal_wall, R.string.code_selected_wall, R.dimen.text_xxlarge, R.font.flaticon2),
        Conversation("conversation", R.string.code_normal_message, R.string.code_selected_message, R.dimen.text_xxlarge, R.font.flaticon2),
        Contact("contact", R.string.code_normal_contact, R.string.code_selected_contact, R.dimen.text_xlarge, R.font.flaticon2),
        SingleShop("singleShop", R.string.fi_rr_shop, R.string.fi_rr_shop, R.dimen.text_20sp, R.font.uicons_regular_rounded, R.font.uicons_solid_rounded),
        MultiShop("multiShop", R.string.fi_rr_shop, R.string.fi_sr_shop, R.dimen.text_20sp, R.font.uicons_regular_rounded, R.font.uicons_solid_rounded),
        ShopCategory("category", R.string.fi_rr_apps, R.string.fi_sr_apps, R.dimen.text_20sp, R.font.uicons_regular_rounded, R.font.uicons_solid_rounded),
        ShopCategoryDetail("shopCategory", R.string.fi_rr_apps, R.string.fi_sr_apps, R.dimen.text_20sp, R.font.uicons_regular_rounded, R.font.uicons_solid_rounded),
        ShopDiscount("discount", R.string.fi_rr_flame, R.string.fi_sr_flame, R.dimen.text_20sp, R.font.uicons_regular_rounded, R.font.uicons_solid_rounded),
        Support("support", R.string.fi_rr_comment_alt, R.string.fi_sr_comment_alt, R.dimen.text_20sp, R.font.uicons_regular_rounded, R.font.uicons_solid_rounded),
        NormalMode("normalMode", R.string.fi_rr_shop, R.string.fi_rr_shop, R.dimen.text_20sp, R.font.uicons_regular_rounded, R.font.uicons_solid_rounded),
        ExpressMode("expressMode", R.string.flaticon_urgent, R.string.flaticon_urgent, R.dimen.text_20sp, R.font.flaticon2, R.font.flaticon2),

        //Just make up in local to change icon of shop home in express mode.
        ExpressShop("expressShop", R.string.flaticon_urgent, R.string.flaticon_urgent, R.dimen.text_xxlarge, R.font.flaticon2, R.font.flaticon2);

        private String mKey;
        @StringRes
        private int mNormalIcon;
        @StringRes
        private int mSelectedIcon;
        @DimenRes
        private int mIconSize;
        @FontRes
        private int mNormalFont;
        @FontRes
        private int mSelectedFont;

        NavMenuButton(String key,
                      int normalIcon,
                      int selectedIcon,
                      int iconSize,
                      int normalFont) {
            mKey = key;
            mNormalIcon = normalIcon;
            mSelectedIcon = selectedIcon;
            mIconSize = iconSize;
            mNormalFont = normalFont;
            mSelectedFont = normalFont;
        }

        NavMenuButton(String key,
                      int normalIcon,
                      int selectedIcon,
                      int iconSize,
                      int normalFont,
                      int selectedFont) {
            mKey = key;
            mNormalIcon = normalIcon;
            mSelectedIcon = selectedIcon;
            mIconSize = iconSize;
            mNormalFont = normalFont;
            mSelectedFont = selectedFont;
        }

        public static NavMenuButton fromValue(String value) {
            for (NavMenuButton type : NavMenuButton.values()) {
                if (TextUtils.equals(type.mKey, value)) {
                    return type;
                }
            }
            return null;
        }

        public String getKey() {
            return mKey;
        }

        public int getNormalIcon() {
            return mNormalIcon;
        }

        public int getSelectedIcon() {
            return mSelectedIcon;
        }

        public int getIconSize(Context context) {
            return context.getResources().getDimensionPixelSize(mIconSize);
        }

        public int getNormalFont() {
            return mNormalFont;
        }

        public int getSelectedFont() {
            return mSelectedFont;
        }
    }

    public enum NavMenuScreen {
        Wall("wall"),
        Conversation("conversation"),
        Contact("contact"),
        Shop("shop"),
        ShopDetail("shopDetail"),
        ShopCategory("categoryDetail"),
        Chat("chat"),
        NormalMode("normalMode"),
        ExpressMode("expressMode");

        private String mKey;

        NavMenuScreen(String key) {
            mKey = key;
        }

        public static NavMenuScreen fromValue(String value) {
            for (NavMenuScreen type : NavMenuScreen.values()) {
                if (TextUtils.equals(type.mKey, value)) {
                    return type;
                }
            }
            return null;
        }

        public String getKey() {
            return mKey;
        }

        /**
         * Determine if the NavMenuScreen is an option which should open a new screen
         *
         * @param navMenuScreen The {@NavMenuScreen} to check
         * @return true if the menu should open a new screen
         */
        public static boolean isOpenNewScreenType(NavMenuScreen navMenuScreen) {
            return navMenuScreen == ShopDetail ||
                    navMenuScreen == ExpressMode ||
                    navMenuScreen == NormalMode ||
                    navMenuScreen == Chat;
        }
    }
}
