package com.proapp.sompom.model;

import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;
import java.time.LocalTime;

public class ShopDeliveryTime extends AbsConciergeModel {

    private LocalTime mStartTime;
    private LocalTime mEndTime;
    private LocalDateTime mSlotDate;

    @SerializedName(AbsConciergeModel.FIELD_START_TIME)
    private String mStartTimeString;

    @SerializedName(AbsConciergeModel.FIELD_SLOT_DATE)
    private String mSlotDateString;

    @SerializedName(AbsConciergeModel.FIELD_END_TIME)
    private String mEndTimeString;

    @SerializedName(AbsConciergeModel.FIELD_IS_DELETED)
    private boolean mIsDeleted;

    @SerializedName(AbsConciergeModel.FIELD_IS_SLOT_EXPIRED)
    private boolean mIsSlotExpired;

    @SerializedName(AbsConciergeModel.FIELD_IS_STOP_ACCEPTING_ORDER)
    private boolean mStopAcceptingOrder;

    @SerializedName(AbsConciergeModel.FIELD_AVAILABLE_PROVINCE)
    private boolean mAvailableProvince;

    private boolean mIsSelected;
    private boolean mIsVisible;
    // Local data to determine if selected slot is province slot
    private boolean mIsProvinceTime;

    public ShopDeliveryTime() {
    }

    public LocalDateTime getSlotDate() {
        return mSlotDate;
    }

    public void setSlotDate(LocalDateTime slotDate) {
        mSlotDate = slotDate;
    }

    public boolean isVisible() {
        return mIsVisible;
    }

    public void setIsVisible(boolean isVisible) {
        this.mIsVisible = isVisible;
    }

    public LocalTime getStartTime() {
        return mStartTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.mStartTime = startTime;
    }

    public LocalTime getEndTime() {
        return mEndTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.mEndTime = endTime;
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    public void setSelected(boolean selected) {
        mIsSelected = selected;
    }

    public String getSlotDateString() {
        return mSlotDateString;
    }

    public void setSlotDateString(String slotDateString) {
        mSlotDateString = slotDateString;
    }

    public String getStartTimeString() {
        return mStartTimeString;
    }

    public void setStartTimeString(String startTimeString) {
        this.mStartTimeString = startTimeString;
    }

    public String getEndTimeString() {
        return mEndTimeString;
    }

    public void setEndTimeString(String endTimeString) {
        this.mEndTimeString = endTimeString;
    }

    public boolean isDeleted() {
        return mIsDeleted;
    }

    public void setDeleted(boolean deleted) {
        mIsDeleted = deleted;
    }

    public boolean isSlotExpired() {
        return mIsSlotExpired;
    }

    public void setSlotExpired(boolean slotExpired) {
        mIsSlotExpired = slotExpired;
    }

    public boolean isStopAcceptingOrder() {
        return mStopAcceptingOrder;
    }

    public void setStopAcceptingOrder(boolean stopAcceptingOrder) {
        mStopAcceptingOrder = stopAcceptingOrder;
    }

    public boolean isAvailableProvince() {
        return mAvailableProvince;
    }

    public void setIsAvailableProvince(boolean isAvailableProvince) {
        mAvailableProvince = isAvailableProvince;
    }

    public void setVisible(boolean visible) {
        mIsVisible = visible;
    }

    public boolean isDisable() {
        return mIsSlotExpired || mIsDeleted || mStopAcceptingOrder;
    }

    public boolean isProvinceTime() {
        return mIsProvinceTime;
    }

    public void setIsProvinceTime(boolean isProvinceTime) {
        mIsProvinceTime = isProvinceTime;
    }

    @Override
    public String toString() {
        return "ShopDeliveryTime{" +
                "mStartTime=" + mStartTime +
                ", mEndTime=" + mEndTime +
                ", mStartTimeString='" + mStartTimeString + '\'' +
                ", mEndTimeString='" + mEndTimeString + '\'' +
                ", mIsSelected=" + mIsSelected +
                ", mIsVisible=" + mIsVisible +
                '}';
    }
}
