package com.proapp.sompom.model;

import com.google.gson.annotations.SerializedName;

public abstract class AbsConciergeModel {

    public static final String FIELD_ID = "_id";
    public static final String FIELD_DATA = "data";
    public static final String FIELD_SHOP_ID = "shopId";
    public static final String FIELD_SHOP_LOGO = "shopLogo";
    public static final String FIELD_LOGO = "logo";
    public static final String FIELD_ITEM_ID = "itemId";
    public static final String FIELD_PREVIEW = "preview";
    public static final String FIELD_LABEL = "label";
    public static final String FIELD_ICON = "icon";
    public static final String FIELD_TYPE = "type";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_PRICE = "price";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_DESCRIPTION = "description";
    public static final String FIELD_LIST = "list";
    public static final String FIELD_QUANTITY = "qty";
    public static final String FIELD_INSTRUCTION = "instruction";
    public static final String FIELD_OPTIONS = "options";
    public static final String FIELD_CHOICES = "choices";
    public static final String FIELD_RECEIVE_DATE = "receiveDate";
    public static final String FIELD_RECEIVE_TYPE = "receiveType";
    public static final String FIELD_DELIVERY_INSTRUCTION = "deliveryInstruction";
    public static final String FIELD_START_TIME = "startTime";
    public static final String FIELD_END_TIME = "endTime";
    public static final String FIELD_IS_DELETED = "isDeleted";
    public static final String FIELD_IS_SLOT_EXPIRED = "slotExpired";
    public static final String FIELD_IS_STOP_ACCEPTING_ORDER = "stopAcceptingOrders";
    public static final String FIELD_AVAILABLE_PROVINCE = "availableProvince";
    public static final String FIELD_CREATED_AT = "createdAt";
    public static final String FIELD_PICTURE = "picture";
    public static final String FIELD_SHOP = "shop";
    public static final String FIELD_SHOPS = "shops";
    public static final String FIELD_DEFAULT_PRICE = "defaultPrice";
    public static final String FIELD_IS_DISPLAY_FULL_PRICE = "displayFullPrice";
    public static final String FIELD_HASH = "hash";
    public static final String FIELD_SHOP_HASH = "shopHash";
    public static final String FIELD_DELIVERY_ADDRESS = "deliveryAddress";
    public static final String FIELD_LATITUDE = "latitude";
    public static final String FIELD_LONGITUDE = "longitude";
    public static final String FIELD_TOTAL_DISCOUNT = "totalDiscount";
    public static final String FIELD_ORIGINAL_PRICE = "originalPrice";
    public static final String FIELD_MENU_ITEMS = "menuItems";
    public static final String FIELD_PREFERENCE_DELIVERY = "preferenceDelivery";
    public static final String FIELD_PAYMENT_METHOD = "paymentMethod";
    public static final String FIELD_PAYMENT_PROVIDER = "paymentProvider";
    public static final String FIELD_SLOT_ID = "slotId";
    public static final String FIELD_RECEIPT_URL = "receiptUrl";
    public static final String FIELD_IS_DISABLED = "isDisabled";
    public static final String FIELD_TOTAL = "total";
    public static final String FIELD_SUB_TOTAL = "subTotal";
    public static final String FIELD_DELIVERY_FEE = "deliveryFee";
    public static final String FIELD_CONTAINER_FEE = "containerFee";
    public static final String FIELD_BASKET_MENU_ITEMS = "basketItems";
    public static final String FIELD_PAYMENT_FORM = "paymentForm";
    public static final String FIELD_MENU_ITEM = "menuItem";
    public static final String FIELD_ORDER_ID = "orderId";
    public static final String FIELD_IS_SAME_DAY = "isSameDay";
    public static final String FIELD_COUPON_ID = "couponCodeId";
    public static final String FIELD_COUPON_CODE = "couponCode";
    public static final String FIELD_TRANSACTION_ID = "transactionId";
    public static final String FIELD_OOS_OPTION = "oosOption";
    public static final String FIELD_ADDRESS_ID = "addressId";
    public static final String FIELD_PAYMENT_OPTION = "paymentOption";
    public static final String FIELD_WEIGHT = "weight";
    public static final String FIELD_SLOT_DATE = "date";
    public static final String FIELD_NEXT_PAGE = "next";
    public static final String FIELD_IMAGE = "image";
    public static final String FIELD_IS_IN_STOCK = "isInStock";
    public static final String FIELD_REDIRECTION_URL = "redirect";
    public static final String FIELD_SUPPLIER_CODE = "supplierCode";
    public static final String FIELD_PHONE = "phone";
    public static final String FIELD_SUPPLIER_LOGO = "supplierLogo";
    public static final String FIELD_SUPPLIER_ID = "supplierId";
    public static final String FIELD_ALLOW_MULTIPLE_BASKET = "allowMultiBasket";
    public static final String FIELD_SUPPLIER = "supplier";
    public static final String FIELD_DRIVER_ID = "driverId";
    public static final String FIELD_IS_APPLY_WALLET = "isApplyWallet";
    public static final String FIELD_AMOUNT = "amount";
    public static final String FIELD_ORDER_AVAILABLE = "orderAvailable";
    public static final String FIELD_ORDER_NEED_PAY = "needPay";
    public static final String FIELD_WALLET_DISCOUNT = "walletDiscount";
    public static final String FIELD_NEW = "new";
    public static final String FIELD_USER_ID = "userId";
    public static final String FIELD_EXPRESS = "express";
    public static final String FIELD_ORDER = "order";
    public static final String FIELD_START_DELIVERY_TIME = "startDeliveryTime";
    public static final String FIELD_END_DELIVERY_TIME = "endDeliveryTime";
    public static final String FIELD_ESTIMATE_TIME_DELIVERY_MINUTE = "estimateDeliveryMinute";
    public static final String FIELD_ADDRESS = "address";
    public static final String FIELD_BRAND = "brand";
    public static final String FIELD_WALLET_AMOUNT = "walletAmount";
    public static final String FIELD_IS_SHOP_OPEN = "isShopOpen";
    public static final String FIELD_AVAILABLE_TOMORROW = "availableTommorrow";
    public static final String FIELD_NEXT_AVAILABLE_DAY = "nextAvailableDay";
    public static final String FIELD_IS_PROVINCE = "isProvince";
    public static final String FIELD_IS_DELIVER_TO_PROVINCE = "isDeliverToProvince";

    @SerializedName(FIELD_ID)
    protected String mId;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }
}
