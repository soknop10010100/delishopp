package com.proapp.sompom.model.emun;

import android.text.TextUtils;

/**
 * Created by nuonveyo
 * on 1/18/19.
 */

public enum OneSignalPushType {

    Message("message"),
    synchronization("synchronize"),
    Call("call"),
    JoinCall("joinCall"),
    OrderProcessing("orderProcessing"),
    OrderDelivering("orderDelivering"),
    OrderArrived("orderArrived"),
    OrderDelivered("orderDelivered"),
    ExpressSlotFree("expressSlotFree"),
    CustomPush("customPush"),
    ShopStatus("shopStatus"),
    WalletUpdate("walletUpdate"),
    Unknown("unknown");

    private String mType;

    OneSignalPushType(String type) {
        mType = type;
    }

    public static OneSignalPushType getType(String type) {
        for (OneSignalPushType type1 : OneSignalPushType.values()) {
            if (TextUtils.equals(type1.getType(), type)) {
                return type1;
            }
        }
        return Unknown;
    }

    public String getType() {
        return mType;
    }
}
