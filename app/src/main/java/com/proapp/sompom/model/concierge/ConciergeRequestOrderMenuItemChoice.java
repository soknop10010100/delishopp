package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;

import java.util.List;

public class ConciergeRequestOrderMenuItemChoice {

    @SerializedName(AbsConciergeModel.FIELD_ID)
    private String mId;

    @SerializedName(AbsConciergeModel.FIELD_OPTIONS)
    private List<String> mOptionIds;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public List<String> getOptionIds() {
        return mOptionIds;
    }

    public void setOptionIds(List<String> optionIds) {
        this.mOptionIds = optionIds;
    }
}
