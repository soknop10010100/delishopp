package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Chhom Veasna on 7/8/2020.
 */
public class AddOrRemoveGroupParticipantBody {

    @SerializedName("memberList")
    private List<String> mParticipantListIds;

    @SerializedName("groupId")
    private String mGroupId;

    public AddOrRemoveGroupParticipantBody(String groupId, List<String> participantListIds) {
        mParticipantListIds = participantListIds;
        mGroupId = groupId;
    }

    public List<String> getParticipantListIds() {
        return mParticipantListIds;
    }

    public void setParticipantListIds(List<String> participantListIds) {
        mParticipantListIds = participantListIds;
    }

    public String getGroupId() {
        return mGroupId;
    }

    public void setGroupId(String groupId) {
        mGroupId = groupId;
    }
}
