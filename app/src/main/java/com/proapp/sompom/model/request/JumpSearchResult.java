package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.result.Chat;

import java.util.List;

/**
 * Created by Chhom Veasna on 18/12/20.
 */
public class JumpSearchResult {

    @SerializedName("list")
    private List<Chat> mData;

    @SerializedName("next")
    private String mNextPage;

    @SerializedName("limitMessageId")
    private String mLimitMessageId;

    public List<Chat> getData() {
        return mData;
    }

    public String getNextPage() {
        return mNextPage;
    }

    public String getLimitMessageId() {
        return mLimitMessageId;
    }
}
