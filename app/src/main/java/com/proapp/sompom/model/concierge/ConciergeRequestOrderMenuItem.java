package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;

import java.util.List;

public class ConciergeRequestOrderMenuItem {

    @SerializedName(AbsConciergeModel.FIELD_ID)
    private String mBasketId;

    @SerializedName(AbsConciergeModel.FIELD_ITEM_ID)
    private String mItemId;

    @SerializedName(AbsConciergeModel.FIELD_QUANTITY)
    private int mQuantity;

    @SerializedName(AbsConciergeModel.FIELD_INSTRUCTION)
    private String mInstruction;

    @SerializedName(AbsConciergeModel.FIELD_OPTIONS)
    private List<String> mOptionId;

    @SerializedName(AbsConciergeModel.FIELD_CHOICES)
    private List<ConciergeRequestOrderMenuItemChoice> mChoices;

    @SerializedName(AbsConciergeModel.FIELD_HASH)
    private Integer mItemHash;

    @SerializedName(AbsConciergeModel.FIELD_SHOP_HASH)
    private Integer mShopHash;

    @SerializedName(AbsConciergeModel.FIELD_DEFAULT_PRICE)
    private float mPrice;

    public String getItemId() {
        return mItemId;
    }

    public void setItemId(String itemId) {
        mItemId = itemId;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public void setQuantity(int quantity) {
        mQuantity = quantity;
    }

    public String getInstruction() {
        return mInstruction;
    }

    public void setInstruction(String instruction) {
        mInstruction = instruction;
    }

    public List<String> getOptionId() {
        return mOptionId;
    }

    public void setOptionId(List<String> optionId) {
        mOptionId = optionId;
    }

    public List<ConciergeRequestOrderMenuItemChoice> getChoices() {
        return mChoices;
    }

    public void setChoices(List<ConciergeRequestOrderMenuItemChoice> choices) {
        this.mChoices = choices;
    }

    public void setItemHash(Integer itemHash) {
        mItemHash = itemHash;
    }

    public void setShopHash(Integer shopHash) {
        mShopHash = shopHash;
    }

    public void setBasketId(String basketId) {
        mBasketId = basketId;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        mPrice = price;
    }
}
