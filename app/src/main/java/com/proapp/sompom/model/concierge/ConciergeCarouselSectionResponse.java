package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.DifferentAdaptive;

import java.util.List;

public class ConciergeCarouselSectionResponse extends AbsConciergeSectionResponse
        implements Parcelable, DifferentAdaptive<ConciergeCarouselSectionResponse> {

    @SerializedName(FIELD_DATA)
    private List<Data> mData;

    protected ConciergeCarouselSectionResponse(Parcel in) {
        mData = in.createTypedArrayList(Data.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mData);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConciergeCarouselSectionResponse> CREATOR = new Creator<ConciergeCarouselSectionResponse>() {
        @Override
        public ConciergeCarouselSectionResponse createFromParcel(Parcel in) {
            return new ConciergeCarouselSectionResponse(in);
        }

        @Override
        public ConciergeCarouselSectionResponse[] newArray(int size) {
            return new ConciergeCarouselSectionResponse[size];
        }
    };

    public List<Data> getData() {
        return mData;
    }

    public void setData(List<Data> data) {
        mData = data;
    }

    @Override
    public boolean areItemsTheSame(ConciergeCarouselSectionResponse other) {
        return getData().size() == other.getData().size();
    }

    @Override
    public boolean areContentsTheSame(ConciergeCarouselSectionResponse other) {
        return areDataTheSame(other.getData());
    }

    private boolean areDataTheSame(List<Data> other) {
        if ((getData() == null && other == null) ||
                (getData().isEmpty() && other.isEmpty())) {
            return true;
        } else if (getData() != null && other != null
                && getData().size() == other.size()) {

            for (int index = 0; index < getData().size(); index++) {
                if (!getData().get(index).areContentsTheSame(other.get(index))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public static class Data implements Parcelable, DifferentAdaptive<Data> {

        @SerializedName(FIELD_PREVIEW)
        private String mPreview;

        @SerializedName(FIELD_TYPE)
        private String mType;

        @SerializedName(FIELD_SHOP_ID)
        private String mShopId;

        @SerializedName(FIELD_ITEM_ID)
        private String mItemId;

        @SerializedName(FIELD_REDIRECTION_URL)
        private String mRedirectionUrl;

        protected Data(Parcel in) {
            mPreview = in.readString();
            mType = in.readString();
            mShopId = in.readString();
            mItemId = in.readString();
            mRedirectionUrl = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mPreview);
            dest.writeString(mType);
            dest.writeString(mShopId);
            dest.writeString(mItemId);
            dest.writeString(mRedirectionUrl);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        public String getRedirectionUrl() {
            return mRedirectionUrl;
        }

        public String getPreview() {
            return mPreview;
        }

        public void setPreview(String preview) {
            mPreview = preview;
        }

        public String getType() {
            return mType;
        }

        public void setType(String type) {
            this.mType = type;
        }

        public String getShopId() {
            return mShopId;
        }

        public void setShopId(String shopId) {
            mShopId = shopId;
        }

        public String getItemId() {
            return mItemId;
        }

        public void setItemId(String itemId) {
            mItemId = itemId;
        }

        @Override
        public boolean areItemsTheSame(Data other) {
            // Here we're checking if both of them are shop type or item type based on their shopId
            // and itemId property, then we'll check if they are the same item based on the ID.
            return !TextUtils.isEmpty(getShopId()) &&
                    !TextUtils.isEmpty(other.getShopId()) &&
                    TextUtils.equals(getShopId(), other.getShopId()) ||
                    !TextUtils.isEmpty(getItemId()) &&
                            !TextUtils.isEmpty(other.getItemId()) &&
                            TextUtils.equals(getItemId(), other.getItemId());
        }

        @Override
        public boolean areContentsTheSame(Data other) {
            return TextUtils.equals(getType(), other.getType()) &&
                    TextUtils.equals(getPreview(), other.getPreview());
        }
    }
}
