package com.proapp.sompom.model.concierge;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.DifferentAdaptive;

public class ConciergeOurServiceSectionResponse extends AbsConciergeSectionResponse
        implements DifferentAdaptive<ConciergeOurServiceSectionResponse> {

    @SerializedName(FIELD_DATA)
    private Data mData;

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    @Override
    public boolean areItemsTheSame(ConciergeOurServiceSectionResponse other) {
        return getData().areItemsTheSame(other.getData());
    }

    @Override
    public boolean areContentsTheSame(ConciergeOurServiceSectionResponse other) {
        return getData().areContentsTheSame(other.getData());
    }

    public static class Data implements DifferentAdaptive<Data> {

        private static final String FIELD_HAS_PENDING = "hasPending";

        @SerializedName(FIELD_LABEL)
        private String mLabel;

        @SerializedName(FIELD_ICON)
        private String mIcon;

        @SerializedName(FIELD_SHOP_ID)
        private String mShopId;

        @SerializedName(FIELD_HAS_PENDING)
        private boolean mIsHasPending;

        public String getLabel() {
            return mLabel;
        }

        public void setLabel(String label) {
            mLabel = label;
        }

        public String getIcon() {
            return mIcon;
        }

        public void setIcon(String icon) {
            mIcon = icon;
        }

        public String getShopId() {
            return mShopId;
        }

        public void setShopId(String shopId) {
            mShopId = shopId;
        }

        public boolean isHasPending() {
            return mIsHasPending;
        }

        public void setIsHasPending(boolean isHasPending) {
            this.mIsHasPending = isHasPending;
        }

        @Override
        public boolean areItemsTheSame(Data other) {
            return TextUtils.equals(getShopId(), other.getShopId());
        }

        @Override
        public boolean areContentsTheSame(Data other) {
            return areItemsTheSame(other) &&
                    TextUtils.equals(getLabel(), other.getLabel()) &&
                    TextUtils.equals(getIcon(), other.getIcon()) &&
                    TextUtils.equals(getShopId(), other.getShopId()) &&
                    isHasPending() == other.isHasPending();
        }
    }
}
