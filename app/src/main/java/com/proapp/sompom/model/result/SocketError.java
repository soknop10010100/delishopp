package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;

public class SocketError {

    @SerializedName("name")
    private String mName;

    @SerializedName("message")
    private String mMessage;

    public String getName() {
        return mName;
    }

    public String getMessage() {
        return mMessage;
    }
}
