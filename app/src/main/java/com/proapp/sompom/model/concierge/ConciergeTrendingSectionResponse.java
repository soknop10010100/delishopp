package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.DifferentAdaptive;

import java.util.List;

public class ConciergeTrendingSectionResponse extends AbsConciergeSectionResponse
        implements Parcelable, DifferentAdaptive<ConciergeTrendingSectionResponse> {

    private final static String NEXT = "next";

    @SerializedName(FIELD_DATA)
    private List<ConciergeMenuItem> mData;

    @SerializedName(NEXT)
    private String mNextPage;

    protected ConciergeTrendingSectionResponse(Parcel in) {
        mData = in.createTypedArrayList(ConciergeMenuItem.CREATOR);
        mNextPage = in.readString();
    }

    public static final Creator<ConciergeTrendingSectionResponse> CREATOR = new Creator<ConciergeTrendingSectionResponse>() {
        @Override
        public ConciergeTrendingSectionResponse createFromParcel(Parcel in) {
            return new ConciergeTrendingSectionResponse(in);
        }

        @Override
        public ConciergeTrendingSectionResponse[] newArray(int size) {
            return new ConciergeTrendingSectionResponse[size];
        }
    };

    public String getNextPage() {
        return mNextPage;
    }

    public void setNextPage(String nextPage) {
        mNextPage = nextPage;
    }

    public List<ConciergeMenuItem> getData() {
        return mData;
    }

    public void setData(List<ConciergeMenuItem> data) {
        mData = data;
    }

    @Override
    public boolean areItemsTheSame(ConciergeTrendingSectionResponse other) {
        return mData.size() == other.getData().size();
//        return TextUtils.equals(getTitle(), other.getTitle());
    }

    @Override
    public boolean areContentsTheSame(ConciergeTrendingSectionResponse other) {
        return areDataTheSame(other.getData());
    }

    private boolean areDataTheSame(List<ConciergeMenuItem> other) {
        if ((getData() == null && other == null) ||
                (getData().isEmpty() && other.isEmpty())) {
            return true;
        } else if (getData() != null && other != null
                && getData().size() == other.size()) {
            for (int index = 0; index < getData().size(); index++) {
                if (!getData().get(index).areContentsTheSame(other.get(index))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mData);
        dest.writeString(mNextPage);
    }
}