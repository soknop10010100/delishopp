package com.proapp.sompom.model.emun;

import android.text.TextUtils;

/**
 * Created by Veasna Chhom on 11/3/22.
 */
public enum ConciergeOnlinePaymentOption {

    ABA_PAY("ABA_PAY"),
    CARD("ABA_CARD"),
    KHQR("BAKONG"),
    UNKNOWN("UNKNOWN");

    private final String mValue;

    ConciergeOnlinePaymentOption(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public static ConciergeOnlinePaymentOption fromValue(String value) {
        for (ConciergeOnlinePaymentOption type : ConciergeOnlinePaymentOption.values()) {
            if (TextUtils.equals(type.mValue, value)) {
                return type;
            }
        }

        return UNKNOWN;
    }
}
