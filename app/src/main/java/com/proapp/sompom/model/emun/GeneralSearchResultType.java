package com.proapp.sompom.model.emun;

public enum GeneralSearchResultType {
    ALL(1),
    PEOPLE(2),
    MESSAGE(3);

    private int mValue;

    GeneralSearchResultType(int value) {
        mValue = value;
    }

    public int getValue() {
        return mValue;
    }

    public static GeneralSearchResultType getFromValue(int value) {
        for (GeneralSearchResultType generalSearchResultType : GeneralSearchResultType.values()) {
            if (generalSearchResultType.mValue == value) {
                return generalSearchResultType;
            }
        }

        return GeneralSearchResultType.ALL;
    }
}