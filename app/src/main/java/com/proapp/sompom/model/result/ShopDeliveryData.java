package com.proapp.sompom.model.result;

import com.proapp.sompom.model.ShopDeliveryDate;
import com.proapp.sompom.model.ShopDeliveryTime;

import java.util.List;

public class ShopDeliveryData {

    private List<ShopDeliveryDate> mShopDeliveryDates;
    private List<ShopDeliveryTime> mShopDeliveryTimes;

    public List<ShopDeliveryDate> getShopDeliveryDates() {
        return mShopDeliveryDates;
    }

    public void setShopDeliveryDates(List<ShopDeliveryDate> shopDeliveryDates) {
        mShopDeliveryDates = shopDeliveryDates;
    }

    public List<ShopDeliveryTime> getShopDeliveryTimes() {
        return mShopDeliveryTimes;
    }

    public void setShopDeliveryTimes(List<ShopDeliveryTime> shopDeliveryTimes) {
        mShopDeliveryTimes = shopDeliveryTimes;
    }
}
