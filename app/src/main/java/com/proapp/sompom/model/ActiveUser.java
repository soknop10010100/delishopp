package com.proapp.sompom.model;

import com.proapp.sompom.helper.IntentData;
import com.proapp.sompom.model.emun.ViewType;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.listener.ConversationDataAdaptive;

import java.util.ArrayList;

/**
 * Created by He Rotha on 7/6/18.
 */
public class ActiveUser extends ArrayList<User> implements ConversationDataAdaptive, Adaptive {
    private String mTitle;
    private boolean mIsShowMore;
    private SellerItemType mSellerItemType;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public boolean isShowMore() {
        return mIsShowMore;
    }

    public void setShowMore(boolean showMore) {
        mIsShowMore = showMore;
    }

    public SellerItemType getSellerItemType() {
        return mSellerItemType;
    }

    public void setSellerItemType(SellerItemType sellerItemType) {
        mSellerItemType = sellerItemType;
    }

    @Override
    public ViewType getTimelineViewType() {
        return ViewType.ActiveUser;
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting activity
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ActiveUser)) return false;
        ActiveUser activeUser = (ActiveUser) o;
        return getSellerItemType() == activeUser.getSellerItemType();
    }

    @Override
    public int hashCode() {
        return getSellerItemType().hashCode();
    }

    public enum SellerItemType {
        YOUR_SELLER,
        ACTIVE_SELLER,
        PEOPLE_NEARBY,
        SUGGESTED_PEOPLE,
        LAST_SEARCH
    }
}
