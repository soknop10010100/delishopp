package com.proapp.sompom.model.result;

/**
 * Created by he.rotha on 6/8/16.
 */
public class DeleteProduct extends ErrorSupportModel {
    //CHECKSTYLE:OFF
    private boolean isDeleted;
    //CHECKSTYLE:OFF

    public boolean isDeleted() {
        return isDeleted;
    }
}
