package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItemHistory;
import com.proapp.sompom.model.concierge.ConciergeShop;

/**
 * Created by Or Vitovongsak on 1/10/21.
 */

public class ConciergeOrderItem extends AbsConciergeModel {

    @SerializedName(FIELD_MENU_ITEM)
    private ConciergeMenuItemHistory mMenuItem;

    @SerializedName(FIELD_QUANTITY)
    private int mQuantity;

    @SerializedName(FIELD_SHOP_ID)
    private String mShopId;

    @SerializedName(FIELD_ORDER_ID)
    private String mOrderId;

    @SerializedName(FIELD_SUB_TOTAL)
    private float mSubTotal;

    @SerializedName(FIELD_TOTAL)
    private float mTotal;

    @SerializedName(ConciergeMenuItem.FIELD_SHOP)
    private ConciergeShop mShop;

    @SerializedName(FIELD_SHOP_HASH)
    private Integer mShopHash;

    @SerializedName(FIELD_HASH)
    private Integer mItemHash;

    @SerializedName(FIELD_INSTRUCTION)
    private String mInstruction;

    public ConciergeMenuItemHistory getMenuItem() {
        return mMenuItem;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public void setQuantity(int quantity) {
        this.mQuantity = quantity;
    }

    public String getShopId() {
        return mShopId;
    }

    public void setShopId(String shopId) {
        this.mShopId = shopId;
    }

    public ConciergeShop getShop() {
        return mShop;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public void setOrderId(String orderId) {
        this.mOrderId = orderId;
    }

    public float getSubTotal() {
        return mSubTotal;
    }

    public void setSubTotal(float subTotal) {
        this.mSubTotal = subTotal;
    }

    public float getTotal() {
        return mTotal;
    }

    public void setTotal(float total) {
        this.mTotal = total;
    }

    public String getInstruction() {
        return mInstruction;
    }

    public int getShopHash() {
        if (mShopHash != null) {
            return mShopHash;
        }

        return 0;
    }


    public void setShopHash(int shopHash) {
        mShopHash = shopHash;
    }

    public int getItemHash() {
        if (mItemHash != null) {
            return mItemHash;
        }

        return 0;
    }

    public void setItemHash(int itemHash) {
        mItemHash = itemHash;
    }
}
