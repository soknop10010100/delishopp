package com.proapp.sompom.model;

import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;
import java.util.List;

public class ShopDeliveryDate {

    private LocalDateTime mDate;

    private boolean mIsEnable;
    private boolean mIsSelected;

    @SerializedName("date")
    private String mResponseDate;

    @SerializedName("list")
    private List<ShopDeliveryTime> mShopDeliveryTimes;

    private boolean mIsHasValidTimeItem;

    public ShopDeliveryDate(LocalDateTime date, boolean isEnable) {
        mDate = date;
        mIsEnable = isEnable;
    }

    public LocalDateTime getDate() {
        return mDate;
    }

    public void setDate(LocalDateTime date) {
        mDate = date;
    }

    public boolean isEnable() {
        return mIsEnable;
    }

    public void setEnable(boolean enable) {
        mIsEnable = enable;
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    public void setSelected(boolean selected) {
        mIsSelected = selected;
    }

    public String getResponseDate() {
        return mResponseDate;
    }

    public List<ShopDeliveryTime> getShopDeliveryTimes() {
        return mShopDeliveryTimes;
    }

    public void setShopDeliveryTimes(List<ShopDeliveryTime> shopDeliveryTimes) {
        mShopDeliveryTimes = shopDeliveryTimes;
    }

    public boolean isHasValidTimeItem() {
        return mIsHasValidTimeItem;
    }

    public void setHasValidTimeItem(boolean hasValidTimeItem) {
        mIsHasValidTimeItem = hasValidTimeItem;
    }

    @Override
    public String toString() {
        return "ShopDeliveryDate{" +
                "mDate=" + mDate +
                ", mIsEnable=" + mIsEnable +
                ", mIsSelected=" + mIsSelected +
                '}';
    }
}
