package com.proapp.sompom.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.proapp.sompom.model.emun.PublishItem;

/**
 * Created by Chhom Veasna on 12/11/2019.
 */
public class ItemPrivacy implements Parcelable {

    private PublishItem mPrivacy;
    private boolean mIsSelected;

    public ItemPrivacy(PublishItem privacy, boolean isSelected) {
        mPrivacy = privacy;
        mIsSelected = isSelected;
    }

    public PublishItem getPrivacy() {
        return mPrivacy;
    }

    public void setPrivacy(PublishItem privacy) {
        mPrivacy = privacy;
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    public void setSelected(boolean selected) {
        mIsSelected = selected;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mPrivacy == null ? -1 : this.mPrivacy.ordinal());
        dest.writeByte(this.mIsSelected ? (byte) 1 : (byte) 0);
    }

    protected ItemPrivacy(Parcel in) {
        int tmpMPrivacy = in.readInt();
        this.mPrivacy = tmpMPrivacy == -1 ? null : PublishItem.values()[tmpMPrivacy];
        this.mIsSelected = in.readByte() != 0;
    }

    public static final Creator<ItemPrivacy> CREATOR = new Creator<ItemPrivacy>() {
        @Override
        public ItemPrivacy createFromParcel(Parcel source) {
            return new ItemPrivacy(source);
        }

        @Override
        public ItemPrivacy[] newArray(int size) {
            return new ItemPrivacy[size];
        }
    };
}
