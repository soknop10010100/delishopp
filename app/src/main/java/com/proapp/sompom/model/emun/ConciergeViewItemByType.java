package com.proapp.sompom.model.emun;

import androidx.annotation.StringRes;

import com.proapp.sompom.R;

/**
 * Created by Veasna Chhom on 4/26/22.
 */
public enum ConciergeViewItemByType {

    NORMAL(1, R.string.fi_sr_list, 1),
    TWO_COLUMNS(2, R.string.fi_sr_apps, 2),
    THREE_COLUMNS(3, R.string.fi_rr_grid, 3);

    private int mId;
    private @StringRes
    int mIConResource;
    private int mSpanCount;

    ConciergeViewItemByType(int id, int IConResource, int spanCount) {
        mId = id;
        mIConResource = IConResource;
        mSpanCount = spanCount;
    }

    public int getSpanCount() {
        return mSpanCount;
    }

    public int getIConResource() {
        return mIConResource;
    }

    public int getId() {
        return mId;
    }

    public static ConciergeViewItemByType from(int id) {
        for (ConciergeViewItemByType deeplinkType : ConciergeViewItemByType.values()) {
            if (deeplinkType.mId == id) {
                return deeplinkType;
            }
        }

        return THREE_COLUMNS;
    }

    public static ConciergeViewItemByType getNextViewByOption(ConciergeViewItemByType currentOption) {
        ConciergeViewItemByType[] values = ConciergeViewItemByType.values();
        for (int i = 0; i < values.length; i++) {
            if (values[i].mId == currentOption.mId && i < (values.length - 1)) {
                return values[i + 1];
            }
        }

        return NORMAL;
    }
}
