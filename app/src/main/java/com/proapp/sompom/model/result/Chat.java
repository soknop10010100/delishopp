package com.proapp.sompom.model.result;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.model.BaseChatModel;
import com.proapp.sompom.model.GroupMediaAdaptive;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.LinkPreviewAdaptive;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.MediaContainerAdaptive;
import com.proapp.sompom.model.MetaGroup;
import com.proapp.sompom.model.Search;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.SpannableUtil;
import com.proapp.sompom.utils.SpecialTextRenderUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;
import timber.log.Timber;

@RealmClass
public class Chat implements Comparable<Chat>, Parcelable, BaseChatModel, RealmModel, LinkPreviewAdaptive,
        GroupMediaAdaptive, MediaContainerAdaptive, Search {
    public static final Creator<Chat> CREATOR = new Creator<Chat>() {
        @Override
        public Chat createFromParcel(Parcel source) {
            return new Chat(source);
        }

        @Override
        public Chat[] newArray(int size) {
            return new Chat[size];
        }
    };
    public static final String FIELD_ID = "messageId";
    public static final String FIELD_DATE = "date";
    public static final String FIELD_CONTENT = "content";
    public static final String FIELD_SENDER = "sender";
    public static final String FIELD_SENDER_ID = "senderId";
    public static final String FIELD_MEDIA = "media";

    private static final String FIELD_PRODUCT_ID = "productId";
    private static final String FIELD_STATUS = "status";
    private static final String FIELD_IS_TYPING = "isTyping";
    private static final String FIELD_CHANNEL_ID = "conversationId";
    private static final String FIELD_SEND_TO = "sendTo";
    private static final String FIELD_PRODUCT = "product";
    private static final String FIELD_SEEN = "seen";
    private static final String FIELD_IS_DELETE = "isDeleted";
    private static final String IS_GROUP = "isGroup";
    private static final String GROUP_NAME = "groupName";
    private static final String TYPE = "messageType";
    private static final String SENDING_TYPE = "type";
    private static final String CALL_DATA = "callData";
    private static final String IS_ENCRYPTED = "isEncrypted";
    private static final String META_GROUP = "metaGroup";
    private static final String FIELD_REFERENCE = "reference";
    private static final String FIELD_USER = "user";
    private static final String FIELD_IS_EDITED = "isEdited";
    private static final String FIELD_IS_LAST_EDITED_DATE = "lastEditedDate";
    private static final String FIELD_TIME_ZONE = "timezone";
    private static final String CALL_DURATION = "callDuration";

    @SerializedName(FIELD_ID)
    @PrimaryKey
    private String mId;
    @SerializedName(FIELD_DATE)
    private Date mDate;
    @SerializedName(FIELD_CONTENT)
    private String mContent;
    @SerializedName(FIELD_MEDIA)
    private RealmList<Media> mMediaList;
    @SerializedName(FIELD_STATUS)
    private String mStatus = MessageState.SENT.getValue();
    @SerializedName(FIELD_SENDER_ID)
    private String mSenderId;
    @SerializedName(FIELD_PRODUCT_ID)
    private String mProductId;
    @SerializedName(FIELD_CHANNEL_ID)
    private String mChannelId;
    @SerializedName(FIELD_SEND_TO)
    private String mSendTo;
    @SerializedName(FIELD_SEEN)
    private RealmList<String> mSeen;
    @SerializedName(FIELD_IS_DELETE)
    private Boolean mIsDelete;
    @Ignore
    @SerializedName(SENDING_TYPE)
    private String mSendingType;
    @Ignore
    @SerializedName(FIELD_IS_TYPING)
    private Boolean mIsTyping;
    @SerializedName(FIELD_SENDER)
    private User mSender;
    @Ignore
    @SerializedName(FIELD_PRODUCT)
    private Product mProduct;
    @Ignore
    private transient boolean mIsExpandHeight; //transient for ignore serialized and parcel
    @Ignore
    private LinkPreviewModel mLinkPreviewModel; //transient for ignore serialized and parcel
    @SerializedName(IS_GROUP)
    private boolean mIsGroup;
    @Ignore
    private User mRecipient;
    @SerializedName(GROUP_NAME)
    private String mGroupName;
    @Ignore
    private List<User> mSeenParticipants;
    @SerializedName(TYPE)
    private String mChatType;
    private boolean mIsSavedFromSeenStatus;
    @Ignore
    @SerializedName(CALL_DATA)
    private Map<String, Object> mCallData;
    @SerializedName(IS_ENCRYPTED)
    private boolean mIsEncrypted;
    @Ignore
    @SerializedName(LifeStream.META_PREVIEW)
    private List<LinkPreviewModel> mMetaPreview;
    // Local property to check if chat is decrypted before, idea is to not modify mIsEncrypted as
    // it is saved on server and modifying can cause some issues
    private boolean mIsDecrypted;
    @SerializedName(META_GROUP)
    private MetaGroup mMetaGroup;
    @Ignore
    private Boolean mIsIgnoreSeenStatus;
    @SerializedName(FIELD_REFERENCE)
    private ChatReferenceWrapper mChatReferenceWrapper;

    //Just for search result chat item navigation on click
    private transient Conversation mParentConversation;

    @SerializedName(FIELD_USER)
    @Ignore
    private User mUser;

    @SerializedName(FIELD_IS_EDITED)
    private Boolean mIsEdited;

    @SerializedName(FIELD_IS_LAST_EDITED_DATE)
    private Date mLastEditedDate;

    @SerializedName(CALL_DURATION)
    private Integer mCallDuration;

    @Ignore
    @SerializedName(FIELD_TIME_ZONE)
    private String mTimeZone;

    //CHECKSTYLE:OFF
    public Chat() {
    }

    public Chat(Chat chat) {
        setId(chat.getId());
        setSenderId(chat.getSenderId());
        setSender(chat.getSender());
        setSendTo(chat.getSendTo());
        setStatus(chat.getStatus());
        setCallDuration(chat.getCallDuration());
        setRawSendingType(chat.getSendingTypeAsString());
        setChannelId(chat.getChannelId());
        setContent(chat.getContent());
        setProductId(chat.getProductId());
        setMediaList(chat.getMediaList());
        setTyping(chat.getTyping());
        setDate(chat.getDate());
        setExpandHeight(isExpandHeight());
        setLinkPreviewModel(chat.getLinkPreviewModel());
        setGroup(chat.isGroup());
        setGroupName(chat.getGroupName());
        setSeenParticipants(chat.getSeenParticipants());
        setChatType(chat.getChatType());
        setSavedFromSeenStatus(chat.getSavedFromSeenStatus());
        setCallData(chat.getCallData());
        setIsEncrypted(chat.isEncrypted());
        setMetaPreview(chat.getMetaPreview());
        setIsDecrypted(chat.isDecrypted());
        setSeen(chat.getSeen());
        setChatReferenceWrapper(chat.getChatReferenceWrapper());
        setEdited(chat.getEdited());
        setLastEditedDate(chat.getLastEditedDate());

        if (chat.getProduct() != null) {
            Product product = new Product();
            product.setUser(chat.getProduct().getUser());
            product.setId(chat.getProduct().getId());
            product.setPrice(chat.getProduct().getPrice());
            product.setProductMedia(chat.getProduct().getMedia());
            product.setName(chat.getProduct().getName());
            product.setCurrency(chat.getProduct().getCurrency());
            setProduct(product);
        }
    }

    public Chat cloneNewChat() {
        Chat chat1 = new Chat();

        chat1.setId(getId());
        chat1.setSenderId(getSenderId());
        chat1.setSender(getSender());
        chat1.setDate(getDate());
        chat1.setStatus(getStatus());
        chat1.setCallDuration(getCallDuration());
        chat1.setRecipient(getRecipient());
        chat1.setSendTo(getSendTo());
        chat1.setChannelId(getChannelId());
        chat1.setMediaList(getMediaList());
        chat1.setContent(getContent());
        chat1.setGroup(isGroup());
        chat1.setGroupName(getGroupName());
        chat1.setSeenParticipants(getSeenParticipants());
        chat1.setChatType(getChatType());
        chat1.setSavedFromSeenStatus(getSavedFromSeenStatus());
        chat1.setCallData(getCallData());
        chat1.setIsEncrypted(isEncrypted());
        chat1.setMetaPreview(getMetaPreview());
        chat1.setIsDecrypted(isDecrypted());
        chat1.setRawSendingType(getSendingTypeAsString());
        chat1.setSeen(getSeen());
        chat1.setChatReferenceWrapper(getChatReferenceWrapper());
        setEdited(getEdited());
        setLastEditedDate(getLastEditedDate());

        return chat1;
    }

    public static Chat newCloneDateInstance(Chat chat) {
        Chat chat1 = new Chat();
        chat1.setDate(chat.getDate());
        return chat1;
    }

    public static Chat getNewChat(Context context, User sendTo, @Nullable Product product) {
        Chat chat = new Chat();
        chat.setId(UUID.randomUUID().toString());
        chat.setSender(SharedPrefUtils.getCurrentUser(context));
        chat.setSenderId(SharedPrefUtils.getUserId(context));
        chat.constructNewSentMessageDate();
        chat.setStatus(MessageState.SENDING);
        chat.setRecipient(sendTo);

        if (product != null) {
            chat.setProductId(product.getId());
            chat.setProduct(product);
        }
        if (sendTo != null) {
            chat.setSendTo(sendTo.getId());
        }

        return chat;
    }

    public static Chat getNewChat(Context context, String conversationId, User sendTo) {
        Chat chat = new Chat();
        chat.setId(UUID.randomUUID().toString());
        chat.setSender(SharedPrefUtils.getCurrentUser(context));
        chat.setSenderId(SharedPrefUtils.getUserId(context));
        chat.constructNewSentMessageDate();
        chat.setStatus(MessageState.SENDING);
        chat.setRecipient(sendTo);
        chat.setChannelId(conversationId);
        if (sendTo != null) {
            chat.setSendTo(sendTo.getId());
        }

        return chat;
    }

    public static Chat getNewCallEventChat(Context context,
                                           String conversationId,
                                           User sendTo,
                                           ChatType chatType,
                                           boolean isGroup) {
        Chat chat = getNewChat(context, conversationId, sendTo);
        chat.setChatType(chatType.getType());
        //Reset some data that no need to be sent via Socket.
        chat.setContent(null);
        chat.setRecipient(null);
        chat.setGroup(isGroup);
        if (sendTo != null) {
            chat.setSendTo(sendTo.getId());
        }

        return chat;
    }

    public boolean isValid() {
        return !TextUtils.isEmpty(getId());
    }

    public Integer getCallDuration() {
        return mCallDuration;
    }

    public void setCallDuration(Integer callDuration) {
        mCallDuration = callDuration;
    }

    public boolean getEdited() {
        return mIsEdited != null && mIsEdited;
    }

    public void setEdited(boolean edited) {
        mIsEdited = edited;
    }

    public Date getLastEditedDate() {
        return mLastEditedDate;
    }

    public void setLastEditedDate(Date lastEditedDate) {
        mLastEditedDate = lastEditedDate;
    }

    public static Chat getNewDeliveredChat(Context context, Chat receivedChat) {
        /*
        We have to send back delivered status to the original message sender
         */
        Chat chat = getNewChat(context, receivedChat.getChannelId(), receivedChat.getSender());
        chat.setId(receivedChat.getId());
        chat.setSendingType(SendingType.DELIVERED);
        chat.setStatus(MessageState.SENT);
        chat.setGroup(receivedChat.isGroup());

        return chat;
    }

    protected Chat(Parcel in) {
        this.mId = in.readString();
        long tmpMDate = in.readLong();
        this.mDate = tmpMDate == -1 ? null : new Date(tmpMDate);
        this.mContent = in.readString();
        this.mMediaList = new RealmList<>();
        ArrayList<Media> mediaList = in.createTypedArrayList(Media.CREATOR);
        if (mediaList != null && !mediaList.isEmpty()) {
            mMediaList.addAll(mediaList);
        }
        this.mStatus = in.readString();
        this.mSenderId = in.readString();
        this.mProductId = in.readString();
        this.mChannelId = in.readString();
        this.mSendTo = in.readString();
        this.mSeen = new RealmList<>();
        ArrayList<String> seen = in.createStringArrayList();
        if (seen != null && !seen.isEmpty()) {
            mSeen.addAll(seen);
        }
        this.mSendingType = in.readString();
        this.mIsTyping = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mSender = in.readParcelable(User.class.getClassLoader());
        this.mProduct = in.readParcelable(Product.class.getClassLoader());
        this.mIsDelete = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mIsGroup = in.readByte() != 0;
        this.mRecipient = in.readParcelable(User.class.getClassLoader());
        this.mGroupName = in.readString();
        this.mSeenParticipants = new RealmList<>();
        ArrayList<User> typedArrayList = in.createTypedArrayList(User.CREATOR);
        if (typedArrayList != null) {
            this.mSeenParticipants.addAll(typedArrayList);
        }
        this.mChatType = in.readString();
        this.mLinkPreviewModel = in.readParcelable(LinkPreviewModel.class.getClassLoader());
        this.mIsSavedFromSeenStatus = in.readByte() != 0;
        this.mIsEncrypted = in.readByte() != 0;
        this.mMetaPreview = in.createTypedArrayList(LinkPreviewModel.CREATOR);
        this.mIsDecrypted = in.readByte() != 0;
        this.mMetaGroup = in.readParcelable(MetaGroup.class.getClassLoader());
        this.mCallDuration = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public void setTimeZone(String timeZone) {
        mTimeZone = timeZone;
    }

    public User getUser() {
        return mUser;
    }

    public Conversation getParentConversation() {
        return mParentConversation;
    }

    public void setParentConversation(Conversation parentConversation) {
        mParentConversation = parentConversation;
    }

    @Override
    public String getPreviewContent() {
        return getContent();
    }

    public ChatReferenceWrapper getChatReferenceWrapper() {
        return mChatReferenceWrapper;
    }

    public void setChatReferenceWrapper(ChatReferenceWrapper chatReferenceWrapper) {
        mChatReferenceWrapper = chatReferenceWrapper;
    }

    public MetaGroup getMetaGroup() {
        return mMetaGroup;
    }

    public void setMetaGroup(MetaGroup metaGroup) {
        mMetaGroup = metaGroup;
    }

    public boolean hasMetaPreview() {
        return mMetaPreview != null && !mMetaPreview.isEmpty();
    }

    public List<LinkPreviewModel> getMetaPreview() {
        return mMetaPreview;
    }

    public void setMetaPreview(List<LinkPreviewModel> metaPreview) {
        mMetaPreview = metaPreview;
    }

    public Map<String, Object> getCallData() {
        return mCallData;
    }

    public void setCallData(Map<String, Object> callData) {
        mCallData = callData;
    }

    public String getChatType() {
        return mChatType;
    }

    public boolean getSavedFromSeenStatus() {
        return mIsSavedFromSeenStatus;
    }

    public void setSavedFromSeenStatus(boolean savedFromSeenStatus) {
        mIsSavedFromSeenStatus = savedFromSeenStatus;
    }

    public Boolean getIgnoreSeenStatus() {
        return mIsIgnoreSeenStatus != null && mIsIgnoreSeenStatus;
    }

    public void setIgnoreSeenStatus(boolean ignoreSeenStatus) {
        mIsIgnoreSeenStatus = ignoreSeenStatus;
    }

    public void setChatType(String chatType) {
        mChatType = chatType;
    }

    public List<User> getSeenParticipants() {
        return mSeenParticipants;
    }

    public void setSeenParticipants(List<User> seenParticipants) {
        mSeenParticipants = seenParticipants;
    }

    @Override
    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public User getSender() {
        return mSender;
    }

    public String getGroupName() {
        return mGroupName;
    }

    public void setGroupName(String groupName) {
        mGroupName = groupName;
    }

    public void setSender(User sender) {
        mSender = sender;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public void constructNewSentMessageDate() {
        /*
            This date field must be assigned value only one time when new message is created to send
            from current user and whenever the this message will be used to manage exchange status
            over socket such as sent or read status, the date value must be maintain as original one
            so as to keep the order of the message.
         */
        mDate = new Date();
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public boolean isExpandHeight() {
        return mIsExpandHeight;
    }

    public void setExpandHeight(boolean expandHeight) {
        mIsExpandHeight = expandHeight;
    }

    public List<Media> getMediaList() {
        return mMediaList;
    }

    @Override
    public List<Media> getMedia() {
        return getMediaList();
    }

    public User getRecipient() {
        return mRecipient;
    }

    public void setRecipient(User recipient) {
        mRecipient = recipient;
    }

    public void setMediaList(List<Media> mediaList) {
        if (mediaList != null) {
            mMediaList = new RealmList<>();
            mMediaList.addAll(mediaList);
        }
    }

    public void setMediaFile(Media mediaFile) {

    }

    public String getSenderId() {
        return mSenderId;
    }

    public void setSenderId(String senderId) {
        mSenderId = senderId;
    }

    public String getChannelId() {
        return mChannelId;
    }

    public void setChannelId(String channelId) {
        mChannelId = channelId;
    }

    public String getProductId() {
        return mProductId;
    }

    public void setProductId(String productId) {
        mProductId = productId;
    }

    public Boolean getTyping() {
        return mIsTyping;
    }

    public void setTyping(Boolean typing) {
        mIsTyping = typing;
    }

    public String getSendTo() {
        return mSendTo;
    }

    public void setSendTo(String sendTo) {
        mSendTo = sendTo;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }

    public boolean isGroup() {
        return mIsGroup;
    }

    public void setGroup(boolean group) {
        mIsGroup = group;
    }

    public boolean isEncrypted() {
        return mIsEncrypted;
    }

    public void setIsEncrypted(boolean isEncrypted) {
        mIsEncrypted = isEncrypted;
    }

    public boolean isDecrypted() {
        return mIsDecrypted;
    }

    public void setIsDecrypted(boolean isDecrypted) {
        mIsDecrypted = isDecrypted;
    }

    private boolean isAllVideo() {
        if (getMediaList().size() < 2) {
            return false;
        }
        for (Media media : getMediaList()
        ) {
            if (media.getType() != MediaType.VIDEO) {
                return false;
            }
        }

        return true;
    }

    private boolean isOnePhotoOneVideo() {
        Media media1 = getMediaList().get(0);
        Media media2 = getMediaList().get(1);
        return (getMediaList().size() == 2)
                && ((media1.getType() == MediaType.IMAGE && media1.getType() == MediaType.VIDEO)
                || (media1.getType() == MediaType.VIDEO && media2.getType() == MediaType.IMAGE));
    }

    private int isAPhotAndManyVideos() {
        int numOfPhoto = 0;
        int numOfVideo = 0;
        for (Media media : getMediaList()) {
            if (media.getType() == MediaType.VIDEO) {
                numOfVideo++;
            } else if (media.getType() == MediaType.IMAGE) {
                numOfPhoto++;
            }

            if (numOfPhoto > 1) {
                return 0;
            }
        }
        return numOfVideo;
    }

    private int isAVideoManyPhoto() {
        int numOfPhoto = 0;
        int numOfVideo = 0;
        for (Media media : getMediaList()) {
            if (media.getType() == MediaType.VIDEO) {
                numOfVideo++;
            } else if (media.getType() == MediaType.IMAGE) {
                numOfPhoto++;
            }

            if (numOfVideo > 1) {
                return 0;
            }
        }
        return numOfPhoto;
    }

    public int getNumOfVideos() {
        int i = 0;
        for (Media media : getMediaList()) {
            if (media.getType() == MediaType.VIDEO) {
                i++;
            }
        }
        return i;
    }

    public int getNumOfImages() {
        int i = 0;
        for (Media media : getMediaList()) {
            if (media.getType() == MediaType.IMAGE) {
                i++;
            }
        }
        return i;
    }

    private int getMediaSize() {
        if (mMediaList != null) {
            return mMediaList.size();
        }

        return 0;
    }

    public Type getType() {
        if (getMediaList() == null || getMediaList().isEmpty()) {
            return Type.TEXT;
        } else {
            Media media = getMediaList().get(0);
            if (isMixMediaType()) {
                return Type.MIX_MEDIA;
            } else if (media.getType() == MediaType.IMAGE) {
                return Type.IMAGE;
            } else if (media.getType() == MediaType.GIF) {
                return Type.GIF;
            } else if (media.getType() == MediaType.VIDEO) {
                return Type.VIDEO;
            } else if (media.getType() == MediaType.TENOR_GIF) {
                return Type.TENOR_GIF;
            } else if (media.getType() == MediaType.AUDIO) {
                return Type.AUDIO;
            } else if (media.getType() == MediaType.FILE) {
                return Type.FILE;
            } else {
                return Type.TEXT;
            }
        }
    }

    private boolean isMixMediaType() {
        Media firstMedia = getMediaList().get(0);
        if (firstMedia.getType() == MediaType.IMAGE && getMediaList().size() > 1) {
            for (Media media1 : getMediaList()) {
                if (media1.getType() == MediaType.VIDEO || media1.getType() == MediaType.GIF) {
                    return true;
                }
            }
        } else if (firstMedia.getType() == MediaType.VIDEO && getMediaList().size() > 1) {
            for (Media media1 : getMediaList()) {
                if (media1.getType() == MediaType.IMAGE || media1.getType() == MediaType.GIF) {
                    return true;
                }
            }
        } else if (firstMedia.getType() == MediaType.GIF && getMediaList().size() > 1) {
            for (Media media1 : getMediaList()) {
                if (media1.getType() == MediaType.IMAGE || media1.getType() == MediaType.VIDEO) {
                    return true;
                }
            }
        }

        return false;
    }

    public RealmList<String> getSeen() {
        return mSeen;
    }

    public void setSeen(RealmList<String> seen) {
        mSeen = seen;
    }

    private User getUserFromParticipant(Conversation conversation, String userId) {
        if (conversation != null && conversation.getParticipants() != null) {
            for (User participant : conversation.getParticipants()) {
                if (TextUtils.equals(participant.getId(), userId)) {
                    return participant;
                }
            }
        }

        return null;
    }

    public String getActualContent(Context context,
                                   Conversation conversation,
                                   boolean forDisplay) {
        String message = "";
        if (isDeleted()) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                message = context.getString(R.string.chat_info_you_remove_message);
            } else {
                if (getSender() == null) {
                    User userFromParticipant = getUserFromParticipant(conversation, getSenderId());
                    if (userFromParticipant != null) {
                        message = userFromParticipant.getFirstName() + " " + context.getString(R.string.chat_info_remove_message);
                    } else {
                        message = context.getString(R.string.chat_info_remove_message);
                    }
                } else {
                    message = getSender().getFirstName() + " " + context.getString(R.string.chat_info_remove_message);
                }
            }
        } else if (getSendingType() == SendingType.UPDATE) {
            if (conversation != null && !conversation.isGroup()) {
                message = context.getString(R.string.chat_info_edited_header_message) + getSenderNameToken() + getContent();
            } else {
                message = getSenderNamePrefix(context, conversation, forDisplay, true, true) + getContent();
            }
        } else if (ChatType.from(mChatType) == ChatType.CREATE_GROUP) {
            message = getSender().getFullName() + getSenderNameToken() + context.getString(R.string.notification_create_new_group,
                    getGroupName());
        } else if (ChatType.from(mChatType) == ChatType.MISSED_CALL) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                if (conversation != null && conversation.getOneToOneRecipient(context) != null) {
                    message = conversation.getOneToOneRecipient(context).getFirstName() + " "
                            + context.getString(R.string.chat_info_missed_your_call_message);
                } else {
                    message = context.getString(R.string.chat_info_missed_your_call_message);
                }
            } else {
                if (conversation != null && conversation.getOneToOneRecipient(context) != null) {
                    message = context.getString(R.string.chat_info_you_missed_a_call_message,
                            conversation.getOneToOneRecipient(context).getFirstName());
                } else {
                    message = context.getString(R.string.chat_info_you_missed_a_call_message, "");
                }
            }
        } else if (ChatType.from(mChatType) == ChatType.MISSED_VIDEO_CALL) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                if (conversation != null && conversation.getOneToOneRecipient(context) != null) {
                    message = conversation.getOneToOneRecipient(context).getFirstName() + " "
                            + context.getString(R.string.chat_info_missed_your_video_call_message);
                } else {
                    message = context.getString(R.string.chat_info_missed_your_video_call_message);
                }
            } else {
                if (conversation != null && conversation.getOneToOneRecipient(context) != null) {
                    message = context.getString(R.string.chat_info_you_missed_a_video_call_message,
                            conversation.getOneToOneRecipient(context).getFirstName());
                } else {
                    message = context.getString(R.string.chat_info_you_missed_a_video_call_message, "");
                }
            }
        } else if (ChatType.from(mChatType) == ChatType.CALLED) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                if (conversation != null && conversation.getOneToOneRecipient(context) != null) {
                    message = context.getString(R.string.chat_info_you_audio_called) + " " + conversation.getOneToOneRecipient(context).getFirstName();
                } else {
                    message = context.getString(R.string.chat_info_you_audio_called);
                }
            } else {
                if (conversation != null && conversation.getOneToOneRecipient(context) != null) {
                    message = conversation.getOneToOneRecipient(context).getFirstName() + " " + context.getString(R.string.chat_info_audio_called_you);
                } else {
                    message = context.getString(R.string.chat_info_audio_called_you);
                }
            }
        } else if (ChatType.from(mChatType) == ChatType.VIDEO_CALLED) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                if (conversation != null && conversation.getOneToOneRecipient(context) != null) {
                    message = context.getString(R.string.chat_info_you_video_called) + " " + conversation.getOneToOneRecipient(context).getFirstName();
                } else {
                    message = context.getString(R.string.chat_info_you_video_called);
                }
            } else {
                if (conversation != null && conversation.getOneToOneRecipient(context) != null) {
                    message = conversation.getOneToOneRecipient(context).getFirstName() + " " + context.getString(R.string.chat_info_video_called_you);
                } else {
                    message = context.getString(R.string.chat_info_video_called_you);
                }
            }
        } else if (ChatType.from(mChatType) == ChatType.START_GROUP_CALL) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                message = context.getString(R.string.chat_info_you_start_group_audio_message);
            } else {
                message = context.getString(R.string.chat_info_start_group_audio_message,
                        getSenderNamePrefix(context, conversation,
                                forDisplay,
                                false,
                                false));
            }
        } else if (ChatType.from(mChatType) == ChatType.START_GROUP_VIDEO_CALL) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                message = context.getString(R.string.chat_info_you_start_group_video_message);
            } else {
                message = context.getString(R.string.chat_info_start_group_video_message,
                        getSenderNamePrefix(context, conversation, forDisplay, false, false));
            }
        } else if (ChatType.from(mChatType) == ChatType.END_GROUP_CALL) {
            message = context.getString(R.string.chat_info_end_group_audio_message);
        } else if (ChatType.from(mChatType) == ChatType.END_GROUP_VIDEO_CALL) {
            message = context.getString(R.string.chat_info_end_group_video_message);
        } else if (ChatType.from(mChatType) == ChatType.CHANGE_GROUP_NAME && getMetaGroup() != null) {
            String text = SpannableUtil.getMessageSubject(context, getSender());
            message = text + " " + context.getString(R.string.chat_info_change_group_name, getMetaGroup().getName());
        } else if (ChatType.from(mChatType) == ChatType.CHANGE_GROUP_PICTURE && getMetaGroup() != null) {
            String text = SpannableUtil.getMessageSubject(context, getSender());
            message = text + " " + context.getString(R.string.chat_info_change_group_picture);
        } else if (ChatType.from(mChatType) == ChatType.ADD_GROUP_MEMBER && getMetaGroup() != null) {
            if (getMetaGroup().getParticipants() != null && !getMetaGroup().getParticipants().isEmpty()) {
                String text = SpannableUtil.getMessageSubject(context, getSender()) +
                        " " +
                        context.getString(R.string.chat_info_added_someone);
                if (getMetaGroup().getParticipants().size() > 1) {
                    text += " " + SpannableUtil.buildMultipleUserNameForGroupModificationMessage(context, getMetaGroup().getParticipants(), false);
                } else {
                    text += " " + SpannableUtil.buildGroupNotificationUserName(context, getMetaGroup().getParticipants().get(0), false);
                }
                text += " " + context.getString(R.string.chat_info_someone_to_group);
                message = text;
            }
        } else if (ChatType.from(mChatType) == ChatType.REMOVE_GROUP_MEMBER && getMetaGroup() != null) {
            if (getMetaGroup().getParticipants() != null && !getMetaGroup().getParticipants().isEmpty()) {
                String text = SpannableUtil.getMessageSubject(context, getSender()) +
                        " " +
                        context.getString(R.string.chat_info_removed_someone);
                if (getMetaGroup().getParticipants().size() > 1) {
                    text += " " + SpannableUtil.buildMultipleUserNameForGroupModificationMessage(context, getMetaGroup().getParticipants(), false);
                } else {
                    text += " " + SpannableUtil.buildGroupNotificationUserName(context, getMetaGroup().getParticipants().get(0), false);
                }
                text += " " + context.getString(R.string.chat_info_someone_from_group);
                message = text;
            }
        } else if (getType() == Chat.Type.TEXT) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                message = context.getString(R.string.chat_info_you_chat) + getSenderNameToken() + getContent();
            } else {
                message = getSenderNamePrefix(context, conversation, forDisplay, false, true) + getContent();
            }
        } else if (getType() == Chat.Type.AUDIO) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                message = context.getString(R.string.chat_info_you_chat) + getSenderNameToken() + context.getString(R.string.chat_info_send_voice_message);
            } else {
                message = getSenderNamePrefix(context, conversation, forDisplay, false, true) + context.getString(R.string.chat_info_send_voice_message);
            }
        } else if (getType() == Type.FILE) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                message = context.getString(R.string.chat_info_you_chat) + getSenderNameToken() + context.getString(R.string.chat_info_send_file_message);
            } else {
                message = getSenderNamePrefix(context, conversation, forDisplay, false, true) + context.getString(R.string.chat_info_send_file_message);
            }
        } else if (getType() == Type.MIX_MEDIA) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                message = context.getString(R.string.chat_info_you_chat) + getSenderNameToken() + context.getString(R.string.chat_info_send_n_medias_message, getMediaSize());
            } else {
                message = getSenderNamePrefix(context, conversation, forDisplay, false, true) + context.getString(R.string.chat_info_send_n_medias_message, getMediaSize());
            }
        } else if (getType() == Chat.Type.IMAGE) {
            if (getMediaList().size() == 1) {
                if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                    message = context.getString(R.string.chat_info_you_chat) + getSenderNameToken() + context.getString(R.string.chat_info_send_photo_message);
                } else {
                    message = getSenderNamePrefix(context, conversation, forDisplay, false, true) + context.getString(R.string.chat_info_send_photo_message);
                }
            } else {
                if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                    message = context.getString(R.string.chat_info_you_chat) + getSenderNameToken() + context.getString(R.string.chat_info_you_send_n_photos_message, getMediaList().size());
                } else {
                    message = getSenderNamePrefix(context, conversation, forDisplay, false, true) + context.getString(R.string.chat_info_you_send_n_photos_message, getMediaList().size());
                }
            }
        } else if (getType() == Type.VIDEO) {
            String backPart = "";
            if (isAllVideo()) {
                backPart = context.getString(R.string.chat_info_send_n_videos_message, getMediaList().size());
            } else {
                backPart = context.getString(R.string.chat_info_send_video_message);
            }

            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                message = context.getString(R.string.chat_info_you_chat) + getSenderNameToken() + backPart;
            } else {
                message = getSenderNamePrefix(context, conversation, forDisplay, false, true) + backPart;
            }
        } else if (getMediaList().get(0).getType() == MediaType.GIF) {
            if (getMediaList().size() == 1) {
                if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                    message = context.getString(R.string.chat_info_you_chat) + getSenderNameToken() + context.getString(R.string.chat_info_send_gif_message);
                } else {
                    message = getSenderNamePrefix(context, conversation, forDisplay, false, true) + context.getString(R.string.chat_info_send_gif_message);
                }
            } else {
                if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                    message = context.getString(R.string.chat_info_you_chat) + getSenderNameToken() + context.getString(R.string.chat_info_send_n_gifs_message, getMediaSize());
                } else {
                    message = getSenderNamePrefix(context, conversation, forDisplay, false, true) + context.getString(R.string.chat_info_send_n_gifs_message, getMediaSize());
                }
            }
        } else if (getMediaList().get(0).getType() == MediaType.TENOR_GIF) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                message = context.getString(R.string.chat_info_you_chat) + getSenderNameToken() + context.getString(R.string.chat_info_send_tenor_gif_message);
            } else {
                message = getSenderNamePrefix(context, conversation, forDisplay, false, true) + context.getString(R.string.chat_info_send_tenor_gif_message);
            }
        }

        Timber.i("ChatType.from(mChatType): " + ChatType.from(mChatType) + ", message: " + message);
        return message;
    }

    private String getSenderNamePrefix(Context context,
                                       Conversation conversation,
                                       boolean forDisplay,
                                       boolean isEdited,
                                       boolean isIncludeSenderNameSpaceToken) {
        //For the group, there must be sender name and for one to one, there is no sender prefix name.
        if (conversation != null && conversation.isGroup()) {
            Timber.i("Group getSender(): " + getSender());
            if (getSender() != null) {
                if (forDisplay && SpecialTextRenderUtils.isMentionedYouInChat(context, getContent())) {
                    return context.getString(R.string.chat_notification_mention_you_in_message,
                            SpannableUtil.capitaliseOnlyFirstLetter(getSender().getFirstName())) +
                            (isEdited ? " " + context.getString(R.string.chat_info_edited_header_message) : "") + (isIncludeSenderNameSpaceToken ? getSenderNameToken() : " ");
                }

                return SpannableUtil.capitaliseOnlyFirstLetter(getSender().getFirstName()) +
                        (isEdited ? " " + context.getString(R.string.chat_info_edited_header_message) : "") +
                        (isIncludeSenderNameSpaceToken ? getSenderNameToken() : " ");
            }
        }

        return "";
    }

    private String getSenderNameToken() {
        return ": ";
    }

    public SendingType getSendingType() {
        return SendingType.from(mSendingType);
    }

    public String getSendingTypeAsString() {
        return mSendingType;
    }

    public void setSendingType(SendingType sendingType) {
        mSendingType = sendingType.mValue;
    }

    public void setRawSendingType(String value) {
        mSendingType = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Chat) {
            final String id = ((Chat) obj).getId();
            return !TextUtils.isEmpty(id) && !TextUtils.isEmpty(mId) && id.equals(mId);
        } else {
            return false;
        }
    }

    public MessageState getStatus() {
        return MessageState.fromValue(mStatus);
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public void setStatus(MessageState status) {
        mStatus = status.getValue();
    }

    public LinkPreviewModel getLinkPreviewModel() {
        return mLinkPreviewModel;
    }

    public void setLinkPreviewModel(LinkPreviewModel linkPreviewModel) {
        mLinkPreviewModel = linkPreviewModel;
    }

    public boolean isDeleted() {
        if (mIsDelete == null) {
            return false;
        }
        return mIsDelete;
    }

    public void setDelete(boolean delete) {
        mIsDelete = delete;
    }

    @Override
    public int hashCode() {
        if (mId != null) {
            return mId.hashCode();
        }

        return super.hashCode();
    }

    @Override
    public String toString() {
        return "id = " + mId + ", sender = " + mSender + ", date = " + mDate + ", content = " + mContent;
    }

    @Override
    public int compareTo(@NonNull Chat another) {
        return Long.compare(getTime(), another.getTime());
    }

    @Override
    public long getTime() {
        if (mDate == null) {
            return -1;
        }

        return mDate.getTime();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeLong(this.mDate != null ? this.mDate.getTime() : -1);
        dest.writeString(this.mContent);
        dest.writeTypedList(this.mMediaList);
        dest.writeString(this.mStatus);
        dest.writeString(this.mSenderId);
        dest.writeString(this.mProductId);
        dest.writeString(this.mChannelId);
        dest.writeString(this.mSendTo);
        dest.writeStringList(this.mSeen);
        dest.writeString(this.mSendingType);
        dest.writeValue(this.mIsTyping);
        dest.writeParcelable(this.mSender, flags);
        dest.writeParcelable(this.mProduct, flags);
        dest.writeValue(this.mIsDelete);
        dest.writeByte(this.mIsGroup ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.mRecipient, flags);
        dest.writeString(this.mGroupName);
        dest.writeTypedList(this.mSeenParticipants);
        dest.writeString(this.mChatType);
        dest.writeParcelable(this.mLinkPreviewModel, flags);
        dest.writeByte(this.mIsSavedFromSeenStatus ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mIsEncrypted ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.mMetaPreview);
        dest.writeByte(this.mIsDecrypted ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.mMetaGroup, flags);
        dest.writeValue(this.mCallDuration);
    }

    public enum Type {
        TEXT(1),
        AUDIO(2),
        IMAGE(3),
        TENOR_GIF(4),
        FILE(5),
        VIDEO(6),
        MIX_MEDIA(7),
        GIF(8);

        private final int mId;

        Type(int id) {
            mId = id;
        }

        public static Type getType(int id) {
            for (Type type : Type.values()) {
                if (id == type.getId()) {
                    return type;
                }
            }
            return TEXT;
        }

        public int getId() {
            return mId;
        }

    }

    public enum SendingType {
        NONE(null),
        TYPING("typing"),
        READ_STATUS("readStatus"),
        DELIVERED("delivered"),
        UPDATE("update"),
        DELETE("delete"),
        EMIT_USER("emitUserId"),
        UPDATE_USER("updateUser");

        private final String mValue;

        SendingType(String s) {
            mValue = s;
        }

        public static SendingType from(String value) {
            for (SendingType sendingType : SendingType.values()) {
                if (TextUtils.equals(sendingType.mValue, value)) {
                    return sendingType;
                }
            }
            return NONE;
        }

        public String getValue() {
            return mValue;
        }
    }

    public enum ChatType {

        MISSED_CALL("missedcall"),
        CALLED("called"),
        MISSED_VIDEO_CALL("videomissedcall"),
        VIDEO_CALLED("videocalled"),
        START_GROUP_VIDEO_CALL("groupvideostart"),
        END_GROUP_VIDEO_CALL("groupvideoended"),
        START_GROUP_CALL("groupaudiostart"),
        END_GROUP_CALL("groupaudioended"),
        ADD_GROUP_MEMBER("groupaddmember"),
        REMOVE_GROUP_MEMBER("groupremovemember"),
        CHANGE_GROUP_PICTURE("groupchangepicture"),
        CHANGE_GROUP_NAME("groupchangename"),
        CREATE_GROUP("createGroup"),
        DELETE_GROUP("deleteGroup"),
        UNKNOWN("UNKNOWN");

        private String mType;

        ChatType(String type) {
            this.mType = type;
        }

        public String getType() {
            return mType;
        }

        public static ChatType from(String value) {
            if (!TextUtils.isEmpty(value)) {
                for (ChatType chatType : ChatType.values()) {
                    if (TextUtils.equals(chatType.mType, value)) {
                        return chatType;
                    }
                }
            }

            return UNKNOWN;
        }
    }
}