package com.proapp.sompom.model;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.APIErrorMessageHelper;
import com.proapp.sompom.model.emun.AuthErrorType;
import com.proapp.sompom.model.result.LocalizationMessage;

public class SupportCustomErrorResponse {

    @SerializedName("code")
    private String mCode;

    @SerializedName("message")
    private LocalizationMessage mMessage; //Error message

    @SerializedName("translated")
    private boolean mIsTranslated;

    public String getCode() {
        return mCode;
    }

    public LocalizationMessage getMessage() {
        return mMessage;
    }

    public boolean isTranslated() {
        return mIsTranslated;
    }

    public boolean isSuccessful() {
        //For success response body these two properties will not response.
        return mMessage == null && mCode == null;
    }

    public String getLocalizationMessage(Context context) {
        if (mIsTranslated) {
            //Will take the error localize from response directly
            String error = APIErrorMessageHelper.getError(context, mMessage);
            if (!TextUtils.isEmpty(error)) {
                return error;
            }
        } else {
            //Will localize the error locally
            AuthErrorType errorType = AuthErrorType.fromValue(mCode);
            if (errorType.getMessage() != -1) {
                return context.getString(errorType.getMessage());
            }

        }
        return getDefaultLocalError(context);
    }

    private String getDefaultLocalError(Context context) {
        return context.getString(R.string.error_general_description);
    }
}
