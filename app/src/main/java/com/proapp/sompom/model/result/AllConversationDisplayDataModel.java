package com.proapp.sompom.model.result;

import java.util.List;

/**
 * Created by nuonveyo
 * on 1/7/19.
 */

public class AllConversationDisplayDataModel {

    private List<Conversation> mRecentConversation;
    private LoadMoreWrapper<Conversation> mMoreConversation;

    public List<Conversation> getRecentConversation() {
        return mRecentConversation;
    }

    public LoadMoreWrapper<Conversation> getMoreConversation() {
        return mMoreConversation;
    }

    public void setRecentConversation(List<Conversation> mUnreadConversationList) {
        this.mRecentConversation = mUnreadConversationList;
    }

    public void setMoreConversation(LoadMoreWrapper<Conversation> mConversation) {
        this.mMoreConversation = mConversation;
    }
}
