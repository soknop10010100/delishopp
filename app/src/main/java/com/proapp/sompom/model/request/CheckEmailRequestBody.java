package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Veasna Chhom on 2/10/22.
 */

public class CheckEmailRequestBody {

    @SerializedName("email")
    private String mEmail;

    public CheckEmailRequestBody(String email) {
        mEmail = email;
    }
}
