package com.proapp.sompom.model.emun;

public enum CallAudioOutputType {

    PHONE(1),
    SPEAKER(2),
    BLUETOOTH(3);

    private int mId;

    CallAudioOutputType(int id) {
        mId = id;
    }

    public static CallAudioOutputType getType(int id) {
        for (CallAudioOutputType commentItemType : CallAudioOutputType.values()) {
            if (commentItemType.getId() == id) {
                return commentItemType;
            }
        }
        return PHONE;
    }

    public int getId() {
        return mId;
    }
}
