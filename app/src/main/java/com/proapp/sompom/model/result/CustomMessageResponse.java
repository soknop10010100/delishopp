package com.proapp.sompom.model.result;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.helper.APIErrorMessageHelper;

public class CustomMessageResponse {

    @SerializedName("status")
    private Integer mStatus;

    @SerializedName("message")
    private LocalizationMessage mMessage;

    public int getStatus() {
        if (mStatus != null) {
            return mStatus;
        }

        return 0;
    }

    public String getMessage(Context context) {
        if (mMessage != null) {
            return APIErrorMessageHelper.getError(context, mMessage);
        }

        return "";
    }

    public boolean isSuccessful() {
        if (mStatus == null) {
            return true;
        }
        return mStatus > 0;
    }
}
