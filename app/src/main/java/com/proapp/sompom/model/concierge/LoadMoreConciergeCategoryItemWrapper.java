package com.proapp.sompom.model.concierge;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.result.LoadMoreWrapper;

/**
 * Created by Chhom Veasna on 6/8/22.
 */

public class LoadMoreConciergeCategoryItemWrapper extends LoadMoreWrapper<JsonObject> {

    @SerializedName("category")
    private ConciergeShopCategory mConciergeSubCategory;

    public ConciergeShopCategory getConciergeCategory() {
        return mConciergeSubCategory;
    }
}
