package com.proapp.sompom.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class SupportCustomErrorResponse3 {

    @SerializedName("statusCode")
    private String mCode;

    @SerializedName("message")
    private String mMessage;

    public String getCode() {
        return mCode;
    }

    public String getMessage() {
        return mMessage;
    }

    public boolean isSuccessful() {
        return TextUtils.isEmpty(mMessage);
    }
}
