package com.proapp.sompom.model.emun;

import android.text.TextUtils;

public enum TimelineDetailRedirectionType {

    LIKE("Like"),
    COMMENT("Comment"),
    NEW_POST("NewPost"),
    FLOATING_VIDEO("FloatingVideo"),
    FROM_WALL("FromWall"),
    UNKNOWN("Unknown");

    private String mValue;

    TimelineDetailRedirectionType(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public static TimelineDetailRedirectionType fromValue(String value) {
        if (!TextUtils.isEmpty(value)) {
            for (TimelineDetailRedirectionType timelineDetailRedirectionType : TimelineDetailRedirectionType.values()) {
                if (timelineDetailRedirectionType.mValue.equals(value)) {
                    return timelineDetailRedirectionType;
                }
            }
        }

        return UNKNOWN;
    }
}
