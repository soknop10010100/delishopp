package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.ConciergeOrderItemDisplayAdaptive;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;
import com.proapp.sompom.model.DifferentAdaptive;

import java.util.List;
import java.util.Objects;

import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.RealmClass;

/**
 * Created by Or Vitovongsak on 31/8/21.
 */

@RealmClass
public class ConciergeShop
//        extends AbsConciergeModel
        implements RealmModel,
        DifferentAdaptive<ConciergeShop>,
        Parcelable,
        ConciergeOrderItemDisplayAdaptive,
        ConciergeShopDetailDisplayAdaptive {

    private static final String FIELD_LOCATION = "address";
    private static final String FIELD_PICTURE = "picture";
    private static final String FIELD_LOGO = "logo";
    private static final String FIELD_DELIVERY_FEE = "deliveryFee";
    private static final String FIELD_MENU = "menus";

    @SerializedName(AbsConciergeModel.FIELD_ID)
    private String mId;

    @SerializedName(AbsConciergeModel.FIELD_NAME)
    private String mName;

    @Ignore
    @SerializedName(FIELD_LOCATION)
    private String mLocation;

    @Ignore
    @SerializedName(AbsConciergeModel.FIELD_DESCRIPTION)
    private String mDescription;

    @Ignore
    @SerializedName(FIELD_PICTURE)
    private String mPicture;

    @SerializedName(FIELD_LOGO)
    private String mLogo;

    @Ignore
    @SerializedName(FIELD_DELIVERY_FEE)
    private float mDeliveryFee;

    @Ignore
    @SerializedName(FIELD_MENU)
    private List<ConciergeMenu> mConciergeMenus;

    @Ignore
    @SerializedName(AbsConciergeModel.FIELD_HASH)
    private Integer mHash;

    @Ignore
    @SerializedName(AbsConciergeModel.FIELD_LATITUDE)
    private Double mLatitude;

    @Ignore
    @SerializedName(AbsConciergeModel.FIELD_LONGITUDE)
    private Double mLongitude;

    public ConciergeShop() {
    }

    protected ConciergeShop(Parcel in) {
        mId = in.readString();
        mName = in.readString();
        mLocation = in.readString();
        mDescription = in.readString();
        mPicture = in.readString();
        mLogo = in.readString();
        mDeliveryFee = in.readFloat();
        mConciergeMenus = in.createTypedArrayList(ConciergeMenu.CREATOR);
        mHash = (Integer) in.readSerializable();
        mLatitude = (Double) in.readSerializable();
        mLongitude = (Double) in.readSerializable();
    }

    public static final Creator<ConciergeShop> CREATOR = new Creator<ConciergeShop>() {
        @Override
        public ConciergeShop createFromParcel(Parcel in) {
            return new ConciergeShop(in);
        }

        @Override
        public ConciergeShop[] newArray(int size) {
            return new ConciergeShop[size];
        }
    };

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        this.mLocation = location;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getPicture() {
        return mPicture;
    }

    public void setPicture(String picture) {
        this.mPicture = picture;
    }

    public String getLogo() {
        return mLogo;
    }

    public void setLogo(String logo) {
        mLogo = logo;
    }

    public float getDeliveryFee() {
        return mDeliveryFee;
    }

    public int getHash() {
        if (mHash != null) {
            return mHash;
        }

        return 0;
    }

    public void setHash(int hash) {
        mHash = hash;
    }

    public void setDeliveryFee(float deliveryFee) {
        this.mDeliveryFee = deliveryFee;
    }

    public List<ConciergeMenu> getConciergeMenus() {
        return mConciergeMenus;
    }

    public void setConciergeMenus(List<ConciergeMenu> conciergeMenus) {
        mConciergeMenus = conciergeMenus;
    }

    public Double getLatitude() {
        if (mLatitude != null) {
            return mLatitude;
        }

        return 0D;
    }

    public ConciergeShop setLatitude(Double latitude) {
        this.mLatitude = latitude;
        return this;
    }

    public Double getLongitude() {
        if (mLongitude != null) {
            return mLongitude;
        }

        return 0D;
    }

    public ConciergeShop setLongitude(Double mLongitude) {
        this.mLongitude = mLongitude;
        return this;
    }

    @Override
    public boolean areItemsTheSame(ConciergeShop other) {
        return getId().matches(other.getId());
    }

    @Override
    public boolean areContentsTheSame(ConciergeShop other) {
        return areItemsTheSame(other) &&
                TextUtils.equals(getName(), other.getName()) &&
                TextUtils.equals(getDescription(), other.getDescription()) &&
                TextUtils.equals(getLocation(), other.getLocation()) &&
                TextUtils.equals(getPicture(), other.getPicture()) &&
                getDeliveryFee() == other.getDeliveryFee() &&
                areConciergeMenuTheSame(other.getConciergeMenus()) &&
                getLatitude().equals(other.getLatitude()) &&
                getLongitude().equals(other.getLongitude());
    }

    private boolean areConciergeMenuTheSame(List<ConciergeMenu> other) {
        if ((getConciergeMenus() == null && other == null) ||
                (getConciergeMenus().isEmpty() && other.isEmpty())) {
            return true;
        } else if (getConciergeMenus() != null && other != null
                && getConciergeMenus().size() == other.size()) {

            for (int index = 0; index < getConciergeMenus().size(); index++) {
                if (!getConciergeMenus().get(index).areContentsTheSame(other.get(index))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Clone the shop with the entire menu
     */
    public ConciergeShop newClone() {
        ConciergeShop shop = new ConciergeShop();
        shop.setId(getId());
        shop.setName(getName());
        shop.setLocation(getLocation());
        shop.setDescription(getDescription());
        shop.setPicture(getPicture());
        shop.setLogo(getLogo());
        shop.setConciergeMenus(getConciergeMenus());

        return shop;
    }

    /**
     * Clone shop but with no product list
     */
    public ConciergeShop cloneEmptyShop() {
        ConciergeShop shop = new ConciergeShop();
        shop.setId(getId());
        shop.setName(getName());
        shop.setLocation(getLocation());
        shop.setDescription(getDescription());
        shop.setPicture(getPicture());
        shop.setLogo(getLogo());

        return shop;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof ConciergeShop) {
            return TextUtils.equals(((ConciergeShop) other).getId(), getId());
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mName, mLocation, mDescription, mPicture, mLogo, mDeliveryFee, mConciergeMenus);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mName);
        dest.writeString(mLocation);
        dest.writeString(mDescription);
        dest.writeString(mPicture);
        dest.writeString(mLogo);
        dest.writeFloat(mDeliveryFee);
        dest.writeTypedList(mConciergeMenus);
        dest.writeSerializable(mHash);
        dest.writeSerializable(mLatitude);
        dest.writeSerializable(mLongitude);
    }
}
