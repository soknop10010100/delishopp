package com.proapp.sompom.model.emun;

/**
 * Created by nuonveyo on 6/5/18.
 */

public enum ErrorLoadingType {

    GENERAL_ERROR(0),
    NO_ITEM(0),
    EMPTY_DATA(0);

    private int mIcon;

    ErrorLoadingType(int icon) {
        mIcon = icon;
    }

    public int getIcon() {
        return mIcon;
    }
}
