package com.proapp.sompom.model.result;

/**
 * Created by Chhom Veasna on 2/4/2020.
 */
public class SingleResponseWrapper<D> extends ResponseWrapper {

    private D mData;

    public D getData() {
        return mData;
    }

    public void setData(D data) {
        mData = data;
    }
}
