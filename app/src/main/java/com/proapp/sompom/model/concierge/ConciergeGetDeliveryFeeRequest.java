package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;

public class ConciergeGetDeliveryFeeRequest {

    @SerializedName("addressId")
    private String mAddressId;

    @SerializedName("shopId")
    private String mShopId;

    @SerializedName("basketTotalPrice")
    private double mBasketTotalPrice;

    @SerializedName("orderType")
    private int mOrderType;

    @SerializedName("order_id")
    private String  mOrderId;

    public ConciergeGetDeliveryFeeRequest(String addressId, String shopId, double basketTotalPrice, int orderType , String orderId) {
        mAddressId = addressId;
        mShopId = shopId;
        mBasketTotalPrice = basketTotalPrice;
        mOrderType = orderType;
        mOrderId = orderId;
    }
}
