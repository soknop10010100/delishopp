package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Veasna Chhom on 2/15/21.
 */

public class GetAllConversationResponse extends LoadMoreWrapper<Conversation> {

    @SerializedName("recentLimit")
    private int mRecentLimit;

    public int getRecentLimit() {
        return mRecentLimit;
    }

    public boolean isThereRecentLimit() {
        return mRecentLimit > 0;
    }
}
