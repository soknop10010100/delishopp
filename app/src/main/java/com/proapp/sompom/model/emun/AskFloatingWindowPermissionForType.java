package com.proapp.sompom.model.emun;

import android.text.TextUtils;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public enum AskFloatingWindowPermissionForType {

    VIDEO("Video"),
    CALL("Call");

    private String mValue;

    AskFloatingWindowPermissionForType(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public static AskFloatingWindowPermissionForType fromValue(String value) {
        for (AskFloatingWindowPermissionForType type : AskFloatingWindowPermissionForType.values()) {
            if (TextUtils.equals(type.mValue, value)) {
                return type;
            }
        }

        return VIDEO;
    }
}
