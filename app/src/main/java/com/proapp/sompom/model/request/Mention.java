package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

public class Mention {
    @SerializedName("typingText")
    private String mTypingText;

    public Mention(String typingText) {
        mTypingText = typingText;
    }
}
