package com.proapp.sompom.model.result;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class BaseSupportCustomErrorResponse {

    @SerializedName("statusCode")
    private int mStatusCode;

    @SerializedName("error")
    private String mError;

    @SerializedName("message")
    private String mMessage;

    public boolean isSuccessful() {
        return TextUtils.isEmpty(mError);
    }

    public String getMessage() {
        return mMessage;
    }
}
