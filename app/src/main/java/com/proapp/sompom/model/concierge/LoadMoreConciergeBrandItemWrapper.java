package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.result.LoadMoreWrapper;

import java.util.List;

/**
 * Created Chhom Veasna on 5/4/22.
 */

public class LoadMoreConciergeBrandItemWrapper extends LoadMoreWrapper<ConciergeMenuItem> {

    @SerializedName("brand")
    private ConciergeBrand mConciergeBrand;

    public ConciergeBrand getConciergeBrand() {
        return mConciergeBrand;
    }
}
