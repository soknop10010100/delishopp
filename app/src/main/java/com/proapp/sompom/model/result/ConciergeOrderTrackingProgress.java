package com.proapp.sompom.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.emun.ConciergeOrderStatus;

import java.util.Date;

/**
 * Created by Or Vitovongsak on 16/12/21.
 */
public class ConciergeOrderTrackingProgress implements Parcelable {

    @SerializedName("title")
    private String mTitle;

    @SerializedName("status")
    private String mStatus;

    @SerializedName("date")
    private Date mDate;

    public ConciergeOrderTrackingProgress() {
    }

    public ConciergeOrderTrackingProgress(String title, String status, Date date) {
        this.mTitle = title;
        this.mStatus = status;
        this.mDate = date;
    }

    protected ConciergeOrderTrackingProgress(Parcel in) {
        mTitle = in.readString();
        mStatus = in.readString();
        mDate = (Date) in.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mStatus);
        dest.writeSerializable(mDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConciergeOrderTrackingProgress> CREATOR = new Creator<ConciergeOrderTrackingProgress>() {
        @Override
        public ConciergeOrderTrackingProgress createFromParcel(Parcel in) {
            return new ConciergeOrderTrackingProgress(in);
        }

        @Override
        public ConciergeOrderTrackingProgress[] newArray(int size) {
            return new ConciergeOrderTrackingProgress[size];
        }
    };

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public ConciergeOrderStatus getStatus() {
        return ConciergeOrderStatus.fromValue(mStatus);
    }

    public void setStatus(ConciergeOrderStatus status) {
        this.mStatus = status.getValue();
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        this.mDate = date;
    }

}
