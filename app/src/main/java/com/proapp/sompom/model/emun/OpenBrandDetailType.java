package com.proapp.sompom.model.emun;

public enum OpenBrandDetailType {

    OPEN_AS_NORMAL_MODE(1),
    OPEN_AS_SEARCH_GENERAL_PRODUCT_MODE(2),
    OPEN_AS_SEARCH_SUPPLIER_PRODUCT_MODE(3);

    private int mValue;

    OpenBrandDetailType(int value) {
        mValue = value;
    }

    public int getValue() {
        return mValue;
    }

    public static OpenBrandDetailType fromValue(int value) {
        for (OpenBrandDetailType changePasswordActionType : values()) {
            if (changePasswordActionType.getValue() == value) {
                return changePasswordActionType;
            }
        }

        return OPEN_AS_NORMAL_MODE;
    }
}
