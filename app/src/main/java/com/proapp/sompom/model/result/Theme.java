package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;

public class Theme {

    @SerializedName("themeId")
    private int mId;
    @SerializedName("themeName")
    private String mThemeName;
    @SerializedName("companyLogo")
    private String mCompanyLogo;
    @SerializedName("companyAvatar")
    private String mCompanyAvatar;
    @SerializedName("mode")
    private String mMode;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getThemeName() {
        return mThemeName;
    }

    public void setThemeName(String themeName) {
        mThemeName = themeName;
    }

    public String getCompanyLogo() {
        return mCompanyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        mCompanyLogo = companyLogo;
    }

    public String getCompanyAvatar() {
        return mCompanyAvatar;
    }

    public void setCompanyAvatar(String companyAvatar) {
        mCompanyAvatar = companyAvatar;
    }

    public String getMode() {
        return mMode;
    }

    public void setMode(String mode) {
        mMode = mode;
    }
}
