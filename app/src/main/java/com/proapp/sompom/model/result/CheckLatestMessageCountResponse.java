package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;

public class CheckLatestMessageCountResponse {

    @SerializedName("count")
    private int mCount;

    public int getCount() {
        return mCount;
    }
}
