package com.proapp.sompom.model.emun;

/**
 * Created by Veasna Chhom on 5/10/22.
 */
public enum ApplicationMode {

    NORMAL_MODE(0),
    EXPRESS_MODE(1);

    private final int mType;

    ApplicationMode(int type) {
        mType = type;
    }

    public static ApplicationMode from(int value) {
        for (ApplicationMode deeplinkType : ApplicationMode.values()) {
            if (deeplinkType.mType == value) {
                return deeplinkType;
            }
        }
        return NORMAL_MODE;
    }

    public int getType() {
        return mType;
    }
}
