package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;

public class ConciergeGetWalletResponse {

    @SerializedName(AbsConciergeModel.FIELD_AMOUNT)
    private Double mBalance;


    public double getBalance() {
        if (mBalance != null) {
            return mBalance;
        }

        return 0;
    }
}
