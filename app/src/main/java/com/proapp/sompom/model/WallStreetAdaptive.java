package com.proapp.sompom.model;

import android.os.Parcelable;
import android.text.TextUtils;

import com.proapp.sompom.model.emun.PublishItem;
import com.proapp.sompom.model.result.ContentStat;
import com.proapp.sompom.model.result.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by He Rotha on 10/31/18.
 */
public interface WallStreetAdaptive extends Parcelable {

    String FIELD_ID = "_id";
    String FIELD_PUBLISH = "visibility";
    String FIELD_CREATE_DATE = "createdAt";
    String FIELD_USER_VIEW = "userView";
    String FIELD_LONGITUDE = "longitude";
    String FIELD_LATITUDE = "latitude";
    String FIELD_ADDRESS = "address"; //Full address
    String LOCATION_NAME = "locationName";
    String LOCATION_COUNTRY = "locationCountry";
    String FIELD_CITY = "city";
    String FIELD_COUNTRY = "country";
    String FIELD_SHARE_URL = "shareUrl";
    String FIELD_CONTENT_STAT = "contentStat";

    String getId();

    void setId(String id);

    default boolean isLike() {
        if (getContentStat() != null) {
            return getContentStat().getLiked();
        }

        return false;
    }

    default void setLike(boolean like) {
        if (getContentStat() != null) {
            getContentStat().setLiked(like);
        }
    }

    Date getCreateDate();

    void setCreateDate(Date createDate);

    default List<User> getUserView() {
        if (getContentStat() != null) {
            return getContentStat().getViewers();
        }

        return new ArrayList<>();
    }

    default void setUserView(List<User> userView) {
        if (getContentStat() != null) {
            getContentStat().setViewers(userView);
        }
    }

    PublishItem getPublish();

    void setPublish(int publish);

    double getLatitude();

    void setLatitude(double latitude);

    String getAddress();

    void setAddress(String address);

    String getCity();

    void setCity(String city);

    String getCountry();

    void setCountry(String country);

    default String getDisplayAddress() {
        if (!TextUtils.isEmpty(getCity()) && !TextUtils.isEmpty(getLocationCountry())) {
            return getCity() + ", " + getLocationCountry();
        } else if (TextUtils.isEmpty(getCity())) {
            return getLocationCountry();
        } else {
            return getCity();
        }
    }

    double getLongitude();

    void setLongitude(double longitude);

    User getUser();

    List<Media> getMedia();

    String getDescription();

    String getShareUrl();

    ContentStat getContentStat();

    void setContentStat(ContentStat contentStat);

    default void setLocationName(String locationName) {

    }

    default String getLocationName() {
        return null;
    }

    default void setLocationCountry(String locationCountry) {

    }

    default String getLocationCountry() {
        return getCountry();
    }

    default boolean shouldShowLinkPreview() {
        return false;
    }

    default boolean shouldShowPlacePreview() {
        return false;
    }
}
