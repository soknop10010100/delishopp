package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;

public class ConciergeGetDeliveryFeeResponse extends SupportConciergeCustomErrorResponse {

    @SerializedName("deliveryFee")
    private double mDeliveryFee;

    @SerializedName("containerFee")
    private double mContainerFee;

    @SerializedName("totalKm")
    private double mTotalKm;

    public double getDeliveryFee() {
        return mDeliveryFee;
    }

    public void setDeliveryFee(double deliveryFee) {
        mDeliveryFee = deliveryFee;
    }

    public double getContainerFee() {
        return mContainerFee;
    }

    public void setContainerFee(double containerFee) {
        mContainerFee = containerFee;
    }

    public double getTotalKm() {
        return mTotalKm;
    }

    public void setTotalKm(double totalKm) {
        mTotalKm = totalKm;
    }
}
