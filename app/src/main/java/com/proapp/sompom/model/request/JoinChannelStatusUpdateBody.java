package com.proapp.sompom.model.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhom Veasna on 7/8/2020.
 */
public class JoinChannelStatusUpdateBody implements Parcelable {

    @SerializedName("userId")
    private String mUserId;
    @SerializedName("conversationId")
    private String mConversationId;
    @SerializedName("isVideo")
    private Boolean mIsVideo;
    @SerializedName("isChannelOpen")
    private Boolean mIsChannelOpen;
    @SerializedName("sendNotification")
    private Boolean mIsSendNotification;
    @SerializedName("duration")
    private Integer mCallDuration;
    @SerializedName("initCall")
    private Boolean mInitCall;

    public JoinChannelStatusUpdateBody() {
    }

    public JoinChannelStatusUpdateBody(String userId, String conversationId) {
        mUserId = userId;
        mConversationId = conversationId;
    }

    public JoinChannelStatusUpdateBody(String userId, String conversationId, boolean isVideo) {
        mUserId = userId;
        mConversationId = conversationId;
        mIsVideo = isVideo;
    }

    public String getUserId() {
        return mUserId;
    }

    public String getConversationId() {
        return mConversationId;
    }

    public Boolean getVideo() {
        return mIsVideo != null && mIsVideo;
    }

    public Boolean getChannelOpen() {
        return mIsChannelOpen != null && mIsChannelOpen;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public void setConversationId(String conversationId) {
        mConversationId = conversationId;
    }

    public void setVideo(Boolean video) {
        mIsVideo = video;
    }

    public void setChannelOpen(Boolean channelOpen) {
        mIsChannelOpen = channelOpen;
    }

    public void setCallDuration(Integer callDuration) {
        mCallDuration = callDuration;
    }

    public Boolean getSendNotification() {
        return mIsSendNotification != null && mIsSendNotification;
    }

    public void setSendNotification(Boolean sendNotification) {
        mIsSendNotification = sendNotification;
    }

    public Boolean getInitCall() {
        return mInitCall;
    }

    public void setInitCall(Boolean initCall) {
        mInitCall = initCall;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mUserId);
        dest.writeString(this.mConversationId);
        dest.writeValue(this.mIsVideo);
        dest.writeValue(this.mIsSendNotification);
        dest.writeValue(this.mIsChannelOpen);
        dest.writeValue(this.mCallDuration);
        dest.writeValue(this.mInitCall);
    }

    protected JoinChannelStatusUpdateBody(Parcel in) {
        this.mUserId = in.readString();
        this.mConversationId = in.readString();
        this.mIsVideo = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mIsSendNotification = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mIsChannelOpen = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mCallDuration = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mInitCall = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Creator<JoinChannelStatusUpdateBody> CREATOR = new Creator<JoinChannelStatusUpdateBody>() {
        @Override
        public JoinChannelStatusUpdateBody createFromParcel(Parcel source) {
            return new JoinChannelStatusUpdateBody(source);
        }

        @Override
        public JoinChannelStatusUpdateBody[] newArray(int size) {
            return new JoinChannelStatusUpdateBody[size];
        }
    };
}
