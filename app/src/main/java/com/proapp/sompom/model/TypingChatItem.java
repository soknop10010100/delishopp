package com.proapp.sompom.model;

import com.proapp.sompom.model.result.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by He Rotha on 9/6/18.
 */
public class TypingChatItem implements BaseChatModel {

    private final List<User> mTypingSenderList = new ArrayList<>();

    public List<User> getTypingSender() {
        return mTypingSenderList;
    }

    public void addTypingSender(User sender) {
        if (!mTypingSenderList.contains(sender)) {
            mTypingSenderList.add(sender);
        }
    }

    public void removeTypingSender(User sender) {
        mTypingSenderList.remove(sender);
    }

    public boolean isEmpty() {
        return mTypingSenderList.isEmpty();
    }

    @Override
    public long getTime() {
        return 0;
    }

    @Override
    public String getId() {
        return UUID.randomUUID().toString();
    }
}
