package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.result.ConciergeMenuItemOption;

import java.util.List;

/**
 * Created by Veasna Chhom on 19/10/21.
 */

public class ConciergeOrderHistoryComboItem extends AbsConciergeModel {

    @SerializedName(FIELD_NAME)
    private String mName;

    @SerializedName(FIELD_DESCRIPTION)
    private String mDescription;

    @SerializedName(FIELD_PICTURE)
    private String mPicture;

    @SerializedName(FIELD_TYPE)
    private String mType;

    @SerializedName(FIELD_DEFAULT_PRICE)
    private float mDefaultPrice;

    @SerializedName(FIELD_PRICE)
    private float mPrice;

    @SerializedName(FIELD_SHOP_ID)
    private String mShopId;

    @SerializedName(FIELD_OPTIONS)
    private List<ConciergeMenuItemOption> mOptions;

    @SerializedName(FIELD_CHOICES)
    private List<ConciergeComboChoiceSection> mChoices;

    @SerializedName(FIELD_SHOP)
    private ConciergeShop mShop;

    // For keeping track of number of item currently in cart
    @SerializedName("orderUnitCount")
    private int mCartCount = 0;

    // For keeping track of base price + selected options price
    @SerializedName("accumulatedPrice")
    private double mAccumulatedPrice;

    // For keeping track of total price, meaning accumulated price + count
    private double mTotalPrice;

    // Special instruction when user add product to cart
    private String mSpecialInstruction;

    public List<ConciergeMenuItemOption> getOptions() {
        return mOptions;
    }

    public String getName() {
        return mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getPicture() {
        return mPicture;
    }

    public String getType() {
        return mType;
    }

    public float getPrice() {
        return mPrice;
    }

    public float getDefaultPrice() {
        return mDefaultPrice;
    }

    public String getShopId() {
        return mShopId;
    }

    public List<ConciergeComboChoiceSection> getChoices() {
        return mChoices;
    }

    public ConciergeShop getShop() {
        return mShop;
    }

    public int getCartCount() {
        return mCartCount;
    }

    public double getAccumulatedPrice() {
        return mAccumulatedPrice;
    }

    public double getTotalPrice() {
        return mTotalPrice;
    }

    public String getSpecialInstruction() {
        return mSpecialInstruction;
    }
}
