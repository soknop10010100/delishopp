package com.proapp.sompom.model.emun;

import android.text.TextUtils;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public enum AuthConfigurationType {

    AGENCY("agency"),
    EMAIL("Email"),
    PHONE("Phone"),
    PHONE_AND_EMAIL("phoneNemail"),
    PHONE_AND_PASSWORD("phoneNpassword"),
    PHONE_AND_EMAIL_AND_PASSWORD("phoneNemailNpassword"),
    EMAIL_AND_PHONE("emailNphone"),
    NONE("None");

    private String mValue;

    AuthConfigurationType(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public static AuthConfigurationType fromValue(String value) {
        for (AuthConfigurationType type : AuthConfigurationType.values()) {
            if (!TextUtils.isEmpty(value) && TextUtils.equals(type.mValue.toLowerCase(),
                    value.toLowerCase())) {
                return type;
            }
        }

        return NONE;
    }
}
