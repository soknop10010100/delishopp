package com.proapp.sompom.model;

import android.text.TextUtils;

import com.proapp.sompom.helper.IntentData;
import com.proapp.sompom.model.emun.ViewType;
import com.proapp.sompom.model.result.JumpCommentResponse;

/**
 * Created by nuonveyo on 12/17/18.
 */

public class LoadCommentDirection implements Adaptive {

    private String mRedirection = JumpCommentResponse.REDIRECTION_PREVIOUS;

    public LoadCommentDirection() {
    }

    public LoadCommentDirection(String redirection) {
        mRedirection = redirection;
    }

    public String getRedirection() {
        return mRedirection;
    }

    public boolean isLoadPreviousCommentMode() {
        return TextUtils.equals(mRedirection, JumpCommentResponse.REDIRECTION_PREVIOUS);
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public ViewType getTimelineViewType() {
        return null;
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting activity
    }
}
