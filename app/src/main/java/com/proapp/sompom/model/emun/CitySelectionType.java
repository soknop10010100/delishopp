package com.proapp.sompom.model.emun;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.StringRes;

import com.proapp.sompom.R;

/**
 * Created by Chhom Veasna on 6/20/22.
 */

public enum CitySelectionType {

    SELECT_HINT("SELECT_HINT", R.string.shop_new_address_city_input_hint),
    PHNOM_PENH("Phnom Penh", R.string.shop_new_address_city_phnom_penh_option_title),
    PROVINCE("Province", R.string.shop_new_address_city_province_option_title);

    private String mKey;
    private @StringRes
    int mDisplayResource;

    CitySelectionType(String key, int displayResource) {
        mKey = key;
        mDisplayResource = displayResource;
    }

    public String getDisplayValue(Context context) {
        return context.getString(mDisplayResource);
    }

    public String getKey() {
        return mKey;
    }

    public static CitySelectionType fromValue(String key) {
        for (CitySelectionType type : values()) {
            if (!TextUtils.isEmpty(key) && TextUtils.equals(key.toLowerCase(), type.mKey.toLowerCase())) {
                return type;
            }
        }

        return SELECT_HINT;
    }
}
