package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.WelcomeDataWrapper;

import java.util.List;

/**
 * Created by Chhom Veasna on 11/26/2019.
 */
public class AppSetting {

    @SerializedName("organisationName")
    private String mOrganizationName;

    @SerializedName("themes")
    private List<Theme> mThemes;

    @SerializedName("contactBackground")
    private String mBackgroundCover;

    @SerializedName("notificationSetting")
    private NotificationSetting mNotificationSetting;

    @SerializedName("welcome")
    private WelcomeDataWrapper mWelcomeData;

    @SerializedName("hideCompanyLogo")
    private boolean mHideCompanyLogo;

    @SerializedName("wallPlaceholderMessage")
    private String mWallPlaceholderMessage;

    public AppSetting() {
    }

    public void setOrganizationName(String organizationName) {
        mOrganizationName = organizationName;
    }

    public WelcomeDataWrapper getWelcomeData() {
        return mWelcomeData;
    }

    public void setWelcomeData(WelcomeDataWrapper welcomeData) {
        mWelcomeData = welcomeData;
    }

    public String getOrganizationName() {
        return mOrganizationName;
    }

    public String getBackgroundCover() {
        return mBackgroundCover;
    }

    public NotificationSetting getNotificationSetting() {
        return mNotificationSetting;
    }

    public List<Theme> getThemes() {
        return mThemes;
    }

    public void setNotificationSetting(NotificationSetting notificationSetting) {
        mNotificationSetting = notificationSetting;
    }

    public boolean isHideCompanyLogo() {
        return mHideCompanyLogo;
    }

    public String getWallPlaceholderMessage() {
        return mWallPlaceholderMessage;
    }

    public void setWallPlaceholderMessage(String wallPlaceholderMessage) {
        mWallPlaceholderMessage = wallPlaceholderMessage;
    }
}
