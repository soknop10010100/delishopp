package com.proapp.sompom.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhom Veasna on 7/17/2020.
 */
public class WelcomeDataItem {

    @SerializedName("title")
    private String mTitle;

    @SerializedName("descriptions")
    private String[] mDescriptions;

    public String getTitle() {
        return mTitle;
    }

    public String[] getDescriptions() {
        return mDescriptions;
    }
}
