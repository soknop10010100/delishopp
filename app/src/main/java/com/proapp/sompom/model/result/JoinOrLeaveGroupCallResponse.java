package com.proapp.sompom.model.result;

public class JoinOrLeaveGroupCallResponse {

    private boolean mIsVideoCall;
    private boolean mIsCallStillActive;

    public boolean isVideoCall() {
        return mIsVideoCall;
    }

    public boolean isCallStillActive() {
        return mIsCallStillActive;
    }

    public void setVideoCall(boolean videoCall) {
        mIsVideoCall = videoCall;
    }

    public void setCallStillActive(boolean callStillActive) {
        mIsCallStillActive = callStillActive;
    }
}
