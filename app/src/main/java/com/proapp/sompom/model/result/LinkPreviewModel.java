package com.proapp.sompom.model.result;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.BasePreviewModel;
import com.proapp.sompom.model.DifferentAdaptive;
import com.proapp.sompom.model.GroupMediaAdaptive;

import java.util.UUID;

import io.realm.RealmObject;

/**
 * Created by nuonveyo on 10/26/18.
 */

public class LinkPreviewModel extends RealmObject implements Parcelable,
        DifferentAdaptive<LinkPreviewModel>,
        GroupMediaAdaptive {
    public static final Parcelable.Creator<LinkPreviewModel> CREATOR = new Parcelable.Creator<LinkPreviewModel>() {
        @Override
        public LinkPreviewModel createFromParcel(Parcel source) {
            return new LinkPreviewModel(source);
        }

        @Override
        public LinkPreviewModel[] newArray(int size) {
            return new LinkPreviewModel[size];
        }
    };

    private String mId;
    @SerializedName("logo")
    private String mLogo;
    @SerializedName("image")
    private String mImage;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("url")
    private String mLink;
    @SerializedName("description")
    private String mDescription;
    @SerializedName(BasePreviewModel.TYPE)
    private String mType;
    @SerializedName(BasePreviewModel.SHOULD_PREVIEW)
    private Boolean mShouldPreview;
    @SerializedName("fileName")
    private String mFileName;
    @SerializedName("fileExtension")
    private String mFileExtension;
    @SerializedName("width")
    private Integer mWidth;
    @SerializedName("height")
    private Integer mHeight;
    @SerializedName("fileType")
    private String mFileType;
    private Boolean mIsLinkDownloaded;
    private String mResourceContent;

    public static LinkPreviewModel newPostInstance(LinkPreviewModel model) {
        LinkPreviewModel linkPreviewModel = new LinkPreviewModel();
        linkPreviewModel.setId(null);
        linkPreviewModel.setTitle(model.getTitle());
        linkPreviewModel.setLogo(model.getLogo());
        linkPreviewModel.setLink(model.getLink());
        linkPreviewModel.setImage(model.getImage());
        linkPreviewModel.setDescription(model.getDescription());
        linkPreviewModel.setType(model.getType());
        linkPreviewModel.setShouldPreview(model.getShouldPreview());
        linkPreviewModel.setWidth(model.getWidth());
        linkPreviewModel.setHeight(model.getHeight());
        linkPreviewModel.setFileName(model.getFileName());
        linkPreviewModel.setFileExtension(model.getFileExtension());
        linkPreviewModel.setFileType(model.getFileType());

        return linkPreviewModel;
    }

    public String getFileName() {
        return mFileName;
    }

    public void setFileName(String fileName) {
        mFileName = fileName;
    }

    public String getFileExtension() {
        return mFileExtension;
    }

    public void setFileExtension(String fileExtension) {
        mFileExtension = fileExtension;
    }

    public Integer getWidth() {
        return mWidth;
    }

    public void setWidth(Integer width) {
        mWidth = width;
    }

    public Integer getHeight() {
        return mHeight;
    }

    public void setHeight(Integer height) {
        mHeight = height;
    }

    public String getFileType() {
        return mFileType;
    }

    public void setFileType(String fileType) {
        mFileType = fileType;
    }

    public Boolean getLinkDownloaded() {
        return mIsLinkDownloaded;
    }

    public void setLinkDownloaded(Boolean linkDownloaded) {
        mIsLinkDownloaded = linkDownloaded;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public Boolean getShouldPreview() {
        return mShouldPreview;
    }

    public void setShouldPreview(Boolean shouldPreview) {
        mShouldPreview = shouldPreview;
    }

    public LinkPreviewModel() {
        mId = UUID.randomUUID().toString();
        mType = BasePreviewModel.PreviewType.LINK.getType();
    }

    public LinkPreviewModel(String id, String resourceContent) {
        mId = id;
        mResourceContent = resourceContent;
    }

    protected LinkPreviewModel(Parcel in) {
        this.mId = in.readString();
        this.mResourceContent = in.readString();
        this.mImage = in.readString();
        this.mDescription = in.readString();
        this.mLink = in.readString();
        this.mTitle = in.readString();
        this.mLogo = in.readString();
        this.mIsLinkDownloaded = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mType = in.readString();
        this.mShouldPreview = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mWidth = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mHeight = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mFileName = in.readString();
        this.mFileExtension = in.readString();
        this.mFileType = in.readString();
    }

    public String getId() {
        return mId;
    }

    public String getResourceContent() {
        return mResourceContent;
    }

    public void setResourceContent(String resourceContent) {
        mResourceContent = resourceContent;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public boolean isLinkDownloaded() {
        return mIsLinkDownloaded;
    }

    public void setLinkDownloaded(boolean linkDownloaded) {
        mIsLinkDownloaded = linkDownloaded;
    }

    public String getLogo() {
        return mLogo;
    }

    public void setLogo(String logo) {
        mLogo = logo;
    }

    public boolean isValidPreviewData() {
        BasePreviewModel.PreviewType type = BasePreviewModel.PreviewType.getFromValue(getType());
        if (type == BasePreviewModel.PreviewType.LINK) {
            return !(TextUtils.isEmpty(getTitle()) &&
                    TextUtils.isEmpty(getDescription()) &&
                    TextUtils.isEmpty(getImage()) &&
                    TextUtils.isEmpty(getLogo()));
        }

        return true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mResourceContent);
        dest.writeString(this.mImage);
        dest.writeString(this.mDescription);
        dest.writeString(this.mLink);
        dest.writeString(this.mTitle);
        dest.writeString(this.mLogo);
        dest.writeValue(this.mIsLinkDownloaded);
        dest.writeString(this.mType);
        dest.writeValue(this.mShouldPreview);
        dest.writeValue(this.mWidth);
        dest.writeValue(this.mHeight);
        dest.writeString(this.mFileName);
        dest.writeString(this.mFileExtension);
        dest.writeString(this.mFileType);
    }

    @Override
    public boolean areItemsTheSame(LinkPreviewModel other) {
        return getLink().equals(other.getLink());
    }

    @Override
    public boolean areContentsTheSame(LinkPreviewModel other) {
        return areItemsTheSame(other) &&
                TextUtils.equals(getTitle(), other.getTitle()) &&
                TextUtils.equals(getLogo(), other.getLogo()) &&
                TextUtils.equals(getImage(), other.getImage()) &&
                TextUtils.equals(getDescription(), other.getDescription()) &&
                TextUtils.equals(getType(), other.getType()) &&
                arePreviewVisibilityTheSame(other);
    }

    private boolean arePreviewVisibilityTheSame(LinkPreviewModel other) {
        if (getShouldPreview() == null && other.getShouldPreview() == null) {
            return true;
        } else {
            return getShouldPreview().equals(other.getShouldPreview());
        }
    }

    @Override
    public String toString() {
        return "LinkPreviewModel{" +
                "mId='" + mId + '\'' +
                ", mLogo='" + mLogo + '\'' +
                ", mImage='" + mImage + '\'' +
                ", mTitle='" + mTitle + '\'' +
                ", mLink='" + mLink + '\'' +
                ", mDescription='" + mDescription + '\'' +
                ", mType='" + mType + '\'' +
                ", mShouldPreview=" + mShouldPreview +
                ", mFileName='" + mFileName + '\'' +
                ", mFileExtension='" + mFileExtension + '\'' +
                ", mWidth=" + mWidth +
                ", mHeight=" + mHeight +
                ", mFileType='" + mFileType + '\'' +
                ", mIsLinkDownloaded=" + mIsLinkDownloaded +
                ", mResourceContent='" + mResourceContent + '\'' +
                '}';
    }
}
