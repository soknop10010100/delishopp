package com.proapp.sompom.model;

public class AutoStartPermissionData {

    private boolean mShowFirstRequest;
    private long mLastShownDate = -1;
    private boolean mIsCompleted;
    private boolean mIsLastResponseIsProceed;

    public boolean isShowFirstRequest() {
        return mShowFirstRequest;
    }

    public void setShowFirstRequest(boolean showFirstRequest) {
        mShowFirstRequest = showFirstRequest;
    }

    public long getLastShownDate() {
        return mLastShownDate;
    }

    public void setLastShownDate(long lastShownDate) {
        mLastShownDate = lastShownDate;
    }

    public boolean isCompleted() {
        return mIsCompleted;
    }

    public void setCompleted(boolean completed) {
        mIsCompleted = completed;
    }

    public boolean isLastResponseIsProceed() {
        return mIsLastResponseIsProceed;
    }

    public void setLastResponseIsProceed(boolean lastResponseIsProceed) {
        mIsLastResponseIsProceed = lastResponseIsProceed;
    }
}
