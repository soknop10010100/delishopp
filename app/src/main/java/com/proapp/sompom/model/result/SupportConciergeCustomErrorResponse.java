package com.proapp.sompom.model.result;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.SupportCustomErrorResponse2;

public class SupportConciergeCustomErrorResponse extends SupportCustomErrorResponse2 {

    @SerializedName("data")
    private ResponseData mResponseData;

    public ResponseDataType getDataType() {
        if (mResponseData != null) {
            return mResponseData.getDataType();
        }

        return null;
    }

    public ResponseData getData() {
        return mResponseData;
    }

    public static class ResponseData {

        @SerializedName("type")
        private String mType;

        public ResponseDataType getDataType() {
            return ResponseDataType.from(mType);
        }
    }

    public enum ResponseDataType {

        SHOP_CLOSED("shopClosed"),
        PAYMENT_IN_PROCESS("paymentInProgress"),
        UNKNOWN("unknown");

        private final String mType;

        ResponseDataType(String type) {
            mType = type;
        }

        public static ResponseDataType from(String value) {
            for (ResponseDataType deeplinkType : ResponseDataType.values()) {
                if (TextUtils.equals(value, deeplinkType.mType)) {
                    return deeplinkType;
                }
            }
            return UNKNOWN;
        }

        public String getType() {
            return mType;
        }
    }
}
