package com.proapp.sompom.model.concierge;

import android.text.TextUtils;

/**
 * Created by Veasna Chhom on 4/12/22.
 */
public enum ConciergeItemOutOfStockAlternativeOption {

    CANCEL_ITEM("Cancel Item"),
    BE_CONTACTED("Be Contacted"), //Change item
    NONE("None");

    private String mValue;

    ConciergeItemOutOfStockAlternativeOption(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public static ConciergeItemOutOfStockAlternativeOption fromValue(String value) {
        for (ConciergeItemOutOfStockAlternativeOption type : ConciergeItemOutOfStockAlternativeOption.values()) {
            if (TextUtils.equals(type.mValue, value)) {
                return type;
            }
        }

        return NONE;
    }
}
