package com.proapp.sompom.model.notification;

import android.content.Context;
import android.text.Spannable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.R;
import com.proapp.sompom.model.concierge.ConciergeShop;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Notification implements RealmModel, NotificationAdaptive {

    @PrimaryKey
    @SerializedName(NotificationListAndRedirectionHelper.FIELD_ID)
    private String mId;
    @SerializedName(NotificationListAndRedirectionHelper.FIELD_VERB)
    private String mVerb;
    @SerializedName(NotificationListAndRedirectionHelper.FIELD_DATE)
    private Date mDate;
    @SerializedName(NotificationListAndRedirectionHelper.FIELD_CREATED_AT)
    private Date mCreatedAt;
    @SerializedName(NotificationListAndRedirectionHelper.FIELD_ACTOR)
    private RealmList<NotificationActor> mActors;
    @SerializedName(NotificationListAndRedirectionHelper.FIELD_OBJECT)
    private RealmList<NotificationObject> mObjects;
    @SerializedName(NotificationListAndRedirectionHelper.FIELD_IS_NEW)
    private boolean mIsNew;
    @SerializedName(NotificationListAndRedirectionHelper.FIELD_SHOP)
    private RealmList<ConciergeShop> mShops;

    public Notification() {
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        mCreatedAt = createdAt;
    }

    @Override
    public RealmList<NotificationActor> getActors() {
        return mActors;
    }

    public void setActors(RealmList<NotificationActor> actors) {
        mActors = actors;
    }

    @Override
    public RealmList<NotificationObject> getObjects() {
        return mObjects;
    }

    public void setObjects(RealmList<NotificationObject> objects) {
        mObjects = objects;
    }

    @Override
    public String getVerb() {
        return mVerb;
    }

    @Override
    public Date getPublished() {
        return mDate;
    }

    @Override
    public RealmList<ConciergeShop> getShops() {
        return mShops;
    }

    public void setShops(RealmList<ConciergeShop> shops) {
        mShops = shops;
    }

    @Override
    public boolean isNewNotification() {
        return mIsNew;
    }

    @Override
    public Spannable getNotificationText(Context context) {
        String notificationActionText = getNotificationActionText(context);
        if (TextUtils.isEmpty(notificationActionText)) {
            notificationActionText = context.getString(R.string.notification_default_text).trim();
        }
        String subjectText;
        if (!getShops().isEmpty()) {
            subjectText = getShops().get(0).getName();
        } else {
            subjectText = getNotificationSubjectText(context);
        }
        return NotificationListAndRedirectionHelper.getNotificationText(context,
                this,
                subjectText,
                notificationActionText);
    }

    @Override
    public RedirectionNotificationData getNotificationExchangeData() {
        NotificationObject firstObject = getFirstObject();
        if (firstObject != null) {
            return new RedirectionNotificationData(getVerb(), firstObject);
        }

        return null;
    }

    @Override
    public String getNotificationSubjectText(Context context) {
        return NotificationListAndRedirectionHelper.getSubjectText(context, getActors());
    }

    @Override
    public String getNotificationActionText(Context context) {
        return NotificationListAndRedirectionHelper.getNotificationActionText(context, getVerb(), getFirstObject());
    }

    @Override
    public boolean isValidNotificationVerb(Context context) {
        return !TextUtils.isEmpty(NotificationListAndRedirectionHelper.getNotificationActionText(context,
                getVerb(),
                getFirstObject()));
    }

    @Override
    public boolean shouldMakeNotificationRedirection(Context context) {
        return getNotificationExchangeData() != null && isValidNotificationVerb(context);
    }
}