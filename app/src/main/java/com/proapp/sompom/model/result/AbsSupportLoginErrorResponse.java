package com.proapp.sompom.model.result;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class AbsSupportLoginErrorResponse {

    @SerializedName("statusCode")
    private int mStatusCode;

    @SerializedName("error")
    private String mError;

    @SerializedName("message")
    private String mMessage;

    public boolean isError() {
        return !TextUtils.isEmpty(mError);
    }

    public String getErrorMessage() {
        return mMessage;
    }

    public int getStatusCode() {
        return mStatusCode;
    }
}
