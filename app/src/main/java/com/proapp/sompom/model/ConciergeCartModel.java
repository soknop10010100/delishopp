package com.proapp.sompom.model;

import com.proapp.sompom.model.concierge.BasketTimeSlot;
import com.proapp.sompom.model.concierge.ConciergeCoupon;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeOnlinePaymentProvider;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 7/9/21.
 * Known as Basket saved in local
 */
public class ConciergeCartModel {

    // New implementation of only saving menu item with the shop data within it,
    private List<ConciergeMenuItem> mBasketItemList = new ArrayList<>();

    // For tracking the cart's total price
    private String mBasketId;
    private String mType;
    private float mTotalPrice;
    private float mTotalDiscountPrice;
    private float mCouponPrice;

    private BasketTimeSlot mBasketTimeSlot;
    private ConciergeUserAddress mDeliveryAddress;
    private String mPaymentMethod;
    private ConciergeOnlinePaymentProvider mPaymentProvider;
    private String mDeliveryInstruction;
    private boolean mIsDisabled;
    private double mDeliveryFee;
    private double mContainerFee;
    private String mReceiptUrl;
    private String mCouponId;
    private ConciergeCoupon mConciergeCoupon;
    private String mOOSOption;
    private boolean mIsAppliedCouponValid;
    private boolean mIsWalletApplied;

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }

    public String getCouponId() {
        return mCouponId;
    }

    public void setCouponId(String couponId) {
        mCouponId = couponId;
    }

    public void setWalletApplied(boolean walletApplied) {
        mIsWalletApplied = walletApplied;
    }

    public boolean isWalletApplied() {
        return mIsWalletApplied;
    }

    public ConciergeCoupon getConciergeCoupon() {
        return mConciergeCoupon;
    }

    public ConciergeUserAddress getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public String getOOSOption() {
        return mOOSOption;
    }

    public void setOOSOption(String OOSOption) {
        mOOSOption = OOSOption;
    }

    public void setDeliveryAddress(ConciergeUserAddress deliveryAddress) {
        mDeliveryAddress = deliveryAddress;
    }

    public BasketTimeSlot getBasketTimeSlot() {
        return mBasketTimeSlot;
    }

    public void setBasketTimeSlot(BasketTimeSlot basketTimeSlot) {
        mBasketTimeSlot = basketTimeSlot;
    }

    public String getPaymentMethod() {
        return mPaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        mPaymentMethod = paymentMethod;
    }

    public ConciergeOnlinePaymentProvider getPaymentProvider() {
        return mPaymentProvider;
    }

    public void setPaymentProvider(ConciergeOnlinePaymentProvider paymentProvider) {
        mPaymentProvider = paymentProvider;
    }

    public String getDeliveryInstruction() {
        return mDeliveryInstruction;
    }

    public void setDeliveryInstruction(String deliveryInstruction) {
        mDeliveryInstruction = deliveryInstruction;
    }

    public boolean isDisabled() {
        return mIsDisabled;
    }

    public void setDisabled(boolean disabled) {
        mIsDisabled = disabled;
    }

    public String getBasketId() {
        return mBasketId;
    }

    public void setBasketId(String basketId) {
        mBasketId = basketId;
    }

    public List<ConciergeMenuItem> getBasketItemList() {
        return mBasketItemList;
    }

    public void setBasketItemList(List<ConciergeMenuItem> basketListItem) {
        mBasketItemList = basketListItem;
    }

    public float getTotalPrice() {
        return mTotalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        mTotalPrice = totalPrice;
    }

    public float getTotalDiscountPrice() {
        if (mTotalDiscountPrice < 0) {
            return 0;
        }

        return mTotalDiscountPrice;
    }

    public void setTotalDiscountPrice(float totalDiscountPrice) {
        this.mTotalDiscountPrice = totalDiscountPrice;
    }

    public int getTotalCartItem() {
        int total = 0;

        for (ConciergeMenuItem item : getBasketItemList()) {
            total += item.getProductCount();
        }

        return total;
    }

    public void setDeliveryFee(double deliveryFee) {
        mDeliveryFee = deliveryFee;
    }

    public double getDeliveryFee() {
        return mDeliveryFee;
    }

    public double getContainerFee() {
        return mContainerFee;
    }

    public void setContainerFee(double containerFee) {
        mContainerFee = containerFee;
    }

    public String getReceiptUrl() {
        return mReceiptUrl;
    }

    public void setReceiptUrl(String receiptUrl) {
        this.mReceiptUrl = receiptUrl;
    }

    public void setConciergeCoupon(ConciergeCoupon conciergeCoupon) {
        mConciergeCoupon = conciergeCoupon;
    }

    public float calculateCouponPrice(float totalPrice) {
//        Timber.i("calculateCouponPrice: " + mConciergeCoupon + ", totalPrice: " + totalPrice);
        if (mConciergeCoupon == null) {
            mCouponPrice = 0;
        } else {
            if (mConciergeCoupon.getMinimumBasketPrice() > 0 && totalPrice < mConciergeCoupon.getMinimumBasketPrice()) {
                mCouponPrice = 0;
            } else {
                if (mConciergeCoupon.getDiscountFixedPrice() > 0) {
                    if (mConciergeCoupon.getDiscountFixedPrice() > mTotalPrice) {
                        mCouponPrice = mTotalPrice;
                    } else {
                        mCouponPrice = mConciergeCoupon.getDiscountFixedPrice();
                    }
                } else if (mConciergeCoupon.getDiscountPercentage() > 0) {
                    mCouponPrice = (mTotalPrice * mConciergeCoupon.getDiscountPercentage()) / 100;
                }

                if (mConciergeCoupon.getMaximumDiscountPrice() > 0 && mCouponPrice > mConciergeCoupon.getMaximumDiscountPrice()) {
                    mCouponPrice = mConciergeCoupon.getMaximumDiscountPrice();
                }
            }
        }

//        Timber.i("mCouponPrice: " + mCouponPrice);
        return mCouponPrice;
    }

    public boolean isAppliedCouponValueValid() {
//        return mCouponPrice > 0 && mCouponPrice <= mTotalPrice;
        return mCouponPrice > 0 && !isTotalPriceBelowCouponMinBasketPrice();
    }

    public void setIsAppliedCouponValid(boolean mIsAppliedCouponValid) {
        this.mIsAppliedCouponValid = mIsAppliedCouponValid;
    }

    public boolean isTotalPriceBelowCouponMinBasketPrice() {
        return mTotalPrice < mConciergeCoupon.getMinimumBasketPrice();
    }

    public void clearCouponPrice() {
        mCouponPrice = 0;
    }

    public float getCouponPrice() {
        return mCouponPrice;
    }

    public void setCouponPrice(float mCouponPrice) {
        this.mCouponPrice = mCouponPrice;
    }
}

