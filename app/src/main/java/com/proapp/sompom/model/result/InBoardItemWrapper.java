package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.InBoardItem;

import java.util.List;

/**
 * Created by Chhom Veasna on 5/28/2020.
 */
public class InBoardItemWrapper {

    @SerializedName("language")
    private String mLanguage;

    @SerializedName("pages")
    private List<InBoardItem> mInBoardItemList;

    public String getLanguage() {
        return mLanguage;
    }

    public void setLanguage(String language) {
        mLanguage = language;
    }

    public List<InBoardItem> getInBoardItemList() {
        return mInBoardItemList;
    }

    public void setInBoardItemList(List<InBoardItem> inBoardItemList) {
        mInBoardItemList = inBoardItemList;
    }
}
