package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Veasna Chhom on 5/18/22.
 */

public class CheckPhoneExistRequestBody {

    @SerializedName("phoneNumber")
    private String mPhoneNumber;

    @SerializedName("countryCode")
    private String mCountryCode;

    public CheckPhoneExistRequestBody(String countryCode, String phoneNumber) {
        mPhoneNumber = phoneNumber;
        mCountryCode = countryCode;
    }
}
