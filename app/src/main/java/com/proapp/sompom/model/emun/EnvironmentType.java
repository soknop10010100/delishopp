package com.proapp.sompom.model.emun;

public enum EnvironmentType {

    DEV("dev"),
    DEMO("demo"),
    LOCAL("local");

    private String mValue;

    EnvironmentType(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }
}
