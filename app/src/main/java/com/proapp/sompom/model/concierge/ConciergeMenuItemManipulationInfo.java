package com.proapp.sompom.model.concierge;

import com.proapp.sompom.helper.ConciergeCartHelper;

public class ConciergeMenuItemManipulationInfo {

    private ConciergeMenuItem mMenuItem;
    private boolean mIsUpdated;
    private ConciergeCartHelper.CartModificationType mCartModificationType;

    public ConciergeMenuItem getMenuItem() {
        return mMenuItem;
    }

    public void setMenuItem(ConciergeMenuItem menuItem) {
        mMenuItem = menuItem;
    }

    public boolean isUpdated() {
        return mIsUpdated;
    }

    public void setUpdated(boolean updated) {
        mIsUpdated = updated;
    }

    public ConciergeCartHelper.CartModificationType getCartModificationType() {
        return mCartModificationType;
    }

    public void setCartModificationType(ConciergeCartHelper.CartModificationType cartModificationType) {
        mCartModificationType = cartModificationType;
    }
}
