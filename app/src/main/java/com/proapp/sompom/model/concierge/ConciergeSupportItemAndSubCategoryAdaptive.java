package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;
import com.proapp.sompom.model.emun.ConciergeMenuItemType;

public class ConciergeSupportItemAndSubCategoryAdaptive extends AbsConciergeModel implements ConciergeShopDetailDisplayAdaptive {

    @SerializedName(FIELD_TYPE)
    protected String mType;

    public void setType(ConciergeMenuItemType type) {
        mType = type.getValue();
    }

    public ConciergeMenuItemType getType() {
        return ConciergeMenuItemType.from(mType);
    }
}
