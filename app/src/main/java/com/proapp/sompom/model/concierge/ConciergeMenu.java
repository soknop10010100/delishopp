package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.DifferentAdaptive;

import java.util.Date;
import java.util.List;

public class ConciergeMenu extends AbsConciergeModel implements Parcelable, DifferentAdaptive<ConciergeMenu> {

    public static final String FIELD_AVAILABLE_DATE = "availableDateTime";
    public static final String FIELD_MENU_SECTION = "menuSections";

    @SerializedName(FIELD_NAME)
    private String mName;

    @SerializedName(FIELD_DESCRIPTION)
    private String mDescription;

    @SerializedName(FIELD_AVAILABLE_DATE)
    private Date mAvailableDate;

    @SerializedName(FIELD_MENU_SECTION)
    private List<ConciergeMenuSection> mConciergeMenuSections;

    protected ConciergeMenu(Parcel in) {
        mId = in.readString();
        mName = in.readString();
        mDescription = in.readString();
        mConciergeMenuSections = in.createTypedArrayList(ConciergeMenuSection.CREATOR);
    }

    public static final Creator<ConciergeMenu> CREATOR = new Creator<ConciergeMenu>() {
        @Override
        public ConciergeMenu createFromParcel(Parcel in) {
            return new ConciergeMenu(in);
        }

        @Override
        public ConciergeMenu[] newArray(int size) {
            return new ConciergeMenu[size];
        }
    };

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Date getAvailableDate() {
        return mAvailableDate;
    }

    public void setAvailableDate(Date availableDate) {
        mAvailableDate = availableDate;
    }

    public List<ConciergeMenuSection> getConciergeMenuSections() {
        return mConciergeMenuSections;
    }

    public void setConciergeMenuSections(List<ConciergeMenuSection> conciergeMenuSections) {
        mConciergeMenuSections = conciergeMenuSections;
    }

    @Override
    public boolean areItemsTheSame(ConciergeMenu other) {
        return getId().matches(other.getId());
    }

    @Override
    public boolean areContentsTheSame(ConciergeMenu other) {
        return areItemsTheSame(other) &&
                TextUtils.equals(getName(), other.getName()) &&
                TextUtils.equals(getDescription(), other.getDescription()) &&
                getAvailableDate().equals(other.getAvailableDate()) &&
                areConciergeMenuSectionItemTheSame(other.getConciergeMenuSections());
    }

    private boolean areConciergeMenuSectionItemTheSame(List<ConciergeMenuSection> other) {
        if ((getConciergeMenuSections() == null && other == null) ||
                (getConciergeMenuSections().isEmpty() && other.isEmpty())) {
            return true;
        } else if (getConciergeMenuSections() != null && other != null
                && getConciergeMenuSections().size() == other.size()) {

            for (int index = 0; index < getConciergeMenuSections().size(); index++) {
                if (!getConciergeMenuSections().get(index).areContentsTheSame(other.get(index))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mName);
        dest.writeString(mDescription);
        dest.writeTypedList(mConciergeMenuSections);
    }
}
