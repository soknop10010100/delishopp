package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhom Veasna on 12/26/2019.
 */
public class AddPlayerIdBody {

    public static final String PLAYER_ID = "playerId";

    @SerializedName(PLAYER_ID)
    private String mPlayerId; //OneSignal PlayerId

    @SerializedName("pushyPlayerId")
    private String mPushyPlayerId;

    @SerializedName("device")
    private int mDeviceType = 1; //1 Android and Web, 2 iOS

    public void setOneSignalPlayerId(String playerId) {
        mPlayerId = playerId;
    }

    public void setPushyPlayerId(String pushyPlayerId) {
        mPushyPlayerId = pushyPlayerId;
    }

    public AddPlayerIdBody() {
    }

    public AddPlayerIdBody(String playerId) {
        mPlayerId = playerId;
    }
}
