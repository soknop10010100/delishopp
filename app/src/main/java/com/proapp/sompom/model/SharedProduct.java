package com.proapp.sompom.model;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.helper.IntentData;
import com.proapp.sompom.intent.newintent.TimelineDetailIntent;
import com.proapp.sompom.intent.newintent.TimelineDetailIntentResult;
import com.proapp.sompom.model.emun.PublishItem;
import com.proapp.sompom.model.emun.TimelineDetailRedirectionType;
import com.proapp.sompom.model.emun.ViewType;
import com.proapp.sompom.model.result.ContentStat;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.sompom.baseactivity.ResultCallback;

import java.util.Date;
import java.util.List;

/**
 * Created by nuonveyo on 7/17/18.
 */

public class SharedProduct implements WallStreetAdaptive, Adaptive, Parcelable {
    public static final Creator<SharedProduct> CREATOR = new Creator<SharedProduct>() {
        @Override
        public SharedProduct createFromParcel(Parcel source) {
            return new SharedProduct(source);
        }

        @Override
        public SharedProduct[] newArray(int size) {
            return new SharedProduct[size];
        }
    };
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_CREATE_DATE)
    private Date mCreateDate;
    @SerializedName(FIELD_PUBLISH)
    private int mPublish;
    @SerializedName(FIELD_LATITUDE)
    private Double mLatitude;
    @SerializedName(FIELD_ADDRESS)
    private String mAddress;
    @SerializedName(FIELD_LONGITUDE)
    private Double mLongitude;
    @SerializedName("userShared")
    private User mUser;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("product")
    private Product mProduct;
    @SerializedName(FIELD_CITY)
    private String mCity;
    @SerializedName(FIELD_SHARE_URL)
    private String mShareUrl;
    @SerializedName(FIELD_CONTENT_STAT)
    private ContentStat mContentStat;
    @SerializedName(FIELD_COUNTRY)
    private String mCountry;

    public SharedProduct() {
    }

    protected SharedProduct(Parcel in) {
        this.mId = in.readString();
        long tmpMCreateDate = in.readLong();
        this.mCreateDate = tmpMCreateDate == -1 ? null : new Date(tmpMCreateDate);
        this.mPublish = in.readInt();
        this.mAddress = in.readString();
        this.mLongitude = (Double) in.readValue(Double.class.getClassLoader());
        this.mLatitude = (Double) in.readValue(Double.class.getClassLoader());
        this.mUser = in.readParcelable(User.class.getClassLoader());
        this.mDescription = in.readString();
        this.mProduct = in.readParcelable(Product.class.getClassLoader());
        this.mCity = in.readString();
        this.mContentStat = in.readParcelable(ContentStat.class.getClassLoader());
    }

    @Override
    public String getCountry() {
        return mCountry;
    }

    @Override
    public void setCountry(String country) {
        mCountry = country;
    }

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public void setId(String id) {
        mId = id;
    }

    @Override
    public boolean shouldShowPlacePreview() {
        return false;
    }

    @Override
    public boolean shouldShowLinkPreview() {
        return false;
    }

    @Override
    public Date getCreateDate() {
        return mCreateDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        mCreateDate = createDate;
    }

    @Override
    public void setUserView(List<User> userView) {
        ((WallStreetAdaptive) this).setUserView(userView);
    }

    @Override
    public PublishItem getPublish() {
        return PublishItem.getItem(mPublish);
    }

    @Override
    public void setPublish(int publish) {
        mPublish = publish;
    }

    @Override
    public ContentStat getContentStat() {
        if (mContentStat == null) {
            mContentStat = new ContentStat();
        }
        return mContentStat;
    }

    @Override
    public void setContentStat(ContentStat contentStat) {
        mContentStat = contentStat;
    }

    @Override
    public double getLatitude() {
        if (mLatitude == null) {
            return 0;
        }
        return mLatitude;
    }

    @Override
    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    @Override
    public String getAddress() {
        return mAddress;
    }

    @Override
    public void setAddress(String address) {
        mAddress = address;
    }

    @Override
    public String getCity() {
        return mCity;
    }

    @Override
    public void setCity(String city) {
        mCity = city;
    }

    @Override
    public double getLongitude() {
        if (mLongitude == null) {
            return 0;
        }
        return mLongitude;
    }

    @Override
    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    @Override
    public String getShareUrl() {
        return mShareUrl;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    @Override
    public List<Media> getMedia() {
        return mProduct.getMedia();
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }

    @Override
    public ViewType getTimelineViewType() {
        return getProduct().getTimelineViewType();
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //there is nothing to start
        AbsBaseActivity activity = requiredIntentData.getActivity();
        TimelineDetailIntent intent = new TimelineDetailIntent(activity,
                this,
                requiredIntentData.getMediaClickPosition(),
                TimelineDetailRedirectionType.FROM_WALL);

        requiredIntentData.getActivity().startActivityForResult(intent, new ResultCallback() {
            @Override
            public void onActivityResultSuccess(int resultCode, Intent data) {
                TimelineDetailIntentResult intentResult = new TimelineDetailIntentResult(data);
                requiredIntentData.onDataResultCallBack((Adaptive) intentResult.getLifeStream(), intentResult.isRemoveItem());
            }
        });
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeLong(this.mCreateDate != null ? this.mCreateDate.getTime() : -1);
        dest.writeInt(this.mPublish);
        dest.writeValue(this.mLatitude);
        dest.writeString(this.mAddress);
        dest.writeValue(this.mLongitude);
        dest.writeParcelable(this.mUser, flags);
        dest.writeString(this.mDescription);
        dest.writeParcelable(this.mProduct, flags);
        dest.writeString(this.mCity);
        dest.writeParcelable(this.mContentStat, flags);
    }
}
