package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.result.LinkPreviewModel;

import java.util.List;

/**
 * Created by nuonveyo
 * on 3/11/19.
 */
public class CommentBody {

    @SerializedName("message")
    private String mMessage;
    @SerializedName(LifeStream.META_PREVIEW)
    List<LinkPreviewModel> mMetaPreview;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public List<LinkPreviewModel> getMetaPreview() {
        return mMetaPreview;
    }

    public void setMetaPreview(List<LinkPreviewModel> metaPreview) {
        mMetaPreview = metaPreview;
    }
}
