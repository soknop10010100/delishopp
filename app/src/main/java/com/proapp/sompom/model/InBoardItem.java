package com.proapp.sompom.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhom Veasna on 5/28/2020.
 */
public class InBoardItem implements Parcelable {

    @SerializedName("title")
    private String mTitle;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("image")
    private String mImage;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mTitle);
        dest.writeString(this.mDescription);
        dest.writeString(this.mImage);
    }

    public InBoardItem() {
    }

    protected InBoardItem(Parcel in) {
        this.mTitle = in.readString();
        this.mDescription = in.readString();
        this.mImage = in.readString();
    }

    public static final Creator<InBoardItem> CREATOR = new Creator<InBoardItem>() {
        @Override
        public InBoardItem createFromParcel(Parcel source) {
            return new InBoardItem(source);
        }

        @Override
        public InBoardItem[] newArray(int size) {
            return new InBoardItem[size];
        }
    };
}
