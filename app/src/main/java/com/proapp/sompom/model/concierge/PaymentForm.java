package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PaymentForm implements Parcelable {

    @SerializedName("checkoutUrl")
    private String mCheckoutUrl;

    @SerializedName("successUrl")
    private String mSuccessUrl;

    @SerializedName("qrString")
    private String mQRString;

    @SerializedName("abapayDeeplink")
    private String mABAPayDeepLink;

    @SerializedName("playStore")
    private String mPlayStore;

    protected PaymentForm(Parcel in) {
        mCheckoutUrl = in.readString();
        mSuccessUrl = in.readString();
        mQRString = in.readString();
        mABAPayDeepLink = in.readString();
        mPlayStore = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mCheckoutUrl);
        dest.writeString(mSuccessUrl);
        dest.writeString(mQRString);
        dest.writeString(mABAPayDeepLink);
        dest.writeString(mPlayStore);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PaymentForm> CREATOR = new Creator<PaymentForm>() {
        @Override
        public PaymentForm createFromParcel(Parcel in) {
            return new PaymentForm(in);
        }

        @Override
        public PaymentForm[] newArray(int size) {
            return new PaymentForm[size];
        }
    };

    public String getCheckoutUrl() {
        return mCheckoutUrl;
    }

    public void setCheckoutUrl(String checkoutUrl) {
        mCheckoutUrl = checkoutUrl;
    }

    public String getQRString() {
        return mQRString;
    }

    public void setQRString(String QRString) {
        mQRString = QRString;
    }

    public String getABAPayDeepLink() {
        return mABAPayDeepLink;
    }

    public void setABAPayDeepLink(String ABAPayDeepLink) {
        mABAPayDeepLink = ABAPayDeepLink;
    }

    public String getPlayStore() {
        return mPlayStore;
    }

    public void setPlayStore(String playStore) {
        mPlayStore = playStore;
    }

    public String getSuccessUrl() {
        return mSuccessUrl;
    }

    public void setSuccessUrl(String successUrl) {
        mSuccessUrl = successUrl;
    }
}
