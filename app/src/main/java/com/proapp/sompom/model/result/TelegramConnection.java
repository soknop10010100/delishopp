package com.proapp.sompom.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class TelegramConnection  implements Parcelable, RealmModel {
    private static final String FIELD_ID = "_id";
    private static final String FIELD_USER_ID = "userId";
    private static final String FIELD_TELEGRAM_CHAT_ID = "telegramChatId";


    @SerializedName(FIELD_ID)
    @PrimaryKey
    private String mId;
    @SerializedName(FIELD_USER_ID)
    private String mUserId;
    @Ignore
    @SerializedName(FIELD_TELEGRAM_CHAT_ID)
    private String mTelegramChatId;

    public TelegramConnection() {
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        this.mUserId = userId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mUserId);
        dest.writeString(this.mTelegramChatId);
    }

    protected TelegramConnection(Parcel in) {
        mId = in.readString();
        mUserId = in.readString();
        mTelegramChatId = in.readString();
    }

    public static final Creator<TelegramConnection> CREATOR = new Creator<TelegramConnection>() {
        @Override
        public TelegramConnection createFromParcel(Parcel in) {
            return new TelegramConnection(in);
        }

        @Override
        public TelegramConnection[] newArray(int size) {
            return new TelegramConnection[size];
        }
    };
}
