package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;

public class ConciergeVerifyPaymentWithABAResponse {

    @SerializedName("isPaid")
    private boolean mIsPaid;

    public boolean isPaid() {
        return mIsPaid;
    }
}
