package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ChhomVeasna on 10/31/17.
 */

public class ForgotPasswordRequestBody {

    @SerializedName("email")
    private String mEmail;

    @SerializedName("url")
    private String mUrl;

    public ForgotPasswordRequestBody(String email, String url) {
        mEmail = email;
        mUrl = url;
    }
}
