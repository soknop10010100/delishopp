package com.proapp.sompom.model.emun;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

import com.proapp.sompom.R;

/**
 * Created by nuonveyo on 10/18/18.
 */

public enum PublishItem {
    Follower(1, R.drawable.ic_cooperate, R.string.privacy_corporate_title, R.string.privacy_corporate_description),
    EveryOne(2, R.drawable.ic_globe, R.string.privacy_public_title, R.string.privacy_public_description),
    OnlyMe(3, R.drawable.ic_globe, R.string.privacy_only_me_title, R.string.privacy_only_me_description);

    private int mId;
    @DrawableRes
    private int mIcon;
    @StringRes
    private int mTitleResource;
    @StringRes
    private int mDescriptionResource;

    PublishItem(int id, int icon, int titleResource, int descriptionResource) {
        mId = id;
        mIcon = icon;
        mTitleResource = titleResource;
        mDescriptionResource = descriptionResource;
    }

    public static PublishItem getItem(int id) {
        for (PublishItem publishItem : PublishItem.values()) {
            if (id == publishItem.getId()) {
                return publishItem;
            }
        }
        return Follower;
    }

    public int getDescriptionResource() {
        return mDescriptionResource;
    }

    public int getTitleResource() {
        return mTitleResource;
    }

    public int getId() {
        return mId;
    }

    public int getIcon() {
        return mIcon;
    }
}
