package com.proapp.sompom.model;

import com.proapp.sompom.model.emun.GroupMediaDetailType;

/**
 * Created by Chhom Veasna on 7/24/2020.
 */
public interface GroupMediaAdaptive {

    default public GroupMediaDetailType getMediaDetailType() {
        return GroupMediaDetailType.UNKNOWN;
    }
}
