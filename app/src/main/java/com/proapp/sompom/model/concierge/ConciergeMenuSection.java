package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;
import com.proapp.sompom.model.DifferentAdaptive;

import java.util.List;

public class ConciergeMenuSection extends AbsConciergeModel implements Parcelable,
        ConciergeShopDetailDisplayAdaptive,
        DifferentAdaptive<ConciergeMenuSection> {

    public static final String FIELD_MENU_ITEM = "menuItems";
    public static final String FIELD_POSITION = "position";

    @SerializedName(FIELD_NAME)
    private String mName;

    @SerializedName(FIELD_POSITION)
    private int mPosition;

    @SerializedName(FIELD_MENU_ITEM)
    private List<ConciergeMenuItem> mConciergeMenuItems;

    protected ConciergeMenuSection(Parcel in) {
        mId = in.readString();
        mName = in.readString();
        mPosition = in.readInt();
        mConciergeMenuItems = in.createTypedArrayList(ConciergeMenuItem.CREATOR);
    }

    public static final Creator<ConciergeMenuSection> CREATOR = new Creator<ConciergeMenuSection>() {
        @Override
        public ConciergeMenuSection createFromParcel(Parcel in) {
            return new ConciergeMenuSection(in);
        }

        @Override
        public ConciergeMenuSection[] newArray(int size) {
            return new ConciergeMenuSection[size];
        }
    };

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    public List<ConciergeMenuItem> getConciergeMenuItems() {
        return mConciergeMenuItems;
    }

    public void setConciergeMenuItems(List<ConciergeMenuItem> conciergeMenuItems) {
        mConciergeMenuItems = conciergeMenuItems;
    }

    @Override
    public boolean isProductCategory() {
        return true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mName);
        dest.writeInt(mPosition);
        dest.writeTypedList(mConciergeMenuItems);
    }

    @Override
    public boolean areItemsTheSame(ConciergeMenuSection other) {
        return getId().matches(other.getId());
    }

    @Override
    public boolean areContentsTheSame(ConciergeMenuSection other) {
        return areItemsTheSame(other) &&
                TextUtils.equals(getName(), other.getName()) &&
                getPosition() == other.getPosition() &&
                areConciergeMenuItemTheSame(other.getConciergeMenuItems());
    }

    private boolean areConciergeMenuItemTheSame(List<ConciergeMenuItem> other) {
        if ((getConciergeMenuItems() == null && other == null) ||
                (getConciergeMenuItems().isEmpty() && other.isEmpty())) {
            return true;
        } else if (getConciergeMenuItems() != null && other != null
                && getConciergeMenuItems().size() == other.size()) {

            for (int index = 0; index < getConciergeMenuItems().size(); index++) {
                if (!getConciergeMenuItems().get(index).areContentsTheSame(other.get(index))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
