package com.proapp.sompom.model.call;

import android.view.SurfaceView;
import android.view.ViewGroup;

import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.result.User;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 6/11/2020.
 */
public class CallData {

    private User mUser;
    private SurfaceView mVideoCallView;
    private boolean mIsCaller;
    private UserGroup mUserGroup;
    private CallAction mCallAction = CallAction.INITIALIZE_ViEW;
    private boolean mIsVideoCall;
    private boolean mHasVideo;
    private boolean mIsReceivingCameraRequest;
    private boolean mIsMuteAudio;

    public CallData(User user, SurfaceView videoCallView) {
        mUser = user;
        mVideoCallView = videoCallView;
    }

    public boolean isReceivingCameraRequest() {
        return mIsReceivingCameraRequest;
    }

    public void setReceivingCameraRequest(boolean receivingCameraRequest) {
        mIsReceivingCameraRequest = receivingCameraRequest;
    }

    public boolean isHasVideo() {
        return mHasVideo;
    }

    public boolean isMuteAudio() {
        return mIsMuteAudio;
    }

    public void setMuteAudio(boolean muteAudio) {
        mIsMuteAudio = muteAudio;
    }

    public void setHasVideo(boolean hasVideo) {
        mHasVideo = hasVideo;
    }

    public boolean isVideoCall() {
        return mIsVideoCall;
    }

    public void setVideoCall(boolean videoCall) {
        mIsVideoCall = videoCall;
    }

    public boolean isCaller() {
        return mIsCaller;
    }

    public void setCaller(boolean caller) {
        mIsCaller = caller;
    }

    public UserGroup getUserGroup() {
        return mUserGroup;
    }

    public void setUserGroup(UserGroup userGroup) {
        mUserGroup = userGroup;
    }

    public CallAction getCallAction() {
        return mCallAction;
    }

    public void setCallAction(CallAction callAction) {
        mCallAction = callAction;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public SurfaceView getVideoCallView() {
        return mVideoCallView;
    }

    public void setVideoCallView(SurfaceView videoCallView) {
        mVideoCallView = videoCallView;
    }

    public void cloneCallStates(CallData other) {
        /*
        Clone all call states from other except user and group data.
         */
        setCaller(other.isCaller());
        setCallAction(other.getCallAction());
        //Copy video state only if there is no video enabled before.
//        if (getVideoCallView() == null || !isVideoCall()) {
        setHasVideo(other.isHasVideo());
        setVideoCall(other.isVideoCall());
        setVideoCallView(other.getVideoCallView());
//        }
    }

    public void clearCallView() {
        if (mVideoCallView != null) {
            if (mVideoCallView.getParent() != null) {
                ((ViewGroup) mVideoCallView.getParent()).removeView(mVideoCallView);
            }
            mVideoCallView = null;
            Timber.i("Clear call view.");
        }
    }

    public enum CallAction {

        INITIALIZE_ViEW,
        ENGAGED_IN_CALL,
        WAITING_OTHER_TO_JOIN,
        UNKNOWN
    }
}
