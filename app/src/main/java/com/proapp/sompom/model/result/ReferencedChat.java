package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;

import java.util.Date;

import io.realm.RealmObject;

public class ReferencedChat extends RealmObject {

    @SerializedName(Chat.FIELD_ID)
    private String mId;

    @SerializedName(Chat.FIELD_CONTENT)
    private String mContent;

    @SerializedName(Chat.FIELD_DATE)
    private Date mDate;

    @SerializedName(Chat.FIELD_SENDER)
    private User mSender;

    @SerializedName(Chat.FIELD_SENDER_ID)
    private String mSenderId;

    @SerializedName(Chat.FIELD_MEDIA)
    private Media mMedia;

    public ReferencedChat() {
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public User getSender() {
        return mSender;
    }

    public void setSender(User sender) {
        mSender = sender;
    }

    public String getSenderId() {
        return mSenderId;
    }

    public void setSenderId(String senderId) {
        mSenderId = senderId;
    }

    public Media getMedia() {
        return mMedia;
    }

    public void setMedia(Media media) {
        mMedia = media;
    }

    public Chat.Type getType() {
        if (mMedia == null) {
            return Chat.Type.TEXT;
        } else {
            Media media = mMedia;
            if (media.getType() == MediaType.IMAGE) {
                return Chat.Type.IMAGE;
            } else if (media.getType() == MediaType.VIDEO) {
                return Chat.Type.VIDEO;
            } else if (media.getType() == MediaType.TENOR_GIF) {
                return Chat.Type.TENOR_GIF;
            } else if (media.getType() == MediaType.AUDIO) {
                return Chat.Type.AUDIO;
            } else if (media.getType() == MediaType.FILE) {
                return Chat.Type.FILE;
            } else {
                return Chat.Type.TEXT;
            }
        }
    }
}
