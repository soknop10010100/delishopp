package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by herotha
 * on 2/20/19.
 */

public class LoadMoreWrapper<T> {

    public final static String NEXT = "next";
    public final static String LIST = "list";
    @SerializedName(NEXT)
    private String mNextPage;
    @SerializedName(LIST)
    private List<T> mData;

    public LoadMoreWrapper() {
    }

    public LoadMoreWrapper(String nextPage, List<T> data) {
        mNextPage = nextPage;
        mData = data;
    }

    public String getNextPage() {
        return mNextPage;
    }

    public void setNextPage(String nextPage) {
        mNextPage = nextPage;
    }

    public List<T> getData() {
        return mData;
    }

    public void setData(List<T> data) {
        mData = data;
    }
}
