package com.proapp.sompom.model.result;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.example.usermentionable.utils.RenderTextAsMentionable;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.clustering.ClusterItem;
import com.proapp.sompom.listener.ConversationDataAdaptive;
import com.proapp.sompom.listener.UserListAdaptive;
import com.proapp.sompom.model.DifferentAdaptive;
import com.proapp.sompom.model.Follow;
import com.proapp.sompom.model.FullJob;
import com.proapp.sompom.model.PlayerIdModel;
import com.proapp.sompom.model.Search;
import com.proapp.sompom.model.UserFeatureSetting;
import com.proapp.sompom.model.emun.AccountStatus;
import com.proapp.sompom.model.emun.StoreStatus;
import com.proapp.sompom.model.emun.UserType;
import com.proapp.sompom.model.notification.NotificationProfileDisplayAdaptive;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass

public class User implements Parcelable,
        ClusterItem,
        Search,
        RealmModel,
        UserListAdaptive,
        ConversationDataAdaptive,
        DifferentAdaptive<User>,
        NotificationProfileDisplayAdaptive {
    //CHECKSTYLE:OFF
    public static final String FIELD_ID = "_id";
    public static final String FIELD_FIRST_NAME = "firstName";
    public static final String FIELD_LAST_NAME = "lastName";
    public static final String FIELD_USER_PROFILE = "profileUrl";
    public static final String FIELD_USER_PROFILE_THUMBNAIL = "profileThumbnail";
    private static final String FIELD_PHONE = "phone";
    private static final String FIELD_COUNTRY_CODE = "countryCode";
    private static final String FIELD_IS_REGISTER_WITH_PHONE = "hasPhone";
    private static final String FIELD_IS_REGISTER_WITH_FB = "hasSocial";
    private static final String FIELD_SHARES = "shares";
    private static final String FIELD_COMMENTS = "comments";
    private static final String FIELD_USER_COVER_PROFILE = "coverUrl";
    private static final String FIELD_USER_COVER_THUMBNAIL = "coverThumbnailUrl";
    private static final String FIELD_LIKES = "likes";
    private static final String FIELD_ADDRESS = "address";
    private static final String FIELD_EMAIL = "email";
    private static final String FIELD_SOCIAL_ID = "socialId";
    private static final String FIELD_SELL = "countSell";
    private static final String FIELD_BUY = "countBuy";
    private static final String FIELD_STATUS = "status";
    private static final String FIELD_ACCESS_TOKEN = "accessToken";
    private static final String FIELD_COUNTRY = "country";
    private static final String FIELD_FOLLOW = "follow";
    public static final String FIELD_NOTIFICATION_SETTING = "notificationSettings";
    public static final String FIELD_STORE_NAME = "storeName";
    private static final String FIELD_CITY = "city";
    private static final String FIELD_CURRENCY = "currency";
    private static final String FIELD_LATITUDE = "latitude";
    private static final String FIELD_LONGITUDE = "longitude";
    public static final String FIELD_IS_ONLINE = "isOnline";
    public static final String FIELD_DATE_LAST_ACTIVITY = "lastActiveDate";
    private static final String FIELD_ACCOUNT_STATUS = "accountStatus";
    private static final String FIELD_CONTENT_STAT = "contentStat";
    private static final String FIELD_IS_FOLLOW = "isFollow";
    private static final String PLAYER_ID = "playerId";
    public static final String LAST_POST_PRIVACY = "lastPostPrivacy";
    private static final String FIELD_THEME_ID = "themeId";
    private static final String PASSWORD_SETUP = "passwordSetup";
    private static final String LANGUAGE = "language";
    private static final String FEATURE_SETTING = "featureSetting";
    private static final String USER_TYPE = "proType";
    private static final String FIELD_HASH = "hash";
    public static final String FIELD_JOB = "job";
    public static final String FIELD_IS_SUPPORT_STAFF = "isSupportStaff";

    @PrimaryKey
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_USER_PROFILE_THUMBNAIL)
    private String mUserProfile;
    @SerializedName(FIELD_USER_PROFILE)
    private String mOriginalUserProfile;
    @SerializedName(FIELD_LAST_NAME)
    private String mLastName;
    @SerializedName(FIELD_FIRST_NAME)
    private String mFirstName;
    @Ignore
    @SerializedName(FIELD_LIKES)
    private Integer mLike;
    @Ignore
    @SerializedName(FIELD_BUY)
    private Integer mBuyCount;
    @Ignore
    @SerializedName(FIELD_PHONE)
    private String mPhone;
    @Ignore
    @SerializedName(FIELD_IS_REGISTER_WITH_PHONE)
    private Boolean mIsRegisterWithPhone;
    @Ignore
    @SerializedName(FIELD_IS_REGISTER_WITH_FB)
    private Boolean mIsRegisterWithFb;
    @Ignore
    @SerializedName(FIELD_SHARES)
    private Integer mShare;
    @Ignore
    @SerializedName(FIELD_COMMENTS)
    private Integer mComment;
    @Ignore
    @SerializedName(FIELD_SELL)
    private Integer mSellCount;
    @Ignore
    @SerializedName(FIELD_USER_COVER_PROFILE)
    private String mUserCoverProfile;
    @Ignore
    @SerializedName(FIELD_USER_COVER_THUMBNAIL)
    private String mUserCoverProfileThumbnail;
    @Ignore
    @SerializedName(FIELD_ADDRESS)
    private String mAddress;
    @Ignore
    @SerializedName(FIELD_EMAIL)
    private String mEmail;
    @Ignore
    @SerializedName(FIELD_SOCIAL_ID)
    private String mSocialId;
    @Ignore
    @SerializedName(FIELD_STATUS)
    private Integer mStatus;
    @Ignore
    @SerializedName(FIELD_ACCESS_TOKEN)
    private String mAccessToken;
    @Ignore
    @SerializedName(FIELD_COUNTRY)
    private String mCountry;
    @Ignore
    @SerializedName(FIELD_NOTIFICATION_SETTING)
    private JsonElement mNotificationSettingModel;
    @Ignore
    @SerializedName(FIELD_LATITUDE)
    private Double mLatitude;
    @Ignore
    @SerializedName(FIELD_LONGITUDE)
    private Double mLongitude;
    @Ignore
    @SerializedName(FIELD_FOLLOW)
    private Follow mFollow;
    @Ignore
    @SerializedName(FIELD_CURRENCY)
    private String mCurrency;
    @Ignore
    @SerializedName(FIELD_STORE_NAME)
    private String mStoreName;
    @Ignore
    @SerializedName(FIELD_CITY)
    private String mCity;
    @Ignore
    private LatLng mLatLng;
    @Ignore
    @SerializedName(FIELD_IS_ONLINE)
    private Boolean mIsOnline;
    @SerializedName(FIELD_DATE_LAST_ACTIVITY)
    private Date mLastActivity;
    @Ignore
    @SerializedName(FIELD_ACCOUNT_STATUS)
    private Integer mAccountStatus;
    @Ignore
    @SerializedName(FIELD_CONTENT_STAT)
    private ContentStat mContentStat;
    @Ignore
    @SerializedName(FIELD_IS_FOLLOW)
    private Boolean mIsFollow;
    @SerializedName("oldSell")
    private Integer mOldSell;
    @SerializedName("storeItems")
    private Integer mStoreItems;
    @Ignore
    private Boolean mIsSeenMessage = true;
    @Ignore
    private Boolean mIsAuthorizedUser;
    @Ignore
    @SerializedName(LAST_POST_PRIVACY)
    private Integer mLastPostPrivacy;
    @Ignore
    @SerializedName(PLAYER_ID)
    private List<PlayerIdModel> mPlayerIds;
    @Ignore
    private Boolean mIsSelected;
    @Ignore
    @SerializedName(FIELD_THEME_ID)
    private Integer mThemeId;
    @Ignore
    @SerializedName(PASSWORD_SETUP)
    private Boolean mPasswordSetup;
    @Ignore
    @SerializedName(LANGUAGE)
    private String mLanguage;
    @Ignore
    @SerializedName(FEATURE_SETTING)
    private UserFeatureSetting mUserFeatureSetting;
    @Ignore
    @SerializedName(FIELD_COUNTRY_CODE)
    private String mCountryCode;
    @Ignore
    @SerializedName(USER_TYPE)
    private String mUserType;
    @SerializedName(FIELD_HASH)
    private Integer mHash;
    @Ignore
    private FullJob mFullJob;
    @Ignore
    private boolean mIsGuestUser;
    @SerializedName(FIELD_IS_SUPPORT_STAFF)
    @Ignore
    private boolean mIsSupportStaff;

    @SerializedName("message")
    @Ignore
    //This field will be used to check if the response from server contain error
    private String mErrorMessage;

    public User() {
    }

    public static User getCommonUserInstance(User user) {
        User newUser = new User();
        newUser.setId(user.getId());
        newUser.setFirstName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setUserProfileThumbnail(user.getUserProfileThumbnail());
        newUser.setOnline(user.isOnline());
        newUser.setSeenMessage(null);
        newUser.setHash(user.getHash());
        newUser.setLastActivity(user.getLastActivity());
        newUser.setFullJob(user.getFullJob());

        return newUser;
    }

    public void copyCommonUserProperties(User user) {
        setId(user.getId());
        setFirstName(user.getFirstName());
        setLastName(user.getLastName());
        setUserProfileThumbnail(user.getUserProfileThumbnail());
        setOnline(user.isOnline());
        setSeenMessage(null);
        setHash(user.getHash());
        setLastActivity(user.getLastActivity());
        setFullJob(user.getFullJob());
    }

    public boolean isResponseError() {
        return !TextUtils.isEmpty(mErrorMessage);
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }

    public FullJob getFullJob() {
        return mFullJob;
    }

    public void setFullJob(FullJob fullJob) {
        mFullJob = fullJob;
    }

    public UserFeatureSetting getUserFeatureSetting() {
        return mUserFeatureSetting;
    }

    public void setUserFeatureSetting(UserFeatureSetting userFeatureSetting) {
        mUserFeatureSetting = userFeatureSetting;
    }

    public Integer getThemeId() {
        return mThemeId;
    }

    public void setThemeId(Integer themeId) {
//        mThemeId = themeId;
    }

    public boolean getSelected() {
        return mIsSelected != null && mIsSelected;
    }

    public void setSelected(Boolean selected) {
        mIsSelected = selected;
    }

    public List<PlayerIdModel> getPlayerIds() {
        return mPlayerIds;
    }

    public boolean getPasswordSetup() {
        return mPasswordSetup != null && mPasswordSetup;
    }

    public void setPlayerIds(List<PlayerIdModel> playerIds) {
        mPlayerIds = playerIds;
    }

    public String getLanguage() {
        return mLanguage;
    }

    public void setLanguage(String language) {
        mLanguage = language;
    }

    public UserType getUserType() {
        return UserType.fromValue(mUserType);
    }

    public void setUserType(UserType userType) {
        mUserType = userType.getValue();
    }

    public int getHash() {
        if (mHash != null) {
            return mHash;
        }

        return 0;
    }

    public void setHash(int hash) {
        mHash = hash;
    }

    public Integer getOldSell() {
        if (mOldSell == null) {
            return 0;
        }
        return mOldSell;
    }

    public void setOldSell(Integer oldSell) {
        mOldSell = oldSell;
    }

    public Integer getStoreItems() {
        if (mStoreItems == null) {
            return 0;
        }
        return mStoreItems;
    }

    public Boolean getIsAuthorizedUser() {
        return mIsAuthorizedUser;
    }

    public void setIsAuthorizedUser(Boolean isAuthorizedUser) {
        mIsAuthorizedUser = isAuthorizedUser;
    }

    public boolean isSeenMessage() {
        return mIsSeenMessage;
    }

    public void setSeenMessage(Boolean seenMessage) {
        mIsSeenMessage = seenMessage;
    }

    public void setStoreItems(Integer storeItems) {
        mStoreItems = storeItems;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getCountryCode() {
        return mCountryCode;
    }

    public void setCountryCode(String countryCode) {
        mCountryCode = countryCode;
    }

    public boolean isIsRegisterWithPhone() {
        if (mIsRegisterWithPhone == null) {
            return false;
        }
        return mIsRegisterWithPhone;
    }

    public void setIsRegisterWithPhone(boolean isRegisterWithPhone) {
        mIsRegisterWithPhone = isRegisterWithPhone;
    }

    public Integer getLastPostPrivacy() {
        if (mLastPostPrivacy != null) {
            return mLastPostPrivacy;
        }

        return -1;
    }

    public void setLastPostPrivacy(Integer lastPostPrivacy) {
        mLastPostPrivacy = lastPostPrivacy;
    }

    public boolean isIsRegisterWithFb() {
        if (mIsRegisterWithFb == null) {
            return false;
        }
        return mIsRegisterWithFb;
    }

    public void setIsRegisterWithFb(boolean isRegisterWithFb) {
        mIsRegisterWithFb = isRegisterWithFb;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double latitude) {
        this.mLatitude = latitude;
    }

    public Double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(Double longitude) {
        this.mLongitude = longitude;
    }

    public int getShare() {
        return mShare;
    }

    public void setShare(Integer share) {
        mShare = share;
    }

    public int getComment() {
        return mComment;
    }

    public void setComment(Integer comment) {
        mComment = comment;
    }

    public int getSellCount() {
        return mSellCount;
    }

    public void setSellCount(Integer sellCount) {
        mSellCount = sellCount;
    }

    public String getUserCoverProfile() {
        return mUserCoverProfile;
    }

    public void setUserCoverProfile(String userCoverProfile) {
        mUserCoverProfile = userCoverProfile;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getFirstName() {
        if (mFirstName != null) {
            return mFirstName.trim();
        }

        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public int getLike() {
        if (mLike == null) {
            return 0;
        }
        return mLike;
    }

    public void setLike(Integer like) {
        mLike = like;
    }

    public String getUserCoverProfileThumbnail() {
        return mUserCoverProfileThumbnail;
    }

    public void setUserCoverProfileThumbnail(String userCoverProfileThumbnail) {
        mUserCoverProfileThumbnail = userCoverProfileThumbnail;
    }

    public String getUserProfileThumbnail() {
        /*
        Because profile thumbnail just added after full profile url, so if the thumbnail does not exist
        we will still use the full profile url.
         */
        if (TextUtils.isEmpty(mUserProfile)) {
            return getOriginalUserProfile();
        }

        return mUserProfile;
    }

    public void setUserProfileThumbnail(String userProfile) {
        mUserProfile = userProfile;
    }

    public String getOriginalUserProfile() {
        return mOriginalUserProfile;
    }

    public void setOriginalUserProfile(String originalUserProfile) {
        mOriginalUserProfile = originalUserProfile;
    }

    public String getLastName() {
        if (mLastName != null) {
            return mLastName.trim();
        }

        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getSocialId() {
        return mSocialId;
    }

    public void setSocialId(String socialId) {
        mSocialId = socialId;
    }

    public int getBuyCount() {
        if (mBuyCount == null) {
            return 0;
        }
        return mBuyCount;
    }

    public void setBuyCount(Integer buyCount) {
        mBuyCount = buyCount;
    }

    public StoreStatus getStatus() {
        return StoreStatus.fromValue(mStatus);
    }

    public void setStatus(StoreStatus status) {
        mStatus = status.getUserStatus();
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public void setAccessToken(String accessToken) {
        mAccessToken = accessToken;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        this.mCurrency = currency;
    }

    public String getFullName() {
        String fullName = getFirstName();
        if (!TextUtils.isEmpty(fullName)) {
            if (!TextUtils.isEmpty(getLastName())) {
                fullName += " " + getLastName();
            }
        } else {
            fullName = getLastName();
        }

        if (TextUtils.isEmpty(fullName)) {
            return "";
        }

        return RenderTextAsMentionable.getValidDisplayName(fullName);
    }

    public String getCountryName() {
        if (mCountry != null) {
            Locale locale = new Locale("", mCountry);
            return locale.getDisplayCountry();
        } else {
            return "";
        }
    }

    public Follow getFollow() {
        if (mFollow == null) {
            mFollow = new Follow();
            mFollow.setFollow(false);
        }
        return mFollow;
    }

    public NotificationSettingModel getNotificationSettingModel() {
        if (mNotificationSettingModel != null && mNotificationSettingModel.isJsonObject()) {
            return new Gson().fromJson(mNotificationSettingModel.toString(), NotificationSettingModel.class);
        }

        return null;
    }

    public void setNotificationSettingModel(NotificationSettingModel notificationSettingModel) {
        Gson gson = new Gson();
        mNotificationSettingModel = gson.fromJson(gson.toJson(notificationSettingModel), JsonObject.class);
    }

    public boolean isGuestUser() {
        return mIsGuestUser;
    }

    public void setGuestUser(boolean guestUser) {
        mIsGuestUser = guestUser;
    }

    public String getStoreName() {
        return mStoreName;
    }

    public void setStoreName(String storeName) {
        mStoreName = storeName;
    }

    public Boolean isOnline() {
        return mIsOnline;
    }

    public void setOnline(Boolean online) {
        mIsOnline = online;
    }

    public Date getLastActivity() {
        return mLastActivity;
    }

    public void setLastActivity(Date lastActivity) {
        mLastActivity = lastActivity;
    }

    public AccountStatus getAccountStatus() {
        return AccountStatus.fromValue(mAccountStatus);
    }

    public void setAccountStatus(Integer accountStatus) {
        mAccountStatus = accountStatus;
    }

    public ContentStat getContentStat() {
        if (mContentStat == null) {
            mContentStat = new ContentStat();
        }
        return mContentStat;
    }

    public void setContentStat(ContentStat contentStat) {
        mContentStat = contentStat;
    }

    public boolean isFollow() {
        if (mIsFollow == null) {
            return false;
        }
        return mIsFollow;
    }

    public String getUserJob() {
        if (mFullJob != null) {
            return mFullJob.getLabel();
        }

        return "";
    }

    public void setFollow(Follow follow) {
        mFollow = follow;
    }

    public void setFollow(Boolean follow) {
        mIsFollow = follow;
    }

    public boolean isSupportStaff() {
        return mIsSupportStaff;
    }

    public void setIsSupportStaff(boolean isSupportStaff) {
        this.mIsSupportStaff = isSupportStaff;
    }

    @Override
    public LatLng getPosition() {
        if (mLatLng == null) {
            mLatLng = new LatLng(mLatitude, mLongitude);
        }
        return mLatLng;
    }

    @Override
    public String getDisplayProfileUrl() {
        return getUserProfileThumbnail();
    }

    @Override
    public String getDisplayFirstName() {
        return getFirstName();
    }

    @Override
    public String getDisplayLastName() {
        return getLastName();
    }

    @Override
    public String getTitle() {
        return getFullName();
    }

    @Override
    public String getSnippet() {
        return null;
    }

    @Override
    public String getDisplayFullName() {
        return getFullName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        if (TextUtils.isEmpty(getId())) {
            return false;
        }
        return Objects.equals(getId(), user.getId());
    }

    public User cloneForSeenStatus() {
        User user = new User();
        user.setId(getId());
        user.setFirstName(getFirstName());
        user.setLastName(getLastName());
        user.setUserProfileThumbnail(getUserProfileThumbnail());
        user.setHash(getHash());
        user.setLastActivity(getLastActivity());

        return user;
    }

    @Override
    public int hashCode() {
        if (TextUtils.isEmpty(getId())) {
            return Objects.hashCode(getFirstName());
        }
        return Objects.hash(getId());
    }

    private boolean isProfileUrlTheSame(User other) {
        if (TextUtils.isEmpty(getUserProfileThumbnail()) && TextUtils.isEmpty(other.getUserProfileThumbnail())) {
            return true;
        } else {
            return (!TextUtils.isEmpty(getUserProfileThumbnail()) &&
                    !TextUtils.isEmpty(other.getUserProfileThumbnail()) &&
                    getUserProfileThumbnail().matches(other.getUserProfileThumbnail()));
        }
    }

    @Override
    public boolean areItemsTheSame(User other) {
        return getId().matches(other.getId());
    }

    @Override
    public boolean areContentsTheSame(User other) {
        return getId().matches(other.getId()) &&
                (!TextUtils.isEmpty(getFirstName()) &&
                        !TextUtils.isEmpty(other.getFirstName()) &&
                        getFirstName().matches(other.getFirstName())) &&
                (!TextUtils.isEmpty(getLastName()) &&
                        !TextUtils.isEmpty(other.getLastName()) &&
                        getLastName().matches(other.getLastName())) &&
                isProfileUrlTheSame(other) &&
                !isOnlineStatusChanged(other) &&
                !isJobChanged(other);
    }

    private boolean isOnlineStatusChanged(User update) {
//        Timber.i("User: " + getFullName() + ", current online: " + isOnline() + ", update: " + update.isOnline());
        if (isOnline() != null && update.isOnline() != null && isOnline().compareTo(update.isOnline()) == 0) {
            return false;
        } else return isOnline() != null || update.isOnline() != null;
    }

    private boolean isJobChanged(User update) {
        return !TextUtils.equals(getUserJob(), update.getUserJob());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mUserProfile);
        dest.writeString(this.mLastName);
        dest.writeString(this.mFirstName);
        dest.writeValue(this.mLike);
        dest.writeValue(this.mBuyCount);
        dest.writeString(this.mPhone);
        dest.writeValue(this.mIsRegisterWithPhone);
        dest.writeValue(this.mIsRegisterWithFb);
        dest.writeValue(this.mShare);
        dest.writeValue(this.mComment);
        dest.writeValue(this.mSellCount);
        dest.writeString(this.mUserCoverProfile);
        dest.writeString(this.mUserCoverProfileThumbnail);
        dest.writeString(this.mAddress);
        dest.writeString(this.mEmail);
        dest.writeString(this.mSocialId);
        dest.writeValue(this.mStatus);
        dest.writeString(this.mAccessToken);
        dest.writeString(this.mCountry);
        dest.writeValue(this.mLatitude);
        dest.writeValue(this.mLongitude);
        dest.writeParcelable(this.mFollow, flags);
        dest.writeString(this.mCurrency);
        dest.writeString(this.mStoreName);
        dest.writeString(this.mCity);
        dest.writeParcelable(this.mLatLng, flags);
        dest.writeValue(this.mIsOnline);
        dest.writeLong(this.mLastActivity != null ? this.mLastActivity.getTime() : -1);
        dest.writeValue(this.mAccountStatus);
        dest.writeParcelable(this.mContentStat, flags);
        dest.writeValue(this.mIsFollow);
        dest.writeValue(this.mOldSell);
        dest.writeValue(this.mStoreItems);
        dest.writeValue(this.mLastPostPrivacy);
        dest.writeValue(this.mHash);
        dest.writeString(this.mOriginalUserProfile);
    }

    protected User(Parcel in) {
        this.mId = in.readString();
        this.mUserProfile = in.readString();
        this.mLastName = in.readString();
        this.mFirstName = in.readString();
        this.mLike = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mBuyCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mPhone = in.readString();
        this.mIsRegisterWithPhone = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mIsRegisterWithFb = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mShare = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mComment = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mSellCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mUserCoverProfile = in.readString();
        this.mUserCoverProfileThumbnail = in.readString();
        this.mAddress = in.readString();
        this.mEmail = in.readString();
        this.mSocialId = in.readString();
        this.mStatus = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mAccessToken = in.readString();
        this.mCountry = in.readString();
        this.mLatitude = (Double) in.readValue(Double.class.getClassLoader());
        this.mLongitude = (Double) in.readValue(Double.class.getClassLoader());
        this.mFollow = in.readParcelable(Follow.class.getClassLoader());
        this.mCurrency = in.readString();
        this.mStoreName = in.readString();
        this.mCity = in.readString();
        this.mLatLng = in.readParcelable(LatLng.class.getClassLoader());
        this.mIsOnline = (Boolean) in.readValue(Boolean.class.getClassLoader());
        long tmpMLastActivity = in.readLong();
        this.mLastActivity = tmpMLastActivity == -1 ? null : new Date(tmpMLastActivity);
        this.mAccountStatus = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mContentStat = in.readParcelable(ContentStat.class.getClassLoader());
        this.mIsFollow = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mOldSell = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mStoreItems = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mLastPostPrivacy = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mHash = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mOriginalUserProfile = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}