package com.proapp.sompom.model.emun;

public enum ChangePasswordActionType {

    CHANGE_PASSWORD(1),
    CHANGE_PASSWORD_WITHOUT_OLD_PASSWORD(2),
    RESET_PASSWORD(3),
    RESET_PASSWORD_VIA_PHONE(4);

    private int mValue;

    ChangePasswordActionType(int value) {
        mValue = value;
    }

    public int getValue() {
        return mValue;
    }

    public static ChangePasswordActionType newInstanceFromValue(int value) {
        for (ChangePasswordActionType changePasswordActionType : values()) {
            if (changePasswordActionType.getValue() == value) {
                return changePasswordActionType;
            }
        }

        return CHANGE_PASSWORD;
    }
}
