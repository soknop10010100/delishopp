package com.proapp.sompom.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by imac on 6/15/17.
 */

public class Locations implements Parcelable {
    public static final Parcelable.Creator<Locations> CREATOR = new Parcelable.Creator<Locations>() {
        @Override
        public Locations createFromParcel(Parcel source) {
            return new Locations(source);
        }

        @Override
        public Locations[] newArray(int size) {
            return new Locations[size];
        }
    };
    private double mLatitude;
    private double mLongitude;
    private String mCountry;

    public Locations() {
    }

    protected Locations(Parcel in) {
        this.mLatitude = in.readDouble();
        this.mLongitude = in.readDouble();
        this.mCountry = in.readString();
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        this.mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        this.mLongitude = longitude;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        this.mCountry = country;
    }

    public boolean isEmpty() {
        return mLatitude <= 0 || mLongitude <= 0;
    }

    public void reset() {
        mLatitude = 0;
        mLongitude = 0;
        mCountry = "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.mLatitude);
        dest.writeDouble(this.mLongitude);
        dest.writeString(this.mCountry);
    }

    @Override
    public String toString() {
        return "Locations{" +
                "mLatitude=" + mLatitude +
                ", mLongitude=" + mLongitude +
                ", mCountry='" + mCountry + '\'' +
                '}';
    }
}
