package com.proapp.sompom.model.result;

import java.net.ConnectException;

/**
 * Created by Chhom Veasna on 2/4/2020.
 */
public abstract class ResponseWrapper {

    private boolean mIsLocalData;
    private Throwable mError;

    public ResponseWrapper() {
    }

    public ResponseWrapper(boolean isLocalData, Throwable error) {
        mIsLocalData = isLocalData;
        mError = error;
    }

    public boolean isLocalData() {
        return mIsLocalData;
    }

    public void setLocalData(boolean localData) {
        mIsLocalData = localData;
    }

    public Throwable getError() {
        return mError;
    }

    public void setError(Throwable error) {
        mError = error;
    }

    public boolean isError() {
        return mError != null;
    }

    public boolean isNoConnectionError() {
        return mError instanceof ConnectException;
    }

}
