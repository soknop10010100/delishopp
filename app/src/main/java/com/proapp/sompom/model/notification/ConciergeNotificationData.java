package com.proapp.sompom.model.notification;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.result.ConciergeOrder;

public class ConciergeNotificationData {

    @SerializedName(AbsConciergeModel.FIELD_TYPE)
    private String mType;

    @SerializedName(AbsConciergeModel.FIELD_EXPRESS)
    private boolean mIsExpress;

    @SerializedName(AbsConciergeModel.FIELD_ORDER)
    private ConciergeOrder mOrder;

    public String getType() {
        return mType;
    }

    public boolean isExpress() {
        return mIsExpress;
    }

    public ConciergeOrder getOrder() {
        return mOrder;
    }
}
