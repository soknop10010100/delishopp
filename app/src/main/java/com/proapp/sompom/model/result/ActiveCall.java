package com.proapp.sompom.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhom Veasna on 7/9/2020.
 */
public class ActiveCall implements Parcelable {

    private static final String IS_CHANNEL_OPEN = "isChannelOpen";
    private static final String IS_VIDEO = "isVideo";

    @SerializedName(IS_CHANNEL_OPEN)
    private boolean mIsChannelOpen;
    @SerializedName(IS_VIDEO)
    private boolean mIsVideo;

    public boolean isChannelOpen() {
        return mIsChannelOpen;
    }

    public void setChannelOpen(boolean channelOpen) {
        mIsChannelOpen = channelOpen;
    }

    public boolean isVideo() {
        return mIsVideo;
    }

    public void setVideo(boolean video) {
        mIsVideo = video;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.mIsChannelOpen ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mIsVideo ? (byte) 1 : (byte) 0);
    }

    public ActiveCall() {
    }

    protected ActiveCall(Parcel in) {
        this.mIsChannelOpen = in.readByte() != 0;
        this.mIsVideo = in.readByte() != 0;
    }

    public static final Creator<ActiveCall> CREATOR = new Creator<ActiveCall>() {
        @Override
        public ActiveCall createFromParcel(Parcel source) {
            return new ActiveCall(source);
        }

        @Override
        public ActiveCall[] newArray(int size) {
            return new ActiveCall[size];
        }
    };
}
