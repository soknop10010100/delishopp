package com.proapp.sompom.model.emun;

import androidx.annotation.StringRes;

import com.proapp.sompom.R;

/**
 * Created by nuonveyo on 7/4/18.
 */

public enum NotificationItem {
    New(1, R.string.notification_title_new),
    Earlier(2, R.string.notification_title_earlier);

    private int mId;
    @StringRes
    private int mTitle;

    NotificationItem(int id, int title) {
        mId = id;
        mTitle = title;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getTitle() {
        return mTitle;
    }

    public void setTitle(int title) {
        mTitle = title;
    }
}
