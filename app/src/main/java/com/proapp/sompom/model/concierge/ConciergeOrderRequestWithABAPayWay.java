package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;

public class ConciergeOrderRequestWithABAPayWay {

    @SerializedName("basket")
    private ConciergeOrderRequest mBasket;

    @SerializedName("abaParam")
    private ABARequestOption mABARequestOption = new ABARequestOption();

    public ConciergeOrderRequest getBasket() {
        return mBasket;
    }

    public void setBasket(ConciergeOrderRequest basket) {
        mBasket = basket;
    }

    public ABARequestOption getABARequestOption() {
        return mABARequestOption;
    }

    public void setABARequestOption(ABARequestOption ABARequestOption) {
        mABARequestOption = ABARequestOption;
    }

    public ConciergeOrderRequestWithABAPayWay(ConciergeOrderRequest basket) {
        mBasket = basket;
    }

    public static class ABARequestOption {

        @SerializedName("viewType")
        private String mViewType = "hosted_view";
    }
}
