package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Or Vitovongsak on 8/6/20.
 */
public class DateList {

    @SerializedName("day")
    private String mDay;

    @SerializedName("weekday")
    private int mWeekday;

    @SerializedName("active")
    private boolean mIsActive;

    @SerializedName("timeList")
    private List<TimeList> mTimeList;

    public DateList() {

    }

    public String getDay() {
        return mDay;
    }

    public int getWeekday() {
        return mWeekday;
    }

    public boolean isActive() {
        return mIsActive;
    }

    public List<TimeList> getTimeList() {
        return mTimeList;
    }
}
