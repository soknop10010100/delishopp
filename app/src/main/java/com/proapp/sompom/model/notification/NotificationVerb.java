package com.proapp.sompom.model.notification;

/**
 * Created by Veasna Chhom on 5/07/21.
 */
public final class NotificationVerb {

    public static final String VERB_CREATE_GROUP = "createGroup";
    public static final String VERB_POST = "post";
    public static final String VERB_SHARE = "share";
    public static final String VERB_LIKE = "like";
    public static final String COMMENT_POST = "commentPost";
    public static final String COMMENT_IMAGE = "commentImage";
    public static final String COMMENT_VIDEO = "commentVideo";
    public static final String COMMENT_MEDIA = "commentMedia";
    public static final String REPLY_COMMENT_POST = "replyCommentPost";
    public static final String REPLY_SUB_COMMENT = "subComment";

    public static final String LIKE_POST = "likePost";
    public static final String LIKE_IMAGE = "likeImage";

    public static final String LIKE_VIDEO = "likeVideo";
    public static final String LIKE_MEDIA = "likeMedia";

    public static final String LIKE_COMMENT = "likeComment";
    public static final String LIKE_SUB_COMMENT = "likeSubComment";

    public static final String MENTION_POST = "mentionPost";
    public static final String MENTION_IN_COMMENT = "mentionComment";
    public static final String MENTION_SUB_COMMENT = "mentionSubComment";

    public static final String ORDER_PROCESSING = "orderProcessing";
    public static final String ORDER_DELIVERING = "orderDelivering";
}
