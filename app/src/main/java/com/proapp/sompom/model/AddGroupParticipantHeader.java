package com.proapp.sompom.model;

import com.proapp.sompom.listener.UserListAdaptive;
import com.proapp.sompom.newui.dialog.AddGroupParticipantDialog;

/**
 * Created by Chhom Veasna on 8/10/2020.
 */
public class AddGroupParticipantHeader implements UserListAdaptive {

    private String mTitle;
    private AddGroupParticipantDialog.AddingType mAddingType;

    public AddGroupParticipantHeader(String title, AddGroupParticipantDialog.AddingType addingType) {
        mTitle = title;
        mAddingType = addingType;
    }

    public String getTitle() {
        return mTitle;
    }

    public AddGroupParticipantDialog.AddingType getAddingType() {
        return mAddingType;
    }
}
