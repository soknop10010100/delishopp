package com.proapp.sompom.model;

/**
 * Created by Chhom Veasna on 11/6/2019.
 */
public interface LinkPreviewAdaptive {

    String getId();

    String getPreviewContent();
}
