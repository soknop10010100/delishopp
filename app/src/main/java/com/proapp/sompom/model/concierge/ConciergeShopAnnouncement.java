package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.ConciergeItemAdaptive;
import com.proapp.sompom.model.DifferentAdaptive;

/**
 * Created by Or Vitovongsak on 23/12/21.
 */
public class ConciergeShopAnnouncement extends AbsConciergeModel
        implements Parcelable,
        DifferentAdaptive<ConciergeShopAnnouncement>,
        ConciergeItemAdaptive {

    public static final String ANNOUNCEMENT_MESSAGE = "message";
    public static final String ANNOUNCEMENT_IS_ACTIVE = "isActive";

    @SerializedName(ANNOUNCEMENT_MESSAGE)
    private String mMessage;

    @SerializedName(ANNOUNCEMENT_IS_ACTIVE)
    private boolean mIsActive;

    protected ConciergeShopAnnouncement(Parcel in) {
        mMessage = in.readString();
        mIsActive = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mMessage);
        dest.writeByte((byte) (mIsActive ? 1 : 0));
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public boolean isActive() {
        return mIsActive;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConciergeShopAnnouncement> CREATOR = new Creator<ConciergeShopAnnouncement>() {
        @Override
        public ConciergeShopAnnouncement createFromParcel(Parcel in) {
            return new ConciergeShopAnnouncement(in);
        }

        @Override
        public ConciergeShopAnnouncement[] newArray(int size) {
            return new ConciergeShopAnnouncement[size];
        }
    };

    @Override
    public boolean areItemsTheSame(ConciergeShopAnnouncement other) {
        return true;
    }

    @Override
    public boolean areContentsTheSame(ConciergeShopAnnouncement other) {
        return TextUtils.equals(getMessage(), other.getMessage()) &&
                isActive() == other.isActive();
    }
}
