package com.proapp.sompom.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.result.User;

import java.util.List;

/**
 * Created by Or Vitovongsak on 5/4/20.
 */
public class GroupDetail implements Parcelable {

    @SerializedName("_id")
    private String mId;
    @SerializedName("name")
    private String mName;
    @SerializedName(UserGroup.GROUP_PICTURE)
    private String mGroupPhoto;
    @SerializedName("participants")
    private List<User> mParticipant;
    @SerializedName("count")
    private GroupMediaCount mGroupMediaCount;
    @SerializedName("owner")
    private List<User> mGroupOwnerList;
    @SerializedName("creator")
    private String mCreatorId;

    public GroupDetail() {
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getGroupPhoto() {
        return mGroupPhoto;
    }

    public void setGroupPhoto(String groupPhoto) {
        mGroupPhoto = groupPhoto;
    }

    public List<User> getParticipant() {
        return mParticipant;
    }

    public void setParticipant(List<User> participant) {
        mParticipant = participant;
    }

    public GroupMediaCount getGroupMediaCount() {
        return mGroupMediaCount;
    }

    public List<User> getGroupOwnerList() {
        return mGroupOwnerList;
    }

    public void setGroupOwnerList(List<User> groupOwnerList) {
        mGroupOwnerList = groupOwnerList;
    }

    public String getCreatorId() {
        return mCreatorId;
    }

    public void setCreatorId(String creatorId) {
        mCreatorId = creatorId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mName);
        dest.writeString(this.mGroupPhoto);
        dest.writeTypedList(this.mParticipant);
        dest.writeParcelable(this.mGroupMediaCount, flags);
        dest.writeTypedList(this.mGroupOwnerList);
        dest.writeString(this.mCreatorId);
    }

    protected GroupDetail(Parcel in) {
        this.mId = in.readString();
        this.mName = in.readString();
        this.mGroupPhoto = in.readString();
        this.mParticipant = in.createTypedArrayList(User.CREATOR);
        this.mGroupMediaCount = in.readParcelable(GroupMediaCount.class.getClassLoader());
        this.mGroupOwnerList = in.createTypedArrayList(User.CREATOR);
        this.mCreatorId = in.readString();
    }

    public static final Creator<GroupDetail> CREATOR = new Creator<GroupDetail>() {
        @Override
        public GroupDetail createFromParcel(Parcel source) {
            return new GroupDetail(source);
        }

        @Override
        public GroupDetail[] newArray(int size) {
            return new GroupDetail[size];
        }
    };
}
