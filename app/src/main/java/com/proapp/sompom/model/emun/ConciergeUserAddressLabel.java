package com.proapp.sompom.model.emun;

import android.text.TextUtils;

import com.proapp.sompom.R;

import java.util.Locale;

/**
 * Created by Or Vitovongsak on 22/11/21.
 */
public enum ConciergeUserAddressLabel {

    HOME("Home", R.string.shop_address_home_label),
    WORK("Work", R.string.shop_address_work_label),
    SCHOOL("SCHOOL", R.string.shop_address_school_label),
    OTHER("OTHER", R.string.shop_new_address_other_label);

    private final String mValue;
    private final int mLabel;

    ConciergeUserAddressLabel(String value, int label) {
        mValue = value;
        mLabel = label;
    }

    public String getValue() {
        return mValue;
    }

    public int getLabel() {
        return mLabel;
    }

    public static ConciergeUserAddressLabel fromValue(String value) {
        for (ConciergeUserAddressLabel type : ConciergeUserAddressLabel.values()) {
            if (TextUtils.equals(type.mValue.toLowerCase(), value.toLowerCase())) {
                return type;
            }
        }

        return null;
    }
}
