package com.proapp.sompom.model.emun;

import android.content.Context;

import androidx.annotation.DrawableRes;

import com.proapp.sompom.R;
import com.proapp.sompom.utils.AttributeConverter;

/**
 * Created by Or Vitovongsak on 20/12/21.
 */
public enum ConciergeOrderTrackingStatus {

    PASSED(R.attr.shop_tracking_order_passed_status,
            R.attr.shop_tracking_order_passed_status_icon,
            R.attr.shop_tracking_order_passed_status_icon_background,
            R.attr.shop_tracking_order_passed_status_vertical_border,
            R.attr.shop_tracking_order_passed_status_vertical_border),
    CURRENT(R.attr.shop_tracking_order_current_status,
            R.attr.shop_tracking_order_current_status_icon,
            R.attr.shop_tracking_order_current_status_icon_background,
            R.attr.shop_tracking_order_passed_status_vertical_border,
            R.attr.shop_tracking_order_next_status_vertical_border),
    NEXT(R.attr.shop_tracking_order_next_status,
            R.attr.shop_tracking_order_next_status_icon,
            R.attr.shop_tracking_order_next_status_icon_background,
            R.attr.shop_tracking_order_next_status_vertical_border,
            R.attr.shop_tracking_order_next_status_vertical_border);

    @DrawableRes
    private final int mStatusTextColor;
    @DrawableRes
    private final int mStatusIconColor;
    @DrawableRes
    private final int mStatusIconBackgroundColor;
    @DrawableRes
    private final int mTopLineColor;
    @DrawableRes
    private final int mBottomLineColor;

    ConciergeOrderTrackingStatus(int statusTextColor,
                                 int statusIconColor,
                                 int statusIconBackgroundColor,
                                 int topLineColor,
                                 int bottomLineColor) {
        mStatusTextColor = statusTextColor;
        mStatusIconColor = statusIconColor;
        mStatusIconBackgroundColor = statusIconBackgroundColor;
        mTopLineColor = topLineColor;
        mBottomLineColor = bottomLineColor;
    }

    public int getStatusTextColor(Context context) {
        return AttributeConverter.convertAttrToColor(context, mStatusTextColor);
    }

    public int getStatusIconColor(Context context) {
        return AttributeConverter.convertAttrToColor(context, mStatusIconColor);
    }

    public int getStatusIconBackgroundColor(Context context) {
        return AttributeConverter.convertAttrToColor(context, mStatusIconBackgroundColor);
    }

    public int getTopLineColor(Context context) {
        return AttributeConverter.convertAttrToColor(context, mTopLineColor);
    }

    public int getBottomLineColor(Context context) {
        return AttributeConverter.convertAttrToColor(context, mBottomLineColor);
    }
}
