package com.proapp.sompom.model.result;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.listener.ConversationDataAdaptive;
import com.proapp.sompom.model.DifferentAdaptive;

public class TelegramRedirectUser implements ConversationDataAdaptive, DifferentAdaptive<TelegramRedirectUser> {
    public static final String FIELD_ID = "_id";
    public static final String FIELD_IS_CURRENTLY_CONNECTED = "isCurrentlyConnected";
    public static final String FIELD_FIRST_NAME = "firstName";
    public static final String FIELD_LAST_NAME = "lastName";
    public static final String FIELD_PROFILE = "profileUrl";

    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_FIRST_NAME)
    private String mFirstName;
    @SerializedName(FIELD_LAST_NAME)
    private String mLastName;
    @SerializedName(FIELD_IS_CURRENTLY_CONNECTED)
    private boolean mIsCurrentlyConnected;
    @SerializedName(FIELD_PROFILE)
    private String mProfileUrl;

    public boolean isIsCurrentlyConnected() {
        return mIsCurrentlyConnected;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public String getProfileUrl() {
        return mProfileUrl;
    }


    public String getName() {
        return mFirstName + mLastName;
    }

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public boolean areItemsTheSame(TelegramRedirectUser other) {
        return TextUtils.equals(getId(), other.getId());
    }

    @Override
    public boolean areContentsTheSame(TelegramRedirectUser other) {
        return TextUtils.equals(getFirstName(), other.getFirstName()) &&
                TextUtils.equals(getLastName(), other.getLastName()) &&
                TextUtils.equals(getProfileUrl(), other.getProfileUrl()) &&
                mIsCurrentlyConnected == other.mIsCurrentlyConnected;
    }
}
