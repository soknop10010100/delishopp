package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.listener.ConversationDataAdaptive;

public class TelegramChatRequest implements ConversationDataAdaptive {
    private static final String FIELD_ID = "_id";
    private static final String FIELD_USER = "user";


    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_USER)
    private User mUser;

    public String getId() {
        return mId;
    }

    public User getUser() {
        return mUser;
    }
}
