package com.proapp.sompom.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/**
 * Created by nuonveyo on 6/11/18.
 */

public class SearchAddressResult implements Parcelable, ConciergeCurrentLocationDisplayAdaptive {

    private String mId;
    private String mAddress;
    private String mFullAddress;
    private String mCity;
    private String mCountry;
    private Locations mLocations;
    private String mPlaceTitle;

    public SearchAddressResult() {
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getPlaceTitle() {
        return mPlaceTitle;
    }

    public void setPlaceTitle(String placeTitle) {
        mPlaceTitle = placeTitle;
    }

    public String getFullAddress() {
        return mFullAddress;
    }

    public void setFullAddress(String fullAddress) {
        mFullAddress = fullAddress;
    }

    public String getValidFullPlaceAddress() {
        if (!TextUtils.isEmpty(getPlaceTitle()) && !getAddress().contains(getPlaceTitle())) {
            return getPlaceTitle() + ", " + getAddress();
        } else if (!getFullAddress().contains(getPlaceTitle())) {
            return getFullAddress();
        } else {
            return getAddress();
        }
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }


    public void setCountry(String country) {
        mCountry = country;
    }

    public Locations getLocations() {
        return mLocations;
    }

    public void setLocations(Locations locations) {
        mLocations = locations;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public boolean isValid() {
        return !TextUtils.isEmpty(getAddress());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mAddress);
        dest.writeString(this.mFullAddress);
        dest.writeString(this.mCity);
        dest.writeString(this.mCountry);
        dest.writeString(this.mPlaceTitle);
        dest.writeParcelable(this.mLocations, flags);
    }

    protected SearchAddressResult(Parcel in) {
        this.mId = in.readString();
        this.mAddress = in.readString();
        this.mFullAddress = in.readString();
        this.mCity = in.readString();
        this.mCountry = in.readString();
        this.mPlaceTitle = in.readString();
        this.mLocations = in.readParcelable(Locations.class.getClassLoader());
    }

    public static final Creator<SearchAddressResult> CREATOR = new Creator<SearchAddressResult>() {
        @Override
        public SearchAddressResult createFromParcel(Parcel source) {
            return new SearchAddressResult(source);
        }

        @Override
        public SearchAddressResult[] newArray(int size) {
            return new SearchAddressResult[size];
        }
    };

    @Override
    public String toString() {
        return "SearchAddressResult{" +
                "mId='" + mId + '\'' +
                ", mAddress='" + mAddress + '\'' +
                ", mFullAddress='" + mFullAddress + '\'' +
                ", mCity='" + mCity + '\'' +
                ", mCountry='" + mCountry + '\'' +
                ", mLocations=" + mLocations +
                ", mPlaceTitle='" + mPlaceTitle + '\'' +
                '}';
    }
}
