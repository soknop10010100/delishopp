package com.proapp.sompom.model.notification;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.usermentionable.utils.RenderTextAsMentionable;
import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.result.User;

import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class NotificationActor implements RealmModel, Parcelable, NotificationProfileDisplayAdaptive {

    @PrimaryKey
    @SerializedName(User.FIELD_ID)
    private String mId;
    @SerializedName(User.FIELD_USER_PROFILE)
    private String mUserProfile;
    @SerializedName(User.FIELD_LAST_NAME)
    private String mLastName;
    @SerializedName(User.FIELD_FIRST_NAME)
    private String mFirstName;
    @Ignore
    @SerializedName(User.FIELD_IS_ONLINE)
    private Boolean mIsOnline;

    public NotificationActor() {
    }

    protected NotificationActor(Parcel in) {
        mId = in.readString();
        mUserProfile = in.readString();
        mLastName = in.readString();
        mFirstName = in.readString();
        this.mIsOnline = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mUserProfile);
        dest.writeString(mLastName);
        dest.writeString(mFirstName);
        dest.writeValue(this.mIsOnline);
    }

    @Override
    public String getDisplayProfileUrl() {
        return getUserProfile();
    }

    @Override
    public String getDisplayFirstName() {
        return getFirstName();
    }

    @Override
    public String getDisplayLastName() {
        return getLastName();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NotificationActor> CREATOR = new Creator<NotificationActor>() {
        @Override
        public NotificationActor createFromParcel(Parcel in) {
            return new NotificationActor(in);
        }

        @Override
        public NotificationActor[] newArray(int size) {
            return new NotificationActor[size];
        }
    };

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getUserProfile() {
        return mUserProfile;
    }

    public void setUserProfile(String userProfile) {
        mUserProfile = userProfile;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getFullName() {
        String fullName = mFirstName + " " + mLastName;
        return RenderTextAsMentionable.getValidDisplayName(fullName);
    }

    public Boolean isOnline() {
        return mIsOnline;
    }

    public void setOnline(Boolean online) {
        mIsOnline = online;
    }

    @Override
    public String getDisplayFullName() {
        return getFullName();
    }
}