package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.SupportCustomErrorResponse2;
import com.proapp.sompom.model.emun.CustomErrorDataType;

import java.util.ArrayList;
import java.util.List;

public class ConciergeOrderResponseCustomErrorData extends SupportCustomErrorResponse2 {

    @SerializedName("types")
    private List<String> mErrorTypeList;

    @SerializedName("basket")
    private ConciergeBasket mBasket;

    public ConciergeBasket getBasket() {
        return mBasket;
    }

    public List<CustomErrorDataType> getErrorTypes() {
        List<CustomErrorDataType> errorDataTypes = new ArrayList<>();
        if (mErrorTypeList != null && !mErrorTypeList.isEmpty()) {
            for (String errorType : mErrorTypeList) {
                CustomErrorDataType customErrorDataType = CustomErrorDataType.fromValue(errorType);
                if (customErrorDataType != CustomErrorDataType.UNKNOWN) {
                    errorDataTypes.add(customErrorDataType);
                }
            }
        }

        return errorDataTypes;
    }
}
