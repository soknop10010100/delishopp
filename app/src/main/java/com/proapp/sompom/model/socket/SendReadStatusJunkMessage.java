package com.proapp.sompom.model.socket;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * This model will be used to save sending read message of current user in case of sending via Socket
 * fail from some reasons such no internet, we can retry sending again by query from this local dta
 * and send them again via Socket.
 */
@RealmClass
public class SendReadStatusJunkMessage implements RealmModel {

    @PrimaryKey
    private String mMessageId;
    private String mConversationId;

    public SendReadStatusJunkMessage() {
    }

    public SendReadStatusJunkMessage(String messageId, String conversationId) {
        mMessageId = messageId;
        mConversationId = conversationId;
    }

    public String getMessageId() {
        return mMessageId;
    }

    public void setMessageId(String messageId) {
        mMessageId = messageId;
    }

    public String getConversationId() {
        return mConversationId;
    }

    public void setConversationId(String conversationId) {
        mConversationId = conversationId;
    }
}
