package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Or Vitovongsak on 8/6/20.
 */
public class NotificationSetting {

    @SerializedName("timezone")
    private String mTimezone;

    @SerializedName("dateList")
    private List<DateList> mDateList;

    public String getTimezone() {
        return mTimezone;
    }

    public List<DateList> getDateList() {
        return mDateList;
    }

    public void setTimezone(String timezone) {
        mTimezone = timezone;
    }

    public void setDateList(List<DateList> dateList) {
        mDateList = dateList;
    }
}
