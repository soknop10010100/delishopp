package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhom Veasna on 23/12/20.
 */
public class SearchGroupMessageRequestBody {

    @SerializedName("conversationId")
    private String mConversationId;

    @SerializedName("keyword")
    private String mKeyword;

    public SearchGroupMessageRequestBody(String conversationId, String keyword) {
        mConversationId = conversationId;
        mKeyword = keyword;
    }
}
