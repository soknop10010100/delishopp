package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.SupportCustomErrorResponse;
import com.proapp.sompom.model.SupportCustomErrorResponse3;
import com.proapp.sompom.model.emun.AuthType;

public class VerifyPhoneResponse extends SupportCustomErrorResponse3 {

    @SerializedName("authType")
    private String mAuthType;

    public AuthType getAuthType() {
        return AuthType.fromValue(mAuthType);
    }
}
