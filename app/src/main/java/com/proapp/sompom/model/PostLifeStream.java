package com.proapp.sompom.model;

import com.proapp.sompom.helper.IntentData;
import com.proapp.sompom.model.emun.ViewType;

/**
 * Created by Chhom Veasna on 11/8/2019.
 */
public class PostLifeStream implements Adaptive {

    private LifeStream mLifeStream;
    private boolean mIsPosting;
    private boolean mIsError;
    private int mErrorNotificationId;

    public int getErrorNotificationId() {
        return mErrorNotificationId;
    }

    public void setErrorNotificationId(int errorNotificationId) {
        mErrorNotificationId = errorNotificationId;
    }

    public PostLifeStream(LifeStream lifeStream) {
        mLifeStream = lifeStream;
    }

    public void setLifeStream(LifeStream lifeStream) {
        mLifeStream = lifeStream;
    }

    public LifeStream getLifeStream() {
        return mLifeStream;
    }

    public boolean isPosting() {
        return mIsPosting;
    }

    public void setPosting(boolean posting) {
        mIsPosting = posting;
    }

    public void setError(boolean error) {
        mIsError = error;
    }

    public boolean isError() {
        return mIsError;
    }

    @Override
    public String getId() {
        if (mLifeStream != null) {
            return mLifeStream.getId();
        }

        return null;
    }

    @Override
    public ViewType getTimelineViewType() {
        return ViewType.POSTING_WALL;
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {

    }
}
