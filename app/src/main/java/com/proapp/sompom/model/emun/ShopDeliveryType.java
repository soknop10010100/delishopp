package com.proapp.sompom.model.emun;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.StringRes;

import com.proapp.sompom.R;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public enum ShopDeliveryType {

    PICKUP("pickUp", R.string.shop_delivery_tab_pickup),
    DELIVERY("delivery", R.string.shop_delivery_tab_delivery),
    NONE("None", -1);

    private String mValue;
    private @StringRes
    int mDisplayValue;

    ShopDeliveryType(String value, @StringRes int displayValue) {
        mValue = value;
        mDisplayValue = displayValue;
    }

    public String getValue() {
        return mValue;
    }

    public String getDisplayValue(Context context) {
        return context.getString(mDisplayValue);
    }

    public static ShopDeliveryType fromValue(String value) {
        for (ShopDeliveryType type : ShopDeliveryType.values()) {
            if (TextUtils.equals(type.mValue, value)) {
                return type;
            }
        }

        return NONE;
    }
}
