package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeliveryRange implements Parcelable {

    @SerializedName("allowPaymentMethod")
    private List<String> mAllowPaymentMethod;

    protected DeliveryRange(Parcel in) {
        mAllowPaymentMethod = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(mAllowPaymentMethod);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DeliveryRange> CREATOR = new Creator<DeliveryRange>() {
        @Override
        public DeliveryRange createFromParcel(Parcel in) {
            return new DeliveryRange(in);
        }

        @Override
        public DeliveryRange[] newArray(int size) {
            return new DeliveryRange[size];
        }
    };

    public List<String> getAllowPaymentMethod() {
        return mAllowPaymentMethod;
    }

    public void setAllowPaymentMethod(List<String> allowPaymentMethod) {
        mAllowPaymentMethod = allowPaymentMethod;
    }
}
