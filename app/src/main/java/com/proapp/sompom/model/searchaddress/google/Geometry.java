package com.proapp.sompom.model.searchaddress.google;

import com.google.gson.annotations.SerializedName;

public class Geometry {

    @SerializedName("location")
    private Location mLocation;

    public Location getLocation() {
        return mLocation;
    }

    public void setLocation(Location location) {
        mLocation = location;
    }
}
