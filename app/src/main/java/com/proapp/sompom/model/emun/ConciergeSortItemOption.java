package com.proapp.sompom.model.emun;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.StringRes;

import com.proapp.sompom.R;

/**
 * Created by Or Vitovongsak on 5/5/22.
 */
public enum ConciergeSortItemOption {

    NAME_A_Z("a-z", R.string.shop_item_sort_by_name_asc_title),
    NAME_Z_A("z-a", R.string.shop_item_sort_by_name_desc_title),
    PRICE_LOW_HIGH("low-high", R.string.shop_item_sort_by_price_low_to_high_title),
    PRICE_HIGH_LOW("high-low", R.string.shop_item_sort_by_price_high_to_low_title);

    private final String mValue;
    private @StringRes
    int mLabelResource;

    ConciergeSortItemOption(String value, int labelResource) {
        mValue = value;
        mLabelResource = labelResource;
    }

    public static ConciergeSortItemOption from(String value) {
        for (ConciergeSortItemOption deeplinkType : ConciergeSortItemOption.values()) {
            if (TextUtils.equals(value, deeplinkType.mValue)) {
                return deeplinkType;
            }
        }

        return NAME_A_Z;
    }

    public String getValue() {
        return mValue;
    }

    public String getDisplayLabel(Context context) {
        return context.getString(mLabelResource);
    }

    public static ConciergeSortItemOption[] getAllOptions() {
        return ConciergeSortItemOption.values();
    }
}
