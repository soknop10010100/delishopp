package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Or Vitovongsak on 24/12/21.
 */
public class ConciergeShopStatusNotification {

    public static final String FIELD_IS_CLOSED = "isClosed";

    @SerializedName(FIELD_IS_CLOSED)
    private boolean mIsShopClosed;

    public boolean isShopClosed() {
        return mIsShopClosed;
    }
}
