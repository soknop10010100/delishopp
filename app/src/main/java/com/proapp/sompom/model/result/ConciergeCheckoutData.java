package com.proapp.sompom.model.result;

import com.proapp.sompom.model.ConciergeCartModel;
import com.proapp.sompom.model.ConciergeOrderItemDisplayAdaptive;
import com.proapp.sompom.model.ShopDeliveryTimeSelection;
import com.proapp.sompom.model.concierge.ConciergeOrderResponseWithABAPayWay;
import com.proapp.sompom.model.concierge.ConciergeShop;

import java.util.List;

public class ConciergeCheckoutData {

    private ConciergeCartModel mCartModel;
    private List<ConciergeOrderItemDisplayAdaptive> mItemList;
    private ShopDeliveryTimeSelection mDeliveryData;
    private float mTotalDiscount;
    private float mDeliveryCharge;
    private float mContainerCharge;
    private String mCompanyName;
    private String mCompanyAddress;
    private String mUserName;
    private ConciergeShop mSingleShop;
    private ConciergeOrderResponseWithABAPayWay mResumePaymentData;

    public void setResumePaymentData(ConciergeOrderResponseWithABAPayWay resumePaymentData) {
        mResumePaymentData = resumePaymentData;
    }

    public ConciergeOrderResponseWithABAPayWay getResumePaymentData() {
        return mResumePaymentData;
    }

    public ConciergeCartModel getCartModel() {
        return mCartModel;
    }

    public void setCartModel(ConciergeCartModel cartModel) {
        mCartModel = cartModel;
    }

    public List<ConciergeOrderItemDisplayAdaptive> getItemList() {
        return mItemList;
    }

    public void setItemList(List<ConciergeOrderItemDisplayAdaptive> itemList) {
        mItemList = itemList;
    }

    public float getTotalDiscount() {
        return mTotalDiscount;
    }

    public void setTotalDiscount(float totalDiscount) {
        mTotalDiscount = totalDiscount;
    }

    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String companyName) {
        mCompanyName = companyName;
    }

    public String getCompanyAddress() {
        return mCompanyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        mCompanyAddress = companyAddress;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public float getDeliveryCharge() {
        return mDeliveryCharge;
    }

    public void setDeliveryCharge(float deliveryCharge) {
        mDeliveryCharge = deliveryCharge;
    }

    public float getContainerCharge() {
        return mContainerCharge;
    }

    public void setContainerCharge(float containerCharge) {
        this.mContainerCharge = containerCharge;
    }

    public ShopDeliveryTimeSelection getDeliveryData() {
        return mDeliveryData;
    }

    public void setDeliveryData(ShopDeliveryTimeSelection deliveryData) {
        mDeliveryData = deliveryData;
    }

    public ConciergeShop getSingleShop() {
        return mSingleShop;
    }

    public void setSingleShop(ConciergeShop singleShop) {
        mSingleShop = singleShop;
    }
}
