package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ChhomVeasna on 1/13/21.
 */

public class ResetPasswordRequestBody {

    @SerializedName("code")
    private String mToken;

    @SerializedName("password")
    private String mPassword;

    @SerializedName("passwordConfirmation")
    private String mPasswordConfirm;

    public ResetPasswordRequestBody(String token, String password, String passwordConfirm) {
        mToken = token;
        mPassword = password;
        mPasswordConfirm = passwordConfirm;
    }
}
