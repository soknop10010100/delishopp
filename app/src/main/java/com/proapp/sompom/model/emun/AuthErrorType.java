package com.proapp.sompom.model.emun;

import android.text.TextUtils;

import com.proapp.sompom.R;

import androidx.annotation.StringRes;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public enum AuthErrorType {

    EMAIL_EXISTED("sign_up_error_email_taken", R.string.sign_up_error_email_taken),
    PHONE_EXISTED("sign_up_error_phone_taken", R.string.sign_up_error_phone_taken),
    INVALID_PHONE_FORMAT("sign_up_error_invalid_phone", R.string.sign_up_error_invalid_phone),
    FIREBASE_TOKEN_INVALID("firebase_token_invalid", R.string.verify_error_firebase_token_invalid),
    UNKNOWN("Unknown", -1);

    private String mCode;
    private @StringRes
    int mMessage;

    AuthErrorType(String code, int message) {
        mCode = code;
        mMessage = message;
    }

    public int getMessage() {
        return mMessage;
    }

    public String getCode() {
        return mCode;
    }

    public static AuthErrorType fromValue(String value) {
        for (AuthErrorType type : AuthErrorType.values()) {
            if (TextUtils.equals(type.mCode, value)) {
                return type;
            }
        }

        return UNKNOWN;
    }
}
