package com.proapp.sompom.model.emun;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.FontRes;
import androidx.annotation.StringRes;

import com.proapp.sompom.R;

/**
 * Created by Veasna Chhom on 20/9/21.
 */
public enum ConciergeOrderStatus {

    PENDING("PENDING", R.string.shop_order_list_status_pending, R.string.fi_rr_receipt, R.font.uicons_solid_rounded),
    PROCESSING("PROCESSING", R.string.shop_order_list_status_processing, R.string.fi_rr_shop, R.font.uicons_solid_rounded),
    PICK_UP("PICK_UP", R.string.shop_order_list_status_pick_up, R.string.fi_rr_user_time, R.font.uicons_solid_rounded),
    DELIVERING("DELIVERING", R.string.shop_order_list_status_delivering, R.string.fi_sr_time_fast, R.font.uicons_solid_rounded),
    ARRIVED("ARRIVED", R.string.shop_order_list_status_arrived, R.string.fi_rr_location_alt, R.font.uicons_solid_rounded),
    DELIVERED("DELIVERED", R.string.shop_order_list_status_delivered, R.string.fi_rr_trophy, R.font.uicons_solid_rounded),
    // NOTE: canceled order icon is a placeholder for now
    CANCELED("CANCELED", R.string.shop_order_list_status_canceled, R.string.fi_rr_receipt, R.font.uicons_solid_rounded),
    UNKNOWN("UNKNOWN", R.string.shop_order_list_status_canceled, R.string.fi_rr_receipt, R.font.uicons_solid_rounded);

    private final String mValue;
    private final @StringRes
    int mLabel;

    @StringRes
    private final int mIcon;
    @FontRes
    private int mFontIcon;

    ConciergeOrderStatus(String value, int label, int icon, int fontIcon) {
        mValue = value;
        mLabel = label;
        mIcon = icon;
        mFontIcon = fontIcon;
    }

    public String getValue() {
        return mValue;
    }

    public String getLabel(Context context) {
        return context.getString(mLabel);
    }

    public String getIcon(Context context) {
        return context.getString(mIcon);
    }

    public int getFontIcon() {
        return mFontIcon;
    }

    public static ConciergeOrderStatus fromValue(String value) {
        for (ConciergeOrderStatus type : ConciergeOrderStatus.values()) {
            if (TextUtils.equals(type.mValue, value)) {
                return type;
            }
        }

        return UNKNOWN;
    }
}
