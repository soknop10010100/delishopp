package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Or Vitovongsak on 8/6/20.
 */
public class TimeList {

    @SerializedName("start")
    private Date mStartTime;

    @SerializedName("end")
    private Date mEndTime;

    public Date getStartTime() {
        return mStartTime;
    }

    public Date getEndTime() {
        return mEndTime;
    }
}
