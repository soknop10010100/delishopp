package com.proapp.sompom.model;

import com.proapp.sompom.helper.IntentData;
import com.proapp.sompom.model.emun.ViewType;

/**
 * Created by Veasna Chhom on 17/12/21.
 */

public class WelcomePostTimelineItem implements Adaptive {

    private String mWelcomePostText;

    public WelcomePostTimelineItem(String welcomePostText) {
        mWelcomePostText = welcomePostText;
    }

    @Override
    public boolean isWelcomePostType() {
        return true;
    }

    @Override
    public String getId() {
        return "539a3ece-3e63-454f-b5c5-d5e17dff4027";
    }

    @Override
    public ViewType getTimelineViewType() {
        return ViewType.Timeline;
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
    }

    @Override
    public String getTitle() {
        return mWelcomePostText;
    }
}
