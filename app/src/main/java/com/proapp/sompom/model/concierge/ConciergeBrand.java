package com.proapp.sompom.model.concierge;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.ConciergeItemAdaptive;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;
import com.proapp.sompom.model.DifferentAdaptive;

public class ConciergeBrand extends AbsConciergeModel implements ConciergeShopDetailDisplayAdaptive,
        DifferentAdaptive<ConciergeBrand>,
        ConciergeItemAdaptive {

    @SerializedName(FIELD_NAME)
    private String mName;

    @SerializedName(FIELD_DESCRIPTION)
    private String mDescription;

    @SerializedName(FIELD_LOGO)
    private String mLogo;

    @SerializedName(FIELD_IMAGE)
    private String mImage;

    private boolean mIsSelected;

    public String getName() {
        return mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getLogo() {
        return mLogo;
    }

    public String getImage() {
        return mImage;
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    public void setSelected(boolean selected) {
        mIsSelected = selected;
    }

    @Override
    public boolean areItemsTheSame(ConciergeBrand other) {
        return TextUtils.equals(other.getId(), getId());
    }

    @Override
    public boolean areContentsTheSame(ConciergeBrand other) {
        return TextUtils.equals(getName(), other.getName()) &&
                TextUtils.equals(getDescription(), other.getDescription()) &&
                TextUtils.equals(getImage(), other.getImage()) &&
                TextUtils.equals(getLogo(), other.getLogo());
    }
}
