package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConciergePreferenceDelivery {

    public static final String FIELD_TYPE = "type";
    public static final String FIELD_SLOTS = "slots";

    @SerializedName(FIELD_TYPE)
    private String mType;

    @SerializedName(FIELD_SLOTS)
    private List<ConciergeDeliverySlot> mSlots;

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public List<ConciergeDeliverySlot> getSlots() {
        return mSlots;
    }

    public void setSlots(List<ConciergeDeliverySlot> preferenceDeliverySlots) {
        this.mSlots = preferenceDeliverySlots;
    }
}
