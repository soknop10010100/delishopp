package com.proapp.sompom.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhom Veasna on 07/07/2020.
 */
public class PlayerIdModel {

    @SerializedName("idOneSignal")
    private String mPlayerId;

    @SerializedName("idPushy")
    private String mPushyPlayerId;

    @SerializedName("device")
    private int mDeviceType = 1; //1 Android and Web, 2 iOS

    public String getPlayerId() {
        return mPlayerId;
    }

    public int getDeviceType() {
        return mDeviceType;
    }

    public String getPushyPlayerId() {
        return mPushyPlayerId;
    }

    public void setPushyPlayerId(String pushyPlayerId) {
        mPushyPlayerId = pushyPlayerId;
    }
}
