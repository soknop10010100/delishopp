package com.proapp.sompom.model.emun;

import android.text.TextUtils;

public enum GroupActionType {

    CHANGE_NAME("group_name"),
    CHANGE_PICTURE("group_picture"),
    REMOVE_MEMBER("group_remove_member"),
    ADD_MEMBER("group_add_member"),
    Unkown("unknow");

    private String mType;

    GroupActionType(String type) {
        mType = type;
    }

    public static GroupActionType getType(String type) {
        for (GroupActionType type1 : GroupActionType.values()) {
            if (TextUtils.equals(type1.getType(), type)) {
                return type1;
            }
        }
        return Unkown;
    }

    public String getType() {
        return mType;
    }
}
