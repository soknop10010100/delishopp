package com.proapp.sompom.model.request;

/**
 * Created by imac on 4/21/16.
 */
public class LikeProductRequest {
    //CHECKSTYLE:OFF
    private String user;
    private String item;
    //CHECKSTYLE:OFF

    public LikeProductRequest(String user, String item) {
        this.user = user;
        this.item = item;
    }
}
