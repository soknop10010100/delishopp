package com.proapp.sompom.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Or Vitovongsak on 15/11/21.
 */

public class OrderStatus {

    public static final String FIELD_TYPE = "type";
    public static final String FIELD_ORDER = "order";
    public static final String FIELD_EXPRESS = "express";

    @SerializedName(FIELD_TYPE)
    private String mType;

    @SerializedName(FIELD_EXPRESS)
    private boolean mIsExpress;

    @SerializedName(FIELD_ORDER)
    private NotificationOrder mOrder;

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public NotificationOrder getOrder() {
        return mOrder;
    }

    public boolean isExpress() {
        return mIsExpress;
    }
}
