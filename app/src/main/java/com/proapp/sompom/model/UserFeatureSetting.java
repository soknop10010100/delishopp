package com.proapp.sompom.model;

import com.google.gson.annotations.SerializedName;

public class UserFeatureSetting {

    @SerializedName("enableCall")
    private boolean mEnableCall;

    @SerializedName("enableVideoCall")
    private boolean mEnableVideoCall;

    @SerializedName("enablePost")
    private boolean mEnablePost;

    public boolean isEnableCall() {
        return mEnableCall;
    }

    public void setEnableCall(boolean enableCall) {
        mEnableCall = enableCall;
    }

    public boolean isEnableVideoCall() {
        return mEnableVideoCall;
    }

    public void setEnableVideoCall(boolean enableVideoCall) {
        mEnableVideoCall = enableVideoCall;
    }

    public boolean isEnablePost() {
        return mEnablePost;
    }

    public void setEnablePost(boolean enablePost) {
        mEnablePost = enablePost;
    }
}
