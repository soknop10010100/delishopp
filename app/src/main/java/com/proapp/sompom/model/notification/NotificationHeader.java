package com.proapp.sompom.model.notification;

import com.proapp.sompom.model.emun.NotificationItem;

/**
 * Created by nuonveyo on 7/4/18.
 */

public class NotificationHeader implements NotificationAdaptive {

    private NotificationItem mNotificationItem;
    private boolean mIsNewsHeader;
    private boolean mEnableMarginTop;

    public NotificationItem getNotificationItem() {
        return mNotificationItem;
    }

    public void setNotificationItem(NotificationItem notificationItem) {
        mNotificationItem = notificationItem;
    }

    public boolean isNewsHeader() {
        return mIsNewsHeader;
    }

    public void setNewsHeader(boolean newsHeader) {
        mIsNewsHeader = newsHeader;
    }

    public void setEnableMarginTop(boolean enableMarginTop) {
        mEnableMarginTop = enableMarginTop;
    }

    @Override
    public boolean enableMarginTop() {
        return mEnableMarginTop;
    }
}
