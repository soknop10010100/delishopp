package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;

public class ConciergePreferredSlot {

    public static final String FIELD_START_TIME = "startTime";
    public static final String FIELD_END_TIME = "endTime";

    @SerializedName(FIELD_START_TIME)
    private String mStartTime;

    @SerializedName(FIELD_END_TIME)
    private String mEndTime;

    public String getStartTime() {
        return mStartTime;
    }

    public void setStartTime(String startTime) {
        this.mStartTime = startTime;
    }

    public String getEndTime() {
        return mEndTime;
    }

    public void setEndTime(String endTime) {
        this.mEndTime = endTime;
    }
}

