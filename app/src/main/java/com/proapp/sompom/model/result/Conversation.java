package com.proapp.sompom.model.result;


import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.listener.ConversationDataAdaptive;
import com.proapp.sompom.model.Search;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by he.rotha
 * on 8/25/16.
 */
@RealmClass
public class Conversation implements Parcelable, Search, ConversationDataAdaptive, RealmModel, Comparable<Conversation> {

    private static final String FIELD_ID = "conversationId";
    private static final String FIELD_LAST_MESSAGE = "lastMessage";
    private static final String FIELD_SENDER = "sender";
    private static final String FIELD_LAST_MESSAGE_DATE = "lastMessageDate";
    private static final String FIELD_PARTICIPANTS = "participants";
    private static final String FIELD_PRODUCT = "productInfos";
    private static final String FIELD_SELLER = "seller";
    private static final String GROUP_ID = "groupId";
    public static final String FIELD_GROUP_IMG_URL = "groupPicture";
    private static final String GROUP_NAME = "name";
    private static final String ACTIVE_CALL = "activeCall";
    private static final String VIRGIL_GROUP_OWNER = "virgilGroupOwner";
    private static final String FIELD_IS_DELETED = "isDeleted";
    private static final String FIELD_UNREAD_COUNT = "unreadCount";
    private static final String FIELD_IS_REMOVED = "isRemoved";
    private static final String FIELD_TYPE = "type";
    private static final String FIELD_REPLY_TEXT_NOTICE = "replyTextNotice";
    private static final String FIELD_TELEGRAM_CONNECTION = "telegramConnection";

    @PrimaryKey
    @SerializedName(FIELD_ID)
    private String mId;
    @Ignore
    @SerializedName(FIELD_LAST_MESSAGE)
    private Chat mLastMessage;
    @SerializedName(FIELD_SENDER)
    private String mSenderID;
    @SerializedName(FIELD_LAST_MESSAGE_DATE)
    private Date mDate;
    @SerializedName(FIELD_PRODUCT)
    private Product mProduct;
    @SerializedName(FIELD_PARTICIPANTS)
    private RealmList<User> mParticipants;
    private User mRecipient;
    private String mContent;
    @SerializedName(FIELD_SELLER)
    private String mSeller;
    private boolean mIsArchived;
    private Boolean mIsRead;
    @SerializedName(FIELD_UNREAD_COUNT)
    private int mUnreadCount;
    @SerializedName(GROUP_ID)
    private String mGroupId;
    @SerializedName(GROUP_NAME)
    private String mGroupName;
    @SerializedName(FIELD_GROUP_IMG_URL)
    private String mGroupImageUrl;
    @Ignore
    private boolean mIsHeaderSection;
    @Ignore
    private String mHeaderSectionTitle;
    @Ignore
    private boolean mIsFromGroupNotification;
    @Ignore
    @SerializedName(ACTIVE_CALL)
    private ActiveCall mActiveCall;
    @SerializedName(FIELD_IS_DELETED)
    private String mIsDeleted;
    @SerializedName(FIELD_IS_REMOVED)
    private Boolean mIsRemoved;
    @SerializedName(FIELD_TYPE)
    private String mType;
    private String mLastMessageId;
    @SerializedName(FIELD_REPLY_TEXT_NOTICE)
    private String mReplyTextNotice;
    @SerializedName(FIELD_TELEGRAM_CONNECTION)
    private TelegramConnection mTelegramConnection;

    //CHECKSTYLE:OFF
    public Conversation() {
    }

    public Conversation(String headerSectionTitle) {
        mIsHeaderSection = true;
        mHeaderSectionTitle = headerSectionTitle;
    }

    public static Conversation getInstanceForCall(Conversation conversation) {
        Conversation conversation1 = new Conversation();
        conversation1.setId(conversation.getId());
        conversation1.setGroupId(conversation.getGroupId());
        conversation1.setGroupName(conversation.getGroupName());
        conversation1.setGroupImageUrl(conversation.getGroupImageUrl());
        conversation1.setParticipants(conversation.getParticipants());

        return conversation1;
    }

    public boolean isChannelOpen() {
        return mActiveCall != null && mActiveCall.isChannelOpen();
    }

    public void setChannelOpen(boolean channelOpen) {
        if (mActiveCall == null) {
            mActiveCall = new ActiveCall();
        }
        mActiveCall.setChannelOpen(channelOpen);
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public boolean isVideo() {
        return mActiveCall != null && mActiveCall.isVideo();
    }

    public void setVideo(boolean video) {
        if (mActiveCall == null) {
            mActiveCall = new ActiveCall();
        }
        mActiveCall.setVideo(video);
    }

    public String getReplyTextNotice() {
        return mReplyTextNotice;
    }

    public void setReplyTextNotice(String replyTextNotice) {
        mReplyTextNotice = replyTextNotice;
    }

    public boolean isFromGroupNotification() {
        return mIsFromGroupNotification;
    }

    public void setFromGroupNotification(boolean fromGroupNotification) {
        mIsFromGroupNotification = fromGroupNotification;
    }

    public boolean isHeaderSection() {
        return mIsHeaderSection;
    }

    public String getHeaderSectionTitle() {
        return mHeaderSectionTitle;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public User getOneToOneRecipient(Context context) {
        /*
        The recipient here refer to the recipient of one to one conversation which is normally
        is one user. For the group, we manage with participant and there are many participant.
        Participants field of conversation hold all info of member in conversation which can be used
        either for one to one or group conversation.
         */
        if (mRecipient == null && mParticipants != null) {
            for (User participant : mParticipants) {
                if (!TextUtils.equals(participant.getId(), SharedPrefUtils.getUserId(context))) {
                    mRecipient = participant;
                    break;
                }
            }
        }
        return mRecipient;
    }

    public String getGroupImageUrl() {
        return mGroupImageUrl;
    }

    public void setGroupImageUrl(String groupImageUrl) {
        mGroupImageUrl = groupImageUrl;
    }

    public Chat getLastMessage() {
        return mLastMessage;
    }

    public void setLastMessage(Chat lastMessage) {
        mLastMessage = lastMessage;
    }

    public String getSenderID() {
        return mSenderID;
    }

    public void setSenderID(String senderID) {
        mSenderID = senderID;
    }

    public String getContent(Context context, boolean forDisplay) {
        /*
        Always update the content property with last message of conversation.
         */
        if (TextUtils.isEmpty(mContent)) {
            if (getLastMessage() != null) {
                mContent = getLastMessage().getActualContent(context, this, forDisplay);
            }
        }

        return mContent;
    }

    public String getLastMessageId() {
        return mLastMessageId;
    }

    public void setLastMessageId(String lastMessageId) {
        mLastMessageId = lastMessageId;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }

    public boolean isArchived() {
        return mIsArchived;
    }

    public void setArchived(boolean archived) {
        mIsArchived = archived;
    }

    public boolean isDeleted() {
        return mIsDeleted != null &&
                !TextUtils.isEmpty(mIsDeleted) &&
                mIsDeleted.equalsIgnoreCase("true");
    }

    public void setIsDeleted(boolean isDeleted) {
        if (isDeleted) {
            mIsDeleted = "true";
        } else {
            mIsDeleted = "false";
        }
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public String getGroupId() {
        return mGroupId;
    }

    public void setGroupId(String groupId) {
        mGroupId = groupId;
    }

    public boolean isRead() {
        return mUnreadCount == 0;
    }

    public Boolean getRemoved() {
        return mIsRemoved != null && mIsRemoved;
    }

    public void setRemoved(Boolean removed) {
        mIsRemoved = removed;
    }

    public int getUnreadCount() {
        return mUnreadCount;
    }

    public void setUnreadCount(int mUnreadCount) {
        this.mUnreadCount = mUnreadCount;
    }

    public List<User> getParticipants() {
        return mParticipants;
    }

    public void setParticipants(List<User> participants) {
        mParticipants = new RealmList<>();
        mParticipants.addAll(participants);
    }

    public String getSeller() {
        return mSeller;
    }

    public void setSeller(String seller) {
        mSeller = seller;
    }

    public String getGroupName() {
        return mGroupName;
    }

    public void setGroupName(String groupName) {
        mGroupName = groupName;
    }

    public TelegramConnection getTelegramConnection() {
        return mTelegramConnection;
    }

    public void setTelegramConnection(TelegramConnection telegramConnection) {
        this.mTelegramConnection = telegramConnection;
    }

    public boolean isSupportTelegram() {
        return TextUtils.equals(mType, "support_telegram");
    }

    public boolean isGroup() {
        return !TextUtils.isEmpty(mGroupId);
    }

    public boolean hasSameContent(Context context, Conversation other) {
        if (isHeaderSection() && other.isHeaderSection()) {
            return true;
        }

        if (isGroup()) {
            return (!TextUtils.isEmpty(getId()) && !TextUtils.isEmpty(other.getId())) &&
                    (getId().matches(other.getId()) &&
                            TextUtils.equals(getContent(), other.getContent()) &&
                            (!TextUtils.isEmpty(getGroupName()) && !TextUtils.isEmpty(other.getGroupName())) &&
                            (getGroupName().matches(other.getGroupName())) &&
                            (getParticipants() != null && other.getParticipants() != null) &&
                            (getParticipants().size() == other.getParticipants().size()) &&
                            (!TextUtils.isEmpty(getGroupImageUrl()) && !TextUtils.isEmpty(other.getGroupImageUrl())) &&
                            (getGroupImageUrl().matches(other.getGroupImageUrl())) &&
                            (getDate() != null && other.getDate() != null) &&
                            (getDate().getTime() == other.getDate().getTime()) &&
                            areLastMessageTheSame(other) &&
                            hasSameGroupParticipant(other) &&
                            isRead() == other.isRead() &&
                            getUnreadCount() == other.getUnreadCount() &&
                            !isNeedRefreshTimestamp());
        } else {
            return (!TextUtils.isEmpty(getId()) && !TextUtils.isEmpty(other.getId())) &&
                    (getId().matches(other.getId()) &&
                            TextUtils.equals(getContent(), other.getContent()) &&
                            (getOneToOneRecipient(context) != null && other.getOneToOneRecipient(context) != null) &&
                            (getOneToOneRecipient(context).areContentsTheSame(other.getOneToOneRecipient(context))) &&
                            (getDate() != null && other.getDate() != null) &&
                            (getDate().getTime() == other.getDate().getTime()) &&
                            areLastMessageTheSame(other) &&
                            isRead() == other.isRead() &&
                            getUnreadCount() == other.getUnreadCount() &&
                            !isNeedRefreshTimestamp());
        }
    }

    private boolean areLastMessageTheSame(Conversation other) {
        return getLastMessage() != null && other.getLastMessage() != null &&
                TextUtils.equals(getLastMessage().getId(), other.getLastMessage().getId()) &&
                getLastMessage().isDeleted() == other.getLastMessage().isDeleted() &&
                TextUtils.equals(getLastMessage().getChatType(), other.getLastMessage().getChatType());
    }

    private boolean isNeedRefreshTimestamp() {
        //So this condition will allow the item to always update the timestamp from last message date to now.
        return getDate() != null && getDate().getTime() < System.currentTimeMillis();
    }

    private boolean hasSameGroupParticipant(Conversation other) {
        boolean hasSameContent = true;
        for (User participant : getParticipants()) {
            for (User otherParticipant : other.getParticipants()) {
                if (participant.getId().matches(otherParticipant.getId())) {
                    if (!participant.areContentsTheSame(otherParticipant)) {
                        hasSameContent = false;
                        break;
                    }
                }
            }
        }

        return hasSameContent;
    }

    public boolean isGroupHasInvalidLastMessage() {
        return isGroup() &&
                getLastMessage() != null &&
                !getLastMessage().isValid();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Conversation)) return false;
        Conversation that = (Conversation) o;
        return TextUtils.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public int compareTo(@NonNull Conversation conversation) {
        if (getDate() == null) {
            return 1;
        }
        if (conversation.getDate() == null) {
            return -1;
        }

        return Long.compare(conversation.getDate().getTime(), getDate().getTime());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeParcelable(this.mLastMessage, flags);
        dest.writeString(this.mSenderID);
        dest.writeLong(this.mDate != null ? this.mDate.getTime() : -1);
        dest.writeParcelable(this.mProduct, flags);
        dest.writeTypedList(this.mParticipants);
        dest.writeParcelable(this.mRecipient, flags);
        dest.writeString(this.mContent);
        dest.writeString(this.mSeller);
        dest.writeByte(this.mIsArchived ? (byte) 1 : (byte) 0);
        dest.writeValue(this.mIsRead);
        dest.writeValue(this.mIsRemoved);
        dest.writeString(this.mGroupId);
        dest.writeString(this.mGroupName);
        dest.writeString(this.mGroupImageUrl);
        dest.writeByte(this.mIsHeaderSection ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mIsFromGroupNotification ? (byte) 1 : (byte) 0);
        dest.writeString(this.mHeaderSectionTitle);
        dest.writeParcelable(this.mActiveCall, flags);
        dest.writeString(this.mIsDeleted);
        dest.writeString(this.mType);
        dest.writeString(this.mLastMessageId);
        dest.writeString(this.mReplyTextNotice);
        dest.writeParcelable(this.mTelegramConnection, flags);
    }

    protected Conversation(Parcel in) {
        this.mId = in.readString();
        this.mLastMessage = in.readParcelable(Chat.class.getClassLoader());
        this.mSenderID = in.readString();
        long tmpMDate = in.readLong();
        this.mDate = tmpMDate == -1 ? null : new Date(tmpMDate);
        this.mProduct = in.readParcelable(Product.class.getClassLoader());
        this.mParticipants = new RealmList<>();
        this.mParticipants.addAll(in.createTypedArrayList(User.CREATOR));
        this.mRecipient = in.readParcelable(User.class.getClassLoader());
        this.mContent = in.readString();
        this.mSeller = in.readString();
        this.mIsArchived = in.readByte() != 0;
        this.mIsRead = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mIsRemoved = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mGroupId = in.readString();
        this.mGroupName = in.readString();
        this.mGroupImageUrl = in.readString();
        this.mIsHeaderSection = in.readByte() != 0;
        this.mIsFromGroupNotification = in.readByte() != 0;
        this.mHeaderSectionTitle = in.readString();
        this.mActiveCall = in.readParcelable(ActiveCall.class.getClassLoader());
        this.mIsDeleted = in.readString();
        this.mType = in.readString();
        this.mLastMessageId = in.readString();
        this.mReplyTextNotice = in.readString();
        this.mTelegramConnection = in.readParcelable(TelegramConnection.class.getClassLoader());
    }

    public static final Creator<Conversation> CREATOR = new Creator<Conversation>() {
        @Override
        public Conversation createFromParcel(Parcel source) {
            return new Conversation(source);
        }

        @Override
        public Conversation[] newArray(int size) {
            return new Conversation[size];
        }
    };

    @Override
    public String toString() {
        return "Conversation{" +
                "mId='" + mId + '\'' +
                '}';
    }
}