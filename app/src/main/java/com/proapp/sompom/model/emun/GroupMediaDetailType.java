package com.proapp.sompom.model.emun;

import android.text.TextUtils;

public enum GroupMediaDetailType {

    PHOTO_VIDEO("PHOTO_VIDEO"),
    FILE("FILE"),
    LINK("LINK"),
    VOICE("VOICE"),
    UNKNOWN("Unknown");

    private String mValue;

    GroupMediaDetailType(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public static GroupMediaDetailType fromValue(String value) {
        if (!TextUtils.isEmpty(value)) {
            for (GroupMediaDetailType timelineRedirectionType : GroupMediaDetailType.values()) {
                if (timelineRedirectionType.mValue.equals(value)) {
                    return timelineRedirectionType;
                }
            }
        }

        return UNKNOWN;
    }
}
