package com.proapp.sompom.model.emun;

import com.proapp.sompom.R;

/**
 * Created by Chhom Veasna on 3/9/21.
 */
public enum ChatMediaBg {

    ChatMeTop(R.drawable.layout_chat_media_me_top, R.drawable.layout_chat_media_me_top_selected),
    ChatMeMid(R.drawable.layout_chat_media_me_middle, R.drawable.layout_chat_media_me_middle_selected),
    ChatMeBottom(R.drawable.layout_chat_media_me_bottom, R.drawable.layout_chat_media_me_bottom_selected),
    ChatMeSingle(R.drawable.layout_chat_media_me_single, R.drawable.layout_chat_media_me_single_selected),
    ChatYouTop(R.drawable.layout_chat_media_you_top, R.drawable.layout_chat_media_you_top_selected),
    ChatYouMid(R.drawable.layout_chat_media_you_middle, R.drawable.layout_chat_media_you_middle_selected),
    ChatYouBottom(R.drawable.layout_chat_media_you_bottom, R.drawable.layout_chat_media_you_bottom_selected),
    ChatYouSingle(R.drawable.layout_chat_media_you_single, R.drawable.layout_chat_media_you_single_selected);

    private int mDrawable;
    private int mSelectDrawable;

    ChatMediaBg(int drawable, int selectDrawable) {
        mDrawable = drawable;
        mSelectDrawable = selectDrawable;
    }

    public int getDrawable(boolean isSelect) {
        if (isSelect) {
            return mSelectDrawable;
        } else {
            return mDrawable;
        }
    }

    public static ChatMediaBg getChatLinkBg(ChatBg chatBg) {
        if (chatBg == ChatBg.ChatMeTop) {
            return ChatMeTop;
        } else if (chatBg == ChatBg.ChatMeMid) {
            return ChatMeMid;
        } else if (chatBg == ChatBg.ChatMeBottom) {
            return ChatMeBottom;
        } else if (chatBg == ChatBg.ChatMeSingle) {
            return ChatMeSingle;
        } else if (chatBg == ChatBg.ChatYouTop) {
            return ChatYouTop;
        } else if (chatBg == ChatBg.ChatYouMid) {
            return ChatYouMid;
        } else if (chatBg == ChatBg.ChatYouBottom) {
            return ChatYouBottom;
        } else {
            return ChatYouSingle;
        }
    }
}
