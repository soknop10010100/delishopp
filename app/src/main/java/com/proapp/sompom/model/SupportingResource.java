package com.proapp.sompom.model;

import android.content.Context;
import android.graphics.Bitmap;

import com.amulyakhare.textdrawable.TextDrawable;
import com.proapp.sompom.viewmodel.binding.ImageViewBindingUtil;

/**
 * Created by He Rotha on 7/19/18.
 */
public class SupportingResource {
    private Bitmap mBitmap;
    private TextDrawable mDrawable;

    public SupportingResource(Bitmap bitmap) {
        mBitmap = bitmap;
    }

    public SupportingResource(Context context, String firstName, String lastName) {
        mDrawable = ImageViewBindingUtil.getTextDrawable(context, firstName, lastName);
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
    }

    public TextDrawable getDrawable() {
        return mDrawable;
    }

    public void setDrawable(TextDrawable drawable) {
        mDrawable = drawable;
    }
}
