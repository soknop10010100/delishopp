package com.proapp.sompom.model.result;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.DifferentAdaptive;
import com.proapp.sompom.model.ProductDetailAdaptive;
import com.proapp.sompom.model.emun.ConciergeMenuItemOptionType;
import com.proapp.sompom.model.emun.ConciergeMenuItemType;

import java.util.List;

/**
 * Created by Or Vitovongsak on 31/8/21.
 */

public class ConciergeMenuItemOptionSection extends AbsConciergeModel
        implements DifferentAdaptive<ConciergeMenuItemOptionSection>,
        ProductDetailAdaptive,
        Parcelable {

    private static final String FIELD_DESCRIPTION = "description";

    @SerializedName(FIELD_TITLE)
    private String mName;

    @SerializedName(FIELD_DESCRIPTION)
    private String mDescription;

    @SerializedName(FIELD_TYPE)
    private String mType;

    @SerializedName(FIELD_LIST)
    private List<ConciergeMenuItemOption> mOptionItems;

    @SerializedName(FIELD_IS_DISPLAY_FULL_PRICE)
    private boolean mIsDisplayFullPrice;

    public ConciergeMenuItemOptionSection() {
    }

    protected ConciergeMenuItemOptionSection(Parcel in) {
        mId = in.readString();
        mName = in.readString();
        mDescription = in.readString();
        mType = in.readString();
        mIsDisplayFullPrice = in.readByte() != 0;
        mOptionItems = in.createTypedArrayList(ConciergeMenuItemOption.CREATOR);
    }

    public static final Creator<ConciergeMenuItemOptionSection> CREATOR = new Creator<ConciergeMenuItemOptionSection>() {
        @Override
        public ConciergeMenuItemOptionSection createFromParcel(Parcel in) {
            return new ConciergeMenuItemOptionSection(in);
        }

        @Override
        public ConciergeMenuItemOptionSection[] newArray(int size) {
            return new ConciergeMenuItemOptionSection[size];
        }
    };

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public boolean isDisplayFullPrice() {
         return mIsDisplayFullPrice;
    }

    public void setIsDisplayFullPrice(boolean isDisplayFullPrice) {
        mIsDisplayFullPrice = isDisplayFullPrice;
    }

    public List<ConciergeMenuItemOption> getOptionItems() {
        return mOptionItems;
    }

    public void setOptionItems(List<ConciergeMenuItemOption> options) {
        this.mOptionItems = options;
    }

    private boolean checkIsSectionFree() {
        boolean isAllFreeItem = true;

        for (ConciergeMenuItemOption option : getOptionItems()) {
            if (option.getPrice() != 0) {
                isAllFreeItem = false;
                break;
            }
        }

        return isAllFreeItem;
    }

    @Override
    public boolean areItemsTheSame(ConciergeMenuItemOptionSection other) {
        // Always return true for now since these option section won't have ID
        return true;
    }

    @Override
    public boolean areContentsTheSame(ConciergeMenuItemOptionSection other) {
        return TextUtils.equals(getName(), other.getName()) &&
                TextUtils.equals(getDescription(), other.getDescription()) &&
                TextUtils.equals(getType(), other.getType()) &&
                areOptionItemTheSame(other.getOptionItems());
    }

    private boolean areOptionItemTheSame(List<ConciergeMenuItemOption> other) {
        if ((getOptionItems() == null && other == null) ||
                (getOptionItems().isEmpty() && other.isEmpty())) {
            return true;
        } else if (getOptionItems() != null && other != null
                && getOptionItems().size() == other.size()) {

            for (int index = 0; index < getOptionItems().size(); index++) {
                if (!getOptionItems().get(index).areContentsTheSame(other.get(index))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mName);
        dest.writeString(mDescription);
        dest.writeString(mType);
        dest.writeByte(mIsDisplayFullPrice ? (byte) 1 : (byte) 0);
        dest.writeTypedList(mOptionItems);
    }
}
