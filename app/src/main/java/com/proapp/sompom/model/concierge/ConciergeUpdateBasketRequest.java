package com.proapp.sompom.model.concierge;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;

public class ConciergeUpdateBasketRequest {

    @SerializedName(AbsConciergeModel.FIELD_PREFERENCE_DELIVERY)
    private ConciergePreferenceDelivery mPreferenceDelivery;

    @SerializedName(AbsConciergeModel.FIELD_DELIVERY_INSTRUCTION)
    private String mDeliveryInstruction;

    @SerializedName(AbsConciergeModel.FIELD_DELIVERY_ADDRESS)
    private ConciergeUserAddress mDeliveryAddress;

    @SerializedName(AbsConciergeModel.FIELD_PAYMENT_METHOD)
    private String mPaymentMethod;

    @SerializedName(AbsConciergeModel.FIELD_PAYMENT_PROVIDER)
    private ConciergeOnlinePaymentProvider mPaymentProvider;

    @SerializedName(AbsConciergeModel.FIELD_RECEIPT_URL)
    private String mReceiptUrl;

    @SerializedName(AbsConciergeModel.FIELD_COUPON_ID)
    @Nullable
    private String mCouponId;

    @SerializedName(AbsConciergeModel.FIELD_OOS_OPTION)
    private String mOOSOption;

    @SerializedName(AbsConciergeModel.FIELD_IS_APPLY_WALLET)
    private Boolean mIsAppliedWallet;

    public void setOOSOption(String OOSOption) {
        mOOSOption = OOSOption;
    }

    public ConciergePreferenceDelivery getPreferenceDelivery() {
        return mPreferenceDelivery;
    }

    public void setPreferenceDelivery(ConciergePreferenceDelivery preferenceDelivery) {
        mPreferenceDelivery = preferenceDelivery;
    }

    public void setAppliedWallet(boolean appliedWallet) {
        mIsAppliedWallet = appliedWallet;
    }

    public String getDeliveryInstruction() {
        return mDeliveryInstruction;
    }

    public void setDeliveryInstruction(String deliveryInstruction) {
        mDeliveryInstruction = deliveryInstruction;
    }

    public ConciergeUserAddress getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public void setDeliveryAddress(ConciergeUserAddress deliveryAddress) {
        mDeliveryAddress = deliveryAddress;
    }

    public String getPaymentMethod() {
        return mPaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        mPaymentMethod = paymentMethod;
    }

    public ConciergeOnlinePaymentProvider getPaymentProvider() {
        return mPaymentProvider;
    }

    public void setPaymentProvider(ConciergeOnlinePaymentProvider paymentProvider) {
        mPaymentProvider = paymentProvider;
    }

    public String getReceiptUrl() {
        return mReceiptUrl;
    }

    public void setReceiptUrl(String mReceiptUrl) {
        this.mReceiptUrl = mReceiptUrl;
    }

    public void setCouponId(String couponId) {
        mCouponId = couponId;
    }
}
