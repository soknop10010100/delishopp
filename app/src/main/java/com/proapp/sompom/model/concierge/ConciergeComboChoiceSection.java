package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.DifferentAdaptive;
import com.proapp.sompom.model.ProductDetailAdaptive;

import java.util.List;

/**
 * Created by Or Vitovongsak on 13/10/21.
 */

public class ConciergeComboChoiceSection extends AbsConciergeModel implements Parcelable,
        DifferentAdaptive<ConciergeComboChoiceSection>,
        ProductDetailAdaptive {

    @SerializedName(FIELD_TITLE)
    private String mTitle;

    @SerializedName(FIELD_LIST)
    private List<ConciergeComboItem> mList;

    @SerializedName(FIELD_IS_DISPLAY_FULL_PRICE)
    private boolean mIsDisplayFullPrice;

    public ConciergeComboChoiceSection() {
    }

    protected ConciergeComboChoiceSection(Parcel in) {
        mTitle = in.readString();
        mList = in.createTypedArrayList(ConciergeComboItem.CREATOR);
    }

    public static final Creator<ConciergeComboChoiceSection> CREATOR = new Creator<ConciergeComboChoiceSection>() {
        @Override
        public ConciergeComboChoiceSection createFromParcel(Parcel in) {
            return new ConciergeComboChoiceSection(in);
        }

        @Override
        public ConciergeComboChoiceSection[] newArray(int size) {
            return new ConciergeComboChoiceSection[size];
        }
    };

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public List<ConciergeComboItem> getList() {
        return mList;
    }

    public void setList(List<ConciergeComboItem> list) {
        this.mList = list;
    }

    public boolean isDisplayFullPrice() {
         return mIsDisplayFullPrice;
    }

    public void setIsDisplayFullPrice(boolean isDisplayFullPrice) {
        mIsDisplayFullPrice = isDisplayFullPrice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeTypedList(mList);
    }

    @Override
    public boolean areItemsTheSame(ConciergeComboChoiceSection other) {
        return true;
    }

    @Override
    public boolean areContentsTheSame(ConciergeComboChoiceSection other) {
        return TextUtils.equals(getTitle(), other.getTitle()) &&
                areComboItemTheSame(other.getList());
    }

    private boolean areComboItemTheSame(List<ConciergeComboItem> other) {
        if ((getList() == null && other == null) ||
                (getList().isEmpty() && other.isEmpty())) {
            return true;
        } else if (getList() != null && other != null
                && getList().size() == other.size()) {

            for (int index = 0; index < getList().size(); index++) {
                if (!getList().get(index).areContentsTheSame(other.get(index))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
