package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;

import java.io.Serializable;

public class ConciergeCoupon implements Serializable {

    private Boolean mIsAppliedCouponValid;
    private Boolean mIsTotalPriceBelowCouponMinBasketPrice;
    private boolean mIsSelected;

    @SerializedName(AbsConciergeModel.FIELD_ID)
    @Expose
    protected String mId;
    @SerializedName(AbsConciergeModel.FIELD_NAME)
    @Expose
    private String mName;
    @SerializedName(AbsConciergeModel.FIELD_DESCRIPTION)
    @Expose
    private String mDescription;
    @SerializedName("couponCode")
    @Expose
    private String mCouponCode;
    @SerializedName("discountPercentage")
    @Expose
    private float mDiscountPercentage;
    @SerializedName("discountFixedPrice")
    @Expose
    private float mDiscountFixedPrice;
    @SerializedName("minimumBasketPrice")
    @Expose
    private float mMinimumBasketPrice;
    @SerializedName("maximumDiscountPrice")
    @Expose
    private float mMaximumDiscountPrice;
    @SerializedName("data")
    @Expose
    private CouponDate data;
    @SerializedName("error")
    @Expose
    private boolean isError;
    @SerializedName("couponCodeId")
    @Expose
    private String couponCodeId;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("discountPrice")
    @Expose
    private double discountPrice;
    @SerializedName("subTotal")
    @Expose
    private double subTotal;
    @SerializedName("finalPrice")
    @Expose
    private double finalPrice;
    @SerializedName("delivery_info")
    @Expose
    private CouponDate.DeliveryInfo deliveryInfo;

    public Boolean getmIsAppliedCouponValid() {
        return mIsAppliedCouponValid;
    }

    public Boolean getmIsTotalPriceBelowCouponMinBasketPrice() {
        return mIsTotalPriceBelowCouponMinBasketPrice;
    }

    public boolean ismIsSelected() {
        return mIsSelected;
    }

    public String getmId() {
        return mId;
    }

    public String getmName() {
        return mName;
    }

    public String getmDescription() {
        return mDescription;
    }

    public String getmCouponCode() {
        return mCouponCode;
    }

    public float getmDiscountPercentage() {
        return mDiscountPercentage;
    }

    public float getmDiscountFixedPrice() {
        return mDiscountFixedPrice;
    }

    public float getmMinimumBasketPrice() {
        return mMinimumBasketPrice;
    }

    public float getmMaximumDiscountPrice() {
        return mMaximumDiscountPrice;
    }

    public String getCouponCodeId() {
        return couponCodeId;
    }

    public String getMessage() {
        return message;
    }

    public double getDiscountPrice() {
        return discountPrice;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public CouponDate.DeliveryInfo getDeliveryInfo() {
        return deliveryInfo;
    }

    public boolean isError() {
        return isError;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getCouponCode() {
        return mCouponCode;
    }

    public void setCouponCode(String code) {
        mCouponCode = code;
    }

    public float getDiscountPercentage() {
        return mDiscountPercentage;
    }

    public void setDiscountPercentage(float discountPercentage) {
        mDiscountPercentage = discountPercentage;
    }

    public float getDiscountFixedPrice() {
        return mDiscountFixedPrice;
    }

    public void setDiscountFixedPrice(float discountFixedPrice) {
        mDiscountFixedPrice = discountFixedPrice;
    }

    public float getMinimumBasketPrice() {
        return mMinimumBasketPrice;
    }

    public void setMinimumBasketPrice(float minimumBasketPrice) {
        mMinimumBasketPrice = minimumBasketPrice;
    }

    public float getMaximumDiscountPrice() {
        return mMaximumDiscountPrice;
    }

    public void setMaximumDiscountPrice(float maximumDiscountPrice) {
        mMaximumDiscountPrice = maximumDiscountPrice;
    }

    public void setAppliedCouponValid(boolean appliedCouponValid) {
        mIsAppliedCouponValid = appliedCouponValid;
    }

    public boolean getAppliedCouponValid() {
        return mIsAppliedCouponValid != null && mIsAppliedCouponValid;
    }

    public boolean getIsTotalPriceBelowCouponMinBasketPrice() {
        return mIsTotalPriceBelowCouponMinBasketPrice != null && mIsTotalPriceBelowCouponMinBasketPrice;
    }

    public void setIsTotalPriceBelowCouponMinBasketPrice(boolean isBelowMinPrice) {
        this.mIsTotalPriceBelowCouponMinBasketPrice = isBelowMinPrice;
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    public void setSelected(boolean selected) {
        mIsSelected = selected;
    }

    public CouponDate getData() {
        return data;
    }

    public void setData(CouponDate data) {
        this.data = data;
    }

    public void setCouponCodeId(String couponCodeId) {
        this.couponCodeId = couponCodeId;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDiscountPrice(double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public void setDeliveryInfo(CouponDate.DeliveryInfo deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }
}
