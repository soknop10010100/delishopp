package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;

public class ConciergeCheckTrackingOrderResponse {

    @SerializedName("hasPendingOrder")
    private boolean mHasPendingOrder;

    public boolean isHasPendingOrder() {
        return mHasPendingOrder;
    }
}
