package com.proapp.sompom.model.result;

import java.util.List;

/**
 * Created by Chhom Veasna on 2/4/2020.
 */
public class ListResponseWrapper<D> extends ResponseWrapper {

    private List<D> mData;

    public ListResponseWrapper() {
    }

    public ListResponseWrapper(List<D> data) {
        mData = data;
    }

    public ListResponseWrapper(boolean isLocalData, Throwable error, List<D> data) {
        super(isLocalData, error);
        mData = data;
    }

    public List<D> getData() {
        return mData;
    }

    public void setData(List<D> data) {
        mData = data;
    }

    public boolean isEmpty() {
        return mData == null || mData.isEmpty();
    }
}
