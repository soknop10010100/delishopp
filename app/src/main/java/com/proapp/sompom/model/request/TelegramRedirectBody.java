package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

public class TelegramRedirectBody {
    public static final String APPGORA_USER_ID = "appgoraUserId";
    public static final String FIELD_TELEGRAM_USER_ID = "telegramUserId";

    @SerializedName(APPGORA_USER_ID)
    private String mAppgoraUserId;
    @SerializedName(FIELD_TELEGRAM_USER_ID)
    private String mTelegramUserId;

    public TelegramRedirectBody(String telegramUserId, String appgoraUserId) {
        this.mAppgoraUserId = appgoraUserId;
        this.mTelegramUserId = telegramUserId;
    }

    public String getAppgoraUserId() {
        return mAppgoraUserId;
    }

    public void setAppgoraUserId(String v) {
        this.mAppgoraUserId = v;
    }

    public String getTelegramUserId() {
        return mTelegramUserId;
    }

    public void setTelegramUserId(String v) {
        this.mTelegramUserId = v;
    }
}
