package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Or Vitovongsak on 27/10/22.
 */
public class SaveCustomerLocationRequestBody {

    @SerializedName("latitude")
    private Double mLatitude;
    @SerializedName("longitude")
    private Double mLongitude;

    public SaveCustomerLocationRequestBody(Double latitude, Double longitude) {
        this.mLatitude = latitude;
        this.mLongitude = longitude;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double latitude) {
        mLatitude = latitude;
    }

    public Double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(Double longitude) {
        mLongitude = longitude;
    }
}
