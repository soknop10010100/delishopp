package com.proapp.sompom.model.emun;

import androidx.annotation.StringRes;

import com.proapp.sompom.R;

/**
 * Created by nuonveyo on 8/1/18.
 */

public enum ActionButtonItem {

    Post(2,
            R.string.create_wall_street_post_button,
            R.string.code_play),
    Media(3,
            R.string.media,
            R.string.code_camera);

    private int mId;
    @StringRes
    private int mTitle;
    @StringRes
    private int mIcon;

    ActionButtonItem(int id, int title, int icon) {
        mId = id;
        mTitle = title;
        mIcon = icon;
    }

    public static ActionButtonItem getValueFromId(int id) {
        for (ActionButtonItem actionButtonItem : ActionButtonItem.values()) {
            if (actionButtonItem.getId() == id) {
                return actionButtonItem;
            }
        }
        return Post;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getTitle() {
        return mTitle;
    }

    public void setTitle(int title) {
        mTitle = title;
    }

    public int getIcon() {
        return mIcon;
    }

    public void setIcon(int icon) {
        mIcon = icon;
    }
}
