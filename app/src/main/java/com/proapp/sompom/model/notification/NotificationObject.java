package com.proapp.sompom.model.notification;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

@RealmClass
public class NotificationObject implements RealmModel, Parcelable {

    @SerializedName(NotificationListAndRedirectionHelper.FIELD_POST_ID)
    private String mPostId;
    @SerializedName(NotificationListAndRedirectionHelper.FIELD_MEDIA_ID)
    private String mMediaId;
    @SerializedName(NotificationListAndRedirectionHelper.FIELD_COMMENT_ID)
    private String mCommentId;
    @SerializedName(NotificationListAndRedirectionHelper.FIELD_SUB_COMMENT_ID)
    private String mSubCommentId;
    @SerializedName(NotificationListAndRedirectionHelper.FIELD_CONVERSATION_ID)
    private String mConversionId;
    @SerializedName(NotificationListAndRedirectionHelper.FIELD_CONVERSATION_NAME)
    private String mConversionName;
    @SerializedName(NotificationListAndRedirectionHelper.FIELD_ORDER_ID)
    private String mOrderId;

    public NotificationObject() {
    }

    public NotificationObject(String postId, String mediaId, String commentId, String subCommentId) {
        mPostId = postId;
        mMediaId = mediaId;
        mCommentId = commentId;
        mSubCommentId = subCommentId;
    }

    protected NotificationObject(Parcel in) {
        mPostId = in.readString();
        mMediaId = in.readString();
        mCommentId = in.readString();
        mSubCommentId = in.readString();
        mConversionId = in.readString();
        mConversionName = in.readString();
        mOrderId = in.readString();
    }

    public String getPostId() {
        return mPostId;
    }

    public String getMediaId() {
        return mMediaId;
    }

    public String getCommentId() {
        return mCommentId;
    }

    public String getSubCommentId() {
        return mSubCommentId;
    }

    public String getConversionId() {
        return mConversionId;
    }

    public String getConversionName() {
        return mConversionName;
    }

    public String getOrderId() {return mOrderId;}

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mPostId);
        dest.writeString(mMediaId);
        dest.writeString(mCommentId);
        dest.writeString(mSubCommentId);
        dest.writeString(mConversionId);
        dest.writeString(mConversionName);
        dest.writeString(mOrderId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NotificationObject> CREATOR = new Creator<NotificationObject>() {
        @Override
        public NotificationObject createFromParcel(Parcel in) {
            return new NotificationObject(in);
        }

        @Override
        public NotificationObject[] newArray(int size) {
            return new NotificationObject[size];
        }
    };
}