package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.SupportCustomErrorResponse;

public class SignUpResponse extends SupportCustomErrorResponse {

    @SerializedName("jwt")
    private String mAccessToken;

    @SerializedName("user")
    private User mUser;

    public String getAccessToken() {
        return mAccessToken;
    }

    public User getUser() {
        return mUser;
    }
}
