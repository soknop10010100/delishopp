package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Veasna Chhom on 4/22/21.
 */

public class VerifyPhoneRequestBody {

    @SerializedName("token")
    private String mToken;

    @SerializedName("phone")
    private String mPhone;

    public VerifyPhoneRequestBody(String token, String phone) {
        mToken = token;
        mPhone = phone;
    }
}
