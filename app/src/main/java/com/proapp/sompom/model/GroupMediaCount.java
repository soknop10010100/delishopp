package com.proapp.sompom.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhom Veasna on 7/28/2020.
 */
public class GroupMediaCount implements Parcelable {

    @SerializedName("medias")
    private int mPhotoAndVideoCount;

    @SerializedName("files")
    private int mFileCount;

    @SerializedName("audios")
    private int mAudioCount;

    @SerializedName("links")
    private int mLinkCount;

    public int getPhotoAndVideoCount() {
        return mPhotoAndVideoCount;
    }

    public int getFileCount() {
        return mFileCount;
    }

    public int getAudioCount() {
        return mAudioCount;
    }

    public int getLinkCount() {
        return mLinkCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mPhotoAndVideoCount);
        dest.writeInt(this.mFileCount);
        dest.writeInt(this.mAudioCount);
        dest.writeInt(this.mLinkCount);
    }

    public GroupMediaCount() {
    }

    protected GroupMediaCount(Parcel in) {
        this.mPhotoAndVideoCount = in.readInt();
        this.mFileCount = in.readInt();
        this.mAudioCount = in.readInt();
        this.mLinkCount = in.readInt();
    }

    public static final Creator<GroupMediaCount> CREATOR = new Creator<GroupMediaCount>() {
        @Override
        public GroupMediaCount createFromParcel(Parcel source) {
            return new GroupMediaCount(source);
        }

        @Override
        public GroupMediaCount[] newArray(int size) {
            return new GroupMediaCount[size];
        }
    };
}
