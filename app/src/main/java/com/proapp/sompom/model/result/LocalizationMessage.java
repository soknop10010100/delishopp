package com.proapp.sompom.model.result;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.resourcemanager.helper.LocaleManager;

public class LocalizationMessage {

    @SerializedName(LocaleManager.ENGLISH_LANGUAGE_KEY)
    private String mMessageEN;

    @SerializedName(LocaleManager.FRENCH_LANGUAGE_KEY)
    private String mMessageFR;

    @SerializedName(LocaleManager.KHMER_LANGUAGE_KEY)
    private String mMessageKM;

    public String getMessageEN() {
        return mMessageEN;
    }

    public String getMessageFR() {
        return mMessageFR;
    }

    public String getMessageKM() {
        return mMessageKM;
    }

    public JsonObject getAsJSON() {
        Gson gson = new Gson();
        return new Gson().fromJson(gson.toJson(this), JsonObject.class);
    }

    public String getDefaultOne() {
        if (!TextUtils.isEmpty(mMessageEN)) {
            return mMessageEN;
        }

        if (!TextUtils.isEmpty(mMessageKM)) {
            return mMessageKM;
        }

        if (!TextUtils.isEmpty(mMessageFR)) {
            return mMessageFR;
        }

        return "";
    }
}
