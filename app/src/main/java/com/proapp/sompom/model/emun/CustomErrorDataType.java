package com.proapp.sompom.model.emun;

import android.text.TextUtils;

/**
 * Created by Veasna Chhom on 20/4/22.
 */
public enum CustomErrorDataType {

    PAYMENT_IN_PROCESS("paymentInProgress"),
    ITEM_REMOVED("itemRemoved"),
    ITEM_CHANGED("itemChanged"),
    NO_ITEM_AVAILABLE("noItemAvailable"),
    COUPON_CHANGED("couponChanged"),
    INVALID_COUPON("invalidCoupon"),
    TIMESLOT_NOT_AVAILABLE("slotNotAvailable"),
    DELIVERY_ADDRESS_OUT_OF_RANGE("outOfRange"),
    DELIVERY_FEE_CHANGED("deliveryFeeChanged"),
    CONTAINER_FEE_CHANGED("containerFeeChanged"),
    ADDRESS_CHANGED("addressChanged"),
    WRONG_ADDRESS_PROVINCE_SLOT("wrongAddressProvinceSlot"),
    WALLET_AMOUNT_CHANGED("walletAmountChanged"),
    UNKNOWN("unknown");

    private final String mValue;

    CustomErrorDataType(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public static CustomErrorDataType fromValue(String value) {
        for (CustomErrorDataType type : CustomErrorDataType.values()) {
            if (TextUtils.equals(type.mValue, value)) {
                return type;
            }
        }

        return UNKNOWN;
    }
}
