package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.ConciergeItemAdaptive;

public class AbsConciergeSectionResponse extends AbsConciergeModel implements ConciergeItemAdaptive {

    @SerializedName(FIELD_TITLE)
    private String mTitle;

    @SerializedName(FIELD_TYPE)
    private String mType;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }
}
