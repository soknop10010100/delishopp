package com.proapp.sompom.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhom Veasna on 6/2/22.
 */

public class ReceivePushNotificationModel {

    public static final String FIELD_TYPE = "type";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_MESSAGE = "message";

    @SerializedName(FIELD_TYPE)
    private String mType;

    @SerializedName(FIELD_TITLE)
    private String mTitle;

    @SerializedName(FIELD_MESSAGE)
    private String mMessage;

    public String getType() {
        return mType;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setType(String type) {
        mType = type;
    }
}
