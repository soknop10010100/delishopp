package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;

public class AvailableDay {

    @SerializedName("day")
    private int mDay;

    @SerializedName("start_time")
    private String mStartTime;

    @SerializedName("end_time")
    private String mEndTime;

    public int getDay() {
        return mDay;
    }

    public String getStartTime() {
        return mStartTime;
    }

    public String getEndTime() {
        return mEndTime;
    }
}
