package com.proapp.sompom.model.emun;

import android.text.TextUtils;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public enum UserType {

    CUSTOMER("customer"),
    STAFF("staff"),
    INTERVENANT("intervenant"),
    ADMINISTRATIVE("administrative"),
    GUEST("guest");

    private String mValue;

    UserType(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public static UserType fromValue(String value) {
        for (UserType type : UserType.values()) {
            if (TextUtils.equals(type.mValue, value)) {
                return type;
            }
        }

        return CUSTOMER;
    }
}
