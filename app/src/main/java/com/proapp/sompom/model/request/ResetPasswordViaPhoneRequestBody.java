package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ChhomVeasna on 12/21/21.
 */

public class ResetPasswordViaPhoneRequestBody {

    @SerializedName("firebaseToken")
    private String mFirebaseToken;

    @SerializedName("phone")
    private String mPhoneNumber;

    @SerializedName("password")
    private String mPassword;

    @SerializedName("confirmPassword")
    private String mConfirmPassword;

    public ResetPasswordViaPhoneRequestBody(String firebaseToken, String phoneNumber, String password) {
        mFirebaseToken = firebaseToken;
        mPhoneNumber = phoneNumber;
        mPassword = password;
        mConfirmPassword = password;
    }
}
