package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;

public class ConciergeOnlinePaymentProvider extends AbsConciergeModel implements Parcelable {

    @SerializedName("displayName")
    private String mDisplayName;

    @SerializedName("provider")
    private String mProvider;

    @SerializedName("accountNumber")
    private String mAccountNumber;

    @SerializedName("accountOwner")
    private String mAccountOwner;

    @SerializedName("isDirect")
    private boolean mIsDirectPayment;

    @SerializedName(FIELD_LOGO)
    private String mLogo;

    public ConciergeOnlinePaymentProvider() {
    }

    protected ConciergeOnlinePaymentProvider(Parcel in) {
        mDisplayName = in.readString();
        mProvider = in.readString();
        mAccountNumber = in.readString();
        mAccountOwner = in.readString();
        mIsDirectPayment = in.readByte() != 0;
        mLogo = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDisplayName);
        dest.writeString(mProvider);
        dest.writeString(mAccountNumber);
        dest.writeString(mAccountOwner);
        dest.writeByte((byte) (mIsDirectPayment ? 1 : 0));
        dest.writeString(mLogo);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConciergeOnlinePaymentProvider> CREATOR = new Creator<ConciergeOnlinePaymentProvider>() {
        @Override
        public ConciergeOnlinePaymentProvider createFromParcel(Parcel in) {
            return new ConciergeOnlinePaymentProvider(in);
        }

        @Override
        public ConciergeOnlinePaymentProvider[] newArray(int size) {
            return new ConciergeOnlinePaymentProvider[size];
        }
    };

    public String getProvider() {
        return mProvider;
    }

    public void setProvider(String provider) {
        mProvider = provider;
    }

    public String getAccountNumber() {
        return mAccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        mAccountNumber = accountNumber;
    }

    public String getAccountOwner() {
        return mAccountOwner;
    }

    public void setAccountOwner(String accountOwner) {
        mAccountOwner = accountOwner;
    }

    public String getLogo() {
        return mLogo;
    }

    public void setLogo(String logo) {
        mLogo = logo;
    }

    public boolean isDirectPayment() {
        return mIsDirectPayment;
    }

    public void setDirectPayment(boolean directPayment) {
        mIsDirectPayment = directPayment;
    }

    public String getDisplayName() {
        return mDisplayName;
    }
}
