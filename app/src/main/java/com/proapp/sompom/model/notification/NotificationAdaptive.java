package com.proapp.sompom.model.notification;

import android.content.Context;
import android.text.Spannable;

import com.proapp.sompom.model.concierge.ConciergeShop;

import java.util.Date;

import io.realm.RealmList;

/**
 * Created by He Rotha on 3/12/19.
 */
public interface NotificationAdaptive {

    default boolean enableMarginTop() {
        return false;
    }

    default String getId() {
        return null;
    }

    default RealmList<NotificationActor> getActors() {
        return new RealmList<>();
    }

    default RealmList<NotificationObject> getObjects() {
        return new RealmList<>();
    }

    default String getVerb() {
        return null;
    }

    default Date getPublished() {
        return null;
    }

    default String getThumbUrl() {
        return null;
    }

    default RealmList<ConciergeShop> getShops() {
        return new RealmList<>();
    }

    default boolean isNewNotification() {
        return false;
    }

    default Spannable getNotificationText(Context context) {
        return null;
    }

    default String getNotificationSubjectText(Context context) {
        return null;
    }

    default String getNotificationActionText(Context context) {
        return null;
    }

    default RedirectionNotificationData getNotificationExchangeData() {
        return null;
    }

    default boolean isValidNotificationVerb(Context context) {
        return false;
    }

    default boolean shouldMakeNotificationRedirection(Context context) {
        return false;
    }

    default NotificationObject getFirstObject() {
        if (getObjects() != null && !getObjects().isEmpty()) {
            return getObjects().get(0);
        }

        return null;
    }

    default boolean isManagedType() {
        return false;
    }
}
