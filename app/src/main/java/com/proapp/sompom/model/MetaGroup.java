package com.proapp.sompom.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.result.User;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class MetaGroup extends RealmObject implements Parcelable {

    @SerializedName("name")
    private String mName;

    @SerializedName("picture")
    private String mProfilePhoto;

    @SerializedName("participants")
    private RealmList<User> mParticipants;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getProfilePhoto() {
        return mProfilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        mProfilePhoto = profilePhoto;
    }

    public List<User> getParticipants() {
        return mParticipants;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mName);
        dest.writeString(this.mProfilePhoto);
        dest.writeTypedList(this.mParticipants);
    }

    public MetaGroup() {
    }

    protected MetaGroup(Parcel in) {
        this.mName = in.readString();
        this.mProfilePhoto = in.readString();
        ArrayList<User> userList = in.createTypedArrayList(User.CREATOR);
        if (userList != null && !userList.isEmpty()) {
            if (this.mParticipants == null) {
                this.mParticipants = new RealmList<>();
            }
            this.mParticipants.addAll(userList);
        }
    }

    public static final Creator<MetaGroup> CREATOR = new Creator<MetaGroup>() {
        @Override
        public MetaGroup createFromParcel(Parcel source) {
            return new MetaGroup(source);
        }

        @Override
        public MetaGroup[] newArray(int size) {
            return new MetaGroup[size];
        }
    };
}
