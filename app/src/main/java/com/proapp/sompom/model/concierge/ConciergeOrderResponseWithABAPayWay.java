package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.result.ConciergeOrderResponse;

public class ConciergeOrderResponseWithABAPayWay extends ConciergeOrderResponse implements Parcelable {

    @SerializedName(AbsConciergeModel.FIELD_PAYMENT_FORM)
    private PaymentForm mPaymentForm;

    @SerializedName(AbsConciergeModel.FIELD_ORDER_NEED_PAY)
    private boolean mNeedToPay;

    private ConciergeOnlinePaymentProvider mPaymentProvider;

    protected ConciergeOrderResponseWithABAPayWay(Parcel in) {
        mPaymentForm = in.readParcelable(PaymentForm.class.getClassLoader());
        mPaymentProvider = in.readParcelable(ConciergeOnlinePaymentProvider.class.getClassLoader());
        mNeedToPay = in.readInt() > 0;
    }

    public ConciergeOnlinePaymentProvider getPaymentProvider() {
        return mPaymentProvider;
    }

    public void setPaymentProvider(ConciergeOnlinePaymentProvider paymentProvider) {
        mPaymentProvider = paymentProvider;
    }

    public boolean isNeedToPay() {
        return mNeedToPay;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mPaymentForm, flags);
        dest.writeParcelable(mPaymentProvider, flags);
        dest.writeInt(mNeedToPay ? 1 : 0);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConciergeOrderResponseWithABAPayWay> CREATOR = new Creator<ConciergeOrderResponseWithABAPayWay>() {
        @Override
        public ConciergeOrderResponseWithABAPayWay createFromParcel(Parcel in) {
            return new ConciergeOrderResponseWithABAPayWay(in);
        }

        @Override
        public ConciergeOrderResponseWithABAPayWay[] newArray(int size) {
            return new ConciergeOrderResponseWithABAPayWay[size];
        }
    };

    public PaymentForm getPaymentForm() {
        return mPaymentForm;
    }
}
