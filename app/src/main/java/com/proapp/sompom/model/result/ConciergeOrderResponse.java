package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.SupportCustomErrorResponse2;
import com.proapp.sompom.model.concierge.ConciergeBasket;
import com.proapp.sompom.model.concierge.ConciergeOrderResponseCustomErrorData;
import com.proapp.sompom.model.emun.CustomErrorDataType;

import java.util.ArrayList;
import java.util.List;

public class ConciergeOrderResponse extends SupportCustomErrorResponse2 {

    @SerializedName("data")
    private ConciergeOrderResponseCustomErrorData mOrderCustomErrorData;

    public List<CustomErrorDataType> getErrorTypes() {
        if (mOrderCustomErrorData != null) {
            return mOrderCustomErrorData.getErrorTypes();
        }

        return new ArrayList<>();
    }

    public ConciergeBasket getBasket() {
        if (mOrderCustomErrorData != null) {
            return mOrderCustomErrorData.getBasket();
        }

        return null;
    }
}
