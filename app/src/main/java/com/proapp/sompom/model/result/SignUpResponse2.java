package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.SupportCustomErrorResponse;
import com.proapp.sompom.model.SupportCustomErrorResponse2;

public class SignUpResponse2 extends SupportCustomErrorResponse2 {

    @SerializedName("jwt")
    private String mAccessToken;

    @SerializedName("user")
    private User mUser;

    public String getAccessToken() {
        return mAccessToken;
    }

    public User getUser() {
        return mUser;
    }
}
