package com.proapp.sompom.model.result;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.DifferentAdaptive;
import com.proapp.sompom.model.ProductDetailAdaptive;

/**
 * Created by Or Vitovongsak on 31/8/21.
 */

public class ConciergeMenuItemOption extends AbsConciergeModel implements DifferentAdaptive<ConciergeMenuItemOption>,
        Parcelable,
        ProductDetailAdaptive {

    @SerializedName(FIELD_LABEL)
    private String mName;

    @SerializedName(FIELD_PRICE)
    private double mPrice;

    private boolean mIsSelected;

    public ConciergeMenuItemOption() {
    }

    protected ConciergeMenuItemOption(Parcel in) {
        mId = in.readString();
        mName = in.readString();
        mPrice = in.readDouble();
        mIsSelected = in.readByte() != 0;
    }

    public static final Creator<ConciergeMenuItemOption> CREATOR = new Creator<ConciergeMenuItemOption>() {
        @Override
        public ConciergeMenuItemOption createFromParcel(Parcel in) {
            return new ConciergeMenuItemOption(in);
        }

        @Override
        public ConciergeMenuItemOption[] newArray(int size) {
            return new ConciergeMenuItemOption[size];
        }
    };

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(double price) {
        this.mPrice = price;
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    public void setIsSelected(boolean isSelected) {
        mIsSelected = isSelected;
    }

    public static ConciergeMenuItemOption copyOption(ConciergeMenuItemOption option) {
        ConciergeMenuItemOption newOption = new ConciergeMenuItemOption();
        newOption.setId(option.getId());
        newOption.setName(option.getName());
        newOption.setPrice(option.getPrice());
        newOption.setIsSelected(option.isSelected());
        return newOption;
    }

    @Override
    public boolean areItemsTheSame(ConciergeMenuItemOption other) {
        return getId().matches(other.getId());
    }

    @Override
    public boolean areContentsTheSame(ConciergeMenuItemOption other) {
        return areItemsTheSame(other) &&
                TextUtils.equals(getName(), other.getName()) &&
                getPrice() == other.getPrice() &&
                isSelected() == other.isSelected();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mName);
        dest.writeDouble(mPrice);
        dest.writeByte((byte) (mIsSelected ? 1 : 0));
    }
}
