package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Veasna Chhom on 7/2/22.
 */

public class RequestGuestUserBody {

    @SerializedName("playerId")
    private String mPlayerId; //OneSignal PlayerId

    @SerializedName("pushyPlayerId")
    private String mPushyPlayerId;

    @SerializedName("device")
    private final String mDevice = "android";

    public void setPlayerId(String playerId) {
        mPlayerId = playerId;
    }

    public void setPushyPlayerId(String pushyPlayerId) {
        mPushyPlayerId = pushyPlayerId;
    }
}
