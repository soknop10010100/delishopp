package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.ConciergeItemAdaptive;
import com.proapp.sompom.model.DifferentAdaptive;

public class ConciergeShopSetting extends AbsConciergeModel
        implements Parcelable,
        ConciergeItemAdaptive,
        DifferentAdaptive<ConciergeShopSetting> {

    public static final String CURRENCY_LABEL = "currencyLabel";
    public static final String SUPPORT_CONVERSATION_ID = "supportConversationId";
    public static final String PRIMARY_SHOP_ID = "primaryShopId";
    public static final String MINIMUM_CHECKOUT_PRICE = "minimumCheckoutPrice";
    public static final String DEFAULT_ESTIMATE_DELIVERY_MINUTE = "defaultEstimateDeliveryMinute";

    @SerializedName(CURRENCY_LABEL)
    private String mCurrencyLabel;

    @SerializedName(SUPPORT_CONVERSATION_ID)
    private String mSupportConversationId;

    @SerializedName(PRIMARY_SHOP_ID)
    private String mPrimaryShopId;

    @SerializedName(MINIMUM_CHECKOUT_PRICE)
    private float mMinimumCheckoutPrice;

    @SerializedName("enableExpress")
    private boolean mEnableExpress;

    @SerializedName(DEFAULT_ESTIMATE_DELIVERY_MINUTE)
    private int mDefaultEstimateTimeDeliveryMinute;

    public ConciergeShopSetting() {
    }

    public ConciergeShopSetting(String mCurrencyLabel) {
        this.mCurrencyLabel = mCurrencyLabel;
    }

    protected ConciergeShopSetting(Parcel in) {
        mCurrencyLabel = in.readString();
        mSupportConversationId = in.readString();
        mPrimaryShopId = in.readString();
        mMinimumCheckoutPrice = in.readFloat();
        mEnableExpress = in.readInt() > 0;
        mDefaultEstimateTimeDeliveryMinute = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mCurrencyLabel);
        dest.writeString(mSupportConversationId);
        dest.writeString(mPrimaryShopId);
        dest.writeFloat(mMinimumCheckoutPrice);
        dest.writeInt(mEnableExpress ? 1 : 0);
        dest.writeInt(mDefaultEstimateTimeDeliveryMinute);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConciergeShopSetting> CREATOR = new Creator<ConciergeShopSetting>() {
        @Override
        public ConciergeShopSetting createFromParcel(Parcel in) {
            return new ConciergeShopSetting(in);
        }

        @Override
        public ConciergeShopSetting[] newArray(int size) {
            return new ConciergeShopSetting[size];
        }
    };

    public float getMinimumCheckoutPrice() {
        return mMinimumCheckoutPrice;
    }

    public String getCurrencyLabel() {
        return mCurrencyLabel;
    }

    public void setCurrencyLabel(String mCurrencyLabel) {
        this.mCurrencyLabel = mCurrencyLabel;
    }

    public String getSupportConversationId() {
        return mSupportConversationId;
    }

    public void setSupportConversationId(String mSupportConversationId) {
        this.mSupportConversationId = mSupportConversationId;
    }

    public int getDefaultEstimateTimeDeliveryMinute() {
        return mDefaultEstimateTimeDeliveryMinute;
    }

    public boolean isEnableExpress() {
        return mEnableExpress;
    }

    public String getPrimaryShopId() {
        return mPrimaryShopId;
    }

    public void setPrimaryShopId(String mPrimaryShopId) {
        this.mPrimaryShopId = mPrimaryShopId;
    }

    @Override
    public boolean areItemsTheSame(ConciergeShopSetting other) {
        return true;
    }

    @Override
    public boolean areContentsTheSame(ConciergeShopSetting other) {
        return TextUtils.equals(getCurrencyLabel(), other.getCurrencyLabel()) &&
                TextUtils.equals(getSupportConversationId(), other.getSupportConversationId()) &&
                TextUtils.equals(getPrimaryShopId(), other.getPrimaryShopId());
    }
}
