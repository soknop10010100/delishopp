package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.emun.CustomErrorDataType;

public class CustomOrderResponseErrorData {

    @SerializedName("type")
    private String mType;

    public CustomErrorDataType getDataType() {
        return CustomErrorDataType.fromValue(mType);
    }
}
