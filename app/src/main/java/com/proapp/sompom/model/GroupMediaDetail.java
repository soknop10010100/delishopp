package com.proapp.sompom.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by Veasna Chhom on 7/24/20.
 */
public class GroupMediaDetail {

    @SerializedName("type")
    private String mType;

    @SerializedName("date")
    private Date mDate;
    @SerializedName("data")
    private List<JsonObject> mData;

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public List<JsonObject> getData() {
        return mData;
    }

    public void setData(List<JsonObject> data) {
        mData = data;
    }
}
