package com.proapp.sompom.model.notification;

import android.os.Parcel;
import android.os.Parcelable;

public class RedirectionNotificationData implements Parcelable {

    private String mVerb;
    private NotificationObject mNotificationObject;

    public RedirectionNotificationData(String verb, NotificationObject notificationObject) {
        mVerb = verb;
        mNotificationObject = notificationObject;
    }

    protected RedirectionNotificationData(Parcel in) {
        mVerb = in.readString();
        mNotificationObject = in.readParcelable(NotificationObject.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mVerb);
        dest.writeParcelable(mNotificationObject, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RedirectionNotificationData> CREATOR = new Creator<RedirectionNotificationData>() {
        @Override
        public RedirectionNotificationData createFromParcel(Parcel in) {
            return new RedirectionNotificationData(in);
        }

        @Override
        public RedirectionNotificationData[] newArray(int size) {
            return new RedirectionNotificationData[size];
        }
    };

    public String getPostId() {
        if (mNotificationObject != null) {
            return mNotificationObject.getPostId();
        }

        return null;
    }

    public String getMediaId() {
        if (mNotificationObject != null) {
            return mNotificationObject.getMediaId();
        }

        return null;
    }

    public String getCommentId() {
        if (mNotificationObject != null) {
            return mNotificationObject.getCommentId();
        }

        return null;
    }

    public String getSubCommentId() {
        if (mNotificationObject != null) {
            return mNotificationObject.getSubCommentId();
        }

        return null;
    }

    public String getOrderId() {
        if (mNotificationObject != null) {
            return mNotificationObject.getOrderId();
        }

        return null;
    }

    public String getVerb() {
        return mVerb;
    }
}
