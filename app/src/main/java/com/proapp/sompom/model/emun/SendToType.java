package com.proapp.sompom.model.emun;

import android.text.TextUtils;

/**
 * Created by Veasna Chhom on 9/28/20.
 */

public enum SendToType {
    ME("me"),
    OTHER("other"),
    EVERYONE("everyone"),
    UNKNOWN("unknown");

    private String mValue;

    public String getValue() {
        return mValue;
    }

    SendToType(String value) {
        mValue = value;
    }

    public static SendToType fromValue(String value) {
        for (SendToType sendToType : values()) {
            if (TextUtils.equals(sendToType.mValue, value)) {
                return sendToType;
            }
        }

        return UNKNOWN;
    }
}
