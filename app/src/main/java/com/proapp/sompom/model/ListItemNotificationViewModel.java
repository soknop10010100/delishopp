package com.proapp.sompom.model;

import android.content.Context;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnNotificationItemClickListener;
import com.proapp.sompom.model.concierge.ConciergeShop;
import com.proapp.sompom.model.emun.HideNotificationOptionItem;
import com.proapp.sompom.model.notification.NotificationActor;
import com.proapp.sompom.model.notification.NotificationAdaptive;
import com.proapp.sompom.model.notification.NotificationHeader;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.DateTimeUtils;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

import timber.log.Timber;

/**
 * Created by nuonveyo on 7/4/18.
 */

public class ListItemNotificationViewModel extends AbsBaseViewModel
        implements PopupMenu.OnMenuItemClickListener {
    public final ObservableBoolean mIsRead = new ObservableBoolean(true);
    public final ObservableBoolean mIsJustNow = new ObservableBoolean();
    public final ObservableField<String> mThumbUrl = new ObservableField<>();
    public final ObservableField<User> mUser = new ObservableField<>();
    private final ObservableInt mShowMarginTop = new ObservableInt(View.GONE);
    private ObservableBoolean mIsNewsSection = new ObservableBoolean();

    private final Context mContext;
    private final int mPosition;
    private final OnNotificationItemClickListener mOnItemClickListener;

    private NotificationHeader mNotificationHeader;
    private NotificationAdaptive mNotificationModel;
    private boolean mIsEnableMargin;

    public ListItemNotificationViewModel(Context context,
                                         NotificationAdaptive baseNotificationModel,
                                         int position,
                                         OnNotificationItemClickListener onItemClickListener) {
        mContext = context;
        mPosition = position;
        mOnItemClickListener = onItemClickListener;
        mShowMarginTop.set(baseNotificationModel.enableMarginTop() ? View.VISIBLE : View.GONE);
        if (baseNotificationModel instanceof NotificationHeader) {
            mNotificationHeader = (NotificationHeader) baseNotificationModel;
        } else {
            mNotificationModel = baseNotificationModel;
            Timber.i("Notification: " + new Gson().toJson(mNotificationModel));
        }

        //Change the section header title color base on the header types, News or Earlier.
        if (baseNotificationModel instanceof NotificationHeader) {
            mIsNewsSection.set(((NotificationHeader) baseNotificationModel).isNewsHeader());
        }
    }

    public User parseShopFromNotification(ConciergeShop shop) {
        User user = new User();
        user.setUserProfileThumbnail(shop.getLogo());
        user.setFirstName(shop.getName());
        user.setOnline(false);
        return user;
    }

    public User parseUserFromNotificationUser(NotificationActor actor) {
        User user = new User();
        user.setId(actor.getId());
        user.setFirstName(actor.getFirstName());
        user.setLastName(actor.getLastName());
        user.setUserProfileThumbnail(actor.getUserProfile());
        user.setOnline(actor.isOnline());

        return user;
    }

    public ObservableBoolean getIsNewsSection() {
        return mIsNewsSection;
    }

    public ObservableInt getShowMarginTop() {
        return mShowMarginTop;
    }

    public String getHeaderTitle() {
        return mContext.getString(mNotificationHeader.getNotificationItem().getTitle());
    }

    public void setEnableMargin(boolean enableMargin) {
        mIsEnableMargin = enableMargin;
    }

    public int getMarginTop() {
        if (mIsEnableMargin) {
            return mContext.getResources().getDimensionPixelSize(R.dimen.space_medium);
        } else {
            return 0;
        }
    }

    public String getDateTime() {
        String date = DateTimeUtils.parse(mContext,
                mNotificationModel.getPublished().getTime(), false, null);
        if (TextUtils.equals(date, mContext.getString(R.string.common_timestamp_just_now_label))) {
            mIsJustNow.set(true);
        }
        return date;
    }

    public void onOptionButtonClick(View view) {
        Context wrapper = new ContextThemeWrapper(mContext, R.style.PopupCustomStyle);
        PopupMenu popupMenu = new PopupMenu(wrapper, view);
        popupMenu.inflate(R.menu.menu_notification);
        Menu menu = popupMenu.getMenu();
        menu.add(0,
                HideNotificationOptionItem.HideNotification.getId(),
                0,
                HideNotificationOptionItem.HideNotification.getTitle());
        menu.add(0,
                HideNotificationOptionItem.HideAllNotification.getId(),
                1,
                mContext.getString(HideNotificationOptionItem.HideAllNotification.getTitle()));
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.show();
    }

    public CharSequence getNotificationModel() {
        return mNotificationModel.getNotificationText(mContext);
    }

    public void onItemClick() {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClick(mNotificationModel, mPosition);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        //TODO: call api after server done
        if (item.getItemId() == HideNotificationOptionItem.HideNotification.getId()) {
            Timber.e("Hide");
            return true;
        } else if (item.getItemId() == HideNotificationOptionItem.HideAllNotification.getId()) {
            Timber.e("Hide all");
            return true;
        }
        return false;
    }

    @BindingAdapter("changeSectionHeaderColor")
    public static void changeSectionHeaderColor(TextView textView, boolean isNew) {
        Timber.i("changeSectionHeaderColor: isNew: " + isNew);
        if (isNew) {
            textView.setTextColor(AttributeConverter.convertAttrToColor(textView.getContext(), R.attr.notification_section_new_title));
        } else {
            textView.setTextColor(AttributeConverter.convertAttrToColor(textView.getContext(), R.attr.notification_section_earlier_title));
        }
    }
}
