package com.proapp.sompom.model.emun;

import android.text.TextUtils;

/**
 * Created by Veasna Chhom on 9/27/21.
 */
public enum ConciergeMenuItemType {

    ITEM("item"),
    COMBO("combo");

    private final String mValue;

    ConciergeMenuItemType(String type) {
        mValue = type;
    }

    public static ConciergeMenuItemType from(String value) {
        for (ConciergeMenuItemType deeplinkType : ConciergeMenuItemType.values()) {
            if (TextUtils.equals(value, deeplinkType.mValue)) {
                return deeplinkType;
            }
        }
        return ITEM;
    }

    public String getValue() {
        return mValue;
    }
}
