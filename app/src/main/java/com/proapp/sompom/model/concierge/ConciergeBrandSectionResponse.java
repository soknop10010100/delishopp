package com.proapp.sompom.model.concierge;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.DifferentAdaptive;

import java.util.List;

/**
 * Created by Chhom Veasna on 5/3/22.
 */
public class ConciergeBrandSectionResponse extends AbsConciergeSectionResponse
        implements DifferentAdaptive<ConciergeBrandSectionResponse> {

    @SerializedName(FIELD_DATA)
    private List<ConciergeBrand> mData;

    public List<ConciergeBrand> getData() {
        return mData;
    }

    public void setData(List<ConciergeBrand> data) {
        this.mData = data;
    }

    @Override
    public boolean areItemsTheSame(ConciergeBrandSectionResponse other) {
        return getData().size() == other.getData().size();
    }

    @Override
    public boolean areContentsTheSame(ConciergeBrandSectionResponse other) {
        return areDataTheSame(other.getData());
    }

    private boolean areDataTheSame(List<ConciergeBrand> other) {
        if ((getData() == null && other == null) ||
                (getData().isEmpty() && other.isEmpty())) {
            return true;
        } else if (getData() != null && other != null
                && getData().size() == other.size()) {

            for (int index = 0; index < getData().size(); index++) {
                if (!getData().get(index).areContentsTheSame(other.get(index))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
