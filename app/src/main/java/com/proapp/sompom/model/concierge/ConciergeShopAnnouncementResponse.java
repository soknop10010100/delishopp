package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.ConciergeItemAdaptive;
import com.proapp.sompom.model.DifferentAdaptive;

public class ConciergeShopAnnouncementResponse extends AbsConciergeModel
        implements Parcelable,
        ConciergeItemAdaptive,
        DifferentAdaptive<ConciergeShopAnnouncementResponse> {

    public static final String IS_SHOP_CLOSED = "isShopClosed";
    public static final String SHOP_ANNOUNCEMENT = "shopAnnouncement";

    @SerializedName(IS_SHOP_CLOSED)
    private boolean mIsShopClosed;

    @SerializedName(SHOP_ANNOUNCEMENT)
    private ConciergeShopAnnouncement mShopAnnouncement;

    public ConciergeShopAnnouncementResponse() {
    }

    public ConciergeShopAnnouncementResponse(boolean mIsShopClosed, ConciergeShopAnnouncement shopAnnouncement) {
        this.mIsShopClosed = mIsShopClosed;
        this.mShopAnnouncement = shopAnnouncement;
    }

    protected ConciergeShopAnnouncementResponse(Parcel in) {
        mIsShopClosed = in.readByte() != 0;
        mShopAnnouncement = in.readParcelable(ConciergeShopAnnouncement.class.getClassLoader());
    }

    public boolean isThereAnnouncement() {
        return mShopAnnouncement != null &&
                !TextUtils.isEmpty(mShopAnnouncement.getMessage()) &&
                mShopAnnouncement.isActive();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (mIsShopClosed ? 1 : 0));
        dest.writeParcelable(mShopAnnouncement, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConciergeShopAnnouncementResponse> CREATOR = new Creator<ConciergeShopAnnouncementResponse>() {
        @Override
        public ConciergeShopAnnouncementResponse createFromParcel(Parcel in) {
            return new ConciergeShopAnnouncementResponse(in);
        }

        @Override
        public ConciergeShopAnnouncementResponse[] newArray(int size) {
            return new ConciergeShopAnnouncementResponse[size];
        }
    };

    public boolean isShopClosed() {
        return mIsShopClosed;
    }

    public void setIsShopClosed(boolean mIsShopClosed) {
        this.mIsShopClosed = mIsShopClosed;
    }

    public ConciergeShopAnnouncement getShopAnnouncement() {
        return mShopAnnouncement;
    }

    public void setShopAnnouncement(ConciergeShopAnnouncement shopAnnouncement) {
        this.mShopAnnouncement = shopAnnouncement;
    }

    public ConciergeShopAnnouncementResponse getInstance() {
        return new ConciergeShopAnnouncementResponse(isShopClosed(), getShopAnnouncement());
    }

    @Override
    public boolean areItemsTheSame(ConciergeShopAnnouncementResponse other) {
        return true;
    }

    @Override
    public boolean areContentsTheSame(ConciergeShopAnnouncementResponse other) {
        return isShopClosed() == other.isShopClosed() &&
                getShopAnnouncement().areContentsTheSame(other.getShopAnnouncement());
    }
}
