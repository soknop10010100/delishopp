package com.proapp.sompom.model.result;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.AbsConciergeModel;

public class ConciergeDriverLocationResponse {

    @SerializedName(AbsConciergeModel.FIELD_LATITUDE)
    private Double mLatitude;

    @SerializedName(AbsConciergeModel.FIELD_LONGITUDE)
    private Double mLongitude;

    @SerializedName("currentStatus")
    private ConciergeOrderTrackingProgress mCurrentOrderStatus;

    public Double getLatitude() {
        return mLatitude;
    }

    public Double getLongitude() {
        return mLongitude;
    }

    public void setLatitude(Double latitude) {
        this.mLatitude = latitude;
    }

    public void setLongitude(Double longitude) {
        mLongitude = longitude;
    }

    public ConciergeOrderTrackingProgress getCurrentOrderStatus() {
        return mCurrentOrderStatus;
    }
}
