package com.proapp.sompom.model.notification;

public interface NotificationProfileDisplayAdaptive {

    default String getDisplayProfileUrl() {
        return null;
    }

    default String getDisplayFirstName() {
        return null;
    }

    default String getDisplayLastName() {
        return null;
    }

    default String getDisplayFullName() {
        return null;
    }
}
