package com.proapp.sompom.model.emun;

import com.proapp.sompom.R;
import com.proapp.sompom.model.GroupSenderAvatarAdaptive;

/**
 * Created by He Rotha on 7/9/18.
 */
public enum ChatBg implements GroupSenderAvatarAdaptive {
    ChatMeTop(R.drawable.layout_chat_me_top, R.drawable.layout_chat_me_top_selected),
    ChatMeMid(R.drawable.layout_chat_me_middle, R.drawable.layout_chat_me_middle_selected),
    ChatMeBottom(R.drawable.layout_chat_me_bottom, R.drawable.layout_chat_me_bottom_selected),
    ChatMeSingle(R.drawable.layout_chat_me_single, R.drawable.layout_chat_me_single_selected),
    ChatYouTop(R.drawable.layout_chat_you_top, R.drawable.layout_chat_you_top_selected),
    ChatYouMid(R.drawable.layout_chat_you_middle, R.drawable.layout_chat_you_middle_selected),
    ChatYouBottom(R.drawable.layout_chat_you_bottom, R.drawable.layout_chat_you_bottom_selected),
    ChatYouSingle(R.drawable.layout_chat_you_single, R.drawable.layout_chat_you_single_selected),
    ChatEmojiOnly(R.drawable.layout_chat_emoji_only, R.drawable.layout_chat_emoji_only);

    private int mDrawable;
    private int mSelectDrawable;

    ChatBg(int drawable, int selectDrawable) {
        mDrawable = drawable;
        mSelectDrawable = selectDrawable;
    }

    public int getDrawable(boolean isSelect) {
        if (isSelect) {
            return mSelectDrawable;
        } else {
            return mDrawable;
        }
    }

    @Override
    public boolean shouldDisplaySenderAvatar() {
        return mDrawable == R.drawable.layout_chat_you_single ||
                mDrawable == R.drawable.layout_chat_you_bottom;
    }
}
