package com.proapp.sompom.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ChhomVeasna on 1/13/21.
 */

public class ChangePasswordRequestBody {

    @SerializedName("oldPassword")
    private String mOldPassword;

    @SerializedName("password")
    private String mPassword;

    public ChangePasswordRequestBody(String oldPassword, String password) {
        mOldPassword = oldPassword;
        mPassword = password;
    }
}
