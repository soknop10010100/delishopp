package com.proapp.sompom.model.concierge;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.proapp.sompom.model.DifferentAdaptive;

import java.util.List;

public class ConciergeFeatureStoreSectionResponse extends AbsConciergeSectionResponse
        implements Parcelable, DifferentAdaptive<ConciergeFeatureStoreSectionResponse> {

    @SerializedName(FIELD_DATA)
    private List<Data> mData;

    protected ConciergeFeatureStoreSectionResponse(Parcel in) {
        mData = in.createTypedArrayList(Data.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mData);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConciergeFeatureStoreSectionResponse> CREATOR = new Creator<ConciergeFeatureStoreSectionResponse>() {
        @Override
        public ConciergeFeatureStoreSectionResponse createFromParcel(Parcel in) {
            return new ConciergeFeatureStoreSectionResponse(in);
        }

        @Override
        public ConciergeFeatureStoreSectionResponse[] newArray(int size) {
            return new ConciergeFeatureStoreSectionResponse[size];
        }
    };

    public List<Data> getData() {
        return mData;
    }

    public void setData(List<Data> data) {
        mData = data;
    }

    @Override
    public boolean areItemsTheSame(ConciergeFeatureStoreSectionResponse other) {
        return getData().size() == other.getData().size();
    }

    @Override
    public boolean areContentsTheSame(ConciergeFeatureStoreSectionResponse other) {
        return areDataTheSame(other.getData());
    }

    private boolean areDataTheSame(List<Data> other) {
        if ((getData() == null && other == null) ||
                (getData().isEmpty() && other.isEmpty())) {
            return true;
        } else if (getData() != null && other != null
                && getData().size() == other.size()) {

            for (int index = 0; index < getData().size(); index++) {
                if (!getData().get(index).areContentsTheSame(other.get(index))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public static class Data implements Parcelable, DifferentAdaptive<Data> {

        @SerializedName(FIELD_LOGO)
        private String mShopLogo;

        @SerializedName(FIELD_ID)
        private String mShopId;

        protected Data(Parcel in) {
            mShopLogo = in.readString();
            mShopId = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mShopLogo);
            dest.writeString(mShopId);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        public String getShopId() {
            return mShopId;
        }


        public void setShopId(String shopId) {
            mShopId = shopId;
        }

        public String getShopLogo() {
            return mShopLogo;
        }

        public void setShopLogo(String shopLogo) {
            mShopLogo = shopLogo;
        }

        @Override
        public boolean areItemsTheSame(Data other) {
            return TextUtils.equals(getShopId(), other.getShopId());
        }

        @Override
        public boolean areContentsTheSame(Data other) {
            return areItemsTheSame(other) &&
                    TextUtils.equals(getShopLogo(), other.getShopLogo());
        }
    }
}
