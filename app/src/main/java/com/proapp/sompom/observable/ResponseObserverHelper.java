package com.proapp.sompom.observable;

import android.content.Context;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.proapp.sompom.listener.OnCompleteListener;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import timber.log.Timber;


/**
 * Created by nuonveyo on 1/4/18.
 */
public class ResponseObserverHelper<T extends Response> extends BaseObserverErrorHandler<T> {

    public ResponseObserverHelper(Context context, Observable<T> observable) {
        super(context);
        mObservable = observable.concatMap(new ResponseHandleErrorFunc<>(mContext, false))
                .onErrorResumeNext(new HandleFailErrorFunc<>(mContext));
    }

    public ResponseObserverHelper(Context context, Observable<T> observable, boolean isSerializeErrorBody) {
        super(context);
        mObservable = observable.concatMap(new ResponseHandleErrorFunc<>(mContext, isSerializeErrorBody))
                .onErrorResumeNext(new HandleFailErrorFunc<>(mContext));
    }

    public ResponseObserverHelper(Context context,
                                  Observable<T> observable,
                                  boolean isSerializeErrorBody,
                                  boolean shouldInterceptTokenError) {
        super(context);
        mObservable = observable.concatMap(new ResponseHandleErrorFunc<>(mContext,
                isSerializeErrorBody,
                shouldInterceptTokenError))
                .onErrorResumeNext(new HandleFailErrorFunc<>(mContext));
    }

    public Disposable execute(final OnCompleteListener<T> listener) {
        return mObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listener::onComplete, throwable -> {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.recordException(throwable);
                    Timber.e(throwable);
                });
    }

    public Disposable execute() {
        return mObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(t -> {
                }, throwable -> {
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.recordException(throwable);
                    Timber.e(throwable);
                });
    }
}

