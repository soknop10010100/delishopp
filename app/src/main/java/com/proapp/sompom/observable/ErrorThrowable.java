package com.proapp.sompom.observable;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.R;

/**
 * Created by He Rotha on 10/2/17.
 */

public class ErrorThrowable extends Throwable {

    public static final int NETWORK_ERROR = 100;
    public static final int CONNECTION_TIME_OUT = 101;
    public static final int GENERAL_ERROR = 9999;

    private final int mCode;

    public static ErrorThrowable newGeneralErrorInstance(Context context) {
        return new ErrorThrowable(GENERAL_ERROR, context.getString(R.string.error_general_description));
    }

    public ErrorThrowable(int code, String message) {
        super(message);
        this.mCode = code;
    }

    public int getCode() {
        return mCode;
    }

    @Override
    public String toString() {
        return getMessage();
    }

    public enum AdditionalError {
        NONE(0, null),
        UPDATE_EXIST_STORE_NAME(101, "user.update.storename"),
        UPDATE_EXIST_EMAIL(102, "user.update.email"),
        UPDATE_EXIST_PHONE(103, "user.update.phone"),

        REGISTER_EXIST_STORE_NAME(UPDATE_EXIST_STORE_NAME.mCode, "user.storename.exists"),
        REGISTER_EXIST_EMAIL(UPDATE_EXIST_EMAIL.mCode, "user.mail.exists"),
        REGISTER_EXIST_PHONE(UPDATE_EXIST_PHONE.mCode, "user.phone.exists");

        private int mCode;
        private String mMessage;

        AdditionalError(int code, String message) {
            mCode = code;
            mMessage = message;
        }

        public static AdditionalError from(String key) {
            for (AdditionalError additionalError : AdditionalError.values()) {
                if (TextUtils.equals(additionalError.mMessage, key)) {
                    return additionalError;
                }
            }
            return NONE;
        }

        public int getCode() {
            return mCode;
        }

        public String getMessage() {
            return mMessage;
        }
    }
}
