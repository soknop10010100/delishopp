package com.proapp.sompom.observable.func;

import com.proapp.sompom.model.result.ResultList;
import com.proapp.sompom.model.Adaptive;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.HttpException;


/**
 * Created by He Rotha on 10/5/17.
 */

public class Adaptive404ErrorCodeFunc implements Function<Throwable, ObservableSource<? extends ResultList<Adaptive>>> {

    @Override
    public ObservableSource<? extends ResultList<Adaptive>> apply(Throwable throwable) {
        if (throwable instanceof HttpException) {
            int code = ((HttpException) throwable).code();
            String message = throwable.getMessage();
            ResultList<Adaptive> errorSupportModel = new ResultList<>();
            errorSupportModel.setCode(code);
            errorSupportModel.setMessage(message);
            return Observable.just(errorSupportModel);
        } else {
            return Observable.error(throwable);
        }
    }
}
