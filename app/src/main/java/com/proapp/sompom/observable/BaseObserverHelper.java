package com.proapp.sompom.observable;

import android.content.Context;

import io.reactivex.Observable;


/**
 * Created by nuonveyo on 1/4/18.
 */
public class BaseObserverHelper<T> extends BaseObserverErrorHandler<T> {

    public BaseObserverHelper(Context context, Observable<T> observable) {
        super(context);
        mObservable = observable.onErrorResumeNext(new HandleFailErrorFunc<>(context));
    }
}

