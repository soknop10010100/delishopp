package com.proapp.sompom.observable;

import android.content.Context;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.model.result.ErrorResult;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Function;
import retrofit2.Response;
import timber.log.Timber;


/**
 * Created by He Rotha on 10/2/17.
 */

public class ResponseHandleErrorFunc<T extends Response> implements Function<T, Observable<? extends T>> {

    private Context mContext;
    private boolean mIsSerializeErrorBody;
    private boolean mShouldInterceptTokenError = true;

    public ResponseHandleErrorFunc(Context context, boolean isSerializeErrorBody) {
        mContext = context;
        mIsSerializeErrorBody = isSerializeErrorBody;
    }

    public ResponseHandleErrorFunc(Context context, boolean isSerializeErrorBody, boolean shouldInterceptTokenError) {
        mContext = context;
        mIsSerializeErrorBody = isSerializeErrorBody;
        mShouldInterceptTokenError = shouldInterceptTokenError;
    }

    @Override
    public Observable<? extends T> apply(T data) {
        return Observable.create(emitter -> {
            //mean error code != 200, mean fail
            if (data.code() >= 200 && data.code() <= 299) {
                emitter.onNext(data);
                emitter.onComplete();
            } else {
                handleFail(data, emitter);
            }
        });
    }

    private void handleFail(T data, ObservableEmitter<T> emitter) {
        int errorCode = 0;
        String message = null;
        if (mIsSerializeErrorBody) {
            try {
                if (data.errorBody() == null) {
                    return;
                }
                ErrorResult result = new Gson().fromJson(data.errorBody().string(), ErrorResult.class);
                if (result.getErrorMessage() != null && result.getStatusCode() != null) {
                    errorCode = result.getStatusCode();
                    message = result.getErrorMessage();
                }
                ErrorThrowable.AdditionalError additionalError = ErrorThrowable.AdditionalError.from(result.getMessage().getError());
                if (additionalError != ErrorThrowable.AdditionalError.NONE) {
                    errorCode = additionalError.getCode();
                    message = additionalError.getMessage();
                }
            } catch (Exception e) {
                Timber.e(e);
            }
        }

        if (errorCode == 0) {
            errorCode = data.code();
            message = mContext.getString(R.string.error_general_description);
        }

        emitter.onError(new ErrorThrowable(errorCode, message));
    }
}
