package com.proapp.sompom.observable;

import android.content.Context;

import com.proapp.sompom.R;
import com.proapp.sompom.model.result.ErrorSupportModel;

import io.reactivex.Observable;
import io.reactivex.functions.Function;


/**
 * Created by He Rotha on 10/2/17.
 */

public class HandleErrorFunc<T extends ErrorSupportModel> implements Function<T, Observable<? extends T>> {
    private Context mContext;

    public HandleErrorFunc(Context context) {
        mContext = context;
    }

    @Override
    public Observable<? extends T> apply(T data) throws Exception {
        return Observable.create(emitter -> {
            int errorCode;
            String message;
            //mean error code != 200, mean fail
            if (data.getCode() >= 200 && data.getCode() <= 299) {
                emitter.onNext(data);
                emitter.onComplete();
            } else {
                errorCode = data.getCode();
                message = mContext.getString(R.string.error_general_description);
                emitter.onError(new ErrorThrowable(errorCode, message));
            }
        });
    }
}
