package com.proapp.sompom.observable;

import android.content.Context;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.utils.NetworkStateUtil;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 1/22/2020.
 */
public class BaseObserverErrorHandler<T> {

    protected Context mContext;
    protected Observable<T> mObservable;

    public BaseObserverErrorHandler(Context context) {
        mContext = context;
    }

    public Disposable execute(final OnCallbackListener<T> listener) {
        return mObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listener::onComplete, throwable -> {
                    Timber.e("throwable: " + throwable.getClass().getSimpleName() + ", Error: " + throwable.getMessage());
                    if (throwable instanceof CompositeException) {
                        for (Throwable exception : ((CompositeException) throwable).getExceptions()) {
                            Timber.e("exception: " + exception.getClass().getSimpleName() + ", Error: " + exception.getMessage());
                        }
                    }
                    FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                    crashlytics.recordException(throwable);
                    Timber.e(throwable);
                    if (throwable instanceof ErrorThrowable) {
                        listener.onFail((ErrorThrowable) throwable);
                    } else if (throwable instanceof HttpException) {
                        int code = ((HttpException) throwable).code();
                        String message = throwable.getMessage();
                        final ErrorThrowable errorThrowable = new ErrorThrowable(code, message);
                        listener.onFail(errorThrowable);
                    } else if (throwable instanceof ConnectException ||
                            throwable instanceof UnknownHostException) {
                        String errorMessage = mContext.getString(R.string.error_general_description);
                        if (!NetworkStateUtil.isNetworkAvailable(mContext)) {
                            errorMessage = mContext.getString(R.string.error_internet_connection_description);
                        }
                        final ErrorThrowable errorThrowable =
                                new ErrorThrowable(ErrorThrowable.NETWORK_ERROR, errorMessage);
                        listener.onFail(errorThrowable);
                    } else if (throwable instanceof SocketTimeoutException) {
                        final ErrorThrowable errorThrowable =
                                new ErrorThrowable(ErrorThrowable.CONNECTION_TIME_OUT,
                                        mContext.getString(R.string.error_general_description));
                        listener.onFail(errorThrowable);
                    } else {
                        final ErrorThrowable errorThrowable =
                                new ErrorThrowable(500,
                                        mContext.getString(R.string.error_general_description));
                        listener.onFail(errorThrowable);
                    }
                });
    }
}
