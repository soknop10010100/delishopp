package com.proapp.sompom.observable;

import android.content.Context;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;


/**
 * Created by He Rotha on 10/2/17.
 */

public class HandleFailErrorFunc<T> implements Function<Throwable, ObservableSource<T>> {

    private Context mContext;

    public HandleFailErrorFunc(Context context) {
        mContext = context;
    }

    @Override
    public ObservableSource<T> apply(Throwable throwable) {
        return Observable.error(throwable);
    }
}
