package com.proapp.sompom.observable;

import android.content.Context;

import com.proapp.sompom.model.result.ErrorSupportModel;

import io.reactivex.Observable;


/**
 * Created by nuonveyo on 1/4/18.
 */
public class ObserverHelper<T extends ErrorSupportModel> extends BaseObserverErrorHandler<T> {

    public ObserverHelper(Context context,
                          Observable<T> observable) {
        super(context);
        mObservable = observable.concatMap(new HandleErrorFunc<>(context))
                .onErrorResumeNext(new HandleFailErrorFunc<>(context));
    }
}

