package com.proapp.sompom;

import static com.proapp.sompom.newui.ChatActivity.REFRESH_NOTIFICATION_BADGE_ACTION;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.UiThread;
import androidx.core.app.NotificationManagerCompat;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDexApplication;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.policy.GlobalUploadPolicy;
import com.flurry.android.FlurryAgent;
import com.flurry.android.FlurryPerformance;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.connection.FileDownloadUrlConnection;
import com.pacoworks.rxpaper2.RxPaperBook;
import com.proapp.sompom.broadcast.NotificationHandlerService;
import com.proapp.sompom.broadcast.onesignal.OneSignalService;
import com.proapp.sompom.chat.call.CallingService;
import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.database.CurrencyDb;
import com.proapp.sompom.helper.AppLifecycleCallback;
import com.proapp.sompom.helper.ConversationHelper;
import com.proapp.sompom.helper.HeaderInterceptor;
import com.proapp.sompom.helper.NotificationServiceHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.injection.application.ApplicationComponent;
import com.proapp.sompom.injection.application.DaggerApplicationComponent;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.intent.newintent.HomeIntent;
import com.proapp.sompom.listener.BasketModificationListener;
import com.proapp.sompom.model.concierge.ConciergeMenuItemManipulationInfo;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observer.AppLifecycleObserver;
import com.proapp.sompom.utils.NotificationUtils;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.resourcemanager.helper.LocaleManager;
import com.sompom.proapp.core.helper.SentryHelper;
import com.sompom.pushy.service.PushyService;
import com.sompom.pushy.service.SendBroadCastHelper;
import com.sompom.videomanager.helper.ExoPlayerBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import im.crisp.client.Crisp;
import io.realm.Realm;
import timber.log.Timber;

/**
 * Created by he.rotha on 2/8/16.
 */
public class MainApplication extends MultiDexApplication {

    public static final float MAP_ZOOM = 14.0f;
    public static Locale sLocal;

    private static boolean sIsAppInBackground = true;
    private static boolean sIsHomeScreenOpened;
    private static boolean sIsCallingScreenOpened;
    private static boolean mIsSocketConnected;
    private static boolean sInExpressMode;

    private ApplicationComponent mApplicationComponent;
    private AppLifecycleCallback mAppLifecycleCallback;
    private List<String> mOpenConversationScreen = new ArrayList<>();
    private CallingService.CallServiceBinder mCallServiceBinder;
    private PushyService mPushyService;
    private SocketService.SocketBinder mSocketBinder;
    private AbsBaseActivity mForceGroundActivity;
    private List<BasketModificationListener> mBasketModificationListeners = new ArrayList<>();
    private boolean mReceivedCommandToShowSessionDialogError;

    @UiThread
    public ApplicationComponent getApplicationComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder().build();
        }
        return mApplicationComponent;
    }

    public void rebuildApplicationComponents() {
        mApplicationComponent = DaggerApplicationComponent.builder().build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sIsHomeScreenOpened = false;
        sIsCallingScreenOpened = false;
        mAppLifecycleCallback = new AppLifecycleCallback();
        registerCode401Event();
        initSentry();
        initCrispChatSupport();
        FirebaseApp.initializeApp(this);
        initFlurryEvent();
        handleClearDataForFreshStart();

        //Register app Lifecycle observer
        AppLifecycleObserver appLifecycleObserver = new AppLifecycleObserver(new AppLifecycleObserver.
                AppLifecycleObserverCallback() {
            @Override
            public void onEnterBackground() {
                Timber.i("App onEnterBackground");
                sIsAppInBackground = true;
            }

            @Override
            public void onEnterForeground() {
                Timber.i("App onEnterForeground");
                if (sIsHomeScreenOpened && mSocketBinder != null) {
                    mSocketBinder.checkToReconnectSocketIfNecessary();
                }
                sIsAppInBackground = false;
                SendBroadCastHelper.verifyAndSendBroadCast(getApplicationContext(),
                        new Intent(REFRESH_NOTIFICATION_BADGE_ACTION));
            }
        });
        ProcessLifecycleOwner.get().getLifecycle().addObserver(appLifecycleObserver);

        //init Timber for logging
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        //init Realm database
        Realm.init(this);
        initNotificationService();
        startSMSRetrieverClient();

        //register lifecycle to track activity lifecycle, and to detect application pause or resume
        this.registerActivityLifecycleCallbacks(mAppLifecycleCallback);

        //Initialize cloudinary
        Map config = new HashMap();
        config.put("cloud_name", getString(R.string.cloudinary_name));
        config.put("api_key", getString(R.string.cloudinary_api_key));
        config.put("api_secret", getString(R.string.cloudinary_api_secret));
        MediaManager.init(this, config);
        MediaManager.get().setGlobalUploadPolicy(
                new GlobalUploadPolicy.Builder()
                        .maxRetries(0)
                        .build());

        ExoPlayerBuilder.initApp(this);
        registerCheckListNotificationReceiver();
        registerDismissNotificationReceiver();
        registerReceiveNewSupportGroupConversationReceiver();
        bindCallService();
        /**
         * just for cache Application's Context, and ':filedownloader' progress will NOT be launched
         * by below code, so please do not worry about performance.
         * @see FileDownloader#init(Context)
         */
        FileDownloader.setupOnApplicationOnCreate(this)
                .connectionCreator(new FileDownloadUrlConnection
                        .Creator(new FileDownloadUrlConnection.Configuration()
                        .connectTimeout(15_000) // set connection timeout.
                        .readTimeout(15_000) // set read timeout.
                ))
                .commit();
        Timber.i("onCreate");

        // Init RxPaper, a NoSQL data storage
        RxPaperBook.init(this);
    }

    public static boolean isInExpressMode() {
        return sInExpressMode;
    }

    public static void setInExpressMode(boolean inExpressMode) {
        sInExpressMode = inExpressMode;
    }

    public boolean isActivityRunning(String activitySimpleName) {
        return mAppLifecycleCallback.isActivityRunning(activitySimpleName);
    }

    public void setReceivedCommandToShowSessionDialogError(boolean receivedCommandToShowSessionDialogError) {
        mReceivedCommandToShowSessionDialogError = receivedCommandToShowSessionDialogError;
    }

    public boolean getReceivedCommandToShowSessionDialogError() {
        return mReceivedCommandToShowSessionDialogError;
    }

    public boolean isHomeActivityRunning() {
        return mAppLifecycleCallback.isHomeActivityRunning();
    }

    public int getHomeActivityCount() {
        return mAppLifecycleCallback.getHomeActivityCount();
    }

    public void handleClearDataForFreshStart() {
        SharedPrefUtils.setUserSelectedLocation(this, null);
        SharedPrefUtils.clearPreloadedConversation(this);
    }

    public boolean isSocketStateConnected() {
        return mSocketBinder != null && mSocketBinder.isSocketConnected();
    }

    public AbsBaseActivity getForceGroundActivity() {
        return mForceGroundActivity;
    }

    public void setForceGroundActivity(AbsBaseActivity forceGroundActivity) {
        mForceGroundActivity = forceGroundActivity;
    }

    public void checkToClearForceGroundActivity(AbsBaseActivity forceGroundActivity) {
        if (mForceGroundActivity != null && mForceGroundActivity.equals(forceGroundActivity)) {
            mForceGroundActivity = null;
        }
    }

    private void bindSocketService() {
        bindService(new Intent(this, SocketService.class),
                new ServiceConnection() {
                    @Override
                    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                        Timber.i("bindSocketService: onServiceConnected");
                        if (iBinder instanceof SocketService.SocketBinder) {
                            mSocketBinder = (SocketService.SocketBinder) iBinder;
                        }
                    }

                    @Override
                    public void onServiceDisconnected(ComponentName componentName) {

                    }
                },
                BIND_AUTO_CREATE);
    }

    public void checkToSendDeliveredMessage(Chat receivedChat) {
        if (mSocketBinder != null) {
            mSocketBinder.checkToSendDeliveredMessage(receivedChat, true);
        }
    }

    public void performReceiveSocketMessageManuallyForChatFromNotification(Chat chat) {
        if (mSocketBinder != null) {
            mSocketBinder.performReceiveSocketMessageManuallyForChatFromNotification(chat);
        }
    }

    public void checkToReconnectSocketIfNecessary() {
        if (mSocketBinder != null) {
            mSocketBinder.checkToReconnectSocketIfNecessary();
        }
    }

    private void initSentry() {
        SentryHelper.initSentry(this);
        if (SharedPrefUtils.isLogin(this)) {
            com.proapp.sompom.helper.SentryHelper.setUser(SharedPrefUtils.getUser(this));
        }
    }

    private void initCrispChatSupport() {
        Crisp.configure(getApplicationContext(), getString(R.string.crisp_website_id));
        UserHelper.setCrispUserProfile(SharedPrefUtils.getUser(this));
    }

    private void initNotificationService() {
        if (NotificationServiceHelper.isPushyProviderEnabled(getApplicationContext())) {
            initPushyNotificationService();
        } else {
            initOneSignalNotificationService();
        }
    }

    private void initOneSignalNotificationService() {
        OneSignalService.initOneSignalNotificationService(getApplicationContext());
    }

    private void initPushyNotificationService() {
        mPushyService = new PushyService(getApplicationContext(), new PushyService.PushyServiceListener() {
            @Override
            public void onRegisterFinished(String token, Exception error) {
                Timber.i("onRegisterFinished: " + token);
                if (error != null) {
                    SentryHelper.logSentryError(error);
                }
            }

            @Override
            public void onReceivedNotification(JSONObject additionalData) {
                Timber.i("onReceivedNotification: " + additionalData);
                if (additionalData != null) {
                    new NotificationHandlerService().remoteNotificationReceived(getApplicationContext(),
                            additionalData);
                }
            }
        });
        mPushyService.init();
    }

    private void startSMSRetrieverClient() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        Task<Void> task = client.startSmsRetriever();
        task.addOnSuccessListener(aVoid -> Timber.i("Successfully started retriever, expect broadcast intent"));

        task.addOnFailureListener(e -> Timber.e("Failed to start retriever, inspect Exception for more details: " + e.getMessage()));
    }

    private void initFlurryEvent() {
        new FlurryAgent.Builder()
                .withDataSaleOptOut(false) //CCPA - the default value is false
                .withCaptureUncaughtExceptions(true)
                .withIncludeBackgroundSessionsInMetrics(true)
                .withLogLevel(Log.VERBOSE)
                .withPerformanceMetrics(FlurryPerformance.ALL)
                .build(this, getString(R.string.flurry_key));
    }

    public static boolean isAppInBackground() {
        return sIsAppInBackground;
    }

    @Override
    protected void attachBaseContext(Context base) {
        sLocal = CurrencyDb.getLocale(base);
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleManager.setLocale(this);
    }

    private void registerCode401Event() {
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive: HTTP_UNAUTHORIZED_EVEN");
                if (mForceGroundActivity != null) {
                    mForceGroundActivity.showSessionDialog();
                }
            }
        };

        registerReceiver(receiver, new IntentFilter(HeaderInterceptor.HTTP_UNAUTHORIZED_EVEN));
    }

    private void registerCheckListNotificationReceiver() {
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive: Checklist alert action.");
                int notificationId = intent.getIntExtra(NotificationUtils.NOTIFICATION_ID,
                        0);
                Timber.i("notificationId: %s", notificationId);
                NotificationManagerCompat.from(context).cancel(notificationId);
            }
        };

        registerReceiver(receiver, new IntentFilter(NotificationUtils.REJECTION_SYNCHRONIZE));
    }

    private void registerDismissNotificationReceiver() {
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive: DISMISS_NOTIFICATION_ACTION.");
                int notificationId = intent.getIntExtra(NotificationUtils.NOTIFICATION_ID,
                        0);
                Timber.i("notificationId: %s", notificationId);
                NotificationManagerCompat.from(context).cancel(notificationId);
            }
        };

        registerReceiver(receiver, new IntentFilter(NotificationUtils.DISMISS_NOTIFICATION_ACTION));
    }

    private void registerReceiveNewSupportGroupConversationReceiver() {
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive: registerReceiveNewSupportGroupConversationReceiver");
                String supportId = intent.getStringExtra(ConversationHelper.NEW_SUPPORT_GROUP_ID);
                Timber.i("supportId: %s", supportId);
                if (!TextUtils.isEmpty(supportId) && mSocketBinder != null) {
                    mSocketBinder.startSubscribeAllGroupChannel(Collections.singletonList(supportId));
                }
            }
        };

        registerReceiver(receiver, new IntentFilter(ConversationHelper.RECEIVE_NEW_SUPPORT_GROUP_EVENT));
    }

    private void bindCallService() {
        getApplicationContext()
                .bindService(new Intent(this, CallingService.class), new ServiceConnection() {
                    @Override
                    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                        Timber.i("onServiceConnected call service");
                        if (iBinder instanceof CallingService.CallServiceBinder) {
                            mCallServiceBinder = (CallingService.CallServiceBinder) iBinder;
                        }
                    }

                    @Override
                    public void onServiceDisconnected(ComponentName componentName) {
                        Timber.e("onServiceDisconnected call service");
                    }
                }, BIND_AUTO_CREATE);
    }

    public static boolean isIsHomeScreenOpened() {
        return sIsHomeScreenOpened;
    }

    public static void setIsHomeScreenOpened(boolean isHomeScreenOpened) {
        sIsHomeScreenOpened = isHomeScreenOpened;
    }

    public static void setIsCallingScreenOpened(boolean isCallingScreenOpened) {
        sIsCallingScreenOpened = isCallingScreenOpened;
    }

    public static boolean isIsCallingScreenOpened() {
        return sIsCallingScreenOpened;
    }

    public void addOpenConversationScreen(String conversationId) {
        mOpenConversationScreen.add(conversationId);
    }

    public void removeOpenConversationScreen(String conversationId) {
        mOpenConversationScreen.remove(conversationId);
    }

    public boolean isConversationScreenAlreadyOpen(String conversationId) {
        return mOpenConversationScreen.contains(conversationId);
    }

    public static void setSocketConnected(boolean isSocketConnected) {
        mIsSocketConnected = isSocketConnected;
    }

    public void clearAllOpenConversations() {
        mOpenConversationScreen.clear();
    }

    public void startFreshHomeScreen(HomeIntent.Redirection redirection) {
        HomeIntent homeIntent = new HomeIntent(getApplicationContext(), redirection);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }

    public void startChatScreen(String conversationId) {
        Conversation conversationById = ConversationDb.getConversationById(this, conversationId);
        if (conversationById != null) {
            ChatIntent chatIntent = new ChatIntent(getApplicationContext(), conversationById);
            chatIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(chatIntent);
        }
    }

    public CallingService.CallServiceBinder getSinchBinder() {
        return mCallServiceBinder;
    }

    public boolean isCallViewInFloatingMode() {
        return mCallServiceBinder != null && mCallServiceBinder.isCallViewInFloatingMode();
    }

    public boolean isCallInProgressNow() {
        return mCallServiceBinder != null && mCallServiceBinder.isCallInProgressNow();
    }

    public void endProgressCall() {
        if (isCallInProgressNow()) {
            mCallServiceBinder.endCallManually();
        }
    }

    public void checkToRemoveEmptyConversationIfNecessary(String conversationId) {
        new Handler().post(() -> ConversationDb.checkToRemoveEmptyConversationIfNecessary(this, conversationId));
    }

    public void addBasketModificationListener(BasketModificationListener listener) {
        mBasketModificationListeners.add(listener);
    }

    public void removeBasketModificationListener(BasketModificationListener listener) {
        mBasketModificationListeners.remove(listener);
    }

    public void fireBasketModificationListener(SupportConciergeCustomErrorResponse response,
                                               String errorMessage,
                                               ConciergeMenuItemManipulationInfo manipulationInfo) {
        for (BasketModificationListener listener : mBasketModificationListeners) {
            if (listener != null) {
                listener.onBasketModificationError(response, errorMessage, manipulationInfo);
            }
        }
    }
}