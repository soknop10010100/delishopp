package com.proapp.sompom.adapter.newadapter;

import android.content.Context;
import android.view.ViewGroup;

import androidx.recyclerview.widget.DiffUtil;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.helper.ConversationDiffCallback;
import com.proapp.sompom.listener.ConversationDataAdaptive;
import com.proapp.sompom.model.result.TelegramChatRequest;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemTelegramChatRequestViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class TelegramChatRequestAdapter extends RefreshableAdapter<ConversationDataAdaptive, BindingViewHolder> {

    private final ListItemTelegramChatRequestViewModel.Listener mItemListener;
    private final Context mContext;

    public TelegramChatRequestAdapter(Context context,
                                      List<ConversationDataAdaptive> data,
                                      ListItemTelegramChatRequestViewModel.Listener listener) {
        super(data);
        mContext = context;
        mItemListener = listener;
        setCanLoadMore(false);
    }

    public void refreshData(List<ConversationDataAdaptive> requests) {
        if (mDatas.isEmpty()) {
            mDatas = requests;
            notifyDataSetChanged();
        } else {
            final ConversationDiffCallback diffCallback = new ConversationDiffCallback(mContext, mDatas, requests);
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
            mDatas.clear();
            mDatas.addAll(requests);
            diffResult.dispatchUpdatesTo(this);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_telegram_chat_request).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        final Context context = holder.getBinding().getRoot().getContext();
        final TelegramChatRequest chatRequest = (TelegramChatRequest) mDatas.get(position);
        ListItemTelegramChatRequestViewModel viewModel = new ListItemTelegramChatRequestViewModel(context, chatRequest, mItemListener);
        holder.getBinding().setVariable(BR.viewModel, viewModel);
    }
}
