package com.proapp.sompom.adapter.newadapter;

import android.text.TextUtils;
import android.view.ViewGroup;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.databinding.ListItemLoadCommentDirectionBinding;
import com.proapp.sompom.databinding.ListItemMainCommentBinding;
import com.proapp.sompom.databinding.ListItemMainCommentGifBinding;
import com.proapp.sompom.databinding.ListItemMainCommentImageBinding;
import com.proapp.sompom.databinding.ListItemMainCommentLinkBinding;
import com.proapp.sompom.listener.OnCommentItemClickListener;
import com.proapp.sompom.listener.OnLoadPreviousCommentClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.LoadCommentDirection;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemLoadCommentRedirectionViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemMainCommentViewModel;

import java.util.List;

/**
 * Created by he.rotha on 3/4/16.
 */
public class CommentAdapter extends RefreshableAdapter<Adaptive, BindingViewHolder> {

    private static final int LOAD_PREVIOUS_ITEM = 0X0200;
    private static final int MAIN_TEXT_ITEM = 0X0201;
    private static final int MAIN_GIF_ITEM = 0X0202;
    private static final int MAIN_IMAGE_ITEM = 0X0203;
    private static final int MAIN_LINK_ITEM = 0X0204;

    final OnCommentItemClickListener mOnCommentItemClickListener;
    protected ListItemLoadCommentRedirectionViewModel mItemLoadMorePreviousCommentViewModel;
    private final OnLoadPreviousCommentClickListener mOnLoadPreviousItemClickListener;
    private boolean mIsReplyComment;
    private boolean mIsCanLoadNext;

    public CommentAdapter(List<Adaptive> comments,
                          OnCommentItemClickListener onCommentItemClickListener,
                          OnLoadPreviousCommentClickListener onLoadPreviousItemClickListener) {
        super(comments);
        mOnLoadPreviousItemClickListener = onLoadPreviousItemClickListener;
        mOnCommentItemClickListener = onCommentItemClickListener;
        setCanLoadMore(false);
    }

    public boolean isCanLoadNext() {
        return mIsCanLoadNext;
    }

    public void setCanLoadNext(boolean canLoadNext) {
        mIsCanLoadNext = canLoadNext;
    }

    public void invokeLoadPreviousComment() {
        if (mItemLoadMorePreviousCommentViewModel != null) {
            mItemLoadMorePreviousCommentViewModel.onItemClick();
        }
    }

    public boolean canInvokeLoadPreviousComment() {
        return mItemLoadMorePreviousCommentViewModel != null;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == MAIN_GIF_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_main_comment_gif).build();
        } else if (viewType == MAIN_IMAGE_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_main_comment_image).build();
        } else if (viewType == LOAD_PREVIOUS_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_load_comment_direction).build();
        } else if (viewType == MAIN_LINK_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_main_comment_link).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_main_comment).build();
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListItemLoadCommentDirectionBinding) {
            LoadCommentDirection loadCommentDirection = (LoadCommentDirection) mDatas.get(position);
            ListItemLoadCommentRedirectionViewModel viewModel = new ListItemLoadCommentRedirectionViewModel(holder.getContext(),
                    mIsReplyComment,
                    position,
                    mOnLoadPreviousItemClickListener,
                    loadCommentDirection);
            holder.setVariable(BR.viewModel, viewModel);
            if (loadCommentDirection.isLoadPreviousCommentMode()) {
                mItemLoadMorePreviousCommentViewModel = viewModel;
            }
        } else if (holder.getBinding() instanceof ListItemMainCommentGifBinding
                || holder.getBinding() instanceof ListItemMainCommentImageBinding
                || holder.getBinding() instanceof ListItemMainCommentBinding ||
                holder.getBinding() instanceof ListItemMainCommentLinkBinding) {
            ListItemMainCommentViewModel commentViewModel = new ListItemMainCommentViewModel(holder.getContext(),
                    position,
                    (Comment) mDatas.get(position),
                    mOnCommentItemClickListener);
            holder.setVariable(BR.viewModel, commentViewModel);
            if (mIsReplyComment) {
                commentViewModel.mIsShowReplayLayout.set(false);
            }
        }
    }

    public int findItemPosition(String checkId) {
        if (!TextUtils.isEmpty(checkId)) {
            for (int i = 0; i < getDatas().size(); i++) {
                if (TextUtils.equals(checkId, getDatas().get(i).getId())) {
                    return i;
                }
            }
        }

        return -1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= 0 && position < mDatas.size()) {
            if (mDatas.get(position) instanceof Comment &&
                    ((Comment) mDatas.get(position)).getMetaPreview() != null &&
                    !((Comment) mDatas.get(position)).getMetaPreview().isEmpty()) {
                return MAIN_LINK_ITEM;
            } else if (mDatas.get(position) instanceof LoadCommentDirection) {
                return LOAD_PREVIOUS_ITEM;
            } else if (((Comment) mDatas.get(position)).getMedia() == null || ((Comment) mDatas.get(position)).getMedia().isEmpty()) {
                return MAIN_TEXT_ITEM;
            } else {
                if (((Comment) mDatas.get(position)).getMedia().get(0).getType() == MediaType.TENOR_GIF) {
                    return MAIN_GIF_ITEM;
                } else if (((Comment) mDatas.get(position)).getMedia().get(0).getType() == MediaType.IMAGE) {
                    return MAIN_IMAGE_ITEM;
                }
            }
        }
        return super.getItemViewType(position);
    }

    public void addLatestComment(Comment comment) {
        this.mDatas.add(comment);
        if (!canLoadMore()) {
            notifyItemInserted(mDatas.size() - 1);
        } else {
            notifyItemInserted(mDatas.size());
        }
    }

    public void notifyItemChanged(int position, Comment comment) {
        mDatas.set(position, comment);
        notifyItemChanged(position);
    }

    void setReplyComment() {
        mIsReplyComment = true;
    }

    public void removeInstanceComment() {
        for (int i = mDatas.size() - 1; i >= 0; i--) {
            if (mDatas.get(i) instanceof Comment) {
                if (TextUtils.isEmpty(((Comment) mDatas.get(i)).getId())) {
                    mDatas.remove(i);
                    notifyItemRemoved(i);
                    break;
                }
            }
        }
    }

    public void checkToRemoveLoadPreviousItem(boolean isLoadMore,
                                              LoadCommentDirection previousComment,
                                              int position) {
        if (!isLoadMore) {
            mItemLoadMorePreviousCommentViewModel = null;
            notifyItemRemove(position);
        } else {
            notifyItemChanged(position, previousComment);
        }
    }

    public void removeLoadNextItem(int position) {
        if (isCanLoadNext()) {
            notifyItemRemove(position);
        }
    }

    public void checkToAddPreviousLoadedComment(boolean isHasMorePrevious,
                                                LoadCommentDirection previousComment,
                                                int position,
                                                List<Comment> comments) {
        if (!isHasMorePrevious) {
            mItemLoadMorePreviousCommentViewModel = null;
            notifyItemRemove(position);
            addLoadedPreviousOrNextComment(position, comments);
        } else {
            notifyItemChanged(position, previousComment);
            addLoadedPreviousOrNextComment(position + 1, comments);
        }
    }

    public void checkToAddNextLoadedComment(boolean isHasMoreNext,
                                            LoadCommentDirection previousComment,
                                            int position,
                                            List<Comment> comments) {
        if (!isHasMoreNext) {
            notifyItemRemove(position);
            addLoadedPreviousOrNextComment(position, comments);
        } else {
            notifyItemChanged(position, previousComment);
            addLoadedPreviousOrNextComment(position, comments);
        }
        setCanLoadNext(isHasMoreNext);
    }

    public void notifyItemRemove(int position) {
        mDatas.remove(position);
        notifyItemRemoved(position);
    }

    public void addLoadedPreviousOrNextComment(int addToIndex, List<Comment> commentList) {
        if (commentList != null && !commentList.isEmpty()) {
            mDatas.addAll(addToIndex, commentList);
            notifyItemRangeInserted(addToIndex, commentList.size());
        }
    }

    public void removeAllCommentInReplyOrTimelineDetailScreen(boolean isFromTimelineDetailScreen) {
        if (!isFromTimelineDetailScreen) {
            setCanLoadNext(false);
            mItemLoadMorePreviousCommentViewModel = null;
            mDatas.clear();
            notifyDataSetChanged();
        } else {
            //Keep the first item
            setCanLoadNext(false);
            int itemCount = getItemCount();
            mItemLoadMorePreviousCommentViewModel = null;
            if (mDatas.size() > 1) {
                mDatas.subList(1, mDatas.size()).clear();
            }
            notifyItemRangeRemoved(1, itemCount - 1);
        }
    }
}
