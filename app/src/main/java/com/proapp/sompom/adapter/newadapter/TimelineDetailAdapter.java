package com.proapp.sompom.adapter.newadapter;

import android.Manifest;
import android.graphics.Point;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ListItemTimelineDetailBinding;
import com.proapp.sompom.databinding.ListItemTimelineDetailFileBinding;
import com.proapp.sompom.databinding.ListItemTimelineDetailMediaBinding;
import com.proapp.sompom.databinding.ListItemTimelineDetailPreviewBinding;
import com.proapp.sompom.databinding.ListItemTimelineDetailShareBinding;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.listener.OnCommentItemClickListener;
import com.proapp.sompom.listener.OnDetailShareItemCallback;
import com.proapp.sompom.listener.OnFloatingVideoClickedListener;
import com.proapp.sompom.listener.OnLoadPreviousCommentClickListener;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.CommonWallAdapter;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.SharedProduct;
import com.proapp.sompom.model.SharedTimeline;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.emun.ViewType;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.FullScreenMediaDialog;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;
import com.proapp.sompom.utils.ToastUtil;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewholder.SupportPlacePreviewViewHolder;
import com.proapp.sompom.viewholder.TimelineViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.AbsTimelineDetailItemViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemDefaultTimelineDetailViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemTimelineDetailPreviewViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemTimelineDetailShareViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemTimelineFileDetailViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemTimelineMediaDetailViewModel;
import com.proapp.sompom.widget.lifestream.CollageView;
import com.proapp.sompom.widget.lifestream.MediaLayout;
import com.sompom.baseactivity.PermissionCheckHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by nuonveyo on 7/12/18.
 */

public class TimelineDetailAdapter extends CommentAdapter implements CommonWallAdapter {

    private static final int MEDIA_ITEM = 86;
    private static final int NORMAL_ITEM = 87;
    private static final int SHARE_ITEM = 88;
    private static final int TIMELINE_FILE = 0x901;
    private static final int TIMELINE_MEDIA_PREVIEW = 0x902;

    private final OnTimelineItemButtonClickListener mOnTimelineItemButtonClickListener;
    private final OnFloatingVideoClickedListener mOnFloatingVideoClickedListener;
    private final AbsBaseActivity mContext;
    private final ApiService mApiService;
    private boolean mCheckLikeComment;
    private final OnDetailShareItemCallback mOnLikeButtonClickListener;
    private WallStreetAdaptive mPost;
    private Map<String, AbsTimelineDetailItemViewModel> mItemViewModelMap = new HashMap<>();
    private TimelineDetailAdapterListener mAdapterListener;
    private boolean mIsInPreviewPostMode;
    private boolean mIsShowCommentForSingleItemPost;
    private List<MediaLayout> mMediaLayoutList = new ArrayList<>();

    public TimelineDetailAdapter(AbsBaseActivity context,
                                 WallStreetAdaptive post,
                                 boolean checkLikeComment,
                                 ApiService apiService,
                                 List<Adaptive> list,
                                 OnTimelineItemButtonClickListener listener,
                                 OnFloatingVideoClickedListener onFloatingVideoClickedListener,
                                 OnCommentItemClickListener onCommentItemClickListener,
                                 OnDetailShareItemCallback onClickListener,
                                 OnLoadPreviousCommentClickListener onLoadPreviousItemClickListener,
                                 TimelineDetailAdapterListener adapterListener,
                                 boolean isInPreviewPostMode,
                                 boolean isShowCommentForSingleItemPost) {
        super(list, onCommentItemClickListener, onLoadPreviousItemClickListener);
        mIsInPreviewPostMode = isInPreviewPostMode;
        mIsShowCommentForSingleItemPost = isShowCommentForSingleItemPost;
        mAdapterListener = adapterListener;
        mPost = post;
        mContext = context;
        mApiService = apiService;
        mCheckLikeComment = checkLikeComment;
        mOnTimelineItemButtonClickListener = listener;
        mOnFloatingVideoClickedListener = onFloatingVideoClickedListener;
        mOnLikeButtonClickListener = onClickListener;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateView(@NonNull ViewGroup parent, int viewType) {
        if (viewType == NORMAL_ITEM) {
            return new SupportPlacePreviewViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                    R.layout.list_item_timeline_detail, parent, false),
                    mContext);
        } else if (viewType == MEDIA_ITEM) {
            @LayoutRes int layout = R.layout.list_item_timeline_detail_media;

            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final ViewDataBinding dataBinding = DataBindingUtil.inflate(inflater, layout, parent, false);
            return new TimelineViewHolder(dataBinding);
        } else if (viewType == SHARE_ITEM) {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final ViewDataBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.list_item_timeline_detail_share, parent, false);
            return new TimelineViewHolder(dataBinding);
        } else if (viewType == TIMELINE_FILE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_timeline_detail_file).build();
        } else if (viewType == TIMELINE_MEDIA_PREVIEW) {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final ViewDataBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.list_item_timeline_detail_preview, parent, false);
            TimelineViewHolder holder = new TimelineViewHolder(dataBinding);
            ListItemTimelineDetailPreviewBinding binding = (ListItemTimelineDetailPreviewBinding) holder.getBinding();
            CollageView collageView = binding.mediaLayouts;
            ViewType v = ViewType.fromValue(mDatas.get(0).getTimelineViewType().getViewType());
            collageView.setOrientation(v.getOrientation());
            collageView.setNumberOfItem(v.getNumberOfItem());
            collageView.setFormat(v.getFormat());
            collageView.startGenerateView();
            return holder;
        }

        return super.onCreateView(parent, viewType);
    }

    public void update(Adaptive adaptive) {
        Timber.e("start update %s", adaptive.getId());
        for (int i = 0; i < mDatas.size(); i++) {
            if (TextUtils.equals(mDatas.get(i).getId(), adaptive.getId())) {
                updateItem(i, adaptive);
                break;
            }
        }

        Timber.e("stop update ");
    }

    public void updateCommentCounter(Adaptive adaptive) {
        AbsTimelineDetailItemViewModel viewModel = mItemViewModelMap.get(adaptive.getId());
        if (viewModel != null &&
                adaptive instanceof WallStreetAdaptive &&
                ((WallStreetAdaptive) adaptive).getContentStat() != null) {
            viewModel.onUpdateCommentCounter(((WallStreetAdaptive) adaptive).getContentStat().getTotalComments());
        }
    }

    public void updateUnreadCommentCounter(Adaptive adaptive) {
        AbsTimelineDetailItemViewModel viewModel = mItemViewModelMap.get(adaptive.getId());
        if (viewModel != null &&
                adaptive instanceof WallStreetAdaptive &&
                ((WallStreetAdaptive) adaptive).getContentStat() != null) {
            viewModel.updateUnreadCommentCounter(((WallStreetAdaptive) adaptive).getContentStat().getUnreadCommentCount());
        }
    }

    @Override
    public void onBindData(@NonNull BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListItemTimelineDetailBinding) {
            LifeStream lifeStream = (LifeStream) mDatas.get(position);
            ListItemDefaultTimelineDetailViewModel viewModel = new ListItemDefaultTimelineDetailViewModel(mContext,
                    holder,
                    mPost,
                    mCheckLikeComment,
                    lifeStream,
                    getDataManager(),
                    position,
                    mOnTimelineItemButtonClickListener,
                    mOnLikeButtonClickListener);
            viewModel.setBottomLineVisibility(shouldDisplayBottomLine(position));
            holder.setVariable(BR.viewModel, viewModel);
            if (viewModel.isShouldRenderPlacePreview() && holder instanceof SupportPlacePreviewViewHolder) {
                ((SupportPlacePreviewViewHolder) holder).bindPlace((LifeStream) mDatas.get(position));
            }
            mItemViewModelMap.put(mDatas.get(position).getId(), viewModel);
        } else if (holder.getBinding() instanceof ListItemTimelineDetailShareBinding) {
            ListItemTimelineDetailShareViewModel viewModel = new ListItemTimelineDetailShareViewModel(mContext,
                    mPost,
                    holder,
                    getDataManager(),
                    (WallStreetAdaptive) mDatas.get(position),
                    mOnTimelineItemButtonClickListener,
                    mOnLikeButtonClickListener);
            viewModel.setBottomLineVisibility(shouldDisplayBottomLine(position));
            holder.setVariable(BR.viewModel, viewModel);
            mItemViewModelMap.put(mDatas.get(position).getId(), viewModel);
        } else if (holder.getBinding() instanceof ListItemTimelineDetailMediaBinding) {
            Media media = (Media) mDatas.get(position);
            ListItemTimelineMediaDetailViewModel viewModel = new ListItemTimelineMediaDetailViewModel(mContext,
                    mPost,
                    mCheckLikeComment,
                    media,
                    getDataManager(),
                    position,
                    mOnTimelineItemButtonClickListener);

            ListItemTimelineDetailMediaBinding binding = (ListItemTimelineDetailMediaBinding) holder.getBinding();
            setMediaLayout(holder, binding.mediaLayouts, media, getMediaListFromAdaptiveList());
            viewModel.setBottomLineVisibility(shouldDisplayBottomLine(position));
            holder.setVariable(BR.viewModel, viewModel);
            mItemViewModelMap.put(mDatas.get(position).getId(), viewModel);
        } else if (holder.getBinding() instanceof ListItemTimelineDetailFileBinding) {
            LifeStream lifeStream = (LifeStream) mDatas.get(position);
            ListItemTimelineFileDetailViewModel viewModel = new ListItemTimelineFileDetailViewModel(mContext,
                    mPost,
                    lifeStream,
                    getDataManager(),
                    position,
                    mOnTimelineItemButtonClickListener);
            viewModel.setBottomLineVisibility(shouldDisplayBottomLine(position));
            holder.setVariable(BR.viewModel, viewModel);
            mItemViewModelMap.put(mDatas.get(position).getId(), viewModel);
        } else if (holder.getBinding() instanceof ListItemTimelineDetailPreviewBinding) {
            Adaptive adaptive = mDatas.get(position);
            ListItemTimelineDetailPreviewViewModel viewModel = new ListItemTimelineDetailPreviewViewModel(mContext,
                    mPost,
                    mCheckLikeComment,
                    adaptive,
                    getDataManager(),
                    position,
                    mOnTimelineItemButtonClickListener);
            ListItemTimelineDetailPreviewBinding binding = (ListItemTimelineDetailPreviewBinding) holder.getBinding();
            viewModel.setBottomLineVisibility(shouldDisplayBottomLine(position));
            holder.setVariable(BR.viewModel, viewModel);
            mItemViewModelMap.put(mDatas.get(position).getId(), viewModel);
            viewModel.bindMediaView(binding.mediaLayouts, holder);
        } else {
            super.onBindData(holder, position);
        }
    }

    private boolean shouldDisplayBottomLine(int position) {
        /*
            Rule of display bottom line in each post item in post detail screen.
            1. If post detail screen is open in preview mode [redirection from notification] and the position is zero.
            2. If post contain only text [normal, link preview, or place preview] and no media ==> not display bottom line separator
            3. If post contain only one media and has no text ==> not display bottom line separator
            3. For the other cases besides mentioning in point 1, 2, and 3, the bottom line will be displayed.
         */
        if (mIsInPreviewPostMode && position == 0) {
            /*
                No need to show the separation line between first item and comment items for in this
                mode, we display as single post item which mean post and comment list are considered as one element.
             */
            return false;
        } else {
            if (mIsShowCommentForSingleItemPost) {
                return false;
            } else {
                return !mDatas.isEmpty() && position <= (mDatas.size() - 1);
            }
        }
    }

    private List<Media> getMediaListFromAdaptiveList() {
        List<Media> mediaList = new ArrayList<>();
        for (Adaptive data : mDatas) {
            if (data instanceof Media) {
                mediaList.add((Media) data);
            }
        }

        return mediaList;
    }

    private int getMediaPosition(int position) {
        if (mDatas.get(0) instanceof LifeStream) {
            int newPos = position - 1;
            if (newPos < 0) {
                return 0;
            } else {
                return newPos;
            }
        } else {
            return position;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= 0 && position < mDatas.size()) {
            //Special display time from the notification redirection.
            if (mIsInPreviewPostMode &&
                    mDatas.get(position) instanceof LifeStream &&
                    mDatas.get(position).getMedia() != null && !mDatas.get(position).getMedia().isEmpty() &&
                    !isFilePostTimeline(position)) {
                return TIMELINE_MEDIA_PREVIEW;
            }

            //Normal checking
            if (mDatas.get(position) instanceof LifeStream) {
                if (isFilePostTimeline(position)) {
                    return TIMELINE_FILE;
                } else {
                    return NORMAL_ITEM;
                }
            } else if (mDatas.get(position) instanceof Media) {
                return MEDIA_ITEM;
            } else if (mDatas.get(position) instanceof SharedTimeline
                    || mDatas.get(position) instanceof SharedProduct) {
                return SHARE_ITEM;
            } else {
                return super.getItemViewType(position);
            }
        }

        return super.getItemViewType(position);
    }

    private boolean isFilePostTimeline(int position) {
        if (mDatas.get(position) instanceof LifeStream) {
            return ((LifeStream) mDatas.get(position)).isFilePostTimelineType();
        }

        return false;
    }

    private ViewGroup.LayoutParams getLayoutParam(MediaLayout mediaLayout, Media media) {
        ViewGroup.LayoutParams param = mediaLayout.getLayoutParams();
        WallStreetHelper.calculatePostHeight(new Point(media.getWidth(), media.getHeight()),
                (desireWidth, desiredHeight, scaleType) -> {
                    param.width = desireWidth;
                    param.height = desiredHeight;
                    mediaLayout.setScaleType(scaleType);
                });
        return param;
    }

    public void checkToReleaseVideoPlayerInMediaLayout() {
        for (MediaLayout mediaLayout : mMediaLayoutList) {
            if (mediaLayout != null) {
                mediaLayout.release();
                Timber.i("checkToReleaseVideoPlayerInMediaLayout");
            }
        }
        mMediaLayoutList.clear();
    }

    private void setMediaLayout(BindingViewHolder holder,
                                MediaLayout mediaLayout,
                                Media media,
                                List<Media> mediaList) {
        mediaLayout.setLayoutParams(getLayoutParam(mediaLayout, media));
        mediaLayout.setMedia(media, true);
        mMediaLayoutList.add(mediaLayout);
        if (media.getType() == MediaType.VIDEO) {
            mediaLayout.setVisibleMuteButton(false);
            mediaLayout.setShowControl(true);
            mediaLayout.setSaveOnPause(true);
            mediaLayout.setVisibleFloatingButton(true);
            mediaLayout.setVisibleFullScreenButton(true);
            mediaLayout.setOnFloatingVideoClickedListener(v -> mOnFloatingVideoClickedListener.onFloatingVideo(getMediaPosition(holder.getAdapterPosition()), mediaLayout.getTime()));
            mediaLayout.setOnFullScreenClickListener(v -> {
                mOnFloatingVideoClickedListener.onFullScreenClicked(getMediaPosition(holder.getAdapterPosition()), mediaLayout.getTime());
            });
            if (holder instanceof TimelineViewHolder) {
                ((TimelineViewHolder) holder).setMediaLayout(mediaLayout, null);
            }
            mediaLayout.setExoPlayerClickListener(view -> {
                if (mAdapterListener != null) {
                    mAdapterListener.onPauseMediaView();
                }
                showFullScreenMedia(media, mediaList);
            });
        }

        /*
          Manage to open the detail of photo in single post
         */
        if (media.getType() == MediaType.IMAGE || media.getType() == MediaType.GIF) {
            mediaLayout.setOnClickListener(v -> {
                Timber.i("Pos: " + holder.getAdapterPosition());
                showFullScreenMedia(media, mediaList);
            });
        }
    }

    private void showFullScreenMedia(Media media, List<Media> mediaList) {
        FullScreenMediaDialog dialog =
                FullScreenMediaDialog.newInstance(mediaList,
                        findMediaPositionInList(media, mediaList),
                        false);
        dialog.setChatImageFullScreenDialogListener(media1 -> {
            AbsBaseActivity absBaseActivity = mContext;
            if (absBaseActivity != null) {
                absBaseActivity.checkPermission(new PermissionCheckHelper.OnPermissionCallback() {
                                                    @Override
                                                    public void onPermissionGranted() {
                                                        dialog.saveMedia(media1);
                                                    }

                                                    @Override
                                                    public void onPermissionDenied(String[] grantedPermission,
                                                                                   String[] deniedPermission,
                                                                                   boolean isUserPressNeverAskAgain) {
                                                        ToastUtil.showToast(mContext, R.string.permission_not_granted, true);
                                                    }
                                                }, Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
        });
        dialog.show(mContext.getSupportFragmentManager(), FullScreenMediaDialog.TAG);
    }

    public void retrieveItemFromSpecificComment(String checkingId,
                                                RetrieveItemFromSpecificCommentListener listener) {
        if (!TextUtils.isEmpty(checkingId)) {
            for (int i = 0; i < getDatas().size(); i++) {
                if (TextUtils.equals(checkingId, getDatas().get(i).getId())) {
                    if (listener != null) {
                        listener.onRetrievedFinished(i, getDatas().get(i));
                    }
                    break;
                }
            }
        }
    }

    private int findMediaPositionInList(Media media, List<Media> mediaList) {
        if (media != null && !mediaList.isEmpty()) {
            for (int i = 0; i < mediaList.size(); i++) {
                if (media.getId().matches(mediaList.get(i).getId())) {
                    return i;
                }
            }
        }

        return -1;
    }

    private WallStreetDataManager getDataManager() {
        return new WallStreetDataManager(mContext,
                mApiService,
                null,
                null);

    }

    public void updateItem(int position, Adaptive adaptive) {
        mDatas.set(position, adaptive);
        notifyItemChanged(position);
    }

    public interface RetrieveItemFromSpecificCommentListener {
        void onRetrievedFinished(int position, Adaptive adaptive);
    }

    public interface TimelineDetailAdapterListener {
        void onPauseMediaView();
    }

    public void addCommentList(List<Adaptive> commentList, boolean isNeedToClearAllOldComment) {
        int previousItemCount;
        if (isNeedToClearAllOldComment) {
            previousItemCount = 1;
            //Keep the first item
            setCanLoadNext(false);
            int itemCount = getItemCount();
            mItemLoadMorePreviousCommentViewModel = null;
            if (mDatas.size() > 1) {
                mDatas.subList(1, mDatas.size()).clear();
            }
            notifyItemRangeRemoved(1, itemCount - 1);
        } else {
            previousItemCount = getItemCount() + 1;
        }
        mDatas.addAll(commentList);
        notifyItemRangeInserted(previousItemCount, commentList.size());
    }
}
