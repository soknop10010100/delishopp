package com.proapp.sompom.adapter.newadapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.DiffUtil;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.proapp.sompom.model.concierge.ConciergeCarouselSectionResponse;
import com.proapp.sompom.newui.fragment.ConciergePromotionItemFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Or Vitovongsak on 18/3/22.
 */
public class ConciergePromotionPagerAdapter extends FragmentStateAdapter {

    private List<ConciergeCarouselSectionResponse.Data> mData;
    private final List<Long> mContentId = new ArrayList<>();
    private final List<Fragment> mFragmentList = new ArrayList<>();

    public ConciergePromotionPagerAdapter(@NonNull FragmentManager fragmentManager,
                                          @NonNull Lifecycle lifecycle,
                                          List<ConciergeCarouselSectionResponse.Data> data) {
        super(fragmentManager, lifecycle);
        mData = data;
        rebuildFragmentList();
        rebuildContentIdList();
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getItemCount() {
        return mFragmentList.size();
    }

    @Override
    public long getItemId(int position) {
        return mContentId.get(position);
    }

    @Override
    public boolean containsItem(long itemId) {
        for (long id : mContentId) {
            if (itemId == id) {
                return true;
            }
        }
        return false;
    }

    public void refreshData(List<ConciergeCarouselSectionResponse.Data> newData) {
        if (mData.isEmpty()) {
            mData = newData;
            rebuildFragmentList();
            rebuildContentIdList();
            notifyDataSetChanged();
        } else {
            final ConciergePromotionListDiffCallback diffCallback =
                    new ConciergePromotionListDiffCallback(mData, newData);
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
            mData = newData;
            rebuildFragmentList();
            rebuildContentIdList();
            diffResult.dispatchUpdatesTo(this);
        }
    }

    private void rebuildContentIdList() {
        mContentId.clear();
        for (ConciergeCarouselSectionResponse.Data data : mData) {
            long hash = Math.abs(data.hashCode());
            mContentId.add(hash);
        }
    }

    private void rebuildFragmentList() {
        mFragmentList.clear();
        for (ConciergeCarouselSectionResponse.Data data : mData) {
            mFragmentList.add(ConciergePromotionItemFragment.newInstance(data));
        }
    }

    public static class ConciergePromotionListDiffCallback extends DiffUtil.Callback {

        private final List<ConciergeCarouselSectionResponse.Data> mOldList;
        private final List<ConciergeCarouselSectionResponse.Data> mNewList;

        public ConciergePromotionListDiffCallback(List<ConciergeCarouselSectionResponse.Data> oldBaseChatModelList,
                                                  List<ConciergeCarouselSectionResponse.Data> newBaseChatModelList) {
            mOldList = oldBaseChatModelList;
            mNewList = newBaseChatModelList;
        }

        @Override
        public int getOldListSize() {
            return mOldList.size();
        }

        @Override
        public int getNewListSize() {
            return mNewList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return mOldList.get(oldItemPosition).areItemsTheSame(mNewList.get(newItemPosition));
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return mOldList.get(oldItemPosition).areContentsTheSame(mNewList.get(newItemPosition));
        }
    }

}
