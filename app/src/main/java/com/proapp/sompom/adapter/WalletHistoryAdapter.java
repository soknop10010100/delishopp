package com.proapp.sompom.adapter;

import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.model.concierge.WalletHistory;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.WalletHistoryItemViewModel;

import java.util.List;

/**
 * Created by Chhom Veasna on 8/23/22.
 */

public class WalletHistoryAdapter extends RefreshableAdapter<WalletHistory, BindingViewHolder> {

    public WalletHistoryAdapter(List<WalletHistory> userList) {
        super(userList);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_wallet_history).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        holder.setVariable(BR.viewModel, new WalletHistoryItemViewModel(holder.getContext(), mDatas.get(position)));
    }

    public void setRefreshData(List<WalletHistory> data, boolean isCanLoadMore) {
        setCanLoadMore(isCanLoadMore);
        mDatas.clear();
        mDatas.addAll(data);
        notifyDataSetChanged();
    }
}
