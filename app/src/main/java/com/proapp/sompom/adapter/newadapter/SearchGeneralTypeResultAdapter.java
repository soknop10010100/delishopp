package com.proapp.sompom.adapter.newadapter;

import android.content.Context;
import android.view.ViewGroup;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.listener.OnLikeItemListener;
import com.proapp.sompom.model.Search;
import com.proapp.sompom.model.emun.FollowItemType;
import com.proapp.sompom.model.emun.OpenChatScreenType;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemConversationMessageViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemLikeViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemSearchMessageResultProductViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class SearchGeneralTypeResultAdapter extends RefreshableAdapter<Search, BindingViewHolder> {

    private static final int PRODUCT = -1;
    private static final int USER = -2;
    private static final int CONVERSATION = -3;

    private final OnLikeItemListener mListener;
    private final Context mContext;
    private String mMyUserId;
    private SearchGeneralTypeResultAdapterListener mSearchGeneralTypeResultAdapterListener;

    public SearchGeneralTypeResultAdapter(Context context,
                                          List<Search> datas,
                                          OnLikeItemListener likeItemListener,
                                          SearchGeneralTypeResultAdapterListener listener) {
        super(datas);
        mSearchGeneralTypeResultAdapterListener = listener;
        mContext = context;
        mMyUserId = SharedPrefUtils.getUserId(context);
        mListener = likeItemListener;
    }

    public void unRegisterReceiver() {
    }

    public void setData(List<Search> searches) {
        mDatas.clear();
        mDatas.addAll(searches);
        notifyDataSetChanged();
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == PRODUCT) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_search_result_message_product).build();
        } else if (viewType == CONVERSATION) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_conversation_message).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_like_viewer).build();
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (mDatas.get(position) instanceof Chat) {
            Chat chat = (Chat) mDatas.get(position);
            ListItemConversationMessageViewModel viewModel = new ListItemConversationMessageViewModel(holder.getContext(),
                    chat.getParentConversation(),
                    mSearchGeneralTypeResultAdapterListener.getSearchKeyWord(),
                    chat);
            holder.setVariable(BR.viewModel, viewModel);
            holder.getBinding().getRoot().setOnClickListener(v -> {
                if (chat.getParentConversation() == null) {
                    if (mSearchGeneralTypeResultAdapterListener != null) {
                        mSearchGeneralTypeResultAdapterListener.noParentConversationOfChatAttached(chat);
                    }
                } else {
                    mContext.startActivity(new ChatIntent(mContext,
                            chat.getParentConversation(),
                            chat,
                            mSearchGeneralTypeResultAdapterListener.getSearchKeyWord(),
                            OpenChatScreenType.GENERAL_SEARCH));
                }
            });
        } else if (mDatas.get(position) instanceof User) {
            final ListItemLikeViewModel viewModel = new ListItemLikeViewModel(mContext,
                    mMyUserId,
                    (User) mDatas.get(position),
                    FollowItemType.FOLLOWER,
                    mListener);
            holder.setVariable(BR.viewModel, viewModel);
        } else if (mDatas.get(position) instanceof Product) {
            final ListItemSearchMessageResultProductViewModel viewModel =
                    new ListItemSearchMessageResultProductViewModel((Product) mDatas.get(position));
            holder.setVariable(BR.viewModel, viewModel);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position < mDatas.size()) {
            if (mDatas.get(position) instanceof Product) {
                return PRODUCT;
            } else if (mDatas.get(position) instanceof User) {
                return USER;
            } else if (mDatas.get(position) instanceof Chat) {
                return CONVERSATION;
            }
        }
        return super.getItemViewType(position);
    }

    public void notifyData() {
        mMyUserId = SharedPrefUtils.getUserId(mContext);
        notifyDataSetChanged();
    }

    public interface SearchGeneralTypeResultAdapterListener {
        void noParentConversationOfChatAttached(Chat chat);

        String getSearchKeyWord();
    }
}
