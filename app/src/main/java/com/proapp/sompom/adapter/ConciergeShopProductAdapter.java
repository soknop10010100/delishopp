package com.proapp.sompom.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ListConciergeShopItemBinding;
import com.proapp.sompom.databinding.ListConciergeShopItemCategoryBinding;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeMenuSection;
import com.proapp.sompom.model.concierge.ConciergeShop;
import com.proapp.sompom.newui.fragment.AbsSupportShopCheckOutFragment;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.ListConciergeShopItemCategoryViewModel;
import com.proapp.sompom.viewmodel.ListConciergeShopItemViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemConciergeProductViewModel;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.List;

/**
 * Created by Veasna Chhom on 10/9/21.
 */

public class ConciergeShopProductAdapter extends RefreshableAdapter<ConciergeShopDetailDisplayAdaptive, BindingViewHolder> {

    private static final int CATEGORY_VIEW_TYPE = 0x090;
    private static final int ITEM_VIEW_TYPE = 0x091;

    private Context mContext;
    private ShopProductAdapterCallback mListener;
    private ConciergeShop mConciergeShop;
    private boolean mIsItemUpdatedWithinSameScreen = false;
    private boolean mIsRequiredRootMarginAndPadding;

    public ConciergeShopProductAdapter(Context context,
                                       ConciergeShop conciergeShop,
                                       List<ConciergeShopDetailDisplayAdaptive> data,
                                       boolean isRequiredRootMarginAndPadding,
                                       ShopProductAdapterCallback listener) {
        super(data);
        mContext = context;
        mIsRequiredRootMarginAndPadding = isRequiredRootMarginAndPadding;
        mConciergeShop = conciergeShop;
        mListener = listener;
        setCanLoadMore(false);
    }

    @Override
    public int getItemViewType(int position) {
        if (position < mDatas.size()) {
            if (mDatas.get(position).isProductCategory()) {
                return CATEGORY_VIEW_TYPE;
            } else {
                return ITEM_VIEW_TYPE;
            }
        }

        return super.getItemViewType(position);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == CATEGORY_VIEW_TYPE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_concierge_shop_item_category).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_concierge_shop_item).build();
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListConciergeShopItemCategoryBinding) {
            ConciergeMenuSection category = (ConciergeMenuSection) mDatas.get(position);
            ListConciergeShopItemCategoryViewModel viewModel = new ListConciergeShopItemCategoryViewModel(category.getName());
            holder.setVariable(BR.viewModel, viewModel);
        } else if (holder.getBinding() instanceof ListConciergeShopItemBinding) {
            ConciergeMenuItem conciergeMenuItem = (ConciergeMenuItem) mDatas.get(position);
            ListConciergeShopItemViewModel viewModel = new ListConciergeShopItemViewModel(holder.getContext(),
                    mConciergeShop,
                    conciergeMenuItem,
                    mIsRequiredRootMarginAndPadding,
                    new ListItemConciergeProductViewModel.ListItemConciergeProductViewModelListener() {
                        @Override
                        public void onProductAddedFromDetail() {
                            if (mListener != null) {
                                mListener.onProductAddedFromDetail();
                            }
                        }

                        @Override
                        public void onProductFailToAdd() {
                            notifyItemChanged(position);
                        }

                        @Override
                        public void onItemCountChanged(int newCount, boolean isRemove) {
                            if (mContext instanceof AppCompatActivity) {
                                conciergeMenuItem.setProductCount(newCount);
                                Intent intent = new Intent(isRemove ?
                                        AbsSupportShopCheckOutFragment.CART_ITEM_REMOVED_EVENT :
                                        AbsSupportShopCheckOutFragment.CART_ITEM_ADDED_EVENT);
                                intent.putExtra(AbsSupportShopCheckOutFragment.CART_EVENT_DATA, conciergeMenuItem);
                                SendBroadCastHelper.verifyAndSendBroadCast(mContext, intent);
                            }
                        }

                        @Override
                        public void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {

                        }
                    });
            holder.setVariable(BR.viewModel, viewModel);
        }
    }

    public void updateProductSelectionCounter(ConciergeMenuItem updateItem) {
        for (int i = 0; i < mDatas.size(); i++) {
            ConciergeShopDetailDisplayAdaptive data = mDatas.get(i);
            if (data instanceof ConciergeMenuItem && TextUtils.equals(data.getId(), updateItem.getId())) {
//                ((ConciergeMenuItem) data).setProductCount(updateItem.getProductCount());
                notifyItemChanged(i);
                break;
            }
        }
    }

    public void onItemUpdatedInCart(ConciergeMenuItem conciergeMenuItem) {
        for (int index = 0; index < mDatas.size(); index++) {
            ConciergeShopDetailDisplayAdaptive adaptive = mDatas.get(index);
            if (adaptive instanceof ConciergeMenuItem) {
                if (!adaptive.isProductCategory() && TextUtils.equals(adaptive.getId(), conciergeMenuItem.getId())) {
                    notifyItemChanged(index);
                    break;
                }
            }
        }
    }

    public void refreshProductInCardCounter() {
        for (int index = 0; index < mDatas.size(); index++) {
            if (mDatas.get(index) instanceof ConciergeMenuItem) {
                notifyItemChanged(index);
            }
        }
    }

    public void onCartCleared() {
        for (int index = 0; index < mDatas.size(); index++) {
            ConciergeShopDetailDisplayAdaptive adaptive = mDatas.get(index);
            if (!adaptive.isProductCategory()) {
                ConciergeMenuItem productOnDisplay = (ConciergeMenuItem) adaptive;
                productOnDisplay.setProductCount(0);
                notifyItemChanged(index);
            }
        }
    }

    public int findItemPositionById(String id) {
        if (!TextUtils.isEmpty(id)) {
            for (int i = 0; i < mDatas.size(); i++) {
                if (TextUtils.equals(id, mDatas.get(i).getId())) {
                    return i;
                }
            }
        }

        return -1;
    }

    public interface ShopProductAdapterCallback {
        void onProductAddedFromDetail();
    }
}
