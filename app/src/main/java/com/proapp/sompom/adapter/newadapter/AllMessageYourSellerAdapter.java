package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.UserListDiffCallback;
import com.proapp.sompom.listener.OnMessageItemClick;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemAllMessageYourSeller;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class AllMessageYourSellerAdapter extends RecyclerView.Adapter<BindingViewHolder> {

    private List<User> mLists;
    private OnMessageItemClick mOnMessageItemClick;
    private boolean mIsShowActiveIcon = true;

    public AllMessageYourSellerAdapter(List<User> lists, OnMessageItemClick messageItemClick) {
        mLists = lists;
        mOnMessageItemClick = messageItemClick;
    }

    public void setShowActiveIcon(boolean showActiveIcon) {
        mIsShowActiveIcon = showActiveIcon;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_all_messag_your_seller).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ListItemAllMessageYourSeller viewModel = new ListItemAllMessageYourSeller(mLists.get(position), mOnMessageItemClick);
        holder.setVariable(BR.viewModel, viewModel);
    }

    @Override
    public int getItemCount() {
        return mLists.size();
    }

    public void refreshData(List<User> data) {
        if (mLists.isEmpty()) {
            mLists = data;
            notifyDataSetChanged();
        } else {
            final UserListDiffCallback diffCallback = new UserListDiffCallback(new ArrayList<>(mLists), new ArrayList<>(data));
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
            mLists.clear();
            mLists.addAll(data);
            diffResult.dispatchUpdatesTo(this);
        }
    }
}
