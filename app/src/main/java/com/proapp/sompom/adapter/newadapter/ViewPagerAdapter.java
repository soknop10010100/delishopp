package com.proapp.sompom.adapter.newadapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by he.rotha on 3/4/16.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> mFragmentProductDetailItems = new ArrayList<>();
    private List<String> mTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentProductDetailItems.add(fragment);
        mTitleList.add(title);
    }

    public Fragment getFr(int position) {
        return mFragmentProductDetailItems.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentProductDetailItems.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentProductDetailItems.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleList.get(position);
    }
}
