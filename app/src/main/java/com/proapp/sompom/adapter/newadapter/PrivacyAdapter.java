package com.proapp.sompom.adapter.newadapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.ViewGroup;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.model.ItemPrivacy;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ItemPrivacyViewModel;

import java.util.List;

public class PrivacyAdapter extends RecyclerView.Adapter<BindingViewHolder> {

    private List<ItemPrivacy> mData;
    private ItemPrivacy mSelectedPrivacy;

    public PrivacyAdapter(List<ItemPrivacy> data) {
        mData = data;
        for (ItemPrivacy datum : mData) {
            if (datum.isSelected()) {
                mSelectedPrivacy = datum;
            }
        }
    }

    public ItemPrivacy getSelectedPrivacy() {
        return mSelectedPrivacy;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_privacy).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ItemPrivacyViewModel data = new ItemPrivacyViewModel(holder.getContext(), mData.get(position));
        holder.setVariable(BR.viewModel, data);
        holder.getBinding().getRoot().setOnClickListener(v -> {
            if (!mData.get(holder.getAdapterPosition()).isSelected()) {
                mSelectedPrivacy = mData.get(holder.getAdapterPosition());
                for (ItemPrivacy datum : mData) {
                    datum.setSelected(false);
                }
                mData.get(holder.getAdapterPosition()).setSelected(true);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
