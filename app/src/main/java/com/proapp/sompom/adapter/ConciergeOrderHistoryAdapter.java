package com.proapp.sompom.adapter;

import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ListItemConciergeOrderHistoryBinding;
import com.proapp.sompom.model.ItemConciergeOrderHistoryAdaptive;
import com.proapp.sompom.model.emun.ConciergeOrderStatus;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.AbsConciergeOrderHistoryViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ItemConciergeOrderHistoryViewModel;

import java.util.Date;
import java.util.List;

/**
 * Created by Veasna Chhom on 10/9/21.
 */

public class ConciergeOrderHistoryAdapter extends AbsConciergeOrderHistoryAdapter<ItemConciergeOrderHistoryAdaptive, BindingViewHolder> {

    private static final int NORMAL_ITEM_TYPE = 0x98;
    private static final int END_ITEM_TYPE = 0x99;
    private final ItemEndConciergeOrderHistoryModel mEndConciergeOrderHistoryModel = new ItemEndConciergeOrderHistoryModel();
    private boolean mShouldDisplayTrackingButton;

    public ConciergeOrderHistoryAdapter(List<ItemConciergeOrderHistoryAdaptive> datas,
                                        boolean shouldDisplayTrackingButton,
                                        AbsConciergeOrderHistoryAdapterListener absListener) {
        super(datas, absListener);
        mShouldDisplayTrackingButton = shouldDisplayTrackingButton;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < mDatas.size()) {
            if (mDatas.get(position) instanceof ConciergeOrder) {
                return NORMAL_ITEM_TYPE;
            } else if (mDatas.get(position) instanceof ConciergeOrderHistoryAdapter) {
                return END_ITEM_TYPE;
            }
        }

        return super.getItemViewType(position);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == NORMAL_ITEM_TYPE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_order_history).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_end_concierge_order_history).build();
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListItemConciergeOrderHistoryBinding) {
            ConciergeOrder order = (ConciergeOrder) mDatas.get(position);
            ItemConciergeOrderHistoryViewModel viewModel =
                    new ItemConciergeOrderHistoryViewModel(holder.getContext(),
                            order,
                            mShouldDisplayTrackingButton,
                            new AbsConciergeOrderHistoryViewModel.AbsConciergeOrderHistoryViewModelListener() {
                                @Override
                                public void onMessageClicked() {
                                    if (mAbsListener != null) {
                                        mAbsListener.onMessageClicked(order);
                                    }
                                }

                                @Override
                                public void onCallClicked() {
                                    if (mAbsListener != null) {
                                        mAbsListener.onCallClicked();
                                    }
                                }

                                @Override
                                public void onOrderCanceled(ConciergeOrder canceledOrder) {
                                    mDatas.set(position, canceledOrder);
                                    notifyItemChanged(position);
                                }
                            });
            holder.setVariable(BR.viewModel, viewModel);
        }
    }

    public void updateOrderStatus(ConciergeOrder updatedOrder) {
        for (int index = 0; index < mDatas.size(); index++) {
            ItemConciergeOrderHistoryAdaptive adaptive = mDatas.get(index);
            if (adaptive instanceof ConciergeOrder) {
                ConciergeOrder orderInList = (ConciergeOrder) adaptive;
                if (orderInList.getId().equals(updatedOrder.getId()) &&
                        orderInList.getOrderNumber().equals(updatedOrder.getOrderNumber())) {
                    orderInList.setOrderStatus(updatedOrder.getOrderStatus());
                    if (!TextUtils.isEmpty(updatedOrder.getStatusString())) {
                        orderInList.setStatusString(updatedOrder.getStatusString());
                    }
                    updateOrderStepTrackingDate(orderInList, updatedOrder.getOrderStatus());
                    notifyItemChanged(index);
                    break;
                }
            }
        }
    }

    private void updateOrderStepTrackingDate(ConciergeOrder orderInList, ConciergeOrderStatus updateStatus) {
        if (orderInList.getTrackingProgress() != null && !orderInList.getTrackingProgress().isEmpty()) {
            for (int i = 0; i < orderInList.getTrackingProgress().size(); i++) {
                if (orderInList.getTrackingProgress().get(i).getStatus() == updateStatus) {
                    if (orderInList.getTrackingProgress().get(i).getDate() == null) {
                        orderInList.getTrackingProgress().get(i).setDate(new Date());
                    }

                    if (i > 0) {
                        for (int i2 = 0; i2 <= i - 1; i2++) {
                            if (orderInList.getTrackingProgress().get(i2).getDate() == null) {
                                orderInList.getTrackingProgress().get(i2).setDate(new Date());
                            }
                        }
                    }

                    break;
                }
            }
        }
    }

    public void addOrRemoveEndOfHistoryItem(boolean isAdd) {
       /*
        Check if it is already added and normally it is at the last position of the list.
        */
        if (!mDatas.isEmpty()) {
            ItemConciergeOrderHistoryAdaptive lastItem = mDatas.get(mDatas.size() - 1);
            if (isAdd) {
                if (!(lastItem instanceof ItemEndConciergeOrderHistoryModel)) {
                    mDatas.add(mEndConciergeOrderHistoryModel);
                    notifyItemInserted(mDatas.size() - 1);
                }
            } else {
                if (lastItem instanceof ItemEndConciergeOrderHistoryModel) {
                    int lastItemIndex = mDatas.size() - 1;
                    mDatas.remove(lastItem);
                    notifyItemRemoved(lastItemIndex);
                }
            }
        } else {
            // If the data is empty to begin with, show empty history item
            if (isAdd) {
                mDatas.add(mEndConciergeOrderHistoryModel);
                notifyItemInserted(mDatas.size() - 1);
            }
        }
    }

    public void setRefreshData(List<ItemConciergeOrderHistoryAdaptive> data, boolean isCanLoadMore) {
        setCanLoadMore(isCanLoadMore);
        setCanLoadMore(isCanLoadMore);
        mDatas.clear();
        mDatas.addAll(data);
        notifyDataSetChanged();
    }

    public static class ItemEndConciergeOrderHistoryModel implements ItemConciergeOrderHistoryAdaptive {
    }
}
