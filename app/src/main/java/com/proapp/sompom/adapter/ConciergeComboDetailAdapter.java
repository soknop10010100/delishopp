package com.proapp.sompom.adapter;

import android.view.ViewGroup;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.model.concierge.ConciergeComboChoiceSection;
import com.proapp.sompom.model.concierge.ConciergeComboItem;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.ListItemConciergeOptionItemViewModel;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Or Vitovongsak on 13/10/21.
 */

public class ConciergeComboDetailAdapter
        extends RefreshableAdapter<ConciergeComboItem, BindingViewHolder> {

    private ConciergeComboChoiceSection mComboChoiceSection;
    private float mDefaultPrice;
    private ConciergeComboDetailAdapterListener mListener;
    private final HashMap<String, ListItemConciergeOptionItemViewModel> mViewModelMap = new HashMap<>();

    public ConciergeComboDetailAdapter(ConciergeComboChoiceSection comboChoiceSection, float defaultPrice) {
        super(comboChoiceSection.getList());
        setCanLoadMore(false);
        mComboChoiceSection = comboChoiceSection;
        mDefaultPrice = defaultPrice;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_option_item).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ConciergeComboItem comboItem = mDatas.get(position);

        if (mViewModelMap.get(comboItem.getId()) == null) {
            ListItemConciergeOptionItemViewModel viewModel = new ListItemConciergeOptionItemViewModel(holder.getContext(),
                    comboItem,
                    mDefaultPrice,
                    mComboChoiceSection.isDisplayFullPrice(),
                    (item, preselectionChoiceVariationId) -> {
                        if (mListener != null) {
                            mListener.onComboClicked(item, preselectionChoiceVariationId);
                        }
                    });
            mViewModelMap.put(comboItem.getId(), viewModel);
        }

        ListItemConciergeOptionItemViewModel viewModel = mViewModelMap.get(comboItem.getId());

        holder.setVariable(BR.viewModel, viewModel);
    }

    public void setListener(ConciergeComboDetailAdapterListener listener) {
        mListener = listener;
    }

    public void updateComboData(List<ConciergeComboItem> newData) {
        for (ConciergeComboItem comboItem : newData) {
            ListItemConciergeOptionItemViewModel viewModel = mViewModelMap.get(comboItem.getId());
            if (viewModel != null) {
                viewModel.initData(comboItem);
            }
        }
        checkShouldShowError();
    }

    public void checkShouldShowError() {
        boolean isRequiredOptionSelected = false;
        for (ConciergeComboItem option : mDatas) {
            ListItemConciergeOptionItemViewModel viewModel = mViewModelMap.get(option.getId());
            if (viewModel != null) {
                if (viewModel.getIsSelected().get()) {
                    isRequiredOptionSelected = true;
                }
            }
        }

        for (ConciergeComboItem option : mDatas) {
            ListItemConciergeOptionItemViewModel viewModel = mViewModelMap.get(option.getId());
            if (viewModel != null) {
                viewModel.shouldShowError(!isRequiredOptionSelected);
            }
        }
    }

    public void makeItemSelection(ConciergeComboItem comboItem, String preselectionOptionId) {
        ListItemConciergeOptionItemViewModel viewModel = mViewModelMap.get(comboItem.getId());
        if (viewModel != null) {
            viewModel.onOptionClick(preselectionOptionId);
        }
    }

    public interface ConciergeComboDetailAdapterListener {
        void onComboClicked(ConciergeComboItem comboItem, String preselectionOptionId);
    }
}
