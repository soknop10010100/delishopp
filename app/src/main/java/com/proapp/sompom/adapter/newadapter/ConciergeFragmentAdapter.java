package com.proapp.sompom.adapter.newadapter;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager2.widget.ViewPager2;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.databinding.ListItemConciergeBrandSectionBinding;
import com.proapp.sompom.databinding.ListItemConciergeFeaturedStoreBinding;
import com.proapp.sompom.databinding.ListItemConciergeNewArrivalPproductSectionBinding;
import com.proapp.sompom.databinding.ListItemConciergeOurServiceBinding;
import com.proapp.sompom.databinding.ListItemConciergeProductBinding;
import com.proapp.sompom.databinding.ListItemConciergePromotionBinding;
import com.proapp.sompom.databinding.ListItemConciergePushItemSectionBinding;
import com.proapp.sompom.databinding.ListItemConciergeShopAnnouncementBinding;
import com.proapp.sompom.databinding.ListItemConciergeSupplierSectionBinding;
import com.proapp.sompom.databinding.ListItemConciergeTrendingTitleBinding;
import com.proapp.sompom.decorataor.ConciergeDiscountItemDecoration;
import com.proapp.sompom.decorataor.GridSpacingItemDecoration;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.ConciergeShopListDiffCallback;
import com.proapp.sompom.model.ConciergeItemAdaptive;
import com.proapp.sompom.model.concierge.ConciergeBrandSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeCarouselSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeFeatureStoreSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeNewArrivalProductSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeOurServiceSectionResponse;
import com.proapp.sompom.model.concierge.ConciergePushItemSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeShopAnnouncement;
import com.proapp.sompom.model.concierge.ConciergeShopAnnouncementResponse;
import com.proapp.sompom.model.concierge.ConciergeSupplierSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeTrendingSectionResponse;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.binding.ViewPagerBindingUtil;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemConciergeOurServiceViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemConciergeProductViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemConciergeShopAnnouncementViewModel;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 30/8/21.
 */
public class ConciergeFragmentAdapter extends RefreshableAdapter<ConciergeItemAdaptive, BindingViewHolder> {

    private static final int CONCIERGE_SHOP_ANNOUNCEMENT_TYPE = 0X001;
    private static final int CONCIERGE_OUR_SERVICE_TYPE = 0X010;
    private static final int CONCIERGE_PROMOTION_TYPE = 0X011;
    private static final int CONCIERGE_FEATURED_STORE_TYPE = 0X012;
    private static final int CONCIERGE_TRENDING_TITLE_TYPE = 0X013;
    private static final int CONCIERGE_TRENDING_TYPE = 0X014;
    private static final int CONCIERGE_PUSH_PRODUCT_TYPE = 0x015;
    private static final int CONCIERGE_NEW_ARRIVAL_PRODUCT_TYPE = 0x016;
    private static final int CONCIERGE_BRAND_TYPE = 0x017;
    private static final int CONCIERGE_SUPPLIER_TYPE = 0x018;

    private final FragmentManager mChildFragmentManager;
    private final Lifecycle mLifeCycle;
    private final ConciergeFragmentAdapterListener mListener;
    private ConciergeNewArrivalProductSectionAdapter mConciergeNewArrivalProductSectionAdapter;
    private ConciergeSupplierSectionAdapter mConciergeSupplierSectionAdapter;
    private boolean mIsInExpressMode;
    private boolean mIsUsedInGeneralSearchMode;

    public ConciergeFragmentAdapter(List<ConciergeItemAdaptive> data,
                                    boolean isUsedInGeneralSearchMode,
                                    boolean isInExpressMode,
                                    FragmentManager childFragmentManager,
                                    Lifecycle lifecycle,
                                    ConciergeFragmentAdapterListener listener) {
        super(data);
        mIsUsedInGeneralSearchMode = isUsedInGeneralSearchMode;
        mIsInExpressMode = isInExpressMode;
        mChildFragmentManager = childFragmentManager;
        mLifeCycle = lifecycle;
        mListener = listener;
        setCanLoadMore(false);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        int resourceId;

        if (viewType == CONCIERGE_SHOP_ANNOUNCEMENT_TYPE) {
            resourceId = R.layout.list_item_concierge_shop_announcement;
        } else if (viewType == CONCIERGE_OUR_SERVICE_TYPE) {
            resourceId = R.layout.list_item_concierge_our_service;
        } else if (viewType == CONCIERGE_PROMOTION_TYPE) {
            resourceId = R.layout.list_item_concierge_promotion;
        } else if (viewType == CONCIERGE_PUSH_PRODUCT_TYPE) {
            resourceId = R.layout.list_item_concierge_push_item_section;
        } else if (viewType == CONCIERGE_FEATURED_STORE_TYPE) {
            resourceId = R.layout.list_item_concierge_featured_store;
        } else if (viewType == CONCIERGE_TRENDING_TITLE_TYPE) {
            resourceId = R.layout.list_item_concierge_trending_title;
        } else if (viewType == CONCIERGE_NEW_ARRIVAL_PRODUCT_TYPE) {
            resourceId = R.layout.list_item_concierge_new_arrival_pproduct_section;
        } else if (viewType == CONCIERGE_BRAND_TYPE) {
            resourceId = R.layout.list_item_concierge_brand_section;
        } else if (viewType == CONCIERGE_SUPPLIER_TYPE) {
            resourceId = R.layout.list_item_concierge_supplier_section;
        } else {
            resourceId = R.layout.list_item_concierge_product;
        }

        return new BindingViewHolder.Builder(parent, resourceId).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListItemConciergeShopAnnouncementBinding) {
            ListItemConciergeShopAnnouncementViewModel viewModel =
                    new ListItemConciergeShopAnnouncementViewModel((ConciergeShopAnnouncement) mDatas.get(position));
            holder.setVariable(BR.viewModel, viewModel);
        } else if (holder.getBinding() instanceof ListItemConciergeOurServiceBinding) {
            ListItemConciergeOurServiceViewModel viewModel = new ListItemConciergeOurServiceViewModel(holder.getContext(),
                    (ConciergeOurServiceSectionResponse) mDatas.get(position));
            holder.setVariable(BR.viewModel, viewModel);
        } else if (holder.getBinding() instanceof ListItemConciergePromotionBinding) {
            ListItemConciergePromotionBinding binding = (ListItemConciergePromotionBinding) holder.getBinding();
            ConciergeCarouselSectionResponse response = (ConciergeCarouselSectionResponse) mDatas.get(position);
            // Initialize the view holder adapter first if it's null
            if (binding.promotionViewPager.getAdapter() == null) {
                ConciergePromotionPagerAdapter adapter =
                        new ConciergePromotionPagerAdapter(mChildFragmentManager, mLifeCycle, response.getData());
                binding.promotionViewPager.setAdapter(adapter);
                binding.promotionViewPager.setClipToOutline(true);
                int itemCount = binding.promotionViewPager.getAdapter().getItemCount();
                binding.promotionViewPager.setOffscreenPageLimit(itemCount > 0
                        ? itemCount
                        : ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT);
                if (itemCount > 1) {
                    // Bind the indicator widget to the viewpager
                    binding.promotionViewPagerIndicator.setViewPager(binding.promotionViewPager);
                    // Initialize view pager auto scroll if there are more than one item
                    ViewPagerBindingUtil.setAutoScrollViewPager(binding.promotionViewPager, itemCount);
                }
            } else {
                // Try to refresh the adapter data if there are any changes
                ConciergePromotionPagerAdapter adapter = (ConciergePromotionPagerAdapter) binding.promotionViewPager.getAdapter();
                adapter.refreshData(response.getData());
            }
//             else {
//                // Hide page indicator if there are only one item
////                binding.promotionViewPagerIndicator.setVisibility(View.GONE);
//                // Cancel any existing auto scroll animation on the viewpager
//                ViewPagerBindingUtil.cancelAutoScrollViewPager(binding.promotionViewPager);
//            }
        } else if (holder.getBinding() instanceof ListItemConciergePushItemSectionBinding) {
            ListItemConciergePushItemSectionBinding binding = (ListItemConciergePushItemSectionBinding) holder.getBinding();
            ConciergePushItemSectionResponse response = (ConciergePushItemSectionResponse) mDatas.get(position);
            binding.textViewTitle.setText(response.getTitle());

            // Initialize the view holder adapter first if it's null
            if (binding.pushItemRecyclerView.getAdapter() == null) {
                ConciergePushItemSectionAdapter adapter = new ConciergePushItemSectionAdapter(response.getData());
                binding.pushItemRecyclerView.addItemDecoration(new ConciergeDiscountItemDecoration(holder.getContext(),
                        holder.getContext().getResources().getDimensionPixelSize(R.dimen.space_large)));
                binding.pushItemRecyclerView.setLayoutManager(
                        new LinearLayoutManager(holder.getContext(), LinearLayoutManager.HORIZONTAL, false));
                binding.pushItemRecyclerView.setAdapter(adapter);

            } else {
                // Try to refresh the adapter data if there are any changes
                ConciergePushItemSectionAdapter adapter = (ConciergePushItemSectionAdapter) binding.pushItemRecyclerView.getAdapter();
                adapter.refreshData(response.getData());
            }
        } else if (holder.getBinding() instanceof ListItemConciergeFeaturedStoreBinding) {
            ListItemConciergeFeaturedStoreBinding binding = (ListItemConciergeFeaturedStoreBinding) holder.getBinding();
            ConciergeFeatureStoreSectionResponse response = (ConciergeFeatureStoreSectionResponse) mDatas.get(position);
            binding.textViewTitle.setText(response.getTitle());

            if (response.getData().size() <= ConciergeHelper.CONCIERGE_FEATURED_STORE_ITEM / 2) {
                ViewGroup.LayoutParams layoutParams = binding.featuredStoreViewPager.getLayoutParams();
                layoutParams.height = holder.getContext().getResources().getDimensionPixelSize(R.dimen.concierge_featured_store_item_height);
                binding.featuredStoreViewPager.setLayoutParams(layoutParams);
            }

            // Initialize the view holder adapter first if it's null
            if (binding.featuredStoreViewPager.getAdapter() == null) {
                ConciergeFeaturedStorePagerAdapter adapter =
                        new ConciergeFeaturedStorePagerAdapter(mChildFragmentManager, mLifeCycle, response.getData());
                binding.featuredStoreViewPager.setAdapter(adapter);
                binding.featuredStoreViewPager.setOffscreenPageLimit(adapter.getItemCount());
                if (adapter.getItemCount() > 1) {
                    binding.featuredStoreViewPagerIndicator.setVisibility(View.VISIBLE);
                    binding.featuredStoreViewPagerIndicator.setViewPager(binding.featuredStoreViewPager);
                }
            } else {
                // Try to refresh the adapter data if there are any changes
                ConciergeFeaturedStorePagerAdapter adapter =
                        (ConciergeFeaturedStorePagerAdapter) binding.featuredStoreViewPager.getAdapter();
                adapter.refreshData(response.getData());
            }
        } else if (holder.getBinding() instanceof ListItemConciergeTrendingTitleBinding) {
            ConciergeTrendingSectionResponse conciergeTrendingSectionResponse = (ConciergeTrendingSectionResponse) mDatas.get(position);
            ((ListItemConciergeTrendingTitleBinding) holder.getBinding()).textViewTitle.setText(conciergeTrendingSectionResponse.getTitle());
        } else if (holder.getBinding() instanceof ListItemConciergeProductBinding) {
            ConciergeMenuItem trendingItem = (ConciergeMenuItem) mDatas.get(position);
            // Retrieve product count int cart to render the add button correctly
            int itemAmountInBasket = ConciergeCartHelper.getExistingProductCountInCart(trendingItem);
            Timber.i("Item: " + trendingItem.getName() + ", count: " + itemAmountInBasket);
            ListItemConciergeProductViewModel trendingItemViewModel = new ListItemConciergeProductViewModel(holder.getContext(),
                    trendingItem,
                    itemAmountInBasket,
                    true,
                    getProductListener(position)
            );
            holder.setVariable(BR.viewModel, trendingItemViewModel);
        } else if (holder.getBinding() instanceof ListItemConciergeNewArrivalPproductSectionBinding) {
            ListItemConciergeNewArrivalPproductSectionBinding binding = (ListItemConciergeNewArrivalPproductSectionBinding) holder.getBinding();
            ConciergeNewArrivalProductSectionResponse response = (ConciergeNewArrivalProductSectionResponse) mDatas.get(position);
            binding.textViewTitle.setText(response.getTitle());

            // Initialize the view holder adapter first if it's null
            if (binding.recyclerView.getAdapter() == null) {
                ConciergeNewArrivalProductSectionAdapter adapter = new ConciergeNewArrivalProductSectionAdapter(holder.getContext(),
                        response.getData(),
                        new ConciergeNewArrivalProductSectionAdapter.ConciergeNewArrivalProductSectionAdapterCallback() {
                            @Override
                            public void onProductAddedFromDetail() {
                                if (mListener != null) {
                                    mListener.onProductAddedFromDetail();
                                }
                            }

                            @Override
                            public void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {
                                if (mListener != null) {
                                    mListener.onAddFirstItemToCart(conciergeMenuItem);
                                }
                            }
                        });
                binding.recyclerView.addItemDecoration(new ConciergeDiscountItemDecoration(holder.getContext()));
                binding.recyclerView.setLayoutManager(
                        new LinearLayoutManager(holder.getContext(),
                                LinearLayoutManager.HORIZONTAL,
                                false));
                binding.recyclerView.setAdapter(adapter);
                mConciergeNewArrivalProductSectionAdapter = adapter;
            } else {
                // Try to refresh the adapter data if there are any changes
                ConciergeNewArrivalProductSectionAdapter adapter = (ConciergeNewArrivalProductSectionAdapter) binding.recyclerView.getAdapter();
                mConciergeNewArrivalProductSectionAdapter = adapter;
                adapter.refreshData(response.getData());
            }
        } else if (holder.getBinding() instanceof ListItemConciergeBrandSectionBinding) {
            ListItemConciergeBrandSectionBinding binding = (ListItemConciergeBrandSectionBinding) holder.getBinding();
            ConciergeBrandSectionResponse response = (ConciergeBrandSectionResponse) mDatas.get(position);
            binding.textViewTitle.setText(response.getTitle());

            // Initialize the view holder adapter first if it's null
            if (binding.recyclerView.getAdapter() == null) {
                ConciergeBrandSectionAdapter adapter = new ConciergeBrandSectionAdapter(response.getData());
                binding.recyclerView.addItemDecoration(new ConciergeDiscountItemDecoration(holder.getContext(),
                        holder.getContext().getResources().getDimensionPixelSize(R.dimen.space_xlarge)));
                binding.recyclerView.setLayoutManager(
                        new LinearLayoutManager(holder.getContext(),
                                LinearLayoutManager.HORIZONTAL,
                                false));
                binding.recyclerView.setAdapter(adapter);
            } else {
                // Try to refresh the adapter data if there are any changes
                ConciergeBrandSectionAdapter adapter = (ConciergeBrandSectionAdapter) binding.recyclerView.getAdapter();
                adapter.refreshData(response.getData());
            }
        } else if (holder.getBinding() instanceof ListItemConciergeSupplierSectionBinding) {
            ListItemConciergeSupplierSectionBinding binding = (ListItemConciergeSupplierSectionBinding) holder.getBinding();
            ConciergeSupplierSectionResponse response = (ConciergeSupplierSectionResponse) mDatas.get(position);
            binding.textViewTitle.setText(response.getTitle());
            if (binding.recyclerView.getAdapter() == null) {
                ConciergeSupplierSectionAdapter adapter = new ConciergeSupplierSectionAdapter(response.getData());
                binding.recyclerView.addItemDecoration(new GridSpacingItemDecoration(2,
                        holder.getContext().getResources().getDimensionPixelSize(R.dimen.space_xxxlarge),
                        false));
                binding.recyclerView.setLayoutManager(
                        new GridLayoutManager(holder.getContext(), 2));
                binding.recyclerView.setAdapter(adapter);
                mConciergeSupplierSectionAdapter = adapter;
            } else {
                // Try to refresh the adapter data if there are any changes
                ConciergeSupplierSectionAdapter adapter = (ConciergeSupplierSectionAdapter) binding.recyclerView.getAdapter();
                adapter.refreshData(response.getData());
                mConciergeSupplierSectionAdapter = adapter;
            }
        }
    }

    private ListItemConciergeProductViewModel.ListItemConciergeProductViewModelListener getProductListener(int position) {
        return new ListItemConciergeProductViewModel.ListItemConciergeProductViewModelListener() {
            @Override
            public void onProductAddedFromDetail() {
                if (mListener != null) {
                    mListener.onProductAddedFromDetail();
                }
            }

            @Override
            public void onProductFailToAdd() {
                notifyItemChanged(position);
            }

            @Override
            public void onItemCountChanged(int newAmount, boolean isRemove) {
                Timber.i("onItemCountChanged: newAmount: " + newAmount + ", isRemove: " + isRemove);
            }

            @Override
            public void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {
                if (mListener != null) {
                    mListener.onAddFirstItemToCart(conciergeMenuItem);
                }
            }
        };
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= 0 && position < mDatas.size()) {
            if (mDatas.get(position) instanceof ConciergeShopAnnouncement) {
                return CONCIERGE_SHOP_ANNOUNCEMENT_TYPE;
            } else if (mDatas.get(position) instanceof ConciergeOurServiceSectionResponse) {
                return CONCIERGE_OUR_SERVICE_TYPE;
            } else if (mDatas.get(position) instanceof ConciergeCarouselSectionResponse) {
                return CONCIERGE_PROMOTION_TYPE;
            } else if (mDatas.get(position) instanceof ConciergePushItemSectionResponse) {
                return CONCIERGE_PUSH_PRODUCT_TYPE;
            } else if (mDatas.get(position) instanceof ConciergeFeatureStoreSectionResponse) {
                return CONCIERGE_FEATURED_STORE_TYPE;
            } else if (mDatas.get(position) instanceof ConciergeTrendingSectionResponse) {
                return CONCIERGE_TRENDING_TITLE_TYPE;
            } else if (mDatas.get(position) instanceof ConciergeNewArrivalProductSectionResponse) {
                return CONCIERGE_NEW_ARRIVAL_PRODUCT_TYPE;
            } else if (mDatas.get(position) instanceof ConciergeBrandSectionResponse) {
                return CONCIERGE_BRAND_TYPE;
            } else if (mDatas.get(position) instanceof ConciergeSupplierSectionResponse) {
                return CONCIERGE_SUPPLIER_TYPE;
            } else {
                if (mIsInExpressMode && !mIsUsedInGeneralSearchMode) {
                    return CONCIERGE_SUPPLIER_TYPE;
                } else {
                    return CONCIERGE_TRENDING_TYPE;
                }
            }
        }

        return super.getItemViewType(position);
    }

    public void addLoadMoreTrendingData(List<ConciergeItemAdaptive> moreTrending) {
        addLoadMoreData(moreTrending);
    }

    public void addLoadMoreSupplierData(List<ConciergeItemAdaptive> data) {
        if (mConciergeSupplierSectionAdapter != null) {
            if (data.get(0) instanceof ConciergeSupplierSectionResponse) {
                mConciergeSupplierSectionAdapter.addLoadMoreData(((ConciergeSupplierSectionResponse) data.get(0)).getData());
            }
        }
    }

    public void onItemUpdatedInCart(ConciergeMenuItem conciergeMenuItem) {
        for (int index = 0; index < mDatas.size(); index++) {
            ConciergeItemAdaptive adaptive = mDatas.get(index);
            if (adaptive instanceof ConciergeMenuItem) {
                if (TextUtils.equals(((ConciergeMenuItem) adaptive).getId(), conciergeMenuItem.getId())) {
                    notifyItemChanged(index);
                    break;
                }
            }
        }

        if (mConciergeNewArrivalProductSectionAdapter != null) {
            mConciergeNewArrivalProductSectionAdapter.onItemUpdatedInCart(conciergeMenuItem);
        }
    }

    public void refreshProductInCardCounter() {
        for (int index = 0; index < mDatas.size(); index++) {
            if (mDatas.get(index) instanceof ConciergeMenuItem) {
                notifyItemChanged(index);
            }
        }
    }

    public void onCartCleared() {
        for (int index = 0; index < mDatas.size(); index++) {
            ConciergeItemAdaptive adaptive = mDatas.get(index);
            if (adaptive instanceof ConciergeMenuItem) {
                ConciergeMenuItem productOnDisplay = (ConciergeMenuItem) adaptive;
                productOnDisplay.setProductCount(0);
                notifyItemChanged(index);
            }
        }

        if (mConciergeNewArrivalProductSectionAdapter != null) {
            mConciergeNewArrivalProductSectionAdapter.onCartCleared();
        }
    }

    public void checkToUpdateNoticeBannerVisibility(ConciergeShopAnnouncementResponse shopAnnouncementResponse) {
        //Normally shop notice section is in the top index of display list.
        if (shopAnnouncementResponse.isThereAnnouncement()) {
            //Will add or update the notice info section.
            if (mDatas.get(0) instanceof ConciergeShopAnnouncement) {
                mDatas.set(0, shopAnnouncementResponse.getShopAnnouncement());
                notifyItemChanged(0);
            } else {
                mDatas.add(0, shopAnnouncementResponse.getShopAnnouncement());
                notifyItemInserted(0);
            }
        } else {
            //Will remove the notice section if it exists
            if (mDatas.get(0) instanceof ConciergeShopAnnouncement) {
                mDatas.remove(0);
                notifyItemRemoved(0);
            }
        }
    }

    public void setData(List<ConciergeItemAdaptive> data) {
        mDatas = data;
        notifyDataSetChanged();
    }

    public void refreshData(List<ConciergeItemAdaptive> newData) {
        if (mDatas.isEmpty()) {
            mDatas = newData;
            notifyDataSetChanged();
        } else {
            final ConciergeShopListDiffCallback diffCallback = new ConciergeShopListDiffCallback(mDatas, newData);
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
            mDatas = newData;
            diffResult.dispatchUpdatesTo(this);
        }
    }

    public void updateProductOrderCounter(ArrayList<String> ids) {
        for (int i = 0; i < mDatas.size(); i++) {
            if (mDatas.get(i) instanceof ConciergeMenuItem) {
                for (String id : ids) {
                    if (TextUtils.equals(id, ((ConciergeMenuItem) mDatas.get(i)).getId())) {
                        notifyItemChanged(i);
                    }
                }
            }
        }
    }

    public interface ConciergeFragmentAdapterListener {

        void onProductAddedFromDetail();

        void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem);
    }
}
