package com.proapp.sompom.adapter.newadapter;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.ReferencedChat;

import java.lang.reflect.Type;

/**
 * Created by Veasna Chhom on 10/08/21.
 */

public class GSonReferenceMessageAdapter implements JsonSerializer<ReferencedChat>, JsonDeserializer<ReferencedChat> {

    private final Gson mGson = new Gson();

    @Override
    public ReferencedChat deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        /*
           Chat.FIELD_MEDIA is expecting data mapping type as single media. In case that server response with
           array of media, we will manage to map only first element from the list.
         */
        if (jsonObject != null &&
                jsonObject.has(Chat.FIELD_MEDIA) &&
                jsonObject.get(Chat.FIELD_MEDIA) instanceof JsonArray) {
            JsonArray medias = jsonObject.get(Chat.FIELD_MEDIA).getAsJsonArray();
            if (medias != null && medias.size() > 0) {
                json.getAsJsonObject().add(Chat.FIELD_MEDIA, medias.get(0));
            }
        }

        return mGson.fromJson(json, ReferencedChat.class);
    }

    @Override
    public JsonElement serialize(ReferencedChat src, Type typeOfSrc, JsonSerializationContext context) {
        return mGson.toJsonTree(src);
    }
}
