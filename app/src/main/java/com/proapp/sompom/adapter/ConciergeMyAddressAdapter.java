package com.proapp.sompom.adapter;

import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ItemConciergeMyAddressViewModel;

import java.util.List;

/**
 * Created by Or Vitovongsak on 22/11/21.
 */
public class ConciergeMyAddressAdapter extends RefreshableAdapter<ConciergeUserAddress, BindingViewHolder> {

    private ConciergeMyAddressListener mListener;

    public ConciergeMyAddressAdapter(List<ConciergeUserAddress> datas, ConciergeMyAddressListener listener) {
        super(datas);
        setCanLoadMore(false);
        mListener = listener;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_my_address).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ConciergeUserAddress conciergeUserAddress = mDatas.get(position);
        ItemConciergeMyAddressViewModel viewModel = new ItemConciergeMyAddressViewModel(holder.getContext(),
                mDatas.get(position),
                new ItemConciergeMyAddressViewModel.ItemConciergeMyAddressViewModelListener() {
                    @Override
                    public void onAddressDeleted() {
                        mDatas.remove(position);
                        notifyItemRemoved(position);
                        if (mDatas.isEmpty()) {
                            if (mListener != null) {
                                mListener.onAllItemRemoved();
                            }
                        } else {
                            if (mListener != null) {
                                mListener.onAddressDeleted(conciergeUserAddress);
                            }
                        }
                    }

                    @Override
                    public void onAddressUpdated(ConciergeUserAddress address) {
                        //Update data model of item
                        mDatas.set(position, address);

                        if (address.isPrimary()) {
                            //Reset primary status from other address items
                            checkToResetPrimaryAddressStatusFromDataItem(address.getId());
                        }

                        if (mListener != null) {
                            mListener.onAddressUpdated(address);
                        }
                    }
                });
        holder.setVariable(BR.viewModel, viewModel);
        holder.itemView.getRootView().setOnClickListener(view -> {
            if (mListener != null) {
                mListener.onItemClicked(mDatas.get(position));
            }
        });
    }

    public ConciergeUserAddress findCurrentPrimaryAddress() {
        for (ConciergeUserAddress data : mDatas) {
            if (data.isPrimary()) {
                return data;
            }
        }

        return null;
    }

    public void setData(List<ConciergeUserAddress> data) {
        mDatas = data;
        notifyDataSetChanged();
    }

    public void onNewAddressAdded(ConciergeUserAddress newAddress) {
        mDatas.add(newAddress);
        if (newAddress.isPrimary()) {
            checkToResetPrimaryAddressStatusFromDataItem(newAddress.getId());
        }
        notifyItemInserted(mDatas.size() - 1);
    }

    private void checkToResetPrimaryAddressStatusFromDataItem(String ignoreId) {
        for (int index = 0; index < mDatas.size(); index++) {
            if (TextUtils.isEmpty(ignoreId) || !TextUtils.equals(ignoreId, mDatas.get(index).getId())) {
                mDatas.get(index).setPrimary(false);
                notifyItemChanged(index);
            }
        }
    }

    public interface ConciergeMyAddressListener {
        void onAllItemRemoved();

        void onItemClicked(ConciergeUserAddress address);

        void onAddressDeleted(ConciergeUserAddress address);

        void onAddressUpdated(ConciergeUserAddress address);
    }
}
