package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ListItemCommentBinding;
import com.proapp.sompom.databinding.ListItemCommentGifBinding;
import com.proapp.sompom.databinding.ListItemCommentImageBinding;
import com.proapp.sompom.databinding.ListItemCommentLinkBinding;
import com.proapp.sompom.listener.OnCommentItemClickListener;
import com.proapp.sompom.listener.OnLoadPreviousCommentClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.LoadCommentDirection;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.CommentViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 7/23/18.
 */

public class ReplyCommentAdapter extends CommentAdapter {

    private static final int COMMENT_ITEM = 0X0100;
    private static final int GIF_ITEM = 0X0101;
    private static final int IMAGE_ITEM = 0X0102;
    private static final int LINK_ITEM = 0X0103;

    public ReplyCommentAdapter(List<Adaptive> commentList,
                               OnCommentItemClickListener onCommentItemClickListener,
                               OnLoadPreviousCommentClickListener onLoadPreviousItemClickListener) {
        super(commentList, onCommentItemClickListener, onLoadPreviousItemClickListener);
        setReplyComment();
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == GIF_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_comment_gif).build();
        } else if (viewType == IMAGE_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_comment_image).build();
        } else if (viewType == COMMENT_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_comment).build();
        } else if (viewType == LINK_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_comment_link).build();
        } else {
            return super.onCreateView(parent, viewType);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= 0 && position < mDatas.size()) {
            if (position == 0 || mDatas.get(position) instanceof LoadCommentDirection) {
                return super.getItemViewType(position);
            } else {
                if (((Comment) mDatas.get(position)).getMedia() == null || ((Comment) mDatas.get(position)).getMedia().isEmpty()) {
                    if (((Comment) mDatas.get(position)).getMetaPreview() != null &&
                            !((Comment) mDatas.get(position)).getMetaPreview().isEmpty()) {
                        return LINK_ITEM;
                    } else {
                        return COMMENT_ITEM;
                    }
                } else {
                    if (((Comment) mDatas.get(position)).getMedia().get(0).getType() == MediaType.TENOR_GIF) {
                        return GIF_ITEM;
                    } else if (((Comment) mDatas.get(position)).getMedia().get(0).getType() == MediaType.IMAGE) {
                        return IMAGE_ITEM;
                    }
                }
            }
        }
        return super.getItemViewType(position);
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListItemCommentGifBinding
                || holder.getBinding() instanceof ListItemCommentImageBinding
                || holder.getBinding() instanceof ListItemCommentBinding ||
                holder.getBinding() instanceof ListItemCommentLinkBinding) {
            CommentViewModel viewModel = new CommentViewModel(holder.getContext(),
                    position,
                    (Comment) mDatas.get(position),
                    mOnCommentItemClickListener,
                    true);
            holder.setVariable(BR.viewModel, viewModel);
        } else {
            super.onBindData(holder, position);
        }
    }

    public void addNewComment(List<Comment> comments) {
        int previousItemCount = getItemCount();
        mDatas.addAll(comments);
        notifyItemRangeInserted(previousItemCount + 1, comments.size());
    }

    public void updateComment(int index, Comment comment) {
        if (index < mDatas.size()) {
            mDatas.set(index, comment);
            notifyItemChanged(index);
        }
    }
}
