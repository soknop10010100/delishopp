package com.proapp.sompom.adapter.newadapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.proapp.sompom.newui.fragment.AbsBaseFragment;

import java.util.List;

/**
 * Created by Or Vitovonsak on 19/10/2021
 */
public class CommonViewPager2Adapter extends FragmentStateAdapter {

    private List<AbsBaseFragment> mFragmentList;

    public CommonViewPager2Adapter(FragmentManager fm, Lifecycle lifecycle, List<AbsBaseFragment> fragmentList) {
        super(fm, lifecycle);
        mFragmentList = fragmentList;
    }

    @Override
    public Fragment createFragment(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getItemCount() {
        return mFragmentList.size();
    }
}
