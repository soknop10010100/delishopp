package com.proapp.sompom.adapter.newadapter;

import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.model.concierge.ConciergeCoupon;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.ItemConciergeCouponViewModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Veasna Chhom on 3/1/22.
 */
public class ConciergeCouponAdapter extends RecyclerView.Adapter<BindingViewHolder> {

    private final List<ConciergeCoupon> mData;
    private final Map<String, ItemConciergeCouponViewModel> mViewModelMap = new HashMap<>();
    private ConciergeCouponAdapterListener mListener;

    public ConciergeCouponAdapter(List<ConciergeCoupon> data, ConciergeCouponAdapterListener listener) {
        mData = data;
        mListener = listener;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_coupon).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ConciergeCoupon conciergeCoupon = mData.get(position);
        ItemConciergeCouponViewModel viewModel = new ItemConciergeCouponViewModel(conciergeCoupon);
        holder.setVariable(BR.viewModel, viewModel);
        holder.itemView.setOnClickListener(view -> {
            viewModel.setSelection(true);
            unselectOption(conciergeCoupon.getId());
            if (mListener != null) {
                mListener.onItemSelected();
            }
        });
        mViewModelMap.put(conciergeCoupon.getId(), viewModel);
    }

    private void unselectOption(String excludeCheckingId) {
        for (Map.Entry<String, ItemConciergeCouponViewModel> entry : mViewModelMap.entrySet()) {
            if (!TextUtils.equals(entry.getKey(), excludeCheckingId) && entry.getValue() != null) {
                entry.getValue().setSelection(false);
            }
        }
    }

    public ConciergeCoupon getSelection() {
        for (ConciergeCoupon datum : mData) {
            if (datum.isSelected()) {
                return datum;
            }
        }

        return null;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public interface ConciergeCouponAdapterListener {
        void onItemSelected();
    }
}
