package com.proapp.sompom.adapter;

import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.model.ItemConciergeOrderHistoryAdaptive;

import java.util.List;

public abstract class AbsConciergeOrderHistoryAdapter<D, T extends RecyclerView.ViewHolder> extends RefreshableAdapter<D, T> {

    protected AbsConciergeOrderHistoryAdapterListener mAbsListener;

    public AbsConciergeOrderHistoryAdapter(List<D> datas, AbsConciergeOrderHistoryAdapterListener absListener) {
        super(datas);
        mAbsListener = absListener;
    }

    public interface AbsConciergeOrderHistoryAdapterListener {
        void onMessageClicked(ItemConciergeOrderHistoryAdaptive adaptive);

        void onCallClicked();

        void onEmptyTrackingOrder();
    }
}
