package com.proapp.sompom.adapter.newadapter;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.proapp.sompom.model.FullJob;
import com.proapp.sompom.model.result.User;

import java.lang.reflect.Type;

/**
 * Created by Veasna Chhom on 19/07/21.
 */

public class GSonUserAdapter implements JsonSerializer<User>, JsonDeserializer<User> {

    private final Gson mGson = new Gson();

    @Override
    public User deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        /*
            If the "lastActiveDate" of user that supposed to map as date is empty then just set it to null to avoid
            mapping error.
         */
        if (jsonObject != null &&
                jsonObject.has(User.FIELD_DATE_LAST_ACTIVITY) &&
                jsonObject.get(User.FIELD_DATE_LAST_ACTIVITY) instanceof JsonPrimitive &&
                TextUtils.isEmpty(jsonObject.get(User.FIELD_DATE_LAST_ACTIVITY).getAsString())) {
            json.getAsJsonObject().add(User.FIELD_DATE_LAST_ACTIVITY, null);
        }

        /*
            Add protection for the "job" mapping field from server that is expected to be @{FullJob}
            because some APIs server response this field with only as String value.
         */
        FullJob fullJob = null;
        if (jsonObject != null &&
                jsonObject.has(User.FIELD_JOB) &&
                !(jsonObject.get(User.FIELD_JOB) instanceof JsonPrimitive)) {
            fullJob = mGson.fromJson((jsonObject.get(User.FIELD_JOB).getAsJsonObject()), FullJob.class);
        }

        User user = mGson.fromJson(json, User.class);
        user.setFullJob(fullJob);

        return user;
    }

    @Override
    public JsonElement serialize(User src, Type typeOfSrc, JsonSerializationContext context) {
        return mGson.toJsonTree(src);
    }
}
