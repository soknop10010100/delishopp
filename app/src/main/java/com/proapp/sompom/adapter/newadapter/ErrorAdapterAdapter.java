package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.LayoutEmptyDataErrorBinding;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.model.emun.ErrorLoadingType;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.ListItemEmptyDataErrorViewModel;
import com.proapp.sompom.viewmodel.ListItemErrorLoadingViewModel;

/**
 * Created by He Rotha on 11/6/17.
 */

public class ErrorAdapterAdapter extends RecyclerView.Adapter<BindingViewHolder> {
    private ErrorLoadingType mErrorLoadingType;
    private String mErrorMessage;
    private String mErrorTitle;
    private OnClickListener mOnClickListener;
    private boolean mIsShowRetry;

    public ErrorAdapterAdapter(ErrorLoadingType errorLoadingType,
                               String errorTitle,
                               String errorMessage,
                               boolean isShowRetry,
                               OnClickListener onClickListener) {
        mErrorLoadingType = errorLoadingType;
        mIsShowRetry = isShowRetry;
        mErrorTitle = errorTitle;
        mErrorMessage = errorMessage;
        mOnClickListener = onClickListener;
    }

    @Override
    @NonNull
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mErrorLoadingType == ErrorLoadingType.EMPTY_DATA) {
            return new BindingViewHolder.Builder(parent, R.layout.layout_empty_data_error).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.layout_base_error).build();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof LayoutEmptyDataErrorBinding) {
            holder.setVariable(BR.viewModel,
                    new ListItemEmptyDataErrorViewModel(holder.getContext(),
                            mErrorLoadingType,
                            mErrorTitle,
                            mErrorMessage));
        } else {
            holder.setVariable(BR.viewModel,
                    new ListItemErrorLoadingViewModel(holder.getContext(),
                            mErrorLoadingType,
                            mErrorMessage,
                            mIsShowRetry,
                            mOnClickListener));
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }
}
