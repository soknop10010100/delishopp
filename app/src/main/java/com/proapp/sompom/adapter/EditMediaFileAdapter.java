package com.proapp.sompom.adapter;

import android.os.Build;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ListItemEditMediaFileBinding;
import com.proapp.sompom.listener.OnRemoveMediaFileItemListener;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.utils.VolumePlaying;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewholder.TimelineViewHolder;
import com.proapp.sompom.viewmodel.ListItemEditMediaFileViewModel;
import com.proapp.sompom.widget.lifestream.MediaLayout;

/**
 * Created by He Rotha on 9/6/17.
 */

public class EditMediaFileAdapter extends RecyclerView.Adapter<BindingViewHolder> {
    private final LifeStream mLifeStream;
    private final OnRemoveMediaFileItemListener mOnCallback;
    private final boolean mIsEdit;

    public EditMediaFileAdapter(LifeStream lifeStream,
                                boolean isEdit,
                                OnRemoveMediaFileItemListener onCallback) {
        mLifeStream = lifeStream;
        mIsEdit = isEdit;
        mOnCallback = onCallback;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItemEditMediaFileBinding binding;
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.list_item_edit_media_file, parent, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.mediaLayouts.setTransitionName(parent.getResources().getString(R.string.media_transition_anim) + viewType);
        }
        return new TimelineViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ListItemEditMediaFileBinding binding = (ListItemEditMediaFileBinding) holder.getBinding();
        Media media = mLifeStream.getMedia().get(position);
        MediaLayout mediaLayout = binding.mediaLayouts;
        mediaLayout.setLayoutParams(getLayoutParam(mediaLayout, media));
        mediaLayout.setCenterCropImage();
        mediaLayout.setMedia(media);
        mediaLayout.setVisibleCloseButton(mIsEdit);
        if (mIsEdit) {
            mediaLayout.setMute(VolumePlaying.isMute(mediaLayout.getContext()));
            mediaLayout.setVisibleMuteButton(true);
        }
        mediaLayout.setOnCloseButtonClickedListener(v -> {
            if (mOnCallback != null) {
                mOnCallback.onRemoveItem(holder.getAdapterPosition());
            }
        });
        if (media.getType() == MediaType.VIDEO) {
            mediaLayout.setShowControl(true);
            mediaLayout.setShouldEnableResizeVideoIcon(false);
            mediaLayout.setSaveOnPause(true);
            if (holder instanceof TimelineViewHolder) {
                ((TimelineViewHolder) holder).setMediaLayout(mediaLayout, null);
            }
        }

        ListItemEditMediaFileViewModel lifeStreamViewModel = new ListItemEditMediaFileViewModel(media.getTitle(),
                new ListItemEditMediaFileViewModel.ViewModelListener() {
                    @Override
                    public void onTextChanged(String text) {
                        mLifeStream.getMedia().get(position).setTitle(text);
                    }

                    @Override
                    public void onCrossButtonClicked() {

                    }
                });
        holder.setVariable(BR.viewModel, lifeStreamViewModel);
    }

    @Override
    public int getItemCount() {
        return mLifeStream.getMedia().size();
    }

    @Override
    public void onViewRecycled(@NonNull BindingViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder instanceof TimelineViewHolder) {
            ((TimelineViewHolder) holder).stopOrRelease();
        }
    }

    private ViewGroup.LayoutParams getLayoutParam(MediaLayout mediaLayout, Media media) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((AppCompatActivity) mediaLayout.getContext()).getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int screenWidth = metrics.widthPixels;
        ViewGroup.LayoutParams param = mediaLayout.getLayoutParams();
        int size;
        if (media.getWidth() == 0 || media.getHeight() == 0) {
            size = screenWidth;
        } else {
            size = media.getHeight() * screenWidth / media.getWidth();
        }
        param.width = screenWidth;
        param.height = size;
        return param;
    }
}
