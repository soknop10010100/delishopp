package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.viewholder.BindingViewHolder;

/**
 * Created by He Rotha on 11/6/17.
 */

public class PlaceHolderAdapter extends RecyclerView.Adapter<BindingViewHolder> {

    public PlaceHolderAdapter() {
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_home_placeholder).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }
}
