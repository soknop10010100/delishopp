package com.proapp.sompom.adapter.newadapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeShopListDiffCallback;
import com.proapp.sompom.helper.DelayViewClickListener;
import com.proapp.sompom.intent.ConciergeSupplierDetailIntent;
import com.proapp.sompom.model.concierge.ConciergeSupplier;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.ListItemConciergeSupplierViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Veasna Chhom on 5/3/22.
 */
public class ConciergeSupplierSectionAdapter extends RecyclerView.Adapter<BindingViewHolder> {

    private List<ConciergeSupplier> mData;

    public ConciergeSupplierSectionAdapter(List<ConciergeSupplier> data) {
        mData = data;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_supplier).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ConciergeSupplier supplier = (ConciergeSupplier) mData.get(position);
        ListItemConciergeSupplierViewModel viewModel = new ListItemConciergeSupplierViewModel(holder.getContext(), supplier);
        holder.setVariable(BR.viewModel, viewModel);
        holder.itemView.setOnClickListener(new DelayViewClickListener() {
            @Override
            public void onDelayClick(@NonNull View widget) {
                holder.getContext().startActivity(new ConciergeSupplierDetailIntent(holder.getContext(), supplier));
            }
        });
    }

    public void refreshData(List<ConciergeSupplier> newData) {
        if (mData.isEmpty()) {
            mData = newData;
            notifyDataSetChanged();
        } else {
            final ConciergeShopListDiffCallback diffCallback = new ConciergeShopListDiffCallback(new ArrayList<>(mData), new ArrayList<>(newData));
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
            mData = newData;
            diffResult.dispatchUpdatesTo(this);
        }
    }

    public void addLoadMoreData(List<ConciergeSupplier> datas) {
        int previousItemCount = getItemCount();
        mData.addAll(datas);
        notifyItemRangeInserted(previousItemCount + 1, datas.size());
    }
}
