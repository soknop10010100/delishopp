package com.proapp.sompom.adapter.newadapter;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.upload.LifeStreamUploadService;
import com.proapp.sompom.helper.TimelineDiffCallback;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.listener.OnTimelineActiveUserItemClick;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.ActiveUser;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.CreateTimelineItem;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.MoreGameItem;
import com.proapp.sompom.model.PostLifeStream;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.WelcomeItem;
import com.proapp.sompom.model.emun.ViewType;
import com.proapp.sompom.model.result.MoreGame;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.GlideLoadUtil;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewholder.SupportPlacePreviewViewHolder;
import com.proapp.sompom.viewmodel.ListItemHomeWelcomeViewModel;
import com.proapp.sompom.viewmodel.ListItemPostViewModel;
import com.proapp.sompom.viewmodel.ViewPagerViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ItemPostingViewModel;

import java.util.List;

import timber.log.Timber;

/**
 * Created by He Rotha on 7/10/18.
 */
public class HomeTimelineAdapter extends TimelineAdapter {

    private static final int WELCOME_TIMELINE_FIX_INDEX = 0;
    private static final int CREATE_TIMELINE_FIX_INDEX = 1;

    private FragmentManager mFragmentManager;
    private ActiveUser mActiveUser;
    private final OnTimelineActiveUserItemClick mOnTimelineActiveUserItemClick;
    private List<MoreGame> mMoreGameList;
    private CreateTimelineItem mCreateTimelineItem;
    private final Context mContext;
    private LifeStreamUploadService.UploadBinder mBinder;
    private ServiceConnection mPostingServiceConnection;
    private LifeStream mWelcomePostTimelineItem;
    private ListItemPostViewModel mCreateNewPostViewModel;

    public HomeTimelineAdapter(AbsBaseActivity context,
                               ApiService apiService,
                               OnTimelineActiveUserItemClick itemClick,
                               OnTimelineItemButtonClickListener onItemClickListener,
                               boolean checkLikeComment) {
        super(context, apiService, null, onItemClickListener, checkLikeComment);
        mOnTimelineActiveUserItemClick = itemClick;
        mContext = context;
        checkToCreateWelcomePostItem();
        addWelcomeItem(mDatas);
        bindPostTimelineService();
    }

    public void bindRecyclerViewListener(RecyclerView recyclerView) {
        recyclerView.setRecyclerListener(viewHolder -> {
            if (viewHolder instanceof SupportPlacePreviewViewHolder) {
                ((SupportPlacePreviewViewHolder) viewHolder).clearPlaceView();
            }
        });
    }

    private void checkToCreateWelcomePostItem() {
        mWelcomePostTimelineItem = WallStreetHelper.createWelcomePost(mContext);
    }

    private void addOrRemoveWelcomePostItem(boolean willAdd) {
        if (mWelcomePostTimelineItem != null) {
            if (willAdd) {
                mDatas.add(mWelcomePostTimelineItem);
                notifyItemInserted(mDatas.size() - 1);
                if (mCreateTimelineItem != null) {
                    mCreateTimelineItem.setEnableBottomPostSeparator(true);
                }
                if (mCreateNewPostViewModel != null) {
                    mCreateNewPostViewModel.setBottomLineVisibility(true);
                }
            } else {
                int index = findPostById(WallStreetHelper.WELCOME_POST_ID);
                if (index >= 0) {
                    mDatas.remove(index);
                    notifyItemRemoved(index);
                    if (mCreateNewPostViewModel != null) {
                        mCreateNewPostViewModel.setBottomLineVisibility(false);
                    }
                }
            }
        }
    }

    private int findPostById(String id) {
        int index = -1;
        for (int i = 0; i < mDatas.size(); i++) {
            if (mDatas.get(i) instanceof LifeStream &&
                    TextUtils.equals(id, mDatas.get(i).getId())) {
                index = i;
                break;
            }
        }

        return index;
    }

    private void bindPostTimelineService() {
        mPostingServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mBinder = (LifeStreamUploadService.UploadBinder) service;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
            }
        };

        mContext.bindService(new Intent(mContext, LifeStreamUploadService.class),
                mPostingServiceConnection,
                AppCompatActivity.BIND_AUTO_CREATE);
    }

    private void startDownloadPreviewLinkImageIfNecessary(List<Adaptive> adaptiveList) {
        for (Adaptive adaptive : adaptiveList) {
            if (WallStreetHelper.shouldRenderLinkPreview(adaptive)) {
                GlideLoadUtil.preCacheImage(mContext, ((LifeStream) adaptive).getLinkPreviewModel().getImage(),
                        null);
            }
        }
    }

    private void preloadImageIfNecessary(List<Adaptive> adaptiveList) {
        for (Adaptive adaptive : adaptiveList) {
            if (adaptive.getMedia() != null && !adaptive.getMedia().isEmpty()) {
                for (Media media : adaptive.getMedia()) {
                    GlideLoadUtil.preCacheImage(mContext, media.getThumbnail(), null);
                }
            }
        }
    }

    @Override
    public void addLoadMoreData(List<Adaptive> datas) {
        preloadImageIfNecessary(datas);
        super.addLoadMoreData(datas);
        startDownloadPreviewLinkImageIfNecessary(datas);
    }

    public void unbindPostServiceConnection() {
        mContext.unbindService(mPostingServiceConnection);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == ViewType.ActiveUser.getTimelineViewType()) {
            BindingViewHolder holder = new BindingViewHolder.Builder(parent, R.layout.list_item_timeline_activite_user).build();
            RecyclerView recyclerView = holder.itemView.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(parent.getContext(), LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setAdapter(new TimelineActiveUserAdapter(mActiveUser, mOnTimelineActiveUserItemClick));
            return holder;
        } else if (viewType == ViewType.ViewPagerAd.getTimelineViewType()) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_viewpager).build();
        } else if (viewType == ViewType.NewPost.getTimelineViewType()) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_new_post).build();
        } else if (viewType == ViewType.POSTING_WALL.getTimelineViewType()) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_posting).build();
        } else if (viewType == ViewType.GREETING_ITEM.getTimelineViewType()) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_home_welcome).build();
        }

        return super.onCreateView(parent, viewType);
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (mDatas.get(position) instanceof MoreGameItem) {
            ViewPagerViewModel model = new ViewPagerViewModel(mMoreGameList, mFragmentManager);
            holder.setVariable(BR.viewModel, model);
        } else if (mDatas.get(position) instanceof CreateTimelineItem) {
            mCreateNewPostViewModel = new ListItemPostViewModel(getAbsBaseActivity(),
                    mCreateTimelineItem,
                    lifeStream -> {
                        Timber.i("onPrepareToPostWallItem: " + new Gson().toJson(lifeStream));
                        addPostingLifeStreamItem(lifeStream);

                    });
            holder.setVariable(BR.viewModel, mCreateNewPostViewModel);
        } else if (mDatas.get(position) instanceof PostLifeStream) {
            PostLifeStream postLifeStream = (PostLifeStream) mDatas.get(position);
            final ItemPostingViewModel viewModel = new ItemPostingViewModel(mContext,
                    postLifeStream,
                    findExistPostProgression(postLifeStream.getId()));
            viewModel.setListener(new ItemPostingViewModel.ItemPostingViewModelListener() {
                @Override
                public void onCancelClicked(LifeStream lifeStream) {
                    if (mBinder != null) {
                        mBinder.onRemovePosting(lifeStream.getId());
                    }
                    mDatas.remove(postLifeStream);
                    notifyItemRemoved(holder.getAdapterPosition());
                }

                @Override
                public void onRetryClicked(LifeStream lifeStream) {
                    viewModel.onRetry(mContext);
                    if (mBinder != null) {
                        mBinder.onRetryPosting(lifeStream);
                    }
                }
            });
            if (mBinder != null) {
                mBinder.addListener(new LifeStreamUploadService.LifeStreamUploadServiceListener() {
                    @Override
                    public void onPostSuccess(LifeStream lifeStream, String id) {
                        if (mBinder != null) {
                            mBinder.removeListenerById(postLifeStream.getId());
                        }
                        viewModel.updatePostProgression(100);
                        int adapterPosition = holder.getAdapterPosition();
                        if (adapterPosition >= 0 && adapterPosition < mDatas.size()) {
                            mDatas.remove(adapterPosition);
                            notifyItemRemoved(adapterPosition);
                            if (lifeStream.isEditedMode()) {
                                update(lifeStream);
                            } else {
                                onAddPostSuccessItem(lifeStream);
                            }
                        }
                    }

                    @Override
                    public void onProgressPosting(String id, int progress, int total) {
                        viewModel.updatePostProgression(progress);
                    }

                    @Override
                    public void onPostFailed(LifeStream lifeStream, int notificationId) {
                        viewModel.onPostError(mContext, lifeStream, notificationId);
                    }

                    @Override
                    public String getId() {
                        return postLifeStream.getLifeStream().getId();
                    }
                });
            }
            if (!postLifeStream.isPosting()) {
                postLifeStream.setPosting(true);
                //Proceed posting in background
                Intent uploadService = LifeStreamUploadService.getIntent(mContext,
                        postLifeStream.getLifeStream(),
                        false);
                mContext.startService(uploadService);
            }
            holder.setVariable(BR.viewModel, viewModel);
        } else if (mDatas.get(position) instanceof WelcomeItem) {
            ListItemHomeWelcomeViewModel viewModel = new ListItemHomeWelcomeViewModel(holder.itemView.getContext(),
                    (WelcomeItem) mDatas.get(position));
            holder.setVariable(BR.viewModel, viewModel);
        } else {
            super.onBindData(holder, position);
        }
    }

    public void beginPostUpdate(LifeStream lifeStream) {
        addPostingLifeStreamItem(lifeStream);
    }

    private int findExistPostProgression(String id) {
        if (mBinder != null) {
            return mBinder.findExistingUploadProgression(id);
        }

        return 0;
    }

    private void onAddPostSuccessItem(LifeStream lifeStream) {
        int indexToAddSuccessPost = findIndexToAddSuccessPost();
        mDatas.add(indexToAddSuccessPost, lifeStream);
        notifyItemInserted(indexToAddSuccessPost);
        addOrRemoveWelcomePostItem(false);
    }

    private void addPostingLifeStreamItem(LifeStream lifeStream) {
        PostLifeStream postLifeStream = new PostLifeStream(lifeStream);
        int lastPostingTimelineItem = findLastAddPostingTimelineItem();
        if (lastPostingTimelineItem >= 0) {
            mDatas.add(lastPostingTimelineItem, postLifeStream);
            notifyItemInserted(lastPostingTimelineItem);
        } else {
            if (mDatas.size() > 1) {
                //Add just after create timeline view
                mDatas.add(CREATE_TIMELINE_FIX_INDEX + 1, postLifeStream);
                notifyItemInserted(CREATE_TIMELINE_FIX_INDEX + 1);
            }
        }
    }

    private int findLastAddPostingTimelineItem() {
        for (int i = 0; i < mDatas.size(); i++) {
            if (mDatas.get(i) instanceof PostLifeStream) {
                return i;
            }
        }

        return -1;
    }

    private int findMostAddPostingTimelineItem() {
        for (int size = mDatas.size() - 1; size >= 0; size--) {
            if (mDatas.get(size) instanceof PostLifeStream) {
                return size;
            }
        }

        return -1;
    }

    private int findIndexToAddSuccessPost() {
        int lastPostingTimelineItem = findMostAddPostingTimelineItem();
        if (lastPostingTimelineItem >= 0) {
            return lastPostingTimelineItem + 1;
        } else {
            //The fist item is wall posting view.
            return CREATE_TIMELINE_FIX_INDEX + 1;
        }
    }

    public void setActiveUser(ActiveUser activeUser) {
        mActiveUser = new ActiveUser();
        mActiveUser.clear();
        mActiveUser.addAll(activeUser);
        mDatas.set(1, mActiveUser);
        notifyItemChanged(1);
    }

    public void checkToAddWelcomePost(List<Adaptive> data) {
        if (data == null || data.isEmpty()) {
            int index = findPostById(WallStreetHelper.WELCOME_POST_ID);
            if (index < 0 && !isThereAnyPostDisplay()) {
                addOrRemoveWelcomePostItem(true);
            }
        }
    }

    private boolean isThereAnyPostDisplay() {
        for (Adaptive data : mDatas) {
            if (data instanceof LifeStream) {
                return true;
            }
        }

        return false;
    }

    public void setRefreshData(List<Adaptive> data) {
        if (data != null && !data.isEmpty()) {
            preloadImageIfNecessary(data);
            injectStaticItemsIfNecessary(data);
            Timber.i("setRefreshData: old size: " + mDatas.size() +
                    ", refresh size: " + data.size() + ", canLoadMore: " + canLoadMore());
            if (mDatas.isEmpty()) {
                mDatas = data;
                notifyDataSetChanged();
            } else {
                final TimelineDiffCallback diffCallback = new TimelineDiffCallback(mDatas, data);
                final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
                mDatas.clear();
                mDatas.addAll(data);
                diffResult.dispatchUpdatesTo(this);
            }
            startDownloadPreviewLinkImageIfNecessary(data);
        }
    }

    private void injectStaticItemsIfNecessary(List<Adaptive> refreshData) {
        if (!mDatas.isEmpty() &&
                refreshData != null &&
                !refreshData.isEmpty()) {
            addWelcomeItem(refreshData);
            if (mDatas.get(CREATE_TIMELINE_FIX_INDEX) instanceof CreateTimelineItem) {
                refreshData.add(CREATE_TIMELINE_FIX_INDEX, mDatas.get(CREATE_TIMELINE_FIX_INDEX));
            }
        }
    }

    private void addWelcomeItem(List<Adaptive> dataList) {
        UserHelper.loadWelcomeData(mContext, new UserHelper.UserHelperListener() {
            @Override
            protected void onLoadWelcomeDataSuccess(WelcomeItem welcomeItem) {
                if (welcomeItem != null) {
                    dataList.add(WELCOME_TIMELINE_FIX_INDEX, welcomeItem);
                }
                checkToAddCreatePostLayout();
            }
        });
    }

    private void checkToAddCreatePostLayout() {
        if (UserHelper.isAbleToCreatePost(mContext)) {
            mCreateTimelineItem = new CreateTimelineItem();
            mDatas.add(mCreateTimelineItem);
        }
    }

    public void updatePostContentState(WallStreetAdaptive adaptive) {
        /*
        Will check to update only content state.
         */
        for (int i = 0; i < mDatas.size(); i++) {
            if (TextUtils.equals(mDatas.get(i).getId(), adaptive.getId())) {
                ((WallStreetAdaptive) mDatas.get(i)).setContentStat(adaptive.getContentStat());
                updateItem(mDatas.get(i), i);
                break;
            }
        }
    }
}
