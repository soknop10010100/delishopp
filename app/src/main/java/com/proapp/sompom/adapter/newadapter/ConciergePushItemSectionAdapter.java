package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeShopListDiffCallback;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemConciergePushItemViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Or Vitovongsak on 13/1/22.
 */
public class ConciergePushItemSectionAdapter extends RecyclerView.Adapter<BindingViewHolder> {

    private List<ConciergeMenuItem> mData;

    public ConciergePushItemSectionAdapter(List<ConciergeMenuItem> data) {
        this.mData = data;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_push_item).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ListItemConciergePushItemViewModel viewModel =
                new ListItemConciergePushItemViewModel(holder.getContext(), mData.get(position));
        holder.setVariable(BR.viewModel, viewModel);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void refreshData(List<ConciergeMenuItem> newData) {
        if (mData.isEmpty()) {
            mData = newData;
            notifyDataSetChanged();
        } else {
            final ConciergeShopListDiffCallback diffCallback =
                    new ConciergeShopListDiffCallback(new ArrayList<>(mData), new ArrayList<>(newData));
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
            mData = newData;
            diffResult.dispatchUpdatesTo(this);
        }
    }
}
