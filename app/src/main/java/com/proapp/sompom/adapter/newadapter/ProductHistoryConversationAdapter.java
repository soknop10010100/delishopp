package com.proapp.sompom.adapter.newadapter;

import android.content.Context;
import android.view.ViewGroup;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.listener.OnCompleteListener;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemProductHistoryConversation;

import java.util.List;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class ProductHistoryConversationAdapter extends RefreshableAdapter<Conversation, BindingViewHolder> {
    private final Context mContext;
    private final OnCompleteListener<Conversation> mOnItemClickListener;

    public ProductHistoryConversationAdapter(Context context,
                                             List<Conversation> datas,
                                             OnCompleteListener<Conversation> listener) {
        super(datas);
        mContext = context;
        mOnItemClickListener = listener;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_product_hitory_conversation).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        final ListItemProductHistoryConversation viewModel = new ListItemProductHistoryConversation(mContext,
                mDatas.get(position),
                mOnItemClickListener);
        holder.setVariable(BR.viewModel, viewModel);
    }

}
