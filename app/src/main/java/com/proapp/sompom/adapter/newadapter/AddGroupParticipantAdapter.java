package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemAddGroupParticipantViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Veasna Chhom on 8/10/20.
 */

public class AddGroupParticipantAdapter extends RecyclerView.Adapter<BindingViewHolder> {

    private List<User> mData;

    public AddGroupParticipantAdapter(List<User> data) {
        mData = data;
    }

    public List<String> getSelectedUserIdList() {
        List<String> ids = new ArrayList<>();
        for (User datum : mData) {
            if (datum.getSelected()) {
                ids.add(datum.getId());
            }
        }
        return ids;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_add_group_participant).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ListItemAddGroupParticipantViewModel viewModel = new ListItemAddGroupParticipantViewModel(mData.get(position));
        holder.setVariable(BR.viewModel, viewModel);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
