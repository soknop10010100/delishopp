package com.proapp.sompom.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.AbsConciergeProductItemDisplayAdapter;
import com.proapp.sompom.databinding.ListConciergeLoadMoreSubCategoryItemBinding;
import com.proapp.sompom.databinding.ListConciergeShopItemBinding;
import com.proapp.sompom.databinding.ListItemConciergeGridProductBinding;
import com.proapp.sompom.databinding.ListItemConciergeSmallGridProductBinding;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeSubCategoryLoadMoreDataModel;
import com.proapp.sompom.model.concierge.ConciergeSupportItemAndSubCategoryAdaptive;
import com.proapp.sompom.model.emun.ConciergeViewItemByType;
import com.proapp.sompom.newui.fragment.AbsSupportShopCheckOutFragment;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.ListConciergeShopItemViewModel;
import com.proapp.sompom.viewmodel.ListLoadMoreConciergeSubCategoryItemViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemConciergeProductViewModel;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.List;

/**
 * Created by Veasna Chhom on 4/26/22.
 */

public class ConciergeSubCategoryItemAdapter extends RecyclerView.Adapter<BindingViewHolder> implements AbsConciergeProductItemDisplayAdapter {

    private static final int NORMAL_ITEM_VIEW_TYPE = 0x0011;
    private static final int GRID_ITEM_WITH_TWO_COLUMNS_VIEW_TYPE = 0x0012;
    private static final int GRID_ITEM_WITH_THREE_COLUMNS_VIEW_TYPE = 0x0013;
    private static final int LOAD_MORE_ITEM_VIEW_TYPE = 0x0014;

    private final Context mContext;
    private final List<ConciergeSupportItemAndSubCategoryAdaptive> mData;
    private final ConciergeSubCategoryItemAdapterCallback mListener;
    private ListLoadMoreConciergeSubCategoryItemViewModel mLoadMoreViewModel;
    private final ConciergeViewItemByType mConciergeViewItemByType;
    private boolean mIsHasMoreItemAfterAddedLoadMoreItem;

    public ConciergeSubCategoryItemAdapter(Context context,
                                           List<ConciergeSupportItemAndSubCategoryAdaptive> data,
                                           ConciergeViewItemByType conciergeViewItemByType,
                                           ConciergeSubCategoryItemAdapterCallback listener) {
        mContext = context;
        mData = data;
        mConciergeViewItemByType = conciergeViewItemByType;
        mListener = listener;
        for (ConciergeSupportItemAndSubCategoryAdaptive datum : mData) {
            if (datum instanceof ConciergeSubCategoryLoadMoreDataModel) {
                mIsHasMoreItemAfterAddedLoadMoreItem = true;
                break;
            }
        }
    }

    @Override
    public boolean shouldApplyGridSpacing(int itemPosition) {
        int itemViewType = getItemViewType(itemPosition);
        return itemViewType == GRID_ITEM_WITH_TWO_COLUMNS_VIEW_TYPE || itemViewType == GRID_ITEM_WITH_THREE_COLUMNS_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    public int getSpanCountOfItemBaseOnItsModel(int position) {
        int viewType = getItemViewType(position);
        //Will manage to display one item view for a whole row for load more or sub category view types
        if (viewType == GRID_ITEM_WITH_TWO_COLUMNS_VIEW_TYPE || viewType == GRID_ITEM_WITH_THREE_COLUMNS_VIEW_TYPE) {
            return 1;
        }

        return mConciergeViewItemByType.getSpanCount();
    }

    @Override
    public boolean hasLoadMore() {
        return mIsHasMoreItemAfterAddedLoadMoreItem;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < 0) {
            return -1;
        }

        if (mData.get(position) instanceof ConciergeSubCategoryLoadMoreDataModel) {
            return LOAD_MORE_ITEM_VIEW_TYPE;
        } else {
            if (mConciergeViewItemByType == ConciergeViewItemByType.NORMAL) {
                return NORMAL_ITEM_VIEW_TYPE;
            } else if (mConciergeViewItemByType == ConciergeViewItemByType.TWO_COLUMNS) {
                return GRID_ITEM_WITH_TWO_COLUMNS_VIEW_TYPE;
            } else {
                return GRID_ITEM_WITH_THREE_COLUMNS_VIEW_TYPE;
            }
        }
    }

    @NonNull
    @Override

    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == LOAD_MORE_ITEM_VIEW_TYPE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_concierge_load_more_sub_category_item).build();
        } else if (viewType == GRID_ITEM_WITH_TWO_COLUMNS_VIEW_TYPE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_grid_product).build();
        } else if (viewType == GRID_ITEM_WITH_THREE_COLUMNS_VIEW_TYPE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_small_grid_product).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_concierge_shop_item).build();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListConciergeShopItemBinding) {
            ConciergeMenuItem conciergeMenuItem = (ConciergeMenuItem) mData.get(position);
            ListConciergeShopItemViewModel viewModel = new ListConciergeShopItemViewModel(holder.getContext(),
                    null,
                    conciergeMenuItem,
                    false,
                    getProductListener(conciergeMenuItem, position));
            holder.setVariable(BR.viewModel, viewModel);
        } else if (holder.getBinding() instanceof ListConciergeLoadMoreSubCategoryItemBinding) {
            mIsHasMoreItemAfterAddedLoadMoreItem = true;
            mLoadMoreViewModel = new ListLoadMoreConciergeSubCategoryItemViewModel((ConciergeSubCategoryLoadMoreDataModel) mData.get(position),
                    mConciergeViewItemByType != ConciergeViewItemByType.NORMAL,
                    loadMoreDataModel -> {
                        if (mListener != null && loadMoreDataModel.getConciergeSubCategory() != null &&
                                !TextUtils.isEmpty(loadMoreDataModel.getConciergeSubCategory().getNextPage())) {
                            mListener.onSubCategoryPerformLoadMoreItem(loadMoreDataModel);
                        }
                    });
            holder.setVariable(BR.viewModel, mLoadMoreViewModel);
        } else if (holder.getBinding() instanceof ListItemConciergeGridProductBinding) {
            ConciergeMenuItem conciergeMenuItem = (ConciergeMenuItem) mData.get(position);
            int itemAmountInBasket = ConciergeCartHelper.getExistingProductCountInCart(conciergeMenuItem);
            ListItemConciergeProductViewModel viewModel = new ListItemConciergeProductViewModel(holder.getContext(),
                    conciergeMenuItem,
                    itemAmountInBasket,
                    true,
                    mConciergeViewItemByType,
                    position,
                    getProductListener(conciergeMenuItem, position));
            holder.setVariable(BR.viewModel, viewModel);
        } else if (holder.getBinding() instanceof ListItemConciergeSmallGridProductBinding) {
            ConciergeMenuItem conciergeMenuItem = (ConciergeMenuItem) mData.get(position);
            int itemAmountInBasket = ConciergeCartHelper.getExistingProductCountInCart(conciergeMenuItem);
            ListItemConciergeProductViewModel viewModel = new ListItemConciergeProductViewModel(holder.getContext(),
                    conciergeMenuItem,
                    itemAmountInBasket,
                    true,
                    mConciergeViewItemByType,
                    position,
                    getProductListener(conciergeMenuItem, position));
            holder.setVariable(BR.viewModel, viewModel);
        }
    }

    private ListItemConciergeProductViewModel.ListItemConciergeProductViewModelListener getProductListener(ConciergeMenuItem conciergeMenuItem, int itemPosition) {
        return new ListItemConciergeProductViewModel.ListItemConciergeProductViewModelListener() {
            @Override
            public void onProductAddedFromDetail() {
                if (mListener != null) {
                    mListener.onProductAddedFromDetail();
                }
            }

            @Override
            public void onProductFailToAdd() {
                notifyItemChanged(itemPosition);
            }

            @Override
            public void onItemCountChanged(int newCount, boolean isRemove) {
                if (mContext instanceof AppCompatActivity) {
                    conciergeMenuItem.setProductCount(newCount);
                    Intent intent = new Intent(isRemove ?
                            AbsSupportShopCheckOutFragment.CART_ITEM_REMOVED_EVENT :
                            AbsSupportShopCheckOutFragment.CART_ITEM_ADDED_EVENT);
                    intent.putExtra(AbsSupportShopCheckOutFragment.CART_EVENT_DATA, conciergeMenuItem);
                    SendBroadCastHelper.verifyAndSendBroadCast(mContext, intent);
                }
            }

            @Override
            public void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {
                if (mListener != null) {
                    mListener.onAddFirstItemToCart(conciergeMenuItem);
                }
            }
        };
    }

    public void addLoadMoreItem(List<ConciergeSupportItemAndSubCategoryAdaptive> moreItems, boolean hasLoadMore) {
        mIsHasMoreItemAfterAddedLoadMoreItem = hasLoadMore;
        if (mLoadMoreViewModel != null) {
            mLoadMoreViewModel.setPerformLoadingMoreStatus(false);
        }
        if (moreItems != null && !moreItems.isEmpty()) {
            int positionAboveLoadMoreView = findPositionAboveLoadMoreView();
            mData.addAll(positionAboveLoadMoreView, moreItems);
            notifyItemRangeInserted(positionAboveLoadMoreView, moreItems.size());
        }

        if (!hasLoadMore) {
            removeLoadMoreItemView();
        }
    }

    private int findPositionAboveLoadMoreView() {
        for (int size = mData.size() - 1; size >= 0; size--) {
            if (mData.get(size) instanceof ConciergeSubCategoryLoadMoreDataModel) {
                return size;
            }
        }

        return mData.size() - 1;
    }

    private void removeLoadMoreItemView() {
        boolean hasRemovedLoadMore = false;
        for (int size = mData.size() - 1; size >= 0; size--) {
            if (mData.get(size) instanceof ConciergeSubCategoryLoadMoreDataModel) {
                mData.remove(size);
                notifyItemRemoved(size);
                hasRemovedLoadMore = true;
                break;
            }
        }

        if (hasRemovedLoadMore) {
            //Notify last item to insert bottom space.
            notifyItemChanged(mData.size() - 1);
        }
    }

    public void onCartCleared() {
        for (int index = 0; index < mData.size(); index++) {
            if (mData.get(index) instanceof ConciergeMenuItem) {
                ConciergeMenuItem productOnDisplay = (ConciergeMenuItem) mData.get(index);
                productOnDisplay.setProductCount(0);
                notifyItemChanged(index);
            }
        }
    }

    public boolean onItemUpdatedInCart(ConciergeMenuItem conciergeMenuItem) {
        for (int index = 0; index < mData.size(); index++) {
            if (mData.get(index) instanceof ConciergeMenuItem) {
                if (TextUtils.equals(((ConciergeMenuItem) mData.get(index)).getId(), conciergeMenuItem.getId())) {
                    notifyItemChanged(index);
                    return true;
                }
            }
        }

        return false;
    }

    public interface ConciergeSubCategoryItemAdapterCallback {
        void onProductAddedFromDetail();

        void onSubCategoryPerformLoadMoreItem(ConciergeSubCategoryLoadMoreDataModel loadMoreDataModel);

        void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem);
    }
}
