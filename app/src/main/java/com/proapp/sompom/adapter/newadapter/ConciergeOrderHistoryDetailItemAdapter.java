package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.model.result.ConciergeOrderItem;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ItemConciergeOrderHistoryDetailItemViewModel;

import java.util.List;

/**
 * Created by Veasna Chhom on 10/9/21.
 */

public class ConciergeOrderHistoryDetailItemAdapter extends RefreshableAdapter<ConciergeOrderItem, BindingViewHolder> {

    public ConciergeOrderHistoryDetailItemAdapter(List<ConciergeOrderItem> data) {
        super(data);
        setCanLoadMore(false);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_order_history_detail_item).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ItemConciergeOrderHistoryDetailItemViewModel viewModel =
                new ItemConciergeOrderHistoryDetailItemViewModel(holder.getContext(), mDatas.get(position));
        holder.setVariable(BR.viewModel, viewModel);
    }
}
