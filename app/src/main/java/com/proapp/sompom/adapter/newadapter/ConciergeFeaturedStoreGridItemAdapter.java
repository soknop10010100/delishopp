package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.concierge.ConciergeFeatureStoreSectionResponse;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.LayoutConciergeFeaturedStoreItemViewModel;

import java.util.List;

/**
 * Created by Or Vitovongsak on 30/8/21.
 */
public class ConciergeFeaturedStoreGridItemAdapter extends RecyclerView.Adapter<BindingViewHolder> {

    private List<ConciergeFeatureStoreSectionResponse.Data> mData;
    private OnItemClickListener<ConciergeFeatureStoreSectionResponse.Data> mListener;

    public ConciergeFeaturedStoreGridItemAdapter(List<ConciergeFeatureStoreSectionResponse.Data> data,
                                                 OnItemClickListener<ConciergeFeatureStoreSectionResponse.Data> listener) {
        mData = data;
        mListener = listener;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.layout_concierge_featured_store_item).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        LayoutConciergeFeaturedStoreItemViewModel viewModel =
                new LayoutConciergeFeaturedStoreItemViewModel(mData.get(position));

        holder.setVariable(BR.viewModel, viewModel);
        holder.getBinding().getRoot().setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onClick(mData.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
