package com.proapp.sompom.adapter;

import android.content.Context;
import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ListItemConciergeCurrentLocationAddressBinding;
import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.ConciergeCurrentLocationDisplayAdaptive;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemConciergeCurrentLocationAddressViewModel;

import java.util.List;

/**
 * Created by Or Vitovongsak on 17/11/21.
 */
public class ConciergeCurrentLocationAdapter extends RefreshableAdapter<ConciergeCurrentLocationDisplayAdaptive, BindingViewHolder> {

    private Context mContext;
    private OnItemClickListener<ConciergeCurrentLocationDisplayAdaptive> mListener;

    public ConciergeCurrentLocationAdapter(Context context,
                                           List<ConciergeCurrentLocationDisplayAdaptive> datas,
                                           OnItemClickListener<ConciergeCurrentLocationDisplayAdaptive> listener) {
        super(datas);
        mContext = context;
        mListener = listener;
        setCanLoadMore(false);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_current_location_address).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListItemConciergeCurrentLocationAddressBinding) {
            ListItemConciergeCurrentLocationAddressViewModel viewModel =
                    new ListItemConciergeCurrentLocationAddressViewModel(mDatas.get(position), mListener);
            holder.setVariable(BR.viewModel, viewModel);
        }
    }

    public void updateAddressList(List<ConciergeCurrentLocationDisplayAdaptive> adaptive) {
        mDatas.clear();
        mDatas.addAll(adaptive);
        notifyDataSetChanged();
    }

    public void onNewAddressAdded(ConciergeUserAddress newAddress) {
        mDatas.add(newAddress);
        notifyItemInserted(mDatas.size() - 1);
    }
}
