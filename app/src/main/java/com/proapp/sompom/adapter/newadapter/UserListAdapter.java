package com.proapp.sompom.adapter.newadapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.DiffUtil;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.helper.UserListDiffCallback;
import com.proapp.sompom.listener.UserListAdaptive;
import com.proapp.sompom.model.AddGroupParticipantHeader;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.dialog.AddGroupParticipantDialog;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemAddUserViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemUserGroupTitleViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemUserViewModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

public class UserListAdapter<D extends UserListAdaptive> extends RefreshableAdapter<D, BindingViewHolder> {

    private static final int VIEW_TYPE_HEADER = 0x1990;
    private static final int VIEW_TYPE_PARTICIPANT = 0x1991;
    private static final int VIEW_TYPE_ADD_PARTICIPANT_HEADER = 0x1992;

    private Context mContext;
    private boolean mIsAbleToMakeVoiceCall;
    private boolean mIsAbleToMakeVideoCall;
    private boolean mIsShowPosition = true;
    private UserListAdapterListener mListener;
    private boolean mShowDummyView = true;
    private boolean mEnableSwipeRevealLayout;
    private Map<String, ListItemUserViewModel> mUserViewModelMap = new HashMap<>();

    public UserListAdapter(List<D> datas, Context context) {
        super(datas);
        mContext = context;
        setCanLoadMore(false);
        checkCallButtonAccessibility();
    }

    public UserListAdapter(List<D> datas,
                           Context context,
                           boolean showPosition,
                           boolean showDummyView,
                           boolean enableSwipeRevealLayout) {
        super(datas);
        mContext = context;
        mShowDummyView = showDummyView;
        mIsShowPosition = showPosition;
        mEnableSwipeRevealLayout = enableSwipeRevealLayout;
        setCanLoadMore(false);
        checkCallButtonAccessibility();
    }

    public void setListener(UserListAdapterListener listener) {
        mListener = listener;
    }

    private void checkCallButtonAccessibility() {
        UserHelper.checkUserCallFeatureSetting(mContext, new UserHelper.UserHelperListener() {
            @Override
            protected void onCheckUserCallFeatureFinished(boolean isCallEnable, boolean isVideoCallEnable) {
                mIsAbleToMakeVoiceCall = isCallEnable;
                mIsAbleToMakeVideoCall = isVideoCallEnable;
            }
        });
    }

    public void removeUser(int position) {
        Timber.i("removeUser: " + position);
        if (position < mDatas.size()) {
            mDatas.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_HEADER) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_user_group_title).build();
        } else if (viewType == VIEW_TYPE_ADD_PARTICIPANT_HEADER) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_add_user).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_user).build();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position < mDatas.size()) {
            if (mDatas.get(position) instanceof UserGroup) {
                return VIEW_TYPE_HEADER;
            } else if (mDatas.get(position) instanceof User) {
                return VIEW_TYPE_PARTICIPANT;
            } else if (mDatas.get(position) instanceof AddGroupParticipantHeader) {
                return VIEW_TYPE_ADD_PARTICIPANT_HEADER;
            } else {
                return super.getItemViewType(position);
            }
        }

        return super.getItemViewType(position);
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_HEADER) {
            UserGroup group = (UserGroup) mDatas.get(position);
            ListItemUserGroupTitleViewModel viewModel = new ListItemUserGroupTitleViewModel(mContext
                    .getResources().getDimensionPixelSize(R.dimen.space_medium), group.getName());
            holder.getBinding().setVariable(BR.viewModel, viewModel);
        } else if (getItemViewType(position) == VIEW_TYPE_ADD_PARTICIPANT_HEADER) {
            AddGroupParticipantHeader addGroupParticipantHeader = ((AddGroupParticipantHeader) mDatas.get(position));
            ListItemAddUserViewModel viewModel = new ListItemAddUserViewModel(addGroupParticipantHeader.getTitle());
            holder.getBinding().setVariable(BR.viewModel, viewModel);
            holder.getBinding().getRoot().setOnClickListener(v -> {
                if (mListener != null) {
                    mListener.onAddGroupParticipantClicked(addGroupParticipantHeader.getAddingType());
                }
            });
        } else {
            User user = (User) mDatas.get(position);
            ListItemUserViewModel viewModel = new ListItemUserViewModel(mContext,
                    user,
                    mIsShowPosition,
                    mShowDummyView,
                    mEnableSwipeRevealLayout,
                    new ListItemUserViewModel.ListItemUserViewModelListener() {
                        @Override
                        public void onDeleteClicked() {
                            if (mListener != null) {
                                mListener.onDeleteClicked(holder.getAdapterPosition(), user);
                            }
                        }

                        @Override
                        public void onAttemptedCallWhenNoNetworkConnectedOrDeviceIsInAnotherCallProcess(boolean isInAnotherCall) {
                            if (mListener != null) {
                                mListener.onAttemptedCallWhenNoNetworkConnectedOrDeviceIsInAnotherCallProcess(isInAnotherCall);
                            }
                        }

                        @Override
                        public void onMakeCallError(String error) {
                            if (mListener != null) {
                                mListener.onMakeCallError(error);
                            }
                        }
                    });
            holder.getBinding().setVariable(BR.viewModel, viewModel);
            viewModel.setVideoCallVisibility(mIsAbleToMakeVideoCall ? View.VISIBLE : View.GONE);
            viewModel.setVoiceCallVisibility(mIsAbleToMakeVoiceCall ? View.VISIBLE : View.GONE);
            mUserViewModelMap.put(user.getId(), viewModel);
        }
    }

    @SuppressWarnings("unchecked")
    public void refreshData(List<D> data) {
//        Timber.i("Refresh data: " + data.size() + ", old size: " + mDatas.size());
        checkCallButtonAccessibility();
        if (mDatas.isEmpty()) {
            mDatas = data;
            notifyDataSetChanged();
        } else {
            final UserListDiffCallback diffCallback = new UserListDiffCallback((List<UserListAdaptive>) mDatas,
                    (List<UserListAdaptive>) data);
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
            mDatas.clear();
            mDatas.addAll(data);
            diffResult.dispatchUpdatesTo(this);
            checkToUpdateCallButtonVisibility();
        }
    }

    private void checkToUpdateCallButtonVisibility() {
        for (ListItemUserViewModel viewModel : mUserViewModelMap.values()) {
            if (viewModel != null) {
                viewModel.setVideoCallVisibility(mIsAbleToMakeVideoCall ? View.VISIBLE : View.GONE);
                viewModel.setVoiceCallVisibility(mIsAbleToMakeVoiceCall ? View.VISIBLE : View.GONE);
            }
        }
    }

    public void removeLoadMoreLayoutIfAny() {
        if (canLoadMore()) {
            notifyItemRemoved(getItemCount());
            setCanLoadMore(false);
        }
    }

    public interface UserListAdapterListener {

        default void onAddGroupParticipantClicked(AddGroupParticipantDialog.AddingType addingType) {
        }

        default void onDeleteClicked(int position, User user) {
        }

        default void onAttemptedCallWhenNoNetworkConnectedOrDeviceIsInAnotherCallProcess(boolean isInAnotherCall) {
        }

        void onMakeCallError(String error);
    }
}
