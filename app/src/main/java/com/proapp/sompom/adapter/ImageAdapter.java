package com.proapp.sompom.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.proapp.sompom.newui.fragment.ImageFragment;

import java.util.List;

/**
 * Created by he.rotha on 3/4/16.
 */
public class ImageAdapter extends FragmentPagerAdapter {
    private final List<ImageFragment> mFragmentProductDetailItems;

    public ImageAdapter(FragmentManager fm, List<ImageFragment> fr) {
        super(fm);
        mFragmentProductDetailItems = fr;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentProductDetailItems.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentProductDetailItems.size();
    }
}
