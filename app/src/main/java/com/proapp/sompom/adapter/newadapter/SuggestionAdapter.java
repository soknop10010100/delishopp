package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import com.proapp.sompom.listener.OnUserClickListener;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.model.emun.SuggestionTab;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemSuggestionViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class SuggestionAdapter extends RefreshableAdapter<User, BindingViewHolder> {
    private SuggestionTab mSuggestionTab;
    private OnUserClickListener mOnUserClickListener;

    public SuggestionAdapter(List<User> userList, SuggestionTab tab, OnUserClickListener onUserClickListener) {
        super(userList);
        mSuggestionTab = tab;
        mOnUserClickListener = onUserClickListener;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_suggestion).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ListItemSuggestionViewModel viewModel = new ListItemSuggestionViewModel(mDatas.get(position),
                mSuggestionTab, mOnUserClickListener);
        holder.setVariable(BR.viewModel, viewModel);
    }

}
