package com.proapp.sompom.adapter.newadapter;

import android.content.Context;
import android.view.ViewGroup;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.listener.OnLikeItemListener;
import com.proapp.sompom.model.emun.FollowItemType;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemLikeViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class LikeViewerAdapter extends RefreshableAdapter<User, BindingViewHolder> {
    private OnLikeItemListener mListener;
    private Context mContext;
    private String mMyUserId;
    private FollowItemType mFollowItemType;
    private ItemType mItemType;

    public LikeViewerAdapter(Context context,
                             ItemType itemType,
                             List<User> datas,
                             FollowItemType followItemType,
                             OnLikeItemListener likeItemListener) {
        super(datas);
        mContext = context;
        mItemType = itemType;
        mMyUserId = SharedPrefUtils.getUserId(context);
        mFollowItemType = followItemType;
        mListener = likeItemListener;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (mItemType == ItemType.LIKE_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_like_viewer).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_follower).build();
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        final ListItemLikeViewModel viewModel = new ListItemLikeViewModel(mContext,
                mMyUserId,
                mDatas.get(position),
                mFollowItemType,
                mListener);
        holder.setVariable(BR.viewModel, viewModel);
    }

    public void notifyData() {
        mMyUserId = SharedPrefUtils.getUserId(mContext);
        notifyDataSetChanged();
    }

    public enum ItemType {
        LIKE_ITEM,
        FOLLOW_ITEM
    }


}
