package com.proapp.sompom.adapter;

import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.DiffUtil;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.model.ConciergeOrderItemDisplayAdaptive;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ItemConciergeOrderDetailViewModel;

import java.util.List;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 10/9/21.
 */

public class ConciergeOrderItemAdapter extends RefreshableAdapter<ConciergeOrderItemDisplayAdaptive, BindingViewHolder> {

    private final ConciergeOrderItemAdapterCallback mListener;
    private ApiService mApiService;

    public ConciergeOrderItemAdapter(List<ConciergeOrderItemDisplayAdaptive> data, ConciergeOrderItemAdapterCallback listener) {
        super(data);
        setCanLoadMore(false);
        mListener = listener;
    }

    public void setApiService(ApiService apiService) {
        mApiService = apiService;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_order_detail).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ItemConciergeOrderDetailViewModel viewModel =
                new ItemConciergeOrderDetailViewModel(holder.getContext(), (ConciergeMenuItem) mDatas.get(position));
        viewModel.addListener(new ItemConciergeOrderDetailViewModel.ItemConciergeOrderDetailViewModelListener() {
            @Override
            public void onAddItemClicked(ConciergeMenuItem product) {
                Timber.i("onAddItemClicked: " + product.getProductCount());
                updateProductCount(product);
            }

            @Override
            public void onRemoveItemClicked(ConciergeMenuItem product) {
                Timber.i("onRemoveItemClicked: " + product.getProductCount());
                updateProductCount(product);
                if (product.getProductCount() == 0) {
                    if (mListener != null) {
                        mListener.onItemRemoved(product);
                    }
                }
            }

            @Override
            public void onItemClicked(ConciergeMenuItem product) {
                Timber.i("onItemClicked: " + product.getProductCount());
                if (mListener != null) {
                    mListener.onItemClicked(product, position);
                }
            }

            @Override
            public void onRemoveWholeItemClicked(ConciergeMenuItem product) {
                if (mListener != null) {
                    mListener.onRemoveWholeItemClicked(product);
                }
            }

            @Override
            public ApiService getAPIService() {
                return mApiService;
            }
        });
        holder.setVariable(BR.viewModel, viewModel);
    }

    public void onUpdateData(List<ConciergeOrderItemDisplayAdaptive> newData) {
        Timber.i("onUpdateData: " + new Gson().toJson(newData));
        final ConciergeOrderItemDiffCallback diffCallback = new ConciergeOrderItemDiffCallback(mDatas, newData);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        mDatas.clear();
        mDatas.addAll(newData);
        diffResult.dispatchUpdatesTo(this);

        // After update are dispatched, if the data list is empty, notify listener
        if (mDatas.isEmpty()) {
            if (mListener != null) {
                mListener.onItemEmpty();
            }
        }
    }

    public double getTotalPrice() {
        double total = 0;
        for (ConciergeOrderItemDisplayAdaptive data : mDatas) {
            if (data instanceof ConciergeMenuItem) {
                total = ConciergeCartHelper.addPrice(total, ((ConciergeMenuItem) data).getTotalPrice());
            }
        }

        return total;
    }

    public void removeItem(ConciergeMenuItem item) {
        int position = -1;
        for (int i = 0; i < mDatas.size(); i++) {
            if (mDatas.get(i) instanceof ConciergeMenuItem && TextUtils.equals(item.getId(), ((ConciergeMenuItem) mDatas.get(i)).getId())) {
                position = i;
                break;
            }
        }

        if (position >= 0) {
            mDatas.remove(position);
            notifyItemRemoved(position);
        }

        if (mDatas.isEmpty()) {
            if (mListener != null) {
                mListener.onItemEmpty();
            }
        }
    }

    private void updateProductCount(ConciergeMenuItem conciergeMenuItem) {
        for (ConciergeOrderItemDisplayAdaptive data : mDatas) {
            if (data instanceof ConciergeMenuItem) {
                if (TextUtils.equals(conciergeMenuItem.getId(), ((ConciergeMenuItem) data).getId())) {
                    ((ConciergeMenuItem) data).setProductCount(conciergeMenuItem.getProductCount());
                    break;
                }
            }
        }
    }

    public interface ConciergeOrderItemAdapterCallback {

        void onItemRemoved(ConciergeMenuItem product);

        void onItemClicked(ConciergeMenuItem product, int position);

        void onRemoveWholeItemClicked(ConciergeMenuItem product);

        void onItemEmpty();
    }

    public static class ConciergeOrderItemDiffCallback extends DiffUtil.Callback {

        List<ConciergeOrderItemDisplayAdaptive> mOldList;
        List<ConciergeOrderItemDisplayAdaptive> mNewList;

        public ConciergeOrderItemDiffCallback(List<ConciergeOrderItemDisplayAdaptive> oldList,
                                              List<ConciergeOrderItemDisplayAdaptive> newList) {
            mOldList = oldList;
            mNewList = newList;
        }

        @Override
        public int getOldListSize() {
            return mOldList.size();
        }

        @Override
        public int getNewListSize() {
            return mNewList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            if (mOldList.get(oldItemPosition) instanceof ConciergeMenuItem && mNewList.get(newItemPosition) instanceof ConciergeMenuItem) {
                return ((ConciergeMenuItem) mOldList.get(oldItemPosition)).areItemsTheSame((ConciergeMenuItem) mNewList.get(newItemPosition));
            }

            return false;
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            if (mOldList.get(oldItemPosition) instanceof ConciergeMenuItem && mNewList.get(newItemPosition) instanceof ConciergeMenuItem) {
                return ((ConciergeMenuItem) mOldList.get(oldItemPosition)).areContentsTheSame((ConciergeMenuItem) mNewList.get(newItemPosition));
            }

            return false;
        }
    }
}
