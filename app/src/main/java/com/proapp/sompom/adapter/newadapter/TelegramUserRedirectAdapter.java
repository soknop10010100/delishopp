package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import androidx.recyclerview.widget.DiffUtil;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.listener.ConversationDataAdaptive;
import com.proapp.sompom.model.ConciergeOrderItemDisplayAdaptive;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.result.TelegramRedirectUser;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemTelegramUserRedirectViewModel;

import java.util.List;

public class TelegramUserRedirectAdapter extends RefreshableAdapter<ConversationDataAdaptive, BindingViewHolder> {

    private final ListItemTelegramUserRedirectViewModel.Listener mItemListener;

    public TelegramUserRedirectAdapter(List<ConversationDataAdaptive> data,
                                       ListItemTelegramUserRedirectViewModel.Listener listener) {
        super(data);
        mItemListener = listener;
        setCanLoadMore(false);
    }

    public void refreshData(List<ConversationDataAdaptive> newUsers) {
        if (mDatas.isEmpty()) {
            mDatas = newUsers;
            notifyDataSetChanged();
        } else {
            TelegramUserRedirectAdapterDiffCallback diffCallback = new TelegramUserRedirectAdapterDiffCallback(mDatas, newUsers);
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
            mDatas.clear();
            mDatas.addAll(newUsers);
            diffResult.dispatchUpdatesTo(this);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_telegram_user_redirect).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        final TelegramRedirectUser user = (TelegramRedirectUser) mDatas.get(position);
        ListItemTelegramUserRedirectViewModel viewModel = new ListItemTelegramUserRedirectViewModel(user, mItemListener);
        holder.getBinding().setVariable(BR.viewModel, viewModel);
    }

    public static class TelegramUserRedirectAdapterDiffCallback extends DiffUtil.Callback {

        List<ConversationDataAdaptive> mOldList;
        List<ConversationDataAdaptive> mNewList;

        public TelegramUserRedirectAdapterDiffCallback(List<ConversationDataAdaptive> oldList,
                                              List<ConversationDataAdaptive> newList) {
            mOldList = oldList;
            mNewList = newList;
        }

        @Override
        public int getOldListSize() {
            return mOldList.size();
        }

        @Override
        public int getNewListSize() {
            return mNewList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            if (mOldList.get(oldItemPosition) instanceof TelegramRedirectUser && mNewList.get(newItemPosition) instanceof TelegramRedirectUser) {
                return ((TelegramRedirectUser) mOldList.get(oldItemPosition)).areItemsTheSame((TelegramRedirectUser) mNewList.get(newItemPosition));
            }

            return false;
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            if (mOldList.get(oldItemPosition) instanceof TelegramRedirectUser && mNewList.get(newItemPosition) instanceof TelegramRedirectUser) {
                return ((TelegramRedirectUser) mOldList.get(oldItemPosition)).areContentsTheSame((TelegramRedirectUser) mNewList.get(newItemPosition));
            }

            return false;
        }
    }
}
