package com.proapp.sompom.adapter;

import android.content.Context;
import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.model.ShopDeliveryDate;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.ItemShopDeliveryDateViewModel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 10/9/21.
 */

public class ConciergeDeliveryDateAdapter extends RefreshableAdapter<ShopDeliveryDate, BindingViewHolder> {

    private final int mSelectedBackgroundColor;
    private final int mUSelectedBackgroundColor;
    private final int mSelectedTextColor;
    private final int mUnselectedTextColor;
    private final int mSelectedDotColor;
    private final int mUnselectedDotColor;
    private int mCurrentSelectedIndex;
    private Map<Integer, ItemShopDeliveryDateViewModel> mDateViewModelMap = new HashMap<>();
    private ConciergeDeliveryDateAdapterCallback mListener;

    private boolean isDefaultDateSelected = false;
    private List<ShopDeliveryDate> mBackupFullDate;

    public ConciergeDeliveryDateAdapter(Context context, List<ShopDeliveryDate> data, ConciergeDeliveryDateAdapterCallback listener) {
        super(data);
        mBackupFullDate = data;
        setCanLoadMore(false);
        mListener = listener;
        mSelectedBackgroundColor = AttributeConverter.convertAttrToColor(context, R.attr.shop_delivery_date_selected_background);
        mUSelectedBackgroundColor = AttributeConverter.convertAttrToColor(context, R.attr.shop_delivery_date_unselected_background);
        mSelectedTextColor = AttributeConverter.convertAttrToColor(context, R.attr.shop_delivery_date_selected_text);
        mUnselectedTextColor = AttributeConverter.convertAttrToColor(context, R.attr.shop_delivery_date_unselected_text);
        mSelectedDotColor = AttributeConverter.convertAttrToColor(context, R.attr.shop_delivery_date_dot_selected);
        mUnselectedDotColor = AttributeConverter.convertAttrToColor(context, R.attr.shop_delivery_date_dot_unselected);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.item_shop_delivery_date_layout).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ItemShopDeliveryDateViewModel viewModel = new ItemShopDeliveryDateViewModel(holder.getContext(),
                mDatas.get(position),
                mSelectedDotColor,
                mUnselectedDotColor,
                mSelectedTextColor,
                mUnselectedTextColor,
                mSelectedBackgroundColor,
                mUSelectedBackgroundColor,
                (date) -> {
                    mCurrentSelectedIndex = holder.getAdapterPosition();
                    resetDateSelectedState(holder.getAdapterPosition());
                    if (mListener != null) {
                        mListener.onDateItemSelected(date, true);
                    }
                });
        holder.setVariable(BR.viewModel, viewModel);
        mDateViewModelMap.put(position, viewModel);

        if (position == 0 && !isDefaultDateSelected) {
            isDefaultDateSelected = true;
            viewModel.onDateClicked();
        }
    }

    public int getCurrentSelectedIndex() {
        return mCurrentSelectedIndex;
    }

    public void shouldOnlyShowProvinceSlot(boolean showProvinceOnly, boolean shouldResetSelection) {
        if (showProvinceOnly) {
            List<ShopDeliveryDate> provinceOnly = new ArrayList<>();
            for (ShopDeliveryDate shopDeliveryDate : mDatas) {
                if (shopDeliveryDate.getShopDeliveryTimes().get(0).isAvailableProvince()) {
                    provinceOnly.add(shopDeliveryDate);
                }
            }
            mDatas = provinceOnly;
            if (mDatas.isEmpty()) {
                if (mListener != null) {
                    mListener.onEmptyProvinceSlots();
                }
            } else {
                isDefaultDateSelected = true;
                if (shouldResetSelection) {
                    // Reset date selection to the first index
                    mCurrentSelectedIndex = 0;
                    mDatas.get(mCurrentSelectedIndex).setSelected(true);
                    notifyItemChanged(mCurrentSelectedIndex);
                    resetDateSelectedState(mCurrentSelectedIndex);
                    if (mListener != null) {
                        mListener.onDateItemSelected(mDatas.get(mCurrentSelectedIndex), true);
                    }
                }
            }
            notifyDataSetChanged();
        } else {
            isDefaultDateSelected = true;
            mDatas = mBackupFullDate;
            if (!mDatas.isEmpty() && shouldResetSelection) {
                // Reset date selection to the first index
                mCurrentSelectedIndex = 0;
                mDatas.get(mCurrentSelectedIndex).setSelected(true);
                notifyItemChanged(mCurrentSelectedIndex);
                resetDateSelectedState(mCurrentSelectedIndex);
                mListener.onDateItemSelected(mDatas.get(mCurrentSelectedIndex), true);
            }
            notifyDataSetChanged();
        }
    }

    public void selectPreviousDate(LocalDateTime previousSelectedDate) {
        Timber.i("Previous selected date: " + previousSelectedDate);
        isDefaultDateSelected = true;
        boolean foundPreviousSelectedDate = false;
        for (int index = 0; index < mDatas.size(); index++) {
            ShopDeliveryDate date = mDatas.get(index);

            // Compare by converting them both to local date to avoid also comparing time.
            if (date.getDate().toLocalDate().isEqual(previousSelectedDate.toLocalDate())) {
                mCurrentSelectedIndex = index;
                mDatas.get(index).setSelected(true);
                notifyItemChanged(index);

                resetDateSelectedState(index);
                foundPreviousSelectedDate = true;
                if (mListener != null) {
                    mListener.onDateItemSelected(mDatas.get(index), false);
                }
            }
        }

        //Select first date as default.
        if (!foundPreviousSelectedDate && !mDatas.isEmpty()) {
            mCurrentSelectedIndex = 0;
            mDatas.get(mCurrentSelectedIndex).setSelected(true);
            notifyItemChanged(mCurrentSelectedIndex);
            resetDateSelectedState(mCurrentSelectedIndex);
            if (mListener != null) {
                mListener.onDateItemSelected(mDatas.get(mCurrentSelectedIndex), true);
            }
        }
    }

    /**
     * Loop through all entry and reset select status for all date that is not equal to the new
     * selected position
     *
     * @param newSelectedPosition The index of the new selected date
     */
    private void resetDateSelectedState(int newSelectedPosition) {
        for (Map.Entry<Integer, ItemShopDeliveryDateViewModel> entry : mDateViewModelMap.entrySet()) {
            if (entry.getKey() != null && entry.getKey() != newSelectedPosition && entry.getValue() != null) {
                entry.getValue().updateState(false);
            }
        }
    }

    public interface ConciergeDeliveryDateAdapterCallback {
        void onDateItemSelected(ShopDeliveryDate date, boolean shouldResetTimeSelection);

        void onEmptyProvinceSlots();
    }
}
