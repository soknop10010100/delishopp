package com.proapp.sompom.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.database.MessageDb;
import com.proapp.sompom.databinding.ListItemChatGroupModificationBinding;
import com.proapp.sompom.databinding.ListItemChatMeBinding;
import com.proapp.sompom.databinding.ListItemChatMeCallEventBinding;
import com.proapp.sompom.databinding.ListItemChatMeEmojiBinding;
import com.proapp.sompom.databinding.ListItemChatMeLinkBinding;
import com.proapp.sompom.databinding.ListItemChatMeMediaAudioBinding;
import com.proapp.sompom.databinding.ListItemChatMeMediaBinding;
import com.proapp.sompom.databinding.ListItemChatMeMediaFileBinding;
import com.proapp.sompom.databinding.ListItemChatMeRemoveMessageBinding;
import com.proapp.sompom.databinding.ListItemChatRecipientCallEventBinding;
import com.proapp.sompom.databinding.ListItemChatRecipientEmojiBinding;
import com.proapp.sompom.databinding.ListItemChatRecipientLinkBinding;
import com.proapp.sompom.databinding.ListItemChatRecipientMediaAudioBinding;
import com.proapp.sompom.databinding.ListItemChatRecipientMediaBinding;
import com.proapp.sompom.databinding.ListItemChatRecipientMediaFileBinding;
import com.proapp.sompom.databinding.ListItemChatRecipientRemoveMessageBinding;
import com.proapp.sompom.databinding.ListItemChatTypingBinding;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.AudioPlayer;
import com.proapp.sompom.helper.ChatDiffCallback;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.ConversationHelper;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.listener.OnChatItemListener;
import com.proapp.sompom.model.BaseChatModel;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.SelectedChat;
import com.proapp.sompom.model.TypingChatItem;
import com.proapp.sompom.model.UnreadChatModel;
import com.proapp.sompom.model.WelcomeSupportConversationModel;
import com.proapp.sompom.model.emun.ChatBg;
import com.proapp.sompom.model.emun.ConversationType;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.ChatUtility;
import com.proapp.sompom.utils.EmojiDetectorUtil;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.ChatEmojiTextViewModel;
import com.proapp.sompom.viewmodel.ChatImageOrVideoViewModel;
import com.proapp.sompom.viewmodel.ChatMediaViewModel;
import com.proapp.sompom.viewmodel.ChatViewModel;
import com.proapp.sompom.viewmodel.ListItemChatLInkPreviewViewModel;
import com.proapp.sompom.viewmodel.ListItemChatRemoveMessageViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.AbsSupportBottomPaddingChatItemViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.CallEventChatViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.FileChatViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.GroupModificationChatViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemChatTypingViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.RealmList;
import timber.log.Timber;

/**
 * Created by he.rotha on 3/4/16.
 */
public class ChatAdapter extends RefreshableTopAdapter<BaseChatModel, BindingViewHolder> {

    private static final int RECIPIENT_VIEW_MEDIA_TYPE = 0X019;
    private static final int ME_VIEW_MEDIA_TYPE = 0X010;
    private static final int ME_VIEW_TYPE = 0X011;
    private static final int RECIPIENT_VIEW_TYPE = 0X012;
    private static final int TYPING_VIEW_TYPE = 0X013;
    private static final int ME_VIEW_MEDIA_AUDIO_TYPE = 0X021;
    private static final int RECIPIENT_VIEW_MEDIA_AUDIO_TYPE = 0X022;
    private static final int ME_VIEW_GIF_TYPE = 0X023;
    private static final int RECIPIENT_VIEW_GIF_TYPE = 0X024;
    private static final int ME_LINK_VIEW_TYPE = 0X025;
    private static final int RECIPIENT_LINK_VIEW_TYPE = 0X026;
    private static final int ME_REMOVE_MESSAGE = 0X027;
    private static final int RECIPIENT_REMOVE_MESSAGE = 0X028;
    private static final int ME_FILE_MASSAGE = 0X029;
    private static final int RECIPIENT_FILE_MASSAGE = 0X030;
    private static final int ME_CALL_EVENT_MASSAGE = 0X031;
    private static final int RECIPIENT_CALL_EVENT_MASSAGE = 0X032;
    private static final int UNREAD_MASSAGE_TITLE_TYPE = 0X033;
    private static final int GROUP_MODIFICATION_MESSAGE = 0x036;
    private static final int ME_EMOJI_ONLY_MESSAGE = 0x037;
    private static final int RECIPIENT_EMOJI_ONLY_MESSAGE = 0x038;
    private static final int WELCOME_SUPPORT_CONVERSATION_TYPE = 0X039;

    private final User mMyUser;
    private final SelectedChat mSelectedChat;
    private final OnChatItemListener mOnImageClickListener;
    private final Activity mActivity;
    private final TypingChatItem mTypingChatItem = new TypingChatItem();
    private ListItemChatTypingViewModel mListItemChatTypingViewModel;
    private Conversation mConversation;
    private User mRecipient;
    private HashMap<String, FileChatViewModel> mFileChatViewModelHashMap = new HashMap<>();
    private HashMap<String, ChatMediaViewModel> mSeenAvatarViewItemInfoHolder = new HashMap<>();
    private HashMap<String, ChatMediaViewModel> mItemViewModelMap = new HashMap<>();
    private ApiService mApiService;
    private boolean mCanLoadMoreFromBottom;
    private String mSearchKeyWord;
    private AudioPlayer mAudioPlayer;
    private SocketService.SocketBinder mSocketBinder;
    private boolean mHasUnreadMessageTitle;
    private final ChatAdapterCallback mCallback;
    private WelcomeSupportConversationModel mWelcomeSupportConversationModel;
    private Map<String, SimpleExoPlayer> mGifVideoPlayerMap = new HashMap<>();

    public ChatAdapter(Activity activity,
                       ApiService apiService,
                       Conversation conversation,
                       User myUser,
                       List<BaseChatModel> chats,
                       String searchKeyWord,
                       OnChatItemListener onItemClickListener,
                       ChatAdapterCallback chatAdapterCallback) {
        super(chats);
        mCallback = chatAdapterCallback;
        mApiService = apiService;
        mConversation = conversation;
        mActivity = activity;
        mSelectedChat = new SelectedChat();
        mSearchKeyWord = searchKeyWord;
        mOnImageClickListener = onItemClickListener;
        mMyUser = myUser;
        if (conversation != null) {
            mRecipient = conversation.getOneToOneRecipient(activity);
        }
        mAudioPlayer = new AudioPlayer(activity);
    }

    public void setSocketBinder(SocketService.SocketBinder socketBinder) {
        mSocketBinder = socketBinder;
    }

    public void resetHighLightSearchKeyword(boolean removeFromViewItem) {
        mSearchKeyWord = null;
        if (removeFromViewItem) {
            for (ChatMediaViewModel value : mItemViewModelMap.values()) {
                if (value instanceof ChatViewModel) {
                    ((ChatViewModel) value).removeHighLightSearchKeyword();
                }
            }
        }
    }

    public void setCanLoadMoreFromBottom(boolean canLoadMoreFromBottom) {
        mCanLoadMoreFromBottom = canLoadMoreFromBottom;
    }

    public void updateConversation(Conversation conversation) {
        mConversation = conversation;
    }

    private boolean isGroupConversation() {
        return mConversation != null && mConversation.isGroup();
    }

    public boolean isCanLoadMoreFromBottom() {
        return mCanLoadMoreFromBottom;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        int resourceId;
        if (viewType == TYPING_VIEW_TYPE) {
            resourceId = R.layout.list_item_chat_typing;
        } else if (viewType == ME_REMOVE_MESSAGE) {
            resourceId = R.layout.list_item_chat_me_remove_message;
        } else if (viewType == ME_VIEW_TYPE) {
            resourceId = R.layout.list_item_chat_me;
        } else if (viewType == ME_LINK_VIEW_TYPE) {
            resourceId = R.layout.list_item_chat_me_link;
        } else if (viewType == ME_VIEW_MEDIA_TYPE) {
            resourceId = R.layout.list_item_chat_me_media;
        } else if (viewType == RECIPIENT_VIEW_MEDIA_TYPE) {
            resourceId = R.layout.list_item_chat_recipient_media;
        } else if (viewType == ME_VIEW_MEDIA_AUDIO_TYPE) {
            resourceId = R.layout.list_item_chat_me_media_audio;
        } else if (viewType == RECIPIENT_REMOVE_MESSAGE) {
            resourceId = R.layout.list_item_chat_recipient_remove_message;
        } else if (viewType == RECIPIENT_VIEW_MEDIA_AUDIO_TYPE) {
            resourceId = R.layout.list_item_chat_recipient_media_audio;
        } else if (viewType == ME_VIEW_GIF_TYPE) {
            resourceId = R.layout.list_item_chat_me_gif;
        } else if (viewType == RECIPIENT_VIEW_GIF_TYPE) {
            resourceId = R.layout.list_item_chat_recipient_gif;
        } else if (viewType == RECIPIENT_LINK_VIEW_TYPE) {
            resourceId = R.layout.list_item_chat_recipient_link;
        } else if (viewType == RECIPIENT_FILE_MASSAGE) {
            resourceId = R.layout.list_item_chat_recipient_media_file;
        } else if (viewType == ME_FILE_MASSAGE) {
            resourceId = R.layout.list_item_chat_me_media_file;
        } else if (viewType == ME_CALL_EVENT_MASSAGE) {
            resourceId = R.layout.list_item_chat_me_call_event;
        } else if (viewType == RECIPIENT_CALL_EVENT_MASSAGE) {
            resourceId = R.layout.list_item_chat_recipient_call_event;
        } else if (viewType == UNREAD_MASSAGE_TITLE_TYPE) {
            mHasUnreadMessageTitle = true;
            resourceId = R.layout.list_item_chat_unread;
        } else if (viewType == GROUP_MODIFICATION_MESSAGE) {
            resourceId = R.layout.list_item_chat_group_modification;
        } else if (viewType == ME_EMOJI_ONLY_MESSAGE) {
            resourceId = R.layout.list_item_chat_me_emoji;
        } else if (viewType == RECIPIENT_EMOJI_ONLY_MESSAGE) {
            resourceId = R.layout.list_item_chat_recipient_emoji;
        } else if (viewType == WELCOME_SUPPORT_CONVERSATION_TYPE) {
            resourceId = R.layout.list_item_support_welcome;
        } else {
            resourceId = R.layout.list_item_chat_recipient;
        }
        return new BindingViewHolder.Builder(parent, resourceId).build();
    }

    @Override
    public int getItemCount() {
        int count = super.getItemCount();
        if (mCanLoadMoreFromBottom) {
            count += 1;
        }

        return count;
    }

    public int findChatPosition(String chatId) {
        for (int i = 0; i < mData.size(); i++) {
            if (TextUtils.equals(chatId, mData.get(i).getId())) {
                return canLoadMore() ? i + 1 : i;
            }
        }

        return -1;
    }

    public void removeLoadMoreFromBottom() {
        int itemCount = getItemCount();
        setCanLoadMoreFromBottom(false);
        notifyItemRemoved(itemCount);
    }

    public Chat getMostEarlierMessage() {
        for (BaseChatModel datum : mData) {
            if (datum instanceof Chat) {
                return (Chat) datum;
            }
        }

        return null;
    }

    public boolean isValidMessageToAdd(Chat addMessage) {
        /*
            The message that allow to add into list will be the message that at least newer than
            the oldest message in the screen and by doing this, we can avoid sometimes the old messages
            display in the chat screen.
         */
        if (addMessage != null) {
            Chat mostEarlierMessage = getMostEarlierMessage();
            if (mostEarlierMessage != null) {
                //Allow to update the existing chat in list.
                if (TextUtils.equals(addMessage.getId(), mostEarlierMessage.getId())) {
                    return true;
                }

                return addMessage.compareTo(mostEarlierMessage) > 0;
            } else {
                return true;
            }
        }

        return false;
    }

    public void resetData(List<BaseChatModel> chats) {
        mData = chats;
        notifyDataSetChanged();
    }

    public void setData(List<BaseChatModel> chats) {
//        Timber.i("setData: " + chats.size() + ", old: " + mData.size());
        if (mData.isEmpty()) {
            mData = chats;
            notifyDataSetChanged();
        } else {
            if (!isUnreadMessageTitleExist(chats)) {
                maintainThePreviousUnreadMessageItem(chats);
            }
            List<BaseChatModel> backUpList = new ArrayList<>(mData);
            final ChatDiffCallback diffCallback = new ChatDiffCallback(mActivity, mConversation, backUpList, chats);
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
            mData = backUpList;
            diffResult.dispatchUpdatesTo(this);
        }
    }

    public List<BaseChatModel> getData() {
        return mData;
    }

    public boolean isUnreadMessageTitleExist(List<BaseChatModel> chats) {
        for (BaseChatModel chat : chats) {
            if (chat instanceof UnreadChatModel) {
                return true;
            }
        }

        return false;
    }

    public int getUnreadMessagePosition() {
        if (!mHasUnreadMessageTitle) {
            //If the unread view item was not previously added, will turn non exist index.
            return -1;
        }

        for (int i = 0; i < mData.size(); i++) {
            if (mData.get(i) instanceof UnreadChatModel) {
                return i;
            }
        }

        return -1;
    }

    private void removeUnreadMessageInternally() {
        removeUnreadMessage(getUnreadMessagePosition());
    }

    public void removeUnreadMessage(int position) {
        if (position >= 0 && position < mData.size()) {
            mData.remove(position);
            safeNotifyItemRemove(position);
        }
    }

    private void maintainThePreviousUnreadMessageItem(List<BaseChatModel> newData) {
//        Timber.e("maintain previous unread message");
        /*
        We manage to maintain the unread message status previously added in chat screen if any for we
        have the process of loading data from local and then update with the server data to make the
        improvement of user experience of loading data in chat screen.
         */
        for (int i = 0; i < mData.size(); i++) {
            if (mData.get(i) instanceof UnreadChatModel) {
                if ((i + 1) < mData.size()) {
                    String firstUnreadMessageId = mData.get(i + 1).getId();
                    int insertIndex = -1;
                    for (int i1 = 0; i1 < newData.size(); i1++) {
                        if (TextUtils.equals(newData.get(i1).getId(), firstUnreadMessageId)) {
                            insertIndex = i1;
                            break;
                        }
                    }
                    if (insertIndex >= 0) {
                        newData.add(insertIndex, mData.get(i));
                    }
                }
                break;
            }
        }
    }

    public void checkToPlayOrStopWritingMessageIndicatorSound(boolean isStop) {
        if (mData.contains(mTypingChatItem)) {
            if (isStop) {
                if (mAudioPlayer.isPlayingWritingMessageIndicatorSound()) {
                    mAudioPlayer.stopWritingMessageIndicatorSound();
                }
            } else {
                if (!mAudioPlayer.isPlayingWritingMessageIndicatorSound()) {
                    mAudioPlayer.playWritingMessageIndicatorSound();
                }
            }
        }
    }

    public void checkToDisplayReceivingTypingIndicator(User user, boolean shouldDisplay) {
        int typingIndicatorIndex = mData.indexOf(mTypingChatItem);
        boolean isTypingItemNotAddedYet = typingIndicatorIndex < 0;
        if (shouldDisplay) {
            mTypingChatItem.addTypingSender(user);
            if (isTypingItemNotAddedYet) {
                mData.add(mTypingChatItem);
                safeNotifyItemInsertChange(mData.size());
                updateLastChatItemBottomPadding();
                //Will play sound only if the app is in foreground
                if (!mCallback.isScreenInBackground()) {
                    mAudioPlayer.playWritingMessageIndicatorSound();
                }
            } else if (mListItemChatTypingViewModel != null) {
                //Check to add more typing sender
                mListItemChatTypingViewModel.addNewTypingSender(user);
            }
        } else {
            /*
                Typing indicator is already added in data list. So we will check to remove it.
                Note: If the list has scrolled up, and the typing indicator is added into list and notify
                the list, but the typing view indicator item will not display immediately unless you scroll
                down to bottom of list as the flow of RecyclerView, so in this case case, we can say
                that the data source has been added but the view is not yet display. So we have to make
                sure that the item of typing be removed from data source even if the typing view does
                not display yet to avoid stuck at typing view and sound do not remove.
             */
            if (!isTypingItemNotAddedYet) {
                mTypingChatItem.removeTypingSender(user);
                if (mListItemChatTypingViewModel != null) {
                    //Typing view is already display
                    mListItemChatTypingViewModel.removeTypingSender(user);
                    if (mListItemChatTypingViewModel.shouldRemoveTypingIndicator()) {
                        mData.remove(typingIndicatorIndex);
                        mListItemChatTypingViewModel = null;
                        safeNotifyItemRemove(typingIndicatorIndex);
                        updateLastChatItemBottomPadding();
                        mAudioPlayer.stopWritingMessageIndicatorSound();
                    }
                } else {
                    if (mTypingChatItem.isEmpty()) {
                        mData.remove(typingIndicatorIndex);
                        mListItemChatTypingViewModel = null;
                        mAudioPlayer.stopWritingMessageIndicatorSound();
                        updateLastChatItemBottomPadding();
                    }
                }
            }
        }
    }

    public void releaseWritingMessageIndicatorSound() {
        mAudioPlayer.releaseWritingMessageIndicatorSound();
    }

    private void updateLastChatItemBottomPadding() {
        if (!mData.isEmpty()) {
            BaseChatModel lastChat = mData.get(mData.size() - 1);
            if (lastChat instanceof Chat) {
                updateBottomLineVisibility((Chat) lastChat);
            }
        }
    }

    @Override
    public int getItemViewType(final int position) {
        if (mData.isEmpty() || (position == 0 && canLoadMore())) {
            //Load more from top will being checked here.
            return super.getItemViewType(position);
        } else {
//            Timber.i("getItemViewType: position: " + position + ", mCanLoadMoreFromBottom: " + mCanLoadMoreFromBottom + ", Data Size: " + mData.size());
            /*
            Check loading view from bottom type.
            1. If there is load more from top (canLoadMore) enable
            2. If there is load more from top (canLoadMore) not enable
             */
            if (mCanLoadMoreFromBottom) {
                if (canLoadMore() && position == (mData.size() + 2)) {
                    return VIEW_PROGRESS;
                }

                if (position == mData.size() + 1) {
                    return VIEW_PROGRESS;
                }
            }

            //Check other item types
            int itemPosition;
            if (canLoadMore() || mCanLoadMoreFromBottom) {
                itemPosition = position - 1;
            } else {
                itemPosition = position;
            }

            if (itemPosition < 0) {
                itemPosition = 0;
            }

            if (mData.get(itemPosition) instanceof Chat) {
                return getItemType(itemPosition);
            } else if (mData.get(itemPosition) instanceof UnreadChatModel) {
                return UNREAD_MASSAGE_TITLE_TYPE;
            } else if (mData.get(itemPosition) instanceof WelcomeSupportConversationModel) {
                return WELCOME_SUPPORT_CONVERSATION_TYPE;
            } else {
                return TYPING_VIEW_TYPE;
            }
        }
    }

    private int getItemType(int itemPosition) {
        if (isGroupModificationMessageType(itemPosition)) {
            return GROUP_MODIFICATION_MESSAGE;
        } else if (isMe(itemPosition)) {
            return getItemTypeChatMe(itemPosition);
        } else {
            return getItemTypeChatRecipient(itemPosition);
        }
    }

    private boolean isGroupModificationMessageType(int position) {
        BaseChatModel baseChatModel = mData.get(position);
        if (baseChatModel instanceof Chat) {
            return ConversationHelper.isGroupModificationMessageType((Chat) baseChatModel);
        }

        return false;
    }

    private boolean isGroupModificationMessageType(Chat toCheck) {
        return ConversationHelper.isGroupModificationMessageType(toCheck);
    }

    private int getItemTypeChatMe(int itemPosition) {
        if (((Chat) mData.get(itemPosition)).isDeleted()) {
            return ME_REMOVE_MESSAGE;
        } else if (((Chat) mData.get(itemPosition)).getType() == Chat.Type.IMAGE ||
                ((Chat) mData.get(itemPosition)).getType() == Chat.Type.GIF ||
                ((Chat) mData.get(itemPosition)).getType() == Chat.Type.VIDEO ||
                ((Chat) mData.get(itemPosition)).getType() == Chat.Type.MIX_MEDIA) {
            return ME_VIEW_MEDIA_TYPE;
        } else if (((Chat) mData.get(itemPosition)).getType() == Chat.Type.AUDIO) {
            return ME_VIEW_MEDIA_AUDIO_TYPE;
        } else if (((Chat) mData.get(itemPosition)).getType() == Chat.Type.TENOR_GIF) {
            return ME_VIEW_GIF_TYPE;
        } else if (((Chat) mData.get(itemPosition)).getType() == Chat.Type.FILE) {
            return ME_FILE_MASSAGE;
        } else if (ChatHelper.isCallEventTypes((Chat) mData.get(itemPosition))) {
            return ME_CALL_EVENT_MASSAGE;
        } else if (EmojiDetectorUtil.isEmojiOnly(((Chat) mData.get(itemPosition)).getContent())) {
            return ME_EMOJI_ONLY_MESSAGE;
        } else {
            if (WallStreetHelper.isContainLink(((Chat) mData.get(itemPosition)).getContent())) {
                return ME_LINK_VIEW_TYPE;
            } else {
                return ME_VIEW_TYPE;
            }
        }
    }

    private int getItemTypeChatRecipient(int itemPosition) {
        if (((Chat) mData.get(itemPosition)).isDeleted()) {
            return RECIPIENT_REMOVE_MESSAGE;
        } else if (((Chat) mData.get(itemPosition)).getType() == Chat.Type.IMAGE ||
                ((Chat) mData.get(itemPosition)).getType() == Chat.Type.GIF ||
                ((Chat) mData.get(itemPosition)).getType() == Chat.Type.VIDEO ||
                ((Chat) mData.get(itemPosition)).getType() == Chat.Type.MIX_MEDIA) {
            return RECIPIENT_VIEW_MEDIA_TYPE;
        } else if (((Chat) mData.get(itemPosition)).getType() == Chat.Type.AUDIO) {
            return RECIPIENT_VIEW_MEDIA_AUDIO_TYPE;
        } else if (((Chat) mData.get(itemPosition)).getType() == Chat.Type.TENOR_GIF) {
            return RECIPIENT_VIEW_GIF_TYPE;
        } else if (((Chat) mData.get(itemPosition)).getType() == Chat.Type.FILE) {
            return RECIPIENT_FILE_MASSAGE;
        } else if (ChatHelper.isCallEventTypes((Chat) mData.get(itemPosition))) {
            return RECIPIENT_CALL_EVENT_MASSAGE;
        } else if (EmojiDetectorUtil.isEmojiOnly(((Chat) mData.get(itemPosition)).getContent())) {
            return RECIPIENT_EMOJI_ONLY_MESSAGE;
        } else {
            Chat chat = (Chat) mData.get(itemPosition);
            if (!TextUtils.isEmpty(chat.getContent()) && WallStreetHelper.isContainLink(chat.getContent())) {
                return RECIPIENT_LINK_VIEW_TYPE;
            }
            return RECIPIENT_VIEW_TYPE;
        }
    }

    private boolean isMe(int position) {
        if (position < 0) {
            position = 0;
        }
        try {
            // Only check that a chat item isMe() if it's an instance of Chat, else return false to
            // avoid spamming sentry exception logger
            if (mData.get(position) instanceof Chat) {
                String userId = ((Chat) mData.get(position)).getSenderId();
                return userId.equals(mMyUser.getId());
            } else {
                return false;
            }
        } catch (Exception ex) {
            SentryHelper.logSentryError(ex);
            return false;
        }
    }

    public void onReceiveUpdateSeenUserIdListUpdate(List<Chat> chats) {
        for (Chat chat : chats) {
            ChatMediaViewModel chatMediaViewModel = mItemViewModelMap.get(chat.getId());
//            Timber.i("onReceiveUpdateSeenUserIdListUpdate: " + chat.getContent() + ", seen: " + new Gson().toJson(chat.getSeen()) + ", chatMediaViewModel: " + chatMediaViewModel);
            if (chatMediaViewModel != null) {
                updateSeenUserIdList(chat.getId(), chatMediaViewModel.updateSeenUserList(chat.getSeen()));
            }
        }
    }

    private void updateSeenUserIdList(String chatId, RealmList<String> seenIds) {
        for (BaseChatModel datum : mData) {
            if (TextUtils.equals(datum.getId(), chatId)) {
                ((Chat) datum).setSeen(seenIds);
                ((Chat) datum).setStatus(MessageState.SEEN);
                break;
            }
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ChatUtility.GroupMessage drawable = ChatUtility.getGroupMessage(mData, position, mMyUser, isMe(position));
        boolean isLastItem = position == (mData.size() - 1);
        if (holder.getBinding() instanceof ListItemChatTypingBinding) {
            ListItemChatTypingViewModel viewModel = new ListItemChatTypingViewModel(mActivity,
                    position,
                    ((TypingChatItem) mData.get(position)).getTypingSender());
            viewModel.updateBottomLineVisibility(isLastItem);
            holder.setVariable(BR.viewModel, viewModel);
            mListItemChatTypingViewModel = viewModel;
        } else if (holder.getBinding() instanceof ListItemChatMeRemoveMessageBinding
                || holder.getBinding() instanceof ListItemChatRecipientRemoveMessageBinding) {
            Chat message = (Chat) mData.get(position);
            ListItemChatRemoveMessageViewModel chatViewModel = new ListItemChatRemoveMessageViewModel(mActivity,
                    mConversation,
                    message,
                    drawable,
                    position,
                    mSelectedChat,
                    mSearchKeyWord,
                    new MyOnChatItemListener(holder));
            setMarginRemovedMessage(holder, drawable);
            chatViewModel.updateBottomLineVisibility(isLastItem);
            holder.setVariable(BR.viewModel, chatViewModel);
            performManuallyCheckingToRemoveOldSeenAvatarOnBindChatItem((Chat) mData.get(position)); //Must call this statement first
            holdSeenAvatarViewItemInfo(((Chat) mData.get(position)).getSeenParticipants(), chatViewModel);
            mItemViewModelMap.put(mData.get(position).getId(), chatViewModel);
        } else if (holder.getBinding() instanceof ListItemChatMeMediaBinding
                || holder.getBinding() instanceof ListItemChatRecipientMediaBinding) {
            Chat message = (Chat) mData.get(position);
            ChatImageOrVideoViewModel chatViewModel = new ChatImageOrVideoViewModel(mActivity,
                    mConversation,
                    message,
                    drawable,
                    position,
                    mSelectedChat,
                    mSearchKeyWord,
                    new MyOnChatItemListener(holder));
            chatViewModel.updateBottomLineVisibility(isLastItem);
            holder.setVariable(BR.viewModel, chatViewModel);
            performManuallyCheckingToRemoveOldSeenAvatarOnBindChatItem((Chat) mData.get(position)); //Must call this statement first
            holdSeenAvatarViewItemInfo(((Chat) mData.get(position)).getSeenParticipants(), chatViewModel);
            mItemViewModelMap.put(mData.get(position).getId(), chatViewModel);
        } else if (holder.getBinding() instanceof ListItemChatMeMediaAudioBinding
                || holder.getBinding() instanceof ListItemChatRecipientMediaAudioBinding) {
            Chat message = (Chat) mData.get(position);
            ChatMediaViewModel chatViewModel = new ChatMediaViewModel(mActivity,
                    mConversation,
                    message,
                    drawable,
                    position,
                    mSelectedChat,
                    mSearchKeyWord,
                    new MyOnChatItemListener(holder));
            chatViewModel.updateBottomLineVisibility(isLastItem);
            holder.setVariable(BR.viewModel, chatViewModel);
            performManuallyCheckingToRemoveOldSeenAvatarOnBindChatItem((Chat) mData.get(position)); //Must call this statement first
            holdSeenAvatarViewItemInfo(((Chat) mData.get(position)).getSeenParticipants(), chatViewModel);
            mItemViewModelMap.put(mData.get(position).getId(), chatViewModel);
        } else if (holder.getBinding() instanceof ListItemChatMeMediaFileBinding
                || holder.getBinding() instanceof ListItemChatRecipientMediaFileBinding) {
            Chat message = (Chat) mData.get(position);
            FileChatViewModel chatViewModel = new FileChatViewModel(mActivity,
                    mConversation,
                    message,
                    drawable,
                    position,
                    mSelectedChat,
                    mSearchKeyWord,
                    getFileNameForFileMediaType(message),
                    new MyOnChatItemListener(holder));
            chatViewModel.updateBottomLineVisibility(isLastItem);
            holder.setVariable(BR.viewModel, chatViewModel);
            mFileChatViewModelHashMap.remove(message.getId());
            mFileChatViewModelHashMap.put(message.getId(), chatViewModel);
            performManuallyCheckingToRemoveOldSeenAvatarOnBindChatItem((Chat) mData.get(position)); //Must call this statement first
            holdSeenAvatarViewItemInfo(((Chat) mData.get(position)).getSeenParticipants(), chatViewModel);
            mItemViewModelMap.put(mData.get(position).getId(), chatViewModel);
        } else if (holder.getBinding() instanceof ListItemChatMeLinkBinding
                || holder.getBinding() instanceof ListItemChatRecipientLinkBinding) {
            Chat message = (Chat) mData.get(position);
            ListItemChatLInkPreviewViewModel chatViewModel = new ListItemChatLInkPreviewViewModel(mActivity,
                    mApiService,
                    mConversation,
                    holder.getBinding().getRoot(),
                    position,
                    message,
                    mSelectedChat,
                    mSearchKeyWord,
                    drawable,
                    holder.getBinding() instanceof ListItemChatMeLinkBinding,
                    new MyOnChatItemListener(holder));
            chatViewModel.updateBottomLineVisibility(isLastItem);
            holder.setVariable(BR.viewModel, chatViewModel);
            performManuallyCheckingToRemoveOldSeenAvatarOnBindChatItem((Chat) mData.get(position)); //Must call this statement first
            holdSeenAvatarViewItemInfo(((Chat) mData.get(position)).getSeenParticipants(), chatViewModel);
            mItemViewModelMap.put(mData.get(position).getId(), chatViewModel);
        } else if (holder.getBinding() instanceof ListItemChatMeCallEventBinding
                || holder.getBinding() instanceof ListItemChatRecipientCallEventBinding) {
            Chat message = (Chat) mData.get(position);
            CallEventChatViewModel chatViewModel = new CallEventChatViewModel(mActivity,
                    mMyUser,
                    mRecipient,
                    mConversation,
                    position,
                    message,
                    mSelectedChat,
                    mSearchKeyWord,
                    drawable,
                    new MyOnChatItemListener(holder));
            chatViewModel.updateBottomLineVisibility(isLastItem);
            holder.setVariable(BR.viewModel, chatViewModel);
            performManuallyCheckingToRemoveOldSeenAvatarOnBindChatItem((Chat) mData.get(position)); //Must call this statement first
            holdSeenAvatarViewItemInfo(((Chat) mData.get(position)).getSeenParticipants(), chatViewModel);
            mItemViewModelMap.put(mData.get(position).getId(), chatViewModel);
        } else if (holder.getBinding() instanceof ListItemChatGroupModificationBinding) {
            GroupModificationChatViewModel chatViewModel = new GroupModificationChatViewModel(mActivity,
                    mConversation,
                    position,
                    (Chat) mData.get(position),
                    mSelectedChat,
                    mSearchKeyWord,
                    drawable,
                    new MyOnChatItemListener(holder));
            chatViewModel.updateBottomLineVisibility(isLastItem);
            holder.setVariable(BR.viewModel, chatViewModel);
            performManuallyCheckingToRemoveOldSeenAvatarOnBindChatItem((Chat) mData.get(position)); //Must call this statement first
            holdSeenAvatarViewItemInfo(((Chat) mData.get(position)).getSeenParticipants(), chatViewModel);
            mItemViewModelMap.put(mData.get(position).getId(), chatViewModel);
        } else if (holder.getBinding() instanceof ListItemChatMeEmojiBinding ||
                holder.getBinding() instanceof ListItemChatRecipientEmojiBinding) {
            Chat message = (Chat) mData.get(position);
            ChatEmojiTextViewModel chatViewModel = new ChatEmojiTextViewModel(mActivity,
                    mConversation,
                    holder.getBinding().getRoot(),
                    position,
                    message,
                    mSelectedChat,
                    mSearchKeyWord,
                    drawable,
                    holder.getBinding() instanceof ListItemChatMeEmojiBinding,
                    new MyOnChatItemListener(holder));
            chatViewModel.updateBottomLineVisibility(isLastItem);
            holder.setVariable(BR.viewModel, chatViewModel);
            performManuallyCheckingToRemoveOldSeenAvatarOnBindChatItem((Chat) mData.get(position)); //Must call this statement first
            holdSeenAvatarViewItemInfo(message.getSeenParticipants(), chatViewModel);
            mItemViewModelMap.put(mData.get(position).getId(), chatViewModel);
        }
        //The default is normal chat
        else if (mData.get(position) instanceof Chat) {
            Chat message = (Chat) mData.get(position);
            ChatViewModel chatViewModel = new ChatViewModel(mActivity,
                    mConversation,
                    holder.getBinding().getRoot(),
                    position,
                    message,
                    mSelectedChat,
                    mSearchKeyWord,
                    drawable,
                    holder.getBinding() instanceof ListItemChatMeBinding,
                    new MyOnChatItemListener(holder));
            chatViewModel.updateBottomLineVisibility(isLastItem);
            holder.setVariable(BR.viewModel, chatViewModel);
            performManuallyCheckingToRemoveOldSeenAvatarOnBindChatItem((Chat) mData.get(position)); //Must call this statement first
            holdSeenAvatarViewItemInfo(message.getSeenParticipants(), chatViewModel);
            mItemViewModelMap.put(mData.get(position).getId(), chatViewModel);
            if (mData.get(position) instanceof Chat && TextUtils.equals(mData.get(position).getId(), "ac097d9a-fa57-4edf-8097-516dd837a23d")) {
                Timber.i("Position: " + chatViewModel.getPosition());
            }
        }

//        Timber.i("Size: " + mItemViewModelMap.size() + ", bind position: " + position + ", adapter position: " + holder.getAdapterPosition());
    }

    private SwipeRevealLayout getSwipeRevealLayoutFromViewItem(View itemRootView) {
        return itemRootView.findViewById(R.id.swipeRevealLayout);
    }

    private void holdSeenAvatarViewItemInfo(List<User> seenUsers, ChatMediaViewModel viewModel) {
        if (seenUsers != null) {
            for (User seenUser : seenUsers) {
                mSeenAvatarViewItemInfoHolder.put(seenUser.getId(), viewModel);
            }
        }
    }

    private String getFileNameForFileMediaType(Chat chat) {
        /**
         * Will retrieve only from the first position in media list.
         */

        if (chat.getMediaList() != null && !chat.getMediaList().isEmpty()) {
            if (!TextUtils.isEmpty(chat.getMediaList().get(0).getFileName())) {
                return chat.getMediaList().get(0).getFileName();
            }

            return chat.getMediaList().get(0).getUrl();
        }

        return "";
    }

    public int findChatIndex(BaseChatModel chat) {
        for (int i = 0; i < mData.size(); i++) {
            if (TextUtils.equals(mData.get(i).getId(), chat.getId())) {
                return i;
            }
        }

        return -1;
    }

    /**
     * @return true: mean add new message, otherwise, update
     */
    public boolean addLatestChat(Chat chat) {
        if (mData.isEmpty()) {
            checkToAddWelcomeSupportConversationItem();
        }

        if (mCallback != null) {
            mCallback.onAddNewMessage(chat);
        }

        Timber.i("addLatestChat: getChatType: " + chat.getChatType() +
                ", status: " + chat.getStatus() +
                ", id: " + chat.getId() +
                ", getSendingType: " + chat.getSendingType() +
                ", date: " + ChatHelper.getChatDateUTC(chat.getDate()) +
                ", getSenderId: " + chat.getSenderId() +
                ", content: " + chat.getContent() +
                ", is ignore seen status: " + chat.getIgnoreSeenStatus() +
                ", isDeleted: " + chat.isDeleted());
        String senderId = chat.getSenderId();
        if (isEmpty()) {
            setShowEmpty(false);
            safeNotifyItemRemove(0);
            setCanLoadMore(false);
        }

        int index = findChatIndex(chat);
        Timber.i("findChatIndex: " + index + ", chat size: " + mData.size());

        if (index < 0) {
            //Default adding index.
            int addIndex = mData.size();

            if (chat.getStatus() != MessageState.SENDING) {
                /*
                    In case of adding message is not sending by current user on current device,
                    will check if there is some sending message of current user in the list. If there is
                    any, we will find the oldest one and just add the received messaged above it.
                */
                for (int i = 0; i < mData.size(); i++) {
                    if (mData.get(i) instanceof Chat && (((Chat) mData.get(i)).getStatus() == MessageState.SENDING)) {
                        addIndex = i;
                        break;
                    }
                }
            }

            /*
                If typing indicator is already display, we have to make sure that the typing indicator
                will displaying at the last position of chat screen.
             */
            int typingIndicatorIndex = mData.indexOf(mTypingChatItem);
            if (typingIndicatorIndex >= 0 && addIndex >= typingIndicatorIndex) {
                addIndex = typingIndicatorIndex;
            }

            Timber.i("addIndex: " + addIndex);
            boolean shouldUpdateSeenStatus = false;
            if (!chat.getIgnoreSeenStatus()) {
//                Timber.i("Check update new seen message.");

                shouldUpdateSeenStatus = true;
                updateSeenStatus(chat, index);
            }

            /*
                - Handle use case of message synchronization between same account logged on different devices.
                - If read message or delivered message status is about to add into list display but the
                original message has not been displayed yet in list, so we have let the receiving finish their
                action like add seen avatar display and just before add into list adapter, we will set
                the correct sender id of message to make sure it display in the correct of message side.
             */
            Chat localChat = MessageDb.queryById(mActivity, chat.getId());
            if (localChat != null) {
                if (!TextUtils.equals(localChat.getSenderId(), chat.getSenderId())) {
                    chat.setSenderId(localChat.getSenderId());
                }
            }

            mData.add(addIndex, chat);
            safeNotifyItemInsertChange(addIndex);
            updatePartlyChatItemData(chat);
            checkToUpdatePreviousCurrentUserMessageOnReceivingNewMessageFromOtherUser(chat);

            //Notify update to remove bottom padding sof previous last item.
            if ((addIndex - 1) >= 0 &&
                    (addIndex - 1) < mData.size() &&
                    mData.get(addIndex - 1) instanceof Chat) {
                updatePartlyChatItemData((Chat) mData.get(addIndex - 1));
            }

            //Should call this method when add new chat
            checkToUpdateOneMostGroupChatTimeStampDisplay();
            removeUnreadMessageInternally();
            updateOneEarlierMessageBackgroundOfNewlyAddedMessage(addIndex);
            if (shouldUpdateSeenStatus) {
                checkToAddSeenUserIdForGroupConversation(chat, senderId);
            }

            return true;
        } else {
            updateChatItemPosition(chat, index);

            //Check SENT status
            if (chat.getStatus() == MessageState.SENT && !ChatHelper.isModifyChatType(chat)) {
                BaseChatModel existChat = mData.get(index);
                if (existChat instanceof Chat && (((Chat) existChat).getStatus() == MessageState.SEEN ||
                        ((Chat) existChat).getStatus() == MessageState.DELIVERED)) {
//                    Timber.i("Ignore update message to sent status since its status is already be delivered or seen.");
                    return false;
                }
            }

            //Check DELIVERED status
            if (chat.getStatus() == MessageState.DELIVERED) {
                BaseChatModel existChat = mData.get(index);
                if (existChat instanceof Chat && (((Chat) existChat).getStatus() == MessageState.SEEN ||
                        ((Chat) existChat).getStatus() == MessageState.DELIVERED)) {
//                    Timber.i("Ignore update message to delivered status since its status is already be delivered or seen.");
                    return false;
                }

                /*
                In case that the last message was seen or was not the message of the current user,
                the delivered status will be ignored.
                 */
                if (index < (mData.size() - 1) && mData.get(mData.size() - 1) instanceof Chat) {
                    Chat lastMessage = (Chat) mData.get(mData.size() - 1);
                    if (lastMessage.getStatus() == MessageState.SEEN ||
                            !TextUtils.equals(lastMessage.getSenderId(), mMyUser.getId())) {
//                        Timber.i("Ignore checking delivered status and update its status to seen.");
                        ((Chat) mData.get(index)).setStatus(MessageState.SEEN);
                        updatePartlyChatItemData(chat);
                        return false;

                    }
                }

                mData.set(index, chat);
                updatePartlyChatItemData(chat);
                Timber.i("Update delivered message.");
                return false;
            }

            //Check SEEN status
            if (chat.getStatus() == MessageState.SEEN) {
                if (chat.getIgnoreSeenStatus() && !isLastMessage(chat.getId())) {
                    if (mData.get(index) instanceof Chat) {
//                        Timber.i("Ignore update seen avatar of this message.");
                        ((Chat) mData.get(index)).setIgnoreSeenStatus(true);
                        updatePartlyChatItemData(chat);
                    }
                } else {
                    Timber.i("Update seen message.");
                    if (updateSeenStatus(chat, index)) {
                        mergeSeenAvatarList((Chat) mData.get(index), chat);
                        mData.set(index, chat);
                        updatePartlyChatItemData(chat);
                        checkToAddSeenUserIdForGroupConversation((Chat) mData.get(index), senderId);
                    }
                }

                return false;
            }

            //Check SENT status
            if (chat.getStatus() == MessageState.SENT && !ChatHelper.isModifyChatType(chat)) {
                if (shouldIgnoreUpdateSentMessageStatus(index)) {
//                    Timber.i("Ignore update message status of sent since the existing state was already got beyond over this state.");
                    return false;
                }

                mData.set(index, chat);
                updatePartlyChatItemData(chat);

                return false;
            } else if (chat.isDeleted() && (chat.getStatus() == MessageState.SENT || chat.getStatus() == MessageState.DELIVERED)) {
                Chat localChat = (Chat) mData.get(index);
                if (localChat != null) {
                    //Backup existing seen avatar list
                    chat.setSeenParticipants(localChat.getSeenParticipants());
                    mData.set(index, chat);
                    updatePartlyChatItemData(chat);
                }

                return false;
            }

            /*
            Will check to update message state. But first we have to make sure the updating existing
            chat status is correct.
             */
            BaseChatModel existedChat = mData.get(index);
            Timber.i("Reach default update chat status");
            if (existedChat instanceof Chat) {
                checkToUpdateExistMessageStatus((Chat) existedChat, chat.getStatus());
                chat.setStatus(((Chat) existedChat).getStatus());
            }
            mData.set(index, chat);
            updatePartlyChatItemData(chat);

            return false;
        }
    }

    private boolean isLastMessage(String messageId) {
        Chat lastMessageInList = getLastMessageInList();
        return lastMessageInList != null && TextUtils.equals(lastMessageInList.getId(), messageId);
    }

    private void mergeSeenAvatarList(Chat chat1, Chat chat2) {
        if (chat1.getSeen() == null) {
            chat1.setSeen(chat2.getSeen());
        } else {
            for (String id : chat2.getSeen()) {
                if (!chat1.getSeen().contains(id)) {
                    chat1.getSeen().add(id);
                }
            }
        }
    }

    private void updateOneEarlierMessageBackgroundOfNewlyAddedMessage(int newAddedIndex) {
        int previousIndex = newAddedIndex - 1;
        if (previousIndex >= 0 && previousIndex < mData.size()) {
            BaseChatModel baseChatModel = mData.get(previousIndex);
            if (baseChatModel instanceof Chat) {
                ChatMediaViewModel chatMediaViewModel = mItemViewModelMap.get(baseChatModel.getId());
                if (chatMediaViewModel != null) {
                    ChatUtility.GroupMessage drawable = ChatUtility.getGroupMessage(mData,
                            previousIndex,
                            mMyUser,
                            isMe(previousIndex));
                    chatMediaViewModel.updateMessageBackground(drawable);
                }
            }
        }
    }

    private void checkToUpdateOneMostGroupChatTimeStampDisplay() {
        for (int size = mData.size() - 1; size >= 0; size--) {
            ChatMediaViewModel chatMediaViewModel = mItemViewModelMap.get(mData.get(size).getId());
            if (chatMediaViewModel != null && chatMediaViewModel.isDisplayingGroupMessageTimeStamp()) {
//                Timber.i("checkToUpdateDisplayTimeStamp");
                chatMediaViewModel.checkToUpdateDisplayTimeStamp();
                break;
            }
        }
    }

    private void checkToUpdatePreviousCurrentUserMessageOnReceivingNewMessageFromOtherUser(Chat newChat) {
        /*
        Check to update previous messages before the adding new message as following:
        If the new message is not from current user and the previous messages are the message
        of current user and those messages'sstatus are not yet seen and they are displaying message status icon like
        sent or delivered, we will check to update those messages's state to seen and remove displaying
        icon of that message.
        */
        for (int size = mData.size() - 2; size >= 0; size--) {
            if (mData.get(size) instanceof Chat) {
                Chat previousChat = (Chat) mData.get(size);
                if (previousChat.getStatus() == MessageState.SEEN) {
                    break;
                }

                if (!TextUtils.equals(newChat.getSenderId(), mMyUser.getId()) &&
                        TextUtils.equals(previousChat.getSenderId(), mMyUser.getId())) {
                    previousChat.setStatus(MessageState.SEEN);
                    ChatMediaViewModel chatMediaViewModel = mItemViewModelMap.get(previousChat.getId());
                    if (chatMediaViewModel != null && chatMediaViewModel.getChat() != null) {
                        chatMediaViewModel.getChat().setStatus(MessageState.SEEN);
                        chatMediaViewModel.checkUpdateStatusIcon();
                    }
                }
            }
        }
    }

    private boolean shouldIgnoreUpdateSentMessageStatus(int existIndex) {
        /*
        If the existing message state is either Seen or delivered, we should not update any state of sent.
        And for the message type that is not support display sending message status will be ignored too.
         */
        if (existIndex < (mData.size() - 1)) {
            if (mData.get(existIndex) instanceof Chat) {
                ChatMediaViewModel chatMediaViewModel = mItemViewModelMap.get(mData.get(existIndex).getId());
                if (chatMediaViewModel != null && chatMediaViewModel.getChat() != null) {
                    return chatMediaViewModel.getChat().getStatus() == MessageState.DELIVERED ||
                            chatMediaViewModel.getChat().getStatus() == MessageState.SEEN;
                }
            }
        }

        return false;
    }

    private void performManuallyCheckingToRemoveOldSeenAvatarOnBindChatItem(Chat chat) {
        if (!MainApplication.isAppInBackground() &&
                chat.getSeenParticipants() != null &&
                !chat.getSeenParticipants().isEmpty() &&
                mItemViewModelMap.get(chat.getId()) == null) {
//            Timber.i("Will performManuallyCheckingToRemoveOldSeenAvatarOnBindChatItem");
            for (User seenParticipant : chat.getSeenParticipants()) {
                removeSeenAvatarFromOldViewItem(seenParticipant.getId());
            }
        }
    }

    private void updatePartlyChatItemData(Chat updateMessage) {
//        Timber.i("updatePartlyChatItemData: " + new Gson().toJson(updateMessage));
        if (updateMessage.isDeleted()) {
            int chatIndex = findChatIndex(updateMessage);
            //Need to update this message of another participant to removed view item message.
            if (chatIndex >= 0) {
                notifyItemChanged(chatIndex);
            }
        } else {
            ChatMediaViewModel chatMediaViewModel = mItemViewModelMap.get(updateMessage.getId());
            if (chatMediaViewModel != null) {
//                Timber.i("updatePartlyChatItemData: " + updateMessage.getId() + ", content: " + updateMessage.getContent());

                if (updateMessage.getStatus() == MessageState.SEEN) {
                    if (updateMessage.getSeenParticipants() != null) {
                        holdSeenAvatarViewItemInfo(updateMessage.getSeenParticipants(), chatMediaViewModel);
                        chatMediaViewModel.getChat().setSeenParticipants(updateMessage.getSeenParticipants());
                        chatMediaViewModel.bindSeenAvatar(true);
//                        Timber.i("updatePartlyChatItemData of seen");
                    }
                }

                //Update model properties
                chatMediaViewModel.getChat().setDelete(updateMessage.isDeleted());
                checkToUpdateExistMessageStatus(chatMediaViewModel.getChat(), updateMessage.getStatus());

                //Re-validate view's value
                if (updateMessage.getSendingType() == Chat.SendingType.UPDATE) {
                    chatMediaViewModel.getChat().setContent(updateMessage.getContent());
                    chatMediaViewModel.getChat().setEdited(updateMessage.getEdited());
                    chatMediaViewModel.getChat().setLastEditedDate(updateMessage.getLastEditedDate());
                    chatMediaViewModel.getChat().setLinkPreviewModel(updateMessage.getLinkPreviewModel());
                    chatMediaViewModel.onChatContentEdited();
                } else {
                    //Ignore update message icon status for update message type
                    chatMediaViewModel.setMessageStatusIconVisibility();
                    chatMediaViewModel.checkUpdateStatusIcon();
                }
                chatMediaViewModel.checkShareIconVisibility();
                chatMediaViewModel.revalidateChatSwipeAbilityStatus();
                chatMediaViewModel.revalidateMessageStatusText();
                chatMediaViewModel.updateBottomLineVisibility(isLastMessage(updateMessage.getId()));

                if (chatMediaViewModel instanceof FileChatViewModel) {
                    ((FileChatViewModel) chatMediaViewModel).bindFileStatus();
                }
            }
        }
    }

    private void checkToUpdatePreviousMessageStatusToSeenStatus() {
        /*
        Will check to update all previous message status to seen after the last seen message index.
         */
        if (mData != null && !mData.isEmpty()) {
            int lastSeenMessageIndex = -1;
            for (int size = mData.size() - 1; size >= 0; size--) {
                if (mData.get(size) instanceof Chat) {
                    if (((Chat) mData.get(size)).getStatus() == MessageState.SEEN) {
                        lastSeenMessageIndex = size;
                        break;
                    }
                }
            }

            for (int size = lastSeenMessageIndex - 1; size >= 0; size--) {
                if (mData.get(size) instanceof Chat) {
                    if (((Chat) mData.get(size)).getStatus() != MessageState.SEEN) {
                        ((Chat) mData.get(size)).setStatus(MessageState.SEEN);
                        ChatMediaViewModel chatMediaViewModel = mItemViewModelMap.get(mData.get(size).getId());
                        if (chatMediaViewModel != null && chatMediaViewModel.getChat() != null) {
                            chatMediaViewModel.getChat().setStatus(MessageState.SEEN);
                            chatMediaViewModel.checkUpdateStatusIcon();
                        }
//                        Timber.i("Update message status to seen.");
                    }
                }
            }
        }
    }

    private void checkToUpdateExistMessageStatus(Chat existedMessage, MessageState updateState) {
//        Timber.i("checkToUpdateExistMessageStatus: existedMessage: " + existedMessage.getStatus() + ", updateState: " + updateState);
        boolean shouldUpdate = false;
        if (updateState == MessageState.SEEN && existedMessage.getStatus() != MessageState.SEEN) {
            shouldUpdate = true;
        } else if ((existedMessage.getStatus() != MessageState.DELIVERED &&
                existedMessage.getStatus() != MessageState.SEEN) ||
                (existedMessage.getStatus() == MessageState.SENT && (updateState == MessageState.DELIVERED ||
                        updateState == MessageState.SEEN))) {
            shouldUpdate = true;
        }
        if (shouldUpdate) {
//            Timber.i("Will update existing message state: " + existedMessage.getStatus() + ", to " + updateState);
            existedMessage.setStatus(updateState);
        }
    }

    private void updateBottomLineVisibility(Chat updateMessage) {
        ChatMediaViewModel chatMediaViewModel = mItemViewModelMap.get(updateMessage.getId());
        if (chatMediaViewModel != null) {
            chatMediaViewModel.updateBottomLineVisibility(isLastMessage(updateMessage.getId()));
        }
    }

    private void updateChatItemPosition(Chat message, int position) {
        ChatMediaViewModel chatMediaViewModel = mItemViewModelMap.get(message.getId());
        if (chatMediaViewModel != null) {
            chatMediaViewModel.updatePosition(position);
        }
    }

    private int findPreviousSendingChatPosition(String checkingId) {
        for (int i = 0; i < mData.size(); i++) {
            if (mData.get(i) instanceof Chat &&
                    ((Chat) mData.get(i)).getStatus() == MessageState.SENDING &&
                    !TextUtils.equals(checkingId, mData.get(i).getId())) {
                return i;
            }
        }

        return -1;
    }

    private boolean isUserAlreadySeenLastMessage(Chat currentSeenMessage, int existIndex) {
        if (existIndex >= 0 && existIndex < (mData.size() - 1) && mData.get(mData.size() - 1) instanceof Chat) {
            //Check the last message
            Chat lastChat = (Chat) mData.get(mData.size() - 1);
            if (lastChat.getSeenParticipants() != null) {
                for (User seenParticipant : lastChat.getSeenParticipants()) {
                    if (TextUtils.equals(seenParticipant.getId(), currentSeenMessage.getSenderId()) &&
                            seenParticipant.isSeenMessage()) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /*
       Manage to update seen status for both individual and group chat when the seen message was
       broadcast by chat socket.
     */
    private boolean updateSeenStatus(Chat seenMessage, int existIndex) {
        String checkingId = seenMessage.getSenderId();
//        Timber.i("Updating seen at index: " + existIndex);
//        Timber.i("Sender id: " + checkingId + ", current user id: " + mMyUser.getId());
        if (TextUtils.isEmpty(checkingId)) {
            return false;
        }

        if (mMyUser.getId().equalsIgnoreCase(checkingId)) {
            /*
              Ignore display seen status avatar of current user sending message.
             */
//            Timber.i("Ignore update seen status avatar for current user.");
            return false;
        }

        if (isUserAlreadySeenLastMessage(seenMessage, existIndex)) {
//            Timber.i("isUserAlreadySeenLastMessage");
            return false;
        }

        //Check to remove seen avatar from previous message if has
        removeSeenAvatarFromOldViewItem(checkingId);

        //Add sender to the seen list of last message.
        List<User> seenAvatarList = getExistSeenAvatarFromDisplayedMessageItem(existIndex);
//        Timber.i("seenAvatarList size: " + seenAvatarList.size());
        if (seenAvatarList.isEmpty()) {
            User participantById = getParticipantById(checkingId);
            if (participantById != null) {
//                Timber.i("new seen avatar: " + participantById.getFullName());
                seenAvatarList.add(participantById.cloneForSeenStatus());
                seenMessage.setSeenParticipants(seenAvatarList);
            }
        } else {
            boolean alreadySeen = false;
            for (User seenParticipant : seenAvatarList) {
                if (seenParticipant.getId().matches(checkingId)) {
                    alreadySeen = true;
                    seenParticipant.setSeenMessage(true);
                    //Backup old seen avatar list for message.
                    seenMessage.setSeenParticipants(seenAvatarList);
//                    Timber.i("User already in the seen message list.");
                    break;
                }
            }

            if (!alreadySeen) {
                User participantById = getParticipantById(checkingId);
                if (participantById != null) {
//                    Timber.i("add new seen avatar: " + participantById.getFullName());
                    if (!isGroupConversation()) {
                        //Normally there is only one seen avatar for individual chat message.
                        seenAvatarList.clear();
                    }
                    seenAvatarList.add(participantById.cloneForSeenStatus());
                    seenMessage.setSeenParticipants(seenAvatarList);
                }
            }
        }

        checkToInjectOriginalGroupMessageSenderInSeenAvatarForLastMessage(seenMessage, existIndex);

        /*
        To maintain the original sender id of message like saved in local database for there will many
        changes of sender id for the display.
         */
        if (existIndex >= 0 && mData.get(existIndex) instanceof Chat) {
            seenMessage.setSenderId(((Chat) mData.get(existIndex)).getSenderId());
        }

        checkIgnoreSeenMessageType(seenMessage, mData);

        return true;
    }

    private void checkToAddSeenUserIdForGroupConversation(Chat existingChat, String seenUserId) {
        if (isGroupConversation() && existingChat != null) {
            RealmList<String> seen = existingChat.getSeen();
            if (seen == null) {
                seen = new RealmList<>();
                seen.add(seenUserId);
                existingChat.setSeen(seen);
                ChatMediaViewModel chatMediaViewModel = mItemViewModelMap.get(existingChat.getId());
                if (chatMediaViewModel != null) {
                    existingChat.setStatus(MessageState.SEEN);
                    chatMediaViewModel.updateSeenUserList(seen);
//                    Timber.i("Add seen user into seen list of chat of user id " + seenUserId +
//                            ", content: " + existingChat.getContent() +
//                            ", id: " + existingChat.getId() +
//                            ", seen: " + existingChat.getSeen().size() +
//                            ", chat: hascode: " + existingChat.hashCode() +
//                            ", seen hascode: " + existingChat.getSeen().hashCode() +
//                            ", seen: " + new Gson().toJson(existingChat.getSeen()));
                }

            } else if (!seen.contains(seenUserId)) {
                seen.add(seenUserId);
                existingChat.setSeen(seen);
                ChatMediaViewModel chatMediaViewModel = mItemViewModelMap.get(existingChat.getId());
                if (chatMediaViewModel != null) {
                    existingChat.setStatus(MessageState.SEEN);
                    chatMediaViewModel.updateSeenUserList(seen);
//                    Timber.i("Add seen user into seen list of chat of user id " + seenUserId +
//                            ", content: " + existingChat.getContent() +
//                            ", id: " + existingChat.getId() +
//                            ", seen: " + existingChat.getSeen().size() +
//                            ", chat: hascode: " + existingChat.hashCode() +
//                            ", seen hascode: " + existingChat.getSeen().hashCode() +
//                            ", seen: " + new Gson().toJson(existingChat.getSeen()));
                }
            }
        }
    }

    private void checkToInjectOriginalGroupMessageSenderInSeenAvatarForLastMessage(Chat seenMessage, int existIndex) {
        /*
        Note: This logic applied as current user is not checking message owner which original sender seen
        avatar of that message should be displayed automatically and below logic will assure that that
        avatar will added.
         */
        if (seenMessage.isGroup() &&
                existIndex >= 0 &&
                existIndex == (mData.size() - 1) &&
                mData.get(existIndex) instanceof Chat &&
                (seenMessage.getSeenParticipants() != null && !seenMessage.getSeenParticipants().isEmpty())) {
            Chat existingChat = (Chat) mData.get(existIndex);
            //Check original message sender is not current user.
            if (!TextUtils.equals(mMyUser.getId(), existingChat.getSenderId())) {
                //Check if the original message sender avatar added into seen avatar list
                boolean existed = false;
                for (User seenParticipant : seenMessage.getSeenParticipants()) {
                    if (TextUtils.equals(seenParticipant.getId(), existingChat.getSenderId())) {
                        existed = true;
                        break;
                    }
                }

                if (!existed) {
                    removeSeenAvatarFromOldViewItem(existingChat.getSenderId());
                    seenMessage.getSeenParticipants().add(existingChat.getSender());
//                    Timber.i("checkToInjectOriginalGroupMessageSenderInSeenAvatarForLastMessage");
                }
            }
        }
    }

    private void removeSeenAvatarFromOldViewItem(String seenUserId) {
        ChatMediaViewModel chatMediaViewModel = mSeenAvatarViewItemInfoHolder.get(seenUserId);
        if (chatMediaViewModel != null &&
                chatMediaViewModel.getChat() != null &&
                chatMediaViewModel.getChat().getSeenParticipants() != null) {
            int existIndex = -1;
            List<User> updateSeenList = new ArrayList<>();
            for (int i = 0; i < chatMediaViewModel.getChat().getSeenParticipants().size(); i++) {
                if (TextUtils.equals(chatMediaViewModel.getChat().getSeenParticipants().get(i).getId(), seenUserId)) {
                    existIndex = i;
                } else {
                    updateSeenList.add(chatMediaViewModel.getChat().getSeenParticipants().get(i));
                }
            }

            chatMediaViewModel.getChat().setSeenParticipants(updateSeenList);
            if (existIndex >= 0) {
                chatMediaViewModel.bindSeenAvatar(true);
                updateSeenAvatarListOfChatData(updateSeenList, chatMediaViewModel.getChat().getId());
//                Timber.i("Remove seen user from old view. " + chatMediaViewModel.getChat().getId() +
//                        ", content: " + chatMediaViewModel.getChat().getContent() +
//                        ", seen: " + chatMediaViewModel.getChat().getSeenParticipants().size());
            }
        }
    }

    private void updateSeenAvatarListOfChatData(List<User> updateSeenList, String chatId) {
        for (BaseChatModel datum : mData) {
            if (TextUtils.equals(datum.getId(), chatId) && datum instanceof Chat) {
                ((Chat) datum).setSeenParticipants(updateSeenList);
                break;
            }
        }
    }

    private void checkIgnoreSeenMessageType(Chat seenMessage, List<BaseChatModel> lists) {
        if (ChatHelper.isCallEventTypes(seenMessage) || isGroupModificationMessageType(seenMessage)) {
            if (seenMessage.getSeenParticipants() != null && !seenMessage.getSeenParticipants().isEmpty()) {
                for (int ind = lists.size() - 2; ind >= 0; ind--) {
                    if (lists.get(ind) instanceof Chat) {
                        Chat existingChat = (Chat) lists.get(ind);
                        if (!ChatHelper.isCallEventTypes(existingChat) && !isGroupModificationMessageType(existingChat)) {
                            if (existingChat.getSeenParticipants() != null && !existingChat.getSeenParticipants().isEmpty() && isGroupConversation()) {
                                for (User user : seenMessage.getSeenParticipants()) {
                                    boolean currentUserExisted = false;
                                    for (User exist : existingChat.getSeenParticipants()) {
                                        if (exist.getId().equals(user.getId())) {
                                            exist.setSeenMessage(true);
                                            currentUserExisted = true;
                                        }
                                    }
                                    if (!currentUserExisted) {
                                        user.setSeenMessage(true);
                                        existingChat.getSeenParticipants().add(user);
                                    }
                                }
                            } else {
                                // If not group chat, just copy over the seen participant list
                                existingChat.setSeenParticipants(seenMessage.getSeenParticipants());
                            }
                            updatePartlyChatItemData(existingChat);
//                            Timber.i("Seen list at index: " + ind + "\n" + new Gson().toJson(existingChat));
                            seenMessage.setSeenParticipants(new ArrayList<>());
                            return;
                        }
                    }
                }
            }
        }
    }

    private List<User> getExistSeenAvatarFromDisplayedMessageItem(int messageIndex) {

        if (messageIndex >= 0 && mData.get(messageIndex) instanceof Chat &&
                ((Chat) mData.get(messageIndex)).getSeenParticipants() != null) {
            return new ArrayList<>(((Chat) mData.get(messageIndex)).getSeenParticipants());
        }

        return new ArrayList<>();
    }

    private User getParticipantById(String id) {
        if (!TextUtils.isEmpty(id) &&
                mConversation != null &&
                mConversation.getParticipants() != null) {
            //Ignore sender avatar
            if (id.matches(mMyUser.getId())) {
                return null;
            }
//            Timber.i("mConversation.getParticipants(): " + new Gson().toJson(mConversation.getParticipants()));
            for (User participant : mConversation.getParticipants()) {
                if (participant.getId().matches(id)) {
                    return participant;
                }
            }
        }

//        Timber.i("Participant not found.");
        return null;
    }

    public void addByLoadMore(List<BaseChatModel> moreChats, boolean isNextPage) {
        setCanLoadMore(isNextPage);
        mData.addAll(0, moreChats);
        notifyItemRangeInserted(0, moreChats.size());
        if (canLoadMore()) {
            notifyItemChanged(moreChats.size() + 1);
        } else {
            notifyItemChanged(moreChats.size());
        }
    }

    public void checkToAddWelcomeSupportConversationItem() {
        //Welcome support message item will be added in earliest order in chat screen.
        if (ConversationType.fromValue(mConversation.getType()) == ConversationType.SUPPORT &&
                !canLoadMore() &&
                mWelcomeSupportConversationModel == null) {
            mWelcomeSupportConversationModel = new WelcomeSupportConversationModel();
            mData.add(0, mWelcomeSupportConversationModel);
            safeNotifyItemInsertChange(0);
            if (mCallback != null) {
                mCallback.onAddWelcomeSupportGroupMessageItem();
            }
        }
    }

    public void addByLoadMoreFromBottom(List<BaseChatModel> moreItems, boolean canLoadMoreFromBottom) {
        setCanLoadMoreFromBottom(canLoadMoreFromBottom);
        if (moreItems != null && !moreItems.isEmpty()) {
            checkToRemoveOldSeenAvatarWhenAddNewMessage(moreItems);
            int previousItemCount = getItemCount();
            mData.addAll(moreItems);
            notifyItemRangeInserted(previousItemCount + 1, moreItems.size());
        }
    }

    private void checkToUpdateExistingChatContentFromServer(List<BaseChatModel> updateList) {
//        Timber.i("checkToUpdateExistingChatContentFromServer: " + updateList.size());
        if (!updateList.isEmpty()) {
//            Timber.i("Last: " + new Gson().toJson(updateList.get(updateList.size() - 1)));
            for (BaseChatModel updateChat : updateList) {
                for (int i = 0; i < mData.size(); i++) {
                    if (TextUtils.equals(mData.get(i).getId(), updateChat.getId())) {
                        if (updateChat instanceof Chat) {
                            removePreviousSeenAvatarIfExisted((Chat) updateChat);
                            mData.set(i, updateChat);
                            updatePartlyChatItemData((Chat) updateChat);
                        }

                        break;
                    }
                }
            }
        }
    }

    public void onGotMessageFromServer(List<BaseChatModel> chatFromServer) {
        checkToUpdateExistingChatContentFromServer(chatFromServer);
        checkToAddNewMessageFromServer(chatFromServer);
    }

    public void checkToAddNewMessageFromServer(List<BaseChatModel> newChat) {
        checkToAddCurrentUserIdIntoSeenIdListOfMessage(newChat);
        int originalSize = newChat.size();
        removeExistedChatFromServer(newChat);
        Timber.i("checkToAddNewMessageFromServer: original size: " + originalSize + ", valid size: " + newChat.size());
        if (newChat.isEmpty()) {
            return;
        }
        ChatHelper.logMessages("checkToAddNewMessageFromServer", ChatHelper.getChatList(newChat));
        checkToRemoveOldSeenAvatarWhenAddNewMessage(newChat);
        Chat earliestChatFromList = getEarliestChatFromList(newChat);
        Timber.i("earliestChatFromList: " + new Gson().toJson(earliestChatFromList));
        if (isAllNewMessages(earliestChatFromList)) {
            Timber.i("Start adding new messages from server into list.");

            //Check if the typing indicator is already presenting
            int typingIndicatorIndex = mData.indexOf(mTypingChatItem);
            Timber.i("typingIndicatorIndex: " + typingIndicatorIndex);
            if (typingIndicatorIndex <= 0) {
                int previousItemCount = getItemCount();
                int previousLastItem = canLoadMore() ? previousItemCount - 2 : previousItemCount - 1;
                mData.addAll(newChat);

                notifyItemRangeInserted(previousItemCount + 1, newChat.size());
                //Need to notify to remove bottom padding of previous last item
                if (previousLastItem < mData.size() && mData.get(previousLastItem) instanceof Chat) {
                    updateBottomLineVisibility((Chat) mData.get(previousLastItem));
                }
            } else {
                mData.addAll(typingIndicatorIndex, newChat);
                notifyItemRangeInserted(typingIndicatorIndex + 1, newChat.size());
            }
            checkToUpdatePreviousMessageStatusToSeenStatus();
        } else {
            Timber.i("Will check to insert missing or new message from server into list.");
            for (BaseChatModel baseChatModel : newChat) {
                if (baseChatModel instanceof Chat) {
                    int indexToInsertChat = findIndexToInsertChat((Chat) baseChatModel);
                    //Check if the typing indicator is already presenting
                    int typingIndicatorIndex = mData.indexOf(mTypingChatItem);
                    Timber.i("indexToInsertChat: " + indexToInsertChat + ", current list size: " + mData.size() +
                            ", typingIndicatorIndex: " + typingIndicatorIndex + "\n of new chat: " + new Gson().toJson(baseChatModel));
                    /*
                        If the typing indicator is already presenting, we have to make sure when add new
                        message that indicator always at the bottom of chat screen
                     */
                    if (typingIndicatorIndex >= 0 && indexToInsertChat >= typingIndicatorIndex) {
                        indexToInsertChat = typingIndicatorIndex;
                    }

                    if (indexToInsertChat >= 0) {
                        mData.add(indexToInsertChat, baseChatModel);
                        safeNotifyItemInsertChange(indexToInsertChat);
                        checkToUpdateChatItemStoringPositionAfterApplyInsertRange(indexToInsertChat);
                    }
                }
            }
        }
    }

    public void checkToAddCurrentUserIdIntoSeenIdListOfMessage(List<BaseChatModel> newChat) {
        for (BaseChatModel baseChatModel : newChat) {
            if (baseChatModel instanceof Chat) {
                if (((Chat) baseChatModel).getSeen() == null) {
                    ((Chat) baseChatModel).setSeen(new RealmList<>());
                }
                if (!((Chat) baseChatModel).getSeen().contains(mMyUser.getId())) {
                    ((Chat) baseChatModel).getSeen().add(mMyUser.getId());
                }
            }
        }
    }

    private void checkToUpdateChatItemStoringPositionAfterApplyInsertRange(int insertIndex) {
        //Update item position index after the insert range position
        for (int size = mData.size() - 1; size > insertIndex; size--) {
            if (mData.get(size) instanceof Chat) {
                ChatMediaViewModel chatMediaViewModel = mItemViewModelMap.get(mData.get(size).getId());
                if (chatMediaViewModel instanceof AbsSupportBottomPaddingChatItemViewModel) {
                    ((AbsSupportBottomPaddingChatItemViewModel) chatMediaViewModel).updatePosition(size);
                }
            }
        }
    }

    private Chat getLatestChatFromList(List<BaseChatModel> newChat) {
        for (int size = newChat.size() - 1; size >= 0; size--) {
            if (newChat.get(size) instanceof Chat) {
                return (Chat) newChat.get(size);
            }
        }

        return null;
    }

    private Chat getEarliestChatFromList(List<BaseChatModel> newChat) {
        for (int i = 0; i < newChat.size(); i++) {
            if (newChat.get(i) instanceof Chat) {
                return (Chat) newChat.get(i);
            }
        }

        return null;
    }

    private void removeExistedChatFromServer(List<BaseChatModel> newChat) {
        List<BaseChatModel> validList = new ArrayList<>();
        for (BaseChatModel baseChatModel : newChat) {
            int chatIndex = findChatIndex(baseChatModel);
            if (chatIndex < 0) {
                validList.add(baseChatModel);
            }
        }
        newChat.clear();
        newChat.addAll(validList);
    }

    private Chat getLastMessageInList() {
        for (int size = mData.size() - 1; size >= 0; size--) {
            if (mData.get(size) instanceof Chat) {
                return (Chat) mData.get(size);
            }
        }

        return null;
    }

    private boolean isAllNewMessages(Chat earliestMessageInList) {
        if (earliestMessageInList == null) {
            return false;
        }

        Chat lastMessageInList = getLastMessageInList();
        if (lastMessageInList != null) {
            return earliestMessageInList.getTime() > lastMessageInList.getTime();
        } else {
            return true;
        }
    }

    private int findIndexToInsertChat(Chat insertChat) {
        if (insertChat != null) {
            for (int size = mData.size() - 1; size >= 0; size--) {
                if (mData.get(size) instanceof Chat) {
                    Chat chat = (Chat) mData.get(size);
                    if (insertChat.getTime() >= chat.getTime()) {
                        return size + 1;
                    }
                }
            }
        }

        if (!mData.isEmpty()) {
            return 0;
        }

        return -1;
    }

    private void checkToRemoveOldSeenAvatarWhenAddNewMessage(List<BaseChatModel> newChats) {
        if (!newChats.isEmpty()) {
            for (int size = newChats.size() - 1; size >= 0; size--) {
                if (newChats.get(size) instanceof Chat) {
                    removePreviousSeenAvatarIfExisted((Chat) newChats.get(size));
                }
            }
        }
    }

    private void removePreviousSeenAvatarIfExisted(Chat newAddedMessage) {
        if (newAddedMessage.getSeenParticipants() != null && !newAddedMessage.getSeenParticipants().isEmpty()) {
            for (User seenParticipant : newAddedMessage.getSeenParticipants()) {
                ChatMediaViewModel chatMediaViewModel = mSeenAvatarViewItemInfoHolder.get(seenParticipant.getId());
                if (chatMediaViewModel != null && TextUtils.equals(chatMediaViewModel.getChat().getId(), newAddedMessage.getId())) {
                    /*
                      Will ignore checking if the checking user is already in the seen list of new message to
                      be checking.
                     */
                    continue;
                }

                if (chatMediaViewModel != null &&
                        chatMediaViewModel.getChat() != null &&
                        chatMediaViewModel.getChat().getSeenParticipants() != null &&
                        !chatMediaViewModel.getChat().getSeenParticipants().isEmpty()) {
                    //Remove user from that previous message
                    List<User> updateList = new ArrayList<>();
                    for (User participant : chatMediaViewModel.getChat().getSeenParticipants()) {
                        if (!TextUtils.equals(participant.getId(), seenParticipant.getId())) {
                            updateList.add(participant);
                        }
                    }
                    chatMediaViewModel.getChat().setSeenParticipants(updateList);
                    chatMediaViewModel.bindSeenAvatar(true);
                }
            }
        }
    }

    public boolean isAllDisplayMessageAreIgnoreDisplaySeenAvatarType() {
        boolean isAllDisplayMessageAreIgnoreDisplaySeenAvatarType = true;
        for (BaseChatModel datum : mData) {
            if (datum instanceof Chat && !ChatHelper.isIgnoreDisplaySeenAvatarMessage((Chat) datum)) {
                isAllDisplayMessageAreIgnoreDisplaySeenAvatarType = false;
                break;
            }
        }

        return isAllDisplayMessageAreIgnoreDisplaySeenAvatarType;
    }

    public void addLoadMoreView() {
        setCanLoadMore(true);
        notifyItemInserted(0);
    }

    public void deleteItem(Chat chat) {
        int position = mData.indexOf(chat);
        if (position >= 0 && position < mData.size()) {
            BaseChatModel data = mData.get(position);
            if (data instanceof Chat) {
                ((Chat) data).setDelete(true);
            }
            notifyItemChanged(position);
        }
    }

    public Chat getOldestChatInList() {
        for (BaseChatModel datum : mData) {
            if (datum instanceof Chat) {
                return (Chat) datum;
            }
        }
        return null;
    }

    public Chat getLatestChatInList() {
        for (int size = mData.size() - 1; size >= 0; size--) {
            BaseChatModel datum = mData.get(size);
            if (datum instanceof Chat) {
                return (Chat) datum;
            }
        }

        return null;
    }

    public void onUploadOrDownloadSuccess(boolean isUpload,
                                          String requestId,
                                          Chat chat,
                                          String mediaId,
                                          String imageUrl,
                                          String thumb) {
        FileChatViewModel fileChatViewModel = mFileChatViewModelHashMap.get(chat.getId());
        if (fileChatViewModel != null) {
            fileChatViewModel.onUploadOrDownloadSuccess(isUpload, requestId, chat, mediaId, imageUrl, thumb);
        }
    }

    public void onUploadOrDownloadFailed(boolean isUpload,
                                         String requestId,
                                         Chat chat,
                                         String mediaId,
                                         Throwable ex) {
        FileChatViewModel fileChatViewModel = mFileChatViewModelHashMap.get(chat.getId());
        if (fileChatViewModel != null) {
            fileChatViewModel.onUploadOrDownloadFailed(isUpload, requestId, chat, mediaId, ex);
        }
    }

    public void onDownloadGetStarted(String requestId, Chat chat, String mediaId) {
        FileChatViewModel fileChatViewModel = mFileChatViewModelHashMap.get(chat.getId());
        if (fileChatViewModel != null) {
            fileChatViewModel.onDownloadGetStarted(requestId, chat, mediaId);
        }
    }

    public void onUploadOrDownloadProgressing(boolean isUpload,
                                              String requestId,
                                              Chat chat,
                                              String mediaId,
                                              int percent,
                                              long percentBytes,
                                              long totalBytes) {
        FileChatViewModel fileChatViewModel = mFileChatViewModelHashMap.get(chat.getId());
        if (fileChatViewModel != null) {
            fileChatViewModel.onUploadOrDownloadProgressing(isUpload, requestId,
                    chat,
                    mediaId,
                    percent,
                    percentBytes,
                    totalBytes);
        }
    }

    private void setMarginRemovedMessage(BindingViewHolder holder, ChatUtility.GroupMessage drawable) {
        if (holder.getBinding() instanceof ListItemChatMeRemoveMessageBinding) {
            ListItemChatMeRemoveMessageBinding binding = (ListItemChatMeRemoveMessageBinding) holder.getBinding();
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) binding.message.getLayoutParams();
            setMarginForMessage(params, drawable);
        }
        if (holder.getBinding() instanceof ListItemChatRecipientRemoveMessageBinding) {
            ListItemChatRecipientRemoveMessageBinding binding = (ListItemChatRecipientRemoveMessageBinding) holder.getBinding();
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) binding.message.getLayoutParams();
            setMarginForMessage(params, drawable);
        }
    }

    private void setMarginForMessage(RelativeLayout.LayoutParams params, ChatUtility.GroupMessage drawable) {
        if (drawable.getChatBg() == ChatBg.ChatMeTop) {
            params.setMargins(0,
                    0,
                    0,
                    mActivity.getResources().getDimensionPixelSize(R.dimen.space_too_small));
        } else if (drawable.getChatBg() == ChatBg.ChatMeMid) {
            params.setMargins(0,
                    mActivity.getResources().getDimensionPixelSize(R.dimen.space_too_small),
                    0,
                    mActivity.getResources().getDimensionPixelSize(R.dimen.space_too_small));
        } else if (drawable.getChatBg() == ChatBg.ChatMeBottom) {
            params.setMargins(0,
                    mActivity.getResources().getDimensionPixelSize(R.dimen.space_too_small),
                    0,
                    0);
        }
    }

    public String getDisplayChatContent(String chatId) {
        ChatMediaViewModel chatMediaViewModel = mItemViewModelMap.get(chatId);
        if (chatMediaViewModel != null) {
            return chatMediaViewModel.getDisplayChatContent();
        }

        return null;
    }

    private class MyOnChatItemListener implements OnChatItemListener {
        private final BindingViewHolder mHolder;

        MyOnChatItemListener(BindingViewHolder holder) {
            mHolder = holder;
        }

        @Override
        public void onImageClick(List<Media> list, int productMediaPosition) {
            mOnImageClickListener.onImageClick(list, productMediaPosition);
        }

        @Override
        public void onFileClick(Chat chat, Media selectedMedia) {
            mOnImageClickListener.onFileClick(chat, selectedMedia);
        }

        @Override
        public void onChatItemLongPressClick(Chat chat, @Nullable Media media) {
            mOnImageClickListener.onChatItemLongPressClick(chat, media);
        }

        @Override
        public void onForwardClick(Chat chat) {
            mOnImageClickListener.onForwardClick(chat);
        }

        @Override
        public void onMessageRetryClick(Chat chat) {
            mOnImageClickListener.onMessageRetryClick(chat);
        }

        @Override
        public User getUser(String id) {
            return mOnImageClickListener.getUser(id);
        }

        @Override
        public void onCallAgainOrBack(Chat chat, AbsCallService.CallType callType) {
//            Timber.i("onCallAgainOrBack: callType: " + callType);
            mOnImageClickListener.onCallAgainOrBack(chat, callType);
        }

        @Override
        public void onRepliedChatClicked(Chat repliedChat, boolean isInInputReplyMode) {
            mOnImageClickListener.onRepliedChatClicked(repliedChat, isInInputReplyMode);
        }

        @Override
        public void onSwipeToReply(Chat repliedChat) {
            mOnImageClickListener.onSwipeToReply(repliedChat);
        }

        @Override
        public void onCalculateSeenDisplayNameHeight(String chatId,
                                                     String seenText,
                                                     boolean isRecipientSide,
                                                     ChatMediaViewModel.CalculateSeenNameDisplayCallback callback) {
            mOnImageClickListener.onCalculateSeenDisplayNameHeight(chatId, seenText, isRecipientSide, callback);
        }

        @Override
        public void onGifVideoPlayerCreated(String mediaId, SimpleExoPlayer player) {
            mGifVideoPlayerMap.put(mediaId, player);
        }

        @Override
        public SimpleExoPlayer getGifVideoPlayer(String mediaId) {
            return mGifVideoPlayerMap.get(mediaId);
        }

        @Override
        public void onGifVideoPlayerReleased(String mediaId) {
            mGifVideoPlayerMap.remove(mediaId);
        }
    }

    public void releaseAllGifVideoPlayer() {
        for (Map.Entry<String, SimpleExoPlayer> entry : mGifVideoPlayerMap.entrySet()) {
            if (entry.getValue() != null) {
                entry.getValue().release();
            }
        }
        mGifVideoPlayerMap.clear();
    }

    public interface ChatAdapterCallback {
        void onAddNewMessage(Chat chat);

        void onAddWelcomeSupportGroupMessageItem();

        boolean isScreenInBackground();
    }
}
