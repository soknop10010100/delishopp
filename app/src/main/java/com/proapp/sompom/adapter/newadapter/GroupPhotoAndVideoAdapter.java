package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ListItemGroupMediaSectionBinding;
import com.proapp.sompom.decorataor.GridItemDecorator;
import com.proapp.sompom.model.GroupMediaSection;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemGroupPhotoViewModel;

import java.util.List;

/**
 * Created by Veasna Chhom on 7/27/20.
 */
public class GroupPhotoAndVideoAdapter extends AbsGroupMediaAdapter<Media> {

    private static final int COLUMN_COUNT = 3;

    public GroupPhotoAndVideoAdapter(List<GroupMediaSection<Media>> datas) {
        super(datas);
    }

    @Override
    protected void bindSectionItem(ListItemGroupMediaSectionBinding binding, GroupMediaSection<Media> section) {
        GroupPhotoAndVideoItemAdapter adapter = new GroupPhotoAndVideoItemAdapter(section.getMediaList());
        binding.recyclerView.setLayoutManager(new GridLayoutManager(binding.getRoot().getContext(), COLUMN_COUNT));
        if (binding.recyclerView.getItemDecorationCount() <= 0) {
            binding.recyclerView.addItemDecoration(new GridItemDecorator(binding.recyclerView
                    .getResources()
                    .getDimensionPixelSize(R.dimen.one_px)));
        }
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setAdapter(adapter);
    }

    public class GroupPhotoAndVideoItemAdapter extends RecyclerView.Adapter<BindingViewHolder> {

        private List<Media> mData;

        public GroupPhotoAndVideoItemAdapter(List<Media> data) {
            mData = data;
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        @NonNull
        @Override
        public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_group_photo).build();
        }

        @Override
        public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {

            ListItemGroupPhotoViewModel viewModel = new ListItemGroupPhotoViewModel(mData.get(position));
            holder.setVariable(BR.viewModel, viewModel);
            holder.getBinding().getRoot().setOnClickListener(v -> onItemClicked(mData.get(position)));
        }
    }
}
