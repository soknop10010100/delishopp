package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ListItemGroupMediaSectionBinding;
import com.proapp.sompom.model.GroupMediaSection;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.utils.MediaUtil;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemGroupFileViewModel;

import java.util.List;

/**
 * Created by Veasna Chhom on 7/27/20.
 */
public class GroupFileAdapter extends AbsGroupMediaAdapter<Media> {


    public GroupFileAdapter(List<GroupMediaSection<Media>> datas) {
        super(datas);
    }

    @Override
    protected void bindSectionItem(ListItemGroupMediaSectionBinding binding, GroupMediaSection<Media> section) {
        GroupFileItemAdapter adapter = new GroupFileItemAdapter(section.getMediaList());
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(binding.getRoot().getContext()));
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setAdapter(adapter);
    }

    public class GroupFileItemAdapter extends RecyclerView.Adapter<BindingViewHolder> {

        private List<Media> mData;

        public GroupFileItemAdapter(List<Media> data) {
            mData = data;
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        @NonNull
        @Override
        public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_group_file).build();
        }

        @Override
        public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
            Media media = MediaUtil.checkIfMediaExist(mData.get(position));
            ListItemGroupFileViewModel viewModel = new ListItemGroupFileViewModel(holder.itemView.getContext(),
                    media);
            holder.setVariable(BR.viewModel, viewModel);
        }
    }
}
