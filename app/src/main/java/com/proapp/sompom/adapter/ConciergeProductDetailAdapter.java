package com.proapp.sompom.adapter;

import android.view.ViewGroup;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.model.emun.ConciergeMenuItemOptionType;
import com.proapp.sompom.model.result.ConciergeMenuItemOption;
import com.proapp.sompom.model.result.ConciergeMenuItemOptionSection;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.ListItemConciergeOptionItemViewModel;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Or Vitovongsak on 13/9/21.
 */

public class ConciergeProductDetailAdapter
        extends RefreshableAdapter<ConciergeMenuItemOption, BindingViewHolder> {

    private float mDefaultPrice;
    private final ConciergeMenuItemOptionSection mItemOptionSection;
    private ConciergeProductDetailAdapterListener mListener;
    private final HashMap<String, ListItemConciergeOptionItemViewModel> mViewModelMap = new HashMap<>();

    public ConciergeProductDetailAdapter(ConciergeMenuItemOptionSection itemOptionSection, float defaultPrice) {
        super(itemOptionSection.getOptionItems());
        setCanLoadMore(false);
        mItemOptionSection = itemOptionSection;
        mDefaultPrice = defaultPrice;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_option_item).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ConciergeMenuItemOption option = mDatas.get(position);
        ConciergeMenuItemOptionType optionType = ConciergeMenuItemOptionType.from(mItemOptionSection.getType());

        if (mViewModelMap.get(option.getId()) == null) {
            ListItemConciergeOptionItemViewModel viewModel = new ListItemConciergeOptionItemViewModel(holder.getContext(),
                    option,
                    optionType,
                    mDefaultPrice,
                    mItemOptionSection.isDisplayFullPrice(),
                    updatedOption -> {
                        checkShouldShowError();
                        if (mListener != null) {
                            mListener.onOptionClicked(updatedOption);
                        }

                    });
            mViewModelMap.put(option.getId(), viewModel);
        }

        ListItemConciergeOptionItemViewModel viewModel = mViewModelMap.get(option.getId());

        holder.setVariable(BR.viewModel, viewModel);
    }

    public void setListener(ConciergeProductDetailAdapterListener listener) {
        mListener = listener;
    }

    public void updateItemOptionData(List<ConciergeMenuItemOption> newData) {
        for (ConciergeMenuItemOption option : newData) {
            ListItemConciergeOptionItemViewModel viewModel = mViewModelMap.get(option.getId());
            if (viewModel != null) {
                viewModel.initData(option);
            }
        }
    }

    public void checkShouldShowError() {
        if (ConciergeMenuItemOptionType.from(mItemOptionSection.getType()) == ConciergeMenuItemOptionType.VARIATION) {
            boolean isRequiredOptionSelected = false;
            for (ConciergeMenuItemOption option : mDatas) {
                ListItemConciergeOptionItemViewModel viewModel = mViewModelMap.get(option.getId());
                if (viewModel != null) {
                    if (viewModel.getIsSelected().get()) {
                        isRequiredOptionSelected = true;
                    }
                }
            }

            for (ConciergeMenuItemOption option : mDatas) {
                ListItemConciergeOptionItemViewModel viewModel = mViewModelMap.get(option.getId());
                if (viewModel != null) {
                    viewModel.shouldShowError(!isRequiredOptionSelected);
                }
            }
        }
    }

    public void makeItemSelection(String optionId) {
        ListItemConciergeOptionItemViewModel viewModel = mViewModelMap.get(optionId);
        if (viewModel != null) {
            viewModel.onOptionClick(null);
        }
    }

    public interface ConciergeProductDetailAdapterListener {
        void onOptionClicked(ConciergeMenuItemOption option);
    }
}
