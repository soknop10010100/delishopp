package com.proapp.sompom.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.proapp.sompom.newui.fragment.MoreGameFragment;

import java.util.List;

/**
 * Created by he.rotha on 4/29/16.
 */
public class MoreGameAdapter extends FragmentStatePagerAdapter {
    private List<MoreGameFragment> mGameFragments;

    public MoreGameAdapter(FragmentManager fm, List<MoreGameFragment> fr) {
        super(fm);
        mGameFragments = fr;
    }

    @Override
    public Fragment getItem(int position) {
        return mGameFragments.get(position);
    }

    @Override
    public int getCount() {
        return mGameFragments.size();
    }
}
