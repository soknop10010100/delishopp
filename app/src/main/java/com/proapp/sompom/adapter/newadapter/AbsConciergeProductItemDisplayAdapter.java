package com.proapp.sompom.adapter.newadapter;

public interface AbsConciergeProductItemDisplayAdapter {

    boolean shouldApplyGridSpacing(int itemPosition);

    default boolean hasLoadMore() {
        return false;
    }

    default boolean isExtraItemSectionTitleViewItem(int currentPosition) {
        return false;
    }
}
