package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.emun.ConciergeUserAddressLabel;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.ListItemConciergeAddressLabelViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Or Vitovongsak on 23/11/21.
 */
public class ConciergeNewAddressLabelAdapter extends RecyclerView.Adapter<BindingViewHolder> {

    private List<ConciergeUserAddressLabel> mDatas;
    private final OnItemClickListener<ConciergeUserAddressLabel> mOnItemClickListener;
    private final List<ListItemConciergeAddressLabelViewModel> mViewModelList = new ArrayList<>();

    public ConciergeNewAddressLabelAdapter(List<ConciergeUserAddressLabel> datas,
                                           OnItemClickListener<ConciergeUserAddressLabel> onItemClickListener) {
        mDatas = datas;
        mOnItemClickListener = onItemClickListener;

        for (ConciergeUserAddressLabel label : mDatas) {
            mViewModelList.add(new ListItemConciergeAddressLabelViewModel(label));
        }
    }

    public static ConciergeNewAddressLabelAdapter getDefaultAdapter(OnItemClickListener<ConciergeUserAddressLabel> onItemClickListener) {
        List<ConciergeUserAddressLabel> labelList = new ArrayList<>();
        labelList.add(ConciergeUserAddressLabel.HOME);
        labelList.add(ConciergeUserAddressLabel.WORK);
        return new ConciergeNewAddressLabelAdapter(labelList, onItemClickListener);
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_address_label).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ListItemConciergeAddressLabelViewModel viewModel = mViewModelList.get(position);
        viewModel.setOnClickListener(() -> {
            updateItemSelection(mDatas.get(position));
        });
        holder.setVariable(BR.viewModel, viewModel);
    }

    public void updateItemSelection(ConciergeUserAddressLabel label) {
        for (int index = 0; index < mDatas.size(); index++) {
            mViewModelList.get(index).setIsSelected(mDatas.get(index) == label);
        }
    }

    public ConciergeUserAddressLabel getSelectedLabel() {
        for (ListItemConciergeAddressLabelViewModel labelViewModel : mViewModelList) {
            if (labelViewModel.getIsSelected().get()) {
                return labelViewModel.getUserAddressLabel();
            }
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }
}
