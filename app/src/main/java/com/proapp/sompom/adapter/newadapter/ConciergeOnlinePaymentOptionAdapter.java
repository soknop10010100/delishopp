package com.proapp.sompom.adapter.newadapter;

import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.concierge.ConciergeOnlinePaymentProvider;
import com.proapp.sompom.model.emun.ConciergeOnlinePaymentOption;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.ItemConciergeOnlinePaymentOption;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Veasna Chhom on 3/1/22.
 */
public class ConciergeOnlinePaymentOptionAdapter extends RecyclerView.Adapter<BindingViewHolder> {

    private static final int ABA_PAY_VIEW_TYPE = 0x010;
    private static final int CREDIT_CARD_VIEW_TYPE = 0x011;
    private static final int KHQR_VIEW_TYPE = 0x012;

    private final List<ConciergeOnlinePaymentProvider> mData;
    private final OnItemClickListener<ConciergeOnlinePaymentProvider> mListener;
    private Map<String, ItemConciergeOnlinePaymentOption> mViewModel = new HashMap<>();
    private int mSelectedPaymentIndex;

    public ConciergeOnlinePaymentOptionAdapter(List<ConciergeOnlinePaymentProvider> data,
                                               int selectedPaymentIndex,
                                               OnItemClickListener<ConciergeOnlinePaymentProvider> listener) {
        mData = data;
        mSelectedPaymentIndex = selectedPaymentIndex;
        mListener = listener;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ABA_PAY_VIEW_TYPE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_aba_pay_payment_option).build();
        } else if (viewType == CREDIT_CARD_VIEW_TYPE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_aba_card_payment_option).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_aba_khqr_payment_option).build();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        final ConciergeOnlinePaymentProvider data = mData.get(position);
        final ItemConciergeOnlinePaymentOption viewModel = new ItemConciergeOnlinePaymentOption(data);
        holder.setVariable(BR.viewModel, viewModel);
        holder.getBinding().getRoot().setOnClickListener(v -> {
            viewModel.updateSelection(true);
            unselectOption(data.getId());
            if (mListener != null) {
                mListener.onClick(data);
            }
        });
        mViewModel.put(data.getId(), viewModel);

        // Select the default item as defined in constructor
        if (position == mSelectedPaymentIndex) {
            holder.getBinding().getRoot().performClick();

            // Prevent duplicate payment selection in case the item gets rebind, since this is really
            // only useful for first init of the adapter
            mSelectedPaymentIndex = -1;
        }
    }

    private void unselectOption(String excludeCheckingId) {
        for (Map.Entry<String, ItemConciergeOnlinePaymentOption> entry : mViewModel.entrySet()) {
            if (!TextUtils.equals(entry.getKey(), excludeCheckingId) && entry.getValue() != null) {
                entry.getValue().updateSelection(false);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        ConciergeOnlinePaymentProvider provider = mData.get(position);
        ConciergeOnlinePaymentOption conciergeOnlinePaymentOption = ConciergeOnlinePaymentOption.fromValue(provider.getProvider());
        if (conciergeOnlinePaymentOption == ConciergeOnlinePaymentOption.ABA_PAY) {
            return ABA_PAY_VIEW_TYPE;
        } else if (conciergeOnlinePaymentOption == ConciergeOnlinePaymentOption.CARD) {
            return CREDIT_CARD_VIEW_TYPE;
        } else {
            return KHQR_VIEW_TYPE;
        }
    }
}
