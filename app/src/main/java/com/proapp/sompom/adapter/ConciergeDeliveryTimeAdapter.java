package com.proapp.sompom.adapter;

import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.model.ShopDeliveryTime;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.ItemShopDeliveryTimeViewModel;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Veasna Chhom on 10/9/21.
 */

public class ConciergeDeliveryTimeAdapter extends RefreshableAdapter<ShopDeliveryTime, BindingViewHolder> {

    private final Map<Integer, ItemShopDeliveryTimeViewModel> mDateViewModelMap = new HashMap<>();
    private ConciergeDeliveryTimeAdapterCallback mListener;

    private int mCurrentSelectedIndex;
    private boolean mIsTimeSelected;
    private boolean mIsViewOnlyMode;

    public ConciergeDeliveryTimeAdapter(List<ShopDeliveryTime> data, boolean isViewOnlyMode, ConciergeDeliveryTimeAdapterCallback listener) {
        super(data);
        mIsViewOnlyMode = isViewOnlyMode;
        setCanLoadMore(false);
        mListener = listener;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.item_shop_delivery_time_layout).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ItemShopDeliveryTimeViewModel viewModel = new ItemShopDeliveryTimeViewModel(holder.getContext(),
                mIsViewOnlyMode,
                mDatas.get(position),
                (time) -> {
                    unselectPreviousTimeSelection(holder.getAdapterPosition());
                    mCurrentSelectedIndex = position;
                    mIsTimeSelected = time.isSelected();
                    if (mListener != null) {
                        mListener.onTimeSelected(time);
                    }
                });
        holder.setVariable(BR.viewModel, viewModel);
        mDateViewModelMap.put(position, viewModel);
    }

    public void selectPreviousTime(LocalTime previousSelectedTime) {
        for (int index = 0; index < mDatas.size(); index++) {
            ShopDeliveryTime time = mDatas.get(index);

            // Convert to localtime with 0 second and nano as to only compare hour and minute
            LocalTime previousTime = previousSelectedTime.withSecond(0).withNano(0);
            LocalTime timeInList = time.getStartTime().withSecond(0).withNano(0);

            if (previousTime.equals(timeInList)) {
                mDatas.get(index).setSelected(true);
                notifyItemChanged(index);

                unselectPreviousTimeSelection(index);
                mCurrentSelectedIndex = index;
                mIsTimeSelected = true;
                if (mListener != null) {
                    mListener.onTimeSelected(mDatas.get(index));
                }
            }
        }
    }

    public void selectFirstTimeForProvinceSlot() {
        if (mDatas != null && !mDatas.isEmpty()) {
            selectPreviousTime(mDatas.get(0).getId());
        }
    }

    public void resetData(List<ShopDeliveryTime> data, boolean isResetSelection) {
        mDatas = data;
        if (isResetSelection) {
            for (ShopDeliveryTime shopDeliveryTime : mDatas) {
                shopDeliveryTime.setSelected(false);
            }
        }
        notifyDataSetChanged();
    }

    public void selectPreviousTime(String timeSlotId) {
        if (!TextUtils.isEmpty(timeSlotId)) {
            for (int index = 0; index < mDatas.size(); index++) {
                ShopDeliveryTime time = mDatas.get(index);
                if (TextUtils.equals(time.getId(), timeSlotId)) {
                    mDatas.get(index).setSelected(true);
                    notifyItemChanged(index);

                    unselectPreviousTimeSelection(index);
                    mCurrentSelectedIndex = index;
                    mIsTimeSelected = true;
                    if (mListener != null) {
                        mListener.onTimeSelected(mDatas.get(index));
                    }

                    break;
                }
            }
        }
    }

    private void unselectPreviousTimeSelection(int selectedPosition) {
        for (Map.Entry<Integer, ItemShopDeliveryTimeViewModel> entry : mDateViewModelMap.entrySet()) {
            if (entry.getKey() != null && entry.getKey() != selectedPosition && entry.getValue() != null) {
                entry.getValue().updateSelectionState(false);
            }
        }
    }

    public int getCurrentSelectedIndex() {
        return mCurrentSelectedIndex;
    }

    public boolean isTimeSelected() {
        return mIsTimeSelected;
    }

    public interface ConciergeDeliveryTimeAdapterCallback {
        void onTimeSelected(ShopDeliveryTime time);
    }
}
