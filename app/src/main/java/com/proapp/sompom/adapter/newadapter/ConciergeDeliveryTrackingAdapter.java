package com.proapp.sompom.adapter.newadapter;

import android.content.Context;
import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;

import com.github.vipulasri.timelineview.TimelineView;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.model.emun.ConciergeOrderStatus;
import com.proapp.sompom.model.result.ConciergeOrderTrackingProgress;
import com.proapp.sompom.viewholder.TrackingItemViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ItemConciergeDeliveryTrackingViewModel;

import java.util.Date;
import java.util.List;

/**
 * Created by Or Vitovongsak on 17/12/21.
 */
public class ConciergeDeliveryTrackingAdapter
        extends RefreshableAdapter<ConciergeOrderTrackingProgress, TrackingItemViewHolder> {

    private Context mContext;
    private ConciergeOrderStatus mCurrentStatus;
    private ConciergeDeliveryTrackingAdapterListener mListener;

    public ConciergeDeliveryTrackingAdapter(Context context,
                                            List<ConciergeOrderTrackingProgress> datas,
                                            ConciergeOrderStatus currentStatus,
                                            ConciergeDeliveryTrackingAdapterListener listener) {
        super(datas);
        mContext = context;
        mCurrentStatus = currentStatus;
        mListener = listener;
        setCanLoadMore(false);
    }

    @Override
    public void onBindData(TrackingItemViewHolder holder, int position) {
        ItemConciergeDeliveryTrackingViewModel viewModel =
                new ItemConciergeDeliveryTrackingViewModel(mContext,
                        mDatas.get(position),
                        mCurrentStatus,
                        holder.getViewType(),
                        () -> {
                            if (mListener != null) {
                                mListener.onCancelOrderClicked();
                            }
                        });
        holder.setVariable(BR.viewModel, viewModel);
    }

    @Override
    public TrackingItemViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new TrackingItemViewHolder
                .Builder(parent, R.layout.list_item_delivery_tracking, viewType)
                .build();
    }

    public void updateConciergeOrderTrackingProgress(ConciergeOrderTrackingProgress status) {
        for (int i = 0; i < mDatas.size(); i++) {
            if (mDatas.get(i).getStatus() == status.getStatus()) {
                //If the date is not nul means that the step was passed.
                if (mDatas.get(i).getDate() == null) {
                    mDatas.set(i, status);
                    mCurrentStatus = status.getStatus();
                    notifyDataSetChanged();
                    if (mListener != null) {
                        mListener.onOrderTrackingStatusUpdated(i, status.getStatus(), true);
                    }
                }
                break;
            }
        }
    }

    public void updateConciergeOrderTrackingProgress(ConciergeOrderStatus status) {
        int foundIndex = -1;
        for (int i = 0; i < mDatas.size(); i++) {
            if (mDatas.get(i).getStatus() == status) {
                foundIndex = i;
                break;
            }
        }

        if (foundIndex >= 0) {
            //Check to mark previous steps as passed.
            if (foundIndex > 0) {
                for (int i = 0; i <= foundIndex - 1; i++) {
                    if (mDatas.get(i).getDate() == null) {
                        mDatas.get(i).setDate(new Date());
                    }
                }
            }

            ConciergeOrderTrackingProgress conciergeOrderTrackingProgress = mDatas.get(foundIndex);
            if (conciergeOrderTrackingProgress.getDate() == null) {
                conciergeOrderTrackingProgress.setDate(new Date());
                mCurrentStatus = status;
                notifyDataSetChanged();
                if (mListener != null) {
                    mListener.onOrderTrackingStatusUpdated(foundIndex, status, false);
                }
            }
        }
    }

    public int findCurrentOrderStatus() {
        for (int i = 0; i < mDatas.size(); i++) {
            if (mDatas.get(i).getStatus() == mCurrentStatus) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    public interface ConciergeDeliveryTrackingAdapterListener {
        void onCancelOrderClicked();

        void onOrderTrackingStatusUpdated(int position, ConciergeOrderStatus orderStatus, boolean needToBroadcastUpdateEvent);
    }
}
