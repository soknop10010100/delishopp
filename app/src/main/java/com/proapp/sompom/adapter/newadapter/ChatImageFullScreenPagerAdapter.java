package com.proapp.sompom.adapter.newadapter;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;

import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.LayoutChatImageFullScreenBinding;
import com.proapp.sompom.databinding.LayoutChatVideoFullScreenBinding;
import com.proapp.sompom.helper.ScreenOrientationHelper;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.viewmodel.newviewmodel.LayoutChatImageFullScreenViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.LayoutChatVideoFullScreenViewModel;
import com.sompom.videomanager.helper.ExoPlayerBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by nuonveyo on 11/28/17.
 */

public class ChatImageFullScreenPagerAdapter extends PagerAdapter {

    private Context mContext;
    private List<Media> mProductMediaList;
    private OnClickListener mOnClickListener;
    private ScreenOrientationHelper mOrientationHelper;
    private Map<String, SimpleExoPlayer> mPlayerMap = new HashMap<>();
    private ChatImageFullScreenPagerAdapterListener mChatImageFullScreenPagerAdapterListener;

    public ChatImageFullScreenPagerAdapter(Context context,
                                           List<Media> productMediaList,
                                           ChatImageFullScreenPagerAdapterListener chatImageFullScreenPagerAdapterListener) {
        mContext = context;
        mProductMediaList = productMediaList;
        mChatImageFullScreenPagerAdapterListener = chatImageFullScreenPagerAdapterListener;
        enableScreenAutoRotation();
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    private boolean isUrlGifFile(Media media) {
        /*
           We will handle the display of gif mp4 with video play on if it is the hosted url. In
           case of local gif, we still keep loading with glide library.
         */
        return media != null &&
                media.getType() == MediaType.GIF &&
                !TextUtils.isEmpty(media.getUrl()) &&
                media.getUrl().startsWith("http");
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        Media media = mProductMediaList.get(position);
        Timber.i("Media: " + media.getUrl());
        if (mProductMediaList.get(position).getType() == MediaType.VIDEO ||
                isUrlGifFile(mProductMediaList.get(position))) {
            LayoutChatVideoFullScreenBinding binding = DataBindingUtil.inflate(inflater,
                    R.layout.layout_chat_video_full_screen,
                    container,
                    false);
            binding.setVariable(BR.viewModel,
                    new LayoutChatVideoFullScreenViewModel(media.getUrl()));
            container.addView(binding.getRoot());
            initExoPlayer(container.getContext(), media, (PlayerView) binding.getRoot());
            return binding.getRoot();
        } else {
            LayoutChatImageFullScreenBinding binding = DataBindingUtil.inflate(inflater,
                    R.layout.layout_chat_image_full_screen,
                    container,
                    false);
            binding.setVariable(BR.viewModel, new LayoutChatImageFullScreenViewModel(media.getUrl(),
                    media.getThumbnail()));
            binding.fullImageView.setOnClickListener(v -> {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick();
                }
            });
            container.addView(binding.getRoot());
            return binding.getRoot();
        }
    }

    private void initExoPlayer(Context context, Media media, PlayerView playerView) {
        //Check to make sure that ExoPlayer will be instantiated only once for each video.
        SimpleExoPlayer simpleExoPlayer = mPlayerMap.get(media.getId());
        if (simpleExoPlayer == null) {
            simpleExoPlayer = ExoPlayerBuilder.build(context,
                    media.getUrl(),
                    isPlaying -> {
                        if (!isUrlGifFile(media)) {
                            Timber.i("onPlayStatus: isPlaying: " + isPlaying + ", Of " + media.getUrl());
                            if (mChatImageFullScreenPagerAdapterListener != null) {
                                mChatImageFullScreenPagerAdapterListener.onVideoPlayStateChang(isPlaying);
                            }
                        }
                    });
            mPlayerMap.put(media.getId(), simpleExoPlayer);
        }
        if (isUrlGifFile(media)) {
            simpleExoPlayer.setPlayWhenReady(true);
            simpleExoPlayer.setVolume(0f);
            simpleExoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
            playerView.setUseController(false);
        } else {
            simpleExoPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);
            simpleExoPlayer.setPlayWhenReady(false);
            playerView.setUseController(true);
        }
        playerView.setPlayer(simpleExoPlayer);
    }

    public void playVideoDirectly(int position) {
        Media media = mProductMediaList.get(position);
        if (media != null) {
            SimpleExoPlayer simpleExoPlayer = mPlayerMap.get(media.getId());
            if (simpleExoPlayer != null) {
                simpleExoPlayer.setPlayWhenReady(true);
            }
        }
    }

    public void resetOtherVideoPlayerPlayPosition(int currentPosition) {
        Media currentMedia = mProductMediaList.get(currentPosition);
        if (currentMedia.getType() == MediaType.VIDEO) {
            for (Map.Entry<String, SimpleExoPlayer> entry : mPlayerMap.entrySet()) {
                SimpleExoPlayer simpleExoPlayer = mPlayerMap.get(entry.getKey());
                if (simpleExoPlayer != null) {
                    simpleExoPlayer.setPlayWhenReady(false);
                    simpleExoPlayer.seekToDefaultPosition();
                }
            }
        }
    }

    public void checkToPlayUrlMP4Gif(int currentPosition) {
        Media currentMedia = mProductMediaList.get(currentPosition);
        if (isUrlGifFile(currentMedia)) {
            SimpleExoPlayer simpleExoPlayer = mPlayerMap.get(currentMedia.getId());
            if (simpleExoPlayer != null) {
                simpleExoPlayer.setPlayWhenReady(true);
            }
        }
    }

    public void releaseVideoPlayer() {
        for (SimpleExoPlayer player : mPlayerMap.values()) {
            player.release();
        }
        mPlayerMap.clear();
    }

    private void enableScreenAutoRotation() {
        if (mOrientationHelper == null) {
            mOrientationHelper = new ScreenOrientationHelper(mContext);
            mOrientationHelper.setCallback(orientation -> {
                if (mContext != null) {
                    if (orientation == ScreenOrientationHelper.ORIENTATION_LANDSCAPE_REVERSE) {
                        ((Activity) mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                    } else if (orientation == ScreenOrientationHelper.ORIENTATION_LANDSCAPE) {
                        ((Activity) mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    } else if (orientation == ScreenOrientationHelper.ORIENTATION_PORTRAIT_REVERSE) {
                        ((Activity) mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
                    } else {
                        ((Activity) mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    }
                }
            });
            mOrientationHelper.enable();
        }
    }

    public void disableAutoOrientation() {
        if (mOrientationHelper != null) {
            mOrientationHelper.disable();
        }
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public int getCount() {
        return mProductMediaList.size();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    public interface ChatImageFullScreenPagerAdapterListener {
        void onVideoPlayStateChang(boolean isPlay);
    }
}
