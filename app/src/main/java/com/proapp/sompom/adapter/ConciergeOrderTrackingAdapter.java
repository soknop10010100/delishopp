package com.proapp.sompom.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ListItemConciergeOrderTrackingBinding;
import com.proapp.sompom.model.ItemConciergeOrderHistoryAdaptive;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.AbsConciergeOrderHistoryViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ItemConciergeOrderTrackingViewModel;

import java.util.List;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 4/11/21.
 */

public class ConciergeOrderTrackingAdapter extends AbsConciergeOrderHistoryAdapter<ItemConciergeOrderHistoryAdaptive, BindingViewHolder> {

    private static final int NORMAL_ITEM_TYPE = 0x98;

    private final Context mContext;
    private final EndItemConciergeOrderTrackingModel mEndItemConciergeOrderTrackingModel = new EndItemConciergeOrderTrackingModel();

    public ConciergeOrderTrackingAdapter(Context context,
                                         List<ItemConciergeOrderHistoryAdaptive> datas,
                                         AbsConciergeOrderHistoryAdapterListener absListener) {
        super(datas, absListener);
        mContext = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < mDatas.size()) {
            return NORMAL_ITEM_TYPE;
        }

        return super.getItemViewType(position);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_order_tracking).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListItemConciergeOrderTrackingBinding) {
            ConciergeOrder order = (ConciergeOrder) mDatas.get(position);
            ItemConciergeOrderTrackingViewModel viewModel =
                    new ItemConciergeOrderTrackingViewModel(holder.getContext(),
                            order,
                            new AbsConciergeOrderHistoryViewModel.AbsConciergeOrderHistoryViewModelListener() {
                                @Override
                                public void onMessageClicked() {
                                    if (mAbsListener != null) {
                                        mAbsListener.onMessageClicked(order);
                                    }
                                }

                                @Override
                                public void onCallClicked() {
                                    if (mAbsListener != null) {
                                        mAbsListener.onCallClicked();
                                    }
                                }

                                @Override
                                public void onOrderCanceled(ConciergeOrder canceledOrder) {
                                    // Whenever an order is canceled, we should remove it from the
                                    // order tracking list.
                                    mDatas.remove(position);
                                    notifyItemRemoved(position);

                                    // If there are only one item left in the adapter, check to remove
                                    // "View all order" button first
//                                    if (mDatas.size() == 1) {
//                                        addOrRemoveEndOfHistoryItem(false);
//                                    }
                                    // If the data is empty after checking to remove "View all order"
                                    // button, notify listener that the adapter is empty so we can
                                    // show the user the empty tracking order screen
                                    if (mDatas.isEmpty()) {
                                        if (mAbsListener != null) {
                                            mAbsListener.onEmptyTrackingOrder();
                                        }
                                    }
                                }
                            });
            holder.setVariable(BR.viewModel, viewModel);
        }
    }

    public void updateOrderStatus(ConciergeOrder updatedOrder) {

        Timber.i("Updated order " + new Gson().toJson(updatedOrder));

        for (int index = 0; index < mDatas.size(); index++) {
            ItemConciergeOrderHistoryAdaptive adaptive = mDatas.get(index);
            if (adaptive instanceof ConciergeOrder) {
                ConciergeOrder orderInList = (ConciergeOrder) adaptive;
                if (TextUtils.equals(orderInList.getId(), updatedOrder.getId())) {
                    orderInList.setOrderStatus(updatedOrder.getOrderStatus());
                    Timber.i("Found order in list: " + new Gson().toJson(orderInList));
                    notifyItemChanged(index);
                    break;
                }
            }
        }
    }

    public void addOrRemoveEndOfHistoryItem(boolean isAdd) {
       /*
        Check if it is already added and normally it is at the last position of the list.
        */
        if (!mDatas.isEmpty()) {
            ItemConciergeOrderHistoryAdaptive lastItem = mDatas.get(mDatas.size() - 1);
            if (isAdd) {
                if (!(lastItem instanceof EndItemConciergeOrderTrackingModel)) {
                    mDatas.add(mEndItemConciergeOrderTrackingModel);
                    notifyItemInserted(mDatas.size() - 1);
                }
            } else {
                if (lastItem instanceof EndItemConciergeOrderTrackingModel) {
                    int lastItemIndex = mDatas.size() - 1;
                    mDatas.remove(lastItem);
                    notifyItemRemoved(lastItemIndex);
                }
            }
        } else {
            // If the data is empty to begin with, show empty history item
            if (isAdd) {
                mDatas.add(mEndItemConciergeOrderTrackingModel);
                notifyItemInserted(mDatas.size() - 1);
            }
        }
    }

    public void setData(List<ItemConciergeOrderHistoryAdaptive> data) {
        mDatas = data;
        notifyDataSetChanged();
    }

    public void setRefreshData(List<ItemConciergeOrderHistoryAdaptive> data, boolean isCanLoadMore) {
        setCanLoadMore(isCanLoadMore);
        mDatas.clear();
        mDatas.addAll(data);
        notifyDataSetChanged();
    }

    // Empty model for rendering show all order button at the end of the tracking list
    public static class EndItemConciergeOrderTrackingModel implements ItemConciergeOrderHistoryAdaptive {
    }
}
