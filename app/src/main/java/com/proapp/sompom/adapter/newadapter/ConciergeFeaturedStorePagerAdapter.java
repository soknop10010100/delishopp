package com.proapp.sompom.adapter.newadapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.DiffUtil;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.model.concierge.ConciergeFeatureStoreSectionResponse;
import com.proapp.sompom.newui.fragment.ConciergeFeaturedStoreGridFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Or Vitovongsak on 18/3/22.
 */
public class ConciergeFeaturedStorePagerAdapter extends FragmentStateAdapter {

    private List<List<ConciergeFeatureStoreSectionResponse.Data>> mChunks;

    public ConciergeFeaturedStorePagerAdapter(@NonNull FragmentManager fragmentManager,
                                              @NonNull Lifecycle lifecycle,
                                              List<ConciergeFeatureStoreSectionResponse.Data> data) {
        super(fragmentManager, lifecycle);
        mChunks = chunkArrayList(data, ConciergeHelper.CONCIERGE_FEATURED_STORE_ITEM);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return ConciergeFeaturedStoreGridFragment.newInstance(mChunks.get(position));
    }

    @Override
    public int getItemCount() {
        return mChunks.size();
    }

    /**
     * Function to creates chunks of data from an arraylist based on chunk size input.
     * <p></p>
     * Example: [a,b,c,d,e,f,g,h] with chunkSize of 3 would return [a,b,c][d,e,f][g,h]
     *
     * @param arrayToChunk The array to chunk
     * @param chunkSize    The size of each chunk
     */
    public List<List<ConciergeFeatureStoreSectionResponse.Data>> chunkArrayList(
            List<ConciergeFeatureStoreSectionResponse.Data> arrayToChunk,
            int chunkSize) {
        List<List<ConciergeFeatureStoreSectionResponse.Data>> chunkList = new ArrayList<>();
        int guide = arrayToChunk.size();
        int index = 0;
        int tale = chunkSize;
        while (tale < arrayToChunk.size()) {
            chunkList.add(arrayToChunk.subList(index, tale));
            guide = guide - chunkSize;
            index = index + chunkSize;
            tale = tale + chunkSize;
        }
        if (guide > 0) {
            chunkList.add(arrayToChunk.subList(index, index + guide));
        }
        return chunkList;
    }

    public void refreshData(List<ConciergeFeatureStoreSectionResponse.Data> newData) {
        if (mChunks.isEmpty()) {
            mChunks = chunkArrayList(newData, ConciergeHelper.CONCIERGE_FEATURED_STORE_ITEM);
            notifyDataSetChanged();
        } else {
            final ConciergeFeaturedStoreDiffCallback diffCallback =
                    new ConciergeFeaturedStoreDiffCallback(mChunks, chunkArrayList(newData, ConciergeHelper.CONCIERGE_FEATURED_STORE_ITEM));
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
            mChunks = chunkArrayList(newData, ConciergeHelper.CONCIERGE_FEATURED_STORE_ITEM);
            diffResult.dispatchUpdatesTo(this);
        }
    }

    public static class ConciergeFeaturedStoreDiffCallback extends DiffUtil.Callback {

        private final List<List<ConciergeFeatureStoreSectionResponse.Data>> mOldList;
        private final List<List<ConciergeFeatureStoreSectionResponse.Data>> mNewList;

        public ConciergeFeaturedStoreDiffCallback(List<List<ConciergeFeatureStoreSectionResponse.Data>> oldBaseChatModelList,
                                                  List<List<ConciergeFeatureStoreSectionResponse.Data>> newBaseChatModelList) {
            mOldList = oldBaseChatModelList;
            mNewList = newBaseChatModelList;
        }

        @Override
        public int getOldListSize() {
            return mOldList.size();
        }

        @Override
        public int getNewListSize() {
            return mNewList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            if (mOldList.size() == mNewList.size()) {
                // Looping through the page containing the featured stores
                for (int pageIndex = 0; pageIndex < mOldList.size(); pageIndex++) {
                    // Looping through all the store item in the current page
                    for (int storeIndex = 0; storeIndex < mOldList.get(pageIndex).size(); storeIndex++) {
                        if (!mOldList.get(pageIndex).get(storeIndex).areItemsTheSame(mNewList.get(pageIndex).get(storeIndex))) {
                            return false;
                        }
                    }
                }
                return true;
            } else {
                return false;
            }
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            if (mOldList.size() == mNewList.size()) {
                // Looping through the page containing the featured stores
                for (int pageIndex = 0; pageIndex < mOldList.size(); pageIndex++) {
                    // Looping through all the store item in the current page
                    for (int storeIndex = 0; storeIndex < mOldList.get(pageIndex).size(); storeIndex++) {
                        if (!mOldList.get(pageIndex).get(storeIndex).areContentsTheSame(mNewList.get(pageIndex).get(storeIndex))) {
                            return false;
                        }
                    }
                }
                return true;
            } else {
                return false;
            }
        }
    }
}
