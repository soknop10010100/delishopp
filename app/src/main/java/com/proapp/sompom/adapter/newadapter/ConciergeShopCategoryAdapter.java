package com.proapp.sompom.adapter.newadapter;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.DelayViewClickListener;
import com.proapp.sompom.model.concierge.ConciergeShopCategory;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.ListItemConciergeShopCategoryViewModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Veasna Chhom on 25/1/22.
 */
public class ConciergeShopCategoryAdapter extends RecyclerView.Adapter<BindingViewHolder> {

    private final List<ConciergeShopCategory> mData;
    private final ConciergeShopCategoryAdapterListener mListener;
    private Map<String, ListItemConciergeShopCategoryViewModel> mCategoryViewModelMap = new HashMap<>();

    public ConciergeShopCategoryAdapter(List<ConciergeShopCategory> data, ConciergeShopCategoryAdapterListener listener) {
        mData = data;
        mListener = listener;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_shop_category).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ConciergeShopCategory category = mData.get(position);
        ListItemConciergeShopCategoryViewModel viewModel = new ListItemConciergeShopCategoryViewModel(holder.getContext(), category);
        holder.setVariable(BR.viewModel, viewModel);
        holder.getBinding().getRoot().setOnClickListener(new DelayViewClickListener() {
            @Override
            public void onDelayClick(@NonNull View widget) {
                updateSelection(category.getId());
                if (mListener != null) {
                    mListener.onCategoryClicked(category);
                }
            }
        });
        mCategoryViewModelMap.put(category.getId(), viewModel);
    }

    private void updateSelection(String selectedId) {
        for (Map.Entry<String, ListItemConciergeShopCategoryViewModel> entry : mCategoryViewModelMap.entrySet()) {
            if (TextUtils.equals(selectedId, entry.getKey())) {
                if (entry.getValue() != null) {
                    entry.getValue().setSelection(true);
                }
            } else {
                if (entry.getValue() != null) {
                    entry.getValue().setSelection(false);
                }
            }
        }
    }

    public void resetAllSelection() {
        for (Map.Entry<String, ListItemConciergeShopCategoryViewModel> entry : mCategoryViewModelMap.entrySet()) {
            if (entry.getValue() != null) {
                entry.getValue().setSelection(false);
            }
        }
    }

    public interface ConciergeShopCategoryAdapterListener {
        void onCategoryClicked(ConciergeShopCategory category);
    }
}
