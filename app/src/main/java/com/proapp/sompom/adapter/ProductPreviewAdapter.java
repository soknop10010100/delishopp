package com.proapp.sompom.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.ViewGroup;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ProductPreviewViewModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by he.rotha on 2/26/16.
 */
public class ProductPreviewAdapter extends RecyclerView.Adapter<BindingViewHolder> {
    private List<Media> mProductUrl;
    private OnProductItemClickListener mListener;

    public ProductPreviewAdapter(List<Media> datas) {
        mProductUrl = datas;
        Collections.sort(mProductUrl);
    }

    public void setProductItemClickListener(OnProductItemClickListener listener) {
        this.mListener = listener;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_image_from).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ProductPreviewViewModel data = new ProductPreviewViewModel(mProductUrl.get(position), mListener);
        holder.setVariable(BR.viewModel, data);
    }

    @Override
    public int getItemCount() {
        return mProductUrl.size();
    }

    public interface OnProductItemClickListener {
        void onItemEditClick();
    }
}
