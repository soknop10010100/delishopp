package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.listener.OnCompleteListener;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemUserStoreDialogViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 10/25/18.
 */

public class UserStoreDialogAdapter extends RefreshableAdapter<Product, BindingViewHolder> {
    private OnCompleteListener<Product> mOnItemClickListener;

    public UserStoreDialogAdapter(List<Product> list, OnCompleteListener<Product> listener) {
        super(list);
        mOnItemClickListener = listener;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_user_store_dialog).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ListItemUserStoreDialogViewModel viewModel = new ListItemUserStoreDialogViewModel(mDatas.get(position), mOnItemClickListener);
        holder.setVariable(BR.viewModel, viewModel);
    }
}
