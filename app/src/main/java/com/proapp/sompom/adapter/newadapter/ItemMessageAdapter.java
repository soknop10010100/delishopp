package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemConversationMessageViewModel;

import java.util.ArrayList;
import java.util.List;

public class ItemMessageAdapter extends RefreshableAdapter<Chat, BindingViewHolder> {

    private Conversation mConversation;
    private ItemMessageAdapterListener mListener;
    private String mSearchKeyWord;

    public ItemMessageAdapter(List<Chat> datas,
                              Conversation conversation,
                              String searchKeyWord,
                              ItemMessageAdapterListener listener) {
        super(datas);
        mListener = listener;
        mSearchKeyWord = searchKeyWord;
        mConversation = conversation;
    }

    public void setSearchKeyWord(String searchKeyWord) {
        mSearchKeyWord = searchKeyWord;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_conversation_message).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ListItemConversationMessageViewModel viewModel = new ListItemConversationMessageViewModel(holder.getContext(),
                mConversation,
                mSearchKeyWord,
                mDatas.get(position));
        holder.setVariable(BR.viewModel, viewModel);
        holder.getBinding().getRoot().setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onItemClicked(mDatas.get(holder.getAdapterPosition()));
            }
        });
    }

    public void resetData() {
        setCanLoadMore(false);
        List<Chat> newList = new ArrayList<>(mDatas);
        newList.clear();
        mDatas = newList;
        notifyDataSetChanged();
    }

    public interface ItemMessageAdapterListener {
        void onItemClicked(Chat chat);
    }
}
