package com.proapp.sompom.adapter.newadapter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.helper.ConversationDiffCallback;
import com.proapp.sompom.listener.ConversationDataAdaptive;
import com.proapp.sompom.listener.OnMessageItemClick;
import com.proapp.sompom.model.ActiveUser;
import com.proapp.sompom.model.emun.SegmentedControlItem;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemAllMessageViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemChatSellerViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemUserGroupTitleViewModel;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class AllMessageAdapter extends RefreshableAdapter<ConversationDataAdaptive, BindingViewHolder> {

    private static final String CONVERSATION_CLICK_EVENT = "CONVERSATION_CLICK_EVENT";
    private static final String SENDER = "SENDER";
    private static final String CONVERSATION_ID = "CONVERSATION_ID";

    private static final int VIEW_TYPE_CONVERSATION = -1;
    private static final int VIEW_TYPE_YOUR_SELLER = -2;
    private static final int VIEW_TYPE_ACTIVE_SELLER = -3;
    private static final int VIEW_TPE_SECTION_HEADER = -4;
    private final OnMessageItemClick mOnMessageItemClick;
    private final SegmentedControlItem mSegmentedControlItem;
    private ActiveUser mActiveSeller;
    private final Context mContext;
    private final Map<String, ListItemAllMessageViewModel> mAllMessageViewModelMap = new HashMap<>();
    private BroadcastReceiver mConversationClickActionReceiver;
    private final String mCurrentUserId;
    private AllMessageAdapterCommunicator mCommunicator;

    public AllMessageAdapter(Context context,
                             List<ConversationDataAdaptive> data,
                             OnMessageItemClick listener,
                             SegmentedControlItem segmentedControlItem,
                             AllMessageAdapterCommunicator communicator) {
        super(data);
        mContext = context;
        mCommunicator = communicator;
        mCurrentUserId = SharedPrefUtils.getUserId(mContext);
        mOnMessageItemClick = listener;
        mSegmentedControlItem = segmentedControlItem;
        initConversationClickActionReceiver();
    }

    private void initConversationClickActionReceiver() {
        mConversationClickActionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int sender = intent.getIntExtra(SENDER, hashCode());
                //Ignore the sending owner class
                if (sender != hashCode()) {
                    Timber.i("onReceive: mConversationClickActionReceiver");
                    handleOnReceiveConversationClickEven(intent.getStringExtra(CONVERSATION_ID));
                }
            }
        };

        mContext.registerReceiver(mConversationClickActionReceiver, new IntentFilter(CONVERSATION_CLICK_EVENT));
    }

    private void broadcastConversationClickEven(String conversationId) {
        Intent intent = new Intent(CONVERSATION_CLICK_EVENT);
        intent.putExtra(SENDER, hashCode());
        intent.putExtra(CONVERSATION_ID, conversationId);
        SendBroadCastHelper.verifyAndSendBroadCast(mContext, intent);
    }

    private void handleOnReceiveConversationClickEven(String conversationId) {
        ListItemAllMessageViewModel viewModel = mAllMessageViewModelMap.get(conversationId);
        if (viewModel != null) {
            viewModel.updateToReadStatusMessage();
        }
    }

    public void unRegisterReceiver() {
        mContext.unregisterReceiver(mConversationClickActionReceiver);
    }

    public int getExistConversationCount() {
        int counter = 0;
        for (ConversationDataAdaptive data : mDatas) {
            if (data instanceof Conversation && !((Conversation) data).isHeaderSection()) {
                counter++;
            }
        }

        return counter;
    }

    public void refreshData(RecyclerView recyclerView, List<ConversationDataAdaptive> chats, boolean isRemoveConversationNotInRemoteList) {
        if (isRemoveConversationNotInRemoteList) {
            markLocalRemoveForNonExistRemoteConversation(chats);
        }

        if (mDatas.isEmpty()) {
            mDatas = chats;
            performNotifyDataSetChanged(recyclerView);
        } else {
            final ConversationDiffCallback diffCallback = new ConversationDiffCallback(mContext,
                    mDatas,
                    chats);
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
            mDatas = chats;
            diffResult.dispatchUpdatesTo(this);
        }
    }

    public void performNotifyDataSetChanged(RecyclerView recyclerView) {
        recyclerView.getRecycledViewPool().clear();
        notifyDataSetChanged();
    }

    private void markLocalRemoveForNonExistRemoteConversation(List<ConversationDataAdaptive> chats) {
        List<String> idList = new ArrayList<>();
        for (ConversationDataAdaptive data : mDatas) {
            boolean isExisted = false;
            for (ConversationDataAdaptive chat : chats) {
                if (TextUtils.equals(data.getId(), chat.getId())) {
                    isExisted = true;
                    break;
                }
            }

            if (!isExisted) {
                idList.add(data.getId());
            }
        }

        if (!idList.isEmpty()) {
            ConversationDb.updateRemovedLocalConversation(mContext, idList);
        }
    }

    public Conversation getConversationById(String conversationId) {
        for (int i = 0; i < mDatas.size(); i++) {
            if (!TextUtils.isEmpty(mDatas.get(i).getId()) &&
                    TextUtils.equals(mDatas.get(i).getId(), conversationId) &&
                    mDatas.get(i) instanceof Conversation) {
                return ((Conversation) mDatas.get(i));
            }
        }

        return null;
    }

    public void updateReadConversation(String conversationId) {
        for (int i = 0; i < mDatas.size(); i++) {
            if (!TextUtils.isEmpty(mDatas.get(i).getId()) &&
                    TextUtils.equals(mDatas.get(i).getId(), conversationId) &&
                    mDatas.get(i) instanceof Conversation) {
                Conversation con = ((Conversation) mDatas.get(i));
                con.setUnreadCount(0);
                ConversationDb.save(mContext, con, "updateReadConversation");
                notifyItemChanged(i);
                break;
            }
        }
    }

    public void checkToRemoveUnreadConversation() {
        for (int i = 0; i < mDatas.size(); i++) {
            if (mDatas.get(i) instanceof Conversation && ((Conversation) mDatas.get(i)).getUnreadCount() > 0) {
                Conversation con = ((Conversation) mDatas.get(i));
                con.setUnreadCount(0);
                ConversationDb.save(mContext, con, "checkToRemoveUnreadConversation");
                notifyItemChanged(i);
            }
        }
    }

    public boolean isConversationRead(String conversationId) {
        for (int i = 0; i < mDatas.size(); i++) {
            if (TextUtils.equals(mDatas.get(i).getId(), conversationId) && mDatas.get(i) instanceof Conversation) {
                return ((Conversation) mDatas.get(i)).isRead();
            }
        }

        return false;
    }

    public int getExistUnreadMessageCounter(String conversationId) {
        for (ConversationDataAdaptive data : mDatas) {
            if (data instanceof Conversation && TextUtils.equals(conversationId, data.getId())) {
                return ((Conversation) data).getUnreadCount();
            }
        }

        return 0;
    }

    public void updateUnreadCount(Conversation conversation) {
        int index = mDatas.indexOf(conversation);
        if (index >= 0) {
            if (mDatas.get(index) instanceof Conversation) {
                Conversation existConversation = (Conversation) mDatas.get(index);
                // Only copying over the unread count and updating the conversation
                existConversation.setUnreadCount(conversation.getUnreadCount());
                Timber.i("updateUnreadCount to " + conversation.getUnreadCount());
                ConversationDb.save(mContext, existConversation, "updateUnreadCount");
                mDatas.set(index, existConversation);
                notifyItemChanged(index);
            }
        }
    }

    public void updateOrCreateConversation(Conversation conversation) {
        if (mSegmentedControlItem == SegmentedControlItem.Message && conversation.isGroup()) {
            //Ignore update group conversation in private message section
            Timber.e("Ignore update group conversation in private message section");
            return;
        }

        int index = mDatas.indexOf(conversation);
        Timber.i("index: " + index);

        if (index >= 0) {
            if (mDatas.get(index) instanceof Conversation) {
                if (conversation.isGroup()) {
                    checkToBackUpJoinGroupCallStatus(conversation, index);
                }

                Conversation existConversation = (Conversation) mDatas.get(index);
                Timber.i("ExistConversation: " + existConversation.getUnreadCount() +
                        ", update conversation: " + conversation.getUnreadCount());
                String senderID = conversation.getSenderID();
                for (User participant : conversation.getParticipants()) {
                    if (participant.getId().equals(senderID)) {
                        participant.setOnline(true);
                    }
                }

                if (conversation.isFromGroupNotification()) {
                    /*
                        Get the update from group notification, so we have update all group properties.
                     */
                    existConversation.setGroupName(conversation.getGroupName());
                    existConversation.setGroupImageUrl(conversation.getGroupImageUrl());
                    existConversation.setParticipants(conversation.getParticipants());
                    existConversation.setLastMessage(conversation.getLastMessage());
                    existConversation.setContent(conversation.getContent());
                    /*
                    The conversation will be considered as read if the last message is from current user.
                     */
                    boolean isRead = false;
                    if (conversation.getLastMessage() != null &&
                            TextUtils.equals(mCurrentUserId, conversation.getLastMessage().getSenderId())) {
                        isRead = true;
                    }
                    notifyItemChanged(index);
                    return;
                }

                if (existConversation.getLastMessage() != null && conversation.getLastMessage() != null) {
                    //Ignore the update if the message has no last message/content for the message that
                    //already existed in list.
                    if (TextUtils.equals(existConversation.getContent(mContext, false),
                            conversation.getContent(mContext, false)) &&
                            existConversation.getDate() != null && conversation.getDate() != null &
                            existConversation.getDate().equals(conversation.getDate())) {
                        checkToMaintainConversationParticipantOnlineState(conversation, existConversation);
                        mDatas.set(index, conversation);
                        notifyItemChanged(index);
                    } else {
                        checkToMaintainConversationParticipantOnlineState(conversation, existConversation);
                        int insertIndex = findProperIndexToInsertUpdateConversation(conversation);
                        Timber.i("Move conversation to position: " + insertIndex);
                        /*
                            If the insert index available will insert into that place. Otherwise, will
                            keep updating at the existing index.
                         */
                        if (insertIndex >= 0 && insertIndex < mDatas.size()) {
                            mDatas.remove(index);
                            mDatas.add(insertIndex, conversation);
                            notifyItemMoved(index, insertIndex);
                            notifyItemChanged(insertIndex);
                            checkToMoveConversationBelowMoreConversationTitle();
                        } else {
                            mDatas.set(index, conversation);
                            notifyItemChanged(index);
                        }
                    }
                } else if (conversation.getLastMessage() != null) {
                    //Update the content from the update conversation directly.
                    checkToMaintainConversationParticipantOnlineState(conversation, existConversation);
                    mDatas.set(index, conversation);
                    notifyItemChanged(index);
                } else {
                    //Check to update read conversation
                    if (conversation.isRead()) {
                        Timber.i("Update read conversation status.");
                        notifyItemChanged(index);
                    }
                }
            }
        } else {
            /*
                If there is last message for new conversation, then will display it on the top list.
                Otherwise, will display it at the bottom of list.
             */
            if (conversation.getLastMessage() == null) {
                mDatas.add(conversation);
                notifyItemInserted(mDatas.size() - 1);
            } else {
                int insertIndex = findProperIndexToInsertUpdateConversation(conversation);
                Timber.i("Insert conversation to position: " + insertIndex);
                if (insertIndex >= 0) {
                    mDatas.add(insertIndex, conversation);
                    notifyItemInserted(insertIndex);
                }
            }
        }
    }

    private void checkToMoveConversationBelowMoreConversationTitle() {
        if (mSegmentedControlItem == SegmentedControlItem.All) {
            ConversationDataAdaptive lastConversation = mDatas.get(mDatas.size() - 1);
            if (lastConversation instanceof Conversation && ((Conversation) lastConversation).isHeaderSection()) {
                /*
                    Use case:
                    When a conversation has moved up "More conversation" section tile and it turns out that,
                    there is no any conversation below "More conversation" section, we will move one conversation
                    to stay under "More conversation" section.
                 */
                if (mDatas.size() > 3) {
                    int beforeMoreConversationTitleIndex = mDatas.size() - 2;
                    ConversationDataAdaptive conversationBeforeMoreConversationTitle = mDatas.get(beforeMoreConversationTitleIndex);
                    mDatas.remove(conversationBeforeMoreConversationTitle);
                    mDatas.add(conversationBeforeMoreConversationTitle);
                    notifyItemMoved(beforeMoreConversationTitleIndex, mDatas.size() - 1);
                }
            } else {
                /*
                    If the recent display conversation size appears to exceed the maximum of display recent
                    conversation size, we have to move the exceed conversation to bottom "More conversation" section.
                 */
                int moreConversationIndex = -1;
                for (int i = 0; i < mDatas.size(); i++) {
                    if (mDatas.get(i) instanceof Conversation && ((Conversation) mDatas.get(i)).isHeaderSection()) {
                        moreConversationIndex = i;
                        break;
                    }
                }

//                Timber.i("Recent size: " + mCommunicator.getDefaultRecentConversationDisplaySize() + ", moreConversationIndex: " + moreConversationIndex);
                if (moreConversationIndex == (mCommunicator.getDefaultRecentConversationDisplaySize() + 1)) {
                    ConversationDataAdaptive conversationBeforeMoreConversationTitle = mDatas.get(moreConversationIndex - 1);
                    mDatas.remove(conversationBeforeMoreConversationTitle);
                    mDatas.add(mCommunicator.getDefaultRecentConversationDisplaySize() + 1, conversationBeforeMoreConversationTitle);
                    notifyItemMoved(moreConversationIndex - 1, mCommunicator.getDefaultRecentConversationDisplaySize() + 1);
                }
            }
        }
    }

    private int findProperIndexToInsertUpdateConversation(Conversation updateConversation) {
        if (updateConversation.getDate() != null) {
            int insertIndex = 0;
            for (int i = 0; i < mDatas.size(); i++) {
                ConversationDataAdaptive data = mDatas.get(i);
                if (data instanceof Conversation && !TextUtils.equals(data.getId(), updateConversation.getId())) {
                    if (((Conversation) data).getDate() != null && ((Conversation) data).getDate().compareTo(updateConversation.getDate()) > 0) {
                        insertIndex = i + 1;
                    }
                }
            }

            return insertIndex;
        }

        return -1;
    }

    private void checkToMaintainConversationParticipantOnlineState(Conversation updateConversation,
                                                                   Conversation existConversation) {
        checkToMaintainParticipantOnlineState(updateConversation.getParticipants(),
                existConversation.getParticipants());
        checkToMaintainRecipientOnlineState(updateConversation.getOneToOneRecipient(mContext),
                existConversation.getOneToOneRecipient(mContext));
    }

    private void checkToMaintainParticipantOnlineState(List<User> updateParticipants, List<User> currentParticipants) {
        for (User updateParticipant : updateParticipants) {
            for (User currentParticipant : currentParticipants) {
                if (TextUtils.equals(updateParticipant.getId(), currentParticipant.getId())) {
                    if (updateParticipant.isOnline() == null) {
                        updateParticipant.setOnline(currentParticipant.isOnline());
                    }
                    if (updateParticipant.getLastActivity() == null) {
                        updateParticipant.setLastActivity(currentParticipant.getLastActivity());
                    }
                    break;
                }
            }
        }
    }

    private void checkToMaintainRecipientOnlineState(User updateRecipient, User currentRecipient) {
        if (updateRecipient != null && currentRecipient != null) {
            if (updateRecipient.isOnline() == null) {
                updateRecipient.setOnline(currentRecipient.isOnline());
            }
            if (updateRecipient.getLastActivity() == null) {
                updateRecipient.setLastActivity(currentRecipient.getLastActivity());
            }
        }
    }

    private void checkToBackUpJoinGroupCallStatus(Conversation updateConversation, int existedIndex) {
        ConversationDataAdaptive existConversation = mDatas.get(existedIndex);
        if (existConversation instanceof Conversation) {
            updateConversation.setChannelOpen(((Conversation) existConversation).isChannelOpen());
            updateConversation.setVideo(((Conversation) existConversation).isVideo());
        }
    }

    public void removeConversationById(String conversationId) {
        int index = -1;
        for (int i = 0; i < mDatas.size(); i++) {
            /**
             * Since there is conversation header which will be use the same Conversation model,
             * so it may not have valid id. So ignore it in the checking condition.
             */
            if (mDatas.get(i) instanceof Conversation &&
                    !TextUtils.isEmpty(mDatas.get(i).getId()) &&
                    mDatas.get(i).getId().matches(conversationId)) {
                index = i;
                break;
            }
        }

        if (index >= 0) {
            mDatas.remove(index);
            notifyItemRemoved(index);
        }

        mAllMessageViewModelMap.remove(conversationId);
    }

    private int getUpdateSellingConversationIndex(Conversation conversation) {
        Product newProduct = conversation.getProduct();
        if (newProduct == null) {
            newProduct = conversation.getLastMessage().getProduct();
        }

        if (newProduct != null && !mDatas.isEmpty()) {
            String newProductId = newProduct.getId();

            for (int i = 0; i < mDatas.size(); i++) {
                Conversation conversation1 = (Conversation) mDatas.get(i);
                Product product = conversation1.getProduct();
                if (product == null) {
                    product = conversation1.getLastMessage().getProduct();
                }

                if (TextUtils.equals(product.getId(), newProductId)) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public void addLoadMoreData(List<ConversationDataAdaptive> moreItems) {
        if (moreItems != null && !moreItems.isEmpty()) {
            checkToMoveInvalidConversationToLast(moreItems);
            int previousItemCount = getItemCount();
            mDatas.addAll(moreItems);
            notifyItemRangeInserted(previousItemCount + 1, moreItems.size());
        }
    }

    private void checkToMoveInvalidConversationToLast(List<ConversationDataAdaptive> moreConversation) {
        //Will move all conversation that have no last message to the last list.
        //Conversation that acts as header is ignored in this condition.
        List<Integer> removeIndex = new ArrayList<>();
        for (int i = 0; i < mDatas.size(); i++) {
            if (mDatas.get(i) instanceof Conversation &&
                    !((Conversation) mDatas.get(i)).isHeaderSection() &&
                    ((Conversation) mDatas.get(i)).getDate() == null &&
                    ((Conversation) mDatas.get(i)).getLastMessage() == null) {
                removeIndex.add(i);
            }
        }

        for (Integer index : removeIndex) {
            moreConversation.add(mDatas.get(index));
            mDatas.remove(index.intValue());
            notifyItemRemoved(index);
        }
    }

    public void updateJoinGroupCallStatus(String conversationId, boolean isJoinAvailable, boolean isVideo) {
        for (ConversationDataAdaptive data : mDatas) {
            if (TextUtils.equals(conversationId, data.getId()) && data instanceof Conversation) {
                ((Conversation) data).setChannelOpen(isJoinAvailable);
                ((Conversation) data).setVideo(isVideo);
                break;
            }
        }
        ListItemAllMessageViewModel viewModel = mAllMessageViewModelMap.get(conversationId);
        if (viewModel != null) {
            viewModel.updateJoinGroupCallStatus(isJoinAvailable, isVideo);
        }
    }

    public void removeConversation(String conversationId) {
        Conversation conversation = new Conversation();
        conversation.setId(conversationId);

        int index = mDatas.indexOf(conversation);
        if (index >= 0) {
            mDatas.remove(index);
            notifyItemRemoved(index);
        }

        mAllMessageViewModelMap.remove(conversationId);
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= 0 && position < mDatas.size()) {
            if (mDatas.get(position) instanceof Conversation) {
                if (((Conversation) mDatas.get(position)).isHeaderSection()) {
                    return VIEW_TPE_SECTION_HEADER;
                }
                return VIEW_TYPE_CONVERSATION;
            } else if (mDatas.get(position) instanceof ActiveUser) {
                if (((ActiveUser) mDatas.get(position)).getSellerItemType() == ActiveUser.SellerItemType.YOUR_SELLER) {
                    return VIEW_TYPE_YOUR_SELLER;
                } else {
                    return VIEW_TYPE_ACTIVE_SELLER;
                }
            }
        }
        return super.getItemViewType(position);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_CONVERSATION) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_all_messag).build();
        } else if (viewType == VIEW_TPE_SECTION_HEADER) {
            return new BindingViewHolder.Builder(parent, R.layout.list_converstaion_section).build();
        } else {
            BindingViewHolder holder = new BindingViewHolder.Builder(parent, R.layout.list_item_chat_seller).build();
            RecyclerView recyclerView = holder.itemView.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(parent.getContext(), LinearLayoutManager.HORIZONTAL, false));

            if (viewType == VIEW_TYPE_YOUR_SELLER) {
                recyclerView.setAdapter(new AllMessageYourSellerAdapter(new ActiveUser(), mOnMessageItemClick));
            } else {
                recyclerView.setAdapter(new AllMessageYourSellerAdapter(mActiveSeller, mOnMessageItemClick));
            }
            return holder;
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        final Context context = holder.getBinding().getRoot().getContext();
        if (mDatas.get(position) instanceof Conversation) {
            if (((Conversation) mDatas.get(position)).isHeaderSection()) {
                ListItemUserGroupTitleViewModel viewModel = new ListItemUserGroupTitleViewModel(holder
                        .getContext().getResources().getDimensionPixelSize(R.dimen.medium_padding),
                        ((Conversation) mDatas.get(position)).getHeaderSectionTitle());
                holder.getBinding().setVariable(BR.viewModel, viewModel);
            } else {
                final Conversation conversation = (Conversation) mDatas.get(position);
                ListItemAllMessageViewModel viewModel = new ListItemAllMessageViewModel(context,
                        conversation,
                        mOnMessageItemClick,
                        mSegmentedControlItem);
                mAllMessageViewModelMap.remove(conversation.getId());
                mAllMessageViewModelMap.put(conversation.getId(), viewModel);
                viewModel.setListener(conversationId -> {
                    broadcastConversationClickEven(conversation.getId());
                });
                holder.setVariable(BR.viewModel, viewModel);
            }
        } else if (mDatas.get(position) instanceof ActiveUser) {
            final ActiveUser activeUser = (ActiveUser) mDatas.get(position);

            ListItemChatSellerViewModel viewModel = new ListItemChatSellerViewModel(activeUser);
            holder.setVariable(BR.viewModel, viewModel);
        }
    }

    public interface AllMessageAdapterCommunicator {
        int getDefaultRecentConversationDisplaySize();
    }
}
