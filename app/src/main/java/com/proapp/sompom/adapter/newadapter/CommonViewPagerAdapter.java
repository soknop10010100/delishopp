package com.proapp.sompom.adapter.newadapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.proapp.sompom.newui.fragment.AbsBaseFragment;

import java.util.List;

/**
 * Created by Chhom.Veasna on 05/28/20.
 */
public class CommonViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<AbsBaseFragment> mFragmentList;

    public CommonViewPagerAdapter(@NonNull FragmentManager fm, List<AbsBaseFragment> fragmentList) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mFragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
