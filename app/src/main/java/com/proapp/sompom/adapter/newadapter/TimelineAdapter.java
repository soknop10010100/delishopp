package com.proapp.sompom.adapter.newadapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.databinding.LayoutAdmobBinding;
import com.proapp.sompom.databinding.ListItemProductMediaBinding;
import com.proapp.sompom.databinding.ListItemTimelineBinding;
import com.proapp.sompom.databinding.ListItemTimelineFileBinding;
import com.proapp.sompom.databinding.ListItemTimelineMediaBinding;
import com.proapp.sompom.databinding.ListItemTimelinePlacePreviewBinding;
import com.proapp.sompom.databinding.ListItemTimelineSharedLiveVideoBinding;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.listener.OnTimelineItemButtonClickDelegateListener;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.CommonWallAdapter;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.ShareAds;
import com.proapp.sompom.model.SharedProduct;
import com.proapp.sompom.model.SharedTimeline;
import com.proapp.sompom.model.emun.ViewType;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewholder.SupportPlacePreviewViewHolder;
import com.proapp.sompom.viewholder.TimelineViewHolder;
import com.proapp.sompom.viewmodel.BannerViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemDefaultTimelineViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemMainTimelineViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemTimelineFileViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemTimelineLiveViewModel;
import com.proapp.sompom.widget.lifestream.CollageView;

import java.util.List;

import timber.log.Timber;

/**
 * Created by He Rotha on 7/10/18.
 */
public class TimelineAdapter extends RefreshableAdapter<Adaptive, BindingViewHolder> implements CommonWallAdapter {

    private static final int TIMELINE_FILE = 0x901;
    private static final int TIMELINE_WITH_PLACE_PREVIEW = 0x902;

    private final OnTimelineItemButtonClickDelegateListener mOnItemClickListener;

    private final WallStreetDataManager mWallStreetDataManager;
    private final ApiService mApiService;
    private final AbsBaseActivity mContext;
    //    private List<AdView> mAdViews;
    private boolean mCheckLikeComment;

    public TimelineAdapter(AbsBaseActivity context,
                           ApiService apiService,
                           List<Adaptive> datas,
                           OnTimelineItemButtonClickListener listener,
                           boolean checkLikeComment) {
        super(datas);
        mContext = context;
        mApiService = apiService;
        mCheckLikeComment = checkLikeComment;
        mOnItemClickListener = new OnTimelineItemButtonClickDelegateListener(listener) {
            @Override
            public void onFollowButtonClick(String id, boolean isFollowing) {
                super.onFollowButtonClick(id, isFollowing);
                for (int i = 0; i < mDatas.size(); i++) {
                    Adaptive data = mDatas.get(i);
                    if (data instanceof LifeStream) {
                        check(i, ((LifeStream) data).getUser(), id, isFollowing);
                    } else if (data instanceof SharedTimeline) {
                        check(i, ((SharedTimeline) data).getUser(), id, isFollowing);
                        check(i, ((SharedTimeline) data).getLifeStream().getUser(), id, isFollowing);
                    } else if (data instanceof Product) {
                        check(i, ((Product) data).getUser(), id, isFollowing);
                    } else if (data instanceof SharedProduct) {
                        check(i, ((SharedProduct) data).getUser(), id, isFollowing);
                        check(i, ((SharedProduct) data).getProduct().getUser(), id, isFollowing);
                    } else if (data instanceof ShareAds) {
                        check(i, ((ShareAds) data).getUser(), id, isFollowing);
                    }
                }
            }

            private void check(int position, User user, String id, boolean isFollow) {
                if (TextUtils.equals(user.getId(), id) && user.isFollow() != isFollow) {
                    user.setFollow(isFollow);
                    notifyItemChanged(position);
                }
            }

            @Override
            public void onCommentClick(Adaptive adaptive, boolean autoDisplayKeyboard) {
                if (checkLikeComment) {
                    super.onCommentClick(adaptive, autoDisplayKeyboard);
                }
            }
        };
        mWallStreetDataManager = new WallStreetDataManager(mContext, mApiService, null, null);

    }


    @Override
    public void onViewRecycled(@NonNull BindingViewHolder holder) {
        super.onViewRecycled(holder);
        //release auto play video
        if (holder instanceof TimelineViewHolder) {
            ((TimelineViewHolder) holder).stopOrRelease();
        }
    }

//    public void setAdViews(List<AdView> adViews) {
//        mAdViews = adViews;
//    }

    public void remove(Adaptive adaptive) {
        for (int i = 0; i < mDatas.size(); i++) {
            if (TextUtils.equals(mDatas.get(i).getId(), adaptive.getId())) {
                mDatas.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }

    public void update(Adaptive adaptive) {
        Timber.i("start update %s", adaptive.getId());
        for (int i = 0; i < mDatas.size(); i++) {
            if (TextUtils.equals(mDatas.get(i).getId(), adaptive.getId())) {
                updateItem(adaptive, i);
                break;
            }
        }

        Timber.i("stop update ");
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
//      WallStreet Style 1
        if (viewType >= ViewType.Product_FreeStyle_1.getViewType() && viewType <= ViewType.Product_SquareAll_5.getViewType()) {
//        WallStreet Style 2
//        if (viewType >= ViewType.ProductThreeOneItem.getViewType() && viewType <= ViewType.ProductThreeThreeItem.getViewType()) {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final ViewDataBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.list_item_product_media, parent, false);
            TimelineViewHolder holder = new TimelineViewHolder(dataBinding);
            ListItemProductMediaBinding binding = (ListItemProductMediaBinding) holder.getBinding();
            CollageView collageView = binding.mediaLayouts;
            ViewType v = ViewType.fromValue(viewType);
            collageView.setOrientation(v.getOrientation());
            collageView.setNumberOfItem(v.getNumberOfItem());
            collageView.setFormat(v.getFormat());
            collageView.startGenerateView();
            return holder;
        } else if (viewType == ViewType.LiveVideoTimeline.getTimelineViewType()) {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final ViewDataBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.list_item_timeline_shared_live_video, parent, false);
            return new TimelineViewHolder(dataBinding);
        } else if (viewType == ViewType.Ads.getTimelineViewType()) {
            return new BindingViewHolder.Builder(parent, R.layout.layout_admob).build();
        } else if (viewType == TIMELINE_WITH_PLACE_PREVIEW) {
            return new SupportPlacePreviewViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                    R.layout.list_item_timeline_place_preview, parent, false),
                    mContext);
        } else if (viewType == ViewType.Timeline.getTimelineViewType()) {
            ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                    R.layout.list_item_timeline, parent, false);
            return new TimelineViewHolder(viewDataBinding);
        } else if (viewType == TIMELINE_FILE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_timeline_file).build();
        } else {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final ViewDataBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.list_item_timeline_media, parent, false);
            TimelineViewHolder holder = new TimelineViewHolder(dataBinding);
            ListItemTimelineMediaBinding binding = (ListItemTimelineMediaBinding) holder.getBinding();
            CollageView collageView = binding.mediaLayouts;
            ViewType v = ViewType.fromValue(viewType);
            collageView.setOrientation(v.getOrientation());
            collageView.setNumberOfItem(v.getNumberOfItem());
            collageView.setFormat(v.getFormat());
            collageView.startGenerateView();
            return holder;
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListItemTimelineBinding) {
            ListItemDefaultTimelineViewModel viewModel = new ListItemDefaultTimelineViewModel(mContext,
                    holder,
                    mWallStreetDataManager,
                    mDatas.get(position),
                    holder.getAdapterPosition(),
                    mOnItemClickListener,
                    mCheckLikeComment);
            holder.setVariable(BR.viewModel, viewModel);
            if (viewModel.isShouldRenderPlacePreview() && holder instanceof SupportPlacePreviewViewHolder) {
                ((SupportPlacePreviewViewHolder) holder).bindPlace((LifeStream) mDatas.get(position));
            }
        } else if (holder.getBinding() instanceof ListItemTimelinePlacePreviewBinding) {
            ListItemDefaultTimelineViewModel viewModel = new ListItemDefaultTimelineViewModel(mContext,
                    holder,
                    mWallStreetDataManager,
                    mDatas.get(position),
                    holder.getAdapterPosition(),
                    mOnItemClickListener,
                    mCheckLikeComment);
            holder.setVariable(BR.viewModel, viewModel);
            ((SupportPlacePreviewViewHolder) holder).bindPlace((LifeStream) mDatas.get(position));
        } else if (holder.getBinding() instanceof ListItemProductMediaBinding
                || holder.getBinding() instanceof ListItemTimelineMediaBinding) {
            ListItemMainTimelineViewModel viewModel = new ListItemMainTimelineViewModel(mContext, holder,
                    mWallStreetDataManager,
                    mDatas.get(position),
                    holder.getAdapterPosition(),
                    mOnItemClickListener,
                    mCheckLikeComment);
            holder.setVariable(BR.viewModel, viewModel);
        } else if (holder.getBinding() instanceof ListItemTimelineSharedLiveVideoBinding) {
            ListItemTimelineLiveViewModel viewModel = new ListItemTimelineLiveViewModel(mContext, holder,
                    mDatas.get(position),
                    mWallStreetDataManager,
                    holder.getAdapterPosition(),
                    mOnItemClickListener,
                    mCheckLikeComment);
            holder.setVariable(BR.viewModel, viewModel);
        } else if (holder.getBinding() instanceof LayoutAdmobBinding) {
//            final BannerViewModel bannerViewModel = new BannerViewModel(position, mAdViews);
            final BannerViewModel bannerViewModel = new BannerViewModel(position);
            ((LayoutAdmobBinding) holder.getBinding()).setViewModel(bannerViewModel);
        } else if (holder.getBinding() instanceof ListItemTimelineFileBinding) {
            ListItemTimelineFileViewModel viewModel = new ListItemTimelineFileViewModel(mContext,
                    holder,
                    mDatas.get(position),
                    mWallStreetDataManager,
                    holder.getAdapterPosition(),
                    mOnItemClickListener,
                    mCheckLikeComment);
            holder.setVariable(BR.viewModel, viewModel);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= 0 && position < mDatas.size()) {
            ViewType timelineViewType = mDatas.get(position).getTimelineViewType();
            if (timelineViewType == ViewType.Timeline) {
                if (isFilePostTimeline(position)) {
                    return TIMELINE_FILE;
                } else if (isTimelinePlaceViewType(position))
                    return TIMELINE_WITH_PLACE_PREVIEW;
                else {
                    return timelineViewType.getTimelineViewType();
                }
            } else {
                return timelineViewType.getTimelineViewType();
            }
        }

        return super.getItemViewType(position);
    }

    private boolean isTimelinePlaceViewType(int position) {
        return !WallStreetHelper.shouldRenderLinkPreview(mDatas.get(position)) &&
                WallStreetHelper.shouldRenderPlacePreview(mDatas.get(position));
    }

    private boolean isFilePostTimeline(int position) {
        if (mDatas.get(position) instanceof LifeStream) {
            return ((LifeStream) mDatas.get(position)).isFilePostTimelineType();
        }

        return false;
    }

    public ApiService getApiService() {
        return mApiService;
    }

    AbsBaseActivity getAbsBaseActivity() {
        return mContext;
    }

    public void updateItem(Adaptive adaptive, int position) {
        //Need to backup the current post owner with the update.
        if (mDatas.get(position) instanceof LifeStream && adaptive instanceof LifeStream) {
            ((LifeStream) adaptive).setStoreUser(((LifeStream) mDatas.get(position)).getUser());
        }
        mDatas.set(position, adaptive);
        notifyItemChanged(position);
    }

    public void removeData(int position) {
        mDatas.remove(position);
        notifyItemRemoved(position);
    }

    public void removeItem(Adaptive adaptive) {
        int itemPositionById = findItemPositionById(adaptive.getId());
        if (itemPositionById >= 0) {
            removeData(itemPositionById);
        }
    }

    private int findItemPositionById(String id) {
        for (int i = 0; i < mDatas.size(); i++) {
            if (TextUtils.equals(mDatas.get(i).getId(), id)) {
                return i;
            }
        }

        return -1;
    }
}
