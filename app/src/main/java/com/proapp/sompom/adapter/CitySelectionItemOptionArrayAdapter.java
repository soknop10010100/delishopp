package com.proapp.sompom.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.proapp.sompom.R;
import com.proapp.sompom.model.emun.CitySelectionType;
import com.proapp.sompom.utils.AttributeConverter;

public class CitySelectionItemOptionArrayAdapter extends ArrayAdapter<CitySelectionType> {

    public CitySelectionItemOptionArrayAdapter(@NonNull Context context) {
        super(context, R.layout.concierge_shop_sort_spinner_item, CitySelectionType.values());
        setDropDownViewResource(R.layout.concierge_shop_sort_spinner_item);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        if (view instanceof AppCompatTextView) {
            CitySelectionType item = getItem(position);
            ((AppCompatTextView) view).setText(item.getDisplayValue(view.getContext()));
            updateTextColor((AppCompatTextView) view, item);
        }

        return view;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        if (view instanceof AppCompatTextView) {
            CitySelectionType item = getItem(position);
            ((AppCompatTextView) view).setText(item.getDisplayValue(view.getContext()));
            updateTextColor((AppCompatTextView) view, item);
        }

        return view;
    }

    private void updateTextColor(AppCompatTextView view, CitySelectionType type) {
        if (view != null) {
            if (type == CitySelectionType.SELECT_HINT) {
                view.setTextColor(AttributeConverter.convertAttrToColor(view.getContext(), R.attr.shop_new_address_input_hint));
            } else {
                view.setTextColor(AttributeConverter.convertAttrToColor(view.getContext(), R.attr.shop_item_sort_popup_title));
            }
        }
    }

    @Override
    public boolean isEnabled(int position) {
        return getItem(position) != CitySelectionType.SELECT_HINT;
    }
}
