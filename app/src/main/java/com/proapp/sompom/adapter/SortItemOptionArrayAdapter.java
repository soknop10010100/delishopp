package com.proapp.sompom.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.model.emun.ConciergeSortItemOption;
import com.proapp.sompom.utils.AttributeConverter;

public class SortItemOptionArrayAdapter extends ArrayAdapter<ConciergeSortItemOption> {

    public SortItemOptionArrayAdapter(@NonNull Context context) {
        super(context, R.layout.concierge_shop_sort_spinner_item, ConciergeSortItemOption.getAllOptions());
        setDropDownViewResource(R.layout.concierge_shop_sort_spinner_item);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        if (view instanceof AppCompatTextView) {
            ConciergeSortItemOption item = getItem(position);
            ((AppCompatTextView) view).setText(getItem(position).getDisplayLabel(view.getContext()));
            if (item == ConciergeHelper.getSortItemOption()) {
                ((AppCompatTextView) view).setTextColor(AttributeConverter.convertAttrToColor(view.getContext(), R.attr.common_primary_color));
            } else {
                ((AppCompatTextView) view).setTextColor(AttributeConverter.convertAttrToColor(view.getContext(), R.attr.shop_category_view_item_by_icon));
            }
        }

        return view;
    }
}
