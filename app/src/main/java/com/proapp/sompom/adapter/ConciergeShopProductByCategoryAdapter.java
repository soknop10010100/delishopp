package com.proapp.sompom.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.AbsConciergeProductItemDisplayAdapter;
import com.proapp.sompom.databinding.ListConciergeShopCategorySelectedItemBinding;
import com.proapp.sompom.databinding.ListConciergeShopInfoItemBinding;
import com.proapp.sompom.databinding.ListConciergeShopItemBinding;
import com.proapp.sompom.databinding.ListConciergeShopSubCategoryItemBinding;
import com.proapp.sompom.databinding.ListItemConciergeGridProductBinding;
import com.proapp.sompom.databinding.ListItemConciergeSmallGridProductBinding;
import com.proapp.sompom.decorataor.ConciergeGridSpacingItemDecoration;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;
import com.proapp.sompom.model.concierge.ConciergeExtraItemTitle;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeShop;
import com.proapp.sompom.model.concierge.ConciergeShopCategory;
import com.proapp.sompom.model.concierge.ConciergeSubCategory;
import com.proapp.sompom.model.concierge.ConciergeSubCategoryLoadMoreDataModel;
import com.proapp.sompom.model.concierge.ConciergeSupplier;
import com.proapp.sompom.model.concierge.ConciergeSupportItemAndSubCategoryAdaptive;
import com.proapp.sompom.model.emun.ConciergeSortItemOption;
import com.proapp.sompom.model.emun.ConciergeViewItemByType;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.newui.dialog.ConciergeFilterProductCriteriaBottomSheetDialog;
import com.proapp.sompom.newui.fragment.AbsSupportShopCheckOutFragment;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.ConciergeSupportAddItemToCardViewModel;
import com.proapp.sompom.viewmodel.ItemConciergeShopInfoViewModel;
import com.proapp.sompom.viewmodel.ListConciergeShopItemViewModel;
import com.proapp.sompom.viewmodel.ListItemConciergeShopCategorySelectedViewModel;
import com.proapp.sompom.viewmodel.ListItemConciergeShopSubCategoryViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemConciergeProductViewModel;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * Created by Veasna Chhom on 3/24/22.
 */

public class ConciergeShopProductByCategoryAdapter extends RefreshableAdapter<ConciergeShopDetailDisplayAdaptive, BindingViewHolder> implements AbsConciergeProductItemDisplayAdapter {

    private static final int CATEGORY_VIEW_TYPE = 0x091;
    private static final int NORMAL_ITEM_VIEW_TYPE = 0x092;
    private static final int SUB_CATEGORY_VIEW_TYPE = 0x093;
    private static final int GRID_ITEM_WITH_TWO_COLUMNS_VIEW_TYPE = 0x094;
    private static final int GRID_ITEM_WITH_THREE_COLUMNS_VIEW_TYPE = 0x095;
    private static final int EXTRA_ITEM_SECTION_TITLE_VIEW_TYPE = 0x096;

    private final Context mContext;
    private final ShopProductAdapterCallback mListener;
    private final Map<String, ConciergeSubCategoryItemAdapter> mSubCategoryItemAdapterMap = new HashMap<>();
    private Map<String, ListItemConciergeShopSubCategoryViewModel> mShopSubCategoryViewModelMap = new HashMap<>();
    private ConciergeViewItemByType mConciergeViewItemByType;
    private AppCompatSpinner mSortSpinner;
    private ListItemConciergeShopCategorySelectedViewModel mCategoryViewModel;
    private ConciergeSupplier mSupplier;

    public ConciergeShopProductByCategoryAdapter(Context context,
                                                 ConciergeViewItemByType conciergeViewItemByType,
                                                 ConciergeSupplier supplier,
                                                 List<ConciergeShopDetailDisplayAdaptive> data,
                                                 ShopProductAdapterCallback listener) {
        super(data);
        mConciergeViewItemByType = conciergeViewItemByType;
        mContext = context;
        mSupplier = supplier;
        mListener = listener;
        setCanLoadMore(false);
    }

    public AppCompatSpinner getSortSpinner() {
        return mSortSpinner;
    }

    public void setConciergeViewItemByType(ConciergeViewItemByType conciergeViewItemByType) {
        mConciergeViewItemByType = conciergeViewItemByType;
    }

    public int getSpanCountOfItemBaseOnItsModel(int position) {
        int viewType = getItemViewType(position);
        //Will manage to display one item view for a whole row for load more or sub category view types
        if (viewType == GRID_ITEM_WITH_TWO_COLUMNS_VIEW_TYPE ||
                viewType == GRID_ITEM_WITH_THREE_COLUMNS_VIEW_TYPE) {
            return 1;
        }

        return mConciergeViewItemByType.getSpanCount();
    }

    @Override
    public boolean hasLoadMore() {
        return canLoadMore();
    }

    @Override
    public int getItemViewType(int position) {
        if (position < mDatas.size()) {
            if (mDatas.get(position) instanceof ConciergeShopCategory) {
                return CATEGORY_VIEW_TYPE;
            } else if (mDatas.get(position) instanceof ConciergeSubCategory) {
                return SUB_CATEGORY_VIEW_TYPE;
            } else if (mDatas.get(position) instanceof ConciergeExtraItemTitle) {
                return EXTRA_ITEM_SECTION_TITLE_VIEW_TYPE;
            } else {
                if (mConciergeViewItemByType == ConciergeViewItemByType.NORMAL) {
                    return NORMAL_ITEM_VIEW_TYPE;
                } else if (mConciergeViewItemByType == ConciergeViewItemByType.TWO_COLUMNS) {
                    return GRID_ITEM_WITH_TWO_COLUMNS_VIEW_TYPE;
                } else {
                    return GRID_ITEM_WITH_THREE_COLUMNS_VIEW_TYPE;
                }
            }
        }

        return super.getItemViewType(position);
    }

    @Override
    public boolean shouldApplyGridSpacing(int itemPosition) {
        int itemViewType = getItemViewType(itemPosition);
        return itemViewType == GRID_ITEM_WITH_TWO_COLUMNS_VIEW_TYPE || itemViewType == GRID_ITEM_WITH_THREE_COLUMNS_VIEW_TYPE;
    }

    @Override
    public boolean isExtraItemSectionTitleViewItem(int currentPosition) {
        return getItemViewType(currentPosition) == EXTRA_ITEM_SECTION_TITLE_VIEW_TYPE;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == CATEGORY_VIEW_TYPE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_concierge_shop_category_selected_item).build();
        } else if (viewType == SUB_CATEGORY_VIEW_TYPE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_concierge_shop_sub_category_item).build();
        } else if (viewType == EXTRA_ITEM_SECTION_TITLE_VIEW_TYPE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_concierge_extra_item_section_title_item).build();
        } else if (viewType == GRID_ITEM_WITH_TWO_COLUMNS_VIEW_TYPE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_grid_product).build();
        } else if (viewType == GRID_ITEM_WITH_THREE_COLUMNS_VIEW_TYPE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_small_grid_product).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_concierge_shop_item).build();
        }
    }

    public ListItemConciergeShopCategorySelectedViewModel getCategoryViewModel() {
        return mCategoryViewModel;
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListConciergeShopInfoItemBinding) {
            holder.setVariable(BR.viewModel, new ItemConciergeShopInfoViewModel((ConciergeShop) mDatas.get(position)));
        } else if (holder.getBinding() instanceof ListConciergeShopCategorySelectedItemBinding) {
            mCategoryViewModel = new ListItemConciergeShopCategorySelectedViewModel(holder.getContext(),
                    (ConciergeShopCategory) mDatas.get(position),
                    mSupplier,
                    mListener.shouldShowOutOfStockItem(),
                    mListener.getBranIds(),
                    new ConciergeFilterProductCriteriaBottomSheetDialog.ConciergeFilterProductCriteriaBottomSheetDialogListener() {
                        @Override
                        public void onClearFilterCriteria() {
                            if (mListener != null) {
                                mListener.onClearFilterCriteria();
                            }
                        }

                        @Override
                        public void onApplyFilterCriteria(boolean isShowOutOfStockProduct, List<String> ids) {
                            if (mListener != null) {
                                mListener.onApplyFilterCriteria(isShowOutOfStockProduct, ids);
                            }
                        }
                    },
                    new ListItemConciergeShopCategorySelectedViewModel.ListItemConciergeShopCategorySelectedViewModelListener() {
                        @Override
                        public void onItemSortOptionChanged(ConciergeSortItemOption sortItemOption) {
                            if (mListener != null) {
                                mListener.onItemSortOptionChanged(sortItemOption);
                            }
                        }

                        @Override
                        public void onViewItemByOptionChanged(ConciergeViewItemByType selectedOption) {
                            if (mListener != null) {
                                mListener.onViewItemByOptionChanged(selectedOption);
                            }
                        }

                        @Override
                        public void onItemAddedToCart(ConciergeMenuItem item) {
                            //Do nothing
                        }

                        @Override
                        public void onHandlePaymentInProgressError(ConciergeSupportAddItemToCardViewModel viewModel,
                                                                   String errorMessage,
                                                                   ConciergeMenuItem menuItem,
                                                                   boolean isUpdateItemProcess) {
                            //Do nothing
                        }

                        @Override
                        public void showAddItemFromDifferentShopWarningPopup(ConciergeSupportAddItemToCardViewModel viewModel,
                                                                             OnCallbackListener<Response<SupportConciergeCustomErrorResponse>> clearBasketCallback) {
                            if (mListener != null) {
                                mListener.showAddItemFromDifferentShopWarningPopup(viewModel, clearBasketCallback);
                            }
                        }
                    });

            holder.setVariable(BR.viewModel, mCategoryViewModel);
            mSortSpinner = ((ListConciergeShopCategorySelectedItemBinding) holder.getBinding()).sortSpinner;
        } else if (holder.getBinding() instanceof ListConciergeShopSubCategoryItemBinding) {
            ConciergeSubCategory subCategory = (ConciergeSubCategory) mDatas.get(position);
            bindSubCategoryItem((ListConciergeShopSubCategoryItemBinding) holder.getBinding(), subCategory);
            ListItemConciergeShopSubCategoryViewModel viewModel = new ListItemConciergeShopSubCategoryViewModel(subCategory,
                    position,
                    subCategory1 -> {
                        resetSubcategoryExpand(subCategory1.getId());
                        if (mListener != null) {
                            mListener.onExpandSubCategory(holder.getAdapterPosition());
                        }
                    });
            holder.setVariable(BR.viewModel, viewModel);
            mShopSubCategoryViewModelMap.put(subCategory.getId(), viewModel);
        } else if (holder.getBinding() instanceof ListConciergeShopItemBinding) {
            ConciergeMenuItem conciergeMenuItem = (ConciergeMenuItem) mDatas.get(position);
            ListConciergeShopItemViewModel viewModel = new ListConciergeShopItemViewModel(holder.getContext(),
                    null,
                    conciergeMenuItem,
                    false,
                    getProductListener(conciergeMenuItem, position));
            holder.setVariable(BR.viewModel, viewModel);
        } else if (holder.getBinding() instanceof ListItemConciergeGridProductBinding) {
            ConciergeMenuItem conciergeMenuItem = (ConciergeMenuItem) mDatas.get(position);
            int itemAmountInBasket = ConciergeCartHelper.getExistingProductCountInCart(conciergeMenuItem);
            ListItemConciergeProductViewModel viewModel = new ListItemConciergeProductViewModel(holder.getContext(),
                    conciergeMenuItem,
                    itemAmountInBasket,
                    true,
                    mConciergeViewItemByType,
                    position,
                    getProductListener(conciergeMenuItem, position));
            holder.setVariable(BR.viewModel, viewModel);
        } else if (holder.getBinding() instanceof ListItemConciergeSmallGridProductBinding) {
            ConciergeMenuItem conciergeMenuItem = (ConciergeMenuItem) mDatas.get(position);
            int itemAmountInBasket = ConciergeCartHelper.getExistingProductCountInCart(conciergeMenuItem);
            ListItemConciergeProductViewModel viewModel = new ListItemConciergeProductViewModel(holder.getContext(),
                    conciergeMenuItem,
                    itemAmountInBasket,
                    true,
                    mConciergeViewItemByType,
                    position,
                    getProductListener(conciergeMenuItem, position));
            holder.setVariable(BR.viewModel, viewModel);
        }
    }

    private void resetSubcategoryExpand(String excludeId) {
        for (Map.Entry<String, ListItemConciergeShopSubCategoryViewModel> entry : mShopSubCategoryViewModelMap.entrySet()) {
            if (!TextUtils.equals(entry.getKey(), excludeId) &&
                    entry.getValue() != null &&
                    entry.getValue().getSubCategory().isSectionExpended()) {
                entry.getValue().resetExpandSection();
            }
        }

        //Reset in adapter data source
        for (ConciergeShopDetailDisplayAdaptive data : mDatas) {
            if (data instanceof ConciergeSubCategory && !TextUtils.equals(data.getId(), excludeId)) {
                ((ConciergeSubCategory) data).setSectionExpended(false);
            }
        }
    }

    private ListItemConciergeProductViewModel.ListItemConciergeProductViewModelListener getProductListener(ConciergeMenuItem conciergeMenuItem,
                                                                                                           int itemPosition) {
        return new ListItemConciergeProductViewModel.ListItemConciergeProductViewModelListener() {
            @Override
            public void onProductAddedFromDetail() {
                if (mListener != null) {
                    mListener.onProductAddedFromDetail();
                }
            }

            @Override
            public void onProductFailToAdd() {
                notifyItemChanged(itemPosition);
            }

            @Override
            public void onItemCountChanged(int newCount, boolean isRemove) {
                if (mContext instanceof AppCompatActivity) {
                    conciergeMenuItem.setProductCount(newCount);
                    Intent intent = new Intent(isRemove ?
                            AbsSupportShopCheckOutFragment.CART_ITEM_REMOVED_EVENT :
                            AbsSupportShopCheckOutFragment.CART_ITEM_ADDED_EVENT);
                    intent.putExtra(AbsSupportShopCheckOutFragment.CART_EVENT_DATA, conciergeMenuItem);
                    SendBroadCastHelper.verifyAndSendBroadCast(mContext, intent);
                }
            }

            @Override
            public void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {
                if (mListener != null) {
                    mListener.onAddFirstItemToCart(conciergeMenuItem);
                }
            }
        };
    }

    public void applyFilterResult(List<ConciergeShopDetailDisplayAdaptive> itemList) {
        ArrayList<ConciergeShopDetailDisplayAdaptive> newList = new ArrayList<>(itemList);
        newList.add(0, mDatas.get(0)); //Backup category
        mDatas = newList;
        notifyItemRangeChanged(1, mDatas.size());
    }

    public void removeItemDecoratorFromRecycleView(RecyclerView recyclerView) {
        int itemDecorationCount = recyclerView.getItemDecorationCount();
        for (int decorationCount = itemDecorationCount - 1; decorationCount >= 0; decorationCount--) {
            recyclerView.removeItemDecorationAt(decorationCount);
        }
    }

    public int getGridSpacingForItemDecorator() {
        return mContext.getResources().getDimensionPixelSize(R.dimen.space_large);
    }

    private void bindSubCategoryItem(ListConciergeShopSubCategoryItemBinding itemBinding, ConciergeSubCategory category) {
        if (category.getMenuItems() != null && !category.getMenuItems().isEmpty()) {
            List<ConciergeSupportItemAndSubCategoryAdaptive> list = new ArrayList<>(category.getMenuItems());
            if (!TextUtils.isEmpty(category.getNextPage())) {
                ConciergeSubCategoryLoadMoreDataModel loadMoreDataModel = new ConciergeSubCategoryLoadMoreDataModel();
                loadMoreDataModel.setConciergeSubCategory(category);
                list.add(loadMoreDataModel);
            }
            ConciergeSubCategoryItemAdapter adapter = new ConciergeSubCategoryItemAdapter(mContext,
                    list,
                    mConciergeViewItemByType,
                    new ConciergeSubCategoryItemAdapter.ConciergeSubCategoryItemAdapterCallback() {
                        /*
                          Will pass all actions from item in sub category to main category item screen listener to
                          handle the action.
                         */
                        @Override
                        public void onProductAddedFromDetail() {
                            if (mListener != null) {
                                mListener.onProductAddedFromDetail();
                            }
                        }

                        @Override
                        public void onSubCategoryPerformLoadMoreItem(ConciergeSubCategoryLoadMoreDataModel loadMoreDataModel) {
                            if (mListener != null) {
                                mListener.onSubCategoryPerformLoadMoreItem(loadMoreDataModel);
                            }
                        }

                        @Override
                        public void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {
                            if (mListener != null) {
                                mListener.onAddFirstItemToCart(conciergeMenuItem);
                            }
                        }
                    });

            removeItemDecoratorFromRecycleView(itemBinding.subCategoryItemRecyclerView);
            if (mConciergeViewItemByType == ConciergeViewItemByType.NORMAL) {
                itemBinding.subCategoryItemRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            } else {
                itemBinding.subCategoryItemRecyclerView.addItemDecoration(new ConciergeGridSpacingItemDecoration(adapter,
                        mConciergeViewItemByType.getSpanCount(),
                        getGridSpacingForItemDecorator(),
                        mContext.getResources().getDimensionPixelSize(R.dimen.space_xlarge),
                        false));
                GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, mConciergeViewItemByType.getSpanCount());
                gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return adapter.getSpanCountOfItemBaseOnItsModel(position);
                    }
                });
                itemBinding.subCategoryItemRecyclerView.setLayoutManager(gridLayoutManager);
            }
            itemBinding.subCategoryItemRecyclerView.setAdapter(adapter);
            mSubCategoryItemAdapterMap.put(category.getId(), adapter);
        } else {
            itemBinding.subCategoryItemRecyclerView.setAdapter(null);
        }
    }

    public void addSubCategoryLoadMoreItem(ConciergeSubCategory loadMoreSubCategoryItem) {
        ConciergeSubCategoryItemAdapter adapter = findSubCategoryItemAdapterBySubCategoryId(loadMoreSubCategoryItem.getId());
//        Timber.i("adapter: " + adapter + ", subCategory: " + new Gson().toJson(subCategory));
        if (adapter != null) {
            adapter.addLoadMoreItem(new ArrayList<>(loadMoreSubCategoryItem.getMenuItems()), !TextUtils.isEmpty(loadMoreSubCategoryItem.getNextPage()));
        }
    }

    private ConciergeSubCategoryItemAdapter findSubCategoryItemAdapterBySubCategoryId(String subCategoryId) {
        return mSubCategoryItemAdapterMap.get(subCategoryId);
    }

    public void onItemUpdatedInCart(ConciergeMenuItem conciergeMenuItem) {
        for (int index = 0; index < mDatas.size(); index++) {
            ConciergeShopDetailDisplayAdaptive adaptive = mDatas.get(index);
            if (adaptive instanceof ConciergeMenuItem) {
                if (!adaptive.isProductCategory() && TextUtils.equals(adaptive.getId(), conciergeMenuItem.getId())) {
                    notifyItemChanged(index);
                    break;
                }
            }
        }
        onItemUpdatedInCartOfSubCategoryItem(conciergeMenuItem);
    }

    public void refreshProductInCardCounter() {
        for (int index = 0; index < mDatas.size(); index++) {
            if (mDatas.get(index) instanceof ConciergeMenuItem) {
                notifyItemChanged(index);
            }
        }
    }

    public void onCartCleared() {
        for (int index = 0; index < mDatas.size(); index++) {
            ConciergeShopDetailDisplayAdaptive adaptive = mDatas.get(index);
            if (adaptive instanceof ConciergeMenuItem) {
                if (!adaptive.isProductCategory()) {
                    ConciergeMenuItem productOnDisplay = (ConciergeMenuItem) adaptive;
                    productOnDisplay.setProductCount(0);
                    notifyItemChanged(index);
                }
            }
        }
        clearSubCategoryItem();
    }

    private void clearSubCategoryItem() {
        for (Map.Entry<String, ConciergeSubCategoryItemAdapter> entry : mSubCategoryItemAdapterMap.entrySet()) {
            if (entry.getValue() != null) {
                entry.getValue().onCartCleared();
            }
        }
    }

    private void onItemUpdatedInCartOfSubCategoryItem(ConciergeMenuItem conciergeMenuItem) {
        for (Map.Entry<String, ConciergeSubCategoryItemAdapter> entry : mSubCategoryItemAdapterMap.entrySet()) {
            if (entry.getValue() != null) {
                if (entry.getValue().onItemUpdatedInCart(conciergeMenuItem)) {
                    break;
                }
            }
        }
    }

    public void updateProductOrderCounter(ArrayList<String> ids) {
        for (int i = 0; i < mDatas.size(); i++) {
            if (mDatas.get(i) instanceof ConciergeMenuItem) {
                for (String id : ids) {
                    if (TextUtils.equals(id, ((ConciergeMenuItem) mDatas.get(i)).getId())) {
                        notifyItemChanged(i);
                    }
                }
            }
        }
    }

    public interface ShopProductAdapterCallback {
        void onProductAddedFromDetail();

        void onSubCategoryPerformLoadMoreItem(ConciergeSubCategoryLoadMoreDataModel loadMoreDataModel);

        void onViewItemByOptionChanged(ConciergeViewItemByType selectedOption);

        default void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {
        }

        void showAddItemFromDifferentShopWarningPopup(ConciergeSupportAddItemToCardViewModel viewModel, OnCallbackListener<Response<SupportConciergeCustomErrorResponse>> clearBasketCallback);

        void onExpandSubCategory(int position);

        default void onItemSortOptionChanged(ConciergeSortItemOption sortItemOption) {
        }

        default void onClearFilterCriteria() {
        }

        default void onApplyFilterCriteria(boolean isShowOutOfStockProduct, List<String> ids) {
        }

        default boolean shouldShowOutOfStockItem() {
            return false;
        }

        default List<String> getBranIds() {
            return new ArrayList<>();
        }

        default ConciergeSortItemOption getSortOption() {
            return ConciergeSortItemOption.NAME_A_Z;
        }
    }
}
