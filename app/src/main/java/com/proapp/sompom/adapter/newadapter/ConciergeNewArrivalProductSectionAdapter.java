package com.proapp.sompom.adapter.newadapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.helper.ConciergeShopListDiffCallback;
import com.proapp.sompom.model.ConciergeItemAdaptive;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.newui.fragment.AbsSupportShopCheckOutFragment;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemConciergeProductViewModel;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Veasna Chhom on 5/3/22.
 */
public class ConciergeNewArrivalProductSectionAdapter extends RecyclerView.Adapter<BindingViewHolder> {

    private List<ConciergeMenuItem> mData;
    private ConciergeNewArrivalProductSectionAdapterCallback mListener;
    private int mItemWidth;

    public ConciergeNewArrivalProductSectionAdapter(Context context,
                                                    List<ConciergeMenuItem> data,
                                                    ConciergeNewArrivalProductSectionAdapterCallback listener) {
        mData = data;
        mListener = listener;
        mItemWidth = context.getResources().getDimensionPixelSize(R.dimen.concierge_push_item_size);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_concierge_grid_product).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        holder.itemView.getLayoutParams().width = mItemWidth;
        ConciergeMenuItem conciergeMenuItem = (ConciergeMenuItem) mData.get(position);
        int itemAmountInBasket = ConciergeCartHelper.getExistingProductCountInCart(conciergeMenuItem);
        ListItemConciergeProductViewModel viewModel = new ListItemConciergeProductViewModel(holder.getContext(),
                conciergeMenuItem,
                itemAmountInBasket,
                true,
                new ListItemConciergeProductViewModel.ListItemConciergeProductViewModelListener() {
                    @Override
                    public void onProductAddedFromDetail() {
                        if (mListener != null) {
                            mListener.onProductAddedFromDetail();
                        }
                    }

                    @Override
                    public void onProductFailToAdd() {
                        notifyItemChanged(holder.getAdapterPosition());
                    }

                    @Override
                    public void onItemCountChanged(int newAmount, boolean isRemove) {
                        if (holder.getContext() instanceof AppCompatActivity) {
                            conciergeMenuItem.setProductCount(newAmount);
                            Intent intent = new Intent(isRemove ?
                                    AbsSupportShopCheckOutFragment.CART_ITEM_REMOVED_EVENT :
                                    AbsSupportShopCheckOutFragment.CART_ITEM_ADDED_EVENT);
                            intent.putExtra(AbsSupportShopCheckOutFragment.CART_EVENT_DATA, conciergeMenuItem);
                            SendBroadCastHelper.verifyAndSendBroadCast(holder.getContext(), intent);
                        }
                    }

                    @Override
                    public void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {
                        if (mListener != null) {
                            mListener.onAddFirstItemToCart(conciergeMenuItem);
                        }
                    }
                }
        );
        holder.setVariable(BR.viewModel, viewModel);
    }

    public void refreshData(List<ConciergeMenuItem> newData) {
        if (mData.isEmpty()) {
            mData = newData;
            notifyDataSetChanged();
        } else {
            final ConciergeShopListDiffCallback diffCallback =
                    new ConciergeShopListDiffCallback(new ArrayList<>(mData), new ArrayList<>(newData));
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
            mData = newData;
            diffResult.dispatchUpdatesTo(this);
        }
    }

    public void onItemUpdatedInCart(ConciergeMenuItem conciergeMenuItem) {
        for (int index = 0; index < mData.size(); index++) {
            ConciergeMenuItem adaptive = mData.get(index);
            if (adaptive != null) {
                if (TextUtils.equals(adaptive.getId(), conciergeMenuItem.getId())) {
                    notifyItemChanged(index);
                    break;
                }
            }
        }
    }

    public void onCartCleared() {
        for (int index = 0; index < mData.size(); index++) {
            ConciergeMenuItem adaptive = mData.get(index);
            if (adaptive != null) {
                adaptive.setProductCount(0);
                notifyItemChanged(index);
            }
        }
    }

    public interface ConciergeNewArrivalProductSectionAdapterCallback {
        void onProductAddedFromDetail();

        void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem);
    }
}
