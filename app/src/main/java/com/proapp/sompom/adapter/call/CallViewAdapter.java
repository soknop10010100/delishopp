package com.proapp.sompom.adapter.call;

import android.content.Context;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ListItemCallViewBinding;
import com.proapp.sompom.model.call.CallData;
import com.proapp.sompom.viewholder.CommonBindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ItemCallViewModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

public class CallViewAdapter extends RecyclerView.Adapter<CommonBindingViewHolder<ListItemCallViewBinding>> {

    private List<CallData> mUserList;
    private CallViewAdapterListener mListener;
    private Map<String, ItemCallViewModel> mCallViewModelMap = new HashMap<>();
    private int mTotalParticipants;

    public CallViewAdapter(List<CallData> userList, int totalParticipants) {
        mUserList = userList;
        mTotalParticipants = totalParticipants;
    }

    public void setListener(CallViewAdapterListener listener) {
        mListener = listener;
    }

    public List<CallData> getUserList() {
        return mUserList;
    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    @NonNull
    @Override
    public CommonBindingViewHolder<ListItemCallViewBinding> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Timber.i("onCreateViewHolder: " + mListener.getCallViewHeight());
        CommonBindingViewHolder<ListItemCallViewBinding> viewHolder = new CommonBindingViewHolder.Builder<ListItemCallViewBinding>(parent,
                R.layout.list_item_call_view).build();
        viewHolder.getBinding().getRoot().getLayoutParams().height = mListener.getCallViewHeight();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CommonBindingViewHolder<ListItemCallViewBinding> holder, int position) {
        Timber.i("onBindViewHolder: position " + position + ", mListener.isInFloatingMode(): " + mListener.isInFloatingMode());
        CallData callData = mUserList.get(position);
        ItemCallViewModel viewModel = new ItemCallViewModel(mListener,
                callData,
                getItemCount(),
                position,
                mTotalParticipants);
        viewModel.setCallSectionInfoVisibility(mListener.isInFloatingMode() ? View.INVISIBLE : View.VISIBLE);
        holder.getBinding().setVariable(BR.viewModel, viewModel);
        if (callData.getVideoCallView() != null) {
            holder.getBinding().videoContainerView.removeAllViews();
            checkToRemovePreviousCallViewParent(callData.getVideoCallView());
            holder.getBinding().videoContainerView.addView(callData.getVideoCallView());
        }
        mCallViewModelMap.put(callData.getUser().getId(), viewModel);
        holder.getBinding().getRoot().setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onCalLViewClicked();
            }
        });
    }

    public ItemCallViewModel getCallViewModelFromUserId(String userId) {
        return mCallViewModelMap.get(userId);
    }

    private void checkToRemovePreviousCallViewParent(SurfaceView callView) {
        if (callView.getParent() != null) {
            ((ViewGroup) callView.getParent()).removeView(callView);
        }
    }

    public Map<String, ItemCallViewModel> getCallViewModelMap() {
        return mCallViewModelMap;
    }

    public interface CallViewAdapterListener {

        default int getCallViewHeight() {
            return 0;
        }

        default void onCalLViewClicked() {
        }

        default boolean isInFloatingMode() {
            return false;
        }

        default Context getApplicationContext() {
            return null;
        }
    }
}
