package com.proapp.sompom.adapter.newadapter;

import android.view.ViewGroup;

import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.databinding.ListItemGroupMediaSectionBinding;
import com.proapp.sompom.model.GroupMediaAdaptive;
import com.proapp.sompom.model.GroupMediaSection;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemGroupMediaSectionViewModel;

import java.util.List;

/**
 * Created by Veasna Chhom on 7/27/20.
 */
public abstract class AbsGroupMediaAdapter<T extends GroupMediaAdaptive> extends
        RefreshableAdapter<GroupMediaSection<T>, BindingViewHolder> {

    protected onItemClickListener<T> mListener;

    public AbsGroupMediaAdapter(List<GroupMediaSection<T>> datas) {
        super(datas);
    }

    public void setListener(onItemClickListener<T> listener) {
        mListener = listener;
    }

    @Override
    public final BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_group_media_section).build();
    }

    @Override
    public final void onBindData(BindingViewHolder holder, int position) {
        ListItemGroupMediaSectionViewModel<T> viewModel = new ListItemGroupMediaSectionViewModel<>(mDatas.get(position));
        holder.setVariable(BR.viewModel, viewModel);
        bindSectionItem((ListItemGroupMediaSectionBinding) holder.getBinding(), mDatas.get(position));
    }

    protected abstract void bindSectionItem(ListItemGroupMediaSectionBinding binding, GroupMediaSection<T> section);

    protected void onItemClicked(T media) {
        if (mListener != null) {
            mListener.onItemClicked(media);
        }
    }

    public interface onItemClickListener<T extends GroupMediaAdaptive> {
        void onItemClicked(T media);
    }
}
