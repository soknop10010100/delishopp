package com.proapp.sompom.adapter;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.proapp.sompom.R;
import com.proapp.sompom.viewholder.BindingViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by he.rotha on 3/7/16.
 */

public abstract class RefreshableAdapter<D, T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    private static final int AT_TOP = 0X000;
    private static final int AT_BOTTOM = 0X001;
    private static final int VIEW_ITEM = 0X002;
    protected static final int VIEW_PROGRESS = 0X003;

    public List<D> mDatas;

    private boolean mCanLoadMore = true;
    private boolean mIsPreventBindLoadMoreItem = true;

    public RefreshableAdapter() {
        mDatas = new ArrayList<>();
    }

    public RefreshableAdapter(List<D> datas) {
        if (datas == null) {
            datas = new ArrayList<>();
        }
        mDatas = datas;
    }

    @Override
    public int getItemCount() {
        if (mCanLoadMore) {
            return mDatas.size() + 1;
        } else {
            return mDatas.size();
        }
    }

    public void setPreventBindLoadMoreItem(boolean preventBindLoadMoreItem) {
        mIsPreventBindLoadMoreItem = preventBindLoadMoreItem;
    }

    public boolean canLoadMore() {
        return mCanLoadMore;
    }

    public void setCanLoadMore(final boolean canLoadMore) {
        mCanLoadMore = canLoadMore;
    }

    public List<D> getDatas() {
        return mDatas;
    }

    public void setDatas(List<D> datas) {
        mDatas = datas;
    }

    public void updateData(List<D> updateLIst, int where) {
        if (where == AT_BOTTOM) {
            mDatas.addAll(updateLIst);
        } else if (where == AT_TOP) {
            mDatas.addAll(0, updateLIst);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final T holder, final int position) {
        if (mIsPreventBindLoadMoreItem) {
            if (position < mDatas.size()) {
                onBindData(holder, position);
            }
        } else {
            onBindData(holder, position);
        }
    }

    public void removeLoadMoreLayout() {
        /*
         Make sure that loadMore status is previously enable and thus the load more layout appeared,
         and now the load more status is disable and we would like to remove it. So normally, we removed
         the last item of the list as it is the load more layout previously set to.
         */
        notifyItemRemoved(getItemCount());
    }

    public void addLoadMoreData(List<D> datas) {
        int previousItemCount = getItemCount();
        mDatas.addAll(datas);
        notifyItemRangeInserted(previousItemCount + 1, datas.size());
    }

    public void clear() {
        mDatas.clear();
        setCanLoadMore(false);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public T onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        if (viewType == VIEW_PROGRESS) {
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                    R.layout.layout_loading, parent, false);
            return (T) new BindingViewHolder(binding);
        } else {
            return onCreateView(parent, viewType);
        }
    }

    public abstract T onCreateView(final ViewGroup parent, final int viewType);

    public abstract void onBindData(final T holder, final int position);

    @Override
    public int getItemViewType(int position) {
        if (position < mDatas.size()) {
            return VIEW_ITEM;
        } else {
            return VIEW_PROGRESS;
        }
    }
}

