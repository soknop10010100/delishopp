package com.proapp.sompom.adapter.newadapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.proapp.sompom.newui.fragment.AbsBaseFragment;

import java.util.List;

/**
 * Created by he.rotha on 3/4/16.
 */
public class StoreViewPagerAdapter extends FragmentStatePagerAdapter {
    private List<AbsBaseFragment> mFragmentProductDetailItems;

    public StoreViewPagerAdapter(FragmentManager fm, List<AbsBaseFragment> fr) {
        super(fm);
        mFragmentProductDetailItems = fr;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentProductDetailItems.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentProductDetailItems.size();
    }

}
