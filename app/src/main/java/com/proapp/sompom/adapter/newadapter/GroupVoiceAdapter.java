package com.proapp.sompom.adapter.newadapter;

import android.app.Activity;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ListItemGroupMediaSectionBinding;
import com.proapp.sompom.model.GroupMediaSection;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemGroupVoiceViewModel;

import java.util.List;

/**
 * Created by Veasna Chhom on 7/27/20.
 */
public class GroupVoiceAdapter extends AbsGroupMediaAdapter<Chat> {

    private Activity mActivity;

    public GroupVoiceAdapter(Activity activity, List<GroupMediaSection<Chat>> datas) {
        super(datas);
        mActivity = activity;
    }

    @Override
    protected void bindSectionItem(ListItemGroupMediaSectionBinding binding, GroupMediaSection<Chat> section) {
        GroupFileItemAdapter adapter = new GroupFileItemAdapter(section.getMediaList());
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(binding.getRoot().getContext()));
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setAdapter(adapter);
    }

    public class GroupFileItemAdapter extends RecyclerView.Adapter<BindingViewHolder> {

        private List<Chat> mData;

        public GroupFileItemAdapter(List<Chat> data) {
            mData = data;
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        @NonNull
        @Override
        public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_group_voice).build();
        }

        @Override
        public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
            ListItemGroupVoiceViewModel viewModel = new ListItemGroupVoiceViewModel(mActivity,
                    mData.get(position));
            holder.setVariable(BR.viewModel, viewModel);
            holder.getBinding().getRoot().setOnClickListener(v -> onItemClicked(mData.get(position)));
        }
    }
}
