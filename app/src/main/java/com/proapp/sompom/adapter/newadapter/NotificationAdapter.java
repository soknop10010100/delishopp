package com.proapp.sompom.adapter.newadapter;

import android.content.Context;
import android.view.ViewGroup;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.RefreshableAdapter;
import com.proapp.sompom.helper.SeparateNotificationSectionHelper;
import com.proapp.sompom.listener.OnNotificationItemClickListener;
import com.proapp.sompom.model.ListItemNotificationViewModel;
import com.proapp.sompom.model.notification.NotificationAdaptive;
import com.proapp.sompom.model.notification.NotificationHeader;
import com.proapp.sompom.viewholder.BindingViewHolder;

import java.util.List;

/**
 * Created by nuonveyo on 7/4/18.
 */

public class NotificationAdapter extends RefreshableAdapter<NotificationAdaptive, BindingViewHolder> {
    private static final int HEADER_ITEM = 0x001;
    private static final int NORMAL_ITEM = 0x002;
    private static final int SEPARATE_HOUR = 24;
    private final SeparateNotificationSectionHelper mSeparateNotificationSectionHelper;
    private final OnNotificationItemClickListener mOnItemClickListener;

    public NotificationAdapter(List<NotificationAdaptive> datas, OnNotificationItemClickListener onItemClickListener) {
        super(datas);
        mSeparateNotificationSectionHelper = new SeparateNotificationSectionHelper();
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == HEADER_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_notification_header).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_notification_normal).build();
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        Context context = holder.getBinding().getRoot().getContext();
        ListItemNotificationViewModel notificationViewModel = new ListItemNotificationViewModel(context,
                mDatas.get(position),
                position,
                mOnItemClickListener);
        notificationViewModel.setEnableMargin(position != 0);
        holder.setVariable(BR.viewModel, notificationViewModel);
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= 0 && position < mDatas.size()) {
            if (mDatas.get(position) instanceof NotificationHeader) {
                return HEADER_ITEM;
            } else {
                return NORMAL_ITEM;
            }
        }
        return super.getItemViewType(position);
    }

    @Override
    public void addLoadMoreData(List<NotificationAdaptive> itemsToAdd) {
        boolean isEarlierAdded = false;
        for (NotificationAdaptive data : mDatas) {
            if (data instanceof NotificationHeader) {
                NotificationHeader notificationHeader = (NotificationHeader) data;
                if (notificationHeader.getNotificationItem().getId() == 2) {
                    isEarlierAdded = true;
                    break;
                }
            }
        }

        if (isEarlierAdded) {
            super.addLoadMoreData(itemsToAdd);
        } else {
            mSeparateNotificationSectionHelper.addMoreNotificationData(SEPARATE_HOUR, itemsToAdd, this::addLoadMoreData);
        }
    }

    public void updateItem(int position, NotificationAdaptive data) {
        mDatas.set(position, data);
        notifyItemChanged(position);
    }
}
