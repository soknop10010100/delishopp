package com.proapp.sompom.intent.newintent;

import android.content.Intent;

import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 7/12/18.
 */

public class TimelineDetailIntentResult extends Intent {

    public TimelineDetailIntentResult(Intent o) {
        super(o);
    }

    public TimelineDetailIntentResult(WallStreetAdaptive lifeStream,
                                      boolean isRemoveItem,
                                      boolean isGrantPermissionFloatingVideoFirstTime) {
        putExtra(SharedPrefUtils.DATA, lifeStream);
        putExtra(SharedPrefUtils.STATUS, isRemoveItem);
        putExtra(SharedPrefUtils.ID, isGrantPermissionFloatingVideoFirstTime);
    }

    public boolean isRemoveItem() {
        return getBooleanExtra(SharedPrefUtils.STATUS, false);
    }

    public WallStreetAdaptive getLifeStream() {
        return getParcelableExtra(SharedPrefUtils.DATA);
    }

    public boolean isGrantPermissionFloatingVideoFirstTime() {
        return getBooleanExtra(SharedPrefUtils.ID, false);
    }
}
