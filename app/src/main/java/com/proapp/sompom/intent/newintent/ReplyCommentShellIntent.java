package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.notification.NotificationListAndRedirectionHelper;
import com.proapp.sompom.model.notification.RedirectionNotificationData;
import com.proapp.sompom.newui.ReplyCommentShellActivity;

public class ReplyCommentShellIntent extends Intent {

    public ReplyCommentShellIntent(Context context, RedirectionNotificationData data) {
        super(context, ReplyCommentShellActivity.class);
        putExtra(NotificationListAndRedirectionHelper.NOTIFICATION_EXCHANGE_DATA, data);
    }

    public ReplyCommentShellIntent(Intent o) {
        super(o);
    }

    public RedirectionNotificationData getRedirectionNotificationData() {
        return getParcelableExtra(NotificationListAndRedirectionHelper.NOTIFICATION_EXCHANGE_DATA);
    }
}
