package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.ConciergeOrderTrackingActivity;

/**
 * Created by Or Vitovongsak on 4/11/21.
 */

public class ConciergeOrderTrackingIntent extends Intent {

    public static String HAS_DATA_FROM_SERVER = "HAS_DATA_FROM_SERVER";

    public ConciergeOrderTrackingIntent(Context packageContext) {
        super(packageContext, ConciergeOrderTrackingActivity.class);
    }
}
