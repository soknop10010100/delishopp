package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.concierge.ConciergeOrderResponseWithABAPayWay;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.newui.ConciergeCheckoutActivity;

/**
 * Created by Or Vitovongsak on 16/9/21.
 */

public class ConciergeCheckoutIntent extends Intent {

    public static final String FIELD_IS_OPEN_FROM_REORDER = "FIELD_IS_OPEN_FROM_REORDER";
    public static final String FIELD_USER_PRIMARY_ADDRESS = "FIELD_USER_PRIMARY_ADDRESS";
    public static final String FIELD_SELECTED_TIMESLOT_ID = "FIELD_SELECTED_TIMESLOT_ID";
    public static final String FIELD_RESUME_ABA_PAYMENT_DATA = "FIELD_RESUME_ABA_PAYMENT_DATA";
    public static final String FIELD_DELIVERY_INSTRUCTION = "FIELD_DELIVERY_INSTRUCTION";
    public static final String FIELD_IS_SLOT_PROVINCE = "FIELD_IS_SLOT_PROVINCE";

    public ConciergeCheckoutIntent(Context context,
                                   ConciergeUserAddress primaryAddress,
                                   String selectedTimeSlotId,
                                   boolean isSlotProvince,
                                   String deliveryInstruction) {
        super(context, ConciergeCheckoutActivity.class);
        putExtra(FIELD_USER_PRIMARY_ADDRESS, primaryAddress);
        putExtra(FIELD_SELECTED_TIMESLOT_ID, selectedTimeSlotId);
        putExtra(FIELD_IS_SLOT_PROVINCE, isSlotProvince);
        putExtra(FIELD_DELIVERY_INSTRUCTION, deliveryInstruction);
    }

    public ConciergeCheckoutIntent(Context context,
                                   ConciergeUserAddress primaryAddress,
                                   String selectedTimeSlotId,
                                   boolean isSlotProvince,
                                   ConciergeOrderResponseWithABAPayWay resumePaymentData,
                                   String deliveryInstruction) {
        super(context, ConciergeCheckoutActivity.class);
        putExtra(FIELD_USER_PRIMARY_ADDRESS, primaryAddress);
        putExtra(FIELD_SELECTED_TIMESLOT_ID, selectedTimeSlotId);
        putExtra(FIELD_IS_SLOT_PROVINCE, isSlotProvince);
        putExtra(FIELD_RESUME_ABA_PAYMENT_DATA, resumePaymentData);
        putExtra(FIELD_DELIVERY_INSTRUCTION, deliveryInstruction);
    }

    public ConciergeCheckoutIntent(Context context, boolean isOpenFromReOrder) {
        super(context, ConciergeCheckoutActivity.class);
        putExtra(FIELD_IS_OPEN_FROM_REORDER, isOpenFromReOrder);
    }

    public ConciergeCheckoutIntent(Intent o) {
        super(o);
    }

    public boolean getIsOpenFromReorder() {
        return getBooleanExtra(FIELD_IS_OPEN_FROM_REORDER, false);
    }

    public ConciergeUserAddress getPrimaryUserAddress() {
        return getParcelableExtra(FIELD_USER_PRIMARY_ADDRESS);
    }

    public String getSelectedTimeSlotId() {
        return getStringExtra(FIELD_SELECTED_TIMESLOT_ID);
    }

    public boolean getIsSlotProvince() {
        return getBooleanExtra(FIELD_IS_SLOT_PROVINCE, false);
    }

    public ConciergeOrderResponseWithABAPayWay getResumeABAPaymentData() {
        return getParcelableExtra(FIELD_RESUME_ABA_PAYMENT_DATA);
    }

    public String getDeliveryInstruction() {
        return getStringExtra(FIELD_DELIVERY_INSTRUCTION);
    }
}
