package com.proapp.sompom.intent;

import android.content.Intent;

import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 6/12/18.
 */

public class SelectAddressIntentResult extends Intent {
    public SelectAddressIntentResult(SearchAddressResult searchAddressResult) {
        putExtra(SharedPrefUtils.DATA, searchAddressResult);
    }

    public SelectAddressIntentResult(Intent data) {
        super(data);
    }

    public SearchAddressResult getResult() {
        return getParcelableExtra(SharedPrefUtils.DATA);
    }
}
