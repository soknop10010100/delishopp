package com.proapp.sompom.intent.newintent;

import android.content.Intent;

/**
 * Created by He Rotha on 2019-08-12.
 */
public class MyFileIntent extends Intent {

    public void performFileSearch() {
        setAction(Intent.ACTION_OPEN_DOCUMENT);
        addCategory(CATEGORY_OPENABLE);
        setType("*/*");
        putExtra(Intent.EXTRA_LOCAL_ONLY, true);
    }
}
