package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.ResetPasswordActivity;
import com.proapp.sompom.viewmodel.ResetPasswordViewModel;

/**
 * Created by Veasna Chhom on 5/18/22.
 */
public class ResetPasswordIntent extends Intent {

    public ResetPasswordIntent(Context packageContext, String email) {
        super(packageContext, ResetPasswordActivity.class);
        putExtra(ResetPasswordViewModel.FIELD_EMAIL, email);
    }

    public ResetPasswordIntent(Intent o) {
        super(o);
    }

    public String getEmail() {
        return getStringExtra(ResetPasswordViewModel.FIELD_EMAIL);
    }
}
