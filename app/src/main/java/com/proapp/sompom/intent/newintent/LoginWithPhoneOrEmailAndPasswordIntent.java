package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.LoginWithPhoneOrEmailAndPasswordActivity;

/**
 * Created by Veasna Chhom on 6/4/18.
 */
public class LoginWithPhoneOrEmailAndPasswordIntent extends Intent {
    public LoginWithPhoneOrEmailAndPasswordIntent(Context packageContext) {
        super(packageContext, LoginWithPhoneOrEmailAndPasswordActivity.class);
    }
}
