package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.emun.OpenBrandDetailType;
import com.proapp.sompom.newui.ConciergeBrandDetailActivity;

/**
 * Created by Chhom Veasna on 5/4/22.
 */
public class ConciergeBrandDetailIntent extends Intent {

    public static final String TEXT_PARAM = "TEXT_PARAM";
    public static final String TITLE_PARAM = "TITLE_PARAM";
    public static final String OPEN_TYPE = "OPEN_TYPE";

    public ConciergeBrandDetailIntent(Context packageContext, String textParam) {
        super(packageContext, ConciergeBrandDetailActivity.class);
        putExtra(ConciergeBrandDetailIntent.TEXT_PARAM, textParam);
        putExtra(OPEN_TYPE, OpenBrandDetailType.OPEN_AS_NORMAL_MODE.getValue());
    }

    public ConciergeBrandDetailIntent(Context packageContext, String textParam, OpenBrandDetailType openBrandDetailType) {
        super(packageContext, ConciergeBrandDetailActivity.class);
        putExtra(ConciergeBrandDetailIntent.TEXT_PARAM, textParam);
        putExtra(OPEN_TYPE, openBrandDetailType.getValue());
    }

    public ConciergeBrandDetailIntent(Context packageContext,
                                      String textParam,
                                      String titleParam,
                                      OpenBrandDetailType openBrandDetailType) {
        super(packageContext, ConciergeBrandDetailActivity.class);
        putExtra(ConciergeBrandDetailIntent.TEXT_PARAM, textParam);
        putExtra(ConciergeBrandDetailIntent.TITLE_PARAM, titleParam);
        putExtra(OPEN_TYPE, openBrandDetailType.getValue());
    }

    public ConciergeBrandDetailIntent(Intent o) {
        super(o);
    }

    public String getBrandId() {
        return getStringExtra(TEXT_PARAM);
    }

    public String getTitleParam() {
        return getStringExtra(TITLE_PARAM);
    }

    public OpenBrandDetailType getOpenType() {
        return OpenBrandDetailType.fromValue(getIntExtra(OPEN_TYPE, OpenBrandDetailType.OPEN_AS_NORMAL_MODE.getValue()));
    }
}
