package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.ShopDeliveryTimeSelection;
import com.proapp.sompom.newui.ConciergeDeliveryActivity;

public class ConciergeDeliveryIntent extends Intent {

    public static final String DATA = "DATA";
    public static final String IS_VIEW_ONLY = "IS_VIEW_ONLY";

    public ConciergeDeliveryIntent(Context context, ShopDeliveryTimeSelection selection) {
        super(context, ConciergeDeliveryActivity.class);
        putExtra(DATA, selection);
    }

    public ConciergeDeliveryIntent(Intent o) {
        super(o);
    }

    public static ConciergeDeliveryIntent getNewViewOnlyInstance(Context context) {
        ConciergeDeliveryIntent conciergeDeliveryIntent = new ConciergeDeliveryIntent(context, null);
        conciergeDeliveryIntent.putExtra(IS_VIEW_ONLY, true);

        return conciergeDeliveryIntent;
    }

    public ShopDeliveryTimeSelection getSelection() {
        return getParcelableExtra(DATA);
    }

    public boolean isViewOnlyMode() {
        return getBooleanExtra(IS_VIEW_ONLY, false);
    }
}
