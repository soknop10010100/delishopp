package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.newui.SearchConversationActivity;

/**
 * Veasna Chhom on 8/6/2020.
 */

public class SearchConversationIntent extends Intent {

    public static final String GROUP_DETAIL = "GROUP_DETAIL";
    public static final String CONVERSATION = "CONVERSATION";

    public SearchConversationIntent(Context context,
                                    Conversation conversation,
                                    GroupDetail groupDetail) {
        super(context, SearchConversationActivity.class);
        putExtra(GROUP_DETAIL, groupDetail);
        putExtra(CONVERSATION, conversation);
    }

    public SearchConversationIntent(Intent o) {
        super(o);
    }

    public GroupDetail getGroupDetail() {
        return getParcelableExtra(GROUP_DETAIL);
    }

    public Conversation getConversation() {
        return getParcelableExtra(CONVERSATION);
    }
}
