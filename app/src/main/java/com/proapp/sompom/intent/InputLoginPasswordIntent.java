package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.newui.InputLoginPasswordActivity;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class InputLoginPasswordIntent extends Intent {

    public static final String DATA = "DATA";

    public InputLoginPasswordIntent(Context packageContext, ExchangeAuthData data) {
        super(packageContext, InputLoginPasswordActivity.class);
        putExtra(DATA, data);
    }

    public InputLoginPasswordIntent(Intent o) {
        super(o);
    }

    public ExchangeAuthData getPassingData() {
        return getParcelableExtra(DATA);
    }
}
