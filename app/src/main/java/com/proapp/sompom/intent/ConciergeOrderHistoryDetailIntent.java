package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.ConciergeOrderHistoryDetailActivity;

/**
 * Created by Or Vitovongsak on 16/9/21.
 */

public class ConciergeOrderHistoryDetailIntent extends Intent {

    public static final String ORDER_ID = "ORDER_ID";
    public static final String ORDER_NUMBER = "ORDER_NUMBER";

    public ConciergeOrderHistoryDetailIntent(Context context,
                                             String orderId,
                                             String orderNumber) {
        super(context, ConciergeOrderHistoryDetailActivity.class);
        putExtra(ORDER_ID, orderId);
        putExtra(ORDER_NUMBER, orderNumber);
    }

    public ConciergeOrderHistoryDetailIntent(Context context, String orderId) {
        super(context, ConciergeOrderHistoryDetailActivity.class);
        putExtra(ORDER_ID, orderId);
    }

    public ConciergeOrderHistoryDetailIntent(Intent o) {
        super(o);
    }

    public String getOrderId() {
        return getStringExtra(ORDER_ID);
    }

    public String getOrderNumber() {
        return getStringExtra(ORDER_NUMBER);
    }
}
