package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.newui.ConciergeNewAddressActivity;

/**
 * Created by Or Vitovongsak on 22/11/21.
 */
public class ConciergeNewAddressIntent extends Intent {

    public static final String IS_EDIT_ADDRESS_MODE = "IS_EDIT_ADDRESS_MODE";
    public static final String IS_ADDRESS_DELETED = "IS_ADDRESS_DELETED";
    public static final String ADDRESS_DATA = "ADDRESS_DATA";
    public static final String SHOULD_DISPLAY_PRIMARY_ADDRESS_FIELD = "SHOULD_DISPLAY_PRIMARY_ADDRESS_FIELD";

    public ConciergeNewAddressIntent(Context packageContext) {
        super(packageContext, ConciergeNewAddressActivity.class);
    }

    public ConciergeNewAddressIntent(Context packageContext, boolean shouldDisplayPrimaryAddressOption) {
        super(packageContext, ConciergeNewAddressActivity.class);
        putExtra(SHOULD_DISPLAY_PRIMARY_ADDRESS_FIELD, shouldDisplayPrimaryAddressOption);
    }

    public static ConciergeNewAddressIntent getEditAddressIntent(Context context,
                                                                 ConciergeUserAddress addressToEdit) {
        ConciergeNewAddressIntent intent = new ConciergeNewAddressIntent(context, true, addressToEdit);
        /*
            Will not display setting primary option for existing primary address.
         */
        intent.putExtra(SHOULD_DISPLAY_PRIMARY_ADDRESS_FIELD, !addressToEdit.isPrimary());

        return intent;
    }

    private ConciergeNewAddressIntent(Context packageContext,
                                      boolean isEditAddress,
                                      ConciergeUserAddress addressToEdit) {
        super(packageContext, ConciergeNewAddressActivity.class);
        putExtra(IS_EDIT_ADDRESS_MODE, isEditAddress);
        putExtra(ADDRESS_DATA, addressToEdit);
    }

    public ConciergeNewAddressIntent(Intent intent) {
        super(intent);
    }

    public ConciergeUserAddress getAddressData() {
        return getParcelableExtra(ADDRESS_DATA);
    }

    public boolean getIsAddressDeleted() {
        return getBooleanExtra(IS_ADDRESS_DELETED, false);
    }

    public boolean getIsEditAddressMode() {
        return getBooleanExtra(IS_EDIT_ADDRESS_MODE, false);
    }

    public boolean shouldDisplayPrimaryAddressField() {
        return getBooleanExtra(SHOULD_DISPLAY_PRIMARY_ADDRESS_FIELD, false);
    }
}
