package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.CheckEmailActivity;

/**
 * Created by Veasna Chhom on 2/10/22.
 */
public class CheckEmailIntent extends Intent {

    public CheckEmailIntent(Context packageContext) {
        super(packageContext, CheckEmailActivity.class);
    }
}
