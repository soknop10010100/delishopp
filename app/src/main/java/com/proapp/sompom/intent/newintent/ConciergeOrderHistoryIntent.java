package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.ConciergeOrderHistoryActivity;

/**
 * Created by Chhom Veasna on 21/9/2021.
 */
public class ConciergeOrderHistoryIntent extends Intent {

    public ConciergeOrderHistoryIntent(Context packageContext) {
        super(packageContext, ConciergeOrderHistoryActivity.class);
    }
}
