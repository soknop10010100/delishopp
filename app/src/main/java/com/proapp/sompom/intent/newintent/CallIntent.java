package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.proapp.sompom.chat.call.AbsCallClient;
import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.chat.call.IncomingCallDataHolder;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 6/21/18.
 */

public class CallIntent extends Intent {

    public static final String START_FROM_TYPE = "START_FROM_TYPE";
    public static final String CALL_NOTIFICATION_ACTION_TYPE = "CALL_NOTIFICATION_ACTION_TYPE";
    public static final String GROUP_ID = "GROUP_ID";
    public static final String CALL_ACTION_TYPE = "CALL_ACTION_TYPE";
    public static final String BUNDLE_DATA = "BUNDLE_DATA";
    public static final String IS_FROM_ANSWER_VIA_NOTIFICATION = "IS_FROM_ANSWER_VIA_NOTIFICATION";
    public static final String NEED_TO_RESIZE_SCREEN_BACK = "NEED_TO_RESIZE_SCREEN_BACK";

    public CallIntent(Context context, Class<?> client) {
        super(context, client);
    }

    public CallIntent(Context context,
                      Class<?> client,
                      IncomingCallDataHolder incomingCallDataHolder) {
        super(context, client);
        /*
           There is parcel parsing error when we put parcel extra and with other data type extra such
           as int along with the intent. To fix this we need to parcel extra in a bundle and put that
           bundle and other extra along with intent.
         */
        Bundle bundle = new Bundle();
        bundle.putParcelable(SharedPrefUtils.STATUS, incomingCallDataHolder);
        putExtra(BUNDLE_DATA, bundle);
        putExtra(CALL_ACTION_TYPE, AbsCallClient.CallActionType.INCOMING.getValue());
        setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    public CallIntent(Context context, Class<?> client, User callTo, Product product) {
        super(context, client);
        putExtra(SharedPrefUtils.ID, callTo);
        putExtra(SharedPrefUtils.PRODUCT, product);
        putExtra(CALL_ACTION_TYPE, AbsCallClient.CallActionType.CALL.getValue());
    }

    public CallIntent(Context context, Class<?> client, User callTo, int startFromType) {
        super(context, client);
        putExtra(SharedPrefUtils.ID, callTo);
        putExtra(START_FROM_TYPE, startFromType);
        putExtra(CALL_ACTION_TYPE, AbsCallClient.CallActionType.CALL.getValue());
    }

    public CallIntent(Context context, Class<?> client,
                      User callTo,
                      Conversation conversation,
                      int startFromType) {
        super(context, client);
        putExtra(SharedPrefUtils.ID, callTo);
        putExtra(CALL_ACTION_TYPE, AbsCallClient.CallActionType.CALL.getValue());
        putExtra(START_FROM_TYPE, startFromType);
        if (conversation != null) {
            putExtra(CallHelper.GROUP, UserGroup.newInstance(conversation));
        }
    }

    public CallIntent(Intent data) {
        super(data);
    }

    public UserGroup getGroup() {
        return getParcelableExtra(CallHelper.GROUP);
    }

    public StartFromType getStartFromType() {
        return StartFromType.from(getIntExtra(START_FROM_TYPE, 0));
    }

    public IncomingCallDataHolder getIncomingCallData() {
        Bundle bundleExtra = getBundleExtra(BUNDLE_DATA);
        if (bundleExtra != null) {
            return bundleExtra.getParcelable(SharedPrefUtils.STATUS);
        }

        return null;
    }

    public Product getProduct() {
        return getParcelableExtra(SharedPrefUtils.PRODUCT);
    }

    public User getRecipient() {
        return getParcelableExtra(SharedPrefUtils.ID);
    }

    public String getCallType() {
        return getStringExtra(CallHelper.TYPE);
    }

    public enum StartFromType {

        CHAT_SCREEN(1),
        CONVERSATION(2),
        GROUP_DETAIL(3),
        UNKNOWN(101);

        private int mType;

        StartFromType(int type) {
            this.mType = type;
        }

        public int getType() {
            return mType;
        }

        public static StartFromType from(int value) {
            for (StartFromType type : StartFromType.values()) {
                if (type.getType() == value) {
                    return type;
                }
            }

            return UNKNOWN;
        }
    }

    public AbsCallService.CallActionType getCallActionType() {
        return AbsCallService.CallActionType.getTypeFromValue(getStringExtra(CALL_ACTION_TYPE));
    }

    public boolean isNeedToResizeCallScreenToFull() {
        return getBooleanExtra(NEED_TO_RESIZE_SCREEN_BACK, false);
    }
}
