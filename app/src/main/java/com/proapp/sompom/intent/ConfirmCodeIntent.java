package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.newui.ConfirmCodeActivity;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class ConfirmCodeIntent extends Intent {

    public static final String DATA = "DATA";
    public static final String FIREBASE_TOKEN = "FIREBASE_TOKEN";

    public ConfirmCodeIntent(Context packageContext, ExchangeAuthData data) {
        super(packageContext, ConfirmCodeActivity.class);
        putExtra(DATA, data);
    }

    public ConfirmCodeIntent(Context packageContext, ExchangeAuthData data, String firebaseToken) {
        super(packageContext, ConfirmCodeActivity.class);
        putExtra(DATA, data);
        putExtra(FIREBASE_TOKEN, firebaseToken);
    }

    public ConfirmCodeIntent(Intent o) {
        super(o);
    }

    public ExchangeAuthData getPassingData() {
        return getParcelableExtra(DATA);
    }

    public String getPassingToken() {
        return getStringExtra(FIREBASE_TOKEN);
    }
}
