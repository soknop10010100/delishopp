package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.InputEmailSignUpDataActivity;
import com.proapp.sompom.newui.ProfileSignUpActivity;

/**
 * Created by Veasna Chhom on 2/10/22.
 */
public class InputEmailSignUpDataIntent extends Intent {

    public static final String EMAIL = "EMAIL";

    public InputEmailSignUpDataIntent(Context packageContext, String email) {
        super(packageContext, InputEmailSignUpDataActivity.class);
        putExtra(EMAIL, email);
    }

    public InputEmailSignUpDataIntent(Intent o) {
        super(o);
    }

    public String getEmail() {
        return getStringExtra(EMAIL);
    }
}
