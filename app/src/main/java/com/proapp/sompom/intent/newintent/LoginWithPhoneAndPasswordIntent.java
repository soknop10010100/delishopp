package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.LoginWithPhoneAndPasswordActivity;

/**
 * Created by Veasna Chhom on 4/23/21.
 */
public class LoginWithPhoneAndPasswordIntent extends Intent {

    public LoginWithPhoneAndPasswordIntent(Context packageContext) {
        super(packageContext, LoginWithPhoneAndPasswordActivity.class);
    }
}
