package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.NotificationActivity;

/**
 * Created by Chhom Veasna on 6/27/2019.
 */
public class NotificationIntent extends Intent {

    public NotificationIntent(Context packageContext) {
        super(packageContext, NotificationActivity.class);
    }
}
