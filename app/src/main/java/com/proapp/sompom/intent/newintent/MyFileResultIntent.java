package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.utils.FileUtils;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by He Rotha on 2019-08-13.
 */
public class MyFileResultIntent {

    private String mUri;
    private String mFileName;
    private String mExtension;
    private long mFileSize;

    public MyFileResultIntent(Intent data, Context context) {
        try {
            mUri = data.getData().toString();
            mFileName = FileUtils.getDisplayFileName(context, data.getData());
            mExtension = FileUtils.getFileExtensionFromFilePath(mFileName);
            mFileSize = FileUtils.getFileSize(context, data.getData());
            /*
            Need to retrieve file extension in another way if it was not yet successfully retrieved
            previously.
             */
            if (TextUtils.isEmpty(mExtension)) {
                mExtension = FileUtils.getFileExtension(context, data.getData());
            }
//            Timber.i("mUri: " + mUri +
//                    ", mFileName: " + mFileName +
//                    ", mExtension: " + mExtension +
//                    ", mFileSize: " + mFileSize);
        } catch (Exception e) {
            Timber.e("MyFileResultIntent error: %s", e.getMessage());
        }
    }

    public List<Media> getFile() {
        ArrayList<Media> arrayList = new ArrayList<>();
        Media media = new Media();
        media.setType(MediaType.FILE);
        media.setUrl(mUri);
        media.setFileName(mFileName);
        media.setTitle(null);
        media.setSize(mFileSize);
        media.setFormat(mExtension);
        media.setFileStatusType(Media.FileStatusType.UPLOADING);
        arrayList.add(media);

        return arrayList;
    }
}
