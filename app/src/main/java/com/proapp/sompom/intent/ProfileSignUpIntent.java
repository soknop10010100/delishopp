package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.newui.ProfileSignUpActivity;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class ProfileSignUpIntent extends Intent {

    public static final String SIGN_UP_DATA = "SIGN_UP_DATA";

    public ProfileSignUpIntent(Context packageContext, ExchangeAuthData data) {
        super(packageContext, ProfileSignUpActivity.class);
        putExtra(SIGN_UP_DATA, data);
    }

    public ProfileSignUpIntent(Intent o) {
        super(o);
    }

    public ExchangeAuthData getSignUpData() {
        return getParcelableExtra(SIGN_UP_DATA);
    }
}
