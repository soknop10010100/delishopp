package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.concierge.ConciergeCoupon;
import com.proapp.sompom.newui.CouponActivity;

/**
 * Created by Chhom Veasna on 7/21/22.
 */
public class CouponIntent extends Intent {

    public static final String SELECTED_COUPON_ID = "SELECTED_COUPON_ID";
    public static final String SELECTED_COUPON = "SELECTED_COUPON";

    public CouponIntent(Context packageContext, String selectionCouponId) {
        super(packageContext, CouponActivity.class);
        putExtra(SELECTED_COUPON_ID, selectionCouponId);
    }

    public CouponIntent(Intent o) {
        super(o);
    }

    public String getSelectedCouponId() {
        return getStringExtra(SELECTED_COUPON_ID);
    }

    public ConciergeCoupon getSelectedCoupon() {
        return getParcelableExtra(SELECTED_COUPON);
    }
}
