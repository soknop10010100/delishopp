package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.newui.InputEmailLoginPasswordActivity;

/**
 * Created by Veasna Chhom on 2/10/22.
 */
public class InputEmailLoginPasswordIntent extends Intent {

    public static final String DATA = "DATA";

    public InputEmailLoginPasswordIntent(Context packageContext, ExchangeAuthData data) {
        super(packageContext, InputEmailLoginPasswordActivity.class);
        putExtra(DATA, data);
    }

    public InputEmailLoginPasswordIntent(Intent o) {
        super(o);
    }

    public ExchangeAuthData getPassingData() {
        return getParcelableExtra(DATA);
    }
}
