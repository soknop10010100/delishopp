package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.EmailSignUpActivity;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class EmailSignUpIntent extends Intent {

    public EmailSignUpIntent(Context packageContext) {
        super(packageContext, EmailSignUpActivity.class);
    }
}
