package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.newui.ConciergeCurrentLocationActivity;

/**
 * Created by Or Vitovongsak on 16/11/21.
 */
public class ConciergeCurrentLocationIntent extends Intent {

    public static final String USER_CURRENT_ADDRESS = "USER_CURRENT_ADDRESS";

    public ConciergeCurrentLocationIntent(Context context) {
        super(context, ConciergeCurrentLocationActivity.class);
    }

    public ConciergeCurrentLocationIntent(Intent intent) {
        super(intent);
    }

    public ConciergeUserAddress getUserCurrentAddress() {
        return getParcelableExtra(USER_CURRENT_ADDRESS);
    }
}
