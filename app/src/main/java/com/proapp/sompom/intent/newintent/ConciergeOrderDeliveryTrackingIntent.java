package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.newui.ConciergeOrderDeliveryTrackingActivity;

/**
 * Created by Or Vitovongsak on 16/12/21.
 */
public class ConciergeOrderDeliveryTrackingIntent extends Intent {

    public static final String ORDER = "ORDER";
    public static final String IS_ORDER_CANCELED = "IS_ORDER_CANCELED";
    public static final String ORDER_ID = "ORDER_ID";

    public ConciergeOrderDeliveryTrackingIntent(Context context, ConciergeOrder conciergeOrder) {
        super(context, ConciergeOrderDeliveryTrackingActivity.class);
        putExtra(ORDER, conciergeOrder);
    }

    public ConciergeOrderDeliveryTrackingIntent(Context context, String orderId) {
        super(context, ConciergeOrderDeliveryTrackingActivity.class);
        putExtra(ORDER_ID, orderId);
    }

    public ConciergeOrderDeliveryTrackingIntent(Intent intent) {
        super(intent);
    }

    public ConciergeOrder getOrder() {
        return getParcelableExtra(ORDER);
    }

    public boolean getIsOrderCanceled() {
        return getBooleanExtra(IS_ORDER_CANCELED, false);
    }

    public String getOrderId() {
        return getStringExtra(ORDER_ID);
    }
}
