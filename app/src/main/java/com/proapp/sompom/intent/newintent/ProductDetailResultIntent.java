package com.proapp.sompom.intent.newintent;

import android.content.Intent;

import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 6/6/18.
 */

public class ProductDetailResultIntent extends Intent {
    public ProductDetailResultIntent(Intent o) {
        super(o);
    }

    public ProductDetailResultIntent(Product product) {
        putExtra(SharedPrefUtils.PRODUCT, product);
    }

    public Product getProduct() {
        return getParcelableExtra(SharedPrefUtils.PRODUCT);
    }
}
