package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.emun.AuthType;
import com.proapp.sompom.newui.ValidatePhoneNumberActivity;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class ValidatePhoneNumberIntent extends Intent {

    public static final String AUTH_TYPE = "AUTH_TYPE";
    public static final String IS_IGNORE_FIREBASE_TOKEN_VALIDATION = "IS_IGNORE_FIREBASE_TOKEN_VALIDATION";

    public ValidatePhoneNumberIntent(Context packageContext, AuthType authType) {
        super(packageContext, ValidatePhoneNumberActivity.class);
        putExtra(AUTH_TYPE, authType.getValue());
    }

    public ValidatePhoneNumberIntent(Context packageContext, AuthType authType, boolean isIgnoreFirebaseTokenValidation) {
        super(packageContext, ValidatePhoneNumberActivity.class);
        putExtra(AUTH_TYPE, authType.getValue());
        putExtra(IS_IGNORE_FIREBASE_TOKEN_VALIDATION, isIgnoreFirebaseTokenValidation);
    }

    public ValidatePhoneNumberIntent(Intent o) {
        super(o);
    }

    public AuthType getAuthType() {
        return AuthType.fromValue(getStringExtra(AUTH_TYPE));
    }

    public boolean isIgnoreFirebaseTokenValidation() {
        return getBooleanExtra(IS_IGNORE_FIREBASE_TOKEN_VALIDATION, false);
    }
}
