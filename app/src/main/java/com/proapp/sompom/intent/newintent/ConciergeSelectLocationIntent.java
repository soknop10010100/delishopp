package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.newui.ConciergeSelectLocationActivity;

/**
 * Created by Or Vitovongsak on 25/11/21.
 */
public class ConciergeSelectLocationIntent extends Intent {

    public static final String ADDRESS_DATA = "ADDRESS_DATA";

    public ConciergeSelectLocationIntent(Context packageContext) {
        super(packageContext, ConciergeSelectLocationActivity.class);
    }

    public ConciergeSelectLocationIntent(Context packageContext, SearchAddressResult result) {
        super(packageContext, ConciergeSelectLocationActivity.class);
        putExtra(ADDRESS_DATA, result);
    }

    public ConciergeSelectLocationIntent(Intent data) {
        super(data);
    }

    public SearchAddressResult getSearchAddressResult() {
        return getParcelableExtra(ADDRESS_DATA);
    }
}
