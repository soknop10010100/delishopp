package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.LoginWithPhoneOrEmailActivity;

/**
 * Created by Veasna Chhom on 4/21/21.
 */
public class LoginWithPhoneOrEmailIntent extends Intent {

    public LoginWithPhoneOrEmailIntent(Context packageContext) {
        super(packageContext, LoginWithPhoneOrEmailActivity.class);
    }
}
