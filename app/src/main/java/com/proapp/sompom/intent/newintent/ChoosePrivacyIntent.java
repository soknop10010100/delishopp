package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.emun.PublishItem;
import com.proapp.sompom.newui.ChoosePrivacyActivity;

public class ChoosePrivacyIntent extends Intent {

    public static final String SELECTED_ID = "SELECTED_ID";
    public static final String DATA = "DATA";

    public ChoosePrivacyIntent(Context packageContext, int selectionId) {
        super(packageContext, ChoosePrivacyActivity.class);
        putExtra(SELECTED_ID, selectionId);
    }

    public ChoosePrivacyIntent(Intent o) {
        super(o);
    }

    public int getSelectionId() {
        return getIntExtra(SELECTED_ID, PublishItem.Follower.getId());
    }
}
