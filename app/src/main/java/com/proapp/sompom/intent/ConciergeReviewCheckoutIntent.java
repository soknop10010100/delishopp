package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.ConciergeReviewCheckoutActivity;

/**
 * Created by Veasna Chhom on 5/4/22.
 */

public class ConciergeReviewCheckoutIntent extends Intent {

    public static final String IS_OPEN_FROM_REORDER = "IS_OPEN_FROM_REORDER";
    public static final String SHOULD_RELOAD_DATA = "SHOULD_RELOAD_DATA";

    public ConciergeReviewCheckoutIntent(Context context) {
        super(context, ConciergeReviewCheckoutActivity.class);
    }

    public ConciergeReviewCheckoutIntent(Context context, boolean isOpenFromReOrder) {
        super(context, ConciergeReviewCheckoutActivity.class);
        putExtra(IS_OPEN_FROM_REORDER, isOpenFromReOrder);
    }

    public ConciergeReviewCheckoutIntent(Intent o) {
        super(o);
    }

    public boolean getIsOpenFromReorder() {
        return getBooleanExtra(IS_OPEN_FROM_REORDER, false);
    }
}
