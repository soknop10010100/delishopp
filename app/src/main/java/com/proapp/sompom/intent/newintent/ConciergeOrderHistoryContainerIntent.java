package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.ConciergeOrderHistoryContainerActivity;

/**
 * Created by Veasna Chhom on 4/25/22.
 */

public class ConciergeOrderHistoryContainerIntent extends Intent {

    public static final String IS_FROM_PROFILE_SCREEN = "IS_FROM_PROFILE_SCREEN";

    public ConciergeOrderHistoryContainerIntent(Context packageContext, boolean isFromProfileScreen) {
        super(packageContext, ConciergeOrderHistoryContainerActivity.class);
        putExtra(IS_FROM_PROFILE_SCREEN, isFromProfileScreen);
    }

    public ConciergeOrderHistoryContainerIntent(Intent o) {
        super(o);
    }

    public boolean isFromProfileScreen() {
        return getBooleanExtra(IS_FROM_PROFILE_SCREEN, false);
    }
}
