package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.TimelineDetailRedirectionType;
import com.proapp.sompom.model.notification.RedirectionNotificationData;
import com.proapp.sompom.model.notification.NotificationListAndRedirectionHelper;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.newui.TimelineDetailActivity;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 7/12/18.
 */

public class TimelineDetailIntent extends Intent {

    public static final String COMMENT_INFO = "COMMENT_INFO";
    public static final String REDIRECTION_TYPE = "REDIRECTION_TYPE";

    public TimelineDetailIntent(Intent o) {
        super(o);
    }

    public TimelineDetailIntent(Context packageContext,
                                WallStreetAdaptive adaptive,
                                int position,
                                TimelineDetailRedirectionType redirectionType) {
        super(packageContext, TimelineDetailActivity.class);
        putExtra(SharedPrefUtils.DATA, adaptive);
        putExtra(SharedPrefUtils.ID, position);
        putExtra(SharedPrefUtils.STATUS, adaptive.getId());
        putExtra(REDIRECTION_TYPE, redirectionType.getValue());
    }

    public TimelineDetailIntent(Context packageContext,
                                WallStreetAdaptive adaptive,
                                String postId,
                                int position,
                                TimelineDetailRedirectionType redirectionType) {
        super(packageContext, TimelineDetailActivity.class);
        putExtra(SharedPrefUtils.DATA, adaptive);
        putExtra(SharedPrefUtils.ID, position);
        //Post or content io will be used to for requesting comment list for a post that is text
        //or has only one media in post detail screen.
        putExtra(SharedPrefUtils.STATUS, postId);
        putExtra(REDIRECTION_TYPE, redirectionType.getValue());
    }

    public TimelineDetailIntent(Context packageContext, LifeStream lifeStream, TimelineDetailRedirectionType redirectionType) {
        super(packageContext, TimelineDetailActivity.class);
        putExtra(SharedPrefUtils.DATA, lifeStream);
        putExtra(SharedPrefUtils.STATUS, lifeStream.getId());
        putExtra(REDIRECTION_TYPE, redirectionType.getValue());
    }

    public TimelineDetailRedirectionType getRedirectionType() {
        return TimelineDetailRedirectionType.fromValue(getStringExtra(REDIRECTION_TYPE));
    }

    public LifeStream getTimeline() {
        return getParcelableExtra(SharedPrefUtils.DATA);
    }

    public TimelineDetailIntent(Context packageContext, String id) {
        super(packageContext, TimelineDetailActivity.class);
        putExtra(SharedPrefUtils.STATUS, id);
    }

    public TimelineDetailIntent(Context context, RedirectionNotificationData data) {
        super(context, TimelineDetailActivity.class);
        putExtra(NotificationListAndRedirectionHelper.NOTIFICATION_EXCHANGE_DATA, data);
    }

    public RedirectionNotificationData getExchangeData() {
        return getParcelableExtra(NotificationListAndRedirectionHelper.NOTIFICATION_EXCHANGE_DATA);
    }

    public int getPosition() {
        return getIntExtra(SharedPrefUtils.ID, 0);
    }

    public String getWallStreetId() {
        return getStringExtra(SharedPrefUtils.STATUS);
    }

    public WallStreetAdaptive getLifeStream() {
        return getParcelableExtra(SharedPrefUtils.DATA);
    }
}
