package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.ConciergeSubCategoryDetailActivity;

/**
 * Created by Chhom Veasna on 5/4/22.
 */
public class ConciergeSubCategoryDetailIntent extends Intent {

    public static final String ID = "ID";

    public ConciergeSubCategoryDetailIntent(Context packageContext, String id) {
        super(packageContext, ConciergeSubCategoryDetailActivity.class);
        putExtra(ID, id);
    }

    public ConciergeSubCategoryDetailIntent(Intent o) {
        super(o);
    }

    public String getId() {
        return getStringExtra(ID);
    }
}
