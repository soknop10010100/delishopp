package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.emun.CitySelectionType;
import com.proapp.sompom.newui.ConciergeMyAddressActivity;

/**
 * Created by Or Vitovongsak on 22/11/21.
 */
public class ConciergeMyAddressIntent extends Intent {

    public static final String IS_FROM_CHECKOUT_MODE = "IS_FROM_CHECKOUT_MODE";
    public static final String PREVIOUS_SELECTED_ADDRESS_ID = "PREVIOUS_SELECTED_ADDRESS_ID";
    public static final String ADDRESS_TYPE_TO_SHOW = "ADDRESS_TYPE_TO_SHOW";

    public ConciergeMyAddressIntent(Context packageContext) {
        super(packageContext, ConciergeMyAddressActivity.class);
    }

    public ConciergeMyAddressIntent(Intent o) {
        super(o);
    }

    public ConciergeMyAddressIntent(Context packageContext,
                                    boolean isFromCheckOut,
                                    CitySelectionType addressTypeToShow,
                                    String previousSelectedAddressId) {
        super(packageContext, ConciergeMyAddressActivity.class);
        putExtra(IS_FROM_CHECKOUT_MODE, isFromCheckOut);
        putExtra(ADDRESS_TYPE_TO_SHOW, addressTypeToShow);
        putExtra(PREVIOUS_SELECTED_ADDRESS_ID, previousSelectedAddressId);
    }

    public boolean isFromCheckout() {
        return getBooleanExtra(IS_FROM_CHECKOUT_MODE, false);
    }

    public CitySelectionType getAddressTypeToShow() {
        return (CitySelectionType) getSerializableExtra(ADDRESS_TYPE_TO_SHOW);
    }

    public String getPreviousSelectedAddressId() {
        return getStringExtra(PREVIOUS_SELECTED_ADDRESS_ID);
    }
}
