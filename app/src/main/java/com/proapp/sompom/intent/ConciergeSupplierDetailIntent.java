package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.concierge.ConciergeSupplier;
import com.proapp.sompom.newui.fragment.ConciergeSupplierDetailActivity;

/**
 * Created by Chhom Veasna on 5/4/22.
 */
public class ConciergeSupplierDetailIntent extends Intent {

    public static final String SUPPLIER = "SUPPLIER";

    public ConciergeSupplierDetailIntent(Context packageContext, ConciergeSupplier supplier) {
        super(packageContext, ConciergeSupplierDetailActivity.class);
        putExtra(SUPPLIER, supplier);
    }

    public ConciergeSupplierDetailIntent(Intent o) {
        super(o);
    }

    public ConciergeSupplier getSupplier() {
        return getParcelableExtra(SUPPLIER);
    }
}
