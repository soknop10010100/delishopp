package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.TelegramUserRedirectActivity;

public class TelegramUserRedirectIntent extends Intent {
    public final static String TELEGRAM_USER_ID = "TELEGRAM_USER_ID";

    public TelegramUserRedirectIntent(Intent o) {
        super(o);
    }

    public TelegramUserRedirectIntent(Context context, String telegramUserId) {
        super(context, TelegramUserRedirectActivity.class);
        putExtra(TELEGRAM_USER_ID, telegramUserId);
    }
}
