package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.PayWithCardActivity;

public class PayWithCardIntent extends Intent {

    public static final String CHECKOUT_URL = "CHECKOUT_URL";
    public static final String CONTINUE_SUCCESS_URL = "CONTINUE_SUCCESS_URL";
    public static final String BASKET_ID = "BASKET_ID";

    public PayWithCardIntent(Context packageContext, String checkOutUrl, String continueSuccessUrl) {
        super(packageContext, PayWithCardActivity.class);
        putExtra(CHECKOUT_URL, checkOutUrl);
        putExtra(CONTINUE_SUCCESS_URL, continueSuccessUrl);
    }

    public PayWithCardIntent(Context packageContext, String checkOutUrl, String continueSuccessUrl, String basketId) {
        super(packageContext, PayWithCardActivity.class);
        putExtra(CHECKOUT_URL, checkOutUrl);
        putExtra(CONTINUE_SUCCESS_URL, continueSuccessUrl);
        putExtra(BASKET_ID, basketId);
    }

    public PayWithCardIntent(Intent o) {
        super(o);
    }

    public String getCheckoutUrl() {
        return getStringExtra(CHECKOUT_URL);
    }

    public String getContinueSuccessUrl() {
        return getStringExtra(CONTINUE_SUCCESS_URL);
    }

    public String getBasketId() {
        return getStringExtra(BASKET_ID);
    }
}
