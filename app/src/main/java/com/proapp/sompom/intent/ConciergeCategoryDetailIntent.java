package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.ConciergeCategoryDetailActivity;
import com.proapp.sompom.newui.ConciergeSubCategoryDetailActivity;

/**
 * Created by Chhom Veasna on 5/4/22.
 */
public class ConciergeCategoryDetailIntent extends Intent {

    public static final String ID = "ID";

    public ConciergeCategoryDetailIntent(Context packageContext, String id) {
        super(packageContext, ConciergeCategoryDetailActivity.class);
        putExtra(ID, id);
    }

    public ConciergeCategoryDetailIntent(Intent o) {
        super(o);
    }

    public String getId() {
        return getStringExtra(ID);
    }
}
