package com.proapp.sompom.intent;

import android.content.Intent;

import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by nuonveyo
 * on 2/13/19.
 */

public class CreateTimelineIntentResult extends Intent {
    public CreateTimelineIntentResult(Intent o) {
        super(o);
    }

    public CreateTimelineIntentResult(LifeStream lifeStream) {
        putExtra(SharedPrefUtils.DATA, lifeStream);
    }

    public LifeStream getLifeStream() {
        return getParcelableExtra(SharedPrefUtils.DATA);
    }
}
