package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.newui.GroupDetailMediaContainerActivity;

/**
 * Created by Chhom Veasna on 7/24/2020.
 */
public class GroupDetailMediaContainerIntent extends Intent {

    public static final String GROUP_DETAIL = "GROUP_DETAIL";
    public static final String INDEX = "INDEX";

    public GroupDetailMediaContainerIntent(Context packageContext, GroupDetail groupDetail, int index) {
        super(packageContext, GroupDetailMediaContainerActivity.class);
        putExtra(GROUP_DETAIL, groupDetail);
        putExtra(INDEX, index);
    }

    public GroupDetailMediaContainerIntent(Intent o) {
        super(o);
    }

    public GroupDetail getGroupDetail() {
        return getParcelableExtra(GROUP_DETAIL);
    }

    public int getIndex() {
        return getIntExtra(INDEX, 0);
    }
}
