package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.proapp.sompom.model.emun.OpenChatScreenType;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.ChatActivity;
import com.proapp.sompom.utils.ConversationUtil;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 5/4/18.
 */

public class ChatIntent extends Intent {

    public static final String CHAT = "CHAT";
    public static final String IS_FROM_NOTIFICATION = "IS_FROM_NOTIFICATION";
    public static final String OPEN_CHAT_TYPE = "OPEN_CHAT_TYPE";
    public static final String SEARCH_KEYWORD = "SEARCH_KEYWORD";
    public static final String IS_GROUP = "IS_GROUP";

    private User mRecipient;
    private Product mProduct;
    private boolean mIsInitCall = false;
    private Conversation mConversation;

    public ChatIntent(Intent o) {
        super(o);
    }

    public ChatIntent(Context context, User user) {
        super(context, ChatActivity.class);
        putExtra(SharedPrefUtils.USER_ID, user);
    }

    public ChatIntent(Context context, Product product) {
        super(context, ChatActivity.class);
        putExtra(SharedPrefUtils.PRODUCT, product);
    }

    public ChatIntent(Context context,
                      Conversation conversation,
                      Chat selectedChat,
                      String searchKeyWord,
                      OpenChatScreenType chatScreenType) {
        super(context, ChatActivity.class);
        putExtra(CHAT, selectedChat);
        putExtra(OPEN_CHAT_TYPE, chatScreenType.getValue());
        putExtra(SEARCH_KEYWORD, searchKeyWord);
        putExtra(SharedPrefUtils.DATA, conversation);
        putExtra(IS_GROUP, conversation.isGroup());
    }

    public OpenChatScreenType getOpenChatScreenType() {
        return OpenChatScreenType.getFromValue(getIntExtra(OPEN_CHAT_TYPE,
                OpenChatScreenType.DEFAULT.getValue()));
    }

    public ChatIntent(Context context, Product product, User user) {
        super(context, ChatActivity.class);
        putExtra(SharedPrefUtils.PRODUCT, product);
        putExtra(SharedPrefUtils.USER_ID, user);
    }

    public ChatIntent(Context context, Conversation conversation) {
        super(context, ChatActivity.class);
        if (conversation != null) {
            if (conversation.getProduct() != null && conversation.getProduct().getUser() == null) {
                if (TextUtils.equals(conversation.getSeller(), SharedPrefUtils.getUserId(context))) {
                    conversation.getProduct().setUser(SharedPrefUtils.getUser(context));
                } else {
                    conversation.getProduct().setUser(conversation.getOneToOneRecipient(context));
                }
            }
            putExtra(SharedPrefUtils.DATA, conversation);
            putExtra(IS_GROUP, conversation.isGroup());
        }
    }

    public void setIsFromNotification(boolean isFromNotifications) {
        putExtra(IS_FROM_NOTIFICATION, isFromNotifications);
    }

    public String getSearchKeyword() {
        return getStringExtra(SEARCH_KEYWORD);
    }

    public boolean isFromNotification() {
        return getBooleanExtra(IS_FROM_NOTIFICATION, false);
    }

    public ChatIntent(Context context, Conversation conversation, User sender) {
        super(context, ChatActivity.class);
        putExtra(SharedPrefUtils.USER_ID, sender);
        if (conversation != null) {
            if (conversation.getProduct() != null && conversation.getProduct().getUser() == null) {
                if (TextUtils.equals(conversation.getSeller(), SharedPrefUtils.getUserId(context))) {
                    conversation.getProduct().setUser(SharedPrefUtils.getUser(context));
                } else {
                    conversation.getProduct().setUser(conversation.getOneToOneRecipient(context));
                }
            }
            putExtra(SharedPrefUtils.DATA, conversation);
            putExtra(IS_GROUP, conversation.isGroup());
        }
    }

    public void init(Context context) {
        mConversation = getParcelableExtra(SharedPrefUtils.DATA);
        if (mConversation != null) {
            mProduct = mConversation.getProduct();
            mRecipient = mConversation.getOneToOneRecipient(context);
        } else {
            mProduct = getParcelableExtra(SharedPrefUtils.PRODUCT);
            mRecipient = getParcelableExtra(SharedPrefUtils.USER_ID);

            if (mRecipient == null && mProduct != null) {
                mRecipient = mProduct.getUser();
            }
        }

        mIsInitCall = true;
    }

    public boolean isGroup() {
        return getBooleanExtra(IS_GROUP, false);
    }

    public Conversation getConversation() {
        return mConversation;
    }

    public User getRecipient() {
        if (!mIsInitCall) {
            throw new InitException();
        }
        return mRecipient;
    }

    public Chat getChat() {
        return getParcelableExtra(CHAT);
    }

    public Product getProduct() {
        if (!mIsInitCall) {
            throw new InitException();
        }
        return mProduct;
    }

    public String getConversationId(Context context) {
        if (!mIsInitCall) {
            throw new InitException();
        }

        if (mConversation != null && mConversation.isGroup()) {
            return mConversation.getGroupId();
        } else if (mRecipient != null) {
            return ConversationUtil.getConversationId(mRecipient.getId(),
                    SharedPrefUtils.getUserId(context));
        } else {
            return null;
        }
    }

    public static ChatIntent newSupportConversationInstance(Context context, Conversation conversation) {
        return new ChatIntent(context, conversation);
    }

    public static class InitException extends RuntimeException {
        public InitException() {
            super("need to call method init first");
        }
    }
}
