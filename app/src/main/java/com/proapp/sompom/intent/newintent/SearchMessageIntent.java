package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.SearchMessageActivity;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class SearchMessageIntent extends Intent {

    public static final String QUERY = "QUERY";
    public static final String SEARCH_TYPE = "SEARCH_TYPE";
    public static final String SUPPLIER_ID = "SUPPLIER_ID";

    public enum SearchType {
        Normal,
        Product
    }

    public SearchMessageIntent(Context context) {
        super(context, SearchMessageActivity.class);
    }

    public SearchMessageIntent(Context context, String query) {
        super(context, SearchMessageActivity.class);
        putExtra(QUERY, query);
    }

    public SearchMessageIntent(Intent o) {
        super(o);
    }

    public void setSearchType(SearchType searchType) {
        putExtra(SEARCH_TYPE, searchType);
    }

    public void setSupplierId(String supplierId) {
        putExtra(SUPPLIER_ID, supplierId);
    }

    public SearchType getSearchType() {
        return (SearchType) getSerializableExtra(SEARCH_TYPE);
    }

    public String getQuery() {
        return getStringExtra(QUERY);
    }

    public String getSupplierId() {
        return getStringExtra(SUPPLIER_ID);
    }

    public static SearchMessageIntent newSearchSupplierProductInstance(Context context, String supplierId) {
        SearchMessageIntent searchMessageIntent = new SearchMessageIntent(context);
        searchMessageIntent.setSupplierId(supplierId);
        searchMessageIntent.setSearchType(SearchType.Product);

        return searchMessageIntent;
    }
}
