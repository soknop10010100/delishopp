package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.ConciergeShopDetailActivity;

public class ConciergeShopDetailIntent extends Intent {

    public static final String ID = "id";
    public static final String IS_DISCOUNT_ONLY = "IS_DISCOUNT_ONLY";

    public ConciergeShopDetailIntent(Context context, String id) {
        super(context, ConciergeShopDetailActivity.class);
        putExtra(ID, id);
    }

    public ConciergeShopDetailIntent(Context context, String id, boolean isDiscountOnly) {
        super(context, ConciergeShopDetailActivity.class);
        putExtra(ID, id);
        putExtra(IS_DISCOUNT_ONLY, isDiscountOnly);
    }

    public ConciergeShopDetailIntent(Intent o) {
        super(o);
    }

    public String getId() {
        return getStringExtra(ID);
    }

    public boolean getIsDiscountOnly() {
        return getBooleanExtra(IS_DISCOUNT_ONLY, false);
    }
}
