package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.newui.SelectAddressActivity;

/**
 * Created by nuonveyo on 5/10/18.
 */

public class SelectAddressIntent extends Intent {

    private static final String DATA = "DATA";

    public SelectAddressIntent(Context context, SearchAddressResult searchAddressResult) {
        super(context, SelectAddressActivity.class);
        putExtra(DATA, searchAddressResult);
    }

    public SelectAddressIntent(Intent data) {
        super(data);
    }

    public SearchAddressResult getSearchAddressResult() {
        return getParcelableExtra(DATA);
    }
}
