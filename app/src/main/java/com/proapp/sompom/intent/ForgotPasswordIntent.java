package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.ForgotPasswordActivity;

/**
 * Created by Veasna Chhom on 5/17/22.
 */
public class ForgotPasswordIntent extends Intent {

    public ForgotPasswordIntent(Context packageContext) {
        super(packageContext, ForgotPasswordActivity.class);
    }
}
