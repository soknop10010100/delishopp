package com.proapp.sompom.intent.newintent;

import android.content.Intent;

import com.desmond.squarecamera.intent.CameraResultIntent;
import com.desmond.squarecamera.model.MediaFile;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.utils.MediaUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 6/12/18.
 */
public class MyCameraResultIntent extends CameraResultIntent {

    public MyCameraResultIntent(Intent o) {
        super(o);
    }

    public ArrayList<Media> getMedia() {
        ArrayList<Media> arrayList = new ArrayList<>();

        List<MediaFile> list = getMediaFiles();

        for (MediaFile mediaFile : list) {
            Media media = new Media();
            media.setTitle(null);
            media.setUrl(mediaFile.getPath());
            media.setWidth(mediaFile.getWidth());
            media.setHeight(mediaFile.getHeight());
            media.setIndex(mediaFile.getPosition());
            if (mediaFile.getType() == com.desmond.squarecamera.model.MediaType.VIDEO) {
                media.setType(MediaType.VIDEO);
                media.setDuration(MediaUtil.getSecondFromMili(mediaFile.getVideoDuration()));
            } else if (mediaFile.getType() == com.desmond.squarecamera.model.MediaType.GIF) {
                media.setType(MediaType.GIF);
            } else {
                media.setType(MediaType.IMAGE);
            }

            arrayList.add(media);
        }

        return arrayList;
    }
}
