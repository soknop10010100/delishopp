package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.concierge.ConciergeComboItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.newui.ConciergeProductDetailActivity;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 13/9/21.
 */

public class ConciergeProductDetailIntent extends Intent {

    public static final String MENU_ITEM = "menuItem";
    public static final String EDIT_MENU_ITEM = "editMenuItem";
    public static final String MENU_ITEM_ID = "menuItemId";
    public static final String IS_OPEN_FROM_COMBO = "isOpenFromCombo";
    public static final String COMBO_ITEM = "comboItem";
    public static final String POSITION_IN_BASKET = "POSITION_IN_BASKET";

    public ConciergeProductDetailIntent(Context context, ConciergeMenuItem item, int positionInBasket) {
        super(context, ConciergeProductDetailActivity.class);
        Timber.i("ConciergeProductDetailIntent: " + item.getProductCount());
        putExtra(EDIT_MENU_ITEM, item);
        putExtra(POSITION_IN_BASKET, positionInBasket);
    }

    public ConciergeProductDetailIntent(Context context,
                                        ConciergeComboItem comboItem,
                                        boolean isOpenFromCombo) {
        super(context, ConciergeProductDetailActivity.class);
        putExtra(COMBO_ITEM, comboItem);
        putExtra(IS_OPEN_FROM_COMBO, isOpenFromCombo);
    }

    public ConciergeProductDetailIntent(Intent o) {
        super(o);
    }

    public ConciergeMenuItem getEditOderItem() {
        return getParcelableExtra(EDIT_MENU_ITEM);
    }

    public String getMenuItemId() {
        return getStringExtra(MENU_ITEM_ID);
    }

    public boolean getIsOpenFromCombo() {
        return getBooleanExtra(IS_OPEN_FROM_COMBO, false);
    }

    public ConciergeComboItem getComboItem() {
        return getParcelableExtra(COMBO_ITEM);
    }

    public int getPositionInBasket() {
        return getIntExtra(POSITION_IN_BASKET, -1);
    }
}
