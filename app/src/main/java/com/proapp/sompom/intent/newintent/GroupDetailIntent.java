package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.newui.GroupDetailActivity;
import com.proapp.sompom.services.datamanager.ChatDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by Or Vitovongsak on 4/28/20.
 */
public class GroupDetailIntent extends Intent {
    private Conversation mConversation;

    public GroupDetailIntent(Intent intent) {
        super(intent);
    }

    public GroupDetailIntent(Context context,
                             Conversation conversation) {
        super(context, GroupDetailActivity.class);
        putExtra(SharedPrefUtils.CONVERSATION, conversation);
    }
}
