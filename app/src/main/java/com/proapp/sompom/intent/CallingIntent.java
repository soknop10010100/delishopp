package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.chat.call.IncomingCallDataHolder;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.intent.newintent.CallIntent;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.CallingActivity;

/**
 * Created by nuonveyo on 6/21/18.
 */

public class CallingIntent extends CallIntent {

    public CallingIntent(Context context) {
        super(context, CallingActivity.class);
    }

    public CallingIntent(Context context, IncomingCallDataHolder incomingCallDataHolder) {
        super(context, CallingActivity.class, incomingCallDataHolder);
    }

    public CallingIntent(Intent data) {
        super(data);
    }

    public CallingIntent(Context context,
                         AbsCallService.CallType callType,
                         User callTo,
                         Product product) {
        super(context, CallingActivity.class, callTo, product);
        putExtra(CallHelper.TYPE, callType.getValue());
    }

    public CallingIntent(Context context,
                         AbsCallService.CallType callType,
                         User callTo,
                         int startFromType) {
        super(context, CallingActivity.class, callTo, startFromType);
        putExtra(CallHelper.TYPE, callType.getValue());
    }

    public CallingIntent(Context context,
                         AbsCallService.CallType callType,
                         User callTo,
                         Conversation conversation,
                         int startFromType) {
        super(context, CallingActivity.class, callTo, conversation, startFromType);
        putExtra(CallHelper.TYPE, callType.getValue());
    }

    public CallingIntent(Context context, AbsCallService.CallActionType callActionType) {
        super(context, CallingActivity.class);
        putExtra(CALL_ACTION_TYPE, callActionType.getValue());
    }

    public CallingIntent(Context context,
                         AbsCallService.CallType callType,
                         AbsCallService.CallActionType callActionType,
                         Conversation conversation,
                         int startFromType) {
        super(context, CallingActivity.class, null, conversation, startFromType);
        putExtra(CallHelper.TYPE, callType.getValue());
        putExtra(CALL_ACTION_TYPE, callActionType.getValue());
    }

    public static CallingIntent getJoinGroupCallIntent(Context context,
                                                       AbsCallService.CallType callType,
                                                       Conversation conversation,
                                                       int startFromType) {
        return new CallingIntent(context,
                callType,
                AbsCallService.CallActionType.JOINED,
                conversation,
                startFromType);
    }

    public static CallingIntent getCallNotificationActionIntent(Context context,
                                                                AbsCallService.CallNotificationActionType actionType,
                                                                String groupId) {
        CallingIntent callingIntent = new CallingIntent(context);
        callingIntent.putExtra(CALL_NOTIFICATION_ACTION_TYPE, actionType.getAction());
        callingIntent.putExtra(GROUP_ID, groupId);

        return callingIntent;
    }

    public String getGroupId() {
        return getStringExtra(GROUP_ID);
    }

    public AbsCallService.CallNotificationActionType getNotificationActionType() {
        return AbsCallService.CallNotificationActionType.getFromValue(getIntExtra(CALL_NOTIFICATION_ACTION_TYPE,
                -1));
    }

    public static CallingIntent getResizeCallScreenToFullIntent(Context context) {
        CallingIntent callingIntent = new CallingIntent(context);
        callingIntent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        callingIntent.putExtra(NEED_TO_RESIZE_SCREEN_BACK, true);

        return callingIntent;
    }
}
