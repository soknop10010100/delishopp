package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.WalletHistoryActivity;

/**
 * Created by Chhom Veasna on 6/27/2019.
 */
public class WalletHistoryIntent extends Intent {

    public WalletHistoryIntent(Context packageContext) {
        super(packageContext, WalletHistoryActivity.class);
    }
}
