package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.newui.HomeActivity;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by He Rotha on 6/21/18.
 */
public class HomeIntent extends Intent {

    public HomeIntent(Context packageContext) {
        this(packageContext, Redirection.None);
    }

    public HomeIntent(Intent o) {
        super(o);
    }

    public HomeIntent(Context packageContext, Redirection redirection) {
        super(packageContext, HomeActivity.class);
        putExtra(SharedPrefUtils.ID, redirection);
    }

    public Redirection getRedirection() {
        return (Redirection) getSerializableExtra(SharedPrefUtils.ID);
    }

    public Chat getChat() {
        return getParcelableExtra(SharedPrefUtils.DATA);
    }

    public enum Redirection {
        None, Conversation, Concierge
    }
}
