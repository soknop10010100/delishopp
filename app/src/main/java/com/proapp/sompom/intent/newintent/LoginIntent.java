package com.proapp.sompom.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.LoginActivity;

/**
 * Created by He Rotha on 6/4/18.
 */
public class LoginIntent extends Intent {
    public LoginIntent(Context packageContext) {
        super(packageContext, LoginActivity.class);
    }
}
