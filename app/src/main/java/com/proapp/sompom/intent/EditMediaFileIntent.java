package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.newui.EditMediaFileActivity;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 5/7/18.
 */

public class EditMediaFileIntent extends Intent {

    public EditMediaFileIntent(Intent o) {
        super(o);
    }

    public EditMediaFileIntent(LifeStream lifeStream) {
        putExtra(SharedPrefUtils.DATA, lifeStream);
    }

    public EditMediaFileIntent(Context context, LifeStream lifeStream, int position, boolean isEdit) {
        super(context, EditMediaFileActivity.class);
        putExtra(SharedPrefUtils.DATA, lifeStream);
        putExtra(SharedPrefUtils.USER_ID, position);
        putExtra(SharedPrefUtils.STATUS, isEdit);
    }

    public int getPosition() {
        return getIntExtra(SharedPrefUtils.USER_ID, 0);
    }

    public boolean isEdit() {
        return getBooleanExtra(SharedPrefUtils.STATUS, false);
    }

    public LifeStream getLifeStream() {
        return getParcelableExtra(SharedPrefUtils.DATA);
    }

}
