package com.proapp.sompom.intent;

import android.content.Context;
import android.content.Intent;

import com.proapp.sompom.newui.LoginWithPhoneOnlyActivity;

/**
 * Created by Veasna Chhom on 4/21/21.
 */
public class LoginWithPhoneOnlyIntent extends Intent {

    public LoginWithPhoneOnlyIntent(Context packageContext) {
        super(packageContext, LoginWithPhoneOnlyActivity.class);
    }
}
