package com.proapp.sompom.intent.newintent;

import android.content.Intent;

import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 6/21/18.
 */

public class EditProfileIntentResult extends Intent {
    public EditProfileIntentResult(boolean isChangeActivateStore) {
        putExtra(SharedPrefUtils.STATUS, isChangeActivateStore);
    }

    public EditProfileIntentResult(Intent data) {
        super(data);
    }

    public boolean isChangeActivateStore() {
        return getBooleanExtra(SharedPrefUtils.STATUS, false);
    }
}
