package com.proapp.sompom.observer;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 5/26/2020.
 */
public class AppLifecycleObserver implements LifecycleObserver {

    private AppLifecycleObserverCallback mListener;

    public AppLifecycleObserver(AppLifecycleObserverCallback listener) {
        mListener = listener;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onEnterForeground() {
        Timber.i("onEnterForeground");
        if (mListener != null) {
            mListener.onEnterForeground();
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onEnterBackground() {
        Timber.i("onEnterBackground");
        if (mListener != null) {
            mListener.onEnterBackground();
        }
    }

    public interface AppLifecycleObserverCallback {
        void onEnterBackground();

        void onEnterForeground();
    }
}
