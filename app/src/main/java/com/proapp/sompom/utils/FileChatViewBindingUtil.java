package com.proapp.sompom.utils;

import android.app.Activity;
import androidx.databinding.BindingAdapter;
import android.text.TextUtils;

import com.resourcemanager.helper.FontHelper;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.widget.chat.ChatMediaFileLayout;

/**
 * Created by He Rotha on 2019-08-16.
 */
public class FileChatViewBindingUtil {

    private FileChatViewBindingUtil() {
    }

    @BindingAdapter(value = {"filename"})
    public static void filename(ChatMediaFileLayout view, String fileName) {
        view.setFile(fileName);
        view.getFileName();


    }


    @BindingAdapter(value = {"setChatFile", "setActivity"})
    public static void setChatFile(ChatMediaFileLayout layout,
                                   Chat chat,
                                   Activity activity) {
        layout.setActivity(activity);
        boolean isMe = TextUtils.equals(chat.getSenderId(), SharedPrefUtils.getUserId(activity));

        layout.setChat(chat, isMe);
        layout.getBinding().textViewFile.setText(layout.getFile());
        layout.getBinding().textViewFile.setTypeface(FontHelper.getBoldFontStyle(layout.getContext()));
        layout.checkExtensionFile();
    }
}
