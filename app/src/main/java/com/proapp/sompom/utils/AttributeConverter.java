package com.proapp.sompom.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * Created by He Rotha on 10/19/18.
 */
public final class AttributeConverter {
    private AttributeConverter() {
    }

    public static int convertAttrToColor(Context context, int attr) {
        return com.resourcemanager.utils.AttributeConverter.convertAttrToColor(context, attr);
    }

    public static Drawable convertAttrToDrawable(Context context, int attr) {
        return com.resourcemanager.utils.AttributeConverter.convertAttrToDrawable(context, attr);
    }

    public static Drawable applyAttrColorToDrawable(Context context, int drawableID, int attr) {
        return com.resourcemanager.utils.AttributeConverter.applyAttrColorToDrawable(context, drawableID, attr);
    }
}
