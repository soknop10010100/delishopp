package com.proapp.sompom.utils;

import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.ShareAds;
import com.proapp.sompom.model.SharedProduct;
import com.proapp.sompom.model.SharedTimeline;
import com.proapp.sompom.model.emun.TimelinePostItem;
import com.proapp.sompom.model.result.Product;

/**
 * Created by nuonveyo on 8/8/18.
 */

public final class TimelinePostItemUtil {
    private TimelinePostItemUtil() {
    }

    public static LifeStream getLifeStream(Adaptive adaptive) {
        LifeStream lifeStream = null;
        if (adaptive instanceof LifeStream || adaptive instanceof SharedTimeline) {
            if (adaptive instanceof LifeStream) {
                lifeStream = (LifeStream) adaptive;
            } else {
                lifeStream = ((SharedTimeline) adaptive).getLifeStream();
            }
        }
        return lifeStream;
    }

    public static Product getProduct(Adaptive adaptive) {
        Product product = null;
        if (adaptive instanceof Product || adaptive instanceof SharedProduct) {
            if (adaptive instanceof Product) {
                product = (Product) adaptive;
            } else {
                product = ((SharedProduct) adaptive).getProduct();
            }
        }
        return product;
    }

    public static TimelinePostItem getPostItem(Adaptive adaptive) {
        TimelinePostItem timelinePostItem;
        if (adaptive instanceof Product || adaptive instanceof SharedProduct) {
            timelinePostItem = TimelinePostItem.ProductItem;
        } else {
            timelinePostItem = TimelinePostItem.TimelineItem;
        }
        return timelinePostItem;
    }

    public static Adaptive getAdaptive(Adaptive adaptive) {
        if (adaptive instanceof SharedTimeline) {
            adaptive = ((SharedTimeline) adaptive).getLifeStream();
        } else if (adaptive instanceof SharedProduct) {
            adaptive = ((SharedProduct) adaptive).getProduct();
        } else if (adaptive instanceof ShareAds) {
            adaptive = ((ShareAds) adaptive).getLifeStream();
        }
        return adaptive;
    }
}
