package com.proapp.sompom.utils;

import android.text.TextUtils;

import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;

import timber.log.Timber;

/**
 * Created by He Rotha on 11/13/18.
 */
public final class ConversationUtil {

    private static final String ONE_TO_ONE_CONVERSATION_SEPARATOR = "_";

    private ConversationUtil() {
    }

    public static String getConversationId(String first,
                                           String second) {

        //format [userId_userId] order Id by alphabet
        int value = first.compareTo(second);
        String newText;
        if (value < 0) {
            newText = first + ONE_TO_ONE_CONVERSATION_SEPARATOR + second;
        } else {
            newText = second + ONE_TO_ONE_CONVERSATION_SEPARATOR + first;
        }
        return newText;

    }

    public static String getProductConversationId(String productId,
                                                  String first,
                                                  String second) {


        //format productID_[userId_userId] order Id by alphabet
        return productId + ONE_TO_ONE_CONVERSATION_SEPARATOR + getConversationId(first, second);

    }

    public static boolean isOneToOneConversationOfUser(String conversationId, String userId) {
        if (!TextUtils.isEmpty(conversationId)) {
            String[] split = conversationId.split(ONE_TO_ONE_CONVERSATION_SEPARATOR);
            for (String userId2 : split) {
                if (TextUtils.equals(userId2, userId)) {
                    return true;
                }
            }
        }

        return false;
    }

    private static String getChatLog(Chat chat) {
        if (chat != null) {
            return "Chat Id: " + chat.getId() + ", content: " + chat.getContent() + ", date: " + chat.getDate();
        }

        return null;
    }

    public static void logConversation(Conversation conversation, String tag) {
        Timber.i(tag + ": " + conversation.getId() + ", date: " + conversation.getDate() + ", Last message: " + getChatLog(conversation.getLastMessage()));
    }
}
