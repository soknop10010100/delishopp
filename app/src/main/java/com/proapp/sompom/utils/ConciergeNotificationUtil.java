package com.proapp.sompom.utils;

import static com.proapp.sompom.utils.NotificationUtils.checkToCreateNotificationChannel;
import static com.proapp.sompom.utils.NotificationUtils.getNotificationChannelId;
import static com.proapp.sompom.utils.NotificationUtils.getNotificationSound;
import static com.proapp.sompom.utils.NotificationUtils.getPriority;
import static com.proapp.sompom.utils.NotificationUtils.getVibration;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ColorResourceHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.intent.ConciergeOrderHistoryDetailIntent;
import com.proapp.sompom.intent.newintent.ConciergeOrderDeliveryTrackingIntent;
import com.proapp.sompom.intent.newintent.EditProfileIntent;
import com.proapp.sompom.intent.newintent.HomeIntent;
import com.proapp.sompom.model.NotificationChannelSetting;
import com.proapp.sompom.model.OrderStatus;
import com.proapp.sompom.model.ReceivePushNotificationModel;
import com.proapp.sompom.model.WalletUpdateReceivePushNotificationModel;
import com.proapp.sompom.model.emun.ConciergeOrderStatus;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.newui.ResumeApplicationReceiverActivity;

/**
 * Created by Or Vitovongsak on 15/11/21.
 */
public class ConciergeNotificationUtil {

    public static final String WALLET = "WALLET";

    private static final int SHOP_ENABLE_STATUS_CHANGE_NOTIFICATION_ID = 0x2019;
    private static final int EXPRESS_SLOT_FREE_NOTIFICATION_ID = 0x2020;
    private static final int WALLET_UPDATE = 0x2021;

    public static void testPushExpressNotification(Context context, boolean isExpress) {
        OrderStatus orderStatus;
        String data;
        if (isExpress) {
            data = "{\n" +
                    "      \"express\": true,\n" +
                    "      \"type\": \"orderDelivering\",\n" +
                    "      \"order\": {\n" +
                    "        \"number\": 49020,\n" +
                    "        \"_id\": 49020,\n" +
                    "        \"title\": \"Delishop\",\n" +
                    "        \"status\": \"DELIVERING\"\n" +
                    "      }\n" +
                    "    }";
        } else {
            data = "{\n" +
                    "      \"express\": false,\n" +
                    "      \"type\": \"orderDelivering\",\n" +
                    "      \"order\": {\n" +
                    "        \"number\": 49021,\n" +
                    "        \"_id\": 49021,\n" +
                    "        \"title\": \"Delishop\",\n" +
                    "        \"status\": \"DELIVERING\"\n" +
                    "      }\n" +
                    "    }";
        }
        orderStatus = new Gson().fromJson(data, OrderStatus.class);

        ConciergeNotificationUtil.pushConciergeOrderStatusNotification(context,
                orderStatus,
                NotificationChannelSetting.OrderStatusUpdate,
                false);
    }

    public static void pushWalletUpdateNotification(Context context,
                                                    WalletUpdateReceivePushNotificationModel data,
                                                    NotificationChannelSetting channelSetting,
                                                    boolean enableBigTextStyle) {
        checkToCreateNotificationChannel(context, channelSetting);
        String title = data.getTitle();
        String content = data.getDescription(context);

        EditProfileIntent editProfileIntent = new EditProfileIntent(context);
        editProfileIntent.putExtra(WALLET, data.getBalance());
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                WALLET_UPDATE,
                editProfileIntent,
                getPendingIntentFlag());

        pushNotificationInternally(context,
                WALLET_UPDATE,
                title,
                content,
                pendingIntent,
                channelSetting,
                enableBigTextStyle);
    }

    public static void pushConciergeOrderStatusNotification(Context context,
                                                            OrderStatus orderStatus,
                                                            NotificationChannelSetting channelSetting,
                                                            boolean enableBigTextStyle) {
        checkToCreateNotificationChannel(context, channelSetting);

        ConciergeOrder order = ConciergeHelper.fromNotificationOrder(context, orderStatus.getOrder());
        Intent intent;
        if (ConciergeHelper.checkShouldShowReOrderButton(orderStatus.getOrder().getOrderStatus())) {
            intent = new ConciergeOrderHistoryDetailIntent(context,
                    order.getId(),
                    order.getOrderNumber());
        } else {
            intent = new ConciergeOrderDeliveryTrackingIntent(context, order.getId());
        }

        int notificationId = Integer.parseInt(order.getOrderNumber());
        String title = getOrderStatusNotificationTitle(context, order.getOrderNumber());
        String content = orderStatus.getOrder().getDescription(context);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                notificationId,
                intent,
                getPendingIntentFlag());

        pushNotificationInternally(context,
                notificationId,
                title,
                content,
                pendingIntent,
                channelSetting,
                enableBigTextStyle);
    }

    public static void pushExpressSlotFreeNotification(Context context,
                                                       ReceivePushNotificationModel receivePushNotificationModel,
                                                       NotificationChannelSetting channelSetting,
                                                       boolean enableBigTextStyle) {
        pushGeneralNotification(context, receivePushNotificationModel, channelSetting, enableBigTextStyle);
    }

    private static void pushNotificationInternally(Context context,
                                                   int notificationId,
                                                   String title,
                                                   String content,
                                                   PendingIntent pendingIntent,
                                                   NotificationChannelSetting channelSetting,
                                                   boolean enableBigTextStyle) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,
                getNotificationChannelId(context, channelSetting))
                .setSmallIcon(NotificationUtils.getNotificationIcon())
                .setColor(ColorResourceHelper.getAccentColor(context))
                .setContentTitle(title)
                .setContentInfo(content)
                .setContentText(content)
                .setPriority(getPriority(context, channelSetting))
                .setContentIntent(pendingIntent)
                .setVibrate(getVibration(context, channelSetting))
                .setSound(getNotificationSound(context, channelSetting))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setAutoCancel(true);

        if (enableBigTextStyle) {
            builder.setStyle(new NotificationCompat.BigTextStyle()
                    .bigText(content)
                    .setBigContentTitle(title));
        }

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(notificationId, builder.build());
    }

    public static void pushConciergeShopEnableStatusChangedNotification(Context context,
                                                                        String title,
                                                                        String content,
                                                                        NotificationChannelSetting channelSetting,
                                                                        boolean enableBigTextStyle) {
        checkToCreateNotificationChannel(context, channelSetting);

        HomeIntent homeIntent = new HomeIntent(context, HomeIntent.Redirection.Concierge);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                SHOP_ENABLE_STATUS_CHANGE_NOTIFICATION_ID,
                homeIntent,
                getPendingIntentFlag());

        pushNotificationInternally(context,
                SHOP_ENABLE_STATUS_CHANGE_NOTIFICATION_ID,
                title,
                content,
                pendingIntent,
                channelSetting,
                enableBigTextStyle);
    }

    public static String getOrderStatusNotificationTitle(Context context, String orderNumber) {
        return context.getString(R.string.shop_notification_order_number_title) + " #" + orderNumber;
    }

    public static String getOrderNotificationMessageFromStatus(Context context, ConciergeOrderStatus orderStatus) {

        String toReturn = "";

        if (orderStatus == ConciergeOrderStatus.PROCESSING) {
            return context.getString(R.string.shop_notification_status_processing);
        } else if (orderStatus == ConciergeOrderStatus.DELIVERING) {
            return context.getString(R.string.shop_notification_status_delivering);
        } else if (orderStatus == ConciergeOrderStatus.ARRIVED) {
            return context.getString(R.string.shop_notification_status_arrived);
        } else if (orderStatus == ConciergeOrderStatus.DELIVERED) {
            return context.getString(R.string.shop_notification_status_delivered);
        }

        return toReturn;
    }

    public static void pushGeneralNotification(Context context,
                                               ReceivePushNotificationModel receivePushNotificationModel,
                                               NotificationChannelSetting channelSetting,
                                               boolean enableBigTextStyle) {
        checkToCreateNotificationChannel(context, channelSetting);
        int notificationId = (int) SystemClock.uptimeMillis();
        String title = receivePushNotificationModel.getTitle();
        String content = receivePushNotificationModel.getMessage();
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                notificationId,
                getResumeApplicationIntent(context),
                getPendingIntentFlag());
        pushNotificationInternally(context, notificationId, title, content, pendingIntent, channelSetting, enableBigTextStyle);
    }

    private static Intent getResumeApplicationIntent(Context context) {
        return new Intent(context, ResumeApplicationReceiverActivity.class);
    }

    public static int getPendingIntentFlag() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            return PendingIntent.FLAG_IMMUTABLE;
        } else {
            return PendingIntent.FLAG_UPDATE_CURRENT;
        }
    }

    public static void testPushResumeAppNotification(Context context) {
        ReceivePushNotificationModel general = new ReceivePushNotificationModel();
        general.setTitle("Test Push");
        general.setMessage("Test custom push notification.");
        pushGeneralNotification(context,
                general,
                NotificationChannelSetting.OrderStatusUpdate,
                true);
    }
}
