package com.proapp.sompom.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.proapp.sompom.R;

/**
 * Created by ChhomVeasna on 8/15/16.
 */
public final class MailUtils {
    private MailUtils() {
    }

    public static void mailToSupporter(Context context) {
        Intent contactSupportIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"));
        contactSupportIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.mail_subject));
        contactSupportIntent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.mail_body));
        contactSupportIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{context.getString(R.string.contact_email)});
        context.startActivity(Intent.createChooser(contactSupportIntent,
                context.getString(R.string.send_mail)));
    }
}
