package com.proapp.sompom.utils;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateUtils;

import com.proapp.sompom.R;
import com.resourcemanager.helper.LocaleManager;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by ChhomVeasna on 8/17/16.
 */
public final class DateTimeUtils {

    public static int getHours(long milli) {
        Date currentDate = new Date(System.currentTimeMillis());
        Date previousDate = new Date(milli);
        long timeBetweenDate = currentDate.getTime() - previousDate.getTime();
        return (int) TimeUnit.MILLISECONDS.toHours(timeBetweenDate);
    }

    public static int getMinutes(long milli) {
        Date currentDate = new Date(System.currentTimeMillis());
        Date previousDate = new Date(milli);
        long timeBetweenDate = currentDate.getTime() - previousDate.getTime();
        return (int) TimeUnit.MILLISECONDS.toMinutes(timeBetweenDate);
    }

    public static int getSecond(long milli) {
        Date currentDate = new Date(System.currentTimeMillis());
        Date previousDate = new Date(milli);
        long timeBetweenDate = currentDate.getTime() - previousDate.getTime();
        return (int) TimeUnit.MILLISECONDS.toSeconds(timeBetweenDate);
    }

    public static String getAudioTime(long milli) {
        return String.format(Locale.getDefault(), "%2d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(milli),
                TimeUnit.MILLISECONDS.toSeconds(milli) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milli))
        );
    }

    public static String getTimestampForMissedCallMessage(Context context, long time) {
        if (DateUtils.isToday(time)) {
            return com.proapp.sompom.utils.DateUtils.getDateWithFormat(context,
                    time,
                    getTimeFormatBaseOnLanguage(context),
                    Locale.getDefault());
        } else {
            return parse(context, time, false, null) +
                    " " +
                    context.getString(R.string.chat_message_call_at) +
                    " " +
                    com.proapp.sompom.utils.DateUtils.getDateWithFormat(context,
                            time,
                            getTimeFormatBaseOnLanguage(context),
                            Locale.getDefault());
        }
    }

    public static String getTimeFormatBaseOnLanguage(Context context) {
        /*
        For French, we don't display AM or PM for the time and for other languages, we keep display as
        normal with the localization.
         */
        String language = SharedPrefUtils.getLanguage(context);
        if (TextUtils.equals(LocaleManager.FRENCH_LANGUAGE_KEY, language)) {
            return com.proapp.sompom.utils.DateUtils.DATE_FORMAT_14;
        }

        return com.proapp.sompom.utils.DateUtils.DATE_FORMAT_15;
    }

    public static String getGroupFileTimeStampDisplay(Context context, long time) {
        return com.proapp.sompom.utils.DateUtils.getDateWithFormat(context,
                time,
                com.proapp.sompom.utils.DateUtils.DATE_FORMAT_22,
                Locale.getDefault()) +
                " " +
                context.getString(R.string.chat_message_call_at) +
                " " +
                com.proapp.sompom.utils.DateUtils.getDateWithFormat(context,
                        time,
                        com.proapp.sompom.utils.DateUtils.DATE_FORMAT_14,
                        Locale.getDefault());
    }

    private static String getCAllDurationDisplay(Context context, int inSeconds) {
        int hour = inSeconds / (60 * 60);
        int minute = inSeconds / 60;
        int second = inSeconds % 60;

        StringBuilder format = new StringBuilder();
        //Add hour
        if (hour > 0) {
            format.append(hour);
            format.append(" ");
            format.append(hour > 1 ? context.getString(R.string.plural_hour_abbrevation) : context.getString(R.string.single_hour_abbrevation));
        }
        //Add minute
        format.append(" ");
        format.append(minute);
        format.append(minute > 1 ? context.getString(R.string.plural_min_abbrevation) : context.getString(R.string.single_min_abbrevation));
        //Add second
        format.append(" ");
        format.append(second);
        format.append(minute > 1 ? context.getString(R.string.plural_sec_abbrevation) : context.getString(R.string.single_sec_abbrevation));

        return format.toString().trim();
    }

    public static String parse(Context context,
                               long time,
                               boolean displayTime,
                               String timePreposition) {
        /*
        Time display logic.
        1. If time less than 1 hour =>  Just now or %d minute/s ago
        2. If time is in today => 10:00 AM
        3. If time in this week => MONDAY
        4. If time is less than 6 months => SEP 6
        5. If time is greater than 6 months => 10/01/2019 (dd/MM/yyyy)
         */

        if (isLessThanSpecificHourPeriod(time, 1)) {
            if (inJustNow(time)) {
                return context.getString(R.string.common_timestamp_just_now_label);
            } else {
                long diffMinutes = getDiffMinutes(time);
                if (diffMinutes > 1) {
                    return context.getString(R.string.common_timestamp_plural_minute_ago_label, diffMinutes);
                } else {
                    return context.getString(R.string.common_timestamp_single_minute_ago_label, diffMinutes);
                }
            }
        } else {
            if (DateUtils.isToday(time)) {
                return com.proapp.sompom.utils.DateUtils.getDateWithFormat(context,
                        time,
                        getTimeFormatBaseOnLanguage(context),
                        Locale.getDefault());
            } else if (isDateInCurrentWeek(time)) {
                String dateWithFormat = com.proapp.sompom.utils.DateUtils.getDateWithFormat(context,
                        time,
                        com.proapp.sompom.utils.DateUtils.DATE_FORMAT_18,
                        Locale.getDefault());
                if (displayTime) {
                    dateWithFormat = addTimeSuffix(context, dateWithFormat, time, timePreposition);
                }
                return dateWithFormat;
            } else if (monthsBetween(new Date(time), new Date()) < 6) {
                String dateWithFormat = com.proapp.sompom.utils.DateUtils.getDateWithFormat(context,
                        time,
                        com.proapp.sompom.utils.DateUtils.DATE_FORMAT_17,
                        Locale.getDefault());
                if (displayTime) {
                    dateWithFormat = addTimeSuffix(context, dateWithFormat, time, timePreposition);
                }
                return dateWithFormat;
            } else {
                //Time is greater than or equal 6 months
                String dateWithFormat = com.proapp.sompom.utils.DateUtils.getDateWithFormat(context,
                        time,
                        com.proapp.sompom.utils.DateUtils.DATE_FORMAT_5,
                        Locale.getDefault());
                if (displayTime) {
                    dateWithFormat = addTimeSuffix(context, dateWithFormat, time, timePreposition);
                }
                return dateWithFormat;
            }
        }
    }

    public static boolean isNowBetweenTime(Date pastDate, Date futureDate) {
        Calendar todayCalendar = DateTimeUtils.getCalendarInstance();
//        Timber.e("Past date: " + pastDate.getTime());
//        Timber.e("Today: " + todayCalendar.getTimeInMillis());
//        Timber.e("Future date: " + futureDate.getTime());
        return todayCalendar.getTime().before(futureDate) && todayCalendar.getTime().after(pastDate);
    }

    public static boolean isDateInThePast(Date date) {
        Calendar todayCalendar = DateTimeUtils.getCalendarInstance();
//        Timber.e("Today: " + todayCalendar.getTimeInMillis());
//        Timber.e("Date to check: " + date.getTime());
        return todayCalendar.getTime().getTime() > date.getTime();
    }

    public static String getGroupSectionTimeFormat(Date checkDate) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        Calendar checkCalendar = Calendar.getInstance();
        checkCalendar.setTime(checkDate);
        if (currentYear == checkCalendar.get(Calendar.YEAR)) {
            //Same year
            return com.proapp.sompom.utils.DateUtils.getDefaultStringDateFormat(checkDate,
                    com.proapp.sompom.utils.DateUtils.DATE_FORMAT_21);
        } else {
            return com.proapp.sompom.utils.DateUtils.getDefaultStringDateFormat(checkDate,
                    com.proapp.sompom.utils.DateUtils.DATE_FORMAT_20);
        }
    }

    private static String addTimeSuffix(Context context, String dateTime, long time, String timePreposition) {
        //Sample: Monday => Monday at 10:00 AM
        return dateTime + " " + timePreposition + " " + com.proapp.sompom.utils.DateUtils.getDateWithFormat(context,
                time,
                getTimeFormatBaseOnLanguage(context),
                Locale.getDefault());
    }

    private static boolean isDateInCurrentWeek(long time) {
        Date date = new Date(time);
        Calendar currentCalendar = getCalendarInstance();
        int week = currentCalendar.get(Calendar.WEEK_OF_YEAR);
        int year = currentCalendar.get(Calendar.YEAR);
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(date);
        int targetWeek = targetCalendar.get(Calendar.WEEK_OF_YEAR);
        int targetYear = targetCalendar.get(Calendar.YEAR);
        return week == targetWeek && year == targetYear;
    }

    public static Calendar getCalendarInstance() {
        return Calendar.getInstance();
    }

    public static long getTimeDiff(long time) {
        return getCalendarInstance().getTimeInMillis() - time;
    }

    private static int monthsBetween(Date a, Date b) {
        Calendar cal = Calendar.getInstance();
        if (a.before(b)) {
            cal.setTime(a);
        } else {
            cal.setTime(b);
            b = a;
        }
        int c = 0;
        while (cal.getTime().before(b)) {
            cal.add(Calendar.MONTH, 1);
            c++;
        }

        return c - 1;
    }

    private static boolean isLessThanSpecificHourPeriod(long time, int hour) {
        long timeDiff = getTimeDiff(time);
        return timeDiff < (hour * 60 * 60 * 1000);
    }

    private static long getDiffMinutes(long milli) {
        Date current = new Date();
        long diff = current.getTime() - milli;
        return diff / (1000 * 60);
    }

    private static boolean inJustNow(long milli) {
        Date current = new Date();
        long diff = current.getTime() - milli;
        //If the milli ago is less than 1 minute, will display just now.
        return diff < (1000 * 60);
    }

    public static String getLastActiveRelativeTimeSpanDisplay(Context context, long time) {
        long currentTimeMillis = System.currentTimeMillis();
        long diff = currentTimeMillis - time;
        if (diff > 0) {
            //Minute ago
            long minute = diff / (1000 * 60);
            if (minute <= 1) {
                return context.getString(R.string.chat_timestamp_active,
                        context.getString(R.string.common_timestamp_single_minute_ago_label, 1));
            } else if (minute < 60) {
                return context.getString(R.string.chat_timestamp_active,
                        context.getString(R.string.common_timestamp_plural_minute_ago_label, minute));
            }

            //Hour ago
            long hour = diff / (1000 * 60 * 60);
            if (hour <= 1) {
                return context.getString(R.string.chat_timestamp_active,
                        context.getString(R.string.chat_timestamp_single_hour_ago_label, 1));
            } else if (hour < 24) {
                return context.getString(R.string.chat_timestamp_active,
                        context.getString(R.string.chat_timestamp_plural_hour_ago_label, hour));
            }

            //Day ago
            long day = diff / (1000 * 60 * 60 * 24);
            if (day <= 1) {
                return context.getString(R.string.chat_timestamp_active,
                        context.getString(R.string.chat_timestamp_single_day_ago_label, 1));
            } else if (day < 30) {
                return context.getString(R.string.chat_timestamp_active,
                        context.getString(R.string.chat_timestamp_plural_day_ago_label, day));
            }

            //Month ago
            long month = day / 30;
            if (month <= 1) {
                return context.getString(R.string.chat_timestamp_active,
                        context.getString(R.string.chat_timestamp_single_month_ago_label, 1));
            } else if (month < 12) {
                return context.getString(R.string.chat_timestamp_active,
                        context.getString(R.string.chat_timestamp_plural_month_ago_label, month));
            }

            //Year ago
            long year = month / 12;
            if (year <= 1) {
                return context.getString(R.string.chat_timestamp_active,
                        context.getString(R.string.chat_timestamp_single_year_ago_label, 1));
            } else {
                return context.getString(R.string.chat_timestamp_active,
                        context.getString(R.string.chat_timestamp_plural_year_ago_label, year));
            }
        } else {
            return context.getString(R.string.chat_header_active_status);
        }
    }
}
