package com.proapp.sompom.utils;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

import androidx.annotation.StringRes;

import com.proapp.sompom.R;

/**
 * Created by He Rotha on 9/27/17.
 */

public final class ToastUtil {

    private ToastUtil() {
    }

    public static void showToast(Context context, String message, boolean isError) {
        com.resourcemanager.utils.ToastUtil.showToast(context, message, isError);
    }

    public static void showToast(Context context, @StringRes int message, boolean isError) {
        com.resourcemanager.utils.ToastUtil.showToast(context, message, isError);
    }

    public static void showToast(Context context, String message, int textColor, int backgroundColor) {
        com.resourcemanager.utils.ToastUtil.showToast(context, message, textColor, backgroundColor);
    }

    public static void showToastForCallScreen(Context context, String message, boolean isError) {
        if (context == null) {
            return;
        }
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, 0);
        customizeToastColor(toast,
                AttributeConverter.convertAttrToColor(context, R.attr.toast_call_text),
                AttributeConverter.convertAttrToColor(context, R.attr.toast_call_background))
                .show();
    }

    private static Toast customizeToastColor(Toast toast, int textColor, int backgroundColor) {
        return com.resourcemanager.utils.ToastUtil.customizeToastColor(toast, textColor, backgroundColor);
    }
}
