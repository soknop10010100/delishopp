package com.proapp.sompom.utils;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.StringRes;

import com.google.android.material.snackbar.Snackbar;
import com.proapp.sompom.R;

import timber.log.Timber;

/**
 * Created by He Rotha on 9/27/17.
 */

public final class SnackBarUtil {
    private SnackBarUtil() {
    }

    public static void showSnackBar(View view, @StringRes int message, boolean isError) {
        try {
            if (view != null) {
                if (view.getContext() != null) {
                    Snackbar snackbar = Snackbar.make(view,
                            view.getContext().getString(message),
                            Snackbar.LENGTH_SHORT);
                    setSnackColor(view.getContext(), snackbar, isError);
                    snackbar.show();
                }
            }
        } catch (Exception ignore) {
            Timber.e(ignore.toString());
        }
    }

    public static void showSnackBar(View view, String message, boolean isError) {
        Timber.e("showSnackBar: " + message);
        if (view == null) {
            return;
        }
        Snackbar snackbar = Snackbar.make(view,
                message,
                Snackbar.LENGTH_SHORT);
        setSnackColor(view.getContext(), snackbar, isError);
        snackbar.show();
    }

    private static void setSnackColor(Context context, Snackbar snackbar, boolean isError) {
        snackbar.getView().setBackgroundColor(AttributeConverter.convertAttrToColor(context,
                isError ? R.attr.toast_error_background : R.attr.toast_normal_background));
        TextView textView = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
        if (textView != null) {
            textView.setTextColor(AttributeConverter.convertAttrToColor(context,
                    isError ? R.attr.toast_error_text : R.attr.toast_normal_text));
        }
    }
}
