package com.proapp.sompom.utils;

import android.text.TextUtils;

import androidx.core.util.PatternsCompat;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by nuonveyo on 2/26/18.
 */

public class GenerateLinkPreviewUtil {

    public static final Pattern URL_CHECKING_PATTERN = PatternsCompat.AUTOLINK_WEB_URL;

    private GenerateLinkPreviewUtil() {
    }

    public static String getPreviewLink(String text) {
        if (TextUtils.isEmpty(text)) {
            return null;
        }

        String replaceText = text.replace("\n", " ");
        String[] splitText = replaceText.split(" ");
        List<String> linksList = new ArrayList<>();

        for (String s : splitText) {
            if (isValidUrl(s)) {
                linksList.add(s);
            }
        }

        //Get last url to preview
        if (!linksList.isEmpty()) {
            String lastLink = linksList.get(linksList.size() - 1);
            String url;
            if (lastLink.contains("http://") || lastLink.contains("https://")) {
                url = lastLink;
            } else {
                url = "http://" + lastLink;
            }
            return url;
        }
        return null;
    }

    public static String getFirstUrlIndex(String content) {
        if (TextUtils.isEmpty(content)) {
            return "";
        }

        String replaceText = content.replace("\n", " ");
        String[] splitText = replaceText.split(" ");
        for (String url : splitText) {
            if (isValidUrl(url)) {
                if (!url.startsWith("http")) {
                    url = "http://" + url;
                }
                return url;
            }
        }

        return null;
    }

    public static String getUrlFromText(String content) {
        String replaceText = content.replace("\n", " ");
        String[] splitText = replaceText.split(" ");
        for (String url : splitText) {
            if (isValidUrl(url)) {
                return url;
            }
        }

        return null;
    }

    public static boolean isValidUrl(String urlString) {
        if (!TextUtils.isEmpty(urlString)) {
            return URL_CHECKING_PATTERN.matcher(urlString).matches();
        }

        return false;
    }
}
