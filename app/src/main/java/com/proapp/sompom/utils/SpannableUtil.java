package com.proapp.sompom.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.DelayClickableSpan;
import com.proapp.sompom.listener.OnSpannableClickListener;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.model.emun.PublishItem;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.widget.ImageProfileLayout;
import com.resourcemanager.helper.FontHelper;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import timber.log.Timber;

/**
 * Created by nuonveyo on 4/25/18.
 */

public final class SpannableUtil {
    private static final String TIME_LOGO = "TIME_LOGO";
    private static final String DOT_FIRST = "DOT_FIRST";
    private static final String DOT_SECOND = "DOT_SECOND";
    private static final String USER = "USER";

    private SpannableUtil() {
    }

    public static Spannable setFacebookButtonText(Context context, String holdText, String boldText, String boldTextPhone) {
        Spannable spannable = new SpannableString(holdText);
        try {
            Typeface typefaceBold = FontHelper.getBoldFontStyle(context);
            int color = AttributeConverter.convertAttrToColor(context, R.attr.colorFirstText);

            spannable.setSpan(new CustomTypefaceSpan("", typefaceBold),
                    holdText.indexOf(boldTextPhone),
                    holdText.indexOf(boldTextPhone) + boldTextPhone.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            spannable.setSpan(new ForegroundColorSpan(color),
                    holdText.indexOf(boldTextPhone),
                    holdText.indexOf(boldTextPhone) + boldTextPhone.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            spannable.setSpan(new CustomTypefaceSpan("", typefaceBold),
                    holdText.indexOf(boldText),
                    holdText.indexOf(boldText) + boldText.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            spannable.setSpan(new ForegroundColorSpan(color),
                    holdText.indexOf(boldText),
                    holdText.indexOf(boldText) + boldText.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            Timber.e("error: %s", e.toString());
        }
        return spannable;
    }

    public static Spannable setNotificationDescriptionText(Context context, String originalText, OnSpannableClickListener listener) {
        String decorateText = context.getString(R.string.edit_profile_disable_all_notification_description_bold);
        int color = AttributeConverter.convertAttrToColor(context, R.attr.colorAccent);

        Spannable spannable = new SpannableString(originalText);
        try {
            spannable.setSpan(new ForegroundColorSpan(color),
                    originalText.indexOf(decorateText),
                    originalText.indexOf(decorateText) + decorateText.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            spannable.setSpan(new StyleSpan(Typeface.BOLD),
                    originalText.indexOf(decorateText),
                    originalText.indexOf(decorateText) + decorateText.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            ClickableSpan span = new DelayClickableSpan() {
                @Override
                public void onDelayClick(View widget) {
                    widget.invalidate();
                    if (listener != null) {
                        listener.onClick();
                    }
                }
            };

            spannable.setSpan(span, originalText.indexOf(decorateText),
                    originalText.indexOf(decorateText) + decorateText.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            Timber.e("e: %s", e.toString());
        }
        return spannable;
    }

    public static Spannable getNotificationName(Context context, String name, String description) {
        Spannable span = Spannable.Factory.getInstance().newSpannable(description);
        try {
            Typeface typefaceBold = FontHelper.getBoldFontStyle(context);
            span.setSpan(new CustomTypefaceSpan("", typefaceBold),
                    description.indexOf(name),
                    description.indexOf(name) + name.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            Typeface typeface = FontHelper.getBoldFontStyle(context);
            span.setSpan(new CustomTypefaceSpan("", typeface),
                    description.indexOf(description),
                    description.indexOf(description) + description.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            Timber.e("Exception: %s", e.getMessage());
        }
        return span;
    }

    public static SpannableString getTimelineDate(Context context,
                                                  String date,
                                                  String address,
                                                  PublishItem publishItem,
                                                  boolean isShowOnlyTime) {
        StringBuilder text = new StringBuilder();
        text.append(TIME_LOGO).append(" ").append(" ");
        text.append(date).append(" ");

        if (!isShowOnlyTime) {
            if (!TextUtils.isEmpty(address)) {
                text
                        .append(" ")
                        .append(DOT_FIRST)
                        .append(" ")
                        .append(" ")
                        .append(address)
                        .append(" ")
                        .append(" ")
                        .append(DOT_SECOND)
                        .append(" ")
                        .append(" ")
                        .append(USER);
            } else {
                text
                        .append(" ")
                        .append(" ")
                        .append(DOT_FIRST)
                        .append(" ")
                        .append(" ")
                        .append(USER);
            }
        }

        SpannableString spannableString = new SpannableString(text);

        spannableString.setSpan(getIconWithColorFilter(context, R.drawable.ic_clock_filled, 1.5f,
                AttributeConverter.convertAttrToColor(context, R.attr.post_sub_title)),
                text.indexOf(TIME_LOGO),
                text.indexOf(TIME_LOGO) + TIME_LOGO.length(),
                0);

        if (!isShowOnlyTime) {
            spannableString.setSpan(
                    getIcon(context, R.drawable.ic_small_cycle, 0.7f, -1),
                    text.indexOf(DOT_FIRST),
                    text.indexOf(DOT_FIRST) + DOT_FIRST.length(),
                    0);

            Pattern pattern = Pattern.compile(DOT_SECOND);
            Matcher matcher = pattern.matcher(text);
            if (matcher.find()) {
                spannableString.setSpan(
                        getIcon(context, R.drawable.ic_small_cycle, 0.7f, -1),
                        text.indexOf(DOT_SECOND),
                        text.indexOf(DOT_SECOND) + DOT_SECOND.length(),
                        0);
            }

            spannableString.setSpan(getIconWithColorFilter(context, publishItem.getIcon(), 1.5f,
                    AttributeConverter.convertAttrToColor(context, R.attr.post_sub_title_privacy)),
                    text.indexOf(USER),
                    text.indexOf(USER) + USER.length(),
                    0);
        }

        return spannableString;
    }

    public static SpannableString getProduct(Context context, String title, String address, PublishItem publishItem) {
        StringBuilder text = new StringBuilder();
        if (!TextUtils.isEmpty(title)) {
            text.append(title.toUpperCase()).append(" ");
        }
        if (!TextUtils.isEmpty(address)) {
            text
                    .append(" ")
                    .append(DOT_FIRST)
                    .append(" ")
                    .append(" ")
                    .append(address)
                    .append(" ")
                    .append(" ")
                    .append(DOT_SECOND)
                    .append(" ")
                    .append(" ")
                    .append(USER);
        } else {
            text
                    .append(" ")
                    .append(" ")
                    .append(DOT_FIRST)
                    .append(" ")
                    .append(" ")
                    .append(USER);
        }

        SpannableString spannableString = new SpannableString(text);
        if (!TextUtils.isEmpty(title)) {
            int orangeColor = AttributeConverter.convertAttrToColor(context, R.attr.colorAccent);
            spannableString.setSpan(new ForegroundColorSpan(orangeColor),
                    0,
                    title.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            Typeface typeface = ResourcesCompat.getFont(context, R.font.sf_pro_bold);
            spannableString.setSpan(new CustomTypefaceSpan("", typeface),
                    0,
                    title.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        spannableString.setSpan(
                getIcon(context, R.drawable.ic_small_cycle, 0.7f, -1),
                text.indexOf(DOT_FIRST),
                text.indexOf(DOT_FIRST) + DOT_FIRST.length(),
                0);

        Pattern pattern = Pattern.compile(DOT_SECOND);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            spannableString.setSpan(
                    getIcon(context, R.drawable.ic_small_cycle, 0.7f, -1),
                    text.indexOf(DOT_SECOND),
                    text.indexOf(DOT_SECOND) + DOT_SECOND.length(),
                    0);
        }

        spannableString.setSpan(getIcon(context, publishItem.getIcon(), 1.5f, -1),
                text.indexOf(USER),
                text.indexOf(USER) + USER.length(),
                0);

        if (!TextUtils.isEmpty(address)) {
            Typeface typeface = ResourcesCompat.getFont(context, R.font.sf_pro_light);
            spannableString.setSpan(new CustomTypefaceSpan("", typeface),
                    text.indexOf(address),
                    text.indexOf(address) + address.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        }

        return spannableString;
    }

    private static Bitmap getBitmap(Drawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    private static ImageSpan getIcon(Context context,
                                     @DrawableRes int drawableRest,
                                     float position,
                                     int colorResource) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableRest);
        Bitmap bitmap = getBitmap(drawable);
        return new ImageSpan(context, bitmap, ImageSpan.ALIGN_BASELINE) {
            public void draw(Canvas canvas, CharSequence text, int start,
                             int end, float x, int top, int y, int bottom,
                             Paint paint) {
                Drawable b = getDrawable();
                if (colorResource != -1) {
                    b.setColorFilter(ContextCompat.getColor(context, colorResource), PorterDuff.Mode.SRC_IN);
                }
                canvas.save();

                //int transY = ((bottom - b.getBounds().bottom) + paint.getFontMetricsInt().descent) / 2;

                int transY = bottom - b.getBounds().bottom;
                transY -= paint.getFontMetricsInt().descent / position;

                canvas.translate(x, transY);
                b.draw(canvas);
                canvas.restore();
            }
        };
    }

    private static ImageSpan getIconWithColorFilter(Context context,
                                                    @DrawableRes int drawableRest,
                                                    float position,
                                                    int color) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableRest);
        if (drawable != null) {
            Bitmap bitmap = getBitmap(drawable);
            return new ImageSpan(context, bitmap, ImageSpan.ALIGN_BASELINE) {
                public void draw(Canvas canvas, CharSequence text, int start,
                                 int end, float x, int top, int y, int bottom,
                                 Paint paint) {
                    Drawable b = getDrawable();
                    if (color != -1) {
                        b.setColorFilter(color, PorterDuff.Mode.SRC_IN);
                    }
                    canvas.save();

                    //int transY = ((bottom - b.getBounds().bottom) + paint.getFontMetricsInt().descent) / 2;

                    int transY = bottom - b.getBounds().bottom;
                    transY -= paint.getFontMetricsInt().descent / position;

                    canvas.translate(x, transY);
                    b.draw(canvas);
                    canvas.restore();
                }
            };
        }

        return null;
    }

    public static SpannableString getUserNameSharedTimeline(Context context,
                                                            User user,
                                                            String shareText,
                                                            ImageProfileLayout.RenderType renderType) {
        String userFullName;
        if (renderType == null || renderType == ImageProfileLayout.RenderType.USER) {
            userFullName = user.getFullName();
        } else {
            userFullName = user.getStoreName();
        }
        String text = userFullName + " " + shareText;
        SpannableString spannableString = new SpannableString(text);

        Typeface typefaceBold = FontHelper.getBoldFontStyle(context);
        spannableString.setSpan(new CustomTypefaceSpan("", typefaceBold),
                text.indexOf(userFullName),
                text.indexOf(userFullName) + userFullName.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        int orangeColor = ContextCompat.getColor(context, R.color.colorPrimary);
        spannableString.setSpan(new ForegroundColorSpan(orangeColor),
                text.indexOf(shareText),
                text.indexOf(shareText) + shareText.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        Typeface typeface = ResourcesCompat.getFont(context, R.font.sf_pro_light);
        spannableString.setSpan(new CustomTypefaceSpan("", typeface),
                text.indexOf(shareText),
                text.indexOf(shareText) + shareText.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    public static SpannableString getUserNameSharedTimeline(Context context,
                                                            User user,
                                                            ImageProfileLayout.RenderType renderType) {
        String userFullName;
        if (renderType == null || renderType == ImageProfileLayout.RenderType.USER) {
            userFullName = user.getFullName();
        } else {
            userFullName = user.getStoreName();
        }

        SpannableString spannableString = new SpannableString(userFullName);

        Typeface typefaceBold = FontHelper.getBoldFontStyle(context);
        spannableString.setSpan(new CustomTypefaceSpan("", typefaceBold),
                0,
                userFullName.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    public static Spannable getUserFullNameCheckIn(Context context,
                                                   User user,
                                                   SearchAddressResult searchAddressResult,
                                                   View.OnClickListener listener) {
        String userFullName = user.getFullName();
        String at = context.getString(R.string.post_location_at);
        String place = searchAddressResult.getPlaceTitle();
        if (TextUtils.isEmpty(place)) {
            place = searchAddressResult.getCity();
        }

        int atIndex = -1;
        if (!TextUtils.isEmpty(place)) {
            userFullName = userFullName + " ";
            atIndex = userFullName.length();
            userFullName += at + " " + place;
        }
        Spannable span = Spannable.Factory.getInstance().newSpannable(userFullName);
        try {
            if (listener != null) {
                ClickableSpan clickableSpan = new DelayClickableSpan() {
                    @Override
                    public void onDelayClick(View textView) {
                        listener.onClick(textView);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                        ds.setColor(AttributeConverter.convertAttrToColor(context,
                                R.attr.post_check_in_place));
                    }
                };
                span.setSpan(clickableSpan,
                        userFullName.indexOf(place),
                        userFullName.indexOf(place) + place.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            if (!TextUtils.isEmpty(place)) {
                Typeface typeface = FontHelper.getLightFontStyle(context);
                span.setSpan(new CustomTypefaceSpan("", typeface),
                        atIndex,
                        atIndex + at.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                //"At" label
                span.setSpan(new ForegroundColorSpan(AttributeConverter.convertAttrToColor(context,
                        R.attr.post_check_in_at)),
                        atIndex,
                        atIndex + at.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        } catch (Exception e) {
            Timber.e("Exception: %s", e.getMessage());
        }
        return span;
    }

    public static Spannable getEmailLink(Context context, Spannable spannable, int color) {
        String text = spannable.toString();
        try {
            String replaceText = text.replace("\n", " ");
            String[] splitText = replaceText.split(" ");
            for (String value : splitText) {
                if (SpecialTextRenderUtils.isEmailAddress(value)) {
                    ClickableSpan clickableSpan = new DelayClickableSpan() {
                        @Override
                        public void onDelayClick(@NonNull View textView) {
                            IntentUtil.openEmail(context, value);
                        }
                    };

                    spannable.setSpan(clickableSpan,
                            text.indexOf(value),
                            text.indexOf(value) + value.length(),
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spannable.setSpan(new ForegroundColorSpan(color),
                            text.indexOf(value),
                            text.indexOf(value) + value.length(),
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        } catch (Exception e) {
            Timber.e("getEmailLink error: %s", e.getMessage());
        }

        return spannable;
    }

    public static Spannable getTextLink(Context context, Spannable spannable, int color) {
        String text = spannable.toString();
        try {
            String replaceText = text.replace("\n", " ");
            String[] splitText = replaceText.split(" ");
            for (String url : splitText) {
                if (GenerateLinkPreviewUtil.isValidUrl(url)) {
                    ClickableSpan clickableSpan = new DelayClickableSpan() {
                        @Override
                        public void onDelayClick(@NonNull View textView) {
                            IntentUtil.deepLinkIntent(context, url);
                        }
                    };

                    spannable.setSpan(clickableSpan,
                            text.indexOf(url),
                            text.indexOf(url) + url.length(),
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spannable.setSpan(new ForegroundColorSpan(color),
                            text.indexOf(url),
                            text.indexOf(url) + url.length(),
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        } catch (Exception e) {
            Timber.e("getTextLink error: %s", e.getMessage());
        }

        return spannable;
    }

    public static Spannable getUserStoreName(Context context, User user) {
        String userStoreName = user.getStoreName();
        if (TextUtils.isEmpty(userStoreName)) {
            userStoreName = user.getFullName();
        }
        String holdText = context.getString(R.string.user_store_dialog_available_title) + " " + userStoreName;
        SpannableString spannableString = new SpannableString(holdText);

        Typeface typeface = FontHelper.getBoldFontStyle(context);
        spannableString.setSpan(new CustomTypefaceSpan("", typeface),
                holdText.indexOf(userStoreName),
                holdText.indexOf(userStoreName) + userStoreName.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        int color = AttributeConverter.convertAttrToColor(context, R.attr.colorFirstText);
        spannableString.setSpan(new ForegroundColorSpan(color),
                holdText.indexOf(userStoreName),
                holdText.indexOf(userStoreName) + userStoreName.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannableString;
    }

    public static Spannable getStoreName(Context context, String user) {
        final String suffix = "@";


        String fullText = suffix + user;
        SpannableString spannableString = new SpannableString(fullText);

        Typeface typeface = FontHelper.getBoldFontStyle(context);
        spannableString.setSpan(new CustomTypefaceSpan("", typeface),
                fullText.indexOf(suffix),
                fullText.indexOf(suffix) + suffix.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        int color = AttributeConverter.convertAttrToColor(context, R.attr.colorFirstText);
        spannableString.setSpan(new ForegroundColorSpan(color),
                fullText.indexOf(suffix),
                fullText.indexOf(suffix) + suffix.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannableString;
    }

    public static Spannable setBoldText(Context context,
                                        String originalText,
                                        String boldText,
                                        int boldColor) {
        SpannableString spannableString = new SpannableString(originalText);
        Typeface typeface = FontHelper.getBoldFontStyle(context);
        int end = originalText.indexOf(boldText) + boldText.length();
        spannableString.setSpan(new CustomTypefaceSpan("", typeface),
                originalText.indexOf(boldText),
                end,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        if (boldColor != -1) {
            spannableString.setSpan(new ForegroundColorSpan(boldColor),
                    originalText.indexOf(boldText),
                    end,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        return spannableString;
    }

    public static String buildMultipleUserNameForGroupModificationMessage(Context context,
                                                                          List<User> userList,
                                                                          boolean isForChatScreen) {
        placeCurrentUserAtLastIndex(context, userList);
        StringBuilder multipleUserNames = new StringBuilder();
        String userId = SharedPrefUtils.getUserId(context);
        if (userList.size() == 2) {
            multipleUserNames.append(userList.get(0).getFullName());
            multipleUserNames.append(" ");
            multipleUserNames.append(isForChatScreen ? context.getString(R.string.chat_message_someone_and_someone) :
                    context.getString(R.string.chat_info_someone_and_someone));
            multipleUserNames.append(" ");
            if (TextUtils.equals(userList.get(1).getId(), userId)) {
                multipleUserNames.append(isForChatScreen ? context.getString(R.string.chat_message_add_or_remove_you).toLowerCase() :
                        context.getString(R.string.chat_info_you_chat).toLowerCase());
            } else {
                multipleUserNames.append(userList.get(1).getFullName());
            }
        } else {
            for (int i = 0; i < userList.size(); i++) {
                if (!TextUtils.isEmpty(multipleUserNames.toString())) {
                    multipleUserNames.append(", ");
                }
                if (i == userList.size() - 1) {
                    //Last name
                    multipleUserNames.append(isForChatScreen ? context.getString(R.string.chat_message_add_or_remove_you).toLowerCase() :
                            context.getString(R.string.chat_info_you_chat).toLowerCase());
                    multipleUserNames.append(" ");
                }

                if (TextUtils.equals(userList.get(i).getId(), userId)) {
                    multipleUserNames.append(isForChatScreen ? context.getString(R.string.chat_message_add_or_remove_you).toLowerCase() :
                            context.getString(R.string.chat_info_you_chat).toLowerCase());
                } else {
                    multipleUserNames.append(userList.get(i).getFullName());
                }
            }
        }

        return multipleUserNames.toString();
    }

    public static String buildGroupNotificationUserName(Context context,
                                                        @NonNull User user,
                                                        boolean isCapitalized) {
        String userId = SharedPrefUtils.getUserId(context);
        if (TextUtils.equals(user.getId(), userId)) {
            if (isCapitalized) {
                return SpannableUtil.capitaliseOnlyFirstLetter(context.getString(R.string.chat_info_you_chat));
            }

            return context.getString(R.string.chat_info_you_chat);
        } else {
            return user.getFullName();
        }
    }

    private static void placeCurrentUserAtLastIndex(Context context, List<User> userList) {
        if (userList != null) {
            String userId = SharedPrefUtils.getUserId(context);
            User currentUser = null;
            for (User user : userList) {
                if (TextUtils.equals(user.getId(), userId)) {
                    currentUser = user;
                    break;
                }
            }

            if (currentUser != null) {
                userList.remove(currentUser);
                userList.add(currentUser);
            }
        }
    }

    public static String capitaliseOnlyFirstLetter(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }

        if (str.length() >= 2) {
            return str.substring(0, 1).toUpperCase() + str.substring(1);
        } else {
            return str.toUpperCase();
        }
    }

    public static String getMessageSubject(Context context, User doer) {
        if (doer != null) {
            return buildGroupNotificationUserName(context, doer, true);
        }

        return "";
    }
}
