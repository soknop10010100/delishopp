package com.proapp.sompom.utils;

import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.text.Spannable;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.Person;
import androidx.core.app.RemoteInput;
import androidx.core.graphics.drawable.IconCompat;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.service.RemoteNotificationBroadcast;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.database.MessageDb;
import com.proapp.sompom.database.NotificationIdDb;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.ColorResourceHelper;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.model.NotificationChannelSetting;
import com.proapp.sompom.model.notification.Notification;
import com.proapp.sompom.model.notification.NotificationActor;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;

import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by He Rotha on 12/6/18.
 */
public final class ChatNotificationUtil {

    public static final String KEY_TEXT_REPLY = "key_text_reply";

    private ChatNotificationUtil() {
    }

    public static void pushChatNotification(Context context, Chat chat, boolean isNoPendingIntent, String tag) {
        Timber.i("pushChatNotification: " + new Gson().toJson(chat));
        if (shouldIgnorePushNotification(context, chat)) {
            Timber.i("Ignore push local chat notification from current user account.");
            return;
        }

        try {
            User myUser = SharedPrefUtils.getUser(context);
            checkToInjectDeleteMessageProperty(chat);
            injectSenderInfoToChatIfNecessary(context, chat);
            Conversation conversationById = ConversationDb.getConversationById(context, chat.getChannelId());
            if (conversationById != null && conversationById.isGroup()) {
                chat.setGroupName(conversationById.getGroupName());
            }
            final Observable<Bitmap> senderBitmap = getSenderIcon(context, conversationById, chat);
            final Observable<Bitmap> myBitmap = NotificationUtils.downloadBitmap(context,
                    myUser.getUserProfileThumbnail(),
                    myUser.getFirstName(),
                    myUser.getLastName());
            final Observable<UserBitmap> userBitmap = Observable.zip(senderBitmap, myBitmap, (bitmap, bitmap2) -> {
                UserBitmap userBitmap12 = new UserBitmap();
                userBitmap12.mSenderBitmap = bitmap;
                userBitmap12.mMyBitmap = bitmap2;
                return userBitmap12;
            });
            userBitmap
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(userBitmap1 ->
                                    pushChatNotification(context,
                                            chat,
                                            userBitmap1.mSenderBitmap,
                                            myUser,
                                            userBitmap1.mMyBitmap,
                                            isNoPendingIntent,
                                            tag),
                            throwable -> pushChatNotification(context,
                                    chat,
                                    null,
                                    myUser,
                                    null,
                                    isNoPendingIntent,
                                    tag));
        } catch (Exception ex) {
            Timber.e("pushChatNotification Exception: %s", ex.toString());
            SentryHelper.logSentryError(ex);
        }
    }

    private static boolean shouldIgnorePushNotification(Context context, Chat chat) {
        String userId = SharedPrefUtils.getUserId(context);
        return TextUtils.equals(userId, chat.getSenderId());
    }

    public static void pushCreateGroupChatNotification(Context context, Notification notification) {
        //We must create a temporary chat model to render notification data
        NotificationActor actor = notification.getActors().get(0);
        if (actor != null) {
            Chat chat = new Chat();
            chat.setId(UUID.randomUUID().toString());
            chat.setChatType(notification.getVerb());
            chat.setChannelId(notification.getFirstObject().getConversionId());
            User sender = new User();
            sender.setId(actor.getId());
            sender.setFirstName(actor.getFirstName());
            sender.setLastName(actor.getLastName());
            sender.setUserProfileThumbnail(actor.getUserProfile());
            chat.setSender(sender);
            pushChatNotification(context, chat, false, "pushCreateGroupChatNotification");
        }
    }

    private static Observable<Bitmap> getSenderIcon(Context context, Conversation conversation, Chat chat) {
        if (conversation.isGroup()) {
            //Need to display group photo as sender
            return NotificationUtils.downloadBitmap(context,
                    conversation.getGroupImageUrl(),
                    conversation.getGroupName(),
                    null);
        } else if (chat.getSender() != null) {
            return NotificationUtils.downloadBitmap(context,
                    chat.getSender().getUserProfileThumbnail(),
                    chat.getSender().getFirstName(),
                    chat.getSender().getLastName());
        }

        return NotificationUtils.downloadBitmap(context,
                null,
                context.getString(R.string.app_name),
                null);
    }

    private static void pushChatNotification(Context context,
                                             Chat chat,
                                             @Nullable Bitmap bitmap,
                                             User myUser,
                                             @Nullable Bitmap myBitmap,
                                             boolean isNoPendingIntent,
                                             String tag) {
        final int notificationId = NotificationIdDb.getId(context, chat.getChannelId());
        Timber.i("push notificationId %s", notificationId + ", tag: " + tag);
        NotificationUtils.checkToCreateNotificationChannel(context, NotificationChannelSetting.Chat);
        NotificationCompat.Style style = createStyle(context, chat, bitmap, myUser, myBitmap);
        if (style == null) {
            Timber.e("cannot push, cos there are no unread message");
            return;
        }

        ChatIntent chatScreenIntent;
        if (ChatHelper.isGroupConversation(context, chat.getChannelId())) {
            Conversation conversationById = ConversationDb.getConversationById(context, chat.getChannelId());
            if (conversationById != null) {
                chatScreenIntent = new ChatIntent(context, conversationById);
            } else {
                //No need to dispatch notification.
                Timber.i("No group conversation found in local.");
                return;
            }
        } else {
            chatScreenIntent = new ChatIntent(context,
                    ConversationDb.getConversationById(context, chat.getChannelId()),
                    chat.getSender());
        }

        //Mark this property to true for later usage in creation of Chat screen.
        if (chatScreenIntent != null) {
            chatScreenIntent.setIsFromNotification(true);
        }

        /*
        Will always open new chat screen
         */
        PendingIntent pendingIntent;
        if (!isNoPendingIntent) {
            pendingIntent = PendingIntent.getActivity(context, notificationId, chatScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            pendingIntent = NotificationUtils.getNoneRedirectionNotificationPendingIntent(context, notificationId);
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,
                NotificationUtils.getNotificationChannelId(context, NotificationChannelSetting.Chat))
                .setSmallIcon(NotificationUtils.getNotificationIcon())
                .setColor(ColorResourceHelper.getAccentColor(context))
                .setStyle(style)
                .setPriority(NotificationUtils.getPriority(context, NotificationChannelSetting.Chat))
                .setContentIntent(pendingIntent)
                .setVibrate(NotificationUtils.getVibration(context, NotificationChannelSetting.Chat))
                .setSound(NotificationUtils.getNotificationSound(context, NotificationChannelSetting.Chat))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setAutoCancel(true);
        if (bitmap != null && !ChatHelper.isGroupConversation(context, chat.getChannelId())) {
            builder.setLargeIcon(bitmap);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            NotificationCompat.Action action = createAction(context, notificationId, chat);
            builder.addAction(action);
        }

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(notificationId, builder.build());
    }

    private static User getUserFromConversationParticipant(Context context, String conversationId, String userId) {
        Conversation conversationById = ConversationDb.getConversationById(context, conversationId);
        if (conversationById != null && conversationById.getParticipants() != null) {
            for (User participant : conversationById.getParticipants()) {
                if (TextUtils.equals(participant.getId(), userId)) {
                    return participant;
                }
            }
        }

        return null;
    }

    private static void checkToInjectDeleteMessageProperty(Chat chat) {
        /*
        Because the "delete" property is mandatory for render the notification message, we must assign
        this value to chat.
         */
        if (chat.getSendingType() == Chat.SendingType.DELETE) {
            chat.setDelete(true);
        }
    }

    private static void injectSenderInfoToChatIfNecessary(Context context, Chat chat) {
        if (chat.getSender() == null && isNeedToCheckSenderChatType(chat)) {
            User sender = getUserFromConversationParticipant(context, chat.getChannelId(), chat.getSenderId());
            if (sender != null) {
                chat.setSender(sender);
            }
        }
    }

    private static boolean isNeedToCheckSenderChatType(Chat chat) {
        /*
        The chat notification that need sender info to display, please include the type checking here.
         */
        return chat.getSendingType() == Chat.SendingType.UPDATE || chat.getSendingType() == Chat.SendingType.DELETE;
    }

    /**
     * @return null, mean that no un-read message
     */
    private static NotificationCompat.Style createStyle(Context context,
                                                        Chat chat,
                                                        Bitmap bitmap,
                                                        User myUser,
                                                        Bitmap myBitmap) {
        String name = "";
        String key = "";
        if (ChatHelper.isGroupConversation(context, chat.getChannelId())) {
            name = chat.getGroupName();
            key = chat.getChannelId();
        } else {
            name = chat.getSender().getFullName();
            key = chat.getSender().getId();
        }

        Person.Builder senderBuilder = new Person.Builder()
                .setName(name)
                .setKey(key)
                .setBot(false)
                .setImportant(false);
        //Do not display sender avatar for group notification
        if (bitmap != null) {
            senderBuilder.setIcon(IconCompat.createWithBitmap(bitmap));
        }
        Person person = senderBuilder.build();

        Person.Builder myUserBuilder = new Person.Builder()
                .setName(myUser.getFullName())
                .setKey(myUser.getId())
                .setBot(false)
                .setImportant(false);
        if (myBitmap != null) {
            myUserBuilder.setIcon(IconCompat.createWithBitmap(myBitmap));
        }
        Person myPerson = myUserBuilder.build();

        NotificationCompat.MessagingStyle style = new NotificationCompat.MessagingStyle(myPerson);

        if (ChatHelper.isGroupConversation(context, chat.getChannelId())) {
            Spannable message = SpecialTextRenderUtils.renderMentionUser(context,
                    chat.getActualContent(context,
                            ConversationDb.getConversationById(context, chat.getChannelId()),
                            true),
                    false,
                    false,
                    Color.BLACK,
                    -1, -1, true,
                    null);
            Timber.i("Message: " + message.toString() + ", Name: " + person.getName() + ", key: " + person.getKey());
            style.addMessage(new NotificationCompat.MessagingStyle.Message(message,
                    chat.getTime(),
                    person));
        } else {
            Timber.i("Manage notification message for individual chat.");
            /*
            For individual chat notification, we will display some old unread messages as group.
            And if the current notification does not exist in local db list, we have to add it manually as
            if the app is not yet open and receive chat message via notification, we don't manage to
            save that chat.
             */
            List<Chat> chatList = MessageDb.queryUnreadMessageForNotificationDisplay(context, chat.getChannelId());
            Chat localChat = MessageDb.queryById(context, chat.getId());
            Timber.i("Is chat exist: " + (localChat != null));
            if (localChat == null) {
                chatList.add(chat);
            }
            checkToAddCurrentMessageInList(chatList, chat);
            if (chatList.isEmpty()) {
                Timber.i("No unread chats for populate individual notification message.");
                return null;
            }

            for (Chat chatDt : chatList) {
                String actualContent = chatDt.getActualContent(context,
                        ConversationDb.getConversationById(context, chat.getChannelId()),
                        true);
                if (!TextUtils.isEmpty(actualContent)) {
                    Spannable message = SpecialTextRenderUtils.renderMentionUser(context,
                            actualContent,
                            false,
                            false,
                            Color.BLACK,
                            -1, -1, true,
                            null);
                    if (message != null && !TextUtils.isEmpty(message.toString())) {
                        style.addMessage(new NotificationCompat.MessagingStyle.Message(message, chatDt.getTime(), person));
                        Timber.e("create style of " + person.getName() + " with message: " + message);
                    }
                }
            }

            Timber.i(" ----- ");
        }

        return style;
    }

    private static void checkToAddCurrentMessageInList(List<Chat> existingList, Chat newChat) {
        boolean existed = false;
        for (Chat chat : existingList) {
            if (TextUtils.equals(chat.getId(), newChat.getId())) {
                existed = true;
                break;
            }
        }

        if (!existed) {
            existingList.add(newChat);
        }
    }

    @NonNull
    private static NotificationCompat.Action createAction(Context context, int notificationId, Chat chat) {
        RemoteInput remoteInput = new RemoteInput.Builder(KEY_TEXT_REPLY)
                .setLabel(context.getString(R.string.chat_notification_reply_button))
                .build();

        PendingIntent replyPendingIntent =
                PendingIntent.getBroadcast(context,
                        0,
                        RemoteNotificationBroadcast.getReplyMessageIntent(context, notificationId, chat),
                        PendingIntent.FLAG_UPDATE_CURRENT);

        return new NotificationCompat.Action.Builder(R.drawable.ic_send,
                context.getString(R.string.chat_notification_reply_button), replyPendingIntent)
                .addRemoteInput(remoteInput)
                .build();
    }

    private static class UserBitmap {
        Bitmap mMyBitmap;
        Bitmap mSenderBitmap;
    }
}
