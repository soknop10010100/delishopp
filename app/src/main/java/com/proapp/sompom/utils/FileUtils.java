package com.proapp.sompom.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import timber.log.Timber;

/**
 * Created by ChhomVeasna on 5/5/20.
 */

public final class FileUtils {

    private static final int MAX_FILE_NAME_BYTE = 255;

    private static final String TAG = FileUtils.class.getSimpleName();

    private FileUtils() {
    }

    public static void writeToFile(Context context,
                                   String fileName,
                                   String data) {
        try {
            try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(fileName,
                    Context.MODE_PRIVATE))) {
                outputStreamWriter.write(data);
            }
        } catch (IOException e) {
            Log.e(TAG, "File write failed: " + e.toString());
        }
    }

    public static String readFromFile(Context context, String fileName) {
        String ret = "";

        try {
            InputStream inputStream = context.openFileInput(fileName);

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e(TAG, "Can not read file: " + e.toString());
        }

        return ret;
    }

    public static String getFileExtensionFromFilePath(String fullFilePath) {
        if (!TextUtils.isEmpty(fullFilePath)) {
            int index = fullFilePath.lastIndexOf(".");
            if (index >= 0 && index < (fullFilePath.length() - 1)) {
                return fullFilePath.substring(index + 1);
            }
        }

        return null;
    }

    public static String getDisplayFileName(Context context, Uri uri) {

        final String[] projection = {
                MediaStore.MediaColumns.DISPLAY_NAME
        };
        try (Cursor cursor = context.getContentResolver().query(uri, projection, null, null,
                null)) {
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
                return cursor.getString(index);
            }
        }
        return null;
    }

    public static long getFileSize(Context context, Uri uri) {
        final String[] projection = {
                MediaStore.MediaColumns.SIZE
        };
        try (Cursor cursor = context.getContentResolver().query(uri, projection, null, null,
                null)) {
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.SIZE);
                return cursor.getLong(index);
            }
        }

        return 0;
    }

    public static String getFileExtension(Context context, Uri uri) {
        return MimeTypeMap.getSingleton().getExtensionFromMimeType(getFileMemeType(context, uri));
    }

    public static String getFileMemeType(Context context, Uri uri) {
        final String[] projection = {
                MediaStore.MediaColumns.MIME_TYPE
        };
        try (Cursor cursor = context.getContentResolver().query(uri, projection, null, null,
                null)) {
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.MIME_TYPE);
                return cursor.getString(index);
            }
        }

        return null;
    }

    public static String getFileNameFromUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            String fileName = url.substring(url.lastIndexOf('/') + 1);
            if (fileName.length() < MAX_FILE_NAME_BYTE) {
                return fileName;
            } else {
                //Will take only 100 character from the last of file name.
                return fileName.substring(fileName.length() - 100);
            }
        } else {
            return null;
        }
    }

    public static byte[] convertFileToByArrayFromFullFilePath(String filePath) {
        File file = new File(filePath);
        byte[] b = new byte[(int) file.length()];
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            fileInputStream.read(b);
            for (byte value : b) {
                System.out.print((char) value);
            }
        } catch (IOException e) {
            Timber.e(e);
        }

        return b;
    }

    public static boolean isFileExisted(String filePath) {
        File file = new File(filePath);
        return file.exists();
    }

    public static byte[] convertFileToByArrayFromStringUri(Context context, String uri) {
        try {
            InputStream fileInputStream = context.getContentResolver().openInputStream(Uri.parse(uri));
            byte[] bytes = getBytes(fileInputStream);
            fileInputStream.close();
            return bytes;
        } catch (IOException e) {
            Timber.e(e);
        }

        return new byte[]{};
    }

    public static byte[] convertFileToByArray(Context context, String stringUri) {
        Timber.i("convertFileToByArray");
        if (isFileExisted(stringUri)) {
            return convertFileToByArrayFromFullFilePath(stringUri);
        } else {
            return convertFileToByArrayFromStringUri(context, stringUri);
        }
    }

    public static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }
}
