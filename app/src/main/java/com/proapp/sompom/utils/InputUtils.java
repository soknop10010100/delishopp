package com.proapp.sompom.utils;

import android.text.TextUtils;

import java.util.regex.Pattern;

public class InputUtils {

    private static final String PASSWORD_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?!.*  )(?=.{8,16}).*";

    public static boolean isPasswordFormatCorrect(String password) {
        return !TextUtils.isEmpty(password) && Pattern.compile(PASSWORD_PATTERN).matcher(password).matches();
    }
}
