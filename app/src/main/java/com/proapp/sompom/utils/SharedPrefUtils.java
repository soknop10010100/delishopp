package com.proapp.sompom.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.core.app.NotificationManagerCompat;

import com.google.gson.Gson;
import com.proapp.sompom.broadcast.onesignal.OneSignalService;
import com.proapp.sompom.database.CurrencyDb;
import com.proapp.sompom.database.RealmDb;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.NotificationServiceHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.licence.db.LicenseSynchronizationDb;
import com.proapp.sompom.model.UserFeatureSetting;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.emun.AccountStatus;
import com.proapp.sompom.model.emun.ConciergeViewItemByType;
import com.proapp.sompom.model.emun.StoreStatus;
import com.proapp.sompom.model.request.QueryProduct;
import com.proapp.sompom.model.result.AllConversationDisplayDataModel;
import com.proapp.sompom.model.result.AppFeature;
import com.proapp.sompom.model.result.AppSetting;
import com.proapp.sompom.model.result.Category;
import com.proapp.sompom.model.result.NotificationSettingModel;
import com.proapp.sompom.model.result.User;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import io.realm.Realm;
import io.sentry.Sentry;
import timber.log.Timber;

/**
 * Created by he.rotha on 2/15/16.
 */
public final class SharedPrefUtils {

    public static final String CONVERSATION = "CONVERSATION";
    public static final String PRODUCT = "PRODUCT";
    public static final String USER_ID = "USER_ID";
    public static final String PATH = "PATH";
    public static final String IS_ACTIVITY = "IS_ACTIVITY";
    public static final String CATEGORY = "CATEGORY";
    public static final String STATUS = "STATUS";
    public static final String ACCOUNT_STATUS = "ACCOUNT_STATUS";
    public static final String IS_SHARE_FB = "IS_SHARE_FB";
    public static final String ID = "ID";
    public static final String DATA = "DATA";
    public static final String TITLE = "TITLE";

    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String EMAIL = "EMAIL";
    private static final String IMAGE_PATH = "IMAGE_PATH";
    private static final String FULL_USER_PROFILE = "FULL_USER_PROFILE";
    private static final String IMAGE_COVER = "IMAGE_COVER";
    private static final String STORE_NAME = "STORE_NAME";
    private static final String QUERY_PRODUCT = "QUERY_PRODUCT";
    private static final String SHARE_PREF = "SHARE_PREF";
    private static final String PHONE = "PHONE";
    public static final String COUNTRY_CODE = "COUNTRY_CODE";
    private static final String SOCIAL_ID = "SOCIAL_ID";
    private static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    private static final String LANGUAGE = "LANGUAGE";
    private static final String NOTIFICATION_SETTING = "NOTIFICATION_SETTING";
    private static final String USER_FEATURE_SETTING = "USER_FEATURE_SETTING";
    private static final String IS_PUSH_NOTIFICATION = "IS_PUSH_NOTIFICATION";
    private static final String IS_MUTE = "IS_MUTE";
    private static final String APP_SETTING = "APP_SETTING";
    public static final String AUTHORIZED_USER_LIST = "AUTHORIZED_USER_ID";
    public static final String USER_NOTIFICATION = "USER_NOTIFICATION";
    public static final String SUGGESTED_USERS = "SUGGESTED_USERS";
    public static final String ALREADY_SHOW_IN_BOARDING = "ALREADY_SHOW_IN_BOARDING";
    public static final String PRELOADED_CONVERSATION = "PRELOAD_CONVERSATION";
    public static final String IS_ENABLE_ENCRYPTION = "IS_ENABLE_ENCRYPTION";
    public static final String CONVERSATION_FIRST_LOAD_STATUS = "CONVERSATION_FIRST_LOAD_STATUS";
    public static final String GROUP_CONVERSATION_IDS = "GROUP_CONVERSATION_IDS";
    public static final String USER_TYPE = "USER_TYPE";
    public static final String DO_NOT_ASK_CALL_FLOATING_PERMISSION_AGAIN = "DO_NOT_ASK_CALL_FLOATING_PERMISSION_AGAIN";
    public static final String LAST_ACTIVE_DATE = "LAST_ACTIVE_DATE";
    public static final String HASH = "HASH";
    public static final String IS_GUEST_USER = "IS_GUEST_USER";
    public static final String IS_GUEST_USER_PERMISSION_REQUESTED = "IS_GUEST_USER_PERMISSION_REQUESTED";
    public static final String IS_SUPPORT_STAFF = "IS_SUPPORT_STAFF";

    public static final String USER_SELECTED_LOCATION = "USER_SELECTED_LOCATION";
    public static final String APP_FEATURE = "APP_FEATURE";

    public static final String SHOULD_OPEN_HOME_SCREEN_AFTER_LOGIN = "SHOULD_OPEN_HOME_SCREEN_AFTER_LOGIN";
    public static final String SELECTED_VIEW_ITEM_BY_OPTION = "SELECTED_VIEW_ITEM_BY_OPTION";

    public static final String IS_IN_EXPRESS_MODE = "IS_IN_EXPRESS_MODE";

    private SharedPrefUtils() {
    }

    public static boolean isLogin(Context context) {
        return !TextUtils.isEmpty(getUserId(context));
    }

    private static SharedPreferences getPref(Context context) {
        return context.getSharedPreferences(
                SHARE_PREF,
                Context.MODE_PRIVATE);
    }

    public static void setString(Context context, String key, String value) {
        SharedPreferences pref = getPref(context);
        pref.edit().putString(key, value).apply();
    }

    public static String getString(Context context, String key) {
        return getPref(context).getString(key, "");
    }

    public static void setBoolean(Context context, String key, boolean value) {
        SharedPreferences pref = getPref(context);
        pref.edit().putBoolean(key, value).apply();
    }

    public static boolean getBoolean(Context context, String key) {
        return getPref(context).getBoolean(key, false);
    }

    public static boolean getBoolean(Context context, String key, boolean defaultValue) {
        return getPref(context).getBoolean(key, defaultValue);
    }

    public static void setLong(Context context, String key, long value) {
        SharedPreferences pref = getPref(context);
        pref.edit().putLong(key, value).apply();
    }

    public static long getLong(Context context, String key) {
        return getPref(context).getLong(key, -1);
    }

    public static void setInt(Context context, String key, int value) {
        SharedPreferences pref = getPref(context);
        pref.edit().putInt(key, value).apply();
    }

    public static int getInt(Context context, String key) {
        return getPref(context).getInt(key, -1);
    }

    public static String getUserId(Context context) {
        if (context == null) {
            return null;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(USER_ID, null);
    }

    public static boolean checkIsMe(Context context, String id) {
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        final String myUserId = prefs.getString(USER_ID, "");
        return !TextUtils.isEmpty(myUserId) && myUserId.equals(id);
    }

    public static void setUserId(String userId, Context context) {
        if (context == null) {
            return;
        }
        if (!NotificationServiceHelper.isPushyProviderEnabled(context)) {
            OneSignalService.subscribeWithUserId(userId);
        }

        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(USER_ID, userId).apply();
    }

    public static void setUserFirstName(String firstName, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(FIRST_NAME, firstName).apply();
    }

    public static String getUserFirstName(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(FIRST_NAME, null);
    }

    public static void setUserLastName(String lastName, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(LAST_NAME, lastName).apply();
    }

    public static String getAccessToken(Context context) {
        if (context == null) {
            return null;
        }
        String token;
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        token = prefs.getString(ACCESS_TOKEN, null);
        return token;
    }

    public static void setAccessToken(String accessToken, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(ACCESS_TOKEN, accessToken).apply();
    }

    public static String getUserLastName(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(LAST_NAME, null);
    }

    public static void setUserEmail(String email, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(EMAIL, email).apply();
    }

    public static String getUserEmail(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(EMAIL, null);
    }

    public static void setImageProfileUrl(String imageUrl, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(IMAGE_PATH, imageUrl).apply();
    }

    public static String getImageProfileUrl(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(IMAGE_PATH, null);
    }

    public static void setImageCover(String imageUrl, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(IMAGE_COVER, imageUrl).apply();
    }

    public static String getImageCover(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(IMAGE_COVER, null);
    }

    public static void setPhone(String value, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(PHONE, value).apply();
    }

    public static String getPhone(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(PHONE, "");
    }

    public static void setSocialId(String value, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(SOCIAL_ID, value).apply();
    }

    public static void setLanguage(String value, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(LANGUAGE, value).apply();
    }

    /**
     * it will return km, fr, en
     */
    public static String getLanguage(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(LANGUAGE, "");
    }


    /**
     * it will return kh, fr, en
     */
    public static String getProperLanguage(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        String language = prefs.getString(LANGUAGE, "en");
        if (language.equals("km")) {
            language = "kh";
        }
        return language;
    }

    public static void setStatus(StoreStatus value, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putInt(STATUS, value.getUserStatus()).apply();
    }

    public static StoreStatus getStatus(Context context) {
        if (context == null) {
            return StoreStatus.ACTIVE;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        int value = prefs.getInt(STATUS, StoreStatus.ACTIVE.getUserStatus());
        return StoreStatus.fromValue(value);
    }

    public static void setAccountStatus(AccountStatus value, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putInt(ACCOUNT_STATUS, value.getStatus()).apply();
    }

    public static AccountStatus getAccountStatus(Context context) {
        if (context == null) {
            return AccountStatus.OPEN;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        int value = prefs.getInt(ACCOUNT_STATUS, AccountStatus.OPEN.getStatus());
        return AccountStatus.fromValue(value);
    }

    public static void setNotificationSetting(Context context, NotificationSettingModel
            notificationSettingModel) {
        if (context != null) {
            SharedPreferences prefs = context.getSharedPreferences(
                    SHARE_PREF, Context.MODE_PRIVATE);
            String data = "";
            if (notificationSettingModel != null) {
                data = new Gson().toJson(notificationSettingModel);
            }
            prefs.edit().putString(NOTIFICATION_SETTING, data).apply();
        }
    }

    public static void setUserFeatureSetting(Context context, UserFeatureSetting
            notificationSettingModel) {
        if (context != null) {
            SharedPreferences prefs = context.getSharedPreferences(
                    SHARE_PREF, Context.MODE_PRIVATE);
            String data = "";
            if (notificationSettingModel != null) {
                data = new Gson().toJson(notificationSettingModel);
            }
            prefs.edit().putString(USER_FEATURE_SETTING, data).apply();
        }
    }

    public static UserFeatureSetting getUserFeatureSetting(Context context) {
        if (context != null) {
            SharedPreferences prefs = context.getSharedPreferences(
                    SHARE_PREF, Context.MODE_PRIVATE);
            String data = prefs.getString(USER_FEATURE_SETTING, "");
            if (data.isEmpty()) {
                return new UserFeatureSetting();
            } else {
                return new Gson().fromJson(data, UserFeatureSetting.class);
            }
        } else {
            return null;
        }
    }

    public static NotificationSettingModel getNotificationSetting(Context context) {
        if (context != null) {
            SharedPreferences prefs = context.getSharedPreferences(
                    SHARE_PREF, Context.MODE_PRIVATE);
            String data = prefs.getString(NOTIFICATION_SETTING, "");
            if (data.isEmpty()) {
                return new NotificationSettingModel();
            } else {
                return new Gson().fromJson(data, NotificationSettingModel.class);
            }
        } else {
            return null;
        }
    }

    public static void setPushNotification(Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putBoolean(IS_PUSH_NOTIFICATION, false).apply();
    }

    public static void setStoreName(String value, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(STORE_NAME, value).apply();
    }

    public static String getStoreName(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(STORE_NAME, "");
    }

    public static void clearPreloadedConversation(Context context) {
        if (context != null) {
            SharedPreferences prefs = context.getSharedPreferences(
                    SHARE_PREF, Context.MODE_PRIVATE);
            prefs.edit().putString(PRELOADED_CONVERSATION, null).apply();
        }
    }

    public static void setPreloadedConversation(AllConversationDisplayDataModel allConversationDisplayDataModel, Context context) {
        if (context != null) {
            SharedPreferences prefs = context.getSharedPreferences(
                    SHARE_PREF, Context.MODE_PRIVATE);
            String data = "";
            if (allConversationDisplayDataModel != null) {
                data = new Gson().toJson(allConversationDisplayDataModel);
            }
            prefs.edit().putString(PRELOADED_CONVERSATION, data).apply();
        }
    }

    public static void setIsSupportStaff(boolean value, Context context) {
        if (context != null) {
            SharedPreferences prefs = context.getSharedPreferences(
                    SHARE_PREF, Context.MODE_PRIVATE);
            prefs.edit().putBoolean(IS_SUPPORT_STAFF, value).apply();
        }
    }

    public static boolean isSupportStaff(Context context) {
        if (context == null) {
            return false;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getBoolean(IS_SUPPORT_STAFF, false);
    }

    public static AllConversationDisplayDataModel getPreloadConversation(Context context) {
        if (context != null) {
            SharedPreferences prefs = context.getSharedPreferences(
                    SHARE_PREF, Context.MODE_PRIVATE);
            String data = prefs.getString(PRELOADED_CONVERSATION, "");
            if (data.isEmpty()) {
                return new AllConversationDisplayDataModel();
            } else {
                try {
                    return new Gson().fromJson(data, AllConversationDisplayDataModel.class);
                } catch (Exception ex) {
                    Timber.e("Failed to get preloaded conversation\n %s", ex);
                    Sentry.captureException(ex);
                    clearPreloadedConversation(context);
                    return new AllConversationDisplayDataModel();
                }
            }
        } else {
            return null;
        }
    }

    /*
        Save status for a conversation that it history has been loaded before
     */

    public static void setConversationHasFirstLoaded(Context context, String conversationId) {
        if (context != null) {
            SharedPreferences prefs = context.getSharedPreferences(SHARE_PREF, Context.MODE_PRIVATE);
            Set<String> set = prefs.getStringSet(CONVERSATION_FIRST_LOAD_STATUS, new HashSet<>());
            set.add(conversationId);
            Timber.e("Conversation has loaded for the first time: " + new Gson().toJson(set));
            prefs.edit().putStringSet(CONVERSATION_FIRST_LOAD_STATUS, set).apply();
        }
    }

    public static void clearConversationFirstLoad(Context context) {
        if (context != null) {
            SharedPreferences prefs = context.getSharedPreferences(SHARE_PREF, Context.MODE_PRIVATE);
            prefs.edit().putStringSet(CONVERSATION_FIRST_LOAD_STATUS, null).apply();
        }
    }

    public static void setIsEnableEncryption(Context context, boolean isEnableEncryption) {
        if (context != null) {
            SharedPreferences prefs = context.getSharedPreferences(SHARE_PREF, Context.MODE_PRIVATE);
            prefs.edit().putBoolean(IS_ENABLE_ENCRYPTION, isEnableEncryption).apply();
        }
    }

    public static boolean isEnableEncryption(Context context) {
        if (context == null) {
            return false;
        }
        boolean isEnableEncryption;
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        isEnableEncryption = prefs.getBoolean(IS_ENABLE_ENCRYPTION, false);
        return isEnableEncryption;
    }

    public static void clearValue(Context context) {
        if (context == null) {
            return;
        }
        if (!NotificationServiceHelper.isPushyProviderEnabled(context)) {
            OneSignalService.logout();
        }
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
        realm.close();

        LicenseSynchronizationDb.clearAllData(context);

        setUserId(null, context);
        setUserFirstName(null, context);
        setUserLastName(null, context);
        setUserEmail(null, context);
        setString(context, USER_TYPE, null);
        setImageProfileUrl(null, context);
        setString(context, FULL_USER_PROFILE, null);
        setPhone(null, context);
        setString(context, COUNTRY_CODE, null);
        setBoolean(context, IS_GUEST_USER, false);
        setSocialId(null, context);
        setStatus(StoreStatus.ACTIVE, context);
        setAccountStatus(AccountStatus.OPEN, context);
        setAccessToken(null, context);
        setImageCover(null, context);
        setNotificationSetting(context, null);
        setUserFeatureSetting(context, null);
        setPushNotification(context);
        setStoreName("", context);
        setString(context, APP_SETTING, null);
        setBoolean(context, IS_ENABLE_ENCRYPTION, false);
        clearConversationFirstLoad(context);
        clearPreloadedConversation(context);
        UserHelper.clearUserData(context);
        setString(context, GROUP_CONVERSATION_IDS, null);
        setBoolean(context, SharedPrefUtils.DO_NOT_ASK_CALL_FLOATING_PERMISSION_AGAIN, false);
        setIsSupportStaff(false, context);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.cancelAll();
        SharedPrefUtils.setInt(context, User.LAST_POST_PRIVACY, -1);
        setInt(context, HASH, 0);
        setLong(context, LAST_ACTIVE_DATE, 0);
        setUserSelectedLocation(context, null);
        setPreloadedConversation(null, context);
        setIsGuestUserPermissionRequested(context, false);
        ConciergeHelper.saveSelectedViewItemByOption(context, ConciergeViewItemByType.NORMAL);
    }

    public static void setUserValue(User user, Context context) {
        if (context == null) {
            return;
        }

        SharedPrefUtils.setUserEmail(user.getEmail(), context);
        SharedPrefUtils.setUserFirstName(user.getFirstName(), context);
        SharedPrefUtils.setUserLastName(user.getLastName(), context);
        SharedPrefUtils.setUserId(user.getId(), context);
        SharedPrefUtils.setImageProfileUrl(user.getUserProfileThumbnail(), context);
        SharedPrefUtils.setString(context, FULL_USER_PROFILE, user.getOriginalUserProfile());
        SharedPrefUtils.setPhone(user.getPhone(), context);
        SharedPrefUtils.setSocialId(user.getSocialId(), context);
        SharedPrefUtils.setStatus(user.getStatus(), context);
        SharedPrefUtils.setImageCover(user.getUserCoverProfile(), context);
        SharedPrefUtils.setStoreName(user.getStoreName(), context);
        SharedPrefUtils.setAccountStatus(user.getAccountStatus(), context);
        SharedPrefUtils.setString(context, COUNTRY_CODE, user.getCountryCode());
        SharedPrefUtils.setString(context, USER_TYPE, user.getUserType().getValue());
        SharedPrefUtils.setIsSupportStaff(user.isSupportStaff(), context);
        setBoolean(context, IS_GUEST_USER, user.isGuestUser());

        if (!TextUtils.isEmpty(user.getCurrency())) {
            CurrencyDb.updateSelectCurrency(context, user.getCurrency());
        }
        if (!TextUtils.isEmpty(user.getAccessToken())) {
            SharedPrefUtils.setAccessToken(user.getAccessToken(), context);
        }

        if (user.getNotificationSettingModel() != null) {
            setNotificationSetting(context, user.getNotificationSettingModel());
        }
        if (user.getUserFeatureSetting() != null) {
            setUserFeatureSetting(context, user.getUserFeatureSetting());
        }
        SharedPrefUtils.setInt(context, User.LAST_POST_PRIVACY, user.getLastPostPrivacy());
        setInt(context, HASH, user.getHash());
        if (user.getLastActivity() != null) {
            setLong(context, LAST_ACTIVE_DATE, user.getLastActivity().getTime());
        }
        UserHelper.setCrispUserProfile(user);
    }

    public static User getUser(Context context) {
        User user = new User();
        user.setId(SharedPrefUtils.getUserId(context));
        user.setFirstName(SharedPrefUtils.getUserFirstName(context));
        user.setLastName(SharedPrefUtils.getUserLastName(context));
        user.setUserProfileThumbnail(SharedPrefUtils.getImageProfileUrl(context));
        user.setUserCoverProfile(SharedPrefUtils.getImageCover(context));
        user.setPhone(SharedPrefUtils.getPhone(context));
        user.setCountryCode(SharedPrefUtils.getString(context, COUNTRY_CODE));
        user.setEmail(SharedPrefUtils.getUserEmail(context));
        user.setStoreName(SharedPrefUtils.getStoreName(context));
        user.setLastPostPrivacy(SharedPrefUtils.getInt(context, User.LAST_POST_PRIVACY));
        user.setHash(getInt(context, HASH));
        user.setOriginalUserProfile(getString(context, FULL_USER_PROFILE));
        user.setGuestUser(getBoolean(context, IS_GUEST_USER));
        long date = getLong(context, LAST_ACTIVE_DATE);
        if (date > 0) {
            user.setLastActivity(new Date(date));
        }

        return user;
    }

    public static void setQueryProduct(QueryProduct queryProduct, Context context) {
        if (context == null) {
            return;
        }
        if (queryProduct.getCategories() != null) {
            for (Category category : queryProduct.getCategories()) {
                category.setCheck(true);
            }
        }
        Gson gson = new Gson();
        String json = gson.toJson(queryProduct);
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(QUERY_PRODUCT, json).apply();
    }

    public static QueryProduct getQueryProduct(Context context) {
        if (context == null) {
            return new QueryProduct(context);
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        String json = prefs.getString(QUERY_PRODUCT, null);
        if (!TextUtils.isEmpty(json)) {
            Gson gson = new Gson();
            return gson.fromJson(json, QueryProduct.class);
        } else {
            return new QueryProduct(context);
        }
    }

    public static void setMuteVolume(Context context, boolean isMute) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putBoolean(IS_MUTE, isMute).apply();
    }

    public static boolean isMuteVolume(Context context) {
        if (context == null) {
            return false;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getBoolean(IS_MUTE, false);
    }

    public static User getCurrentUser(Context context) {
        User user = new User();
        user.setId(SharedPrefUtils.getUserId(context));
        user.setFirstName(SharedPrefUtils.getUserFirstName(context));
        user.setLastName(SharedPrefUtils.getUserLastName(context));
        user.setUserProfileThumbnail(SharedPrefUtils.getImageProfileUrl(context));
        return user;
    }

    public static void setAppSetting(Context context, AppSetting appSetting) {
        if (appSetting.getWelcomeData() == null) {
            appSetting.setWelcomeData(UserHelper.getLocalWelcomeData(context));
        }
        setString(context, APP_SETTING, new Gson().toJson(appSetting));
    }

    public static AppSetting getAppSetting(Context context) {
        return new Gson().fromJson(getString(context, APP_SETTING), AppSetting.class);
    }

    public static void setUserSelectedLocation(Context context, ConciergeUserAddress userAddress) {
        if (context == null) {
            return;
        }
        setString(context, USER_SELECTED_LOCATION, new Gson().toJson(userAddress));
    }

    public static ConciergeUserAddress getUserSelectedLocation(Context context) {
        return new Gson().fromJson(getString(context, USER_SELECTED_LOCATION), ConciergeUserAddress.class);
    }

    public static void setAppFeature(Context context, AppFeature appFeature) {
        if (context == null) {
            return;
        }
        ApplicationHelper.injectUpdateHomeExpressIcon(appFeature);
        setString(context, APP_FEATURE, new Gson().toJson(appFeature));
    }

    public static void clearAppFeature(Context context) {
        setString(context, APP_FEATURE, null);
    }

    public static void setShouldOpenHomeScreenAfterLogin(Context context, boolean shouldOpen) {
        if (context == null) {
            return;
        }
        setBoolean(context, SHOULD_OPEN_HOME_SCREEN_AFTER_LOGIN, shouldOpen);
    }

    public static boolean getShouldOpenHomeScreenAfterLogin(Context context) {
        return getBoolean(context, SHOULD_OPEN_HOME_SCREEN_AFTER_LOGIN);
    }

    public static boolean isGuestUserPermissionRequested(Context context) {
        return getBoolean(context, IS_GUEST_USER_PERMISSION_REQUESTED);
    }

    public static void setIsGuestUserPermissionRequested(Context context, boolean isPrompt) {
        if (context == null) {
            return;
        }
        setBoolean(context, IS_GUEST_USER_PERMISSION_REQUESTED, isPrompt);
    }
}
