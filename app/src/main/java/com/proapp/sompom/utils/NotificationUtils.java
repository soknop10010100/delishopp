package com.proapp.sompom.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.Person;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.graphics.drawable.IconCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.proapp.sompom.R;
import com.proapp.sompom.database.NotificationIdDb;
import com.proapp.sompom.helper.ColorResourceHelper;
import com.proapp.sompom.helper.DeviceHelper;
import com.proapp.sompom.helper.LocalNotificationDispatchHelper;
import com.proapp.sompom.helper.NotificationChannelHelper;
import com.proapp.sompom.helper.WorkingTimeNotificationHelper;
import com.proapp.sompom.intent.newintent.HomeIntent;
import com.proapp.sompom.model.NotificationChannelSetting;
import com.proapp.sompom.model.notification.Notification;
import com.proapp.sompom.model.notification.NotificationActor;
import com.proapp.sompom.model.notification.NotificationProfileDisplayAdaptive;
import com.proapp.sompom.newui.HomeActivity;
import com.proapp.sompom.viewmodel.binding.ImageViewBindingUtil;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Random;

import io.reactivex.Observable;


/**
 * Created by Veasna Chhom on 5/31/21.
 */
public final class NotificationUtils {

    public static final String REJECTION_SYNCHRONIZE = "rejection_synchronize";
    public static final String NOTIFICATION_ID = "notification_id";
    public static final String DISMISS_NOTIFICATION_ACTION = "DISMISS_NOTIFICATION_ACTION";

    private NotificationUtils() {
    }

    public static int getNotificationIcon() {
        return R.drawable.ic_notification;
    }

    public static Observable<Bitmap> downloadBitmap(Context context,
                                                    String url,
                                                    String firstName,
                                                    String lastName) {
        return Observable.create(e -> {
            try {
                if (TextUtils.isEmpty(url)) {
                    e.onNext(getDefaultProfileByName(context, firstName, lastName));
                } else {
                    FutureTarget<Bitmap> futureTarget = Glide
                            .with(context)
                            .asBitmap()
                            .load(url)
                            .apply(new RequestOptions()
                                    .centerCrop()
                                    .dontAnimate()
                                    .circleCrop())
                            .submit(300, 300);

                    Bitmap bitmap = futureTarget.get();
                    e.onNext(bitmap);
                }
                e.onComplete();
            } catch (Exception ex) {
                e.onNext(getDefaultProfileByName(context, firstName, lastName));
                e.onComplete();
            }
        });
    }

    public static void loadNotificationProfile(Context context,
                                               NotificationProfileDisplayAdaptive adaptive,
                                               LoadNotificationProfilePhotoCallback callback) {
        Bitmap defaultProfileByName = getDefaultProfileByName(context,
                adaptive.getDisplayFirstName(),
                adaptive.getDisplayLastName());
        Glide.with(context)
                .asBitmap()
                .load(adaptive.getDisplayProfileUrl())
                .apply(new RequestOptions()
                        .centerCrop()
                        .dontAnimate()
                        .circleCrop())
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull @NotNull Bitmap resource,
                                                @Nullable @org.jetbrains.annotations.Nullable Transition<? super Bitmap> transition) {
                        if (callback != null) {
                            callback.onLoadFinished(resource);
                        }
                    }

                    @Override
                    public void onLoadCleared(@Nullable @org.jetbrains.annotations.Nullable Drawable placeholder) {

                    }

                    @Override
                    public void onLoadFailed(@Nullable @org.jetbrains.annotations.Nullable Drawable errorDrawable) {
                        if (callback != null) {
                            callback.onLoadFinished(defaultProfileByName);
                        }
                    }
                });
    }

    private static Bitmap getDefaultProfileByName(Context context, String firstName, String lastName) {
        Drawable drawable = ImageViewBindingUtil.getTextDrawable(context, firstName, lastName);
        drawable = (DrawableCompat.wrap(drawable)).mutate();
        Bitmap bitmap = Bitmap.createBitmap(300,
                300, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static void pushGeneralNotification(Context context,
                                               String title,
                                               String content,
                                               Intent intent,
                                               NotificationChannelSetting channelSetting,
                                               Notification notification,
                                               boolean enableBigTextStyle,
                                               long when) {
        NotificationActor notificationActor = notification.getActors().get(0);
        NotificationUtils.loadNotificationProfile(context,
                notificationActor,
                profile -> {
                    checkToCreateNotificationChannel(context, channelSetting);
                    Random rand = new Random();
                    final int notificationId = rand.nextInt(99999) + 1;
                    PendingIntent pendingIntent;
                    if (intent != null) {
                        pendingIntent = PendingIntent.getActivity(context,
                                notificationId,
                                intent,
                                PendingIntent.FLAG_UPDATE_CURRENT);
                    } else {
                        pendingIntent = getNoneRedirectionNotificationPendingIntent(context, notificationId);
                    }
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(context,
                            getNotificationChannelId(context, channelSetting))
                            .setSmallIcon(NotificationUtils.getNotificationIcon())
                            .setColor(ColorResourceHelper.getAccentColor(context))
                            .setStyle(getGeneralNotificationMessageStyle(profile, content, when, notificationActor))
                            .setContentTitle(title)
                            .setContentInfo(content)
                            .setContentText(content)
                            .setPriority(getPriority(context, channelSetting))
                            .setContentIntent(pendingIntent)
                            .setVibrate(getVibration(context, channelSetting))
                            .setSound(getNotificationSound(context, channelSetting))
                            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                            .setAutoCancel(true);

                    if (when != -1) {
                        builder.setWhen(when);
                    }

                    if (enableBigTextStyle) {
                        builder.setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(content)
                                .setBigContentTitle(title));
                    }

                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
                    notificationManager.notify(notificationId, builder.build());
                });
    }

    public static void checkToCreateNotificationChannel(Context context, NotificationChannelSetting channelSetting) {
        //For Android version O and higher only
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (WorkingTimeNotificationHelper.isValidToDispatchLocalNotification(context)) {
                if (DeviceHelper.shouldPlaySoundOnTheDevice(context)) {
                    createNotificationChannelInternally(context, channelSetting);
                } else if (DeviceHelper.shouldVibrateTheDevice(context)) {
                    createNotificationChannelInternally(context, NotificationChannelSetting.Vibrate);
                } else {
                    createNotificationChannelInternally(context, NotificationChannelSetting.Silent);
                }
            } else {
                createNotificationChannelInternally(context, NotificationChannelSetting.Silent);
            }
        }
    }

    public static void checkToCreateNotificationChannelDirectly(Context context, NotificationChannelSetting channelSetting) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannelInternally(context, channelSetting);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private static void createNotificationChannelInternally(Context context, NotificationChannelSetting channelSetting) {
        NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
        if (notificationManager.getNotificationChannel(channelSetting.getLatestNotificationChannelId()) == null) {
//            Timber.i("Will create new notification channel of " + channelSetting.getLatestNotificationChannelId());
            NotificationChannelHelper.deleteAllPreviousChatChannelVersion(context, channelSetting.getBaseNotificationChannelId());

            LocalNotificationDispatchHelper.NotificationData notificationData = LocalNotificationDispatchHelper.getNotificationData(context,
                    channelSetting.getBaseNotificationChannelId());
            NotificationChannel channel =
                    new NotificationChannel(channelSetting.getLatestNotificationChannelId(),
                            context.getString(channelSetting.getNotificationName()),
                            notificationData.getImportance());
            channel.setVibrationPattern(notificationData.getVibrationPattern());
            if (!TextUtils.isEmpty(notificationData.getSoundUri())) {
                AudioAttributes att = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build();
                channel.setSound(parseNotificationSound(notificationData.getSoundUri()),
                        att);
            } else {
                channel.setSound(null, null);
            }
            Objects.requireNonNull(notificationManager).createNotificationChannel(channel);
        }
    }

    private static Uri parseNotificationSound(String uri) {
//        return Uri.parse(uri);
        //Will only use default notification sound from system
        return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    }

    public static void pushGeneralSynchronizeNotification(Context context, String title, String content) {
        Random rand = new Random();
        final int notificationId = rand.nextInt(99999) + 1;
        checkToCreateNotificationChannel(context, NotificationChannelSetting.FeatureSyn);
        Intent intent1 = new HomeIntent(context);
        intent1.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, notificationId, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent dismissIntent = getDismissIntent(context, notificationId);
        PendingIntent rejectIntent = getRejectIntent(context, notificationId);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,
                getNotificationChannelId(context, NotificationChannelSetting.FeatureSyn))
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(NotificationUtils.getNotificationIcon())
                .setColor(ColorResourceHelper.getAccentColor(context))
                .setPriority(getPriority(context, NotificationChannelSetting.FeatureSyn))
                .setContentIntent(pendingIntent)
                .setVibrate(getVibration(context, NotificationChannelSetting.FeatureSyn))
                .setSound(getNotificationSound(context, NotificationChannelSetting.FeatureSyn))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .addAction(R.drawable.ic_check_black_24dp, "yes", dismissIntent)
                .addAction(R.drawable.ic_close_black_24dp, "no", rejectIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(notificationId, builder.build());
    }

    public static PendingIntent getDismissIntent(Context context, int notificationId) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.putExtra("notificationYes", notificationId);
        intent.putExtra("sync", 10);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent dismissIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return dismissIntent;
    }

    public static PendingIntent getRejectIntent(Context context, int notificationId) {
        Intent intent = new Intent(REJECTION_SYNCHRONIZE);
        intent.putExtra(NOTIFICATION_ID, notificationId);
        return PendingIntent.getBroadcast(context, 3, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static PendingIntent getNoneRedirectionNotificationPendingIntent(Context context, int notificationId) {
        Intent dismissIntent = new Intent(DISMISS_NOTIFICATION_ACTION);
        dismissIntent.putExtra(NOTIFICATION_ID, notificationId);
        return PendingIntent.getBroadcast(context, notificationId, dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static int getPriority(Context context, NotificationChannelSetting channelSetting) {
        if (!WorkingTimeNotificationHelper.isValidToDispatchLocalNotification(context) ||
                DeviceHelper.isRingingModeSilent(context)) {
            return NotificationCompat.PRIORITY_LOW;
        } else if (DeviceHelper.isRingingModeVibrate(context)) {
            return NotificationCompat.PRIORITY_DEFAULT;
        } else {
            LocalNotificationDispatchHelper.NotificationData notificationData = LocalNotificationDispatchHelper.getNotificationData(context,
                    channelSetting.getBaseNotificationChannelId());
            return notificationData.getPriority();
        }
    }

    public static Uri getNotificationSound(Context context, NotificationChannelSetting channelSetting) {
        if (!WorkingTimeNotificationHelper.isValidToDispatchLocalNotification(context) ||
                DeviceHelper.isRingingModeSilent(context) ||
                DeviceHelper.isRingingModeVibrate(context)) {
            return null;
        }

        LocalNotificationDispatchHelper.NotificationData notificationData = LocalNotificationDispatchHelper.getNotificationData(context,
                channelSetting.getBaseNotificationChannelId());
        return parseNotificationSound(notificationData.getSoundUri());
    }

    public static String getNotificationChannelId(Context context, NotificationChannelSetting channelSetting) {
        if (!WorkingTimeNotificationHelper.isValidToDispatchLocalNotification(context) ||
                DeviceHelper.isRingingModeSilent(context)) {
            return NotificationChannelSetting.Silent.getLatestNotificationChannelId();
        } else if (DeviceHelper.isRingingModeVibrate(context)) {
            return NotificationChannelSetting.Vibrate.getLatestNotificationChannelId();
        } else {
            return channelSetting.getLatestNotificationChannelId();
        }
    }

    public static long[] getVibration(Context context, NotificationChannelSetting channelSetting) {
        if (!WorkingTimeNotificationHelper.isValidToDispatchLocalNotification(context) ||
                DeviceHelper.isRingingModeSilent(context)) {
            return LocalNotificationDispatchHelper.NONE_VIBRATION;
        }

        LocalNotificationDispatchHelper.NotificationData notificationData = LocalNotificationDispatchHelper.getNotificationData(context,
                channelSetting.getBaseNotificationChannelId());
        return notificationData.getVibrationPattern();
    }

    public static void cancelNotificationByLocalId(Context context, String notificationID) {
        final NotificationManagerCompat manager = NotificationManagerCompat.from(context);
        manager.cancel(NotificationIdDb.getId(context, notificationID));
    }

    public static void cancelNotificationById(Context context, int id) {
        NotificationManagerCompat manager = NotificationManagerCompat.from(context);
        manager.cancel(id);
    }

    public static NotificationCompat.Style getGeneralNotificationMessageStyle(Bitmap bitmap,
                                                                              String message,
                                                                              long time,
                                                                              NotificationActor actor) {
        Person sender = new Person.Builder()
                .setName(actor.getFullName())
                .setKey(actor.getId())
                .setBot(false)
                .setImportant(false)
                .setIcon(IconCompat.createWithBitmap(bitmap))
                .build();
        NotificationCompat.MessagingStyle style = new NotificationCompat.MessagingStyle(sender);
        style.addMessage(new NotificationCompat.MessagingStyle.Message(message, time, sender));

        return style;
    }

    public static void testPushNotification(Context context) {
        String title = "Test Local Push";
        String message = "Test notification of " + context.getString(R.string.app_name);
        NotificationChannelSetting channelSetting = NotificationChannelSetting.Home;
        int notificationId = 1992;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,
                getNotificationChannelId(context, channelSetting))
                .setSmallIcon(NotificationUtils.getNotificationIcon())
                .setColor(ColorResourceHelper.getAccentColor(context))
                .setContentTitle(title)
                .setContentInfo(message)
                .setContentText(message)
                .setPriority(getPriority(context, channelSetting))
                .setContentIntent(getNoneRedirectionNotificationPendingIntent(context, notificationId))
                .setVibrate(getVibration(context, channelSetting))
                .setSound(getNotificationSound(context, channelSetting))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(notificationId, builder.build());
    }

    public interface LoadNotificationProfilePhotoCallback {
        void onLoadFinished(Bitmap profile);
    }
}