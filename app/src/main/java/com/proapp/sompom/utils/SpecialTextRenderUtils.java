package com.proapp.sompom.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.text.ParcelableSpan;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Patterns;
import android.view.View;

import androidx.annotation.NonNull;

import com.example.usermentionable.mentions.MentionSpan;
import com.example.usermentionable.utils.RenderTextAsMentionable;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.DelayClickableSpan;
import com.proapp.sompom.helper.NavigateSellerStoreHelper;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.intent.newintent.SearchMessageIntent;
import com.proapp.sompom.model.result.User;
import com.resourcemanager.helper.FontHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import timber.log.Timber;

/**
 * Created by nuonveyo on 11/14/18.
 */

public final class SpecialTextRenderUtils {

    private static final String PATTER_HAST_TAG = "[#]+[\\p{Alnum}\\p{M}*+_]+\\b";
    private static final String PATTER_USER_MENTION = "@\\[(\\S*):(.*?)]"; //"@[5dba51c31bbfb52a2f1b809b:Chhaykoung Hay]"

    private SpecialTextRenderUtils() {
    }

    public static Spannable renderMentionUser(Context context,
                                              String content,
                                              boolean isRenderHashTag,
                                              boolean isRenderLink,
                                              int mentionTextColor,
                                              int linkTextColor,
                                              int hashtagTextColor,
                                              boolean isDisplayAtSign,
                                              RenderTextListener listener) {
        Matcher matcher;
        SpannableStringBuilder builder;
        Pattern pattern = Pattern.compile(PATTER_USER_MENTION);

        // Null check in case content is null.
        try {
            builder = new SpannableStringBuilder(content);
            matcher = pattern.matcher(content);
        } catch (Exception e) {
            SentryHelper.logSentryError(e);
            Timber.e(e);
            // Do not exit the function if content is null, continue with empty message instead to
            // preserve functionality of tapping on reply message
            builder = new SpannableStringBuilder("");
            matcher = pattern.matcher("");
        }

        while (matcher.find()) {
            final String substring = matcher.group();
            String[] parseUserMentionPattern = parseUserMentionPattern(substring, isDisplayAtSign);
            if (parseUserMentionPattern.length == 2) {
                StringBuilder userName = new StringBuilder(RenderTextAsMentionable
                        .getValidDisplayName(parseUserMentionPattern[1]));
                final String userId = parseUserMentionPattern[0];

                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(userName);
                ClickableSpan clickableSpan = new DelayClickableSpan() {
                    @Override
                    public void onDelayClick(@NonNull View textView) {
                        Timber.e("open userId: " + userName.toString() + " " + userId);
                        User user = new User();
                        user.setStoreName(userName.toString());
                        user.setId(userId);
                        if (listener == null) {
                            new NavigateSellerStoreHelper(context, user).openSellerStore();
                        } else {
                            listener.onUserMentionClicked(context, user);
                        }
                    }

                    @Override
                    public void updateDrawState(@NonNull TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                        ds.setColor(mentionTextColor);
                    }
                };
                spannableStringBuilder.setSpan(clickableSpan,
                        0,
                        userName.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                Typeface typefaceBold = FontHelper.getBoldFontStyle(context);
                spannableStringBuilder.setSpan(new CustomTypefaceSpan("", typefaceBold),
                        0,
                        userName.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                builder.replace(matcher.start(), matcher.end(), spannableStringBuilder);
                matcher.reset(builder.toString());
            }
        }

        if (isRenderHashTag) {
            return renderHashTag(context, hashtagTextColor, linkTextColor, builder, isRenderLink);
        } else if (isRenderLink) {
            return renderLinkOrEmail(context, linkTextColor, builder);
        } else {
            return builder;
        }
    }

    private static Spannable renderHashTag(Context context,
                                           int hashtagColor,
                                           int linkColor,
                                           SpannableStringBuilder builder,
                                           boolean isRenderLink) {
        Pattern tagPatter = Pattern.compile(PATTER_HAST_TAG);
        Matcher tagMatcher = tagPatter.matcher(builder.toString());

        while (tagMatcher.find()) {
            String substring = tagMatcher.group();
            ClickableSpan clickableSpan = new DelayClickableSpan() {
                @Override
                public void onDelayClick(@NonNull View textView) {
                    //ToastUtil.showToast(context, substring);
                    context.startActivity(new SearchMessageIntent(context, substring));
                }

                @Override
                public void updateDrawState(@NonNull TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                    int color = hashtagColor;
                    if (hashtagColor == -1) {
                        //Set the default color
                        color = AttributeConverter.convertAttrToColor(context, R.attr.colorFirstText);
                    }
                    ds.setColor(color);
                }
            };
            builder.setSpan(clickableSpan,
                    tagMatcher.start(),
                    tagMatcher.end(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            Typeface typefaceBold = FontHelper.getSemiBoldFontStyle(context);
            builder.setSpan(new CustomTypefaceSpan("", typefaceBold),
                    tagMatcher.start(),
                    tagMatcher.end(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        }

        if (!isRenderLink) {
            return builder;
        } else {
            return renderLinkOrEmail(context, linkColor, builder);
        }
    }

    private static Spannable renderLinkOrEmail(Context context, int linkTextColor, SpannableStringBuilder builder) {
        String text = builder.toString();
        try {
            String replaceText = text.replace("\n", " ");
            String[] splitText = replaceText.split(" ");
            for (String value : splitText) {
                if (isEmailAddress(value)) {
                    buildSpannableForLinkOrEmail(context, builder, text, value, linkTextColor, true);
                } else if (GenerateLinkPreviewUtil.isValidUrl(value)) {
                    buildSpannableForLinkOrEmail(context, builder, text, value, linkTextColor, false);
                }
            }
        } catch (Exception e) {
            Timber.e("renderLinkOrEmail error: %s", e.getMessage());
        }

        return builder;
    }

    public static boolean isContainEmail(CharSequence content) {
        String replaceText = content.toString().replace("\n", " ");
        String[] splitText = replaceText.split(" ");
        for (String value : splitText) {
            if (isEmailAddress(value)) {
                return true;
            }
        }

        return false;
    }

    private static void buildSpannableForLinkOrEmail(Context context,
                                                     SpannableStringBuilder builder,
                                                     String content,
                                                     String value,
                                                     int linkTextColor,
                                                     boolean isEmail) {
        int start = content.indexOf(value);
        int end = content.indexOf(value) + value.length();
        if (isEmail) {
            builder.setSpan(new DelayClickableSpan() {
                                @Override
                                public void onDelayClick(@NonNull View widget) {
                                    IntentUtil.openEmail(context, value);
                                }

                                @Override
                                public void updateDrawState(@NonNull TextPaint ds) {
                                    super.updateDrawState(ds);
                                    //To remove the link underline
                                    ds.setUnderlineText(false);
                                    int color = linkTextColor;
                                    if (linkTextColor == -1) {
                                        color = AttributeConverter.convertAttrToColor(context, R.attr.colorAccent);
                                    }
                                    ds.setColor(color);
                                }
                            },
                    start,
                    end,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            builder.setSpan(new DelayClickableSpan() {
                                @Override
                                public void onDelayClick(@NonNull View widget) {
                                    WallStreetHelper.openCustomTab(context, value);
                                }

                                @Override
                                public void updateDrawState(@NonNull TextPaint ds) {
                                    super.updateDrawState(ds);
                                    //To remove the link underline
                                    ds.setUnderlineText(false);
                                    int color = linkTextColor;
                                    if (linkTextColor == -1) {
                                        color = AttributeConverter.convertAttrToColor(context, R.attr.colorAccent);
                                    }
                                    ds.setColor(color);
                                }
                            },
                    start,
                    end,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        Typeface typefaceBold = FontHelper.getSemiBoldFontStyle(context);
        builder.setSpan(new CustomTypefaceSpan("", typefaceBold),
                start,
                end,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    public static Spannable renderDescriptionWithSeeMore(Context context,
                                                         CharSequence charSequence,
                                                         int endIndexOfLastDisplayLine) {
        String seeMore = context.getString(R.string.post_description_see_more);
        String moreText = "... " + seeMore;

        SpannableStringBuilder builder;
        int validMoreTextLength = (moreText.length());

        CharSequence subCharSequence = charSequence.subSequence(0, (endIndexOfLastDisplayLine - validMoreTextLength));
        builder = new SpannableStringBuilder(subCharSequence);
        builder.append(moreText);

        try {
            builder.setSpan(new ForegroundColorSpan(AttributeConverter.convertAttrToColor(context, R.attr.post_description_seemore)),
                    builder.length() - seeMore.length(),
                    builder.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            Timber.e("renderDescriptionWithSeeMore: %s", e.getMessage());
        }

        return builder;
    }

    public static boolean isContainSpecialInput(CharSequence text, boolean isEditMode) {
        if (isContainEmail(text)) {
            return true;
        }

        if (isContainLink(text)) {
            return true;
        }

        if (isContainHasTag(text)) {
            return true;
        }

        if (isEditMode) {
            return isContainMentionDuringInput(text);
        } else {
            return isContainMentionFromPostText(text);
        }
    }

    public static boolean isContainLink(CharSequence text) {
        return !TextUtils.isEmpty(text) && !TextUtils.isEmpty(GenerateLinkPreviewUtil.getPreviewLink(text.toString()));
    }

    public static boolean isContainHasTag(CharSequence text) {
        if (!TextUtils.isEmpty(text)) {
            Pattern pattern = Pattern.compile(PATTER_HAST_TAG);
            Matcher matcher = pattern.matcher(text);
            boolean found = false;
            while (matcher.find()) {
                if (found) {
                    break;
                }
                found = true;
            }

            return found;
        }

        return false;
    }

    public static boolean isContainMentionDuringInput(CharSequence text) {
        if (!TextUtils.isEmpty(text) && text instanceof Spanned) {
            MentionSpan[] mentionSpans = ((Spanned) text).getSpans(0, text.length(),
                    MentionSpan.class);
            return mentionSpans.length > 0;
        }

        return false;
    }

    public static boolean isContainMentionFromPostText(CharSequence text) {
        if (!TextUtils.isEmpty(text)) {
            Pattern pattern = Pattern.compile(PATTER_USER_MENTION);
            Matcher matcher = pattern.matcher(text);
            boolean found = false;
            while (matcher.find()) {
                if (found) {
                    break;
                }
                found = true;
            }

            return found;
        }

        return false;

    }

    public static boolean isMentionedYouInChat(Context context, CharSequence text) {
        if (!TextUtils.isEmpty(text)) {
            Pattern pattern = Pattern.compile(PATTER_USER_MENTION);
            Matcher matcher = pattern.matcher(text);
            String currentUserId = SharedPrefUtils.getUserId(context);
            while (matcher.find()) {
                final String substring = matcher.group();
                String[] parseUserMentionPattern = parseUserMentionPattern(substring,
                        false);
                final String userId = parseUserMentionPattern[0];
                if (TextUtils.equals(userId, currentUserId)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static List<ParcelableSpan> buildHighLightBackgroundKeyWordSpan(Spannable text,
                                                                           String key,
                                                                           int highLightColor,
                                                                           Integer highLightFontColor) {
        List<ParcelableSpan> highLightSpannableList = new ArrayList<>();
        String checkingContent = text.toString().toLowerCase();
        int fromIndex = 0;
        while (true) {
            int findIndex = checkingContent.indexOf(key.toLowerCase(), fromIndex);
//            Timber.i("text: " + text + ", fromIndex: " + fromIndex + ", findIndex: " + findIndex + ", key: " + key);
            if (findIndex >= 0) {
                //Add high light font color if any
                if (highLightFontColor != null) {
                    ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(highLightFontColor);
                    highLightSpannableList.add(foregroundColorSpan);
                    text.setSpan(foregroundColorSpan,
                            findIndex,
                            findIndex + key.length(),
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }

                //Add high light background color
                BackgroundColorSpan backgroundColorSpan = new BackgroundColorSpan(highLightColor);
                highLightSpannableList.add(backgroundColorSpan);
                text.setSpan(backgroundColorSpan,
                        findIndex,
                        findIndex + key.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                fromIndex = (findIndex + key.length());
            } else {
                break;
            }
        }

        return highLightSpannableList;
    }

    public static boolean isEmailAddress(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static class RenderTextListener {

        public void onUserMentionClicked(Context context, User user) {
            new NavigateSellerStoreHelper(context, user).openSellerStore();
        }
    }

    public static String[] parseUserMentionPattern(String userMentionPattern, boolean isDisplayAtSignWithUserName) {
        //Mention user name pattern:  "@[5dba51c31bbfb52a2f1b809b:Chhaykoung Hay]"
        String userIdToken = "@[";
        String[] split = userMentionPattern.split(":");
        String userId = split[0].substring(split[0].indexOf(userIdToken) + userIdToken.length()).trim();
        String userName = split[1].substring(0, split[1].length() - 1).trim();
        if (isDisplayAtSignWithUserName) {
            userName = "@" + userName;
        }
        return new String[]{userId, userName};
    }
}
