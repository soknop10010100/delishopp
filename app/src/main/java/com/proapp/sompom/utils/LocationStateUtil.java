package com.proapp.sompom.utils;

import android.content.Context;
import android.location.LocationManager;

/**
 * Created by He Rotha on 9/27/17.
 */

public final class LocationStateUtil {

    private static final int MAX_ADDRESS_INDEX = 3;

    private LocationStateUtil() {
    }

    public static boolean isLocationEnable(Context context) {
        LocationManager service = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return service.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || service.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

    }


    public static String getOnlyAddressFromFullPlace(String address) {
        /*
            So we will take only 3 maximum separated values by comma of full address.
            Ex: "Takhmao Market, Ta Khmau, Phnom Penh, Cambodia" full address and the return will be
            "Ta Khmau, Phnom Penh, Cambodia".
         */
        String[] split = address.split(",");
        StringBuilder validAddress = new StringBuilder();
        int startIndex = split.length - MAX_ADDRESS_INDEX;
        if (startIndex >= 0) {
            for (int i = startIndex; i < split.length; i++) {
                if (validAddress.length() > 0) {
                    validAddress.append(", ");
                }
                validAddress.append(split[i]);
            }
        } else {
            return address;
        }

        return validAddress.toString();
    }

    public static String getPlaceNameFromFullAddress(String address) {
        /*
          Ex: 1. Takhmao Market, Ta Khmau, Phnom Penh, Cambodia => Takhmao Market
          2. 2nd, Takhmao Market, Ta Khmau, Phnom Penh, Cambodia => 2nd, Takhmao Market
          3.  Ta Khmau, Phnom Penh, Cambodia =>  Ta Khmau
         */
        String[] split = address.split(",");
        if (split.length > MAX_ADDRESS_INDEX) {
            int remain = split.length - MAX_ADDRESS_INDEX;
            StringBuilder placeName = new StringBuilder();
            for (int i = 0; i < remain; i++) {
                if (placeName.length() > 0) {
                    placeName.append(", ");
                }
                placeName.append(split[i]);
            }
            return placeName.toString();
        } else {
            return address.substring(0, address.indexOf(',')).trim();
        }
    }

    public static String makeAddressValidation(String address) {
        return address.replace("\n", " ");
    }
}
