package com.proapp.sompom.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;

import androidx.exifinterface.media.ExifInterface;

import com.desmond.squarecamera.utils.media.FileNameUtil;
import com.desmond.squarecamera.utils.media.ImageSaver;
import com.desmond.squarecamera.utils.media.VideoSaver;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;

import timber.log.Timber;

/**
 * Created by desmond on 24/10/14.
 */
public final class MediaUtil {

    private static final String IMAGE = "image";
    private static final String VIDEO = "video";

    private MediaUtil() {
    }

    public static long getAudioDuration(String path) {
        String mediaPath = Uri.parse(path).getPath();
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(mediaPath);
        String duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        mmr.release();

        if (TextUtils.isEmpty(duration)) {
            return 0;
        }
        return Long.parseLong(duration);
    }

    public static int[] getRemoteVideoResolution(String url) {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(url, new HashMap<String, String>());
        String width = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
        String height = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
        int[] value = new int[]{Integer.parseInt(width), Integer.parseInt(height)};
        mmr.release();

        return value;
    }


    public static long getSecondFromMili(long mili) {
        return (long) Math.round(((float) mili / 1000f));
    }

    public static String getVideoDurationDisplayFormat(long timeSecond) {
        int[] durations = MediaUtil.splitToComponentTimes(timeSecond);
        if (durations[0] > 0) {
            return String.format(Locale.getDefault(),
                    "%02d:%02d:%02d",
                    durations[0], durations[1], durations[2]);
        }

        return String.format(Locale.getDefault(),
                "%02d:%02d",
                durations[1], durations[2]);
    }

    public static int[] splitToComponentTimes(long timeSecond) {
        int hours = (int) timeSecond / 3600;
        int remainder = (int) timeSecond - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        int[] ints = {hours, mins, secs};
        return ints;
    }

    public static Media getMediaFromOtherApp(Context context, Uri uri) {
        if (uri == null) {
            return null;
        }

        return getLocalMediaFromUri(context, uri);
    }

    public static boolean isHttpUrl(String url) {
        return URLUtil.isHttpsUrl(url) || URLUtil.isHttpUrl(url);
    }

    public static Media getLocalImageMediaFromUri(Context context, Uri uri) {
        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver != null) {
            String[] columns = {MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media.WIDTH,
                    MediaStore.Images.Media.HEIGHT};
            Cursor cursor = contentResolver.query(uri, columns,
                    null,
                    null,
                    null);
            cursor.moveToFirst();
            String realPath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA));
            int width = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.WIDTH));
            int height = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.HEIGHT));

            Media media = new Media();
            media.setWidth(width);
            media.setHeight(height);
            media.setUrl(realPath);

            cursor.close();

            return media;
        }

        return null;
    }

    public static Media getLocalMediaFromUri(Context context, Uri uri) {
        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver != null) {
            String[] firstQuery = getRealPathAndMimeTypeFromURI(context, uri);
            if (TextUtils.isEmpty(firstQuery[0]) || TextUtils.isEmpty(firstQuery[1])) {
                return getLocalMediaFileFromSaving(context, uri);
            }

            String realPath = firstQuery[0];
            String mimeType = firstQuery[1];
            MediaType mediaType = MediaType.IMAGE;
            String[] columns = {MediaStore.Files.FileColumns.WIDTH, MediaStore.Files.FileColumns.HEIGHT,};
            if (mimeType.startsWith(VIDEO)) {
                columns = new String[]{MediaStore.Files.FileColumns.WIDTH,
                        MediaStore.Files.FileColumns.HEIGHT,
                        MediaStore.Files.FileColumns.DURATION};
                mediaType = MediaType.VIDEO;
            }

            Cursor cursor = contentResolver.query(uri,
                    columns,
                    null,
                    null,
                    null);
            cursor.moveToFirst();
            Media media = new Media();
            int width = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.WIDTH));
            int height = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.HEIGHT));
            if (mediaType == MediaType.VIDEO) {
                int durationIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.DURATION);
                int duration = 0;
                if (durationIndex >= 0) {
                    duration = cursor.getInt(durationIndex);
                }
                /*
                The retrieved duration format is in milli second and we have to convert it to second.
                */
                duration = duration / 1000;
                if (duration < 0) {
                    duration = 0;
                }
                media.setDuration(duration);
            }

            media.setWidth(width);
            media.setHeight(height);
            media.setUrl(realPath);
            media.setType(mediaType);

            cursor.close();

            return media;
        }

        return null;
    }

    public static String[] getRealPathAndMimeTypeFromURI(Context context, Uri contentUri) {
        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver != null) {
            try {
                String[] columns = {MediaStore.Files.FileColumns.DATA, MediaStore.Files.FileColumns.MIME_TYPE};
                Cursor cursor = contentResolver.query(contentUri,
                        columns,
                        null,
                        null,
                        null);
                int realPathColumnIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA);
                int mimeTypeColumnIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.MIME_TYPE);
                cursor.moveToFirst();
                String realPath = null;
                String mimeType = null;
                if (realPathColumnIndex >= 0) {
                    realPath = cursor.getString(realPathColumnIndex);
                }
                if (mimeTypeColumnIndex >= 0) {
                    mimeType = cursor.getString(mimeTypeColumnIndex);
                }
                cursor.close();
                return new String[]{realPath, mimeType};
            } catch (Exception exception) {
                Timber.e(exception);
            }
        }

        return new String[]{};
    }

    private static Media getLocalMediaFileFromSaving(Context context, Uri uri) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
        if (!TextUtils.isEmpty(extension)) {
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            if (!TextUtils.isEmpty(mimeType)) {
                if (mimeType.toLowerCase().startsWith(VIDEO)) {
                    return getLocalVideoMediaFileFromSaving(context, uri);
                } else {
                    return getLocalImageMediaFileFromSaving(context, uri);
                }
            }
        }

        return null;
    }

    public static Media getLocalImageMediaFileFromSaving(Context context, Uri uri) {
        try {
            InputStream input = context.getContentResolver().openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            String file = FileNameUtil.getDirectory(context, true) +
                    File.separator +
                    FileNameUtil.convertNameFromUrl(uri.toString());
            String realPath = ImageSaver.with(context)
                    .load(bitmap)
                    .into(file)
                    .resize(false)
                    .setBroadcast(false)
                    .execute();
            ExifInterface exif = new ExifInterface(realPath);
            int width = exif.getAttributeInt(ExifInterface.TAG_IMAGE_WIDTH, 0);
            int height = exif.getAttributeInt(ExifInterface.TAG_IMAGE_LENGTH, 0);
            Media media = new Media();
            media.setType(MediaType.IMAGE);
            media.setUrl(realPath);
            media.setWidth(width);
            media.setHeight(height);

            return media;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Media getLocalVideoMediaFileFromSaving(Context context, Uri uri) {
        try {
            InputStream input = context.getContentResolver().openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            String file = FileNameUtil.getDirectory(context, true) +
                    File.separator +
                    FileNameUtil.convertNameFromUrl(uri.toString());
            String realPath = VideoSaver.with(context)
                    .load(bitmap)
                    .into(file)
                    .setBroadcast(false)
                    .execute();
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(realPath, new HashMap<String, String>());
            String width = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
            String height = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
            String duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            mmr.release();

            Media media = new Media();
            media.setType(MediaType.VIDEO);
            media.setUrl(realPath);
            media.setWidth(Integer.parseInt(width));
            media.setHeight(Integer.parseInt(height));
            media.setDuration(Integer.parseInt(duration));

            return media;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Media checkIfMediaExist(Media toCheck) {
        if (toCheck != null && !TextUtils.isEmpty(toCheck.getDownloadedPath())) {
            File file = new File(toCheck.getDownloadedPath());
            if (!file.exists()) {
                // Reset status if file does not locally exist to allow for re-download
                toCheck.setDownloadedPath(null);
                toCheck.resetFileStatusType();
            } else {
                toCheck.setFileStatusType(Media.FileStatusType.DOWNLOADED);
            }
        }
        return toCheck;
    }
}
