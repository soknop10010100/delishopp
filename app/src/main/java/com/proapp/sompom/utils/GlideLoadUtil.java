package com.proapp.sompom.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.proapp.sompom.R;
import com.proapp.sompom.glide.SvgSoftwareLayerSetter;
import com.proapp.sompom.helper.GlideApp;
import com.proapp.sompom.helper.GlideRequest;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.viewmodel.binding.ImageViewBindingUtil;

import java.io.File;

import jp.wasabeef.glide.transformations.BlurTransformation;
import timber.log.Timber;

/**
 * Created by he.rotha on 5/18/16.
 */
public final class GlideLoadUtil {

    private static final String GIFT = "gif";
    private static final int BLUR_RADIUS = 25;

    private GlideLoadUtil() {
    }

    public static boolean isGiftFile(String url) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        return !TextUtils.isEmpty(extension) && TextUtils.equals(extension.toLowerCase(), GIFT);
    }

    public static void preCacheImage(Context context, String url, GlideLoadUtilListener listener) {
        if (!isValidContext(context)) {
            return;
        }

        GlideApp.with(context)
                .load(url)
                .addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e,
                                                Object model,
                                                Target<Drawable> target,
                                                boolean isFirstResource) {
                        if (listener != null) {
                            listener.onLoadFailed(e, false);
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource,
                                                   Object model,
                                                   Target<Drawable> target,
                                                   DataSource dataSource,
                                                   boolean isFirstResource) {
//                        Timber.i("onResourceReady: getIntrinsicWidth: " +
//                                resource.getIntrinsicWidth() + "getIntrinsicHeight: "
//                                + resource.getIntrinsicHeight() + ", url: " + url);
                        if (listener != null) {
                            listener.onLoadSuccess(resource, isFirstResource);
                        }
                        return false;
                    }
                })
                .preload();
    }

    public static void loadLinkPreviewResource(ImageView imageView,
                                               String url,
                                               Drawable loadedResource,
                                               Drawable placeHolder,
                                               boolean isCenterCrop) {
        if (isValidContext(imageView.getContext())) {
            if (isGiftFile(url)) {
                //For gif file, we will have to load the url directly to make the animation work properly.
                loadDefault(imageView, url, placeHolder, false, null);
            } else {
                GlideRequest<Drawable> request = GlideApp.with(imageView).load(loadedResource);
                if (isCenterCrop) {
                    request.centerCrop().into(imageView);
                } else {
                    request.fitCenter().into(imageView);
                }
            }
        }
    }

    public static void loadDefault(ImageView imageView,
                                   String url,
                                   Drawable placeHolder,
                                   boolean isCenterCrop,
                                   GlideLoadUtilListener listener) {
        if (!isValidContext(imageView.getContext())) {
            return;
        }

        GlideApp.with(imageView).clear(imageView);
        GlideRequest<Drawable> request = GlideApp.with(imageView.getContext())
                .load(url);
        if (isGiftFile(url)) {
            request.placeholder(placeHolder)
                    .addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e,
                                                    Object model,
                                                    Target<Drawable> target,
                                                    boolean isFirstResource) {
                            if (listener != null) {
                                listener.onLoadFailed(e, false);
                            }
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource,
                                                       Object model,
                                                       Target<Drawable> target,
                                                       DataSource dataSource, boolean isFirstResource) {
                            if (listener != null) {
                                listener.onLoadSuccess(resource, false);
                            }
                            return false;
                        }
                    })
                    .error(placeHolder)
                    .into(imageView);
        } else {
            if (isCenterCrop) {
                request = request.centerCrop();
            } else {
                request = request.fitCenter();
            }
            request = request.placeholder(placeHolder).error(placeHolder);
            if (placeHolder != null) {
                imageView.setImageDrawable(placeHolder);
            }
            request = request.addListener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e,
                                            Object model,
                                            Target<Drawable> target,
                                            boolean isFirstResource) {
                    if (listener != null) {
                        listener.onLoadFailed(e, false);
                    }
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource,
                                               Object model,
                                               Target<Drawable> target,
                                               DataSource dataSource, boolean isFirstResource) {
                    new Handler(Looper.getMainLooper()).post(() -> loadDrawable(imageView, resource, listener));
                    return false;
                }
            });
            request.submit();
        }
    }

    private static void loadDrawable(ImageView imageView, Drawable drawable, GlideLoadUtilListener listener) {
        GlideApp.with(imageView)
                .load(drawable)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        if (listener != null) {
                            listener.onLoadFailed(e, false);
                        }

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        if (listener != null) {
                            listener.onLoadSuccess(resource, false);
                        }

                        return false;
                    }
                })
                .into(imageView);
    }

    public static boolean isSVGImageUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            String fileExtensionFromUrl = MimeTypeMap.getFileExtensionFromUrl(url);
            return !TextUtils.isEmpty(fileExtensionFromUrl) &&
                    TextUtils.equals(fileExtensionFromUrl.toLowerCase(),
                            ImageViewBindingUtil.SVG_EXTENSION.toLowerCase());
        }

        return false;
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(),
                    Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static GlideRequest<PictureDrawable> createLoadSVGRequest(Context context,
                                                                     String url,
                                                                     Drawable placeHolder,
                                                                     int overwriteWidth,
                                                                     int overwriteHeight,
                                                                     boolean isCenterCrop) {
        if (!isValidContext(context)) {
            return null;
        }

        Uri uri = Uri.parse(url);
        GlideRequest<PictureDrawable> request = GlideApp.with(context)
                .as(PictureDrawable.class)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .placeholder(placeHolder)
                .error(placeHolder)
                .load(uri);

        if (overwriteWidth < 0) {
            overwriteWidth = 0;
        }
        if (overwriteHeight < 0) {
            overwriteHeight = 0;
        }
        if (overwriteWidth > 0 || overwriteHeight > 0) {
            request = request.override(overwriteWidth, overwriteHeight);
        }
        if (isCenterCrop) {
            request = request.centerCrop();
        }

        return request;
    }

    public static void loadImageWithSVGFileSupport(ImageView imageView,
                                                   String url,
                                                   Drawable placeHolder,
                                                   int overwriteWidth,
                                                   int overwriteHeight,
                                                   boolean isCenterCrop,
                                                   boolean isNeedPresetPlaceholder,
                                                   GlideLoadUtilListener listener) {
        if (!isValidContext(imageView.getContext())) {
            return;
        }

        if (!TextUtils.isEmpty(url)) {
            if (isNeedPresetPlaceholder) {
                imageView.setImageDrawable(placeHolder);
            }

            if (isSVGImageUrl(url)) {
                //SVG file url
                Timber.i("Will load file SVG image url.");
                GlideRequest<PictureDrawable> request = createLoadSVGRequest(imageView.getContext(),
                        url,
                        placeHolder,
                        overwriteWidth,
                        overwriteHeight,
                        isCenterCrop);
                if (request == null) {
                    return;
                }

                request = request.listener(new SvgSoftwareLayerSetter() {
                    @Override
                    public boolean onResourceReady(PictureDrawable resource,
                                                   Object model,
                                                   Target<PictureDrawable> target,
                                                   DataSource dataSource,
                                                   boolean isFirstResource) {
                        Timber.e("onResourceReady SVG");
                        if (imageView.getContext() instanceof Activity) {
                            ((Activity) imageView.getContext()).runOnUiThread(() ->
                                    imageView.setImageDrawable(resource));
                        }

                        if (listener != null) {
                            listener.onLoadSuccess(resource, true);
                        }

                        return super.onResourceReady(resource, model, target, dataSource, isFirstResource);
                    }

                    @Override
                    public boolean onLoadFailed(GlideException e,
                                                Object model,
                                                Target<PictureDrawable> target,
                                                boolean isFirstResource) {
                        Timber.e("Load SVG image failed: " + e.getMessage());
                        if (listener != null) {
                            listener.onLoadFailed(e, true);
                        }

                        return super.onLoadFailed(e, model, target, isFirstResource);
                    }
                });

                request.submit();
            } else {
                GlideRequest<Drawable> request = GlideApp.with(imageView)
                        .load(url)
                        .error(placeHolder)
                        .placeholder(placeHolder);

                if (overwriteWidth < 0) {
                    overwriteWidth = 0;
                }
                if (overwriteHeight < 0) {
                    overwriteHeight = 0;
                }
                if (overwriteWidth > 0 || overwriteHeight > 0) {
                    request = request.override(overwriteWidth, overwriteHeight);
                }
                if (isCenterCrop) {
                    request = request.centerCrop();
                }

                request = request.addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        if (listener != null) {
                            listener.onLoadFailed(e, false);
                        }

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        if (listener != null) {
                            listener.onLoadSuccess(resource, false);
                        }

                        return false;
                    }
                });

                request.into(imageView);
            }
        } else {
            imageView.setImageDrawable(placeHolder);
        }
    }

    public static void loadDefaultLinkPreviewLogo(ImageView imageView,
                                                  final String url,
                                                  Drawable placeHolder,
                                                  GlideLoadUtilListener listener) {
        if (!isValidContext(imageView.getContext())) {
            return;
        }

        Timber.i("loadDefaultLinkPreviewLogo: url: " + url);
        if (!TextUtils.isEmpty(url)) {
            if (isSVGImageUrl(url)) {
                //SVG file url
                Timber.i("Will load file SVG image url.");
                Uri uri = Uri.parse(url);
                GlideApp.with(imageView)
                        .as(PictureDrawable.class)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .load(uri)
                        .listener(new SvgSoftwareLayerSetter() {
                            @Override
                            public boolean onResourceReady(PictureDrawable resource,
                                                           Object model,
                                                           Target<PictureDrawable> target,
                                                           DataSource dataSource,
                                                           boolean isFirstResource) {
                                Timber.i("loadDefaultLinkPreviewLogo: onResourceReady SVG of " + url);
                                if (listener != null) {
                                    listener.onLoadSuccess(resource, true);
                                }

                                return super.onResourceReady(resource, model, target, dataSource, isFirstResource);
                            }

                            @Override
                            public boolean onLoadFailed(GlideException e,
                                                        Object model,
                                                        Target<PictureDrawable> target,
                                                        boolean isFirstResource) {
                                Timber.e("loadDefaultLinkPreviewLogo: Load SVG image failed: " + e.getMessage() + " of url: " + url);
                                if (listener != null) {
                                    listener.onLoadFailed(e, true);
                                }

                                return super.onLoadFailed(e, model, target, isFirstResource);
                            }
                        }).submit();
            } else {
                GlideRequest<Drawable> request = GlideApp.with(imageView.getContext())
                        .load(url);
                request = request.placeholder(placeHolder).error(placeHolder);
                request = request.addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e,
                                                Object model,
                                                Target<Drawable> target,
                                                boolean isFirstResource) {
                        Timber.e("loadDefaultLinkPreviewLogo: onLoadFailed: " + e + " of url: " + url);
                        if (listener != null) {
                            listener.onLoadFailed(e, false);
                        }

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource,
                                                   Object model,
                                                   Target<Drawable> target,
                                                   DataSource dataSource,
                                                   boolean isFirstResource) {
                        Timber.i("loadDefaultLinkPreviewLogo: onResourceReady of url: " + url);
                        if (listener != null) {
                            listener.onLoadSuccess(resource, false);
                        }
                        return false;
                    }
                });
                request.submit();
            }
        } else {
            imageView.setImageDrawable(placeHolder);
        }
    }

    public static void load(ImageView imageView, String url, Drawable placeHolder) {
        if (!isValidContext(imageView.getContext())) {
            return;
        }

        GlideApp.with(imageView)
                .load(url)
                .placeholder(placeHolder)
                .error(placeHolder)
                .into(imageView);
    }

    public static void loadCircle(ImageView imageView, String url, Drawable placeHolder) {
        if (isValidContext(imageView.getContext())) {
//            Timber.i("loadCircle: " + url + ", placeHolder: " + placeHolder);
            GlideApp.with(imageView)
                    .load(url)
                    .placeholder(placeHolder)
                    .error(placeHolder)
                    .circleCrop()
                    .into(imageView);
        }
    }

    public static void loadCircle2(Context context, ImageView imageView, String url, Drawable placeHolder) {
        if (isValidContext(context)) {
            GlideApp.with(context)
                    .load(url)
                    .placeholder(placeHolder)
                    .error(placeHolder)
                    .circleCrop()
                    .into(imageView);
        }
    }

    public static void loadFromResource(ImageView imageView, int resourceId) {
        if (!isValidContext(imageView.getContext())) {
            return;
        }

        GlideApp.with(imageView.getContext())
                .load(resourceId)
                .into(imageView);
    }

    public static void load(ImageView imageView, String url) {
        if (!isValidContext(imageView.getContext())) {
            return;
        }

        try {
            if (!TextUtils.isEmpty(url)) {
                if (!url.startsWith("http")) {
                    GlideApp.with(imageView)
                            .load(new File(url))
                            .centerCrop()
                            .override(0, 500)
                            .into(imageView);
                } else {
                    GlideApp.with(imageView)
                            .load(url)
                            .override(0, 500)
                            .centerCrop()
                            .into(imageView);
                }
            }
        } catch (IllegalArgumentException ex) {
            Timber.e(ex);
            SentryHelper.logSentryError(ex);
        }
    }

    public static void load(ImageView imageView, String url, boolean isCenterCrop) {
        if (!isValidContext(imageView.getContext())) {
            return;
        }

        try {

            if (!TextUtils.isEmpty(url)) {
                if (!url.startsWith("http")) {
                    if (isCenterCrop) {
                        GlideApp.with(imageView)
                                .load(new File(url))
                                .centerCrop()
                                .override(0, 500)
                                .into(imageView);
                    } else {
                        GlideApp.with(imageView)
                                .load(new File(url))
                                .override(0, 500)

                                .into(imageView);
                    }
                } else {
                    if (isCenterCrop) {
                        GlideApp.with(imageView)
                                .load(url)
                                .centerCrop()
                                .override(0, 500)

                                .into(imageView);
                    } else {
                        GlideApp.with(imageView)
                                .load(url)
                                .override(0, 500)
                                .into(imageView);
                    }
                }
            }
        } catch (IllegalArgumentException ex) {
            Timber.e(ex);
            SentryHelper.logSentryError(ex);
        }
    }

    public static void loadChatDetailImage(ImageView imageView, String url, GlideLoadUtilListener listener) {
        if (!isValidContext(imageView.getContext())) {
            return;
        }

        Drawable placeHolder = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_400dp);
        loadImageWithSVGFileSupport(imageView,
                url,
                placeHolder,
                0,
                0,
                false,
                true,
                listener);
    }

    public static void setChatMultiImageView(ImageView imageView, String url, int size, GlideLoadUtilListener listener) {
        if (!isValidContext(imageView.getContext())) {
            return;
        }

        Drawable placeHolder = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_400dp);
        loadImageWithSVGFileSupport(imageView,
                url,
                placeHolder,
                0,
                size,
                false,
                true,
                listener);
    }

    public static int[] getRemoteImageResolution(Context context, String url) {
        int[] resolution = new int[]{-1, -1}; //[0]=width,[1]=height
        FutureTarget<Bitmap> submit = GlideApp.with(context)
                .asBitmap()
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.NONE)//No need to cache since we just need the size.
                .submit();
        try {
            Bitmap bitmap = submit.get();
            if (bitmap != null && !bitmap.isRecycled()) {
                resolution[0] = bitmap.getWidth();
                resolution[1] = bitmap.getHeight();
                bitmap.recycle();
            }
        } catch (Exception e) {
            Timber.e(e);
            SentryHelper.logSentryError(e);
        }

        return resolution;
    }

    public static void loadBlurImage(Context context, ImageView imageView, String url, Drawable placeHolder) {
        if (TextUtils.isEmpty(url)) {
            loadDrawableBlurImage(context, imageView, placeHolder);
        } else {
            GlideApp.with(context)
                    .load(url)
                    .transform(new BlurTransformation(BLUR_RADIUS))
                    .addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e,
                                                    Object model,
                                                    Target<Drawable> target,
                                                    boolean isFirstResource) {
                            loadDrawableBlurImage(context, imageView, placeHolder);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource,
                                                       Object model,
                                                       Target<Drawable> target,
                                                       DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(imageView);
        }
    }

    public static boolean isValidContext(Context context) {
        if (context != null) {
            if (context instanceof Activity) {
                return !((Activity) context).isDestroyed() && !((Activity) context).isFinishing();
            }

            return true;
        }

        return false;
    }

    public static void loadDrawableBlurImage(Context context, ImageView imageView, Drawable image) {
        GlideApp.with(context)
                .load(image)
                .transform(new BlurTransformation(BLUR_RADIUS))
                .into(imageView);
    }

    public static class GlideLoadUtilListener {

        public void onLoadFailed(GlideException ex, boolean isSVG) {
            //Empty Impl...
        }

        public void onLoadSuccess(Drawable resource, boolean isSVG) {
            //Empty Impl...
        }
    }
}
