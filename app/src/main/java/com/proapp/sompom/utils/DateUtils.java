package com.proapp.sompom.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.proapp.sompom.R;
import com.resourcemanager.helper.LocaleManager;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;


/**
 * Created by Dora Khiev on 9/2/15.
 */
@SuppressWarnings("DefaultFileTemplate")
public final class DateUtils {

    public static final String TAG = DateUtils.class.getName();
    public static final int MILLISECONDS_MULTIPLIER = 1000;

    //Date format
    public static final String DATE_FORMAT_1 = "MMM dd,yyyy";
    public static final String DATE_FORMAT_2 = "yyyy/MM/dd";
    public static final String DATE_FORMAT_3 = "yyyy-MM-dd";
    public static final String DATE_FORMAT_4 = "MM/dd/yyyy";
    public static final String DATE_FORMAT_5 = "dd/MM/yyyy";
    public static final String DATE_FORMAT_6 = "MM/dd/yy";
    public static final String DATE_FORMAT_7 = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_8 = "dd MMM. yyyy";
    public static final String DATE_FORMAT_9 = "dd MMM yy";
    public static final String DATE_FORMAT_10 = "dd-MM-yyyy";
    public static final String DATE_FORMAT_11 = "yyyy-MM-dd_HH_mm_ss";
    public static final String DATE_FORMAT_12 = "yyyy-MMM-dd";
    public static final String DATE_FORMAT_13 = "HH:mm, MMM dd,yyyy";
    public static final String DATE_FORMAT_14 = "HH:mm";
    public static final String DATE_FORMAT_15 = "hh:mm a";
    public static final String DATE_FORMAT_16 = "EEE";
    public static final String DATE_FORMAT_17 = "MMM dd";
    public static final String DATE_FORMAT_18 = "EEEE"; //Display full name in week.
    public static final String DATE_FORMAT_19 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_FORMAT_20 = "MMMM yyyy";
    public static final String DATE_FORMAT_21 = "MMMM";
    public static final String DATE_FORMAT_22 = "MM.dd.yyyy";
    public static final String DATE_FORMAT_23 = "dd/MM/yyyy hh:mm a";
    public static final String DATE_FORMAT_24 = "E MMM dd";
    public static final String DATE_FORMAT_25 = "E dd MMM yyyy";
    public static final String DATE_FORMAT_26 = "MMM dd, yyyy h:mm a";
    public static final String DATE_FORMAT_27 = "yyyy-MM-dd HH:mm:ss";

    public static final String CONCIERGE_DELIVERY_TODAY_TOMORROW_FORMAT = ", hh:mm a";
    public static final String CONCIERGE_DELIVERY_OTHER_DAY_WITH_TIME_FORMAT = "dd MMM, hh:mm a";
    public static final String CONCIERGE_DELIVERY_OTHER_DAY_FORMAT = "dd MMM";
    public static final String CONCIERGE_TRACKING_DATE_FORMAT = DATE_FORMAT_23;
    public static final String CONCIERGE_DELIVERY_TRACKING_DATE_FORMAT = "EEEE dd, MMMM yyyy";

    public static final String CONCIERGE_DELIVERY_TODAY_TOMORROW_FORMAT_FR = ", HH:mm";
    public static final String CONCIERGE_DELIVERY_OTHER_DAY_WITH_TIME_FORMAT_FR = "dd MMM HH:mm";
    public static final String CONCIERGE_DELIVERY_OTHER_DAY_FORMAT_FR = "dd MMM";
    public static final String CONCIERGE_TRACKING_DATE_FORMAT_FR = "dd MM yyyy HH:mm";
    public static final String CONCIERGE_START_TIME_SLOT_FORMAT = "h";
    public static final String CONCIERGE_END_TIME_SLOT_FORMAT = "ha";
    public static final String CONCIERGE_SUPPLIER_ESTIMATE_DELIVERY_TIME_FORMAT = "h:mma";

    private DateUtils() {
        //no called
    }

    public static String getUTCDateTimeFormat(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                Locale.getDefault());
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat.format(date);
    }

    /**
     * Convert date value in long into the format we want
     *
     * @param dateValue date value in miliseconds
     * @return date with format in String
     */
    @SuppressLint("LogNotTimber")
    public static String getDateWithFormat(long dateValue) {
        try {
            Date dateInput = new Date(dateValue);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtils.DATE_FORMAT_3, Locale.getDefault());
            return simpleDateFormat.format(dateInput);
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    public static String getDateWithFormatWithoutLocale(long dateValue) {
        try {
            Date dateInput = new Date(dateValue);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtils.DATE_FORMAT_3);
            return simpleDateFormat.format(dateInput);
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    @SuppressLint("LogNotTimber")
    public static String getDateWithFormat(Context context,
                                           long dateValue,
                                           String formatOutput,
                                           @NonNull Locale locale) {
        try {
            Date dateInput = new Date(dateValue);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatOutput, locale);
            DateFormatSymbols formatSymbols = new DateFormatSymbols();
            formatSymbols.setAmPmStrings(new String[]{context.getString(R.string.common_timestamp_am_symbol),
                    context.getString(R.string.common_timestamp_pm_symbol)});
            simpleDateFormat.setDateFormatSymbols(formatSymbols);
            return simpleDateFormat.format(dateInput);
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    @SuppressLint("LogNotTimber")
    public static Date getDateFromStringDate(String dateString) {
        if (dateString == null || dateString.isEmpty()) {
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtils.DATE_FORMAT_3, Locale.getDefault());
        Date date;
        try {
            date = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            Log.d(TAG, e.getMessage());
            date = null;
        }
        return date;
    }

    @SuppressLint("LogNotTimber")
    public static Date getDateFromStringDateWithSpecificFormat(String dateString, String format) {
        if (dateString == null || dateString.isEmpty()) {
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        Date date;
        try {
            date = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            Log.d(TAG, e.getMessage());
            date = null;
        }
        return date;
    }

    public static int compareDateWithoutTimestamp(long dateMilli1, long dateMilli2) {
        Date date1 = getEmptyTimestampDate(dateMilli1);
        Date date2 = getEmptyTimestampDate(dateMilli2);

        return date1.compareTo(date2);
    }

    private static Date getEmptyTimestampDate(long dateMilli) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateMilli);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static Date getDateFromValues(int year, int month, int dayOfMonth) {
        String dateString = year + "-" + month + "-" + dayOfMonth;
        return getDateFromStringDate(dateString);
    }

    public static String getDateStringWithSpecificFormat(Date date) {
        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar.get(Calendar.YEAR) + "-"
                    + (calendar.get(Calendar.MONTH) + 1) + "-"
                    + calendar.get(Calendar.DAY_OF_MONTH);
        }
        return null;
    }

    private static Date getNewDateWithFormat3(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return DateUtils.getDateFromValues(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH) + 1,
                calendar.get(Calendar.DAY_OF_MONTH));
    }

    @SuppressLint("LogNotTimber")
    public static int getDayBetweenSpecificDateAndCurrentDate(Date currentDate, Date specificDate) {
        long diff = currentDate.getTime() - specificDate.getTime();
        Log.d(TAG, "Diff = " + diff);
        int day = (int) (diff / (1000 * 60 * 60 * 24));
        Log.d(TAG, "Day = " + day);
        return day;
    }

    @SuppressLint("LogNotTimber")
    public static int getDayBetweenSpecificDateWithFormat3(Date currentDate, Date specificDate) {
        Date newCurrentDate = getNewDateWithFormat3(currentDate);
        Date newSpecificDate = getNewDateWithFormat3(specificDate);
        long diff = newCurrentDate.getTime() - newSpecificDate.getTime();
        Log.d(TAG, "Diff = " + diff);
        int day = (int) (diff / (1000 * 60 * 60 * 24));
        Log.d(TAG, "Day = " + day);
        return day;
    }

    @SuppressLint("LogNotTimber")
    public static boolean isAgeIsEqualOrMoreThanSpecificAge(Date currentDate, Date birthDate, int compareAge) {
        if (currentDate == null || birthDate == null) {
            return false;
        }
        int age = DateUtils.getDayBetweenSpecificDateAndCurrentDate(currentDate, birthDate) / 365;
        Log.d(TAG, "Age = " + age);
        return age >= compareAge;
    }

    public static String getDateToDisplay(long second) {
        Date date = new Date(second * 1000);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_3, Locale.getDefault());
        return simpleDateFormat.format(date);
    }

    public static String parseToZeroTimeZoneDate(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_19, Locale.getDefault());
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat.format(date);
    }

    /**
     * Default format is yyyy-MM-dd
     */
    public static String getDefaultStringDateFormat(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_3, Locale.getDefault());
        return simpleDateFormat.format(date);
    }

    public static String getDefaultStringDateFormat(Date date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        return simpleDateFormat.format(date);
    }

    public static long getMilliSecondBetweenDate(long previousMilli, long currentMilli) {
        return currentMilli - previousMilli;
    }

    public static boolean isToDay(long milli) {
        return android.text.format.DateUtils.isToday(milli);
    }

    public static boolean isEqual(long dateMillSecond1, long dateMillSecond2) {
        String dateString1 = DateUtils.getDateWithFormatWithoutLocale(dateMillSecond1);
        String dateString2 = DateUtils.getDateWithFormatWithoutLocale(dateMillSecond2);

//        Timber.i("dateString1: %s", dateString1);
//        Timber.i("dateString2: %s", dateString2);

        return dateString1 != null && dateString1.equalsIgnoreCase(dateString2);
    }

    public static boolean isDateEqual(long second1, long second2) {
        String dateString1 = DateUtils.getDateWithFormat(getMillisecondFromSecond(second1));
        String dateString2 = DateUtils.getDateWithFormat(getMillisecondFromSecond(second2));

        return dateString1 != null && dateString1.equalsIgnoreCase(dateString2);
    }

    public static Date getDateFromSecond(long second) {
        return new Date(second * MILLISECONDS_MULTIPLIER);
    }

    @SuppressLint("LogNotTimber")
    public static Date getDateFromSecond(long second, String format) {
        try {
            Date date = new Date(second * MILLISECONDS_MULTIPLIER);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
            String dateString = simpleDateFormat.format(date);
            Log.d(TAG, dateString);
            return simpleDateFormat.parse(dateString);
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    @SuppressLint("LogNotTimber")
    public static Date getDateFromMilliSecond(long milliseconds) {
        try {
            Date date = new Date(milliseconds);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtils.DATE_FORMAT_3,
                    Locale.getDefault());
            String dateString = simpleDateFormat.format(date);
            Log.d(TAG, dateString);
            return simpleDateFormat.parse(dateString);
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    public static long getSecondFromDate(@NonNull Date date) {
        return date.getTime() / MILLISECONDS_MULTIPLIER;
    }

    public static long getMillisecondFromSecond(long second) {
        return second * MILLISECONDS_MULTIPLIER;
    }

    @SuppressLint("LogNotTimber")
    public static String getDateStringFromSecond(long second, String formatOutput) {
        try {
            Date dateInput = new Date(second * MILLISECONDS_MULTIPLIER);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatOutput, Locale.getDefault());
            return simpleDateFormat.format(dateInput);
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    public static Date getDateFromDefaultLocale(String dateString, int style) {
        try {
            return DateFormat.getDateInstance(style).parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getDefaultLocaleFormat(Date date) {
        return DateFormat.getDateInstance(DateFormat.MEDIUM).format(date);
    }

    public static String getDefaultLocaleFormatFromSecond(long second) {
        return DateFormat.getDateInstance(DateFormat.MEDIUM).format(getDateFromSecond(second));
    }

    public static String getDefaultLongDateFormat(Date date) {
        return DateFormat.getDateInstance(DateFormat.LONG).format(date);
    }

    public static long getSecondFromDefaultLocaleFormat(String dateString) {
        Date date = getDateFromDefaultLocale(dateString, DateFormat.MEDIUM);
        if (date != null) {
            return getSecondFromDate(date);
        } else {
            throw new RuntimeException("Can not parse string to date.");
        }
    }

    public static long getMilliSecondFromDefaultLocaleFormat(String dateString) {
        Date date = getDateFromDefaultLocale(dateString, DateFormat.MEDIUM);
        if (date != null) {
            return getSecondFromDate(date);
        } else {
            throw new RuntimeException("Can not parse string to date.");
        }
    }


    public static Date finAddingDate(Date currentDate, int value) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(Calendar.DAY_OF_MONTH, value);
        return calendar.getTime();
    }

    public static boolean isDateIsInCurrentWeekOfMonth(long checkDateMilli, Locale locale) {
        Date checkDate = getDateFromMilliSecond(checkDateMilli);
        Calendar currentCalendar = Calendar.getInstance(locale);
//        Date currentDate = currentCalendar.getTime();

//        Timber.i("currentCalendar.getFirstDayOfWeek(): %s", currentCalendar.getFirstDayOfWeek());

        currentCalendar.set(Calendar.DAY_OF_WEEK, currentCalendar.getFirstDayOfWeek());
        Date firstDayOfWeek = getDateFromMilliSecond(currentCalendar.getTimeInMillis());
//        Timber.i("firstDayOfWeekL: %s", Objects.requireNonNull(firstDayOfWeek).toString());

        currentCalendar.add(Calendar.DAY_OF_MONTH, 6);
        Date lastDayOfWeek = getDateFromMilliSecond(currentCalendar.getTimeInMillis());
//        Timber.i("lastDayOfWeek: %s", Objects.requireNonNull(lastDayOfWeek).toString());

//        Timber.i("Week: " + currentCalendar.get(Calendar.WEEK_OF_MONTH) + ", Start: " + firstDayOfWeek.toString() +
//                ", Last: " + lastDayOfWeek.toString() + ", Current: " + currentDate.toString() +
//                ", checkDate: " + Objects.requireNonNull(checkDate).toString());

        return Objects.requireNonNull(checkDate).getTime() >= Objects.requireNonNull(firstDayOfWeek).getTime() &&
                checkDate.getTime() <= Objects.requireNonNull(lastDayOfWeek).getTime();
    }

    public static String getConciergeDeliveryTodayTomorrowFormat(Context context) {
        return SharedPrefUtils.getLanguage(context).equals(LocaleManager.FRENCH_LANGUAGE_KEY)
                ? CONCIERGE_DELIVERY_TODAY_TOMORROW_FORMAT_FR
                : CONCIERGE_DELIVERY_TODAY_TOMORROW_FORMAT;
    }

    public static String getConciergeDeliveryOtherDayFormat(Context context) {
        return SharedPrefUtils.getLanguage(context).equals(LocaleManager.FRENCH_LANGUAGE_KEY)
                ? CONCIERGE_DELIVERY_OTHER_DAY_FORMAT_FR
                : CONCIERGE_DELIVERY_OTHER_DAY_FORMAT;
    }

    public static String getConciergeDeliveryOtherDayWithTimeFormat(Context context) {
        return SharedPrefUtils.getLanguage(context).equals(LocaleManager.FRENCH_LANGUAGE_KEY)
                ? CONCIERGE_DELIVERY_OTHER_DAY_WITH_TIME_FORMAT_FR
                : CONCIERGE_DELIVERY_OTHER_DAY_WITH_TIME_FORMAT;
    }
}
