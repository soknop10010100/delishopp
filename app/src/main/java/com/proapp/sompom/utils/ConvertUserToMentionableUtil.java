package com.proapp.sompom.utils;

import android.content.Context;
import android.text.TextUtils;

import com.example.usermentionable.model.UserMentionable;
import com.proapp.sompom.model.emun.StoreStatus;
import com.proapp.sompom.model.request.Mention;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ResponseHandleErrorFunc;
import com.proapp.sompom.services.ApiService;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by nuonveyo on 12/17/18.
 */

public final class ConvertUserToMentionableUtil {
    private ConvertUserToMentionableUtil() {
    }

    public static Observable<List<UserMentionable>> getUserMentionList(Context context, ApiService apiService, String userName) {
        final Mention mention = new Mention(userName);
        return apiService.getUserMentionList(mention)
                .concatMap(new ResponseHandleErrorFunc<>(context, false))
                .concatMap(listResponse -> {
                    List<UserMentionable> userMentionables = new ArrayList<>();
                    List<User> userList = listResponse.body();
                    if (userList != null && !userList.isEmpty()) {
                        for (User user : userList) {
                            if (!TextUtils.isEmpty(userName)) {
                                if (user.getFullName().toLowerCase().contains(userName.toLowerCase())) {
                                    userMentionables.add(new UserMentionable(user.getId(),
                                            user.getFirstName(),
                                            user.getLastName(),
                                            user.getUserProfileThumbnail(),
                                            user.getStatus() == StoreStatus.ACTIVE));
                                }
                            } else {
                                userMentionables.add(new UserMentionable(user.getId(),
                                        user.getFirstName(),
                                        user.getLastName(),
                                        user.getUserProfileThumbnail(),
                                        user.getStatus() == StoreStatus.ACTIVE));
                            }
                        }
                    }
                    return Observable.just(userMentionables);
                });
    }
}
