package com.proapp.sompom.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;

import androidx.core.content.FileProvider;

import com.proapp.sompom.BuildConfig;
import com.proapp.sompom.R;

import java.io.File;
import java.util.Locale;
import java.util.Objects;

import timber.log.Timber;

public final class DownloaderUtils {

    private DownloaderUtils() {
    }

    public static String getRootDirPath(Context context) {
        return getRootDocumentPath(context);

        // We're going to save all the app's download to a folder that is the app's name with spaces
        // removed, which should be located in the phone internal storage when accessing from file manager.
        // return Environment.getExternalStoragePublicDirectory(context.getString(R.string.app_name).replaceAll("\\s", "")).getPath();
    }

    public static String getRootDocumentPath(Context context) {
        // With implementation of scope storage, app now have limited access to external directory,
        // so previous implementation of creating folder in root dir of phone no longer works,
        // we'll saved future files into the phone's document folder
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS + "/" + context.getString(R.string.app_name).replaceAll("\\s", "")).getPath();
    }

    public static String getProgressDisplayLine(long currentBytes, long totalBytes) {
        return getBytesToMBString(currentBytes) + "/" + getBytesToMBString(totalBytes);
    }

    public static String getProgressWithSpeedDisplayLine(long currentBytes, long totalBytes, String speed) {
        return getBytesToMBString(currentBytes) + "/" + getBytesToMBString(totalBytes) + "(" + speed + ")";
    }

    private static String getBytesToMBString(long bytes) {
        return String.format(Locale.ENGLISH, "%.2fMB", ((double) bytes) / 1048576.0d);
    }

    public static String getFileNameFromUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            return url.substring(url.lastIndexOf('/') + 1);
        }

        return null;
    }

    private static Uri getFileUri(Context context, String filePath) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return FileProvider.getUriForFile(Objects.requireNonNull(context.getApplicationContext()),
                    BuildConfig.APPLICATION_ID + ".provider", new File(filePath));
        } else {
            return Uri.fromFile(new File(filePath));
        }
    }

    private static String getExtensionFromFilePath(String filePath) {
        int lastIndexOf = filePath.lastIndexOf(".");
        if (lastIndexOf >= 0 && (lastIndexOf + 1) < filePath.length()) {
            return filePath.substring(lastIndexOf + 1);
        }

        return null;
    }

    public static void openDownloadedFile(Context context, String filePath) {
        Uri uri = getFileUri(context, filePath);
        Intent openIntent = new Intent(Intent.ACTION_VIEW);
        String extension = getExtensionFromFilePath(filePath);
        Timber.i("openDownloadedFile: filePath: " + filePath + ", extension: " + extension);

        if (extension == null || TextUtils.isEmpty(extension)) {
            openIntent.setDataAndType(uri, "*/*");
        } else {
            if (extension.equalsIgnoreCase("doc") || extension.equalsIgnoreCase("docx")) {
                // Word document
                openIntent.setDataAndType(uri, "application/msword");
            } else if (extension.equalsIgnoreCase("pdf")) {
                // PDF file
                openIntent.setDataAndType(uri, "application/pdf");
            } else if (extension.equalsIgnoreCase("ppt") || extension.equalsIgnoreCase("pptx")) {
                // Powerpoint file
                openIntent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (extension.equalsIgnoreCase("xls") || extension.equalsIgnoreCase("xlsx")) {
                openIntent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (extension.equalsIgnoreCase("txt")) {
                // Text file
                openIntent.setDataAndType(uri, "text/plain");
            } else {
                openIntent.setDataAndType(uri, "*/*");
            }
        }

        try {
            openIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            openIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(openIntent);
        } catch (Exception ex) {
            Timber.e(ex);
            openIntent.setDataAndType(uri, "*/*");
            openIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            openIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(openIntent);
        }
    }
}
