
package com.proapp.sompom.chat.call;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.Person;
import androidx.core.graphics.drawable.IconCompat;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.ColorResourceHelper;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.model.NotificationChannelSetting;
import com.proapp.sompom.model.notification.NotificationProfileDisplayAdaptive;
import com.proapp.sompom.newui.CallingActivity;
import com.proapp.sompom.utils.NotificationUtils;
import com.proapp.sompom.utils.SpannableUtil;

import java.util.Map;

import static android.app.PendingIntent.FLAG_ONE_SHOT;
import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

public final class CallNotificationHelper {

    public static final String CALL_ANSWERED = "is_call_answered";
    public static final String IS_INCOMING_CALL_FROM_NOTIFICATION = "IS_INCOMING_CALL_FROM_NOTIFICATION";
    public static final String CALL_DECLINED_ACTION = "CALL_DECLINED_ACTION";
    private static final int CALL_INFO_NOTIFICATION_ID = 0x1993;
    private static final int CALL_PENDING_INTENT_REQUEST_CODE = 0x2019;
    private static final int INCOMING_CALL_NOTIFICATION_ID = 1992;

    /**
     * It is must a point to remember when using pending intent. Must supply a none zero
     * value to request to make the intent works correctly with any flag set with activity.
     */
    public static void startCallInfoActionNotification(Context context,
                                                       AbsCallService.CallActionType actionType,
                                                       AbsCallService.CallType callType,
                                                       AbsCallClient.NotificationData data,
                                                       String title,
                                                       String userName,
                                                       String groupId,
                                                       boolean isCaller) {
//        Timber.i("startCallActionNotification: callType: " + callType);
        Intent notificationIntent = new Intent(context, CallingActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                CALL_PENDING_INTENT_REQUEST_CODE,
                notificationIntent
                , PendingIntent.FLAG_UPDATE_CURRENT);

        String callNotificationMessage = getCallNotificationMessage(context,
                actionType,
                callType,
                !TextUtils.isEmpty(groupId),
                userName);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,
                getCallInfoNotificationChannelId(context))
                .setContentTitle(title)
                .setOngoing(isSupportOnGoingNotification(actionType, isCaller))
                .setShowWhen(true)
                .setStyle(getCallMessageStyle(data.getCallProfilePhoto(),
                        callNotificationMessage,
                        System.currentTimeMillis(),
                        data.getNotificationProfileDisplayAdaptive()))
                .setContentText(callNotificationMessage)
                .setSmallIcon(CallHelper.getCallTypeIconForNotification(callType))
                .setColor(ColorResourceHelper.getAccentColor(context))
                .setUsesChronometer(true)
                .setShowWhen(false)
                .setContentIntent(pendingIntent);

        addActionButtons(context, actionType, groupId, builder);
        if (actionType == AbsCallService.CallActionType.SPEAKING) {
            builder.setUsesChronometer(true);
            cancelCallInfoNotification(context);
        } else {
            builder.setUsesChronometer(false);
        }
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(CALL_INFO_NOTIFICATION_ID, builder.build());
    }

    public static String getCallNotificationMessage(Context context,
                                                    AbsCallService.CallActionType actionType,
                                                    AbsCallService.CallType callType,
                                                    boolean isGroup,
                                                    String userName) {
        String message;
        if (isGroup) {
            //Group Call
            if (actionType == AbsCallService.CallActionType.INCOMING) {
                if (CallHelper.isVideoCall(callType)) {
                    message = context.getString(R.string.chat_info_start_group_video_message, userName);
                } else {
                    message = context.getString(R.string.chat_info_you_start_group_audio_message);
                }
            } else {
                String callStatus = "";
                if (actionType == AbsCallService.CallActionType.CALL) {
                    callStatus = context.getString(R.string.call_screen_call_label);
                } else if (actionType == AbsCallService.CallActionType.RINGING) {
                    callStatus = context.getString(R.string.call_screen_ring_label);
                }

                if (CallHelper.isVideoCall(callType)) {
                    message = context.getString(R.string.call_notification_group_video_status, callStatus);
                } else {
                    message = context.getString(R.string.call_notification_group_audio_status, callStatus);
                }
            }
        } else {
            //One to One Call
            if (actionType == AbsCallService.CallActionType.INCOMING) {
                message = context.getString(R.string.recieving_call_from,
                        context.getString(R.string.app_name));
            } else {
                String callStatus = "";
                if (actionType == AbsCallService.CallActionType.CALL) {
                    callStatus = context.getString(R.string.call_screen_call_label);
                } else if (actionType == AbsCallService.CallActionType.RINGING) {
                    callStatus = context.getString(R.string.call_screen_ring_label);
                }

                if (CallHelper.isVideoCall(callType)) {
                    message = context.getString(R.string.call_notification_video_status, callStatus);
                } else {
                    message = context.getString(R.string.call_notification_audio_status, callStatus);
                }
            }
        }

        if (!TextUtils.isEmpty(message)) {
            message = message.trim();
            message = SpannableUtil.capitaliseOnlyFirstLetter(message);
        }

        return message;
    }

    private static boolean isSupportOnGoingNotification(AbsCallService.CallActionType actionType, boolean isCaller) {
//        Timber.i("isSupportOnGoingNotification: " + actionType + ", isCaller: " + isCaller);
        return actionType == AbsCallService.CallActionType.CALL ||
                actionType == AbsCallService.CallActionType.SPEAKING ||
                actionType == AbsCallService.CallActionType.RINGING;
    }

    private static String getCallInfoNotificationChannelId(Context context) {
        NotificationUtils.checkToCreateNotificationChannelDirectly(context, NotificationChannelSetting.Call);
        return NotificationChannelSetting.Call.getLatestNotificationChannelId();
    }

    private static void addActionButtons(Context context,
                                         AbsCallService.CallActionType actionType,
                                         String groupId,
                                         NotificationCompat.Builder builder) {
        if (actionType == AbsCallService.CallActionType.INCOMING) {
            builder.addAction(createAction(context,
                    context.getString(R.string.call_screen_answer_button),
                    AbsCallService.CallNotificationActionType.ACCEPT,
                    groupId));
            builder.addAction(createAction(context,
                    context.getString(R.string.call_screen_decline_button),
                    AbsCallService.CallNotificationActionType.DECLINE,
                    groupId));
        } else {
            builder.addAction(createAction(context,
                    context.getString(R.string.call_notification_end_button),
                    AbsCallService.CallNotificationActionType.END,
                    groupId
            ));
        }
    }

    private static NotificationCompat.Action createAction(Context context,
                                                          String actionLabel,
                                                          AbsCallService.CallNotificationActionType actionType,
                                                          String groupId) {
        PendingIntent pendingIntent =
                PendingIntent.getActivity(context,
                        actionType.getRequestCode(),
                        CallingIntent.getCallNotificationActionIntent(context, actionType, groupId),
                        PendingIntent.FLAG_UPDATE_CURRENT);

        return new NotificationCompat.Action.Builder(0,
                actionLabel,
                pendingIntent)
                .build();
    }

    public static void showIncomingCallNotification(Context context,
                                                    Map<String, Object> callData,
                                                    AbsCallService.CallType callType,
                                                    Intent intent) {
        AbsCallClient.generateCallNotificationData(context,
                callData,
                null,
                new AbsCallClient.CallClientHelperListener() {
                    @Override
                    public void onGenerateCallDisplayNotificationData(AbsCallClient.NotificationData data) {
                        String message = getCallNotificationMessage(context,
                                AbsCallService.CallActionType.INCOMING,
                                callType,
                                data.isGroupCall(),
                                data.getUserName());
                        showIncomingCallNotification(context,
                                callType,
                                data,
                                data.getTitle(),
                                message,
                                intent);
                    }
                });
    }

    private static void showIncomingCallNotification(Context context,
                                                     AbsCallService.CallType callType,
                                                     AbsCallClient.NotificationData data,
                                                     String title,
                                                     String content,
                                                     Intent intent) {
        PendingIntent mainActionPendingIntent = PendingIntent.getActivity(context, 0,
                intent.putExtra(IS_INCOMING_CALL_FROM_NOTIFICATION, true), FLAG_UPDATE_CURRENT | FLAG_ONE_SHOT);
        PendingIntent answerPendingIntent = PendingIntent.getActivity(context, 1,
                intent.putExtra(CALL_ANSWERED, true), FLAG_UPDATE_CURRENT);
        PendingIntent declinePendingIntent = PendingIntent.getBroadcast(context, 2,
                new Intent().setAction(CALL_DECLINED_ACTION), FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, getIncomingCallChannel(context))
                .setContentTitle(title)
                .setContentText(content)
                .setStyle(getCallMessageStyle(data.getCallProfilePhoto(),
                        content,
                        System.currentTimeMillis(),
                        data.getNotificationProfileDisplayAdaptive()))
                .setFullScreenIntent(mainActionPendingIntent, true)
                .addAction(0, context.getString(R.string.call_screen_answer_button), answerPendingIntent)
                .addAction(0, context.getString(R.string.call_screen_decline_button), declinePendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setOngoing(true)
                .setCategory(NotificationCompat.CATEGORY_CALL)
                .setSmallIcon(CallHelper.getCallTypeIconForNotification(callType))
                .setVibrate(NotificationUtils.getVibration(context, NotificationChannelSetting.IncomingCall))
                .setColor(ColorResourceHelper.getAccentColor(context));
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(INCOMING_CALL_NOTIFICATION_ID, builder.build());
    }

    private static String getIncomingCallChannel(Context context) {
        NotificationUtils.checkToCreateNotificationChannelDirectly(context, NotificationChannelSetting.IncomingCall);
        return NotificationChannelSetting.IncomingCall.getLatestNotificationChannelId();
    }

    public static void cancelInComingCallNotification(Context context) {
        NotificationUtils.cancelNotificationById(context, INCOMING_CALL_NOTIFICATION_ID);
    }

    public static void cancelCallInfoNotification(Context context) {
        NotificationUtils.cancelNotificationById(context, CALL_INFO_NOTIFICATION_ID);
    }

    public static NotificationCompat.Style getCallMessageStyle(Bitmap bitmap,
                                                               String message,
                                                               long time,
                                                               NotificationProfileDisplayAdaptive actor) {
        Person sender = new Person.Builder()
                .setName(actor.getDisplayFullName())
                .setBot(false)
                .setImportant(false)
                .setIcon(IconCompat.createWithBitmap(bitmap))
                .build();
        NotificationCompat.MessagingStyle style = new NotificationCompat.MessagingStyle(sender);
        style.addMessage(new NotificationCompat.MessagingStyle.Message(message, time, sender));

        return style;
    }
}
