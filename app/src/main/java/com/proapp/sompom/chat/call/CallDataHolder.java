package com.proapp.sompom.chat.call;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 6/16/2020.
 */
public class CallDataHolder<T> {

    private Map<String, CallInfoHolder<T>> mCallInfoHolders = new HashMap<>();
    private Map<String, Boolean> mAudioStateMap = new HashMap<>();
    private List<T> mUserVideoEnableUIds = new ArrayList<>();
    private Conversation mConversation;
    private List<String> mRejectCallIdList = new ArrayList<>();
    private String mCurrentUserId;
    private List<String> mJoinCallUserId = new ArrayList<>();
    private List<String> mBackupJoinCallUserId = new ArrayList<>(); //The user id will not be removed when they left call, unless completed call ended.
    private Map<String, List<String>> mSendBusyStatusToUserList = new HashMap<>();
    private List<String> mVideoFrozenUserIdList = new ArrayList<>();

    public void setCurrentUserId(String currentUserId) {
        mCurrentUserId = currentUserId;
    }

    public void addCallInfo(User user, T uid) {
        if (user != null) {
            CallInfoHolder<T> callInfo = new CallInfoHolder<T>(uid, user);
            mCallInfoHolders.put(user.getId(), callInfo);
            //Ignore current user id
            if (!TextUtils.equals(user.getId(), mCurrentUserId)) {
                addJoinedUserId(user.getId());
            }
        }
    }

    public boolean shouldBroadCastVideoFrozen(String userId) {
        if (!mVideoFrozenUserIdList.contains(userId)) {
            mVideoFrozenUserIdList.add(userId);
            return true;
        }

        return false;
    }

    public boolean shouldBroadCastResumeVideoBackFromFrozen(String userId) {
        if (mVideoFrozenUserIdList.contains(userId)) {
            mVideoFrozenUserIdList.remove(userId);
            return true;
        }

        return false;
    }

    public boolean isUserEngagedInCall(String userId) {
        return mCallInfoHolders.get(userId) != null;
    }

    public void addSendBusyStatusToUser(String channelId, String userId) {
        List<String> userIdList = mSendBusyStatusToUserList.get(channelId);
        if (userIdList == null) {
            userIdList = new ArrayList<>();
        }
        if (!userIdList.contains(userId)) {
            userIdList.add(userId);
        }
        mSendBusyStatusToUserList.put(channelId, userIdList);
    }

    public boolean isUserListedInPreviousSendBusyToStatus(String channelId, String userId) {
        List<String> userIdList = mSendBusyStatusToUserList.get(channelId);
        if (userIdList != null) {
            return userIdList.contains(userId);
        }

        return false;
    }

    public T getRemoteCallViewIdOfUser(String userId) {
        for (CallInfoHolder<T> value : mCallInfoHolders.values()) {
            if (TextUtils.equals(value.getUser().getId(), userId)) {
                return value.getUId();
            }
        }

        return null;
    }

    public void addRejectedCallUser(String userId) {
        if (!mRejectCallIdList.contains(userId)) {
            mRejectCallIdList.add(userId);
        }
    }

    public void addJoinedUserId(String userId) {
        if (!mJoinCallUserId.contains(userId)) {
            mJoinCallUserId.add(userId);
            mBackupJoinCallUserId.add(userId);
        }
    }

    public boolean isUserInCallingOrUsedToBeInCall(String userId) {
        return mBackupJoinCallUserId.contains(userId);
    }

    public List<String> getJoinCallUserId() {
        return mJoinCallUserId;
    }

    public void removeJoinedUserId(String userId) {
        mJoinCallUserId.remove(userId);
    }

    public int getJoinedUserCallCount() {
        return mJoinCallUserId.size();
    }

    /*
    Current user is a caller of group call and call this method to check if all the participant jejected
    the call.
     */
    public boolean isAllParticipantRejectGroupCall() {
        if (mConversation != null && mConversation.getParticipants() != null) {
            List<String> participantIdList = getGroupParticipantIdExceptCurrentUser();
            return participantIdList.containsAll(mRejectCallIdList) && mRejectCallIdList.containsAll(participantIdList);
        }

        return false;
    }

    private List<String> getGroupParticipantIdExceptCurrentUser() {
        List<String> idList = new ArrayList<>();
        if (mConversation != null && mConversation.getParticipants() != null) {
            for (User participant : mConversation.getParticipants()) {
                if (!TextUtils.equals(participant.getId(), mCurrentUserId)) {
                    idList.add(participant.getId());
                }
            }
        }

        return idList;
    }

    public void removeRecordJoinedParticipant(User user) {
        mCallInfoHolders.remove(user);
    }

    public void addUserVideoEnable(T uid) {
        if (!mUserVideoEnableUIds.contains(uid)) {
            mUserVideoEnableUIds.add(uid);
        }
    }

    public void removeUserVideoEnable(T uid) {
        mUserVideoEnableUIds.remove(uid);
    }

    public boolean isUserHasVideoEnable(T uid) {
        return mUserVideoEnableUIds.contains(uid);
    }

    public void addAudioState(String userId, boolean isOn) {
        mAudioStateMap.put(userId, isOn);
    }

    public boolean getAudioState(String userId) {
        Boolean audioOn = mAudioStateMap.get(userId);
        if (audioOn == null) {
            //Enable by default
            return true;
        }

        return audioOn;
    }

    public int getUserVideoEnableUIdCount() {
        return mUserVideoEnableUIds.size();
    }

    public int getJoinCallParticipantCount() {
        return mCallInfoHolders.size();
    }

    public User getUserFromUId(T uid) {
        String checkUIDString = String.valueOf(uid);
        for (Map.Entry<String, CallInfoHolder<T>> entry : mCallInfoHolders.entrySet()) {
            if (entry.getValue() != null && TextUtils.equals(String.valueOf(entry.getValue().mUId),
                    checkUIDString)) {
                return entry.getValue().getUser();
            }
        }

        return null;
    }

    public User getParticipantFromCall(String id) {
        if (mConversation != null && mConversation.getParticipants() != null) {
            for (User participant : mConversation.getParticipants()) {
                if (TextUtils.equals(participant.getId(), id)) {
                    return participant;
                }
            }
        }

        return null;
    }

    public Conversation getConversation() {
        return mConversation;
    }

    public void setConversation(Context context,
                                String conversationId,
                                User participant1x1,
                                boolean isGroupCall) {
        mConversation = ConversationDb.getConversationById(context, conversationId);
        if (mConversation != null && !isGroupCall) {
            User currentUser = SharedPrefUtils.getUser(context);
            mConversation.setParticipants(Arrays.asList(currentUser, participant1x1));
        }
        Timber.i("mConversation: " + new Gson().toJson(mConversation));
    }

    public void resetCallData() {
        mConversation = null;
        mCallInfoHolders.clear();
        mUserVideoEnableUIds.clear();
        mAudioStateMap.clear();
        mRejectCallIdList.clear();
        mJoinCallUserId.clear();
        mSendBusyStatusToUserList.clear();
        mBackupJoinCallUserId.clear();
        mVideoFrozenUserIdList.clear();
    }

    public void clearRejectUserCallId() {
        mRejectCallIdList.clear();
    }

    public class CallInfoHolder<T> {

        private T mUId; //User id that from the call framework
        private User mUser;

        public CallInfoHolder(T UId, User user) {
            mUId = UId;
            mUser = user;
        }

        public T getUId() {
            return mUId;
        }

        public void setUId(T UId) {
            mUId = UId;
        }

        public User getUser() {
            return mUser;
        }

        public void setUser(User user) {
            mUser = user;
        }
    }
}
