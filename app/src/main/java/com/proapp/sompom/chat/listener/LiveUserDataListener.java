package com.proapp.sompom.chat.listener;

import com.proapp.sompom.model.emun.Presence;
import com.proapp.sompom.model.result.User;

import java.util.Date;

/**
 * Created by He Rotha on 12/31/18.
 */
public interface LiveUserDataListener {

    void onPresenceUpdate(String userId, Presence presence, Date lastOnlineTime);

    String getUserId();

    default void onProfileAvatarUpdated(String userId, int hash, String userProfile) {
    }
}
