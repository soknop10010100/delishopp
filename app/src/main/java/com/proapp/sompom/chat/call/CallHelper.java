package com.proapp.sompom.chat.call;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothHeadset;
import android.content.Context;
import android.media.AudioManager;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import androidx.annotation.DrawableRes;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.AudioManagerProvider;
import com.proapp.sompom.helper.GsonHelper;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 3/4/2020.
 */
public final class CallHelper {

    //Common Call Keys
    public static final String TYPE = "type";
    public static final String CALLER = "caller";
    public static final String GROUP = "group"; //Container group conversation object
    public static final String SENDER = "sender";
    public static final String RECIPIENT_IDS = "recipientIds";

    private static List<String> mRejectedCallList = new ArrayList<>();

    private CallHelper() {
    }

    public static Map<String, Object> getExchangeCallData(Context context,
                                                          String senderId,
                                                          String recipientId,
                                                          UserGroup group) {
        Map<String, Object> headers = new HashMap<>();
        headers.put(CALLER, buildCallerInfo(context));
        headers.put(SENDER, senderId);
        if (group != null) {
            headers.put(RECIPIENT_IDS, createRecipientIds(context, group));
            //No need to exchange participant
            group.setParticipants(null);
            //Group call.
            headers.put(GROUP, group);
        } else {
            //Individual call
            headers.put(RECIPIENT_IDS, new String[]{recipientId});
        }

        return headers;
    }

    public static Map<String, Object> generateGroupJoinCallData(Context context, UserGroup group) {
        Map<String, Object> headers = new HashMap<>();
        if (group != null) {
            headers.put(RECIPIENT_IDS, createRecipientIds(context, group));
            //No need to exchange participant
            group.setParticipants(null);
            //Group call.
            headers.put(GROUP, group);
        }

        return headers;
    }


    public static String[] getRecipientIds(Map<String, Object> callData) {
        if (callData.containsKey(RECIPIENT_IDS)) {
            return (String[]) callData.get(RECIPIENT_IDS);
        } else {
            return new String[]{};
        }
    }

    private static String[] createRecipientIds(Context context, UserGroup userGroup) {
        String currentUserId = SharedPrefUtils.getUserId(context);
        List<String> recipientIds = new ArrayList<>();
        if (userGroup.getParticipants() != null && !userGroup.getParticipants().isEmpty()) {
            for (User participant : userGroup.getParticipants()) {
                //Ignore current user id
                if (!TextUtils.equals(participant.getId(), currentUserId)) {
                    recipientIds.add(participant.getId());
                }
            }
        }

        return recipientIds.toArray(new String[]{});
    }

    public static boolean isCurrentUserCaller(Context context, Map<String, Object> data) {
        User caller = getCaller(data);
        return caller != null && TextUtils.equals(caller.getId(),
                SharedPrefUtils.getUserId(context));
    }

    public static boolean isCurrentUserCaller(String checkingId, Map<String, Object> data) {
        User caller = getCaller(data);
        return caller != null && TextUtils.equals(caller.getId(), checkingId);
    }

    public static String getSenderId(Map<String, Object> data) {
        if (data.containsKey(SENDER)) {
            return data.get(SENDER).toString();
        }

        return null;
    }

    private static User buildCallerInfo(Context context) {
        return User.getCommonUserInstance(SharedPrefUtils.getUser(context));
    }

    public static UserGroup getGroup(Map<String, Object> data) {
        if (data != null) {
            Gson gson = GsonHelper.getGsonForAPICommunication();
            Object object = data.get(GROUP);
            if (object != null) {
                UserGroup userGroup = gson.fromJson(gson.toJson(object), UserGroup.class);
                if (userGroup != null && userGroup.isValidGroup()) {
                    return userGroup;
                }
            }
        }

        return null;
    }

    public static User getCaller(Map<String, Object> header) {
        if (header != null) {
            Gson gson = GsonHelper.getGsonForAPICommunication();
            Object user = header.get(CALLER);
            if (user != null) {
                String data = gson.toJson(user);
                return gson.fromJson(data, User.class);
            }
        }

        return null;
    }

    public static AbsCallService.CallType getCallType(Map<String, Object> callData) {
        if (callData != null) {
            Object callType = callData.get(TYPE);
            if (callType != null) {
                return AbsCallService.CallType.getFromValue(callType.toString());
            }
        }

        return AbsCallService.CallType.UNKNOWN;
    }

    public static Map<String, Object> changeCaller(Map<String, Object> incomingCallData,
                                                   User newCaller) {
        Map<String, Object> cloneData = cloneCallData(incomingCallData);
        cloneData.put(CALLER, newCaller);
        return cloneData;
    }

    public static void injectGroupParticipants(Map<String, Object> callData, List<User> participants) {
        UserGroup group = getGroup(callData);
        if (group != null) {
            group.setParticipants(participants);
            callData.put(GROUP, group);
        }
    }

    public static Map<String, Object> removeGroupParticipants(Map<String, Object> callData) {
        Map<String, Object> cloneCallData = cloneCallData(callData);
        UserGroup group = getGroup(cloneCallData);
        if (group != null) {
            group.setParticipants(null);
            cloneCallData.put(GROUP, group);
        }

        return cloneCallData;
    }

    public static Map<String, Object> cloneCallData(Map<String, Object> callData) {
        Gson gson = GsonHelper.getGsonForAPICommunication();
        return gson.fromJson(gson.toJson(callData), new TypeToken<Map<String, Object>>() {
        }.getType());
    }

    public static void updateCallType(Map<String, Object> callData,
                                      AbsCallService.CallType callType) {
        callData.put(TYPE, callType.getValue());
    }

    public static void updateSenderId(Map<String, Object> callData,
                                      String senderId) {
        callData.put(SENDER, senderId);
    }

    public static boolean isInComingVOIPCallMessageType(Chat message) {
        AbsCallService.CallType callType = AbsCallService.CallType.getFromValue(message.getSendingTypeAsString());
        return callType == AbsCallService.CallType.VOICE ||
                callType == AbsCallService.CallType.VIDEO ||
                callType == AbsCallService.CallType.GROUP_VOICE ||
                callType == AbsCallService.CallType.GROUP_VIDEO;
    }

    public static boolean isInComingGroupVOIPCallMessageType(Chat message) {
        AbsCallService.CallType callType = AbsCallService.CallType.getFromValue(message.getSendingTypeAsString());
        return callType == AbsCallService.CallType.GROUP_VOICE || callType == AbsCallService.CallType.GROUP_VIDEO;
    }


    public static boolean isVOIPCallMessage(Chat message) {
        AbsCallService.CallType callType = AbsCallService.CallType.getFromValue(message.getSendingTypeAsString());
        return callType != AbsCallService.CallType.UNKNOWN;
    }

    public static boolean isRingingVOIPCallMessageType(Chat message) {
        AbsCallService.CallType callType = AbsCallService.CallType.getFromValue(message.getSendingTypeAsString());
        return callType == AbsCallService.CallType.RINGING;
    }

    public static boolean isRejectVOIPCallMessageType(Chat message) {
        AbsCallService.CallType callType = AbsCallService.CallType.getFromValue(message.getSendingTypeAsString());
        return callType == AbsCallService.CallType.REJECT_CALL;
    }

    public static boolean isVoiceCall(AbsCallService.CallType callType) {
        return callType == AbsCallService.CallType.VOICE || callType == AbsCallService.CallType.GROUP_VOICE;
    }

    public static boolean isVideoCall(AbsCallService.CallType callType) {
        return callType == AbsCallService.CallType.VIDEO || callType == AbsCallService.CallType.GROUP_VIDEO;
    }

    public static boolean isBusyVOIPCallMessageType(Chat message) {
        AbsCallService.CallType callType = AbsCallService.CallType.getFromValue(message.getSendingTypeAsString());
        return callType == AbsCallService.CallType.BUSY;
    }

    public static boolean isCallAcceptedVOIPCallMessageType(Chat message) {
        AbsCallService.CallType callType = AbsCallService.CallType.getFromValue(message.getSendingTypeAsString());
        return callType == AbsCallService.CallType.CALL_ACCEPTED;
    }

    public static boolean isCalledHistoryMessageType(Chat message) {
        Chat.ChatType type = Chat.ChatType.from(message.getChatType());
        return type == Chat.ChatType.CALLED ||
                type == Chat.ChatType.VIDEO_CALLED ||
                type == Chat.ChatType.END_GROUP_CALL ||
                type == Chat.ChatType.END_GROUP_VIDEO_CALL;
    }

    public static AbsCallService.CallType getChatCallType(Chat message) {
        return AbsCallService.CallType.getFromValue(message.getSendingTypeAsString());
    }

    public static void addToRejectedCallList(String conversationId) {
        if (!mRejectedCallList.contains(conversationId)) {
            mRejectedCallList.add(conversationId);
        }
    }

    public static boolean isStillInRejectedCallList(String conversationId) {
        return mRejectedCallList.contains(conversationId);
    }

    public static void clearRejectedCallList() {
        mRejectedCallList.clear();
    }

    public static boolean shouldShowIncomingCallScreenOnReceiveIncomingInsideScreen() {
        /*
        For Android version 10+ will manage to show incoming call notification if the app in background.
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return !MainApplication.isAppInBackground();
        } else {
            return true;
        }
    }

    public static boolean isDeviceInPhoneCallProcess(Context context) {
        int callState = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getCallState();
        Timber.i("Phone call state: " + callState);
        return callState != TelephonyManager.CALL_STATE_IDLE;
    }

    public static void resetDeviceAudioStateAfterCallEnded(Context context) {
        if (context != null) {
            AudioManager am = AudioManagerProvider.getInstance(context).getAudioManager();
            Timber.i("Reset call Audio mode to normal.");
            am.setMode(AudioManager.MODE_NORMAL);
        }
    }

    public static boolean isBluetoothHeadsetDeviceConnected() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()
                && mBluetoothAdapter.getProfileConnectionState(BluetoothHeadset.HEADSET) == BluetoothHeadset.STATE_CONNECTED;
    }

    public static void enableCallAudioOutputOnPhoneOrSpeaker(Context context, boolean isSpeaker) {
        AudioManager audioManager = AudioManagerProvider.getInstance(context).getAudioManager();
        if (audioManager != null) {
            Timber.i("enableCallAudioOutputOnPhoneOrSpeaker: " + isSpeaker +
                    ", isBluetoothScoOn: " + audioManager.isBluetoothScoOn() +
                    ", audioManager: " + audioManager.hashCode());
            audioManager.setMode(isSpeaker ? AudioManager.MODE_NORMAL : AudioManager.MODE_IN_COMMUNICATION);
            audioManager.setSpeakerphoneOn(isSpeaker);
        }
    }

    public static void setIncomingCallRingMode(Context context, boolean isSpeaker) {
        AudioManager audioManager = AudioManagerProvider.getInstance(context).getAudioManager();
        Timber.i("setIncomingCallRingMode: isSpeaker: " + isSpeaker);
        if (audioManager != null) {
            audioManager.setMode(AudioManager.MODE_NORMAL);
            audioManager.setSpeakerphoneOn(isSpeaker);
        }
    }

    public static String formatCallDurationDisplay(Context context, int callDuration) {
        int hour = callDuration / (60 * 60);
        int minute = (callDuration % (60 * 60)) / 60;
        int second = callDuration % 60;

        StringBuilder duration = new StringBuilder();
        if (hour > 0) {
            duration.append(hour);
            duration.append(" ");
            duration.append(hour > 1 ? context.getString(R.string.common_timestamp_plural_abbreviation_hour_label) :
                    context.getString(R.string.common_timestamp_single_abbreviation_hour_label));
        }
        if (minute > 0) {
            if (!TextUtils.isEmpty(duration)) {
                duration.append(" ");
            }
            duration.append(minute);
            duration.append(" ");
            duration.append(minute > 1 ? context.getString(R.string.common_timestamp_plural_abbreviation_minute_label) :
                    context.getString(R.string.common_timestamp_single_abbreviation_minute_label));
        }

        if (second > 0) {
            if (!TextUtils.isEmpty(duration)) {
                duration.append(" ");
            }
            duration.append(second);
            duration.append(" ");
            duration.append(second > 1 ? context.getString(R.string.common_timestamp_plural_abbreviation_second_label) :
                    context.getString(R.string.common_timestamp_single_abbreviation_second_label));
        }

        return duration.toString();
    }

    public static boolean hasFloatingPermissionEnable(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return Settings.canDrawOverlays(context);
        } else {
            return true;
        }
    }


    public static void vibrate(Context context) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            v.vibrate(100);
        }
    }

    public static @DrawableRes
    int getCallTypeIconForNotification(AbsCallService.CallType callType) {
        return CallHelper.isVideoCall(callType) ? R.drawable.ic_big_video_camera :
                R.drawable.ic_white_telephone;
    }
}
