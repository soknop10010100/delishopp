package com.proapp.sompom.chat.listener;

import com.proapp.sompom.model.emun.SegmentedControlItem;
import com.proapp.sompom.model.result.Conversation;

/**
 * Created by He Rotha on 7/6/18.
 */
public interface ChannelListener {
    void onChannelCreateOrUpdate(Conversation channel);

    void onChannelRemoved(String channelId);

    SegmentedControlItem getCurrentSegmentedControlItem();

}
