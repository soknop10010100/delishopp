package com.proapp.sompom.chat;

import android.os.Binder;

import com.proapp.sompom.chat.call.agora.VOIPCallMessageListener;
import com.proapp.sompom.chat.listener.ChannelListener;
import com.proapp.sompom.chat.listener.ChatListener;
import com.proapp.sompom.chat.listener.GlobalChatListener;
import com.proapp.sompom.chat.listener.LiveUserDataListener;
import com.proapp.sompom.chat.listener.ServiceStateListener;
import com.proapp.sompom.chat.service.ChatSocket;
import com.proapp.sompom.listener.OnBadgeUpdateListener;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 7/6/18.
 */
public abstract class AbsChatBinder extends Binder {

    private final List<ChannelListener> mChannelListener = new ArrayList<>();
    private final List<ChatListener> mChatListener = new ArrayList<>();
    private final List<VOIPCallMessageListener> mVOIPCallMessageListeners = new ArrayList<>();
    private final List<LiveUserDataListener> mLiveUserDataListeners = new ArrayList<>();
    private final List<GlobalChatListener> mGlobalChatListeners = new ArrayList<>();
    private final List<OnBadgeUpdateListener> mBadgeListeners = new ArrayList<>();

    public List<ChannelListener> getChannelListener() {
        return mChannelListener;
    }

    public List<ChatListener> getChatListener() {
        return mChatListener;
    }

    protected List<LiveUserDataListener> getLiveUserDataListeners() {
        return mLiveUserDataListeners;
    }

    public List<GlobalChatListener> getGlobalChatListeners() {
        return mGlobalChatListeners;
    }

    public List<OnBadgeUpdateListener> getBadgeListeners() {
        return mBadgeListeners;
    }

    public abstract void startService(ServiceStateListener listener);

    public abstract void startSubscribeAllGroupChannel();

    public abstract void clearServiceStateListener(ServiceStateListener listener);

    public abstract void startSubscribeAllGroupChannel(List<String> channelIds);

    public abstract void checkToSubscribeOrUnsubscribeGroupConversation(String groupChannelId, boolean isRemoved);

    public abstract void stopService();

    public abstract void sendMessage(User user, Chat message);

    public abstract void sendVOIPCallMessage(Chat message, ChatSocket.SendMessageCallback callback);

    public abstract ChatSocket.SendMessageCallback getNewPublishMessageCallbackInstance(Chat newChat, ChatSocket.PublishMessageCallback callback);

    public abstract void startTyping(boolean isGroup, String recipientUserId, String productId, String channelID);

    public abstract void stopTyping(String recipientUserId, String productId, String channelID);

    public abstract void removeMessage(Chat chat);

    public abstract void onResume(String conversationId);

    public abstract void archivedConversation(String conversationId);

    public void addChannelListener(ChannelListener listener) {
        if (!mChannelListener.contains(listener)) {
            mChannelListener.add(listener);
        }
    }

    public void removeChannelListener(ChannelListener listener) {
        mChannelListener.remove(listener);
    }

    public void addChatListener(ChatListener listener) {
        if (!mChatListener.contains(listener)) {
            mChatListener.add(listener);
        }
    }

    public void removeChatListener(ChatListener listener) {
        mChatListener.remove(listener);
    }

    public void removeLiveUserDataListener(LiveUserDataListener listener) {
        mLiveUserDataListeners.remove(listener);
    }

    public void addLiveUserDataListener(LiveUserDataListener listener) {
        if (!mLiveUserDataListeners.contains(listener)) {
            mLiveUserDataListeners.add(listener);
        }
    }

    public void addGlobalChatListener(GlobalChatListener listener) {
        if (!mGlobalChatListeners.contains(listener)) {
            mGlobalChatListeners.add(listener);
        }
    }

    public void removeGlobalChatListener(GlobalChatListener listener) {
        mGlobalChatListeners.remove(listener);
    }

    public void addBadgeListener(OnBadgeUpdateListener listener) {
        if (!mBadgeListeners.contains(listener)) {
            mBadgeListeners.add(listener);
        }
    }

    public void removeBadgeListener(OnBadgeUpdateListener listener) {
        mBadgeListeners.remove(listener);
    }

    public List<VOIPCallMessageListener> getVOIPCallMessageListeners() {
        return mVOIPCallMessageListeners;
    }

    public void removeCallVOIPMessageListeners(VOIPCallMessageListener listener) {
        if (listener != null) {
            mVOIPCallMessageListeners.remove(listener);
        }
    }

    public void addCallVOIPMessageListeners(VOIPCallMessageListener listener) {
        if (listener != null) {
            mVOIPCallMessageListeners.add(listener);
        }
    }
}
