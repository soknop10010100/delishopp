package com.proapp.sompom.chat;

import android.text.TextUtils;

import com.proapp.sompom.R;

/**
 * Created by He Rotha on 7/6/18.
 */
public enum MessageState {

    SENDING("sending", R.drawable.ic_chat_sending_status, R.string.chat_sending_status),
    FAIL("fail", R.drawable.ic_chat_retry_status, R.string.chat_failed_status),
    SENT("sent", R.drawable.ic_chat_sent_status, R.string.chat_sent_status),
    SEEN("seen", R.drawable.ic_chat_seen_status, R.string.chat_seen_status),
    DELIVERED("delivered", R.drawable.ic_chat_seen_status, R.string.chat_delivered_status);

    private String mStatus;
    private int mIcon;
    private int mTextStatus;

    MessageState(String status, int icon, int textStatus) {
        mStatus = status;
        mIcon = icon;
        mTextStatus = textStatus;
    }

    public static MessageState fromValue(String value) {
        if (TextUtils.isEmpty(value)) {
            return SENT;
        }
        for (MessageState s : MessageState.values()) {
            if (s.getValue().equalsIgnoreCase(value)) {
                return s;
            }
        }
        return SENT;
    }

    public String getStatus() {
        return mStatus;
    }

    public int getIcon() {
        return mIcon;
    }

    public int getTextStatus() {
        return mTextStatus;
    }

    public String getValue() {
        return mStatus;
    }
}
