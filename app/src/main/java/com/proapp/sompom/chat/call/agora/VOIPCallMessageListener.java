package com.proapp.sompom.chat.call.agora;

import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.SocketError;

/**
 * Created by Chhom Veasna on 6/8/2020.
 */

public interface VOIPCallMessageListener {

    void onReceiveVOIPCallMessage(Chat message);

    default void onCallError(SocketError error) {
    }
}
