package com.proapp.sompom.chat.call;

import android.content.Context;

import com.proapp.sompom.chat.AbsChatBinder;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.ConversationUtil;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 6/29/2020.
 */
public class CallMessageHelper {

    private Context mContext;
    private AbsChatBinder mChatBinder;

    public CallMessageHelper(Context context, AbsChatBinder chatBinder, String tag) {
        Timber.i("CallMessageHelper: " + chatBinder + " tag: " + tag);
        mContext = context;
        mChatBinder = chatBinder;
    }

    public void updateChatBinderIfNecessary(AbsChatBinder chatBinder) {
        Timber.i("updateChatBinderIfNecessary: " + chatBinder);
        if (mChatBinder == null) {
            mChatBinder = chatBinder;
        }
    }

    private Chat buildUserCallActonMessage(Map<String, Object> callData,
                                           String sendTo,
                                           String recipientId,
                                           AbsCallService.CallType callType) {
        Map<String, Object> cloneCallData = CallHelper.cloneCallData(callData);
        CallHelper.updateSenderId(cloneCallData, SharedPrefUtils.getUserId(mContext));
        Chat chat = buildVOIPCallMessageWithChannelId(cloneCallData, getChannelId(callData, recipientId));
        chat.setRawSendingType(callType.getValue());
        chat.setSendTo(sendTo);
        chat.setSenderId(SharedPrefUtils.getUserId(mContext));

        return chat;
    }

    public void sendRequestShareVideo(Map<String, Object> callData, String recipientId) {
        mChatBinder.sendVOIPCallMessage(buildUserCallActonMessage(callData,
                recipientId,
                recipientId,
                AbsCallService.CallType.REQUEST_VIDEO), null);
    }

    public void sendRequestShareVideoToUserDirectly(Map<String, Object> callData, String recipientId) {
        mChatBinder.sendVOIPCallMessage(buildUserCallActonMessage(callData,
                recipientId,
                recipientId,
                AbsCallService.CallType.REQUEST_VIDEO), null);
    }

    public void sendRequestShareVideoForGroup(Map<String, Object> callData, List<String> joinedUserId) {
        Timber.i("sendRequestShareVideoForGroup: " + joinedUserId.size());
        if (!joinedUserId.isEmpty()) {
            Chat baseChat = buildUserCallActonMessage(callData,
                    null,
                    null,
                    AbsCallService.CallType.REQUEST_VIDEO);
            for (String userId : joinedUserId) {
                Chat chat1 = baseChat.cloneNewChat();
                chat1.setSendTo(userId);
                mChatBinder.sendVOIPCallMessage(chat1, null);
            }
        }
    }

    public void sendAcceptedRequestVideo(Map<String, Object> callData, String recipientId) {
        mChatBinder.sendVOIPCallMessage(buildUserCallActonMessage(callData,
                recipientId,
                recipientId,
                AbsCallService.CallType.ACCEPTED_VIDEO), null);
    }

    public void sendRejectRequestVideo(Map<String, Object> callData, String recipientId) {
        mChatBinder.sendVOIPCallMessage(buildUserCallActonMessage(callData,
                recipientId,
                recipientId,
                AbsCallService.CallType.REJECT_VIDEO), null);
    }

    public void sendCancelRequestVideo(Map<String, Object> callData, String recipientId) {
        mChatBinder.sendVOIPCallMessage(buildUserCallActonMessage(callData,
                recipientId,
                recipientId,
                AbsCallService.CallType.REJECT_VIDEO), null);
    }

    public void sendRingingStatusToCaller(Map<String, Object> callData) {
        User caller = CallHelper.getCaller(callData);
        if (caller != null) {
            Chat chat = buildVOIPCallMessageWithChannelId(callData, getChannelId(callData, caller.getId()));
            chat.setRawSendingType(AbsCallService.CallType.RINGING.getValue());
            chat.setSendTo(caller.getId());
            chat.setSenderId(SharedPrefUtils.getUserId(mContext));
            chat.setGroup(CallHelper.getGroup(callData) != null);
            mChatBinder.sendVOIPCallMessage(chat, null);
        }
    }

    /*
    Will send message to current call account that login on other devices that this account is
    already in call engagement and those device calling should end themselves.
     */
    public void sendCallAcceptedToCurrentCallAccount(Map<String, Object> callData, String channelId) {
        String currentUserId = SharedPrefUtils.getUserId(mContext);
        CallHelper.updateSenderId(callData, currentUserId);
        User caller = CallHelper.getCaller(callData);
        if (caller != null) {
            Chat chat = buildVOIPCallMessageWithChannelId(callData, channelId);
            chat.setRawSendingType(AbsCallService.CallType.CALL_ACCEPTED.getValue());
            chat.setSendTo(currentUserId);
            chat.setSenderId(SharedPrefUtils.getUserId(mContext));
            mChatBinder.sendVOIPCallMessage(chat, null);
        } else {
            Timber.e("Caller require to send call status from current user.");
        }
    }

    public void sendCallAcceptedToCurrentCallAccount(Map<String, Object> callData) {
        User caller = CallHelper.getCaller(callData);
        if (caller != null) {
            sendCallAcceptedToCurrentCallAccount(callData, getChannelId(callData, caller.getId()));
        }
    }

    public void sendCallAcceptedToOneToOneCaller(Map<String, Object> callData, String channelId) {
        User caller = CallHelper.getCaller(callData);
        if (caller != null) {
            Chat chat = buildVOIPCallMessageWithChannelId(callData, channelId);
            chat.setRawSendingType(AbsCallService.CallType.CALL_ACCEPTED.getValue());
            chat.setSendTo(caller.getId());
            chat.setSenderId(SharedPrefUtils.getUserId(mContext));
            mChatBinder.sendVOIPCallMessage(chat, null);
        } else {
            Timber.e("Caller require to send call status from current user.");
        }
    }

    public Chat buildVOIPCallMessageWithChannelId(Map<String, Object> callData, String channelId) {
        Map<String, Object> newCallData = CallHelper.removeGroupParticipants(callData);
        Chat chat = new Chat();
        chat.setCallData(newCallData);
        chat.setChannelId(channelId);
        return chat;
    }

    public String getChannelId(Map<String, Object> callData, String recipientIdFor1x1) {
        UserGroup group = CallHelper.getGroup(callData);
        if (group != null) {
            return group.getId();
        } else {
            return ConversationUtil.getConversationId(SharedPrefUtils.getUserId(mContext), recipientIdFor1x1);
        }
    }

    public Chat buildBusyVOIPCallMessage(Context context, Map<String, Object> callData, String tag) {
        String currentUserId = SharedPrefUtils.getUserId(context);
        Map<String, Object> clonedCallData = CallHelper.cloneCallData(callData);
        CallHelper.updateSenderId(clonedCallData, currentUserId);
        UserGroup group = CallHelper.getGroup(clonedCallData);
        User caller = CallHelper.getCaller(clonedCallData);
        if (caller != null) {
            String channelId = getChannelId(clonedCallData,
                    caller.getId());
            Chat chat = buildVOIPCallMessageWithChannelId(clonedCallData, channelId);
            chat.setSenderId(currentUserId);
            chat.setRawSendingType(AbsCallService.CallType.BUSY.getValue());
            chat.setSendTo(group != null ? group.getId() : caller.getId());
            Timber.i("sendUserBusyVOIPCallMessage to caller: " + caller.getFullName() + ", tag: " + tag);
            return chat;
        } else {
            return null;
        }
    }

    public Chat buildRejectVOIPCallMessage(Context context,
                                           Map<String, Object> callData,
                                           String recipientId) {
        //Current must be the sender of this kind of message.
        String currentUserId = SharedPrefUtils.getUserId(context);
        Map<String, Object> clonedCallData = CallHelper.cloneCallData(callData);
        CallHelper.updateSenderId(clonedCallData, currentUserId);
        UserGroup group = CallHelper.getGroup(clonedCallData);
        User caller = CallHelper.getCaller(clonedCallData);
        if (caller != null) {
            String channelId = getChannelId(clonedCallData,
                    caller.getId());
            Chat chat = buildVOIPCallMessageWithChannelId(clonedCallData, channelId);
            chat.setSenderId(currentUserId);
            chat.setRawSendingType(AbsCallService.CallType.REJECT_CALL.getValue());
            chat.setSendTo(group != null ? group.getId() : recipientId);
            return chat;
        } else {
            return null;
        }
    }
}
