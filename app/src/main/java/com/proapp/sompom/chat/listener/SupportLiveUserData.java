package com.proapp.sompom.chat.listener;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import com.proapp.sompom.model.result.User;

public interface SupportLiveUserData {

    default String getActivityContainerName() {
        if (getViewContext() instanceof AppCompatActivity) {
            return ((AppCompatActivity) getViewContext()).getClass().getSimpleName();
        }

        return null;
    }

    Context getViewContext();

    User getUser();

    default String getUserName() {
        if (getUser() != null) {
            return getUser().getFullName();
        }

        return null;
    }
}
