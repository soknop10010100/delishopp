package com.proapp.sompom.chat.service;


import static com.proapp.sompom.newui.ChatActivity.REFRESH_NOTIFICATION_BADGE_ACTION;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFrame;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.chat.listener.ServiceStateListener;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.database.MessageDb;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.ConversationHelper;
import com.proapp.sompom.helper.GsonHelper;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.listener.ChatMediaUploadListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnChatSocketListener;
import com.proapp.sompom.listener.OnMessageDbListener;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.concierge.ABAPaymentPushBack;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.SocketError;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.ChatActivity;
import com.proapp.sompom.newui.fragment.AbsSupportABAPaymentFragment;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.ServerUrl;
import com.proapp.sompom.utils.ChatNotificationUtil;
import com.proapp.sompom.utils.NotificationUtils;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.sompom.pushy.service.SendBroadCastHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import io.github.sac.Ack;
import io.github.sac.CustomBasicListener;
import io.github.sac.CustomSocket;
import io.github.sac.ReconnectStrategy;
import io.reactivex.Observable;
import io.realm.RealmList;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by He Rotha on 10/26/18.
 */
public class ChatSocket extends CustomSocket {

    public static final String UPDATE_USER_EVENT = "UPDATE_USER_EVENT";

    private final Context mContext;
    private final ApiService mApiService;
    private final LinkedList<Chat> mChatUploadLinkedList = new LinkedList<>();
    private OnChatSocketListener mOnChatSocketListener;
    private Listener mSubscribeListener;
    private ServiceStateListener mServiceStateListener;
    private SocketService mSocketService;

    private final OnMessageDbListener mOnMessageDbListener = new OnMessageDbListener() {
        @Override
        public void onConversationDelete(String conversationId) {

            if (mOnChatSocketListener != null) {
                mOnChatSocketListener.onConversationDelete(conversationId);
            }
        }

        @Override
        public void onConversationUpdate(Conversation conversation) {
            if (mOnChatSocketListener != null) {
                mOnChatSocketListener.onConversationCreateOrUpdate(conversation, "mOnMessageDbListener");
            }
        }
    };
    private String mMyUserId;
    private Gson mGson;
    private List<ChatMediaUploadListener> mChatMediaUploadListeners = new ArrayList<>();

    ChatSocket(Context context, SocketService socketService, ServiceStateListener listener, ApiService apiService) {
        super(ServerUrl.getSocketUrl(context));
        mSocketService = socketService;
        mContext = context;
        mApiService = apiService;
        initSubscriptionCallback();
        setReconnection(new ReconnectStrategy());
        mServiceStateListener = listener;
        setListener(new CustomBasicListener() {
            @Override
            public void onConnected(CustomSocket socket, Map<String, List<String>> headers) {
                Timber.e("onConnected");
                MainApplication.setSocketConnected(true);
                subscribeAllConversationChannels();
                try {
                    if (mServiceStateListener != null) {
                        mServiceStateListener.onServiceStart();
                    }

                    //clear all queue message, cos when network reconnected,
                    //unsent message will be resent, and start save queue again
                    mChatUploadLinkedList.clear();

                    checkToSendRemainingLocalSendingReadMessageStatus();
                    checkToSendRemainingSendingStatusLocalMessage();
                } catch (Exception ex) {
                    Timber.e("ex: %s", ex.toString());
                    SentryHelper.logSentryError(ex);
                }
            }

            @Override
            public void onDisconnected(CustomSocket socket,
                                       WebSocketFrame serverCloseFrame,
                                       WebSocketFrame clientCloseFrame,
                                       boolean closedByServer) {
                Timber.e("onDisconnected: closedByServer: " + closedByServer +
                        ", serverCloseFrame: " + new Gson().toJson(serverCloseFrame) +
                        ", clientCloseFrame: " + new Gson().toJson(clientCloseFrame));
                MainApplication.setSocketConnected(false);
            }

            @Override
            public void onConnectError(CustomSocket socket, WebSocketException ex) {
                Timber.e("onConnectError: " + ex.getMessage() + ", " + ex.getError().name());
                MainApplication.setSocketConnected(false);
                if (mServiceStateListener != null) {
                    mServiceStateListener.onServiceFail(ex);
                }
            }

            @Override
            public void onAuthentication(CustomSocket socket, Boolean status) {
                Timber.i("onAuthentication: status: " + status);
            }

            @Override
            public void onSetAuthToken(String token, CustomSocket socket) {
                Timber.i("onSetAuthToken: token: " + token);
            }

            @Override
            public void onNoMoreFrameFromSocketServer() {
                Timber.i("onNoMoreFrameFromSocketServer");
                /*
                    Socket serve has close the connection without sending client the close frame.
                    So we have to reconnect socket server manually.
                 */
                connectAsync();
            }
        });
    }

    public void clearSubscribeListener() {
        mSubscribeListener = null;
    }

    private void checkToSendRemainingSendingStatusLocalMessage() {
        List<Chat> sending = MessageDb.queryByStatusAndUserId(mContext, MessageState.SENDING, mMyUserId);
        if (sending != null && !sending.isEmpty()) {
            Timber.i("checkToSendRemainingLocalMessage %s", sending.size());
            for (Chat chat : sending) {
                sendTo(chat, false);
            }
        }
    }

    private void checkToSendRemainingLocalSendingReadMessageStatus() {
        long counter = MessageDb.getSendingReadStatusMessageOfCurrentUserCount(mContext);
        Timber.i("checkToSendRemainingLocalSendingReadMessageStatus: counter: " + counter);
        if (counter > 0) {
            List<Chat> remainingSendReadStatusMessages = MessageDb.buildSendingReadStatusOfCurrentUser(mContext);
            resendReadStatusMessage(remainingSendReadStatusMessages);
        }
    }

    public void clearServiceStateListener(ServiceStateListener listener) {
        if (listener != null && listener.equals(mServiceStateListener)) {
            Timber.i("clearServiceStateListener");
            mServiceStateListener = null;
        }
    }

    private void checkToUpdateCurrentUserData(Chat updateMessage) {
        if (updateMessage.getUser() != null) {
            Timber.i("checkToUpdateCurrentUserData: " + new Gson().toJson(updateMessage.getUser()));
            SharedPrefUtils.setUserValue(updateMessage.getUser(), mContext);
            SendBroadCastHelper.verifyAndSendBroadCast(mContext, new Intent(UPDATE_USER_EVENT));
        }
    }

    private void checkToBroadCastUserOnlineStatusOnReceivingMessage(Chat chat) {
        //Construct temp sender if not exist for broadcasting user online status inside screen.
        if (chat != null && chat.getSender() == null) {
            User sender = new User();
            sender.setId(chat.getSenderId());
            sender.setLastActivity(chat.getDate());
            chat.setSender(sender);
        }

        if (chat != null &&
                chat.getSender() != null &&
                mSocketService != null &&
                mSocketService.getBinder() != null &&
                !TextUtils.equals(chat.getSenderId(), mMyUserId) &&
                chat.getSendingType() != Chat.SendingType.EMIT_USER) {
            //If current use receive a chat via Socket, that sender should mark as online too.
            mSocketService.getBinder().setUserStatus(chat.getSender(),
                    true,
                    chat.getSendingType() == Chat.SendingType.TYPING,
                    "checkToBroadCastUserOnlineStatusOnReceivingMessage");
            Timber.i("Broadcast chat sender online status.");
        }
    }

    private void handleOnReceiveSocketMessageFromSubscription(Chat chat) {
        /*
           As the sender of message, after receiving sent message status back from socket with own channel
           subscription, we will manage to update local date chat with the date from this receiving chat.
           The idea is to make sure we have the correct date from server. For sometimes, as we send
           message from current device, the date between the creation of socket message which is managed locallay
           during the sending and the date saved on server might a bit different which might lead to
           the incorrect message display order when there are chats fast at the same times from other
           participants.
         */
        if (TextUtils.equals(chat.getSenderId(), mMyUserId) &&
                chat.getStatus() == MessageState.SENT &&
                chat.getSendingType() != Chat.SendingType.TYPING &&
                chat.getSendingType() != Chat.SendingType.READ_STATUS &&
                !ChatHelper.isModifyChatType(chat)) {
            if (chat.getDate() != null) {
                Timber.i("Receive sent message callback from socket of current account and" +
                        " will update the local date of that message. Date of message: " + ChatHelper.getChatDateUTC(chat.getDate()));
                MessageDb.updateLocalMessageDate(mContext, chat.getId(), chat.getDate());
            }
        }

        /*
            Must call this method at this place to make sure that only handling receiving message
            will invoke this checking
        */
        checkToBroadCastUserOnlineStatusOnReceivingMessage(chat);

        if (Chat.ChatType.from(chat.getChatType()) == Chat.ChatType.CREATE_GROUP) {
            ConversationHelper.checkIfConversationExistedAndBelongToCurrentUser(mContext,
                    chat.getChannelId(),
                    mApiService,
                    () -> {
                        subscribeToAllGroupChannels(Collections.singletonList(chat.getChannelId()));
                        ConversationHelper.broadcastUpdateConversationListAction(mContext);
                        ChatNotificationUtil.pushChatNotification(mContext,
                                chat,
                                false,
                                "ChatSocket: CREATE_GROUP");
                    });
            return;
        }

        if (Chat.ChatType.from(chat.getChatType()) == Chat.ChatType.DELETE_GROUP) {
            ConversationHelper.onGroupConversationRemoved(mContext, chat.getChannelId());
            return;
        }

        if (chat.getSendingType() == Chat.SendingType.UPDATE_USER && chat.getUser() != null) {
            Timber.i("Receive synchronize current user update information.");
            checkToUpdateCurrentUserData(chat);
            return;
        }

        //Need to emit back user id to server to inform sever that current user connected to Socket.
        if (chat.getSendingType() == Chat.SendingType.EMIT_USER && TextUtils.equals(chat.getSendTo(),
                SharedPrefUtils.getUserId(mContext))) {
            registerUserToSocketServer();
        }

        if (ignoreSomeMessageTypeFromOwnAccount(chat)) {
            Timber.i("Ignore some message types from own user account.");
            return;
        }

            /*
                Start checking receiving message base on type.
             */
        if (chat.getSendingType() == Chat.SendingType.DELIVERED) {
            checkToUpdateLocalDeliveredStatusMessage(chat);
            return;
        }

        if (CallHelper.isVOIPCallMessage(chat)) {
            if (mOnChatSocketListener != null) {
                mOnChatSocketListener.onMessageReceive(chat, null);
            }
            return;
        }

        Chat.SendingType sendingType = chat.getSendingType();
        if (sendingType == Chat.SendingType.TYPING) {
            Timber.e("Sender is typing...");
            mOnChatSocketListener.onRecipientTypingMessage(chat);
        } else if (sendingType == Chat.SendingType.READ_STATUS) {
            if (MessageDb.queryById(mContext, chat.getId()) == null) {
                Timber.i("Ignore checking read status for none existing local chat.");
                return;
            }

            Timber.i("Received read status from other participant or same account. " + new Gson().toJson(chat));
                /*
                    Handle current user read status message synchronization from different logged in
                    devices. In this case, we just mark that last message of conversation which seen by
                    current user on another device and refresh message badge.
                 */
            if (TextUtils.equals(chat.getSenderId(), mMyUserId) &&
                    MessageDb.isLastMessageOfConversation(mContext, chat.getChannelId(), chat.getId())) {
                Chat lastMessageOfConversation = MessageDb.getLastMessageOfConversation(mContext, chat.getChannelId());
                if (lastMessageOfConversation != null) {
                    lastMessageOfConversation.setStatus(MessageState.SEEN);
                    MessageDb.save(mContext, lastMessageOfConversation);

                    ConversationDb.updateLastMessageOfConversation(mContext,
                            lastMessageOfConversation,
                            true,
                            true);

                    ConversationDb.updateLastSeenMessageStatusOfCurrentUser(mContext,
                            lastMessageOfConversation,
                            null);

                    Intent readLastMessageBroadcastIntent = new Intent(ChatActivity.UPDATE_READ_CONVERSATION_STATUS_ACTION);
                    readLastMessageBroadcastIntent.putExtra(ChatActivity.CONVERSATION_ID, chat.getChannelId());
                    SendBroadCastHelper.verifyAndSendBroadCast(mContext, readLastMessageBroadcastIntent);

                    Intent refreshBadgeBroadcastIntent = new Intent(REFRESH_NOTIFICATION_BADGE_ACTION);
                    refreshBadgeBroadcastIntent.setPackage(mContext.getPackageName());
                    SendBroadCastHelper.verifyAndSendBroadCast(mContext, refreshBadgeBroadcastIntent);
                }
            }

                /*
                    For individual chat, if there is no previous unseen messages and return process.
                 */
            if (!chat.isGroup()) {
                /*
                 We just update the message status to seen and keep the original message info which
                 will be important for later use and to have the same behavior as server which only
                 the status of message is updated when exchange.
                 */
                List<Chat> chats = MessageDb.queryUnreadMessageFromIdAndUpdateStatusToSeen(mContext,
                        chat.getId(),
                        chat.getChannelId());
                if (chats == null || chats.isEmpty()) {
                    return;
                }

                for (Chat chat1 : chats) {
                    Timber.i("Receive seen message status from individual chat message.");
                    /*
                        Will update previous un seen messages to be seen by current sending read status user
                        at run time.
                        Will update the senderId and sendTo id base on received message so as to update
                        the UI display properly and be noted that it is just run time update and
                        not been saved in local db.
                     */
                    chat1.setSenderId(chat.getSenderId());
                    if (!TextUtils.equals(chat.getId(), chat1.getId())) {
                            /*
                            Will ignore update seen avatar view in list and just remove message status icon and will
                            let the last message handle the seen avatar.
                             */
                        chat1.setIgnoreSeenStatus(true);
                    }
                    Timber.i("Seen chat: " + chat1.getContent());
                    mOnChatSocketListener.onMessageReceive(chat1, null);
                }
            } else {
                Chat localChat = MessageDb.queryById(mContext, chat.getId());
                boolean isIgnoreViewUpdate = false;
                if (localChat == null) {
                    /*
                     * If the seen message does not exist in database, then save it into local.
                     * For some case, the seen status message may arrive before the receiving message.
                     */
                    chat.setSavedFromSeenStatus(true);
                    MessageDb.save(mContext, chat);
                    localChat = MessageDb.queryById(mContext, chat.getId());
                    isIgnoreViewUpdate = true; //No need to update view for we only need to save its data.
                }

                if (localChat != null) {
                    Timber.i("Receive seen message status from group chat message.:");
                    /*
                        Update seen status if that message which sent by current user has seen
                        by other users.
                     */
                    if (localChat.getStatus() != MessageState.SEEN) {
                        localChat.setStatus(MessageState.SEEN);
                        MessageDb.save(mContext, localChat);
                    }

                    //Mark sender of message to seen their message locally too.
                    Timber.i("Mark this message been seen by original sender locally.");
                    ConversationDb.updateLastSeenMessageStatusOfUser(mContext,
                            chat,
                            chat.getSenderId(),
                            "READ_STATUS",
                            mOnChatSocketListener);
                 /*
                    We just update the message status to seen and keep the original message info which
                    will be important for later use and to have the same behavior as server which only
                    the status of message is updated when exchange.
                 */
                    List<Chat> chats = MessageDb.queryUnreadMessageFromIdAndUpdateStatusToSeen(mContext,
                            chat.getId(),
                            chat.getChannelId());
                    if (chats != null && !chats.isEmpty()) {
                        for (Chat chat2 : chats) {
                            //Ignore the last message.
                            if (!chat2.getId().matches(chat.getId())) {
                                Timber.i("Update previous unread message to read after receive read status: " + chat2.getId() +
                                        ", content: " + chat2.getContent());
                                    /*
                                    Will update previous un seen messages to be seen by current sending read status user
                                    at run time.
                                    Will update the senderId id base on received message so as to update
                                    the UI display properly and be noted that it is just run time update and
                                    not been saved in local db.
                                    */
                                chat2.setSenderId(chat.getSenderId());
                                    /*
                                    Will ignore update seen avatar view in list and just remove message status icon and will
                                    let the last message handle the seen avatar.
                                     */
                                chat2.setIgnoreSeenStatus(true);
                                mOnChatSocketListener.onMessageReceive(chat2, "Update previous unread message to read after receive read status");
                            }
                        }
                    }

                    if (!isIgnoreViewUpdate) {
                        //Update at run time to make UI aware that should send the read status so
                        //as to update seen avatar properly.
                        localChat.setSenderId(chat.getSenderId());
                        mOnChatSocketListener.onMessageReceive(localChat, "!isIgnoreViewUpdate");
                    }
                }
            }
        } else if (sendingType == Chat.SendingType.DELETE) {
            //Check double check to make sure the deleted message status will display properly
            if (!chat.isDeleted()) {
                chat.setDelete(true);
            }
            Timber.i("The other person deleted a message");
            Chat newChat = MessageDb.queryById(mContext, chat.getId());
            MessageDb.delete(mContext, newChat, mOnMessageDbListener);
            mOnChatSocketListener.onMessageReceive(chat, "The other person deleted a message");
            if (mOnChatSocketListener.isInBg(chat.getChannelId()) && !isChatOfCurrentUser(chat)) {
                ChatNotificationUtil.pushChatNotification(mContext,
                        chat,
                        false,
                        "ChatSocket: The other person deleted a message");
            }
        } else if (sendingType == Chat.SendingType.UPDATE) {
            MessageDb.updateEditedMessage(mContext, chat);
            Timber.i("Will update message status to edited in local.");
            mOnChatSocketListener.onMessageReceive(chat, "Will update message status to edited in local.");
            if (MessageDb.isLastMessageOfConversation(mContext, chat.getChannelId(), chat.getId())) {
                Conversation conversation = ConversationDb.updateLastMessageOfConversation(mContext,
                        chat,
                        false,
                        true);
                if (conversation != null) {
                    mOnChatSocketListener.onConversationCreateOrUpdate(conversation, "UPDATE");
                }
            }
            if (mOnChatSocketListener.isInBg(chat.getChannelId()) &&
                    !isChatOfCurrentUser(chat)) {
                ChatNotificationUtil.pushChatNotification(mContext,
                        chat,
                        false,
                        "ChatSocket: Will update message status to edited in local.");
            }
        } else if (sendingType == Chat.SendingType.NONE) {
            Timber.i("You receive a new message");
            receiveChat(chat);
        }
    }

    private void initSubscriptionCallback() {
        mSubscribeListener = (name, data) -> {
            Timber.i("onSubscribe callback: " + name + ", data:" + data.toString());
            ABAPaymentPushBack abaPaymentPushBack = getGson().fromJson(data.toString(), ABAPaymentPushBack.class);
            if (abaPaymentPushBack.getType() == ABAPaymentPushBack.ABAPaymentPushBackType.ORDER_PAYMENT_SUCCESS ||
                    abaPaymentPushBack.getType() == ABAPaymentPushBack.ABAPaymentPushBackType.ORDER_PAYMENT_FAIL) {
                Timber.i("Got ABAPaymentPushBack and will broadcast this even to subscriber of this event.");
                Intent intent = new Intent(AbsSupportABAPaymentFragment.RECEIVE_ABA_PAYMENT_PUSHBACK_EVENT);
                intent.putExtra(AbsSupportABAPaymentFragment.BASKET_ID, abaPaymentPushBack.getOrder().getId());
                intent.putExtra(AbsSupportABAPaymentFragment.IS_ABA_PAYMENT_SUCCESS,
                        abaPaymentPushBack.getType() == ABAPaymentPushBack.ABAPaymentPushBackType.ORDER_PAYMENT_SUCCESS);
                intent.putExtra(AbsSupportABAPaymentFragment.ABA_PAYMENT_PUSHBACK_MESSAGE, abaPaymentPushBack.getMessage());
                SendBroadCastHelper.verifyAndSendBroadCast(mContext, intent);
            } else {
                Chat chat = getGson().fromJson(data.toString(), Chat.class);
                handleOnReceiveSocketMessageFromSubscription(chat);
            }
        };
    }

    public void performReceiveSocketMessageManuallyForChatFromNotification(Chat chat) {
        handleOnReceiveSocketMessageFromSubscription(chat);
    }

    public void addUploadMediaListener(ChatMediaUploadListener listener) {
        if (!mChatMediaUploadListeners.contains(listener)) {
            mChatMediaUploadListeners.add(listener);
        }
    }

    public void removeUploadMediaListener(ChatMediaUploadListener listener) {
        mChatMediaUploadListeners.remove(listener);
    }

    public void clearAllUploadMediaListeners() {
        mChatMediaUploadListeners.clear();
    }

    public static boolean isSeenChatAlreadySaved(Context context, Chat chat) {
        Chat localChat = MessageDb.queryById(context, chat.getId());
        return localChat != null && localChat.getStatus() == MessageState.SEEN;
    }

    /**
     * @return true, mean chat is existed, otherwise, it doesn't
     */
    public static boolean isExistingChat(Context context, Chat chat) {
        Chat localChat = MessageDb.queryById(context, chat.getId());
        if (localChat != null) {
            Timber.i("isExistingChat: getSavedFromSeenStatus: " + localChat.getSavedFromSeenStatus() + ", MessageState: " + localChat.getStatus());
        }
        return localChat != null &&
                !localChat.getSavedFromSeenStatus() &&
                (localChat.getStatus() == chat.getStatus() || localChat.getStatus() == MessageState.SEEN);
    }

    public void publishVOIPCallMessage(Chat message, SendMessageCallback callback) {
        /*
        Must inject date for call socket for we will use this date in receiving call notification to
        check if the incoming call is still in active or time-out.
         */
        if (message.getDate() == null) {
            message.setDate(new Date());
        }
        injectSomeValueToSendingMessageOfCurrentUser(message);
        Timber.i("publishVOIPCallMessage message: " + new Gson().toJson(message));
        publishMessage(message, callback == null ? new SendMessageCallback(message) : callback);
    }

    private void injectSomeValueToSendingMessageOfCurrentUser(Chat chat) {
        //Must add the sender of message in in seen id list by default before send via Socket.
        if (chat.getSeen() == null) {
            chat.setSeen(new RealmList<>());
            chat.getSeen().add(mMyUserId);
        } else if (!chat.getSeen().contains(mMyUserId)) {
            chat.getSeen().add(mMyUserId);
        }
    }

    private void publishMessage(Chat chat, BasePublishMessageCallback ack) {
        checkToUpdateSendTo(chat);
        Timber.i("publishMessage: Id: " + chat.getId() +
                ", sendTo: " + chat.getSendTo() +
                ", senderId: " + chat.getSenderId() +
                ", channel id: " + chat.getChannelId() +
                ", date: " + ChatHelper.getChatDateUTC(chat.getDate()) +
                ", isGroup: " + chat.isGroup());
        excludeSomeLocalPropertiesOfFileChatMedia(chat);
        /*
            As current send message via Socket, we have to mark current as online along with data
            to make sure on the receiver side update the user online status correctly.
         */
        injectSomeMandatoryDataOfSender(chat);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(getGson().toJson(chat));
            excludePropertiesFromSendObject(jsonObject, "mIsSavedFromSeenStatus", "mIsDecrypted");
            if (chat.getSendingType().equals(Chat.SendingType.READ_STATUS)
                    || chat.getSendingType().equals(Chat.SendingType.DELETE)
                    || chat.getSendingType().equals(Chat.SendingType.TYPING)) {
                // Delete isEncrypted status if not regular chat
                excludePropertiesFromSendObject(jsonObject, "isEncrypted");
            }
        } catch (Exception e) {
            Timber.e("publishMessage Error: " + e.getMessage());
            SentryHelper.logSentryError(e);
        }
        Timber.i("123456789 " + getGson().toJson(chat));
        if (jsonObject != null) {
            Timber.i("publishMessage: " + jsonObject.toString());
            if (ack != null) {
                publish(getPublishChannelId(chat), jsonObject, ack);
            } else {
                publish(getPublishChannelId(chat), jsonObject);
            }
        } else {
            Timber.e("Can't publish message since the jsonObject is null.");
        }
    }

    private void injectSomeMandatoryDataOfSender(Chat chat) {
        //Normally current user is the sender.
        if (chat.getSender() != null) {
            User user = SharedPrefUtils.getUser(mContext);
            Timber.i("Current user: ");
            chat.getSender().setHash(user.getHash());
            chat.getSender().setLastActivity(user.getLastActivity());
            chat.getSender().setUserProfileThumbnail(user.getUserProfileThumbnail());
            chat.getSender().setOnline(true);
        }
    }

    private void checkToUpdateSendTo(Chat chat) {
        /*
        For group we will send to specific group id channel and for individual we keep sending to server
        channel.
         */
        if (chat.isGroup()) {
            chat.setSendTo(chat.getChannelId());
        }
    }

    private String getPublishChannelId(Chat chat) {
        /*
        For group we will send to specific group id channel and for individual we keep sending to server
        channel.
         */
        if (chat.isGroup()) {
            return chat.getChannelId();
        } else {
            return chat.getSendTo();
        }

    }

    private void excludeSomeLocalPropertiesOfFileChatMedia(Chat chat) {
        if (chat.getType() == Chat.Type.FILE && chat.getMediaList() != null) {
            for (Media media : chat.getMediaList()) {
                media.resetFileStatusType();
                media.setDownloadedPath(null);
            }
        }

        //Remove link preview
        chat.setLinkPreviewModel(null);
    }

    private void excludePropertiesFromSendObject(JSONObject jsonObject, String... keys) {
        if (jsonObject != null && keys != null && keys.length > 0) {
            for (String key : keys) {
                jsonObject.remove(key);
            }
        }
    }

    private void loadNewMessageOfCurrentScreen(Context context) {
        List<String> channels = mOnChatSocketListener.getCurrentChannel();
        for (String channel : channels) {
            final Chat chat = MessageDb.getLastUnseenMessage(context, channel);
            if (chat == null) {
                continue;
            }
            //Download message data from server if db is empty
            Observable<Response<List<Chat>>> ob = ChatHelper.concatLoadChatResultFromServer(mContext,
                    chat.getChannelId(),
                    mApiService.getLatestChatList(chat.getChannelId(), chat.getId()));
            ResponseObserverHelper<Response<List<Chat>>> call = new ResponseObserverHelper<>(mContext, ob);
            call.execute(new LoadNewMessageCallback(chat));
        }
    }

    void startListener(OnChatSocketListener onChatSocketListener) {
        mOnChatSocketListener = onChatSocketListener;
        mMyUserId = SharedPrefUtils.getUserId(mContext);
        if (TextUtils.isEmpty(mMyUserId)) {
            return;
        }
        Timber.i("Listener my channel %s", mMyUserId);
        //Subscribe own user channel id for individual chatting over Socket.
        subscribeToMyOwnConversationChannel();
    }

    private void subscribeToMyOwnConversationChannel() {
        //Subscribe too my own channel
        if (getChannelByName(mMyUserId) == null) {
            createChannel(mMyUserId).subscribe();
            onSubscribe(mMyUserId, mSubscribeListener);
            Timber.i("Subscribe to my own channel.");
        }
    }

    private void subscribeAllConversationChannels() {
        Timber.i("subscribeAllConversationChannels");
        subscribeToMyOwnConversationChannel();
        subscribeToAllGroupChannels();
    }

    private void registerUserToSocketServer() {
        emit("register", mMyUserId, (name, error, data) ->
                Timber.i("Emitting for self registering: name: " + name + ", error: " + error + ", data: " + data));
    }

    public void subscribeToAllGroupChannels() {
        subscribeToAllGroupChannels(ConversationHelper.getUserGroupConversationId(mContext));
    }

    public void subscribeToAllGroupChannels(List<String> channelIds) {
          /*
            Will subscribe all group conversation channel ids for group chatting over Socket to optimize
            broadcast sending message on server side which sometimes failed to relay the message to client.
         */
        Timber.i("subscribeToAllGroupChannels: " + new Gson().toJson(channelIds));
        if (channelIds != null && !channelIds.isEmpty()) {
            for (String groupId : channelIds) {
                if (getChannelByName(groupId) == null) {
                    createChannel(groupId).subscribe();
                    onSubscribe(groupId, mSubscribeListener);
                    Timber.i("Subscribe to group channel: " + groupId);
                }
            }
        }
    }

    private boolean ignoreSomeMessageTypeFromOwnAccount(Chat chat) {
        return TextUtils.equals(chat.getSenderId(), mMyUserId) &&
                (chat.getSendingType() == Chat.SendingType.DELIVERED ||
                        chat.getSendingType() == Chat.SendingType.TYPING ||
                        CallHelper.isInComingVOIPCallMessageType(chat));

    }

    private boolean isGroupConversation(String channelId) {
        Conversation conversationById = ConversationDb.getConversationById(mContext, channelId);
        return conversationById != null && conversationById.isGroup();
    }

    void receiveChat(Chat chat) {
        Timber.i("receiveChat: " + new Gson().toJson(chat));
        ConversationHelper.checkIfConversationExistedAndBelongToCurrentUser(mContext,
                chat.getChannelId(),
                mApiService,
                () -> {
                    if (ConversationHelper.isGroupModificationMessageType(chat) && chat.getMetaGroup() == null) {
                        Timber.i("Invalid group modification message types.");
                        return;
                    }
                    checkToSendDeliveredMessage(chat, false, false);
                    processReceivedChat(chat);
                });
    }

    void processReceivedChat(Chat chat) {
        if (isExistingChat(mContext, chat)) {
            Timber.i("Chat is existed in local db.");
            return;
        }

        //If user is not in chat screen, the state of is in background is consider as true too.
        boolean isInBg = mOnChatSocketListener.isInBg(chat.getChannelId());
        Timber.i("processReceivedChat: isInBg: " + isInBg);

        /*
         Always reset this status to false for receiving message for at some point this property is
         updated to true when receiving seen status come earlier than receiving first sending message.
         */
        chat.setSavedFromSeenStatus(false);
        Timber.i("Is in chat screen: " + mOnChatSocketListener.isInChatScreen(chat.getChannelId()));
         /*
            Must check to update group conversation in local first if the notification type is from
            group modification so as to have the latest group info for future usage.
         */
        boolean isRemoveMeFromGroup = false;
        if (ConversationHelper.isGroupModificationMessageType(chat)) {
            //Currently server send the message status as sent which we have to update manually for this type
            if (!(chat.getStatus() == MessageState.SENT || chat.getStatus() == MessageState.SEEN)) {
                chat.setStatus(MessageState.SENT);
            }

            if (ConversationHelper.isAddMeToGroup(mContext, chat, chat.getMetaGroup())) {
                checkToSubscribeOrUnsubscribeGroupConversation(chat.getChannelId(), false);
            }

            isRemoveMeFromGroup = ConversationHelper.checkToUpdateGroupData(mContext,
                    chat.getChannelId(),
                    chat.getMetaGroup(),
                    chat,
                    "ChatSocket");

            if (isRemoveMeFromGroup) {
                checkToSubscribeOrUnsubscribeGroupConversation(chat.getChannelId(), true);
            }
        }

        /*
        Update message badge display
        No need to add conversation badge if the current user was removed from any group conversation for
        there is no more display of that group conversation.
         */
        if (!isChatOfCurrentUser(chat) &&
                !isRemoveMeFromGroup &&
                !mOnChatSocketListener.isInChatScreen(chat.getChannelId())) {
            Intent intent = new Intent(REFRESH_NOTIFICATION_BADGE_ACTION);
            intent.setPackage(mContext.getPackageName());
            SendBroadCastHelper.verifyAndSendBroadCast(mContext, intent);
        }

        //if app in background, just update badge, save, push notification...
        if (isInBg && !isReceivingGroupModificationOfCurrentUser(chat)) {
            //save chat in db
            MessageDb.save(mContext, chat);

            //update conversation
            ConversationDb.updateLastMessageOfConversation(mContext,
                    chat,
                    isChatOfCurrentUser(chat),
                    false);

            //Check to dispatch local chat notification.
            if (!isChatOfCurrentUser(chat) &&
                    !isIgnoreCallMessageNotification(chat) &&
                    !isUserTheSenderOfOneToOneCallMessage(chat)) {
                ChatNotificationUtil.pushChatNotification(mContext,
                        chat,
                        isRemoveMeFromGroup,
                        "ChatSocket: Check to dispatch local chat notification.");
            }
        } else {
            MessageDb.save(mContext, chat);
            mOnChatSocketListener.onMessageReceive(chat, null);

            ConversationDb.updateLastMessageOfConversation(mContext,
                    chat,
                    isChatOfCurrentUser(chat),
                    false);
            if (!isChatOfCurrentUser(chat)) {
                //Ignore set seen status for call even chat.
                if (!TextUtils.equals(chat.getSenderId(), mMyUserId)) {
                    chat.setStatus(MessageState.SEEN);
                    MessageDb.save(mContext, chat);
                }
                Conversation conversationById = ConversationDb.getConversationById(mContext, chat.getChannelId());
                Timber.i("conversationById: " + new Gson().toJson(conversationById));
                boolean isGroupConversation = isGroupConversation(conversationById);
                sendReadStatusMessage(chat, isGroupConversation ? chat.getChannelId() : chat.getSenderId());
                List<Chat> chats = MessageDb.queryUnreadMessageFromIdAndUpdateStatusToSeen(mContext,
                        chat.getId(),
                        chat.getChannelId());
                if (chats != null && !chats.isEmpty()) {
                    for (Chat chat1 : chats) {
                        if (!TextUtils.equals(chat.getId(), chat1.getId())) {
                            /*
                            Will ignore update seen avatar view in list and just remove message status icon and will
                            let the last message handle the seen avatar.
                             */
                            chat1.setIgnoreSeenStatus(true);
                        }
                        mOnChatSocketListener.onMessageReceive(chat1, "processReceivedChat queryUnreadMessageFromIdAndUpdateStatusToSeen");
                    }
                }
            }
        }

        //Mark sender of the message to seen this message as their last seen locally.
        ConversationDb.updateLastSeenMessageStatusOfUser(mContext,
                chat,
                chat.getSenderId(),
                "processReceivedChat",
                mOnChatSocketListener);

        Conversation conversationById = ConversationDb.getConversationById(mContext,
                chat.getChannelId());
        if (conversationById != null && conversationById.getLastMessage() == null) {
            /*
                Since the last message field of conversation will not saved in database and to update
                last message in conversation screen, we need last message, so we will set current message
                as last message of the conversation.
             */
            conversationById.setLastMessage(chat);
        }

        if (!ConversationHelper.isGroupModificationMessageType(chat)) {
            mOnChatSocketListener.onConversationCreateOrUpdate(conversationById, "processReceivedChat");
        }
    }

    public void invokeIncreaseMessageBadge(int increaseValue) {
        mOnChatSocketListener.onBadgeUpdate(increaseValue);
    }

    public void checkToSubscribeOrUnsubscribeGroupConversation(String groupChannelId, boolean isRemoved) {
        if (isRemoved) {
            Channel channelByName = getChannelByName(groupChannelId);
            if (channelByName != null) {
                Timber.i("Remove subscribe to group channel: " + groupChannelId);
                channelByName.unsubscribe();
            }
        } else {
            if (getChannelByName(groupChannelId) == null) {
                createChannel(groupChannelId).subscribe();
                onSubscribe(groupChannelId, mSubscribeListener);
                Timber.i("Add subscribe to group channel: " + groupChannelId);
            }
        }
    }

    private boolean isReceivingGroupModificationOfCurrentUser(Chat chat) {
        return ConversationHelper.isGroupModificationMessageType(chat) && TextUtils.equals(mMyUserId, chat.getSenderId());
    }

    private boolean isChatOfCurrentUser(Chat chat) {
        //Current user is the sender of the message.
        return TextUtils.equals(mMyUserId, chat.getSenderId());
    }

    private boolean isIgnoreCallMessageNotification(Chat chat) {
        Chat.ChatType type = Chat.ChatType.from(chat.getChatType());
        return type == Chat.ChatType.CALLED ||
                type == Chat.ChatType.START_GROUP_CALL ||
                type == Chat.ChatType.START_GROUP_VIDEO_CALL ||
                type == Chat.ChatType.END_GROUP_CALL ||
                type == Chat.ChatType.END_GROUP_VIDEO_CALL;
    }

    public void checkToSendDeliveredMessage(Chat receivedChat,
                                            boolean shouldDisconnectSocketAfterSend,
                                            boolean ignoreIfLocalChatExisted) {
        /*
          As the recipient of the message, we will send back the delivered status to sender only if the
          message has following condition, a fresh receiving message.
         */
        Chat chat = MessageDb.queryById(mContext, receivedChat.getId());
        if ((ignoreIfLocalChatExisted || chat == null) &&
                !isChatOfCurrentUser(receivedChat) &&
                TextUtils.isEmpty(receivedChat.getChatType()) &&
                TextUtils.isEmpty(receivedChat.getSendingTypeAsString())) {
            Timber.i("Will send back the delivered status to the message sender: shouldDisconnectSocketAfterSend: " + shouldDisconnectSocketAfterSend);
            Chat newDeliveredChat = Chat.getNewDeliveredChat(mContext, receivedChat);
            Timber.i("newDeliveredChat: " + new Gson().toJson(newDeliveredChat));
            if (!shouldDisconnectSocketAfterSend) {
                publishMessage(newDeliveredChat, new BasePublishMessageCallback(newDeliveredChat) {
                    @Override
                    void onCustomCall(String channelName, Object error, Chat ackChat) {
                        Timber.i("checkToSendDeliveredMessage callback: channelName: " + channelName
                                + ", error: " + error +
                                ", ackChat: " + new Gson().toJson(ackChat));
                        if (error == null) {
                            MessageDb.markLocalMessageAsDelivered(mContext, receivedChat.getId());
                        }
                    }
                });
            } else {
                publishMessage(newDeliveredChat,
                        new BasePublishMessageCallback(newDeliveredChat,
                                (channelName, error, ackChat) -> {
                                    if (error == null) {
                                        MessageDb.markLocalMessageAsDelivered(mContext, receivedChat.getId());
                                    }
                                    disconnect("checkToSendDeliveredMessage");
                                }));
            }
        }
    }

    public void disconnect(String tag) {
        Timber.i("Call disconnect socket from " + tag);
        super.disconnect();
    }

    private void checkToUpdateLocalDeliveredStatusMessage(Chat receivedChat) {
        if (receivedChat.getSendingType() == Chat.SendingType.DELIVERED) {
            //Update previous sent message to delivered.
            List<Chat> list = MessageDb.updatePreviousMessageToDelivered(mContext, receivedChat, receivedChat.getChannelId());
            if (list != null) {
                for (Chat chat1 : list) {
                    mOnChatSocketListener.onMessageReceive(chat1, "checkToUpdateLocalDeliveredStatusMessage");
                }
            }
        }
    }

    private boolean isCurrentUserCaller(Chat chat) {
        String userId = SharedPrefUtils.getUserId(mContext);
        return TextUtils.equals(userId, chat.getSenderId());
    }

    private boolean isUserTheSenderOfOneToOneCallMessage(Chat chat) {
        return !chat.isGroup() && isCurrentUserCaller(chat);
    }

    public static boolean isCallEvenChat(Chat chat) {
        return Chat.ChatType.from(chat.getChatType()) == Chat.ChatType.MISSED_CALL
                || Chat.ChatType.from(chat.getChatType()) == Chat.ChatType.CALLED
                || Chat.ChatType.from(chat.getChatType()) == Chat.ChatType.MISSED_VIDEO_CALL
                || Chat.ChatType.from(chat.getChatType()) == Chat.ChatType.VIDEO_CALLED
                || Chat.ChatType.from(chat.getChatType()) == Chat.ChatType.START_GROUP_CALL
                || Chat.ChatType.from(chat.getChatType()) == Chat.ChatType.END_GROUP_CALL
                || Chat.ChatType.from(chat.getChatType()) == Chat.ChatType.START_GROUP_VIDEO_CALL
                || Chat.ChatType.from(chat.getChatType()) == Chat.ChatType.END_GROUP_VIDEO_CALL;
    }

    public void sendReadStatusMessage(Chat chat, String sendTo) {
        Timber.i("sendReadStatusMessage to: " + sendTo);
        final Chat newChat = Chat.newCloneDateInstance(chat);
        newChat.setId(chat.getId());
        newChat.setSendingType(Chat.SendingType.READ_STATUS);
        newChat.setSender(SharedPrefUtils.getUser(mContext));
        newChat.setSendTo(sendTo);
        newChat.setChannelId(chat.getChannelId());
        newChat.setGroup(chat.isGroup());
        newChat.setSenderId(mMyUserId);
        sendReadMessageStatusInternally(newChat, false);
        ConversationDb.updateLastSeenMessageStatusOfCurrentUser(mContext, chat, mOnChatSocketListener);
    }

    public void resendReadStatusMessage(List<Chat> list) {
        if (list != null) {
//            Timber.i("resendReadStatusMessage: " + list.size());
            for (Chat chat : list) {
                sendReadMessageStatusInternally(chat, true);
            }
        }
    }

    private void sendReadMessageStatusInternally(Chat readStatusChat, boolean isFromCheckToResendReadStatus) {
        //Will save this sending read status message.
        if (!isFromCheckToResendReadStatus) {
            MessageDb.addSendingReadStatusMessageOfCurrentUser(mContext, readStatusChat);
        }
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            try {
                publishMessage(readStatusChat, new BasePublishMessageCallback(readStatusChat) {
                    @Override
                    void onCustomCall(String channelName, Object error, Chat ackChat) {
                        Timber.i("Send read status callback. error: " + error + ", data: " + new Gson().toJson(ackChat));
                        //Will only update message to read for current user after the send read status success.
                        if (error == null) {
                            MessageDb.removeSendingReadStatusMessageOfCurrentUser(mContext, readStatusChat.getId());
                        }
//                        Timber.i("Sending read status message count: " + MessageDb.getSendingReadStatusMessageOfCurrentUserCount(mContext));
                    }
                });
            } catch (Exception e) {
                Timber.e("sendReadStatus failed %s", e.getMessage());
                SentryHelper.logSentryError(e);
            }
        }, 2000);
    }

    public void sendReadStatusWithoutSavingToLocal(Chat chat, String conversationId) {
        Timber.i("sendReadStatusWithoutSavingToLocal: conversationId: " + conversationId);
        String sendTo = isGroupConversation(conversationId) ? conversationId : chat.getSenderId();
        Chat newChat = Chat.newCloneDateInstance(chat);
        newChat.setId(chat.getId());
        newChat.setSendingType(Chat.SendingType.READ_STATUS);
        newChat.setSender(SharedPrefUtils.getUser(mContext));
        newChat.setSendTo(sendTo);
        newChat.setChannelId(chat.getChannelId());
        newChat.setGroup(chat.isGroup());
        newChat.setSenderId(mMyUserId);
        publishMessage(newChat, null);
    }

    private boolean isGroupConversation(Conversation conversation) {
        return conversation != null && !TextUtils.isEmpty(conversation.getGroupId());
    }

    void sendReply(Chat chat, boolean needToDisconnectSocketAfterSend) {
        if (TextUtils.equals(chat.getSendTo(), mMyUserId)) {
            //user cannot send message to himself
            return;
        }
        MessageDb.save(mContext, chat);

        // Save last message when user reply to a message notification so the conversation screen
        // get updated correctly
        Conversation con = ConversationDb.updateLastMessageOfConversation(mContext,
                chat,
                false,
                true);
        if (mOnChatSocketListener != null) {
            mOnChatSocketListener.onMessageReceive(chat, null);
            if (con != null) {
                mOnChatSocketListener.onConversationCreateOrUpdate(con, "sendReply");
            }
        }

        Chat newChat = new Chat(chat);
        newChat.setStatus(MessageState.SENT);
        Timber.e("sendReply %s", getGson().toJson(newChat));
        publishMessage(newChat, new BasePublishMessageCallback(newChat) {
            @Override
            void onCustomCall(String name, Object error, Chat ackChat) {
                Timber.i("callback: " + name + ", error: " + error + ", data:" + ackChat);
                if (error == null) {
                    ackChat.setStatus(MessageState.SENT);
                } else {
                    ackChat.setStatus(MessageState.FAIL);
                }

                MessageDb.save(mContext, ackChat);
                ConversationDb.updateLastMessageOfConversation(mContext,
                        ackChat,
                        false,
                        true);

                if (!needToDisconnectSocketAfterSend) {
                    if (mOnChatSocketListener != null) {
                        mOnChatSocketListener.onMessageReceive(ackChat, null);
                    }

                    checkToSendReadStatusFromCurrentUser(ackChat.getChannelId(), "sendReply");
                } else {
                    disconnect("sendReply");
                }
            }
        });
    }

    boolean sendTo(Chat chat, boolean isNeedToSaveDb) {
        Timber.i("Before injection: " + chat.getStatus());
        injectSomeValueToSendingMessageOfCurrentUser(chat);
        Timber.i("sendTo: isNeedToSaveDb: => " + isNeedToSaveDb + ", Chat: " + new Gson().toJson(chat));
        if (TextUtils.equals(chat.getSendTo(), mMyUserId)) {
            //user cannot send message to himself
            return false;
        }
        Chat newChat = new Chat(chat);
        if (isNeedToSaveDb) {
            MessageDb.save(mContext, chat);
        }

        if (!isconnected()) {
            //even chat is disconnected, but message is saved
            Timber.i("Socket is not connected.");
            if (chat.getType() == Chat.Type.FILE) {
                for (ChatMediaUploadListener chatMediaUploadListener : mChatMediaUploadListeners) {
                    chatMediaUploadListener.onUploadFailed(null, chat, null, null);
                }
                onUploadFileChatFailure(chat, null, newChat, MessageState.FAIL);
            }
        }

        mChatUploadLinkedList.clear();
        if (newChat.getMediaList() != null
                && !newChat.getMediaList().isEmpty()
                && isContainerLocalMedia(newChat.getMediaList())) {
            Timber.i("newChat: " + newChat.getMediaList().get(0));
            mChatUploadLinkedList.add(newChat);
            if (mChatUploadLinkedList.size() <= 1) {
                AsynchronousUpload asynchronousUpload = new AsynchronousUpload(mContext, newChat);
                asynchronousUpload.startUpload(new ChatMediaUploadListener() {
                    @Override
                    public void onUploadSuccess(String requestId,
                                                Chat chat,
                                                String mediaId,
                                                String imageUrl,
                                                String thumb) {
                        for (ChatMediaUploadListener chatMediaUploadListener : mChatMediaUploadListeners) {
                            chatMediaUploadListener.onUploadSuccess(requestId, chat, mediaId, imageUrl, thumb);
                        }
                        //Set new date when the chat is ready to sent to Socket.
                        chat.constructNewSentMessageDate();
                        sendTo(chat, true);
                    }

                    @Override
                    public void onUploadFailed(String requestId,
                                               Chat failedChat,
                                               String mediaId,
                                               Exception ex) {
                        for (ChatMediaUploadListener chatMediaUploadListener : mChatMediaUploadListeners) {
                            chatMediaUploadListener.onUploadFailed(requestId, chat, mediaId, ex);
                        }
                        onUploadFileChatFailure(chat, mediaId, newChat, MessageState.FAIL);
                    }

                    @Override
                    public void onUploadProgressing(String requestId,
                                                    Chat chat,
                                                    String mediaId,
                                                    int percent,
                                                    long percentBytes,
                                                    long totalBytes) {
                        for (ChatMediaUploadListener chatMediaUploadListener : mChatMediaUploadListeners) {
                            chatMediaUploadListener.onUploadProgressing(requestId,
                                    chat,
                                    mediaId,
                                    percent,
                                    percentBytes,
                                    totalBytes);
                        }
                    }

                    @Override
                    public void onCancel(String requestId, Chat chat, String mediaId) {
                        for (ChatMediaUploadListener chatMediaUploadListener : mChatMediaUploadListeners) {
                            chatMediaUploadListener.onCancel(requestId, chat, mediaId);
                        }
                    }
                });
            }
        } else {
            if (newChat.getMediaList() != null &&
                    !newChat.getMediaList().isEmpty() &&
                    !mChatUploadLinkedList.isEmpty()) {
                mChatUploadLinkedList.removeFirst();
            }

            newChat.setStatus(MessageState.SENT);
            newChat.setSendTo(newChat.getSendTo());
            Timber.e(getGson().toJson(newChat));
            publishMessage(newChat, new SendMessageCallback(chat));
            if (newChat.getMediaList() != null &&
                    !newChat.getMediaList().isEmpty() &&
                    !mChatUploadLinkedList.isEmpty()) {
                sendTo(mChatUploadLinkedList.getFirst(), false);
            }
        }
        return true;
    }

    private boolean isContainerLocalMedia(List<Media> mediaList) {
        for (Media productMedia : mediaList) {
            if (!productMedia.getUrl().startsWith("http")) {
                return true;
            }
        }
        return false;
    }

    void startTyping(boolean isGroupChat,
                     String sendTo,
                     @Nullable String productId,
                     String channelId,
                     boolean isTyping) {
        if (TextUtils.isEmpty(sendTo) || TextUtils.equals(sendTo, mMyUserId)) {
            return;
        }
        Chat chat = new Chat();
        chat.setId(UUID.randomUUID().toString());
        chat.setSender(SharedPrefUtils.getCurrentUser(mContext));
        chat.setSenderId(mMyUserId);
        chat.setProductId(productId);
        chat.setChannelId(channelId);
        chat.setTyping(isTyping);
        chat.setSendingType(Chat.SendingType.TYPING);
        chat.setGroup(isGroupChat);
        chat.setSendTo(sendTo);
        try {
            Timber.e("startTyping %s", new Gson().toJson(chat));
            publishMessage(chat, new BasePublishMessageCallback(chat) {
                @Override
                void onCustomCall(String channelName, Object error, Chat ackChat) {
                    Timber.i("Send typing ack: " + new Gson().toJson(ackChat));
                }
            });
        } catch (Exception e) {
            Timber.e("isConnect %s", isconnected());
            SentryHelper.logSentryError(e);
        }
    }

    void checkToSendReadStatusFromCurrentUser(String conversationId, String tag) {
        NotificationUtils.cancelNotificationByLocalId(mContext, conversationId);
        Chat lastMessageOfConversation = MessageDb.getLastMessageOfConversation(mContext, conversationId);
        Timber.i("checkToSendReadStatusFromCurrentUser: " + lastMessageOfConversation + ", from " + tag + ", status: " + (lastMessageOfConversation != null ? lastMessageOfConversation.getStatus() : null));
        if (lastMessageOfConversation != null) {
            String lastSeenMessageIdOfCurrentUser = ConversationDb.getLastSeenMessageIdOfCurrentUser(mContext, conversationId);
            if (!TextUtils.equals(lastMessageOfConversation.getId(), lastSeenMessageIdOfCurrentUser)) {
                if (TextUtils.equals(lastMessageOfConversation.getSenderId(), mMyUserId)) {
                    Timber.i("Ignore sending read message status of own sender account.");
                    //Just need to update last seen message of current user locally
                    ConversationDb.updateLastSeenMessageStatusOfCurrentUser(mContext, lastMessageOfConversation, mOnChatSocketListener);
                    return;
                }

                Timber.i("Will send read status of message id: " + lastMessageOfConversation.getId() + " from current user.");
                if (isconnected()) {
                    Timber.i("Will send read status immediately.");
                    sendReadStatusMessage(lastMessageOfConversation,
                            isGroupConversation(conversationId) ? conversationId : lastMessageOfConversation.getSenderId());
                } else {
                    MessageDb.addSendingReadStatusMessageOfCurrentUser(mContext, lastMessageOfConversation);
                    //Even we can't send read status at the moment, we will still update the last seen message of user to make
                    //sure unread message status will update properly in chat screen
                    ConversationDb.updateLastSeenMessageStatusOfCurrentUser(mContext, lastMessageOfConversation, mOnChatSocketListener);
                }

                //Update all previous message to seen
                List<Chat> list = MessageDb.updatePreviousMessageStatusToSeen(mContext, Objects.requireNonNull(lastMessageOfConversation), conversationId);
                for (Chat chat1 : list) {
                     /*
                        Will ignore update seen avatar view in list and just remove message status icon and will
                        let the last message handle the seen avatar.
                             */
                    if (!TextUtils.equals(chat1.getId(), lastMessageOfConversation.getId())) {
                        chat1.setIgnoreSeenStatus(true);
                    }
                    mOnChatSocketListener.onMessageReceive(chat1, " Will ignore update seen avatar view in list and just remove message status icon and will\n" +
                            "                        let the last message handle the seen avatar.");
                }

                //Dispatch update conversation to view
                Conversation conversation = ConversationDb.getConversationById(mContext, conversationId);
                if (conversation != null) {
                    ConversationDb.save(mContext,
                            conversation,
                            "checkToSendReadStatusFromCurrentUser");
                    mOnChatSocketListener.onConversationCreateOrUpdate(conversation, "checkToSendReadStatusFromCurrentUser");
                }
            } else if (lastMessageOfConversation.getStatus() == MessageState.SEEN) {
                Timber.i("Just need to double check if previous message of last seen message have seen status.");
                List<Chat> list = MessageDb.updatePreviousMessageStatusToSeen(mContext, Objects.requireNonNull(lastMessageOfConversation), conversationId);
                for (Chat chat1 : list) {
                     /*
                        Will ignore update seen avatar view in list and just remove message status icon and will
                        let the last message handle the seen avatar.
                             */
                    if (!TextUtils.equals(chat1.getId(), lastMessageOfConversation.getId())) {
                        chat1.setIgnoreSeenStatus(true);
                    }
                    mOnChatSocketListener.onMessageReceive(chat1, "Just need to double check if previous message of last seen message have seen status.");
                }
            }
        }
    }

    void archivedConversation(String conversationId) {
        //archived conversation in local
        ConversationDb.archivedConversation(mContext, conversationId);

        //fire listener delete conversation
        if (mOnChatSocketListener != null) {
            mOnChatSocketListener.onConversationDelete(conversationId);
        }
    }

    void deleteChat(Chat chat) {
        Timber.i("deleteChat: getStatus: " + new Gson().toJson(chat));
        Chat newChat = new Chat();
        newChat.setId(chat.getId());
        newChat.setSendingType(Chat.SendingType.DELETE);
        newChat.setSendTo(chat.getSendTo());
        newChat.setChannelId(chat.getChannelId());
        newChat.setSenderId(mMyUserId);
        newChat.setSender(UserHelper.getNecessaryUserInfo(mContext));
        newChat.setGroup(chat.isGroup());
        /*
        For failed message type, we will delete only local chat item and no need to send delete over
        Socket.
         */
        if (chat.getStatus() != MessageState.FAIL) {
            try {
                publishMessage(newChat, null);
            } catch (Exception e) {
                Timber.e("deleteChat fail %s", e.toString());
                SentryHelper.logSentryError(e);
            }
        }

        //update local and fire listener of delete
        MessageDb.delete(mContext, chat, mOnMessageDbListener);
    }

    private Gson getGson() {
        if (mGson == null) {
            mGson = GsonHelper.getGsonForSocketCommunication();
        }
        return mGson;
    }

    public interface PublishMessageCallback {
        void onPublishedFinished(String channelName, Object error, Chat ackChat);
    }

    private class LoadNewMessageCallback implements OnCallbackListener<Response<List<Chat>>> {
        private Chat mChat;

        private LoadNewMessageCallback(Chat chat) {
            this.mChat = chat;
        }

        @Override
        public void onFail(ErrorThrowable ex) {
            Timber.e("onFail");
            checkToSendReadStatusFromCurrentUser(mChat.getChannelId(), "LoadNewMessageCallback Failure");
        }

        @Override
        public void onComplete(Response<List<Chat>> result) {
            List<Chat> chatList = result.body();
            if (chatList != null && !chatList.isEmpty()) {
                MessageDb.save(mContext, chatList);
                for (Chat chat : chatList) {
                    mOnChatSocketListener.onMessageReceive(chat, "LoadNewMessageCallback");
                }
            }
            Timber.i("LoadNewMessageCallback ");
            checkToSendReadStatusFromCurrentUser(mChat.getChannelId(), "LoadNewMessageCallback Success");
        }
    }

    public SendMessageCallback getNewPublishMessageCallbackInstance(Chat newChat, PublishMessageCallback callback) {
        return new SendMessageCallback(newChat, callback);
    }

    public class BasePublishMessageCallback implements Ack {

        protected Chat mMessage;
        private PublishMessageCallback mPublishMessageCallback;

        public BasePublishMessageCallback(Chat message) {
            mMessage = new Chat(message);
        }

        public BasePublishMessageCallback(Chat message, PublishMessageCallback callback) {
            mMessage = new Chat(message);
            mPublishMessageCallback = callback;
        }

        @Override
        public final void call(String name, Object error, Object data) {
            Timber.i("BasePublishMessageCallback: Channel Name: " + name +
                    ", error: " + error +
                    ", data: " + new Gson().toJson(data));
            Chat ackChat;
            if (data != null) {
                ackChat = getGson().fromJson(data.toString(), Chat.class);
            } else {
                ackChat = mMessage;
            }

            onCustomCall(name, error, ackChat);
            if (mPublishMessageCallback != null) {
                mPublishMessageCallback.onPublishedFinished(name, error, ackChat);
            }
        }

        //TODO: Client must implement this method
        void onCustomCall(String channelName, Object error, Chat ackChat) {
        }
    }

    public class SendMessageCallback extends BasePublishMessageCallback {

        public SendMessageCallback(Chat newChat) {
            super(newChat);
        }

        public SendMessageCallback(Chat newChat, PublishMessageCallback callback) {
            super(newChat, callback);
        }

        @Override
        void onCustomCall(String channelName, Object error, Chat ackChat) {
            Timber.i("SendMessageCallback: Channel Name: " + channelName +
                    ", error: " + error +
                    ", data: " + new Gson().toJson(ackChat));
            if (CallHelper.isVOIPCallMessage(ackChat)) {
                Timber.i("Ignore the acknowledgment of VOIP call message.");
                if (TextUtils.equals(mMyUserId, ackChat.getSenderId()) &&
                        CallHelper.isInComingVOIPCallMessageType(ackChat) &&
                        error != null) {
                    Timber.i("There is an error when attempt to make call.");
                    if (mOnChatSocketListener != null) {
                        mOnChatSocketListener.onCallError(new Gson().fromJson(error.toString(), SocketError.class));
                    }
                }
                onPublishedFinished(channelName, error, ackChat);
                return;
            }
            Timber.i("ackChat: " + ackChat.getContent());
            ackChat.setContent(mMessage.getContent());
            if (error == null) {
                ackChat.setStatus(MessageState.SENT);
                Timber.i("Message has been sent: " + ackChat.getId() + ", date: " + ChatHelper.getChatDateUTC(ackChat.getDate()));
                if (mOnChatSocketListener != null) {
                    mOnChatSocketListener.onMessageSent(ackChat);
                }
            } else {
                ackChat.setStatus(MessageState.FAIL);
            }

            if (ackChat.isGroup()) {
                //Maintain the previous seen status
                Chat chat = MessageDb.queryById(mContext, ackChat.getId());
                if (chat != null && chat.getStatus() == MessageState.SEEN) {
                    ackChat.setStatus(MessageState.SEEN);
                }
            }

            if (ackChat.getStatus() == MessageState.SENT) {
                //We will not update local chat if the chat is already exist with sent status to maintain
                //the chat date.
                if (!MessageDb.isLocalMessageStatusIsSent(mContext, ackChat.getId())) {
                    MessageDb.save(mContext, ackChat);
                }
            } else {
                MessageDb.save(mContext, ackChat);
            }

            Conversation con = ConversationDb.updateLastMessageOfConversation(mContext,
                    ackChat,
                    true,
                    true);

            if (mOnChatSocketListener != null) {
                mOnChatSocketListener.onMessageReceive(ackChat, null);
                if (con != null) {
                    mOnChatSocketListener.onConversationCreateOrUpdate(con, "onCustomCall");
                }
            }
            onPublishedFinished(channelName, error, ackChat);
        }

        public void onPublishedFinished(String channelName, Object error, Chat ackChat) {
            //Client Impl...
        }
    }

    private void onUploadFileChatFailure(Chat refChat,
                                         String mediaId,
                                         Chat sendingChat,
                                         MessageState messageState) {
        /*
          Since the upload process of media failed in the chat, so will retrieve the media from ref chat
          and update its status to retry upload.
         */
        if (refChat.getMediaList() != null) {
            for (Media media : refChat.getMediaList()) {
                /*
                  If there is no checking media id provided, then will update all media items inside chat.
                 */
                if (TextUtils.isEmpty(mediaId)) {
                    media.setFileStatusType(Media.FileStatusType.RETRY_UPLOAD);
                } else if (media.getId().matches(mediaId)) {
                    media.setFileStatusType(Media.FileStatusType.RETRY_UPLOAD);
                    break;
                }
            }
        }
        sendingChat.setStatus(messageState);
        MessageDb.save(mContext, sendingChat);
        if (mOnChatSocketListener != null) {
            mOnChatSocketListener.onMessageReceive(sendingChat, null);
        }
    }

    public static void logMessages(String tag,
                                   List<Chat> chats) {
        Timber.i("Start log messages ...........****........ " + tag);
        for (Chat chat : chats) {
            Timber.i("id: " + chat.getId() +
                    ", content: " + chat.getContent() +
                    ", status: " + chat.getStatus() +
                    ", sent type: " + chat.getSendingType() +
                    ", sender id: " + chat.getSenderId() +
                    ", sendTo id: " + chat.getSendTo());
        }
        Timber.i("End log messages ...........****........" + tag);
    }
}
