package com.proapp.sompom.chat.call;

import android.os.CountDownTimer;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 3/19/2020.
 * This class is being used to check the call time-out of both caller and recipient.
 */
public class CallTimerHelper {

    public static final long CALL_TIME_OUT = 30000;  //    30 Seconds
    public static final long VALID_INCOMING_CALL_TIME = 90000;  //    90 Seconds
    //    private static final long REQUEST_CAMERA_TIME_OUT = 10000;  //    10 Seconds
    private static final long REQUEST_CAMERA_TIME_OUT = 15000;  //    10 Seconds
    public static final long TIME_INTERVAL = 1000;  //  1 second.

    private CountDownTimer mCallingObserveTimer;
    private CallTimerHelperListener mLister;
    private boolean mIsGroupCall;
    private String mRecipientId;
    private String mGroupId;
    private boolean mIsTimerTicking;
    private TimerType mTimerType;
    private StarterTimerType mStarterTimerType;

    public CallTimerHelper(CallTimerHelperListener lister) {
        mLister = lister;
    }

    private void initiateTimer(TimerType timerType) {
        long duration = CALL_TIME_OUT;
        if (timerType == TimerType.REQUEST_CAMERA) {
            duration = REQUEST_CAMERA_TIME_OUT;
        }
        mCallingObserveTimer = new CountDownTimer(duration, TIME_INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                Timber.i("onTick: " + millisUntilFinished +
                        " , mTimerType: " + timerType +
                        " , starter: " + mStarterTimerType);
                mIsTimerTicking = true;
            }

            @Override
            public void onFinish() {
                Timber.i("onFinish: " + mStarterTimerType);
                mIsTimerTicking = false;
                if (mLister != null) {
                    mLister.onCallTimerUp(mTimerType,
                            mStarterTimerType,
                            mIsGroupCall,
                            mRecipientId,
                            mGroupId);
                }
            }
        };
    }

    public TimerType getTimerType() {
        return mTimerType;
    }

    public StarterTimerType getStarterTimerType() {
        return mStarterTimerType;
    }

    public boolean isTimerTicking() {
        return mIsTimerTicking;
    }

    public void startOneToOneTimer(TimerType timerType,
                                   StarterTimerType starterTimerType,
                                   String recipientId) {
        if (mCallingObserveTimer != null) {
            mCallingObserveTimer.cancel();
        }
        initiateTimer(timerType);
        mTimerType = timerType;
        mStarterTimerType = starterTimerType;
        mRecipientId = recipientId;
        mIsGroupCall = false;
        mCallingObserveTimer.cancel();
        mCallingObserveTimer.start();
    }

    public void startGroupTimer(TimerType timerType,
                                StarterTimerType starterTimerType,
                                String groupId) {
        initiateTimer(timerType);
        mTimerType = timerType;
        mStarterTimerType = starterTimerType;
        mRecipientId = null;
        mIsGroupCall = true;
        mGroupId = groupId;
        mCallingObserveTimer.cancel();
        mCallingObserveTimer.start();
    }

    public void cancel() {
        if (mCallingObserveTimer != null) {
            mCallingObserveTimer.cancel();
        }
        resetStatus();
    }

    private void resetStatus() {
        mGroupId = null;
        mRecipientId = null;
        mIsGroupCall = false;
        mIsTimerTicking = false;
        mStarterTimerType = StarterTimerType.UNDEFINED;
        mTimerType = TimerType.UNDEFINED;
    }

    public interface CallTimerHelperListener {

        void onCallTimerUp(TimerType timerType,
                           StarterTimerType starterTimerType,
                           boolean isGroup,
                           String recipientId,
                           String groupId);
    }

    public enum TimerType {
        CALL,
        REQUEST_CAMERA,
        UNDEFINED
    }

    public enum StarterTimerType {
        CALLER, //Requester
        RECIPIENT,
        UNDEFINED
    }
}
