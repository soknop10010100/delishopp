package com.proapp.sompom.chat.service;

import android.os.CountDownTimer;

public class SocketConnectionChecker {

    private static final long INTERVAL = 7000L;

    private CountDownTimer mCountDownTimer;
    private final SocketConnectionCheckerListener mListener;

    public SocketConnectionChecker(SocketConnectionCheckerListener listener) {
        mListener = listener;
    }

    public void startTimer() {
        stopTimer();
        mCountDownTimer = new CountDownTimer(Long.MAX_VALUE, INTERVAL) {
            @Override
            public void onTick(long l) {
                if (mListener != null) {
                    mListener.onShouldCheckConnection();
                }
            }

            @Override
            public void onFinish() {
                startTimer();
            }
        };
        mCountDownTimer.start();
    }

    public void stopTimer() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
    }

    public interface SocketConnectionCheckerListener {
        void onShouldCheckConnection();
    }
}
