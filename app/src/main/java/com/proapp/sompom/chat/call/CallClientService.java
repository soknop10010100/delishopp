package com.proapp.sompom.chat.call;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.SurfaceView;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.AbsChatBinder;
import com.proapp.sompom.chat.call.agora.AgoraCallViewBuilder;
import com.proapp.sompom.chat.service.ChatSocket;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.call.CallData;
import com.proapp.sompom.model.request.JoinChannelStatusUpdateBody;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.ConversationUtil;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.models.UserInfo;
import io.agora.rtc.video.VideoCanvas;
import io.agora.rtc.video.VideoEncoderConfiguration;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 2/24/2020.
 */
public class CallClientService extends AbsCallClient<Object> {

    private boolean mIsMissedCall;
    private CallingService.CallServiceBinder mBinder;

    //Agora Client
    private RtcEngine mRtcEngine;
    private IRtcEngineEventHandler mRtcEventHandler;

    private CallTimerHelper mCallTimerHelper;
    private AbsCallService.CallType mCallType;

    private CallData myCallData;
    private CallDataHolder<Integer> mCallDataHolder = new CallDataHolder<>();
    private AgoraCallViewBuilder mAgoraCallViewBuilder;
    private CallMessageHelper mCallMessageHelper;
    private boolean mIsCallInProgress;
    private boolean mIsCurrentUserJoinedChannel;
    private boolean mIsDisableAllRemoteVideo;
    private String mGotUserBusyResponseFromOneToOneUser;
    private boolean mIsMuteForOneToOneCall;
    private boolean mIsRequestingJoinChannel;
    private boolean mIsRequestingLeaveChannel;

    public CallClientService(Context context,
                             CallingService.CallServiceBinder binder,
                             ClientInitializeCallback initializeCallback,
                             GeneralCallBehavior generalCallBehavior) {
        super(context, initializeCallback, generalCallBehavior);
        mBinder = binder;
        mGeneralCallBehavior = generalCallBehavior;
        mCallMessageHelper = new CallMessageHelper(context, mBinder.getChatBinder(), "VoiceCallClientService");
        initCallTimer();
    }

    public CallMessageHelper getCallMessageHelper() {
        return mCallMessageHelper;
    }

    public CallDataHolder<Integer> getCallDataHolder() {
        return mCallDataHolder;
    }

    public void loadCurrentUserId() {
        mCurrentUserId = SharedPrefUtils.getUserId(mContext);
        Timber.i("mCurrentUserId: " + mCurrentUserId);
        mCallDataHolder.setCurrentUserId(mCurrentUserId);
    }

    public void setGotUserBusyResponseFromOneToOneUser(String gotUserBusyResponseFromOneToOneUser) {
        mGotUserBusyResponseFromOneToOneUser = gotUserBusyResponseFromOneToOneUser;
    }

    public String getGotUserBusyResponseFromOneToOneUser() {
        return mGotUserBusyResponseFromOneToOneUser;
    }

    private void initCallTimer() {
        mCallTimerHelper = new CallTimerHelper((timerType, starterTimerType, isGroup, recipientId, groupId) -> {
            Timber.i("onCallTimerUp: " + timerType + ", starterTimerType: " + starterTimerType);
            if (timerType == CallTimerHelper.TimerType.REQUEST_CAMERA) {
                broadcastCallTimeOut(getCallType(), timerType, starterTimerType);
            } else if (timerType == CallTimerHelper.TimerType.CALL) {
                checkToSendVOIPCallRejectMessageIfNecessary();
                broadcastOnCallEnd(getCallType(),
                        getChannelId(getCurrentCallData(),
                                mCurrentUserId,
                                get1x1CallParticipantId()),
                        mBinder.getRecipient(),
                        mIsMissedCall,
                        0,
                        false,
                        true,
                        false);
            }
        });
    }

    public boolean isCurrentUserJoinedChannel() {
        return mIsCurrentUserJoinedChannel;
    }

    public boolean isCallInProgress() {
        return mIsCallInProgress;
    }

    public boolean isMissedCall() {
        return mIsMissedCall;
    }

    @Override
    public AbsCallService.CallType getCallType() {
        return mCallType;
    }

    @Override
    public void startService(String userId) {
        initAgoraClient();
    }

    @Override
    public void toggleVideoCamera(boolean isOn) {
        Timber.i("toggleVideoCamera: " + isOn + ", Result: " + mRtcEngine.muteLocalVideoStream(!isOn));
        cancelTimer();
        broadcastVideoCameraStateChanged(SharedPrefUtils.getUserId(mContext), !isOn, true);
        checkToBroadcastRequestCameraOfCurrentUser(isOn);
        if (isOn && isGroupCall() && !mCallViewCommunicationListener.isCurrentCallViewHasVideo()) {
            //Add current call video since it was not added previously.
            broadcastBindCallView("toggleVideoCamera",
                    getCallType(),
                    true,
                    Collections.singletonList(buildMyCallData(true,
                            CallData.CallAction.ENGAGED_IN_CALL)));
        }
    }

    private void setReceivingAllRemoteVideoStatesChanged(boolean isEnabled) {
        mIsDisableAllRemoteVideo = !isEnabled;
        mRtcEngine.muteAllRemoteVideoStreams(!isEnabled);
    }

    private void setEnableReceivingRemoteVideoStatesChanged(int uid, boolean isEnabled) {
        mRtcEngine.muteRemoteVideoStream(uid, !isEnabled);
    }

    public void muteLocalVideo() {
        mRtcEngine.muteLocalVideoStream(true);
    }

    public void enableLocalVideo() {
        mRtcEngine.muteLocalVideoStream(false);
    }

    public void checkToBroadcastRequestCameraOfCurrentUser(boolean isRequestedCamera) {
        if (!isVideoCall(getCallType())) {
            getHandler().post(() -> {
                CallData.CallAction callAction = CallData.CallAction.ENGAGED_IN_CALL;
                if (mCallViewCommunicationListener.getCurrentUserCallAction() == CallData.CallAction.WAITING_OTHER_TO_JOIN) {
                    //Maintain if user in waiting other to join call state.
                    callAction = CallData.CallAction.WAITING_OTHER_TO_JOIN;
                }
                broadcastRequestVideoFromCurrentUser(buildMyCallData(isRequestedCamera,
                        callAction),
                        isRequestedCamera);
            });
        }
    }

    private Handler getHandler() {
        return new Handler(Looper.getMainLooper());
    }

    private void initAgoraClient() {
        if (mRtcEngine == null) {
            mRtcEventHandler = new IRtcEngineEventHandler() {
                @Override
                public void onLocalUserRegistered(int uid, String userAccount) {
                    Timber.i("onLocalUserRegistered: " + uid + ", userAccount: " + userAccount);
                    User user = mCallDataHolder.getParticipantFromCall(userAccount);
                    if (user != null) {
                        mCallDataHolder.addCallInfo(user, uid);
                    }
                }

                @Override
                public void onUserInfoUpdated(int uid, UserInfo userInfo) {
                    Timber.i("onUserInfoUpdated");
                    User user = mCallDataHolder.getParticipantFromCall(userInfo.userAccount);
//                    Timber.i("onUserInfoUpdated: uid: " + user +
//                            ", userInfo: " + new Gson().toJson(userInfo) +
//                            ", Participants: " + new Gson().toJson(mCallDataHolder.getConversation().getParticipants()));
                    if (user != null) {
                        mCallDataHolder.addCallInfo(user, uid);
                    }
                    if (isOneToOneCallRecipientNotYetAcceptedCall()) {
                        Timber.i("isOneToOneCallRecipientNotYetAcceptedCall");
                        return;
                    }
                    if (user != null) {
                        onOtherParticipantJoinedCall(user, uid);
                    }
                }

                @Override
                public void onUserJoined(int uid, int reason) {
                    super.onUserJoined(uid, reason);
                    Timber.i("onUserJoined: " + uid);
                    if (isOneToOneCallRecipientNotYetAcceptedCall()) {
                        Timber.i("isOneToOneCallRecipientNotYetAcceptedCall");
                        return;
                    }

                    /*
                    Temporary disable the receiving video state changed from this remote user. And will
                    be enable back after the remote data has been save successfully in "onUserInfoUpdated"
                    callback.
                    Note: Disable only if the remote user data is not yet saved before.
                     */
                    if (mCallDataHolder.getUserFromUId(uid) == null) {
                        setEnableReceivingRemoteVideoStatesChanged(uid, false);
                    }
                }

                @Override
                public void onUserOffline(int uid, int reason) {
                    Timber.i("onUserOffline");
                    //All user that engaged in call process should be added in call data holder. Otherwise
                    //those users are not valid in that call process.
                    if (mCallDataHolder.getUserFromUId(uid) != null) {
                        Timber.i("onUserOffline: uid: " + uid);
                        if (isGroupCall()) {
                            User userFromUId = mCallDataHolder.getUserFromUId(uid);
                            if (userFromUId != null) {
                            /*
                            Reset audio state to true since the data of left user will not be cleared
                            to serve join group call process.
                             */
                                mCallDataHolder.removeJoinedUserId(userFromUId.getId());
                                mCallDataHolder.addAudioState(userFromUId.getId(), true);
                                broadcastRemoveCallView(userFromUId.getId());
                                Timber.i("getJoinCallParticipantCount: " + mCallDataHolder.getJoinCallParticipantCount());
                                broadcastGroupMemberLeftGroupCall(userFromUId);
                            }
                        } else if (!isMissedCall()) {
                            //Will dispatch call ended callback only if the call was previously engaged.
                            broadcastOnCallEnd(getCallType(),
                                    getChannelId(getCurrentCallData(), mCurrentUserId, get1x1CallParticipantId()),
                                    mBinder.getRecipient(),
                                    false,
                                    0,
                                    false,
                                    false,
                                    true);
                        }
                    }
                }

                @Override
                public void onRejoinChannelSuccess(String channel, int i, int i1) {
                    Timber.i("Current user onRejoinChannelSuccess: channel " + channel);
                    mIsRequestingJoinChannel = false;
                    mIsRequestingLeaveChannel = false;
                    mIsCurrentUserJoinedChannel = true;
                    broadcastOnCurrentUserJoinOrLeaveGroupCall(getChannelId(getCurrentCallData(),
                            mCurrentUserId,
                            get1x1CallParticipantId()),
                            false,
                            0);
                }

                @Override
                public void onJoinChannelSuccess(String channel, int i, int i1) {
                    //Current user join channel success
                    Timber.i("Current user onJoinChannelSuccess: channel " + channel + ", mIsRequestingLeaveChannel: " + mIsRequestingLeaveChannel);
                    if (mIsRequestingLeaveChannel) {
                        //User has request leaving channel pending or error. So we have to request leave channel again.
                        mIsRequestingLeaveChannel = false;
                        leaveChannel();
                    } else {
                        mIsRequestingJoinChannel = false;
                        mIsRequestingLeaveChannel = false;
                        if (!isGroupCall()) {
                            muteLocalAudioStream(true);
                            mIsMuteForOneToOneCall = true;
                        }
                        mIsCurrentUserJoinedChannel = true;
                        broadcastOnCurrentUserJoinOrLeaveGroupCall(getChannelId(getCurrentCallData(),
                                mCurrentUserId,
                                get1x1CallParticipantId()),
                                false,
                                0);
                        if (isVideoCall(getCallType())) {
                            enableVideoCallViewForCurrentUserIfNecessary();
                        }
                    }
                }

                @Override
                public void onLeaveChannel(RtcStats rtcStats) {
                    Timber.i("Current user onLeaveChannel: " + rtcStats.users);
                    setProgressCallState("Leave Channel", false);
                    mIsRequestingJoinChannel = false;
                    mIsRequestingLeaveChannel = false;
                    mIsCurrentUserJoinedChannel = false;
                    broadcastOnCurrentUserJoinOrLeaveGroupCall(getChannelId(getCurrentCallData(),
                            mCurrentUserId,
                            get1x1CallParticipantId()),
                            true,
                            rtcStats.totalDuration);
                }

                @Override
                public void onError(int i) {
                    Timber.e("Algora onError: " + i);
                    super.onError(i);
                }

                @Override
                public void onRemoteVideoStats(RemoteVideoStats stats) {
                    Timber.i("onRemoteVideoStats: receivedBitrate: " + stats.receivedBitrate +
                            ", width: " + stats.width +
                            ", height: " + stats.height);
                    if (stats.receivedBitrate == 0 && stats.width == 0 && stats.height == 0) {
                        /*
                            Remote video was off or frozen. In this case, w have to treat this action
                            like remote video offs.
                         */
                        User userFromUId = mCallDataHolder.getUserFromUId(stats.uid);
                        if (userFromUId != null && mCallDataHolder.shouldBroadCastVideoFrozen(userFromUId.getId())) {
                            Timber.i("Force broadcast remote camera off when received remote video off or frozen.");
                            broadcastVideoCameraStateChanged(userFromUId.getId(), true, false);
                        }
                    } else if (stats.receivedBitrate > 0 && stats.width > 0 && stats.height > 0) {
                        //Check the resume frozen video back state
                        User userFromUId = mCallDataHolder.getUserFromUId(stats.uid);
                        if (userFromUId != null && mCallDataHolder.shouldBroadCastResumeVideoBackFromFrozen(userFromUId.getId())) {
                            Timber.i("Force broadcast remote camera on.");
                            broadcastVideoCameraStateChanged(userFromUId.getId(), false, false);
                        }
                    }
                }

                @Override
                public void onRemoteAudioStateChanged(int uid, int state, int reason, int elapsed) {
                    Timber.i("onRemoteAudioStateChanged: uid: " + uid +
                            ",state: " + state +
                            ", reason: " + reason +
                            ", elapsed: " + elapsed);
                    if (isOneToOneCallRecipientNotYetAcceptedCall()) {
                        Timber.i("isOneToOneCallRecipientNotYetAcceptedCall");
                        return;
                    }

                    //Enable local audio back
                    if (mIsMuteForOneToOneCall) {
                        mIsMuteForOneToOneCall = false;
                        //Must take the latest mic enable value from call screen
                        muteLocalAudioStream(mCallViewCommunicationListener.isMicOff());
                    }

                    if (state == Constants.REMOTE_AUDIO_STATE_STARTING) {
                        if (!isVideoCall(getCallType()) &&
                                (!mCallViewCommunicationListener.isShowingRequestCameraScreen() &&
                                        !mCallDataHolder.isUserHasVideoEnable(uid))) {
                            User user = mCallDataHolder.getUserFromUId(uid);
                            if (user != null) {
                                //Broadcast call engaged.
                                setProgressCallState("Add voice call view on remote user audio get started", true);
                                mIsMissedCall = false;
                                cancelTimer();
                                broadcastJoinGroupCallAvailable("Add voice call view on remote user audio get started.");
                                broadcastCallEstablished(getCallType(),
                                        getChannelId(getCurrentCallData(),
                                                mCurrentUserId, get1x1CallParticipantId()));
                                if (!mCallViewCommunicationListener.isParticipantCallViewAdded(user.getId())) {
                                    Timber.i("Add voice call view on remote user audio get started.");
                                    broadcastBindCallView("Add voice call view on remote user audio get started.",
                                            getCallType(),
                                            isGroupCall(),
                                            Collections.singletonList(mAgoraCallViewBuilder.buildJoinAudioCallData(user)));
                                }
                            }
                        }
                    } else if (state == Constants.REMOTE_AUDIO_STATE_STOPPED && reason == Constants.REMOTE_AUDIO_REASON_REMOTE_MUTED) {
                        //Mute audio
                        User userFromUId = mCallDataHolder.getUserFromUId(uid);
                        if (userFromUId != null) {
                            mCallDataHolder.addAudioState(userFromUId.getId(), false);
                            broadcastOnAudioStateChanged(userFromUId.getId(), true);
                        }
                    } else if (state == Constants.REMOTE_AUDIO_STATE_DECODING && reason == Constants.REMOTE_AUDIO_REASON_REMOTE_UNMUTED) {
                        //Turn on back on audio
                        User userFromUId = mCallDataHolder.getUserFromUId(uid);
                        if (userFromUId != null) {
                            mCallDataHolder.addAudioState(userFromUId.getId(), true);
                            broadcastOnAudioStateChanged(userFromUId.getId(), false);
                        }
                    }
                }

                @Override
                public void onRemoteVideoStateChanged(int uid, int state, int reason, int elapsed) {
                    Timber.i("onRemoteVideoStateChanged: uid: " + uid + ", state: " + state + ", reason: " + reason + ", elapsed: " + elapsed);
                    if (isOneToOneCallRecipientNotYetAcceptedCall()) {
                        Timber.i("isOneToOneCallRecipientNotYetAcceptedCall");
                        return;
                    }
                    getHandler().postDelayed(() -> {
                        User user = mCallDataHolder.getUserFromUId(uid);
                        Timber.i("onRemoteVideoStateChanged: uid: " + uid +
                                ",name: " + (user != null ? user.getFullName() : "Unknown") +
                                ",state: " + state +
                                ", reason: " + reason +
                                ", elapsed: " + elapsed);

                        if (state == Constants.REMOTE_VIDEO_STATE_STARTING) {
                            Timber.i("Video in starting mode");
                            if (isVideoCall(getCallType()) || mCallViewCommunicationListener.shouldAddOtherParticipantVideo()) {
                                getHandler().post(() -> {
                                    addParticipantVideoCallView(uid);
                                });
                            }
                        }
                        //To check if remote user has changed camera state
                        else if (state == Constants.REMOTE_VIDEO_STATE_STOPPED &&
                                reason == Constants.REMOTE_VIDEO_STATE_REASON_REMOTE_MUTED) {
                            Timber.i("Remote video is in turn off mode.");
                            //Update the view on video off status
                            User userFromUId = mCallDataHolder.getUserFromUId(uid);
                            if (userFromUId != null) {
                                broadcastVideoCameraStateChanged(userFromUId.getId(),
                                        true,
                                        false);
                            }
                        } else if (state == Constants.REMOTE_VIDEO_STATE_DECODING &&
                                reason == Constants.REMOTE_VIDEO_STATE_REASON_REMOTE_UNMUTED) {
                            Timber.i("Remote video is in turn on mode.");
                            User userFromUId = mCallDataHolder.getUserFromUId(uid);
                            if (userFromUId != null) {
                                broadcastVideoCameraStateChanged(userFromUId.getId(),
                                        false,
                                        false);
                            }
                        }
                    }, 100);
                }
            };

            try {
                mRtcEngine = RtcEngine.create(mContext,
                        mContext.getString(R.string.agora_app_id),
                        mRtcEventHandler);
                mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_COMMUNICATION);
            } catch (Exception e) {
                Timber.e(e);
                SentryHelper.logSentryError(e);
            }
        }
        if (!isCallInProgress()) {
            mAgoraCallViewBuilder = new AgoraCallViewBuilder();
        }
        onClientConnected();
    }

    private void onOtherParticipantJoinedCall(User user, int uid) {
        getHandler().postDelayed(() -> {
            if (!mIsCurrentUserJoinedChannel) {
                Timber.i("Current user is not yet joined call channel. So will ignore onOtherParticipantJoinedCall process.");
                return;
            }

            mCallDataHolder.addCallInfo(user, uid);
            if (user != null) {
                broadcastGroupMemberJoinedCall(user);
            }
                            /*
                            Check to enable back the receiving of remote video state after the info of that remote
                            user has been saved temporary and is ready to create the call view.
                            Note: This process is critical for the remote user data has to be saved before
                            creating the view and the view creation will be done in remote video state changed
                            callback which will be only enable after the user data has been saved as following.
                             */
            getHandler().postDelayed(() -> {
                if (mIsDisableAllRemoteVideo) {
                    setReceivingAllRemoteVideoStatesChanged(true);
                }
                setEnableReceivingRemoteVideoStatesChanged(uid, true);
            }, 10);

            //Broadcast call engaged.
            setProgressCallState("onUserJoinedCall: " + (user != null ? user.getFullName() : null), true);
            mIsMissedCall = false;
            cancelTimer();

            broadcastJoinGroupCallAvailable("onUserJoinedCall");
            broadcastCallEstablished(getCallType(),
                    getChannelId(getCurrentCallData(),
                            mCurrentUserId, get1x1CallParticipantId()));

            if (user != null) {
                                /*
                                Will add the call view only for voice call. For the video call, we have to
                                add call view in onRemoteVideoStateChanged() callback.
                                 */
                if (!isVideoCall(getCallType()) && (!mCallViewCommunicationListener.isShowingRequestCameraScreen() ||
                        !mCallDataHolder.isUserHasVideoEnable(uid))) {
                    broadcastBindCallView("onUserJoinedCall",
                            getCallType(),
                            isGroupCall(),
                            Collections.singletonList(mAgoraCallViewBuilder.buildJoinAudioCallData(user)));
                } else {
                    addParticipantVideoCallView(uid);
                }
            }
        }, 1000);
    }

    private void setProgressCallState(String tag, boolean isCallInProgress) {
        Timber.e("Tag: " + tag + ", isCallInProgress: " + isCallInProgress);
        mIsCallInProgress = isCallInProgress;
    }

    private boolean isOneToOneCallRecipientNotYetAcceptedCall() {
        return !isGroupCall() &&
                isCurrentUserCaller() &&
                !mCallViewCommunicationListener.isOneToOneRecipientAcceptedCall();
    }

    public void checkToEngageOneToOneCallAfterRecipientAcceptedCall(String userId, boolean isMute) {
        Integer uid = mCallDataHolder.getRemoteCallViewIdOfUser(userId);
        if (uid != null) {
            User userFromUId = mCallDataHolder.getUserFromUId(uid);
            if (userFromUId != null) {
                onOtherParticipantJoinedCall(userFromUId, uid);
                muteLocalAudioStream(isMute);
            }
        }
    }

    public void onOtherUserAcceptedShareVideo(boolean isCurrentUserRequester, String acceptedId) {
        Integer remoteCallViewIdOfUser = mCallDataHolder.getRemoteCallViewIdOfUser(acceptedId);
        if (remoteCallViewIdOfUser != null) {
            if (remoteCallViewIdOfUser != -1) {
                updateToVideoCallTye();
                if (isCurrentUserRequester) {
                    cancelTimer();
                    getHandler().post(() -> {
                        addParticipantVideoCallView(remoteCallViewIdOfUser);
                    });
                } else {
                    mCallDataHolder.addUserVideoEnable(remoteCallViewIdOfUser);
                    User userFromUId = mCallDataHolder.getUserFromUId(remoteCallViewIdOfUser);
                    if (userFromUId != null && !TextUtils.equals(userFromUId.getId(),
                            mCallViewCommunicationListener.getVideoRequesterId())) {
                        Timber.i("Other participant has accepted video request.");
                        getHandler().post(() -> {
                            broadcastOtherParticipantAcceptVideoRequest(buildParticipantVideoCallData(userFromUId,
                                    remoteCallViewIdOfUser));
                        });
                    }
                }
            }
        }
    }

    public void setCallSpeaker() {
        /*
        If device is connected to Bluetooth, the audio will be handled via Bluetooth device.
         */
        if (!CallHelper.isBluetoothHeadsetDeviceConnected()) {
            //Enable loud speaker for video call.
            CallHelper.enableCallAudioOutputOnPhoneOrSpeaker(mContext, CallHelper.isVideoCall(getCallType()));
        }
    }

    private void setupVideoConfig() {
        // In simple use cases, we only need to enable video capturing
        // and rendering once at the initialization step.
        // Note: audio recording and playing is enabled by default.
        mRtcEngine.enableVideo();

        // Please go to this page for detailed explanation
        // https://docs.agora.io/en/Video/API%20Reference/java/classio_1_1agora_1_1rtc_1_1_rtc_engine.html#af5f4de754e2c1f493096641c5c5c1d8f
        mRtcEngine.setVideoEncoderConfiguration(new VideoEncoderConfiguration(
                VideoEncoderConfiguration.VD_640x360,
                VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_15,
                VideoEncoderConfiguration.STANDARD_BITRATE,
                VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT));
    }

    public List<String> getJoinCallUserId() {
        return mCallDataHolder.getJoinCallUserId();
    }

    public boolean isUserInCallingOrUsedToBeInCall(String userId) {
        return mCallDataHolder.isUserInCallingOrUsedToBeInCall(userId);
    }

    public UserGroup getCallGroup() {
        return CallHelper.getGroup(getCurrentCallData());
    }

    public void checkToSendVOIPCallRejectMessageIfNecessary() {
        sendRejectVOIPCallMessageInternally(getCurrentCallData());
    }

    public void sendBackToCallerThatTheIncomingCallShowing() {
        //Only the recipient of the call that will invoke this method.
        if (!CallHelper.isCurrentUserCaller(mContext, getCurrentCallData())) {
            User caller = CallHelper.getCaller(getCurrentCallData());
            if (caller != null) {
                sendRingingVOIPCallMessage(getCurrentCallData(), caller.getId());
            }
        }
    }

    public void addRejectedCallUser(String userId) {
        mCallDataHolder.addRejectedCallUser(userId);
    }

    public boolean isAllParticipantRejectGroupCall() {
        return mCallDataHolder.isAllParticipantRejectGroupCall();
    }

    private boolean isCurrentUserCaller() {
        User caller = CallHelper.getCaller(getCurrentCallData());
        return caller != null && TextUtils.equals(caller.getId(), SharedPrefUtils.getUserId(mContext));
    }

    private void joinChannel() {
        if (mIsRequestingJoinChannel) {
            Timber.i("Ignore join channel request because another join process is already in progress.");
            return;
        }

        /*
        Stop receiving remote video state change callback for the call first engagement to make sure
        that the preparation of adding view will work fine.
         */
        mIsRequestingJoinChannel = true;
        setReceivingAllRemoteVideoStatesChanged(false);
        mIsCurrentUserJoinedChannel = false;
        String channelName = getChannelId(getCurrentCallData(), mCurrentUserId, get1x1CallParticipantId());
        Timber.i("Join channel: " + channelName + ", userId: " + SharedPrefUtils.getUserId(mContext));
        mRtcEngine.joinChannelWithUserAccount(null, channelName, SharedPrefUtils.getUserId(mContext));
    }

    public void startRequestVideoTimer(boolean isRequester) {
        Timber.i("startRequestVideoTimer");
        /*
        As recipient of request camera call, there will be also request screen timer too.
         */
        getHandler().post(() -> mCallTimerHelper.startOneToOneTimer(CallTimerHelper.TimerType.REQUEST_CAMERA,
                isRequester ? CallTimerHelper.StarterTimerType.CALLER : CallTimerHelper.StarterTimerType.RECIPIENT,
                null));
    }

    @Override
    public void call(AbsCallService.CallType callType, Map<String, Object> callData) {
        Timber.i("start calling : " + callType);
        resetAlreadyBroadcastCallEstablished();
        mCallDataHolder.resetCallData();
        mIsMuteForOneToOneCall = false;
        mGotUserBusyResponseFromOneToOneUser = null;
        setProgressCallState("make call", true);
        mCallType = callType;
        mIsMissedCall = true;
        setCurrentCallData(callData);
        generateCallViewData();
        joinChannel();
        sendOutGoingVOIPCallMessage(callData);
        mCallTimerHelper.startOneToOneTimer(CallTimerHelper.TimerType.CALL,
                CallTimerHelper.StarterTimerType.CALLER,
                mBinder.getRecipient().getId());
    }

    public void muteLocalAudioStream(boolean isMute) {
        if (mRtcEngine != null) {
            mRtcEngine.muteLocalAudioStream(isMute);
        }
    }

    @Override
    public void joinGroupCall(AbsCallService.CallType callType, Map<String, Object> callData) {
        Timber.i("joinGroupCall: callType: " + callType + ", callData: " + new Gson().toJson(callData));
        mCallType = callType;
        mCallDataHolder.resetCallData();
        setCurrentCallData(callData);
        mIsMissedCall = true;
        setupVideoConfig();
        if (!isVideoCall(getCallType())) {
            muteLocalVideo();
        } else {
            enableLocalVideo();
        }
        CallData currentUserData = buildMyCallData(CallHelper.isVideoCall(mCallType),
                CallData.CallAction.WAITING_OTHER_TO_JOIN);
        broadcastBindCallView("joinGroupCall", getCallType(), isGroupCall(), Collections.singletonList(currentUserData));
        joinChannel();
        broadcastJoinGroupCallAvailable("joinGroupCall");
    }

    private void broadcastJoinGroupCallAvailable(String tag) {
        if (isGroupCall()) {
            ChatHelper.broadcastGroupJoinStatusOnGroupCallEngaged(tag,
                    mContext,
                    getChannelId(),
                    isVideoCall(getCallType()));
        }
    }

    @Override
    public void setCurrentCallData(Map<String, Object> currentCallData) {
        checkToLoadLocalConversation(currentCallData);
        //Set back the participant to call data holder item for the exchange call data
        //of group does not include participant info.
        if (mCallDataHolder.getConversation() != null) {
            CallHelper.injectGroupParticipants(currentCallData,
                    mCallDataHolder.getConversation().getParticipants());
        }
        mAgoraCallViewBuilder.setCallData(currentCallData);
        mCallDataHolder.clearRejectUserCallId();
        super.setCurrentCallData(currentCallData);
    }

    public void cancelTimer() {
        Timber.i("cancelTimer");
        mCallTimerHelper.cancel();
    }

    /**
     * This method must be called only call data properly set up.
     */
    private void generateCallViewData() {
        getHandler().post(() -> {
            setupVideoConfig();
            if (isVideoCall(mCallType)) {
                if (isCurrentUserCaller()) {
                    SurfaceView myVideoCallView = buildMyVideoCallView();
                    myCallData = initiateCallDataInstance(SharedPrefUtils.getUser(mContext),
                            myVideoCallView,
                            true,
                            CallData.CallAction.INITIALIZE_ViEW);
                    broadcastBindCallView("generateCallViewData", getCallType(), isGroupCall(), Collections.singletonList(myCallData));
                } else {
                    //Incoming call
                    CallData callData = initiateCallDataInstance(SharedPrefUtils.getUser(mContext),
                            null,
                            false,
                            CallData.CallAction.INITIALIZE_ViEW);
                    broadcastBindCallView("generateCallViewData", getCallType(),
                            isGroupCall(),
                            Collections.singletonList(callData));
                }
            } else {
                muteLocalVideo();
                if (isCurrentUserCaller()) {
                    myCallData = initiateCallDataInstance(isGroupCall() ? SharedPrefUtils.getUser(mContext) :
                                    mBinder.getRecipient(),
                            null,
                            false,
                            CallData.CallAction.INITIALIZE_ViEW);
                    broadcastBindCallView("generateCallViewData", getCallType(), isGroupCall(), Collections.singletonList(myCallData));
                } else {
                    CallData callData = initiateCallDataInstance(isGroupCall() ? SharedPrefUtils.getUser(mContext) :
                                    CallHelper.getCaller(getCurrentCallData()),
                            null,
                            false,
                            CallData.CallAction.INITIALIZE_ViEW);
                    broadcastBindCallView("generateCallViewData", getCallType(), isGroupCall(), Collections.singletonList(callData));
                }
            }
        });
    }

    private CallData initiateCallDataInstance(User user,
                                              SurfaceView callVideoView,
                                              boolean hasVideo,
                                              CallData.CallAction callAction) {
        CallData callData = new CallData(user, callVideoView);
        callData.setMuteAudio(!mCallDataHolder.getAudioState(user.getId()));
        callData.setHasVideo(hasVideo);
        callData.setCaller(CallHelper.isCurrentUserCaller(user.getId(), getCurrentCallData()));
        UserGroup group = CallHelper.getGroup(getCurrentCallData());
        if (group != null && mCallDataHolder.getConversation() != null) {
            //Set back the participant to call data holder item for the exchange call data
            //of group does not include participant info.
            group.setParticipants(mCallDataHolder.getConversation().getParticipants());
        }
        callData.setUserGroup(group);
        callData.setCallAction(callAction);
        callData.setVideoCall(hasVideo);

        return callData;
    }


    private void addParticipantVideoCallView(int participantUId) {
        getHandler().post(() -> {
            //Add video call view of other participants
            User userFromUId = mCallDataHolder.getUserFromUId(participantUId);
            CallData recipientCallData = null;
            if (userFromUId != null) {
                SurfaceView remoteVideoCallView = buildRemoteVideoCallView(participantUId);
                recipientCallData = initiateCallDataInstance(userFromUId,
                        remoteVideoCallView,
                        true,
                        CallData.CallAction.ENGAGED_IN_CALL);
            }
            List<CallData> callData = new ArrayList<>();
            if (recipientCallData != null) {
                Timber.i("addParticipantVideoCallView: " + participantUId);
                callData.add(recipientCallData);
                broadcastBindCallView("addParticipantVideoCallView", getCallType(), isGroupCall(), callData);
            }
        });
    }

    public void onReceivingCameraRequestStateChanged(int participantUId, boolean isRequestingVideo) {
        getHandler().post(() -> {
            User userFromUId = mCallDataHolder.getUserFromUId(participantUId);
            if (userFromUId != null) {
                CallData recipientCallData = initiateCallDataInstance(userFromUId,
                        null,
                        isRequestingVideo,
                        CallData.CallAction.ENGAGED_IN_CALL);
                if (isRequestingVideo) {
                    recipientCallData.setVideoCallView(buildRemoteVideoCallView(participantUId));
                }
                broadcastReceivingCameraRequestStateChanged(recipientCallData);
            }
        });
    }

    private CallData buildParticipantVideoCallData(User participant, int uid) {
        SurfaceView remoteVideoCallView = buildRemoteVideoCallView(uid);
        CallData callData = initiateCallDataInstance(participant,
                remoteVideoCallView,
                true,
                CallData.CallAction.ENGAGED_IN_CALL);

        return callData;
    }

    public void enableVideoCallViewForCurrentUserIfNecessary() {
        getHandler().post(() -> {
            Timber.i("enableVideoCallViewForCurrentUserIfNecessary");
            //Add current user video call view
            broadcastBindCallView("enableVideoCallViewForCurrentUserIfNecessary",
                    getCallType(),
                    isGroupCall(),
                    Collections.singletonList(buildMyCallData(true,
                            CallData.CallAction.ENGAGED_IN_CALL)));
        });
    }

    /*
     If the camera in enable in voice call, then the call type should be updated video.
     */
    public AbsCallService.CallType updateToVideoCallTye() {
        Timber.i("updateToVideoCallTye");
        AbsCallService.CallType callType = getCallType();
        if (callType == AbsCallService.CallType.VOICE) {
            mCallType = AbsCallService.CallType.VIDEO;
        } else if (callType == AbsCallService.CallType.GROUP_VOICE) {
            mCallType = AbsCallService.CallType.GROUP_VIDEO;
        }
        CallHelper.updateCallType(getCurrentCallData(), mCallType);
        setCallSpeaker();
        if (mCallViewCommunicationListener != null) {
            mCallViewCommunicationListener.onShouldHideAudioOption();
        }
        return mCallType;
    }

    private CallData buildMyCallData(boolean isVideo, CallData.CallAction callAction) {
        if (isVideo) {
            SurfaceView myVideoCallView = buildMyVideoCallView();
            myCallData = initiateCallDataInstance(SharedPrefUtils.getUser(mContext),
                    myVideoCallView,
                    true,
                    callAction);
            myCallData.setVideoCall(true);
        } else {
            myCallData = initiateCallDataInstance(SharedPrefUtils.getUser(mContext),
                    null,
                    false,
                    callAction);
            myCallData.setVideoCall(false);
        }

        return myCallData;
    }

    private SurfaceView buildMyVideoCallView() {
        // This is used to set a local preview.
        // The steps setting local and remote view are very similar.
        // But note that if the local user do not have a uid or do
        // not care what the uid is, he can set his uid as ZERO.
        // Our server will assign one and return the uid via the event
        // handler callback function (onJoinChannelSuccess) after
        // joining the channel successfully.
        SurfaceView localVideoView = RtcEngine.CreateRendererView(mContext);
        localVideoView.setZOrderMediaOverlay(true);
        // Initializes the local video view.
        // RENDER_MODE_HIDDEN: Uniformly scale the video until it fills the visible boundaries.
        // One dimension of the video may have clipped contents.
        mRtcEngine.setupLocalVideo(new VideoCanvas(localVideoView,
                VideoCanvas.RENDER_MODE_HIDDEN,
                0));

        return localVideoView;
    }

    public void switchCamera() {
        if (mRtcEngine != null) {
            mRtcEngine.switchCamera();
        }
    }

    private SurfaceView buildRemoteVideoCallView(int uId) {
//        SurfaceView existedCallView = mCallDataIdentifier.getRemoteCallView(uId);
//        if (existedCallView != null) {
//            return existedCallView;
//        }

        SurfaceView remoteView = RtcEngine.CreateRendererView(mContext);
        mRtcEngine.setupRemoteVideo(new VideoCanvas(remoteView, VideoCanvas.RENDER_MODE_HIDDEN, uId));
//        mCallDataIdentifier.addRemoteCallView(uId, remoteView);

        return remoteView;
    }

    public void refreshSomeCallServiceData() {
        mCallMessageHelper.updateChatBinderIfNecessary(mBinder.getChatBinder());
    }

    public void setIncomingCallData(AbsCallService.CallType callType, Map<String, Object> callData) {
        Timber.i("setIncomingCallData: " + callType + ", data: " + new Gson().toJson(callData));
        mCallType = callType;
        mGotUserBusyResponseFromOneToOneUser = null;
        mIsMuteForOneToOneCall = false;
        mCallDataHolder.resetCallData();
        setCurrentCallData(callData);
        mIsMissedCall = true;
        cancelTimer();
        mCallTimerHelper.startOneToOneTimer(CallTimerHelper.TimerType.CALL,
                CallTimerHelper.StarterTimerType.RECIPIENT,
                mBinder.getRecipient().getId());
        generateCallViewData();
    }

    private void checkToLoadLocalConversation(Map<String, Object> callData) {
        UserGroup group = CallHelper.getGroup(callData);
        if (group != null) {
            mCallDataHolder.setConversation(mContext, group.getId(),
                    mBinder.get1x1Participant(),
                    true);
        } else {
            mCallDataHolder.setConversation(mContext, getChannelId(callData,
                    mCurrentUserId,
                    get1x1CallParticipantId()),
                    mBinder.get1x1Participant(),
                    false);
        }
    }

    private void sendOutGoingVOIPCallMessage(Map<String, Object> callData) {
        UserGroup group = CallHelper.getGroup(callData);
        if (group != null) {
            Timber.i("Broadcast out-going call for group.");
            Chat chat = buildVOIPCallMessage(callData, mCurrentUserId, null);
            chat.setRawSendingType(mCallType.getValue());
            chat.setSendTo(chat.getChannelId());
            chat.setTimeZone(UserHelper.getTimezone(mContext));
            mBinder.getChatBinder().sendVOIPCallMessage(chat, null);
        } else {
            Timber.i("Broadcast out-going call for 1x1.");
            String recipientId = null;
            if (CallHelper.isCurrentUserCaller(mContext, callData)) {
                recipientId = mBinder.getRecipient().getId();
            } else {
                User caller = CallHelper.getCaller(callData);
                if (caller != null) {
                    recipientId = caller.getId();
                }
            }
            Chat chat = buildVOIPCallMessage(callData, mCurrentUserId, recipientId);
            chat.setRawSendingType(mCallType.getValue());
            chat.setSendTo(recipientId);
            chat.setTimeZone(UserHelper.getTimezone(mContext));
            mBinder.getChatBinder().sendVOIPCallMessage(chat, null);
        }
    }

    private void sendRingingVOIPCallMessage(Map<String, Object> callData, String recipientId) {
        Chat chat = buildVOIPCallMessage(callData, mCurrentUserId, recipientId);
        chat.setRawSendingType(AbsCallService.CallType.RINGING.getValue());
        chat.setSendTo(recipientId);
        mBinder.getChatBinder().sendVOIPCallMessage(chat, null);
    }

    public void sendRejectVOIPCallMessageInternally(Map<String, Object> callData) {
        UserGroup group = CallHelper.getGroup(callData);
        if (group != null) {
            Timber.i("Send reject of group call to all participants");
            sendRejectVOIPCallMessage(mContext,
                    callData,
                    mCurrentUserId,
                    group.getId(),
                    mBinder.getChatBinder(), null);
        } else {
            Timber.i("Send reject of 1x1 call.");
            sendRejectVOIPCallMessage(mContext, callData,
                    mCurrentUserId,
                    mBinder.get1x1Participant().getId(),
                    mBinder.getChatBinder(), null);
        }
    }

    public void sendUserBusyVOIPCallMessage(Map<String, Object> callData) {
        /*
        Use case:
        When current is in another call process and then current user is receiving group incoming call
        which normally current user will send status busy back to that group caller. In this case,
        we will manage to allow enable join button locally for that incoming group call too which current
        can join that call after finishing his current one.
         */
        checkEnableJoinOptionForIncomingGroupCall(callData);

        Chat chat = mCallMessageHelper.buildBusyVOIPCallMessage(mContext,
                callData,
                CallClientService.class.getSimpleName());
        if (chat != null) {
            User caller = CallHelper.getCaller(callData);
            if (caller != null) {
                String channelId = getChannelId(callData,
                        SharedPrefUtils.getUserId(mContext),
                        caller.getId());
                mCallDataHolder.addSendBusyStatusToUser(channelId, caller.getId());
                mBinder.getChatBinder().sendVOIPCallMessage(chat,
                        mBinder.getChatBinder().getNewPublishMessageCallbackInstance(chat,
                                (channelName, error, ackChat) -> {
                                    getHandler().postDelayed(() -> {
                                        mCallMessageHelper.sendCallAcceptedToCurrentCallAccount(callData);
                                    }, 1000);
                                }));
            }
        }
    }

    private void checkEnableJoinOptionForIncomingGroupCall(Map<String, Object> callData) {
        UserGroup group = CallHelper.getGroup(callData);
        if (group != null) {
            JoinChannelStatusUpdateBody body = new JoinChannelStatusUpdateBody();
            body.setChannelOpen(true);
            body.setConversationId(group.getId());
            body.setVideo(CallHelper.isVideoCall(CallHelper.getCallType(callData)));
            ChatHelper.broadcastGroupJoinStatusEven(mContext, body);
        }
    }

    public void sendCallAcceptedToCurrentCallAccountOnOtherDevices() {
        if (mBinder.get1x1Participant() != null) {
            String channelId = getChannelId(getCurrentCallData(), mCurrentUserId, get1x1CallParticipantId());
            mCallMessageHelper.sendCallAcceptedToCurrentCallAccount(getCurrentCallData(), channelId);
        }
    }

    public void sendCallAcceptedToOneToOneCaller() {
        if (!isGroupCall()) {
            String channelId = getChannelId(getCurrentCallData(), mCurrentUserId, get1x1CallParticipantId());
            mCallMessageHelper.sendCallAcceptedToOneToOneCaller(getCurrentCallData(), channelId);
        }
    }

    public boolean isVOIPCallMessageFromCurrentCall(Chat voipMessage) {
        return TextUtils.equals(voipMessage.getChannelId(),
                getChannelId(getCurrentCallData(),
                        mCurrentUserId,
                        get1x1CallParticipantId()));
    }

    public String get1x1CallParticipantId() {
        if (mBinder.get1x1Participant() != null) {
            return mBinder.get1x1Participant().getId();
        }

        return null;
    }

    public static void sendRejectVOIPCallMessage(Context context,
                                                 Map<String, Object> callData,
                                                 String currentUserId,
                                                 String recipientId,
                                                 AbsChatBinder binder,
                                                 ChatSocket.SendMessageCallback callback) {
        //Current must be the sender of this kind of message.
        Map<String, Object> cloneCallData = CallHelper.cloneCallData(callData);
        CallHelper.updateSenderId(cloneCallData, SharedPrefUtils.getUserId(context));
        Chat chat = buildVOIPCallMessage(cloneCallData, currentUserId, recipientId);
        chat.setRawSendingType(AbsCallService.CallType.REJECT_CALL.getValue());
        chat.setSendTo(recipientId);
        binder.sendVOIPCallMessage(chat, callback);
    }

    public User getUserFromCurrentCalLData(String userId) {
        return mCallDataHolder.getParticipantFromCall(userId);
    }

    private static Chat buildVOIPCallMessage(Map<String, Object> callData,
                                             String currentUserId,
                                             String oneToOneParticipantId) {
        Map<String, Object> newCallData = CallHelper.removeGroupParticipants(callData);
        Chat chat = new Chat();
        chat.setSenderId(currentUserId);
        chat.setGroup(CallHelper.getGroup(callData) != null);
        chat.setCallData(newCallData);
        chat.setChannelId(getChannelId(newCallData, currentUserId, oneToOneParticipantId));
        return chat;
    }

    private static Chat buildVOIPCallMessageWithChannelId(Map<String, Object> callData, String channelId) {
        Map<String, Object> newCallData = CallHelper.removeGroupParticipants(callData);
        Chat chat = new Chat();
        chat.setGroup(CallHelper.getGroup(callData) != null);
        chat.setCallData(newCallData);
        chat.setChannelId(channelId);
        return chat;
    }

    private static String getChannelId(Map<String, Object> callData,
                                       String currentUserId,
                                       String oneToOneParticipantId) {
        UserGroup group = CallHelper.getGroup(callData);
        if (group != null) {
            return group.getId();
        } else {
            return ConversationUtil.getConversationId(currentUserId, oneToOneParticipantId);
        }
    }

    public boolean isUserEngagedInCall(String userId) {
        return mCallDataHolder.isUserEngagedInCall(userId);
    }

    public String getChannelId() {
        return getChannelId(getCurrentCallData(), mCurrentUserId, get1x1CallParticipantId());
    }

    public void setEnableLoudSpeaker(boolean isLoudSpeaker) {
        CallHelper.enableCallAudioOutputOnPhoneOrSpeaker(mContext, isLoudSpeaker);
    }

    @Override
    public void enableMics(boolean isEnable) {
        mCallDataHolder.addAudioState(mCurrentUserId, isEnable);
        if (mRtcEngine != null) {
            mRtcEngine.muteLocalAudioStream(!isEnable);
        }
        broadcastOnAudioStateChanged(mCurrentUserId, !isEnable);
    }

    @Override
    public void answer() {
        resetAlreadyBroadcastCallEstablished();
        setProgressCallState("Answer call", true);
        mIsMissedCall = false;
        joinChannel();
        sendCallAcceptedToCurrentCallAccountOnOtherDevices();
        sendCallAcceptedToOneToOneCaller();
        broadcastJoinGroupCallAvailable("answer");
    }

    /**
     * @param isEndedByClickAction
     * @return true mean it has error
     */
    @Override
    public boolean hangup(boolean isEndedByClickAction) {
        Timber.i("hangup: isEndedByClickAction: " + isEndedByClickAction);
        if (isEndedByClickAction && (isMissedCall() || !isGroupCall())) {
            //If user is already in call process, we don't send call reject
            checkToSendVOIPCallRejectMessageIfNecessary();
        }
        cancelTimer();
        leaveChannel();
        setProgressCallState("Hang up call", false);
        resetAlreadyBroadcastCallEstablished();

        return false;
    }

    @Override
    public void stop() {
        hangup(false);
        mCallListener.clear(); // prevent duplicate incoming call screen
    }

    private void leaveChannel() {
        if (mIsRequestingLeaveChannel) {
            Timber.i("Ignore leaving channel request because another leaving process is already in progress.");
            return;
        }

        if (mIsCurrentUserJoinedChannel || mIsRequestingJoinChannel) {
            mIsRequestingLeaveChannel = true;
            Timber.i("Current user request to leave channel");
            mRtcEngine.leaveChannel();
        }
    }

    @Override
    public void logout() {
        leaveChannel();
        setProgressCallState("Log out", false);
        resetAlreadyBroadcastCallEstablished();
    }
}
