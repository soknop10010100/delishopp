package com.proapp.sompom.chat.call;

import android.os.CountDownTimer;

/**
 * Created by Chhom Veasna on 6/10/2020.
 */
public class CallDurationHelper {

    private static final long TIME_INTERVAL = 1000;  //  1 second.

    private int mMilliSecondCounter;
    private CountDownTimer mCallingObserveTimer;
    private CallDurationHelperListener mListener;
    private boolean mIsInProgress;

    public CallDurationHelper(CallDurationHelperListener listener) {
        mListener = listener;
        mCallingObserveTimer = new CountDownTimer(Long.MAX_VALUE, TIME_INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (mListener != null) {
                    mListener.onTimerTick(mMilliSecondCounter++);
                }
            }

            @Override
            public void onFinish() {
                mIsInProgress = false;
            }
        };
    }

    public boolean isInProgress() {
        return mIsInProgress;
    }

    public void startTimer() {
        cancelTimer();
        mIsInProgress = true;
        mCallingObserveTimer.start();
    }

    public void cancelTimer() {
        mMilliSecondCounter = 0;
        mCallingObserveTimer.cancel();
        mIsInProgress = false;
    }

    public interface CallDurationHelperListener {

        void onTimerTick(int milliSecondCounter);
    }
}
