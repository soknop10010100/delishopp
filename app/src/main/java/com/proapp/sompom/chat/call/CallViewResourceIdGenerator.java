package com.proapp.sompom.chat.call;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 3/11/2020.
 */
public class CallViewResourceIdGenerator {

    private static final int DEFAULT_SUBSCRIBER_SIZE = 2;

    private static CallViewResourceIdGenerator sInstance;

    private List<Integer> mSubscriberViewIds = new ArrayList<>();
    private int mPublisherViewId;

    public static CallViewResourceIdGenerator newInstance() {
        if (sInstance == null) {
            sInstance = new CallViewResourceIdGenerator();
        }

        return sInstance;
    }

    private CallViewResourceIdGenerator() {
        generateDefaultId();
    }

    private void generateDefaultId() {
        mPublisherViewId = View.generateViewId();
        mSubscriberViewIds.clear();
        for (int i = 0; i < DEFAULT_SUBSCRIBER_SIZE; i++) {
            mSubscriberViewIds.add(View.generateViewId());
        }
    }

    public int getPublisherViewId() {
        return mPublisherViewId;
    }

    public int getSubscriberViewId(int index) {
        if (index >= 0 && index < mSubscriberViewIds.size()) {
            return mSubscriberViewIds.get(index);
        }

        return 0;
    }

    public void checkToGenerateMoreSubscriberViewIdIfNecessary(int subscriberSize) {
        if (subscriberSize > mSubscriberViewIds.size()) {
            int moreIds = subscriberSize - mSubscriberViewIds.size();
            Timber.i("Need to generate subscriber view id " + moreIds + " more.");
            for (int i = 0; i < moreIds; i++) {
                mSubscriberViewIds.add(View.generateViewId());
            }
        }
    }

    public List<Integer> getSubscriberViewIds() {
        return mSubscriberViewIds;
    }

    public void resetIds() {
        generateDefaultId();
    }
}
