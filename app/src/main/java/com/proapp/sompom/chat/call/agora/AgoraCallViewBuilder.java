package com.proapp.sompom.chat.call.agora;

import android.view.SurfaceView;

import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.call.CallData;
import com.proapp.sompom.model.result.User;

import java.util.Map;

/**
 * Created by Chhom Veasna on 6/16/2020.
 */
public class AgoraCallViewBuilder {

    private Map<String, Object> mCallData;

    public AgoraCallViewBuilder() {
    }

    public void setCallData(Map<String, Object> callData) {
        mCallData = callData;
    }

    public CallData buildJoinAudioCallData(User user) {
        CallData callData = initiateCallDataInstance(user,
                null,
                CallData.CallAction.ENGAGED_IN_CALL,
                false);

        return callData;
    }

    private CallData initiateCallDataInstance(User user,
                                              SurfaceView callVideoView,
                                              CallData.CallAction callAction,
                                              boolean isVideoType) {
        CallData callData = new CallData(user, callVideoView);
        callData.setCaller(CallHelper.isCurrentUserCaller(user.getId(), mCallData));
        UserGroup group = CallHelper.getGroup(mCallData);
        callData.setUserGroup(group);
        callData.setCallAction(callAction);
        callData.setVideoCall(isVideoType);

        return callData;
    }
}
