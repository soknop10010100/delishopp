package com.proapp.sompom.chat.call;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;

import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.call.CallData;
import com.proapp.sompom.model.notification.NotificationProfileDisplayAdaptive;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.NotificationUtils;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 2/24/2020.
 */
public abstract class AbsCallClient<T> {

    protected Context mContext;
    protected ClientInitializeCallback mInitializeCallback;
    protected List<AbsCallService.CallingListener> mCallListener = new ArrayList<>();
    protected Map<String, Object> mCurrentCallData;
    protected GeneralCallBehavior mGeneralCallBehavior;
    protected CallViewCommunicationListener mCallViewCommunicationListener;
    protected boolean mIsAlreadyBroadcastCallEstablished;
    protected String mCurrentUserId;

    public AbsCallClient(Context context,
                         ClientInitializeCallback initializeCallback,
                         GeneralCallBehavior generalCallBehavior) {
        mContext = context;
        mInitializeCallback = initializeCallback;
        mGeneralCallBehavior = generalCallBehavior;
    }

    public void setInitializeCallback(ClientInitializeCallback initializeCallback) {
        mInitializeCallback = initializeCallback;
    }

    public void resetAlreadyBroadcastCallEstablished() {
        mIsAlreadyBroadcastCallEstablished = false;
    }

    public Map<String, Object> getCurrentCallData() {
        return mCurrentCallData;
    }

    public void setCurrentCallData(Map<String, Object> currentCallData) {
        mCurrentCallData = currentCallData;
    }

    public void setCallViewCommunicationListener(CallViewCommunicationListener callViewCommunicationListener) {
        mCallViewCommunicationListener = callViewCommunicationListener;
    }

    public CallViewCommunicationListener getCallViewCommunicationListener() {
        return mCallViewCommunicationListener;
    }

    protected void onClientConnected() {
        Timber.i("onClientConnected: " + mInitializeCallback);
        if (mInitializeCallback != null) {
            mInitializeCallback.onClientConnected(getCallType());
        }
    }

    protected void onClientDisconnected() {
        Timber.i("onClientDisconnected: " + mInitializeCallback);
        if (mInitializeCallback != null) {
            mInitializeCallback.onClientDisconnected(getCallType());
        }
    }

    public boolean isGroupCall() {
        if (getCurrentCallData() == null) {
            throw new AndroidRuntimeException("Call data must exist before calling this method.");
        } else {
            return CallHelper.getGroup(getCurrentCallData()) != null;
        }
    }

    public boolean isVideoCall(AbsCallService.CallType callType) {
        return CallHelper.isVideoCall(callType);
    }

    protected void onClientInitError(Throwable ex) {
        Timber.i("onClientInitError: " + mInitializeCallback);
        if (mInitializeCallback != null) {
            mInitializeCallback.onInitError(getCallType(), ex);
        }
    }

    public void onCallTypeUpdated() {
        if (mInitializeCallback != null) {
            mInitializeCallback.onCallTypeUpdated(getCallType());
        }
    }

    public void removeCallListener(AbsCallService.CallingListener callListener) {
        mCallListener.remove(callListener);
    }

    protected void broadcastIncomingCall(AbsCallService.CallType callType, Map<String, Object> headers) {
        for (AbsCallService.CallingListener callingListener : mCallListener) {
            callingListener.onIncomingCall(callType, headers, null);
        }
    }

    protected void broadcastCallEstablished(AbsCallService.CallType callType, String channelId) {
        Timber.i("mIsAlreadyBroadcastCallEstablished: " + mIsAlreadyBroadcastCallEstablished);
        if (!mIsAlreadyBroadcastCallEstablished) {
            mIsAlreadyBroadcastCallEstablished = true;
            boolean groupCall = isGroupCall();
            for (AbsCallService.CallingListener callingListener : mCallListener) {
                callingListener.onCallEstablished(callType, channelId, groupCall);
            }
            if (mGeneralCallBehavior != null) {
                mGeneralCallBehavior.vibrate();
            }
        }
    }

    public void dispatchGroupCallNotification(Context context,
                                              AbsCallService.CallActionType callActionType,
                                              User recipient) {
        generateCallNotificationData(context,
                getCurrentCallData(),
                recipient,
                new CallClientHelperListener() {
                    @Override
                    public void onGenerateCallDisplayNotificationData(NotificationData data) {
                        dispatchCallNotification(callActionType,
                                getCallType(),
                                data,
                                data.getTitle(),
                                data.getUserName(),
                                data.getGroupId(),
                                data.isCaller());
                    }
                });
    }

    public static void generateCallNotificationData(Context context,
                                                    Map<String, Object> data,
                                                    User recipient,
                                                    CallClientHelperListener listener) {
        if (data != null) {
            String title = "";
            String userName = "";
            String groupId = null;
            boolean isCaller = false;
            User caller = CallHelper.getCaller(data);
            UserGroup group = CallHelper.getGroup(data);
            NotificationProfileDisplayAdaptive notificationProfileDisplayAdaptive = null;
            if (group != null) {
                if (!TextUtils.isEmpty(group.getName())) {
                    groupId = group.getId();
                    title = group.getName();
                }
                if (caller != null) {
                    userName = caller.getFullName();
                }
                notificationProfileDisplayAdaptive = group;
            } else {
                if (CallHelper.isCurrentUserCaller(context, data)) {
                    title = recipient.getFullName();
                    userName = recipient.getFullName();
                    notificationProfileDisplayAdaptive = recipient;
                } else if (caller != null) {
                    title = caller.getFullName();
                    userName = caller.getFullName();
                    notificationProfileDisplayAdaptive = caller;
                }
            }

            if (caller != null) {
                isCaller = TextUtils.equals(SharedPrefUtils.getUserId(context), caller.getId());
            }

            Timber.i("notificationProfileDisplayAdaptive: " + notificationProfileDisplayAdaptive);
            final NotificationData notificationData = new NotificationData(title, userName, isCaller, groupId);
            notificationData.setNotificationProfileDisplayAdaptive(notificationProfileDisplayAdaptive);
            if (notificationProfileDisplayAdaptive != null) {
                NotificationUtils.loadNotificationProfile(context,
                        notificationProfileDisplayAdaptive,
                        profile -> {
                            notificationData.setCallProfilePhoto(profile);
                            if (listener != null) {
                                listener.onGenerateCallDisplayNotificationData(notificationData);
                            }
                        });
            } else {
                if (listener != null) {
                    listener.onGenerateCallDisplayNotificationData(notificationData);
                }
            }
        }
    }

    protected void broadcastVideoCameraStateChanged(String userId, boolean isOff, boolean isCurrentUser) {
        for (AbsCallService.CallingListener callingListener : mCallListener) {
            callingListener.onVideoCameraStateChanged(userId,
                    isOff,
                    isCurrentUser);
        }
    }

    protected void broadcastBindCallView(String tag,
                                         AbsCallService.CallType callType,
                                         boolean isGroupCall,
                                         List<CallData> callDataList) {
//        Timber.i("broadcastBindCallView from " + tag);
//        logCallData(callDataList);
        for (AbsCallService.CallingListener callingListener : mCallListener) {
            callingListener.addCallView(callType,
                    isGroupCall,
                    callDataList
            );
        }
    }

    private void logCallData(List<CallData> callData) {
        for (CallData callData1 : callData) {
            Timber.i("User: " + callData1.getUser().getFullName() +
                    ", Call Action: " + callData1.getCallAction() +
                    ", isVideo: " + callData1.isVideoCall() +
                    ", isHasVideo: " + callData1.isHasVideo() +
                    ", isCaller: " + callData1.isCaller());
        }
    }

    protected void broadcastRequestVideoFromCurrentUser(CallData callData, boolean isRequestedCamera) {
        for (AbsCallService.CallingListener callingListener : mCallListener) {
            callingListener.onRequestingCameraStateChanged(callData, isRequestedCamera);
        }
    }

    protected void broadcastOtherParticipantAcceptVideoRequest(CallData participantData) {
        for (AbsCallService.CallingListener callingListener : mCallListener) {
            callingListener.onOtherParticipantAcceptVideoRequest(participantData);
        }
    }

    protected void broadcastReceivingCameraRequestStateChanged(CallData participantData) {
        for (AbsCallService.CallingListener callingListener : mCallListener) {
            callingListener.onReceivingCameraRequestStateChanged(participantData);
        }
    }

    protected void broadcastRemoveCallView(String userId) {
        for (AbsCallService.CallingListener callingListener : mCallListener) {
            callingListener.removeCallView(userId);
        }
    }

    protected void broadcastOnCheckShowingRequestVideoCamera(boolean shouldShow, User requester) {
        for (AbsCallService.CallingListener callingListener : mCallListener) {
            callingListener.onCheckShowingRequestVideoCamera(shouldShow,
                    getCallType(),
                    requester,
                    isGroupCall());
        }
    }

    protected void broadcastOnAudioStateChanged(String userId, boolean isMute) {
        for (AbsCallService.CallingListener callingListener : mCallListener) {
            callingListener.onAudioStateChanged(userId, isMute);
        }
    }

    protected void broadcastGroupMemberJoinedCall(User user) {
        for (AbsCallService.CallingListener callingListener : mCallListener) {
            callingListener.onGroupMemberJoinedCall(user);
        }
    }

    protected void broadcastGroupMemberLeftGroupCall(User user) {
        for (AbsCallService.CallingListener callingListener : mCallListener) {
            callingListener.onGroupMemberLeftGroupCall(user);
        }
    }

    protected void broadcastOnCurrentUserJoinOrLeaveGroupCall(String channelId, boolean isLeft, int callDuration) {
        AbsCallService.CallType callType = getCallType();
        for (AbsCallService.CallingListener callingListener : mCallListener) {
            callingListener.onCurrentUserJoinOrLeaveGroupCall(callType, channelId, isLeft, callDuration);
        }
    }

    public void addCallListener(AbsCallService.CallingListener callListener) {
        if (mCallListener.contains(callListener)) {
            return;
        }
        mCallListener.add(callListener);
    }

    protected void broadcastOnCallEnd(AbsCallService.CallType callType,
                                      String channelId,
                                      User recipient,
                                      boolean isMissedCall,
                                      int calledDuration,
                                      boolean isEndedByClickAction,
                                      boolean isCauseByTimeOut,
                                      boolean isCausedByParticipantOffline) {
        boolean groupCall = isGroupCall();
        for (AbsCallService.CallingListener callingListener : mCallListener) {
            callingListener.onCallEnd(callType,
                    channelId,
                    groupCall,
                    recipient,
                    isMissedCall,
                    calledDuration,
                    isEndedByClickAction,
                    isCauseByTimeOut,
                    isCausedByParticipantOffline);
        }
        if (mGeneralCallBehavior != null) {
            mGeneralCallBehavior.cancelNotification();
        }
        CallHelper.vibrate(mContext);
    }

    protected void broadcastCallTimeOut(AbsCallService.CallType callType,
                                        CallTimerHelper.TimerType timerType,
                                        CallTimerHelper.StarterTimerType starterTimerType) {
        for (AbsCallService.CallingListener callingListener : mCallListener) {
            callingListener.onCallTimerUp(callType,
                    timerType,
                    starterTimerType);
        }
    }

    public void clearAllCallListener() {
        mCallListener.clear();
    }

    public abstract void startService(String userId);

    public abstract void call(AbsCallService.CallType callType, Map<String, T> headers);

    public abstract void joinGroupCall(AbsCallService.CallType callType, Map<String, T> headers);

    public abstract void enableMics(boolean isEnable);

    public abstract void toggleVideoCamera(boolean isOn);

    public abstract void answer();

    public abstract boolean hangup(boolean isEndedByClickAction);

//    public abstract NotificationResult relayRemotePushNotificationPayload(final Map payload);

    //Completely stop all service
    public abstract void stop();

    public void logout() {
        //Empty Impl...
    }

    public abstract AbsCallService.CallType getCallType();

    public enum CallActionType {
        CALL("call"),
        INCOMING("incoming"),
        DECLINE("decline"),
        CANCEL("cancel"),
        PROGRESS("progress"),
        BUSY("busy"),
        JOINED("joined"),
        CANCEL_REQUEST_VIDEO("cancelRequestVideo"),
        NONE("none");

        private String mValue;

        CallActionType(String value) {
            mValue = value;
        }

        public String getValue() {
            return mValue;
        }

        public static CallActionType getTypeFromValue(String value) {
            for (CallActionType callActionType : CallActionType.values()) {
                if (TextUtils.equals(callActionType.getValue(), value)) {
                    return callActionType;
                }
            }

            return NONE;
        }
    }

    protected void dispatchCallNotification(AbsCallService.CallActionType actionType,
                                            AbsCallService.CallType callType,
                                            NotificationData data,
                                            String title,
                                            String userName,
                                            String groupId,
                                            boolean isCaller) {
        if (mGeneralCallBehavior != null) {
            mGeneralCallBehavior.dispatchCallNotification(actionType,
                    callType,
                    data,
                    title,
                    userName,
                    groupId,
                    isCaller);
        }
    }

    public interface ClientInitializeCallback {

        void onClientConnected(AbsCallService.CallType callType);

        void onClientDisconnected(AbsCallService.CallType callType);

        void onCallTypeUpdated(AbsCallService.CallType callType);

        void onInitError(AbsCallService.CallType callType, Throwable ex);
    }

    public interface CallViewCommunicationListener {

        boolean isCurrentCallViewHasVideo();

        boolean isShowingRequestCameraScreen();

        String getVideoRequesterId();

        CallData.CallAction getCurrentUserCallAction();

        boolean shouldAddOtherParticipantVideo();

        boolean isOneToOneRecipientAcceptedCall();

        boolean isMicOff();

        void onShouldHideAudioOption();

        boolean isParticipantCallViewAdded(String userId);
    }

    public interface CallClientHelperListener {
        default void onGenerateCallDisplayNotificationData(NotificationData data) {
        }
    }

    public static class NotificationData {

        private String mTitle;
        private String mUserName;
        private boolean mIsCaller;
        private String mGroupId;
        private Bitmap mCallProfilePhoto;
        private NotificationProfileDisplayAdaptive mNotificationProfileDisplayAdaptive;

        public NotificationData(String title, String userName) {
            mTitle = title;
            mUserName = userName;
        }

        public NotificationData(String title, String userName, boolean isCaller, String groupId) {
            mTitle = title;
            mUserName = userName;
            mIsCaller = isCaller;
            mGroupId = groupId;
        }

        public Bitmap getCallProfilePhoto() {
            return mCallProfilePhoto;
        }

        public void setCallProfilePhoto(Bitmap callProfilePhoto) {
            mCallProfilePhoto = callProfilePhoto;
        }

        public NotificationProfileDisplayAdaptive getNotificationProfileDisplayAdaptive() {
            return mNotificationProfileDisplayAdaptive;
        }

        public void setNotificationProfileDisplayAdaptive(NotificationProfileDisplayAdaptive notificationProfileDisplayAdaptive) {
            mNotificationProfileDisplayAdaptive = notificationProfileDisplayAdaptive;
        }

        public boolean isCaller() {
            return mIsCaller;
        }

        public void setCaller(boolean caller) {
            mIsCaller = caller;
        }

        public String getGroupId() {
            return mGroupId;
        }

        public void setGroupId(String groupId) {
            mGroupId = groupId;
        }

        public String getTitle() {
            return mTitle;
        }

        public void setTitle(String title) {
            mTitle = title;
        }

        public String getUserName() {
            return mUserName;
        }

        public void setUserName(String userName) {
            mUserName = userName;
        }

        public boolean isGroupCall() {
            return !TextUtils.isEmpty(mGroupId);
        }
    }
}
