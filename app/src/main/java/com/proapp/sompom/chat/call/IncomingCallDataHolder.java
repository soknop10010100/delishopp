package com.proapp.sompom.chat.call;

import android.os.Parcel;
import android.os.Parcelable;

import com.proapp.sompom.helper.AbsCallService;

import java.util.HashMap;
import java.util.Map;

public class IncomingCallDataHolder implements Parcelable {

    private Map<String, Object> mCallData;
    private boolean mIsBackground = false;
    private String mCallType;

    public IncomingCallDataHolder() {
    }

    public void setCallType(String callType) {
        mCallType = callType;
    }

    public AbsCallService.CallType getCallType() {
        return AbsCallService.CallType.getFromValue(mCallType);
    }

    public Map<String, Object> getCallData() {
        return mCallData;
    }

    public void setCallData(Map<String, Object> callData) {
        this.mCallData = callData;
    }

    public boolean isBackground() {
        return mIsBackground;
    }

    public void setBackground(boolean background) {
        mIsBackground = background;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mCallData.size());
        for (Map.Entry<String, Object> entry : this.mCallData.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeValue(entry.getValue());
        }
        dest.writeByte(this.mIsBackground ? (byte) 1 : (byte) 0);
        dest.writeString(this.mCallType);
    }

    protected IncomingCallDataHolder(Parcel in) {
        int mCallDataSize = in.readInt();
        this.mCallData = new HashMap<String, Object>(mCallDataSize);
        for (int i = 0; i < mCallDataSize; i++) {
            String key = in.readString();
            Object value = in.readValue(Object.class.getClassLoader());
            this.mCallData.put(key, value);
        }
        this.mIsBackground = in.readByte() != 0;
        this.mCallType = in.readString();
    }

    public static final Creator<IncomingCallDataHolder> CREATOR = new Creator<IncomingCallDataHolder>() {
        @Override
        public IncomingCallDataHolder createFromParcel(Parcel source) {
            return new IncomingCallDataHolder(source);
        }

        @Override
        public IncomingCallDataHolder[] newArray(int size) {
            return new IncomingCallDataHolder[size];
        }
    };
}
