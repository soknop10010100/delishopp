package com.proapp.sompom.chat.call;

import android.text.TextUtils;

import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.model.result.User;

import java.util.List;
import java.util.Map;

/**
 * Created by Chhom Veasna on 3/25/2020.
 */
public class GroupCallHelper {

    private static GroupJoinDataModel sCurrentGroupJoinDataSelection;

    private GroupCallHelper() {
    }

    public static void setsCurrentGroupJoinDataSelection(GroupJoinDataModel groupJoinDataSelection) {
        GroupCallHelper.sCurrentGroupJoinDataSelection = groupJoinDataSelection;
    }

    public static GroupJoinDataModel getCurrentGroupJoinDataSelection() {
        return sCurrentGroupJoinDataSelection;
    }

//    public static void checkIfGroupIsInCalling(Context context,
//                                               Conversation groupConversation,
//                                               Map<String, Stream> recordedSteamMap,
//                                               GroupCallHelperListener listener) {
//        if (recordedSteamMap == null || recordedSteamMap.isEmpty()) {
//            if (listener != null) {
//                listener.onNoJoinCallAvailable(groupConversation.getGroupId());
//            }
//        } else {
//            List<Stream> availableGroupStream = new ArrayList<>();
//            for (Map.Entry<String, Stream> entry : recordedSteamMap.entrySet()) {
//                String publisherId = CallHelper.getPublisherId(entry.getValue().getName());
//                if (isUserInList(groupConversation.getParticipants(), publisherId)) {
//                    String publisherGroupId = CallHelper.getPublisherGroupId(entry.getValue().getName());
//                    if (TextUtils.equals(publisherGroupId, groupConversation.getGroupId())) {
//                        availableGroupStream.add(entry.getValue());
//                    }
//                }
//            }
//
//            if (!availableGroupStream.isEmpty()) {
//                if (listener != null) {
//                 /*
//            Normally call type and caller id in all items in availableGroupStream must be the same, so
//            we pick up the first element to use.
//             */
//                    AbsCallService.CallType callType = CallHelper.getPublisherCallType(availableGroupStream.get(0).getName());
//                    User caller = findUserInList(groupConversation.getParticipants(),
//                            CallHelper.getCallerIdFromPublisher(availableGroupStream.get(0).getName()));
//                    GroupJoinDataModel groupJoinDataModel = new GroupJoinDataModel(groupConversation.getGroupId(),
//                            callType,
//                            availableGroupStream,
//                            CallHelper.createJoinCallingExtraDataForOpenTokService(context,
//                                    callType,
//                                    caller,
//                                    UserGroup.newInstance(groupConversation)));
//                    listener.onJoinCallAvailable(groupJoinDataModel);
//                }
//            } else {
//                if (listener != null) {
//                    listener.onNoJoinCallAvailable(groupConversation.getGroupId());
//                }
//            }
//        }
//    }

    private static boolean isUserInList(List<User> userList, String userId) {
        for (User user : userList) {
            if (TextUtils.equals(user.getId(), userId)) {
                return true;
            }
        }

        return false;
    }

    private static User findUserInList(List<User> userList, String userId) {
        for (User user : userList) {
            if (TextUtils.equals(user.getId(), userId)) {
                return user;
            }
        }

        return null;
    }

    public static class GroupCallHelperListener {

        public void onJoinCallAvailable(GroupJoinDataModel groupJoinDataModel) {
            //Empty Impl...
        }

        public void onNoJoinCallAvailable(String groupId) {
            //Empty Impl...
        }
    }

    public static class GroupJoinDataModel {

        private String mGroupId;
        private AbsCallService.CallType mCallType;
        private Map<String, Object> mNewCallData;

        public GroupJoinDataModel(String groupId,
                                  AbsCallService.CallType callType,
                                  Map<String, Object> newCallData) {
            mGroupId = groupId;
            mCallType = callType;
            mNewCallData = newCallData;
        }

        public String getGroupId() {
            return mGroupId;
        }

        public AbsCallService.CallType getCallType() {
            return mCallType;
        }

        public Map<String, Object> getNewCallData() {
            return mNewCallData;
        }
    }
}
