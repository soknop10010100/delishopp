package com.proapp.sompom.chat.call;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.model.result.Chat;

import timber.log.Timber;

public class CallServiceConnectionProvider {

    private static CallServiceConnectionProvider sCallServiceConnectionProvider;
    //There will be only one Call Service Connection object.
    private CallServiceConnectionHelper mCallServiceConnectionHelper;

    private CallServiceConnectionProvider() {
    }

    public static CallServiceConnectionProvider getInstance() {
        if (sCallServiceConnectionProvider == null) {
            sCallServiceConnectionProvider = new CallServiceConnectionProvider();
        }
        return sCallServiceConnectionProvider;
    }

    public void unbindAllServiceConnection() {
        Timber.i("unbindAllServiceConnection");
        if (mCallServiceConnectionHelper != null) {
            mCallServiceConnectionHelper.unBindAllServiceConnections();
            mCallServiceConnectionHelper = null;
        }
    }

    public CallServiceConnectionHelper getCallServiceConnection(Context context) {
        if (mCallServiceConnectionHelper == null) {
            mCallServiceConnectionHelper = new CallServiceConnectionHelper(context);
        }

        return mCallServiceConnectionHelper;
    }

    public boolean isIncomingBeingChecked(Chat callMessage) {
        if (mCallServiceConnectionHelper == null || mCallServiceConnectionHelper.getCallMessage() == null) {
            return false;
        } else {
            return TextUtils.equals(mCallServiceConnectionHelper.getCallMessage().getChannelId(),
                    callMessage.getChannelId());
        }
    }

    public boolean isShowingIncomingCallNotification() {
        return mCallServiceConnectionHelper != null && mCallServiceConnectionHelper.isDisplayingCallNotification();
    }
}

