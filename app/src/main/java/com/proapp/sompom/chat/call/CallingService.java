package com.proapp.sompom.chat.call;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.desmond.squarecamera.utils.ThemeManager;
import com.proapp.sompom.chat.AbsChatBinder;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.CallDataManager;
import com.proapp.sompom.widget.call.CallScreenView;

import java.util.Map;

import javax.inject.Inject;

import timber.log.Timber;

public class CallingService extends AbsCallService {

    @Inject
    public ApiService mApiService;
    private WindowManager mWindowManager;
    private CallScreenView mCallScreenView;
    private CallServiceBinder mBinder = new CallServiceBinder();
    private CallClientService mCallClientService;
    private CallType mInCallActionType = CallType.VOICE;
    private boolean mIsInFloatingMode;

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.i("onCreate");
        if (mApiService == null) {
            getControllerComponent().inject(CallingService.this);
        }
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(getApplicationContext(),
                ThemeManager.getAppTheme(getApplicationContext()).getThemeRes());
        mCallScreenView = new CallScreenView(contextThemeWrapper,
                getApplicationContext(),
                new CallDataManager(getApplicationContext(), mApiService),
                mBinder,
                new CallScreenView.CallScreenViewListener() {
                    @Override
                    public void onCallEnded() {
                        Timber.e("onCallEnded");
                        getHandler().post(() -> {
                            CallNotificationHelper.cancelCallInfoNotification(getApplicationContext());
                            removeFloatingCallView();
                            if (mBinder.getCallStateListener() != null) {
                                mBinder.getCallStateListener().onCallEnded();
                            }
                        });
                    }

                    @Override
                    public void onCallChannelIdIdGenerated(String channelId) {
                        mBinder.setInProgressCallChannelId(channelId);
                    }

                    @Override
                    public void onMessageClicked(ChatIntent chatIntent) {
                        getHandler().post(() -> {
                            if (mBinder.getCallStateListener() != null) {
                                mBinder.getCallStateListener().onMessageClicked(chatIntent);
                            }
                        });
                    }

                    @Override
                    public void onResizeScreenBackToFullAction() {
//                        getHandler().post(() -> {
//                            removeFloatingCallView();
//                            startActivity(CallingIntent.getResizeCallScreenToFullIntent(getApplicationContext()));
//                        });
                    }
                });
    }

    public Handler getHandler() {
        return new Handler(Looper.getMainLooper());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Timber.i("onDestroy");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Timber.i("onStartCommand: flags: " + flags + ", startId: " + startId);
        return super.onStartCommand(intent, flags, startId);
    }

    private void removeFloatingCallView() {
        if (mWindowManager != null && mCallScreenView.getParent() != null && mIsInFloatingMode) {
            mWindowManager.removeView(mCallScreenView);
            mCallScreenView.setOnTouchListener(null);
            mIsInFloatingMode = false;
            Timber.i("removeCallViewFromWindowManager: " + mWindowManager.hashCode());
        }
    }

    private void bindCallViewToActivity(ViewGroup activityRootView) {
        mIsInFloatingMode = false;
        checkToRemoveCallViewFromItsParentView();
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mCallScreenView.setAlpha(1f);
        activityRootView.addView(mCallScreenView, params);
        Timber.e("bindCallViewToActivity: activityRootView: " + activityRootView.hashCode());
    }

    private void checkToRemoveCallViewFromItsParentView() {
        if (mCallScreenView.getParent() != null) {
            if (mCallScreenView.getParent() instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) mCallScreenView.getParent();
                viewGroup.removeView(mCallScreenView);
                viewGroup = null;
                Timber.i("Remove call screen view from view group.");
            } else if (mWindowManager != null && mCallScreenView.getWindowToken() != null) {
                try {
                    mWindowManager.removeView(mCallScreenView);
                    Timber.i("Remove call screen view from window manager.");
                } catch (Exception ex) {
                    Timber.e("Remove call screen view from window manager failed: " + ex);
                    SentryHelper.logSentryError(ex);
                }
            }
        }
    }

    private int getFloatingWindowType() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            return WindowManager.LayoutParams.TYPE_PHONE;
        }
    }

    private void enableFloatingCallView(AppCompatActivity appCompatActivity) {
        if (CallHelper.hasFloatingPermissionEnable(getApplicationContext())) {
            mWindowManager = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
            final Point screenSize = new Point();
            mWindowManager.getDefaultDisplay().getSize(screenSize);
            WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    screenSize.x / 3,
                    screenSize.y / 3,
                    getFloatingWindowType(),
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                            | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH |
                            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    PixelFormat.TRANSLUCENT);
            //Specify the view start position
            params.gravity = Gravity.TOP | Gravity.START;
            params.x = 0;
            params.y = 0;
            checkToRemoveCallViewFromItsParentView();
            getHandler().postDelayed(() -> {
                Timber.i("enableFloatingCallView");
                mCallScreenView.onFloatingWindowEnable(true);
                mWindowManager.addView(mCallScreenView, params);
                mIsInFloatingMode = true;
                final FloatingCallViewGesture viewGesture = new FloatingCallViewGesture(mCallScreenView,
                        mWindowManager,
                        screenSize,
                        isNeedResizeBackToFullScreen -> {
                            getHandler().post(() -> {
                                removeFloatingCallView();
                                if (isNeedResizeBackToFullScreen) {
                                    startActivity(CallingIntent.getResizeCallScreenToFullIntent(getApplicationContext()));
                                }
                            });
                        });
                final FloatingCallGesture gesture = new FloatingCallGesture(mWindowManager,
                        mCallScreenView,
                        viewGesture,
                        (x, y) -> {
                            // Move the animation listener out here to directly update the
                            // mCallScreenView with the exact layoutParam that was set when adding
                            // the call screen to window manager
                            try {
                                // Check if the call is currently in floating mode before updating
                                // the view
                                if (mIsInFloatingMode) {
                                    params.x = x;
                                    params.y = y;
                                    mWindowManager.updateViewLayout(mCallScreenView, params);
                                }
                            } catch (Exception ex) {
                                Timber.e(ex);
                                SentryHelper.logSentryError(ex);
                            }
                        });
                getHandler().postDelayed(() -> {
                    mCallScreenView.adaptViewOnFloating();
                    mCallScreenView.setOnTouchListener(gesture);
                }, 100);
            }, 100);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public CallServiceBinder getBinder() {
        return mBinder;
    }

    public class CallServiceBinder extends Binder implements GeneralCallBehavior {

        private User mRecipient;
        private AbsChatBinder mChatBinder;
        private User m1x1Participant;
        private String mInProgressCallChannelId;
        private CallStateListener mCallStateListener;

        public User get1x1Participant() {
            return m1x1Participant;
        }

        public void set1x1Participant(User participant) {
            this.m1x1Participant = participant;
        }

        public String getInProgressCallChannelId() {
            return mInProgressCallChannelId;
        }

        public void setInProgressCallChannelId(String inProgressCallChannelId) {
            mInProgressCallChannelId = inProgressCallChannelId;
        }

        public AbsChatBinder getChatBinder() {
            return mChatBinder;
        }

        public User getRecipient() {
            return mRecipient;
        }

        public void setRecipient(User recipient) {
            mRecipient = recipient;
        }

        public void setChatBinder(AbsChatBinder chatBinder) {
            Timber.i("setChatBinder: " + chatBinder);
            mChatBinder = chatBinder;
        }

        public void start(String userId, AbsCallClient.ClientInitializeCallback callback) {
            AbsCallClient.ClientInitializeCallback initializeCallback = new AbsCallClient.ClientInitializeCallback() {
                @Override
                public void onClientConnected(CallType callType) {
                    Timber.i("callback: " + callback);
                    if (callback != null) {
                        callback.onClientConnected(callType);
                    }
                }

                @Override
                public void onClientDisconnected(CallType callType) {
                    Timber.i("onClientDisconnected");
                    if (callback != null) {
                        callback.onClientDisconnected(callType);
                    }
                }

                @Override
                public void onCallTypeUpdated(CallType callType) {
                    Timber.i("onCallTypeUpdated: " + callType);
                    mInCallActionType = callType;
                    if (callback != null) {
                        callback.onCallTypeUpdated(callType);
                    }
                }

                @Override
                public void onInitError(CallType callType, Throwable ex) {
                    if (callback != null) {
                        callback.onInitError(callType, ex);
                    }
                }
            };

            if (mCallClientService == null) {
                mCallClientService = new CallClientService(getApplicationContext(),
                        mBinder,
                        initializeCallback,
                        this);
            }
            //Must call this method after login again to make sure the correct user Id will be used.
            mCallClientService.loadCurrentUserId();
            //Start both voice and video client services at the same time since we use two different
            //frameworks for voice and video call.
            mCallClientService.setInitializeCallback(initializeCallback);
            mCallClientService.startService(userId);
            Timber.i("Start call service: " + CallingService.this.hashCode() + ", mVoiceCallClientService: " + mCallClientService.hashCode());
        }

        private CallStateListener getCallStateListener() {
            return mCallStateListener;
        }

        public CallClientService getVoiceCallClientService() {
            return mCallClientService;
        }

        public void call(CallType callType,
                         Map<String, Object> headers,
                         User recipient) {
            Timber.i("callType: " + callType);
            mInCallActionType = callType;
            mCallClientService.call(callType, headers);
        }

        public void answer() {
            if (mCallClientService != null) {
                mCallClientService.answer();
            }
        }

        public boolean hangup(boolean isEndedByClickAction) {
            Timber.i("hangup: " + mInCallActionType);
            if (mCallClientService != null) {
                mCallClientService.hangup(isEndedByClickAction);
            }

            return false;
        }

        public void clearAllCallListener() {
            mCallClientService.clearAllCallListener();
        }

        public void addCallListener(CallingListener callListener) {
            mCallClientService.addCallListener(callListener);
        }

        @Nullable
        public Product getCurrentProduct(Map<String, String> header) {
            return null;
        }

        public void removeCallListener(CallingListener callListener) {
            mCallClientService.removeCallListener(callListener);
        }

        public void stop() {
            cancelNotification();
            if (mCallClientService != null) {
                mCallClientService.stop();
            }
            CallingService.this.stopSelf();
        }

        public void logout() {
            if (isCallInProgressNow()) {
                endCallManually();
            }
            stop();
            if (mCallClientService != null) {
                mCallClientService.logout();
            }
        }

        @Override
        public void dispatchCallNotification(CallActionType actionType,
                                             CallType callType,
                                             AbsCallClient.NotificationData data,
                                             String title,
                                             String userName,
                                             String groupId,
                                             boolean isCaller) {


            CallNotificationHelper.startCallInfoActionNotification(getApplicationContext(),
                    actionType,
                    callType,
                    data,
                    title,
                    userName,
                    groupId,
                    isCaller);
        }

        @Override
        public void cancelNotification() {
            CallNotificationHelper.cancelCallInfoNotification(getApplicationContext());
        }

        @Override
        public void vibrate() {
            CallHelper.vibrate(getApplicationContext());
        }

        public void startIncomingCall(ViewGroup rootViewOfActivity,
                                      IncomingCallDataHolder incomingCallData,
                                      boolean isIncomingCallFromNotification,
                                      CallStateListener listener) {
            mCallStateListener = listener;
            bindCallViewToActivity(rootViewOfActivity);
            mCallScreenView.startIncomingCall(rootViewOfActivity.getContext(), incomingCallData, isIncomingCallFromNotification);
        }

        public void startOutGoingCall(ViewGroup rootViewOfActivity,
                                      CallType callType,
                                      User callTo,
                                      UserGroup group,
                                      CallStateListener listener) {
            mCallStateListener = listener;
            bindCallViewToActivity(rootViewOfActivity);
            mCallScreenView.startOutGoingCall(rootViewOfActivity.getContext(), callType, callTo, group);
        }

        public void startJoinGroupCall(ViewGroup rootViewOfActivity,
                                       CallType callType,
                                       UserGroup group,
                                       CallStateListener listener) {
            mCallStateListener = listener;
            bindCallViewToActivity(rootViewOfActivity);
            mCallScreenView.startJoinGroupCall(rootViewOfActivity.getContext(), callType, group);
        }

        public void checkToStopIncomingToneWhenDeviceButtonPressed() {
            if (mCallScreenView != null) {
                mCallScreenView.checkToStopIncomingToneWhenDeviceButtonPressed();
            }
        }

        public void performAnswerCall() {
            if (mCallScreenView != null) {
                mCallScreenView.performAnswerCall();
            }
        }

        public void handleNotificationAction(AbsCallService.CallNotificationActionType type, String groupId) {
            if (mCallScreenView != null) {
                mCallScreenView.handleNotificationAction(type, groupId);
            }
        }

        public boolean isShowingIncomingCall() {
            if (mCallScreenView != null) {
                return mCallScreenView.isShowingIncomingCall();
            }

            return false;
        }

        public void endCallManually() {
            if (mCallScreenView != null) {
                mCallScreenView.endCallManually();
            }
        }

        public boolean isCallViewInFloatingMode() {
            return mIsInFloatingMode;
        }

        public boolean isCallInProgressNow() {
            return mCallClientService != null && mCallClientService.isCallInProgress();
        }

        public void enableFloatingCallWindow(AppCompatActivity appCompatActivity) {
            enableFloatingCallView(appCompatActivity);
        }

        public void resizeBackToFullScreen(ViewGroup activityRootView, CallingService.CallStateListener listener) {
            mCallStateListener = listener;
            getHandler().post(() -> {
                mCallScreenView.onFloatingWindowEnable(false);
                removeFloatingCallView();
                getHandler().postDelayed(() -> {
                    Timber.i("resizeBackToFullScreen");
                    bindCallViewToActivity(activityRootView);
                    getHandler().postDelayed(() -> mCallScreenView.adaptViewBackToFullScreen(), 100);
                }, 100);
            });
        }

        public void clearCallStateListener() {
            mCallStateListener = null;
        }
    }

    public interface CallStateListener {
        void onCallEnded();

        void onMessageClicked(ChatIntent chatIntent);
    }
}
