package com.proapp.sompom.chat.service;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.neovisionaries.ws.client.WebSocketState;
import com.proapp.sompom.BuildConfig;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.broadcast.AbsService;
import com.proapp.sompom.chat.AbsChatBinder;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.chat.call.CallMessageHelper;
import com.proapp.sompom.chat.call.agora.VOIPCallMessageListener;
import com.proapp.sompom.chat.listener.ChannelListener;
import com.proapp.sompom.chat.listener.ChatListener;
import com.proapp.sompom.chat.listener.GlobalChatListener;
import com.proapp.sompom.chat.listener.LiveUserDataListener;
import com.proapp.sompom.chat.listener.ServiceStateListener;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.database.LinkPreviewDb;
import com.proapp.sompom.database.UserDb;
import com.proapp.sompom.helper.ConversationHelper;
import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.listener.OnBadgeUpdateListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnChatSocketListener;
import com.proapp.sompom.model.LiveUseProfileData;
import com.proapp.sompom.model.emun.Presence;
import com.proapp.sompom.model.emun.SegmentedControlItem;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.CheckWorkingHourResponse;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.model.result.SocketError;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.GenerateLinkPreviewUtil;
import com.proapp.sompom.utils.NotificationUtils;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.newviewmodel.ChatDialogViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

public class SocketService extends AbsService {

    private static final long KEEP_ALIVE_SOCKET_FRAME_INTERVAL = 10000L;

    private final SocketBinder mBinder = new SocketBinder();
    @Inject
    public ApiService mApiService;
    private ChatSocket mSocket;
    private TypingIndicator mTypingIndicator;
    private SocketConnectionChecker mSocketConnectionChecker;

    public static Intent getReplyChatIntent(Context context,
                                            Chat chat) {
        Intent intent = new Intent(context, SocketService.class);
        intent.setAction(RemoteNotificationBroadcast.REPLY_ACTION);
        intent.putExtra(SharedPrefUtils.DATA, chat);
        return intent;
    }

    public SocketBinder getBinder() {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        Timber.e("onStartCommand " + intent + " " + startId + " " + hashCode());
        if (intent != null) { //intent == null, mean app is destroyed
            if (TextUtils.equals(intent.getAction(), RemoteNotificationBroadcast.REPLY_ACTION)) {
                handleActionReply(intent);
            }
        }
        return START_STICKY;
    }

    private void handleActionReply(Intent intent) {
        boolean appCompletelyClosed = isAppCompletelyClosed();
        Timber.i("handleActionReply: appCompletelyClosed: " + appCompletelyClosed);
        final Chat chat = intent.getParcelableExtra(SharedPrefUtils.DATA);
        if (mSocket == null || !mSocket.isconnected()) {
            mBinder.startService(null);
            mBinder.startSubscribeAllGroupChannel();
        }
        /*
         **Must be called before sending reply message.
         */
        if (chat != null) {
            ConversationHelper.saveLastLocalMessageId(this, chat.getChannelId(), false);
        }
        sendMessageViaNotification(chat, appCompletelyClosed);
    }

    private void sendMessageViaNotification(Chat chat, boolean shouldDisconnectSocketAfterSending) {
        if (mSocket != null && chat != null) {
            String url = GenerateLinkPreviewUtil.getFirstUrlIndex(chat.getContent());
            if (!TextUtils.isEmpty(url)) {
                requestLinkPreview(chat, url, previewModel -> {
                    chat.setMetaPreview(Collections.singletonList(previewModel));
                    mSocket.sendReply(chat, shouldDisconnectSocketAfterSending);
                });
            } else {
                mSocket.sendReply(chat, shouldDisconnectSocketAfterSending);
            }
            NotificationUtils.cancelNotificationByLocalId(getApplicationContext(), chat.getChannelId());
        }
    }

    private boolean isAppCompletelyClosed() {
        return MainApplication.isAppInBackground() && !MainApplication.isIsHomeScreenOpened();
    }

    private void requestLinkPreview(Chat chat, String url, ChatDialogViewModel.LinkPreviewCallback listener) {
        Observable<Response<LinkPreviewModel>> call = LinkPreviewRetriever.getPreviewLink(getApplicationContext(),
                mApiService,
                url);
        ResponseObserverHelper<Response<LinkPreviewModel>> helper = new ResponseObserverHelper<>(getApplicationContext(), call);
        helper.execute(new OnCallbackListener<Response<LinkPreviewModel>>() {
            @Override
            public void onComplete(Response<LinkPreviewModel> result) {
                Timber.i("requestLinkPreview: onComplete: " + new Gson().toJson(result.body()));
                LinkPreviewModel linkPreviewModel = result.body();
                if (linkPreviewModel != null && linkPreviewModel.isValidPreviewData()) {
                    linkPreviewModel.setId(chat.getId());
                    linkPreviewModel.setResourceContent(chat.getContent());
                    linkPreviewModel.setLinkDownloaded(true);
                    LinkPreviewDb.save(getApplicationContext(), linkPreviewModel, true);
                }
                if (listener != null) {
                    listener.onGetLinkPreviewFinished(linkPreviewModel);
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e("requestLinkPreview: onFail: " + ex.getMessage());
                if (listener != null) {
                    listener.onGetLinkPreviewFinished(null);
                }
            }
        });
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class SocketBinder extends AbsChatBinder {

        final LiveUserDataService mLiveUserDataService = new LiveUserDataService();

        public void getGroupLastActiveInfo(List<User> participants, LiveUserDataService.LiveUserDataServiceCallback callback) {
            mLiveUserDataService.getGroupLastActiveInfo(participants, callback);
        }

        public ChatSocket getChatSocket() {
            return mSocket;
        }

        @Override
        public void startService(ServiceStateListener listener) {
            if (!SharedPrefUtils.isLogin(getApplicationContext())) {
                return;
            }

            if (mSocket != null) {
                Timber.i("startService with existing socket: isconnected: " + mSocket.isconnected());
                if (mSocket.isconnected()) {
                    listener.onServiceStart();
                    return;
                } else {
                    Timber.i("Will clear the existing socket and create new");
                    mSocket.removeAllCallbacks();
                    mSocket.clearSubscribeListener();
                    mSocket.disconnect("startService and exist socket is not connected");
                    mSocket = null;
                }
            }
            if (mApiService == null) {
                getControllerComponent().inject(SocketService.this);
            }
            mSocket = new ChatSocket(getApplicationContext(), SocketService.this, listener, mApiService);
            mSocketConnectionChecker = new SocketConnectionChecker(() -> {
                /*
                   There is an issue that on some devices socket service is not working anymore after
                   some specific time periods like 30 minutes to 2 hours which lead to not receiving message
                   nor sending message. The cause of this issue was not sure and as searched some online
                   solutions, it appeared they faced the same issue. The main problem is that, the socket
                   still keep connected but seem getting stuck and sending and receiving until we
                   manually reconnect the socket service. So as to fix this issue, we have to leverage the
                   sending empty frame of the socket library which supposed to be sent from client in the
                   interval of 7 to 8 seconds for each message. So in the case that interval is bigger than that and the socket
                   connect is still connected, we consider that this issue is presenting and we will manage to
                   reconnect the socket manually.
                 */
                if (mSocket.getLastKeepAliveFrameMillisecond() > 0) {
                    long diff = System.currentTimeMillis() - mSocket.getLastKeepAliveFrameMillisecond();
//                    Timber.i("onShouldCheckConnection: Keep alive socket frame interval: " + diff);
                    if (diff > KEEP_ALIVE_SOCKET_FRAME_INTERVAL &&
                            mSocket.getCurrentState() != WebSocketState.CONNECTING &&
                            mSocket.isconnected()) {
                        Timber.i("Will reconnect socket service since keep alive socket frame is bigger than expected value.");
                        mSocket.connectAsync();
                    }
                }
            });
            mSocket.resetLastKeepAliveFrameMillisecond();
            mSocketConnectionChecker.startTimer();
            if (!BuildConfig.DEBUG) {
                mSocket.disableLogging();
            }
            mSocket.connectAsync();
            mSocket.startListener(new SocketListener());
            mLiveUserDataService.clearAllInfo();
        }

        public void checkToReconnectSocketIfNecessary() {
            Timber.i("checkToReconnectSocketIfNecessary");
            if (mSocket != null && !mSocket.isconnected()) {
                Timber.i("Will reconnect the socket");
                mSocket.connectAsync();
            }
        }

        public boolean isSocketConnected() {
            return mSocket.isconnected();
        }

        @Override
        public void clearServiceStateListener(ServiceStateListener listener) {
            mSocket.clearServiceStateListener(listener);
        }

        @Override
        public void startSubscribeAllGroupChannel() {
            mSocket.subscribeToAllGroupChannels();
        }

        @Override
        public void startSubscribeAllGroupChannel(List<String> channelIds) {
            mSocket.subscribeToAllGroupChannels(channelIds);
        }

        @Override
        public void checkToSubscribeOrUnsubscribeGroupConversation(String groupChannelId, boolean isRemoved) {
            mSocket.checkToSubscribeOrUnsubscribeGroupConversation(groupChannelId, isRemoved);
        }

        /*
            This method must be called in the following case:
            When app is already opened and Socket service is alive, but at some points, its connection
            is lost or connecting. In this case we will call this method to make the performance like
            we are receiving message from Socket because we have everything handle with that message.
            So we just call to reuse this method by supplying chat from remote notification source.
         */
        public void performReceiveSocketMessageManuallyForChatFromNotification(Chat chat) {
            mSocket.performReceiveSocketMessageManuallyForChatFromNotification(chat);
        }

        public void disconnect() {
            if (mSocket != null) {
                mSocket.disconnect("Stop socket service for test.");
            }
        }

        public void connect() {
            if (mSocket != null) {
                mSocket.connectAsync();
            }
        }

        @Override
        public void stopService() {
            if (mSocketConnectionChecker != null) {
                mSocketConnectionChecker.stopTimer();
            }
            if (mSocket != null) {
                mSocket.disconnect("Stop socket service");
            }
            mLiveUserDataService.clearAllInfo();
            getChannelListener().clear();
            getChatListener().clear();
            getLiveUserDataListeners().clear();
            getGlobalChatListeners().clear();
        }

        @Override
        public void sendMessage(User user, Chat message) {
            if (mSocket != null) {
                Chat newChat = new Chat(message);
                boolean canSend = mSocket.sendTo(message, true);
                if (canSend) {
                    final Conversation con = ConversationDb.updateLastMessageOfConversation(getApplicationContext(),
                            newChat,
                            true,
                            true);
                    if (con != null) {
                        onChannelCreateOrUpdate(con, "sendMessage");
                    }
                }
            }
        }

        @Override
        public void sendVOIPCallMessage(Chat message, ChatSocket.SendMessageCallback callback) {
            mSocket.publishVOIPCallMessage(message, callback);
        }

        @Override
        public ChatSocket.SendMessageCallback getNewPublishMessageCallbackInstance(Chat newChat, ChatSocket.PublishMessageCallback callback) {
            return mSocket.getNewPublishMessageCallbackInstance(newChat, callback);
        }

        public void sendGroupChat(Chat messagesToSend, Conversation conversation) {
            boolean canSend = mSocket.sendTo(messagesToSend, true);
            if (canSend) {
                final Conversation con = ConversationDb.updateLastMessageOfConversation(getApplicationContext(),
                        messagesToSend,
                        true,
                        true);
                onChannelCreateOrUpdate(con, "sendGroupChat");
            }
        }

        public void checkToSendDeliveredMessage(Chat receivedChat, boolean shouldDisconnectSocketAfterSend) {
            if (mSocket == null || !mSocket.isconnected()) {
                mBinder.startService(new ServiceStateListener() {
                    @Override
                    public void onServiceStart() {
                        mSocket.checkToSendDeliveredMessage(receivedChat, shouldDisconnectSocketAfterSend, false);
                    }

                    @Override
                    public void onServiceFail(Exception ex) {

                    }
                });
            }
        }

        public void onChannelCreateOrUpdate(@Nullable Conversation conversation, String tag) {
            Timber.i("onChannelCreateOrUpdate from: " + tag);
            if (conversation == null) {
                return;
            }
            SegmentedControlItem item;

            if (!TextUtils.isEmpty(conversation.getGroupId())) {
                //Group
                item = SegmentedControlItem.Buying; //=> Replace by group
            } else {
                item = SegmentedControlItem.Message;
            }

            for (ChannelListener channelListener : getChannelListener()) {
                if (channelListener.getCurrentSegmentedControlItem() == SegmentedControlItem.All ||
                        channelListener.getCurrentSegmentedControlItem() == item) {
                    channelListener.onChannelCreateOrUpdate(conversation);
                }
            }
        }

        @Override
        public void startTyping(boolean isGroupChat, String recipientUserId, String productId, String channelID) {
            if (mTypingIndicator == null) {
                mTypingIndicator = new TypingIndicator((sendTo,
                                                        isGroup,
                                                        productId1,
                                                        channelId,
                                                        isTyping) -> {
                    if (mSocket == null) {
                        return;
                    }

                    //One to one conversation
                    mSocket.startTyping(isGroup, sendTo.get(0), productId1, channelId, isTyping);
                });
            }
            //Must call this method to update group status before calling to typing service.
            mTypingIndicator.setGroupChatting(isGroupChat);
            mTypingIndicator.startTyping(recipientUserId, productId, channelID);
        }

        @Override
        public void stopTyping(String recipientUserId, String productId, String channelID) {
            if (mTypingIndicator != null) {
                mTypingIndicator.stopTyping();
            }
        }

        @Override
        public void removeMessage(Chat chat) {
            if (mSocket != null) {
                mSocket.deleteChat(chat);
            }
        }

        @Override
        public void onResume(String conversationId) {
            Timber.i("onResume " + conversationId + ", mSocket.isconnected(): " + (mSocket != null ? mSocket.isconnected() : "Null"));
            checkToSendReadStatusFromCurrentUser(conversationId);
        }

        public void checkToSendReadStatusFromCurrentUser(String conversationId) {
            if (mSocket != null) {
                mSocket.checkToSendReadStatusFromCurrentUser(conversationId, "Chat Screen Resume");
            }
        }

        public void sendReadStatusWithoutSavingToLocal(Chat chat, String conversationId) {
            if (mSocket != null) {
                mSocket.sendReadStatusWithoutSavingToLocal(chat, conversationId);
            }
        }

        @Override
        public void archivedConversation(String conversationId) {
            if (mSocket != null) {
                mSocket.archivedConversation(conversationId);
            }
        }

        @Override
        public void addLiveUserDataListener(LiveUserDataListener listener) {
            super.addLiveUserDataListener(listener);
            if (listener != null) {
                autoBroadcastLiveUserDataOnCallbackRegistration(listener.getUserId(), listener);
            }
        }

        private void autoBroadcastLiveUserDataOnCallbackRegistration(String userId, LiveUserDataListener listener) {
            LiveUseProfileData liveUseProfileData = mLiveUserDataService.getInfo(userId);
            if (liveUseProfileData != null && listener != null) {
//                Timber.i("broadcastLiveUserData for user: " + liveUseProfileData.getUser().getFullName() + ", presence: " + liveUseProfileData.getPresence());
                //Auto broadcast online status at callback registration
                listener.onPresenceUpdate(userId,
                        liveUseProfileData.getPresence(),
                        liveUseProfileData.getActivityDate());

                //Auto broadcast profile avatar at callback registration
                listener.onProfileAvatarUpdated(userId,
                        liveUseProfileData.getUserHash(),
                        liveUseProfileData.getUserProfileAvatar());
            }
        }

        public void setUserStatus(User user,
                                  boolean isFromReceivingChatSocket,
                                  boolean isTypingMessage,
                                  String tag) {
            if (user != null) {
                LiveUseProfileData liveUseProfileData = mLiveUserDataService.getInfo(user.getId());
//                if (shouldLog(user.getId())) {
//                    Timber.i("setUserStatus: user: " + user.getFullName() +
//                            ", tag: " + tag +
//                            ", user: " + user.isOnline() +
//                            ", date: " + user.getLastActivity() +
//                            ", hash: " + user.getHash() +
//                            ", user id: " + user.getId() +
//                            ", getUserProfile: " + user.getUserProfile() +
//                            ", isFromReceivingChatSocket: " + isFromReceivingChatSocket);
//                }
                if (liveUseProfileData == null) {
                    liveUseProfileData = new LiveUseProfileData();
                }

                /*
                    If current use receive a chat via Socket, that sender should mark as online too.
                    Will mark that user as online and copy last active date from existing user data.
                 */
                if (isFromReceivingChatSocket) {
                    user.setOnline(true);
                    if (isTypingMessage) {
                        /*
                            Because typing message server will not record as last active date of user,
                            therefore, we have to maintain the last active date existing in local.
                         */
                        user.setLastActivity(liveUseProfileData.getActivityDate());
                    }
                }

                /*
                    1. Check to update user and broadcast user online status change to all subscribers
                */
                //Check to update user presence status
                boolean shouldBroadcast = false;
                if (user.isOnline() != null && user.getLastActivity() != null) {
                    if (liveUseProfileData.getActivityDate() == null ||
                            liveUseProfileData.getPresence() == null ||
                            liveUseProfileData.getPresence() == Presence.Unknown) {
                        shouldBroadcast = true;
                    } else if (liveUseProfileData.getPresence() == Presence.Offline && user.isOnline()) {
                        shouldBroadcast = (user.getLastActivity().getTime() > liveUseProfileData.getActivityDate().getTime()) || isFromReceivingChatSocket;
                    } else if (user.getLastActivity().getTime() >= liveUseProfileData.getActivityDate().getTime()) {
                        shouldBroadcast = true;
                    }
                    if (shouldBroadcast) {
                        liveUseProfileData.setPresence(user.isOnline() ? Presence.Online : Presence.Offline);
                        liveUseProfileData.setActivityDate(user.getLastActivity());
                    }
                } else {
                    shouldBroadcast = true;
//                    Timber.i("Exist live user data: " + liveUseProfileData.getPresence() + ", Last active date: " + liveUseProfileData.getActivityDate());
                    if ((liveUseProfileData.getPresence() == null || liveUseProfileData.getPresence() == Presence.Unknown) &&
                            liveUseProfileData.getActivityDate() == null) {
                        liveUseProfileData.setPresence(Presence.Offline);
//                        if (shouldLog(user.getId())) {
//                            Timber.i("Completely got new created user status");
//                        }
                    }
                }
                if (shouldBroadcast) {
                    mLiveUserDataService.addInfo(user.getId(), liveUseProfileData);
                    //Delegate the action to all action subscribers
//                    if (shouldLog(user.getId())) {
//                        Timber.i("Will broadcast online status update of " + user.getFullName() + " to " + liveUseProfileData.getPresence());
//                    }
                    for (LiveUserDataListener liveUserDataListener : getLiveUserDataListeners()) {
                        if (TextUtils.equals(liveUserDataListener.getUserId(), user.getId())) {
                            liveUserDataListener.onPresenceUpdate(user.getId(),
                                    liveUseProfileData.getPresence(),
                                    liveUseProfileData.getActivityDate());
                        }
                    }
                }

                 /*
                  2. Check to update user and broadcast user profile avatar change to all subscribers
                 */
//                Timber.i("Current user: " + new Gson().toJson(liveUseProfileData.getUser()) + ", set user: " + new Gson().toJson(user));
                if (user.getHash() > liveUseProfileData.getUserHash()) {
//                    Timber.i("Will update user hash of " + user.getFullName());
                    UserDb.updateUserHash(getApplicationContext(), user);
                    liveUseProfileData.updateUserHash(user);
                    //Delegate the action to all action subscribers
                    for (LiveUserDataListener liveUserDataListener : getLiveUserDataListeners()) {
                        if (TextUtils.equals(liveUserDataListener.getUserId(), user.getId())) {
                            liveUserDataListener.onProfileAvatarUpdated(user.getId(),
                                    user.getHash(),
                                    user.getUserProfileThumbnail());
                        }
                    }
                }
            }
        }

        private boolean shouldLog(String userId) {
            return TextUtils.equals(userId, "608770a227a5aabc564962d6");
        }

        private class SocketListener implements OnChatSocketListener {

            private CallMessageHelper mCallMessageHelper;

            public SocketListener() {
                Timber.i("Init SocketListener");
            }

            private void checkWorkingHourForGroupIncomingCall(Chat chat) {
                Observable<Response<CheckWorkingHourResponse>> checkingWorkingHour = mApiService.getCheckingWorkingHour();
                ResponseObserverHelper<Response<CheckWorkingHourResponse>> helper = new ResponseObserverHelper<>(getApplicationContext(),
                        checkingWorkingHour);
                helper.execute(new OnCallbackListener<Response<CheckWorkingHourResponse>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                    }

                    @Override
                    public void onComplete(Response<CheckWorkingHourResponse> result) {
                        if (result.body() != null && result.body().isInWorkingHour()) {
                            checkOnReceiveVOIPCall(chat);
                        }
                    }
                });
            }

            private void checkOnReceiveVOIPCall(Chat chat) {
                if (CallHelper.isInComingVOIPCallMessageType(chat) &&
                        CallHelper.isDeviceInPhoneCallProcess(getApplicationContext())) {
                        /*
                        Will send busy status to caller.
                         */
                    if (mCallMessageHelper == null) {
                        mCallMessageHelper = new CallMessageHelper(getApplicationContext(),
                                mBinder,
                                SocketBinder.SocketListener.class.getSimpleName());
                    }
                    Chat chat1 = mCallMessageHelper.buildBusyVOIPCallMessage(getApplicationContext(),
                            chat.getCallData(),
                            SocketBinder.SocketListener.class.getSimpleName());
                    if (chat1 != null) {
                        sendVOIPCallMessage(chat1,
                                getNewPublishMessageCallbackInstance(chat,
                                        (channelName, error, ackChat) -> new Handler(getMainLooper()).postDelayed(() ->
                                                        mCallMessageHelper.sendCallAcceptedToCurrentCallAccount(chat.getCallData()),
                                                1000)));
                    }
                } else {
                    for (VOIPCallMessageListener listener : getVOIPCallMessageListeners()) {
                        listener.onReceiveVOIPCallMessage(chat);
                    }
                }
            }

            @Override
            public void onMessageReceive(Chat chat, String tag) {
                Timber.i("onMessageReceive: tag: " + tag + ",\nMessage: " + new Gson().toJson(chat));
                if (CallHelper.isInComingGroupVOIPCallMessageType(chat)) {
                    Timber.i("Will check if user in working hour.");
                    checkWorkingHourForGroupIncomingCall(chat);
                    return;
                }

                if (CallHelper.isVOIPCallMessage(chat)) {
                    //Record the rejected call status in the list.
                    if (CallHelper.isRejectVOIPCallMessageType(chat)) {
                        CallHelper.addToRejectedCallList(chat.getChannelId());
                    }
                    checkOnReceiveVOIPCall(chat);
                } else {
                    for (ChatListener chatListener : getChatListener()) {
                        if (TextUtils.equals(chatListener.getChannelId(), chat.getChannelId())) {
                            chatListener.onReceiveMessage(chat);
                        }
                    }
                }
            }

            @Override
            public void onReceiveUpdateSeenUserIdListUpdate(String conversationId, List<Chat> chats) {
                for (ChatListener chatListener : getChatListener()) {
                    if (TextUtils.equals(chatListener.getChannelId(), conversationId)) {
                        chatListener.onReceiveUpdateSeenUserIdListUpdate(conversationId, chats);
                    }
                }
            }

            @Override
            public void onRecipientTypingMessage(Chat chat) {
                for (ChatListener chatListener : getChatListener()) {
                    if (TextUtils.equals(chatListener.getChannelId(), chat.getChannelId())) {
                        chatListener.onRecipientTyping(chat);
                    }
                }
            }

            @Override
            public void onConversationCreateOrUpdate(Conversation conversation, String tag) {
                SocketBinder.this.onChannelCreateOrUpdate(conversation, tag);
            }

            @Override
            public void onConversationDelete(String conversationId) {
                for (ChannelListener channelListener : getChannelListener()) {
                    channelListener.onChannelRemoved(conversationId);
                }
            }

            @Override
            public void onMessageSent(Chat chat) {
                for (GlobalChatListener globalChatListener : mBinder.getGlobalChatListeners()) {
                    globalChatListener.onMessageUpdated(chat, MessageState.SENT);
                }
            }

            @Override
            public void onCallError(SocketError error) {
                for (VOIPCallMessageListener voipCallMessageListener : getVOIPCallMessageListeners()) {
                    voipCallMessageListener.onCallError(error);
                }
            }

            @Override
            public boolean isInBg(String channelId) {
                boolean isInBg = true;
                for (ChatListener chatListener : getChatListener()) {
                    isInBg = chatListener.isInBackground();
                    if (!isInBg && !TextUtils.equals(channelId, chatListener.getChannelId())) {
                        isInBg = true;
                    }
                }
                return isInBg;
            }

            @Override
            public boolean isInChatScreen(String channelId) {
                for (ChatListener chatListener : getChatListener()) {
                    if (TextUtils.equals(channelId, chatListener.getChannelId())) {
                        return chatListener.isChatScreen();
                    }
                }

                return false;
            }

            @Override
            public List<String> getCurrentChannel() {
                final List<String> channels = new ArrayList<>();
                for (ChatListener chatListener : getChatListener()) {
                    channels.add(chatListener.getChannelId());
                }
                return channels;
            }

            @Override
            public void onBadgeUpdate(int value) {
                for (OnBadgeUpdateListener badgeListener : getBadgeListeners()) {
                    badgeListener.onConversationBadgeUpdate(value);
                }
            }
        }
    }
}
