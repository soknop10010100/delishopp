package com.proapp.sompom.chat.call;

import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.chat.AbsChatBinder;
import com.proapp.sompom.chat.call.agora.VOIPCallMessageListener;
import com.proapp.sompom.chat.listener.ServiceStateListener;
import com.proapp.sompom.chat.service.ChatSocket;
import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.AudioPlayer;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.CustomServiceConnection;
import com.proapp.sompom.helper.DeviceHelper;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.model.NotificationChannelSetting;
import com.proapp.sompom.model.request.JoinChannelStatusUpdateBody;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.NotificationUtils;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 6/10/2020.
 */
public class CallServiceConnectionHelper {

    private Chat mCallMessage;
    private Context mContext;
    private AbsChatBinder mChatBinder;

    private CallingService.CallServiceBinder mCallBinder;
    private BroadcastReceiver mCallNotificationRejectReceiver;
    private CallTimerHelper mCallTimerHelper;
    private CallMessageHelper mCallMessageHelper;
    private CallServiceConnectionListener mServiceListener;
    private boolean mShowNotificationDirectly;
    private boolean mDisplayingCallNotification;
    private AudioPlayer mCallTonePlayer;
    private BroadcastReceiver mJoinGroupCallStatusReceiver;
    private List<String> mShouldIgnoreIncomingCallIds = new ArrayList<>();
    private CustomServiceConnection mSocketServiceConnection;
    private CustomServiceConnection mCallServiceServiceConnection;
    private ServiceStateListener mChatSocketStartServiceListener;
    private AbsCallClient.ClientInitializeCallback mCallClientInitializeCallback;
    private boolean mIsServiceStart;

    public CallServiceConnectionHelper(Context context) {
        mContext = context;
    }

    public void startService(Chat callMessage,
                             boolean showNotificationDirectly,
                             CallServiceConnectionListener listener) {
        mServiceListener = listener;
        mShowNotificationDirectly = showNotificationDirectly;
        mCallMessage = callMessage;
        initCallServiceConnection();
        if (SharedPrefUtils.isLogin(mContext.getApplicationContext())) {
            //Try to unbind all previous service connections
            unBindAllServiceConnections();
            mContext.bindService(new Intent(mContext.getApplicationContext(),
                            CallingService.class),
                    mCallServiceServiceConnection,
                    Context.BIND_AUTO_CREATE);
        }
        mIsServiceStart = true;
    }

    private void initCallServiceConnection() {
        mCallServiceServiceConnection = new CustomServiceConnection() {
            @Override
            public void onCustomServiceConnected(ComponentName name, IBinder service) {
                Timber.i("onServiceConnected");
                if (mCallMessage.getCallData() != null) {
                    mCallBinder = (CallingService.CallServiceBinder) service;
                    if (mCallClientInitializeCallback == null) {
                        mCallClientInitializeCallback = new AbsCallClient.ClientInitializeCallback() {
                            @Override
                            public void onClientConnected(AbsCallService.CallType callType) {
                                Timber.i("Call onClientConnected: mShowNotificationDirectly: " + mShowNotificationDirectly + ", ");
                                ((CallingService.CallServiceBinder) service).addCallListener(new AbsCallService.CallingListener() {
                                    @Override
                                    public void onCallEnd(AbsCallService.CallType callType,
                                                          String channelId, boolean isGroup, User recipient,
                                                          boolean isMissedCall,
                                                          int calledDuration,
                                                          boolean isEndedByClickAction,
                                                          boolean isCauseByTimeOut,
                                                          boolean isCausedByParticipantOffline) {
                                        CallNotificationHelper.cancelInComingCallNotification(mContext);
                                    }

                                    @Override
                                    public void onIncomingCall(AbsCallService.CallType calType,
                                                               Map<String, Object> data1,
                                                               Map<String, Object> data2) {
                                        Timber.i("onIncomingCall: " + new Gson().toJson(data1));

                                    }

                                    @Override
                                    public void onCallEstablished(AbsCallService.CallType callType, String channelId, boolean isGroup) {
                                        Timber.i("onCallEstablished");
                                        if (mCallTimerHelper != null) {
                                            mCallTimerHelper.cancel();
                                        }
                                        DeviceHelper.stopVibration(mContext);
                                        CallNotificationHelper.cancelInComingCallNotification(mContext);
                                        unregisterCallNotificationRejectReceiver(true);
                                    }

                                    @Override
                                    public void onCallInfoUpdated(AbsCallService.CallType callType,
                                                                  String info,
                                                                  boolean isCallProfileVisible,
                                                                  boolean showCallControls) {

                                    }

                                    @Override
                                    public void onCheckShowingRequestVideoCamera(boolean shouldShow,
                                                                                 AbsCallService.CallType callType,
                                                                                 User requester,
                                                                                 boolean isGroupCall) {

                                    }

                                    @Override
                                    public void onCallTimerUp(AbsCallService.CallType callType,
                                                              CallTimerHelper.TimerType timerType,
                                                              CallTimerHelper.StarterTimerType starterTimerType) {

                                    }
                                });
                                bindChatSocketService();
                                if (mShowNotificationDirectly && android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                    if (!isIncomingCallInIgnoreList(mCallMessage)) {
                                        showIncomingCallNotification();
                                    } else {
                                        removeIgnoreIncomingCall(mCallMessage);
                                        cancelTimer();
                                        onRejectVOIPCallMessage(false, false);
                                    }
                                } else if (mShowNotificationDirectly && !isInForeground()) {
                                    Timber.i("Open incoming call screen directly from call service.");
                                    mContext.getApplicationContext().startActivity(getIncomingCallIntent());
                                }
                            }

                            @Override
                            public void onClientDisconnected(AbsCallService.CallType callType) {

                            }

                            @Override
                            public void onCallTypeUpdated(AbsCallService.CallType callType) {

                            }

                            @Override
                            public void onInitError(AbsCallService.CallType callType, Throwable ex) {

                            }
                        };
                    }
                    ((CallingService.CallServiceBinder) service)
                            .start(SharedPrefUtils.getUserId(mContext.getApplicationContext()),
                                    mCallClientInitializeCallback);
                }
            }

            @Override
            public void onCustomServiceDisconnected(ComponentName name) {
                Timber.e("onServiceDisconnected: " + name.toShortString());
                if (mCallTimerHelper != null) {
                    mCallTimerHelper.cancel();
                }
                DeviceHelper.stopVibration(mContext);
                unregisterCallNotificationRejectReceiver(true);
            }
        };
    }

    private Intent getIncomingCallIntent() {
        IncomingCallDataHolder callVo = new IncomingCallDataHolder();
        callVo.setBackground(true);
        callVo.setCallType(mCallMessage.getSendingTypeAsString());
        callVo.setCallData(mCallMessage.getCallData());
        return new CallingIntent(mContext.getApplicationContext(), callVo);
    }

    public boolean isDisplayingCallNotification() {
        return mDisplayingCallNotification;
    }

    public boolean isChatSocketBound() {
        return mChatBinder != null;
    }


    public Chat getCallMessage() {
        return mCallMessage;
    }

    private void stopInComingCallTone() {
        if (mCallTonePlayer != null) {
            mCallTonePlayer.stopContactingOrIncomingTone();
        }
    }

    public void addIgnoreIncomingCall(Chat callMessage) {
        mShouldIgnoreIncomingCallIds.add(callMessage.getChannelId());
    }

    public void removeIgnoreIncomingCall(Chat callMessage) {
        mShouldIgnoreIncomingCallIds.remove(callMessage.getChannelId());
    }

    public boolean isIncomingCallInIgnoreList(Chat callMessage) {
        return mShouldIgnoreIncomingCallIds.contains(callMessage.getChannelId());
    }

    private void playIncomingCallTone() {
        if (mCallTonePlayer == null) {
            mCallTonePlayer = new AudioPlayer(mContext);
        }
        mCallTonePlayer.playIncomingCallTone(false);
    }

    private void checkToUnbindAllServiceConnection() {
        try {
            if (mCallServiceServiceConnection != null && mCallServiceServiceConnection.isConnected()) {
                Timber.i("Unbind mCallServiceServiceConnection");
                mContext.unbindService(mCallServiceServiceConnection);
            }
            if (mSocketServiceConnection != null && mSocketServiceConnection.isConnected()) {
                Timber.i("Unbind mSocketServiceConnection");
                mContext.unbindService(mSocketServiceConnection);
            }
        } catch (Exception ex) {
            Timber.e("Unbind all service connections. Error: " + ex.getMessage());
            SentryHelper.logSentryError(ex);
        }
    }

    public void unBindAllServiceConnections() {
        try {
            if (mCallMessage != null) {
                NotificationUtils.cancelNotificationByLocalId(mContext, mCallMessage.getChannelId());
            }
            stopInComingCallTone();
            checkToUnbindAllServiceConnection();
            if (mChatBinder != null) {
                mChatBinder.clearServiceStateListener(mChatSocketStartServiceListener);
            }
            mIsServiceStart = false;
            Timber.i("unBindAllServiceConnections");
        } catch (Exception ex) {
            Timber.i("unBindAllServiceConnections: " + ex.getMessage());
            SentryHelper.logSentryError(ex);
        }
    }

    private void registerCallNotificationRejectReceiver() {
        mCallNotificationRejectReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive call notification reject.");
                if (mCallTimerHelper != null) {
                    mCallTimerHelper.cancel();
                }
                DeviceHelper.stopVibration(mContext);
                onRejectVOIPCallMessage(true, true);
            }
        };
        Timber.i("registerCallNotificationRejectReceiver");
        mContext.registerReceiver(mCallNotificationRejectReceiver,
                new IntentFilter(CallNotificationHelper.CALL_DECLINED_ACTION));
    }

    private void registerUpdateJoinGroupCallStatusReceiver() {
        mJoinGroupCallStatusReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                JoinChannelStatusUpdateBody data = intent.getParcelableExtra(SharedPrefUtils.DATA);
                if (data != null) {
                      /*
                        Use case for only incoming group call:
                        When current user receive a group incoming call, but then there is notification
                        from server indicated that this group call has been ended. So current group incoming call
                        screen must be ended too.
                    */
                    Timber.i("onReceive: JoinGroupCallStatusReceiver: " + new Gson().toJson(data));
                    if (mCallMessage != null && mCallMessage.isGroup() &&
                            TextUtils.equals(data.getConversationId(), mCallMessage.getChannelId()) &&
                            !data.getChannelOpen()) {
                        Timber.i("Will end group incoming call notification manually for this group call has been ended.");
                        CallNotificationHelper.cancelInComingCallNotification(mContext);
                        if (mCallTimerHelper != null) {
                            mCallTimerHelper.cancel();
                        }
                        DeviceHelper.stopVibration(mContext);
                        onRejectVOIPCallMessage(false, true);
                    }
                }
            }
        };
        mContext.registerReceiver(mJoinGroupCallStatusReceiver,
                new IntentFilter(ChatHelper.JOIN_GROUP_CALL_STATUS_UPDATE));
    }

    public void cancelTimer() {
        if (mCallTimerHelper != null) {
            mCallTimerHelper.cancel();
        }
        DeviceHelper.stopVibration(mContext);
    }

    public void onRejectVOIPCallMessage(boolean shouldSendMessage, boolean shouldClearCallAndSocketService) {
        Timber.i("onRejectVOIPCallMessage: shouldClearCallAndSocketService: " + shouldClearCallAndSocketService);
        stopInComingCallTone();
        CallHelper.clearRejectedCallList();
        mDisplayingCallNotification = false;
        CallNotificationHelper.cancelInComingCallNotification(mContext);

        //Send reject call message via Chat Socket.
        if (shouldSendMessage && mCallMessage.getCallData()
                != null && mChatBinder != null) {
            User caller = CallHelper.getCaller(mCallMessage.getCallData());
            if (caller != null) {
                Chat chat = mCallMessageHelper.buildRejectVOIPCallMessage(mContext, mCallMessage.getCallData(), caller.getId());
                if (chat != null) {
                    //Send back the reject message to the original caller or group that current user reject the call.
                    //And it is the same for 1x1 and group call. We will send the reject directly to the caller.
                    mChatBinder.sendVOIPCallMessage(chat, mChatBinder.getNewPublishMessageCallbackInstance(chat, new ChatSocket.PublishMessageCallback() {
                        @Override
                        public void onPublishedFinished(String channelName, Object error, Chat ackChat) {
                            Timber.i("onPublishedFinished");
                            if (shouldClearCallAndSocketService) {
                                clearAllChatAndCallServices();
                            }
                        }
                    }));
                }
            }
        } else {
            if (shouldClearCallAndSocketService) {
                clearAllChatAndCallServices();
            }
        }

        unregisterCallNotificationRejectReceiver(true);
    }

    private void clearAllChatAndCallServices() {
        //Just make sure the app is not yet opened
        if (!MainApplication.isIsHomeScreenOpened()) {
            if (mChatBinder != null) {
                mChatBinder.stopService();
            }
            if (mCallBinder != null) {
                mCallBinder.stop();
            }
            Timber.i("clearAllChatAndCallServices");
        }
    }

    private void initCallTimer() {
        mCallTimerHelper = new CallTimerHelper((timerType, starterTimerType, isGroup, recipientId, groupId) -> {
            Timber.i("onCallTimerUp: " + timerType + ", starterTimerType: " + starterTimerType);
            onRejectVOIPCallMessage(true, true);
        });
    }

    private void unregisterCallNotificationRejectReceiver(boolean unbindService) {
        if (mContext != null) {
            Timber.i("unregisterCallNotificationRejectReceiver");
            try {
                // Make sure the broadcastReceiver are unregistered and cleared, as to avoid
                // use-case where the broadcastReceiver was already unregistered, but someone tried
                // to unregister it a second time
                if (mCallNotificationRejectReceiver != null) {
                    mContext.unregisterReceiver(mCallNotificationRejectReceiver);
                    mCallNotificationRejectReceiver = null;
                }
                if (mJoinGroupCallStatusReceiver != null) {
                    mContext.unregisterReceiver(mJoinGroupCallStatusReceiver);
                    mJoinGroupCallStatusReceiver = null;
                }
                if (unbindService) {
                    CallServiceConnectionProvider.getInstance().unbindAllServiceConnection();
                }
            } catch (Exception ex) {
                Timber.e("Unregister receiver error: " + ex.getMessage());
                SentryHelper.logSentryError(ex);
            }
        }
    }

    private void bindChatSocketService() {
        /*
        Need to bind chat socket service for in case that user reject the call via notification, we
        have to broadcast the reject call message over ChatSocket.
         */
        if (mSocketServiceConnection == null) {
            mSocketServiceConnection = new CustomServiceConnection() {
                @Override
                public void onCustomServiceConnected(ComponentName name, IBinder binder) {
                    mChatBinder = (AbsChatBinder) binder;
                    Timber.i("Socket: onServiceConnected : " + mChatBinder.hashCode() + ", size: " + mChatBinder.getVOIPCallMessageListeners().size());
                    if (mCallBinder != null) {
                        mCallBinder.setChatBinder(mChatBinder);
                    }

                    if (mChatSocketStartServiceListener == null) {
                        mChatSocketStartServiceListener = new ServiceStateListener() {
                            @Override
                            public void onServiceStart() {
                                Timber.i("Chat onServiceStart");
                                mCallMessageHelper = new CallMessageHelper(mContext, mChatBinder, "bindChatSocketService");
                                mChatBinder.addCallVOIPMessageListeners(new VOIPCallMessageListener() {
                                    @Override
                                    public void onReceiveVOIPCallMessage(Chat message) {
                                        if (!mIsServiceStart) {
                                            return;
                                        }

                                        Timber.i("onReceiveVOIPCallMessage: " + new Gson().toJson(message));
//                            Timber.i("mCallMessage: " + new Gson().toJson(mCallMessage));
                                        String channelId = mCallMessageHelper.getChannelId(mCallMessage.getCallData(),
                                                mCallMessage.getSenderId());
//                            Timber.i("channelId: " + channelId);
                                        if (TextUtils.equals(message.getChannelId(), channelId)) {
                                            if (!MainApplication.isIsCallingScreenOpened() &&
                                                    CallHelper.isCallAcceptedVOIPCallMessageType(message)) {
                                                Timber.i("Got call accepted within current account from another deice.");
                                                mCallTimerHelper.cancel();
                                                DeviceHelper.stopVibration(mContext);
                                                onRejectVOIPCallMessage(false, true);
                                            } else if (CallHelper.isRejectVOIPCallMessageType(message) &&
                                                    !TextUtils.equals(message.getSenderId(), SharedPrefUtils.getUserId(mContext))) {
                                                Timber.i("Got cancel call from the caller while showing incoming call notification.");
                                                mCallTimerHelper.cancel();
                                                DeviceHelper.stopVibration(mContext);
                                                onRejectVOIPCallMessage(false, true);
                                            }
                                        }
                                    }
                                });
                                if (mServiceListener != null) {
                                    mServiceListener.onServiceReady();
                                }
                                mChatBinder.startSubscribeAllGroupChannel();
                                if (mIsServiceStart && mDisplayingCallNotification) {
                                    if (mCallMessageHelper != null) {
                                        mCallMessageHelper.sendRingingStatusToCaller(mCallMessage.getCallData());
                                    }
                                }
                            }

                            @Override
                            public void onServiceFail(Exception ex) {
                                Timber.e("Chat onServiceFail %s", ex.toString());
                            }
                        };
                    }
                    mChatBinder.startService(mChatSocketStartServiceListener);
                }

                @Override
                public void onCustomServiceDisconnected(ComponentName name) {
                    Timber.e("Socket: onServiceDisconnected");
                }
            };
        }
        mContext.bindService(new Intent(mContext.getApplicationContext(),
                        SocketService.class),
                mSocketServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    private void showIncomingCallNotification() {
        boolean notificationChannelEnable = DeviceHelper.isNotificationChannelEnable(mContext,
                NotificationChannelSetting.IncomingCall.getLatestNotificationChannelId());
        Timber.i("showIncomingCallNotification: isNotificationChannelEnable: " + notificationChannelEnable);
        initCallTimer();
        registerCallNotificationRejectReceiver();
        registerUpdateJoinGroupCallStatusReceiver();
        if (notificationChannelEnable) {
            playIncomingCallTone();
            DeviceHelper.performVibration(mContext);
        }
        CallNotificationHelper.showIncomingCallNotification(mContext,
                mCallMessage.getCallData(),
                AbsCallService.CallType.getFromValue(mCallMessage.getSendingTypeAsString()),
                getIncomingCallIntent());
        new Handler(mContext.getMainLooper()).post(() -> {
            // UI work
            //Need to run in UI Thread. Otherwise, the timer will not be running.
            mCallTimerHelper.startOneToOneTimer(CallTimerHelper.TimerType.CALL,
                    CallTimerHelper.StarterTimerType.RECIPIENT,
                    null);
        });
        mDisplayingCallNotification = true;
    }

    public void sendUserBusyVOIPCallMessage(Map<String, Object> callData) {
        Timber.i("sendUserBusyVOIPCallMessage");
        if (mCallMessageHelper != null) {
            Chat chat = mCallMessageHelper.buildBusyVOIPCallMessage(mContext,
                    callData,
                    CallServiceConnectionHelper.class.getSimpleName());
            if (chat != null) {
                mChatBinder.sendVOIPCallMessage(chat,
                        mChatBinder.getNewPublishMessageCallbackInstance(chat,
                                (channelName, error, ackChat) -> new Handler(mContext.getMainLooper()).postDelayed(() -> {
                                            /*
                                            We manage to send call accept to same account on other devices of incoming call
                                            after sending call busy to that caller due to following reason:
                                            1. The case is managing for same account synchronization as following case:
                                               - User 'A' login in both device 1 and 2, device 1 is tyring to call another user, 'B', so
                                               technically user 'A' on device 1 is currently busy with a call. Then another user, 'C', tying to call
                                               user 'A', so normally both device 1 and 2 of user 'A' will be receiving incoming from user 'C' and as
                                               the device 1 of user 'A' is currently busy with another call and device 2 will be display incoming call
                                               from user 'C'. So when device 1 is receiving incoming call from user 'C', it will send busy status to
                                               user 'C' and at the same time that device will also send call accepted status to device 2 that user 'A'
                                               account is on too so as to inform device 2 to close that incoming call screen from user 'C' since user 'A'
                                               on device 1 has already send busy to that account. And to make sure that use case will be covered
                                               correctly, we will send call accepted to same account only after
                                               the sending call busy status is finished.
                                             */
                                            mCallMessageHelper.sendCallAcceptedToCurrentCallAccount(callData);
                                        },
                                        1000)));
            }
        }
    }

    public static boolean isInForeground() {
        ActivityManager.RunningAppProcessInfo appProcessInfo =
                new ActivityManager.RunningAppProcessInfo();
        ActivityManager.getMyMemoryState(appProcessInfo);
        return (appProcessInfo.importance == IMPORTANCE_FOREGROUND
                || appProcessInfo.importance == IMPORTANCE_VISIBLE);
    }

    public interface CallServiceConnectionListener {
        void onServiceReady();
    }
}
