package com.proapp.sompom.chat.listener;

import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.model.result.Chat;

/**
 * Created by He Rotha on 7/6/18.
 */
public interface GlobalChatListener {
    //currently, this feature used by Forward Screen only.
    //so, only my Chat & MessageState.Sent can retrieved
    void onMessageUpdated(Chat message, MessageState state);
}
