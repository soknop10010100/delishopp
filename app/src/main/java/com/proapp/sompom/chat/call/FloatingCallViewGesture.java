package com.proapp.sompom.chat.call;

import android.graphics.Point;
import android.view.MotionEvent;
import android.view.WindowManager;

import com.proapp.sompom.helper.CoordinateValueAnimator;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.widget.call.CallScreenView;

import timber.log.Timber;

public class FloatingCallViewGesture implements FloatingCallGesture.OnGestureListener {

    private CallScreenView mCallScreenView;
    private WindowManager mWindowManager;
    private FloatingCallViewGestureListener mListener;
    private Point mScreenSize;

    public FloatingCallViewGesture(CallScreenView callScreenView,
                                   WindowManager windowManager,
                                   Point screenSize,
                                   FloatingCallViewGestureListener listener) {
        mCallScreenView = callScreenView;
        mScreenSize = screenSize;
        mWindowManager = windowManager;
        mListener = listener;
    }

    @Override
    public void onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        Timber.i("onFling");
        if (mCallScreenView == null) {
            return;
        }
        mCallScreenView.setOnTouchListener(null);
        WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) mCallScreenView.getLayoutParams();
        CoordinateValueAnimator animator = new CoordinateValueAnimator(CoordinateValueAnimator.Interpolator.LINEAR);
        animator.setAnimatorListener((x, y) -> {
            try {
                if (x == animator.getEndX() && y == animator.getEndY()) {
                    if (mListener != null) {
                        mListener.onRemoveFloatingWindow(false);
                        Timber.i("onRemoveFloatingWindow");
                    }
                } else {
                    final WindowManager.LayoutParams param = (WindowManager.LayoutParams) mCallScreenView.getLayoutParams();
                    param.x = x;
                    param.y = y;
                    mWindowManager.updateViewLayout(mCallScreenView, param);
                    int alpha = x * 100 / animator.getEndX();
                    mCallScreenView.setAlpha(Math.abs(100 - alpha));
//                    Timber.i("updateViewLayout on fling: " + param.x + ", param.y: " + param.y + ", alpha: " + mCallScreenView.getAlpha());
                }
            } catch (Exception e) {
                animator.cancel();
                SentryHelper.logSentryError(e);
            }
        });

        if (e1.getY() < e2.getY()) {
            Point point = findLinearOfTwoVector(e1.getX(), e1.getY(), e2.getX(), e2.getY(), mScreenSize.y);
            animator.start(layoutParams.x, point.x, layoutParams.y, point.y);
        } else {
            Point point = findLinearOfTwoVector(e1.getX(), e1.getY(), e2.getX(), e2.getY(), 0);
            if (e1.getX() < e2.getX() && point.x < e2.getRawX()) {
                point.x = point.x * 2;
            }
            animator.start(layoutParams.x, point.x, layoutParams.y, point.y);
        }
    }

    @Override
    public void onSingleTapUp(MotionEvent motionEvent) {
        Timber.i("onSingleTapUp");
        mCallScreenView.setOnTouchListener(null);
        if (mListener != null) {
            mListener.onRemoveFloatingWindow(true);
        }
    }

    private Point findLinearOfTwoVector(float x1, float y1, float x2, float y2, float y3) {
        Point point = new Point();
        float b = (((y1 / x1) - (y2 / x2)) * (x1 * x2)) / ((-1 * x1) + x2);
        float a = (y1 - b) / x1;
        float x = (-b + y3) / a;
        point.x = (int) x;
        point.y = (int) y3;
        return point;
    }

    public interface FloatingCallViewGestureListener {
        void onRemoveFloatingWindow(boolean isNeedResizeBackToFullScreen);
    }
}
