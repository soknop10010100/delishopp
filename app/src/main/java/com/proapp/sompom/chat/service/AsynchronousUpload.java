package com.proapp.sompom.chat.service;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;

import com.proapp.sompom.database.MessageDb;
import com.proapp.sompom.helper.upload.AbsChatUploader;
import com.proapp.sompom.helper.upload.FileType;
import com.proapp.sompom.helper.upload.UploadListener;
import com.proapp.sompom.listener.ChatMediaUploadListener;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.result.Chat;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by He Rotha on 11/15/18.
 */
class AsynchronousUpload {
    private static final int MAX_UPLOAD_COUNTER = 4;
    private final Context mContext;
    private final Chat mChat;

    private int mIntervalUpload = 1;
    private int mMediaSize;
    private int mStartFromIndex;

    AsynchronousUpload(Context context, Chat chat) {
        mContext = context;
        mChat = chat;

        int mediaSize = mChat.getMediaList().size();
        if (mediaSize > MAX_UPLOAD_COUNTER) {
            mMediaSize = MAX_UPLOAD_COUNTER;
            mIntervalUpload = getLoopCounter(mediaSize);
        } else {
            mMediaSize = mediaSize;
        }
    }

    void startUpload(ChatMediaUploadListener uploadListener) {
        List<Pair<String, Integer>> uploadUrl = new ArrayList<>();
        for (int i = 0; i < mMediaSize; i++) {
            Media productMedia = mChat.getMediaList().get(mStartFromIndex);

            if (!productMedia.getUrl().startsWith("http")) {
                FileType fileType;
                if (mChat.getType() == Chat.Type.MIX_MEDIA) {
                    if (productMedia.getType() == MediaType.IMAGE) {
                        fileType = FileType.IMAGE;
                    } else if (productMedia.getType() == MediaType.GIF) {
                        fileType = FileType.GIF;
                    } else {
                        fileType = FileType.VIDEO;
                    }
                } else if (mChat.getType() == Chat.Type.IMAGE) {
                    fileType = FileType.IMAGE;
                } else if (mChat.getType() == Chat.Type.GIF) {
                    fileType = FileType.GIF;
                } else if (mChat.getType() == Chat.Type.VIDEO) {
                    fileType = FileType.VIDEO;
                } else if (mChat.getType() == Chat.Type.AUDIO) {
                    fileType = FileType.Audio;
                } else if (mChat.getType() == Chat.Type.FILE) {
                    fileType = FileType.FILE;
                } else {
                    break;
                }
                AbsChatUploader uploader = new AbsChatUploader(mContext,
                        fileType,
                        productMedia.getUrl(),
                        productMedia.getFileName());
                uploader.setListener(new UploadListener() {
                    @Override
                    public void onUploadSucceeded(String requestId, String imageUrl, String thumb) {
                        Timber.e("onUploadSucceeded: %s of %s", requestId, imageUrl);
                        onUploadSuccess(uploadUrl,
                                requestId,
                                productMedia.getId(),
                                imageUrl,
                                thumb,
                                uploadListener);
                    }

                    @Override
                    public void onUploadFail(String id, Exception ex) {
                        Timber.e("onUploadFail: %s", id);
                        if (uploadListener != null) {
                            uploadListener.onUploadFailed(id, mChat, productMedia.getId(), ex);
                        }
                    }

                    @Override
                    public void onUploadProgressing(String id,
                                                    int percent,
                                                    long uploadedBytes,
                                                    long totalBytes) {
                        Timber.e("onUploadProgressing: %s of %d", id, percent);
                        if (uploadListener != null) {
                            uploadListener.onUploadProgressing(id,
                                    mChat,
                                    productMedia.getId(),
                                    percent,
                                    uploadedBytes,
                                    totalBytes);
                        }
                    }

                    @Override
                    public void onCancel(String id) {
                        Timber.e("onCancel: %s", id);
                        if (uploadListener != null) {
                            uploadListener.onCancel(id, mChat, productMedia.getId());
                        }
                    }
                });
                String request = uploader.doUpload();
                Pair<String, Integer> pair = new Pair<>(request, mStartFromIndex);
                uploadUrl.add(pair);
            } else {
                Timber.e("loop continue");
            }

            mStartFromIndex += 1;
        }
    }

    private void onUploadSuccess(List<Pair<String, Integer>> uploadUrl,
                                 String requestId,
                                 String mediaId,
                                 String imageUrl,
                                 String thumbnailUrl,
                                 ChatMediaUploadListener uploadListener) {
        for (int i = uploadUrl.size() - 1; i >= 0; i--) {
            if (TextUtils.equals(uploadUrl.get(i).first, requestId)) {
                Integer index = uploadUrl.get(i).second;
                if (index != null) {
                    Media media = mChat.getMediaList().get(index);
                    media.setUrl(imageUrl);
                    media.setThumbnail(thumbnailUrl);
                    uploadUrl.remove(i);
                    MessageDb.save(mContext, mChat);
                }
            }
        }

        Timber.e("uploadUrl size: %s", uploadUrl.size());
        if (uploadUrl.isEmpty()) {
            mIntervalUpload -= 1;
            Timber.e("mMediaSize: %s, mIntervalUpload: %s", mMediaSize, mIntervalUpload);
            if (mIntervalUpload >= 1) {
                int originalSize = mChat.getMediaList().size();
                int loopCounter = getLoopCounter(originalSize) - mIntervalUpload;
                mMediaSize = getMediaSize(originalSize, (mMediaSize * loopCounter));
                startUpload(uploadListener);
            } else {
                uploadListener.onUploadSuccess(requestId, mChat, mediaId, imageUrl, thumbnailUrl);
            }
        }
    }

    private int getLoopCounter(int mediaSize) {
        int number = mediaSize / MAX_UPLOAD_COUNTER;
        if (mediaSize % MAX_UPLOAD_COUNTER > 0) {
            number += 1;
        }
        Timber.e("number: %s", number);
        return number;
    }

    private int getMediaSize(int originalSize, int mediaSize) {
        int result = originalSize - mediaSize;
        Timber.e("getMediaSize result: %s", result);
        if (result > MAX_UPLOAD_COUNTER) {
            return MAX_UPLOAD_COUNTER;
        } else {
            return result;
        }
    }
}
