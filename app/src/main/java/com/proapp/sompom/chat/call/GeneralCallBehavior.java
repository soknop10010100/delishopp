package com.proapp.sompom.chat.call;

import com.proapp.sompom.helper.AbsCallService;

/**
 * Created by Chhom Veasna on 2/24/2020.
 */
public interface GeneralCallBehavior {

    void dispatchCallNotification(AbsCallService.CallActionType actionType,
                                  AbsCallService.CallType callType,
                                  AbsCallClient.NotificationData data,
                                  String title,
                                  String userName,
                                  String groupId,
                                  boolean isCaller);

    void cancelNotification();

    void vibrate();
}
