package com.proapp.sompom.chat.service;

import android.text.TextUtils;

import com.proapp.sompom.model.LiveUseProfileData;
import com.proapp.sompom.model.emun.Presence;
import com.proapp.sompom.model.result.User;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import timber.log.Timber;

public class LiveUserDataService {

    final HashMap<String, LiveUseProfileData> mLiveUseProfileData = new HashMap<>();

    public void addInfo(String userId, LiveUseProfileData data) {
        mLiveUseProfileData.put(userId, data);
    }

    public LiveUseProfileData getInfo(String userId) {
        return mLiveUseProfileData.get(userId);
    }

    public void clearAllInfo() {
        mLiveUseProfileData.clear();
    }

    public void getGroupLastActiveInfo(List<User> participants, LiveUserDataServiceCallback callback) {
        //1. First need to find the last online user from the participant list
        User lastActiveUser = null;
        Date lastActiveDate = null;
        for (User participant : participants) {
            if (participant.isOnline() != null && participant.isOnline() &&
                    (lastActiveDate == null || (participant.getLastActivity() != null &&
                            participant.getLastActivity().getTime() > lastActiveDate.getTime()))) {
                lastActiveUser = participant;
                lastActiveDate = lastActiveUser.getLastActivity();
            }
        }
        if (lastActiveUser != null) {
            Timber.i("Online user of group from participant list: " + lastActiveUser.getFullName());
            if (callback != null) {
                callback.onGotGroupLastActiveInfo(lastActiveDate, lastActiveUser);
            }
        }

        /*
            2. Since there is no active online user from the participant list, we will get the most last active user
            from the runtime user live data info we manage.
         */
        lastActiveUser = null;
        lastActiveDate = null;
        Set<String> keys = new HashSet<>(mLiveUseProfileData.keySet());
        for (String key : keys) {
            if (!TextUtils.isEmpty(key)) {
                for (User participant : participants) {
                    if (TextUtils.isEmpty(participant.getId())) {
                        continue;
                    }

                    if (participant.getId().matches(key)) {
                        LiveUseProfileData liveUseProfileData = mLiveUseProfileData.get(key);
                        if (liveUseProfileData != null) {
                            if (liveUseProfileData.getActivityDate() != null) {
                                if (lastActiveDate == null || lastActiveDate.getTime() < liveUseProfileData.getActivityDate().getTime()) {
                                    lastActiveDate = liveUseProfileData.getActivityDate();
                                    lastActiveUser = participant;
                                    //Must assign user online status
                                    if (liveUseProfileData.getPresence() != null && liveUseProfileData.getPresence() != Presence.Unknown) {
                                        lastActiveUser.setOnline(liveUseProfileData.getPresence() == Presence.Online);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //In case that no last active found, will take the first user from list as default one.
        if (lastActiveUser == null) {
//            Timber.i("getGroupLastActiveInfo: Take the default user from list.");
            lastActiveUser = participants.get(0);
            lastActiveDate = lastActiveUser.getLastActivity();
        }
//        Timber.i("Last active user of group from runtime data: " + lastActiveUser.getFullName() + ", isOnline: " + lastActiveUser.isOnline() + ", lastActiveDate: " + lastActiveDate);
        if (callback != null) {
            callback.onGotGroupLastActiveInfo(lastActiveDate, lastActiveUser);
        }
    }

    public static class LiveUserDataServiceCallback {

        protected void onGotGroupLastActiveInfo(Date lastActiveDate, User lastActiveUser) {
        }
    }
}
