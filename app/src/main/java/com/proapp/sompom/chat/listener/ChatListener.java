package com.proapp.sompom.chat.listener;

import com.proapp.sompom.model.result.Chat;

import java.util.List;

/**
 * Created by He Rotha on 7/6/18.
 */
public interface ChatListener {

    void onReceiveMessage(Chat message);

    void onRecipientTyping(Chat message);

    boolean isInBackground();

    String getChannelId();

    default boolean isChatScreen() {
        return false;
    }

    default void onReceiveUpdateSeenUserIdListUpdate(String conversationId, List<Chat> chats) {
    }
}
