package com.proapp.sompom.listener;

import android.view.View;

import com.proapp.sompom.model.result.Category;

/**
 * Created by He Rotha on 9/15/17.
 */
public interface OnArticleSelectListener {
    void onNormalArticleSelected(View view, Category selectedArticle);

    void onAllArticleSelected(View view, Category selectedArticle);
}
