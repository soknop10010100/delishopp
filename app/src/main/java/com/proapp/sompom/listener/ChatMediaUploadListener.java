package com.proapp.sompom.listener;

import com.proapp.sompom.model.result.Chat;

/**
 * Created by Chhom Veasna on 10/4/2019.
 */
public interface ChatMediaUploadListener {

    void onUploadSuccess(String requestId,
                         Chat chat,
                         String mediaId,
                         String imageUrl,
                         String thumb);

    void onUploadFailed(String requestId,
                        Chat chat,
                        String mediaId,
                        Exception ex);

    void onUploadProgressing(String requestId,
                             Chat chat,
                             String mediaId,
                             int percent,
                             long percentBytes,
                             long totalBytes);

    void onCancel(String requestId, Chat chat, String mediaId);
}
