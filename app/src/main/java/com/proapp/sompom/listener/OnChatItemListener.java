package com.proapp.sompom.listener;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.viewmodel.ChatMediaViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 8/30/18.
 */

public interface OnChatItemListener {
    void onImageClick(List<Media> list,
                      int productMediaPosition);

    void onFileClick(Chat chat, Media selectedMedia);

    void onChatItemLongPressClick(Chat chat, @Nullable Media media);

    void onForwardClick(Chat chat);

    void onMessageRetryClick(Chat chat);


    User getUser(String id);

    void onCallAgainOrBack(Chat chat, AbsCallService.CallType callType);

    default void onRepliedChatClicked(Chat repliedChat, boolean isInInputReplyMode) {
    }

    default void onSwipeToReply(Chat repliedChat) {
    }

    default void onCalculateSeenDisplayNameHeight(String chatId,
                                                  String seenText,
                                                  boolean isRecipientSide,
                                                  ChatMediaViewModel.CalculateSeenNameDisplayCallback callback) {
    }

    default void onGifVideoPlayerCreated(String mediaId, SimpleExoPlayer player) {
    }

    default SimpleExoPlayer getGifVideoPlayer(String mediaId) {
        return null;
    }

    default void onGifVideoPlayerReleased(String mediaId) {
    }
}
