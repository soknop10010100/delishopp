package com.proapp.sompom.listener;


import com.proapp.sompom.observable.ErrorThrowable;

/**
 * Created by He Rotha on 11/6/17.
 */

public interface OnCallbackListener<T> extends OnCompleteListener<T> {
    void onFail(ErrorThrowable ex);
}
