package com.proapp.sompom.listener;

import com.proapp.sompom.model.result.Conversation;

/**
 * Created by He Rotha on 12/26/18.
 */
public interface OnMessageDbListener {
    void onConversationDelete(String conversationId);

    void onConversationUpdate(Conversation conversation);
}
