package com.proapp.sompom.listener;

import com.proapp.sompom.model.result.User;

/**
 * Created by He Rotha on 6/14/18.
 */
public interface OnLikeItemListener {
    void onFollowClick(User user, boolean isFollow);

    void onNotifyItem();
}
