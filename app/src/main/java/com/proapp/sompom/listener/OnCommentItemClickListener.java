package com.proapp.sompom.listener;

import com.proapp.sompom.model.result.Comment;

import retrofit2.Response;

/**
 * Created by nuonveyo on 7/23/18.
 */

public interface OnCommentItemClickListener {
    void onReplyButtonClick(Comment comment, int position);

    void onLikeButtonClick(Comment comment, int position, OnCallbackListener<Response<Object>> listener);

    void onLongPress(Comment comment, int position);
}
