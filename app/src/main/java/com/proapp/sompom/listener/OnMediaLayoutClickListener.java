package com.proapp.sompom.listener;

import com.proapp.sompom.widget.lifestream.MediaLayout;

/**
 * Created by He Rotha on 9/7/17.
 */

public interface OnMediaLayoutClickListener {
    void onMediaLayoutClick(MediaLayout mediaLayout);
}
