package com.proapp.sompom.listener;

import com.proapp.sompom.model.LoadCommentDirection;

/**
 * Created by nuonveyo
 * on 3/12/19.
 */
public interface OnLoadPreviousCommentClickListener {
    void onClick(LoadCommentDirection previousComment, int position);
}
