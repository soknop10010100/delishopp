package com.proapp.sompom.listener;

import com.proapp.sompom.model.WallStreetAdaptive;

/**
 * Created by nuonveyo on 12/5/18.
 */

public interface OnDetailShareItemCallback extends OnClickListener {
    void onMediaClick(WallStreetAdaptive adaptive, int position);
}
