package com.proapp.sompom.listener;

import com.proapp.sompom.viewmodel.BaseMediaViewModel;

/**
 * Created by He Rotha on 8/29/17.
 */

public interface OnMediaHandler {
    void onPlay(BaseMediaViewModel model);

    void onClick(BaseMediaViewModel model);
}
