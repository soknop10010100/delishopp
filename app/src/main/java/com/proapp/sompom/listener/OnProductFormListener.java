package com.proapp.sompom.listener;

import com.proapp.sompom.model.Media;

import java.util.List;

/**
 * Created by He Rotha on 6/13/18.
 */
public interface OnProductFormListener {
    void onSetImage(List<Media> list);
}
