package com.proapp.sompom.listener;

import com.proapp.sompom.model.result.NotificationSettingModel;

/**
 * Created by He Rotha on 11/29/18.
 */
public interface OnEditProfileListener extends OnCompleteListener<NotificationSettingModel> {
    void onLogoutClick();
}
