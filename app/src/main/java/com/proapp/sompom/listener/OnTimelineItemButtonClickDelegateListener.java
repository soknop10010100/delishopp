package com.proapp.sompom.listener;

import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.result.User;
import com.sompom.videomanager.widget.MediaRecyclerView;

public class OnTimelineItemButtonClickDelegateListener implements OnTimelineItemButtonClickListener {

    private OnTimelineItemButtonClickListener mListener;

    public OnTimelineItemButtonClickDelegateListener(OnTimelineItemButtonClickListener listener) {
        mListener = listener;
    }

    @Override
    public void onCommentButtonClick(Adaptive adaptive, int position) {
        mListener.onCommentButtonClick(adaptive, position);
    }

    @Override
    public void onShareButtonClick(Adaptive adaptive, int position) {
        mListener.onShareButtonClick(adaptive, position);
    }

    @Override
    public void onUserViewMediaClick(Adaptive adaptive) {
        mListener.onUserViewMediaClick(adaptive);
    }

    @Override
    public void onLikeViewerClick(Adaptive adaptive) {
        mListener.onLikeViewerClick(adaptive);
    }

    @Override
    public void onFollowButtonClick(String id, boolean isFollowing) {
        mListener.onFollowButtonClick(id, isFollowing);
    }

    @Override
    public void onShowPostContextMenuClick(Adaptive adaptive, User user, int position) {
        mListener.onShowPostContextMenuClick(adaptive, user, position);
    }

    @Override
    public void onCommentClick(Adaptive adaptive, boolean autoDisplayKeyboard) {
        mListener.onCommentClick(adaptive, autoDisplayKeyboard);
    }

    @Override
    public MediaRecyclerView getRecyclerView() {
        return mListener.getRecyclerView();
    }

    @Override
    public void onShowSnackBar(boolean isError, String message) {
        mListener.onShowSnackBar(isError, message);
    }

    @Override
    public void onVideoFullScreenClicked(Media media, int position, long time) {
        mListener.onVideoFullScreenClicked(media, position, time);
    }
}
