package com.proapp.sompom.listener;

import android.content.Context;

import com.proapp.sompom.model.result.AppSetting;
import com.proapp.sompom.utils.SharedPrefUtils;

public interface BaseAppSettingUI {

    default boolean shouldDisplayCompanyLogo(Context context) {
        AppSetting appSetting = SharedPrefUtils.getAppSetting(context);
        if (appSetting != null) {
            return !appSetting.isHideCompanyLogo();
        }

        return false;
    }
}
