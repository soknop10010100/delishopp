package com.proapp.sompom.listener;

import com.proapp.sompom.model.notification.NotificationAdaptive;

/**
 * Created by nuonveyo
 * on 2/13/19.
 */

public interface OnNotificationItemClickListener {
    void onItemClick(NotificationAdaptive data, int position);
}
