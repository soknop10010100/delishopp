package com.proapp.sompom.listener;

public interface OnBadgeUpdateListener {
    void onConversationBadgeUpdate(int value);
}
