package com.proapp.sompom.listener;

import com.proapp.sompom.model.result.Comment;

/**
 * Created by nuonveyo on 7/23/18.
 */

public interface OnCommentDialogClickListener {

    default void onReplaceCommentFragment(Comment comment, int position) {
    }

    default void onDismissDialog(boolean isDismiss) {
    }

    default void onNotifyCommentItemChange(Comment comment) {
    }

    default void onMainCommentItemRemoved(int position, Comment comment) {
    }

    default void onRemoveSubCommentSuccess() {
    }

    default void onPostSubCommentSuccess() {
    }
}
