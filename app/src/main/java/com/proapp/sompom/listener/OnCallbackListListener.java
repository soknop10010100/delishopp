package com.proapp.sompom.listener;


import com.proapp.sompom.observable.ErrorThrowable;

/**
 * Created by He Rotha on 11/6/17.
 */

public interface OnCallbackListListener<T> extends OnCompleteListListener<T> {
    void onFail(ErrorThrowable ex);
}
