package com.proapp.sompom.listener;

import android.text.Spanned;

/**
 * Created by nuonveyo on 11/2/18.
 */

public abstract class AbsLinkPreviewCallback {

    public abstract void onRemoveListener();

    public abstract void onStartDownloadLinkPreview(String textLink);

    public abstract void onShowGifLinkFormKeyboard(String link);

    public abstract void onSetEditText(String text, boolean isFromEditPost);

    public abstract void shouldHidePreviewCloseButton();
}
