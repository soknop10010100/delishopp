package com.proapp.sompom.listener;

import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;

/**
 * Created by He Rotha on 7/6/18.
 */
public interface OnMessageItemClick {

    default void onConversationClick(Conversation conversation, int newBadgeCount) {
        //nothing to do on this stuff
    }

    default void onConversationLongClick(Conversation conversation) {
        //nothing to do on this stuff
    }

    void onConversationUserClick(User user);
}
