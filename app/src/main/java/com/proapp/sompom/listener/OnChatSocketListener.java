package com.proapp.sompom.listener;

import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.SocketError;

import java.util.List;

/**
 * Created by He Rotha on 12/24/18.
 */
public interface OnChatSocketListener {

    void onMessageReceive(Chat chat, String tag);

    void onRecipientTypingMessage(Chat chat);

    void onConversationCreateOrUpdate(Conversation conversation, String tag);

    void onConversationDelete(String conversationId);

    void onMessageSent(Chat chat);

    boolean isInBg(String channelId);

    List<String> getCurrentChannel();

    void onBadgeUpdate(int value);

    default boolean isInChatScreen(String channelId) {
        return false;
    }

    default void onCallError(SocketError error) {
    }

    default void onReceiveUpdateSeenUserIdListUpdate(String conversationId, List<Chat> chats) {
    }
}
