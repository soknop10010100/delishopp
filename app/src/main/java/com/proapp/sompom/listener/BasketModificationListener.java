package com.proapp.sompom.listener;

import com.proapp.sompom.model.concierge.ConciergeMenuItemManipulationInfo;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;

public interface BasketModificationListener {

    void onBasketModificationError(SupportConciergeCustomErrorResponse response,
                                   String errorMessage,
                                   ConciergeMenuItemManipulationInfo manipulationInfo);
}
