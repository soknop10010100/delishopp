package com.proapp.sompom.listener;

import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.Media;

/**
 * Created by nuonveyo on 7/12/18.
 */

public interface OnTimelineItemButtonClickListener extends OnTimelineHeaderItemClickListener {
    void onCommentButtonClick(Adaptive adaptive, int position);

    void onShareButtonClick(Adaptive adaptive, int position);

    void onUserViewMediaClick(Adaptive adaptive);

    void onLikeViewerClick(Adaptive adaptive);

    void onShowSnackBar(boolean isError, String message);

    default void onVideoFullScreenClicked(Media media, int position, long time) {

    }
}
