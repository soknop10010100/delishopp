package com.proapp.sompom.listener;

import com.proapp.sompom.model.emun.SegmentedControlItem;

/**
 * Created by nuonveyo on 6/26/18.
 */

public interface OnSegmentedClickListener {
    void onClick(SegmentedControlItem item);
}
