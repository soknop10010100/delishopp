package com.proapp.sompom.listener;

import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.result.User;
import com.sompom.videomanager.widget.MediaRecyclerView;

/**
 * Created by nuonveyo on 7/12/18.
 */

public interface OnTimelineHeaderItemClickListener {
    void onFollowButtonClick(String id, boolean isFollowing);

    void onShowPostContextMenuClick(Adaptive adaptive,
                                    User user,
                                    int position);

    void onCommentClick(Adaptive adaptive, boolean autoDisplayKeyboard);

    MediaRecyclerView getRecyclerView();
}
