package com.proapp.sompom.listener;

import com.proapp.sompom.model.result.Conversation;

/**
 * Created by He Rotha on 9/14/17.
 */
public interface OnChatItemClickListener {
    void onChatItemClicked(Conversation chatItemModel);
}
