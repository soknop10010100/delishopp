package com.proapp.sompom.listener;

import com.proapp.sompom.model.result.Product;

/**
 * Created by nuonveyo on 7/9/18.
 */

public interface OnLikeProductListener {
    void onLikeClick(Product product, int position);
}
