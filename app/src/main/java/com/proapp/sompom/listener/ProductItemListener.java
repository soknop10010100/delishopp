package com.proapp.sompom.listener;

import com.proapp.sompom.model.concierge.ConciergeMenuItem;

public interface ProductItemListener {

    void onAddProduct(ConciergeMenuItem item);

    void onRemoveProduct(ConciergeMenuItem item);
}
