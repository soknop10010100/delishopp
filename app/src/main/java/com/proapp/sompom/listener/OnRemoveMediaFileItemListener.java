package com.proapp.sompom.listener;

/**
 * Created by nuonveyo on 5/8/18.
 */

public interface OnRemoveMediaFileItemListener {
    void onRemoveItem(int position);
}
