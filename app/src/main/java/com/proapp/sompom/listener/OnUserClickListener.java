package com.proapp.sompom.listener;

import com.proapp.sompom.model.result.User;

public interface OnUserClickListener {
    void onUserClick(User user);
}
