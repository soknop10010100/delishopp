package com.proapp.sompom.listener;

import com.proapp.sompom.model.Media;

/**
 * Created by He Rotha on 8/29/17.
 */

public interface ViewModelLifeCycle {
    void onPause();

    void onResume();

    void onResumePlaybackMedia(Media media);
}
