package com.proapp.sompom.listener;

import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.result.Product;

/**
 * Created by He Rotha on 9/11/17.
 */
public interface OnProductItemClickListener {
    void onProductItemClick(Adaptive product);

    void onProductItemEdit(Product product);

}
