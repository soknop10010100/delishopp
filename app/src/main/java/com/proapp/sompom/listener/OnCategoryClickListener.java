package com.proapp.sompom.listener;

import com.proapp.sompom.model.result.Category;

public interface OnCategoryClickListener {
    void onCategoryClick(Category category);
}
