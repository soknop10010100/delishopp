package com.proapp.sompom.listener;

import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.observable.ErrorThrowable;

import java.util.List;

public interface LoadMoreCommentDirectionCallback {

    void onFail(ErrorThrowable ex, boolean isInJumpMode, boolean isLoadPrevious, boolean isCanLoadNext);

    void onLoadCommentDirectionSuccess(List<Comment> listComment, boolean isInJumpMode, boolean isLoadPrevious, boolean isCanLoadNext);
}
