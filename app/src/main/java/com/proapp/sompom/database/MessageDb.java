package com.proapp.sompom.database;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.listener.OnChatSocketListener;
import com.proapp.sompom.listener.OnMessageDbListener;
import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.model.socket.SendReadStatusJunkMessage;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.realm.Case;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import timber.log.Timber;

/**
 * Created by He Rotha on 10/30/18.
 */
public final class MessageDb {

    private static final int LIMIT = 25;

    private static final int MINIMUM_UNREAD = 5;
    private static final String FIELD_SENDER_ID = "mSenderId";
    private static final String FIELD_STATUS = "mStatus";
    private static final String FIELD_DATE = "mDate";
    private static final String FIELD_ID = "mId";
    private static final String FIELD_SEEN = "mSeen";
    public static final String FIELD_CHANNEL_ID = "mChannelId";
    public static final String FIELD_IS_DELETED = "mIsDelete";
    public static final String FIELD_MESSAGE_ID = "mId";
    public static final String FIELD_CONTENT = "mContent";

    private MessageDb() {
    }

    public static void updateLocalMessageDate(Context context, String chatId, Date updateDate) {
        Chat chat = queryById(context, chatId);
        if (chat != null) {
            chat.setDate(updateDate);
            save(context, chat);
        }
    }

    public static void addReadMessageToSeenListOfMessage(Context context,
                                                         String userId,
                                                         Chat lastSeenMessage,
                                                         OnChatSocketListener socketListener) {
//        Timber.i("addReadMessageToSeenListOfMessage: userId: " + userId +
//                ", chat id: " + lastSeenMessage.getId());
        Chat localChat = queryById(context, lastSeenMessage.getId());
        if (localChat != null) {
            RealmList<String> seen = localChat.getSeen();
            if (seen == null) {
                seen = new RealmList<>();
            }
//            Timber.i("addReadMessageToSeenListOfMessage: localChat exist: local seen: " + new Gson().toJson(localChat.getSeen()));
            if (!seen.contains(userId)) {
                seen.add(userId);
                save(context, localChat);
                if (socketListener != null) {
                    socketListener.onReceiveUpdateSeenUserIdListUpdate(lastSeenMessage.getChannelId(),
                            Collections.singletonList(localChat));
                }
                injectSeenUserIdToPreviousMessages(context, userId, lastSeenMessage, socketListener);
            }
        } else {
            checkToAddSeenUserId(lastSeenMessage, userId);
            save(context, lastSeenMessage);
            if (socketListener != null) {
                socketListener.onReceiveUpdateSeenUserIdListUpdate(lastSeenMessage.getChannelId(),
                        Collections.singletonList(lastSeenMessage));
            }
            injectSeenUserIdToPreviousMessages(context, userId, lastSeenMessage, socketListener);
        }
    }

    public static boolean isLocalMessageStatusIsSent(Context context, String chatId) {
        Chat chat = queryById(context, chatId);
        return chat != null && chat.getStatus() == MessageState.SENT;
    }

    public static boolean isUserSeenMessage(Context context, String userId, String messageId) {
        Chat localChat = queryById(context, messageId);
        if (localChat != null && localChat.getSeen() != null && !localChat.getSeen().isEmpty()) {
            for (String id : localChat.getSeen()) {
                if (TextUtils.equals(id, userId)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static String getLastSeenMessageIdOfUser(Context context, String userId, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);
        RealmResults<Chat> results = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, conversationId)
                .sort(FIELD_DATE, Sort.DESCENDING)
                .findAll();
        String lastSeenMessageId = null;
        for (Chat chat : results) {
            if (!TextUtils.isEmpty(lastSeenMessageId)) {
                //Already found
                break;
            }

            if (chat.getSeen() != null && !chat.getSeen().isEmpty()) {
                for (String id : chat.getSeen()) {
                    if (TextUtils.equals(id, userId)) {
                        lastSeenMessageId = chat.getId();
                        break;
                    }
                }
            }
        }
        realm.close();

        return lastSeenMessageId;
    }

    public static void markLocalMessageAsDelivered(Context context, String messageId) {
        Chat chat = queryById(context, messageId);
        if (chat != null && chat.getStatus() != MessageState.DELIVERED) {
            chat.setStatus(MessageState.DELIVERED);
            save(context, chat);
        }
    }

    private static void checkToAddSeenUserId(Chat chat, String userId) {
        if (chat.getSeen() == null) {
            chat.setSeen(new RealmList<>());
        }
        if (!chat.getSeen().contains(userId)) {
            chat.getSeen().add(userId);
        }
    }

    private static void injectSeenUserIdToPreviousMessages(Context context,
                                                           String userId,
                                                           Chat lastSeenMessage,
                                                           OnChatSocketListener socketListener) {
        /*
            Will check to add current seen user into previous message's seen list if that user was not
            yet added.
        */
        new Handler(Looper.getMainLooper()).post(() -> {
            Realm realm = RealmDb.getInstance(context);
            RealmResults<Chat> results = realm.where(Chat.class)
                    .equalTo(FIELD_CHANNEL_ID, lastSeenMessage.getChannelId())
                    .lessThan("mDate", lastSeenMessage.getDate())
                    .findAllAsync();
            OrderedRealmCollectionChangeListener<RealmResults<Chat>> listener = (chats, changeSet) -> {
                Timber.i("onChange: isLoaded: " + results.isLoaded());
                if (results.isLoaded()) {
                    Timber.i("Loaded size: " + results.size());
                    results.removeAllChangeListeners();
                    List<Chat> updateChat = new ArrayList<>();
                    for (Chat result : results) {
                        if (result.getSeen() == null || !result.getSeen().contains(userId)) {
                            Chat chat = realm.copyFromRealm(result);
                            if (chat.getSeen() == null) {
                                chat.setSeen(new RealmList<>());
                            }
                            chat.getSeen().add(userId);
                            updateChat.add(chat);
                        }
                    }

                    if (!updateChat.isEmpty()) {
                        Timber.i("updateChat size: " + updateChat.size());
                        realm.executeTransactionAsync(realm1 -> realm1.insertOrUpdate(updateChat));
                        if (socketListener != null) {
                            socketListener.onReceiveUpdateSeenUserIdListUpdate(lastSeenMessage.getChannelId(), updateChat);
                        }
                    }
                }
            };
            results.addChangeListener(listener);
        });
    }

    public static void save(Context context, Chat chat) {
        if (!TextUtils.isEmpty(chat.getId())) {
            ChatHelper.logMessages("Save single chat", Collections.singletonList(chat));
            maintainSeenAndContentStateOfMessage(context, Collections.singletonList(chat));
            checkToMaintainLocalDownloadedMedia(context, Collections.singletonList(chat));
            Realm realm = RealmDb.getInstance(context);
            RealmDb.refresh(realm);
            realm.beginTransaction();
            realm.insertOrUpdate(chat);
            realm.commitTransaction();
            realm.close();
        }
    }

    private static void checkToMaintainLocalDownloadedMedia(Context context, List<Chat> chatList) {
        List<Media> mediaList = new ArrayList<>();
        for (Chat chat : chatList) {
            if (chat.getMediaList() != null && !chatList.isEmpty()) {
                mediaList.addAll(chat.getMediaList());
            }
        }
        if (!mediaList.isEmpty()) {
            MediaDb.backupLocalDownloadedMFileMediaPath(context, mediaList);
        }
    }

    public static void maintainSeenAndContentStateOfMessage(Context context, List<Chat> chats) {
        /*
        Since we are using user seen id of a message for display seen user message list when user
        click on that message in chat screen, we need to make sure that the seen id will save properly
        by always preserve the existing and add new id into seen list of each chat before saving to
        local db.
         */
        if (chats != null && !chats.isEmpty()) {
            String[] chatIds = new String[chats.size()];
            for (int i = 0; i < chats.size(); i++) {
                chatIds[i] = chats.get(i).getId();
            }

            Realm realm = RealmDb.getInstance(context);
            RealmResults<Chat> localList = realm.where(Chat.class)
                    .in("mId", chatIds)
                    .findAll();
            if (localList != null && !localList.isEmpty()) {
                for (Chat local : localList) {
                    for (Chat chat : chats) {
                        if (TextUtils.equals(local.getId(), chat.getId())) {
                            //Merge seen user ids here.
                            if (chat.getSeen() != null && !chat.getSeen().isEmpty()) {
                                RealmList<String> seen = new RealmList<>();
                                seen.addAll(local.getSeen());

                                if (seen.isEmpty()) {
                                    seen.addAll(chat.getSeen());
                                } else {
                                    for (String id : chat.getSeen()) {
                                        if (!seen.contains(id)) {
                                            seen.add(id);
                                        }
                                    }
                                }

                                chat.setSeen(seen);
                            }

                            /*
                                Need to maintain message seen status like the one in local because message
                                from server response with incorrect status.
                             */
                            if (local.getStatus() == MessageState.SEEN) {
                                chat.setStatus(local.getStatus());
                            }

                            //Maintain chat content if necessary
                            if (TextUtils.isEmpty(chat.getContent())) {
                                chat.setContent(local.getContent());
                            }

                            break;
                        }
                    }
                }
            }
        }
    }

    public static void updateEditedMessage(Context context, Chat editedMessage) {
        if (editedMessage != null) {
            Chat localChat = queryById(context, editedMessage.getId());
            if (localChat != null) {
                localChat.setContent(editedMessage.getContent());
                localChat.setEdited(true);
                localChat.setLastEditedDate(editedMessage.getLastEditedDate());
                save(context, localChat);
            }
        }
    }

    public static boolean isLastMessageOfConversation(Context context, String conversationId, String messageId) {
        Chat lastChat = getLastMessageOfConversation(context, conversationId);
        return lastChat != null && TextUtils.equals(lastChat.getId(), messageId);
    }

    public static void debugSave(Context context, Chat chat) {
        Timber.e("Debug save chat : " + new Gson().toJson(chat));
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        realm.insertOrUpdate(chat);
        realm.commitTransaction();
        realm.close();
        Timber.e("Debug save chat ended");
    }

    public static void save(Context context, List<Chat> chats) {
        ChatHelper.logMessages("Save chat list: ", chats);
        maintainSeenAndContentStateOfMessage(context, chats);
        checkToMaintainLocalDownloadedMedia(context, chats);
        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);
        realm.beginTransaction();
        realm.insertOrUpdate(chats);
        realm.commitTransaction();
        realm.close();
    }

    public static void saveWithoutCheckingToBackupLocalProperty(Context context, List<Chat> chats) {
        ChatHelper.logMessages("saveWithoutCheckingToBackupLocalProperty: ", chats);
        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);
        realm.beginTransaction();
        realm.insertOrUpdate(chats);
        realm.commitTransaction();
        realm.close();
    }

    public static void deleteByConversation(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        realm.executeTransaction(realm1 -> {
            RealmResults<Chat> data = realm1.where(Chat.class)
                    .equalTo(FIELD_CHANNEL_ID, conversationId)
                    .findAll();
            if (data != null) {
                data.deleteAllFromRealm();
            }
        });
        realm.close();
    }

    public static void delete(Context context, Chat deleteChat, OnMessageDbListener listener) {
        if (deleteChat == null) {
            return;
        }
        deleteChat.setDelete(true);

        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        realm.insertOrUpdate(deleteChat);

        Chat lastMessage = getLastMessageOfConversation(context, deleteChat.getChannelId());
        if (lastMessage != null && TextUtils.equals(deleteChat.getId(), lastMessage.getId())) {
            /*
            Last message of conversation was removed. So we have to update the group's last message
            status.
             */
            lastMessage.setDelete(true);
            Conversation conversation = realm.where(Conversation.class)
                    .equalTo(FIELD_ID, lastMessage.getChannelId())
                    .findFirst();
            if (conversation != null) {
                conversation.setContent(lastMessage.getActualContent(context,
                        conversation,
                        true));
                conversation = realm.copyFromRealm(conversation);
                conversation.setLastMessage(lastMessage);
                realm.insertOrUpdate(conversation);
                if (listener != null) {
                    listener.onConversationUpdate(conversation);
                }
            }
        }
        realm.commitTransaction();
        realm.close();
    }

    public static Chat queryById(Context context, String id) {
        Realm realm = RealmDb.getInstance(context);
        Chat chat = realm.where(Chat.class)
                .equalTo(FIELD_ID, id)
                .findFirst();
        if (chat == null) {
            realm.close();
            return null;
        } else {
            chat = realm.copyFromRealm(chat);
            realm.close();
            return chat;
        }
    }

    public static List<Chat> queryByIds(Context context, String... ids) {
        Realm realm = RealmDb.getInstance(context);
        RealmResults<Chat> chats = realm.where(Chat.class)
                .in(FIELD_ID, ids)
                .findAll();
        if (chats == null) {
            realm.close();
            return new ArrayList<>();
        } else {
            List<Chat> chatList = realm.copyFromRealm(chats);
            realm.close();
            return chatList;
        }
    }

    public static List<Chat> queryUnreadMessageFromIdAndUpdateStatusToSeen(Context context, String id, String channelId) {
        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);

        Chat currentChat = realm.where(Chat.class)
                .equalTo(FIELD_ID, id)
                .findFirst();

        if (currentChat == null) {
            realm.close();
            return new ArrayList<>();
        }

        String[] messageStates = new String[]{MessageState.SENT.getValue(), MessageState.DELIVERED.getValue()};
        Date earliestDateInDb = realm.where(Chat.class).equalTo(FIELD_CHANNEL_ID, channelId).minimumDate(FIELD_DATE);
        RealmQuery<Chat> chatRealmQuery = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, channelId)
                .in(FIELD_STATUS, messageStates)
                .sort(FIELD_DATE, Sort.DESCENDING)
                .between(FIELD_DATE, earliestDateInDb, currentChat.getDate());

        List<Chat> chatList = chatRealmQuery.findAll();

        chatList = realm.copyFromRealm(chatList);
        for (Chat chat : chatList) {
            chat.setStatus(MessageState.SEEN);
        }
        Collections.sort(chatList);
        realm.beginTransaction();
        realm.insertOrUpdate(chatList);
        realm.commitTransaction();
        realm.close();
        return chatList;
    }

    public static long getMessageCount(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        RealmResults<Chat> result = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, conversationId)
                .notEqualTo(FIELD_STATUS, MessageState.SENDING.getValue())
                .findAll();
        int validChatCount = getValidChatCount(result);
        realm.close();
        return validChatCount;
    }

    public static long getMessageCountForAllStatus(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        RealmResults<Chat> result = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, conversationId)
                .findAll();
        int validChatCount = getValidChatCount(result);
        realm.close();
        return validChatCount;
    }

    private static int getValidChatCount(RealmResults<Chat> chats) {
        int counter = 0;
        for (Chat chat : chats) {
            if (isValidChat(chat)) {
                counter++;
            }
        }

        return counter;
    }

    public static boolean isValidChat(Chat chat) {
        return Chat.ChatType.from(chat.getChatType()) != Chat.ChatType.UNKNOWN ||
                chat.getType() != Chat.Type.TEXT ||
                ((chat.getType() == Chat.Type.TEXT) && !TextUtils.isEmpty(chat.getContent()));
    }

    @Nullable
    public static Chat getLastUnseenMessage(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        Chat chat;
        try {
            chat = realm.where(Chat.class)
                    .equalTo(FIELD_CHANNEL_ID, conversationId)
                    .equalTo(FIELD_STATUS, MessageState.SENT.getValue())
                    .sort(FIELD_DATE, Sort.ASCENDING)
                    .findFirst();
            if (chat == null) {
                chat = realm.where(Chat.class)
                        .equalTo(FIELD_CHANNEL_ID, conversationId)
                        .and()
                        .beginGroup()
                        .notEqualTo(FIELD_STATUS, MessageState.SENDING.getValue())
                        .or()
                        .notEqualTo(FIELD_STATUS, MessageState.FAIL.getValue())
                        .endGroup()
                        .sort(FIELD_DATE, Sort.DESCENDING)
                        .findFirst();
            }
            if (chat != null) {
                chat = realm.copyFromRealm(chat);
            }
        } catch (Exception ex) {
            chat = null;
            SentryHelper.logSentryError(ex);
        }
        realm.close();
        return chat;
    }

    @Nullable
    public static Chat getLastMessageOfConversation(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        Chat chat = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, conversationId)
                .and()
                .sort(FIELD_DATE, Sort.DESCENDING)
                .findFirst();

        if (chat != null) {
            chat = realm.copyFromRealm(chat);
        }

        realm.close();
        return chat;
    }

    @Nullable
    public static Chat getLastMessageOfConversationThatSent(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        Chat chat = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, conversationId)
                .and()
                .notEqualTo(FIELD_STATUS, MessageState.SENDING.getValue())
                .and()
                .notEqualTo(FIELD_STATUS, MessageState.FAIL.getValue())
                .and()
                .sort(FIELD_DATE, Sort.DESCENDING)
                .findFirst();

        if (chat != null) {
            chat = realm.copyFromRealm(chat);
        }

        realm.close();
        return chat;
    }

    public static Chat getOldestMessageOfConversation(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        Chat chat = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, conversationId)
                .and()
                .sort(FIELD_DATE, Sort.ASCENDING)
                .findFirst();

        if (chat != null) {
            chat = realm.copyFromRealm(chat);
        }

        realm.close();
        return chat;
    }

    public static List<Chat> queryChatListForChatScreen(Context context,
                                                        String conversationId,
                                                        boolean refreshDatabase) {
        Realm realm = RealmDb.getInstance(context);
        if (refreshDatabase) {
            RealmDb.refresh(realm);
        }
        List<Chat> list = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, conversationId)
                .sort(FIELD_DATE, Sort.DESCENDING)
                .limit(LIMIT)
                .findAll();
        if (list != null) {
            list = realm.copyFromRealm(list);
            sortMessageByDateASC(list);
        }
        realm.close();
        return list;
    }

    private static void sortMessageByDateASC(List<Chat> chatList) {
        /*
           In the chat screen, we have to display the latest message in the bottom of list. So we
           have to sort the message list by message's date ASC because some APIs response does not
           always response what client prefer.
         */
        if (chatList != null) {
            Collections.sort(chatList);
        }
    }

    public static List<Chat> queryToSelectedMessage(Context context,
                                                    Chat selectedChat,
                                                    String conversationId,
                                                    boolean refreshDatabase) {
        Realm realm = RealmDb.getInstance(context);
        if (refreshDatabase) {
            RealmDb.refresh(realm);
        }
        Chat lastMessageOfConversation = getLastMessageOfConversation(context, conversationId);
        Timber.i("lastMessageOfConversation: " + lastMessageOfConversation);
        RealmQuery<Chat> chatRealmQuery = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, conversationId);
        if (lastMessageOfConversation != null && selectedChat != null) {
            chatRealmQuery = chatRealmQuery.between(FIELD_DATE,
                    selectedChat.getDate(),
                    lastMessageOfConversation.getDate());
        }
        List<Chat> list = chatRealmQuery
                .sort(FIELD_DATE, Sort.DESCENDING)
                .findAll();
        if (list != null && list.size() < LIMIT) {
            /*
            The select message is in among latest message of @LIMIT number. So we will have to query
            within the limit of latest message which selected message is in. If the selected message is not
            in the default latest message amount, we have to query from the latest message to the selected
            message and this logic is already implemented in above code.
             */
            list = realm.where(Chat.class)
                    .equalTo(FIELD_CHANNEL_ID, conversationId)
                    .sort(FIELD_DATE, Sort.DESCENDING)
                    .limit(LIMIT)
                    .findAll();
        }

        if (list != null) {
            list = realm.copyFromRealm(list);
            sortMessageByDateASC(list);
        }
        realm.close();

        return list;
    }

    public static List<Chat> queryFromNewerMessageToOldMessage(Context context, Chat newerChat, Chat olderChat) {
        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);
           /*
        Will ignore newer message from the list. In this context, we just want to load from a specific
        newer message to a specific older message range to be added into chat list which that newer
        message was already presented.
         */
        List<Chat> list = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, newerChat.getChannelId())
                .between(FIELD_DATE, olderChat.getDate(), newerChat.getDate())
                .notEqualTo(FIELD_ID, newerChat.getId())
                .sort(FIELD_DATE, Sort.ASCENDING)
                .findAll();
        if (list != null) {
            list = realm.copyFromRealm(list);
        }

        realm.close();

        return list;
    }


    public static List<Chat> queryChatListForChatScreen(Context context,
                                                        String conversationId,
                                                        int limit,
                                                        boolean refreshDatabase) {
        Realm realm = RealmDb.getInstance(context);
        if (refreshDatabase) {
            RealmDb.refresh(realm);
        }
        List<Chat> list = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, conversationId)
                .sort(FIELD_DATE, Sort.DESCENDING)
                .limit(limit)
                .findAll();
        list = realm.copyFromRealm(list);
        realm.close();
        return list;
    }

    public static List<Chat> queryLoadMore(Context context, Chat fromMessage) {
        Realm realm = RealmDb.getInstance(context);
        List<Chat> list = new ArrayList<>();
        Date earliestDateInDb = realm.where(Chat.class).minimumDate(FIELD_DATE);
        if (earliestDateInDb != null && !earliestDateInDb.equals(fromMessage.getDate())) {
            list = realm.where(Chat.class)
                    .equalTo(FIELD_CHANNEL_ID, fromMessage.getChannelId())
                    .sort(FIELD_DATE, Sort.DESCENDING)
                    .between(FIELD_DATE, earliestDateInDb, fromMessage.getDate())
                    .notEqualTo(FIELD_ID, fromMessage.getId())
                    .limit(50)
                    .findAll();
            list = realm.copyFromRealm(list);
        }
        realm.close();
        return list;
    }

    public static List<Chat> queryLoadMoreFromOldToNew(Context context, Chat fromMessage, int limit) {
        Realm realm = RealmDb.getInstance(context);
        List<Chat> list = new ArrayList<>();
        RealmResults<Chat> results = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, fromMessage.getChannelId())
                .notEqualTo(FIELD_ID, fromMessage.getId())
                .sort(FIELD_DATE, Sort.ASCENDING)
                .greaterThanOrEqualTo(FIELD_DATE, fromMessage.getDate())
                .limit(limit)
                .findAll();

        if (results != null) {
            list = realm.copyFromRealm(results);
        }
        realm.close();
        return list;
    }

    public static boolean isHasMoreLocalChat(Context context, Chat fromMessage) {
        Realm realm = RealmDb.getInstance(context);
        Date earliestDateInDb = realm.where(Chat.class).minimumDate(FIELD_DATE);
        if (earliestDateInDb != null &&
                fromMessage.getDate() != null &&
                !earliestDateInDb.equals(fromMessage.getDate())) {
            long count = realm.where(Chat.class)
                    .equalTo(FIELD_CHANNEL_ID, fromMessage.getChannelId())
                    .sort(FIELD_DATE, Sort.DESCENDING)
                    .between(FIELD_DATE, earliestDateInDb, fromMessage.getDate())
                    .notEqualTo(FIELD_ID, fromMessage.getId())
                    .count();
            realm.close();
            return count > 0;
        }
        realm.close();

        return false;
    }

    public static boolean isHasMoreLocalLatestMessage(Context context, Chat fromMessage) {
        Realm realm = RealmDb.getInstance(context);
        long count = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, fromMessage.getChannelId())
                .sort(FIELD_DATE, Sort.DESCENDING)
                .greaterThanOrEqualTo(FIELD_DATE, fromMessage.getDate())
                .notEqualTo(FIELD_ID, fromMessage.getId())
                .count();
        realm.close();
        return count > 0;
    }

    public static List<Chat> queryUnreadMessageForNotificationDisplay(Context context, String channelId) {
        Realm realm = RealmDb.getInstance(context);
        List<Chat> list = realm.where(Chat.class)
                .in(FIELD_STATUS, new String[]{MessageState.SENT.getValue(),
                        MessageState.DELIVERED.getValue()})
                .notEqualTo(FIELD_SENDER_ID, SharedPrefUtils.getUserId(context))
                .equalTo(FIELD_CHANNEL_ID, channelId)
                .limit(MINIMUM_UNREAD)
                .findAll();
        list = realm.copyFromRealm(list);

        realm.close();

        return list;
    }

    public static List<Chat> queryByStatusAndUserId(Context context,
                                                    MessageState status,
                                                    String senderId) {
        Realm realm = RealmDb.getInstance(context);
        List<Chat> list = realm.where(Chat.class)
                .equalTo(FIELD_STATUS, status.getValue())
                .equalTo(FIELD_SENDER_ID, senderId)
                .findAll();
        list = realm.copyFromRealm(list);
        realm.close();
        User user = SharedPrefUtils.getUser(context);
        for (Chat chat : list) {
            chat.setSender(user);
        }
        return list;
    }

    public static List<Chat> queryMessageToSendDeliverStatus(Context context) {
        Realm realm = RealmDb.getInstance(context);
        User user = SharedPrefUtils.getUser(context);
        List<Chat> list = realm.where(Chat.class)
                .equalTo(FIELD_STATUS, MessageState.SENT.getValue())
                .notEqualTo(FIELD_SENDER_ID, user.getId())
                .findAll();
        list = realm.copyFromRealm(list);
//        //Must set current user as sender of the deliver status message.
//        for (Chat chat : list) {
//            chat.setSender(user);
//            chat.setSenderId(user.getId());
//        }
        realm.close();

        return list;
    }

    public static List<Chat> updatePreviousMessageStatusToSeen(Context context,
                                                               Chat chat,
                                                               String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        List<Chat> chats;
        /*
        Some messages status from server are SENT and SENDING. So we need to check both.
         */
        String[] checkTypes = new String[]{MessageState.SENT.getValue(),
                MessageState.DELIVERED.getValue(),
                MessageState.SENDING.getValue()};
        Date earliestChat = realm.where(Chat.class).equalTo(FIELD_CHANNEL_ID, conversationId).minimumDate(FIELD_DATE);
        chats = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, conversationId)
                .in(FIELD_STATUS, checkTypes)
                .sort(FIELD_DATE, Sort.DESCENDING)
                .between(FIELD_DATE, earliestChat, chat.getDate())
                .findAll();

        chats = realm.copyFromRealm(chats);

        for (Chat chatItem : chats) {
            chatItem.setStatus(MessageState.SEEN);
        }
        Collections.sort(chats);
        realm.beginTransaction();
        realm.insertOrUpdate(chats);

        //update conversation as read true
        RealmResults<Conversation> list = realm.where(Conversation.class)
                .equalTo(FIELD_ID, conversationId)
                .findAll();
        realm.commitTransaction();
        realm.close();
        return chats;
    }

    public static List<Chat> updatePreviousMessageToDelivered(Context context, Chat chat, String conversationId) {
        Realm realm = RealmDb.getInstance(context);

        List<Chat> chats;
        Date earliestChat = realm.where(Chat.class).equalTo(FIELD_CHANNEL_ID, conversationId).minimumDate(FIELD_DATE);
        chats = realm.where(Chat.class)
                .beginGroup()
                .equalTo(FIELD_STATUS, MessageState.SENT.getValue())
                .equalTo(FIELD_CHANNEL_ID, conversationId)
                .endGroup()
                .and()
                .beginGroup()
                .between(FIELD_DATE, earliestChat, chat.getDate())
                .or()
                .equalTo(FIELD_ID, chat.getId())
                .endGroup()
                .sort(FIELD_DATE, Sort.DESCENDING)
                .findAll();

        chats = realm.copyFromRealm(chats);

        for (Chat chatItem : chats) {
            chatItem.setStatus(MessageState.DELIVERED);
        }
        Collections.sort(chats);
        realm.beginTransaction();
        realm.insertOrUpdate(chats);

        realm.commitTransaction();
        realm.close();
        return chats;
    }

    public static Observable<List<Chat>> queryLocalWithPagination(Context context,
                                                                  String query,
                                                                  GroupDetail groupDetail,
                                                                  Chat lastOldChat,
                                                                  int limit) {
        return Observable.create(e -> {
            Realm realm = RealmDb.getInstance(context);
            RealmDb.refresh(realm);
            RealmQuery<Chat> realmQuery = realm.where(Chat.class)
                    .equalTo(FIELD_CHANNEL_ID, groupDetail.getId())
                    .and()
                    .contains(FIELD_CONTENT, query, Case.INSENSITIVE);
            if (lastOldChat != null) {
                Date earliestChat = realm.where(Chat.class).equalTo(FIELD_CHANNEL_ID, groupDetail.getId()).minimumDate(FIELD_DATE);
                if (earliestChat != null) {
                    Timber.i("queryLocalWithPagination with earliestChat");
                    realmQuery = realmQuery
                            .and()
                            .between(FIELD_DATE, earliestChat, lastOldChat.getDate())
                            .and()
                            .notEqualTo(FIELD_ID, lastOldChat.getId());
                }
            }
            realmQuery = realmQuery
                    .sort(FIELD_DATE, Sort.DESCENDING)
                    .limit(limit);
            List<Chat> list = realmQuery.findAll();
            if (list == null) {
                list = new ArrayList<>();
            } else {
                list = realm.copyFromRealm(list);
            }
            realm.close();
            injectMessageSenderOfGroupConversation(groupDetail.getParticipant(), list);
            e.onNext(list);
            e.onComplete();
        });
    }

    private static void injectMessageSenderOfGroupConversation(List<User> participantList, List<Chat> chats) {
        if (participantList != null && chats != null) {
            for (Chat chat : chats) {
                for (User user : participantList) {
                    if (TextUtils.equals(chat.getSenderId(), user.getId())) {
                        chat.setSender(user);
                        break;
                    }
                }
            }
        }
    }

    public static void addSendingReadStatusMessageOfCurrentUser(Context context, Chat readMessage) {
        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);
        /*
            There will be only one read message of a conversation sent from current user and normally
            that message is the last message of conversation. So before adding to send read list,
            we have to clear the previous message if any of the same conversation.
         */
        RealmResults<SendReadStatusJunkMessage> result = realm.where(SendReadStatusJunkMessage.class).equalTo("mConversationId",
                readMessage.getChannelId()).findAll();
        if (result != null && !result.isEmpty()) {
            realm.beginTransaction();
            result.deleteAllFromRealm();
            realm.commitTransaction();
        }

        //Add send read status message
        SendReadStatusJunkMessage sendReadStatusJunkMessage = new SendReadStatusJunkMessage(readMessage.getId(),
                readMessage.getChannelId());
        realm.beginTransaction();
        realm.insertOrUpdate(sendReadStatusJunkMessage);
        realm.commitTransaction();

        realm.close();
    }

    public static void removeSendingReadStatusMessageOfCurrentUser(Context context, String messageId) {
        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);
        RealmResults<SendReadStatusJunkMessage> result = realm.where(SendReadStatusJunkMessage.class)
                .equalTo("mMessageId", messageId).findAll();
        if (result != null && !result.isEmpty()) {
            realm.beginTransaction();
            result.deleteAllFromRealm();
            realm.commitTransaction();
        }
        realm.close();
    }

    public static List<SendReadStatusJunkMessage> getSendingReadStatusMessageOfCurrentUser(Context context) {
        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);
        RealmResults<SendReadStatusJunkMessage> result = realm.where(SendReadStatusJunkMessage.class)
                .findAll();
        if (result != null && !result.isEmpty()) {
            List<SendReadStatusJunkMessage> copyResult = realm.copyFromRealm(result);
            realm.close();
            return copyResult;
        } else {
            realm.close();
            return new ArrayList<>();
        }
    }

    public static long getSendingReadStatusMessageOfCurrentUserCount(Context context) {
        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);
        long count = realm.where(SendReadStatusJunkMessage.class).count();
        realm.close();

        return count;
    }

    public static List<Chat> buildSendingReadStatusOfCurrentUser(Context context) {
        List<SendReadStatusJunkMessage> sendReadStatusMessages = getSendingReadStatusMessageOfCurrentUser(context);
        if (!sendReadStatusMessages.isEmpty()) {
            String[] messageIds = new String[sendReadStatusMessages.size()];
            for (int i = 0; i < sendReadStatusMessages.size(); i++) {
                messageIds[i] = sendReadStatusMessages.get(i).getMessageId();
            }

            Realm realm = RealmDb.getInstance(context);
            RealmDb.refresh(realm);
            RealmResults<Chat> localChatList = realm.where(Chat.class).in("mId", messageIds).findAll();
            if (localChatList != null && !localChatList.isEmpty()) {
                User user = SharedPrefUtils.getUser(context);
                List<Chat> sendReadStatusChatList = new ArrayList<>();
                for (Chat chat : localChatList) {
                    final Chat newChat = Chat.newCloneDateInstance(chat);
                    newChat.setId(chat.getId());
                    newChat.setSendingType(Chat.SendingType.READ_STATUS);
                    newChat.setSender(user);
                    newChat.setChannelId(chat.getChannelId());
                    newChat.setGroup(chat.isGroup());
                    newChat.setSenderId(user.getId());
                    if (chat.isGroup()) {

                        newChat.setSendTo(chat.getChannelId());
                    } else {
                        newChat.setSendTo(chat.getSenderId());
                    }
                    sendReadStatusChatList.add(newChat);
                }

                return sendReadStatusChatList;
            }
        }

        return new ArrayList<>();
    }
}
