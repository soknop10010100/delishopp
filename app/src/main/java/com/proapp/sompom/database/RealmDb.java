package com.proapp.sompom.database;

import android.content.Context;

import com.proapp.sompom.database.modules.AppModule;
import com.proapp.sompom.helper.SentryHelper;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by imac on 6/20/17.
 */

public final class RealmDb {

    private RealmDb() {
    }

    public static Realm getInstance(Context context) {
        Realm realm;
        try {
            RealmConfiguration appConfig = new RealmConfiguration.Builder()
                    .name("app.realm")
                    .modules(new AppModule())
                    .schemaVersion(ClientRealmMigration.SCHEMA_VERSION)
                    .migration(new ClientRealmMigration())
                    .build();
            realm = Realm.getInstance(appConfig);
        } catch (IllegalStateException ex) {
            Realm.init(context.getApplicationContext());
            realm = Realm.getDefaultInstance();
            SentryHelper.logSentryError(ex);
        }

        return realm;
    }

    public static void refresh(Realm realm) {
        if (!realm.isInTransaction()) {
            realm.refresh();
        }
    }
}
