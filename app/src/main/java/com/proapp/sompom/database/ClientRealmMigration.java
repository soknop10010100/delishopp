package com.proapp.sompom.database;

import androidx.annotation.Nullable;

import com.proapp.sompom.model.LastMessageIdTemp;

import java.util.Date;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 3/13/2020.
 */
public class ClientRealmMigration implements RealmMigration {

    public static final int SCHEMA_VERSION = 34;

    @Override
    public void migrate(DynamicRealm realm1, long oldVersion, long newVersion) {
        Timber.i("oldVersion: " + oldVersion + ", newVersion: " + newVersion);
        if (oldVersion == 0) {
            RealmObjectSchema linkPreviewModelSchema = realm1.getSchema().get("LinkPreviewModel");
            if (linkPreviewModelSchema != null) {
                linkPreviewModelSchema.addField("mId", String.class);
                linkPreviewModelSchema.addField("mIsLinkDownloaded", Boolean.class);
                linkPreviewModelSchema.addField("mResourceContent", String.class);

                //Delete all previous LinkPreview data
                realm1.delete(linkPreviewModelSchema.getClassName());
            }

            RealmObjectSchema commentTempSchema = realm1.getSchema().get("CommentTemp");
            if (commentTempSchema != null) {
                commentTempSchema.addField("mPublished", Date.class);
                realm1.delete(commentTempSchema.getClassName());
            }

            oldVersion++;
        }

        if (oldVersion == 3) {
            RealmObjectSchema schema = realm1.getSchema().get("CommentTemp");
            if (schema != null) {
                schema.addField("mMainCommentId", String.class);
                realm1.delete(schema.getClassName());
            }
            oldVersion++;
        }

        if (oldVersion == 4) {
            RealmObjectSchema schema = realm1.getSchema().get("LifeStreamTemp");
            if (schema != null) {
                schema.addField("mContentId", String.class);
                realm1.delete(schema.getClassName());
            }
            oldVersion++;
        }

        if (oldVersion == 5) {
            RealmObjectSchema schema = realm1.getSchema().get("Chat");
            if (schema != null) {
                schema.addField("mIsSavedFromSeenStatus", boolean.class);
            }
            oldVersion++;
        }

        if (oldVersion == 6) {
            RealmObjectSchema linkPreviewModelSchema = realm1.getSchema().get("LinkPreviewModel");
            if (linkPreviewModelSchema != null) {
                linkPreviewModelSchema.addField("mType", String.class);
                linkPreviewModelSchema.addField("mShouldPreview", Boolean.class);

                //Delete all previous LinkPreview data
                realm1.delete(linkPreviewModelSchema.getClassName());
            }
            oldVersion++;
        }

        if (oldVersion == 7) {
            RealmObjectSchema linkPreviewModelSchema = realm1.getSchema().get("LinkPreviewModel");
            if (linkPreviewModelSchema != null) {
                linkPreviewModelSchema.addField("mFileName", String.class);
                linkPreviewModelSchema.addField("mFileExtension", String.class);
                linkPreviewModelSchema.addField("mFileType", String.class);
                linkPreviewModelSchema.addField("mWidth", Integer.class);
                linkPreviewModelSchema.addField("mHeight", Integer.class);

                //Delete all previous LinkPreview data
                realm1.delete(linkPreviewModelSchema.getClassName());
            }
            oldVersion++;
        }

        if (oldVersion == 8) {
            RealmObjectSchema chatModelSchema = realm1.getSchema().get("Chat");
            if (chatModelSchema != null) {
                chatModelSchema.addField("mIsEncrypted", boolean.class);

                realm1.delete(chatModelSchema.getClassName());
            }

            oldVersion++;
        }

        if (oldVersion == 9) {
            RealmObjectSchema conversationModelSchema = realm1.getSchema().get("Conversation");
            if (conversationModelSchema != null) {
                conversationModelSchema.addField("mVirgilGroupOwner", String.class);

                realm1.delete(conversationModelSchema.getClassName());
            }

            oldVersion++;
        }

        if (oldVersion == 10) {
            RealmObjectSchema chatModelSchema = realm1.getSchema().get("Chat");
            if (chatModelSchema != null) {
                chatModelSchema.addField("mIsDecrypted", boolean.class);

                realm1.delete(chatModelSchema.getClassName());
            }

            oldVersion++;
        }

        if (oldVersion == 11) {
            RealmObjectSchema conversationModelSchema = realm1.getSchema().get("Conversation");
            if (conversationModelSchema != null) {
                conversationModelSchema.addField("mIsDeleted", boolean.class);

                realm1.delete(conversationModelSchema.getClassName());
            }

            oldVersion++;
        }

        if (oldVersion == 12) {
            RealmObjectSchema conversationModelSchema = realm1.getSchema().get("Conversation");
            if (conversationModelSchema != null) {
                conversationModelSchema.addField("temp_id", String.class)
                        .transform(obj -> {
                            String bool;
                            if (obj.getBoolean("mIsDeleted")) {
                                bool = "true";
                            } else {
                                bool = "false";
                            }
                            obj.setString("temp_id", bool);
                        })
                        .removeField("mIsDeleted")
                        .renameField("temp_id", "mIsDeleted");
            }

            oldVersion++;
        }

        if (oldVersion == 13) {
            //Create MetaGroup Schema
            realm1.getSchema().create("MetaGroup")
                    .addField("mName", String.class)
                    .addField("mProfilePhoto", String.class)
                    .addRealmListField("mParticipants", realm1.getSchema().get("User"));

            RealmObjectSchema metaGroupSchema = realm1.getSchema().get("MetaGroup");
            if (metaGroupSchema != null) {
                RealmObjectSchema chatSchema = realm1.getSchema().get("Chat");
                if (chatSchema != null) {
                    chatSchema.addRealmObjectField("mMetaGroup", metaGroupSchema);
                }
            }

            oldVersion++;
        }

        if (oldVersion == 14) {
            RealmObjectSchema userSchema = realm1.getSchema().get("User");
            if (userSchema != null) {
                RealmObjectSchema chatSchema = realm1.getSchema().get("Chat");
                if (chatSchema != null) {
                    chatSchema.addRealmObjectField("mSender", userSchema);
                }
            }

            RealmObjectSchema conversationSchema = realm1.getSchema().get("Conversation");
            if (conversationSchema != null) {
                conversationSchema.addField("mIsRemoved", Boolean.class);
            }

            oldVersion++;
        }

        if (oldVersion == 15) {
            RealmObjectSchema conversationModelSchema = realm1.getSchema().get("Conversation");
            if (conversationModelSchema != null) {
                conversationModelSchema.addField("mUnreadCount", int.class);

                realm1.delete(conversationModelSchema.getClassName());

            }

            oldVersion++;
        }

        if (oldVersion == 16) {
            //Create ReferencedChat Schema
            realm1.getSchema().create("ReferencedChat")
                    .addField("mId", String.class)
                    .addField("mContent", String.class)
                    .addField("mDate", Date.class)
                    .addRealmObjectField("mSender", realm1.getSchema().get("User"))
                    .addField("mSenderId", String.class)
                    .addRealmObjectField("mMedia", realm1.getSchema().get("Media"));

            //Create ChatReferenceWrapper Schema
            realm1.getSchema().create("ChatReferenceWrapper")
                    .addRealmObjectField("mReferencedChat", realm1.getSchema().get("ReferencedChat"));

            //Add Chat Reference Wrapper to Chat Schema
            RealmObjectSchema chatSchema = realm1.getSchema().get("Chat");
            if (chatSchema != null) {
                RealmObjectSchema chatReferenceWrapperSchema = realm1.getSchema().get("ChatReferenceWrapper");
                if (chatReferenceWrapperSchema != null) {
                    chatSchema.addRealmObjectField("mChatReferenceWrapper", chatReferenceWrapperSchema);
                }
            }

            oldVersion++;
        }

        if (oldVersion == 17) {
            //Create LastMessageIdTemp Schema
            realm1.getSchema().create("LastMessageIdTemp")
                    .addField(LastMessageIdTemp.FILED_ID, String.class, FieldAttribute.PRIMARY_KEY)
                    .addField(LastMessageIdTemp.FILED_LAST_MESSAGE_ID, String.class)
                    .addField(LastMessageIdTemp.FILED_LAST_MESSAGE_DATE, Date.class);

            oldVersion++;
        }

        if (oldVersion == 18) {
            //Add "mIsEdited" and "mLastEditedDate" to Chat schema
            RealmObjectSchema chatModelSchema = realm1.getSchema().get("Chat");
            if (chatModelSchema != null) {
                chatModelSchema.addField("mIsEdited", Boolean.class);
                chatModelSchema.addField("mLastEditedDate", Date.class);
            }

            oldVersion++;
        }

        if (oldVersion == 19) {
            RealmObjectSchema schema = realm1.getSchema().get("LastMessageIdTemp");
            if (schema != null) {
                schema.addField("mShouldRequestHistoryFromServer", Boolean.class);
            }

            oldVersion++;
        }

        if (oldVersion == 20) {
            RealmObjectSchema chatModelSchema = realm1.getSchema().get("Chat");
            if (chatModelSchema != null) {
                chatModelSchema.addField("mCallDuration", Integer.class);
            }

            oldVersion++;
        }

        if (oldVersion == 21) {
            RealmObjectSchema mediaModelSchema = realm1.getSchema().get("Media");
            if (mediaModelSchema != null) {
                mediaModelSchema.removeField("mIsLike");
            }

            oldVersion++;
        }

        if (oldVersion == 22) {
            //Refactor how notification list save on local here.

            //1.Remove some Realm class here which no longer use.
            realm1.getSchema().remove("NotificationUser");
            realm1.getSchema().remove("NotificationProduct");
            realm1.getSchema().remove("NotificationGeneral");
            realm1.getSchema().remove("NotificationComment");
            realm1.getSchema().remove("NotificationTimeline");
            realm1.getSchema().remove("LifeStreamTemp");
            realm1.getSchema().remove("ProductTemp");
            realm1.getSchema().remove("MediaTemp");
            realm1.getSchema().remove("CommentTemp");
            realm1.getSchema().remove("UserTemp");
            realm1.getSchema().remove("GeneralObjectTemp");

            //2. Add some new notification Realm classes
            //Add NotificationActor
            realm1.getSchema().create("NotificationActor")
                    .addField("mId", String.class, FieldAttribute.PRIMARY_KEY)
                    .addField("mUserProfile", String.class)
                    .addField("mFirstName", String.class)
                    .addField("mLastName", String.class);
            //Add NotificationObject
            realm1.getSchema().create("NotificationObject")
                    .addField("mPostId", String.class)
                    .addField("mMediaId", String.class)
                    .addField("mCommentId", String.class)
                    .addField("mSubCommentId", String.class);

            //Add NotificationObject
            realm1.getSchema().create("Notification")
                    .addField("mId", String.class, FieldAttribute.PRIMARY_KEY)
                    .addField("mVerb", String.class)
                    .addField("mDate", Date.class)
                    .addField("mCreatedAt", Date.class)
                    .addField("mIsNew", boolean.class)
                    .addRealmListField("mActors", realm1.getSchema().get("NotificationActor"))
                    .addRealmListField("mObjects", realm1.getSchema().get("NotificationObject"));

            oldVersion++;
        }

        if (oldVersion == 23) {
            RealmObjectSchema realmObjectSchema = realm1.getSchema().get("NotificationObject");
            if (realmObjectSchema != null) {
                realmObjectSchema.addField("mConversionId", String.class);
                realmObjectSchema.addField("mConversionName", String.class);
            }

            oldVersion++;
        }

        if (oldVersion == 24) {
            RealmObjectSchema realmObjectSchema = realm1.getSchema().get("User");
            if (realmObjectSchema != null) {
                realmObjectSchema.addField("mHash", Integer.class);
            }

            oldVersion++;
        }

        if (oldVersion == 25) {
            RealmObjectSchema realmObjectSchema = realm1.getSchema().get("User");
            if (realmObjectSchema != null) {
                realmObjectSchema.addField("mLastActivity", Date.class);
            }

            oldVersion++;
        }

        if (oldVersion == 26) {
            RealmObjectSchema conversationModelSchema = realm1.getSchema().get("Conversation");
            if (conversationModelSchema != null) {
                conversationModelSchema.removeField("mBackUpStatusSeenConversation");
            }

            oldVersion++;
        }

        if (oldVersion == 27) {
            RealmObjectSchema conversationModelSchema = realm1.getSchema().get("Conversation");
            if (conversationModelSchema != null) {
                conversationModelSchema.addField("mType", String.class);
            }

            oldVersion++;
        }

        if (oldVersion == 28) {
            /*
              @link {SendReadStatusJunkMessage} schema
             */
            realm1.getSchema().create("SendReadStatusJunkMessage")
                    .addField("mMessageId", String.class, FieldAttribute.PRIMARY_KEY)
                    .addField("mConversationId", String.class);

            oldVersion++;
        }

        if (oldVersion == 29) {
            RealmObjectSchema conversationModelSchema = realm1.getSchema().get("Conversation");
            if (conversationModelSchema != null) {
                conversationModelSchema.removeField("mVirgilGroupOwner");
                conversationModelSchema.addField("mLastMessageId", String.class);
            }

            oldVersion++;
        }

        if (oldVersion == 30) {
            RealmObjectSchema conversationModelSchema = realm1.getSchema().get("Conversation");
            if (conversationModelSchema != null) {
                conversationModelSchema.addField("mReplyTextNotice", String.class);
            }

            oldVersion++;
        }

        if (oldVersion == 31) {
            RealmObjectSchema notificationObjectSchema = realm1.getSchema().get("NotificationObject");
            if (notificationObjectSchema != null) {
                notificationObjectSchema.addField("mOrderId", String.class);
            }

            realm1.getSchema().create("ConciergeShop")
                    .addField("mId", String.class)
                    .addField("mName", String.class)
                    .addField("mLogo", String.class);

            RealmObjectSchema notificationSchema = realm1.getSchema().get("Notification");
            if (notificationSchema != null) {
                notificationSchema.addRealmListField("mShops", realm1.getSchema().get("ConciergeShop"));
            }

            oldVersion++;
        }

        if (oldVersion == 32) {
            RealmObjectSchema userSchema = realm1.getSchema().get("User");
            if (userSchema != null) {
                userSchema.addField("mOriginalUserProfile", String.class);
            }

            oldVersion++;
        }

        if (oldVersion == 33) {
            realm1.getSchema().create("TelegramConnection")
                    .addField("mId", String.class, FieldAttribute.PRIMARY_KEY)
                    .addField("mUserId", String.class);

            RealmObjectSchema conversationSchema = realm1.getSchema().get("Conversation");
            RealmObjectSchema telegramConnectionSchema = realm1.getSchema().get("TelegramConnection");
            if (conversationSchema != null && telegramConnectionSchema != null) {
                Timber.i("MIGRATION COMPLETED");
                conversationSchema.addRealmObjectField("mTelegramConnection", telegramConnectionSchema);
            }

            oldVersion++;
        }
    }

    @Override
    public int hashCode() {
        //To make sure there will once type of instance to use with Realm migration configuration.
        return 0x00111;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return obj != null && hashCode() == obj.hashCode();
    }
}
