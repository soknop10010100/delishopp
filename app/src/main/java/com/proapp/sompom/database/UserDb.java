package com.proapp.sompom.database;

import android.content.Context;

import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.result.User;

import io.realm.Realm;
import timber.log.Timber;

public final class UserDb {

    private UserDb() {
    }

    public static void saveDownloadedMedia(Context context, Media media) {
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        realm.insertOrUpdate(media);
        realm.commitTransaction();
        realm.close();
    }

    public static void updateUserHash(Context context, User user) {
        Realm realm = RealmDb.getInstance(context);
        User userFromRealm = realm.where(User.class)
                .equalTo("mId", user.getId())
                .findFirst();
        if (userFromRealm != null) {
            realm.beginTransaction();
            userFromRealm.setHash(user.getHash());
            userFromRealm.setUserProfileThumbnail(user.getUserProfileThumbnail());
            realm.insertOrUpdate(userFromRealm);
            realm.commitTransaction();
            Timber.i("Updated user hash of " + user.getFullName() + " in local db.");
        }
        realm.close();
    }
}
