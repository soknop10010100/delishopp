package com.proapp.sompom.database;

import android.content.Context;

import com.proapp.sompom.model.LastMessageIdTemp;
import com.proapp.sompom.model.result.Chat;

import io.realm.Realm;
import io.realm.RealmResults;
import timber.log.Timber;

public final class LastMessageIdTempDb {

    private LastMessageIdTempDb() {
    }

    public static void saveLastMessageId(Context context, Chat lastMessage, boolean willOverwrite) {
        /*
        Only one record will be saved.
         */
        if (lastMessage != null) {
            boolean willSave = false;
            if (willOverwrite) {
                willSave = true;
            } else if (getLastMessageId(context, lastMessage.getChannelId()) == null) {
                willSave = true;
            }

            if (willSave) {
                LastMessageIdTemp temp = new LastMessageIdTemp();
                temp.setConversationId(lastMessage.getChannelId());
                temp.setLastMessageId(lastMessage.getId());
                temp.setLastMessageDate(lastMessage.getDate());
                Realm realm = RealmDb.getInstance(context);
                realm.beginTransaction();
                realm.insertOrUpdate(temp);
                realm.commitTransaction();
                realm.close();
//                Timber.i("Save last temp message id success: conversationId: " + lastMessage.getContent() + ", id: " + lastMessage.getId());
            }
        }
    }

    public static void saveLastLocalMessageIdForGettingAllChatHistoryLater(Context context, String conversationId) {
        LastMessageIdTemp temp = new LastMessageIdTemp();
        temp.setConversationId(conversationId);
        temp.setShouldRequestHistoryFromServer(true);
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        realm.insertOrUpdate(temp);
        realm.commitTransaction();
        realm.close();
    }

    public static boolean shouldRequestAllChatHistory(Context context, String conversationId) {
        LastMessageIdTemp lastMessageId = getLastMessageId(context, conversationId);
        return lastMessageId != null && lastMessageId.getShouldRequestHistoryFromServer();
    }

    public static void removeLastMessageId(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        RealmResults<LastMessageIdTemp> temps = realm.where(LastMessageIdTemp.class)
                .equalTo(LastMessageIdTemp.FILED_ID, conversationId)
                .findAll();
        if (temps != null) {
            realm.beginTransaction();
            temps.deleteAllFromRealm();
            realm.commitTransaction();
            Timber.i("Remove last temp message id success: conversationId: " + conversationId);
        }
        realm.close();
    }

    public static LastMessageIdTemp getLastMessageId(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);
        LastMessageIdTemp temp = realm.where(LastMessageIdTemp.class)
                .equalTo(LastMessageIdTemp.FILED_ID, conversationId)
                .findFirst();
        if (temp != null) {
            temp = realm.copyFromRealm(temp);
        }
        realm.close();
        return temp;
    }
}
