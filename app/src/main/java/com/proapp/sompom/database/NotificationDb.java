package com.proapp.sompom.database;

import android.content.Context;

import com.proapp.sompom.model.notification.Notification;
import com.proapp.sompom.model.notification.NotificationAdaptive;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public final class NotificationDb {

    private static final int LIMIT = 200;

    private NotificationDb() {
    }

    public static void save(Context context, List<Notification> notificationList) {
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        realm.insertOrUpdate(notificationList);
        realm.commitTransaction();
        realm.close();
    }

    public static List<NotificationAdaptive> query(Context context) {
        List<NotificationAdaptive> data = new ArrayList<>();
        Realm realm = RealmDb.getInstance(context);
        RealmResults<Notification> notifications = realm.where(Notification.class).limit(LIMIT).findAll();
        if (notifications != null && !notifications.isEmpty()) {
            data.addAll(realm.copyFromRealm(notifications));
        }
        Collections.sort(data, (o1, o2) -> Long.compare(o2.getPublished().getTime(), o1.getPublished().getTime()));
        realm.close();
        return data;
    }
}
