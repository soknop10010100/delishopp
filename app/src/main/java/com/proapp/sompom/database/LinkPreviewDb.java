package com.proapp.sompom.database;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.proapp.sompom.model.LinkPreviewAdaptive;
import com.proapp.sompom.model.result.LinkPreviewModel;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import timber.log.Timber;

public final class LinkPreviewDb {

    private LinkPreviewDb() {
    }

    public static void printAll(Context context) {
        Realm realm = RealmDb.getInstance(context);
        Timber.i("All: " + new Gson().toJson(realm.copyFromRealm(realm.where(LinkPreviewModel.class).findAll())));
    }

    public static List<LinkPreviewModel> findLocalData(Context context, String[] checkingIds,
                                                       String[] checkingResources) {
        List<LinkPreviewModel> validResult = new ArrayList<>();
        Realm realm = RealmDb.getInstance(context);
        RealmResults<LinkPreviewModel> result = realm.where(LinkPreviewModel.class)
                .in("mId", checkingIds)
                .and()
                .in("mResourceContent", checkingResources)
                .findAll();
        if (result != null) {
            validResult = realm.copyFromRealm(result);
        }
        realm.close();

        return validResult;
    }

    public static <T extends LinkPreviewAdaptive> void parseCheckingData(List<T> linkPreviewAdaptiveList,
                                                                         LinkPreviewDbListener listener) {
        String[] ids = new String[linkPreviewAdaptiveList.size()];
        String[] previews = new String[linkPreviewAdaptiveList.size()];
        for (int i = 0; i < linkPreviewAdaptiveList.size(); i++) {
            ids[i] = linkPreviewAdaptiveList.get(i).getId();
            previews[i] = linkPreviewAdaptiveList.get(i).getPreviewContent();
        }

        if (listener != null) {
            listener.onParseDataSuccess(ids, previews);
        }
    }

    public static void save(Context context,
                            LinkPreviewModel linkPreviewModel,
                            boolean ignoreLogo) {
        //Since preview logo is the most important, we decide to save only if the logo present.
        if (linkPreviewModel != null &&
                !TextUtils.isEmpty(linkPreviewModel.getId()) &&
                (ignoreLogo || !TextUtils.isEmpty(linkPreviewModel.getImage()))) {
            Timber.i("Save link: preview: " + new Gson().toJson(linkPreviewModel));
            Realm realm = RealmDb.getInstance(context);
            realm.beginTransaction();
            realm.insertOrUpdate(linkPreviewModel);
            realm.commitTransaction();
            realm.close();
        }
    }

    public static void remove(Context context, String chatId) {
        Realm realm = RealmDb.getInstance(context);
        RealmResults<LinkPreviewModel> results = realm.where(LinkPreviewModel.class).equalTo("mId",
                chatId).findAll();
        if (results != null && !results.isEmpty()) {
            realm.beginTransaction();
            results.deleteAllFromRealm();
            realm.commitTransaction();
        }
        realm.close();
    }

    public static class LinkPreviewDbListener {

        protected void onParseDataSuccess(String[] ids, String[] previewContents) {
            //Empty Impl...
        }
    }
}
