package com.proapp.sompom.database.modules;

import io.realm.annotations.RealmModule;

/**
 * Created by He Rotha on 2019-09-12.
 */

@RealmModule(allClasses = true)
public class AppModule {
}
