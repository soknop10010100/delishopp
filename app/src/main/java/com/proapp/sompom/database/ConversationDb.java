package com.proapp.sompom.database;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.listener.OnChatSocketListener;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.services.datamanager.AllMessageDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import timber.log.Timber;

/**
 * Created by He Rotha on 10/30/18.
 */
public final class ConversationDb {

    private static final int PAGINATION_SIZE = AllMessageDataManager.DEFAULT_PAGINATION_SIZE;

    private ConversationDb() {
    }

    public static void checkToRemoveEmptyConversationIfNecessary(Context context, String conversationId) {
        Conversation conversationById = getConversationById(context, conversationId);
        if (conversationById != null) {
            long messageCountForAllStatus = MessageDb.getMessageCountForAllStatus(context, conversationId);
            if (messageCountForAllStatus == 0) {
//                Timber.i("Will remove empty conversation with id: " + conversationId);
                Realm realm = RealmDb.getInstance(context);
                RealmResults<Conversation> realmResults = realm.where(Conversation.class)
                        .equalTo("mId", conversationId)
                        .findAll();
                if (realmResults != null && !realmResults.isEmpty()) {
                    realm.beginTransaction();
                    realmResults.deleteAllFromRealm();
                    realm.commitTransaction();
                }
                realm.close();
            }
        }
    }

    public static void save(Context context, Conversation conversation, String tag) {
//        Timber.i("save conversation: tag: " + tag + ", data: " + new Gson().toJson(conversation));
        if (conversation != null) {
            setLastMessageToConversationBeforeSaving(Collections.singletonList(conversation));
            checkToSetConversationContentBeforeSavingIfNecessary(context, conversation);
            Realm realm = RealmDb.getInstance(context);
            realm.beginTransaction();
            realm.insertOrUpdate(conversation);
            realm.commitTransaction();
            realm.close();
        }
    }

    private static void setLastMessageToConversationBeforeSaving(List<Conversation> conversationList) {
        if (conversationList != null && !conversationList.isEmpty()) {
            for (Conversation conversation : conversationList) {
                if (conversation != null && conversation.getLastMessage() != null && !TextUtils.isEmpty(conversation.getLastMessage().getId())) {
                    conversation.setLastMessageId(conversation.getLastMessage().getId());
                }
            }
        }
    }

    public static void updateRemovedLocalConversation(Context context, List<String> idList) {
        String[] ids = new String[idList.size()];
        idList.toArray(ids);
//        Timber.i("updateRemovedLocalConversation: " + new Gson().toJson(idList));
        Realm realm = RealmDb.getInstance(context);
        RealmResults<Conversation> realmResults = realm.where(Conversation.class)
                .in("mId", ids)
                .findAll();
        if (realmResults != null && !realmResults.isEmpty()) {
            List<Conversation> conversations = realm.copyFromRealm(realmResults);
            realm.close();
            for (Conversation conversation : conversations) {
                conversation.setRemoved(true);
            }
//            Timber.i("Found conversation: " + new Gson().toJson(ids));
            saveConversation(context, conversations, false);
        } else {
            realm.close();
        }
    }

    private static void checkToSetConversationContentBeforeSavingIfNecessary(Context context, Conversation conversation) {
        if (TextUtils.isEmpty(conversation.getContent()) && conversation.getLastMessage() != null) {
            if (conversation.isGroupHasInvalidLastMessage()) {
                //Consider as new group, so last message should be welcome message of group.
                conversation.setContent(context.getString(R.string.chat_info_create_new_group,
                        conversation.getGroupName()));
            } else if (conversation.getLastMessage() != null) {
                conversation.setContent(conversation.getLastMessage().getActualContent(context,
                        conversation,
                        true));
            }
        }
    }

    private static void checkToSetConversationContentBeforeSavingIfNecessary(Context context, List<Conversation> conversationList) {
        if (conversationList != null && !conversationList.isEmpty()) {
            for (Conversation conversation : conversationList) {
                checkToSetConversationContentBeforeSavingIfNecessary(context, conversation);
            }
        }
    }

    public static void delete(Context context, String conversationId) {
        Timber.e("delete conversation " + conversationId);
        Realm realm = RealmDb.getInstance(context);
        realm.executeTransaction(realm1 -> {
            RealmResults<Conversation> conById = realm1.where(Conversation.class)
                    .equalTo("mId", conversationId)
                    .findAll();
            if (conById != null) {
                conById.deleteAllFromRealm();
            }
        });
        realm.close();
        MessageDb.deleteByConversation(context, conversationId);
    }

    public static boolean isRead(Context context, Chat chat) {
        Realm realm = RealmDb.getInstance(context);
        List<Conversation> list = realm.where(Conversation.class)
                .equalTo("mId", chat.getChannelId()).findAll();
        list = realm.copyFromRealm(list);
        Timber.e("isread " + new Gson().toJson(list));

        long count = realm.where(Conversation.class)
                .equalTo("mId", chat.getChannelId())
                .equalTo("mIsRead", true)
                .count();
        realm.close();
        return count > 0;
    }

    public static Conversation createNewConversation(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        Conversation conversation = new Conversation();
        conversation.setId(conversationId);
        realm.beginTransaction();
        realm.insertOrUpdate(conversation);
        realm.commitTransaction();
        realm.close();
        return conversation;
    }

    public static Conversation updateLastMessageOfConversation(Context context,
                                                               Chat chat,
                                                               Boolean isRead,
                                                               boolean willCreateNewIfNotExisted) {
        Timber.e("Update conversation last message and read status. isRead: " + isRead);
        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);

        realm.beginTransaction();
        Conversation conversation = realm.where(Conversation.class)
                .equalTo("mId", chat.getChannelId())
                .findFirst();
        if (conversation == null) {
            if (willCreateNewIfNotExisted) {
                conversation = new Conversation();
                conversation.setId(chat.getChannelId());
            }
        } else {
            conversation = realm.copyFromRealm(conversation);
        }

        if (conversation == null) {
            realm.close();
            return null;
        }

        if (isRead != null && isRead) {
            conversation.setUnreadCount(0);
        }

        conversation.setSenderID(chat.getSenderId());
        conversation.setArchived(false);

        //check to update last message content of conversation
        Chat lastMessageOfConversation = MessageDb.getLastMessageOfConversation(context, conversation.getId());
        if (lastMessageOfConversation == null || !TextUtils.equals(lastMessageOfConversation.getId(), chat.getId()) ||
                conversation.getContent() == null || conversation.getDate() == null ||
                !TextUtils.equals(conversation.getLastMessageId(), chat.getId())) {
            Timber.i("Will update last message content and date of conversation id: " + conversation.getId());
            conversation.setLastMessage(chat);
            conversation.setContent(chat.getActualContent(context, conversation, true));
            if (chat.getDate() != null) {
                conversation.setDate(chat.getDate());
            }
        }

        realm.insertOrUpdate(conversation);
        realm.commitTransaction();
        realm.close();

        return conversation;
    }

    public static Conversation getConversationById(Context context, String conversationId) {

        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);
        Conversation conversation = realm.where(Conversation.class)
                .equalTo("mId", conversationId)
                .findFirst();
        if (conversation == null) {
            return null;
        } else {
            conversation = realm.copyFromRealm(conversation);
            Chat lastMessageOfConversation = MessageDb.getLastMessageOfConversation(context, conversationId);
            if (lastMessageOfConversation != null) {
                conversation.setLastMessage(lastMessageOfConversation);
                conversation.setContent(lastMessageOfConversation.getActualContent(context, conversation, true));
                if (conversation.getDate() == null && lastMessageOfConversation.getDate() != null) {
                    conversation.setDate(lastMessageOfConversation.getDate());
                }
            }
        }
        realm.close();
        return conversation;
    }

    public static boolean isGroupConversation(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);
        Conversation conversation = realm.where(Conversation.class)
                .equalTo("mId", conversationId)
                .findFirst();
        boolean isGroupConversation = conversation != null && conversation.isGroup();
        realm.close();
        return isGroupConversation;
    }

    public static List<Conversation> getConversation(Context context) {
        return getConversation(context, 18);
    }

    public static List<Conversation> getConversation(Context context, int limit) {
        Timber.i("getConversation list");
        Realm realm = RealmDb.getInstance(context);
        List<Conversation> conversations = buildBaseQuery(realm).limit(limit).findAll();
        conversations = realm.copyFromRealm(conversations);
        realm.close();
        Collections.sort(conversations); //sort by date desc
        return conversations;
    }

    public static RealmQuery<Conversation> buildBaseQuery(Realm realm) {
        return realm.where(Conversation.class)
                .equalTo("mIsArchived", false)
                .beginGroup()
                .equalTo("mIsRemoved", false)
                .or()
                .isNull("mIsRemoved")
                .endGroup()
                .notEqualTo("mIsDeleted", "true");
    }

    public static List<Conversation> getConversationByIds(Context context, String... ids) {
        Realm realm = RealmDb.getInstance(context);
        List<Conversation> conversations = realm
                .where(Conversation.class)
                .in("mId", ids)
                .equalTo("mIsArchived", false)
                .beginGroup()
                .equalTo("mIsRemoved", false)
                .or()
                .isNull("mIsRemoved")
                .endGroup()
                .notEqualTo("mIsDeleted", "true")
                .findAll();
        conversations = realm.copyFromRealm(conversations);
        realm.close();
        return conversations;
    }

    public static List<Conversation> getIndividualOrGroupConversation(Context context, boolean isOnlyGroup) {
        Realm realm = RealmDb.getInstance(context);
        RealmQuery<Conversation> query = buildBaseQuery(realm);

        if (isOnlyGroup) {
            query = query
                    .isNotNull("mGroupId")
                    .isNotEmpty("mGroupId");

        } else {
            query = query
                    .isEmpty("mGroupId")
                    .or()
                    .isNull("mGroupId");
        }

        List<Conversation> conversations = query
                .sort("mDate", Sort.DESCENDING)
                .limit(PAGINATION_SIZE)
                .findAll();

        conversations = realm.copyFromRealm(conversations);
        realm.close();

        Timber.i("getIndividualOrGroupConversation: is group: " + isOnlyGroup + ", conversations: " + +conversations.size());

        return conversations;
    }

    public static List<Conversation> getUnreadConversation(Context context) {
        Realm realm = RealmDb.getInstance(context);

        RealmQuery<Conversation> query = realm
                .where(Conversation.class)
                .equalTo("mIsArchived", false)
                .beginGroup()
                .equalTo("mIsRemoved", false)
                .or()
                .isNull("mIsRemoved")
                .endGroup()
                .notEqualTo("mIsDeleted", "true");
        List<Conversation> conversations = query
                .sort("mDate", Sort.DESCENDING)
                .limit(50)
                .findAll();
        conversations = realm.copyFromRealm(conversations);
        realm.close();

        List<Conversation> unread = new ArrayList<>();

        for (Conversation conversation : conversations) {
            if (!conversation.isRead()) {
                unread.add(conversation);
            }
        }

        Collections.sort(unread);
        return unread;
    }

    public static List<Conversation> getReadConversation(Context context) {
        Realm realm = RealmDb.getInstance(context);

        RealmQuery<Conversation> query = realm
                .where(Conversation.class)
                .equalTo("mIsArchived", false)
                .beginGroup()
                .equalTo("mIsRemoved", false)
                .or()
                .isNull("mIsRemoved")
                .endGroup()
                .notEqualTo("mIsDeleted", "true");
        List<Conversation> conversations = query
                .sort("mDate", Sort.DESCENDING)
                .limit(50)
                .findAll();
        conversations = realm.copyFromRealm(conversations);
        realm.close();

        List<Conversation> read = new ArrayList<>();

        for (Conversation conversation : conversations) {
            if (conversation.isRead()) {
                read.add(conversation);
            }
        }

        Collections.sort(read);
        return read;
    }

    public static void saveConversation(Context context, List<Conversation> conversations, boolean isRemoteConversation) {
//        Timber.i("saveConversation: " + conversations.size());
        if (isRemoteConversation) {
            synchronizeLastMessageReadStatusOfConversationFromServer(context, conversations);
        }
        setLastMessageToConversationBeforeSaving(conversations);
        checkToSetConversationContentBeforeSavingIfNecessary(context, conversations);
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        //server doesn't support recipient field, yet. So, we get recipient from local instead
        for (Conversation conversation : conversations) {
            Conversation localConversation = realm
                    .where(Conversation.class)
                    .equalTo("mId", conversation.getId())
                    .findFirst();
            if (localConversation != null) {
                //server doesn't handle archived conversation, so it need handle locally
                conversation.setArchived(localConversation.isArchived());
            }
        }
        realm.insertOrUpdate(conversations);
        realm.commitTransaction();
        realm.close();
    }

    public static void synchronizeLastMessageReadStatusOfConversationFromServer(Context context, List<Conversation> remoteConversations) {
        List<Chat> validRemoteMessageToCheck = new ArrayList<>();
        for (Conversation remoteConversation : remoteConversations) {
            if (remoteConversation.getLastMessage() != null && remoteConversation.getUnreadCount() > 0) {
                validRemoteMessageToCheck.add(remoteConversation.getLastMessage());
            }
        }

        if (validRemoteMessageToCheck.isEmpty()) {
            return;
        }

        String[] queryMessageIds = new String[validRemoteMessageToCheck.size()];
        for (int i = 0; i < validRemoteMessageToCheck.size(); i++) {
            queryMessageIds[i] = validRemoteMessageToCheck.get(i).getId();
        }

        List<Chat> localMessages = MessageDb.queryByIds(context, queryMessageIds);
        String userId = SharedPrefUtils.getUserId(context);
        boolean shouldUpdateLocal = false;
        if (localMessages != null && !localMessages.isEmpty()) {
            for (Chat localMessage : localMessages) {
                if (localMessage.getSeen() != null && !localMessage.getSeen().isEmpty()) {
                      /*
                          Check to remove current user id from seen list since there is unread
                          counter of this conversation. By doing this, we will make sure that
                          when user enter chat screen, read status of that last message will be
                          sent to server.
                      */
                    if (localMessage.getSeen().contains(userId)) {
                        localMessage.getSeen().remove(userId);
                        localMessage.setStatus(MessageState.SENT);
                        shouldUpdateLocal = true;
                    }
                }
            }

            if (shouldUpdateLocal) {
                MessageDb.saveWithoutCheckingToBackupLocalProperty(context, localMessages);
            }
        }
    }

    public static void archivedConversation(Context context, String conversationsId) {
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        RealmResults<Conversation> data = realm.where(Conversation.class)
                .equalTo("mId", conversationsId)
                .findAll();
        for (Conversation conversation : data) {
            conversation.setArchived(true);
        }
        realm.commitTransaction();
        realm.close();
    }

    public static void updateLastSeenMessageStatusOfUser(Context context,
                                                         Chat lastSeenMessage,
                                                         String userId,
                                                         String tag,
                                                         OnChatSocketListener socketListener) {
        Timber.i("updateLastSeenMessageStatusOfUser: userId: " + userId +
                ", lastSeenMessageId: " + lastSeenMessage.getId() +
                ", tag: " + tag);
        MessageDb.addReadMessageToSeenListOfMessage(context, userId, lastSeenMessage, socketListener);
    }

    public static boolean isConversationExisted(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);
        long count = realm.where(Conversation.class)
                .equalTo("mId", conversationId)
                .equalTo("mIsArchived", false)
                .beginGroup()
                .equalTo("mIsRemoved", false)
                .or()
                .isNull("mIsRemoved")
                .endGroup()
                .notEqualTo("mIsDeleted", "true")
                .count();
        Timber.i("isConversationExisted: of " + conversationId + ", count: " + count);
        return count > 0;
    }

    public static void updateLastSeenMessageStatusOfCurrentUser(Context context,
                                                                Chat lastSeenMessage,
                                                                OnChatSocketListener socketListener) {
        updateLastSeenMessageStatusOfUser(context,
                lastSeenMessage,
                SharedPrefUtils.getUserId(context),
                "updateLastSeenMessageStatusOfCurrentUser", socketListener);
    }

    public static String getLastSeenMessageIdOfCurrentUser(Context context, String conversationId) {
        String userId = SharedPrefUtils.getUserId(context);
        return MessageDb.getLastSeenMessageIdOfUser(context, userId, conversationId);
    }
}
