package com.proapp.sompom.database;

import android.content.Context;

import com.google.gson.Gson;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.MediaContainerAdaptive;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import timber.log.Timber;

public final class MediaDb {

    private MediaDb() {
    }

    private static List<Media> findLocalDownloadMedia(Context context, String[] checkingIds) {
        List<Media> validResult = new ArrayList<>();
        Realm realm = RealmDb.getInstance(context);
        RealmDb.refresh(realm);
        RealmResults<Media> result = realm.where(Media.class).isNotNull("mDownloadedPath")
                .and()
                .in("mId", checkingIds)
                .findAll();
        if (result != null) {
            validResult = realm.copyFromRealm(result);
        }
        realm.close();

        return validResult;
    }

    private static List<Media> getAllMedia(Context context) {
        List<Media> validResult = new ArrayList<>();
        Realm realm = RealmDb.getInstance(context);
        RealmResults<Media> result = realm.where(Media.class)
                .findAll();
        if (result != null) {
            validResult = realm.copyFromRealm(result);
        }
        realm.close();

        return validResult;
    }

    public static void saveDownloadedMedia(Context context, Media media) {
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        realm.insertOrUpdate(media);
        realm.commitTransaction();
        realm.close();
    }

    public static void backupLocalDownloadedMFileMediaPath(Context context, List<Media> mediaList) {
        List<Media> localDownloadMedia = MediaDb.findLocalDownloadMedia(context,
                MediaDb.getMediaIds(mediaList));
//        Timber.i("localDownloadMedia: " + new Gson().toJson(localDownloadMedia));
        if (localDownloadMedia != null && !localDownloadMedia.isEmpty()) {
            for (Media media : mediaList) {
                for (Media media1 : localDownloadMedia) {
                    if (media.getId().matches(media1.getId())) {
                        media.setDownloadedPath(media1.getDownloadedPath());
                        media.setFileStatusType(Media.FileStatusType.DOWNLOADED);
                    }
                }
            }
        }
    }

    public static <T extends MediaContainerAdaptive> List<Media> getMediaList(List<T> mediaContainerList) {
        List<Media> mediaList = new ArrayList<>();
        for (MediaContainerAdaptive adaptive : mediaContainerList) {
            if (adaptive.getMedia() != null && !adaptive.getMedia().isEmpty()) {
                mediaList.addAll(adaptive.getMedia());
            }
        }

        return mediaList;
    }

    public static String[] getMediaIds(List<Media> listMedia) {
        String[] ids = new String[listMedia.size()];
        for (int i = 0; i < listMedia.size(); i++) {
            ids[i] = listMedia.get(i).getId();
        }

        return ids;
    }

}
