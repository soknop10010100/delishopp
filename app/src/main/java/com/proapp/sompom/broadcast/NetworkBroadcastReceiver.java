package com.proapp.sompom.broadcast;

/**
 * Created by he.rotha on 2/26/16.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import com.proapp.sompom.utils.NetworkStateUtil;


public class NetworkBroadcastReceiver extends BroadcastReceiver {

    private NetworkListener mListener;
    private IntentFilter mIntentFilter;

    public NetworkBroadcastReceiver() {
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
    }

    public void setListener(NetworkListener listener) {
        mListener = listener;
    }

    public IntentFilter getIntentFilter() {
        return mIntentFilter;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        if (mListener == null) {
            return;
        }

        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            if (NetworkStateUtil.isNetworkAvailable(context)) {
                mListener.onNetworkState(NetworkState.Connected);
            } else {
                mListener.onNetworkState(NetworkState.Disconnected);
            }
        }
    }

    public enum NetworkState {
        Connected, Disconnected, Nothing
    }

    public interface NetworkListener {

        void onNetworkState(NetworkState networkState);
    }
}