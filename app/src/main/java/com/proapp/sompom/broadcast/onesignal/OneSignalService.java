package com.proapp.sompom.broadcast.onesignal;

import android.content.Context;

import com.onesignal.OneSignal;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.NotificationServiceHelper;

import org.json.JSONObject;

import java.util.Collections;

import timber.log.Timber;

public class OneSignalService {

    private static final String USER_ID = "userId";

    public static void initOneSignalNotificationService(Context context) {
        // Enable verbose OneSignal logging to debug issues if needed.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);

        // OneSignal Initialization
        OneSignal.initWithContext(context);
        OneSignal.setAppId(context.getString(R.string.onesignal_app_id));
        /*
          OneSignal.setAppId(getString(R.string.one_signal_key));
          We want to keep receiving notification even if user go to app's setting and disable
          the notification for the app because we are handling with silent notification and we
          manage to dispatch local notification by ourself.
         */
        OneSignal.unsubscribeWhenNotificationsAreDisabled(false);
    }

    public static void subscribeWithUserId(String userId) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(USER_ID, userId);
            Timber.e("Subscribe to OneSignal: " + jsonObject.toString());
            OneSignal.sendTags(jsonObject);
            Timber.e("User has subscribed to OneSignal successfully.");
        } catch (Exception e) {
            Timber.e(e.getMessage());
        }
    }

    public static void logout() {
        OneSignal.deleteTags(Collections.singletonList(USER_ID));
    }
}
