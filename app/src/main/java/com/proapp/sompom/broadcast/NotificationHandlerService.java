package com.proapp.sompom.broadcast;

import static com.proapp.sompom.newui.ChatActivity.REFRESH_NOTIFICATION_BADGE_ACTION;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.chat.call.CallServiceConnectionHelper;
import com.proapp.sompom.chat.call.CallServiceConnectionProvider;
import com.proapp.sompom.chat.call.CallTimerHelper;
import com.proapp.sompom.chat.call.IncomingCallDataHolder;
import com.proapp.sompom.chat.service.ChatSocket;
import com.proapp.sompom.database.MessageDb;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.ConversationHelper;
import com.proapp.sompom.helper.GsonHelper;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.injection.broadcast.BroadcastComponent;
import com.proapp.sompom.injection.broadcast.BroadcastModule;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.licence.db.LicenseSynchronizationDb;
import com.proapp.sompom.licence.model.SynchroniseData;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.NotificationChannelSetting;
import com.proapp.sompom.model.OrderStatus;
import com.proapp.sompom.model.ReceivePushNotificationModel;
import com.proapp.sompom.model.WalletUpdateReceivePushNotificationModel;
import com.proapp.sompom.model.emun.OneSignalPushType;
import com.proapp.sompom.model.notification.Notification;
import com.proapp.sompom.model.notification.NotificationActor;
import com.proapp.sompom.model.notification.NotificationListAndRedirectionHelper;
import com.proapp.sompom.model.notification.NotificationVerb;
import com.proapp.sompom.model.request.JoinChannelStatusUpdateBody;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.newui.ChatActivity;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.ChatNotificationUtil;
import com.proapp.sompom.utils.ConciergeNotificationUtil;
import com.proapp.sompom.utils.NotificationUtils;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.SpannableUtil;
import com.sompom.pushy.service.SendBroadCastHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;


/**
 * Created by Veasna Chhom on 4/5/21.
 */
public class NotificationHandlerService {

    public static final String CONTENT_TYPE = "type";
    public static final String GROUP_ACTION_DATA = "content";
    public static final String FIELD_SHOP = "shop";

    @Inject
    public ApiService mApiService;
    private BroadcastComponent mControllerComponent;
    private Gson mGson;

    public void remoteNotificationReceived(Context context, JSONObject json) {
        Timber.i("remoteNotificationReceived: isInForeground: " + CallServiceConnectionHelper.isInForeground());
        if (!SharedPrefUtils.isLogin(context)) {
            return;
        }

        try {
            Timber.i("getBody: " + json);
            OneSignalPushType type = OneSignalPushType.getType(getType(json));
            Timber.i("Type: %s", type);
            JSONObject contentData = json.getJSONObject(GROUP_ACTION_DATA);
            Timber.i(json.toString());

            if (type == OneSignalPushType.OrderProcessing ||
                    type == OneSignalPushType.OrderDelivering ||
                    type == OneSignalPushType.OrderArrived ||
                    type == OneSignalPushType.OrderDelivered) {
                handleConciergeOrderUpdateNotification(context, contentData);
            } else if (type == OneSignalPushType.ExpressSlotFree) {
                handleExpressSlotFreeNotification(context, contentData);
            } else if (type == OneSignalPushType.CustomPush) {
                handleCustomPushNotification(context, contentData);
            } else if (type == OneSignalPushType.ShopStatus) {
                handleShopStatusUpdateNotification(context, contentData);
            } else if (isValidNotificationToHandle(contentData)) {
                handleValidNotification(context, contentData);
            } else if (type == OneSignalPushType.WalletUpdate) {
                handleConciergeUpdateWalletNotification(context, contentData);
            }
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    private boolean isValidNotificationToHandle(JSONObject content) {
        /*
           This method will determine if the notification need to handled. Normally the data here is
           expectedly the same as inside notification list screen which right now are all about the post,
           but we can have more just after.
         */
        if (content != null) {
            return NotificationListAndRedirectionHelper.isValidNotificationData(getGson().fromJson(content.toString(),
                    JsonObject.class));
        }

        return false;
    }

    private boolean isNotificationFromCurrentUserActor(Context context, Notification notification) {
        String currentUserId = SharedPrefUtils.getUserId(context);
        if (notification != null &&
                notification.getActors() != null &&
                !notification.getActors().isEmpty()) {
            NotificationActor notificationActor = notification.getActors().get(0);
            if (notificationActor != null) {
                return TextUtils.equals(currentUserId, notificationActor.getId());
            }
        }

        return false;
    }

    private void handleCustomPushNotification(Context context, JSONObject content) {
        try {
            ReceivePushNotificationModel pushNotificationModel = getGson().fromJson(content.toString(), ReceivePushNotificationModel.class);
            ConciergeNotificationUtil.pushGeneralNotification(context,
                    pushNotificationModel,
                    NotificationChannelSetting.OrderStatusUpdate,
                    false);
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    private void handleExpressSlotFreeNotification(Context context, JSONObject content) {
        try {
            ReceivePushNotificationModel pushNotificationModel = getGson().fromJson(content.toString(), ReceivePushNotificationModel.class);
            ConciergeNotificationUtil.pushExpressSlotFreeNotification(context,
                    pushNotificationModel,
                    NotificationChannelSetting.OrderStatusUpdate,
                    false);
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    private void handleConciergeOrderUpdateNotification(Context context, JSONObject content) {
        try {
            OrderStatus orderStatus = getGson().fromJson(content.toString(), OrderStatus.class);
            ConciergeNotificationUtil.pushConciergeOrderStatusNotification(context,
                    orderStatus,
                    NotificationChannelSetting.OrderStatusUpdate,
                    false);
            ConciergeHelper.broadcastOrderStatusUpdateEvent(context, ConciergeHelper.fromNotificationOrder(context, orderStatus.getOrder()));
            SendBroadCastHelper.verifyAndSendBroadCast(context,
                    new Intent(REFRESH_NOTIFICATION_BADGE_ACTION));
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    private void handleConciergeUpdateWalletNotification(Context context, JSONObject content) {
        try {
            WalletUpdateReceivePushNotificationModel orderStatus = getGson().fromJson(content.toString(),
                    WalletUpdateReceivePushNotificationModel.class);
            ConciergeNotificationUtil.pushWalletUpdateNotification(context,
                    orderStatus,
                    NotificationChannelSetting.OrderStatusUpdate,
                    false);
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    private void handleShopStatusUpdateNotification(Context context, JSONObject content) {
        try {
            if (content.has(FIELD_SHOP)) {
                JSONObject jsonShop = content.getJSONObject(FIELD_SHOP);
                if (jsonShop.has(ConciergeHelper.FIELD_IS_SHOP_CLOSED)) {
                    boolean isShopClosed = jsonShop.getBoolean(ConciergeHelper.FIELD_IS_SHOP_CLOSED);
                    ConciergeHelper.broadcastShopStatusUpdatedEvent(context, isShopClosed);
                    ConciergeNotificationUtil.pushConciergeShopEnableStatusChangedNotification(context,
                            context.getString(R.string.app_name),
                            isShopClosed ? context.getString(R.string.shop_notification_shop_close_status) :
                                    context.getString(R.string.shop_notification_shop_open_status),
                            NotificationChannelSetting.OrderStatusUpdate,
                            true);
                }
            }
        } catch (Exception e) {
            Timber.e(e);
            SentryHelper.logSentryError(e);
        }
    }

    private void handleValidNotification(Context context, JSONObject content) {
        try {
            Notification notification = getGson().fromJson(content.toString(), Notification.class);
            if (isNotificationFromCurrentUserActor(context, notification)) {
                Timber.i("Ignore handle handleValidNotification from current user as actor.");
                return;
            }

            String subjectText = NotificationListAndRedirectionHelper.getSubjectText(context, notification.getActors());
            String notificationActionText = NotificationListAndRedirectionHelper.getNotificationActionText(context,
                    notification.getVerb(),
                    notification.getFirstObject());
            long when = notification.getDate() != null ? notification.getDate().getTime() : -1;
            if (TextUtils.isEmpty(notificationActionText)) {
                notificationActionText = context.getString(R.string.notification_default_text).trim();
            }
            if (TextUtils.equals(notification.getVerb(), NotificationVerb.VERB_CREATE_GROUP)) {
                /*
                    We will manage to push local notification of create new group only if the app has not yet
                    opened before. In case, it is already running, we will ignore this notification type and let
                    it be handled via Socket instead.
                    For create new group notification, we first have to check if the conversation is already
                    existed or not. If not, we will manage to download it in background.
                 */
                if (isAppHasNotYetOpenBefore()) {
                    Timber.i("Receive create new group push notification.");
                    if (mApiService == null) {
                        getControllerComponent(context).inject(this);
                    }
                    ConversationHelper.checkIfConversationExistedAndBelongToCurrentUser(context,
                            notification.getFirstObject().getConversionId(),
                            mApiService,
                            () -> ChatNotificationUtil.pushCreateGroupChatNotification(context, notification));
                }
            } else {
                NotificationUtils.pushGeneralNotification(context,
                        subjectText,
                        SpannableUtil.capitaliseOnlyFirstLetter(notificationActionText),
                        NotificationListAndRedirectionHelper.getRedirectionIntent(context, notification),
                        NotificationChannelSetting.Home,
                        notification,
                        false,
                        when);
            }
            SendBroadCastHelper.verifyAndSendBroadCast(context, new Intent(ChatActivity.REFRESH_NOTIFICATION_BADGE_ACTION));
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    private boolean isInComingCallStillValid(Chat incomingCall) {
        if (incomingCall.getDate() != null) {
            Date currentDate = new Date();
            long diff = currentDate.getTime() - incomingCall.getDate().getTime();
            Timber.i("diffSecond: " + (diff / CallTimerHelper.TIME_INTERVAL));
            return diff <= CallTimerHelper.VALID_INCOMING_CALL_TIME;
        }

        return true;
    }

    private void handleCallTypeNotification(Context context, JSONObject contentData) {
        Chat callMessage = getGson().fromJson(contentData.toString(), Chat.class);
        if (callMessage == null) {
            Timber.i("Got null message.");
            return;
        }

        if (CallHelper.isInComingVOIPCallMessageType(callMessage) && !isInComingCallStillValid(callMessage)) {
            Timber.i("Ignore incoming call because the call period reached time out.");
            return;
        }

        if (mApiService == null) {
            getControllerComponent(context).inject(this);
        }

        ConversationHelper.checkIfConversationExistedAndBelongToCurrentUser(context,
                callMessage.getChannelId(),
                mApiService,
                () -> {
                    //Record the rejected call status in the list.
                    if (CallHelper.isRejectVOIPCallMessageType(callMessage)) {
                        CallHelper.addToRejectedCallList(callMessage.getChannelId());
                    }

                    /*
                    Check callAccepted message type from OneSignal too for Android 10+ so as to manage
                    dismiss the incoming call notification.
                     */
                    if (CallHelper.isCallAcceptedVOIPCallMessageType(callMessage) &&
                            Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q &&
                            TextUtils.equals(callMessage.getSenderId(),
                                    SharedPrefUtils.getUserId(context))) {
                        CallServiceConnectionHelper callServiceConnectionHelper = CallServiceConnectionProvider.getInstance()
                                .getCallServiceConnection(context);
                        /*
                        Will close the incoming call notification if it receive call accepted from
                        same account on another device
                         */
                        if (callServiceConnectionHelper.isDisplayingCallNotification() &&
                                !MainApplication.isIsCallingScreenOpened()) {
                            Timber.i("Dismiss incoming call notification immediately.");
                            callServiceConnectionHelper.cancelTimer();
                            callServiceConnectionHelper.onRejectVOIPCallMessage(false,
                                    false);
                        } else {
                            Timber.i("Add ignore incoming call.");
                            callServiceConnectionHelper.addIgnoreIncomingCall(callMessage);
                        }
                    }

                    boolean inForeground = CallServiceConnectionHelper.isInForeground();
                    Timber.i("inForeground: " + inForeground);
                    if (!inForeground) {
                        if (CallHelper.isInComingVOIPCallMessageType(callMessage)) {
                            CallHelper.clearRejectedCallList();
                            Timber.i("Receive incoming call notification when app in background.");
                            /*
                            On Android version 10 up, we have to display incoming call notification instead of
                            showing screen directly as Android policy.
                             */
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                if (CallHelper.isDeviceInPhoneCallProcess(context)) {
                                   /*
                                    If the home screen was already, the socket service is already running and be able
                                    to handle the call busy. Otherwise, we will send call busy from call service
                                    connection.
                                    */
                                    if (!MainApplication.isIsHomeScreenOpened()) {
                                        CallServiceConnectionHelper callServiceConnectionHelper = CallServiceConnectionProvider.getInstance()
                                                .getCallServiceConnection(context);
                                        callServiceConnectionHelper.startService(callMessage,
                                                false,
                                                () -> callServiceConnectionHelper.sendUserBusyVOIPCallMessage(callMessage.getCallData()));
                                    }
                                } else {
                                    CallServiceConnectionProvider serviceConnectionProvider = CallServiceConnectionProvider.getInstance();
                                    //Need to check to avoid duplicate call service initialization within duplicate incoming call.
//                                    if (!serviceConnectionProvider.isIncomingBeingChecked(callMessage)) {
                                    CallServiceConnectionHelper callServiceConnectionHelper = CallServiceConnectionProvider.getInstance()
                                            .getCallServiceConnection(context);
                                    if (!serviceConnectionProvider.isShowingIncomingCallNotification()) {
                                        Timber.i("Start showing incoming call notification.");
                                        callServiceConnectionHelper.startService(callMessage,
                                                true,
                                                null);
                                    } else {
                                        //Need to send current user busy status
                                        Timber.i("Send user call busy status for Android 10 up.");
                                        if (callServiceConnectionHelper.isChatSocketBound()) {
                                            callServiceConnectionHelper.sendUserBusyVOIPCallMessage(callMessage.getCallData());
                                        } else {
                                            callServiceConnectionHelper.startService(callMessage,
                                                    false,
                                                    () -> callServiceConnectionHelper.sendUserBusyVOIPCallMessage(callMessage.getCallData()));
                                        }
                                    }
//                                    }
//                                else {
//                                        Timber.i("Incoming call notification is already being checked or displayed.");
//                                    }
                                }
                            } else {
                                /*
                                If app receiving incoming notification in background and the device version
                                is not Android 10 up, we will check the following condition to handle that incoming
                                call.
                                 */
                                if (isAppHasNotYetOpenBefore()) {
                                    /*
                                    The app is not yet running any screen yet. So we check if the device is in another call process such
                                    as phone number or other app's VOIP calls, we will just send the call busy
                                    to the caller. Otherwise. we will show incoming call screen directly.
                                    Note: If the app is running, we will only manage to show incoming call
                                    on Android version below 10 with Socket message only.
                                     */
                                    if (CallHelper.isDeviceInPhoneCallProcess(context)) {
                                        CallServiceConnectionHelper callServiceConnectionHelper = CallServiceConnectionProvider.getInstance()
                                                .getCallServiceConnection(context);
                                        callServiceConnectionHelper.startService(callMessage,
                                                false,
                                                () -> callServiceConnectionHelper.sendUserBusyVOIPCallMessage(callMessage.getCallData()));
                                    } else {
                                        Timber.i("Will open incoming call screen from background.");
                                        IncomingCallDataHolder callVo = new IncomingCallDataHolder();
                                        callVo.setBackground(true);
                                        callVo.setCallType(callMessage.getSendingTypeAsString());
                                        callVo.setCallData(callMessage.getCallData());
                                        context.startActivity(new CallingIntent(context, callVo));
                                    }
                                }
                            }
                        }
                    }
                });
    }

    private void handleJoinGroupCallNotification(Context context, JSONObject contentData) {
        if (contentData != null) {
            JoinChannelStatusUpdateBody body = getGson().fromJson(contentData.toString(),
                    JoinChannelStatusUpdateBody.class);
            if (ConversationHelper.isConversationOfCurrentUser(context, body.getConversationId())) {
                ChatHelper.broadcastGroupJoinStatusEven(context, body);
            }
        }
    }

    private void handleSynchronizeNotification(Context context) {
        if (mApiService == null) {
            getControllerComponent(context).inject(this);
        }
        Observable<Response<List<SynchroniseData>>> call = mApiService.getClientFeatureData();
        ResponseObserverHelper<Response<List<SynchroniseData>>> helper = new ResponseObserverHelper<>(context, call);
        helper.execute(new OnCallbackListener<Response<List<SynchroniseData>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e(ex);
            }

            @Override
            public void onComplete(Response<List<SynchroniseData>> result) {
                if (result.isSuccessful() && result.body() != null) {
                    LicenseSynchronizationDb.saveSynchroniseData(context, result.body().get(0));
                }
            }
        });
    }

    private void checkToSendDeliveredStatusMessage(Context context, Chat chat) {
        if (context instanceof MainApplication && isAppHasNotYetOpenBefore()) {
            ((MainApplication) context).checkToSendDeliveredMessage(chat);
        }
    }

    private void printSocketState(Context context) {
        if (context.getApplicationContext() instanceof MainApplication) {
            Timber.i("isSocketStateConnected: " + ((MainApplication) context.getApplicationContext()).isSocketStateConnected());
        }
    }

    private void checkToHandleReceiveMessageAsViaSocket(Context context, Chat chat) {
        Chat localChat = MessageDb.queryById(context, chat.getId());
        Timber.i("checkToHandleReceiveMessageAsViaSocket is chat existed: " + (localChat != null));
        /*
            If the receiving message notification is not yet saved in local and the app is opening, we
            will handle display and saving like receiving via chat socket service. This use case may
            happen when socket is receiving late or sometimes socket lost connection. And by performing
            like receiving via Socket, it will reconnect socket if it lost the connection.
         */
        if (context.getApplicationContext() instanceof MainApplication &&
                localChat == null &&
                !isAppHasNotYetOpenBefore()) {
            Timber.i("Will perform receiving message as receiving via Socket.");
            if (!((MainApplication) context.getApplicationContext()).isSocketStateConnected()) {
                ((MainApplication) context.getApplicationContext()).checkToReconnectSocketIfNecessary();
            }
            ((MainApplication) context.getApplicationContext()).performReceiveSocketMessageManuallyForChatFromNotification(chat);
        }
    }

    private void handleMessageOfConversationNotification(Context context, JSONObject jsonData) {
        Chat chat = getGson().fromJson(jsonData.toString(), Chat.class);
        Timber.i("handleMessageOfConversationNotification: " + getGson().toJson(chat));
        checkToHandleReceiveMessageAsViaSocket(context, chat);

        if (mApiService == null) {
            getControllerComponent(context).inject(this);
        }

        ConversationHelper.checkIfConversationExistedAndBelongToCurrentUser(context,
                chat.getChannelId(),
                mApiService,
                () -> {
                    Timber.i("Receiving message of user.");

                    // Always run the send delivered status message on main thread. This is mostly
                    // for use case when notification are received via OneSignal. Pushy seems to
                    // already dispatched notification on either main thread or a thread that is
                    // already running, but OneSignal notification we need to handle manually.
                    new Handler(Looper.getMainLooper()).post(() -> checkToSendDeliveredStatusMessage(context, chat));

                    checkToUpdateInfoOfRemovedOrEditedMessage(context, chat);
                    if (ConversationHelper.isGroupModificationMessageType(chat) && chat.getMetaGroup() == null) {
                        Timber.i("Invalid group modification message types.");
                        return;
                    }

                    //Currently server send the message status as sent which we have to update manually for context type
                    if (ConversationHelper.isGroupModificationMessageType(chat)) {
                        if (!(chat.getStatus() == MessageState.SENT || chat.getStatus() == MessageState.SEEN)) {
                            chat.setStatus(MessageState.SENT);
                        }
                    }

                    //Chat should not receive from myself
                    if (TextUtils.equals(SharedPrefUtils.getUserId(context), chat.getSenderId()) &&
                            !ConversationHelper.isGroupModificationMessageType(chat)) {
                        Timber.i("Ignore message from current user account and is not related to group modification.");
                        return;
                    }

                    //If context chat is already existed, we don't need to do anything
                    if (!ChatSocket.isExistingChat(context, chat)) {
                        boolean appInBackground = isAppHasNotYetOpenBefore();
                        Timber.i("Chat does not exist: appInBackground" + appInBackground);
                        if (appInBackground) {
                            Timber.i("Will check to make local push notification of message.");
                              /*
                            Must check to update group conversation in local first if the notification type is from
                            group modification so as to have the latest group info for future usage.
                        */
                            boolean isRemoveMeFromGroup = false;
                            if (ConversationHelper.isGroupModificationMessageType(chat)) {
                                isRemoveMeFromGroup = ConversationHelper.checkToUpdateGroupData(context,
                                        chat.getChannelId(),
                                        chat.getMetaGroup(),
                                        chat,
                                        "OneSignal");
                            }
                            handlePushNormalTypeConversationNotification(context, chat, isRemoveMeFromGroup);
                        }
                    } else {
                        Timber.i("Chat is already existed in local. So no need to push notification.");
                    }
                });
    }

    private void checkToUpdateInfoOfRemovedOrEditedMessage(Context context, Chat updateChat) {
        /*
        If the chat socket service is already running, the dispatch local notification should be
        handled with socket service. Otherwise, we will dispatch directly from here.
         */
        boolean appInBackground = isAppHasNotYetOpenBefore();
        if (updateChat.getSendingType() == Chat.SendingType.DELETE) {
            Chat localChat = MessageDb.queryById(context, updateChat.getId());
            if (localChat != null && !localChat.isDeleted()) {
                MessageDb.delete(context, updateChat, null);
                Timber.i("Will update delete message in local.");
            }
            if (appInBackground) {
                ChatNotificationUtil.pushChatNotification(context,
                        updateChat,
                        false,
                        "NotificationHandlerService: Will update delete message in local.");
            }
        } else if (updateChat.getSendingType() == Chat.SendingType.UPDATE) {
            MessageDb.updateEditedMessage(context, updateChat);
            Timber.i("Will update message status to edited in local.");
            if (appInBackground) {
                ChatNotificationUtil.pushChatNotification(context,
                        updateChat,
                        false,
                        "NotificationHandlerService: Will update message status to edited in local.");
            }
        }
    }

    private void handlePushNormalTypeConversationNotification(Context context, Chat chat, boolean isRemoveMeFromGroup) {
        Timber.i("handlePushNormalTypeConversationNotification: " + getGson().toJson(chat));
        // Will ignore push notification for called chat type
        if (Chat.ChatType.from(chat.getChatType()) != Chat.ChatType.CALLED && !(chat.getSendingType() == Chat.SendingType.DELETE ||
                chat.getSendingType() == Chat.SendingType.UPDATE)) {
            if (isAppHasNotYetOpenBefore()) {
                Timber.i("Will save last message id of context conversation");
                ConversationHelper.saveLastLocalMessageId(context, chat.getChannelId(), false);
            }
            Timber.i("handlePushConversationNotification: " + chat.getId() + ", content: " + chat.getContent());
            ChatNotificationUtil.pushChatNotification(context,
                    chat,
                    isRemoveMeFromGroup,
                    "NotificationHandlerService: handlePushNormalTypeConversationNotification");
        }
    }

    private boolean isAppHasNotYetOpenBefore() {
        return MainApplication.isAppInBackground() &&
                !MainApplication.isIsHomeScreenOpened();
    }

    private BroadcastComponent getControllerComponent(Context context) {
        if (mControllerComponent == null) {
            mControllerComponent = ((MainApplication) context)
                    .getApplicationComponent()
                    .newBroadcastComponent(new BroadcastModule(context));
        }
        return mControllerComponent;
    }

    private String getType(JSONObject jsonObject) {
        try {
            return jsonObject.getString(CONTENT_TYPE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    private Gson getGson() {
        if (mGson == null) {
            mGson = GsonHelper.getGsonForSocketCommunication();
        }
        return mGson;
    }
}
