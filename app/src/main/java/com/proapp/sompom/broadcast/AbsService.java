package com.proapp.sompom.broadcast;

import android.app.Service;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.injection.broadcast.BroadcastComponent;
import com.proapp.sompom.injection.broadcast.BroadcastModule;

/**
 * Created by He Rotha on 7/13/18.
 */
public abstract class AbsService extends Service {

    private BroadcastComponent mControllerComponent;

    public BroadcastComponent getControllerComponent() {
        if (mControllerComponent == null) {
            mControllerComponent = ((MainApplication) getApplicationContext())
                    .getApplicationComponent()
                    .newBroadcastComponent(new BroadcastModule(getApplicationContext()));
        }
        return mControllerComponent;
    }
}
