/*
 * Copyright (c) 2018. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.proapp.sompom.broadcast.upload;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;

import androidx.core.app.NotificationCompat;

import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.AbsService;
import com.proapp.sompom.model.NotificationChannelSetting;
import com.proapp.sompom.newui.HomeActivity;
import com.proapp.sompom.utils.NotificationUtils;

import java.util.Random;


/**
 * Created by He Rotha on 2/20/18.
 */

public abstract class NotificationServices extends AbsService {

    private static final int NOTIFICATION_FOREGROUND = 1;
    private static final int NOTIFICATION_OPTION = 2;
    private NotificationManager mNotifyManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public void startForegroundNotification(NotificationChannelSetting setting) {
        /*
        Will display the preparing notification to process, so we pass argument as 101 percentage
        to make the indeterminate effect on the notification display.
         */
        dispatchProgressNotification(101,
                getString(R.string.create_wall_posting_notification_preparing),
                setting);
    }

    public void updateProgress(int percent, String title, NotificationChannelSetting setting) {
        dispatchProgressNotification(percent, title, setting);
    }

    public int pushSingleNotification(String title, PendingIntent pendingIntent, NotificationChannelSetting setting) {
        Random rand = new Random();
        int notificationId = rand.nextInt(99999) + 1;
        dispatchNotification(title, pendingIntent, setting, notificationId, false);
        return notificationId;
    }

    public void pushQueueNotification(String title, PendingIntent pendingIntent, NotificationChannelSetting setting) {
        dispatchNotification(title, pendingIntent, setting, NOTIFICATION_OPTION, true);
    }

    private void dispatchNotification(String title,
                                      PendingIntent pendingIntent,
                                      NotificationChannelSetting setting,
                                      int notificationId,
                                      boolean isOnGoing) {
        NotificationCompat.Builder notification = createCommonComponent(title, pendingIntent, setting, isOnGoing);
        mNotifyManager.notify(notificationId, notification.build());
    }

    private void dispatchProgressNotification(int percent,
                                              String title,
                                              NotificationChannelSetting setting) {
        Intent notificationIntent = new Intent(getApplicationContext(), HomeActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                0,
                notificationIntent,
                0);

        NotificationCompat.Builder notification = createCommonComponent(title,
                pendingIntent,
                setting,
                true);

        if (percent >= 0 && percent <= 100) {
            notification.setProgress(100, percent, false);
        } else {
            notification.setProgress(100, 100, true);
        }
        mNotifyManager.notify(NOTIFICATION_FOREGROUND, notification.build());
    }

    private NotificationCompat.Builder createCommonComponent(String title,
                                                             PendingIntent pendingIntent,
                                                             NotificationChannelSetting setting,
                                                             boolean isOnGoing) {
        NotificationUtils.checkToCreateNotificationChannel(getApplicationContext(), setting);
        return new NotificationCompat.Builder(getApplicationContext(),
                NotificationUtils.getNotificationChannelId(getApplicationContext(), setting))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(title)
                .setSmallIcon(NotificationUtils.getNotificationIcon())
                .setAutoCancel(true)
                .setOnlyAlertOnce(true)
                .setContentIntent(pendingIntent)
                .setSound(getNotificationSound())
                .setOngoing(isOnGoing);
    }

    protected Uri getNotificationSound() {
        return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    }

    public void cancelQueueNotification() {
        mNotifyManager.cancel(NOTIFICATION_OPTION);
    }

    public void cancelInProgressNotification() {
        mNotifyManager.cancel(NOTIFICATION_FOREGROUND);
    }
}
