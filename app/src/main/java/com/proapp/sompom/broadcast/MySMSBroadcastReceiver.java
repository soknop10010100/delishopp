package com.proapp.sompom.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import timber.log.Timber;

public class MySMSBroadcastReceiver extends BroadcastReceiver {

    private static final String OTP_REGEX = "[0-9]{6}";
    public static final String RECEIVED_OTP_ACTION = "RECEIVED_OTP_ACTION";
    public static final String RECEIVED_OTP = "RECEIVED_OTP";

    @Override
    public void onReceive(Context context, Intent intent) {
        Timber.i("onReceive");
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);
            switch (status.getStatusCode()) {
                case CommonStatusCodes.SUCCESS:
                    // Get SMS message contents
                    String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                    // Extract one-time code from the message and complete verification
                    // by sending the code back to your server.
                    String otpFromMessage = getOTPFromMessage(message);
                    Timber.i("otpFromMessage: " + otpFromMessage);
                    if (!TextUtils.isEmpty(otpFromMessage)) {
                        Intent intent2 = new Intent(RECEIVED_OTP_ACTION);
                        intent2.putExtra(RECEIVED_OTP, otpFromMessage);
                        SendBroadCastHelper.verifyAndSendBroadCast(context, intent2);
                    }
                    break;
                case CommonStatusCodes.TIMEOUT:
                    // Waiting for SMS timed out (5 minutes)
                    // Handle the error ...
                    break;
            }
        }
    }

    private String getOTPFromMessage(String message) {
        Pattern pattern = Pattern.compile(OTP_REGEX);
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            return matcher.group();
        }

        return null;
    }
}
