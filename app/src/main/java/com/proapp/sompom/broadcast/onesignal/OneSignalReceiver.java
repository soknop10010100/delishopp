package com.proapp.sompom.broadcast.onesignal;

import android.content.Context;

import com.onesignal.OSNotificationReceivedEvent;
import com.onesignal.OneSignal;
import com.proapp.sompom.broadcast.NotificationHandlerService;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by he.rotha on 5/2/16.
 */
public class OneSignalReceiver implements OneSignal.OSRemoteNotificationReceivedHandler {

    @Override
    public void remoteNotificationReceived(Context context, OSNotificationReceivedEvent notificationReceivedEvent) {
        //Must call this method with null value to not showing default local notification of OneSignal.
        notificationReceivedEvent.complete(null);
        if (SharedPrefUtils.isLogin(context)) {
            if (notificationReceivedEvent.getNotification().getAdditionalData() != null) {
                new NotificationHandlerService().remoteNotificationReceived(context, notificationReceivedEvent.getNotification().getAdditionalData());
            }
        }
    }
}
