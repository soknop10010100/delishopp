package com.proapp.sompom.broadcast.upload;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.helper.upload.FileType;
import com.proapp.sompom.helper.upload.Orientation;
import com.proapp.sompom.intent.CreateTimelineIntent;
import com.proapp.sompom.intent.newintent.HomeIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.NotificationChannelSetting;
import com.proapp.sompom.model.notification.NotificationListAndRedirectionHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by imac on 8/16/17.
 */

public class LifeStreamUploadService extends UploadService {

    public static final String POST_SUCCESS_ACTION = "POST_SUCCESS_ACTION";

    private final UploadBinder mBinder = new UploadBinder();
    private final List<LifeStream> mLifeStreams = new ArrayList<>();
    @Inject
    public ApiService mApiService;
    private final HashMap<String, Disposable> mDisposableHashMap = new HashMap<>();
    private final List<String> mContentFromOtherAppList = new ArrayList<>();

    public static Intent getIntent(Context activity, LifeStream lifeStream, boolean isFromOtherAppContent) {
        Intent intent = new Intent(activity, LifeStreamUploadService.class);
        intent.putExtra(SharedPrefUtils.PRODUCT, lifeStream);
        intent.putExtra(CreateTimelineIntent.IS_FROM_OTHER_APP_CONTENT, isFromOtherAppContent);
        return intent;
    }

    @Override
    protected Uri getNotificationSound() {
        return Uri.parse("android.resource://" + getPackageName() +
                "/" + R.raw.feed_notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Timber.i("onStartCommand: " + mLifeStreams.size());
        if (intent == null) {
            return START_STICKY;
        }
        if (mApiService == null) {
            getControllerComponent().inject(this);
        }
        startUploading(intent.getParcelableExtra(SharedPrefUtils.PRODUCT),
                intent.getBooleanExtra(CreateTimelineIntent.IS_FROM_OTHER_APP_CONTENT, false));
        return START_STICKY;
    }

    private void startUploading(LifeStream lifeStream, boolean isFromOtherAppContent) {
        Timber.i("startUploading: " + new Gson().toJson(lifeStream));
        if (TextUtils.isEmpty(lifeStream.getId()) && !lifeStream.isEditedMode()) {
            lifeStream.setId(String.valueOf(System.currentTimeMillis()));
        }
        if (isFromOtherAppContent && !mContentFromOtherAppList.contains(lifeStream.getId())) {
            mContentFromOtherAppList.add(lifeStream.getId());
        }
        mLifeStreams.add(lifeStream);
        if (mLifeStreams.size() == 1) {
            cancelQueueNotification();
            startUploading(convertTimelineToUploadMedia(lifeStream),
                    NotificationChannelSetting.Home);
        } else {
            Intent homeIntent = new HomeIntent(getApplicationContext());
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                    (int) System.currentTimeMillis(),
                    homeIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            pushQueueNotification(getApplicationContext().getString(R.string.create_wall_posting_notification_in_queue),
                    pendingIntent, NotificationChannelSetting.Home);
        }
    }

    private UploadMedia convertTimelineToUploadMedia(LifeStream lifeStream) {
        if (lifeStream.getMedia() == null || lifeStream.getMedia().isEmpty()) {
            return new UploadMedia(lifeStream.getId(), getApplicationContext().getString(R.string.app_name), new String[]{}, new FileType[]{});
        }
        String[] paths = new String[lifeStream.getMedia().size()];
        FileType[] fileTypes = new FileType[lifeStream.getMedia().size()];
        Orientation[] orientations = new Orientation[lifeStream.getMedia().size()];
        String[] thumbnail = new String[lifeStream.getMedia().size()];
        String[] fileTitle = new String[lifeStream.getMedia().size()];

        for (int i = 0; i < paths.length; i++) {
            paths[i] = lifeStream.getMedia().get(i).getUrl();
            thumbnail[i] = lifeStream.getMedia().get(i).getThumbnail();
            fileTypes[i] = lifeStream.getMedia().get(i).getUploadType();
            fileTitle[i] = lifeStream.getMedia().get(i).getFileName();

            if (lifeStream.getMedia().get(i).getHeight() >
                    lifeStream.getMedia().get(i).getWidth()) {
                orientations[i] = Orientation.Portrait;
            } else {
                orientations[i] = Orientation.Landscape;
            }
        }
        UploadMedia uploadMedia = new UploadMedia(lifeStream.getId(), getApplicationContext().getString(R.string.app_name), paths, fileTypes, fileTitle, orientations);
        uploadMedia.setThumbnail(thumbnail);
        return uploadMedia;
    }

    @Override
    void onUploadSuccess(final UploadMedia uploadMedia) {
        Timber.i("onUploadSuccess: mLifeStreams size " + mLifeStreams.size());
        for (final LifeStream lifeStream : mLifeStreams) {
            if (lifeStream.getId().equals(uploadMedia.getId())) {
                for (int i = 0; i < uploadMedia.getFilePath().length; i++) {
                    lifeStream.getMedia().get(i).setUrl(uploadMedia.getFilePath()[i]);
                    lifeStream.getMedia().get(i).setThumbnail(uploadMedia.getThumbnail()[i]);
                }

                Observable<Response<LifeStream>> callback = WallStreetHelper.getCreateOrUpdatePostObservable(this,
                        mApiService,
                        new LifeStream(lifeStream),
                        !lifeStream.isEditedMode() ? null : lifeStream.getId(),
                        !lifeStream.isEditedMode());
                try {
                    ResponseObserverHelper<Response<LifeStream>> helper = new ResponseObserverHelper<>(getApplicationContext(),
                            callback);
                    mDisposableHashMap.put(uploadMedia.getId(),
                            helper.execute(new OnCallbackListener<Response<LifeStream>>() {
                                @Override
                                public void onFail(ErrorThrowable ex) {
                                    Timber.e("post fail: %s", ex.toString());
                                    generateFailAction(uploadMedia, lifeStream);
                                }

                                @Override
                                public void onComplete(Response<LifeStream> value) {
                                    handleSuccessResponse(uploadMedia,
                                            value.body(),
                                            lifeStream);
                                    UserHelper.setUserLastPostPrivacy(getApplicationContext(),
                                            mApiService,
                                            lifeStream.getPublish().getId());
                                }
                            }));
                } catch (Exception e) {
                    Timber.e("error post to server: %s", e.toString());
                }
                break;
            }
        }
    }

    private void backupSomePostPropertiesAfterUpdate(LifeStream result, LifeStream localUpdateLifeStream) {
        /*
          The update properties may not response back properly by the server which we have to
          manually maintain.
         */
        if (result != null && localUpdateLifeStream != null) {
            result.setEditedMode(localUpdateLifeStream.isEditedMode());
            /*
               Inject the store user from the edited post since this property is being after the
               update success which will cause the app crush if it is not supplied.
            */
            if (result.getStoreUser() == null) {
                result.setStoreUser(localUpdateLifeStream.getUser());
            }

            /*
                Backup lat and log from update post since the server does not return thees properties.
             */
            result.setLatitude(localUpdateLifeStream.getLatitude());
            result.setLongitude(localUpdateLifeStream.getLongitude());
            result.setRawLinkPreviewModelList(localUpdateLifeStream.getRawLinkPreviewModelList());
            result.setBackupLinkPreviewModelList(localUpdateLifeStream.getBackupLinkPreviewModelList());
        }
    }

    private void cancelPostingRequest(String id) {
        Disposable disposable = mDisposableHashMap.get(id);
        if (disposable != null) {
            disposable.dispose();
        }
    }

    private void handleSuccessResponse(UploadMedia uploadMedia,
                                       LifeStream response,
                                       LifeStream localUpdateLifeStream) {
        for (int i = mLifeStreams.size() - 1; i >= 0; i--) {
            if (TextUtils.equals(mLifeStreams.get(i).getId(), uploadMedia.getId())) {
                mLifeStreams.remove(i);
                break;
            }
        }
        if (localUpdateLifeStream.isEditedMode()) {
            backupSomePostPropertiesAfterUpdate(response, localUpdateLifeStream);
        }
        if (response != null) {
            if (response.getUser() == null) {
                response.setStoreUser(SharedPrefUtils.getUser(this));
            }
            mBinder.removeUploadProgression(uploadMedia.getId());
            Intent intent = NotificationListAndRedirectionHelper.getCreateNewPostIntent(this, response.getId());
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                    (int) System.currentTimeMillis(),
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            pushSingleNotification(getApplicationContext().getString(R.string.create_wall_posting_notification_success),
                    pendingIntent,
                    NotificationChannelSetting.Home);
            stopForeground(true);
            stopSelf();
            LifeStreamUploadServiceListener listenerById = findListenerById(uploadMedia.getId());
            if (listenerById != null) {
                Timber.i("onPostSuccess: " + response.isEditedMode());
                listenerById.onPostSuccess(response, uploadMedia.getId());
            }
            cancelPostingRequest(uploadMedia.getId());
            if (isPostFromOtherAppContent(uploadMedia.getId())) {
                mContentFromOtherAppList.remove(uploadMedia.getId());
                SendBroadCastHelper.verifyAndSendBroadCast(this, new Intent(POST_SUCCESS_ACTION));
            }
            checkNextUpload();
        } else {
            generateFailAction(uploadMedia, response);
        }
    }

    private boolean isPostFromOtherAppContent(String id) {
        return mContentFromOtherAppList.contains(id);
    }

    @Override
    void onProgressUpload(UploadMedia uploadMedia, int percentage, int totalValue) {
        mBinder.addUploadProgression(uploadMedia.getId(), percentage);
        LifeStreamUploadServiceListener listenerById = findListenerById(uploadMedia.getId());
        Timber.i("onProgressUpload: done " + percentage + "% of " + totalValue);
        if (listenerById != null) {
            listenerById.onProgressPosting(uploadMedia.getId(),
                    percentage,
                    totalValue);
        }
    }

    @Override
    void onUploadFail(UploadMedia uploadMedia) {
        Timber.e("onUploadFail");
        LifeStream lifeStream = null;
        for (int i = mLifeStreams.size() - 1; i >= 0; i--) {
            if (mLifeStreams.get(i).getId().equals(uploadMedia.getId())) {
                lifeStream = mLifeStreams.get(i);
                mLifeStreams.remove(i);
                break;
            }
        }
        generateFailAction(uploadMedia, lifeStream);
    }

    private LifeStreamUploadServiceListener findListenerById(String id) {
        for (LifeStreamUploadServiceListener listener : mBinder.mListeners) {
            if (TextUtils.equals(listener.getId(), id)) {
                return listener;
            }
        }

        return null;
    }

    private void generateFailAction(UploadMedia uploadMedia, LifeStream lifeStream) {
        if (lifeStream != null) {
            mLifeStreams.remove(lifeStream);
            cancelPostingRequest(lifeStream.getId());

            Intent intent = LifeStreamUploadService.getIntent(getApplicationContext(),
                    lifeStream,
                    isPostFromOtherAppContent(uploadMedia.getId()));
            PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(),
                    (int) System.currentTimeMillis(),
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            int notificationId = pushSingleNotification(getApplicationContext().getString(R.string.create_wall_posting_notification_failed),
                    pendingIntent, NotificationChannelSetting.Home);

            LifeStreamUploadServiceListener listenerById = findListenerById(uploadMedia.getId());
            if (listenerById != null) {
                listenerById.onPostFailed(lifeStream, notificationId);
            }
        }
        checkNextUpload();
    }

    private LifeStream findLifeStreamById(String id) {
        for (LifeStream lifeStream : mLifeStreams) {
            if (TextUtils.equals(lifeStream.getId(), id)) {
                return lifeStream;
            }
        }

        return null;
    }

    public void onCancelPosting(String id) {
        Timber.i("onCancelPosting: " + id);
        addOrRemovePostFromCancelList(id, true);
        if (isInProgress()) {
            cancelLastMediaUpload();
        }
        cancelPostingRequest(id);
        proceedNextUploadOnPostingGetCanceled(id);
    }

    private void proceedNextUploadOnPostingGetCanceled(String id) {
        LifeStream lifeStreamById = findLifeStreamById(id);
        Timber.i("lifeStreamById: " + lifeStreamById);
        if (lifeStreamById != null) {
            mLifeStreams.remove(lifeStreamById);
            cancelInProgressNotification();
        }
        checkNextUpload();
    }

    private void checkNextUpload() {
        Timber.i("checkNextUpload: mLifeStreams: " + mLifeStreams.size());
        if (mLifeStreams.size() == 1) {
            cancelQueueNotification();
        }
        if (!mLifeStreams.isEmpty()) {
            startUploading(convertTimelineToUploadMedia(mLifeStreams.get(0)),
                    NotificationChannelSetting.Home);
        } else {
            cancelInProgressNotification();
            cancelQueueNotification();
            stopForeground(true);
            stopSelf();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class UploadBinder extends Binder {

        private HashMap<String, Integer> mUploadProgression = new HashMap<>();
        private List<LifeStreamUploadServiceListener> mListeners = new ArrayList<>();

        public UploadService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LifeStreamUploadService.this;
        }

        public void addListener(LifeStreamUploadServiceListener listener) {
            if (listener != null) {
                removeListenerById(listener.getId());
                mListeners.add(listener);
            }
        }

        public void removeListenerById(String id) {
            for (int size = mListeners.size() - 1; size >= 0; size--) {
                if (TextUtils.equals(mListeners.get(size).getId(), id)) {
                    mListeners.remove(size);
                    break;
                }
            }
        }

        public List<LifeStreamUploadServiceListener> getListeners() {
            return mListeners;
        }

        public int findExistingUploadProgression(String id) {
            Integer integer = mUploadProgression.get(id);
            if (integer != null) {
                return integer;
            } else {
                return 0;
            }
        }

        public void removeUploadProgression(String id) {
            mUploadProgression.remove(id);
        }

        public void addUploadProgression(String id, int progression) {
            mUploadProgression.put(id, progression);
        }

        public void onRemovePosting(String id) {
            onCancelPosting(id);
        }

        public void onRetryPosting(LifeStream lifeStream) {
            startUploading(lifeStream, isPostFromOtherAppContent(lifeStream.getId()));
        }
    }

    public interface LifeStreamUploadServiceListener {

        void onPostSuccess(LifeStream lifeStream, String id);

        void onProgressPosting(String id, int progress, int total);

        void onPostFailed(LifeStream lifeStream, int notificationId);

        String getId();
    }
}
