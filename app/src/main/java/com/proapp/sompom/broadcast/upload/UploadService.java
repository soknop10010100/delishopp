package com.proapp.sompom.broadcast.upload;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.upload.AbsLifeStreamUploader;
import com.proapp.sompom.helper.upload.AbsMyUploader;
import com.proapp.sompom.helper.upload.AbsProductUploader;
import com.proapp.sompom.helper.upload.UploadListener;
import com.proapp.sompom.model.NotificationChannelSetting;
import com.proapp.sompom.utils.MediaUtil;
import com.proapp.sompom.utils.NetworkStateUtil;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by imac on 8/16/17.
 */

public abstract class UploadService extends NotificationServices {

    private static final int PERCENTAGE_FACTOR = 100;

    private int mTotalUploadedPercentage = 0;
    private int mTotalUploadValue;
    private String mLastRequestId;
    private AbsMyUploader mUploader;
    protected boolean mIsInProgress;
    private List<String> mCancelPostIds = new ArrayList<>();

    public void startUploading(UploadMedia media, NotificationChannelSetting setting) {
        addOrRemovePostFromCancelList(media.getId(), false);
        if (!NetworkStateUtil.isNetworkAvailable(this)) {
            //Will invoke the failure callback immediately if there is no internet connection.
            UploadService.this.onUploadFail(media);
        } else {
            mIsInProgress = false;
            mTotalUploadedPercentage = 0;
            mTotalUploadValue = getTotalUploadValueProgression(media);
            startForegroundNotification(setting);
            if (media.getFilePath() != null && media.getFilePath().length > 0) {
                upload(media, 0, setting);
            } else {
                startPosting(media, setting);
            }
        }
    }

    public void addOrRemovePostFromCancelList(String postId, boolean isAdd) {
        if (isAdd) {
            if (!mCancelPostIds.contains(postId)) {
                mCancelPostIds.add(postId);
            }
        } else {
            mCancelPostIds.remove(postId);
        }
    }

    private boolean isCreatePostAlreadyCancelled(String postId) {
        return mCancelPostIds.contains(postId);
    }

    public boolean isInProgress() {
        return mIsInProgress;
    }

    public void cancelLastMediaUpload() {
        mIsInProgress = false;
        if (mUploader != null) {
            Timber.i("Cancel last upload " + mLastRequestId);
            mUploader.cancelUpload(mLastRequestId);
        }
    }

    public void startPosting(UploadMedia uploadMedia, NotificationChannelSetting setting) {
        updateProgress(-1, getString(R.string.create_wall_posting_notification), setting);
        onUploadSuccess(uploadMedia);
    }

    private boolean isMediaAlreadyUpload(String media) {
        return MediaUtil.isHttpUrl(media);
    }

    private int getTotalUploadValueProgression(UploadMedia uploadMedia) {
        return uploadMedia.getFilePath().length * PERCENTAGE_FACTOR;
    }

    public void upload(final UploadMedia uploadMedia,
                       final int index,
                       final NotificationChannelSetting setting) {
        if (isMediaAlreadyUpload(uploadMedia.getFilePath()[index])) {
            mTotalUploadedPercentage += 100; //100 means a single upload has been done 100%.
            checkNextUpload(uploadMedia, index, setting);
        } else {
            if (setting == NotificationChannelSetting.Home) {
                mUploader = new AbsLifeStreamUploader(this,
                        uploadMedia.getFilePath()[index],
                        uploadMedia.getFileTypes()[index],
                        uploadMedia.getFileTitle()[index],
                        uploadMedia.getOrientations()[index]);
            } else {
                mUploader = new AbsProductUploader(this,
                        uploadMedia.getFilePath()[index],
                        uploadMedia.getFileTypes()[index],
                        uploadMedia.getOrientations()[index]);
            }

            mUploader.setListener(new UploadListener() {
                private int mCurrentPercent = 0;

                @Override
                public void onUploadSucceeded(String requestId, String imageUrl, String thumb) {
                    boolean isCreatePostAlreadyCancelled = isCreatePostAlreadyCancelled(uploadMedia.getId());
                    Timber.i("onUploadSucceeded: isCreatePostAlreadyCancelled: " + isCreatePostAlreadyCancelled);
                    if (isCreatePostAlreadyCancelled) {
                        return;
                    }

                    mIsInProgress = false;
                    uploadMedia.getFilePath()[index] = imageUrl;
                    uploadMedia.getThumbnail()[index] = thumb;
                    checkNextUpload(uploadMedia, index, setting);
                }

                @Override
                public void onUploadFail(String requestId, Exception ex) {
                    Timber.i("onUploadFail: " + ex.getMessage());
                    UploadService.this.onUploadFail(uploadMedia);
                }

                @Override
                public void onUploadProgressing(String id, int percent, long uploadedBytes, long totalBytes) {
                    boolean isCreatePostAlreadyCancelled = isCreatePostAlreadyCancelled(uploadMedia.getId());
                    Timber.i("onUploadProgressing: " + percent + ", isCreatePostAlreadyCancelled: " + isCreatePostAlreadyCancelled);
                    if (isCreatePostAlreadyCancelled) {
                        return;
                    }

                    int totalPercentage;
                    if (percent == 100) {
                        mTotalUploadedPercentage += percent;
                        totalPercentage = mTotalUploadedPercentage;
                    } else {
                        totalPercentage = mTotalUploadedPercentage + percent;
                    }
                    int progressPercentage = (totalPercentage * 100) / mTotalUploadValue;
//                    Timber.i("onUploadProgressing: sub percent: " + percent + ", totalPercentage: " +
//                            totalPercentage + ", total upload value: " + progressPercentage +
//                            ", mTotalUploadedPercentage: " + mTotalUploadedPercentage);
                    onProgressUpload(uploadMedia, progressPercentage, mTotalUploadValue);
                    if (mCurrentPercent != percent) {
                        if (percent % 5 == 0) {
                            mCurrentPercent = percent;
                            String title = getString(R.string.create_wall_posting_notification_progression,
                                    index + 1, uploadMedia.getFilePath().length);
                            updateProgress(percent, title, setting);
                        }
                    }
                }

                @Override
                public void onCancel(String id) {
                    mIsInProgress = false;
                    Timber.e("onCancel Upload: %s", id);
                }
            });

            mIsInProgress = true;
            mLastRequestId = mUploader.doUpload();
        }
    }

    private void checkNextUpload(UploadMedia uploadMedia, int index, NotificationChannelSetting setting) {
        boolean isCreatePostAlreadyCancelled = isCreatePostAlreadyCancelled(uploadMedia.getId());
        Timber.i("startPosting: isCreatePostAlreadyCancelled: " + isCreatePostAlreadyCancelled);
        if (isCreatePostAlreadyCancelled) {
            return;
        }

        if (index + 1 < uploadMedia.getFilePath().length) {
            upload(uploadMedia, index + 1, setting);
        } else {
            startPosting(uploadMedia, setting);
        }
    }

    abstract void onUploadSuccess(UploadMedia uploadMedia);

    abstract void onUploadFail(UploadMedia uploadMedia);

    abstract void onProgressUpload(UploadMedia uploadMedia, int percentage, int totalValue);
}
