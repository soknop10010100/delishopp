package com.proapp.sompom.viewholder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.listener.ViewModelLifeCycle;
import com.proapp.sompom.model.Media;

/**
 * Created by Veasna Chhom on 6/11/20.
 */

public class CommonBindingViewHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder implements ViewModelLifeCycle {

    private T mBinding;
    private ViewModelLifeCycle mViewModelLifeCycle;

    public CommonBindingViewHolder(T itemView) {
        super(itemView.getRoot());
        mBinding = itemView;
    }

    public T getBinding() {
        return mBinding;
    }

    public void setVariable(int variableId, Object value) {
        getBinding().setVariable(variableId, value);
        getBinding().executePendingBindings();
    }

    public void setViewModelLifeCycle(ViewModelLifeCycle viewModelLifeCycle) {
        mViewModelLifeCycle = viewModelLifeCycle;
    }

    @Override
    public void onPause() {
        if (mViewModelLifeCycle != null) {
            mViewModelLifeCycle.onPause();
        }
    }

    @Override
    public void onResume() {
        if (mViewModelLifeCycle != null) {
            mViewModelLifeCycle.onResume();
        }
    }

    @Override
    public void onResumePlaybackMedia(Media media) {
        if (mViewModelLifeCycle != null) {
            mViewModelLifeCycle.onResumePlaybackMedia(media);
        }
    }

    public Context getContext() {
        return getBinding().getRoot().getContext();
    }

    public static class Builder<T extends ViewDataBinding> {
        private ViewGroup mParent;
        @LayoutRes
        private int mLayoutRes;

        public Builder(ViewGroup parent, @LayoutRes int layoutRes) {
            mParent = parent;
            mLayoutRes = layoutRes;
        }

        public CommonBindingViewHolder<T> build() {
            final LayoutInflater inflater = LayoutInflater.from(mParent.getContext());
            final T binding = DataBindingUtil.inflate(inflater, mLayoutRes, mParent, false);
            return new CommonBindingViewHolder(binding);
        }
    }
}
