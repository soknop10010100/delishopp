package com.proapp.sompom.viewholder;

import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;

import com.google.android.exoplayer2.ui.PlayerView;
import com.proapp.sompom.utils.VolumePlaying;
import com.proapp.sompom.widget.lifestream.MediaLayout;
import com.sompom.videomanager.helper.OnButtonPlayClickListener;
import com.sompom.videomanager.model.MediaListItem;

import timber.log.Timber;

/**
 * Created by He Rotha on 7/19/18.
 */
public class TimelineViewHolder extends BindingViewHolder implements MediaListItem {
    private MediaLayout mMediaLayout;

    public TimelineViewHolder(ViewDataBinding itemView) {
        super(itemView);
        Timber.i("create");
    }

    public void setMediaLayout(MediaLayout mediaLayout, String tag) {
        mMediaLayout = mediaLayout;
        Timber.e("setMediaLayout " + getMediaTag() + ", tag: " + tag);
    }

    @Override
    public void onMediaPlay(boolean isStartPlay) {
        Timber.e("onMediaPlay " + isStartPlay + "  " + getMediaTag());
        if (mMediaLayout != null) {
            mMediaLayout.setMute(VolumePlaying.isMute(mMediaLayout.getContext()));
            mMediaLayout.onMediaPlay(isStartPlay);
        }
    }

    @Override
    public void onMediaPause() {
        Timber.i("onMediaPause " + getMediaTag());
        if (mMediaLayout != null) {
            mMediaLayout.onMediaPause();
        }
    }

    @Override
    public void onVolumeClick(OnButtonPlayClickListener listener) {
        Timber.i("onVolumeClick " + getMediaTag());
        if (mMediaLayout != null) {
            mMediaLayout.setOnVolumeClickListener(new MediaLayout.OnVolumeClickListener() {
                @Override
                public void onButtonPlayClick(boolean isPlay) {
                    listener.onButtonPlayClick(isPlay);
                }

                @Override
                public void onVolumeClick(boolean isMute) {
                    listener.onButtonMuteClick(isMute);
                }
            });
        }
    }

    @Nullable
    @Override
    public PlayerView getPlayerView() {
//        Timber.i("getPlayerView " + getMediaTag());
        if (mMediaLayout == null) {
            return null;
        }
        return mMediaLayout.getPlayerView();
    }

    public void stopOrRelease() {
        Timber.i("stopOrRelease " + getMediaTag());
        if (mMediaLayout != null) {
            mMediaLayout.release();
        }
    }

    private String getMediaTag() {
        if (mMediaLayout != null) {
            return "Hash Code: " + mMediaLayout.hashCode() + ", type: " + mMediaLayout.getMediaLayoutType();
        }

        return null;
    }
}
