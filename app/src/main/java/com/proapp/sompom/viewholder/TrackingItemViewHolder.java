package com.proapp.sompom.viewholder;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.github.vipulasri.timelineview.TimelineView;
import com.proapp.sompom.R;

/**
 * Created by Or Vitovongsak on 17/12/21.
 */
public class TrackingItemViewHolder extends BindingViewHolder {

    private int mViewType;

    public TrackingItemViewHolder(ViewDataBinding itemView, int viewType) {
        super(itemView);
        mViewType = viewType;
    }

    public int getViewType() {
        return mViewType;
    }

    public static class Builder {
        private ViewGroup mParent;
        @LayoutRes
        private int mLayoutRes;
        private int mViewType;

        public Builder(ViewGroup parent, @LayoutRes int layoutRes, int viewType) {
            mParent = parent;
            mLayoutRes = layoutRes;
            mViewType = viewType;
        }

        public TrackingItemViewHolder build() {
            final LayoutInflater inflater = LayoutInflater.from(mParent.getContext());
            final ViewDataBinding binding = DataBindingUtil.inflate(inflater, mLayoutRes, mParent, false);
            TimelineView timelineView = binding.getRoot().findViewById(R.id.timeline);
            if (timelineView != null) {
                timelineView.initLine(mViewType);
            }
            return new TrackingItemViewHolder(binding, mViewType);
        }
    }
}
