package com.proapp.sompom.viewholder;

import androidx.databinding.ViewDataBinding;

import com.google.android.gms.maps.GoogleMap;
import com.proapp.sompom.R;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.widget.PlacePreviewLayout;

import timber.log.Timber;

public class SupportPlacePreviewViewHolder extends TimelineViewHolder
        implements PlacePreviewLayout.PlacePreviewLayoutListener {

    private PlacePreviewLayout mMapView;
    private PlacePreviewLayout.PlacePreviewData mPreviewData;
    private AbsBaseActivity.ActivityLifeCycleListener mActivityLifeCycleListener;
    private AbsBaseActivity mAbsBaseActivity;

    public SupportPlacePreviewViewHolder(ViewDataBinding itemView, AbsBaseActivity baseActivity) {
        super(itemView);
        mAbsBaseActivity = baseActivity;
        mMapView = itemView.getRoot().findViewById(R.id.placePreview);
        if (mMapView != null) {
            mMapView.onCreate(null, false);
            mMapView.setListener(this);
        }
    }

    public void bindPlace(LifeStream lifeStream) {
        if (mMapView != null) {
            if (mActivityLifeCycleListener == null) {
                mActivityLifeCycleListener = new AbsBaseActivity.ActivityLifeCycleListener() {
                    @Override
                    protected void onStart() {
                        mMapView.onStart();
                    }

                    @Override
                    protected void onStop() {
                        mMapView.onStop();
                    }

                    @Override
                    protected void onResume() {
                        mMapView.onResume();
                    }

                    @Override
                    protected void onPause() {
                        mMapView.onPause();
                    }

                    @Override
                    protected void onDestroy() {
                        mMapView.onDestroy();
                    }
                };
            }
            mAbsBaseActivity.addActivityLifeCycleListener(mActivityLifeCycleListener);

            mPreviewData = new PlacePreviewLayout.PlacePreviewData();
            mPreviewData.setLatitude(lifeStream.getLatitude());
            mPreviewData.setLongitude(lifeStream.getLongitude());
            mPreviewData.setPaceTitle(lifeStream.getLocationName());
            mPreviewData.setPlaceDescription(lifeStream.getAddress());
            if (mMapView.isPreviewable()) {
                bindPreviewMap();
            }
        }
    }

    public void clearPlaceView() {
        mAbsBaseActivity.removeActivityLifeCycleListener(mActivityLifeCycleListener);
        if (mMapView != null) {
            Timber.i("clearPlaceView");
            mMapView.clearMap();
            mMapView.hideLayout();
        }
    }

    private void bindPreviewMap() {
        mMapView.showLayout();
        mMapView.bindPlace(mPreviewData);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        if (mMapView != null && mPreviewData != null) {
            bindPreviewMap();
        }
    }

    @Override
    public void onPreviewClosed() {

    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
