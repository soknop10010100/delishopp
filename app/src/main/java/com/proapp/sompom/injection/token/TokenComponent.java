package com.proapp.sompom.injection.token;

import com.proapp.sompom.helper.TokenAuthenticator;
import com.proapp.sompom.injection.controller.ControllerScope;

import dagger.Subcomponent;

/**
 * Created by Rotha on 8/19/2017.
 */
@Subcomponent(modules = {TokenModule.class})
@ControllerScope
public interface TokenComponent {
    void inject(TokenAuthenticator receiver);
}
