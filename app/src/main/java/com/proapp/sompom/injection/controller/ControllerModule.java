package com.proapp.sompom.injection.controller;

import android.app.Activity;
import android.content.Context;

import com.proapp.sompom.injection.mywallserialize.productserialize.MyWallSerializeModule;
import com.proapp.sompom.injection.notificationserialize.NotificationSerializeModule;
import com.proapp.sompom.injection.productserialize.ProductSerializeModule;
import com.proapp.sompom.injection.game.MoreGameModule;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Rotha on 8/17/2017.
 */
@Module(includes = {ProductSerializeModule.class, MoreGameModule.class, NotificationSerializeModule.class, MyWallSerializeModule.class})
public class ControllerModule {

    private Activity mActivity;

    public ControllerModule(Activity activity) {
        mActivity = activity;
    }

    @Provides
    @ControllerScope
    Context context() {
        return mActivity;
    }

    @Provides
    @ControllerScope
    Activity activity() {
        return mActivity;
    }

}
