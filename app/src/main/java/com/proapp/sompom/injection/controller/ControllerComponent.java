package com.proapp.sompom.injection.controller;


import com.proapp.sompom.injection.google.GoogleModule;
import com.proapp.sompom.newui.HomeActivity;
import com.proapp.sompom.newui.dialog.AddGroupParticipantDialog;
import com.proapp.sompom.newui.dialog.ChangeGroupNameDialog;
import com.proapp.sompom.newui.dialog.ChangePasswordDialog;
import com.proapp.sompom.newui.dialog.ConciergeFilterProductCriteriaBottomSheetDialog;
import com.proapp.sompom.newui.dialog.EditUserInfoDialog;
import com.proapp.sompom.newui.dialog.ForgotPasswordDialog;
import com.proapp.sompom.newui.dialog.ListFollowDialog;
import com.proapp.sompom.newui.dialog.OtherUserProfileDialog;
import com.proapp.sompom.newui.dialog.ThemeDialog;
import com.proapp.sompom.newui.dialog.UserStoreBottomSheetDialog;
import com.proapp.sompom.newui.fragment.AdvancedNotificationSettingFragment;
import com.proapp.sompom.newui.fragment.AllMessageFragment;
import com.proapp.sompom.newui.fragment.CallingFragment;
import com.proapp.sompom.newui.fragment.ChatFragment;
import com.proapp.sompom.newui.fragment.CheckEmailFragment;
import com.proapp.sompom.newui.fragment.ConciergeBrandDetailFragment;
import com.proapp.sompom.newui.fragment.ConciergeCategoryDetailFragment;
import com.proapp.sompom.newui.fragment.ConciergeCheckoutFragment;
import com.proapp.sompom.newui.fragment.ConciergeCurrentLocationFragment;
import com.proapp.sompom.newui.fragment.ConciergeDeliveryFragment;
import com.proapp.sompom.newui.fragment.ConciergeFragment;
import com.proapp.sompom.newui.fragment.ConciergeMyAddressFragment;
import com.proapp.sompom.newui.fragment.ConciergeNewAddressFragment;
import com.proapp.sompom.newui.fragment.ConciergeOrderDeliveryTrackingFragment;
import com.proapp.sompom.newui.fragment.ConciergeOrderHistoryDetailFragment;
import com.proapp.sompom.newui.fragment.ConciergeOrderHistoryFragment;
import com.proapp.sompom.newui.fragment.ConciergeOrderTrackingFragment;
import com.proapp.sompom.newui.fragment.ConciergeProductDetailFragment;
import com.proapp.sompom.newui.fragment.ConciergeReviewCheckoutFragment;
import com.proapp.sompom.newui.fragment.ConciergeShopCategoryFragment;
import com.proapp.sompom.newui.fragment.ConciergeShopDetailFragment;
import com.proapp.sompom.newui.fragment.ConciergeSubCategoryDetailFragment;
import com.proapp.sompom.newui.fragment.ConciergeSupplierDetailFragment;
import com.proapp.sompom.newui.fragment.ConfirmCodeFragment;
import com.proapp.sompom.newui.fragment.CouponFragment;
import com.proapp.sompom.newui.fragment.CreateTimelineFragment;
import com.proapp.sompom.newui.fragment.EditProfileFragment;
import com.proapp.sompom.newui.fragment.EmailSignUpFragment;
import com.proapp.sompom.newui.fragment.FilterCategoryFragment;
import com.proapp.sompom.newui.fragment.FilterDetailFragment;
import com.proapp.sompom.newui.fragment.ForgotPasswordFragment;
import com.proapp.sompom.newui.fragment.ForwardFragment;
import com.proapp.sompom.newui.fragment.GroupDetailFileFragment;
import com.proapp.sompom.newui.fragment.GroupDetailFragment;
import com.proapp.sompom.newui.fragment.GroupDetailLinkFragment;
import com.proapp.sompom.newui.fragment.GroupDetailPhotoAndVideoFragment;
import com.proapp.sompom.newui.fragment.GroupDetailVoiceFragment;
import com.proapp.sompom.newui.fragment.HomeFragment;
import com.proapp.sompom.newui.fragment.InBoardingFragment;
import com.proapp.sompom.newui.fragment.InputEmailLoginPasswordFragment;
import com.proapp.sompom.newui.fragment.InputEmailSignUpDataFragment;
import com.proapp.sompom.newui.fragment.InputLoginPasswordFragment;
import com.proapp.sompom.newui.fragment.LoginFragment;
import com.proapp.sompom.newui.fragment.LoginWithPhoneAndPasswordFragment;
import com.proapp.sompom.newui.fragment.LoginWithPhoneOrEmailAndPasswordFragment;
import com.proapp.sompom.newui.fragment.MyWallStreetFragment;
import com.proapp.sompom.newui.fragment.NearbyMapFragment;
import com.proapp.sompom.newui.fragment.NotificationFragment;
import com.proapp.sompom.newui.fragment.PayWithCardFragment;
import com.proapp.sompom.newui.fragment.PopUpCommentFragment;
import com.proapp.sompom.newui.fragment.ProfileSignUpFragment;
import com.proapp.sompom.newui.fragment.ReplyCommentFragment;
import com.proapp.sompom.newui.fragment.ResetPasswordFragment;
import com.proapp.sompom.newui.fragment.SearchAddressFragment;
import com.proapp.sompom.newui.fragment.SearchConversationFragment;
import com.proapp.sompom.newui.fragment.SearchGeneralContainerResultFragment;
import com.proapp.sompom.newui.fragment.SearchGeneralFragment;
import com.proapp.sompom.newui.fragment.SearchGeneralTypeResultFragment;
import com.proapp.sompom.newui.fragment.SearchProductResultFragment;
import com.proapp.sompom.newui.fragment.SplashFragment;
import com.proapp.sompom.newui.fragment.TelegramChatRequestFragment;
import com.proapp.sompom.newui.fragment.TelegramUserRedirectFragment;
import com.proapp.sompom.newui.fragment.TimelineDetailFragment;
import com.proapp.sompom.newui.fragment.UserListFragment;
import com.proapp.sompom.newui.fragment.WallStreetFragment;
import com.proapp.sompom.newui.fragment.WalletHistoryFragment;
import com.proapp.sompom.widget.LikeViewerLayout;

import dagger.Subcomponent;

/**
 * Created by Rotha on 8/17/2017.
 */

@Subcomponent(modules = {ControllerModule.class, GoogleModule.class, PublicModule.class})
@ControllerScope
public interface ControllerComponent {

    void inject(AdvancedNotificationSettingFragment fragment);

    void inject(CreateTimelineFragment fragment);

    void inject(LoginFragment loginFragment);

    void inject(SearchAddressFragment fragment);

    void inject(EditProfileFragment fragment);

    void inject(FilterCategoryFragment fragment);

    void inject(LikeViewerLayout likeViewerLayout);

    void inject(PopUpCommentFragment popUpCommentLayout);

    void inject(ChatFragment dialog);

    void inject(GroupDetailFragment groupDetail);

    void inject(AllMessageFragment fragment);

    void inject(HomeFragment fragment);

    void inject(SearchGeneralTypeResultFragment fragment);

    void inject(NotificationFragment fragment);

    void inject(ListFollowDialog fragment);

    void inject(WallStreetFragment wallStreetFragment);

    void inject(TimelineDetailFragment wallStreetFragment);

    void inject(NearbyMapFragment nearbyMapFragment);

    void inject(ReplyCommentFragment fragment);

    void inject(MyWallStreetFragment fragment);

    void inject(SearchGeneralFragment fragment);

    void inject(ForwardFragment forwardFragment);

    void inject(UserStoreBottomSheetDialog forwardFragment);

    void inject(FilterDetailFragment filterDetailFragment);

    void inject(SearchGeneralContainerResultFragment searchGeneralContainerResultFragment);

    void inject(ChangePasswordDialog dialog);

    void inject(ForgotPasswordDialog dialog);

    void inject(OtherUserProfileDialog dialog);

    void inject(UserListFragment dialog);

    void inject(SplashFragment dialog);

    void inject(ChangeGroupNameDialog dialog);

    void inject(InBoardingFragment dialog);

    void inject(HomeActivity activity);

    void inject(CallingFragment fragment);

    void inject(GroupDetailPhotoAndVideoFragment fragment);

    void inject(GroupDetailFileFragment fragment);

    void inject(GroupDetailVoiceFragment fragment);

    void inject(GroupDetailLinkFragment fragment);

    void inject(AddGroupParticipantDialog fragment);

    void inject(ThemeDialog fragment);

    void inject(SearchConversationFragment fragment);

    void inject(EmailSignUpFragment fragment);

    void inject(ProfileSignUpFragment fragment);

    void inject(ConfirmCodeFragment fragment);

    void inject(InputLoginPasswordFragment fragment);

    void inject(LoginWithPhoneAndPasswordFragment fragment);

    void inject(LoginWithPhoneOrEmailAndPasswordFragment fragment);

    void inject(ConciergeFragment fragment);

    void inject(ConciergeShopDetailFragment fragment);

    void inject(ConciergeDeliveryFragment fragment);

    void inject(ConciergeCheckoutFragment fragment);

    void inject(ConciergeOrderHistoryFragment fragment);

    void inject(ConciergeOrderHistoryDetailFragment fragment);

    void inject(ConciergeProductDetailFragment fragment);

    void inject(ConciergeOrderTrackingFragment fragment);

    void inject(ConciergeCurrentLocationFragment fragment);

    void inject(ConciergeMyAddressFragment fragment);

    void inject(ConciergeNewAddressFragment fragment);

    void inject(ConciergeOrderDeliveryTrackingFragment fragment);

    void inject(ConciergeShopCategoryFragment fragment);

    void inject(SearchProductResultFragment fragment);

    void inject(CheckEmailFragment fragment);

    void inject(InputEmailSignUpDataFragment fragment);

    void inject(InputEmailLoginPasswordFragment fragment);

    void inject(TelegramChatRequestFragment fragment);

    void inject(TelegramUserRedirectFragment fragment);

    void inject(ConciergeReviewCheckoutFragment fragment);

    void inject(PayWithCardFragment fragment);

    void inject(ConciergeBrandDetailFragment fragment);

    void inject(ConciergeSubCategoryDetailFragment fragment);

    void inject(ConciergeFilterProductCriteriaBottomSheetDialog fragment);

    void inject(ForgotPasswordFragment fragment);

    void inject(ResetPasswordFragment fragment);

    void inject(ConciergeCategoryDetailFragment fragment);

    void inject(ConciergeSupplierDetailFragment fragment);

    void inject(EditUserInfoDialog fragment);

    void inject(CouponFragment fragment);

    void inject(WalletHistoryFragment fragment);
}
