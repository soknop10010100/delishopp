package com.proapp.sompom.injection.token;

import android.content.Context;

import com.proapp.sompom.injection.controller.ControllerScope;
import com.proapp.sompom.injection.controller.PublicModule;
import com.proapp.sompom.injection.productserialize.ProductSerializeModule;
import com.proapp.sompom.injection.game.MoreGameModule;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Rotha on 8/17/2017.
 */
@Module(includes = {ProductSerializeModule.class, MoreGameModule.class, PublicModule.class})
public class TokenModule {

    private Context mContext;

    public TokenModule(Context context) {
        mContext = context;
    }

    @Provides
    @ControllerScope
    Context context() {
        return mContext;
    }

}
