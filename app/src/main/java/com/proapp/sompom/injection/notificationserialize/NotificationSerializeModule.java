package com.proapp.sompom.injection.notificationserialize;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.proapp.sompom.helper.NotificationAdapterDeserializer;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.injection.controller.ControllerScope;
import com.proapp.sompom.injection.controller.NetworkModule;
import com.proapp.sompom.services.ApiService;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by He Rotha on 10/2/17.
 */
@Module(includes = NetworkModule.class)
public class NotificationSerializeModule {

    @Provides
    @NotificationSerializeQualifier
    @ControllerScope
    public ApiService getProductService(@NotificationSerializeQualifier Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @NotificationSerializeQualifier
    @ControllerScope
    public Retrofit.Builder getProductRetrofitBuilder(OkHttpClient okHttpClient,
                                                      @NotificationSerializeQualifier Gson gson) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));
    }

    @Provides
    @NotificationSerializeQualifier
    @ControllerScope
    public Retrofit getProductRetrofit(@ControllerScope String url, @NotificationSerializeQualifier Retrofit.Builder builder) {
        return builder.baseUrl(url).build();
    }

    @Provides
    @NotificationSerializeQualifier
    @ControllerScope
    public Gson getDeserializerGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LoadMoreWrapper.class, new NotificationAdapterDeserializer());
        return builder.create();
    }
}
