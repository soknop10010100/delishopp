package com.proapp.sompom.injection.broadcast;

import android.content.Context;

import com.proapp.sompom.injection.controller.ControllerScope;
import com.proapp.sompom.injection.controller.NetworkModule;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Rotha on 8/17/2017.
 */
@Module(includes = NetworkModule.class)
public class BroadcastModule {

    private Context mContext;

    public BroadcastModule(Context context) {
        mContext = context;
    }

    @Provides
    @ControllerScope
    Context context() {
        return mContext;
    }


}
