package com.proapp.sompom.injection.game;

import com.google.gson.Gson;
import com.proapp.sompom.injection.controller.ControllerScope;
import com.proapp.sompom.injection.controller.NetworkModule;
import com.proapp.sompom.services.ApiService;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import io.sentry.android.okhttp.SentryOkHttpInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by He Rotha on 10/2/17.
 */
@Module(includes = NetworkModule.class)
public class MoreGameModule {

    @Provides
    @MoreGameAPIQualifier
    @ControllerScope
    public ApiService getMoreGameService(@MoreGameAPIQualifier Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @MoreGameAPIQualifier
    @ControllerScope
    public Retrofit getMoreGameRetrofit(@MoreGameAPIQualifier Retrofit.Builder builder) {
        return builder.baseUrl("http://www.sompom.com/").build();
    }

    @Provides
    @MoreGameAPIQualifier
    @ControllerScope
    public Retrofit.Builder getMoreGameRetrofitBuilder(@MoreGameAPIQualifier OkHttpClient okHttpClient,
                                                       Gson gson) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));
    }


    @Provides
    @MoreGameAPIQualifier
    @ControllerScope
    public OkHttpClient getMoreGameOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor,
                                                SentryOkHttpInterceptor sentryOkHttpInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(sentryOkHttpInterceptor)
                .readTimeout(1, TimeUnit.MINUTES)
                .connectTimeout(1, TimeUnit.MINUTES)
                .build();
    }
}
