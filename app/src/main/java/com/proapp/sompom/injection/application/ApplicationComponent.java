package com.proapp.sompom.injection.application;

import com.proapp.sompom.injection.broadcast.BroadcastComponent;
import com.proapp.sompom.injection.broadcast.BroadcastModule;
import com.proapp.sompom.injection.controller.ControllerComponent;
import com.proapp.sompom.injection.controller.ControllerModule;
import com.proapp.sompom.injection.token.TokenComponent;
import com.proapp.sompom.injection.token.TokenModule;

import dagger.Component;

/**
 * Created by Rotha on 8/17/2017.
 */

@ApplicationScope
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    // Each subcomponent can depend on more than one module
    ControllerComponent newControllerComponent(ControllerModule module);

    //    BroadcastComponent newServiceComponent(BroadcastModule module);
    BroadcastComponent newBroadcastComponent(BroadcastModule serviceModule);

    TokenComponent newTokenComponent(TokenModule serviceModule);


}