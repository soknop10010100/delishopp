package com.proapp.sompom.injection.controller;

import android.content.Context;

import com.google.gson.Gson;
import com.proapp.sompom.helper.GsonHelper;
import com.proapp.sompom.helper.HeaderInterceptor;
import com.proapp.sompom.helper.TokenAuthenticator;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.ServerUrl;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import io.sentry.android.okhttp.SentryOkHttpInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by He Rotha on 10/2/17.
 */
@Module
public class NetworkModule {

    @Provides
    @ControllerScope
    public String getUrl(Context context) {
        return ServerUrl.generateUrl(context);
    }

    @Provides
    @ControllerScope
    public Gson getGson() {
        return GsonHelper.getGsonForAPICommunication();
    }

    @Provides
    @ControllerScope
    public HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor(message -> Timber.i(message));
        logger.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logger;
    }

    @Provides
    @ControllerScope
    public HeaderInterceptor getHeaderInterceptor(Context context) {
        return new HeaderInterceptor(context);
    }

    @Provides
    @ControllerScope
    public SentryOkHttpInterceptor getSentryOkHttpInterceptor() {
        return new SentryOkHttpInterceptor();
    }

    @Provides
    @ControllerScope
    public TokenAuthenticator getAuthenticator(Context context) {
        return new TokenAuthenticator(context);
    }

    @Provides
    @ControllerScope
    public ApiService getServices(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @ControllerScope
    public Retrofit.Builder getRetrofitBuilder(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));
    }

    @Provides
    @ControllerScope
    public Retrofit getServerRetrofit(Retrofit.Builder builder, String url) {
        return builder.baseUrl(url).build();
    }

    @Provides
    @ControllerScope
    public OkHttpClient getOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor,
                                        HeaderInterceptor headerInterceptor,
                                        SentryOkHttpInterceptor sentryOkHttpInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(sentryOkHttpInterceptor)
                .readTimeout(1, TimeUnit.MINUTES)
                .connectTimeout(1, TimeUnit.MINUTES)
                .addInterceptor(headerInterceptor)
                .build();
    }
}
