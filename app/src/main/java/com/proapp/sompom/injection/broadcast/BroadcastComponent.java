package com.proapp.sompom.injection.broadcast;

import com.proapp.sompom.broadcast.NotificationHandlerService;
import com.proapp.sompom.broadcast.upload.LifeStreamUploadService;
import com.proapp.sompom.chat.call.CallingService;
import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.injection.controller.ControllerScope;

import dagger.Subcomponent;

/**
 * Created by Rotha on 8/19/2017.
 */
@Subcomponent(modules = {BroadcastModule.class})
@ControllerScope
public interface BroadcastComponent {

    void inject(LifeStreamUploadService lifeStreamUploadService);

    void inject(SocketService socketBinder);

    void inject(CallingService socketBinder);

    void inject(NotificationHandlerService oneSignalReceiver);
}
