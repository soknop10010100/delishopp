package com.proapp.sompom.widget.segmentedcontrol;

import android.annotation.TargetApi;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.LayoutSegmentedButtonBinding;
import com.resourcemanager.helper.FontHelper;

import timber.log.Timber;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class SegmentedButton extends LinearLayout {

    private LayoutSegmentedButtonBinding mBinding;
    @ColorInt
    private int mSelectedTextColor;
    @ColorInt
    private int mUnSelectedTextColor;
    @DrawableRes
    private int mSelectedBackground;
    @DrawableRes
    private int mUnSelectedBackground;
    private OnSegmentedButtonClick mSegmentedButtonClick;

    public SegmentedButton(Context context) {
        super(context);
        init();
    }

    public SegmentedButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SegmentedButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SegmentedButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void setSelectedTextColor(int selectedTextColor) {
        mSelectedTextColor = selectedTextColor;
    }

    public void setUnSelectedTextColor(int unSelectedTextColor) {
        mUnSelectedTextColor = unSelectedTextColor;
    }

    public void setSelectedBackground(int selectedBackground) {
        mSelectedBackground = selectedBackground;
    }

    public void setUnSelectedBackground(int unSelectedBackground) {
        mUnSelectedBackground = unSelectedBackground;
    }

    public void setIsSelected(boolean isSelected) {
        if (isSelected) {
//            mBinding.textView.setTextColor(mSelectedTextColor);
//            Currently set all selected button to have white text and green background
            mBinding.textView.setTextColor(getResources().getColor(R.color.white));
            setBackground(ContextCompat.getDrawable(getContext(), mSelectedBackground));
            mBinding.textView.setTypeface(FontHelper.getBoldFontStyle(getContext()));
        } else {
            mBinding.textView.setTextColor(mUnSelectedTextColor);
            setBackground(ContextCompat.getDrawable(getContext(), mUnSelectedBackground));
            mBinding.textView.setTypeface(FontHelper.getRegularFontStyle(getContext()));
        }
    }

    public void setTitle(@StringRes int title) {
        mBinding.textView.setText(title);
    }

    public void setOnSegmentedButtonClick(OnSegmentedButtonClick onSegmentedButtonClick) {
        mSegmentedButtonClick = onSegmentedButtonClick;
    }

    private void init() {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_segmented_button,
                this,
                true);
        setOnClickListener(v -> {
            if (mSegmentedButtonClick != null) {
                mSegmentedButtonClick.onClick(v);
            }
        });
    }

    public interface OnSegmentedButtonClick {
        void onClick(View view);
    }
}
