package com.proapp.sompom.widget;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.UserListAdapter;
import com.proapp.sompom.databinding.LayoutLikeViewerBinding;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnLayoutLikeListener;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.dialog.AbsBindingFragmentDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.LikeViewerDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.newviewmodel.LayoutLikeViewModel;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class LikeViewerLayout extends AbsBindingFragmentDialog<LayoutLikeViewerBinding>
        implements OnLayoutLikeListener, LayoutLikeViewModel.LayoutLikeViewModelListener {
    public static final String TAG = LikeViewerLayout.class.getName();

    @Inject
    public ApiService mApiService;

    private LayoutLikeViewModel mViewModel;
    private UserListAdapter<User> mViewerAdapter;

    public static LikeViewerLayout newInstance(String productId) {
        Bundle args = new Bundle();
        args.putString(SharedPrefUtils.DATA, productId);
        LikeViewerLayout fragment = new LikeViewerLayout();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        String productId = getArguments().getString(SharedPrefUtils.DATA);
        getBinding().buttonClose.setOnClickListener(v -> dismiss());
//        getBinding().textviewClose.setOnClickListener(v -> dismiss());

        LikeViewerDataManager dataManager = new LikeViewerDataManager(getActivity(), mApiService);
        mViewModel = new LayoutLikeViewModel(productId, dataManager, this, this);
        setVariable(BR.viewModel, mViewModel);
        mViewModel.getData();
    }

    @Override
    public void onComplete(List<User> result, boolean canLoadMore) {
        LoaderMoreLayoutManager layoutManager = new LoaderMoreLayoutManager(getActivity());
        if (canLoadMore) {
            layoutManager.setOnLoadMoreListener(() -> mViewModel.loadMore(new OnCallbackListListener<List<User>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    mViewerAdapter.setCanLoadMore(false);
                    layoutManager.setOnLoadMoreListener(null);
                    layoutManager.loadingFinished();
                }

                @Override
                public void onComplete(List<User> result, boolean canLoadMore) {
                    mViewerAdapter.addLoadMoreData(result);
                    mViewerAdapter.setCanLoadMore(canLoadMore);
                    layoutManager.loadingFinished();
                }
            }));
        }
        getBinding().recyclerview.setLayoutManager(layoutManager);

        mViewerAdapter = new UserListAdapter<>(result, requireContext());
        mViewerAdapter.setListener(new UserListAdapter.UserListAdapterListener() {
            @Override
            public void onAttemptedCallWhenNoNetworkConnectedOrDeviceIsInAnotherCallProcess(boolean isInAnotherCall) {
                showSnackBar(R.string.call_toast_no_internet, true);
            }

            @Override
            public void onMakeCallError(String error) {
                showSnackBar(error, true);
            }
        });
        mViewerAdapter.setCanLoadMore(canLoadMore);
        getBinding().recyclerview.setAdapter(mViewerAdapter);
    }

    @Override
    public void onNotifyItem() {
        if (mViewerAdapter != null) {
            mViewerAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.layout_like_viewer;
    }

    @Override
    public void onButtonBackClicked() {
        dismiss();
    }
}
