package com.proapp.sompom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.StoreViewPagerAdapter;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.listener.SupportConciergeCheckoutBottomSheetDisplay;
import com.proapp.sompom.model.result.AppFeature;
import com.proapp.sompom.newui.fragment.AbsBaseFragment;
import com.proapp.sompom.newui.fragment.ConciergeFragment;
import com.proapp.sompom.newui.fragment.ConciergeShopCategoryFragment;
import com.proapp.sompom.newui.fragment.EmptyFragment;
import com.proapp.sompom.newui.fragment.MessageFragment;
import com.proapp.sompom.newui.fragment.UserListFragment;
import com.proapp.sompom.newui.fragment.WallStreetFragment;

import java.util.ArrayList;
import java.util.List;

import io.sentry.Sentry;

/**
 * Created by He Rotha on 9/20/18.
 */
public class HomeViewPager extends ViewPager {

    private final List<AbsBaseFragment> mFragments = new ArrayList<>();
    private final List<AppFeature.NavigationBarMenu> mNavMenus = new ArrayList<>();
    private boolean mIsEnableSwipe = true;
    private boolean mLoadAppToolbarFailed = false;

    public HomeViewPager(@NonNull Context context) {
        super(context);
    }

    public HomeViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void setupAdapter(Context context,
                             FragmentManager manager,
                             com.proapp.sompom.listener.OnClickListener callback,
                             boolean isLoadMessageData,
                             HomeTabLayout homeTabLayout) {
        mLoadAppToolbarFailed = false;
        mFragments.clear();
        AppFeature appFeature = ApplicationHelper.getAppFeature(context);
        if (appFeature != null) {
            // Dynamically build the toolbar menu button based on AppFeature retrieved from server.
            if (appFeature.getNavigationBarMenu() != null && !appFeature.getNavigationBarMenu().isEmpty()) {
                for (AppFeature.NavigationBarMenu navigationBarMenu : appFeature.getNavigationBarMenu()) {
                    if (navigationBarMenu.isEnabled() &&
                            navigationBarMenu.getNavMenuScreen() != null &&
                            navigationBarMenu.getNavMenuButton() != null) {
                        mNavMenus.add(navigationBarMenu);
                        switch (navigationBarMenu.getNavMenuScreen()) {
                            case Wall:
                                mFragments.add(WallStreetFragment.newInstance());
                                break;
                            case Conversation:
                                mFragments.add(MessageFragment.newInstance(isLoadMessageData));
                                break;
                            case Contact:
                                mFragments.add(UserListFragment.newInstance());
                                break;
                            case Shop:
                                mFragments.add(ConciergeFragment.newInstance());
                                break;
                            case ShopCategory:
                                mFragments.add(ConciergeShopCategoryFragment.newInstance());
                                break;
                            case ShopDetail:
                            case Chat:
                                mFragments.add(EmptyFragment.newInstance());
                                break;
                        }
                    }
                }
            }
        } else {
            Sentry.captureException(new Exception("AppFeature is null when tyring to build all sections"));
            // If AppFeature is null, show error screen notifying the user that they should restart
            // the app.
            mLoadAppToolbarFailed = true;
            mFragments.clear();
            mFragments.add(EmptyFragment.newInstance(getContext().getString(R.string.post_error_message),
                    getContext().getString(R.string.error_general_description)));
        }

        if (mFragments.isEmpty()) {
            mLoadAppToolbarFailed = true;
            // Somehow, the screen list is empty, which shouldn't be possible and shouldn't have
            // happened. Show an error screen notifying user that they should restart the app.
            mFragments.add(EmptyFragment.newInstance(getContext().getString(R.string.post_error_message),
                    getContext().getString(R.string.error_general_description)));
        }

        checkShouldEnableSwipe();
        StoreViewPagerAdapter adapter = new StoreViewPagerAdapter(manager, mFragments);
        setAdapter(adapter);
        setOffscreenPageLimit(adapter.getCount());
    }

    public boolean isLoadAppToolbarFailed() {
        return mLoadAppToolbarFailed;
    }

    public void checkShouldEnableSwipe() {
        mIsEnableSwipe = true;
        for (AppFeature.NavigationBarMenu menu : mNavMenus) {
            if (AppFeature.NavMenuScreen.isOpenNewScreenType(menu.getNavMenuScreen())) {
                mIsEnableSwipe = false;
                break;
            }
        }
    }

    public void onAppBarOffsetChange(int offset) {
        for (AbsBaseFragment fragment : mFragments) {
            if (fragment instanceof SupportConciergeCheckoutBottomSheetDisplay) {
                ((SupportConciergeCheckoutBottomSheetDisplay) fragment).onAppBarOffsetChange(offset);
            }
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // Intercept user swipe event to determine if user is able to swipe the page or not.
        // TODO: Should we feedback to user if they cannot swipe? To avoid confusing user when they
        //  cannot swipe the tabs
        if (mIsEnableSwipe) {
            return super.onInterceptTouchEvent(event);
        } else {
            return false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Intercept user swipe event to determine if user is able to swipe the page or not.
        // TODO: Should we feedback to user if they cannot swipe? To avoid confusing user when they
        //  cannot swipe the tabs
        if (mIsEnableSwipe) {
            return super.onTouchEvent(event);
        } else {
            return false;
        }
    }

    public List<AbsBaseFragment> getAllFragments() {
        return mFragments;
    }

    public List<AppFeature.NavigationBarMenu> getNavMenus() {
        return mNavMenus;
    }

    public AbsBaseFragment getCurrentFragment() {
        return mFragments.get(getCurrentItem());
    }

    public AbsBaseFragment getFragmentAt(int position) {
        return mFragments.get(position);
    }
}
