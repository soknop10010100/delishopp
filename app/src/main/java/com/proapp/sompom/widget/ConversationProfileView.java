package com.proapp.sompom.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.example.usermentionable.utils.GlideUtil;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.listener.LiveUserDataListener;
import com.proapp.sompom.chat.listener.SupportLiveUserData;
import com.proapp.sompom.chat.service.LiveUserDataService;
import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.databinding.LayoutConversationProfileBinding;
import com.proapp.sompom.model.emun.Presence;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.utils.GlideLoadUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConversationProfileView extends LinearLayout implements SupportLiveUserData {

    private static final float GROUP_PROFILE_PERCENTAGE_OF_PARENT_SIZE = 0.75f; //Percentage of parent

    private LayoutConversationProfileBinding mBinding;
    private boolean mIsShowStatus;
    private SocketService.SocketBinder mBinder;
    private Context mApplicationContext;
    private HashMap<User, ImageView> mImageViewProfileMap = new HashMap<>();
    private Presence mPresence;
    private Date mLastActiveDate;
    private Map<String, LiveUserDataListener> mLiveUserDataListeners = new HashMap<>();
    private ConversationProfileViewCallback mConversationProfileViewCallback;
    private List<User> mParticipants = new ArrayList<>();
    private boolean mDisplayGroupProfile;

    public ConversationProfileView(Context context) {
        super(context);
        init(null);
    }

    public ConversationProfileView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ConversationProfileView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ConversationProfileView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    public void setConversationProfileViewCallback(ConversationProfileViewCallback conversationProfileViewCallback) {
        mConversationProfileViewCallback = conversationProfileViewCallback;
    }

    public void setApplicationContext(Context applicationContext) {
        mApplicationContext = applicationContext;
    }

    private void init(AttributeSet attr) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_conversation_profile,
                this,
                true);
        if (attr != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attr,
                    R.styleable.ConversationProfileView,
                    0,
                    0);
            mIsShowStatus = typedArray.getBoolean(R.styleable.ConversationProfileView_showStatus,
                    false);
            typedArray.recycle();
        }
    }

    public void setShowStatus(boolean showStatus) {
        mIsShowStatus = showStatus;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
//        Timber.i("onAttachedToWindow");
        if (mBinder != null && mParticipants != null && !mParticipants.isEmpty()) {
            registerLiveUserDataListenerFoParticipant(mParticipants);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
//        Timber.i("onDetachedFromWindow");
        removeAllLiveUserDataRegisterListener();
    }

    private void removeAllLiveUserDataRegisterListener() {
        if (mBinder != null) {
            for (LiveUserDataListener value : mLiveUserDataListeners.values()) {
                if (value != null) {
                    mBinder.removeLiveUserDataListener(value);
                }
            }
        }
    }

    private void updateOnlineStatusViewSize() {
        post(() -> {
            int width = getWidth();
            mBinding.imageViewOnlineStatus.setVisibility(mIsShowStatus ? View.VISIBLE : View.GONE);
            int iconSize = width * getResources().getInteger(R.integer.single_status_size_percent) / 100;
//            Timber.i("assignGroupStatusSize: width: " + width + ", mIsShowStatus: " + mIsShowStatus + ", icon status size: " + iconSize);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(iconSize, iconSize);
            params.gravity = Gravity.BOTTOM | Gravity.END;
            mBinding.imageViewOnlineStatus.setLayoutParams(params);
        });
    }

    private void setUserGroupStatus(List<User> participants) {
        if (getContext() instanceof AbsBaseActivity) {
            if (mBinder == null) {
                ((AbsBaseActivity) getContext()).addOnServiceListener(binder -> {
                    mBinder = (SocketService.SocketBinder) binder;
                    registerLiveUserDataListenerFoParticipant(participants);
                    mBinder.getGroupLastActiveInfo(participants,
                            new LiveUserDataService.LiveUserDataServiceCallback() {
                                @Override
                                protected void onGotGroupLastActiveInfo(Date lastActiveDate, User lastActiveUser) {
                                    if (lastActiveUser != null) {
                                        //Need to apply the status online view at the first set up too.
                                        updateOnlineStatusView(lastActiveUser.isOnline() != null && lastActiveUser.isOnline());
                                        mBinder.setUserStatus(lastActiveUser, false, false, "ConversationProfileView setUserGroupStatus");
                                    }
                                    onBroadcastGotDefaultUserOnlineStatus(lastActiveDate, lastActiveUser);
                                }
                            });
                });
            } else {
                registerLiveUserDataListenerFoParticipant(participants);
                mBinder.getGroupLastActiveInfo(participants,
                        new LiveUserDataService.LiveUserDataServiceCallback() {
                            @Override
                            protected void onGotGroupLastActiveInfo(Date lastActiveDate, User lastActiveUser) {
                                if (lastActiveUser != null) {
                                    //Need to apply the status online view at the first set up too.
                                    updateOnlineStatusView(lastActiveUser.isOnline() != null && lastActiveUser.isOnline());
                                    mBinder.setUserStatus(lastActiveUser, false, false, "ConversationProfileView setUserGroupStatus");
                                }
                                onBroadcastGotDefaultUserOnlineStatus(lastActiveDate, lastActiveUser);
                            }
                        });
            }
        }
    }

    private void onBroadcastGotDefaultUserOnlineStatus(Date lastActiveDate, User lastActiveUser) {
        if (lastActiveUser != null) {
            Presence presence = Presence.Offline;
            if (lastActiveUser.isOnline() != null) {
                presence = lastActiveUser.isOnline() ? Presence.Online : Presence.Offline;
            }
            if (mConversationProfileViewCallback != null) {
                mConversationProfileViewCallback.onGotDefaultUserOnlineStatus(lastActiveUser.getId(), presence, lastActiveDate);
            }
        }
    }

    private void registerLiveUserDataListenerFoParticipant(List<User> participants) {
        for (User participant : participants) {
            LiveUserDataListener listener = mLiveUserDataListeners.get(participant.getId());
            if (listener == null) {
                listener = new LiveUserDataListener() {
                    @Override
                    public void onPresenceUpdate(String userId, Presence presence, Date lastOnlineTime) {
//                        Timber.i("onPresenceUpdate of " + getUserName() +
//                                ", userId: " + userId +
//                                ", current presence: " + mPresence +
//                                ", update presence: " + presence +
//                                ", lastOnlineTime : " + lastOnlineTime +
//                                ", mPresence" + mPresence +
//                                ", mLastActiveDate: " + mLastActiveDate +
//                                ", activity: " + getActivityContainerName());
                        ((Activity) getContext()).runOnUiThread(() -> {
                            if (isUpdateLastActiveDateValid(lastOnlineTime)) {
                                if (presence == Presence.Online) {
                                    mPresence = presence;
                                    applyOnlineStatusUpdate(userId, presence, lastOnlineTime);
                                } else if (presence == Presence.Offline && (mPresence == null || mPresence == Presence.Online)) {
                                    mPresence = presence;
                                    applyOnlineStatusUpdate(userId, presence, lastOnlineTime);
                                }
                            }
                        });
                    }

                    @Override
                    public String getUserId() {
                        return participant.getId();
                    }

                    @Override
                    public void onProfileAvatarUpdated(String userId, int hash, String userProfile) {
                        if (!TextUtils.isEmpty(userProfile) && !mDisplayGroupProfile) {
//                            Timber.i("onProfileAvatarUpdated of " + userId + ", userProfile: " + userProfile);
                            for (Map.Entry<User, ImageView> entry : mImageViewProfileMap.entrySet()) {
                                if (entry != null && entry.getKey() != null && entry.getValue() != null) {
                                    if (TextUtils.equals(entry.getKey().getId(), userId)) {
                                        entry.getKey().setUserProfileThumbnail(userProfile);
                                        entry.getKey().setHash(hash);
                                        new Handler(getContext().getMainLooper()).post(() -> {
                                            loadProfile(entry.getValue(), entry.getKey(), false);
//                                        Timber.i("Apply update profile avatar of " + entry.getKey().getFullName());
                                        });
                                        break;
                                    }
                                }
                            }
                        }
                    }
                };
                mLiveUserDataListeners.put(participant.getId(), listener);
            }
            mBinder.addLiveUserDataListener(listener);
        }
    }

    private boolean isUpdateLastActiveDateValid(Date updateDate) {
        if (mLastActiveDate == null) {
            return true;
        }

        return updateDate != null && updateDate.getTime() >= mLastActiveDate.getTime();
    }

    private void setUpIndividualOnlineStatus(User trackingUser) {
        if (getContext() instanceof AbsBaseActivity) {
//            Timber.i("setUpIndividualOnlineStatus: isOnline: " + trackingUser.isOnline() +
//                    ", getLastActivity: " + trackingUser.getLastActivity() +
//                    ", name: " + trackingUser.getFullName());
            //Need to apply the status online view at the first set up too.
            updateOnlineStatusView(trackingUser.isOnline() != null && trackingUser.isOnline());
            if (mBinder == null) {
                ((AbsBaseActivity) getContext()).addOnServiceListener(binder -> {
                    mBinder = (SocketService.SocketBinder) binder;
                    registerLiveUserDataListenerFoParticipant(Collections.singletonList(trackingUser));
                    mBinder.setUserStatus(trackingUser, false, false, "ConversationProfileView setUpIndividualOnlineStatus");
                });
            } else {
                registerLiveUserDataListenerFoParticipant(Collections.singletonList(trackingUser));
                mBinder.setUserStatus(trackingUser, false, false, "ConversationProfileView setUpIndividualOnlineStatus");
            }
            onBroadcastGotDefaultUserOnlineStatus(trackingUser.getLastActivity(), trackingUser);
        }
    }

    private void applyOnlineStatusUpdate(String userId, Presence presence, Date lastOnlineTime) {
//        Timber.i("applyOnlineStatusUpdate: userId: " + userId + ", presence: " + presence + ", lastOnlineTime: " + lastOnlineTime);
        mLastActiveDate = lastOnlineTime;
        updateOnlineStatusView(presence == Presence.Online);
        if (mConversationProfileViewCallback != null) {
            mConversationProfileViewCallback.onPresenceUpdate(userId, presence, lastOnlineTime);
        }
    }

    private void updateOnlineStatusView(boolean isOnline) {
//        Timber.i("updateOnlineStatusView: isOnline: " + isOnline + ", mIsShowStatus: " + mIsShowStatus);
        if (getViewContext() instanceof AppCompatActivity && !((AppCompatActivity) getViewContext()).isFinishing()) {
            ((AppCompatActivity) getViewContext()).runOnUiThread(() -> {
                if (mIsShowStatus) {
                    mBinding.imageViewOnlineStatus.setVisibility(VISIBLE);
                    if (isOnline) {
                        mBinding.imageViewOnlineStatus.setImageDrawable(ContextCompat.getDrawable(getContext(),
                                R.drawable.user_online_status_background));
//                        Timber.i("Will update online status of " + getUserName() + " to online.");
                    } else {
                        mBinding.imageViewOnlineStatus.setImageDrawable(ContextCompat.getDrawable(getContext(),
                                R.drawable.user_offline_status_background));
//                        Timber.i("Will update online status of " + getUserName() + " to offline.");
                    }
                }
            });
        }
    }

    @Override
    public User getUser() {
        return null;
    }

    private RelativeLayout.LayoutParams getProfileLayoutParam(int size, int marginLeft) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(size, size);
        layoutParams.leftMargin = marginLeft;
        return layoutParams;
    }

    private void loadProfile(ImageView imageView, User user, boolean shouldAddToMap) {
//        Timber.i("Load profile of " + user.getFullName());
        loadImage(imageView,
                user.getUserProfileThumbnail(),
                GlideUtil.getDrawableProfileFromName(getContext(),
                        user.getFirstName(),
                        user.getLastName(), false));
        if (shouldAddToMap) {
            mImageViewProfileMap.put(user, imageView);
        }
    }

    private void loadImage(ImageView imageView, String url, Drawable placeHolder) {
        Context context = mApplicationContext;
        if (context == null) {
            context = imageView.getContext();
        }
        if (placeHolder == null) {
            placeHolder = ContextCompat.getDrawable(context, R.drawable.ic_photo_placeholder_400dp);
        }
        GlideLoadUtil.loadCircle2(context, imageView, url, placeHolder);
    }

    private void setProfileGroupSize(int calculatedSize,
                                     ImageView imageView,
                                     User user,
                                     int marginLeft) {
        imageView.setLayoutParams(getProfileLayoutParam(calculatedSize, marginLeft));
        imageView.setVisibility(View.VISIBLE);
        loadProfile(imageView, user, true);
    }

    public void bindData(Context context,
                         List<User> participants,
                         String groupImgUrl,
                         String groupName) {
        mParticipants = participants;
        mPresence = null;
        mLastActiveDate = null;
//        Timber.i("bindData: groupName: " + groupName +
//                ", groupImgUrl: " + groupImgUrl +
//                ", participants: " + participants +
//                ", mIsShowStatus: " + mIsShowStatus);
        if (mIsShowStatus) {
            mBinding.imageViewOnlineStatus.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(groupImgUrl)) {
            mDisplayGroupProfile = true;
            // If the conversation is the group type and the group image is available put the image
            // as the profile of the group
            mBinding.textViewMore.setVisibility(View.GONE);
            mBinding.profileImageViewSingle.setVisibility(View.VISIBLE);
            mBinding.groupProfileRootView.setVisibility(GONE);
            updateOnlineStatusViewSize();
            setUserGroupStatus(participants);

            loadImage(mBinding.profileImageViewSingle,
                    groupImgUrl,
                    GlideUtil.getDrawableProfileFromName(context, groupName, null, false));
            return;
        }

        mDisplayGroupProfile = false;

        if (participants.size() == 1) {
            mBinding.groupProfileRootView.setVisibility(GONE);
            mBinding.textViewMore.setVisibility(View.GONE);
            mBinding.profileImageViewSingle.setVisibility(View.VISIBLE);

            updateOnlineStatusViewSize();
            //Single profile size will match its parent
            loadProfile(mBinding.profileImageViewSingle, participants.get(0), true);
            setUpIndividualOnlineStatus(participants.get(0));
        } else if (participants.size() == 2) {
            calculateSingleProfileSize(
                    childSizeDefault -> {
                        mBinding.groupProfileRootView.setVisibility(View.VISIBLE);
                        mBinding.profileImageView3.setVisibility(View.GONE);
                        mBinding.textViewMore.setVisibility(View.GONE);
                        mBinding.profileImageViewSingle.setVisibility(View.GONE);

                        setProfileGroupSize(childSizeDefault, mBinding.profileImageView1,
                                participants.get(0),
                                getMarginLeftSizeCompareToRootSize(0f));
                        setProfileGroupSize(childSizeDefault, mBinding.profileImageView2,
                                participants.get(1),
                                getMarginLeftSizeCompareToRootSize(0.25f));
                        updateOnlineStatusViewSize();
                        setUserGroupStatus(participants);
                    });
        } else if (participants.size() == 3) {
            calculateSingleProfileSize(
                    childSizeDefault -> {
                        mBinding.groupProfileRootView.setVisibility(View.VISIBLE);
                        mBinding.profileImageView3.setVisibility(View.VISIBLE);
                        mBinding.textViewMore.setVisibility(View.GONE);
                        mBinding.profileImageViewSingle.setVisibility(View.GONE);

                        setProfileGroupSize(childSizeDefault, mBinding.profileImageView1,
                                participants.get(0),
                                getMarginLeftSizeCompareToRootSize(0f));
                        setProfileGroupSize(childSizeDefault, mBinding.profileImageView2,
                                participants.get(1),
                                getMarginLeftSizeCompareToRootSize(0.1f));
                        setProfileGroupSize(childSizeDefault, mBinding.profileImageView3,
                                participants.get(2),
                                getMarginLeftSizeCompareToRootSize(0.25f));
                        updateOnlineStatusViewSize();
                        setUserGroupStatus(participants);
                    });
        } else {
            calculateSingleProfileSize(
                    childSizeDefault -> {
                        mBinding.groupProfileRootView.setVisibility(View.VISIBLE);
                        mBinding.profileImageView3.setVisibility(View.GONE);
                        mBinding.textViewMore.setVisibility(View.VISIBLE);
                        mBinding.profileImageViewSingle.setVisibility(View.GONE);

                        setProfileGroupSize(childSizeDefault, mBinding.profileImageView1,
                                participants.get(0),
                                getMarginLeftSizeCompareToRootSize(0f));
                        setProfileGroupSize(childSizeDefault, mBinding.profileImageView2,
                                participants.get(1),
                                getMarginLeftSizeCompareToRootSize(0.1f));
                        assignMoreProfileText(childSizeDefault,
                                getMarginLeftSizeCompareToRootSize(0.25f),
                                participants.size());
                        updateOnlineStatusViewSize();
                        setUserGroupStatus(participants);
                    });
        }
    }

    private void calculateSingleProfileSize(InternalCallback callback) {
        post(() -> {
            if (getWidth() > 0) {
                int childSizeDefault = (int) (getWidth() * GROUP_PROFILE_PERCENTAGE_OF_PARENT_SIZE);
                if (callback != null) {
                    callback.onCalculateChildSizeCompleted(childSizeDefault);
                }
            }
        });
    }

    private int getMarginLeftSizeCompareToRootSize(float factor) {
        return (int) (getWidth() * factor);
    }

    /**
     * If the profile user size is more than 3, we will display two user profile and one more text view.
     * Ex: If the user size is 4, will display two user views and one more text view with value "+2".
     */
    private void assignMoreProfileText(int profileSize,
                                       int marginLeft,
                                       int participantSize) {
        mBinding.textViewMore.setLayoutParams(getProfileLayoutParam(profileSize,
                marginLeft));
        int textSize = profileSize - getResources().getDimensionPixelSize(R.dimen.space_too_small);
        textSize = (textSize * 45) / 100;
        mBinding.textViewMore.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        mBinding.textViewMore.setText(String.format("+%s", (participantSize - 2)));
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    private interface InternalCallback {
        void onCalculateChildSizeCompleted(int defaultChildSize);
    }

    public interface ConversationProfileViewCallback {
        void onPresenceUpdate(String userId, Presence presence, Date lastOnlineTime);

        void onGotDefaultUserOnlineStatus(String userId, Presence presence, Date lastOnlineTime);
    }
}

