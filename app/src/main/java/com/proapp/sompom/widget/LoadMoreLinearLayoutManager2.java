package com.proapp.sompom.widget;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.databinding.LayoutLoadingBinding;

/**
 * Created by Chhom Veasna on 5/13/22.
 * This class will only support detection of load mor with @link{LayoutLoadingBinding} instance only
 */

public class LoadMoreLinearLayoutManager2 extends LinearLayoutManager {

    private static final String TAG = LoadMoreLinearLayoutManager2.class.getName();

    private boolean mIsLoadingMore;
    private boolean mShouldDetectLoadMore = true;
    private LoadMoreLinearLayoutManagerListener mOnLoadMoreListener;
    private boolean mSupportsPredictiveItemAnimations = true;

    public LoadMoreLinearLayoutManager2(Context context,
                                        RecyclerView recyclerView,
                                        boolean supportsPredictiveItemAnimations) {
        super(context, LinearLayoutManager.VERTICAL, false);
        mSupportsPredictiveItemAnimations = supportsPredictiveItemAnimations;
        Class<Object> objectClass = null;
        /*
        Manage to check if we have to force checking the load more possibility for sometimes the
        load more view display on screen but the logic in scrollVerticallyBy() does not work as expected.
         */
        if (recyclerView != null) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if (newState == RecyclerView.SCROLL_STATE_IDLE && !mIsLoadingMore) {
                        checkToPerformLoadMore();
                    }
                }
            });
        }
    }

    public LoadMoreLinearLayoutManager2(Context context, RecyclerView recyclerView) {
        super(context, LinearLayoutManager.VERTICAL, false);
        /*
        Manage to check if we have to force checking the load more possibility for sometimes the
        load more view display on screen but the logic in scrollVerticallyBy() does not work as expected.
         */
        if (recyclerView != null) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if (newState == RecyclerView.SCROLL_STATE_IDLE && !mIsLoadingMore) {
                        checkToPerformLoadMore();
                    }
                }
            });
        }
    }

    /**
     * Disable predictive animations. There is a bug in RecyclerView which causes views that
     * are being reloaded to pull invalid ViewHolders from the internal recycler stack if the
     * adapter size has decreased since the ViewHolder was recycled.
     */
    @Override
    public boolean supportsPredictiveItemAnimations() {
        return mSupportsPredictiveItemAnimations;
    }

    @Override
    public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler, RecyclerView.State state) {
        checkToPerformLoadMore();
        return super.scrollVerticallyBy(dy, recycler, state);
    }

    private void checkToPerformLoadMore() {
        if (mShouldDetectLoadMore) {
//            Timber.i("scrollVerticallyBy: " + mIsLoadingMore);
            if (mOnLoadMoreListener != null) {
                if (!mIsLoadingMore) {
                    if (isReachedLastItem()) {
                        mIsLoadingMore = true;
                        mOnLoadMoreListener.onReachedLoadMoreBottom();
                    }
                }
            }
        }
    }

    public void checkLToInvokeReachLoadMoreByDefaultIfNecessary() {
        if (mShouldDetectLoadMore) {
//            Log.i(TAG, "checkLToInvokeReachLoadMoreByDefaultIfNecessary: mIsLoadingMore ==> " + mIsLoadingMore);
            if (isReachedLastItem() && !mIsLoadingMore) {
                Log.i(TAG, "onReachedLoadMoreByDefault");
                mIsLoadingMore = true;
                if (mOnLoadMoreListener != null) {
                    Log.i(TAG, "mOnLoadMoreListener d" + mOnLoadMoreListener);
                    mOnLoadMoreListener.onReachedLoadMoreByDefault();
                }
            }
        }
    }

    private boolean isReachedLastItem() {
        int lastVisibleItemPosition = findLastVisibleItemPosition();
        ViewDataBinding binding = DataBindingUtil.getBinding(findViewByPosition(lastVisibleItemPosition));
        return binding instanceof LayoutLoadingBinding;
    }

    public void setLoadMoreLinearLayoutManagerListener(final LoadMoreLinearLayoutManagerListener onLoadMoreListener) {
        mOnLoadMoreListener = onLoadMoreListener;
    }

    public void loadingFinished() {
        mIsLoadingMore = false;
    }

    public void setShouldDetectLoadMore(boolean shouldDetectLoadMore) {
        mShouldDetectLoadMore = shouldDetectLoadMore;
    }

    public interface LoadMoreLinearLayoutManagerListener {
        //When user scroll bottom to reach load more layout
        void onReachedLoadMoreBottom();

        //When load more layout appear by layout change which happen when item has been removed
        void onReachedLoadMoreByDefault();
    }
}
