package com.proapp.sompom.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.LayoutOverlapImageContainerBinding;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.AttributeConverter;

import java.util.List;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 5/5/20.
 */
public class GroupDetailProfileView extends OverlapImageContainer {

    private static final int MAX_DISPLAY_VIEW_COUNT = 5;

    private LayoutOverlapImageContainerBinding mBinding;

    public GroupDetailProfileView(Context context) {
        super(context);
        mBinding = getBinding();
    }

    public GroupDetailProfileView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mBinding = getBinding();
    }

    public GroupDetailProfileView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mBinding = getBinding();
    }

    public GroupDetailProfileView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mBinding = getBinding();
    }

    /**
     * Will display with the more display text like the {@link ConversationProfileView}
     * Ex: Limit display is 5 views and the list user size is 7. So will display 4 user avatars and one
     * more user view view with value "+3".
     */
    public void addGroupItem(Context context,
                             String conversationId,
                             List<User> list,
                             String groupImageUrl,
                             String groupName) {
        mViewModel.setImageBorder(ContextCompat.getDrawable(context, R.drawable.group_profile_border));
        LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams((int) getResources().getDimension(R.dimen.chat_multi_image_group_size),
                (int) getResources().getDimension(R.dimen.chat_multi_image_group_size));
        RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen.chat_multi_image_group_size),
                (int) getResources().getDimension(R.dimen.chat_multi_image_group_size));

        LinearLayout.LayoutParams containerParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        containerParam.setMarginStart((int) getResources().getDimension(R.dimen.overlap_image_margin_group_detail));

        if (!TextUtils.isEmpty(groupImageUrl)) {
            setVisibility(VISIBLE);
            mBinding.moreTextView.setVisibility(GONE);
            mImageViews[0].setLayoutParams(linearParams);
            /*
                Since ImageProfileLayout data source is User object, we will construct a user information
                from group data.
             */
            User groupFromUser = new User();
            groupFromUser.setId(conversationId);
            groupFromUser.setFirstName(groupName);
            groupFromUser.setUserProfileThumbnail(groupImageUrl);
            mImageViews[0].setUser(groupFromUser, linearParams.width);
            mViewGroups[0].setVisibility(View.VISIBLE);
            containerParam.setMarginStart(0);
            mViewGroups[0].setLayoutParams(containerParam);

            for (int loop = 1; loop < MAX_DISPLAY_VIEW_COUNT; loop++) {
                mViewGroups[loop].setVisibility(GONE);
            }
        } else {
            if (list != null && !list.isEmpty()) {
                mBinding.moreTextView.setVisibility(GONE);
                Timber.i("AddItem with user list: " + list.size());
                setVisibility(VISIBLE);
                for (int i = 0; i < list.size(); i++) {
                    if (i != 0 && i < MAX_DISPLAY_VIEW_COUNT) {
                        mViewGroups[i].setLayoutParams(containerParam);
                    }
                    if (i > (MAX_DISPLAY_VIEW_COUNT - 1)) {
                        String more = "+" + (list.size() - (MAX_DISPLAY_VIEW_COUNT - 1));
                        Timber.i("More people" + more);
                        mBinding.moreTextView.setBackground(ContextCompat.getDrawable(context, R.drawable.more_participant_profile_background));
                        mBinding.moreTextView.setTextColor(AttributeConverter.convertAttrToColor(context, R.attr.group_more_text));
                        mBinding.moreTextView.setLayoutParams(relativeLayoutParams);
                        mBinding.moreTextView.setText(more);
                        mBinding.moreTextView.setVisibility(VISIBLE);
                        mBinding.moreTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                                getResources().getDimension(R.dimen.text_xlarge));
                        mImageViews[MAX_DISPLAY_VIEW_COUNT - 1].setVisibility(GONE);
                        break;
                    } else {
                        mImageViews[i].setLayoutParams((i == (MAX_DISPLAY_VIEW_COUNT - 1)) ? relativeLayoutParams : linearParams);
                        mImageViews[i].setUser(list.get(i), linearParams.width);
                        mViewGroups[i].setVisibility(VISIBLE);
                    }
                }

                if (list.size() < mImageViews.length) {
                    for (int i = list.size(); i < mImageViews.length; i++) {
                        mViewGroups[i].setVisibility(GONE);
                    }
                }
            } else {
                setVisibility(GONE);
            }
        }
    }
}
