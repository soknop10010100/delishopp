package com.proapp.sompom.widget.choosedistance;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Build;
import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.LayoutChooseDistanceBinding;
import com.resourcemanager.helper.FontHelper;
import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.emun.ChooseDistanceItem;

/**
 * Created by nuonveyo on 7/5/18.
 */

public class ChooseDistanceView extends LinearLayout {
    @ColorInt
    private int mSelectedColor;
    @ColorInt
    private int mUnSelectedColor;
    private ChooseDistanceItem mChooseDistanceItem;
    private OnItemClickListener<View> mViewOnItemClickListener;

    private LayoutChooseDistanceBinding mBinding;

    public ChooseDistanceView(Context context) {
        super(context);
        init();
    }

    public ChooseDistanceView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChooseDistanceView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ChooseDistanceView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void setViewOnItemClickListener(OnItemClickListener<View> viewOnItemClickListener) {
        mViewOnItemClickListener = viewOnItemClickListener;
    }

    public void setSelectedColor(int selectedColor) {
        mSelectedColor = selectedColor;
    }

    public void setUnSelectedColor(int unSelectedColor) {
        mUnSelectedColor = unSelectedColor;
    }

    public void setData(ChooseDistanceItem item) {
        mChooseDistanceItem = item;
        mBinding.title.setText(mChooseDistanceItem.getTitle());
        mBinding.imageIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), mChooseDistanceItem.getUnCheckIcon()));
    }

    public void setSelected(boolean isSelected) {
        if (isSelected) {
            Typeface typefaceBold =  FontHelper.getBoldFontStyle(getContext());
            mBinding.title.setTypeface(typefaceBold);
            mBinding.title.setTextColor(mSelectedColor);
            mBinding.imageIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), mChooseDistanceItem.getCheckIcon()));
            setImageBackground(R.drawable.oval_orange);
            mBinding.dropDownImageView.setVisibility(VISIBLE);
        } else {
            Typeface typeface =  FontHelper.getRegularFontStyle(getContext());
            mBinding.title.setTypeface(typeface);
            mBinding.title.setTextColor(mUnSelectedColor);
            mBinding.imageIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), mChooseDistanceItem.getUnCheckIcon()));
            setImageBackground(R.drawable.oval_grey_stroke);
            mBinding.dropDownImageView.setVisibility(INVISIBLE);
        }
    }

    private void init() {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_choose_distance,
                this,
                true);
        mBinding.dropDownImageView.setVisibility(INVISIBLE);
        mBinding.title.setTextColor(mUnSelectedColor);
        setImageBackground(R.drawable.oval_grey_stroke);
        setOnClickListener(v -> {
            if (mViewOnItemClickListener != null) {
                mViewOnItemClickListener.onClick(v);
            }
        });
    }

    private void setImageBackground(@DrawableRes int background) {
        mBinding.imageIcon.setBackground(ContextCompat.getDrawable(getContext(), background));
    }
}
