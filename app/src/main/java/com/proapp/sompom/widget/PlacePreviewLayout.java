package com.proapp.sompom.widget;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.LayoutPlacePreviewBinding;
import com.proapp.sompom.utils.BitmapConverter;
import com.proapp.sompom.viewmodel.binding.ImageViewBindingUtil;

import timber.log.Timber;

public class PlacePreviewLayout extends LinearLayout {

    private static final float ZOOM_LEVEL = 13f;

    private LayoutPlacePreviewBinding mBinding;
    private GoogleMap mGoogleMap;
    private PlacePreviewLayoutListener mListener;

    public PlacePreviewLayout(Context context) {
        super(context);
        init();
    }

    public PlacePreviewLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PlacePreviewLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public PlacePreviewLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    /*
        This method should be called once time in screen first created.`
     */
    public void onCreate(Bundle bundle, boolean showCloseButton) {
        Timber.i("onCreate: " + getMapTag());
        showCloseIcon(showCloseButton);
        mBinding.rootView.setBackground(showCloseButton ? ContextCompat.getDrawable(getContext(), R.drawable.post_background) : null);
        mBinding.mapView.onCreate(bundle);
        mBinding.mapView.getMapAsync(googleMap -> {
            Timber.i("onMapReady: " + mBinding.mapView.hashCode());
            MapsInitializer.initialize(getContext());
            mGoogleMap = googleMap;
            AppTheme theme = ThemeManager.getAppTheme(getContext());
            if (theme == AppTheme.Black) {
                googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.google_map_dark));
            } else if (theme == AppTheme.White) {
                googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.google_map_light));
            }
            googleMap.getUiSettings().setAllGesturesEnabled(false);
            googleMap.getUiSettings().setMapToolbarEnabled(false);

            if (mListener != null) {
                mListener.onMapReady(googleMap);
            }
        });
    }

    public boolean isPreviewable() {
        return mGoogleMap != null;
    }

    public void showCloseIcon(boolean showIcon) {
        mBinding.textViewClose.setVisibility(showIcon ? VISIBLE : GONE);
    }

    public void setListener(PlacePreviewLayoutListener listener) {
        mListener = listener;
    }

    private String getMapTag() {
        if (mBinding.mapView != null) {
            return String.valueOf(mBinding.mapView.hashCode());
        }

        return null;
    }

    public void onStart() {
//        if (mBinding.mapView != null) {
//            Timber.i("onStart: " + getMapTag());
//            mBinding.mapView.onStart();
//        }
    }

    public void onPause() {
//        if (mBinding.mapView != null) {
//            Timber.i("onPause: " + getMapTag());
//            mBinding.mapView.onPause();
//        }
    }

    public void onResume() {
//        if (mBinding.mapView != null) {
//            Timber.i("onResume: " + getMapTag());
//            mBinding.mapView.onResume();
//        }
    }

    public void onStop() {
//        if (mBinding.mapView != null) {
//            Timber.i("onStop: " + getMapTag());
//            mBinding.mapView.onStop();
//        }
    }

    public void onDestroy() {
//        if (mBinding.mapView != null) {
//            Timber.i("onDestroy: " + getMapTag());
//            try {
//                mBinding.mapView.onDestroy();
//            } catch (Exception ex) {
//                Timber.e(ex);
//            }
//        }
    }

    private void init() {
        setVisibility(GONE);
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_place_preview,
                this,
                true);
        mBinding.textViewClose.setOnClickListener(v -> {
            hideLayout();
            if (mListener != null) {
                mListener.onPreviewClosed();
            }
        });
        updateMapViewHeight();
    }

    private void updateMapViewHeight() {
        mBinding.mapView.getLayoutParams().height = (int) (Resources.getSystem().getDisplayMetrics().widthPixels * 0.5f);
    }

    public void clearMap() {
        // Clear the map and free up resources by changing the map type to none.
        // Also reset the map when it gets reattached to layout, so the previous map would
        // not be displayed.
        if (mGoogleMap != null) {
            Timber.i("Clear map: " + getMapTag());
            mGoogleMap.clear();
            mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
        }
    }

    public void resetPreviousMarker() {
        if (mGoogleMap != null) {
            mGoogleMap.clear();
        }
    }

    public void bindPlace(PlacePreviewData data) {
        Timber.i("bindPlace: " + new Gson().toJson(data));
        if (!isPreviewable()) {
            throw new AndroidRuntimeException("Maps was initialized incorrectly.");
        }
        mBinding.textViewTitle.setText(data.getPaceTitle());
        mBinding.textViewDescription.setText(data.getPlaceDescription());
        if (!TextUtils.isEmpty(data.getLogo())) {
            mBinding.imageViewLogo.setVisibility(VISIBLE);
            ImageViewBindingUtil.loadImageUrl(mBinding.imageViewLogo,
                    data.getLogo(),
                    ContextCompat.getDrawable(getContext(), R.drawable.ic_photo_placeholder_200dp),
                    false);
        } else {
            mBinding.imageViewLogo.setVisibility(GONE);
        }
        loadPreviewPlace(data);
    }

    private void loadPreviewPlace(PlacePreviewData data) {
        Timber.i("loadPreviewPlace: " + getMapTag());
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLng latLng = new LatLng(data.getLatitude(), data.getLongitude());
        mGoogleMap.setOnCameraMoveStartedListener(i -> {
            Timber.i("onCameraMoveStarted: " + getMapTag());
            showLoading(true);
        });
        mGoogleMap.setOnCameraIdleListener(() -> {
            Timber.i("onCameraIdle: " + getMapTag());
            showLoading(false);
        });
        mGoogleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                Timber.i("onCameraMove: " + getMapTag());
            }
        });
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_LEVEL));
        MarkerOptions options = new MarkerOptions();
        options.icon(BitmapConverter.bitmapDescriptorFromVector(getContext(), R.drawable.ic_pin_filled_icon));
        options.position(latLng);
        mGoogleMap.addMarker(options);
    }

    public void showLoading(boolean isShow) {
//        mBinding.loading.setVisibility(isShow ? VISIBLE : GONE);
    }

    public void showLayout() {
        setVisibility(VISIBLE);
    }

    public void hideLayout() {
        showLoading(false);
        setVisibility(GONE);
    }

    public static class PlacePreviewData {

        private double mLatitude;
        private double mLongitude;
        private String mPaceTitle;
        private String mPlaceDescription;
        private String mLogo;

        public double getLatitude() {
            return mLatitude;
        }

        public void setLatitude(double latitude) {
            mLatitude = latitude;
        }

        public double getLongitude() {
            return mLongitude;
        }

        public void setLongitude(double longitude) {
            mLongitude = longitude;
        }

        public String getPaceTitle() {
            return mPaceTitle;
        }

        public void setPaceTitle(String paceTitle) {
            mPaceTitle = paceTitle;
        }

        public String getPlaceDescription() {
            return mPlaceDescription;
        }

        public void setPlaceDescription(String placeDescription) {
            mPlaceDescription = placeDescription;
        }

        public String getLogo() {
            return mLogo;
        }

        public void setLogo(String logo) {
            mLogo = logo;
        }
    }

    public interface PlacePreviewLayoutListener {
        void onMapReady(GoogleMap map);

        void onPreviewClosed();
    }
}
