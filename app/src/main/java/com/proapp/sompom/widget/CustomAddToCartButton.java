
package com.proapp.sompom.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import com.proapp.sompom.R.styleable;

/**
 * Created by Or Vitovongsak on 3/9/21.
 */


/**
 * A fork of mcxtzhang's AnimShopButton
 * <p>
 * Link: https://github.com/mcxtzhang/AnimShopButton
 */
public class CustomAddToCartButton extends View {
    protected static final String TAG = CustomAddToCartButton.class.getName();
    protected static final int DEFAULT_DURATION = 150;
    protected int mLeft;
    protected int mTop;
    protected int mWidth;
    protected int mHeight;
    protected Region mAddRegion;
    protected Region mDelRegion;
    protected Path mAddPath;
    protected Path mDelPath;
    protected Paint mAddPaint;
    protected boolean isAddFillMode;
    protected int mAddEnableBgColor;
    protected int mAddEnableFgColor;
    protected int mAddDisableBgColor;
    protected int mAddDisableFgColor;
    protected Paint mDelPaint;
    protected boolean isDelFillMode;
    protected int mDelEnableBgColor;
    protected int mDelEnableFgColor;
    protected int mDelDisableBgColor;
    protected int mDelDisableFgColor;
    protected int mMaxCount;
    protected int mItemCount;
    protected float mRadius;
    protected float mCircleWidth;
    protected float mLineWidth;
    protected float mGapBetweenCircle;
    protected float mTextSize;
    protected Paint mTextPaint;
    protected Paint.FontMetrics mFontMetrics;
    protected ValueAnimator mAnimAdd;
    protected ValueAnimator mAniDel;
    protected float mAnimFraction;
    protected ValueAnimator mAnimExpandHint;
    protected ValueAnimator mAnimReduceHint;
    protected int mPerAnimDuration;
    protected boolean ignoreHintArea;
    protected boolean isHintMode;
    protected float mAnimExpandHintFraction;
    protected boolean isShowHintText;
    protected Paint mHintPaint;
    protected int mHintBgColor;
    protected int mHingTextSize;
    protected String mHintText;
    protected int mHintFgColor;
    protected int mHintBgRoundValue;
    protected boolean isReplenish;
    protected Paint mReplenishPaint;
    protected int mReplenishTextColor;
    protected int mReplenishTextSize;
    protected String mReplenishText;
    protected OnAddDeleteListener mOnAddDelListener;
    // TODO: Consider creating "mode" enum to represent different button customization instead of
    //  user manually setting all variable

    // New implementation for concierge module
    protected int mAddButtonDrawable;
    protected int mDeleteButtonDrawable;
    protected int mAddButtonDrawableColor;
    protected int mAddButtonBackgroundColor;
    protected int mDeleteButtonDrawableColor;
    protected int mDeleteButtonBackgroundColor;
    protected int mItemCountColor;
    protected int mMinCount;

    public CustomAddToCartButton(Context context) {
        this(context, (AttributeSet) null);
    }

    public CustomAddToCartButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomAddToCartButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mPerAnimDuration = DEFAULT_DURATION;
        init(context, attrs, defStyleAttr);
    }

    public int getCount() {
        return mItemCount;
    }

    public CustomAddToCartButton setCount(int count) {
        mItemCount = count;
        cancelAllAnim();
        initAnimSettingsByCount();
        return this;
    }

    private void cancelAllAnim() {
        if (mAnimAdd != null && mAnimAdd.isRunning()) {
            mAnimAdd.cancel();
        }

        if (mAniDel != null && mAniDel.isRunning()) {
            mAniDel.cancel();
        }

        if (mAnimExpandHint != null && mAnimExpandHint.isRunning()) {
            mAnimExpandHint.cancel();
        }

        if (mAnimReduceHint != null && mAnimReduceHint.isRunning()) {
            mAnimReduceHint.cancel();
        }

    }

    public OnAddDeleteListener getOnAddDelListener() {
        return mOnAddDelListener;
    }

    public int getMaxCount() {
        return mMaxCount;
    }

    public CustomAddToCartButton setMaxCount(int maxCount) {
        mMaxCount = maxCount;
        return this;
    }

    public boolean isReplenish() {
        return isReplenish;
    }

    public CustomAddToCartButton setReplenish(boolean replenish) {
        isReplenish = replenish;
        if (isReplenish && null == mReplenishPaint) {
            mReplenishPaint = new Paint(1);
            mReplenishPaint.setTextSize((float) mReplenishTextSize);
            mReplenishPaint.setColor(mReplenishTextColor);
        }

        return this;
    }

    public int getReplenishTextColor() {
        return mReplenishTextColor;
    }

    public CustomAddToCartButton setReplenishTextColor(int replenishTextColor) {
        mReplenishTextColor = replenishTextColor;
        return this;
    }

    public int getReplenishTextSize() {
        return mReplenishTextSize;
    }

    public CustomAddToCartButton setReplenishTextSize(int replenishTextSize) {
        mReplenishTextSize = replenishTextSize;
        return this;
    }

    public String getReplenishText() {
        return mReplenishText;
    }

    public CustomAddToCartButton setReplenishText(String replenishText) {
        mReplenishText = replenishText;
        return this;
    }

    public boolean isAddFillMode() {
        return isAddFillMode;
    }

    public CustomAddToCartButton setAddFillMode(boolean addFillMode) {
        isAddFillMode = addFillMode;
        return this;
    }

    public int getAddEnableBgColor() {
        return mAddEnableBgColor;
    }

    public CustomAddToCartButton setAddEnableBgColor(int addEnableBgColor) {
        mAddEnableBgColor = addEnableBgColor;
        return this;
    }

    public int getAddEnableFgColor() {
        return mAddEnableFgColor;
    }

    public CustomAddToCartButton setAddEnableFgColor(int addEnableFgColor) {
        mAddEnableFgColor = addEnableFgColor;
        return this;
    }

    public int getAddDisableBgColor() {
        return mAddDisableBgColor;
    }

    public CustomAddToCartButton setAddDisableBgColor(int addDisableBgColor) {
        mAddDisableBgColor = addDisableBgColor;
        return this;
    }

    public int getAddDisableFgColor() {
        return mAddDisableFgColor;
    }

    public CustomAddToCartButton setAddDisableFgColor(int addDisableFgColor) {
        mAddDisableFgColor = addDisableFgColor;
        return this;
    }

    public boolean isDelFillMode() {
        return isDelFillMode;
    }

    public CustomAddToCartButton setDelFillMode(boolean delFillMode) {
        isDelFillMode = delFillMode;
        return this;
    }

    public int getDelEnableBgColor() {
        return mDelEnableBgColor;
    }

    public CustomAddToCartButton setDelEnableBgColor(int delEnableBgColor) {
        mDelEnableBgColor = delEnableBgColor;
        return this;
    }

    public int getDelEnableFgColor() {
        return mDelEnableFgColor;
    }

    public CustomAddToCartButton setDelEnableFgColor(int delEnableFgColor) {
        mDelEnableFgColor = delEnableFgColor;
        return this;
    }

    public int getDelDisableBgColor() {
        return mDelDisableBgColor;
    }

    public CustomAddToCartButton setDelDisableBgColor(int delDisableBgColor) {
        mDelDisableBgColor = delDisableBgColor;
        return this;
    }

    public int getDelDisableFgColor() {
        return mDelDisableFgColor;
    }

    public CustomAddToCartButton setDelDisableFgColor(int delDisableFgColor) {
        mDelDisableFgColor = delDisableFgColor;
        return this;
    }

    public float getRadius() {
        return mRadius;
    }

    public CustomAddToCartButton setRadius(float radius) {
        mRadius = radius;
        return this;
    }

    public float getCircleWidth() {
        return mCircleWidth;
    }

    public CustomAddToCartButton setCircleWidth(float circleWidth) {
        mCircleWidth = circleWidth;
        return this;
    }

    public float getLineWidth() {
        return mLineWidth;
    }

    public CustomAddToCartButton setLineWidth(float lineWidth) {
        mLineWidth = lineWidth;
        return this;
    }

    public float getTextSize() {
        return mTextSize;
    }

    public CustomAddToCartButton setTextSize(float textSize) {
        mTextSize = textSize;
        return this;
    }

    public float getGapBetweenCircle() {
        return mGapBetweenCircle;
    }

    public CustomAddToCartButton setGapBetweenCircle(float gapBetweenCircle) {
        mGapBetweenCircle = gapBetweenCircle;
        return this;
    }

    public int getPerAnimDuration() {
        return mPerAnimDuration;
    }

    public CustomAddToCartButton setPerAnimDuration(int perAnimDuration) {
        mPerAnimDuration = perAnimDuration;
        return this;
    }

    public boolean isIgnoreHintArea() {
        return ignoreHintArea;
    }

    public CustomAddToCartButton setIgnoreHintArea(boolean ignoreHintArea) {
        ignoreHintArea = ignoreHintArea;
        return this;
    }

    public int getHintBgColor() {
        return mHintBgColor;
    }

    public CustomAddToCartButton setHintBgColor(int hintBgColor) {
        mHintBgColor = hintBgColor;
        return this;
    }

    public int getHingTextSize() {
        return mHingTextSize;
    }

    public CustomAddToCartButton setHingTextSize(int hingTextSize) {
        mHingTextSize = hingTextSize;
        return this;
    }

    public String getHintText() {
        return mHintText;
    }

    public CustomAddToCartButton setHintText(String hintText) {
        mHintText = hintText;
        return this;
    }

    public int getHintFgColor() {
        return mHintFgColor;
    }

    public CustomAddToCartButton setHintFgColor(int hintFgColor) {
        mHintFgColor = hintFgColor;
        return this;
    }

    public int getHintBgRoundValue() {
        return mHintBgRoundValue;
    }

    public CustomAddToCartButton setHintBgRoundValue(int hintBgRoundValue) {
        mHintBgRoundValue = hintBgRoundValue;
        return this;
    }

    public CustomAddToCartButton setOnAddDelListener(OnAddDeleteListener onAddDeleteListener) {
        mOnAddDelListener = onAddDeleteListener;
        return this;
    }

    protected void init(Context context, AttributeSet attrs, int defStyleAttr) {
        initDefaultValue(context);
        TypedArray ta = context.obtainStyledAttributes(attrs, styleable.CustomAddToCartButton, defStyleAttr, 0);
        int indexCount = ta.getIndexCount();

        for (int i = 0; i < indexCount; ++i) {
            int index = ta.getIndex(i);
            if (index == styleable.CustomAddToCartButton_gapBetweenCircle) {
                mGapBetweenCircle = ta.getDimension(index, mGapBetweenCircle);
            } else if (index == styleable.CustomAddToCartButton_isAddFillMode) {
                isAddFillMode = ta.getBoolean(index, isAddFillMode);
            } else if (index == styleable.CustomAddToCartButton_addEnableBgColor) {
                mAddEnableBgColor = ta.getColor(index, mAddEnableBgColor);
            } else if (index == styleable.CustomAddToCartButton_addEnableFgColor) {
                mAddEnableFgColor = ta.getColor(index, mAddEnableFgColor);
            } else if (index == styleable.CustomAddToCartButton_addDisableBgColor) {
                mAddDisableBgColor = ta.getColor(index, mAddDisableBgColor);
            } else if (index == styleable.CustomAddToCartButton_addDisableFgColor) {
                mAddDisableFgColor = ta.getColor(index, mAddDisableFgColor);
            } else if (index == styleable.CustomAddToCartButton_isDelFillMode) {
                isDelFillMode = ta.getBoolean(index, isDelFillMode);
            } else if (index == styleable.CustomAddToCartButton_delEnableBgColor) {
                mDelEnableBgColor = ta.getColor(index, mDelEnableBgColor);
            } else if (index == styleable.CustomAddToCartButton_delEnableFgColor) {
                mDelEnableFgColor = ta.getColor(index, mDelEnableFgColor);
            } else if (index == styleable.CustomAddToCartButton_delDisableBgColor) {
                mDelDisableBgColor = ta.getColor(index, mDelDisableBgColor);
            } else if (index == styleable.CustomAddToCartButton_delDisableFgColor) {
                mDelDisableFgColor = ta.getColor(index, mDelDisableFgColor);
            } else if (index == styleable.CustomAddToCartButton_maxCount) {
                mMaxCount = ta.getInteger(index, mMaxCount);
            } else if (index == styleable.CustomAddToCartButton_itemCount) {
                mItemCount = ta.getInteger(index, mItemCount);
            } else if (index == styleable.CustomAddToCartButton_radius) {
                mRadius = ta.getDimension(index, mRadius);
            } else if (index == styleable.CustomAddToCartButton_circleStrokeWidth) {
                mCircleWidth = ta.getDimension(index, mCircleWidth);
            } else if (index == styleable.CustomAddToCartButton_lineWidth) {
                mLineWidth = ta.getDimension(index, mLineWidth);
            } else if (index == styleable.CustomAddToCartButton_numTextSize) {
                mTextSize = ta.getDimension(index, mTextSize);
            } else if (index == styleable.CustomAddToCartButton_hintText) {
                mHintText = ta.getString(index);
            } else if (index == styleable.CustomAddToCartButton_hintBgColor) {
                mHintBgColor = ta.getColor(index, mHintBgColor);
            } else if (index == styleable.CustomAddToCartButton_hintFgColor) {
                mHintFgColor = ta.getColor(index, mHintFgColor);
            } else if (index == styleable.CustomAddToCartButton_hingTextSize) {
                mHingTextSize = ta.getDimensionPixelSize(index, mHingTextSize);
            } else if (index == styleable.CustomAddToCartButton_hintBgRoundValue) {
                mHintBgRoundValue = ta.getDimensionPixelSize(index, mHintBgRoundValue);
            } else if (index == styleable.CustomAddToCartButton_ignoreHintArea) {
                ignoreHintArea = ta.getBoolean(index, false);
            } else if (index == styleable.CustomAddToCartButton_perAnimDuration) {
                mPerAnimDuration = ta.getInteger(index, DEFAULT_DURATION);
            } else if (index == styleable.CustomAddToCartButton_replenishText) {
                mReplenishText = ta.getString(index);
            } else if (index == styleable.CustomAddToCartButton_replenishTextColor) {
                mReplenishTextColor = ta.getColor(index, mReplenishTextColor);
            } else if (index == styleable.CustomAddToCartButton_replenishTextSize) {
                mReplenishTextSize = ta.getDimensionPixelSize(index, mReplenishTextSize);
            } else if (index == styleable.CustomAddToCartButton_addButtonDrawable) {
                mAddButtonDrawable = ta.getResourceId(index, 0);
            } else if (index == styleable.CustomAddToCartButton_addButtonDrawableColor) {
                mAddButtonDrawableColor = ta.getColor(index, mAddButtonDrawableColor);
            } else if (index == styleable.CustomAddToCartButton_addButtonBackgroundColor) {
                mAddButtonBackgroundColor = ta.getColor(index, mAddButtonBackgroundColor);
            } else if (index == styleable.CustomAddToCartButton_deleteButtonDrawable) {
                mDeleteButtonDrawable = ta.getResourceId(index, 0);
            } else if (index == styleable.CustomAddToCartButton_deleteButtonDrawableColor) {
                mDeleteButtonDrawableColor = ta.getColor(index, mDeleteButtonDrawableColor);
            } else if (index == styleable.CustomAddToCartButton_deleteButtonBackgroundColor) {
                mDeleteButtonBackgroundColor = ta.getColor(index, mDeleteButtonBackgroundColor);
            } else if (index == styleable.CustomAddToCartButton_itemCountColor) {
                mItemCountColor = ta.getColor(index, mItemCountColor);
            } else if (index == styleable.CustomAddToCartButton_minCount) {
                mMinCount = ta.getInteger(index, 0);
            }
        }

        ta.recycle();
        mAddRegion = new Region();
        mDelRegion = new Region();
        mAddPath = new Path();
        mDelPath = new Path();
        mAddPaint = new Paint(1);
        if (isAddFillMode) {
            mAddPaint.setStyle(Paint.Style.FILL);
        } else {
            mAddPaint.setStyle(Paint.Style.STROKE);
        }

        mDelPaint = new Paint(1);
        if (isDelFillMode) {
            mDelPaint.setStyle(Paint.Style.FILL);
        } else {
            mDelPaint.setStyle(Paint.Style.STROKE);
        }

        mHintPaint = new Paint(1);
        mHintPaint.setStyle(Paint.Style.FILL);
        mHintPaint.setTextSize((float) mHingTextSize);
        mTextPaint = new Paint(1);
        mTextPaint.setTextSize(mTextSize);
        mFontMetrics = mTextPaint.getFontMetrics();
        mAnimAdd = ValueAnimator.ofFloat(1.0F, 0.0F);
        mAnimAdd.addUpdateListener(animation -> {
            mAnimFraction = (Float) animation.getAnimatedValue();
            invalidate();
        });
        mAnimAdd.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
            }
        });
        mAnimAdd.setDuration((long) mPerAnimDuration);
        mAnimReduceHint = ValueAnimator.ofFloat(0.0F, 1.0F);
        mAnimReduceHint.addUpdateListener(animation -> {
            mAnimExpandHintFraction = (Float) animation.getAnimatedValue();
            invalidate();
        });
        mAnimReduceHint.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                if (mItemCount >= 1) {
                    isHintMode = false;
                }

                if (mItemCount >= 1) {
                    Log.d(CustomAddToCartButton.TAG, "现在还是》=1 开始收缩动画");
                    if (mAnimAdd != null && !mAnimAdd.isRunning()) {
                        mAnimAdd.start();
                    }
                }
            }

            public void onAnimationStart(Animator animation) {
                if (mItemCount == 1) {
                    isShowHintText = false;
                }

            }
        });
        mAnimReduceHint.setDuration(ignoreHintArea ? 0L : (long) mPerAnimDuration);
        mAniDel = ValueAnimator.ofFloat(0.0F, 1.0F);
        mAniDel.addUpdateListener(animation -> {
            mAnimFraction = (Float) animation.getAnimatedValue();
            invalidate();
        });
        mAniDel.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                if (mItemCount == 0) {
                    Log.d(CustomAddToCartButton.TAG, "现在还是0onAnimationEnd() called with: animation = [" + animation + "]");
                    if (mAnimExpandHint != null && !mAnimExpandHint.isRunning()) {
                        mAnimExpandHint.start();
                    }
                }

            }
        });
        mAniDel.setDuration((long) mPerAnimDuration);
        mAnimExpandHint = ValueAnimator.ofFloat(1.0F, 0.0F);
        mAnimExpandHint.addUpdateListener(animation -> {
            mAnimExpandHintFraction = (Float) animation.getAnimatedValue();
            invalidate();
        });
        mAnimExpandHint.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                if (mItemCount == 0) {
                    isShowHintText = true;
                }
            }

            public void onAnimationStart(Animator animation) {
                if (mItemCount == 0) {
                    isHintMode = true;
                }

            }
        });
        mAnimExpandHint.setDuration(ignoreHintArea ? 0L : (long) mPerAnimDuration);
    }

    private void initDefaultValue(Context context) {
        mGapBetweenCircle = TypedValue.applyDimension(1, 15.0F, context.getResources().getDisplayMetrics());
        isAddFillMode = true;
        mAddEnableBgColor = -9125;
        mAddEnableFgColor = -16777216;
        mAddDisableBgColor = -6842473;
        mAddDisableFgColor = -16777216;
        isDelFillMode = true;
        mDelEnableBgColor = -6842473;
        mDelEnableFgColor = -6842473;
        mDelDisableBgColor = -6842473;
        mDelDisableFgColor = -6842473;
        mRadius = TypedValue.applyDimension(1, 12.5F, getResources().getDisplayMetrics());
        mCircleWidth = TypedValue.applyDimension(1, 1.0F, getResources().getDisplayMetrics());
        mLineWidth = TypedValue.applyDimension(1, 2.0F, getResources().getDisplayMetrics());
        mTextSize = TypedValue.applyDimension(2, 14.5F, getResources().getDisplayMetrics());
        mHintText = "HintText";
        mHintBgColor = mAddEnableBgColor;
        mHintFgColor = mAddEnableFgColor;
        mHingTextSize = (int) TypedValue.applyDimension(2, 12.0F, context.getResources().getDisplayMetrics());
        mHintBgRoundValue = (int) TypedValue.applyDimension(1, 5.0F, context.getResources().getDisplayMetrics());
        mReplenishText = "ReplenishText";
        mReplenishTextColor = -840389;
        mReplenishTextSize = mHingTextSize;
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int wMode = View.MeasureSpec.getMode(widthMeasureSpec);
        int wSize = View.MeasureSpec.getSize(widthMeasureSpec);
        int hMode = View.MeasureSpec.getMode(heightMeasureSpec);
        int hSize = View.MeasureSpec.getSize(heightMeasureSpec);
        int computeSize;
        switch (wMode) {
            case -2147483648:
                computeSize = (int) ((float) getPaddingLeft() + mRadius * 2.0F + mGapBetweenCircle + mRadius * 2.0F + (float) getPaddingRight() + mCircleWidth * 2.0F);
                wSize = Math.min(computeSize, wSize);
                break;
            case 0:
                computeSize = (int) ((float) getPaddingLeft() + mRadius * 2.0F + mGapBetweenCircle + mRadius * 2.0F + (float) getPaddingRight() + mCircleWidth * 2.0F);
                wSize = computeSize;
            case 1073741824:
        }

        switch (hMode) {
            case -2147483648:
                computeSize = (int) ((float) getPaddingTop() + mRadius * 2.0F + (float) getPaddingBottom() + mCircleWidth * 2.0F);
                hSize = Math.min(computeSize, hSize);
                break;
            case 0:
                computeSize = (int) ((float) getPaddingTop() + mRadius * 2.0F + (float) getPaddingBottom() + mCircleWidth * 2.0F);
                hSize = computeSize;
            case 1073741824:
        }

        setMeasuredDimension(wSize, hSize);
        cancelAllAnim();
        initAnimSettingsByCount();
    }

    private void initAnimSettingsByCount() {
        if (mItemCount == 0) {
            mAnimFraction = 1.0F;
        } else {
            mAnimFraction = 0.0F;
        }

        if (mItemCount == 0) {
            isHintMode = true;
            isShowHintText = true;
            mAnimExpandHintFraction = 0.0F;
        } else {
            isHintMode = false;
            isShowHintText = false;
            mAnimExpandHintFraction = 1.0F;
        }

    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mLeft = (int) ((float) getPaddingLeft() + mCircleWidth);
        mTop = (int) ((float) getPaddingTop() + mCircleWidth);
        mWidth = w;
        mHeight = h;
    }

    protected void onDraw(Canvas canvas) {
        int baseX;
        if (isReplenish) {
            baseX = (int) ((float) (mWidth / 2) - mReplenishPaint.measureText(mReplenishText) / 2.0F);
            baseX = (int) ((float) (mHeight / 2) - (mReplenishPaint.descent() + mReplenishPaint.ascent()) / 2.0F);
            canvas.drawText(mReplenishText, (float) baseX, (float) baseX, mReplenishPaint);
        } else {
            if (!ignoreHintArea && isHintMode) {
                Drawable addButton = getResources().getDrawable(mAddButtonDrawable);
                if (addButton != null) {
                    mAddPaint.setColor(mAddButtonBackgroundColor);
                    mAddPaint.setStrokeWidth(mCircleWidth);
                    float left = mLeft + mRadius * 2.0F + mGapBetweenCircle;
                    mAddPath.addCircle(left + mRadius, (float) mTop + mRadius, mRadius, Path.Direction.CW);
                    mAddRegion.setPath(mAddPath, new Region(mLeft, mTop, mWidth - getPaddingRight(), mHeight - getPaddingBottom()));
                    canvas.drawPath(mAddPath, mAddPaint);

//                    Rect addButtonRect = new Rect(
//                            (int) left,
//                            mTop - getPaddingTop(),
//                            (int) (left + mRadius * 2),
//                            (int) (mTop + mRadius + mRadius));
//
//                    mAddPaint.setColor(mDeleteButtonDrawableColor);
//                    canvas.drawRect(addButtonRect, mAddPaint);

                    addButton.mutate();
                    addButton.setColorFilter(new PorterDuffColorFilter(mAddButtonDrawableColor, PorterDuff.Mode.SRC_ATOP));
                    addButton.setBounds(
                            (int) left,
                            mTop - getPaddingTop(),
                            (int) (left + mRadius * 2),
                            (int) (mTop + mRadius + mRadius));
                    addButton.draw(canvas);
                }
            } else {
                float animOffsetMax = mRadius * 2.0F + mGapBetweenCircle;
                int animAlphaMax = 255;
                int animRotateMax = 360;

                /**
                 *  Previously manually draw delete button
                 */
//                if (mCount > 0) {
//                    mDelPaint.setColor(mDelEnableBgColor);
//                } else {
//                    mDelPaint.setColor(mDelDisableBgColor);
//                }
//                mDelPaint.setAlpha((int) ((float) animAlphaMax * (1.0F - mAnimFraction)));
//                mDelPaint.setStrokeWidth(mCircleWidth);
//                mDelPath.reset();
//                mDelPath.addCircle(animOffsetMax * mAnimFraction + (float) mLeft + mRadius, (float) mTop + mRadius, mRadius, Path.Direction.CW);
//                mDelRegion.setPath(mDelPath, new Region(mLeft, mTop, mWidth - getPaddingRight(), mHeight - getPaddingBottom()));
//                canvas.drawPath(mDelPath, mDelPaint);
//                if (mCount > 0) {
//                    mDelPaint.setColor(mDelEnableFgColor);
//                } else {
//                    mDelPaint.setColor(mDelDisableFgColor);
//                }
//                mDelPaint.setStrokeWidth(mLineWidth);
//                canvas.save();
//                canvas.translate(animOffsetMax * mAnimFraction + (float) mLeft + mRadius, (float) mTop + mRadius);
//                canvas.rotate((float) ((int) ((float) animRotateMax * (1.0F - mAnimFraction))));
//                canvas.drawLine(-mRadius / 2.0F, 0.0F, mRadius / 2.0F, 0.0F, mDelPaint);
//                canvas.restore();
//                canvas.save();

                /**
                 * Now draw a supplied resource drawable instead
                 */
                Drawable deleteButton = getResources().getDrawable(mDeleteButtonDrawable);
                if (deleteButton != null) {
                    // TODO: Consider replacing drawing the background manually with a drawable?

                    // Draw the delete button background
                    mDelPaint.setAlpha((int) ((float) animAlphaMax * (1.0F - mAnimFraction)));
                    mDelPaint.setColor(mDeleteButtonBackgroundColor);
                    mDelPaint.setStrokeWidth(mCircleWidth);
                    mDelPath.reset();
                    mDelPath.addCircle(animOffsetMax * mAnimFraction + (float) mLeft + mRadius, (float) mTop + mRadius, mRadius, Path.Direction.CW);
                    mDelRegion.setPath(mDelPath, new Region(mLeft, mTop, mWidth - getPaddingRight(), mHeight - getPaddingBottom()));
                    canvas.drawPath(mDelPath, mDelPaint);

                    // Draws a solid background between the delete button and the add button to
                    // simulate a filled button, comment out this block to see the original animation
                    float left = animOffsetMax * mAnimFraction + (float) mLeft + mRadius;
                    float right = (animOffsetMax) + mLeft + mRadius;
                    RectF rect = new RectF(left,
                            mTop,
                            right,
                            mTop + mRadius * 2);
                    canvas.drawRect(rect, mDelPaint);

                    // Draws the delete button icon
                    canvas.save();
                    canvas.translate(
                            animOffsetMax * mAnimFraction + (float) mLeft + mRadius,
                            0);
                    deleteButton.mutate();
                    deleteButton.setColorFilter(new PorterDuffColorFilter(mDeleteButtonDrawableColor, PorterDuff.Mode.SRC_ATOP));
                    deleteButton.setBounds((int) -mRadius,
                            mTop,
                            (int) mRadius,
                            (int) (mTop + mRadius + mRadius));
                    deleteButton.draw(canvas);

                    canvas.restore();
                    canvas.save();
                }

                /**
                 *  Previously manually draw the add button, which is a circle with two line drawn
                 *  in the shape of a cross
                 */
//                if (mCount < mMaxCount) {
//                    mAddPaint.setColor(mAddEnableBgColor);
//                } else {
//                    mAddPaint.setColor(mAddDisableBgColor);
//                }
//                mAddPaint.setStrokeWidth(mCircleWidth);
//                float left = (float) mLeft + mRadius * 2.0F + mGapBetweenCircle;
//                mAddPath.reset();
//                mAddPath.addCircle(left + mRadius, (float) mTop + mRadius, mRadius, Path.Direction.CW);
//                mAddRegion.setPath(mAddPath, new Region(mLeft, mTop, mWidth - getPaddingRight(), mHeight - getPaddingBottom()));
//                canvas.drawPath(mAddPath, mAddPaint);
//                if (mCount < mMaxCount) {
//                    mAddPaint.setColor(mAddEnableFgColor);
//                } else {
//                    mAddPaint.setColor(mAddDisableFgColor);
//                }
//                mAddPaint.setStrokeWidth(mLineWidth);
//                canvas.drawLine(left + mRadius / 2.0F, (float) mTop + mRadius, left + mRadius / 2.0F + mRadius, (float) mTop + mRadius, mAddPaint);
//                canvas.drawLine(left + mRadius, (float) mTop + mRadius / 2.0F, left + mRadius, (float) mTop + mRadius / 2.0F + mRadius, mAddPaint);

                /**
                 * Now draw a supplied resource drawable instead
                 */
                Drawable addButton = getResources().getDrawable(mAddButtonDrawable);
                if (addButton != null) {
                    // TODO: Consider replacing drawing the background manually?
                    mAddPaint.setColor(mAddButtonBackgroundColor);
                    mAddPaint.setStrokeWidth(mCircleWidth);
                    float left = mLeft + mRadius * 2.0F + mGapBetweenCircle;
                    mAddPath.addCircle(left + mRadius, (float) mTop + mRadius, mRadius, Path.Direction.CW);
                    mAddRegion.setPath(mAddPath, new Region(mLeft, mTop, mWidth - getPaddingRight(), mHeight - getPaddingBottom()));
                    canvas.drawPath(mAddPath, mAddPaint);

                    addButton.mutate();
                    addButton.setColorFilter(new PorterDuffColorFilter(mAddButtonDrawableColor, PorterDuff.Mode.SRC_ATOP));
                    addButton.setBounds((int) left,
                            mTop,
                            (int) (left + mRadius * 2),
                            (int) (mTop + mRadius + mRadius));
                    addButton.draw(canvas);

                    canvas.restore();
                    canvas.save();
                }

                // Draw item count text
                canvas.translate(mAnimFraction * (mGapBetweenCircle / 2.0F - mTextPaint.measureText(mItemCount + "") / 2.0F + mRadius), 0.0F);
                // Disable rotation animation for item counter
                // canvas.rotate(360.0F * mAnimFraction, mGapBetweenCircle / 2.0F + (float) mLeft + mRadius * 2.0F, (float) mTop + mRadius);
                mTextPaint.setAlpha((int) (255.0F * (1.0F - mAnimFraction)));
                mTextPaint.setColor(mItemCountColor);
                canvas.drawText(mItemCount + "",
                        mGapBetweenCircle / 2.0F - mTextPaint.measureText(mItemCount + "") / 2.0F + (float) mLeft + mRadius * 2.0F,
                        (float) mTop + mRadius - (mFontMetrics.top + mFontMetrics.descent) / 2.0F,
                        mTextPaint);
                canvas.restore();
            }

        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case 0:
                if (!isReplenish) {
//                    if (isHintMode) {
//                        onAddClick();
//                        return true;
//                    } else
                    if (mAddRegion.contains((int) event.getX(), (int) event.getY())) {
                        onAddClick();
                        return true;
                    } else if (mDelRegion.contains((int) event.getX(), (int) event.getY())) {
                        onDelClick();
                        return true;
                    }
                }
            case 1:
            case 2:
            case 3:
            default:
                return super.onTouchEvent(event);
        }
    }

    protected void onDelClick() {
        if (mItemCount > mMinCount) {
            --mItemCount;
            onCountDelSuccess();
            if (null != mOnAddDelListener) {
                mOnAddDelListener.onDeleteSuccess(mItemCount);
            }
        } else if (null != mOnAddDelListener) {
            mOnAddDelListener.onDeleteFailed(mItemCount, OnAddDeleteListener.FailType.COUNT_MIN);
        }

    }

    protected void onAddClick() {
        if (mItemCount < mMaxCount) {
            onCountAddSuccess(mItemCount == 0);
            if (mItemCount == 0) {
                mItemCount++;
            }
            if (null != mOnAddDelListener) {
                mOnAddDelListener.onAddSuccess(mItemCount + 1);
            }
        } else if (null != mOnAddDelListener) {
            mOnAddDelListener.onAddFailed(mItemCount, OnAddDeleteListener.FailType.COUNT_MAX);
        }

    }

    public void onCountAddSuccess(boolean isFirstAdd) {
        if (mItemCount == 1 && isFirstAdd) {
            cancelAllAnim();
            mAnimReduceHint.start();
        } else {
            mAnimFraction = 0.0F;
            invalidate();
        }

    }

    public void onCountDelSuccess() {
        if (mItemCount == 0) {
            cancelAllAnim();
            mAniDel.start();
        } else {
            mAnimFraction = 0.0F;
            invalidate();
        }

    }

    public interface OnAddDeleteListener {
        void onAddSuccess(int newCount);

        void onAddFailed(int count, FailType failType);

        void onDeleteSuccess(int newCount);

        void onDeleteFailed(int count, FailType failType);

        enum FailType {
            COUNT_MAX,
            COUNT_MIN;

            FailType() {
            }
        }
    }
}
