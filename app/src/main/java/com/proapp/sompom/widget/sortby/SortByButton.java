package com.proapp.sompom.widget.sortby;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Build;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.LayoutFilterDetailSortByBinding;
import com.resourcemanager.helper.FontHelper;

/**
 * Created by nuonveyo on 7/5/18.
 */

public class SortByButton extends LinearLayout {
    private LayoutFilterDetailSortByBinding mBiding;

    public SortByButton(Context context) {
        super(context);
        init();
    }

    public SortByButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SortByButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SortByButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void setSelected(boolean isSelected) {
        if (isSelected) {
            mBiding.dropDownImageView.setVisibility(VISIBLE);
            mBiding.title.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            Typeface typeface = FontHelper.getBoldFontStyle(getContext());
            mBiding.title.setTypeface(typeface);
            mBiding.title.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.round_orange_stroke_background));
        } else {
            mBiding.dropDownImageView.setVisibility(INVISIBLE);
            mBiding.title.setTextColor(ContextCompat.getColor(getContext(), R.color.darkGrey));
            Typeface typeface = FontHelper.getRegularFontStyle(getContext());
            mBiding.title.setTypeface(typeface);
            mBiding.title.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.round_grey_stroke_background));
        }
    }

    public void setTitle(@StringRes int title) {
        mBiding.title.setText(title);
    }

    private void init() {
        mBiding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_filter_detail_sort_by,
                this,
                true);
    }
}
