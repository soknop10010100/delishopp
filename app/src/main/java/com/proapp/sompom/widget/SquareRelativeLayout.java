package com.proapp.sompom.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.proapp.sompom.R;

/**
 * Created by Chhom Veasna on 4/27/22.
 */
public class SquareRelativeLayout extends RelativeLayout {

    private float mWidthPercentage = 1f;
    private float mHeightPercentage = 1f;

    public SquareRelativeLayout(@NonNull Context context) {
        super(context);
    }

    public SquareRelativeLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SquareRelativeLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int viewWidth = MeasureSpec.getSize(widthMeasureSpec);
        int newViewWidth = (int) (viewWidth * mWidthPercentage);
        int newViewHeight = (int) (viewWidth * mHeightPercentage);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int newWidthMeasureSpec = MeasureSpec.makeMeasureSpec(newViewWidth, widthMode);
        int newHeightMeasureSpec = MeasureSpec.makeMeasureSpec(newViewHeight, widthMode);
        super.onMeasure(newWidthMeasureSpec, newHeightMeasureSpec);
    }

    private void init(AttributeSet attr) {
        if (attr != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attr,
                    R.styleable.SquareRelativeLayout,
                    0,
                    0);
            mWidthPercentage = typedArray.getFloat(R.styleable.SquareRelativeLayout_widthPercentageOfParentRelativeLayout, 1);
            mHeightPercentage = typedArray.getFloat(R.styleable.SquareRelativeLayout_heightPercentageOfParentRelativeLayout, 1);
            typedArray.recycle();
        }
    }
}
