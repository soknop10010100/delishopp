package com.proapp.sompom.widget;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by he.rotha on 3/7/16.
 */
public class LoaderMoreLayoutManager extends LinearLayoutManager {

    private boolean mIsLoadingMore = false;
    private OnLoadMoreCallback mOnLoadMoreListener;
    private LinearSmoothScroller mLinearSmoothScroller;

    public LoaderMoreLayoutManager(final Context context) {
        super(context);
    }

    public LoaderMoreLayoutManager(final Context context, final int orientation, final boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public LoaderMoreLayoutManager(final Context context, final AttributeSet attrs,
                                   final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public LoaderMoreLayoutManager(Context context, RecyclerView recyclerView) {
        super(context);
        /*
        Manage to check if we have to force checking the load more possibility for sometimes the
        load more view display on screen but the logic in scrollVerticallyBy() does not work as expected.
         */
        if (recyclerView != null) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if (newState == RecyclerView.SCROLL_STATE_IDLE && !mIsLoadingMore) {
                        checkToPerformLoadMoreFromBottom();
                    }
                }
            });
        }
    }

    private void checkToPerformLoadMoreFromBottom() {
        if (mOnLoadMoreListener != null &&
                !mIsLoadingMore &&
                ((findFirstVisibleItemPosition() + getChildCount()) >= getItemCount() - 1)) {
            mIsLoadingMore = true;
            mOnLoadMoreListener.onLoadMoreFromBottom();
        }
    }

    @Override
    public int scrollVerticallyBy(final int dy, final RecyclerView.Recycler recycler, final RecyclerView.State state) {
        if (mOnLoadMoreListener == null) {
            return super.scrollVerticallyBy(dy, recycler, state);
        }

        checkToPerformLoadMoreFromBottom();
        return super.scrollVerticallyBy(dy, recycler, state);
    }

    public void setOnLoadMoreListener(final OnLoadMoreCallback onLoadMoreListener) {
        mOnLoadMoreListener = onLoadMoreListener;
    }

    public void loadingFinished() {
        mIsLoadingMore = false;
    }

    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state,
                                       int position) {
        if (mLinearSmoothScroller == null) {
            mLinearSmoothScroller = new TopSnappedSmoothScroller(recyclerView.getContext());
        }
        mLinearSmoothScroller.setTargetPosition(position);
        startSmoothScroll(mLinearSmoothScroller);
    }

    public interface OnLoadMoreCallback {
        void onLoadMoreFromBottom();
    }

    private class TopSnappedSmoothScroller extends LinearSmoothScroller {
        public TopSnappedSmoothScroller(Context context) {
            super(context);

        }

        @Override
        public PointF computeScrollVectorForPosition(int targetPosition) {
            return LoaderMoreLayoutManager.this
                    .computeScrollVectorForPosition(targetPosition);
        }

        @Override
        protected int getVerticalSnapPreference() {
            return SNAP_TO_START;
        }
    }
}
