package com.proapp.sompom.widget.tablayout;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.tabs.TabLayout;
import com.resourcemanager.helper.FontHelper;

import java.lang.reflect.Field;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 7/5/20.
 * <p>
 * Originally by Chenxi
 * https://stackoverflow.com/questions/45715737/android-tablayout-custom-indicator-width/58381087#58381087
 */

public class CustomTabIndicatorLayout extends TabLayout {

    private Typeface mTypeface;
    private boolean mIsSelectOnScroll;

    public CustomTabIndicatorLayout(Context context) {
        this(context, null);
    }

    public CustomTabIndicatorLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomTabIndicatorLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        mTypeface = FontHelper.getSFProFontFamily(getContext());
        setTabIndicatorFullWidth(false);
        setIndicatorWidth(70);
        addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(Tab tab) {
//                Timber.i("onTabSelected: " + tab.getPosition());
                changTabTextFontStyle(tab, true);
            }

            @Override
            public void onTabUnselected(Tab tab) {
//                Timber.i("onTabUnselected: " + tab.getPosition());
                changTabTextFontStyle(tab, false);
            }

            @Override
            public void onTabReselected(Tab tab) {
//                Timber.i("onTabReselected: " + tab.getPosition());
            }
        });
    }

    public void scrollSelectTab(@Nullable Tab tab, boolean isSelectOnScroll) {
        mIsSelectOnScroll = isSelectOnScroll;
        selectTab(tab);
    }

    public void addOnTabSelectedListener(@NonNull OnTabSelectedListener listener) {
        addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(Tab tab) {
                listener.onTabSelected(tab, mIsSelectOnScroll);
                changTabTextFontStyle(tab, true);
                if (mIsSelectOnScroll) mIsSelectOnScroll = false;
            }

            @Override
            public void onTabUnselected(Tab tab) {
                listener.onTabUnselected(tab);
                changTabTextFontStyle(tab, true);
            }

            @Override
            public void onTabReselected(Tab tab) {
                listener.onTabReselected(tab);
            }
        });
    }

    private void changTabTextFontStyle(Tab tab, boolean isSelected) {
        LinearLayout tabLayout = (LinearLayout) ((ViewGroup) getChildAt(0)).getChildAt(tab.getPosition());
        TextView tabTextView = (TextView) tabLayout.getChildAt(1);
        tabTextView.setTypeface(mTypeface, isSelected ? Typeface.BOLD : Typeface.NORMAL);
    }

    public void setSelectionTab(int position) {
        Timber.i("setSelectionTab: " + position);
        Tab tabAt = getTabAt(position);
        if (tabAt != null) {
            changTabTextFontStyle(tabAt, true);
        }
    }

    private class DefPreDrawListener implements ViewTreeObserver.OnPreDrawListener {

        private LinearLayout tabStrip = null;
        private int tabWidth;
        private Field fieldLeft;
        private Field fieldRight;

        public void setTabStrip(LinearLayout tabStrip, int width) {
            try {
                this.tabStrip = tabStrip;
                this.tabWidth = width;
                Class cls = tabStrip.getClass();
                fieldLeft = cls.getDeclaredField("indicatorLeft");
                fieldLeft.setAccessible(true);
                fieldRight = cls.getDeclaredField("indicatorRight");
                fieldRight.setAccessible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public boolean onPreDraw() {
            try {
                if (tabWidth > 0 && fieldLeft != null && fieldRight != null) {
                    int left = fieldLeft.getInt(this.tabStrip);
                    int right = fieldRight.getInt(this.tabStrip);
                    int diff = right - left - tabWidth;
                    left = left + diff / 2;
                    right = right - diff / 2;
                    fieldLeft.setInt(this.tabStrip, left);
                    fieldRight.setInt(this.tabStrip, right);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }
    }

    private DefPreDrawListener defPreDrawListener = new DefPreDrawListener();

    public void setIndicatorWidth(int widthDp) {
        Class<?> tabLayout = TabLayout.class;
        Field tabStrip = null;
        try {
            tabStrip = tabLayout.getDeclaredField("slidingTabIndicator");
            tabStrip.setAccessible(true);
            LinearLayout tabIndicator = (LinearLayout) tabStrip.get(this);
            int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, widthDp, Resources.getSystem().getDisplayMetrics());
            //avoid add preDrawListener multi times
            tabIndicator.getViewTreeObserver().removeOnPreDrawListener(defPreDrawListener);
            tabIndicator.getViewTreeObserver().addOnPreDrawListener(defPreDrawListener);
            defPreDrawListener.setTabStrip(tabIndicator, width);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public interface OnTabSelectedListener {
        void onTabSelected(Tab tab, boolean isSelectOnScroll);

        void onTabUnselected(Tab tab);

        void onTabReselected(Tab tab);
    }
}
