package com.proapp.sompom.widget.chat;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.gridlayout.widget.GridLayout;

import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;
import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnItemClickListener;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.ChatBg;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.emun.MultiChatImageItemType;
import com.proapp.sompom.utils.DrawChatImageRoundedUtil;
import com.proapp.sompom.utils.GlideLoadUtil;
import com.proapp.sompom.utils.MediaUtil;
import com.proapp.sompom.widget.ChatImageViewContainer;
import com.proapp.sompom.widget.CustomExoPlayer;
import com.proapp.sompom.widget.RoundedImageView;
import com.sompom.videomanager.helper.ExoPlayerBuilder;

import java.util.List;

import timber.log.Timber;

/**
 * Created by nuonveyo on 8/29/18.
 */

public class MultiImageChatView extends GridLayout {

    private boolean mIsRepliedChat;
    private GifVideoPlayerAddListener mListener;

    public MultiImageChatView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MultiImageChatView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MultiImageChatView(Context context) {
        super(context);
    }

    public void generateLayout(boolean isRepliedChat,
                               List<Media> mediaList,
                               OnItemClickListener<Integer> onItemClickListener,
                               OnLongClickListener onLongClickListener,
                               ChatBg chatBg,
                               GifVideoPlayerAddListener listener) {
        mListener = listener;
        setClipToOutline(true);
        mIsRepliedChat = isRepliedChat;
        removeAllViews();
        MultiChatImageItemType colItem = MultiChatImageItemType.getMultiImageType(mediaList.size());
        int colCount = colItem.getCol();
        int rowCount = mediaList.size() / colCount;

        setColumnCount(colCount);
        setRowCount(rowCount + 1);

        if (mediaList.size() % colCount > 0) {
            rowCount += 1;
        }

        int imageSize = getImageSize(colItem);
//        Timber.i("imageSize: " + imageSize + ", colItem: " + colItem);
        int marginRight = getContext().getResources().getDimensionPixelSize(R.dimen.space_tiny);
        int marginTop = getContext().getResources().getDimensionPixelSize(R.dimen.space_too_small);
        for (int i = 0, column = 0, row = 0; i < mediaList.size(); i++, column++) {
            if (column == colItem.getCol()) {
                column = 0;
                row++;
            }

            int validMarginTop = marginTop;
            int validMarginRight = marginRight;
            if (mIsRepliedChat) {
                if (row == 0) {
                    validMarginTop = 0;
                }
                if (column == (colCount - 1)) {
                    //Last column in row
                    validMarginRight = 0;
                }
            }

            addView(generateChildView(mediaList,
                    mediaList.get(i),
                    colItem,
                    rowCount,
                    colCount,
                    row,
                    column,
                    i,
                    imageSize,
                    validMarginTop,
                    validMarginRight,
                    onItemClickListener,
                    onLongClickListener,
                    chatBg));
        }
    }

    private View generateChildView(List<Media> mediaList,
                                   Media media,
                                   MultiChatImageItemType colItem,
                                   int rowCount,
                                   int colCount,
                                   int row,
                                   int column,
                                   int itemIndex,
                                   int imageSize,
                                   int marginTop,
                                   int marginRight,
                                   OnItemClickListener<Integer> onItemClickListener,
                                   OnLongClickListener onLongClickListener,
                                   ChatBg chatBg) {
        //Set the default size if media's size is not valid so as to render a proper layout
        if (media.getWidth() <= 0) {
            media.setWidth(getContext().getResources().getDimensionPixelSize(R.dimen.chat_media_width));
        }
        if (media.getHeight() <= 0) {
            media.setHeight(getContext().getResources().getDimensionPixelSize(R.dimen.chat_media_height));
        }

        View childRootView;
        RoundedImageView imageView = null;
        CustomExoPlayer customExoPlayer = null;
        ChatImageViewContainer chatImageViewContainer = null;
        ImageView mGifVideoThumbnail = null;

        if (media.getType() == MediaType.IMAGE || (media.getType() == MediaType.GIF && !isUrlGifFile(media))) {
            imageView = new RoundedImageView(getContext());
            childRootView = imageView;
        } else if (isUrlGifFile(media)) {
            ViewDataBinding dataBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                    R.layout.item_chat_gif_video_layout,
                    this,
                    false);
            customExoPlayer = dataBinding.getRoot().findViewById(R.id.playerView);
            mGifVideoThumbnail = dataBinding.getRoot().findViewById(R.id.imageViewThumbnail);
            chatImageViewContainer = (ChatImageViewContainer) dataBinding.getRoot();
            childRootView = dataBinding.getRoot();
        } else {
            //Video type
            ViewDataBinding dataBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                    R.layout.item_video_layout,
                    this,
                    false);
            TextView durationTv = dataBinding.getRoot().findViewById(R.id.duration);
            durationTv.setText(MediaUtil.getVideoDurationDisplayFormat(media.getDuration()));
            imageView = dataBinding.getRoot().findViewById(R.id.imageView);
            childRootView = dataBinding.getRoot();
        }

        if (imageView != null) {
            imageView.setRepliedChat(mIsRepliedChat);
            imageView.setRowIndex(row);
        }

        if (chatImageViewContainer != null) {
            chatImageViewContainer.setRepliedChat(mIsRepliedChat);
            chatImageViewContainer.setRowIndex(row);
        }

        if (colItem == MultiChatImageItemType.OneCol) {
            if (imageView != null) {
                imageView.setCircle(false);
                imageView.setScaleCenterCrop(true);
                imageView.setCornerType(DrawChatImageRoundedUtil.getCornerType(chatBg));
                imageView.setTag(R.id.action_chat, media.getUrl());
            }

            if (chatImageViewContainer != null) {
                chatImageViewContainer.setCircle(false);
                chatImageViewContainer.setScaleCenterCrop(true);
                chatImageViewContainer.setCornerType(DrawChatImageRoundedUtil.getCornerType(chatBg));
            }

            childRootView.setOnClickListener(v -> onItemClickListener.onClick(itemIndex));

            childRootView.setOnLongClickListener(view -> {
                view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY,
                        HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);
                onLongClickListener.onLongClickAtPosition(media);
                return false;
            });
            GridLayout.LayoutParams param = getLayoutParam(LayoutParams.WRAP_CONTENT, column, row);
            Orientation orientation = Orientation.getOrientation(media);

            if (orientation == Orientation.SQUARE) {
                int size = getContext().getResources().getDimensionPixelSize(R.dimen.chat_media_width);
                param.width = size;
                param.height = size;
            } else if (orientation == Orientation.ORIENTATION) {
                int maxWidth = getContext().getResources().getDimensionPixelSize(R.dimen.chat_media_width);
                int maxHeight = maxWidth * media.getHeight() / media.getWidth();
                param.width = maxWidth;
                param.height = maxHeight;
            } else if (orientation == Orientation.PORTRAIT) {
                int maxHeight = getContext().getResources().getDimensionPixelOffset(R.dimen.chat_media_height);
                int width = maxHeight * media.getWidth() / media.getHeight();
                int height = maxHeight;
                int maxWidth = getContext().getResources().getDimensionPixelSize(R.dimen.chat_media_width);
                if (width > maxWidth) {
                    width = maxWidth;
                    height = maxWidth * media.getHeight() / media.getWidth();
                }
                param.width = width;
                param.height = height;
            } else {
                int size = getContext().getResources().getDimensionPixelSize(R.dimen.chat_media_width);

                if (imageView != null) {
                    imageView.setMaxWidth(size);
                    imageView.setMaxHeight(size);
                }

                if (chatImageViewContainer != null) {
//                    chatImageViewContainer.setMaxWidth(size);
//                    chatImageViewContainer.setMaxHeight(size);
                }
            }

            int chatMediaWidth = getContext().getResources().getDimensionPixelSize(R.dimen.chat_media_width);
            Timber.i("orientation: " + orientation +
                    ", chatMediaWidth: " + chatMediaWidth +
                    ", media width: " + media.getWidth() +
                    ", media height: " + media.getHeight() +
                    ", param.width: " + param.width +
                    ", param.height: " + param.height);
            //Recheck the calculated size of image for only replied message having one media
            if (mIsRepliedChat && mediaList.size() == 1 && param.width < chatMediaWidth) {
                /*
                    Check replied chat that has only one media. If this case is reached, we have to check
                    the width of that media and if it does not the default one we expect, we have to resize
                    its width to make sure its width is bigger enough for not truncated the the replied chat
                    header text.
                */
                int maxWidth = chatMediaWidth;
                int maxHeight = maxWidth * media.getHeight() / media.getWidth();
                param.width = maxWidth;
                param.height = maxHeight;
            }

            childRootView.setLayoutParams(param);

            if (imageView != null) {
                ImageView finalImageView = imageView;
                childRootView.postDelayed(() -> GlideLoadUtil.setChatMultiImageView(finalImageView,
                        getThumbnailUrl(media),
                        600, null), 100);
            }

            if (chatImageViewContainer != null) {
                initExoPlayer(getContext(), media, customExoPlayer, mGifVideoThumbnail);
            }
        } else {
            if (imageView != null) {
                imageView.setCircle(false);
                imageView.setScaleCenterCrop(true);
                imageView.setCornerType(DrawChatImageRoundedUtil.getCornerType(chatBg,
                        rowCount,
                        colCount,
                        row,
                        column,
                        itemIndex,
                        mediaList.size()));

                GlideLoadUtil.setChatMultiImageView(imageView,
                        getThumbnailUrl(media),
                        300, null);
                imageView.setTag(R.id.action_chat, media.getUrl());
            }

            if (chatImageViewContainer != null) {
                chatImageViewContainer.setCircle(false);
                chatImageViewContainer.setScaleCenterCrop(true);
                chatImageViewContainer.setCornerType(DrawChatImageRoundedUtil.getCornerType(chatBg,
                        rowCount,
                        colCount,
                        row,
                        column,
                        itemIndex,
                        mediaList.size()));
                initExoPlayer(getContext(), media, customExoPlayer, mGifVideoThumbnail);
            }

            childRootView.setOnClickListener(v -> onItemClickListener.onClick(itemIndex));
            childRootView.setOnLongClickListener(view -> {
                onLongClickListener.onLongClickAtPosition(mediaList.get(itemIndex));
                return false;
            });

            GridLayout.LayoutParams param = getLayoutParam(imageSize, column, row);
            param.rightMargin = marginRight;
            param.topMargin = marginTop;
            childRootView.setLayoutParams(param);

        }

//        Timber.i("" + imageView.getCornerType() + ", row: " + row + ", column: " + column + ", itemIndex: " + itemIndex);

        return childRootView;
    }

    private boolean isUrlGifFile(Media media) {
        /*
           We will handle the display of gif mp4 with video play on if it is the hosted url. In
           case of local gif, we still keep loading with glide library.
         */
        return media != null &&
                media.getType() == MediaType.GIF &&
                !TextUtils.isEmpty(media.getUrl()) &&
                media.getUrl().startsWith("http");
    }

    private String getThumbnailUrl(Media media) {
        if (!TextUtils.isEmpty(media.getThumbnail())) {
            return media.getThumbnail();
        } else {
            return media.getUrl();
        }
    }

    private int getImageSize(MultiChatImageItemType colItem) {
        int imageSize = getContext().getResources().getDimensionPixelSize(R.dimen.chat_multi_image_size);
        if (colItem == MultiChatImageItemType.TwoCol) {
            imageSize = imageSize * MultiChatImageItemType.ThreeCol.getCol() / 2;
        }

        return imageSize;
    }

    private GridLayout.LayoutParams getLayoutParam(int size, int column, int row) {
        GridLayout.LayoutParams param = new GridLayout.LayoutParams();
        param.width = size;
        param.height = size;
        param.columnSpec = GridLayout.spec(column);
        param.rowSpec = GridLayout.spec(row);
        return param;
    }

    private enum Orientation {
        UNDEFINE,
        SQUARE,
        ORIENTATION,
        PORTRAIT;

        static Orientation getOrientation(Media media) {
            if (media.getWidth() > 0 && media.getHeight() > 0) {
                if (media.getWidth() == media.getHeight()) {
                    return SQUARE;
                } else if (media.getWidth() > media.getHeight()) {
                    return ORIENTATION;
                } else {
                    return PORTRAIT;
                }
            }
            return UNDEFINE;
        }
    }

    private void initExoPlayer(Context context, Media media, PlayerView playerView, ImageView thumbnail) {
        thumbnail.setVisibility(VISIBLE);
        SimpleExoPlayer gifVideoPlayer = mListener.getGifVideoPlayer(media.getId());
        if (gifVideoPlayer == null) {
            gifVideoPlayer = ExoPlayerBuilder.build(context,
                    media.getUrl(),
                    isPlaying -> {
                        if (isPlaying) {
                            thumbnail.setVisibility(INVISIBLE);
                        }
                    });
            mListener.onGifVideoPlayerCreated(media.getId(), gifVideoPlayer);
//            Timber.i("Create new gif video player for media id " + media.getId());
        }

        gifVideoPlayer.setPlayWhenReady(true);
        gifVideoPlayer.setVolume(0f);
        gifVideoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
        playerView.setUseController(false);
        playerView.setPlayer(gifVideoPlayer);

        playerView.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View view) {
                SimpleExoPlayer gifVideoPlayer1 = mListener.getGifVideoPlayer(media.getId());
                if (gifVideoPlayer1 != null) {
//                    Timber.i("onViewAttachedToWindow of media id " + media.getId() + ", hash player: " + gifVideoPlayer1.hashCode());
                    gifVideoPlayer1.setPlayWhenReady(true);
                } else {
                    SimpleExoPlayer gifVideoPlayer2 = ExoPlayerBuilder.build(context,
                            media.getUrl(),
                            isPlaying -> {
                                if (isPlaying) {
                                    thumbnail.setVisibility(INVISIBLE);
                                }
                            });
                    mListener.onGifVideoPlayerCreated(media.getId(), gifVideoPlayer2);
//                    Timber.i("onViewAttachedToWindow create new gif video player for media id " + media.getId());
                    gifVideoPlayer2.setPlayWhenReady(true);
                    gifVideoPlayer2.setVolume(0f);
                    gifVideoPlayer2.setRepeatMode(Player.REPEAT_MODE_ONE);
                    playerView.setUseController(false);
                    playerView.setPlayer(gifVideoPlayer2);
                }
            }

            @Override
            public void onViewDetachedFromWindow(View view) {
                SimpleExoPlayer gifVideoPlayer1 = mListener.getGifVideoPlayer(media.getId());
                if (gifVideoPlayer1 != null) {
//                    Timber.i("onViewDetachedFromWindow of media id " + media.getId() + ", hash player: " + gifVideoPlayer1.hashCode());

                    gifVideoPlayer1.stop();
                    gifVideoPlayer1.release();
                    gifVideoPlayer1 = null;
                    mListener.onGifVideoPlayerReleased(media.getId());
                }
            }
        });
    }

    public interface OnLongClickListener {
        void onLongClickAtPosition(Media media);
    }

    @BindingAdapter("setAdaptivePlayIconSize")
    public static void setAdaptivePlayIconSize(TextView textView, ViewGroup viewGroup) {
        viewGroup.postDelayed(() -> {
            int newSize = (int) (viewGroup.getWidth() * 0.25); //25% width of its parent
            int maxSize = textView.getContext().getResources().getDimensionPixelSize(R.dimen.text_a_bit_large);
            if (newSize > maxSize) {
                newSize = maxSize;
            }
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, newSize);
            textView.setVisibility(View.VISIBLE);
        }, 100);
    }

    public interface GifVideoPlayerAddListener {
        void onGifVideoPlayerCreated(String mediaId, SimpleExoPlayer exoPlayer);

        SimpleExoPlayer getGifVideoPlayer(String mediaId);

        void onGifVideoPlayerReleased(String mediaId);
    }
}
