package com.proapp.sompom.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;

import com.amulyakhare.textdrawable.TextDrawable;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.listener.LiveUserDataListener;
import com.proapp.sompom.chat.listener.SupportLiveUserData;
import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.databinding.CustomLayoutUserImageProfileBinding;
import com.proapp.sompom.helper.GlideApp;
import com.proapp.sompom.model.emun.Presence;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.utils.GlideLoadUtil;
import com.proapp.sompom.viewmodel.binding.ImageViewBindingUtil;

import java.util.Date;

/**
 * Created by nuonveyo on 8/6/18.
 */

public class ImageProfileLayout extends LinearLayout implements LiveUserDataListener, SupportLiveUserData {

    private CustomLayoutUserImageProfileBinding mBinding;
    private boolean mIsShowStatus;
    private RenderType mRenderType = RenderType.USER;
    private int mImageDimension;
    private ImageSize mImageSize;
    private SocketService.SocketBinder mBinder;
    private User mUser;
    private int mStatusSizePercent;
    private Presence mPresence;
    private int mItemPosition = -1;

    public ImageProfileLayout(Context context) {
        super(context);
        init(null);
    }

    public ImageProfileLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ImageProfileLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ImageProfileLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    public void setShowStatus(boolean showStatus) {
        mIsShowStatus = showStatus;
    }

    private void init(AttributeSet attr) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.custom_layout_user_image_profile,
                this,
                true);

        if (attr != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attr,
                    R.styleable.ImageProfileLayout,
                    0,
                    0);
            mImageDimension = typedArray.getDimensionPixelSize(R.styleable.ImageProfileLayout_dimension, 0);
            mImageSize = ImageSize.fromValue(typedArray.getInt(R.styleable.ImageProfileLayout_size, ImageSize.MEDIUM.mValue));
            mRenderType = RenderType.fromValue(typedArray.getInt(R.styleable.ImageProfileLayout_render, RenderType.USER.mValue));
            mIsShowStatus = typedArray.getBoolean(R.styleable.ImageProfileLayout_status, true);
            mStatusSizePercent = typedArray.getInteger(R.styleable.ImageProfileLayout_statusSizePercent, 35);

            validationLayout();
            typedArray.recycle();
        }
    }

    public void setImageDrawable(Drawable drawable) {
        mBinding.imageViewProfile.setImageDrawable(drawable);
    }

    public void setImageBitmap(Bitmap bitmap) {
        mBinding.imageViewProfile.setImageBitmap(bitmap);
    }

    public void setImageSize(ImageSize imageSize) {
        mImageSize = imageSize;
    }

    public void loadPreviewUrl(String url) {
        User user = new User();
        user.setUserProfileThumbnail(url);
        user.setFirstName(mUser.getFirstName());
        user.setLastName(mUser.getLastName());
        loadProfile(mBinding.imageViewProfile, user);
    }

    public void setUser(User user, boolean revalidateLayout, int itemPosition) {
        mItemPosition = itemPosition;
//        Timber.i("setUser at " + mItemPosition + ", user: " + user.getFullName() + ", online: " + user.isOnline());
        setUser(user, revalidateLayout);
    }

    public void setUser(User user, boolean revalidateLayout) {
//        Timber.i("setUser: " + user.getFullName() +
//                ", isOnline: " + user.isOnline() +
//                ", last active date: " + user.getLastActivity() +
//                ", hash: " + user.getHash() +
//                ", Profile: " + user.getUserProfile());
        mUser = user;
        if (revalidateLayout) {
            validationLayout();
        }

        if (mRenderType == RenderType.USER) {
            loadProfile(mBinding.imageViewProfile, user);
            setStatus(user);
        } else {
            loadProfile(mBinding.imageViewActive, user);
            String coverUrl = user.getUserCoverProfileThumbnail();
            if (TextUtils.isEmpty(coverUrl)) {
                coverUrl = user.getUserCoverProfile();
            }
            setChatMultiImageView(mBinding.imageViewProfile, coverUrl);
        }
    }

    public void loadSimpleUrl(String url) {
        GlideLoadUtil.loadCircle(mBinding.imageViewProfile, url, null);
        mBinding.imageViewActive.setVisibility(GONE);
    }

    public void setUser(User user, int dimension) {
        mImageDimension = dimension;
        setUser(user, true);
    }

    private void setChatMultiImageView(ImageView imageView, String imageUrl) {
        if (!GlideLoadUtil.isValidContext(imageView.getContext())) {
            return;
        }

        Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_200dp);
        GlideApp.with(imageView)
                .load(imageUrl)
                .centerCrop()
                .placeholder(drawable)
                .override(0, 100)
                .into(imageView);
    }

    private int getDimension() {
        if (mImageSize == ImageSize.SMALL) {
            return getContext().getResources().getDimensionPixelSize(R.dimen.image_seller);
        }

        return getContext().getResources().getDimensionPixelSize(R.dimen.user_profile_medium_size);
    }

    private void setStatus(final User user) {
        AbsBaseActivity absBaseActivity = getAbsBaseActivity();
        if (absBaseActivity != null) {
            if (mBinder == null) {
                absBaseActivity.addOnServiceListener(binder -> {
                    mBinder = (SocketService.SocketBinder) binder;
                    mBinder.setUserStatus(user, false, false, "ImageProfileLayout setStatus");
                    if (isAttachedToWindow()) {
                        mBinder.addLiveUserDataListener(ImageProfileLayout.this);
                    }
                });
            } else {
                mBinder.setUserStatus(user, false, false, "ImageProfileLayout setStatus");
                if (isAttachedToWindow()) {
                    mBinder.addLiveUserDataListener(ImageProfileLayout.this);
                }
            }
        }
    }

    private AbsBaseActivity getAbsBaseActivity() {
        if (getContext() instanceof android.view.ContextThemeWrapper &&
                ((android.view.ContextThemeWrapper) getContext()).getBaseContext() instanceof AbsBaseActivity) {
            return (AbsBaseActivity) ((android.view.ContextThemeWrapper) getContext()).getBaseContext();
        } else if (getContext() instanceof AbsBaseActivity) {
            return (AbsBaseActivity) getContext();
        }

        return null;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
//        Timber.i("onAttachedToWindow");
        if (mUser != null && mBinder != null) {
            mBinder.addLiveUserDataListener(ImageProfileLayout.this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
//        Timber.i("onDetachedFromWindow");
        if (mBinder != null) {
            mBinder.removeLiveUserDataListener(ImageProfileLayout.this);
        }
    }

    public void setRenderType(RenderType renderType) {
        if (renderType == null) {
            return;
        }
        mRenderType = renderType;
        validationLayout();
    }

    private void validationLayout() {
        if (mImageDimension <= 0) {
            mImageDimension = getDimension();
        }

//        Timber.i("mImageDimension: " + mImageDimension + ", mRenderType: " + mRenderType);

        if (mRenderType == RenderType.USER) {
            mBinding.imageViewProfile.getLayoutParams().width = mImageDimension;
            mBinding.imageViewProfile.getLayoutParams().height = mImageDimension;

            ((MarginLayoutParams) mBinding.imageViewProfile.getLayoutParams()).bottomMargin = 0;
            ((MarginLayoutParams) mBinding.imageViewProfile.getLayoutParams()).rightMargin = 0;

            if (mIsShowStatus) {
                mBinding.imageViewActive.setVisibility(VISIBLE);
                int iconSize = mImageDimension * mStatusSizePercent / 100;
                mBinding.imageViewActive.getLayoutParams().width = iconSize;
                mBinding.imageViewActive.getLayoutParams().height = iconSize;
                mBinding.imageViewActive.setBackgroundResource(R.drawable.oval_white_bg);
            } else {
                mBinding.imageViewActive.setVisibility(GONE);
            }

        } else {
//            mBinding.imageViewProfile.setCircle(false);
//            mBinding.imageViewProfile.setCornerType(RoundCornerType.Single);
            int space = getResources().getDimensionPixelSize(R.dimen.space_medium);
//            mBinding.imageViewProfile.setMediumRadius(space);

            mBinding.imageViewProfile.getLayoutParams().width = mImageDimension;
            mBinding.imageViewProfile.getLayoutParams().height = mImageDimension;

            ((MarginLayoutParams) mBinding.imageViewProfile.getLayoutParams()).bottomMargin = mImageDimension / 9;
            ((MarginLayoutParams) mBinding.imageViewProfile.getLayoutParams()).rightMargin = mImageDimension / 9;

            mBinding.imageViewActive.getLayoutParams().width = (int) (mImageDimension / 1.6);
            mBinding.imageViewActive.getLayoutParams().height = (int) (mImageDimension / 1.6);
        }
    }

    public void hideOnlineStatus() {
        mIsShowStatus = false;
        mBinding.imageViewActive.setVisibility(GONE);
    }

    @Override
    public void onPresenceUpdate(String userId, Presence presence, Date lastOnlineTime) {
//        Timber.i("onPresenceUpdate of " + getUserName() +
//                ", current presence: " + mPresence +
//                ", update presence: " + presence +
//                ", activity: " + getActivityContainerName() +
//                getItemPosition());
        if (mIsShowStatus && mUser != null && TextUtils.equals(userId, mUser.getId())) {
            AbsBaseActivity absBaseActivity = getAbsBaseActivity();
            if (absBaseActivity != null) {
                absBaseActivity.runOnUiThread(() -> {
                    if (presence == Presence.Online) {
                        mPresence = presence;
                        applyOnlineStatusUpdate(true);
                    } else if (presence == Presence.Offline && (mPresence == null || mPresence == Presence.Online)) {
                        mPresence = presence;
                        applyOnlineStatusUpdate(false);
                    }
                });
            }
        }
    }

    private void applyOnlineStatusUpdate(boolean isOnline) {
        new Handler(getContext().getMainLooper()).post(() -> {
            if (isOnline) {
                mBinding.imageViewActive.setImageDrawable(ContextCompat.getDrawable(getContext(),
                        R.drawable.user_online_status_background));
                mBinding.imageViewActive.setVisibility(VISIBLE);
//                Timber.i("Will update online status of " + getUserName() + " to online.");
            } else {
                mBinding.imageViewActive.setImageDrawable(ContextCompat.getDrawable(getContext(),
                        R.drawable.user_offline_status_background));
                mBinding.imageViewActive.setVisibility(VISIBLE);
//                Timber.i("Will update online status of " + getUserName() + " to offline.");
            }
        });
    }

    private String getItemPosition() {
        if (mItemPosition >= 0) {
            return ", Item position: " + mItemPosition;
        }

        return "";
    }

    @Override
    public User getUser() {
        return mUser;
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public void onProfileAvatarUpdated(String userId, int hash, String userProfile) {
        if (!TextUtils.isEmpty(userProfile)) {
//            Timber.i("onProfileAvatarUpdated of " + userId + ", hash: " + hash + ", userProfile: " + userProfile);
            mUser.setUserProfileThumbnail(userProfile);
            mUser.setHash(hash);
            new Handler(getContext().getMainLooper()).post(() -> {
//                Timber.i("Apply update profile avatar of: " + mUser.getFullName() +
//                        ", activity: " + getActivityContainerName() + getItemPosition());
                loadProfile(mBinding.imageViewProfile, mUser);
            });
        }
    }

    private void loadProfile(ImageView imageView, User user) {
        TextDrawable drawable = ImageViewBindingUtil.getTextDrawable(imageView.getContext(),
                user.getFirstName(),
                user.getLastName());
        GlideLoadUtil.loadCircle(imageView, user.getUserProfileThumbnail(), drawable);
    }

    @Override
    public String getUserId() {
        if (mUser == null) {
            return null;
        }
        return mUser.getId();
    }

    public enum RenderType {
        USER(1),
        SHOP(2);

        private final int mValue;

        RenderType(int value) {
            mValue = value;
        }

        private static RenderType fromValue(int value) {
            for (RenderType renderType : values()) {
                if (renderType.mValue == value) {
                    return renderType;
                }
            }
            return USER;
        }

    }

    public enum ImageSize {

        SMALL(1),
        MEDIUM(2);

        private final int mValue;

        ImageSize(int value) {
            mValue = value;
        }

        private static ImageSize fromValue(int value) {
            for (ImageSize renderType : values()) {
                if (renderType.mValue == value) {
                    return renderType;
                }
            }

            return MEDIUM;
        }
    }

    @BindingAdapter("setShowStatus")
    public static void setShowStatus(ImageProfileLayout imageProfileLayout, boolean isShowStatus) {
        imageProfileLayout.setShowStatus(isShowStatus);
        if (!isShowStatus) {
            imageProfileLayout.hideOnlineStatus();
        }
    }
}
