package com.proapp.sompom.widget.lifestream;

import android.graphics.Point;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.proapp.sompom.databinding.LayoutProductMediaBinding;
import com.proapp.sompom.databinding.ListItemImageBinding;
import com.proapp.sompom.helper.WallStreetIntentData;
import com.proapp.sompom.listener.OnMediaLayoutClickListener;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.SharedProduct;
import com.proapp.sompom.model.SharedTimeline;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.utils.VolumePlaying;
import com.proapp.sompom.viewholder.BindingViewHolder;
import com.proapp.sompom.viewholder.TimelineViewHolder;
import com.proapp.sompom.viewmodel.binding.ImageViewBindingUtil;

import java.util.List;

public class CollageViewBinder implements OnBindItemListener {

    private final List<Media> mMedias;
    private boolean mIsMediaVideoAdded;
    private Adaptive mAdaptive;
    private OnTimelineItemButtonClickListener mOnItemClickListener;
    private WallStreetIntentData mMyIntentData;
    private BindingViewHolder mBindingViewHolder;

    public CollageViewBinder(List<Media> medias,
                             Adaptive adaptive,
                             OnTimelineItemButtonClickListener onItemClickListener,
                             WallStreetIntentData myIntentData,
                             BindingViewHolder bindingViewHolder) {
        mMedias = medias;
        mAdaptive = adaptive;
        mOnItemClickListener = onItemClickListener;
        mMyIntentData = myIntentData;
        mBindingViewHolder = bindingViewHolder;
    }

    @Override
    public void onBind(View view, int position) {
        ViewDataBinding bind = DataBindingUtil.bind(view);
        if (bind instanceof LayoutProductMediaBinding) {
            ((LayoutProductMediaBinding) bind).mediaLayout.setMedia(mMedias.get(position));
            ((LayoutProductMediaBinding) bind).mediaLayout.setOnMediaLayoutClickListener(new OnMediaLayoutClickListener() {
                @Override
                public void onMediaLayoutClick(MediaLayout mediaLayout) {
                    mOnItemClickListener.getRecyclerView().pause();
                    mOnItemClickListener.getRecyclerView().setAutoResume(false);
                    mMyIntentData.setMediaPosition(position);
                    getAdaptive().startActivityForResult(mMyIntentData);
                }

                private Adaptive getAdaptive() {
                    if (mAdaptive instanceof SharedTimeline) {
                        return ((SharedTimeline) mAdaptive).getLifeStream();
                    }
                    return mAdaptive;
                }
            });


            if (mMedias.get(position).getType() == MediaType.VIDEO && !mIsMediaVideoAdded) {
                mIsMediaVideoAdded = true;
                ((LayoutProductMediaBinding) bind).mediaLayout.setVisibleFullScreenButton(true);
                ((LayoutProductMediaBinding) bind).mediaLayout.setMute(VolumePlaying.isMute(view.getContext()));
                ((LayoutProductMediaBinding) bind).mediaLayout.setVisibleMuteButton(true);
                if (mBindingViewHolder instanceof TimelineViewHolder) {
                    ((TimelineViewHolder) mBindingViewHolder).setMediaLayout(((LayoutProductMediaBinding) bind).mediaLayout, null);
                }
                ((LayoutProductMediaBinding) bind).mediaLayout.setOnFullScreenClickListener(v ->
                        mOnItemClickListener.onVideoFullScreenClicked(mMedias.get(position), position, ((LayoutProductMediaBinding) bind).mediaLayout.getTime()));
            }
        } else if (bind instanceof ListItemImageBinding) {
            ImageViewBindingUtil.loadImageUrl(((ListItemImageBinding) bind).imageView,
                    ((WallStreetAdaptive) mAdaptive).getMedia().get(position).getUrl(),
                    0,
                    300,
                    null,
                    false);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickListener.getRecyclerView().pause();
                    mMyIntentData.setMediaPosition(position);
                    mOnItemClickListener.getRecyclerView().setAutoResume(false);
                    getAdaptive().startActivityForResult(mMyIntentData);
                }

                private Adaptive getAdaptive() {
                    if (mAdaptive instanceof SharedProduct) {
                        return ((SharedProduct) mAdaptive).getProduct();
                    }
                    return mAdaptive;
                }
            });
        }
    }

    @Override
    public Point onRequiredWidthHeight() {
        Media media = mMedias.get(0);
        return new Point(media.getWidth(), media.getHeight());
    }
}