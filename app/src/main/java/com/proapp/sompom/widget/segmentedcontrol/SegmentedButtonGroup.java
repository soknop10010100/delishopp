package com.proapp.sompom.widget.segmentedcontrol;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnSegmentedClickListener;
import com.proapp.sompom.model.emun.SegmentedControlItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class SegmentedButtonGroup extends LinearLayout {
    private int mCheckPosition;
    @ColorInt
    private int mSelectedTextColor;
    @ColorInt
    private int mUnSelectedTextColor;
    private List<SegmentedButton> mSegmentedButtons = new ArrayList<>();
    private OnSegmentedClickListener mClickListener;

    public SegmentedButtonGroup(Context context) {
        super(context);
        init(null);
    }

    public SegmentedButtonGroup(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SegmentedButtonGroup(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attributeSet) {
        setOrientation(HORIZONTAL);
        if (attributeSet != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attributeSet,
                    R.styleable.SegmentedButtonGroup,
                    0,
                    0);
            mCheckPosition = typedArray.getInt(R.styleable.SegmentedButtonGroup_checkedPosition, 0);
            mSelectedTextColor = typedArray.getColor(R.styleable.SegmentedButtonGroup_selectedBackgroundColor, 0);
            mUnSelectedTextColor = typedArray.getColor(R.styleable.SegmentedButtonGroup_unSelectedBackgroundColor, 0);

            typedArray.recycle();
        }
    }

    public void addChildView(int id, @StringRes int title, boolean isLast) {
        LinearLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(getResources().getDimensionPixelSize(R.dimen.tab_text_padding),
                0,
                getResources().getDimensionPixelSize(R.dimen.tab_text_padding),
                0);
        SegmentedButton segmentedButton = new SegmentedButton(getContext());
        segmentedButton.setId(id);
        segmentedButton.setSelectedTextColor(mSelectedTextColor);
        segmentedButton.setUnSelectedTextColor(mUnSelectedTextColor);
        segmentedButton.setTitle(title);
        segmentedButton.setSelectedBackground(R.drawable.selected_message_segment_background);
        segmentedButton.setUnSelectedBackground(R.drawable.unselected_message_segment_background);
        segmentedButton.setIsSelected(getChildCount() == mCheckPosition);
        segmentedButton.setOnSegmentedButtonClick(view -> {
            if (mCheckPosition != view.getId()) {
                for (SegmentedButton button : mSegmentedButtons) {
                    button.setIsSelected(view.getId() == button.getId());
                }
                mCheckPosition = view.getId();
                mClickListener.onClick(SegmentedControlItem.getSegmentedControlItem(mCheckPosition));
            }
        });
        mSegmentedButtons.add(segmentedButton);
        addView(segmentedButton, params);
    }

    public void setOnSegmentedItemClickListener(OnSegmentedClickListener listener) {
        mClickListener = listener;
    }
}
