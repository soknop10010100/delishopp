package com.proapp.sompom.widget;

import android.content.Context;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.DelayViewClickListener;
import com.proapp.sompom.helper.PostTextGenerateStyleHelper;
import com.proapp.sompom.helper.WallSeeMoreTextHelper;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.CopyTextUtil;
import com.proapp.sompom.utils.SpecialTextRenderUtils;

import java.util.List;

import timber.log.Timber;

/**
 * Created by nuonveyo on 11/16/18.
 */

public class WallStreetDescriptionTextView extends androidx.appcompat.widget.AppCompatTextView {

    private boolean mIsSeeMore;
    private PostTextGenerateStyleHelper mGenerateStyleHelper;
    private boolean shouldShowLinkPreview;
    private boolean shouldShowPlacePreview;

    public WallStreetDescriptionTextView(Context context) {
        super(context);
        init();
    }

    public WallStreetDescriptionTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WallStreetDescriptionTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mGenerateStyleHelper = new PostTextGenerateStyleHelper(this,
                null,
                getResources().getDimensionPixelSize(R.dimen.text_chat),
                getResources().getDimensionPixelSize(R.dimen.text_xxlarge),
                R.font.sf_pro_regular,
                R.font.sf_pro_light,
                false,
                () -> !(shouldShowLinkPreview || shouldShowPlacePreview));
    }

    public void setShouldShowLinkPreview(boolean shouldShowLinkPreview) {
        this.shouldShowLinkPreview = shouldShowLinkPreview;
    }

    public void setShouldShowPlacePreview(boolean shouldShowPlacePreview) {
        this.shouldShowPlacePreview = shouldShowPlacePreview;
    }

    public void setDescription(String description,
                               String postId,
                               boolean isMaxLine,
                               int maxSeeMoreLine,
                               List<Media> postMedia,
                               com.proapp.sompom.listener.OnClickListener onClickListener) {
        Timber.i("maxSeeMoreLine: " + maxSeeMoreLine);

        mIsSeeMore = false;
        postDelayed(() -> {
            mGenerateStyleHelper.setPostMedia(postMedia);
            mGenerateStyleHelper.setOriginalText(description);
            mGenerateStyleHelper.setIgnoreTextChangeCheck(false);
            setText(SpecialTextRenderUtils.renderMentionUser(getContext(),
                    description,
                    true,
                    true,
                    AttributeConverter.convertAttrToColor(getContext(), R.attr.post_description_mention),
                    AttributeConverter.convertAttrToColor(getContext(), R.attr.post_description_url),
                    AttributeConverter.convertAttrToColor(getContext(), R.attr.post_description_hashtag),
                    false,
                    null));

            if (!mGenerateStyleHelper.shouldRenderAsQuoteType() && isMaxLine) {
                mIsSeeMore = false;
                setMaxLines(maxSeeMoreLine);

                //Check to retrieve previous see more management if exist.
                if (!TextUtils.isEmpty(postId)) {
                    Integer lineEndIndexForSeeMoreText = WallSeeMoreTextHelper.getLineEndIndexForSeeMoreText(postId, description);
                    Timber.i("lineEndIndexForSeeMoreText: " + lineEndIndexForSeeMoreText);
                    if (lineEndIndexForSeeMoreText != null) {
                        //See more text was already managed for this post, so just re-use it.
                        mIsSeeMore = true;
                        mGenerateStyleHelper.setIgnoreTextChangeCheck(true);
                        setText(SpecialTextRenderUtils.renderDescriptionWithSeeMore(getContext(),
                                getText(),
                                lineEndIndexForSeeMoreText));
                    } else {
                        performCheckToDisplaySeeMore(postId, description);
                    }
                } else {
                    performCheckToDisplaySeeMore(postId, description);
                }
            } else {
                //Remove set max line
                setMaxLines(Integer.MAX_VALUE);
            }

            setOnClickListener(new DelayViewClickListener() {
                @Override
                public void onDelayClick(View v) {
                    if (WallStreetDescriptionTextView.this.getSelectionStart() == -1
                            && WallStreetDescriptionTextView.this.getSelectionEnd() == -1
                            && onClickListener != null) {
                        onClickListener.onClick();
                    }
                }
            });
            setMovementMethod(LinkMovementMethod.getInstance());
        }, 50);
    }

    private void performCheckToDisplaySeeMore(String postId, String description) {
        int maxLine = getMaxLines();
        ViewTreeObserver vto = getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                Timber.i("onGlobalLayout");
                ViewTreeObserver obs = getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                Timber.i("getLineCount(): " + getLineCount());

                if (maxLine > 0 && getLineCount() >= maxLine) {
                    int lineEndIndex = getLayout().getLineEnd(maxLine - 1);
                    if (getText().length() > lineEndIndex) {
                        mIsSeeMore = true;
                                /*
                                Ignore text change validation for rendering quote since there
                                was already checked and display with more line
                                 */
                        mGenerateStyleHelper.setIgnoreTextChangeCheck(true);
                        setText(SpecialTextRenderUtils.renderDescriptionWithSeeMore(getContext(),
                                getText(),
                                lineEndIndex));
                        if (!TextUtils.isEmpty(postId)) {
                            WallSeeMoreTextHelper.addLineEndIndexForSeeMoreText(postId, description, lineEndIndex);
                        }
                    }
                }
            }
        });
    }

    public boolean isSeeMore() {
        return mIsSeeMore;
    }

    @BindingAdapter("setLongPressCopyText")
    public static void setLongPressCopyText(TextView textView, String value) {
        //Make long press to copy text
        textView.setOnLongClickListener(v -> {
            CopyTextUtil.copyTextToClipboard(textView.getContext(), value);
            return true;
        });
    }
}
