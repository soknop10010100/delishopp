package com.proapp.sompom.widget.lifestream;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.DelayViewClickListener;
import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.helper.PlayControlDispatcher;
import com.proapp.sompom.listener.OnMediaLayoutClickListener;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.VolumePlaying;
import com.proapp.sompom.widget.CustomExoPlayer;
import com.proapp.sompom.widget.MediaImageView;
import com.sompom.videomanager.helper.ExoPlayerBuilder;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by He Rotha on 8/28/17.
 */

public class MediaLayout extends RelativeLayout implements PlayingHandler.OnPlayListener {

    private View mImageFloatingVideo;
    private View mImageViewFullScreen;
    private CustomExoPlayer mPlayerView;
    private TextView mAudioMute;
    private View mPlayIcon;
    private MediaImageView mThumbnailImageView;
    private View mCloseView;
    private ImageView.ScaleType mScaleType = ImageView.ScaleType.CENTER_CROP;

    private SimpleExoPlayer mPlayer;
    private Media mMedia;
    private boolean mIsShowControl = false;
    private boolean mIsMute;
    private boolean mIsSaveOnPause = false;
    private boolean mIsCenterCrop = true;
    private OnMediaLayoutClickListener mOnMediaLayoutClickListener;
    private OnVolumeClickListener mOnVolumeClickListener;
    private PlayingHandler mMyHandler = new PlayingHandler(this);
    private boolean mIsInDetailPostScreen;
    private boolean mShouldEnableResizeVideoIcon = true;
    private MediaLayoutType mMediaLayoutType = MediaLayoutType.POST_MEDIA;
    private ExoPlayerBuilder.OnPlayerListener mOnPlayerListener;
    private List<SimpleExoPlayer> mSimpleExoPlayers = new ArrayList<>();

    private View.OnClickListener mMuteAudioClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            mIsMute = !mIsMute;
            VolumePlaying.updateMute(getContext(), mIsMute);
            if (getPlayer() != null) {
                if (mIsMute) {
                    getPlayer().setVolume(0f);
                } else {
                    getPlayer().setVolume(1f);
                }
            }
            setMute(mIsMute);
            if (mOnVolumeClickListener != null) {
                mOnVolumeClickListener.onVolumeClick(mIsMute);
            }
        }
    };

    public void setOnPlayerListener(ExoPlayerBuilder.OnPlayerListener onPlayerListener) {
        mOnPlayerListener = onPlayerListener;
    }

    public void setMediaLayoutType(MediaLayoutType mediaLayoutType) {
        mMediaLayoutType = mediaLayoutType;
    }

    public void setInDetailPostScreen(boolean inDetailPostScreen) {
        mIsInDetailPostScreen = inDetailPostScreen;
    }

    public MediaLayoutType getMediaLayoutType() {
        return mMediaLayoutType;
    }

    public void setShouldEnableResizeVideoIcon(boolean shouldEnableResizeVideoIcon) {
        mShouldEnableResizeVideoIcon = shouldEnableResizeVideoIcon;
    }

    public void setExoPlayerClickListener(CustomExoPlayer.CustomExoPlayerClickListener exoPlayerClickListener) {
        if (mPlayerView != null) {
            mPlayerView.setExoPlayerClickListener(exoPlayerClickListener);
        }
    }

    public void setScaleType(ImageView.ScaleType scaleType) {
        mScaleType = scaleType;
    }

    public MediaLayout(Context context) {
        super(context);
        init(context, null);
    }

    public MediaLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public MediaLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attributeSet) {
        ViewDataBinding binding;
        TypedArray a = context.obtainStyledAttributes(attributeSet, R.styleable.MediaLayout);
        boolean enableBackground = false;
        if (a.hasValue(R.styleable.MediaLayout_includeControl)) {
            enableBackground = a.getBoolean(R.styleable.MediaLayout_includeControl, false);
        }
        a.recycle();
        if (enableBackground) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_media_control, this, true);
        } else {
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_media, this, true);
        }
        setOnClickListener(new DelayViewClickListener() {
            @Override
            public void onDelayClick(@NonNull View widget) {
                if (mOnMediaLayoutClickListener != null) {
                    mOnMediaLayoutClickListener.onMediaLayoutClick(MediaLayout.this);
                }
            }
        });
        View view = binding.getRoot();
        mPlayerView = view.findViewById(R.id.player_view);
        mImageFloatingVideo = mPlayerView.findViewById(R.id.imageFloatingVideo);
        mImageViewFullScreen = view.findViewById(R.id.imageViewFullScreen);
        mAudioMute = view.findViewById(R.id.imageAudio);
        mPlayIcon = view.findViewById(R.id.imageViewPlayIcon);
        mThumbnailImageView = mPlayerView.findViewById(R.id.exo_thumbnail);
        mThumbnailImageView.setBackgroundColor(AttributeConverter.convertAttrToColor(getContext(), R.attr.image_place_holder_background));
        mCloseView = view.findViewById(R.id.imageClose);
        resetVideoIconVisibility();
        if (mPlayerView != null && mPlayerView.getUseController()) {
            mPlayerView.setControllerHideOnTouch(!mIsInDetailPostScreen);
        }
    }

    private void resetVideoIconVisibility() {
        if (mAudioMute != null) {
            mAudioMute.setVisibility(GONE);
        }
        if (mPlayIcon != null) {
            mPlayIcon.setVisibility(GONE);
        }
    }

    public void setCenterCropImage() {
        mIsCenterCrop = false;
    }

    public void setOnMediaLayoutClickListener(OnMediaLayoutClickListener onMediaLayoutClickListener) {
        mOnMediaLayoutClickListener = onMediaLayoutClickListener;
    }

    public void setVisibleMuteButton(boolean isVisible) {
        if (mAudioMute == null) {
            return;
        }
        if (isVisible && mMedia != null
                && isSoundSupportMedia()) {
            mAudioMute.setVisibility(VISIBLE);
            mAudioMute.setOnClickListener(mMuteAudioClickListener);
        } else {
            mAudioMute.setVisibility(GONE);
            mAudioMute.setOnClickListener(null);
        }
    }

    public void setMute(boolean isMute) {
        mIsMute = isMute;
        if (mAudioMute == null) {
            return;
        }
        if (isSoundSupportMedia()) {
            if (isMute) {
                mAudioMute.setText(R.string.code_sound_off);
            } else {
                mAudioMute.setText(R.string.code_sound_on);
            }
        }
    }

    public void release() {
//        if (ExoPlayerBuilder.isPlayerStoppable(mPlayer)) {
//            Timber.i("Release ExoPlayer");
//            mPlayer.stop();
//            mPlayer.release();
//            setKeepScreenOn(false);
//        }
        pauseOrReleaseMedia(true);
    }

    private void pauseOrReleaseMedia(boolean isRelease) {
        Timber.i("Mode " + (isRelease ? "Release" : "Pause"));
        Timber.i("Player size: " + mSimpleExoPlayers.size());
        for (SimpleExoPlayer simpleExoPlayer : mSimpleExoPlayers) {
            if (isRelease) {
                ExoPlayerBuilder.stopAndReleaseMediaPlayer(simpleExoPlayer);
            } else {
                ExoPlayerBuilder.stopMediaPlayer(simpleExoPlayer);
            }
        }
        if (isRelease) {
            mSimpleExoPlayers.clear();
        }
        setKeepScreenOn(false);
    }

    public void setMedia(Media media, boolean isInDetailPostScreen) {
        mIsInDetailPostScreen = isInDetailPostScreen;
        setMedia(media);
    }

    public void setMedia(Media media) {
        Timber.i("setMedia: " + media.getType() + ", mIsInDetailPostScreen: " + mIsInDetailPostScreen);
        if (mImageViewFullScreen != null) {
            mImageViewFullScreen.setVisibility(View.GONE);
        }
        resetVideoIconVisibility();
        mMedia = media;
        if (TextUtils.isEmpty(mMedia.getUrl())) {
            return;
        }
        resetPhoto();
        if (isSoundSupportMedia()) {
            resetScaleType();
            updatePhotoScaleType();
            if (mMediaLayoutType == MediaLayoutType.LINK_PREVIEW) {
                //Update mute icon
                setMute(VolumePlaying.isMute(getContext()));
                setShowControl(true);
                initPlayer(mIsInDetailPostScreen);
            } else {
                handleSetMediaTypeAsVideo();
            }

            if (mAudioMute != null) {
                mAudioMute.setVisibility(GONE);
            }
        } else if (media.getType() == MediaType.IMAGE) {
            handleSetMediaTypeAsImage(media);
        } else if (media.getType() == MediaType.GIF) {
            if (isUrlGifFile()) {
                handleSetMediaTypeAsVideo();
                onMediaPlay(true);
            } else {
                //Will treat as normal image loading.
                handleSetMediaTypeAsImage(media);
            }
        } else {
            resetScaleType();
        }
    }

    private void handleSetMediaTypeAsImage(Media media) {
        updatePhotoScaleType();
        mThumbnailImageView.setVisibility(VISIBLE);
        if (!mIsInDetailPostScreen && media.getThumbnail() != null) {
            Timber.i("Loading thumbnail with url: " + media.getThumbnail());
            mThumbnailImageView.loadImage(media.getThumbnail(),
                    null,
                    mScaleType == ImageView.ScaleType.CENTER_CROP);
        } else {
            Timber.i("Loading image with url: " + media.getUrl());
            mThumbnailImageView.loadImage(media.getThumbnail(),
                    media.getUrl(),
                    mScaleType == ImageView.ScaleType.CENTER_CROP);
        }
        if (mAudioMute != null) {
            mAudioMute.setVisibility(GONE);
        }
    }

    private void handleSetMediaTypeAsVideo() {
        mThumbnailImageView.setVisibility(VISIBLE);
        if (mMedia.getUrl().startsWith("http")) {
            Timber.i("Loading video with thumbnail: " + mMedia.getThumbnail());
            mThumbnailImageView.loadImage(mMedia.getThumbnail(),
                    null,
                    mScaleType == ImageView.ScaleType.CENTER_CROP);
        } else {
            Timber.i("Loading video with url: " + mMedia.getUrl());
            mThumbnailImageView.loadImage(null,
                    mMedia.getUrl(),
                    mScaleType == ImageView.ScaleType.CENTER_CROP);
        }
    }

    private boolean isUrlGifFile() {
        /*
           We will handle the display of gif mp4 with video play on if it is the hosted url. In
           case of local gif, we still keep loading with glide library.
         */
        return mMedia != null &&
                mMedia.getType() == MediaType.GIF &&
                !TextUtils.isEmpty(mMedia.getUrl()) &&
                mMedia.getUrl().startsWith("http");
    }

    private boolean isSoundSupportMedia() {
        return mMedia != null && (mMedia.getType() == MediaType.VIDEO
                || mMedia.getType() == MediaType.LIVE_VIDEO);
    }

    private void resetPhoto() {
        mThumbnailImageView.resetPhoto();
    }

    private void resetScaleType() {
        mScaleType = ImageView.ScaleType.CENTER_CROP;
    }

    private void updatePhotoScaleType() {
//        Timber.i("updatePhotoScaleType: " + mScaleType);
        mThumbnailImageView.setScaleType(mScaleType);
    }

    public void setSaveOnPause(boolean saveOnPause) {
        mIsSaveOnPause = saveOnPause;
    }

    public void setShowControl(boolean showControl) {
        mIsShowControl = showControl;
    }

    private SimpleExoPlayer setupPlayer(String url) {
        Timber.i("mIsShowControl: " + mIsShowControl + mMediaLayoutType);
        mPlayerView.setUseController(mIsShowControl);
        SimpleExoPlayer exoPlayer = ExoPlayerBuilder.build(getContext(), url, isPlaying -> {
            Timber.i("onPlayStatus " + isPlaying);
            if (mOnPlayerListener != null) {
                mOnPlayerListener.onPlayStatus(isPlaying);
            }
            if (mIsSaveOnPause) {
                mMedia.setPause(!isPlaying);
            }
            if (mMyHandler.isStartPlay()) {
                mThumbnailImageView.setVisibility(INVISIBLE);
            }
            setPlayIconVisibility(!isPlaying);
        });
        Timber.i("New Player Id: " + exoPlayer.hashCode() + ", MediaView: " + hashCode());
        mPlayerView.setPlayer(exoPlayer);
        Timber.i("mPlayerView.getUseController(): " + mPlayerView.getUseController());
        if (mPlayerView.getUseController()) {
            mPlayerView.setControlDispatcher(new PlayControlDispatcher() {
                @Override
                public void onButtonPlayClick(boolean isPlay) {
                    Timber.i("onButtonPlayClick: " + isPlay);
                    if (mOnVolumeClickListener != null) {
                        mOnVolumeClickListener.onButtonPlayClick(isPlay);
                    }
                    if (isPlay) {
                        mThumbnailImageView.setVisibility(INVISIBLE);
                    }
                    mMyHandler.setStartPlay(isPlay);
                    if (mMediaLayoutType == MediaLayoutType.LINK_PREVIEW &&
                            isPlay &&
                            !isVideoPlaying()) {
                        Timber.i("Force start video on play icon clicked.");
                        onPlay(true);
                    }
                }
            });
        }
        return exoPlayer;
    }

    public long getTime() {
        if (getPlayer() == null) {
            return 0;
        } else {
            return getPlayer().getCurrentPosition();
        }
    }

    private SimpleExoPlayer getPlayer() {
        return mPlayer;
    }

    public PlayerView getPlayerView() {
        return mPlayerView;
    }

    public void onMediaPlay(final boolean isStartPlay) {
        Timber.i(hashCode() + " onMediaPlay1 " + isStartPlay);
        if (mMediaLayoutType == MediaLayoutType.LINK_PREVIEW) {
            Timber.i("Is video preview: " + LinkPreviewRetriever.isVideoType(mMedia.getUrl()) + ", url: " + mMedia.getUrl());
        }
        mMyHandler.setStartPlay(isStartPlay);
        mMyHandler.startDelay();
    }

    public void onMediaPause() {
        Timber.i(hashCode() + " onMediaPause ");
        setPlayIconVisibility(true);
        mMyHandler.destroy();
        if (mPlayer != null
                && (mMedia.getType() == MediaType.VIDEO
                || mMedia.getType() == MediaType.LIVE_VIDEO)) {
//            if (ExoPlayerBuilder.isPlayerStoppable(mPlayer)) {
//                mPlayer.stop();
//            }
            pauseOrReleaseMedia(false);
            mMedia.setTime(mPlayer.getCurrentPosition());
            setKeepScreenOn(false);
        }
    }

    public void setVisibleCloseButton(boolean visible) {
        if (mCloseView != null) {
            mCloseView.setVisibility(visible ? VISIBLE : View.GONE);
        }
    }

    public void setOnCloseButtonClickedListener(OnClickListener onClickListener) {
        if (onClickListener != null && mCloseView != null) {
            mCloseView.setOnClickListener(onClickListener);
        }
    }

    public void setVisibleFloatingButton(boolean isShow) {
        if (mImageFloatingVideo == null) {
            return;
        }
        if (isShow) {
            mImageFloatingVideo.setVisibility(VISIBLE);
        } else {
            mImageFloatingVideo.setVisibility(GONE);
        }
    }

    public void setOnFloatingVideoClickedListener(OnClickListener onClickListener) {
        if (onClickListener != null && mImageFloatingVideo != null) {
            mImageFloatingVideo.setOnClickListener(onClickListener);
        }
    }

    public void setVisibleFullScreenButton(boolean isShow) {
        if (mImageViewFullScreen == null) {
            return;
        }
        if (isShow && mShouldEnableResizeVideoIcon) {
            mImageViewFullScreen.setVisibility(VISIBLE);
        } else {
            mImageViewFullScreen.setVisibility(GONE);
        }
    }

    public void setOnFullScreenClickListener(OnClickListener onFullScreenClickListener) {
        if (onFullScreenClickListener != null && mImageViewFullScreen != null) {
            mImageViewFullScreen.setOnClickListener(onFullScreenClickListener);
        }
    }

    private void setPlayIconVisibility(boolean isShowPlayIcon) {
        if (isVideoType()) {
            if (isShowPlayIcon) {
                if (mPlayIcon != null) {
                    mPlayIcon.setVisibility(VISIBLE);
                }
//                if (mAudioMute != null) {
//                    mAudioMute.setVisibility(GONE);
//                }
                setVisibleFullScreenButton(false);
                if (mMediaLayoutType == MediaLayoutType.LINK_PREVIEW) {
                    setVisibleMuteButton(false);
                }
            } else {
                if (mPlayIcon != null) {
                    mPlayIcon.setVisibility(GONE);
                }
//                if (mAudioMute != null) {
//                    mAudioMute.setVisibility(isSoundSupportMedia() ? VISIBLE : GONE);
//                }
                setVisibleFullScreenButton(true);
                if (mMediaLayoutType == MediaLayoutType.LINK_PREVIEW) {
                    setVisibleMuteButton(true);
                }
            }
        }
    }

    private boolean isVideoType() {
        return mMedia != null && mMedia.getType() == MediaType.VIDEO;
    }

    @Override
    public void onPlay(boolean isStartPlay) {
        Timber.i(hashCode() + " setPlayWhenReady " + isStartPlay + ", mIsMute: " + mIsMute);
        setPlayIconVisibility(false);

        if (TextUtils.isEmpty(mMedia.getUrl())) {
            return;
        }
        if (mMedia.getType() == MediaType.VIDEO ||
                isUrlGifFile() ||
                mMedia.getType() == MediaType.LIVE_VIDEO) {
            initPlayer(isStartPlay);
        }
    }

    private void initPlayer(boolean isStartPlay) {
        if (ExoPlayerBuilder.isPlayerStoppable(getPlayer())) {
            try {
                getPlayer().stop();
            } catch (Exception ex) {
                Timber.e("Stop video error: " + ex.getMessage());
            }
        }
        mPlayerView.setPlayer(null);
        SimpleExoPlayer simpleExoPlayer = setupPlayer(mMedia.getUrl());
//        if (mPlayer != null) {
//            Timber.i("Release previous video player of the media view");
//            ExoPlayerBuilder.stopAndReleaseMediaPlayer(mPlayer);
//        }
        mPlayer = simpleExoPlayer;
        mPlayer.setRepeatMode(isUrlGifFile() ? Player.REPEAT_MODE_ONE : Player.REPEAT_MODE_OFF);
        if (!mSimpleExoPlayers.contains(mPlayer)) {
            mSimpleExoPlayers.add(mPlayer);
        }
        if ((mIsMute && !mIsInDetailPostScreen) || isUrlGifFile()) {
            mPlayer.setVolume(0f);
        } else {
            mPlayer.setVolume(1f);
        }
        mPlayer.setPlayWhenReady(isStartPlay);
        mPlayer.seekTo(mMedia.getTime());
        setKeepScreenOn(true);
    }

    public boolean isVideoPlaying() {
        return mPlayer != null && mPlayer.getPlayWhenReady() && mPlayer.getPlaybackState() == Player.STATE_READY;
    }

    public void setOnVolumeClickListener(OnVolumeClickListener onVolumeClickListener) {
        mOnVolumeClickListener = onVolumeClickListener;
    }

    public interface OnVolumeClickListener {
        void onButtonPlayClick(boolean isPlay);

        void onVolumeClick(boolean isMute);
    }

    public enum MediaLayoutType {
        POST_MEDIA,
        LINK_PREVIEW,
        UNDEFINED;

        public static MediaLayoutType getFromValue(int value) {
            for (MediaLayoutType mediaLayoutType : MediaLayoutType.values()) {
                if (mediaLayoutType.ordinal() == value) {
                    return mediaLayoutType;
                }
            }

            return UNDEFINED;
        }
    }
}
