package com.proapp.sompom.widget;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;

import com.proapp.sompom.R;
import com.proapp.sompom.model.emun.RoundCornerType;
import com.tistory.zladnrms.roundablelayout.RoundableLayout;

public class ChatImageViewContainer extends RoundableLayout {

    private boolean mIsCircle = true;
    private boolean mIsScaleCenterCrop;
    private RoundCornerType mCornerType;
    private int mCirclePadding;
    private boolean mIsRepliedChat;
    private int mRowIndex;
    private boolean mIsAlreadyDrawn;
    private int mChatItemPosition;

    public ChatImageViewContainer(@NonNull Context context, @NonNull AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public ChatImageViewContainer(@NonNull Context context, @NonNull AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public ChatImageViewContainer(@NonNull Context context) {
        super(context);
        initView();
    }

    public void setRepliedChat(boolean repliedChat) {
        mIsRepliedChat = repliedChat;
    }

    public void setCirclePadding(int circlePadding) {
        mCirclePadding = circlePadding;
    }

    public void setCircle(boolean circle) {
        mIsCircle = circle;
    }

    public void setScaleCenterCrop(boolean scaleCenterCrop) {
        mIsScaleCenterCrop = scaleCenterCrop;
    }

    public RoundCornerType getCornerType() {
        return mCornerType;
    }

    public void setRowIndex(int rowIndex) {
        mRowIndex = rowIndex;
    }

    public void setCornerType(RoundCornerType cornerType) {
        mCornerType = cornerType;
        generateCorner();
    }

    private void initView() {
    }

    private void generateCorner() {
        int normalCornerSize = getResources().getDimensionPixelSize(R.dimen.chat_radius);
        int smallCornerSize = getResources().getDimensionPixelSize(R.dimen.chat_small_radius);

        if (mCornerType == RoundCornerType.Single) {
            if (!mIsRepliedChat) {
                setCornerLeftTop(normalCornerSize);
                setCornerRightTop(normalCornerSize);
            }
            setCornerRightBottom(normalCornerSize);
            setCornerLeftBottom(normalCornerSize);
        } else if (mCornerType == RoundCornerType.MeTop) {
            if (!mIsRepliedChat) {
                setCornerLeftTop(normalCornerSize);
                setCornerRightTop(normalCornerSize);
            }
            setCornerLeftBottom(normalCornerSize);
            setCornerRightBottom(smallCornerSize);
        } else if (mCornerType == RoundCornerType.MeMid) {
            if (!mIsRepliedChat || mRowIndex > 0) {
                setCornerLeftTop(normalCornerSize);
                setCornerRightTop(smallCornerSize);
            }
            setCornerLeftBottom(normalCornerSize);
            setCornerRightBottom(smallCornerSize);
        } else if (mCornerType == RoundCornerType.MeBottom) {
            if (!mIsRepliedChat || mRowIndex > 0) {
                setCornerLeftTop(normalCornerSize);
                setCornerRightTop(smallCornerSize);
            }
            setCornerLeftBottom(normalCornerSize);
            setCornerRightBottom(normalCornerSize);
        } else if (mCornerType == RoundCornerType.YouTop) {
            if (!mIsRepliedChat) {
                setCornerLeftTop(normalCornerSize);
                setCornerRightTop(normalCornerSize);
            }
            setCornerLeftBottom(smallCornerSize);
            setCornerRightBottom(normalCornerSize);
        } else if (mCornerType == RoundCornerType.YouMid) {
            if (!mIsRepliedChat || mRowIndex > 0) {
                setCornerLeftTop(smallCornerSize);
                setCornerRightTop(normalCornerSize);
            }
            setCornerLeftBottom(smallCornerSize);
            setCornerRightBottom(normalCornerSize);
        } else if (mCornerType == RoundCornerType.YouBottom) {
            if (!mIsRepliedChat || mRowIndex > 0) {
                setCornerLeftTop(smallCornerSize);
                setCornerRightTop(normalCornerSize);
            }
            setCornerLeftBottom(normalCornerSize);
            setCornerRightBottom(normalCornerSize);
        } else if (mCornerType == RoundCornerType.TopLeft) {
            if (!mIsRepliedChat) {
                setCornerLeftTop(normalCornerSize);
                setCornerRightTop(smallCornerSize);
            }
            setCornerLeftBottom(smallCornerSize);
            setCornerRightBottom(smallCornerSize);
        } else if (mCornerType == RoundCornerType.TopRight) {
            if (!mIsRepliedChat) {
                setCornerLeftTop(smallCornerSize);
                setCornerRightTop(normalCornerSize);
            }
            setCornerLeftBottom(smallCornerSize);
            setCornerRightBottom(smallCornerSize);
        } else if (mCornerType == RoundCornerType.BottomLeft) {
            if (!mIsRepliedChat || mRowIndex > 0) {
                setCornerLeftTop(smallCornerSize);
                setCornerRightTop(smallCornerSize);
            }
            setCornerLeftBottom(normalCornerSize);
            setCornerRightBottom(smallCornerSize);
        } else if (mCornerType == RoundCornerType.BottomRight) {
            if (!mIsRepliedChat || mRowIndex > 0) {
                setCornerLeftTop(smallCornerSize);
                setCornerRightTop(smallCornerSize);
            }
            setCornerLeftBottom(smallCornerSize);
            setCornerRightBottom(normalCornerSize);
        } else if (mCornerType == RoundCornerType.Bottom) {
            if (!mIsRepliedChat || mRowIndex > 0) {
                setCornerLeftTop(smallCornerSize);
                setCornerRightTop(smallCornerSize);
            }
            setCornerLeftBottom(normalCornerSize);
            setCornerRightBottom(normalCornerSize);
        } else {
            if (!mIsRepliedChat || mRowIndex > 0) {
                setCornerLeftTop(smallCornerSize);
                setCornerRightTop(smallCornerSize);
            }
            setCornerLeftBottom(smallCornerSize);
            setCornerRightBottom(smallCornerSize);
        }
    }
}
