package com.proapp.sompom.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.proapp.sompom.R;

/**
 * Created by Chhom Veasna on 1/20/2020.
 */
public class BadgeTextView extends AppCompatTextView {

    public BadgeTextView(Context context) {
        super(context);
        init();
    }

    public BadgeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BadgeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 1) {
                    setBackgroundResource(R.drawable.plural_badge_background);
                    setPadding(getResources().getDimensionPixelSize(R.dimen.capture_button_space),
                            getResources().getDimensionPixelSize(R.dimen.space_too_small),
                            getResources().getDimensionPixelSize(R.dimen.capture_button_space),
                            getResources().getDimensionPixelSize(R.dimen.space_too_small));

                } else {
                    setBackgroundResource(R.drawable.singular_badge_background);
                    setPadding(0, 0, 0, 0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
