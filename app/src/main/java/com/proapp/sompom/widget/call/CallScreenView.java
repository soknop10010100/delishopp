package com.proapp.sompom.widget.call;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.NetworkBroadcastReceiver;
import com.proapp.sompom.chat.AbsChatBinder;
import com.proapp.sompom.chat.call.CallNotificationHelper;
import com.proapp.sompom.chat.call.CallingService;
import com.proapp.sompom.chat.call.IncomingCallDataHolder;
import com.proapp.sompom.databinding.FragmentIncomingCallBinding;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.request.JoinChannelStatusUpdateBody;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.services.datamanager.CallDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.CallingViewModel;
import com.resourcemanager.utils.ToastUtil;
import com.sompom.pushy.service.SendBroadCastHelper;

import timber.log.Timber;

public class CallScreenView extends FrameLayout implements CallingViewModel.CallingViewModelListener,
        CallingService.CallingListener {

    public static final String IN_ACTION_CALL_EVENT = "IN_ACTION_CALL_EVENT";
    public static final String IS_OPEN = "IS_OPEN";

    private FragmentIncomingCallBinding mBinding;
    private CallingViewModel mCallingViewModel;
    private CallDataManager mCallDataManager;
    private CallingService.CallServiceBinder mCallServiceBinder;
    private CallScreenViewListener mListener;
    private BroadcastReceiver mBluetoothConnectionStateChangeReceiver;
    private NetworkBroadcastReceiver mNetworkBroadcastReceiver;
    private BroadcastReceiver mJoinGroupCallStatusReceiver;
    private boolean mIsCallEndedInvoked;
    private boolean mIsAlreadyBroadcastStartGroupCallMessage;
    private final Context mApplicationContext;

    public CallScreenView(@NonNull Context context,
                          Context applicationContext,
                          CallDataManager callDataManager,
                          CallingService.CallServiceBinder callServiceBinder,
                          CallScreenViewListener listener) {
        super(context);
        /*
            mApplicationContext is mandatory for loading image into ImageViews for this CallScreenView view, will be
            first created with activity context, and then it will be removed from that activity if
            user enable floating call view. Note: When user enable floating window call, we will destroy
            call screen activity and present this call view into floating window instead, and in contrast,
            then user resume back the call screen, we remove this call view from floating and add it back
            to newly created call activity. So that is only application context that is reliable to be used
            for loading image not the activity context for it might be removed and destroyed many times.
         */
        mApplicationContext = applicationContext;
        initView(callDataManager, callServiceBinder, listener);
    }

    private void initView(CallDataManager callDataManager,
                          CallingService.CallServiceBinder callServiceBinder,
                          CallScreenViewListener listener) {
        mCallDataManager = callDataManager;
        mCallServiceBinder = callServiceBinder;
        mListener = listener;
    }

    public void startIncomingCall(Context context,
                                  IncomingCallDataHolder incomingCallData,
                                  boolean isIncomingCallFromNotification) {
        initCallScreen(context,
                "startIncomingCall",
                incomingCallData.getCallType(),
                AbsCallService.CallActionType.INCOMING,
                incomingCallData,
                null,
                null,
                isIncomingCallFromNotification,
                null);
    }

    public void startOutGoingCall(Context context,
                                  AbsCallService.CallType callType,
                                  User callTo,
                                  UserGroup group) {
        initCallScreen(context,
                "startOutGoingCall",
                callType,
                AbsCallService.CallActionType.CALL,
                null,
                callTo,
                group,
                false,
                null);
    }

    public void startJoinGroupCall(Context context,
                                   AbsCallService.CallType callType,
                                   UserGroup group) {
        initCallScreen(context,
                "startJoinGroupCall",
                callType,
                AbsCallService.CallActionType.JOINED,
                null,
                null,
                group,
                false,
                null);
    }

    private void initCallScreen(Context uiContext,
                                String tag,
                                AbsCallService.CallType callType,
                                AbsCallService.CallActionType callActionType,
                                IncomingCallDataHolder callVo,
                                User recipient,
                                UserGroup group,
                                boolean isIncomingCallFromNotification,
                                Product product) {
        /*
            To make the inflating this layout work from service background for all Android variety versions,
            the inflate context must be the UI context and in this case, it is activity. And normally this
            UI context can be destroyed if user enable floating window usage. So in case that some other
            stuff need the context object involvement, we must use "mApplicationContext" which is the whole
            application context that stays alive all the time of app running.
         */
        Timber.e("initCallScreen of: " + tag +
                ", old Call layout: " + (mBinding != null ? mBinding.getRoot().hashCode() : null) +
                ", old mCallingViewModel: " + (mCallingViewModel != null ? mCallingViewModel.hashCode() : null));

        removeAllViews();
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(uiContext),
                R.layout.fragment_incoming_call,
                this,
                true);

        mIsCallEndedInvoked = false;
        mIsAlreadyBroadcastStartGroupCallMessage = false;
        mBinding.callViewContainer.setApplicationContext(mApplicationContext);
        mCallingViewModel = new CallingViewModel(mApplicationContext,
                mCallDataManager,
                callType,
                callActionType,
                callVo,
                recipient,
                group,
                isIncomingCallFromNotification,
                product,
                mCallServiceBinder,
                this,
                this);
        mBinding.setVariable(BR.viewModel, mCallingViewModel);
        registerBluetoothConnectionStateChangeReceiver();
        registerNetworkBroadcastReceiver();
        registerUpdateJoinGroupCallStatusReceiver();
        mCallingViewModel.getHandler().postDelayed(() -> broadcastInActionCallEvent(true),
                1000);
        Timber.e("initCallScreen of: " + tag +
                ", new Call layout: " + mBinding.getRoot().hashCode() +
                ", new mCallingViewModel: " + mCallingViewModel.hashCode());
    }

    private void registerBluetoothConnectionStateChangeReceiver() {
        mBluetoothConnectionStateChangeReceiver =
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String action = intent.getAction();
                        Timber.i("onReceive: mBluetoothConnectionStateChangeReceiver: action: " + action);
                        if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                            Timber.i("BluetoothDevice connected");
                            mCallingViewModel.onUpdateBluetoothConnectionStatus(true);
                        } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                            Timber.i("BluetoothDevice disconnected");
                            mCallingViewModel.onUpdateBluetoothConnectionStatus(false);
                        }
                    }
                };
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        getContext().registerReceiver(mBluetoothConnectionStateChangeReceiver, filter);
    }

    private void registerNetworkBroadcastReceiver() {
        mNetworkBroadcastReceiver = new NetworkBroadcastReceiver();
        mNetworkBroadcastReceiver.setListener(networkState -> {
            Timber.i("onNetworkState: " + networkState);
            onNetworkStateChange(networkState);
        });
        getContext().registerReceiver(mNetworkBroadcastReceiver, mNetworkBroadcastReceiver.getIntentFilter());
    }

    private void registerUpdateJoinGroupCallStatusReceiver() {
        mJoinGroupCallStatusReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                JoinChannelStatusUpdateBody data = intent.getParcelableExtra(SharedPrefUtils.DATA);
                if (data != null) {
                    Timber.i("onReceive: JoinGroupCallStatusReceiver: " + new Gson().toJson(data));
                    mCallingViewModel.checkToUpdateToVideoCallTypeIfNecessary(data.getConversationId(),
                            data.getChannelOpen(),
                            data.getVideo());
                }
            }
        };
        getContext().registerReceiver(mJoinGroupCallStatusReceiver,
                new IntentFilter(ChatHelper.JOIN_GROUP_CALL_STATUS_UPDATE));
    }

    private FragmentIncomingCallBinding getBinding() {
        return mBinding;
    }

    @Override
    public CallViewContainer getCallViewContainer() {
        return getBinding().callViewContainer;
    }

    @Override
    public View getTopCallOptionContainerView() {
        return getBinding().topCallOptionContainerView;
    }

    @Override
    public View getBottomCallOptionContainerView() {
        return getBinding().bottomCallOptionContainerView;
    }

    @Override
    public void shouldDispatchStartGroupCall(AbsCallService.CallType callType) {
        Timber.i("shouldDispatchStartGroupCall: " + callType);
        if (!mIsAlreadyBroadcastStartGroupCallMessage &&
                mCallingViewModel.isCurrentUserCaller() &&
                isAbleToSendMessage()) {
            mIsAlreadyBroadcastStartGroupCallMessage = true;
            checkToSendGroupStartCallMessage(callType, SharedPrefUtils.getCurrentUser(getContext()));
        }
    }

    private void checkToSendGroupStartCallMessage(AbsCallService.CallType callType, User sender) {
        UserGroup group = mCallingViewModel.getCallService().getCallGroup();
        if (group != null) {
            Timber.i("checkToSendGroupStartCallMessage");
            Chat groupChat = ChatHelper.buildStartOrEndGroupCallEventMessages(getContext(),
                    callType,
                    sender,
                    group,
                    true);
            groupChat.setSendTo(group.getId());
            getSocketService().sendMessage(null, groupChat);
        }
    }

    private boolean isAbleToSendMessage() {
        return mCallServiceBinder != null && mCallServiceBinder.getChatBinder() != null;
    }

    private AbsChatBinder getSocketService() {
        return mCallServiceBinder.getChatBinder();
    }

    @Override
    public void onCallChannelIdIdGenerated(String channelId) {
        Timber.i("onCallChannelIdIdGenerated: " + channelId);
        if (mListener != null) {
            mListener.onCallChannelIdIdGenerated(channelId);
        }
    }

    @Override
    public void onCallEnd(AbsCallService.CallType callType,
                          String channelId,
                          boolean isGroup,
                          User recipient,
                          boolean isMissedCall,
                          int calledDuration,
                          boolean isEndedByClickAction,
                          boolean isCauseByTimeOut,
                          boolean isCausedByParticipantOffline) {
        Timber.i("onCallEnd: mIsCallEndedInvoked: " + mIsCallEndedInvoked + ", mListener: " + mListener);
        if (!mIsCallEndedInvoked) {
            mIsCallEndedInvoked = true;
            Timber.i("onCallEnd: recipient: " + (recipient != null ? recipient.getId() : null)
                    + ", callType: " + callType
                    + ", isMissedCall: " + isMissedCall
                    + ", calledDuration: " + calledDuration
                    + ", isEndedByClickAction: " + isEndedByClickAction +
                    ", isCausedByParticipantOffline: " + isCausedByParticipantOffline);
            mCallingViewModel.playCallOnOrOffTone(true);
            CallNotificationHelper.cancelInComingCallNotification(getContext());
            unregisterAllBroadCastReceivers();
            resetCallViewData();
            checkToOneToOneEndCallMessage(callType, channelId, recipient, isMissedCall, calledDuration);
            mCallingViewModel.onDestroy();
            if (mListener != null) {
                mListener.onCallEnded();
            }
        }
    }

    /**
     * Will manage to send for only one to one call and only caller that will dispatch this message.
     */
    private void checkToOneToOneEndCallMessage(AbsCallService.CallType callType,
                                               String channelId,
                                               User recipient,
                                               boolean isMissedCall,
                                               int calledDuration) {
        if (!mCallingViewModel.isGroupCall() && mCallingViewModel.isCurrentUserCaller() && isAbleToSendMessage()) {
            Timber.i("checkToOneToOneEndCallMessage");
            mCallServiceBinder.getChatBinder().sendMessage(recipient,
                    ChatHelper.buildOneToOneCallEventMessage(getContext(),
                            callType,
                            recipient,
                            isMissedCall,
                            calledDuration));
            //TODO: Need to check need to broadcast adding new message to chat screen
        }
    }

    private void unregisterAllBroadCastReceivers() {
        try {
            getContext().unregisterReceiver(mNetworkBroadcastReceiver);
            getContext().unregisterReceiver(mBluetoothConnectionStateChangeReceiver);
            getContext().unregisterReceiver(mJoinGroupCallStatusReceiver);
        } catch (Exception ex) {
            Timber.e(ex);
            SentryHelper.logSentryError(ex);
        }
    }

    public void checkToStopIncomingToneWhenDeviceButtonPressed() {
        mCallingViewModel.checkToStopIncomingToneWhenDeviceButtonPressed();
    }

    public void performAnswerCall() {
        mCallingViewModel.onAnswerClick(null);
    }

    public void handleNotificationAction(AbsCallService.CallNotificationActionType type, String groupId) {
        mCallingViewModel.handleNotificationAction(type, groupId);
    }

    public RecyclerView getCallContainerView() {
        return getBinding().callViewContainer.getCallContainerView();
    }

    public boolean isShowingIncomingCall() {
        return mCallingViewModel.isShowingIncomingCall();
    }

    private void onNetworkStateChange(NetworkBroadcastReceiver.NetworkState networkState) {
        if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
            ToastUtil.showToast(getContext(),
                    getContext().getString(R.string.error_internet_connection_description),
                    true);
            mCallingViewModel.getHandler().postDelayed(() -> {
                        Toast toast = ToastUtil.buildToast(getContext(),
                                getContext().getString(R.string.call_screen_connect_label),
                                true);
                        getBinding().toastRootView.addView(toast.getView());
                    },
                    2000);
        } else {
            getBinding().toastRootView.removeAllViews();
        }
    }

    /**
     * This method must be called in onViewCreated of this fragment to inform all subscribers that
     * the screen of call action has been opened.
     */
    private void broadcastInActionCallEvent(boolean isOpen) {
        Intent intent = new Intent(IN_ACTION_CALL_EVENT);
        intent.putExtra(IS_OPEN, isOpen);
        SendBroadCastHelper.verifyAndSendBroadCast(getContext(), intent);
    }

    private void resetCallViewData() {
        mCallingViewModel.getHandler().post(() -> getBinding().callViewContainer.resetCallViewAndDataState());
    }

    @Override
    public void onMessageClicked(ChatIntent chatIntent) {
        if (mListener != null) {
            mListener.onMessageClicked(chatIntent);
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        Timber.i("onInterceptTouchEvent: " + ev.getAction());
        /*
        To handle the movable floating call window, we will consume the touch event in the parent if
        the call is in floating mode.
         */
        return mCallServiceBinder.isCallViewInFloatingMode();
    }

    @Override
    public void onCallEstablished(AbsCallService.CallType callType, String channelId, boolean isGroup) {
        Timber.i("onCallEstablished: callType: " + callType + ", channelId: " + channelId + ", isGroup: " + isGroup);
    }

    @Override
    public void onResizeScreenBackToFullAction() {
        if (mListener != null) {
            mListener.onResizeScreenBackToFullAction();
        }
    }

    public void endCallManually() {
        mCallingViewModel.performHangUp(true);
    }

    public void onFloatingWindowEnable(boolean isEnable) {
        mCallingViewModel.onFloatingWindowEnable(isEnable);
    }

    public void adaptViewOnFloating() {
//        mCallingViewModel.onFloatingWindowEnable(true);
        this.post(() -> {
            getBinding().callViewContainer.adaptViewOnFloating();
            int adaptSize = getContext().getResources().getDimensionPixelSize(R.dimen.main_call_profile_size) / 2;
            mBinding.mainCallProfile.getLayoutParams().width = adaptSize;
            mBinding.mainCallProfile.getLayoutParams().height = adaptSize;
            mBinding.mainCallProfile.requestLayout();
        });
    }

    public void adaptViewBackToFullScreen() {
//        mCallingViewModel.onFloatingWindowEnable(false);
        this.post(() -> {
            getBinding().callViewContainer.adaptViewBackToFullScreen();
            int adaptSize = getContext().getResources().getDimensionPixelSize(R.dimen.main_call_profile_size);
            mBinding.mainCallProfile.getLayoutParams().width = adaptSize;
            mBinding.mainCallProfile.getLayoutParams().height = adaptSize;
            mBinding.mainCallProfile.requestLayout();
        });
    }

    public interface CallScreenViewListener {
        void onCallEnded();

        void onCallChannelIdIdGenerated(String channelId);

        void onMessageClicked(ChatIntent chatIntent);

        default void onResizeScreenBackToFullAction() {
        }
    }
}
