package com.proapp.sompom.widget.chat;

import android.app.Activity;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.LayoutChatMediaFileBinding;
import com.proapp.sompom.model.result.Chat;

/**
 * Created by He Rotha on 2019-08-16.
 */
public class ChatMediaFileLayout extends LinearLayout {
    private LayoutChatMediaFileBinding mBinding;
    private Chat mChat;
    private Activity mActivity;
    private boolean mIsMe;
    private String ext = "";
    private String file;
    public static final  String doc = "doc";
    public static final  String docx = "docx";
    public static final  String pdf = "pdf";
    public static final  String ppt = "ppt";
    public static final  String pptx = "pptx";
    public static final  String xls= "xls";
    public static final  String xlsx= "xlsx";



    public void setActivity(Activity activity) {
        mActivity = activity;
    }

    public String getFile() {
        return file;
    }

    public LayoutChatMediaFileBinding getBinding() {
        return mBinding;
    }

    public void setFile(String file) {
        this.file = file;
    }

    private void init() {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_chat_media_file,
                this,
                true);
        mBinding.textViewFile.setText(file);
    }



    public void setChat(Chat chat, boolean isMe) {
        mChat = chat;
        mIsMe = isMe;

        if(mIsMe){
            updateColorResource(R.color.white,
                    R.drawable.layout_chat_me_single);
        }
        else {
            updateColorResource(R.color.black,
                    R.drawable.layout_chat_you_single);
        }
    }

    private void updateColorResource(@ColorRes int textColor,
                                     @DrawableRes int indicatorBg) {
        mBinding.textViewFile.setTextColor(ContextCompat.getColor(mActivity, textColor));
        mBinding.layoutFile.setBackground(ContextCompat.getDrawable(mActivity, indicatorBg));
    }


    public ChatMediaFileLayout(Context context) {
        super(context);
        init();
    }

    public ChatMediaFileLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChatMediaFileLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ChatMediaFileLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }
    public void getFileName(){
        if (file.startsWith("http://")||file.startsWith("https://")){
            int dotposition= file.indexOf("_");
            file = file.substring(dotposition+1);
            for (int i = 0 ; i < 2 ; i++){
                dotposition = file.indexOf("_");
                file = file.substring(dotposition+1);
            }



        }
        else {
            int dotposition = file.lastIndexOf("/");
            file = file.substring(dotposition + 1);
        }
    }

    public void checkExtensionFile(){
        if(file == null){
            mBinding.imageViewIconFile.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.file_reciver));
        }
        else {
            int dotposition= file.lastIndexOf(".");
            ext = file.substring(dotposition + 1, file.length());
        }
        if (mIsMe){
            if (ext.equals(doc)||ext.equals(docx)) {
                mBinding.imageViewIconFile.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.word_fiel_reciver));
            }
            else if (ext.equals(pdf)){
                mBinding.imageViewIconFile.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.pdf_file_reciver));
            }
            else if (ext.equals(ppt)||ext.equals(pptx)){
                mBinding.imageViewIconFile.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.power_point_file_reciver));
            }
            else if (ext.equals(xls)||ext.equals(xlsx)) {
                mBinding.imageViewIconFile.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.excel_file_reciver));
            }
            else{
                mBinding.imageViewIconFile.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.file_reciver));
            }

        }
        else {
            if (ext.equals(doc)||ext.equals(docx)) {
                mBinding.imageViewIconFile.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.word_fiel));
            }
            else if (ext.equals(pdf)){
                mBinding.imageViewIconFile.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.pdf_file));
            }
            else if (ext.equals(pptx)||ext.equals(ppt)){
                mBinding.imageViewIconFile.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.power_point_file));
            }
            else if (ext.equals(xlsx)||ext.equals(xls)){
                mBinding.imageViewIconFile.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.excel_file));
            }
            else{
                mBinding.imageViewIconFile.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.file));
            }
        }

    }
}
