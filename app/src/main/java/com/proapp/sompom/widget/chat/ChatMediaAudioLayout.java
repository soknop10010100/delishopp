package com.proapp.sompom.widget.chat;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.chat.PlayAudioService;
import com.proapp.sompom.databinding.LayoutChatMediaAudioBinding;
import com.proapp.sompom.listener.PlayAudioServiceListener;
import com.proapp.sompom.model.emun.ChatBg;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.AbsSoundControllerActivity;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.DateTimeUtils;
import com.proapp.sompom.utils.ToastUtil;
import com.resourcemanager.utils.ViewUtils;

/**
 * Created by nuonveyo on 9/19/18.
 */

public class ChatMediaAudioLayout extends LinearLayout {
    private LayoutChatMediaAudioBinding mBinding;
    private PlayAudioService mPlayAudioService;
    private Chat mChat;
    private IconType mIconType;
    private long mDuration;
    private Activity mActivity;
    private boolean mIsMe;
    private final PlayAudioServiceListener mPlayAudioServiceListener = new PlayAudioServiceListener() {

        @Override
        public void onPause(String currentId) {
            mIconType = IconType.PAUSE;
            setIcon();
        }

        @Override
        public void onPlaying(String currentId, long currentDuration) {
            if (isLoading()) {
                setShowLoading(false);
            }
            if (mIconType == IconType.PAUSE) {
                mIconType = IconType.PLAYING;
                setIcon();
            }
            long playingDuration = mDuration - currentDuration;
            mBinding.textViewDuration.setText(DateTimeUtils.getAudioTime(playingDuration));
            mBinding.progressBar.setProgress((int) currentDuration);
        }

        @Override
        public void onFinish() {
            removeAudioPlayListener();
            if (isLoading()) {
                setShowLoading(false);
            }
            mBinding.textViewDuration.setText(DateTimeUtils.getAudioTime(mDuration));
            mIconType = IconType.PAUSE;
            ValueAnimator valueAnimator = ValueAnimator.ofInt(mBinding.progressBar.getProgress(), 0);
            valueAnimator.setDuration(300);
            valueAnimator.addUpdateListener(valueAnimator1 -> mBinding.progressBar.setProgress((Integer) valueAnimator1.getAnimatedValue()));
            valueAnimator.start();
            setIcon();
        }

        @Override
        public void onResume(String currentId) {
            mIconType = IconType.PLAYING;
            setIcon();
        }

        @Override
        public void onError() {
            ToastUtil.showToast(mActivity,
                    mActivity.getString(R.string.chat_toast_cant_play_audio),
                    true);
            setShowLoading(false);
        }

        @Override
        public void hideLoading(String currentId) {
            if (TextUtils.equals(currentId, mChat.getId())) {
                setShowLoading(false);
            }
        }
    };
    private com.proapp.sompom.listener.OnClickListener mOnLongClickListener;

    public ChatMediaAudioLayout(Context context) {
        super(context);
        init();
    }

    public ChatMediaAudioLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChatMediaAudioLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ChatMediaAudioLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void setActivity(Activity activity) {
        mActivity = activity;
    }

    public void setChat(Chat chat, ChatBg chatBg, boolean isMe, com.proapp.sompom.listener.OnClickListener listener) {
        mChat = chat;
        mIsMe = isMe;
        mDuration = chat.getMediaList().get(0).getDuration();
        mOnLongClickListener = listener;

        mIconType = IconType.PAUSE;
        mBinding.textViewDuration.setText(DateTimeUtils.getAudioTime(mDuration));
        mBinding.progressBar.setMax((int) mDuration);

        setBackgroundVoice(chatBg);
        if (mIsMe) {
            setLoadingColor(AttributeConverter.convertAttrToColor(getContext(), R.attr.chat_me_voice_loading));
            updateColorResource(R.drawable.chat_me_voice_progress_background,
                    AttributeConverter.convertAttrToColor(getContext(), R.attr.chat_me_voice_duration),
                    R.drawable.chat_me_voice_duration_background,
                    R.drawable.chat_me_voice_play_background,
                    AttributeConverter.convertAttrToColor(getContext(), R.attr.chat_me_voice_unprogress_background));
        } else {
            setLoadingColor(AttributeConverter.convertAttrToColor(getContext(), R.attr.chat_recipient_voice_loading));
            updateColorResource(R.drawable.chat_recipient_voice_progress_background,
                    AttributeConverter.convertAttrToColor(getContext(), R.attr.chat_recipient_voice_duration),
                    R.drawable.chat_recipient_voice_duration_background,
                    R.drawable.chat_recipient_voice_play_background,
                    AttributeConverter.convertAttrToColor(getContext(), R.attr.chat_recipient_voice_unprogress_background));
        }

        /*
          Remove and than set listener again when this item is recycled while scrolling,
          that be able to set progress and duration working properly.
         */
        if (mActivity instanceof AbsBaseActivity) {
            mPlayAudioService = getPlayAudioService();
            if (mPlayAudioService != null
                    && (mPlayAudioService.isPlaying() || mPlayAudioService.isPause())
                    && TextUtils.equals(mChat.getId(), mPlayAudioService.getCurrentAudioId())) {

                if (mPlayAudioService.isPause() && mPlayAudioService.getCurrentPauseDuration() > 0) {
                    long playingDuration = mDuration - mPlayAudioService.getCurrentPauseDuration();
                    mBinding.textViewDuration.setText(DateTimeUtils.getAudioTime(playingDuration));
                    mBinding.progressBar.setProgress((int) mPlayAudioService.getCurrentPauseDuration());
                }

                removeAudioPlayListener();
                mPlayAudioService = getPlayAudioService();
                if (mPlayAudioService != null) {
                    ((AbsSoundControllerActivity) mActivity).setPlayAudioServiceListener(mPlayAudioServiceListener);
                }
            }
        }
    }

    private void updateColorResource(@DrawableRes int drawable,
                                     int durationTextColor,
                                     @DrawableRes int durationBg,
                                     @DrawableRes int playBg,
                                     int indicatorBg) {
        mBinding.progressBar.setProgressDrawable(ContextCompat.getDrawable(mActivity, drawable));
        mBinding.textViewDuration.setTextColor(durationTextColor);
        mBinding.textViewDuration.setBackground(ContextCompat.getDrawable(mActivity, durationBg));
        mBinding.containerPlayButton.setBackground(ContextCompat.getDrawable(mActivity, playBg));
        mBinding.imageViewIcon.setImageDrawable(mIconType.getIcon(mActivity, mIsMe));
        mBinding.backgroundOfLine.setBackgroundColor(indicatorBg);
    }

    private void setBackgroundVoice(ChatBg chatBg) {
        if (chatBg == null) {
            return;
        }
        Drawable drawable = ContextCompat.getDrawable(getContext(), chatBg.getDrawable(false));
        if (drawable != null) {
            drawable = ViewUtils.setColorFilter(drawable, AttributeConverter.convertAttrToColor(getContext(),
                    mIsMe ? R.attr.chat_me_voice_background : R.attr.chat_recipient_voice_background));
            mBinding.layoutRoot.setBackground(drawable);
        }
    }

    private void setLoadingColor(int tintColor) {
        Drawable indeterminateDrawable = mBinding.loading.getIndeterminateDrawable();
        indeterminateDrawable = ViewUtils.setColorFilter(indeterminateDrawable, tintColor);
        mBinding.loading.setIndeterminateDrawable(indeterminateDrawable);
    }

    private void init() {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_chat_media_audio,
                this,
                true);
        mBinding.getRoot().setOnClickListener(v -> {
            if (mActivity instanceof AbsBaseActivity) {
                mPlayAudioService = getPlayAudioService();
                if (mPlayAudioService != null) {
                    // Click on current playing item
                    if (TextUtils.equals(mChat.getId(), mPlayAudioService.getCurrentAudioId())) {
                        if (mPlayAudioService.isPlaying()) {
                            mPlayAudioService.pause();
                        } else if (mPlayAudioService.isPause()) {
                            mPlayAudioService.resume();
                        } else {
                            mPlayAudioService.checkToHideLoading();
                            setShowLoading(true);
                            ((AbsSoundControllerActivity) mActivity).setPlayAudioServiceListener(mPlayAudioServiceListener);
                            PlayAudioService.startService(mActivity, mChat, true);
                        }
                    } else {
                        mPlayAudioService.checkToHideLoading();
                        setShowLoading(true);
                        if (mPlayAudioService.isPlaying() || mPlayAudioService.isPause()) {
                            mPlayAudioService.stop();
                        }
                        ((AbsSoundControllerActivity) mActivity).setPlayAudioServiceListener(mPlayAudioServiceListener);
                        PlayAudioService.startService(mActivity, mChat, true);
                    }
                }
            }
        });

        mBinding.getRoot().setOnLongClickListener(v -> {
            if (mOnLongClickListener != null) {
                if (mPlayAudioService != null && mPlayAudioService.isPlaying()) {
                    mPlayAudioService.pause();
                }
                mOnLongClickListener.onClick();
            }
            return true;
        });
    }

    private void setIcon() {
        mBinding.imageViewIcon.setImageDrawable(mIconType.getIcon(mActivity, mIsMe));
    }

    private void removeAudioPlayListener() {
        if (mActivity instanceof AbsBaseActivity) {
            mPlayAudioService = null;
            ((AbsSoundControllerActivity) mActivity).setPlayAudioServiceListener(null);
        }
    }

    private PlayAudioService getPlayAudioService() {
        return ((AbsSoundControllerActivity) mActivity).getPlayAudioService();
    }

    private void setShowLoading(boolean isShow) {
        if (isShow) {
            mBinding.getRoot().setClickable(false);
            mBinding.imageViewIcon.setVisibility(GONE);
            mBinding.loading.setVisibility(VISIBLE);
        } else {
            mBinding.getRoot().setClickable(true);
            mBinding.loading.setVisibility(GONE);
            mBinding.imageViewIcon.setVisibility(VISIBLE);
        }
    }

    private boolean isLoading() {
        return mBinding.loading.getVisibility() == VISIBLE;
    }

    public enum IconType {

        PLAYING(R.drawable.ic_pause_green_dark),
        PAUSE(R.drawable.ic_right_arrow_small_green);

        @DrawableRes
        private final int mIcon;

        IconType(int icon) {
            mIcon = icon;
        }

        public int getIconResource() {
            return mIcon;
        }

        public Drawable getIcon(Context context, boolean isMe) {
            Drawable drawable = ContextCompat.getDrawable(context, getIconResource());
            if (drawable != null) {
                if (isMe) {
                    drawable = ViewUtils.setColorFilter(drawable, AttributeConverter.convertAttrToColor(context, R.attr.chat_me_voice_play_icon));
                } else {
                    drawable = ViewUtils.setColorFilter(drawable, AttributeConverter.convertAttrToColor(context, R.attr.chat_recipient_voice_play_icon));
                }
            }

            return drawable;
        }
    }
}
