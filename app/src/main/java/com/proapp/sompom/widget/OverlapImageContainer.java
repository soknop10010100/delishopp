package com.proapp.sompom.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.LayoutOverlapImageContainerBinding;
import com.proapp.sompom.model.SupportingResource;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.viewmodel.OverlapImageContainerViewModel;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by nuonveyo on 7/13/18.
 */

public class OverlapImageContainer extends LinearLayout {

    public static final int MAX_DISPLAY_COUNT = 5;

    protected ViewGroup[] mViewGroups;
    protected ImageProfileLayout[] mImageViews;
    protected int mLimit;
    protected OverlapImageContainerViewModel mViewModel = new OverlapImageContainerViewModel();
    private LayoutOverlapImageContainerBinding mBinding;

    public LayoutOverlapImageContainerBinding getBinding() {
        return mBinding;
    }

    public OverlapImageContainer(Context context) {
        super(context);
        init(null);
    }

    public OverlapImageContainer(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public OverlapImageContainer(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public OverlapImageContainer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    protected void init(AttributeSet attributeSet) {
        setOrientation(HORIZONTAL);
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_overlap_image_container,
                this,
                true);
        mBinding.setVariable(BR.viewModel, mViewModel);
        if (attributeSet != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attributeSet,
                    R.styleable.OverlapImageContainer,
                    0,
                    0);
            mLimit = typedArray.getInteger(R.styleable.OverlapImageContainer_limit, MAX_DISPLAY_COUNT);
            if (mLimit > MAX_DISPLAY_COUNT) {
                mLimit = MAX_DISPLAY_COUNT;
            }
            typedArray.recycle();
        }

        mViewGroups = new ViewGroup[]{
                mBinding.container1,
                mBinding.container2,
                mBinding.container3,
                mBinding.container4,
                mBinding.container5
        };
        mImageViews = new ImageProfileLayout[]{
                mBinding.imageView1,
                mBinding.imageView2,
                mBinding.imageView3,
                mBinding.imageView4,
                mBinding.imageView5
        };
    }

    public void addItem(long totalViewCount, List<User> list) {
        mBinding.moreTextView.setVisibility(GONE);
        if (list != null && !list.isEmpty()) {
            int moreViewer = (int) (totalViewCount - list.size());
            Timber.i("AddItem with user list: " + list.size() + ", moreViewer: " + moreViewer);
            setVisibility(VISIBLE);
            for (int i = 0; i < list.size(); i++) {
                if (i + 1 == mLimit && moreViewer > 0) {
                    Timber.i("More people" + moreViewer);
                    mBinding.moreTextView.setText("+" + moreViewer);
                    mBinding.moreTextView.setVisibility(VISIBLE);
                    break;
                } else if (i < mImageViews.length) {
                    mImageViews[i].setUser(list.get(i), true);
                    mViewGroups[i].setVisibility(VISIBLE);
                }
            }

            //To hide the view that do not show any avatar
            if (list.size() < mImageViews.length) {
                for (int i = list.size(); i < mImageViews.length; i++) {
                    mViewGroups[i].setVisibility(GONE);
                }
            }
        } else {
            setVisibility(GONE);
        }
    }

    protected void validateViewerList(List<User> userList) {
        List<User> userList1 = new ArrayList<>();
        if (userList != null && !userList.isEmpty()) {
            for (User user : userList) {
                if (!userList1.contains(user)) {
                    userList1.add(user);
                }
            }

            userList.clear();
            userList.addAll(userList1);
        }
    }

    public void addBitMapItem(List<SupportingResource> list, int totalImage) {
        mBinding.moreTextView.setVisibility(GONE);

        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < mImageViews.length; i++) {
                if (i >= list.size() && totalImage < mLimit) {
                    mViewGroups[i].setVisibility(GONE);
                } else {
                    if (i >= mLimit && totalImage > mLimit + 1) {

                        String more = "+" + (totalImage - mLimit);
                        Timber.i("More people" + more);
                        mBinding.moreTextView.setText(more);
                        mBinding.moreTextView.setVisibility(VISIBLE);
                        mImageViews[i].setVisibility(GONE);
                    } else {

                        if (list.get(i).getBitmap() == null) {
                            mImageViews[i].setImageDrawable(list.get(i).getDrawable());
                        } else {
                            mImageViews[i].setImageBitmap(list.get(i).getBitmap());
                        }
                        mImageViews[i].setVisibility(VISIBLE);
                        mViewGroups[i].setVisibility(VISIBLE);
                    }
                }
            }
        }
    }

    @BindingAdapter("setCustomImageViewBorder")
    public static void setCustomImageViewBorder(View imageViewContainer, Drawable imageBorder) {
        if (imageBorder != null) {
            imageViewContainer.setBackground(imageBorder);
        }
    }

    @BindingAdapter({"setDisplayViewerList", "setTotalViewerCounter"})
    public static void setDisplayViewerList(OverlapImageContainer view, List<User> viewerList, long totalViewerCounter) {
        view.addItem(totalViewerCounter, viewerList);
    }
}
