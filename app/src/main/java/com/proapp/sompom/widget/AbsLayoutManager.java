package com.proapp.sompom.widget;

/**
 * Created by Chhom Veasna on 4/27/22.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface AbsLayoutManager {

    void checkToPerformLoadMore();

    void checkLToInvokeReachLoadMoreByDefaultIfNecessary();

    boolean isReachedLastItem();

    void setLoadMoreLinearLayoutManagerListener(final AbsLayoutManagerListener onLoadMoreListener);

    void loadingFinished();

    void setShouldDetectLoadMore(boolean shouldDetectLoadMore);

    interface AbsLayoutManagerListener {
        //When user scroll bottom to reach load more layout
        void onReachedLoadMoreBottom();

        //When load more layout appear by layout change which happen when item has been removed
        void onReachedLoadMoreByDefault();
    }
}
