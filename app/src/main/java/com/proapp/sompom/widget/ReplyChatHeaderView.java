package com.proapp.sompom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.ChatReferenceWrapper;
import com.proapp.sompom.model.result.ReferencedChat;
import com.proapp.sompom.viewmodel.newviewmodel.AbsReplyMessageItemTypeViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ReplyMessageTypeMediaViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ReplyMessageTypeTextViewModel;

public class ReplyChatHeaderView extends LinearLayout {

    private Chat mRepliedChat;

    public ReplyChatHeaderView(Context context) {
        super(context);
    }

    public ReplyChatHeaderView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ReplyChatHeaderView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ReplyChatHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public String getReplyingChatId() {
        if (mRepliedChat != null) {
            return mRepliedChat.getId();
        }

        return null;
    }

    public void buildReplayView(Chat replyChat,
                                Chat mainChat,
                                boolean isInInputReplyMode,
                                boolean isCurrentUserSender,
                                AbsReplyMessageItemTypeViewModel.ReplyMessageItemTypeViewModelListener listener) {
        mRepliedChat = replyChat;
        removeAllViews();
        ViewDataBinding viewDataBinding;
        if (isConsiderAsReplyAsTextLayout(replyChat)) {
            viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                    R.layout.reply_message_type_text_layout,
                    this,
                    true);
            ReplyMessageTypeTextViewModel viewModel = new ReplyMessageTypeTextViewModel(getContext(),
                    replyChat,
                    isInInputReplyMode,
                    isCurrentUserSender,
                    mainChat);
            viewModel.setListener(listener);
            viewDataBinding.setVariable(BR.viewModel, viewModel);
        } else {
            viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                    R.layout.reply_message_type_media_layout,
                    this,
                    true);
            ReplyMessageTypeMediaViewModel viewModel = new ReplyMessageTypeMediaViewModel(getContext(),
                    replyChat,
                    isInInputReplyMode,
                    isCurrentUserSender,
                    mainChat);
            viewModel.setListener(listener);
            viewDataBinding.setVariable(BR.viewModel, viewModel);
        }
    }

    private boolean isConsiderAsReplyAsTextLayout(Chat replyChat) {
        return replyChat.getType() == Chat.Type.AUDIO ||
                replyChat.getType() == Chat.Type.FILE ||
                replyChat.getType() == Chat.Type.TEXT;
    }

    public ChatReferenceWrapper getRepliedChat() {
        if (mRepliedChat != null) {
            ReferencedChat referencedChat = new ReferencedChat();
            referencedChat.setId(mRepliedChat.getId());
            referencedChat.setContent(mRepliedChat.getContent());
            referencedChat.setDate(mRepliedChat.getDate());
            referencedChat.setSender(mRepliedChat.getSender());
            referencedChat.setSenderId(mRepliedChat.getSenderId());
            if (mRepliedChat.getMediaList() != null && !mRepliedChat.getMediaList().isEmpty()) {
                referencedChat.setMedia(mRepliedChat.getMediaList().get(0));
            }

            return new ChatReferenceWrapper(referencedChat);
        }

        return null;
    }
}
