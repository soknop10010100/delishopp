package com.proapp.sompom.widget;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Chhom Veasna on 4/27/22.
 */
public class ConciergeLoadMoreGridLayoutManager extends GridLayoutManager implements AbsLayoutManager {

    private static final String TAG = ConciergeLoadMoreGridLayoutManager.class.getName();

    private boolean mIsLoadingMore;
    private boolean mShouldDetectLoadMore = true;
    private AbsLayoutManagerListener mOnLoadMoreListener;
    private boolean mSupportsPredictiveItemAnimations = true;

    public ConciergeLoadMoreGridLayoutManager(Context context,
                                              RecyclerView recyclerView,
                                              boolean supportsPredictiveItemAnimations,
                                              int spanCount) {
        super(context, spanCount);
        mSupportsPredictiveItemAnimations = supportsPredictiveItemAnimations;
        /*
        Manage to check if we have to force checking the load more possibility for sometimes the
        load more view display on screen but the logic in scrollVerticallyBy() does not work as expected.
         */
        if (recyclerView != null) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if (newState == RecyclerView.SCROLL_STATE_IDLE && !mIsLoadingMore) {
                        checkToPerformLoadMore();
                    }
                }
            });
        }
    }

    public ConciergeLoadMoreGridLayoutManager(Context context, RecyclerView recyclerView, int spanCount) {
        super(context, spanCount);
        /*
        Manage to check if we have to force checking the load more possibility for sometimes the
        load more view display on screen but the logic in scrollVerticallyBy() does not work as expected.
         */
        if (recyclerView != null) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if (newState == RecyclerView.SCROLL_STATE_IDLE && !mIsLoadingMore) {
                        checkToPerformLoadMore();
                    }
                }
            });
        }
    }

    /**
     * Disable predictive animations. There is a bug in RecyclerView which causes views that
     * are being reloaded to pull invalid ViewHolders from the internal recycler stack if the
     * adapter size has decreased since the ViewHolder was recycled.
     */
    @Override
    public boolean supportsPredictiveItemAnimations() {
        return mSupportsPredictiveItemAnimations;
    }

    @Override
    public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler, RecyclerView.State state) {
        checkToPerformLoadMore();
        return super.scrollVerticallyBy(dy, recycler, state);
    }

    @Override
    public void checkToPerformLoadMore() {
        if (mShouldDetectLoadMore) {
//            Timber.i("scrollVerticallyBy: " + mIsLoadingMore);
            if (mOnLoadMoreListener != null) {
                if (!mIsLoadingMore) {
                    if (isReachedLastItem()) {
                        mIsLoadingMore = true;
                        mOnLoadMoreListener.onReachedLoadMoreBottom();
                    }
                }
            }
        }
    }

    @Override
    public void checkLToInvokeReachLoadMoreByDefaultIfNecessary() {
        if (mShouldDetectLoadMore) {
//            Log.i(TAG, "checkLToInvokeReachLoadMoreByDefaultIfNecessary: mIsLoadingMore ==> " + mIsLoadingMore);
            if (isReachedLastItem() && !mIsLoadingMore) {
                Log.i(TAG, "onReachedLoadMoreByDefault");
                mIsLoadingMore = true;
                if (mOnLoadMoreListener != null) {
                    Log.i(TAG, "mOnLoadMoreListener d" + mOnLoadMoreListener);
                    mOnLoadMoreListener.onReachedLoadMoreByDefault();
                }
            }
        }
    }

    @Override
    public boolean isReachedLastItem() {
//        Log.i(TAG, "findFirstVisibleItemPosition: " + (findFirstVisibleItemPosition() + getChildCount()));
//        Log.i(TAG, "getChildCount: " + getChildCount());
//        Log.i(TAG, "getItemCount: " + getItemCount());
        return (findFirstVisibleItemPosition() + getChildCount()) >= getItemCount() - 1;
    }

    @Override
    public void setLoadMoreLinearLayoutManagerListener(final AbsLayoutManagerListener onLoadMoreListener) {
        mOnLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public void loadingFinished() {
        mIsLoadingMore = false;
    }

    @Override
    public void setShouldDetectLoadMore(boolean shouldDetectLoadMore) {
        mShouldDetectLoadMore = shouldDetectLoadMore;
    }
}
