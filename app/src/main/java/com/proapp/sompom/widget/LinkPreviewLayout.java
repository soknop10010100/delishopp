package com.proapp.sompom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.LayoutLinkPreviewBinding;
import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.model.BasePreviewModel;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.viewmodel.preview.AbsItemLinkPreviewViewModel;
import com.proapp.sompom.viewmodel.preview.ItemDefaultLinkPreviewViewModel;
import com.proapp.sompom.viewmodel.preview.ItemFileLinkPreviewViewModel;
import com.proapp.sompom.viewmodel.preview.ItemVideoLinkPreviewViewModel;
import com.proapp.sompom.widget.lifestream.MediaLayout;

import timber.log.Timber;

/**
 * Created by nuonveyo on 11/2/18.
 */

public class LinkPreviewLayout extends LinearLayout implements AbsItemLinkPreviewViewModel.ItemLinkPreviewViewModelListener {

    private LayoutLinkPreviewBinding mBinding;
    private com.proapp.sompom.listener.OnClickListener mOnCloseLinkPreviewListener;
    private MediaLayout mVideoLayout;
    private boolean isNeeToResumeVideoPlaying;
    private boolean mIsShouldHideCrossButton;

    public LinkPreviewLayout(Context context) {
        super(context);
        init();
    }

    public LinkPreviewLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LinkPreviewLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public LinkPreviewLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void setOnCloseLinkPreviewListener(com.proapp.sompom.listener.OnClickListener onCloseLinkPreviewListener) {
        mOnCloseLinkPreviewListener = onCloseLinkPreviewListener;
    }

    private void init() {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_link_preview,
                this,
                true);
        mBinding.textViewClose.setOnClickListener(v -> {
            if (mOnCloseLinkPreviewListener != null) {
                hideLayout();
                mOnCloseLinkPreviewListener.onClick();
            }
        });
    }

    public void onPause() {
        if (mVideoLayout != null) {
            isNeeToResumeVideoPlaying = mVideoLayout.isVideoPlaying();
            mVideoLayout.onMediaPause();
        }
    }

    public void onResume() {
        if (mVideoLayout != null) {
            if (isNeeToResumeVideoPlaying) {
                mVideoLayout.onMediaPlay(true);
            }
        }
    }

    public void onDestroy() {
        if (mVideoLayout != null) {
            isNeeToResumeVideoPlaying = false;
            mVideoLayout.release();
        }
    }

    public void hideCloseButton() {
        mIsShouldHideCrossButton = true;
        mBinding.textViewClose.setVisibility(GONE);
    }

    public void showLoading(boolean isShow) {
        if (!mIsShouldHideCrossButton) {
            mBinding.textViewClose.setVisibility(isShow ? GONE : VISIBLE);
        }
        mBinding.loading.setVisibility(isShow ? VISIBLE : GONE);
    }

    public void showLayout() {
        mBinding.containerLink.setVisibility(GONE);
        showLoading(true);
        setVisibility(VISIBLE);
    }

    public void hideLayout() {
        mBinding.containerLink.setVisibility(GONE);
        showLoading(false);
        setVisibility(GONE);
    }

    public void showGifLinkFromKeyboard(String link) {
//        checkToShowLinkContent(false);
//        setVisibility(VISIBLE);
//        mBinding.textViewClose.setVisibility(GONE);
//        mBinding.containerLink.setVisibility(VISIBLE);
//        showLoading(true);
//        checkToShowLinkGif(true);
//        GlideApp
//                .with(getContext())
//                .asGif()
//                .load(link)
//                .listener(new RequestListener<GifDrawable>() {
//                    @Override
//                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
//                        showLoading(false);
//                        mBinding.textViewClose.setVisibility(VISIBLE);
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
//                        mBinding.textViewClose.setVisibility(VISIBLE);
//                        showLoading(false);
//                        return false;
//                    }
//                })
//                .into(mBinding.imageViewGifLink);
    }

    public void setLinkPreviewModel(LinkPreviewModel linkPreviewModel) {
        isNeeToResumeVideoPlaying = false;
        showLoading(true);
        BasePreviewModel.PreviewType type = BasePreviewModel.PreviewType.getFromValue(linkPreviewModel.getType());
        Timber.i("setLinkPreviewModel: " + new Gson().toJson(linkPreviewModel));
        mBinding.containerLink.removeAllViews();
        if (type == BasePreviewModel.PreviewType.FILE) {
            if (LinkPreviewRetriever.isImageType(linkPreviewModel.getLink())) {
                ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                        R.layout.item_image_link_preview,
                        mBinding.containerLink,
                        false);
                AbsItemLinkPreviewViewModel viewModel = new ItemDefaultLinkPreviewViewModel(linkPreviewModel,
                        AbsItemLinkPreviewViewModel.PreviewContext.CREATE_FEED);
                addPreviewLayout(viewDataBinding, viewModel);
            } else if (LinkPreviewRetriever.isVideoType(linkPreviewModel.getLink())) {
                ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                        R.layout.item_video_link_preview,
                        mBinding.containerLink,
                        false);
                AbsItemLinkPreviewViewModel viewModel = new ItemVideoLinkPreviewViewModel(linkPreviewModel,
                        AbsItemLinkPreviewViewModel.PreviewContext.CREATE_FEED);
                addPreviewLayout(viewDataBinding, viewModel);
            } else {
                ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                        R.layout.list_item_file_link_preview,
                        mBinding.containerLink,
                        false);
                AbsItemLinkPreviewViewModel viewModel = new ItemFileLinkPreviewViewModel(linkPreviewModel,
                        AbsItemLinkPreviewViewModel.PreviewContext.CREATE_FEED,
                        null);
                addPreviewLayout(viewDataBinding, viewModel);
            }
        } else {
            //Link
            ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                    R.layout.item_link_preview,
                    mBinding.containerLink,
                    false);
            AbsItemLinkPreviewViewModel viewModel = new ItemDefaultLinkPreviewViewModel(linkPreviewModel,
                    AbsItemLinkPreviewViewModel.PreviewContext.CREATE_FEED);
            addPreviewLayout(viewDataBinding, viewModel);
        }
        showLoading(false);
        mBinding.containerLink.setVisibility(VISIBLE);
    }

    @Override
    public void updateCrossVisibility(boolean isVisible) {
        mBinding.textViewClose.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBindVideoLayout(MediaLayout videoLayout) {
        mVideoLayout = videoLayout;
    }

    private void addPreviewLayout(ViewDataBinding viewDataBinding, AbsItemLinkPreviewViewModel viewModel) {
        mBinding.containerLink.addView(viewDataBinding.getRoot(), 0);
        viewDataBinding.setVariable(BR.viewModel, viewModel);
        viewModel.setListener(this);
    }
}
