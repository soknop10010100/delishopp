package com.proapp.sompom.widget;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.android.exoplayer2.ui.PlayerView;
import com.proapp.sompom.helper.DelayClickListener;

import timber.log.Timber;

public class CustomExoPlayer extends PlayerView implements DelayClickListener {

    private GestureDetector mGestureDetector;
    private CustomExoPlayerClickListener mExoPlayerClickListener;
    private boolean mIsClickable = true;

    public CustomExoPlayer(Context context) {
        super(context);
        initView();
    }

    public CustomExoPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CustomExoPlayer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    @Override
    public void onDelayClick(@NonNull View view) {
        if (mExoPlayerClickListener != null) {
            mExoPlayerClickListener.onClicked(CustomExoPlayer.this);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        mGestureDetector.onTouchEvent(ev);
        return super.onTouchEvent(ev);
    }

    public void setExoPlayerClickListener(CustomExoPlayerClickListener exoPlayerClickListener) {
        mExoPlayerClickListener = exoPlayerClickListener;
    }

    private void initView() {
        mGestureDetector = new GestureDetector(getContext(), new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                Timber.i("onSingleTapUp");
                if (mIsClickable) {
                    mIsClickable = false;
                    onDelayClick(CustomExoPlayer.this);
                    new Handler().postDelayed(() -> mIsClickable = true, DELAY_DURATION);
                }
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {

            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });
    }

    public interface CustomExoPlayerClickListener {
        void onClicked(View view);
    }
}
