package com.proapp.sompom.widget;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;

import com.google.android.material.tabs.TabLayout;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.LayoutCustomHomeTabBinding;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.listener.OnTabSelectListener;
import com.proapp.sompom.model.result.AppFeature;
import com.proapp.sompom.model.result.UserBadge;
import com.proapp.sompom.newui.fragment.AbsBaseFragment;
import com.proapp.sompom.newui.fragment.HomeFragment;
import com.proapp.sompom.viewmodel.newviewmodel.LayoutCustomHomeTabViewModel;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by He Rotha on 9/20/18.
 */
public class HomeTabLayout extends TabLayout implements OnClickListener {
    private final List<LayoutCustomHomeTabViewModel> mTabViewModels = new ArrayList<>();
    private OnTabLayoutChangeListener mTabLayoutChangeListener;
    private HomeViewPager mHomeViewPager;
    private TabChangeCallback mTabChangeCallback;
    private View mCustomIndicator;
    private int mPreviousTab;
    private Runnable mAnimationRunnable;
//    private ObjectAnimator mAnimator;

    private TranslateAnimation mAnimation;

    private final int mMaxTabBeforeScroll = 4;

    public HomeTabLayout(Context context) {
        super(context);
    }

    public HomeTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HomeTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setCustomIndicator(View view) {
        mCustomIndicator = view;
    }

    public void setTabChangeCallback(TabChangeCallback tabChangeCallback) {
        mTabChangeCallback = tabChangeCallback;
    }

    public void setTabLayoutChangeListener(OnTabLayoutChangeListener tabLayoutChangeListener) {
        mTabLayoutChangeListener = tabLayoutChangeListener;
    }

    public List<LayoutCustomHomeTabViewModel> getTabViewModels() {
        return mTabViewModels;
    }

    public void setupWithViewPager(@Nullable HomeViewPager viewPager) {
        super.setupWithViewPager(viewPager);

        if (viewPager == null) {
            return;
        }

        mHomeViewPager = viewPager;
        mTabViewModels.clear();

        final LayoutInflater inflater = LayoutInflater.from(getContext());
        for (int i = 0; i < viewPager.getNavMenus().size(); i++) {
            final AppFeature.NavigationBarMenu navigationBarMenu = viewPager.getNavMenus().get(i);
            LayoutCustomHomeTabBinding binding = DataBindingUtil.inflate(inflater, R.layout.layout_custom_home_tab, this, false);
            final LayoutCustomHomeTabViewModel viewModel = new LayoutCustomHomeTabViewModel(getContext(), navigationBarMenu);
            binding.setVariable(BR.viewModel, viewModel);
            mTabViewModels.add(viewModel);
            Tab tab = getTabAt(i);
            if (tab != null) {
                // Replace the tab with our own custom tab view
                tab.setCustomView(binding.getRoot());
                // Check if the current tab's screen option is the type to open a new screen
                if (AppFeature.NavMenuScreen.isOpenNewScreenType(navigationBarMenu.getNavMenuScreen())) {
                    // Disable click to block tab's internal behavior
                    tab.view.setClickable(false);
                    // Handle touch listener ourself to determine when to open a new screen
                    tab.view.setOnTouchListener((v, event) -> {
                        // ACTION_UP only occurs when user tap a view, so we check if the event
                        // touch event is ACTION_UP
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            mTabLayoutChangeListener.onPageClick(viewModel.getNavBarMenu());
                        }
                        return true;
                    });
                }
            }
        }

        if (!mTabViewModels.isEmpty()) {
            mTabViewModels.get(viewPager.getCurrentItem()).setIsSelected(true);
            mTabViewModels.get(viewPager.getCurrentItem()).setIsIndicatorVisible(true);
            if (mTabLayoutChangeListener != null) {
                mTabLayoutChangeListener.onPageChanged(mHomeViewPager.getCurrentFragment(), mHomeViewPager.getCurrentItem());
            }

            addOnTabSelectedListener(new OnTabSelectedListener() {
                @Override
                public void onTabSelected(Tab tab) {
                    mTabViewModels.get(tab.getPosition()).setIsSelected(true);
                    AbsBaseFragment fr = mHomeViewPager.getFragmentAt(tab.getPosition());
                    if (fr instanceof OnTabSelectListener) {
                        ((OnTabSelectListener) fr).onTabSelected(true);
                        fr.onResume();
                    }
                    mTabLayoutChangeListener.onPageChanged(fr, tab.getPosition());
                    //Clear the badge for Contact tab
                    if (mTabViewModels.get(tab.getPosition()).getNavBarMenu().getNavMenuScreen() ==
                            AppFeature.NavMenuScreen.Contact) {
                        mTabViewModels.get(tab.getPosition()).setBadgeValue(0);
                    }
                    if (mTabChangeCallback != null) {
                        mTabChangeCallback.onTabChanged(tab);
                    }
                    checkShouldHideOrderBadge(
                            mTabViewModels.get(tab.getPosition()).getNavBarMenu().getNavMenuScreen() == AppFeature.NavMenuScreen.Shop);

                    int previousTab = mPreviousTab;
                    mPreviousTab = tab.getPosition();

                    if (mCustomIndicator != null) {
                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.removeCallbacks(mAnimationRunnable);
                        mAnimationRunnable = () -> {
                            int selectedTab = tab.getPosition();
                            float start = findCentralXPositionOfSelectedTab(getTabAt(previousTab), mCustomIndicator.getWidth());
                            float destination = findCentralXPositionOfSelectedTab(getTabAt(selectedTab), mCustomIndicator.getWidth());
                            performHorizontalTranslationAnimation(mCustomIndicator,
                                    start,
                                    destination,
                                    true,
                                    new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {
                                            // Hide previously selected tab indicator
                                            mCustomIndicator.setAlpha(1f);
                                            mTabViewModels.get(previousTab).setIsIndicatorVisible(false);
                                        }

                                        @Override
                                        public void onAnimationEnd(Animation animation) {
                                            // Show the new selected tab indicator
                                            mTabViewModels.get(selectedTab).setIsIndicatorVisible(true);
                                            mCustomIndicator.setAlpha(0f);
                                        }

                                        @Override
                                        public void onAnimationRepeat(Animation animation) {

                                        }
                                    });
                        };
                        handler.postDelayed(mAnimationRunnable, 200);
                    }
                }

                @Override
                public void onTabUnselected(Tab tab) {
                    AbsBaseFragment fr = mHomeViewPager.getFragmentAt(tab.getPosition());
                    if (fr instanceof OnTabSelectListener) {
                        ((OnTabSelectListener) fr).onTabSelected(false);
                        fr.onPause();
                    }
                    mTabViewModels.get(tab.getPosition()).setIsSelected(false);
                }

                @Override
                public void onTabReselected(Tab tab) {
                    mTabLayoutChangeListener.onPageReselect(mHomeViewPager.getFragmentAt(tab.getPosition()), tab.getPosition());
                    checkShouldHideOrderBadge(
                            mTabViewModels.get(tab.getPosition()).getNavBarMenu().getNavMenuScreen() == AppFeature.NavMenuScreen.Shop);
                }
            });
        }
    }

    public void checkShouldHideOrderBadge(boolean isHide) {
        Timber.i("Should show badge in concierge screen? " + isHide);
        for (LayoutCustomHomeTabViewModel viewModel : mTabViewModels) {
            if (viewModel.getNavBarMenu().getNavMenuScreen() == AppFeature.NavMenuScreen.Shop) {
                viewModel.hideBadgeCount(isHide);
                break;
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        /*
         Here, we're handling the size of each tab item dynamically

         - If the tab count is <= mMaxTabBeforeScroll, each tab width will be: screen width / tab count
         - If the tab count is > mMaxTabBeforeScroll, each tab width will be: screen width / mMaxTabBeforeScroll
         - When tab count is > mMaxTabBeforeScroll, the width of all the tab combine will be greater
         than the tabLayout width, so scroll is automatically handled. Same goes if tab count is less
         than mMaxTabBeforeScroll, it would not be scrollable since all the tab combine would only have
         the same width as the tabLayout, so there will not be any scrolling
         */

        ViewGroup tabLayout = (ViewGroup) getChildAt(0);
        int childCount = tabLayout.getChildCount();
        if (childCount > 0) {
            int widthPixels = MeasureSpec.getSize(widthMeasureSpec);
            int tabMinWidth;
            if (childCount > mMaxTabBeforeScroll) {
                tabMinWidth = widthPixels / mMaxTabBeforeScroll;
            } else {
                tabMinWidth = widthPixels / childCount;
            }
            int remainderPixels = widthPixels % childCount;
            for (int index = 0; index < childCount; index++) {
                if (remainderPixels > 0) {
                    tabLayout.getChildAt(index).setMinimumWidth(tabMinWidth + 1);
                    remainderPixels--;
                } else {
                    tabLayout.getChildAt(index).setMinimumWidth(tabMinWidth);
                }
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setBadgeValue(UserBadge userBadge) {
        if (!mTabViewModels.isEmpty()) {
            for (LayoutCustomHomeTabViewModel tabViewModel : mTabViewModels) {
                if (tabViewModel.getNavBarMenu().getNavMenuButton() == AppFeature.NavMenuButton.Conversation ||
                        tabViewModel.getNavBarMenu().getNavMenuButton() == AppFeature.NavMenuButton.Support) {
                    if (tabViewModel.getBadgeCount().get() > 0 && userBadge.getUnreadMessage() <= 0) {
                        //Need to broadcast that conversation badge has updated to zero.
                        SendBroadCastHelper.verifyAndSendBroadCast(getContext(),
                                new Intent(HomeFragment.ON_CONVERSATION_BADGE_UPDATE_TO_ZERO_EVENT));
                    }
                    tabViewModel.setBadgeValue(userBadge.getUnreadMessage());
                    break;
                } else if (tabViewModel.getNavBarMenu().getNavMenuScreen() == AppFeature.NavMenuScreen.Shop) {
                    tabViewModel.setBadgeValue(userBadge.getConciergeOrderBadge());
                }
            }
        }
    }

    public void addConversationBadge(int value) {
        Timber.e("addConversationBadge " + value);
        if (!mTabViewModels.isEmpty()) {
            for (LayoutCustomHomeTabViewModel tabViewModel : mTabViewModels) {
                if (tabViewModel.getNavBarMenu().getNavMenuScreen() ==
                        AppFeature.NavMenuScreen.Conversation) {
                    tabViewModel.setBadgeValue(tabViewModel.getBadgeCount().get() + value);
                    break;
                }
            }
        }
    }

    @Override
    public void onClick() {
        Timber.e("onUpdate BadgeValue: %s, %s",
                mHomeViewPager.getCurrentFragment(),
                mHomeViewPager.getCurrentItem());
    }

    public interface OnTabLayoutChangeListener {
        void onPageReselect(AbsBaseFragment fragment, int position);

        void onPageChanged(AbsBaseFragment fragment, int position);

        void onPageClick(AppFeature.NavigationBarMenu navigationBarMenu);
    }

    public interface TabChangeCallback {
        void onTabChanged(Tab tab);
    }

    @BindingAdapter("setCustomHomeTabIndicator")
    public static void setCustomHomeTabIndicator(HomeTabLayout homeTabLayout, View customIndicator) {
        // Set the custom indicator for HomeTabLayout
        homeTabLayout.setCustomIndicator(customIndicator);

        //Will select first tab by default
        if (homeTabLayout.getTabCount() > 0 &&
                homeTabLayout.getTabAt(0) != null &&
                homeTabLayout.getTabAt(0).getCustomView() != null) {
            homeTabLayout.getTabAt(0).getCustomView().post(() -> {
                float centerOfTab =
                        findCentralXPositionOfSelectedTab(homeTabLayout.getTabAt(homeTabLayout.getSelectedTabPosition()),
                                customIndicator.getWidth());
                customIndicator.animate().x(centerOfTab).setDuration(0).start();
            });

        }
    }

    private static float findCentralXPositionOfSelectedTab(Tab tab, int customIndicatorWidth) {
        View customView = tab.getCustomView();
        if (customView != null) {
            View customTabSegmentView = customView.findViewById(R.id.icon);
            float xPos = getRealXOrYPosition(customTabSegmentView, true);
            return xPos + ((customTabSegmentView.getWidth() - customIndicatorWidth) / 2);
        }

        return 0f;
    }

    private static float getRealXOrYPosition(View view, boolean isX) {
        int[] locations = new int[2];
        view.getLocationOnScreen(locations);
        return isX ? locations[0] : locations[1];
    }

    private void performHorizontalTranslationAnimation(View view,
                                                       float fromX,
                                                       float desX,
                                                       boolean isAnimate,
                                                       Animation.AnimationListener listener) {
        if (mAnimation != null) {
            mAnimation.cancel();
        }
        mAnimation = new TranslateAnimation(Animation.ABSOLUTE,
                fromX,
                Animation.ABSOLUTE,
                desX,
                Animation.ABSOLUTE,
                0f,
                Animation.ABSOLUTE,
                0f);
        mAnimation.setDuration(isAnimate ? 200 : 0);
        mAnimation.setInterpolator(new FastOutSlowInInterpolator());
        mAnimation.setFillAfter(true);
        mAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setAlpha(1f);
                if (listener != null) {
                    listener.onAnimationStart(animation);
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setAlpha(0f);
                if (listener != null) {
                    listener.onAnimationEnd(animation);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                if (listener != null) {
                    listener.onAnimationRepeat(animation);
                }
            }
        });
        view.clearAnimation();
        view.startAnimation(mAnimation);

//        if (mAnimator != null) {
//            mAnimator.cancel();
//        }
//        mAnimator = ObjectAnimator.ofFloat(view, View.X, desX);
//        mAnimator.setDuration(isAnimate ? 250 : 0);
//        mAnimator.setInterpolator(new FastOutSlowInInterpolator());
//        mAnimator.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//                if (listener != null) {
//                    listener.onAnimationStart(null);
//                }
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                if (listener != null) {
//                    listener.onAnimationEnd(null);
//                }
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//                if (listener != null) {
//                    listener.onAnimationEnd(null);
//                }
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        });
//        mAnimator.start();
    }
}
