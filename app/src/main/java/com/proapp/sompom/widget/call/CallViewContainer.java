package com.proapp.sompom.widget.call;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.call.CallViewAdapter;
import com.proapp.sompom.databinding.CallContainerLayoutBinding;
import com.proapp.sompom.databinding.ListItemCallViewBinding;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.model.call.CallData;
import com.proapp.sompom.model.emun.GeneralCallBehaviorType;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.newviewmodel.ItemCallViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 6/10/2020.
 */
public class CallViewContainer extends RelativeLayout {

    private static final int COL_NUM = 2;
    public static final int FLOATING_SCREEN_FACTOR = 3; //Will take screen divide by 3.

    private CallContainerLayoutBinding mBinding;
    private CallViewAdapter mCallViewAdapter;
    private CallViewContainerListener mListener;
    private List<CallData> mCallDataList = new ArrayList<>();
    private ItemCallViewModel mCurrentSingleUserViewModel;
    private ListItemCallViewBinding mCurrentSingleUserBinding;

    private int mSingleCallViewMarginTop;
    private String mCurrentUserId;
    private boolean mIsCurrentUserRequestingCamera;
    private boolean mDisplayingRequestingVideo;
    private Context mApplicationContext;

    public CallViewContainer(Context context) {
        super(context);
        initView();
    }

    public CallViewContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CallViewContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public CallViewContainer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    public void setApplicationContext(Context applicationContext) {
        mApplicationContext = applicationContext;
    }

    public Context getApplicationContext() {
        return mApplicationContext;
    }

    public boolean isDisplayingRequestingVideo() {
        return mDisplayingRequestingVideo;
    }

    public void resetDisplayingRequestingVideo() {
        mDisplayingRequestingVideo = false;
    }

    public int getJoiningCallViewCount() {
        if (mCallViewAdapter != null) {
            return mCallViewAdapter.getItemCount();
        }

        return 0;
    }

    public void setDimViewVisibility(int callBehaviour) {
        mBinding.dimView.setVisibility((callBehaviour == GeneralCallBehaviorType.SHOWING_REQUEST_CAMERA.ordinal() ||
                callBehaviour == GeneralCallBehaviorType.RECEIVING_CALL.ordinal() ||
                callBehaviour == GeneralCallBehaviorType.MAKING_CALL.ordinal()) ?
                View.VISIBLE : View.GONE);
    }

    public boolean isCurrentUserRequestingCamera() {
        return mIsCurrentUserRequestingCamera;
    }

    public boolean isCurrentUserWaitingOtherToJoinGroupCall() {
        CallData currentUser = findMyUserCallDataInList(mCallDataList);
        return currentUser != null && currentUser.getCallAction() == CallData.CallAction.WAITING_OTHER_TO_JOIN;
    }

    private void initView() {
        mCurrentUserId = SharedPrefUtils.getUserId(getContext());
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.call_container_layout,
                this,
                false);
        addView(mBinding.getRoot());
    }

    public void setListener(CallViewContainerListener listener) {
        mListener = listener;
    }

    public CallViewContainerListener getListener() {
        return mListener;
    }

    public void setCurrentUserProfileMarginTop(int currentUserProfileMarginTop) {
        mSingleCallViewMarginTop = currentUserProfileMarginTop;
        ((LayoutParams) mBinding.singleCallViewContainer.getLayoutParams()).topMargin = currentUserProfileMarginTop;
    }

    public void resetSingleCallViewMarginTop(boolean inHideCallOptionMode) {
        LayoutParams layoutParams = (LayoutParams) mBinding.singleCallViewContainer.getLayoutParams();
        if (inHideCallOptionMode) {
            layoutParams.topMargin = getResources().getDimensionPixelSize(R.dimen.normal_padding);
        } else {
            layoutParams.topMargin = mSingleCallViewMarginTop;
        }
        Timber.i("resetSingleCallViewMarginTop: " + layoutParams.topMargin);
        mBinding.singleCallViewContainer.setLayoutParams(layoutParams);
    }

    private void animateMarginTop(int marginTop) {
        final View animatedView = mBinding.singleCallViewContainer;
        final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) animatedView.getLayoutParams();
        ValueAnimator animator = ValueAnimator.ofInt(params.topMargin, 0);
        animator.addUpdateListener(valueAnimator -> {
            params.bottomMargin = (Integer) valueAnimator.getAnimatedValue();
            animatedView.requestLayout();
        });
        animator.setDuration(300);
        animator.start();

        ObjectAnimator animation = ObjectAnimator.ofFloat(mBinding.singleCallViewContainer,
                "layoutMarginTop", 100f);
        animation.setDuration(1000);
        animation.start();
    }

    private void populateData(List<CallData> callDataList, boolean shouldDisplayMainCallProfile) {
        checkToPutCurrentUserViewOnTopList(callDataList);
        Timber.i("populateData: " + callDataList.size());
        mCallViewAdapter = new CallViewAdapter(new ArrayList<>(callDataList),
                mListener.getJoinedGroupCallParticipantCounter());
        mCallViewAdapter.setListener(new CallViewAdapter.CallViewAdapterListener() {
            @Override
            public int getCallViewHeight() {
                return CallViewContainer.this.getCallViewHeight(mCallViewAdapter.getItemCount());
            }

            @Override
            public void onCalLViewClicked() {
                if (mListener != null) {
                    mListener.onCalLViewClicked();
                }
            }

            @Override
            public boolean isInFloatingMode() {
                if (mListener != null) {
                    return mListener.isInFloatingMode();
                }

                return false;
            }

            @Override
            public Context getApplicationContext() {
                return mApplicationContext;
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), COL_NUM);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (mCallViewAdapter.getItemCount() > 2) {
                    if ((mCallViewAdapter.getItemCount() % 2) != 0 && position == 0) {
                        //Make first row full screen for odd participant list.
                        return COL_NUM;
                    } else {
                        return 1;
                    }
                } else {
                    //Make full row this item.
                    return COL_NUM;
                }
            }
        });
        mBinding.recyclerView.setLayoutManager(gridLayoutManager);
        mBinding.recyclerView.setAdapter(mCallViewAdapter);

        if (mListener != null) {
            mListener.onUpdateMainCallProfileVisibilityChanged(shouldDisplayMainCallProfile,
                    false);
        }

        if (callDataList.size() == 1 && callDataList.get(0).getCallAction() == CallData.CallAction.WAITING_OTHER_TO_JOIN) {
            if (mListener != null) {
                mListener.onWaitingOtherMemberToJoinCall();
            }
        }
    }

    private void checkToPutCurrentUserViewOnTopList(List<CallData> callDataList) {
        if (callDataList.size() > 1) {
            CallData myUserCallDataInList = findMyUserCallDataInList(callDataList);
            if (myUserCallDataInList != null) {
                callDataList.remove(myUserCallDataInList);
                callDataList.add(0, myUserCallDataInList);
            }
        }
    }

    public boolean shouldDisplayCallDuration() {
        return isCallEngaged() && getJoiningCallViewCount() <= 2;
    }

    public boolean isCallEngaged() {
        boolean hasCallEngaged = false;
        for (CallData callData : mCallDataList) {
            if (callData.getCallAction() == CallData.CallAction.ENGAGED_IN_CALL) {
                hasCallEngaged = true;
                break;
            }
        }

        return hasCallEngaged;
    }

    public boolean shouldDisplayMainCallProfile() {
        /*
          If all call participants has no video enable, the should display the main call profile.
         */
        Timber.i("mCallDataList.size: " + mCallDataList.size() + ", All no video: " + isAllParticipantHasNoVideoEnable());
        return mCallDataList.size() == 1 ||
                (mCallDataList.size() == 2 && isAllParticipantHasNoVideoEnable()) ||
                mListener.getCurrentCallBehavior() == GeneralCallBehaviorType.SHOWING_REQUEST_CAMERA;
    }

    public boolean isAllParticipantHasNoVideoEnable() {
        boolean noVideoForAll = true;
        if (mCallViewAdapter != null) {
            List<CallData> userList = mCallViewAdapter.getUserList();
            for (CallData callData : userList) {
                if (callData.isHasVideo()) {
                    noVideoForAll = false;
                    break;
                }
            }

            if (noVideoForAll && userList.size() == 1 && !TextUtils.equals(userList.get(0).getUser().getId(), mCurrentUserId)) {
                /*
                 Need to check the current user data for it might not display in the call view list.
                 If it is there is only two participants in call, the current user view will put at the top
                 of screen and not put into call view list.
                 */
                Timber.i("Check video enable for current user.");
                CallData myCallData = findMyUserCallDataInList(mCallDataList);
                if (myCallData != null) {
                    noVideoForAll = !myCallData.isHasVideo();
                }
            }
        }

        Timber.i("isAllParticipantHasNoVideoEnable: " + noVideoForAll);

        return noVideoForAll;
    }

    private int getCallViewHeight(int totalParticipant) {
        int deviceHeight = getHeight();
//        Timber.i("deviceHeight: " + deviceHeight);
        if (totalParticipant <= 2) {
            return deviceHeight;
        } else {
            //Find row without current user view include
            int rowCount = (totalParticipant - 1) / COL_NUM;
            //Current user view will display as whole row. So add the row for current user view too.
            rowCount++;

            return deviceHeight / rowCount;
        }
    }

    public void resetCurrentUserRequestingCamera() {
        mIsCurrentUserRequestingCamera = false;
    }

    /*
    Current user is requesting video camera from other participants in call process.
     */
    public void onUpdateCallViewOnRequestingCamera(CallData currentUserData, boolean isRequestingCamera) {
        Timber.i("onRequestingCamera: " + isRequestingCamera);
        //Always reset this status when there is a camera request in call
        mIsCurrentUserRequestingCamera = false;
        addCallDataIntoList(Collections.singletonList(currentUserData));
        logCallData("onRequestingCamera", Collections.singletonList(currentUserData));
        if (!mListener.isGroupCall()) {
            if (TextUtils.equals(currentUserData.getUser().getId(), mCurrentUserId)) {
                if (isRequestingCamera) {
                    populateData(Collections.singletonList(currentUserData),
                            true);
                    mIsCurrentUserRequestingCamera = true;
                } else {
                    //Add recipient back
                    populateData(Collections.singletonList(findParticipantOf1x1Call()),
                            true);
                }
            } else {
                //Add recipient back
                populateData(Collections.singletonList(findParticipantOf1x1Call()),
                        true);
            }
        } else {
            mIsCurrentUserRequestingCamera = isRequestingCamera;
            populateData(getValidCallDatToDisplay(), shouldDisplayMainCallProfile());
            if (mListener.isGroupCall() && mCallDataList.size() == 2) {
                addCurrentUserCallView();
            }
        }
    }

    private void addCurrentUserCallView() {
        CallData myCallData = findMyUserCallDataInList(mCallDataList);
        if (myCallData != null && myCallData.getVideoCallView() != null) {
            bindMySingleCallView(myCallData);
        }
    }

    /*
      Current user receiving requesting camera from other participant.
       */
    public void onReceivingCameraRequestStateChanged(CallData participantData) {
        /*
        @param participantData parameter is holding information to whether there is video request or not.
         */
        mDisplayingRequestingVideo = participantData.isHasVideo();
        Timber.i("onReceivingCameraRequestStateChanged.");
        addCallDataIntoList(Collections.singletonList(participantData));
        logCallData("onReceivingCameraRequestStateChanged", Collections.singletonList(participantData));
        if (!mListener.isGroupCall() || mCallDataList.size() <= 2) {
            populateData(Collections.singletonList(participantData), true);
        } else {
            updateReceivingCameraRequestStatus(true, participantData.getUser().getId());
            populateData(getValidCallDatToDisplay(), participantData.isHasVideo());
        }
    }

    /*
    Current user reject requesting camera from other participant.
     */
    public void onRejectRequestingCamera(String requesterId) {
        Timber.i("onRejectRequestingCamera: " + requesterId + ": " + mListener.isVideoCall());
        List<CallData> userList = mCallViewAdapter.getUserList();
        if (!mListener.isGroupCall()) {
            CallData requesterData = findUserCallDataInList(userList, requesterId);
            if (requesterData != null && !mListener.isVideoCall()) {
                //Reset the requester display video if the call is not yet become video call.
                requesterData.setHasVideo(false);
                requesterData.setVideoCall(false);
                requesterData.setVideoCallView(null);
            }
            updateDataState(requesterData);
        }
        if (mListener.isGroupCall()) {
            updateReceivingCameraRequestStatus(false, requesterId);
        }
        populateData(userList, shouldDisplayMainCallProfile());
    }

    public void disableVideoViewForSpecificUser(String requesterId) {
        Timber.i("disableVideoViewForSpecificUser: " + requesterId);
        ItemCallViewModel itemCallViewModel = mCallViewAdapter.getCallViewModelMap().get(requesterId);
        if (itemCallViewModel != null) {
            CallData requesterCalLData = itemCallViewModel.getCallData();
            if (requesterCalLData != null) {
                requesterCalLData.setHasVideo(false);
                requesterCalLData.setVideoCall(false);
                requesterCalLData.setVideoCallView(null);
            }
            updateDataState(requesterCalLData);
        }
    }

    public void onOtherParticipantAcceptVideoRequest(CallData callData) {
        Timber.i("onReceivingCameraRequestStateChanged");
        addCallDataIntoList(Collections.singletonList(callData));
        populateData(mCallViewAdapter.getUserList(), shouldDisplayMainCallProfile());
    }

    public void resetReceivingCameraRequestStatusForGroup() {
        if (mListener.isGroupCall()) {
            Timber.i("resetReceivingCameraRequestStatusForGroup: " + mCallViewAdapter.getCallViewModelMap().size());
            updateReceivingCameraRequestStatus(false, null);
            for (Map.Entry<String, ItemCallViewModel> entry : mCallViewAdapter.getCallViewModelMap().entrySet()) {
                logCallData("Reset", entry.getValue().getCallData());
                entry.getValue().checkUpdateItemState(mCallViewAdapter.getCallViewModelMap().size());
            }
            logCallData("resetReceivingCameraRequestStatusForGroup", mCallDataList);
            mListener.onUpdateMainCallProfileVisibilityChanged(shouldDisplayMainCallProfile(), false);
        }
    }

    private void updateReceivingCameraRequestStatus(boolean isReceivingCameraRequest, String excludeId) {
        for (CallData callData : mCallDataList) {
            if (!TextUtils.equals(excludeId, callData.getUser().getId())) {
                callData.setReceivingCameraRequest(isReceivingCameraRequest);
            }
        }
    }

    public CallData findParticipantOf1x1Call() {
        for (CallData callData : mCallDataList) {
            if (!TextUtils.equals(callData.getUser().getId(), mCurrentUserId)) {
                return callData;
            }
        }

        return null;
    }

    public synchronized void addCallView(List<CallData> callData) {
        Timber.i("addCallView: " + callData.size());
        logCallData("Adding", callData);
        addCallDataIntoList(callData);
        checkToUpdateCurrentUserCallViewState();
        logCallData("Validated: ", mCallDataList);
        Timber.i("Backup call data list: " + mCallDataList.size());
        if (hasCallVideoViewEnable()) {
            if (mListener.isVideoCall() && mCallDataList.size() == 2) {
                populateData(ignoreMyUserData(mCallDataList), shouldDisplayMainCallProfile());
                if (isCurrentCallViewHasVideo()) {
                    addCurrentUserCallView();
                } else {
                    setSingleCallViewContainerVisibility(false);
                }
            } else {
                resetCurrentUserCallViewState(mCallDataList);
                setSingleCallViewContainerVisibility(false);
                populateData(getValidCallDatToDisplay(), shouldDisplayMainCallProfile());
            }
        } else {
            resetCurrentUserCallViewState(mCallDataList);
            setSingleCallViewContainerVisibility(false);
            populateData(getValidCallDatToDisplay(), shouldDisplayMainCallProfile());
        }
    }

    private List<CallData> getValidCallDatToDisplay() {
        Timber.i("getValidCallDatToDisplay: mCallDataList.size: " + mCallDataList.size() + ", hasVideoCallView: " + hasVideoCallView());
        if (mListener.isGroupCall() && mCallDataList.size() == 2) {
           /*
            There are only two participant in a group voice call now.
            So we need to only display one participant view
           */
            CallData currentUserData = findMyUserCallDataInList(mCallDataList);
            ArrayList<CallData> callData = new ArrayList<>(mCallDataList);
            callData.remove(currentUserData);
            return callData;

//            if (mListener.isGroupCall()) {
//                  /*
//              There are only two participant in a group voice call now.
//              So we need to only display one participant view
//             */
//                CallData currentUserData = findMyUserCallDataInList(mCallDataList);
//                ArrayList<CallData> callData = new ArrayList<>(mCallDataList);
//                callData.remove(currentUserData);
//                return callData;
//            } else if (isCurrentUserRequestingCamera()) {
//                //Will populate only current user call view in list
//                CallData currentUserData = findMyUserCallDataInList(mCallDataList);
//                if (currentUserData != null) {
//                    return Collections.singletonList(currentUserData);
//                }
//            }
        }

        return mCallDataList;
    }

    private void addCallDataIntoList(List<CallData> callData) {
        List<CallData> newValidList = new ArrayList<>();
        if (mCallDataList.isEmpty()) {
            newValidList.addAll(callData);
        } else {
            for (CallData newCallData : callData) {
                //User is must.
                CallData existCallDataByUserId = getExistCallDataByUserId(newCallData.getUser().getId());
                if (existCallDataByUserId != null) {
                    existCallDataByUserId.cloneCallStates(newCallData);
                } else {
                    newValidList.add(newCallData);
                }
            }
        }

        mCallDataList.addAll(newValidList);
    }

    private void updateDataState(CallData newCallSate) {
        for (CallData callData : mCallDataList) {
            if (TextUtils.equals(callData.getUser().getId(), newCallSate.getUser().getId())) {
                callData.cloneCallStates(newCallSate);
                break;
            }
        }
    }

    private CallData getExistCallDataByUserId(String userId) {
        for (CallData existCallData : mCallDataList) {
            if (TextUtils.equals(userId,
                    existCallData.getUser().getId())) {
                return existCallData;
            }
        }

        return null;
    }

    public void removeCallView(String userId) {
        Timber.i("Remove call view of user: " + userId + ", before delete call size: " + mCallDataList.size());
        for (int size = mCallDataList.size() - 1; size >= 0; size--) {
            if (TextUtils.equals(mCallDataList.get(size).getUser().getId(), userId)) {
                mCallDataList.remove(size);
                break;
            }
        }
        if (mListener.isGroupCall() && mCallDataList.size() == 1) {
            mCallDataList.get(0).setCallAction(CallData.CallAction.WAITING_OTHER_TO_JOIN);
        }
        Timber.i("After delete call size: " + mCallDataList.size());
        addCallView(new ArrayList<>(mCallDataList));
    }

    private void resetCurrentUserCallViewState(List<CallData> mCallDataList) {
        CallData myUserCallDataInList = findMyUserCallDataInList(mCallDataList);
        if (myUserCallDataInList != null && myUserCallDataInList.getVideoCallView() != null) {
            //To not allow the call view display top of other views.
            myUserCallDataInList.getVideoCallView().setZOrderOnTop(false);
        }
    }

    private List<CallData> ignoreMyUserData(List<CallData> mCallDataList) {
        String myUserId = mCurrentUserId;
        List<CallData> dataList = new ArrayList<>();
        for (CallData callData : mCallDataList) {
            if (!TextUtils.equals(myUserId, callData.getUser().getId())) {
                dataList.add(callData);
            }
        }

        return dataList;
    }

    private void logCallData(String tag, List<CallData> mCallDataList) {
        for (CallData callData : mCallDataList) {
            logCallData(tag, callData);
        }
    }

    private void logCallData(String tag, CallData callData) {
        Timber.i("Tag: " + tag +
                ", User: " + callData.getUser().getFullName() +
                ", Video view has code: " + (callData.getVideoCallView() != null ?
                callData.getVideoCallView().hashCode() : null) + "," +
                "Call action: " + callData.getCallAction() +
                ", has video: " + callData.isHasVideo());
    }

    private CallData findMyUserCallDataInList(List<CallData> mCallDataList) {
        String myUserId = mCurrentUserId;
        for (int i = 0; i < mCallDataList.size(); i++) {
            if (TextUtils.equals(myUserId, mCallDataList.get(i).getUser().getId())) {
                return mCallDataList.get(i);
            }
        }

        return null;
    }

    private CallData findUserCallDataInList(List<CallData> callDataList, String userId) {
        for (int i = 0; i < callDataList.size(); i++) {
            if (TextUtils.equals(userId, callDataList.get(i).getUser().getId())) {
                return callDataList.get(i);
            }
        }

        return null;
    }

    private void checkToUpdateCurrentUserCallViewState() {
        if (mCallDataList.size() > 1) {
            //There are at least two participant in call. So the call view should be in engagement mode
            for (CallData callData : mCallDataList) {
                callData.setCallAction(CallData.CallAction.ENGAGED_IN_CALL);
            }
        }
    }

    private void removeCallViewFromItsParent(View callView) {
        if (callView != null && callView.getParent() != null) {
            ((ViewGroup) callView.getParent()).removeView(callView);
        }
    }

    private void bindMySingleCallView(CallData callData) {
        Timber.i("bindMySingleCallView");
        removeCallViewFromItsParent(callData.getVideoCallView());
        setSingleCallViewContainerVisibility(true);
        mBinding.singleCallViewContainer.removeAllViews();

        mCurrentSingleUserBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.list_item_call_view,
                mBinding.singleCallViewContainer,
                true);
        mCurrentSingleUserViewModel = new ItemCallViewModel(new CallViewAdapter.CallViewAdapterListener() {
            @Override
            public Context getApplicationContext() {
                return mApplicationContext;
            }
        },
                callData,
                1,
                0,
                mListener.getJoinedGroupCallParticipantCounter());
        mCurrentSingleUserBinding.setVariable(BR.viewModel, mCurrentSingleUserViewModel);
        if (callData.getVideoCallView() != null) {
            mCurrentSingleUserBinding.videoContainerView.removeAllViews();
            //To make the call view of current user display at the top of other view.
            callData.getVideoCallView().setZOrderOnTop(true);
            mCurrentSingleUserBinding.videoContainerView.addView(callData.getVideoCallView());
        }
    }

    public void resetCallViewAndDataState() {
        for (CallData callData : mCallDataList) {
            callData.clearCallView();
        }
        mCallDataList.clear();
        mCallViewAdapter = null;
        mCurrentSingleUserViewModel = null;
        mCurrentSingleUserBinding = null;
        mSingleCallViewMarginTop = 0;
        mIsCurrentUserRequestingCamera = false;
        mDisplayingRequestingVideo = false;
        mBinding.recyclerView.setAdapter(null);
    }

    private void setSingleCallViewContainerVisibility(boolean isVisible) {
        mBinding.singleCallViewContainer.setVisibility(isVisible ? VISIBLE : GONE);
    }

    public void onToggleVideoCameraOfCurrentUser(boolean isEnable) {
        if (getJoiningCallViewCount() == 1 &&
                mCurrentSingleUserViewModel != null) {
            Timber.i("onToggleVideoCameraOfCurrentUser: isEnable: " + isEnable);
            if (isEnable) {
                mCurrentSingleUserViewModel.onVideoCameraOn();
                setSingleCallViewContainerVisibility(true);
                setCurrentUserCallViewVisibleMostTopWindow(true);
            } else {
                mCurrentSingleUserViewModel.onVideoCameraOff(false, getJoiningCallViewCount());
                setSingleCallViewContainerVisibility(false);
                setCurrentUserCallViewVisibleMostTopWindow(false);
            }
            updateCallDataCameraState(mCurrentSingleUserViewModel.getCallData(), isEnable);
        } else {
            ItemCallViewModel viewModel = mCallViewAdapter.getCallViewModelFromUserId(SharedPrefUtils.getUserId(getContext()));
            Timber.i("onToggleVideoCameraOfCurrentUser: viewModel: " + viewModel + ", isEnable: " + isEnable);
            if (viewModel != null) {
                onCameraStateChanged(SharedPrefUtils.getUserId(getContext()), isEnable);
                if (isEnable) {
                    if (mCallDataList.size() == 1 && viewModel.getCallData().getCallAction() == CallData.CallAction.INITIALIZE_ViEW) {
                    } else if (getJoiningCallViewCount() == 1 && mBinding.singleCallViewContainer.getVisibility() != View.VISIBLE) {
                        setSingleCallViewContainerVisibility(true);
                    }
                } else {
                    if (mCallDataList.size() == 1 && viewModel.getCallData().getCallAction() == CallData.CallAction.INITIALIZE_ViEW) {
                    } else if (mBinding.singleCallViewContainer.getVisibility() == View.VISIBLE) {
                        setSingleCallViewContainerVisibility(false);
                    }
                }
            } else {
                //TODO: Need to add current user view
            }
        }
        checkToUpdateTo1x1AudioCallStyleIfNecessary();
        revalidateVideoCameraStatus();
    }

    private void updateCallDataCameraState(CallData callData, boolean isEnable) {
        callData.setHasVideo(isEnable);
    }

    public void onToggleVideoCameraOfParticipant(String userId, boolean isEnable) {
        ItemCallViewModel participantViewModel = mCallViewAdapter.getCallViewModelFromUserId(userId);
        if (participantViewModel != null) {
            //Update video state of participant call data
            participantViewModel.getCallData().setVideoCall(isEnable);
            participantViewModel.getCallData().setHasVideo(isEnable);

            if ((isEnable && !participantViewModel.isVideoCallViewDisplay()) ||
                    (!isEnable && participantViewModel.isVideoCallViewDisplay())) {
                /*
                   Need to enable/disable call video only if the state is different from the old state.
                 */
                Timber.i("onToggleVideoCameraOfParticipant: " + userId + ", isEnable: " + isEnable);
                onCameraStateChanged(userId, isEnable);
                checkToUpdateTo1x1AudioCallStyleIfNecessary();
                revalidateVideoCameraStatus();
            }
        }
    }

    private void onCameraStateChanged(String userId, boolean isEnable) {
        ItemCallViewModel viewModel = mCallViewAdapter.getCallViewModelFromUserId(userId);
        Timber.i("onCameraStateChanged: viewModel: " + viewModel + ", userId: " + userId + ", isEnable: " + isEnable);
        if (viewModel != null) {
            updateCallDataCameraState(viewModel.getCallData(), isEnable);
            if (isEnable) {
                viewModel.onVideoCameraOn();
            } else {
                viewModel.onVideoCameraOff(shouldShowSubCallInfo(userId, viewModel.getCallData()),
                        getJoiningCallViewCount());
            }
        }
    }

    private boolean shouldShowSubCallInfo(String userId, CallData callData) {
        Timber.i("shouldShowSubCallInfo: " + callData.getCallAction() + ", getJoiningCallViewCount(): " + getJoiningCallViewCount() +
                ", isAllParticipantHasNoVideoEnable: " + isAllParticipantHasNoVideoEnable() + ", userId: " + userId + ", hasVideo: " + callData.isHasVideo());
        String currentUserId = mCurrentUserId;
        if (TextUtils.equals(currentUserId, userId)) {
            return getJoiningCallViewCount() > 1;
        } else {
            return getJoiningCallViewCount() > 1 &&
                    callData.getCallAction() != CallData.CallAction.INITIALIZE_ViEW;
        }
    }

    private void setCurrentUserCallViewVisibleMostTopWindow(boolean visibleMostTop) {
        Timber.i("setCurrentUserCallViewVisibleMostTopWindow: visibleMostTop: " + visibleMostTop);
        if (mCurrentSingleUserViewModel.getCallData().getVideoCallView() != null) {
            if (visibleMostTop) {
                removeCallViewFromItsParent(mCurrentSingleUserViewModel.getCallData().getVideoCallView());
                mCurrentSingleUserBinding.videoContainerView.removeAllViews();
                mCurrentSingleUserBinding.videoContainerView.addView(mCurrentSingleUserViewModel.getCallData().getVideoCallView());
            } else {
                if (mCurrentSingleUserBinding.videoContainerView.getChildCount() > 0) {
                    SurfaceView videoCallView = (SurfaceView) mCurrentSingleUserBinding.videoContainerView.getChildAt(0);
                    mCurrentSingleUserBinding.videoContainerView.removeView(videoCallView);
                }
            }
        }
    }

    private void checkToUpdateTo1x1AudioCallStyleIfNecessary() {
        //Will display main call profile and duration such 1x1 audio style.
        if (isAllParticipantHasNoVideoEnable() && getJoiningCallViewCount() <= 2) {
            mListener.onUpdateMainCallProfileVisibilityChanged(true,
                    getJoiningCallViewCount() <= 2);
//            showOnlyAnotherParticipantInCall();
            hideSubCallInfoOfAllCallParticipantViews();
        } else if (getJoiningCallViewCount() > 1 || isCurrentUserTopCallViewVisible()) {
            mListener.onUpdateMainCallProfileVisibilityChanged(false, false);
        } else if (getJoiningCallViewCount() == 0 && isCurrentUserTopCallViewVisible()) {
            mListener.onUpdateMainCallProfileVisibilityChanged(true, true);
        }
    }

    public void revalidateVideoCameraStatus() {
        if (mCallViewAdapter.getCallViewModelMap().size() > 0) {
            if (!isAllParticipantHasNoVideoEnable() || mListener.isGroupCall()) {
                for (Map.Entry<String, ItemCallViewModel> entry : mCallViewAdapter.getCallViewModelMap().entrySet()) {
                    entry.getValue().revalidateVideoCameraStatus(getJoiningCallViewCount());
                }
            } else {
                //Hide sub call info
                for (Map.Entry<String, ItemCallViewModel> entry : mCallViewAdapter.getCallViewModelMap().entrySet()) {
                    entry.getValue().hideSubInfo();
                }
            }
        }
    }

    public CallData.CallAction getCurrentUserCallAction() {
        CallData currentUser = findMyUserCallDataInList(mCallDataList);
        if (currentUser != null) {
            return currentUser.getCallAction();
        }

        return CallData.CallAction.UNKNOWN;
    }

    private void hideSubCallInfoOfAllCallParticipantViews() {
        for (Map.Entry<String, ItemCallViewModel> entry : mCallViewAdapter.getCallViewModelMap().entrySet()) {
            entry.getValue().hideSubInfo();
        }
    }

    private boolean isCurrentUserTopCallViewVisible() {
        return mBinding.singleCallViewContainer.getVisibility() == VISIBLE &&
                mBinding.singleCallViewContainer.getChildCount() > 0;
    }

    public boolean isCurrentCallViewHasVideo() {
        CallData myCallData = findMyUserCallDataInList(mCallDataList);
        return myCallData != null && myCallData.isHasVideo() && myCallData.getVideoCallView() != null;
    }

    private boolean hasVideoCallView() {
        return mListener.isVideoCall() || hasCallVideoViewEnable();
    }

    private boolean hasCallVideoViewEnable() {
        boolean hasVideoViewEnable = false;
        for (CallData callData : mCallDataList) {
            if (callData.getVideoCallView() != null && callData.isHasVideo()) {
                hasVideoViewEnable = true;
                break;
            }
        }

        return hasVideoViewEnable;
    }

    public void onAudioStateChanged(String userId, boolean isMute) {
        if (getJoiningCallViewCount() > 2) {
            ItemCallViewModel callViewModelFromUserId = mCallViewAdapter.getCallViewModelFromUserId(userId);
            if (callViewModelFromUserId != null) {
                callViewModelFromUserId.getCallData().setMuteAudio(isMute);
                if (isMute) {
                    callViewModelFromUserId.onAudioOff(mListener.getJoinedGroupCallParticipantCounter());
                } else {
                    callViewModelFromUserId.onAudioOn();
                }
            }
        }
    }

    public RecyclerView getCallContainerView() {
        return mBinding.recyclerView;
    }

    public void adaptViewOnFloating() {
        requestLayout();
        mBinding.recyclerView.requestLayout();
        mBinding.recyclerView.setAdapter(null);
        mBinding.recyclerView.post(() -> mBinding.recyclerView.setAdapter(mCallViewAdapter));
        mBinding.singleCallViewContainer.requestLayout();
        mBinding.singleCallViewContainer.getLayoutParams().width = getContext().getResources().getDimensionPixelSize(R.dimen.publisher_width) / 2;
        mBinding.singleCallViewContainer.getLayoutParams().height = getContext().getResources().getDimensionPixelSize(R.dimen.publisher_height) / 2;

    }

    public void adaptViewBackToFullScreen() {
        requestLayout();
        mBinding.recyclerView.requestLayout();
        mBinding.recyclerView.setAdapter(null);
        mBinding.recyclerView.post(() -> mBinding.recyclerView.setAdapter(mCallViewAdapter));
        mBinding.singleCallViewContainer.requestLayout();
        mBinding.singleCallViewContainer.getLayoutParams().width = getContext().getResources().getDimensionPixelSize(R.dimen.publisher_width);
        mBinding.singleCallViewContainer.getLayoutParams().height = getContext().getResources().getDimensionPixelSize(R.dimen.publisher_height);
    }

    public boolean isParticipantCallViewAdded(String userId) {
        if (mCallViewAdapter != null) {
            for (CallData callData : mCallViewAdapter.getUserList()) {
                if (TextUtils.equals(callData.getUser().getId(), userId)) {
                    return true;
                }
            }
        }

        return false;
    }

    public interface CallViewContainerListener {

        boolean isVideoCall();

        boolean isGroupCall();

        void onUpdateMainCallProfileVisibilityChanged(boolean isVisible, boolean shouldDisplayDuration);

        void onWaitingOtherMemberToJoinCall();

        AbsCallService.CallType getCallType();

        void onCalLViewClicked();

        GeneralCallBehaviorType getCurrentCallBehavior();

        boolean isReceivedOrRequestShareVideoRequest();

        int getJoinedGroupCallParticipantCounter();

        boolean isInFloatingMode();
    }

    @BindingAdapter("dimEnable")
    public static void setDimViewVisibility(CallViewContainer viewContainer, int callBehaviour) {
        Timber.i("setDimViewVisibility: " + callBehaviour);
        viewContainer.setDimViewVisibility(callBehaviour);
    }
}
