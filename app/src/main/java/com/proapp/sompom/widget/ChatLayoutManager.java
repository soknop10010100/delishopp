package com.proapp.sompom.widget;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Veasna Chhom on 3/16/22.
 */
public class ChatLayoutManager extends LinearLayoutManager {

    private boolean mIsLoadingMore = false;
    private OnLoadMoreCallback mOnLoadMoreListener;
    private RecyclerView mRecyclerView;

    public ChatLayoutManager(final Context context) {
        super(context);
    }

    public ChatLayoutManager(Context context, RecyclerView recyclerView) {
        super(context);
        mRecyclerView = recyclerView;
        /*
        Manage to check if we have to force checking the load more possibility for sometimes the
        load more view display on screen but the logic in scrollVerticallyBy() does not work as expected.
         */
        if (mRecyclerView != null) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if (newState == RecyclerView.SCROLL_STATE_IDLE && !mIsLoadingMore) {
                        checkToPerformLoadMoreFromTop();
                        checkToPerformLoadMoreFromBottom();
                    }
                }
            });
        }
    }

    @Override
    public boolean supportsPredictiveItemAnimations() {
        return false;
    }

    public ChatLayoutManager(final Context context, final int orientation, final boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public ChatLayoutManager(final Context context, final AttributeSet attrs,
                             final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public int scrollVerticallyBy(final int dy, final RecyclerView.Recycler recycler, final RecyclerView.State state) {
        if (mOnLoadMoreListener == null) {
            return super.scrollVerticallyBy(dy, recycler, state);
        }
        checkToPerformLoadMoreFromTop();
        checkToPerformLoadMoreFromBottom();
        return super.scrollVerticallyBy(dy, recycler, state);
    }

    private void checkToPerformLoadMoreFromTop() {
        if (mOnLoadMoreListener != null &&
                !mIsLoadingMore &&
                findFirstVisibleItemPosition() == 0) {
            mIsLoadingMore = true;
            mOnLoadMoreListener.onLoadMoreFromTop();
        }
    }

    private void checkToPerformLoadMoreFromBottom() {
        if (mOnLoadMoreListener != null &&
                !mIsLoadingMore &&
                ((findFirstVisibleItemPosition() + getChildCount()) >= getItemCount() - 1)) {
            mIsLoadingMore = true;
            mOnLoadMoreListener.onLoadMoreFromBottom();
        }
    }

    public void setOnLoadMoreListener(final OnLoadMoreCallback onLoadMoreListener) {
        mOnLoadMoreListener = onLoadMoreListener;
    }

    public void loadingFinished() {
        mIsLoadingMore = false;
    }

    @Override
    public void smoothScrollToPosition(final RecyclerView recyclerView, RecyclerView.State state, int position) {
        RecyclerView.SmoothScroller smoothScroller = new CenterSmoothScroller(recyclerView.getContext());
        smoothScroller.setTargetPosition(position);
        startSmoothScroll(smoothScroller);
    }

    private static class CenterSmoothScroller extends LinearSmoothScroller {

        CenterSmoothScroller(Context context) {
            super(context);
        }

        @Override
        public int calculateDtToFit(int viewStart, int viewEnd, int boxStart, int boxEnd, int snapPreference) {
            return (boxStart + (boxEnd - boxStart) / 2) - (viewStart + (viewEnd - viewStart) / 2);
        }
    }

    public void checkLToInvokeReachLoadMoreFromBottomByDefaultIfNecessary() {
        if ((findFirstVisibleItemPosition() + getChildCount()) >= (getItemCount() - 1) && !mIsLoadingMore) {
            mIsLoadingMore = true;
            if (mOnLoadMoreListener != null) {
                mOnLoadMoreListener.onReachedLoadMoreFromBottomByDefault();
            }
        }
    }

    public void checkLToInvokeReachLoadMoreFromTopByDefaultIfNecessary() {
        if ((findFirstVisibleItemPosition() == 0) && !mIsLoadingMore) {
            mIsLoadingMore = true;
            if (mOnLoadMoreListener != null) {
                mOnLoadMoreListener.onReachedLoadMoreFromTopByDefault();
            }
        }
    }

    public interface OnLoadMoreCallback {

        void onLoadMoreFromTop();

        default void onLoadMoreFromBottom() {
            //nothing to do on super class
        }

        default void onReachedLoadMoreFromBottomByDefault() {
        }

        default void onReachedLoadMoreFromTopByDefault() {
        }
    }
}
