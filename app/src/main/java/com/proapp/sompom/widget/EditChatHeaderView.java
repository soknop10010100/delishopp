package com.proapp.sompom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.viewmodel.newviewmodel.EditMessageTextViewModel;

public class EditChatHeaderView extends LinearLayout {

    private Chat mChat;

    public EditChatHeaderView(Context context) {
        super(context);
    }

    public EditChatHeaderView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public EditChatHeaderView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public Chat getChat() {
        return mChat;
    }

    public EditChatHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void buildView(Chat chat) {
        mChat = chat;
        removeAllViews();
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.edit_message_type_text_layout,
                this,
                true);
        EditMessageTextViewModel viewModel = new EditMessageTextViewModel(getContext(), chat);
        viewDataBinding.setVariable(BR.viewModel, viewModel);
    }
}
