package com.proapp.sompom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.LayoutPostContextMenuBinding;
import com.proapp.sompom.model.PostContextMenuItem;
import com.proapp.sompom.utils.SpannableUtil;

/**
 * Created by nuonveyo on 10/31/18.
 */

public class PostContextMenuLayout extends LinearLayout {
    private LayoutPostContextMenuBinding mBinding;

    public PostContextMenuLayout(Context context) {
        super(context);
        init();
    }

    public PostContextMenuLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PostContextMenuLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public PostContextMenuLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_post_context_menu, this, true);
    }

    public void setData(PostContextMenuItem postContextMenuItem) {
        if (postContextMenuItem.getFontStyle() != -1) {
            mBinding.iconTextView.setTypeface(ResourcesCompat.getFont(getContext(), postContextMenuItem.getFontStyle()));
        }
        if (postContextMenuItem.getIconColor() != -1) {
            mBinding.iconTextView.setTextColor(postContextMenuItem.getIconColor());
        }
        if (postContextMenuItem.getTextColor() != -1) {
            mBinding.titleTextView.setTextColor(postContextMenuItem.getTextColor());
        }
        mBinding.iconTextView.setText(postContextMenuItem.getIcon());
        mBinding.titleTextView.setText(SpannableUtil.capitaliseOnlyFirstLetter(getContext().getString(postContextMenuItem.getTitle())));
    }
}
