package com.proapp.sompom.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.proapp.sompom.R;
import com.proapp.sompom.model.emun.RoundCornerType;
import com.proapp.sompom.utils.BitmapConverter;
import com.proapp.sompom.utils.DrawChatImageRoundedUtil;

/**
 * temporary disable rounding code, cos Glide already transform image to Round
 */
public class RoundedImageView extends AppCompatImageView {

    private final int mSmallRadius = getContext().getResources().getDimensionPixelSize(R.dimen.chat_small_radius);
    private int mMediumRadius = getContext().getResources().getDimensionPixelSize(R.dimen.chat_radius);

    private boolean mIsCircle = true;
    private boolean mIsScaleCenterCrop;
    private RoundCornerType mCornerType;
    private int mCirclePadding;
    private boolean mIsRepliedChat;
    private int mRowIndex;

    public RoundedImageView(Context context) {
        super(context);
        init(null);
    }

    public RoundedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public void setRepliedChat(boolean repliedChat) {
        mIsRepliedChat = repliedChat;
    }

    public void setCirclePadding(int circlePadding) {
        mCirclePadding = circlePadding;
    }

    public void setMediumRadius(int mediumRadius) {
        mMediumRadius = mediumRadius;
    }

    public void setCircle(boolean circle) {
        mIsCircle = circle;
    }

    public void setScaleCenterCrop(boolean scaleCenterCrop) {
        mIsScaleCenterCrop = scaleCenterCrop;
    }

    public RoundCornerType getCornerType() {
        return mCornerType;
    }

    public void setRowIndex(int rowIndex) {
        mRowIndex = rowIndex;
    }

    public void setCornerType(RoundCornerType cornerType) {
        mCornerType = cornerType;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        try {
            Drawable drawable = getDrawable();
            if (drawable == null) {
                return;
            }

            if (getWidth() == 0 || getHeight() == 0) {
                return;
            }

            Bitmap b;
            if (drawable instanceof VectorDrawable) {
                b = BitmapConverter.getBitmapFromVectorDrawable(getContext(), drawable);
            } else if (drawable instanceof GifDrawable) {
                b = BitmapConverter.getBitmapFromGifDrawable(getContext(), (GifDrawable) drawable);
            } else {
                b = ((BitmapDrawable) drawable).getBitmap();
            }

//            Timber.i("mCornerType: " + mCornerType + ", mIsCircle: " + mIsCircle);

            if (mIsCircle) {
                b = DrawChatImageRoundedUtil.scaleCenterCrop(b, getHeight() - mCirclePadding,
                        getWidth() - mCirclePadding);
                Bitmap bitmap = b.copy(Bitmap.Config.ARGB_8888, true);
                int padding = getPaddingStart();
                int w = getWidth() - (mCirclePadding * 2);
                Bitmap roundBitmap = DrawChatImageRoundedUtil.cropCircle(bitmap, w);
                canvas.drawBitmap(roundBitmap, padding, padding, null);
            } else {
                Bitmap bitmap;
                if (drawable instanceof VectorDrawable || mIsScaleCenterCrop) {
                    b = DrawChatImageRoundedUtil.scaleCenterCrop(b, getHeight(), getWidth());
                    bitmap = b.copy(Bitmap.Config.ARGB_8888, true);
                } else {
                    bitmap = b.copy(Bitmap.Config.ARGB_8888, true);

                }

                /*
                Will check if the image view is being used in normal or replied mode.
                If it is in replied message mode, we won't draw any top corner for replied message
                has the replied header and the image will just be attached to the header and we keep
                drawing bottom corner as the same.
                 */
                if (mCornerType == RoundCornerType.Single) {
                    if (!mIsRepliedChat) {
                        Bitmap trans = DrawChatImageRoundedUtil.roundAll(bitmap, mMediumRadius);
                        canvas.drawBitmap(trans, 0, 0, null);
                    } else {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundBottomLeft(bitmap, mMediumRadius);
                        Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomRight(trans1, mMediumRadius);
                        canvas.drawBitmap(trans2, 0, 0, null);
                    }
                } else if (mCornerType == RoundCornerType.MeTop) {
                    if (!mIsRepliedChat) {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundTop(bitmap, mMediumRadius);
                        Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomLeft(trans1, mMediumRadius);
                        Bitmap trans3 = DrawChatImageRoundedUtil.roundBottomRight(trans2, mSmallRadius);
                        canvas.drawBitmap(trans3, 0, 0, null);
                    } else {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundBottomLeft(bitmap, mMediumRadius);
                        Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomRight(trans1, mSmallRadius);
                        canvas.drawBitmap(trans2, 0, 0, null);
                    }
                } else if (mCornerType == RoundCornerType.MeMid) {
                    if (!mIsRepliedChat) {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundLeft(bitmap, mMediumRadius);
                        Bitmap trans2 = DrawChatImageRoundedUtil.roundRight(trans1, mSmallRadius);
                        canvas.drawBitmap(trans2, 0, 0, null);
                    } else {
                        if (mRowIndex > 0) {
                            Bitmap trans1 = DrawChatImageRoundedUtil.roundLeft(bitmap, mMediumRadius);
                            Bitmap trans2 = DrawChatImageRoundedUtil.roundRight(trans1, mSmallRadius);
                            canvas.drawBitmap(trans2, 0, 0, null);
                        } else {
                            Bitmap trans1 = DrawChatImageRoundedUtil.roundBottomLeft(bitmap, mMediumRadius);
                            Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomRight(trans1, mSmallRadius);
                            canvas.drawBitmap(trans2, 0, 0, null);
                        }
                    }
                } else if (mCornerType == RoundCornerType.MeBottom) {
                    if (!mIsRepliedChat) {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundTopLeft(bitmap, mMediumRadius);
                        Bitmap trans2 = DrawChatImageRoundedUtil.roundTopRight(trans1, mSmallRadius);
                        Bitmap trans3 = DrawChatImageRoundedUtil.roundBottom(trans2, mMediumRadius);
                        canvas.drawBitmap(trans3, 0, 0, null);
                    } else {
                        if (mRowIndex > 0) {
                            Bitmap trans1 = DrawChatImageRoundedUtil.roundTopLeft(bitmap, mMediumRadius);
                            Bitmap trans2 = DrawChatImageRoundedUtil.roundTopRight(trans1, mSmallRadius);
                            Bitmap trans3 = DrawChatImageRoundedUtil.roundBottom(trans2, mMediumRadius);
                            canvas.drawBitmap(trans3, 0, 0, null);
                        } else {
                            Bitmap trans1 = DrawChatImageRoundedUtil.roundBottom(bitmap, mMediumRadius);
                            canvas.drawBitmap(trans1, 0, 0, null);
                        }
                    }
                } else if (mCornerType == RoundCornerType.YouTop) {
                    if (!mIsRepliedChat) {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundTop(bitmap, mMediumRadius);
                        Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomLeft(trans1, mSmallRadius);
                        Bitmap trans3 = DrawChatImageRoundedUtil.roundBottomRight(trans2, mMediumRadius);
                        canvas.drawBitmap(trans3, 0, 0, null);
                    } else {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundBottomLeft(bitmap, mSmallRadius);
                        Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomRight(trans1, mMediumRadius);
                        canvas.drawBitmap(trans2, 0, 0, null);
                    }
                } else if (mCornerType == RoundCornerType.YouMid) {
                    if (!mIsRepliedChat) {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundLeft(bitmap, mSmallRadius);
                        Bitmap trans2 = DrawChatImageRoundedUtil.roundRight(trans1, mMediumRadius);
                        canvas.drawBitmap(trans2, 0, 0, null);
                    } else {
                        if (mRowIndex > 0) {
                            Bitmap trans1 = DrawChatImageRoundedUtil.roundLeft(bitmap, mSmallRadius);
                            Bitmap trans2 = DrawChatImageRoundedUtil.roundRight(trans1, mMediumRadius);
                            canvas.drawBitmap(trans2, 0, 0, null);
                        } else {
                            Bitmap trans1 = DrawChatImageRoundedUtil.roundBottomLeft(bitmap, mSmallRadius);
                            Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomRight(trans1, mMediumRadius);
                            canvas.drawBitmap(trans2, 0, 0, null);
                        }
                    }
                } else if (mCornerType == RoundCornerType.YouBottom) {
                    if (!mIsRepliedChat) {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundTopLeft(bitmap, mSmallRadius);
                        Bitmap trans2 = DrawChatImageRoundedUtil.roundTopRight(trans1, mMediumRadius);
                        Bitmap trans3 = DrawChatImageRoundedUtil.roundBottom(trans2, mMediumRadius);
                        canvas.drawBitmap(trans3, 0, 0, null);
                    } else {
                        if (mRowIndex > 0) {
                            Bitmap trans1 = DrawChatImageRoundedUtil.roundTopLeft(bitmap, mSmallRadius);
                            Bitmap trans2 = DrawChatImageRoundedUtil.roundTopRight(trans1, mMediumRadius);
                            Bitmap trans3 = DrawChatImageRoundedUtil.roundBottom(trans2, mMediumRadius);
                            canvas.drawBitmap(trans3, 0, 0, null);
                        } else {
                            Bitmap trans1 = DrawChatImageRoundedUtil.roundBottom(bitmap, mMediumRadius);
                            canvas.drawBitmap(trans1, 0, 0, null);
                        }
                    }
                } else if (mCornerType == RoundCornerType.TopLeft) {
                    if (!mIsRepliedChat) {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundTopLeft(bitmap, mMediumRadius);
                        Bitmap trans2 = DrawChatImageRoundedUtil.roundTopRight(trans1, mSmallRadius);
                        Bitmap trans3 = DrawChatImageRoundedUtil.roundBottom(trans2, mSmallRadius);
                        canvas.drawBitmap(trans3, 0, 0, null);
                    } else {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundBottom(bitmap, mSmallRadius);
                        canvas.drawBitmap(trans1, 0, 0, null);
                    }
                } else if (mCornerType == RoundCornerType.TopRight) {
                    if (!mIsRepliedChat) {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundTopLeft(bitmap, mSmallRadius);
                        Bitmap trans2 = DrawChatImageRoundedUtil.roundTopRight(trans1, mMediumRadius);
                        Bitmap trans3 = DrawChatImageRoundedUtil.roundBottom(trans2, mSmallRadius);
                        canvas.drawBitmap(trans3, 0, 0, null);
                    } else {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundBottom(bitmap, mSmallRadius);
                        canvas.drawBitmap(trans1, 0, 0, null);
                    }
                } else if (mCornerType == RoundCornerType.BottomLeft) {
                    if (!mIsRepliedChat) {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundTop(bitmap, mSmallRadius);
                        Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomLeft(trans1, mMediumRadius);
                        Bitmap trans3 = DrawChatImageRoundedUtil.roundBottomRight(trans2, mSmallRadius);
                        canvas.drawBitmap(trans3, 0, 0, null);
                    } else {
                        if (mRowIndex > 0) {
                            Bitmap trans1 = DrawChatImageRoundedUtil.roundTop(bitmap, mSmallRadius);
                            Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomLeft(trans1, mMediumRadius);
                            Bitmap trans3 = DrawChatImageRoundedUtil.roundBottomRight(trans2, mSmallRadius);
                            canvas.drawBitmap(trans3, 0, 0, null);
                        } else {
                            Bitmap trans1 = DrawChatImageRoundedUtil.roundBottomLeft(bitmap, mMediumRadius);
                            Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomRight(trans1, mSmallRadius);
                            canvas.drawBitmap(trans2, 0, 0, null);
                        }
                    }
                } else if (mCornerType == RoundCornerType.BottomRight) {
                    if (!mIsRepliedChat) {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundTop(bitmap, mSmallRadius);
                        Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomLeft(trans1, mSmallRadius);
                        Bitmap trans3 = DrawChatImageRoundedUtil.roundBottomRight(trans2, mMediumRadius);
                        canvas.drawBitmap(trans3, 0, 0, null);
                    } else {
                        if (mRowIndex > 0) {
                            Bitmap trans1 = DrawChatImageRoundedUtil.roundTop(bitmap, mSmallRadius);
                            Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomLeft(trans1, mSmallRadius);
                            Bitmap trans3 = DrawChatImageRoundedUtil.roundBottomRight(trans2, mMediumRadius);
                            canvas.drawBitmap(trans3, 0, 0, null);
                        } else {
                            Bitmap trans1 = DrawChatImageRoundedUtil.roundBottomLeft(bitmap, mSmallRadius);
                            Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomRight(trans1, mMediumRadius);
                            canvas.drawBitmap(trans2, 0, 0, null);
                        }
                    }
                } else if (mCornerType == RoundCornerType.Bottom) {
                    if (!mIsRepliedChat) {
                        Bitmap trans1 = DrawChatImageRoundedUtil.roundTop(bitmap, mSmallRadius);
                        Bitmap trans2 = DrawChatImageRoundedUtil.roundBottom(trans1, mMediumRadius);
                        canvas.drawBitmap(trans2, 0, 0, null);
                    } else {
                        if (mRowIndex > 0) {
                            Bitmap trans1 = DrawChatImageRoundedUtil.roundTop(bitmap, mSmallRadius);
                            Bitmap trans2 = DrawChatImageRoundedUtil.roundBottom(trans1, mMediumRadius);
                            canvas.drawBitmap(trans2, 0, 0, null);
                        } else {
                            Bitmap trans1 = DrawChatImageRoundedUtil.roundBottom(bitmap, mMediumRadius);
                            canvas.drawBitmap(trans1, 0, 0, null);
                        }
                    }
                } else {
                    if (!mIsRepliedChat) {
                        Bitmap trans = DrawChatImageRoundedUtil.roundAll(bitmap, mSmallRadius);
                        canvas.drawBitmap(trans, 0, 0, null);
                    } else {
                        if (mRowIndex > 0) {
                            Bitmap trans = DrawChatImageRoundedUtil.roundAll(bitmap, mSmallRadius);
                            canvas.drawBitmap(trans, 0, 0, null);
                        } else {
                            Bitmap trans = DrawChatImageRoundedUtil.roundBottom(bitmap, mSmallRadius);
                            canvas.drawBitmap(trans, 0, 0, null);
                        }
                    }
                }
            }
        } catch (ClassCastException ex) {
//            Timber.e("onDraw Image Error: %s", ex.toString());
            super.onDraw(canvas);
        }
    }

    private void init(AttributeSet attr) {
        if (attr != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attr, R.styleable.RoundedImageView);
            mIsCircle = typedArray.getBoolean(R.styleable.RoundedImageView_isCircle, true);
            typedArray.recycle();
        }
    }
}