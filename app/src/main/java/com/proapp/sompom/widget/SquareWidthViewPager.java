package com.proapp.sompom.widget;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;

/**
 * Created by he.rotha on 3/9/16.
 */
public class SquareWidthViewPager extends ViewPager {

    public SquareWidthViewPager(Context context) {
        super(context);
    }

    public SquareWidthViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
