package com.proapp.sompom.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.core.content.ContextCompat;

import com.proapp.sompom.R;
import com.proapp.sompom.utils.GlideLoadUtil;

public class MediaImageView extends RelativeLayout {

    private ImageView mThumbnailImageView;
    private ImageView mFullImageView;

    public MediaImageView(Context context) {
        super(context);
        initView();
    }

    public MediaImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public MediaImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public MediaImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView() {
        mThumbnailImageView = new ImageView(getContext());
        mFullImageView = new ImageView(getContext());
        RelativeLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        addView(mThumbnailImageView, params);
        addView(mFullImageView, params);
        mThumbnailImageView.setVisibility(INVISIBLE);
        mFullImageView.setVisibility(INVISIBLE);
    }

    public void loadImage(String thumbnailUrl, String originalUrl, boolean isCenterCrop) {
        if (TextUtils.isEmpty(thumbnailUrl)) {
            mThumbnailImageView.setVisibility(INVISIBLE);
            //Will load only full image
            if (!TextUtils.isEmpty(originalUrl)) {
                mFullImageView.setVisibility(VISIBLE);
                GlideLoadUtil.loadDefault(mFullImageView,
                        originalUrl,
                        ContextCompat.getDrawable(getContext(), R.drawable.ic_photo_placeholder_400dp),
                        isCenterCrop,
                        null);
            } else {
                mFullImageView.setVisibility(INVISIBLE);
            }
        } else {
            mThumbnailImageView.setVisibility(VISIBLE);
            mFullImageView.setVisibility(INVISIBLE);
            //Will load thumbnail first
            GlideLoadUtil.loadDefault(mThumbnailImageView,
                    thumbnailUrl,
                    ContextCompat.getDrawable(getContext(), R.drawable.ic_photo_placeholder_400dp),
                    isCenterCrop,
                    null);

            if (!TextUtils.isEmpty(originalUrl)) {
                //Will load full url too
                GlideLoadUtil.loadDefault(mFullImageView,
                        originalUrl,
                        ContextCompat.getDrawable(getContext(), R.drawable.ic_photo_placeholder_400dp),
                        isCenterCrop,
                        new GlideLoadUtil.GlideLoadUtilListener() {
                            @Override
                            public void onLoadSuccess(Drawable resource, boolean isSVG) {
                                mFullImageView.setVisibility(VISIBLE);
                            }
                        });
            }
        }
    }

    public void setScaleType(ImageView.ScaleType scaleType) {
        mThumbnailImageView.setScaleType(scaleType);
        mFullImageView.setScaleType(scaleType);
    }

    public void resetPhoto() {
        mThumbnailImageView.setImageDrawable(null);
        mThumbnailImageView.setImageResource(0);
        mThumbnailImageView.setImageBitmap(null);

        mFullImageView.setImageDrawable(null);
        mFullImageView.setImageResource(0);
        mFullImageView.setImageBitmap(null);
    }
}
