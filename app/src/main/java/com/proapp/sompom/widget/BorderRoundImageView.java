package com.proapp.sompom.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.makeramen.roundedimageview.Corner;
import com.makeramen.roundedimageview.RoundedImageView;
import com.proapp.sompom.R;
import com.proapp.sompom.model.emun.RoundCornerType;
import com.proapp.sompom.utils.AttributeConverter;

import timber.log.Timber;

public class BorderRoundImageView extends RoundedImageView {

    private final int mSmallRadius = getContext().getResources().getDimensionPixelSize(R.dimen.chat_small_radius);
    private int mMediumRadius = getContext().getResources().getDimensionPixelSize(R.dimen.chat_radius);

    private boolean mIsCircle = true;
    private boolean mIsScaleCenterCrop;
    private RoundCornerType mCornerType;
    private int mCirclePadding;
    private boolean mIsRepliedChat;
    private int mRowIndex;

    public BorderRoundImageView(Context context) {
        super(context);
    }

    public BorderRoundImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BorderRoundImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setRepliedChat(boolean repliedChat) {
        mIsRepliedChat = repliedChat;
    }

    public void setCirclePadding(int circlePadding) {
        mCirclePadding = circlePadding;
    }

    public void setMediumRadius(int mediumRadius) {
        mMediumRadius = mediumRadius;
    }

    public void setCircle(boolean circle) {
        mIsCircle = circle;
    }

    public void setScaleCenterCrop(boolean scaleCenterCrop) {
        mIsScaleCenterCrop = scaleCenterCrop;
    }

    public RoundCornerType getCornerType() {
        return mCornerType;
    }

    public void setRowIndex(int rowIndex) {
        mRowIndex = rowIndex;
    }

    public void setCornerType(RoundCornerType cornerType) {
        mCornerType = cornerType;
    }

    public void performCheckUp() {
        Timber.i("mCornerType: " + mCornerType + ", mIsCircle: " + mIsCircle);
        setBorderColor(AttributeConverter.convertAttrToColor(getContext(), R.attr.chat_preview_border));
        setBorderWidth(1f);
        if (mCornerType == RoundCornerType.Single) {
            if (!mIsRepliedChat) {
                setCornerRadius(mMediumRadius, mMediumRadius, mMediumRadius, mMediumRadius);
            } else {
                setCornerRadius(0, 0, mMediumRadius, mMediumRadius);
            }
        } else if (mCornerType == RoundCornerType.MeTop) {
            if (!mIsRepliedChat) {
                setCornerRadius(mMediumRadius, mMediumRadius, mMediumRadius, mSmallRadius);
            } else {
                setCornerRadius(0, 0, mMediumRadius, mMediumRadius);
            }
        } else if (mCornerType == RoundCornerType.MeMid) {
            if (!mIsRepliedChat) {
                //Round left and round right
                setCornerRadius(mMediumRadius, 0, mMediumRadius, 0);
                setCornerRadius(0, mSmallRadius, 0, mSmallRadius);
            } else {
                if (mRowIndex > 0) {
                    //Round left and round right
                    setCornerRadius(mMediumRadius, 0, mMediumRadius, 0);
                    setCornerRadius(0, mSmallRadius, 0, mSmallRadius);
                } else {
                    //Bottom left and right
                    setCornerRadius(0, mSmallRadius, mMediumRadius, mSmallRadius);
                }
            }
        } else if (mCornerType == RoundCornerType.MeBottom) {
            if (!mIsRepliedChat) {
                //Top left, top right, and bottom
                setCornerRadius(mMediumRadius, mSmallRadius, mMediumRadius, mMediumRadius);
            } else {
                if (mRowIndex > 0) {
                    //Top left, top right, and bottom
                    setCornerRadius(mMediumRadius, mSmallRadius, mMediumRadius, mMediumRadius);
                } else {
                    //Round bottom
                    setCornerRadius(0, 0, mMediumRadius, mMediumRadius);
                }
            }
        } else if (mCornerType == RoundCornerType.YouTop) {
            if (!mIsRepliedChat) {
                //Round top and bottom left and right
                setCornerRadius(mMediumRadius, mMediumRadius, mSmallRadius, mMediumRadius);
            } else {
                //Round bottom left and right
                setCornerRadius(0, 0, mSmallRadius, mMediumRadius);
            }
        } else if (mCornerType == RoundCornerType.YouMid) {
            if (!mIsRepliedChat) {
                //Round left and right
                setCornerRadius(mSmallRadius, mMediumRadius, mSmallRadius, mMediumRadius);
            } else {
                if (mRowIndex > 0) {
                    //Round left and right
                    setCornerRadius(mSmallRadius, mMediumRadius, mSmallRadius, mMediumRadius);
                } else {
                    //Round bottom left and right
                    setCornerRadius(0, 0, mSmallRadius, mMediumRadius);
                }
            }
        } else if (mCornerType == RoundCornerType.YouBottom) {
            if (!mIsRepliedChat) {
                //Round top left and right and round bottom
                setCornerRadius(mSmallRadius, mMediumRadius, mMediumRadius, mMediumRadius);
            } else {
                if (mRowIndex > 0) {
                    //Round top left and right and round bottom
                    setCornerRadius(mSmallRadius, mMediumRadius, mMediumRadius, mMediumRadius);
                } else {
                    //Round bottom
                    setCornerRadius(0, 0, mMediumRadius, mMediumRadius);
                }
            }
        } else if (mCornerType == RoundCornerType.TopLeft) {
            if (!mIsRepliedChat) {
                //Round top and right and round bottom
//                setCornerRadius(mMediumRadius, 0, 0, mSmallRadius);
                setCornerRadius(Corner.TOP_LEFT, mMediumRadius);
                setCornerRadius(Corner.TOP_RIGHT, mSmallRadius);
                setCornerRadius(Corner.BOTTOM_LEFT, mSmallRadius);
                setCornerRadius(Corner.BOTTOM_RIGHT, mSmallRadius);
            } else {
                //Round bottom
                setCornerRadius(0, 0, mSmallRadius, mSmallRadius);
            }
        } else if (mCornerType == RoundCornerType.TopRight) {
            if (!mIsRepliedChat) {
                //Round top and right and round bottom
                setCornerRadius(mSmallRadius, mMediumRadius, mSmallRadius, mSmallRadius);
            } else {
                //Round bottom
                setCornerRadius(mSmallRadius, mMediumRadius, mSmallRadius, mSmallRadius);
            }
        } else if (mCornerType == RoundCornerType.BottomLeft) {
            if (!mIsRepliedChat) {
                //Round top and bottom left and right
                setCornerRadius(mSmallRadius, mSmallRadius, mMediumRadius, mMediumRadius);
            } else {
                if (mRowIndex > 0) {
                    //Round top and bottom left and right
                    setCornerRadius(mSmallRadius, mSmallRadius, mMediumRadius, mMediumRadius);
                } else {
                    //Round bottom left and right
                    setCornerRadius(0, 0, mMediumRadius, mMediumRadius);
                }
            }
        } else if (mCornerType == RoundCornerType.BottomRight) {
            if (!mIsRepliedChat) {
                //Round top and bottom left and right
                setCornerRadius(mSmallRadius, mSmallRadius, mSmallRadius, mMediumRadius);
            } else {
                if (mRowIndex > 0) {
                    //Round top and bottom left and right
                    setCornerRadius(mSmallRadius, mSmallRadius, mSmallRadius, mMediumRadius);
                } else {
                    //Round bottom left and right
                    setCornerRadius(mSmallRadius, mSmallRadius, mSmallRadius, mMediumRadius);
                }
            }
        } else if (mCornerType == RoundCornerType.Bottom) {
            if (!mIsRepliedChat) {
                //Round top and bottom
                setCornerRadius(mSmallRadius, mSmallRadius, mMediumRadius, mMediumRadius);
            } else {
                if (mRowIndex > 0) {
                    //Round top and bottom
                    setCornerRadius(mSmallRadius, mSmallRadius, mMediumRadius, mMediumRadius);
                } else {
                    //Round bottom
                    setCornerRadius(0, 0, mMediumRadius, mMediumRadius);
                }
            }
        } else {
            if (!mIsRepliedChat) {
                //Round all
                setCornerRadius(mSmallRadius, mSmallRadius, mSmallRadius, mSmallRadius);
            } else {
                if (mRowIndex > 0) {
                    //Round all
                    setCornerRadius(mSmallRadius, mSmallRadius, mSmallRadius, mSmallRadius);
                } else {
                    //Round bottom
                    setCornerRadius(0, 0, mSmallRadius, mSmallRadius);
                }
            }
        }
    }
}
