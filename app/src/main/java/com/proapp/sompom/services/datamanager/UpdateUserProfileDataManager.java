package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

public class UpdateUserProfileDataManager extends AbsDataManager {

    public UpdateUserProfileDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<User>> updateUser(String firstName, String lastName, String email) {
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);

        return mApiService.updateUser2(getMyUserId(), UserHelper.getUpdateUserObject(user));
    }
}
