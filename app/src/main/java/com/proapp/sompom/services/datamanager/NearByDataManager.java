package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.services.ApiService;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by He Rotha on 7/17/18.
 */
public class NearByDataManager extends AbsDataManager {
    public NearByDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<List<User>>> getUser(Locations locations) {
        return mApiService.getNearbyUser(locations.getLatitude(), locations.getLongitude());
    }
}
