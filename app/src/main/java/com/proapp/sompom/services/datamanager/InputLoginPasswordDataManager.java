package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.request.LoginBody;
import com.proapp.sompom.model.result.AuthResponse;
import com.proapp.sompom.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 4/25/18.
 */

public class InputLoginPasswordDataManager extends AbsLoginDataManager {

    private ExchangeAuthData mExchangeAuthData;

    public InputLoginPasswordDataManager(Context context,
                                         ApiService publicApiService,
                                         ApiService privateApiService,
                                         ExchangeAuthData exchangeAuthData) {
        super(context, publicApiService, privateApiService);
        mExchangeAuthData = exchangeAuthData;
    }

    public ExchangeAuthData getExchangeAuthData() {
        return mExchangeAuthData;
    }

    public boolean isLogInWithPhone() {
        return !TextUtils.isEmpty(mExchangeAuthData.getCountyCode()) && !TextUtils.isEmpty(mExchangeAuthData.getPhoneNumber());
    }

    public Observable<Response<AuthResponse>> getLoginService(String password) {
        if (isLogInWithPhone()) {
            return getConcatLoginService(mApiService.loginViaPhone(new LoginBody(mExchangeAuthData.getPhoneNumber(), password)));
        } else {
            return getLoginViaEmail(new LoginBody(mExchangeAuthData.getEmail(), password));
        }
    }
}
