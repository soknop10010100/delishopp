package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.model.SupportCustomErrorResponse2;
import com.proapp.sompom.model.request.ForgotPasswordRequestBody;
import com.proapp.sompom.model.request.ResetPasswordRequestBody2;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Chhom Veasna on 5/18/22.
 */

public class ResetPasswordDataManager extends AbsDataManager {

    public ResetPasswordDataManager(Context context, ApiService publicAPIService) {
        super(context, publicAPIService);
    }

    public Observable<Response<SupportCustomErrorResponse2>> getResetPasswordService(String email, String password, String opt) {
        return mApiService.resetPassword2(new ResetPasswordRequestBody2(email, password, opt), LocaleManager.getAppLanguage(mContext));
    }

    public Observable<Response<SupportCustomErrorResponse2>> getForgotPasswordService(String email) {
        return mApiService.forgotPassword2(new ForgotPasswordRequestBody(email, null), LocaleManager.getAppLanguage(mContext));
    }
}
