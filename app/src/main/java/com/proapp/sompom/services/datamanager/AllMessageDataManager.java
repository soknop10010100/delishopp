package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.database.MessageDb;
import com.proapp.sompom.helper.ConversationHelper;
import com.proapp.sompom.model.ActiveUser;
import com.proapp.sompom.model.ActiveUserWrapper;
import com.proapp.sompom.model.emun.SegmentedControlItem;
import com.proapp.sompom.model.result.AllConversationDisplayDataModel;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.ListResponseWrapper;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseHandleErrorFunc;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/27/18.
 */

public class AllMessageDataManager extends AbsLoadMoreDataManager {

    public static final String DEFAULT_PAGINATION = "0";
    public static final int DEFAULT_PAGINATION_SIZE = 20;
    public static final int DEFAULT_RECENT_DISPLAY_CONVERSATION_SIZE = 5;

    private final SegmentedControlItem mItemType;
    private int mUnreadMessageSize;
    private String mNextPage = DEFAULT_PAGINATION;
    private AllMessageDataManagerListener mListener;
    private int mDefaultRecentConversationDisplaySize;

    public AllMessageDataManager(Context context,
                                 ApiService apiService,
                                 SegmentedControlItem itemType,
                                 AllMessageDataManagerListener listener) {
        super(context, apiService);
        mListener = listener;
        mItemType = itemType;
        setPaginationSize(DEFAULT_PAGINATION_SIZE);
    }

    public Observable<ActiveUserWrapper> getSeller(boolean isShowMoreTitle) {
        Observable<ActiveUser> yourSellerOb = handleError(mApiService.getSuggestionMySeller());
        Observable<ActiveUser> activeSellerOb = handleError(mApiService.getSuggestionSeller());

        return Observable.zip(yourSellerOb, activeSellerOb, (yourSeller, activeSeller) -> {
            yourSeller.setShowMore(activeSeller.isEmpty() && isShowMoreTitle);
            yourSeller.setTitle(getContext().getString(R.string.message_your_sellers_title));
            yourSeller.setSellerItemType(ActiveUser.SellerItemType.YOUR_SELLER);

            activeSeller.setShowMore(isShowMoreTitle);
            activeSeller.setTitle(getContext().getString(R.string.message_activate_seller_title));
            activeSeller.setSellerItemType(ActiveUser.SellerItemType.ACTIVE_SELLER);

            final ActiveUserWrapper wrapper = new ActiveUserWrapper();
            wrapper.setYourSeller(yourSeller);
            wrapper.setActiveSeller(activeSeller);
            return wrapper;
        });
    }

    private Observable<ActiveUser> handleError(Observable<Response<ActiveUser>> ob) {
        return ob.concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap((Function<Response<ActiveUser>, ObservableSource<ActiveUser>>) activeUserResponse -> {
                    ActiveUser activeUser = activeUserResponse.body();
                    if (activeUser == null || activeUser.isEmpty()) {
                        return Observable.error(new NullPointerException());
                    }
                    return Observable.just(activeUser);
                })
                .onErrorResumeNext((Function<Throwable, ObservableSource<ActiveUser>>) throwable -> Observable.just(new ActiveUser()));
    }

    public int getUnreadMessageSize() {
        return mUnreadMessageSize;
    }

    public SegmentedControlItem getItemType() {
        return mItemType;
    }

    public Observable<Response<Conversation>> getConversationDetail(String groupId) {
        return mApiService.getConversationDetail(groupId);
    }

    public Observable<ListResponseWrapper<Conversation>> getAllMessageList(boolean isRefresh) {
        Timber.d("getFromApi");
        if (mItemType == SegmentedControlItem.All) {
            return getAllConversation(isRefresh);
        } else {
            return getConversationByType();
        }
    }

    public Observable<ListResponseWrapper<Conversation>> getAllConversationListFromDb() {
        Timber.d("getFromDB");
        if (mItemType == SegmentedControlItem.All) {
            return getAllConversationFromDb();
        } else {
            return getConversationByTypeFromDb();
        }
    }

    private Observable<ListResponseWrapper<Conversation>> getAllConversationFromDb() {
        AllConversationDisplayDataModel allConversationDisplayDataModel = SharedPrefUtils.getPreloadConversation(mContext);
        if (SharedPrefUtils.getPreloadConversation(mContext) != null) {
            SharedPrefUtils.clearPreloadedConversation(mContext);
        }
        return Observable.just(allConversationDisplayDataModel)
                .concatMap(wrapper -> manageConversationSection(wrapper, true))
                .concatMap(listResponse -> {
                    if (listResponse != null) {
                        resetPagination();
                        return Observable.just(listResponse);
                    } else {
                        List<Conversation> toDisplay = ConversationDb.getConversation(mContext);
                        if (toDisplay.size() > getDefaultRecentConversationDisplaySize()) {
                            //Add more conversation header
                            toDisplay.add(getDefaultRecentConversationDisplaySize(), new Conversation(mContext.getString(R.string.conversation_screen_more_conversation_title)));
                        }
                        ListResponseWrapper<Conversation> listResponseWrapper = new ListResponseWrapper<>(toDisplay);
                        listResponseWrapper.setLocalData(true);
                        resetPagination();
                        return Observable.just(listResponseWrapper);
                    }
                });
    }

    private Observable<ListResponseWrapper<Conversation>> getAllConversation(boolean isRefresh) {
        mUnreadMessageSize = 0;
        return getAllConversationInternally(false)
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(conversationWrapperResponse -> manageConversationSection(conversationWrapperResponse.body(), false))
                .concatMap(conversationList -> {
                    // Handle dynamic subscribe to group channel on socket
                    checkToSubscribeGroupChannelForChatSocket(conversationList.getData());
                    return Observable.just(conversationList);
                })
                .onErrorResumeNext(throwable -> {
                    Timber.e("Something went wrong!");
                    throwable.printStackTrace();
                    List<Conversation> conversationList = ConversationDb.getConversation(getContext());
                    if (conversationList.size() > getDefaultRecentConversationDisplaySize()) {
                        //Add more conversation header
                        conversationList.add(getDefaultRecentConversationDisplaySize(), new Conversation(mContext.getString(R.string.conversation_screen_more_conversation_title)));
                    }

                    ListResponseWrapper<Conversation> listResponseWrapper = new ListResponseWrapper<>(conversationList);
                    listResponseWrapper.setError(throwable);
                    listResponseWrapper.setLocalData(true);
                    return Observable.just(listResponseWrapper);
                });
    }

    private Observable<Response<AllConversationDisplayDataModel>> getAllConversationInternally(boolean isLoadMore) {
        if (!isLoadMore) {
            resetPagination();
        }
        return mApiService.getAllConversation(mNextPage, getPaginationSize()).concatMap(response -> {
            if (response.body() != null) {
                if (!isLoadMore) {
                    mDefaultRecentConversationDisplaySize = ConversationHelper.getValidRecentConversationDisplayCount(response);
                }
                AllConversationDisplayDataModel allConversationDisplayDataModel = ConversationHelper.parseAllConversationResponse(response,
                        isLoadMore);
                if (allConversationDisplayDataModel != null) {
                    return Observable.just(Response.success(allConversationDisplayDataModel));
                }
            }

            return Observable.error(new ErrorThrowable(ErrorThrowable.GENERAL_ERROR,
                    getContext().getString(R.string.error_general_description)));
        });
    }

    public int getDefaultRecentConversationDisplaySize() {
        if (mDefaultRecentConversationDisplaySize <= 0) {
            return DEFAULT_RECENT_DISPLAY_CONVERSATION_SIZE;
        }

        return mDefaultRecentConversationDisplaySize;
    }

    private Observable<ListResponseWrapper<Conversation>> manageConversationSection(AllConversationDisplayDataModel allConversationDisplayDataModel, boolean isLoadFromDb) {
        if (allConversationDisplayDataModel != null) {
            if (!isLoadFromDb) {
                checkPagination(allConversationDisplayDataModel.getMoreConversation().getNextPage());
            }

            // Manage to sort all result of conversation
            if (allConversationDisplayDataModel.getRecentConversation() != null
                    && !allConversationDisplayDataModel.getRecentConversation().isEmpty()) {
                Collections.sort(allConversationDisplayDataModel.getRecentConversation());
            }
            if (allConversationDisplayDataModel.getMoreConversation() != null &&
                    allConversationDisplayDataModel.getMoreConversation().getData() != null &&
                    !allConversationDisplayDataModel.getMoreConversation().getData().isEmpty()) {
                Collections.sort(allConversationDisplayDataModel.getMoreConversation().getData());
            }

            // Start managing conversation section
            List<Conversation> conversations = new ArrayList<>();

            // Firstly, we'll check to see if there are conversations in the unreadConversation list.
            //
            // For context, the unreadConversation really refer to conversations that should be above
            // the "More conversation" label.
            if (allConversationDisplayDataModel.getRecentConversation() != null
                    && !allConversationDisplayDataModel.getRecentConversation().isEmpty()) {
                mUnreadMessageSize = allConversationDisplayDataModel.getRecentConversation().size();

                // If there are conversations in the unreadConversation list, add them all to the
                // conversation list
                conversations.addAll(allConversationDisplayDataModel.getRecentConversation());
            }

            /*
                Check to display recently message with at least 5 items.
                And must manage to check adding recently message first before conversations.
             */
            int moreConversationIndex = -1;

            // Make sure archived and deleted messages are removed before determining the position
            // of the "More conversation" label
            conversations = checkArchivedDeletedStatus(conversations, isLoadFromDb);

            // Check if recently message size from server is smaller than the default recently size
            if (conversations.size() <= getDefaultRecentConversationDisplaySize()) {
                LoadMoreWrapper<Conversation> responseConversation = allConversationDisplayDataModel.getMoreConversation();
                if (responseConversation != null &&
                        responseConversation.getData() != null &&
                        !responseConversation.getData().isEmpty()) {
                    // Make sure archived and deleted conversations are removed before determining
                    // the position of the more conversation label
                    List<Conversation> conversationFromResponse = checkArchivedDeletedStatus(responseConversation.getData(), isLoadFromDb);

                    // If recently messages are empty, just add all conversations from response
                    // and add the More conversation label at the default position
                    if (conversations.isEmpty()) {
                        conversations.addAll(conversationFromResponse);
                        if (conversations.size() > getDefaultRecentConversationDisplaySize()) {
                            moreConversationIndex = getDefaultRecentConversationDisplaySize();
                        }
                    } else {
                        // There are some recently messages, so use the size of the recently message
                        // as index for the more conversation label and then add all conversation
                        // from the response
                        moreConversationIndex = conversations.size();
                        conversations.addAll(conversationFromResponse);
                    }
                }
            } else {
                // If recently messages from server are greater than the default recently size,
                // Add the more conversation label at the end of the recently message, then add the
                // conversations from response
                LoadMoreWrapper<Conversation> responseConversation = allConversationDisplayDataModel.getMoreConversation();
                if (responseConversation.getData() != null && !responseConversation.getData().isEmpty()) {
                    // Here, the conversations list only contains recently message, which has already
                    // been checked to remove deleted and archived message, so we can safely use the
                    // size of the recently message list as the position of the more message label
                    moreConversationIndex = conversations.size();
                    // Make sure archived and deleted conversations are removed first before adding
                    // all the conversation from response
                    conversations.addAll(checkArchivedDeletedStatus(responseConversation.getData(), isLoadFromDb));
                }
            }

            checkStatusOfConversationLastMessage(mContext, conversations);
            List<Conversation> conversationToDisplay = checkArchivedDeletedStatus(conversations, isLoadFromDb);
            Collections.sort(conversationToDisplay);
            if (moreConversationIndex >= 0) {
                //Add more conversation header
                conversationToDisplay.add(moreConversationIndex, new Conversation(mContext.getString(R.string.conversation_screen_more_conversation_title)));
            }
            return Observable.just(new ListResponseWrapper<>(conversationToDisplay));
        }
        return Observable.error(new NullPointerException());
    }

    private List<Conversation> checkArchivedDeletedStatus(List<Conversation> conversations, boolean isFromDb) {
        List<Conversation> conversationToDisplay = new ArrayList<>();
        // Loop through all conversation to check archived status and delete status
        List<Conversation> conversationToSave = new ArrayList<>();
        for (Conversation conversationToCheck : conversations) {
            Conversation fromDb = ConversationDb.getConversationById(mContext, conversationToCheck.getId());
            if (fromDb != null) {
                if (fromDb.isArchived()) {
                    conversationToCheck.setArchived(true);
                }
                if (conversationToCheck.getDate() == null || fromDb.getDate() == null) {
                    // New empty message both on server and local, do nothing?
                } else if (fromDb.getDate().before(conversationToCheck.getDate()) && fromDb.isArchived()) {
                    // Is in db and have archived status and receive new message for that group
                    Timber.e("Clearing archive status for conversation: " + conversationToCheck.getGroupName());
                    conversationToCheck.setArchived(false);
                }
            }
            if (!conversationToCheck.isDeleted() && !conversationToCheck.isArchived()) {
                conversationToDisplay.add(conversationToCheck);
            }
            conversationToSave.add(conversationToCheck);
        }
        if (!isFromDb) {
            Collections.sort(conversationToSave);
            ConversationDb.saveConversation(getContext(), conversationToSave, true);
        }
        Collections.sort(conversationToDisplay);
        return conversationToDisplay;
    }

    public Observable<ListResponseWrapper<Conversation>> loadMore() {
        if (isCanLoadMore()) {
            if (mItemType == SegmentedControlItem.All) {
                return getAllConversationInternally(true)
                        .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                        .concatMap(conversationWrapperResponse -> {
                            AllConversationDisplayDataModel allConversationDisplayDataModel = conversationWrapperResponse.body();
                            List<Conversation> conversationToDisplay = new ArrayList<>();
                            if (allConversationDisplayDataModel != null) {
                                LoadMoreWrapper<Conversation> responseConversation = allConversationDisplayDataModel.getMoreConversation();
                                // Check the pagination of the conversation list response
                                checkPagination(responseConversation.getNextPage());
                                List<Conversation> conversations = new ArrayList<>();

                                // Here, we're adding unreadMessage from load more response into the
                                // conversation list as well, since server currently return conversation
                                // both in conversation list and unreadMessages list. In case server
                                // stop returning unreadMessage list for load more response, everything
                                // will still be just fine
                                if (allConversationDisplayDataModel.getRecentConversation() != null &&
                                        !allConversationDisplayDataModel.getRecentConversation().isEmpty()) {
                                    conversations.addAll(allConversationDisplayDataModel.getRecentConversation());
                                }

                                // Add all conversation from response
                                conversations.addAll(responseConversation.getData());
                                // Check through all conversation to see if they are archived or deleted,
                                // and return a sorted conversation list
                                conversationToDisplay = checkArchivedDeletedStatus(conversations, false);
                            }
                            return Observable.just(new ListResponseWrapper<>(conversationToDisplay));
                        })
                        .concatMap(listConversation -> {
                            // Handle dynamic subscribe to group channel on socket
                            checkToSubscribeGroupChannelForChatSocket(listConversation.getData());
                            return Observable.just(listConversation);
                        });
            } else {
                return getConversationByType();
            }
        }
        return Observable.error(new Throwable(mContext.getString(R.string.error_general_description)));
    }

    private Observable<ListResponseWrapper<Conversation>> getConversationByTypeFromDb() {
        List<Conversation> conversationsInDb = ConversationDb.getIndividualOrGroupConversation(getContext(),
                isRequestGroupType());

        return Observable.just(new ListResponseWrapper<>(conversationsInDb))
                .concatMap(responseConversation -> {
                    List<Conversation> conversations = new ArrayList<>();
                    if (responseConversation != null) {
                        conversations = responseConversation.getData();
                    }
                    checkStatusOfConversationLastMessage(mContext, conversations);
                    List<Conversation> toDisplay = checkArchivedDeletedStatus(conversations, true);
                    Timber.i("conversations: " + new Gson().toJson(conversations));
                    return Observable.just(new ListResponseWrapper<>(toDisplay));
                }).onErrorResumeNext(throwable -> {
                    Timber.e(throwable);
                    List<Conversation> conversationList = ConversationDb.getIndividualOrGroupConversation(getContext(),
                            isRequestGroupType());
                    ListResponseWrapper<Conversation> listResponseWrapper = new ListResponseWrapper<>(conversationList);
                    listResponseWrapper.setError(throwable);
                    listResponseWrapper.setLocalData(true);
                    return Observable.just(listResponseWrapper);
                });
    }

    private Observable<ListResponseWrapper<Conversation>> getConversationByType() {
        //Old code
        // return mApiService.getConversationList(mItemType.getValue(), mNextPage, getPaginationSize())
        //New code which manage to retrieve user group conversation list
        return getConversationServiceByType()
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(conversationResponse -> {
                    List<Conversation> conversations = new ArrayList<>();
                    LoadMoreWrapper<Conversation> responseConversation = conversationResponse.body();
                    if (responseConversation != null) {
                        AllMessageDataManager.this.checkPagination(responseConversation.getNextPage());
                        conversations = responseConversation.getData();
                    }
                    checkStatusOfConversationLastMessage(mContext, conversations);
                    List<Conversation> conversationToDisplay = checkArchivedDeletedStatus(conversations, false);
                    Timber.i("conversations: " + new Gson().toJson(conversations));
                    return Observable.just(new ListResponseWrapper<>(conversationToDisplay));
                })
                .concatMap(conversationToDisplay -> {
                    // Handle dynamic subscribe to group channel on socket
                    checkToSubscribeGroupChannelForChatSocket(conversationToDisplay.getData());
                    return Observable.just(conversationToDisplay);
                })
                .onErrorResumeNext(throwable -> {
                    Timber.e(throwable);
                    List<Conversation> conversationList = ConversationDb.getIndividualOrGroupConversation(getContext(),
                            isRequestGroupType());
                    ListResponseWrapper<Conversation> listResponseWrapper = new ListResponseWrapper<>(conversationList);
                    listResponseWrapper.setError(throwable);
                    listResponseWrapper.setLocalData(true);
                    return Observable.just(listResponseWrapper);
                });
    }

    public static void checkStatusOfConversationLastMessage(Context context, List<Conversation> conversations) {
        /*
           Since the delete message status is not yet implemented on server side. We only have the local
           deletion. So we have maintain the delete status of conversation last message by following the
           local status as temporary til the server fully implemented this feature.
         */
        List<String> lastMessagesIds = new ArrayList<>();
        for (Conversation conversation : conversations) {
            if (conversation.getLastMessage() != null && !TextUtils.isEmpty(conversation.getLastMessage().getId())) {
                lastMessagesIds.add(conversation.getLastMessage().getId());
            }
        }

        if (!lastMessagesIds.isEmpty()) {
            String[] ids = new String[lastMessagesIds.size()];
            lastMessagesIds.toArray(ids);
            List<Chat> chatList = MessageDb.queryByIds(context, ids);
            if (!chatList.isEmpty()) {
                for (Conversation conversation : conversations) {
                    if (conversation.getLastMessage() != null &&
                            !TextUtils.isEmpty(conversation.getLastMessage().getId())) {
                        for (Chat chat : chatList) {
                            if (TextUtils.equals(chat.getId(),
                                    conversation.getLastMessage().getId())) {
                                conversation.getLastMessage().setDelete(chat.isDeleted());
                            }
                        }
                    }
                }
            }
        }
    }

    private Observable<Response<LoadMoreWrapper<Conversation>>> getConversationServiceByType() {
        if (mItemType == SegmentedControlItem.Buying) {
            return mApiService.getGroupConversationList(getMyUserId(),
                    mNextPage,
                    getPaginationSize());
        } else {
            return mApiService.getConversationList(mItemType.getValue(),
                    mNextPage,
                    getPaginationSize());
        }
    }

    private void checkToSubscribeGroupChannelForChatSocket(List<Conversation> conversationList) {
        if (conversationList != null && !conversationList.isEmpty()) {
            List<String> groupIds = new ArrayList<>();
            for (Conversation con : conversationList) {
                if (con.isGroup() && !con.isDeleted()) {
                    groupIds.add(con.getId());
                }
            }

            if (!groupIds.isEmpty() && mListener != null) {
                mListener.checkToSubscribeGroupChannelId(groupIds);
            }
        }
    }

    private boolean isRequestGroupType() {
        return mItemType == SegmentedControlItem.Buying;
    }

    public void resetNextPage() {
        setCanLoadMore(false);
        mNextPage = DEFAULT_PAGINATION;
    }

    private void checkPagination(String nextPage) {
        if (!TextUtils.isEmpty(nextPage)) {
            setCanLoadMore(true);
            mNextPage = nextPage;
        } else {
            setCanLoadMore(false);
        }

        Timber.e("mNextPage: %s", mNextPage);
    }

    public interface AllMessageDataManagerListener {
        void checkToSubscribeGroupChannelId(List<String> channelIds);
    }
}
