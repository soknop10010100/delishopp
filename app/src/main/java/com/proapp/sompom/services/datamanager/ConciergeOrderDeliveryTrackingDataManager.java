package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.google.gson.Gson;
import com.proapp.sompom.model.emun.ConciergeOrderStatus;
import com.proapp.sompom.model.result.ConciergeDriverLocationResponse;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 16/12/21.
 */
public class ConciergeOrderDeliveryTrackingDataManager extends AbsDataManager {

    private ConciergeOrder mOrder;
    private final String mOrderId;

    public ConciergeOrderDeliveryTrackingDataManager(Context context,
                                                     ApiService apiService,
                                                     String orderId,
                                                     ConciergeOrder order) {
        super(context, apiService);
        mOrderId = orderId;
        mOrder = order;
        Timber.i("mOrder: " + new Gson().toJson(mOrder));
    }

    public ConciergeOrder getOrder() {
        return mOrder;
    }

    public boolean shouldRequestDriverLocation() {
        return mOrder != null &&
                (mOrder.getOrderStatus() == ConciergeOrderStatus.DELIVERING ||
                        mOrder.getOrderStatus() == ConciergeOrderStatus.ARRIVED);
    }

    public Observable<Response<ConciergeOrder>> getOrderDetail() {
        return mApiService.getConciergeOrderHistoryDetail(mOrderId,
                LocaleManager.getAppLanguage(mContext)).concatMap(conciergeOrderResponse -> {
            if (conciergeOrderResponse.isSuccessful()) {
                mOrder = conciergeOrderResponse.body();
            }
            return Observable.just(conciergeOrderResponse);
        });
    }

    public Observable<Response<ConciergeDriverLocationResponse>> getDriverLocation() {
        return mApiService.getDriverLocation(mOrder.getId());
    }
}
