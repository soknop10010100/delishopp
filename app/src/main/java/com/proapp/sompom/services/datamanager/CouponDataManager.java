package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.model.concierge.ConciergeCoupon;
import com.proapp.sompom.services.ApiService;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Response;

/**
 * Created by Chhom Veasna on 7/21/22.
 */

public class CouponDataManager extends AbsDataManager {

    private String mPreviousSelectedId;
    private boolean mHasFoundPreviousSelection;

    public CouponDataManager(Context context, ApiService apiService, String previousSelectedId) {
        super(context, apiService);
        mPreviousSelectedId = previousSelectedId;
    }

    public boolean isHasFoundPreviousSelection() {
        return mHasFoundPreviousSelection;
    }

    public Observable<Response<List<ConciergeCoupon>>> getCouponListService() {
        return mApiService.getPublicCoupon().concatMap(listResponse -> {
            if (!TextUtils.isEmpty(mPreviousSelectedId) &&
                    listResponse.isSuccessful() &&
                    listResponse.body() != null &&
                    !listResponse.body().isEmpty()) {
                for (ConciergeCoupon conciergeCoupon : listResponse.body()) {
                    if (TextUtils.equals(conciergeCoupon.getId(), mPreviousSelectedId)) {
                        conciergeCoupon.setSelected(true);
                        mHasFoundPreviousSelection = true;
                        break;
                    }
                }
            }

            return Observable.just(listResponse);
        });
    }
}
