package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.model.SupportCustomErrorResponse2;
import com.proapp.sompom.model.request.ForgotPasswordRequestBody;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Chhom Veasna on 5/17/22.
 */

public class ForgotPasswordDataManager2 extends AbsLoginDataManager {

    public ForgotPasswordDataManager2(Context context, ApiService publicApiService, ApiService privateApiService) {
        super(context, publicApiService, privateApiService);
    }

    public Observable<Response<SupportCustomErrorResponse2>> getForgotPasswordService(String email) {
        return mApiService.forgotPassword2(new ForgotPasswordRequestBody(email, null), LocaleManager.getAppLanguage(mContext));
    }
}
