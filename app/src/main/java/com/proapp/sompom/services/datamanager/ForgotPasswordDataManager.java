package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.R;
import com.proapp.sompom.model.request.ForgotPasswordRequestBody;
import com.proapp.sompom.model.result.CustomMessageResponse;
import com.proapp.sompom.model.result.ForgotPasswordResponse;
import com.proapp.sompom.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

public class ForgotPasswordDataManager extends AbsDataManager {

    public ForgotPasswordDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<CustomMessageResponse>> getForgotPasswordService(String email) {
        return mApiService.forgotPassword(new ForgotPasswordRequestBody(email,
                mContext.getString(R.string.request_forgot_password_url)));
    }
}
