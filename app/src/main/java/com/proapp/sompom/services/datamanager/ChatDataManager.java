package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.example.usermentionable.model.UserMentionable;
import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.chat.service.ChatSocket;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.database.LastMessageIdTempDb;
import com.proapp.sompom.database.LinkPreviewDb;
import com.proapp.sompom.database.MediaDb;
import com.proapp.sompom.database.MessageDb;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.ConversationHelper;
import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.BaseChatModel;
import com.proapp.sompom.model.UnreadChatModel;
import com.proapp.sompom.model.emun.ConversationType;
import com.proapp.sompom.model.emun.OpenChatScreenType;
import com.proapp.sompom.model.emun.StoreStatus;
import com.proapp.sompom.model.request.JumpSearchRequestBody;
import com.proapp.sompom.model.request.JumpSearchResult;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.CheckLatestMessageCountResponse;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.GenerateLinkPreviewUtil;
import com.proapp.sompom.utils.NetworkStateUtil;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/25/18.
 */

public class ChatDataManager extends AbsLoadMoreDataManager {

    private static final int PAGINATION_SIZE = 18;
    private static final int MAX_LOAD_LATEST_MESSAGE_COUNT = 25;

    private Conversation mConversation;
    private boolean mIsAlreadyBindSeenStatusForIndividualChat;
    private Chat mSelectedChat;
    private OpenChatScreenType mOpenChatScreenType = OpenChatScreenType.DEFAULT;
    private Chat mOldestLocalMessage;
    private boolean mCanLoadMoreFromBottom;
    private String mNextPageBottom = "0";
    private boolean mIsJumpSearch;
    private JumpSearchResult mJumpSearchResult;
    private boolean mIsJumpMessageScrollDownReachedLocalChat;
    private Chat mLastNewLoadMoreJumpMessage;
    private boolean mIsUnreadSectionAlreadyAdded;
    private boolean mHasLoadMessageFailed;
    private final Map<String, Chat> mLastSeenMessageOfParticipant = new HashMap<>();
    private boolean mIsSupportConversation;
    private final ChatDataManagerCommunicator mListener;
    private final List<UserMentionable> mGroupMentionableUser = new ArrayList<>();
    private boolean mIsRequestMessageHistory;

    public ChatDataManager(Context context,
                           ApiService apiService,
                           ChatDataManagerCommunicator listener,
                           Conversation conversation,
                           Chat selectedChat,
                           OpenChatScreenType openChatScreenType) {
        super(context, apiService);
        mListener = listener;
        mOpenChatScreenType = openChatScreenType;
        mSelectedChat = selectedChat;
        mConversation = conversation;
        //Load the updated conversation from local
        loadConversationFromLocal();
        checkToCreateConversationIfNotExist(conversation.getId());
        setPaginationSize(PAGINATION_SIZE);
        Timber.i("ChatDataManager: mOpenChatScreenType: " + mOpenChatScreenType);
        checkToLoadOldestMessageForJumpMode();
        buildGroupMentionableUser();
    }

    public boolean isSupportConversation() {
        return mIsSupportConversation;
    }

    public boolean isHasLoadMessageFailed() {
        return mHasLoadMessageFailed;
    }

    public boolean isRequestMessageHistory() {
        return mIsRequestMessageHistory;
    }

    public void resetToNormalModeChat() {
        mOpenChatScreenType = OpenChatScreenType.DEFAULT;
        mSelectedChat = null;
        setPaginationSize(PAGINATION_SIZE);
        mOldestLocalMessage = null;
        mCanLoadMoreFromBottom = false;
        mJumpSearchResult = null;
        mIsJumpMessageScrollDownReachedLocalChat = false;
        mLastNewLoadMoreJumpMessage = null;
        resetPagination();
    }

    private void checkToLoadOldestMessageForJumpMode() {
        if ((mOpenChatScreenType == OpenChatScreenType.GENERAL_SEARCH ||
                mOpenChatScreenType == OpenChatScreenType.GROUP_SEARCH) &&
                mSelectedChat != null) {
            mOldestLocalMessage = MessageDb.getOldestMessageOfConversation(mContext, mConversation.getId());
        }
    }

    public boolean isJumpSearch() {
        return (mOpenChatScreenType == OpenChatScreenType.GENERAL_SEARCH ||
                mOpenChatScreenType == OpenChatScreenType.GROUP_SEARCH);
    }

    private void buildGroupMentionableUser() {
        if (mConversation != null &&
                mConversation.getParticipants() != null &&
                !mConversation.getParticipants().isEmpty()) {
            mGroupMentionableUser.clear();
            for (User participant : mConversation.getParticipants()) {
                mGroupMentionableUser.add(new UserMentionable(participant.getId(),
                        participant.getFirstName(),
                        participant.getLastName(),
                        participant.getUserProfileThumbnail(),
                        participant.getStatus() == StoreStatus.ACTIVE));
            }
        }
    }

    public OpenChatScreenType getOpenChatScreenType() {
        return mOpenChatScreenType;
    }

    public Chat getSelectedChat() {
        return mSelectedChat;
    }

    public Observable<List<UserMentionable>> getUserMentionList(String userName) {
        return Observable.create(e -> {
            List<UserMentionable> data = new ArrayList<>();
            if (!TextUtils.isEmpty(userName)) {
                if (!mGroupMentionableUser.isEmpty()) {
                    for (UserMentionable participant : mGroupMentionableUser) {
                        if (participant.getFullName().toLowerCase().contains(userName.toLowerCase())) {
                            data.add(participant);
                        }
                    }
                }
            } else {
                //Show all participants
                data.addAll(mGroupMentionableUser);
            }

            e.onNext(data);
            e.onComplete();
        });
    }

    public void loadConversationFromLocal() {
        mConversation = ConversationDb.getConversationById(getContext(), mConversation.getId());
        if (mConversation != null) {
            ConversationType conversationType = ConversationType.fromValue(mConversation.getType());
            mIsSupportConversation = conversationType == ConversationType.SUPPORT;
        }
    }

    private void checkToCreateConversationIfNotExist(String conversationId) {
        if (mConversation == null) {
            /*
            If a local conversation does not exist, we will create a new one and add flag when
            user come back to that conversation later, we will try to get chat history from
            server again.
             */
            mConversation = ConversationDb.createNewConversation(getContext(), conversationId);
            Timber.i("saveLastLocalMessageIdForGettingAllChatHistoryLater");
            LastMessageIdTempDb.saveLastLocalMessageIdForGettingAllChatHistoryLater(mContext, conversationId);
        } else if (!NetworkStateUtil.isNetworkAvailable(getContext()) &&
                MessageDb.getMessageCountForAllStatus(getContext(), mConversation.getId()) <= 0) {
            Timber.i("saveLastLocalMessageIdForGettingAllChatHistoryLater");
            LastMessageIdTempDb.saveLastLocalMessageIdForGettingAllChatHistoryLater(mContext, conversationId);
        }
    }

    public Conversation getConversation() {
        return mConversation;
    }

    public Disposable checkToRequestGroupConversationDetailIfNecessary(ChatDataManagerListener listener) {
        /*
        Will always load group conversation detail from server to have update seen avatar list.
         */
        if (!mConversation.isGroup()) {
            //Ignore this checking and considered as checked.
            if (listener != null) {
                listener.onCheckGroupConversationFinished();
            }
            return null;
        }

        Observable<Response<Conversation>> ob = mApiService.getConversationDetail(mConversation.getId());
        BaseObserverHelper<Response<Conversation>> helper = new BaseObserverHelper<>(getContext(), ob);
        return helper.execute(new OnCallbackListener<Response<Conversation>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (listener != null) {
                    listener.onCheckGroupConversationFinished();
                }
            }

            @Override
            public void onComplete(Response<Conversation> result) {
                Timber.i("Load group conversation detail success");
                ConversationDb.save(getContext(), result.body(),
                        "checkToRequestGroupConversationDetailIfNecessary");
                loadConversationFromLocal();
                if (listener != null) {
                    listener.onCheckGroupConversationFinished();
                }
            }
        });
    }

    public Disposable requestGroupDetailForOnlineStatusUpdate(OnCallbackListener<Conversation> listener) {
        Observable<Response<Conversation>> ob = mApiService.getConversationDetail(mConversation.getId());
        BaseObserverHelper<Response<Conversation>> helper = new BaseObserverHelper<>(getContext(), ob);
        return helper.execute(new OnCallbackListener<Response<Conversation>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (listener != null) {
                    listener.onFail(ex);
                }
            }

            @Override
            public void onComplete(Response<Conversation> result) {
                if (listener != null) {
                    listener.onComplete(result.body());
                }
            }
        });
    }

    public Observable<List<BaseChatModel>> checkToLoadChatHistoryFromServer(Chat lastLocalMessage) {
        /*
            We add the handling in case that new messages what will be loaded from conversation history
            are many such 100 or 200 messages which can happen if user don't use app for quite a while or
            user are using the application with different device. So when user open the app, the new message
            need to be synchronized are a lot and it will take time to load in chat screen. So we will handle
            as following.
            1. We call an API to check if the latest message count of a conversation is reach the maximum we
            set, 25 messages. If it reaches the maximum value, we will load directly the latest 25 chat history
            from server and display for user instead of loading all new messages. But if the count is
            lower than the maximum amount set, we will follow the existing request message history logic.
         */
        long messageCount = MessageDb.getMessageCount(getContext(), mConversation.getId());
        Timber.i("getChatList2: messageCount: " + messageCount);
        if (messageCount > 0) {
            //Need to check latest message count
            Chat lastMessageOfConversation = MessageDb.getLastMessageOfConversation(mContext, mConversation.getId());
            Timber.i("lastMessageOfConversation: " + lastMessageOfConversation);
            if (lastMessageOfConversation != null) {
                return mApiService.checkLatestMessageCount(mConversation.getId(),
                        lastMessageOfConversation.getId()).onErrorResumeNext(throwable -> {
                    //Just return the value zero.
                    return Observable.just(Response.success(new CheckLatestMessageCountResponse()));
                }).concatMap(checkLatestMessageCountResponseResponse -> {
                    if (checkLatestMessageCountResponseResponse.body() != null &&
                            checkLatestMessageCountResponseResponse.body().getCount() >= MAX_LOAD_LATEST_MESSAGE_COUNT) {
                        //Will request some latest messages history from server directly
                        return getOldChatFromServer(mConversation.getId(),
                                getCurrentPage(),
                                MAX_LOAD_LATEST_MESSAGE_COUNT)
                                .onErrorResumeNext(throwable -> {
                                    mHasLoadMessageFailed = true;
                                    Timber.e(throwable);
                                    return Observable.create(e -> {
                                        List<Chat> list = MessageDb.queryChatListForChatScreen(getContext(),
                                                mConversation.getId(),
                                                true);
                                        bindSeenStatus(list);
                                        setCanLoadMore(list.size() > getPaginationSize());
                                        e.onNext(new ArrayList<>(list));
                                        e.onComplete();
                                    });
                                });
                    }

                    return getChatHistoryFromServerInternally(lastLocalMessage);
                });
            }
        }

        return getChatHistoryFromServerInternally(lastLocalMessage);
    }

    public Observable<List<BaseChatModel>> getChatHistoryFromServerInternally(Chat lastLocalMessage) {
        mHasLoadMessageFailed = false;
        final Observable<List<BaseChatModel>> ob;
        boolean shouldRequestAllChatHistory = LastMessageIdTempDb.shouldRequestAllChatHistory(getContext(), mConversation.getId());
        Timber.i("shouldRequestAllChatHistory: " + shouldRequestAllChatHistory);
        if ((MessageDb.getMessageCount(getContext(), mConversation.getId()) > 0 &&
                lastLocalMessage != null &&
                !shouldRequestAllChatHistory)) {
            Timber.i("Getting getLatestMessageFromServer");
            /*
                In this approach, we decide to request message list from the oldest message display
                in chat screen so as to update the status of existing displaying messages in screen and
                to load the missing or new messages.
             */
            ob = getLatestMessageFromServer(mConversation.getId(), lastLocalMessage);
        } else {
            Timber.i("Getting getOldChatFromServer");
            if (NetworkStateUtil.isNetworkAvailable(getContext()) && shouldRequestAllChatHistory) {
                LastMessageIdTempDb.removeLastMessageId(getContext(), mConversation.getId());
            }
            ob = getOldChatFromServer(mConversation.getId(), getCurrentPage(), MAX_LOAD_LATEST_MESSAGE_COUNT);
        }
        return ob.onErrorResumeNext(throwable -> {
            mHasLoadMessageFailed = true;
            Timber.e(throwable);
            return Observable.create(e -> {
                List<Chat> list = MessageDb.queryChatListForChatScreen(getContext(), mConversation.getId(), true);
                bindSeenStatus(list);
                setCanLoadMore(list.size() > getPaginationSize());
                e.onNext(new ArrayList<>(list));
                e.onComplete();
            });
        });
    }

    private Observable<List<BaseChatModel>> getOldChatFromServer(String conversationId, String page, int pageSize) {
        mIsRequestMessageHistory = true;
        Observable<List<BaseChatModel>> ob;
        ob = ChatHelper.concatLoadMoreAbleChatResultFromServer(mContext,
                conversationId,
                mApiService.getChatList(conversationId, page, pageSize))
                .concatMap((Function<Response<LoadMoreWrapper<Chat>>, ObservableSource<List<Chat>>>) listResponse -> {
                    if (listResponse.body() != null) {
                        checkPagination(listResponse.body());
                        setCanLoadMore(listResponse.body().getData().size() >= getPaginationSize());
                        Timber.i("Oldest chat size: " + listResponse.body().getData().size());
                        List<Chat> list = listResponse.body().getData();
                        sortMessageByDateASC(list);
                        return Observable.just(new ArrayList<>(list));
                    }
                    return Observable.just(new ArrayList<>());
                })
                .concatMap((Function<List<Chat>, ObservableSource<List<BaseChatModel>>>) listResponse -> {
                    if (!listResponse.isEmpty()) {
                        backupLocalDownloadedMediaPath(listResponse);
                        MessageDb.save(getContext(), listResponse);
                    }
                    List<Chat> list;
                    if (mSelectedChat != null) {
                        list = MessageDb.queryToSelectedMessage(getContext(), mSelectedChat, conversationId, true);
                    } else {
                        list = MessageDb.queryChatListForChatScreen(getContext(), conversationId, true);
                    }
                    injectLocalLoadedPreviewLink(list);
                    bindSeenStatus(list);
                    return Observable.just(injectUnreadMessageTitle(list));
                });
        return ob;
    }

    public Observable<List<BaseChatModel>> getLatestMessageFromDb(String conversationId) {
        return Observable.create(e -> {
            List<Chat> list;
            if (mSelectedChat != null) {
//                Timber.i("Query the latest message to the selected message: " + new Gson().toJson(mSelectedChat));
                list = MessageDb.queryToSelectedMessage(getContext(),
                        mSelectedChat,
                        conversationId,
                        true);
            } else {
                list = MessageDb.queryChatListForChatScreen(getContext(), conversationId, true);
//                Timber.i("Local size: " + list.size() + ",\n First index: " + new Gson().toJson(list.get(0)) +
//                        ",\nEnd index: " + new Gson().toJson(list.get(list.size() - 1)));
            }
            //                        Timber.e("Latest from DB: " + new Gson().toJson(lists));
            Observable.just(list)
                    .concatMap(Observable::just)
                    .subscribe(localChat -> {
                        backupLocalDownloadedMediaPath(localChat);
                        injectLocalLoadedPreviewLink(localChat);
                        bindSeenStatus(localChat);
                        sortMessageByDateASC(localChat);
                        MessageDb.save(mContext, localChat);
                        boolean hasLocalLoadMore = false;
                        if (!localChat.isEmpty()) {
                            hasLocalLoadMore = MessageDb.isHasMoreLocalChat(mContext, localChat.get(0));
                            setCanLoadMore(hasLocalLoadMore);
                        }

                        if (!isCanLoadMore()) {
                            /*
                                Will check to force enable load more from top with local data in the following
                                conditions:
                                1.  If user just logged in and open conversion and the first default size of chat will be
                                    the same as default pagination or we can say, the first one of chat is loaded and
                                    save locally. Then user exit and open again this conversation, the local chat will be
                                    the same as pagination and there will be no load more data at local. So in this
                                    case we will enable load more by default if the loaded local size is equal or greater
                                    than pagination loaded from server.

                                2. If the total local message count of conversation is less than or equal the pagination size.
                             */
                            if (localChat.size() >= PAGINATION_SIZE) {
                                setCanLoadMore(true);
                            } else if (!hasLocalLoadMore && MessageDb.getMessageCountForAllStatus(mContext, mConversation.getId()) <= PAGINATION_SIZE) {
                                setCanLoadMore(true);
                            }
                        }

                        checkToUpdatePreviousMessageStatusToSeenStatus(localChat);
                        e.onNext(injectUnreadMessageTitle(localChat));
                        e.onComplete();
                    });
        });
    }

    private Observable<List<BaseChatModel>> getLatestMessageFromServer(String conversationId, Chat lastCheckingMessage) {
        mIsRequestMessageHistory = false;
        return Observable.create((ObservableOnSubscribe<Chat>) e -> {
//            Timber.i("lastCheckingMessage: " + new Gson().toJson(lastCheckingMessage));
            if (lastCheckingMessage == null) {
                e.onError(new ErrorThrowable(ErrorThrowable.GENERAL_ERROR,
                        "There is no last unseen message in local."));
            } else {
                Timber.i("lastCheckingMessage: " + new Gson().toJson(lastCheckingMessage));
                e.onNext(lastCheckingMessage);
            }
            e.onComplete();
        })
                .concatMap((Function<Chat, ObservableSource<Response<List<Chat>>>>) chat ->
                        ChatHelper.concatLoadChatResultFromServer(mContext,
                                conversationId,
                                mApiService.getLatestChatList(conversationId,
                                        chat.getId())))
                .concatMap((Function<Response<List<Chat>>, ObservableSource<List<Chat>>>) listResponse -> {
                    /*
                    Will remove the last message id temp of current conversation if the process of get
                    latest message from server success.
                     */
                    if (listResponse.isSuccessful()) {
                        sortMessageByDateASC(listResponse.body());
                        LastMessageIdTempDb.removeLastMessageId(mContext, conversationId);
                    }

                    if (listResponse.body() != null) {
                        List<Chat> list = listResponse.body();
                        return Observable.just(new ArrayList<>(list));
                    }
                    return Observable.just(new ArrayList<>());
                })
                .concatMap((Function<List<Chat>, ObservableSource<List<BaseChatModel>>>) listOfChat -> {
                    return Observable.create(e -> {
                        if (!listOfChat.isEmpty()) {
                            MessageDb.save(getContext(), listOfChat);
                            Timber.i("Last message: " + MessageDb.getLastMessageOfConversation(mContext, mConversation.getId()));
                            backupLocalDownloadedMediaPath(listOfChat);
                            injectLocalLoadedPreviewLink(listOfChat);
                            bindSeenStatus(listOfChat);
                            //Auto add unread message
                            List<BaseChatModel> listFromServer = new ArrayList<>(listOfChat);
                            Timber.i("newList: " + listFromServer.size());
                            if (!listFromServer.isEmpty()) {
                                int unreadMessageIndexForNewMessage = getUnreadMessageIndexForNewMessage(getNewChatFromList(listFromServer));
                                if (unreadMessageIndexForNewMessage >= 0) {
                                    listFromServer.add(unreadMessageIndexForNewMessage, new UnreadChatModel());
                                }
                            }
                            checkToUpdatePreviousMessageStatusToSeenStatus(getChatListFromBaseChat(listFromServer));
                            e.onNext(listFromServer);
                        } else {
                            e.onNext(new ArrayList<>());
                        }
                        e.onComplete();
                    });
                })
                .onErrorResumeNext(throwable -> {
                    Timber.i("onErrorResumeNext: " + throwable.getMessage());
                    mHasLoadMessageFailed = true;
                    return Observable.create(e -> {
                        e.onNext(new ArrayList<>());
                        e.onComplete();
                    });
                });
    }

    private int getUnreadMessageIndexForNewMessage(List<BaseChatModel> newList) {
        if (newList.isEmpty()) {
            return -1;
        }

        int smallestUnreadMessageIndex = -1;
        for (int i = 0; i < newList.size(); i++) {
            Chat chat = (Chat) newList.get(i);
            if (chat.getSeen() != null && !chat.getSeen().isEmpty() && chat.getSeen().contains(getMyUserId())) {
                smallestUnreadMessageIndex = -1;
                continue;
            }

            if (smallestUnreadMessageIndex < 0) {
                smallestUnreadMessageIndex = i;
            }
        }

        return smallestUnreadMessageIndex;
    }

    private List<BaseChatModel> getNewChatFromList(List<BaseChatModel> list) {
        //Message list is sorted by: ASC, which mean the last message is at the bottom of list.

        if (mListener != null && mListener.getLastChatInList() != null) {
            List<BaseChatModel> newList = new ArrayList<>();
            for (BaseChatModel baseChatModel : list) {
                if (((Chat) baseChatModel).compareTo((Chat) mListener.getLastChatInList()) > 0) {
                    newList.add(baseChatModel);
                }
            }
            return newList;
        } else {
            return list;
        }
    }

    private void mergeLocalDisplayChatWithNewChatFromServer(List<Chat> newChatFromServer, Chat lastLocalChat) {
        /*
        We assume the newChatFromServer was already sorted DESC. So the additional chat that must be added
        into exist list must be those that are not yet in the list.
         */
        //Find the last local in new server list
        int existId = -1;
        for (int i = 0; i < newChatFromServer.size(); i++) {
            if (TextUtils.equals(lastLocalChat.getId(), newChatFromServer.get(i).getId())) {
                existId = i;
                break;
            }
        }

        if (existId >= 0) {
            List<Chat> newList = new ArrayList<>();
            for (int i = existId + 1; i < newChatFromServer.size(); i++) {
                newList.add(newChatFromServer.get(i));
            }

            newChatFromServer.clear();
            newChatFromServer.addAll(newList);
        }
    }

    private void injectLocalLoadedPreviewLink(List<Chat> chatList) {
        LinkPreviewDb.printAll(getContext());
        if (chatList != null && !chatList.isEmpty()) {
            LinkPreviewDb.parseCheckingData(chatList, new LinkPreviewDb.LinkPreviewDbListener() {
                @Override
                protected void onParseDataSuccess(String[] ids, String[] previewContents) {
                    List<LinkPreviewModel> localData = LinkPreviewDb.findLocalData(getContext(),
                            ids,
                            previewContents);
                    if (localData != null && !localData.isEmpty()) {
                        for (Chat chat : chatList) {
                            for (LinkPreviewModel localDatum : localData) {
                                if (chat.getId().matches(localDatum.getId())) {
                                    chat.setLinkPreviewModel(localDatum);
                                    break;
                                }
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * @param chatList must be the sorted list.
     */
    private List<BaseChatModel> injectUnreadMessageTitle(List<Chat> chatList) {
        if (mIsUnreadSectionAlreadyAdded) {
            Timber.i("Ignore check to add unread message item for it was previous added into list.");
            return new ArrayList<>(chatList);
        }

        String userId = SharedPrefUtils.getUserId(mContext);
        List<BaseChatModel> result = new ArrayList<>(chatList);
        if (mConversation != null) {
            String lastSeenMessageId = ConversationDb.getLastSeenMessageIdOfCurrentUser(mContext, mConversation.getId());
            Timber.i("lastSeenMessageId: " + lastSeenMessageId + ", chat: " + result.size());
            if (!TextUtils.isEmpty(lastSeenMessageId)) {
                int insertIndex = -1;
                for (int i = 0; i < result.size(); i++) {
//                    Timber.i("lastSeenMessageId: " + lastSeenMessageId + ", chat id: " + result.get(i).getId());
                    if (TextUtils.equals(lastSeenMessageId, ((Chat) result.get(i)).getId()) &&
                            i != (result.size() - 1)) {
//                        Timber.i("Should check to insert the unread message tile.");
                        //Will ignore the call even and group modification message types.
                        //Find the inset index
                        for (int i1 = i + 1; i1 < chatList.size(); i1++) {
                            if (!ChatSocket.isCallEvenChat((Chat) result.get((i1))) &&
                                    !ConversationHelper.isGroupModificationMessageType((Chat) result.get((i1))) &&
                                    !TextUtils.equals(((Chat) result.get(i1)).getSenderId(), userId)) {
                                insertIndex = i1;
                                break;
                            }
                        }
                        break;
                    }
                }

                if (insertIndex >= 0) {
                    result.add(insertIndex, new UnreadChatModel());
                    mIsUnreadSectionAlreadyAdded = true;
                    Timber.i("Insert unread message.");
                }
            } else if (!result.isEmpty()) {
                result.add(0, new UnreadChatModel());
                mIsUnreadSectionAlreadyAdded = true;
                Timber.i("Insert unread message at the top of conversation list.");
            }
        }

        return result;
    }

    private void backupLocalDownloadedMediaPath(List<Chat> remoteChats) {
        MediaDb.backupLocalDownloadedMFileMediaPath(getContext(), MediaDb.getMediaList(remoteChats));
    }

    public Observable<List<BaseChatModel>> loadMore(Chat lastChatDate) {
        if (lastChatDate == null) {
            return Observable.error(new Throwable(mContext.getString(R.string.error_general_description)));
        }
        Timber.e("load more at " + lastChatDate.getContent());
        SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        mDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        String date = mDateFormat.format(lastChatDate.getDate());
        Observable<List<BaseChatModel>> ob;

        if (NetworkStateUtil.isNetworkAvailable(mContext)) {
            ob = ChatHelper.concatLoadMoreAbleChatResultFromServer(mContext,
                    mConversation.getId(),
                    mApiService.getChatList(mConversation.getId(), date, getPaginationSize()))
                    .concatMap((Function<Response<LoadMoreWrapper<Chat>>, ObservableSource<LoadMoreWrapper<Chat>>>) listResponse -> {
                        if (listResponse.body() != null) {
                            /*
                                In some case, server return load more data that include the oldest message
                                in list, so we have to remove the duplicate.
                             */
                            if (listResponse.body().getData() != null) {
                                for (int size = listResponse.body().getData().size() - 1; size >= 0; size--) {
                                    if (TextUtils.equals(listResponse.body().getData().get(size).getId(), lastChatDate.getId())) {
                                        listResponse.body().getData().remove(size);
                                    }
                                }
                            }

                            checkPagination(listResponse.body());
                            return Observable.just(listResponse.body());
                        }

                        return null;
                    })
                    .concatMap((Function<LoadMoreWrapper<Chat>,
                            ObservableSource<List<BaseChatModel>>>) listResponse -> {
                        List<Chat> chatList = listResponse.getData();
                        sortMessageByDateASC(chatList);
                        checkToEnableCheckToBindSeenAvatarForOneToOneConversationWhenAddMoreOrUpdateMessageData();
                        bindLoadMoreSeenStatus(chatList);
                        backupLocalDownloadedMediaPath(chatList);
                        injectLocalLoadedPreviewLink(chatList);
                        if (mOpenChatScreenType == OpenChatScreenType.DEFAULT) {
                            MessageDb.save(getContext(), chatList);
                        }
                        return Observable.just(new ArrayList<>(chatList));
                    });
        } else {
            List<Chat> fromDb = MessageDb.queryLoadMore(getContext(), lastChatDate);
            if (fromDb != null && !fromDb.isEmpty() && fromDb.size() > 1) {
                ob = Observable.just(fromDb)
                        .concatMap((Function<List<Chat>,
                                ObservableSource<List<BaseChatModel>>>) chatList -> {
                            sortMessageByDateASC(chatList);
                            checkToEnableCheckToBindSeenAvatarForOneToOneConversationWhenAddMoreOrUpdateMessageData();
                            bindLoadMoreSeenStatus(chatList);
                            backupLocalDownloadedMediaPath(chatList);
                            injectLocalLoadedPreviewLink(chatList);
                            if (mOpenChatScreenType == OpenChatScreenType.DEFAULT) {
                                MessageDb.save(getContext(), chatList);
                            }
                            return Observable.just(new ArrayList<>(chatList));
                        });
            } else {
                setCanLoadMore(false);
                return Observable.just(new ArrayList<>());
            }
        }

        return ob;
    }

    private void checkToEnableCheckToBindSeenAvatarForOneToOneConversationWhenAddMoreOrUpdateMessageData() {
        /*
           In case that all display messages in screen have no seen avatar display because those message
           types are prohibited from display. We will check to display on the new added or updated message to list.
         */
        if (mListener.isAllDisplayMessageAreIgnoreDisplaySeenAvatarType()) {
            mIsAlreadyBindSeenStatusForIndividualChat = false;
        }
    }

    private void sortMessageByDateASC(List<Chat> chatList) {
        /*
           In the chat screen, we have to display the latest message in the bottom of list. So we
           have to sort the message list by message's date ASC because some APIs response does not
           always response what client prefer.
         */
        if (chatList != null) {
            Collections.sort(chatList);
        }
    }

    private void bindSeenStatus(List<Chat> chatList) {
        if (isGroupConversation()) {
            bindSeenStatusForGroupChat(chatList);
        } else {
            bindSeenStatusForIndividualChat(chatList);
        }
    }

    private void bindLoadMoreSeenStatus(List<Chat> chatList) {
        if (isGroupConversation()) {
            bindSeenStatusForGroupChat(chatList);
        } else if (!mIsAlreadyBindSeenStatusForIndividualChat) {
            //Normally seen status for individual chat will bind only one time for the last seen message.
            bindSeenStatusForIndividualChat(chatList);
        }
    }

    private void checkParticipantLastSeenMessage(List<Chat> chatList, boolean isForceReset) {
        for (User participant : mConversation.getParticipants()) {
            for (Chat chat : chatList) {
                if (chat.getSeen() != null && !chat.getSeen().isEmpty()) {
                    for (String id : chat.getSeen()) {
                        if (TextUtils.equals(id, participant.getId())) {
                            Chat lastSeenMessage = mLastSeenMessageOfParticipant.get(participant.getId());
                            if (lastSeenMessage == null || isForceReset) {
                                mLastSeenMessageOfParticipant.put(participant.getId(), chat);
                            } else {
                                //Check to store the last seen message
                                if (lastSeenMessage.compareTo(chat) < 0) {
                                    mLastSeenMessageOfParticipant.put(participant.getId(), chat);
                                }
                            }

                            break;
                        }
                    }
                }
            }
        }

//        Timber.i("mLastSeenMessageOfParticipant: " + new Gson().toJson(mLastSeenMessageOfParticipant));
    }

    private void bindSeenStatusForGroupChat(List<Chat> chatList) {
        if (chatList != null &&
                !chatList.isEmpty() &&
                mConversation != null &&
                mConversation.getParticipants() != null) {
            /*
               In case that all display messages in screen have no seen avatar display because those message
               types are prohibited from display. We will check to display on the new added or updated message to list.
               So we have to reset record last seen message of participant again.
            */
            checkParticipantLastSeenMessage(chatList, mListener.isAllDisplayMessageAreIgnoreDisplaySeenAvatarType());
            String myUserId = getMyUserId();
            for (Map.Entry<String, Chat> elementEntry : mLastSeenMessageOfParticipant.entrySet()) {
                if (elementEntry != null &&
                        !TextUtils.isEmpty(elementEntry.getKey()) &&
                        elementEntry.getValue() != null) {
                    String chatId = elementEntry.getValue().getId();
                    if (!TextUtils.isEmpty(chatId)) {
                        for (Chat chat : chatList) {
                            if (!TextUtils.isEmpty(chat.getId()) && chat.getId().matches(chatId)) {
                                //Retrieve participant from group and assign to each chat.
                                for (User participant : mConversation.getParticipants()) {
                                    //Current user will not put in the seen message list.
                                    if (!myUserId.matches(elementEntry.getKey()) &&
                                            elementEntry.getKey().matches(participant.getId())) {
                                        if (chat.getSeenParticipants() == null) {
                                            chat.setSeenParticipants(new LinkedList<>());
                                        }
                                        if (!chat.getSeenParticipants().contains(participant)) {
                                            chat.getSeenParticipants().add(participant.cloneForSeenStatus());
                                            chat.setStatus(MessageState.SEEN);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            checkIgnoreSeenMessageType(chatList, false);
        }
    }

    private void checkIgnoreSeenMessageType(List<Chat> lists, boolean isIndividualChat) {
        /*
            The message list must be in the sort of date ASC (old to new chat) to make the following
            logic work correctly.
         */
        for (int mainLoop = lists.size() - 1; mainLoop >= 0; mainLoop--) {
            Chat seenMessage = lists.get(mainLoop);
            if (ChatHelper.isIgnoreDisplaySeenAvatarMessage(seenMessage)) {
                if (seenMessage.getSeenParticipants() != null && !seenMessage.getSeenParticipants().isEmpty()) {
                    for (int ind = mainLoop - 1; ind >= 0; ind--) {
                        Chat existingChat = lists.get(ind);
                        if (!ChatHelper.isIgnoreDisplaySeenAvatarMessage(existingChat)) {
                            if (existingChat.getSeenParticipants() != null && !existingChat.getSeenParticipants().isEmpty() && !isIndividualChat) {
                                for (User user : seenMessage.getSeenParticipants()) {
                                    boolean currentUserExisted = false;
                                    for (User exist : existingChat.getSeenParticipants()) {
                                        if (exist.getId().equals(user.getId())) {
                                            exist.setSeenMessage(true);
                                            currentUserExisted = true;
                                        }
                                    }
                                    if (!currentUserExisted) {
                                        user.setSeenMessage(true);
                                        existingChat.getSeenParticipants().add(user);
                                    }
                                }
                            } else {
                                existingChat.setSeenParticipants(seenMessage.getSeenParticipants());
                            }
                            seenMessage.setSeenParticipants(new ArrayList<>());
                            if (isIndividualChat) {
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    private void checkToUpdatePreviousMessageStatusToSeenStatus(List<Chat> chatList) {
        /*
        Will check to update all previous message status to seen after the last seen message index.
         */
        if (chatList != null && !chatList.isEmpty()) {
            sortMessageByDateASC(chatList);
            int lastSeenMessageIndex = -1;
            for (int size = chatList.size() - 1; size >= 0; size--) {
                if (chatList.get(size).getStatus() == MessageState.SEEN) {
                    lastSeenMessageIndex = size;
                    break;
                }
            }

            for (int size = lastSeenMessageIndex - 1; size >= 0; size--) {
                if (chatList.get(size).getStatus() != MessageState.SEEN) {
                    chatList.get(size).setStatus(MessageState.SEEN);
//                    Timber.i("Update message status to seen.");
                }
            }
        }
    }

    private List<Chat> getChatListFromBaseChat(List<BaseChatModel> chatModels) {
        List<Chat> chats = new ArrayList<>();
        if (chatModels != null && !chatModels.isEmpty()) {
            for (BaseChatModel chatModel : chatModels) {
                if (chatModel instanceof Chat) {
                    chats.add((Chat) chatModel);
                }
            }
        }

        return chats;
    }

    private void bindSeenStatusForIndividualChat(List<Chat> chatList) {
        if (chatList != null &&
                !chatList.isEmpty() &&
                mConversation != null &&
                mConversation.getParticipants() != null) {
//            logChat("bindSeenStatusForIndividualChat", chatList);
            /*
             * The screen avatar for individual conversation, will be display only the recipient side.
             * The current user avatar will never display.
             * So we starting from the most last message.
             */
//            Timber.i("First message Id: " + chatList.get(0).getId() + ", content: " + chatList.get(0).getContent() + ", " + chatList.get(0).getStatus());
            for (int size = chatList.size() - 1; size >= 0; size--) {
                Chat chat = chatList.get(size);
//                Timber.i("Checking: " + new Gson().toJson(chat));
                boolean isOneToOneChatParticipantReadMessage = chat.getStatus() == MessageState.SEEN ||
                        isOneToOneChatParticipantReadMessage(chat);

                if (!TextUtils.equals(chat.getSenderId(), getMyUserId()) ||
                        isOneToOneChatParticipantReadMessage) {
                    User recipient = findSeenAvatarForIndividualChat(mConversation.getParticipants(),
                            false);
                    if (recipient != null) {
                        chat.setSeenParticipants(Collections.singletonList(recipient));
                        mIsAlreadyBindSeenStatusForIndividualChat = true;
                        chat.setStatus(MessageState.SEEN);
                        Timber.i("Bind seen avatar on " + chat.getId() + ", content: " + chat.getContent() + ", " + chat.getStatus());
                        break;
                    }
                }
            }

            checkIgnoreSeenMessageType(chatList, true);
        }
    }

    private User findSeenAvatarForIndividualChat(List<User> userList, boolean isFindCurrentUser) {
        String currentUserId = SharedPrefUtils.getUserId(getContext());
        for (User user : userList) {
            if (isFindCurrentUser) {
                if (TextUtils.equals(user.getId(), currentUserId)) {
                    return user;
                }
            } else {
                if (!TextUtils.equals(user.getId(), currentUserId)) {
                    return user;
                }
            }
        }

        return null;
    }

    private boolean isOneToOneChatParticipantReadMessage(Chat chat) {
        //Check if chat recipient has already in seen list of that chat.
        if (mConversation != null && mConversation.getParticipants() != null && chat.getSeen() != null) {
            for (String id : chat.getSeen()) {
                for (User participant : mConversation.getParticipants()) {
                    if (!TextUtils.equals(participant.getId(), getMyUserId()) && TextUtils.equals(id, participant.getId())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private boolean isGroupConversation() {
        return mConversation != null && mConversation.isGroup();
    }

    public Observable<Response<LinkPreviewModel>> getLinkPreview(Chat chat) {
        return LinkPreviewRetriever.getPreviewLink(getContext(),
                mApiService,
                GenerateLinkPreviewUtil.getFirstUrlIndex(chat.getContent()));
    }

    public boolean isCanLoadMoreFromBottom() {
        return mCanLoadMoreFromBottom;
    }

    public Observable<List<BaseChatModel>> jumpSearchMessage(boolean isLoadMoreBottom) {
        if (isLoadMoreBottom && mIsJumpMessageScrollDownReachedLocalChat) {
            return loadMoreToLatestMessageInLocal().concatMap(list -> {
                injectLocalLoadedPreviewLink(list);
                if (!isLoadMoreBottom) {
                    bindSeenStatus(list);
                } else {
                    bindLoadMoreSeenStatus(list);
                }
                sortMessageByDateASC(list);
                return Observable.just(new ArrayList<>(list));
            });
        } else {
            return mApiService.jumpSearchMessage(new JumpSearchRequestBody(mSelectedChat.getId(),
                            mOldestLocalMessage != null ? mOldestLocalMessage.getId() : null),
                    mNextPageBottom,
                    getPaginationSize()).concatMap(resultResponse -> {
                if (resultResponse.isSuccessful() && resultResponse.body() != null) {
                    if (mJumpSearchResult == null) {
                        mJumpSearchResult = resultResponse.body();
                    }
                    List<Chat> list = resultResponse.body().getData();
                    mIsJumpMessageScrollDownReachedLocalChat = isJumpMessageScrollDownReachedLocalChat(list.get(list.size() - 1));
                    if (mIsJumpMessageScrollDownReachedLocalChat) {
                        //Next load more bottom will be taken from local data.
                        mLastNewLoadMoreJumpMessage = mOldestLocalMessage;
                    } else {
                        mLastNewLoadMoreJumpMessage = list.get(list.size() - 1);
                    }
                    Timber.i("mIsJumpMessageScrollDownReachedLocalChat: " + mIsJumpMessageScrollDownReachedLocalChat);

                    //Check pagination of load more bottom
                    if (!mIsJumpMessageScrollDownReachedLocalChat) {
                        mNextPageBottom = resultResponse.body().getNextPage();
                    } else {
                        //Allow to load more from bottom locally
                        mNextPageBottom = mLastNewLoadMoreJumpMessage.getId();
                    }
                    mCanLoadMoreFromBottom = !TextUtils.isEmpty(mNextPageBottom);

                    /*
                        Check pagination of load more top
                        Auto enable load more from top since we don't have status yet from jump result to check
                        if we can load more from top, so mark it enable by default.
                     */
                    setCanLoadMore(!isLoadMoreBottom);

                    injectLocalLoadedPreviewLink(list);
                    if (!isLoadMoreBottom) {
                        bindSeenStatus(list);
                    } else {
                        bindLoadMoreSeenStatus(list);
                    }
                    sortMessageByDateASC(list);
                    return Observable.just(new ArrayList<>(list));
                } else {
                    return Observable.error(new ErrorThrowable(resultResponse.code(),
                            resultResponse.message()));
                }
            });
        }
    }

    public Observable<List<Chat>> loadMoreToLatestMessageInLocal() {
        return Observable.create(e -> {
            Timber.i("Load more from bottom with local data.");
            List<Chat> list = MessageDb.queryLoadMoreFromOldToNew(mContext,
                    mLastNewLoadMoreJumpMessage,
                    getPaginationSize());
            if (mOldestLocalMessage != null && TextUtils.equals(mOldestLocalMessage.getId(), mLastNewLoadMoreJumpMessage.getId())) {
                /*
                    Reach first load mor bottom from local data and will add oldest local chat into result
                    list because result from load more bottom of local does not included it.
                 */
                Timber.i("Add oldest chat in load more from bottom list.");
                list.add(0, mOldestLocalMessage);
            }

            if (!list.isEmpty()) {
                mLastNewLoadMoreJumpMessage = list.get(list.size() - 1);
                //Check pagination of load more bottom
                if (MessageDb.isHasMoreLocalLatestMessage(mContext, mLastNewLoadMoreJumpMessage)) {
                    // we don't actually use it for local pagination.
                    mNextPageBottom = mLastNewLoadMoreJumpMessage.getId();
                } else {
                    mNextPageBottom = null;
                }
            } else {
                mNextPageBottom = null;
            }
            mCanLoadMoreFromBottom = !TextUtils.isEmpty(mNextPageBottom);
            e.onNext(new ArrayList<>(list));
            e.onComplete();
        });
    }

    private boolean isJumpMessageScrollDownReachedLocalChat(Chat lastJumpSearch) {
        return mJumpSearchResult != null &&
                TextUtils.equals(mJumpSearchResult.getLimitMessageId(), lastJumpSearch.getId());
    }

    public interface ChatDataManagerListener {
        void onCheckGroupConversationFinished();
    }

    public interface ChatDataManagerCommunicator {
        Chat getLastChatInList();

        boolean isAllDisplayMessageAreIgnoreDisplaySeenAvatarType();
    }
}
