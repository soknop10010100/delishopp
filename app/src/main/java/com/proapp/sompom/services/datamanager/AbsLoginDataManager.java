package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.helper.PreAPIRequestHelper;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.model.request.AddPlayerIdBody;
import com.proapp.sompom.model.request.LoginBody;
import com.proapp.sompom.model.request.SaveCustomerLocationRequestBody;
import com.proapp.sompom.model.result.AuthResponse;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.resourcemanager.helper.LocaleManager;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Veasna Chhom on 4/21/21.
 */

public class AbsLoginDataManager extends AbsDataManager {

    protected ApiService mPrivateApiService;

    public AbsLoginDataManager(Context context, ApiService publicApiService, ApiService privateApiService) {
        super(context, publicApiService);
        mPrivateApiService = privateApiService;
    }

    public ApiService getPrivateApiService() {
        return mPrivateApiService;
    }

    public Observable<Response<AuthResponse>> getLoginViaEmail(LoginBody loginBody) {
        Observable<Response<AuthResponse>> loginRequest = mApiService.loginViaEmail(loginBody,
                LocaleManager.getAppLanguage(getContext()));
        return loginRequest.concatMap(authResponseResponse -> {
            if (authResponseResponse.isSuccessful() &&
                    authResponseResponse.body() != null &&
                    authResponseResponse.body().isError()) {
                //There is error response error in success body. So need to return error too.
                return Observable.error(new ErrorThrowable(authResponseResponse.body().getStatusCode(),
                        authResponseResponse.body().getErrorMessage()));
            } else {
                return getConcatLoginService(Observable.just(authResponseResponse));
            }
        });
    }

    public Observable<Response<AuthResponse>> getConcatLoginService(Observable<Response<AuthResponse>> loginService) {
        return loginService
                .concatMap(authResponse -> {
                    //Need to save user access token for private request APIs below.
                    if (authResponse.body() != null && authResponse.body().getUser() != null) {
                        authResponse.body().getUser().setAccessToken(authResponse.body().getAccessToken());
                        SharedPrefUtils.setAccessToken(authResponse.body().getAccessToken(), getContext());
                    }

                    return Observable.just(authResponse);
                }).concatMap(authResponse -> {
                    PreAPIRequestHelper.requestMandatoryAPIs(mContext, mApiService, mPrivateApiService, Observable.just(authResponse));
                    if (authResponse.body() != null) {
                        SharedPrefUtils.setUserValue(authResponse.body().getUser(), mContext);
                        SentryHelper.setUser(authResponse.body().getUser());
                    }
                    return Observable.just(authResponse);
                });
    }

    public Observable<Response<Object>> addPlayerId(AddPlayerIdBody body) {
        return mPrivateApiService.addPlayerId(body);
    }

    public Observable<Response<AuthResponse>> getLoginWithPhoneService(String phoneNumber, String password) {
        return getConcatLoginService(mApiService.loginViaPhone(new LoginBody(phoneNumber, password)));
    }

    public Observable<Response<Object>> saveCustomerLastLocation(SaveCustomerLocationRequestBody body) {
        return mPrivateApiService.saveCustomerLocation(body);
    }
}
