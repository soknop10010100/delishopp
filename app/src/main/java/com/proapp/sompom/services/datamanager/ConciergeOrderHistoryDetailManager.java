package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.google.gson.Gson;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.PreAPIRequestHelper;
import com.proapp.sompom.model.ConciergeCartModel;
import com.proapp.sompom.model.concierge.ConciergeBasket;
import com.proapp.sompom.model.emun.CustomErrorDataType;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.model.result.ConciergeOrderItem;
import com.proapp.sompom.model.result.ConciergeOrderResponse;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

public class ConciergeOrderHistoryDetailManager extends AbsLoadMoreDataManager {

    private String mOrderId;

    public ConciergeOrderHistoryDetailManager(Context context, ApiService apiService, String orderId) {
        super(context, apiService);
        mOrderId = orderId;
    }

    public Observable<Response<ConciergeOrder>> getConciergeOrderHistoryService() {
        return mApiService.getConciergeOrderHistoryDetail(mOrderId, LocaleManager.getAppLanguage(mContext))
                .concatMap(conciergeOrderResponse -> {
                    /*
                       Check to set the shop object to each menu item in case that the shop property
                       does not exist in menu item model because the shop property is mandatory for
                       making order in local.
                     */
                    if (conciergeOrderResponse.isSuccessful() &&
                            conciergeOrderResponse.body() != null &&
                            conciergeOrderResponse.body().getOrderItems() != null) {
                        for (ConciergeOrderItem orderItem : conciergeOrderResponse.body().getOrderItems()) {
                            //Need to assign shop model order if the response does not have it in the model.
                            if (orderItem.getMenuItem() != null && orderItem.getMenuItem().getShop() == null) {
                                orderItem.getMenuItem().setShop(orderItem.getShop());
                            }

                            /*
                                Need to make sure the shop hash is the same as the one with item shop hash history.
                                By doing that, we will make sure that the shop hash will make the re-order are the
                                one from order history.
                             */
                            if (orderItem.getShop() != null) {
                                orderItem.getShop().setHash(orderItem.getShopHash());
                            }
                            if (orderItem.getMenuItem().getShop() != null) {
                                orderItem.getMenuItem().getShop().setHash(orderItem.getShopHash());
                            }
                        }
                    }

                    return Observable.just(conciergeOrderResponse);
                })
                .concatMap(conciergeOrderResponse -> {
                    ConciergeHelper.loadUserInfo(mContext, conciergeOrderResponse.body());
                    return Observable.just(conciergeOrderResponse);
                });
    }

    public Observable<Response<ConciergeBasket>> getReorderService() {
        return mApiService.reorder(mOrderId, LocaleManager.getAppLanguage(mContext)).concatMap(conciergeOrderResponseResponse -> {
            if (conciergeOrderResponseResponse.isSuccessful()) {
                Gson gson = new Gson();
                ConciergeOrderResponse conciergeOrderResponse = gson.fromJson(conciergeOrderResponseResponse.body(),
                        ConciergeOrderResponse.class);
//                Timber.i("conciergeOrderResponse: " + new Gson().toJson(conciergeOrderResponse));
                if (!conciergeOrderResponse.isError()) {
                    ConciergeBasket basket = gson.fromJson(gson.toJson(conciergeOrderResponseResponse.body()), ConciergeBasket.class);
//                    Timber.i("basket: " + new Gson().toJson(basket));
                    if (basket != null && !basket.isBasketEmpty()) {
                        ConciergeCartModel conciergeCartModel = ConciergeHelper.transformRemoteBasketIntoLocalBasket(basket);
                        return ConciergeCartHelper.saveBasket(mContext, conciergeCartModel).concatMap(conciergeCartModel1 -> {
                            ConciergeHelper.broadcastRefreshLocalBasketEvent(mContext);
                            return Observable.just(Response.success(basket));
                        });
                    } else {
                        return Observable.error(ErrorThrowable.newGeneralErrorInstance(mContext));
                    }
                } else {
                    //Check the error type
                    List<CustomErrorDataType> errorTypes = conciergeOrderResponse.getErrorTypes();
//                    Timber.i("errorTypes: " + new Gson().toJson(errorTypes));
                    boolean hasNoItemAvailableError = false;
                    for (CustomErrorDataType errorType : errorTypes) {
                        if (errorType == CustomErrorDataType.NO_ITEM_AVAILABLE) {
                            hasNoItemAvailableError = true;
                            break;
                        }
                    }

                    if (!hasNoItemAvailableError) {
                        ConciergeBasket basket = conciergeOrderResponse.getBasket();

                        //Will save basket and consider as re-order success
                        if (basket != null && !basket.isBasketEmpty()) {
                            basket.setMessage(conciergeOrderResponse.getErrorMessage());
                            ConciergeCartModel conciergeCartModel = ConciergeHelper.transformRemoteBasketIntoLocalBasket(basket);
                            return ConciergeCartHelper.saveBasket(mContext, conciergeCartModel).concatMap(conciergeCartModel1 -> {
                                ConciergeHelper.broadcastRefreshLocalBasketEvent(mContext);
                                return Observable.just(Response.success(basket));
                            });
                        } else {
                            return Observable.just(Response.success(buildErrorResponse(conciergeOrderResponse.getErrorMessage())));
                        }
                    } else {
                        return Observable.just(Response.success(buildErrorResponse(conciergeOrderResponse.getErrorMessage())));
                    }
                }
            } else {
                return Observable.error(ErrorThrowable.newGeneralErrorInstance(mContext));
            }
        });
    }

    private ConciergeBasket buildErrorResponse(String errorMessage) {
        ConciergeBasket basket = new ConciergeBasket();
        basket.setMessage(errorMessage);

        return basket;
    }

    public Observable<Response<ConciergeBasket>> checkBasket() {
        return mApiService.getBasket(ApplicationHelper.getRequestAPIMode(mContext), LocaleManager.getAppLanguage(mContext));
    }

    public Observable<Response<ConciergeBasket>> requestUserBasket() {
        return PreAPIRequestHelper.requestUserBasket(mContext,
                mApiService,
                false,
                false);
    }
}
