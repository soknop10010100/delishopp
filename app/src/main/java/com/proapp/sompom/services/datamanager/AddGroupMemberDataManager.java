package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.request.AddOrRemoveGroupOwnerBody;
import com.proapp.sompom.model.request.AddOrRemoveGroupParticipantBody;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.dialog.AddGroupParticipantDialog;
import com.proapp.sompom.services.ApiService;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;


public class AddGroupMemberDataManager extends AbsLoadMoreDataManager {

    private String mGroupId;
    private AddGroupParticipantDialog.AddingType mAddingType;

    public AddGroupMemberDataManager(Context context,
                                     ApiService apiService,
                                     String groupId,
                                     AddGroupParticipantDialog.AddingType addingType) {
        super(context, apiService);
        mGroupId = groupId;
        mAddingType = addingType;
    }

    public AddGroupParticipantDialog.AddingType getAddingType() {
        return mAddingType;
    }

    public Observable<Response<List<User>>> getGroupAuthorizedUser() {
        return mApiService.getListOfUserForGroupAddingAction(getMyUserId());
    }

    public Observable<Response<GroupDetail>> addGroupParticipants(List<String> userIds) {
        if (mAddingType == AddGroupParticipantDialog.AddingType.OWNER) {
            return mApiService.addGroupOwners(new AddOrRemoveGroupOwnerBody(mGroupId, userIds));
        } else {
            return mApiService.addGroupParticipants(new AddOrRemoveGroupParticipantBody(mGroupId, userIds));
        }
    }
}
