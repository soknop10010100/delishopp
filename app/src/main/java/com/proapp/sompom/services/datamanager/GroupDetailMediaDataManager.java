package com.proapp.sompom.services.datamanager;

import android.content.Context;

import androidx.annotation.NonNull;

import com.proapp.sompom.R;
import com.proapp.sompom.database.MediaDb;
import com.proapp.sompom.model.GroupMediaAdaptive;
import com.proapp.sompom.model.GroupMediaSection;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.GroupMediaDetailType;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.services.ApiService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;
import retrofit2.Response;


/**
 * Created by Veasna Chhom on 7/24/20.
 */

public class GroupDetailMediaDataManager extends AbsLoadMoreDataManager {

    private String mGroupId;
    private GroupMediaDetailType mGroupMediaDetailType;

    public GroupDetailMediaDataManager(Context context,
                                       ApiService apiService,
                                       @NonNull String groupId,
                                       GroupMediaDetailType groupMediaDetailType) {
        super(context, apiService);
        mGroupId = groupId;
        mGroupMediaDetailType = groupMediaDetailType;
    }

    public GroupMediaDetailType getGroupMediaDetailType() {
        return mGroupMediaDetailType;
    }

    public String getGroupId() {
        return mGroupId;
    }

    public Observable<Response<List<GroupMediaSection<Media>>>> getGroupMediaDetailObservable(boolean isLoadMore) {
        return getObservable(isLoadMore,
                mApiService.getGroupPhotoAndVideoDetail(mGroupId,
                        getCurrentPage(),
                        getPaginationSize()));
    }

    public Observable<Response<List<GroupMediaSection<Media>>>> getGroupFileDetailObservable(boolean isLoadMore) {
        return getObservable(isLoadMore,
                mApiService.getGroupFileDetail(mGroupId,
                        getCurrentPage(),
                        getPaginationSize())).concatMap(listResponse -> {
            if (listResponse.body() != null) {
                for (GroupMediaSection<Media> mediaGroupMediaSection : listResponse.body()) {
                    MediaDb.backupLocalDownloadedMFileMediaPath(mContext, mediaGroupMediaSection.getMediaList());
                }
            }

            return Observable.just(listResponse);
        });
    }

    public Observable<Response<List<GroupMediaSection<LinkPreviewModel>>>> getGroupLinkDetailObservable(boolean isLoadMore) {
        return getObservable(isLoadMore,
                mApiService.getGroupLinkDetail(mGroupId,
                        getCurrentPage(),
                        getPaginationSize()));
    }

    public Observable<Response<List<GroupMediaSection<Chat>>>> getGroupVoiceDetailObservable(boolean isLoadMore) {
        return getObservable(isLoadMore,
                mApiService.getGroupVoiceDetail(mGroupId,
                        getCurrentPage(),
                        getPaginationSize()))
                .concatMap(listResponse -> {
                    /*
                    Need to transform all voice media into chat so as to be able to use this data
                    with existing voice layout structure of chat screen.
                     */
                    List<GroupMediaSection<Chat>> voiceSecondList = new ArrayList<>();
                    for (GroupMediaSection<Media> mediaGroupMediaSection : listResponse.body()) {
                        GroupMediaSection<Chat> section = new GroupMediaSection<>();
                        section.setCreatedAt(mediaGroupMediaSection.getCreatedAt());
                        List<Chat> chatList = new ArrayList<>();
                        for (Media media : mediaGroupMediaSection.getMediaList()) {
                            Chat chat = new Chat();
                            chat.setId(UUID.randomUUID().toString());
                            chat.setMediaList(Collections.singletonList(media));
                            chatList.add(chat);
                        }
                        section.setMediaList(chatList);
                        voiceSecondList.add(section);
                    }

                    return Observable.just(Response.success(voiceSecondList));
                });
    }

    public <T extends GroupMediaAdaptive> Observable<Response<List<GroupMediaSection<T>>>> getObservable(boolean isLoadMore,
                                                                                                         Observable<Response<LoadMoreWrapper<GroupMediaSection<T>>>> route) {

        if (!isLoadMore) {
            resetPagination();
        }
        return route.concatMap((Response<LoadMoreWrapper<GroupMediaSection<T>>> response) -> {
            if (response.isSuccessful() && response.body() != null) {
                if (response.body().getData() != null) {
                    checkPagination(response.body());
                    return Observable.just(Response.success(response.body().getData()));
                } else {
                    return Observable.just(Response.success(new ArrayList<>()));
                }
            } else {
                return Observable.error(new Throwable(getContext().getString(R.string.error_general_description)));
            }
        });
    }
}
