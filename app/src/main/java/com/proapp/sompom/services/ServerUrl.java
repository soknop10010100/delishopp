package com.proapp.sompom.services;


import android.content.Context;

import androidx.annotation.NonNull;

import com.proapp.sompom.R;
import com.proapp.sompom.model.emun.EnvironmentType;

import timber.log.Timber;

/**
 * Created by imac on 12/9/15.
 */
public final class ServerUrl {

    private static String sAPIServer = "";
    private static String sSocketServer = "";
    private static EnvironmentType sSelectedEnvironment;

    private ServerUrl() {

    }

    @NonNull
    public static String generateUrl(Context context) {
        if (sAPIServer.isEmpty()) {
            return getDefaultServer(context);
        }
        return sAPIServer;
    }

    @NonNull
    public static String getSocketUrl(Context context) {
        if (sSocketServer.isEmpty()) {
            return getDefaultSocket(context);
        }
        return sSocketServer;
    }

    public static void switchToDemoEnvironment(Context context) {
        sSelectedEnvironment = EnvironmentType.DEMO;
        sAPIServer = context.getString(R.string.api_demo);
        sSocketServer = context.getString(R.string.socket_demo);
    }

    public static void switchToLocalEnvironment(Context context) {
        String localIP4Address = context.getString(R.string.local_ip_address);
        sSelectedEnvironment = EnvironmentType.LOCAL;
        sAPIServer = "http://" + localIP4Address + ":1337/";
        sSocketServer = "ws://" + localIP4Address + ":8084/socketcluster/";
        Timber.i("sSocketServer: " + sSocketServer);
    }

    public static void switchToDevEnvironment(Context context) {
        sSelectedEnvironment = EnvironmentType.DEV;
        sAPIServer = context.getString(R.string.api_dev);
        sSocketServer = context.getString(R.string.socket_dev);
    }

    public static String getDefaultServer(Context context) {
        switchToDevEnvironment(context);
        return sAPIServer;
    }

    public static String getDefaultSocket(Context context) {
        switchToDevEnvironment(context);
        return sSocketServer;
    }

    public static EnvironmentType getSelectedEnvironment(Context context) {
        return sSelectedEnvironment;
    }

    public static String getDevServerUrl(Context context) {
        return context.getString(R.string.api_dev);
    }

    public static String getDevSocketUrl(Context context) {
        return context.getString(R.string.socket_dev);
    }
}
