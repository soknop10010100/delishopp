package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.PreAPIRequestHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.licence.db.LicenseSynchronizationDb;
import com.proapp.sompom.licence.model.SynchroniseData;
import com.proapp.sompom.model.request.SaveCustomerLocationRequestBody;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Response;
import timber.log.Timber;

public class SplashScreenDataManager extends AbsDataManager {

    private ApiService mPublicAPIService;

    public SplashScreenDataManager(Context context, ApiService apiService, ApiService publicAPIService) {
        super(context, apiService);
        mPublicAPIService = publicAPIService;
    }

    public Observable<Response<User>> getMyUserProfile() {
        Observable<Response<User>> getUserById = mApiService.getUserById(getMyUserId(), SharedPrefUtils.getLanguage(mContext));
        return getUserById.concatMap(userResponse -> {
            PreAPIRequestHelper.requestMandatoryAPIs(mContext, mApiService, mApiService, Observable.just(userResponse));
            return Observable.just(userResponse);
        });
    }

    public Observable<Response<Object>> saveCustomerCurrentLocation(SaveCustomerLocationRequestBody body) {
        return mApiService.saveCustomerLocation(body);
    }

    public Observable<Response<Object>> requestNecessaryDataForNoneLoginUser() {
        if (ApplicationHelper.shouldRequestVisitorUserData(mContext) &&
                !UserHelper.isGuestUserExist(getContext())) {
            return mPublicAPIService.getShopSetting(ApplicationHelper.getUserId(mContext)).concatMap(conciergeShopSettingResponse -> {
                Timber.i("getShopSetting: isSuccessful " + conciergeShopSettingResponse.isSuccessful());
                if (conciergeShopSettingResponse.isSuccessful() && conciergeShopSettingResponse.body() != null) {
                    ConciergeHelper.setConciergeShopSetting(mContext, conciergeShopSettingResponse.body());
                }
                return PreAPIRequestHelper.requestGuestUser(mContext, mPublicAPIService).concatMap((Function<Object, ObservableSource<? extends Response<Object>>>) o ->
                        PreAPIRequestHelper.requestAppFeature(mContext, mApiService).concatMap(appFeatureResponse ->
                                mPublicAPIService.getAppSetting().concatMap(listResponse -> {
                                    if (listResponse.body() != null && !listResponse.body().isEmpty()) {
                                        SharedPrefUtils.setAppSetting(getContext(), listResponse.body().get(0));
                                    }
                                    return mApiService.getClientFeatureData().concatMap(listResponse1 -> {
                                        if (listResponse1.isSuccessful()) {
                                            if (listResponse1.body() != null && !listResponse1.body().isEmpty()) {
                                                SynchroniseData synchroniseData = listResponse1.body().get(0);
                                                LicenseSynchronizationDb.saveSynchroniseData(getContext(), synchroniseData);
                                            }
                                        }
                                        return Observable.just(Response.success(new Object()));
                                    });
                                })));
            });
        } else {

            return mPublicAPIService.getShopSetting(ApplicationHelper.getUserId(mContext)).concatMap(conciergeShopSettingResponse -> {
                Timber.i("getShopSetting: isSuccessful " + conciergeShopSettingResponse.isSuccessful());
                if (conciergeShopSettingResponse.isSuccessful() && conciergeShopSettingResponse.body() != null) {
                    ConciergeHelper.setConciergeShopSetting(mContext, conciergeShopSettingResponse.body());
                }
                return PreAPIRequestHelper.requestAppFeature(mContext, mApiService).concatMap(appFeatureResponse ->
                        mPublicAPIService.getAppSetting().concatMap(listResponse -> {
                            if (listResponse.body() != null && !listResponse.body().isEmpty()) {
                                SharedPrefUtils.setAppSetting(getContext(), listResponse.body().get(0));
                            }
                            return mApiService.getClientFeatureData().concatMap(listResponse1 -> {
                                if (listResponse1.isSuccessful()) {
                                    if (listResponse1.body() != null && !listResponse1.body().isEmpty()) {
                                        SynchroniseData synchroniseData = listResponse1.body().get(0);
                                        LicenseSynchronizationDb.saveSynchroniseData(getContext(), synchroniseData);
                                    }
                                }
                                return Observable.just(Response.success(new Object()));
                            });
                        }));
            });
        }
    }
}