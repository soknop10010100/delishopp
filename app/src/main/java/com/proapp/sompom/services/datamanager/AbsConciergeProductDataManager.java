package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.model.concierge.ConciergeBasketMenuItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Chhom Veasna on 5/10/22.
 */
public class AbsConciergeProductDataManager extends AbsLoadMoreDataManager {

    protected Context mContext;
    protected ApiService mApiService;

    public AbsConciergeProductDataManager(Context context, ApiService apiService) {
        super(context, apiService);
        mContext = context;
        mApiService = apiService;
    }

    public Observable<Response<ConciergeBasketMenuItem>> addItemToBasket(ConciergeMenuItem menuItem) {
        return mApiService.addItemToBasket(ConciergeHelper.createOrderMenuItem(menuItem),
                ApplicationHelper.getRequestAPIMode(mContext),
                LocaleManager.getAppLanguage(mContext));
    }

    public Observable<Response<SupportConciergeCustomErrorResponse>> clearBasket() {
        return mApiService.clearBasket(ApplicationHelper.getRequestAPIMode(mContext)).concatMap(response -> {
            if (response.isSuccessful() && response.body() != null && !response.body().isError()) {
                return ConciergeCartHelper.clearCart(mContext).concatMap(o -> Observable.just(response));
            } else {
                return Observable.just(response);
            }
        });
    }
}
