package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.upload.ConciergeAddressUploader;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by Or Vitovongsak on 22/11/21.
 */
public class ConciergeNewAddressDataManager extends AbsLoadMoreDataManager {

    public ConciergeNewAddressDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<ConciergeUserAddress>> createNewAddress(ConciergeUserAddress newAddress) {

        Observable<ConciergeAddressUploader.UploadResult> addressUpload = null;
        Observable<Response<ConciergeUserAddress>> observable;

        if (!TextUtils.isEmpty(newAddress.getPhotoUrl()) && !newAddress.getPhotoUrl().startsWith("http")) {
            addressUpload = ConciergeAddressUploader.uploadConciergeAddressPhoto(getContext(), newAddress.getPhotoUrl())
                    .concatMap(uploadResult -> {
                        if (uploadResult == null) {
                            uploadResult = new ConciergeAddressUploader.UploadResult();
                        }
                        newAddress.setPhotoUrl(uploadResult.getUrl());
                        return Observable.just(uploadResult);
                    });
        }


        if (addressUpload != null) {
            observable = addressUpload.concatMap(uploadResult -> mApiService.createConciergeUserAddress(newAddress,
                            LocaleManager.getAppLanguage(mContext),
                            ApplicationHelper.getRequestAPIMode(mContext))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()));
        } else {
            if (!TextUtils.isEmpty(newAddress.getPhotoUrl())) {
                newAddress.setPhotoUrl("");
            }
            observable = mApiService.createConciergeUserAddress(newAddress,
                    LocaleManager.getAppLanguage(mContext),
                    ApplicationHelper.getRequestAPIMode(mContext));
        }

        return observable;
    }

    public Observable<Response<ConciergeUserAddress>> updateAddress(ConciergeUserAddress addressToUpdate,
                                                                    ConciergeUserAddress previousAddress) {
        Observable<ConciergeAddressUploader.UploadResult> addressUpload = null;
        Observable<Response<ConciergeUserAddress>> observable;

        // Only upload if it's not empty, not a url, and not the same thing
        if (!TextUtils.isEmpty(addressToUpdate.getPhotoUrl())
                && !addressToUpdate.getPhotoUrl().startsWith("http")) {
            addressUpload = ConciergeAddressUploader.uploadConciergeAddressPhoto(getContext(), addressToUpdate.getPhotoUrl())
                    .concatMap(uploadResult -> {
                        if (uploadResult == null) {
                            uploadResult = new ConciergeAddressUploader.UploadResult();
                        }
                        addressToUpdate.setPhotoUrl(uploadResult.getUrl());
                        return Observable.just(uploadResult);
                    });
        }

        if (addressUpload != null) {
            observable = addressUpload.concatMap(uploadResult -> {
                Gson gson = new Gson();
                ConciergeUserAddress address = gson.fromJson(gson.toJson(addressToUpdate), ConciergeUserAddress.class);
                address.setId(null);
                return mApiService.updateConciergeUserAddress(addressToUpdate.getId(),
                                address,
                                LocaleManager.getAppLanguage(mContext),
                                ApplicationHelper.getRequestAPIMode(mContext))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            });
        } else {
            Gson gson = new Gson();
            ConciergeUserAddress address = gson.fromJson(gson.toJson(addressToUpdate), ConciergeUserAddress.class);
            address.setId(null);
            observable = mApiService.updateConciergeUserAddress(addressToUpdate.getId(),
                    address,
                    LocaleManager.getAppLanguage(mContext),
                    ApplicationHelper.getRequestAPIMode(mContext));
        }

        return observable;
    }

    public Observable<Response<SupportConciergeCustomErrorResponse>> deleteAddress(ConciergeUserAddress userAddress) {
        return mApiService.deleteConciergeUserAddress(userAddress.getId(), LocaleManager.getAppLanguage(mContext));
    }
}
