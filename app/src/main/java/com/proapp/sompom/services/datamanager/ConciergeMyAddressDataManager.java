package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Or Vitovongsak on 22/11/21.
 */
public class ConciergeMyAddressDataManager extends AbsLoadMoreDataManager {

    public ConciergeMyAddressDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<List<ConciergeUserAddress>> getUserAddresses(boolean isRefresh, boolean isLoadMore) {
        if (!isLoadMore) {
            resetPagination();
        }

        return mApiService.getConciergeUserAddress(Integer.parseInt(getCurrentPage()),
                        getPaginationSize(),
                        ApplicationHelper.getRequestAPIMode(mContext))
                .concatMap(loadMoreWrapperResponse -> {
                    if (loadMoreWrapperResponse.isSuccessful()) {
                        LoadMoreWrapper<ConciergeUserAddress> wrapper = loadMoreWrapperResponse.body();
                        checkPagination(wrapper);
                        return Observable.just(wrapper.getData());
                    } else {
                        return Observable.error(new ErrorThrowable(loadMoreWrapperResponse.code(),
                                mContext.getString(R.string.error_general_description)));
                    }
                });
    }
}
