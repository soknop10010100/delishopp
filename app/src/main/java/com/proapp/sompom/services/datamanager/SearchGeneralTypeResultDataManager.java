package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.broadcast.SoundEffectService;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.helper.UserSearchDataHolder;
import com.proapp.sompom.model.Search;
import com.proapp.sompom.model.SearchGeneralTypeResult;
import com.proapp.sompom.model.emun.GeneralSearchResultType;
import com.proapp.sompom.model.request.FollowBody;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SpecialTextRenderUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class SearchGeneralTypeResultDataManager extends AbsLoadMoreDataManager {

    private static final String HASH_TAG = "hashtag";
    private static final String NO_NEXT_PAGE = "0";

    private String mUserNextPage;
    private String mChatNextPage;
    private List<User> mCurrentLocalUserFilterResult = new ArrayList<>();

    public SearchGeneralTypeResultDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<SearchGeneralTypeResult>> searchGeneralByType(String keyword) {
        resetPagination();
        return mApiService.searchGeneralByType(keyword,
                SpecialTextRenderUtils.isContainHasTag(keyword) ? HASH_TAG : null,
                NO_NEXT_PAGE,
                mUserNextPage,
                NO_NEXT_PAGE,
                NO_NEXT_PAGE,
                mChatNextPage,
                getPaginationSize()).concatMap(response -> {
            checkLoadMore(response.body(), GeneralSearchResultType.ALL, false);
            if (response.body() != null) {
                if (response.body().getChatList() != null && response.body().getChatList().getData() != null) {
                    checkToInjectLocalParentConversation(response.body().getChatList().getData());
                }
                if (response.body().getUser() != null && response.body().getUser().getData() != null) {
                    UserHelper.updateContactUserInfo(getContext(), response.body().getUser().getData());
                    checkToExcludeUserThatExistInLocalResult(response.body().getUser().getData());
                    UserSearchDataHolder.sortUserResult(response.body().getUser().getData());
                }
            }
            return Observable.just(response);
        });
    }

    public boolean isCanLoadMore(GeneralSearchResultType resultType) {
        if (resultType == GeneralSearchResultType.ALL) {
            return isHasNexPage(mChatNextPage) || isHasNexPage(mUserNextPage);
        } else if (resultType == GeneralSearchResultType.PEOPLE) {
            return isHasNexPage(mUserNextPage);
        } else if (resultType == GeneralSearchResultType.MESSAGE) {
            return isHasNexPage(mChatNextPage);
        }
        return false;
    }

    private boolean isHasNexPage(String nextPge) {
        return !TextUtils.isEmpty(nextPge) && !TextUtils.equals(nextPge, NO_NEXT_PAGE);
    }

    public Observable<Response<List<Search>>> loadMore(String keyword, GeneralSearchResultType resultType) {
        String userNextPage = null;
        String chatNextPage = null;
        if (resultType == GeneralSearchResultType.ALL) {
            userNextPage = mUserNextPage;
            chatNextPage = mChatNextPage;
        } else if (resultType == GeneralSearchResultType.PEOPLE) {
            userNextPage = mUserNextPage;
        } else if (resultType == GeneralSearchResultType.MESSAGE) {
            chatNextPage = mChatNextPage;
        }

        final String finalUserNextPage = userNextPage;
        final String finalChatNextPage = chatNextPage;

//        Timber.i("loadMore=> chatNextPage: " + chatNextPage + ", userNextPage: " + userNextPage + "resultType: " + resultType);
        return mApiService.searchGeneralByType(keyword,
                SpecialTextRenderUtils.isContainHasTag(keyword) ? HASH_TAG : null,
                NO_NEXT_PAGE,
                userNextPage,
                NO_NEXT_PAGE,
                NO_NEXT_PAGE,
                chatNextPage,
                getPaginationSize()).concatMap(response -> {
            if (response.isSuccessful()) {
                //Need to add user first and then chat into result list.
                List<Search> result = new ArrayList<>();
                /*
                    Since server will also return user or chat list section data even those section have
                    no next page to load more. So we have to check if we have to add those result in
                    load more result or not base on its original next page status.
                 */
                boolean shouldAddLoadMoreUser = !TextUtils.isEmpty(finalUserNextPage) && !TextUtils.equals(finalUserNextPage, NO_NEXT_PAGE);
                boolean shouldAddLoadMoreChat = !TextUtils.isEmpty(finalChatNextPage) && !TextUtils.equals(finalChatNextPage, NO_NEXT_PAGE);
                if (response.body() != null) {
                    checkLoadMore(response.body(), resultType, true);
                    if (shouldAddLoadMoreUser && response.body().getUser() != null && response.body().getUser().getData() != null) {
                        UserHelper.updateContactUserInfo(getContext(), response.body().getUser().getData());
                        checkToExcludeUserThatExistInLocalResult(response.body().getUser().getData());
                        UserSearchDataHolder.sortUserResult(response.body().getUser().getData());
                        result.addAll(response.body().getUser().getData());
                    }

                    if (shouldAddLoadMoreChat && response.body().getChatList() != null && response.body().getChatList().getData() != null) {
                        checkToInjectLocalParentConversation(response.body().getChatList().getData());
                        result.addAll(response.body().getChatList().getData());
                    }
                }
                return Observable.just(Response.success(result));
            } else {
                return Observable.error(new ErrorThrowable(response.code(),
                        response.message()));
            }
        });
    }

    private void checkLoadMore(SearchGeneralTypeResult result,
                               GeneralSearchResultType resultType,
                               boolean isLoadMore) {
        /*
        Currently we check only user and chat sections for search result display now on mobile are
        only user and chat.
         */
        if (!isLoadMore || resultType == GeneralSearchResultType.ALL) {
            boolean hasLoadMore = false;
            if (result.getChatList() != null) {
                mChatNextPage = result.getChatList().getNextPage();
                hasLoadMore = !TextUtils.isEmpty(mChatNextPage);
            }
            if (result.getUser() != null) {
                mUserNextPage = result.getUser().getNextPage();
                if (!hasLoadMore) {
                    hasLoadMore = !TextUtils.isEmpty(mUserNextPage);
                }
            }
            setCanLoadMore(hasLoadMore);
        } else {
            if (resultType == GeneralSearchResultType.PEOPLE) {
                mUserNextPage = result.getUser().getNextPage();
            } else if (resultType == GeneralSearchResultType.MESSAGE) {
                mChatNextPage = result.getChatList().getNextPage();
            }
            setCanLoadMore(!TextUtils.isEmpty(mUserNextPage) || !TextUtils.isEmpty(mChatNextPage));
        }

//        Timber.i("checkLoadMore: mChatNextPage: " + mChatNextPage + ", mUserNextPage: " + mUserNextPage + ", resultType: " + resultType);
    }

    private void checkToInjectLocalParentConversation(List<Chat> chatList) {
        List<String> conversationIdList = new ArrayList<>();
        for (Chat chat : chatList) {
            if (!conversationIdList.contains(chat.getChannelId())) {
                conversationIdList.add(chat.getChannelId());
            }
        }

        String[] validIds = new String[conversationIdList.size()];
        conversationIdList.toArray(validIds);
        List<Conversation> conversationByIds = ConversationDb.getConversationByIds(mContext, validIds);
        if (conversationByIds != null && !conversationByIds.isEmpty()) {
            for (Chat chat : chatList) {
                for (Conversation conversationById : conversationByIds) {
                    if (TextUtils.equals(conversationById.getId(), chat.getChannelId())) {
                        chat.setParentConversation(conversationById);
                    }
                }
            }
        }
    }

    public Observable<Response<Conversation>> getConversationDetail(String conversationId) {
        return mApiService.getConversationDetail(conversationId);
    }

    public void setCurrentLocalUserFilterResult(List<User> currentLocalUserFilterResult) {
        mCurrentLocalUserFilterResult = currentLocalUserFilterResult;
    }

    private void checkToExcludeUserThatExistInLocalResult(List<User> responseUserList) {
        if (mCurrentLocalUserFilterResult != null && !mCurrentLocalUserFilterResult.isEmpty()) {
            List<User> addUserList = new ArrayList<>();
            for (User user : mCurrentLocalUserFilterResult) {
                boolean existed = false;
                for (User user1 : responseUserList) {
                    if (TextUtils.equals(user.getId(), user1.getId())) {
                        existed = true;
                        break;
                    }
                }

                if (!existed) {
                    addUserList.add(user);
                }
            }

            responseUserList.addAll(addUserList);
        }
    }

    public Observable<Response<Object>> postFollow(String followStoreId) {
        FollowBody body = new FollowBody(followStoreId);
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
        return mApiService.postFollow(body);
    }


    public Observable<Response<Object>> unFollow(String followStoreId) {
        FollowBody body = new FollowBody(followStoreId);
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        return mApiService.unFollow(body);
    }

    @Override
    public void resetPagination() {
        mUserNextPage = NO_NEXT_PAGE;
        mChatNextPage = NO_NEXT_PAGE;
    }
}
