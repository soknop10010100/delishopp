package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.database.MessageDb;
import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.request.SearchGroupMessageRequestBody;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.services.ApiService;

import java.util.List;

import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by Veasna Chhom on 8/7/20.
 */

public class SearchConversationDataManager extends AbsLoadMoreDataManager {

    private GroupDetail mGroupDetail;
    private Chat mOldestMessage;

    public SearchConversationDataManager(Context context, GroupDetail groupDetail, ApiService apiService) {
        super(context, apiService);
        mContext = context;
        mGroupDetail = groupDetail;
    }

    public Observable<List<Chat>> searchMessageFromServer(String query, boolean isLoadMore) {
        if (!isLoadMore) {
            resetPagination();
            mOldestMessage = null;
        }
        return mApiService.searchGroupConversation(new SearchGroupMessageRequestBody(mGroupDetail.getId(),
                        query),
                getCurrentPage(),
                getPaginationSize()).concatMap(loadMoreWrapperResponse -> {
            Timber.i("searchMessageFromServer: isSuccessful: " + loadMoreWrapperResponse.isSuccessful());
            if (loadMoreWrapperResponse.isSuccessful()) {
                checkPagination(loadMoreWrapperResponse.body());
                if (!loadMoreWrapperResponse.body().getData().isEmpty()) {
                    assignOldestMessage(loadMoreWrapperResponse.body().getData());
                }
                return Observable.just(loadMoreWrapperResponse.body().getData());

            } else {
                Timber.e("searchMessageFromServer is not successful: " + loadMoreWrapperResponse.message());
                return searchMessageFromLocal(query, isLoadMore);
            }
        }).onErrorResumeNext(throwable -> {
            Timber.e("searchMessageFromServer: error: " + throwable);
            return searchMessageFromLocal(query, isLoadMore);
        });
    }

    private void assignOldestMessage(List<Chat> chatList) {
        if (!chatList.isEmpty()) {
            mOldestMessage = chatList.get(chatList.size() - 1);
        } else {
            mOldestMessage = null;
        }
    }

    private Observable<List<Chat>> searchMessageFromLocal(String query, boolean isLoadMore) {
        if (!isLoadMore) {
            mOldestMessage = null;
        }
        return MessageDb.queryLocalWithPagination(mContext,
                query,
                mGroupDetail,
                mOldestMessage,
                getPaginationSize()).concatMap(chatList -> {
            assignOldestMessage(chatList);
            setCanLoadMore(chatList.size() >= getPaginationSize());
            return Observable.just(chatList);
        });
    }
}
