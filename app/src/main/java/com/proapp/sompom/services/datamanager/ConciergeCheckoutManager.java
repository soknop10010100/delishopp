package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.GsonHelper;
import com.proapp.sompom.helper.PreAPIRequestHelper;
import com.proapp.sompom.model.ConciergeCartModel;
import com.proapp.sompom.model.ConciergeOrderItemDisplayAdaptive;
import com.proapp.sompom.model.ShopDeliveryTimeSelection;
import com.proapp.sompom.model.concierge.ConciergeBasket;
import com.proapp.sompom.model.concierge.ConciergeCoupon;
import com.proapp.sompom.model.concierge.ConciergeGetDeliveryFeeRequest;
import com.proapp.sompom.model.concierge.ConciergeGetDeliveryFeeResponse;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeOnlinePaymentProvider;
import com.proapp.sompom.model.concierge.ConciergeOrderRequest;
import com.proapp.sompom.model.concierge.ConciergeOrderRequestWithABAPayWay;
import com.proapp.sompom.model.concierge.ConciergeOrderResponseWithABAPayWay;
import com.proapp.sompom.model.concierge.ConciergeShop;
import com.proapp.sompom.model.concierge.ConciergeUpdateBasketRequest;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.concierge.ConciergeVerifyPaymentWithABAResponse;
import com.proapp.sompom.model.emun.ConciergeOnlinePaymentOption;
import com.proapp.sompom.model.result.ConciergeCheckoutData;
import com.proapp.sompom.model.result.ConciergeOrderResponse;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.resourcemanager.helper.LocaleManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Response;
import timber.log.Timber;

public class ConciergeCheckoutManager extends AbsLoadMoreDataManager {

    private final Gson mGson;
    private List<ConciergeOnlinePaymentProvider> mPaymentOptions;
    private String mBasketId;
    private ConciergeCheckoutData mConciergeCheckoutData;
    private ConciergeUserAddress mConciergeUserPrimaryAddress;
    private double mWalletBalance;

    public ConciergeCheckoutManager(Context context, ApiService apiService) {
        super(context, apiService);
        mGson = GsonHelper.getSupportedGSonGMTDate();
        Timber.i("mBasketId: " + ConciergeHelper.getBasketId(mContext));
    }

    public List<ConciergeOnlinePaymentProvider> getPaymentOptions() {
        return mPaymentOptions;
    }

    public Observable<Response<ConciergeVerifyPaymentWithABAResponse>> verifyOrderPaymentStatus() {
        return ConciergeHelper.verifyOrderPaymentStatus(mContext, mApiService).concatMap(result -> {
            if (result.isSuccessful() && result.body() != null && !result.body().isPaid()) {
                //Will auto enable back basket from being locked of payment.
                return mApiService.enableBasket(ApplicationHelper.getRequestAPIMode(mContext)).concatMap(response ->
                        Observable.just(result)).concatMap(conciergeVerifyPaymentWithABAResponseResponse -> mApiService.getUserWallet().concatMap(walletResponseResponse -> {
                    if (walletResponseResponse.isSuccessful() && walletResponseResponse.body() != null) {
                        mWalletBalance = walletResponseResponse.body().getBalance();
                        Timber.i("mWalletBalance: " + mWalletBalance);
                    }
                    return Observable.just(result);
                }));
            }

            return Observable.just(result);
        });
    }

    public void setConciergeUserPrimaryAddress(ConciergeUserAddress conciergeUserPrimaryAddress) {
        mConciergeUserPrimaryAddress = conciergeUserPrimaryAddress;
    }

    public double getWalletBalance() {
        return mWalletBalance;
    }

    public Observable<Response<ConciergeOrderResponse>> getRequestOrderWithCash(ShopDeliveryTimeSelection shopDeliveryTimeSelection,
                                                                                String oosOption,
                                                                                String couponId,
                                                                                String deliveryInstruction,
                                                                                ConciergeUserAddress userAddress,
                                                                                boolean isAppliedWallet) {
//        Timber.i("shopDeliveryTimeSelection: " + shopDeliveryTimeSelection.toString());
        return getMenuItemFromBasket().concatMap(conciergeMenuItems -> {
            ConciergeOrderRequest request = new ConciergeOrderRequest();
            request.setDeliveryInstruction(deliveryInstruction);
            request.setOOSOption(oosOption);

            if (mConciergeCheckoutData.getCartModel().getConciergeCoupon() != null &&
                    mConciergeCheckoutData.getCartModel().getConciergeCoupon().getmCouponCode() != null) {
                request.setCouponId(mConciergeCheckoutData.getCartModel().getConciergeCoupon().getCouponCodeId());
            } else  {
                request.setCouponId("");
            }

            if (mConciergeCheckoutData.getCartModel().getConciergeCoupon() != null &&
                    mConciergeCheckoutData.getCartModel().getConciergeCoupon().getDeliveryInfo().getDeliveryFee() == 0) {
                request.setDeliveryFee(0f);
            } else {
                request.setDeliveryFee(mConciergeCheckoutData.getDeliveryCharge());
            }

            request.setContainerFee(mConciergeCheckoutData.getContainerCharge());

            if (mConciergeCheckoutData.getCartModel().getConciergeCoupon() != null) {
                request.setTotalDiscount((float) mConciergeCheckoutData.getCartModel().getConciergeCoupon().getDiscountPrice());
            } else {
                request.setTotalDiscount(0f);
            }

            request.setAppliedWallet(isAppliedWallet);
            request.setWalletAmount(mWalletBalance);
            buildOrderDelivery(request, shopDeliveryTimeSelection, userAddress);

            if (!conciergeMenuItems.isEmpty()) {
                request.setMenuItems(ConciergeHelper.parseOrderMenuItems(conciergeMenuItems));
            }

            JsonObject requestJson = GsonHelper.getSupportedGSonGMTDate().fromJson(mGson.toJson(request), JsonObject.class);
            Timber.i("request: " + requestJson.toString());

            return mApiService.orderWithCash(requestJson, ApplicationHelper.getRequestAPIMode(mContext), LocaleManager.getAppLanguage(mContext));
        });
    }

    public Observable<Response<Object>> clearCartData() {
        return ConciergeCartHelper.clearCart(mContext).concatMap(o -> Observable.just(Response.success(new Object())));
    }

    public Observable<Response<ConciergeOrderResponseWithABAPayWay>> getRequestOrderWithABA(ShopDeliveryTimeSelection shopDeliveryTimeSelection,
                                                                                            String oosOption,
                                                                                            String couponId,
                                                                                            String deliveryInstruction,
                                                                                            ConciergeUserAddress userAddress,
                                                                                            ConciergeOnlinePaymentProvider paymentProvider,
                                                                                            boolean isAppliedWallet) {
        return getMenuItemFromBasket().concatMap(conciergeMenuItems -> {
            ConciergeOrderRequest request = new ConciergeOrderRequest();
            request.setOOSOption(oosOption);
            request.setCouponId(couponId);
            request.setPaymentOption(paymentProvider.getProvider());
            request.setDeliveryInstruction(deliveryInstruction);
            request.setDeliveryFee(mConciergeCheckoutData.getDeliveryCharge());
            request.setContainerFee(mConciergeCheckoutData.getContainerCharge());
            if (mConciergeCheckoutData.getCartModel().getConciergeCoupon() != null) {
                request.setTotalDiscount( (float) mConciergeCheckoutData.getCartModel().getConciergeCoupon().getDiscountPrice());
            } else {
                request.setTotalDiscount(0);
            }
            request.setAppliedWallet(isAppliedWallet);
            request.setWalletAmount(mWalletBalance);
            buildOrderDelivery(request, shopDeliveryTimeSelection, userAddress);

            if (!conciergeMenuItems.isEmpty()) {
                request.setMenuItems(ConciergeHelper.parseOrderMenuItems(conciergeMenuItems));
            }
            ConciergeOrderRequestWithABAPayWay orderRequestWithABAPayWay = new ConciergeOrderRequestWithABAPayWay(request);
            return mApiService.orderWithABAPayWay(GsonHelper.getSupportedGSonGMTDate().fromJson(mGson.toJson(orderRequestWithABAPayWay),
                            JsonObject.class),
                    ApplicationHelper.getRequestAPIMode(mContext),
                    LocaleManager.getAppLanguage(mContext)).concatMap(responseObservable -> {
                Timber.i("Success: " + new Gson().toJson(responseObservable.body()));
                return Observable.just(responseObservable);
            });
        });
    }

    public Observable<Response<List<ConciergeOnlinePaymentProvider>>> getOnlinePaymentOptions() {
        return mApiService.getOnlinePaymentProvider().concatMap(response -> {
            if (response.isSuccessful() && response.body() != null) {
                //Validate the payment type
                List<ConciergeOnlinePaymentProvider> validList = new ArrayList<>();
                ConciergeOnlinePaymentOption[] values = ConciergeOnlinePaymentOption.values();
                for (ConciergeOnlinePaymentProvider conciergeOnlinePaymentProvider : response.body()) {
                    for (ConciergeOnlinePaymentOption value : values) {
                        if (value == ConciergeOnlinePaymentOption.fromValue(conciergeOnlinePaymentProvider.getProvider())) {
                            validList.add(conciergeOnlinePaymentProvider);
                            break;
                        }
                    }
                }
                mPaymentOptions = validList;
                return Observable.just(Response.success(mPaymentOptions));
            } else {
                return Observable.just(Response.error(response.code(), response.errorBody()));
            }
        });
    }

    private void buildOrderDelivery(ConciergeOrderRequest request,
                                    ShopDeliveryTimeSelection deliverySelection,
                                    ConciergeUserAddress userAddress) {
        Timber.i("buildOrderDelivery: \n" + deliverySelection.toString());
        Timber.i("user address: " + new Gson().toJson(userAddress));

        request.setTimeSlotId(deliverySelection.getTimeSlotId());
        request.setIsProvince(deliverySelection.isProvince());
        if (userAddress != null) {
            request.setAddressId(userAddress.getId());
            if (TextUtils.isEmpty(userAddress.getCountryCode()) ||
                    TextUtils.isEmpty(userAddress.getPhone())) {
                userAddress.setCountryCode(SharedPrefUtils.getString(mContext, SharedPrefUtils.COUNTRY_CODE));
                userAddress.setPhone(SharedPrefUtils.getPhone(mContext));
            }
            request.setDeliveryAddress(userAddress);
        }
        Timber.d("Build order delivery: " + new Gson().toJson(request));
    }

    private ConciergeShop getSingleShopFromItemList(List<ConciergeMenuItem> conciergeMenuItemList) {
        for (ConciergeMenuItem conciergeMenuItem : conciergeMenuItemList) {
            if (conciergeMenuItem.getShop() != null) {
                return conciergeMenuItem.getShop();
            }
        }

        return null;
    }

    public Observable<Response<ConciergeCheckoutData>> getCheckoutData(boolean isFirstLoadData, boolean shouldRequestUserWallet) {
        if (isFirstLoadData) {
            return PreAPIRequestHelper.requestUserBasket(mContext, mApiService, true, false).
                    concatMap(conciergeBasketResponse -> {
                        mBasketId = ConciergeHelper.getBasketId(mContext);
                        Timber.i("mBasketId: " + mBasketId);
                        return mApiService.getUserWallet().concatMap(walletResponseResponse -> {
                            if (walletResponseResponse.isSuccessful() && walletResponseResponse.body() != null) {
                                mWalletBalance = walletResponseResponse.body().getBalance();
                                Timber.i("mWalletBalance: " + mWalletBalance);
                            }

                            return getOnlinePaymentOptions().concatMap((Function<Response<List<ConciergeOnlinePaymentProvider>>,
                                    ObservableSource<Response<ConciergeCheckoutData>>>) listResponse -> {
                                if (!listResponse.isSuccessful()) {
                                    return Observable.just(Response.error(listResponse.code(), listResponse.errorBody()));
                                } else {
                                    return ConciergeCartHelper.getCart().concatMap(conciergeCartModel -> {
                                        mConciergeCheckoutData = new ConciergeCheckoutData();
                                        ConciergeHelper.loadUserInfo(mContext, mConciergeCheckoutData);
                                        mConciergeCheckoutData.setSingleShop(getSingleShopFromItemList(conciergeCartModel.getBasketItemList()));
                                        mConciergeCheckoutData.setSingleShop(getSingleShopFromItemList(conciergeCartModel.getBasketItemList()));
                                        mConciergeCheckoutData.setCartModel(conciergeCartModel);
                                        mConciergeCheckoutData.setItemList(parseDisplayOrderItem(conciergeCartModel));

                                        ConciergeUserAddress userAddress;
                                        if (conciergeBasketResponse.body().getDeliveryAddress() != null) {
                                            userAddress = conciergeBasketResponse.body().getDeliveryAddress();
                                            return getDeliveryFee(userAddress).concatMap(conciergeGetDeliveryFeeResponseResponse ->
                                                    Observable.just(Response.success(mConciergeCheckoutData)));
                                        } else {
                                            userAddress = mConciergeUserPrimaryAddress;
                                            ConciergeUpdateBasketRequest request = new ConciergeUpdateBasketRequest();
                                            request.setDeliveryAddress(mConciergeUserPrimaryAddress);
                                            //Must add the address too basket before request delivery fee
                                            return mApiService.updateBasket(request,
                                                    ApplicationHelper.getRequestAPIMode(mContext),
                                                    LocaleManager.getAppLanguage(mContext)).concatMap(conciergeBasketResponse1 ->
                                                    getDeliveryFee(userAddress).concatMap(conciergeGetDeliveryFeeResponseResponse ->
                                                            Observable.just(Response.success(mConciergeCheckoutData))));
                                        }
                                    });
                                }
                            });
                        });
                    });
        } else {
            if (shouldRequestUserWallet) {
                return mApiService.getUserWallet().concatMap(conciergeGetWalletResponseResponse -> {
                    if (conciergeGetWalletResponseResponse.isSuccessful() && conciergeGetWalletResponseResponse.body() != null) {
                        mWalletBalance = conciergeGetWalletResponseResponse.body().getBalance();
                        Timber.i("mWalletBalance: " + mWalletBalance);
                    }

                    return ConciergeCartHelper.getCart().concatMap(conciergeCartModel -> {
                        mConciergeCheckoutData = new ConciergeCheckoutData();
                        ConciergeHelper.loadUserInfo(mContext, mConciergeCheckoutData);
                        mConciergeCheckoutData.setDeliveryCharge((float) conciergeCartModel.getDeliveryFee());
                        mConciergeCheckoutData.setContainerCharge((float) conciergeCartModel.getContainerFee());
                        mConciergeCheckoutData.setSingleShop(getSingleShopFromItemList(conciergeCartModel.getBasketItemList()));
                        mConciergeCheckoutData.setCartModel(conciergeCartModel);
                        mConciergeCheckoutData.setItemList(parseDisplayOrderItem(conciergeCartModel));
                        return Observable.just(Response.success(mConciergeCheckoutData));
                    });
                });
            } else {
                return ConciergeCartHelper.getCart().concatMap(conciergeCartModel -> {
                    mConciergeCheckoutData = new ConciergeCheckoutData();
                    ConciergeHelper.loadUserInfo(mContext, mConciergeCheckoutData);
                    mConciergeCheckoutData.setDeliveryCharge((float) conciergeCartModel.getDeliveryFee());
                    mConciergeCheckoutData.setContainerCharge((float) conciergeCartModel.getContainerFee());
                    mConciergeCheckoutData.setSingleShop(getSingleShopFromItemList(conciergeCartModel.getBasketItemList()));
                    mConciergeCheckoutData.setCartModel(conciergeCartModel);
                    mConciergeCheckoutData.setItemList(parseDisplayOrderItem(conciergeCartModel));
                    return Observable.just(Response.success(mConciergeCheckoutData));
                });
            }
        }
    }

    public Observable<List<ConciergeMenuItem>> getMenuItemFromBasket() {
        return ConciergeCartHelper.getCart().concatMap(cartModel -> {
            if (cartModel.getBasketItemList() != null) {
                return Observable.just(cartModel.getBasketItemList());
            }

            return Observable.just(new ArrayList<>());

        });
    }

    public Observable<Response<SupportConciergeCustomErrorResponse>> clearBasket() {
        return mApiService.clearBasket(ApplicationHelper.getRequestAPIMode(mContext)).concatMap(response -> {
            if (response.isSuccessful() && response.body() != null && !response.body().isError()) {
                return ConciergeCartHelper.clearCart(mContext).concatMap(o -> Observable.just(response));
            } else {
                return Observable.just(response);
            }
        });
    }

    private List<ConciergeOrderItemDisplayAdaptive> parseDisplayOrderItem(ConciergeCartModel cartModel) {
        List<ConciergeOrderItemDisplayAdaptive> list = new ArrayList<>();
        if (cartModel.getBasketItemList() != null && !cartModel.getBasketItemList().isEmpty()) {
            for (ConciergeMenuItem conciergeMenuItem : cartModel.getBasketItemList()) {
                if (!conciergeMenuItem.isSoftDeleted()) {
                    list.add(conciergeMenuItem);
                }
            }
        }

        return list;
    }

    public Observable<Response<ConciergeCoupon>> getApplyCouponService(String couponId , String type , String orderId) {
        return mApiService.getCoupon(couponId, LocaleManager.getAppLanguage(mContext) ,type,orderId).concatMap(conciergeCouponResponse -> {
            if (conciergeCouponResponse.isSuccessful() &&
                    conciergeCouponResponse.body() != null &&
                    !conciergeCouponResponse.body().isError()) {
                return applyCouponToLocalBasket(conciergeCouponResponse.body()).concatMap(isSuccessful -> {
                    conciergeCouponResponse.body().setAppliedCouponValid(isSuccessful.body());
                    return Observable.just(conciergeCouponResponse);
                });
            }

            return Observable.just(conciergeCouponResponse);
        });
    }

    public Observable<Response<Boolean>> applyCouponToLocalBasket(ConciergeCoupon conciergeCoupon) {
        mConciergeCheckoutData.getCartModel().setConciergeCoupon(conciergeCoupon);
        mConciergeCheckoutData.getCartModel().setCouponId(conciergeCoupon.getId());
        Timber.i("applyCouponToLocalBasket: " + new Gson().toJson(mConciergeCheckoutData.getCartModel()));
        ConciergeCartHelper.calculateCartTotalPrice(mConciergeCheckoutData.getCartModel());
        conciergeCoupon.setIsTotalPriceBelowCouponMinBasketPrice(mConciergeCheckoutData.getCartModel().isTotalPriceBelowCouponMinBasketPrice());
        if (!mConciergeCheckoutData.getCartModel().isAppliedCouponValueValid()) {
            mConciergeCheckoutData.getCartModel().setConciergeCoupon(null);
            mConciergeCheckoutData.getCartModel().setCouponId(null);
            mConciergeCheckoutData.getCartModel().clearCouponPrice();
            return Observable.just(Response.success(Boolean.FALSE));
        } else {
            return ConciergeCartHelper.saveBasket(mContext, mConciergeCheckoutData.getCartModel()).concatMap(cartModel ->
                    Observable.just(Response.success(Boolean.TRUE)));
        }
    }

    public Observable<Response<ConciergeCartModel>> removeCouponToLocalBasket() {
        mConciergeCheckoutData.getCartModel().setConciergeCoupon(null);
        mConciergeCheckoutData.getCartModel().setCouponId(null);
        mConciergeCheckoutData.getCartModel().clearCouponPrice();

        return ConciergeCartHelper.saveBasket(mContext, mConciergeCheckoutData.getCartModel()).concatMap(cartModel ->
                Observable.just(Response.success(cartModel)));
    }

    public Observable<Response<ConciergeGetDeliveryFeeResponse>> getDeliveryFee(ConciergeUserAddress userAddress) {

        ConciergeGetDeliveryFeeRequest request = new ConciergeGetDeliveryFeeRequest(userAddress.getId(),
                mConciergeCheckoutData.getSingleShop().getId(),
                mConciergeCheckoutData.getCartModel().getTotalPrice(),
                ApplicationHelper.getRequestAPIMode(mContext) , mConciergeCheckoutData.getCartModel().getBasketId() );
        return mApiService.getDeliveryFee(request, LocaleManager.getAppLanguage(mContext)).concatMap(response -> {
            if (response.isSuccessful() && response.body() != null) {
                mConciergeCheckoutData.setDeliveryCharge((float) response.body().getDeliveryFee());
                mConciergeCheckoutData.setContainerCharge((float) response.body().getContainerFee());
                mConciergeCheckoutData.getCartModel().setDeliveryAddress(userAddress);
                if (!response.body().isError()) {
                    mConciergeCheckoutData.getCartModel().setDeliveryFee(response.body().getDeliveryFee());
                    mConciergeCheckoutData.getCartModel().setContainerFee(response.body().getContainerFee());
                } else {
                    mConciergeCheckoutData.getCartModel().setDeliveryFee(0);
                    mConciergeCheckoutData.getCartModel().setContainerFee(0);
                }
                Timber.i("Delivery fee: " + mConciergeCheckoutData.getCartModel().getDeliveryFee());
                return ConciergeCartHelper.saveBasket(mContext, mConciergeCheckoutData.getCartModel()).concatMap(cartModel ->
                        Observable.just(response));
            }

            return Observable.just(response);
        });
    }

    public Observable<Response<ConciergeCartModel>> updateLocalBasket(ConciergeBasket remoteBasket) {
        ConciergeCartModel conciergeCartModel = ConciergeHelper.transformRemoteBasketIntoLocalBasket(remoteBasket);
        return ConciergeCartHelper.saveBasket(mContext, conciergeCartModel).concatMap(conciergeCartModel1 -> {
            ConciergeHelper.broadcastRefreshLocalBasketEvent(mContext);
            return Observable.just(Response.success(conciergeCartModel1));
        });
    }
}
