package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.request.AddOrRemoveGroupOwnerBody;
import com.proapp.sompom.model.request.AddOrRemoveGroupParticipantBody;
import com.proapp.sompom.services.ApiService;

import java.util.Collections;

import io.reactivex.Observable;
import retrofit2.Response;


/**
 * Created by Veasna Chhom on 7/24/20.
 */

public class GroupDetailDataManager extends AbsLoadMoreDataManager {

    private String mGroupId;

    public GroupDetailDataManager(Context context, ApiService apiService, String groupId) {
        super(context, apiService);
        mGroupId = groupId;
    }

    public Observable<Response<GroupDetail>> updateGroupDetail(GroupDetail groupDetail) {
        return mApiService.updateGroup(mGroupId, groupDetail);
    }

    public Observable<Response<GroupDetail>> getGroupDetail() {
        return mApiService.getGroupDetail(mGroupId);
    }

    public Observable<Response<GroupDetail>> removeGroupParticipant(String userId) {
        return mApiService.removeGroupParticipants(new AddOrRemoveGroupParticipantBody(mGroupId,
                Collections.singletonList(userId)));
    }

    public Observable<Response<GroupDetail>> removeGroupOwner(String userId) {
        return mApiService.removeGroupOwners(new AddOrRemoveGroupOwnerBody(mGroupId,
                Collections.singletonList(userId)));
    }
}
