package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.R;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import retrofit2.Response;

public class ConciergeOrderHistoryDataManager extends AbsLoadMoreDataManager {

    private final boolean mIsLoadCurrentOrder;

    public ConciergeOrderHistoryDataManager(Context context, ApiService apiService, boolean isLoadCurrentOrder) {
        super(context, apiService);
        mIsLoadCurrentOrder = isLoadCurrentOrder;
    }

    public Observable<List<ConciergeOrder>> getConciergeOrderHistoryService(boolean isLoadMore) {
        if (!isLoadMore) {
            resetPagination();
        }

        return getHistoryService()
                .concatMap(loadMoreWrapperResponse -> {
                    if (loadMoreWrapperResponse.isSuccessful()) {
                        LoadMoreWrapper<ConciergeOrder> wrapper = loadMoreWrapperResponse.body();
                        checkPagination(wrapper);
                        return Observable.just(wrapper.getData());
                    } else {
                        return Observable.error(new ErrorThrowable(loadMoreWrapperResponse.code(),
                                mContext.getString(R.string.error_general_description)));
                    }
                });
    }

    private Observable<Response<LoadMoreWrapper<ConciergeOrder>>> getHistoryService() {
        if (mIsLoadCurrentOrder) {
            return mApiService.getConciergeTrackingOrder(getCurrentPage(),
                    getPaginationSize(),
                    LocaleManager.getAppLanguage(mContext));
        } else {
            return mApiService.getConciergeOrderHistory(getCurrentPage(),
                    getPaginationSize(),
                    LocaleManager.getAppLanguage(mContext));
        }
    }
}
