package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.GsonHelper;
import com.proapp.sompom.helper.PreAPIRequestHelper;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.ConciergeItemAdaptive;
import com.proapp.sompom.model.concierge.ConciergeBrandSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeCarouselSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeFeatureStoreSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeNewArrivalProductSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeOurServiceSectionResponse;
import com.proapp.sompom.model.concierge.ConciergePushItemSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeShopAnnouncementResponse;
import com.proapp.sompom.model.concierge.ConciergeShopSetting;
import com.proapp.sompom.model.concierge.ConciergeSupplierSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeTrendingSectionResponse;
import com.proapp.sompom.model.emun.ConciergeMainSectionType;
import com.proapp.sompom.model.result.ConciergeCheckTrackingOrderResponse;
import com.proapp.sompom.model.result.ConciergeShopFeature;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Response;

public class ConciergeFragmentDataManager extends AbsConciergeProductDataManager {

    private final Context mContext;
    private final ApiService mApiService;

    private int mCurrentOffset = 0;
    private boolean mIsHasPending;
    private ConciergeShopAnnouncementResponse mConciergeShopAnnouncement;
    private boolean mIsInExpressMode;

    public ConciergeFragmentDataManager(Context context, ApiService apiService, boolean isInExpressMode) {
        super(context, apiService);
        mIsInExpressMode = isInExpressMode;
        mContext = context;
        mApiService = apiService;
        setPaginationSize(20);
    }

    public boolean isHasPending() {
        return mIsHasPending;
    }

    public ConciergeShopAnnouncementResponse getConciergeShopAnnouncement() {
        return mConciergeShopAnnouncement;
    }

    public Observable<Response<List<ConciergeItemAdaptive>>> getShopFeature(boolean isLoadMoreData) {
        if (!isLoadMoreData) {
            resetPagination();
        }

        if (!isLoadMoreData) {
            return PreAPIRequestHelper.requestUserBasket(mContext, mApiService, true, false).concatMap(conciergeBasketResponse -> getShopSettingService()
                    .concatMap((Function<Response<ConciergeShopSetting>, ObservableSource<Response<List<ConciergeItemAdaptive>>>>)
                            conciergeShopSettingResponse -> getShopAnnouncementService().concatMap(conciergeShopSettingResponse1 ->
                                    ConciergeFragmentDataManager.this.getShopFeatureInternally(isLoadMoreData))));
        } else {
            return getShopFeatureInternally(isLoadMoreData);
        }
    }

    private Observable<Response<ConciergeShopSetting>> getShopSettingService() {
        return mApiService.getShopSetting(ApplicationHelper.getUserId(mContext)).concatMap(conciergeShopSettingResponse -> {
            if (conciergeShopSettingResponse.isSuccessful() && conciergeShopSettingResponse.body() != null) {
                ConciergeHelper.setConciergeShopSetting(mContext, conciergeShopSettingResponse.body());
            }
            return Observable.just(conciergeShopSettingResponse);
        });
    }

    public Observable<Response<ConciergeShopAnnouncementResponse>> getShopAnnouncementService() {
        return mApiService.getShopAnnouncement().concatMap(conciergeShopSettingResponse -> {
            mConciergeShopAnnouncement = conciergeShopSettingResponse.body();
            return Observable.just(conciergeShopSettingResponse);
        });
    }

    private Observable<Response<List<ConciergeItemAdaptive>>> getShopFeatureInternally(boolean isLoadMoreTrendingMenuItem) {
        return getShopFeatureAPIService()
                .concatMap(shopFeatureResponse -> {
                    if (shopFeatureResponse.isSuccessful() && shopFeatureResponse.body() != null) {
                        ConciergeShopFeature conciergeShopFeature = shopFeatureResponse.body();
                        if (!ApplicationHelper.isInVisitorMode(getContext())) {
                            return mApiService.checkPendingOrderStatus().onErrorResumeNext(throwable -> {
                                return Observable.just(Response.success(new ConciergeCheckTrackingOrderResponse()));
                            }).concatMap(conciergeCheckTrackingOrderResponseResponse -> {
                                if (conciergeCheckTrackingOrderResponseResponse.isSuccessful() && conciergeCheckTrackingOrderResponseResponse.body() != null) {
                                    conciergeShopFeature.setIsHasPendingOrder(conciergeCheckTrackingOrderResponseResponse.body().isHasPendingOrder());
                                }

                                return Observable.just(Response.success(parseShopFeature(conciergeShopFeature,
                                        isLoadMoreTrendingMenuItem)));
                            });
                        } else {
                            return Observable.just(Response.success(parseShopFeature(conciergeShopFeature,
                                    isLoadMoreTrendingMenuItem)));
                        }
                    } else {
                        resetPagination();
                        return Observable.just(Response.error(shopFeatureResponse.code(),
                                shopFeatureResponse.errorBody()));
                    }
                });
    }

    private Observable<Response<ConciergeShopFeature>> getShopFeatureAPIService() {
        if (ApplicationHelper.isInExpressMode(mContext)) {
            return mApiService.getExpressShopFeature(mCurrentOffset, getPaginationSize(), LocaleManager.getAppLanguage(mContext));
        } else {
            return mApiService.getShopFeature(mCurrentOffset, getPaginationSize(), LocaleManager.getAppLanguage(mContext), ApplicationHelper.getUserId(mContext));
        }
    }

    private List<ConciergeItemAdaptive> parseShopFeature(ConciergeShopFeature conciergeShopFeature, boolean isLoadMoreData) {
        /*
           If the data is for load more trending item, we will not add other section data item into list.
           We will just add the trending item.
         */

        // Check if current user has pending order
        mIsHasPending = conciergeShopFeature.isHasPendingOrder();

        List<ConciergeItemAdaptive> list = new ArrayList<>();
        Gson gson = GsonHelper.getGsonForAPICommunication();
        for (JsonObject object : conciergeShopFeature.getSection()) {
            if (object.has(AbsConciergeModel.FIELD_TYPE)) {
                ConciergeMainSectionType type = ConciergeMainSectionType.from(object.get(AbsConciergeModel.FIELD_TYPE).getAsString());
                if (type == ConciergeMainSectionType.BUTTON && !isLoadMoreData) {
                    ConciergeOurServiceSectionResponse ourServiceSectionResponse =
                            gson.fromJson(object.toString(), ConciergeOurServiceSectionResponse.class);
                    if (!TextUtils.isEmpty(ourServiceSectionResponse.getData().getShopId()) &&
                            !TextUtils.isEmpty(ourServiceSectionResponse.getData().getIcon()) &&
                            !TextUtils.isEmpty(ourServiceSectionResponse.getData().getLabel())) {
                        list.add(ourServiceSectionResponse);
                    }
                } else if (type == ConciergeMainSectionType.CAROUSEL && !isLoadMoreData) {
                    ConciergeCarouselSectionResponse carouselSectionResponse =
                            gson.fromJson(object.toString(), ConciergeCarouselSectionResponse.class);
                    if (!carouselSectionResponse.getData().isEmpty()) {
                        list.add(carouselSectionResponse);
                    }
                } else if (type == ConciergeMainSectionType.TREND_SHOP && !isLoadMoreData) {
                    ConciergeFeatureStoreSectionResponse featureStoreSectionResponse =
                            gson.fromJson(object.toString(), ConciergeFeatureStoreSectionResponse.class);

                    if (!featureStoreSectionResponse.getData().isEmpty()) {
                        list.add(featureStoreSectionResponse);
                    }
                } else if (type == ConciergeMainSectionType.TREND_ITEM) {
                    ConciergeTrendingSectionResponse trendingSectionResponse = gson.fromJson(object.toString(),
                            ConciergeTrendingSectionResponse.class);
                    if (!trendingSectionResponse.getData().isEmpty()) {
                        /*
                           For trending item, we manage the view type directly in main adapter list,
                           so we have to differentiate the section title and normal item of trending item.
                           When load more trending item, we will add the load more result directly in
                           main adapter data.
                        */
                        if (!isLoadMoreData) {
                            //To display trending item section title
                            list.add(trendingSectionResponse);
                        }
                        list.addAll(trendingSectionResponse.getData());
                    }
                } else if (type == ConciergeMainSectionType.PUSH_ITEM && !isLoadMoreData) {
                    ConciergePushItemSectionResponse pushItemSectionResponse = gson.fromJson(object.toString(),
                            ConciergePushItemSectionResponse.class);
                    if (!pushItemSectionResponse.getData().isEmpty()) {
                        list.add(pushItemSectionResponse);
                    }
                } else if (type == ConciergeMainSectionType.NEW_ARRIVAL && !isLoadMoreData) {
                    ConciergeNewArrivalProductSectionResponse productSectionResponse = gson.fromJson(object.toString(),
                            ConciergeNewArrivalProductSectionResponse.class);
                    if (!productSectionResponse.getData().isEmpty()) {
                        list.add(productSectionResponse);
                    }
                } else if (type == ConciergeMainSectionType.BRAND && !isLoadMoreData) {
                    ConciergeBrandSectionResponse productSectionResponse = gson.fromJson(object.toString(),
                            ConciergeBrandSectionResponse.class);
                    if (!productSectionResponse.getData().isEmpty()) {
                        list.add(productSectionResponse);
                    }
                } else if (type == ConciergeMainSectionType.SUPPLIER) {
                    /*
                        For supplier section, we support load more supplier. We will add only supplier section into
                        list which contain list of supplier and the main adapter will take this single object to add more item into
                        internal supplier adapter directly.
                     */
                    ConciergeSupplierSectionResponse supplierSectionResponse = gson.fromJson(object.toString(),
                            ConciergeSupplierSectionResponse.class);
                    if (!supplierSectionResponse.getData().isEmpty()) {
                        list.add(supplierSectionResponse);
                    }
                }
            }
        }

        checkItemPagination(conciergeShopFeature);
        return list;
    }

    public void resetPagination() {
        mCurrentOffset = 0;
    }

    private void checkItemPagination(ConciergeShopFeature conciergeShopFeature) {
        if (conciergeShopFeature.getSection().isEmpty() || conciergeShopFeature.getNext() == null) {
            resetPagination();
            setCanLoadMore(false);
        } else {
            if (conciergeShopFeature.getNext() != null) {
                setCanLoadMore(true);
                mCurrentOffset = conciergeShopFeature.getNext();
            } else {
                resetPagination();
                setCanLoadMore(false);
            }
        }
    }
}
