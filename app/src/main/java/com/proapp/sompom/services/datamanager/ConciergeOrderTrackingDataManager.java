package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.R;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.resourcemanager.helper.LocaleManager;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Or Vitovongsak on 4/11/21.
 */

public class ConciergeOrderTrackingDataManager extends AbsLoadMoreDataManager {

    public ConciergeOrderTrackingDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<List<ConciergeOrder>> getConciergeOrderTracking(boolean isLoadMore) {
        if (!isLoadMore) {
            resetPagination();
        }

        String userId = SharedPrefUtils.getUserId(mContext);

        return mApiService.getConciergeTrackingOrder(getCurrentPage(),
                        getPaginationSize(),
                        LocaleManager.getAppLanguage(mContext))
                .concatMap(loadMoreWrapperResponse -> {
                    if (loadMoreWrapperResponse.isSuccessful()) {
                        LoadMoreWrapper<ConciergeOrder> wrapper = loadMoreWrapperResponse.body();
                        checkPagination(wrapper);
                        return Observable.just(wrapper.getData());
                    } else {
                        return Observable.error(new ErrorThrowable(loadMoreWrapperResponse.code(),
                                mContext.getString(R.string.error_general_description)));
                    }
                });
    }
}
