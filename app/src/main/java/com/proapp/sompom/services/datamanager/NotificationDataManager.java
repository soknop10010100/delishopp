package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.proapp.sompom.model.emun.NotificationItem;
import com.proapp.sompom.model.notification.ConciergeNotification;
import com.proapp.sompom.model.notification.NotificationAdaptive;
import com.proapp.sompom.model.notification.NotificationHeader;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by He Rotha on 10/19/17.
 */

public class NotificationDataManager extends AbsLoadMoreDataManager {

    private boolean mIsNewNotificationSectionAdded;
    private boolean mIsOldNotificationSectionAdded;

    public NotificationDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<List<NotificationAdaptive>> loadMore() {
        return geNotification2(true);
    }

    private void validateTheCheckingType(List<NotificationAdaptive> notificationData) {
        Gson gson = new Gson();
        //Currently we will handle only order status change notification types
        if (notificationData != null && !notificationData.isEmpty()) {
            for (int size = notificationData.size() - 1; size >= 0; size--) {
                String data = gson.toJson(notificationData.get(size));
                ConciergeNotification notification = gson.fromJson(data,
                        ConciergeNotification.class);
                if (!(notification != null && notification.getData() != null && notification.getOrder() != null)) {
                    notificationData.remove(size);
                }
            }
        }
    }

    public Observable<List<NotificationAdaptive>> geNotification2(boolean isLoadMore) {
        if (!isLoadMore) {
            mIsNewNotificationSectionAdded = false;
            mIsOldNotificationSectionAdded = false;
        }

        return mApiService.getNotification2(getCurrentPage(),
                getPaginationSize(),
                LocaleManager.getAppLanguage(mContext)).concatMap(loadMoreWrapperResponse -> {
            LoadMoreWrapper<ConciergeNotification> loadMoreWrapper = new Gson().fromJson(loadMoreWrapperResponse.body().toString(),
                    new TypeToken<LoadMoreWrapper<ConciergeNotification>>() {
                    }.getType());
            if (loadMoreWrapper != null) {
                List<NotificationAdaptive> notificationAdaptiveList = new ArrayList<>(loadMoreWrapper.getData());
                Timber.i("notificationAdaptiveList: " + new Gson().toJson(notificationAdaptiveList));
                NotificationDataManager.this.validateTheCheckingType(notificationAdaptiveList);
                NotificationDataManager.this.checkPagination(loadMoreWrapper);
                if (loadMoreWrapper.getData() != null && !loadMoreWrapper.getData().isEmpty()) {
                    NotificationDataManager.this.checkToInjectNotificationSectionTitle(notificationAdaptiveList);
                    return Observable.just(notificationAdaptiveList);
                } else {
                    return Observable.just(new ArrayList<>());
                }
            }

            return Observable.just(new ArrayList<>());
        });
    }

    private void checkToInjectNotificationSectionTitle(List<NotificationAdaptive> notificationList) {
        if (notificationList != null && !notificationList.isEmpty()) {
            int newNotificationSectionIndex = -1;
            int newNotificationCount = 0;
            int oldNotificationSectionIndex = -1;
            for (int i = 0; i < notificationList.size(); i++) {
                if (newNotificationSectionIndex >= 0 && oldNotificationSectionIndex >= 0) {
                    //All section are found.
                    break;
                }

                if (notificationList.get(i).isNewNotification()) {
                    if (newNotificationSectionIndex < 0) {
                        //Will inject new index notification section and normally it always on the top
                        newNotificationSectionIndex = 0;
                    }
                    newNotificationCount++;
                } else if (oldNotificationSectionIndex < 0) {
                    //Will inject old index notification section.
                    oldNotificationSectionIndex = i;
                }
            }

//            Timber.i("newNotificationSectionIndex: " + newNotificationSectionIndex +
//                    ", newNotificationCount: " + newNotificationCount +
//                    ", oldNotificationSectionIndex: " + oldNotificationSectionIndex);
            //Check to add new notification section.
            if (!mIsNewNotificationSectionAdded && newNotificationSectionIndex >= 0) {
                mIsNewNotificationSectionAdded = true;
                NotificationHeader notificationHeaderNew = new NotificationHeader();
                notificationHeaderNew.setNotificationItem(NotificationItem.New);
                notificationHeaderNew.setNewsHeader(true);
                notificationList.add(newNotificationSectionIndex, notificationHeaderNew);
            }

            //Check to add old notification section.
            if (!mIsOldNotificationSectionAdded && oldNotificationSectionIndex >= 0) {
                mIsOldNotificationSectionAdded = true;
                NotificationHeader notificationHeaderEarlier = new NotificationHeader();
                //There is only old notifications display. So need to make margin top
                if (newNotificationSectionIndex < 0) {
                    notificationHeaderEarlier.setEnableMarginTop(true);
                }
                notificationHeaderEarlier.setNotificationItem(NotificationItem.Earlier);
                if (newNotificationCount > 0) {
                    oldNotificationSectionIndex = newNotificationCount + 1;
                }
                notificationList.add(oldNotificationSectionIndex, notificationHeaderEarlier);
            }
        }
    }
}
