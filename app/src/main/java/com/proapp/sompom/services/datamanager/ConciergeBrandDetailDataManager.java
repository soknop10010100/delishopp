package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.model.concierge.ConciergeBrand;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.LoadMoreConciergeBrandItemWrapper;
import com.proapp.sompom.model.emun.ConciergeViewItemByType;
import com.proapp.sompom.model.emun.OpenBrandDetailType;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Response;

/**
 * Created by Chhom Veasna on 16/11/22.
 */
public class ConciergeBrandDetailDataManager extends AbsConciergeProductDataManager {

    private ConciergeBrand mBrand;
    private final String mPassParam;
    private final OpenBrandDetailType mOpenType;
    private final String mTitleParam;

    public ConciergeBrandDetailDataManager(Context context,
                                           ApiService apiService,
                                           String passParam,
                                           String titleParam,
                                           OpenBrandDetailType openType) {
        super(context, apiService);
        mPassParam = passParam;
        mTitleParam = titleParam;
        mOpenType = openType;
    }

    public OpenBrandDetailType getOpenType() {
        return mOpenType;
    }

    public String getTitleParam() {
        return mTitleParam;
    }

    public String getPassParam() {
        return mPassParam;
    }

    public boolean isOpenAsSearchGeneralProductMode() {
        return mOpenType == OpenBrandDetailType.OPEN_AS_SEARCH_GENERAL_PRODUCT_MODE;
    }

    public boolean isOpenAsSearchSupplierProductMode() {
        return mOpenType == OpenBrandDetailType.OPEN_AS_SEARCH_SUPPLIER_PRODUCT_MODE;
    }

    @Override
    public int getPaginationSize() {
        ConciergeViewItemByType selectedViewItemByOption = ConciergeHelper.getSelectedViewItemByOption(mContext);
        if (selectedViewItemByOption == ConciergeViewItemByType.THREE_COLUMNS) {
            return 21;
        } else {
            return 20;
        }
    }

    public ConciergeBrand getBrand() {
        return mBrand;
    }

    public Observable<Response<List<ConciergeMenuItem>>> getProductItem(boolean isLoadMore, String searchText) {
        if (mOpenType == OpenBrandDetailType.OPEN_AS_SEARCH_GENERAL_PRODUCT_MODE) {
            return getSearchGeneralProductItem(isLoadMore, searchText);
        } else if (mOpenType == OpenBrandDetailType.OPEN_AS_SEARCH_SUPPLIER_PRODUCT_MODE) {
            return getSearchSupplierProduct(isLoadMore, searchText);
        } else {
            return getProductItemOfBran(isLoadMore, searchText);
        }
    }

    public Observable<Response<List<ConciergeMenuItem>>> getProductItemOfBran(boolean isLoadMore, String searchText) {
        if (!isLoadMore) {
            resetPagination();
        }

        return mApiService.getBrandItem(mPassParam,
                        Integer.valueOf(getCurrentPage()),
                        getPaginationSize(),
                        searchText,
                        ConciergeHelper.getSortOption(),
                        LocaleManager.getAppLanguage(mContext),
                        ApplicationHelper.getUserId(mContext))
                .concatMap((Function<Response<LoadMoreConciergeBrandItemWrapper>,
                        ObservableSource<Response<List<ConciergeMenuItem>>>>) response -> {
                    if (response.isSuccessful()) {
                        mBrand = response.body().getConciergeBrand();
                        checkPagination(response.body());

                        return Observable.just(Response.success(response.body().getData()));
                    } else {
                        return Observable.just(Response.error(response.code(), response.errorBody()));
                    }
                });
    }

    public Observable<Response<List<ConciergeMenuItem>>> getSearchGeneralProductItem(boolean isLoadMore, String searchText) {
        if (!isLoadMore) {
            resetPagination();
        }

        return mApiService.searchGeneralProduct(Integer.valueOf(getCurrentPage()),
                        getPaginationSize(),
                        searchText,
                        ApplicationHelper.getRequestAPIMode(mContext),
                        LocaleManager.getAppLanguage(getContext()),
                        ApplicationHelper.getUserId(mContext))
                .concatMap((Function<Response<LoadMoreWrapper<ConciergeMenuItem>>,
                        ObservableSource<Response<List<ConciergeMenuItem>>>>) response -> {
                    if (response.isSuccessful()) {
                        checkPagination(response.body());

                        return Observable.just(Response.success(response.body().getData()));
                    } else {
                        return Observable.just(Response.error(response.code(), response.errorBody()));
                    }
                });
    }

    public Observable<Response<List<ConciergeMenuItem>>> getSearchSupplierProduct(boolean isLoadMore, String searchText) {
        if (!isLoadMore) {
            resetPagination();
        }

        return mApiService.searchSupplierProduct(mPassParam,
                Integer.valueOf(getCurrentPage()),
                getPaginationSize(),
                searchText,
                ConciergeHelper.getSortOption(),
                LocaleManager.getAppLanguage(mContext)).concatMap((Function<Response<LoadMoreWrapper<ConciergeMenuItem>>,
                ObservableSource<Response<List<ConciergeMenuItem>>>>) response -> {
            if (response.isSuccessful()) {
                checkPagination(response.body());

                return Observable.just(Response.success(response.body().getData()));
            } else {
                return Observable.just(Response.error(response.code(), response.errorBody()));
            }
        });
    }
}
