package com.proapp.sompom.services.datamanager;

import android.annotation.SuppressLint;
import android.content.Context;

import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.model.ShopDeliveryDate;
import com.proapp.sompom.model.ShopDeliveryTime;
import com.proapp.sompom.model.result.ShopDeliveryData;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.DateUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

public class DeliveryDataManager extends AbsLoadMoreDataManager {

    private static final int REMAINING_DAY_AMOUNT_TO_ADD = 5;

    public DeliveryDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<ShopDeliveryData>> getDeliveryData() {
        return mApiService.getDeliveryTime()
                .concatMap(response -> {
                    ShopDeliveryData toReturn = new ShopDeliveryData();
                    if (response.isSuccessful() && response.body() != null) {
                        toReturn.setShopDeliveryTimes(convertTimeStringToLocalTime(response.body()));
                    }
                    // Currently we're still building the date manually
                    toReturn.setShopDeliveryDates(buildDateList());

                    return Observable.just(Response.success(toReturn));
                });
    }

    public Observable<Response<ShopDeliveryData>> getTimeSlotService() {
        return mApiService.getTimeSlot(ApplicationHelper.getRequestAPIMode(mContext)).concatMap(listResponse -> {
            ShopDeliveryData toReturn = new ShopDeliveryData();
            List<ShopDeliveryDate> deliveryDates = listResponse.body();
            List<ShopDeliveryDate> validDateList = new ArrayList<>();
            toReturn.setShopDeliveryDates(validDateList);
            for (ShopDeliveryDate deliveryDate : deliveryDates) {
                //Parse date and time of the date.
                Date dateFromStringDate = DateUtils.getDateFromStringDate(deliveryDate.getResponseDate());
                Calendar instance = Calendar.getInstance();
                instance.setTime(dateFromStringDate);
                LocalDateTime dateTime = LocalDateTime.of(instance.get(Calendar.YEAR),
                        instance.get(Calendar.MONTH) + 1,
                        instance.get(Calendar.DAY_OF_MONTH),
                        0,
                        0);
                deliveryDate.setDate(dateTime);
                Timber.i("dateTime: " + dateTime.toString());
                List<ShopDeliveryTime> shopDeliveryTimes = convertTimeStringToLocalTime(deliveryDate.getShopDeliveryTimes());
                deliveryDate.setShopDeliveryTimes(shopDeliveryTimes);
                validDateList.add(deliveryDate);
            }

            return Observable.just(Response.success(toReturn));
        });
    }

    private void validateAvailableTimeSlotOfDate(ShopDeliveryDate deliveryDate) {
        if (deliveryDate.getShopDeliveryTimes() != null && !deliveryDate.getShopDeliveryTimes().isEmpty()) {
            boolean isHasValidTimeItem = false;
            List<ShopDeliveryTime> validTimeList = new ArrayList<>();
            for (ShopDeliveryTime shopDeliveryTime : deliveryDate.getShopDeliveryTimes()) {
                //Validate the past time
                if (deliveryDate.getDate().toLocalDate().isEqual(LocalDate.now())) {
                    LocalTime now = LocalTime.now();
                    if (!shopDeliveryTime.getStartTime().isBefore(now)) {
                        validTimeList.add(shopDeliveryTime);
                    }
                } else {
                    validTimeList.add(shopDeliveryTime);
                }
            }

            deliveryDate.setShopDeliveryTimes(validTimeList);
        }
    }

    @SuppressLint("NewApi")
    public static List<ShopDeliveryTime> convertTimeStringToLocalTime(List<ShopDeliveryTime> timeList) {
        Timber.i("convertTimeStringToLocalTime: " + timeList.size());
        // Here, the server return time list as a list of string. We'll convert it to a LocalTime
        // instance for easier usage of time, while still keeping the original time string data
        timeList.forEach(time -> {
            LocalTime startTime = LocalTime.parse(time.getStartTimeString());
            LocalTime endTime = LocalTime.parse(time.getEndTimeString());
            time.setStartTime(startTime);
            time.setEndTime(endTime);
            time.setIsVisible(true);
        });

        return timeList;
    }

    private List<ShopDeliveryDate> buildDateList() {
        /*
         *** How the date list selection will be generated ***
         * Will display only one month period from the current date.
         * 1. If the day remaining counter from current date to end of month is equal or less than
         * @{REMAINING_DAY_AMOUNT_TO_ADD}, then the date list start from current date of current month,
         *  plus some dates from next month.
         * 2. If the day remaining counter from current date to end of month is bigger than
         * @{REMAINING_DAY_AMOUNT_TO_ADD}, then the date list will start from current date to the end
         * date of current month.
         */

        LocalDate today = LocalDate.now();

        int currentDayOfCurrentMonth = today.getDayOfMonth();
        int totalDayOfCurrentMonth = today.lengthOfMonth();
        int remainingDayBeforeEndOfMonth = today.lengthOfMonth() - today.getDayOfMonth();
        int loopCounter;

//        Timber.i("getDayOfMonth: " + today.getDayOfMonth() +
//                ", lengthOfMonth: " + today.lengthOfMonth() +
//                ", getMonthValue: " + today.getMonthValue() +
//                ", remainingDayBeforeEndOfMonth: " + remainingDayBeforeEndOfMonth);

        if (remainingDayBeforeEndOfMonth <= REMAINING_DAY_AMOUNT_TO_ADD) {
            LocalDate nextMonth;
            if (today.getMonthValue() == 12) {
                //Reset to next month of next year date
                nextMonth = today.withYear(today.getYear() + 1).withMonth(1).withDayOfMonth(1);
            } else {
                //Next month within the same year
                nextMonth = today.withMonth(today.getMonthValue() + 1).withDayOfMonth(1);
            }
            int minusNextMonthCounter = nextMonth.lengthOfMonth() - currentDayOfCurrentMonth;
            if (minusNextMonthCounter < 0) {
                minusNextMonthCounter = 0;
            }

            int dayOfNextMonthToBeAddedCount = (nextMonth.lengthOfMonth() - minusNextMonthCounter);
            //We will not display the same current day value in the next month date list.
            if (dayOfNextMonthToBeAddedCount == currentDayOfCurrentMonth) {
                dayOfNextMonthToBeAddedCount--;
            }
            loopCounter = remainingDayBeforeEndOfMonth + dayOfNextMonthToBeAddedCount;
//            Timber.i("Next getDayOfMonth: " + nextMonth.getDayOfMonth() +
//                    ", Next lengthOfMonth: " + nextMonth.lengthOfMonth() +
//                    ", Next getMonthValue: " + nextMonth.getMonthValue() +
//                    ", dayOfNextMonthToBeAddedCount: " + dayOfNextMonthToBeAddedCount
//                    + ", remainingDayBeforeEndOfMonth: " + remainingDayBeforeEndOfMonth +
//                    ", loopCounter: " + loopCounter);
        } else {
            loopCounter = totalDayOfCurrentMonth - currentDayOfCurrentMonth;
        }

        List<ShopDeliveryDate> dates = new ArrayList<>();
        for (int i = 0; i <= loopCounter; i++) {
//            Calendar calendar = Calendar.getInstance();
//            calendar.add(Calendar.DATE, i);

            LocalTime zeroTime = LocalTime.parse("00:00");
            LocalDateTime date = LocalDateTime.now().with(zeroTime).plusDays(i);
            dates.add(new ShopDeliveryDate(date, true));
//            Timber.i("Date: " + DateUtils.getDateStringWithSpecificFormat(calendar.getTime()));
        }

        return dates;
    }

    private List<ShopDeliveryTime> buildTimeList() {
//        Calendar calendar = Calendar.getInstance();
////        Start at 6:00 AM to 6:00 PM
//        calendar.set(Calendar.HOUR_OF_DAY, 6);
//        calendar.set(Calendar.MINUTE, 0);

        LocalTime time = LocalTime.now().withHour(6).withMinute(0);
        List<ShopDeliveryTime> times = new ArrayList<>();

        for (int i = 0; i < 24; i++) {
//            Date starTime = calendar.getTime();
//            calendar.add(Calendar.MINUTE, 30);
//            Date endTime = calendar.getTime();
//            times.add(new ShopDeliveryTime(starTime, endTime, !(i == 5)));

            LocalTime startTime = time.plusMinutes(i * 30);
            LocalTime endTime = startTime.plusMinutes(30);
            ShopDeliveryTime deliveryTime = new ShopDeliveryTime();

            deliveryTime.setStartTime(startTime);
            deliveryTime.setEndTime(endTime);

            times.add(deliveryTime);
        }

        return times;
    }
}
