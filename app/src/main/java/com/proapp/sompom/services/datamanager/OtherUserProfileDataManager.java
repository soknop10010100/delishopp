package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.model.result.AppSetting;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Response;
import timber.log.Timber;


public class OtherUserProfileDataManager extends AbsLoadMoreDataManager {

    public OtherUserProfileDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<User>> getUserProfile(String id) {
        return getAppSetting().onErrorResumeNext(throwable -> {
            return Observable.just(Response.success(new ArrayList<>()));
        }).concatMap((Function<Response<List<AppSetting>>, ObservableSource<Response<User>>>) listResponse -> {
            if (listResponse.body() != null && !listResponse.body().isEmpty()) {
                SharedPrefUtils.setAppSetting(getContext(), listResponse.body().get(0));
            }
            return mApiService.getUserById(id, SharedPrefUtils.getLanguage(mContext));
        });
    }

    private Observable<Response<List<AppSetting>>> getAppSetting() {
        return mApiService.getAppSetting();
    }
}
