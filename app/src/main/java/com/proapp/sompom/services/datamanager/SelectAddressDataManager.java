package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.text.TextUtils;

import com.proapp.sompom.R;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.model.searchaddress.google.AddressComponent;
import com.proapp.sompom.model.searchaddress.google.AddressDetailResponse;
import com.proapp.sompom.model.searchaddress.google.AutoCompleteResponse;
import com.proapp.sompom.model.searchaddress.google.Prediction;
import com.proapp.sompom.model.searchaddress.mapbox.ContextItem;
import com.proapp.sompom.model.searchaddress.mapbox.Feature;
import com.proapp.sompom.services.ApiGoogle;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.LocationStateUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/12/18.
 */

public class SelectAddressDataManager extends AbsDataManager {

    private static final String COUNTRY = "country";
    private static final String LOCALITY = "locality";
    private ApiGoogle mApiGoogle;
    private String mGoogleApiKey;
    private String mMapBoxToken;
    private Strategy mStrategy;

    public SelectAddressDataManager(Context context,
                                    ApiService apiService,
                                    ApiGoogle apiGoogle,
                                    Strategy strategy) {
        super(context, apiService);
        mApiGoogle = apiGoogle;
        mStrategy = strategy;
    }

    public Observable<List<SearchAddressResult>> searchAddress(String query) {
        if (mStrategy == Strategy.Google) {
            return mApiGoogle.searchGoogleAddress(getGoogleApiKey(), query)
                    .concatMap(autoCompleteResponseResponse -> {
                        List<SearchAddressResult> searchAddressResults = new ArrayList<>();
                        AutoCompleteResponse autoCompleteResponse = autoCompleteResponseResponse.body();
                        if (autoCompleteResponse != null &&
                                autoCompleteResponse.getPredictions() != null) {
                            for (Prediction prediction : autoCompleteResponse.getPredictions()) {
                                SearchAddressResult searchAddressResult = new SearchAddressResult();
                                searchAddressResult.setId(prediction.getPlaceId());
                                if (prediction.getTerms() != null && !prediction.getTerms().isEmpty()) {
                                    //Place title is at the first index.
                                    searchAddressResult.setPlaceTitle(prediction.getTerms().get(0).getValue());
                                }
                                searchAddressResult.setAddress(LocationStateUtil.getOnlyAddressFromFullPlace(prediction.getDescription()));
                                searchAddressResult.setFullAddress(prediction.getDescription());
                                searchAddressResults.add(searchAddressResult);
                            }
                        }
                        return Observable.just(searchAddressResults);
                    });
        } else {
            return mApiGoogle.searchMapBoxAddress(query, getMapBoxToken())
                    .concatMap(mapBoxDataResponse -> {
                        List<SearchAddressResult> searchAddressResults = new ArrayList<>();
                        if (mapBoxDataResponse.body() != null && mapBoxDataResponse.body().getFeatures() != null) {
                            for (Feature feature : mapBoxDataResponse.body().getFeatures()) {
                                searchAddressResults.add(parseMapBoxPlaceData(feature));
                            }
                        }
                        return Observable.just(searchAddressResults);
                    });
        }
    }

    private SearchAddressResult parseMapBoxPlaceData(Feature feature) {
        SearchAddressResult searchAddressResult = new SearchAddressResult();
        searchAddressResult.setId(feature.getId());
        searchAddressResult.setPlaceTitle(feature.getText());
        searchAddressResult.setAddress(LocationStateUtil.getOnlyAddressFromFullPlace(feature.getPlaceName()));
        searchAddressResult.setFullAddress(feature.getPlaceName());

        //Since the MapBox response does not response specific city and country of a result place,
        //so we have to parse the data from response context property
        String city = null;
        String country = null;
        if (feature.getContextItemList() != null) {
            for (ContextItem contextItem : feature.getContextItemList()) {
                if (contextItem.isRegionOrCity()) {
                    city = contextItem.getText();
                }
                if (contextItem.isCountry()) {
                    country = contextItem.getText();
                }
            }
        }
        searchAddressResult.setCity(city);
        searchAddressResult.setCountry(country);

        //Parse location
        Locations locations = new Locations();
        locations.setLatitude(feature.getCenter().get(1));
        locations.setLongitude(feature.getCenter().get(0));
        searchAddressResult.setLocations(locations);

        return searchAddressResult;
    }

    public Observable<SearchAddressResult> searchAddressDetail(SearchAddressResult data) {
        if (data.getLocations() == null) {
            return mApiGoogle.searchGoogleAddressDetail(getGoogleApiKey(), data.getId())
                    .concatMap(addressDetailResponseResponse -> {
                        AddressDetailResponse detailResponse = addressDetailResponseResponse.body();
                        SearchAddressResult searchAddressResult = new SearchAddressResult();

                        if (detailResponse != null) {
                            double lat = detailResponse.getResult().getGeometry().getLocation().getLng();
                            double lng = detailResponse.getResult().getGeometry().getLocation().getLng();
                            String countryCode = null;
                            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                            List<Address> addresses;
                            try {
                                addresses = geocoder.getFromLocation(lat, lng, 1);

                                if (addresses != null && !addresses.isEmpty()) {
                                    countryCode = addresses.get(0).getCountryCode();
                                }

                            } catch (Exception ignored) {
                                Timber.e(ignored.toString());
                            }

                            Locations locations = new Locations();
                            locations.setLongitude(lat);
                            locations.setLatitude(lng);
                            locations.setCountry(countryCode);

                            searchAddressResult.setFullAddress(data.getFullAddress());
                            searchAddressResult.setLocations(locations);
                            for (AddressComponent addressComponent : detailResponse.getResult().getAddressComponents()) {
                                for (String s : addressComponent.getTypes()) {
                                    if (s.equalsIgnoreCase(COUNTRY)) {
                                        searchAddressResult.setCountry(addressComponent.getLongName());
                                    } else if (s.equalsIgnoreCase(LOCALITY)) {
                                        searchAddressResult.setCity(addressComponent.getLongName());
                                    }
                                }
                            }
                        }
                        return Observable.just(searchAddressResult);
                    });
        } else {
            return Observable.just(data);
        }
    }

    private String getGoogleApiKey() {
        if (TextUtils.isEmpty(mGoogleApiKey)) {
            mGoogleApiKey = mContext.getString(R.string.google_place_key);
        }
        return mGoogleApiKey;
    }

    private String getMapBoxToken() {
        if (TextUtils.isEmpty(mMapBoxToken)) {
            mMapBoxToken = mContext.getString(R.string.mapbox_token);
        }
        return mMapBoxToken;
    }

    public enum Strategy {
        Google, Mapbox
    }
}
