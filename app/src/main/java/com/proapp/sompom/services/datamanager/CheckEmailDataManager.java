package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.model.request.CheckEmailRequestBody;
import com.proapp.sompom.model.result.CheckEmailResponse;
import com.proapp.sompom.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Chhom Veasna on 2/10/22.
 */

public class CheckEmailDataManager extends AbsLoginDataManager {

    public CheckEmailDataManager(Context context, ApiService publicApiService, ApiService privateApiService) {
        super(context, publicApiService, privateApiService);
    }

    public Observable<Response<CheckEmailResponse>> getCheckEmailService(String email) {
        return mApiService.checkIfEmailExist(new CheckEmailRequestBody(email));
    }
}
