package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.model.concierge.ConciergeBasketMenuItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Or Vitovongsak on 29/9/21.
 */
public class ConciergeProductDetailFragmentDataManager extends AbsConciergeProductDataManager {

    private Context mContext;
    private ApiService mApiService;

    public ConciergeProductDetailFragmentDataManager(Context context, ApiService apiService) {
        super(context, apiService);
        mContext = context;
        mApiService = apiService;
    }

    public Observable<Response<ConciergeMenuItem>> getData(String menuItemId) {
        return mApiService.getConciergeItemDetail(menuItemId, LocaleManager.getAppLanguage(mContext), ApplicationHelper.getUserId(mContext));
    }

    public Observable<Response<ConciergeBasketMenuItem>> updateOrRemoveItemInBasket(String basketITemId, ConciergeMenuItem menuItem) {
        return mApiService.updateOrRemoveItemInBasket(basketITemId,
                ConciergeHelper.createOrderMenuItem(menuItem),
                ApplicationHelper.getRequestAPIMode(mContext),
                LocaleManager.getAppLanguage(mContext));
    }
}
