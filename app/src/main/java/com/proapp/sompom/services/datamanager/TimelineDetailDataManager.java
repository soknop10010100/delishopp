package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.SoundEffectService;
import com.proapp.sompom.helper.OfflinePostDataHelper;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.notification.RedirectionNotificationData;
import com.proapp.sompom.model.request.FollowBody;
import com.proapp.sompom.model.request.ReportRequest;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.HTTPResponse;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 7/13/18.
 */

public class TimelineDetailDataManager extends PopUpCommentDataManager {

    public TimelineDetailDataManager(Context context, ApiService apiService, RedirectionNotificationData redirectionNotificationData) {
        super(context, apiService, redirectionNotificationData);
    }

    public Media getRedirectionMediaTarget(WallStreetAdaptive adaptive) {
        if (adaptive != null && adaptive.getMedia() != null && !adaptive.getMedia().isEmpty()) {
            for (Media media : adaptive.getMedia()) {
                if (TextUtils.equals(media.getId(), mRedirectionNotificationData.getMediaId())) {
                    return media;
                }
            }
        }

        return null;
    }

    public Observable<Response<Object>> postFollow(String id) {
        FollowBody body = new FollowBody(id);
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
        return mApiService.postFollow(body);
    }

    public Observable<Response<Object>> unFollow(String id) {
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        FollowBody body = new FollowBody(id);
        return mApiService.unFollow(body);
    }

    public Observable<Response<Object>> reportPost(String id, ReportRequest reportRequest) {
        return mApiService.report(ContentType.POST.getValue(), id, reportRequest);
    }

    public Observable<Response<Object>> unFollowShop(String id) {
        FollowBody body = new FollowBody(id);
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        return mApiService.unFollowShop(body);
    }

    public Observable<Response<LifeStream>> getWallStreetDetail(String id) {
        if (isNetworkAvailable()) {
            return mApiService.getWallStreetDetail(id);
        } else {
            return Observable.create(emitter -> OfflinePostDataHelper.findPostByIdOrActivityId(getContext(),
                    id,
                    new OfflinePostDataHelper.OfflinePostDataHelperListener() {
                        @Override
                        public void onFindPostSuccess(LifeStream lifeStream) {
                            if (lifeStream == null) {
                                emitter.onError(new ErrorThrowable(HTTPResponse.NOT_FOUND,
                                        getContext().getString(R.string.error_no_data)));
                            } else {
                                emitter.onNext(Response.success(lifeStream));
                                emitter.onComplete();
                            }
                        }
                    }));
        }
    }

    public Observable<Response<Object>> deletePost(String id) {
        return mApiService.deleteLifeStream(id);
    }
}
