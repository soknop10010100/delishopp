package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.database.CurrencyDb;
import com.proapp.sompom.model.Price;
import com.proapp.sompom.model.result.Currency;
import com.proapp.sompom.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

public class PriceDataManager extends AbsDataManager {
    public PriceDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<Price>> getPrice() {
        Currency currency = CurrencyDb.getSelectedCurrency(mContext);
        return mApiService.getMinMaxPrice(currency.getName());
    }
}
