package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.model.ActiveUser;
import com.proapp.sompom.model.result.SearchGeneralUserWrapper;
import com.proapp.sompom.observable.ResponseHandleErrorFunc;
import com.proapp.sompom.services.ApiService;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by nuonveyo on 6/27/18.
 */

public class SearchGeneralDataManager extends AbsLoadMoreDataManager {
    public SearchGeneralDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<List<ActiveUser>> getSearchGeneralUser() {
        return mApiService.getSearchGeneralUser()
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(searchGeneralUserWrapperResponse -> {
                    SearchGeneralUserWrapper wrapper = searchGeneralUserWrapperResponse.body();
                    if (wrapper != null) {
                        List<ActiveUser> activeUser = new ArrayList<>();

//                        ActiveUser nearby = wrapper.getNearbyUsers();
//                        if (nearby != null && !nearby.isEmpty()) {
//                            nearby.setTitle(getContext().getString(R.string.search_general_nearby_title));
//                            nearby.setSellerItemType(ActiveUser.SellerItemType.PEOPLE_NEARBY);
//                            activeUser.add(nearby);
//                        }

                        ActiveUser suggestion = wrapper.getSuggestedUsers();
                        if (suggestion != null && !suggestion.isEmpty()) {
                            UserHelper.saveSuggestedUser(getContext(), suggestion);
                            suggestion.setTitle(getContext().getString(R.string.search_home_screen_suggested_people_title));
                            suggestion.setSellerItemType(ActiveUser.SellerItemType.SUGGESTED_PEOPLE);
                            activeUser.add(suggestion);
                        }

//                        ActiveUser lastSearch = wrapper.getLastSearchUsers();
//                        if (lastSearch != null && !lastSearch.isEmpty()) {
//                            wrapper.getLastSearchUsers().setTitle(getContext().getString(R.string.search_general_last_search_title));
//                            wrapper.getLastSearchUsers().setSellerItemType(ActiveUser.SellerItemType.LAST_SEARCH);
//                            activeUser.add(lastSearch);
//                        }

                        return Observable.just(activeUser);
                    }
                    return Observable.error(new NullPointerException());
                });
    }
}
