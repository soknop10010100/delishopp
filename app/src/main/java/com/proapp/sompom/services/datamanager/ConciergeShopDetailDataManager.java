package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.model.concierge.ConciergeShop;
import com.proapp.sompom.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

public class ConciergeShopDetailDataManager extends AbsLoadMoreDataManager {

    private final String mStoreId;
    private boolean mIsShopClosed;
    private boolean mIsDiscountOnly;

    public ConciergeShopDetailDataManager(Context context,
                                          ApiService apiService,
                                          String storeId,
                                          boolean isDiscountOnly) {
        super(context, apiService);
        mStoreId = storeId;
        mIsDiscountOnly = isDiscountOnly;
    }

    public boolean isShopClosed() {
        return mIsShopClosed;
    }

    public Observable<Response<ConciergeShop>> getStoreDetailService() {
        return mApiService.getShopAnnouncement().concatMap(announcementResponseResponse -> {
            if (announcementResponseResponse.isSuccessful() && announcementResponseResponse.body() != null) {
                mIsShopClosed = announcementResponseResponse.body().isShopClosed();
            }

            return mApiService.getShopDetail(mStoreId, mIsDiscountOnly, false)
                    .concatMap(response -> {
                        if (response.isSuccessful()) {
                            return Observable.just(response);
                        } else {
                            return Observable.just(Response.error(response.code(), response.errorBody()));
                        }
                    });
        });
    }
}
