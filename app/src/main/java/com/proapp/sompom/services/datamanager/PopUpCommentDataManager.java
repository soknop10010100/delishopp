package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.example.usermentionable.model.UserMentionable;
import com.proapp.sompom.broadcast.SoundEffectService;
import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.notification.RedirectionNotificationData;
import com.proapp.sompom.model.request.CommentBody;
import com.proapp.sompom.model.request.LikeRequestBody;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.model.result.JumpCommentResponse;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.ConvertUserToMentionableUtil;
import com.proapp.sompom.utils.GenerateLinkPreviewUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class PopUpCommentDataManager extends AbsLoadMoreDataManager {

    protected RedirectionNotificationData mRedirectionNotificationData;
    private String mPreviousJumpCommentDate;
    private String mNextJumpCommentDate;
    private Comment mMainCommentOfJumpSubComment;
    /*
        List of unread comment id and the will be checking and adding during the response of getting
        comment list result. If the comment property of contentstate, "isCommentRead", is false, we
        will add it into list. This list will be used when to update the total unread comment counter of
        the requesting content item when exit comment screen or in single post detail item screen.
     */
    private List<String> mNewReadCommentCounterList = new ArrayList<>();


    public PopUpCommentDataManager(Context context, ApiService apiService) {
        super(context, apiService);
//        int pageSize = getContext().getResources().getInteger(R.integer.number_request_item);
        //TODO: To Remove
        int pageSize = 5;
        setPaginationSize(pageSize);
    }

    public void resetToNormalTimeLineDetailMode() {
        mRedirectionNotificationData = null;
        mPreviousJumpCommentDate = null;
        mNextJumpCommentDate = null;
    }

    public RedirectionNotificationData getRedirectionNotificationData() {
        return mRedirectionNotificationData;
    }

    public PopUpCommentDataManager(Context context, ApiService apiService, RedirectionNotificationData exchangeData) {
        super(context, apiService);
//        int pageSize = getContext().getResources().getInteger(R.integer.number_request_item);
        //TODO: To Remove
        int pageSize = 5;
        mRedirectionNotificationData = exchangeData;
        setPaginationSize(pageSize);
    }

    public Comment getMainCommentOfJumpSubComment() {
        return mMainCommentOfJumpSubComment;
    }

    public boolean isCanLoadPrevious() {
        if (!isInJumpCommentMode()) {
            return isCanLoadMore();
        } else {
            return !TextUtils.isEmpty(mPreviousJumpCommentDate);
        }
    }

    public boolean isCanLoadNext() {
        return !TextUtils.isEmpty(mNextJumpCommentDate);
    }

    public RedirectionNotificationData getNotificationExchangeData() {
        return mRedirectionNotificationData;
    }

    public boolean isInJumpCommentMode() {
        return mRedirectionNotificationData != null;
    }

    public Observable<Response<Comment>> postComment(String contentId, Comment comment) {
        Comment newComment = Comment.getNewComment(comment, false);
        String fistLinkFromComment = getFistLinkFromComment(comment.getContent());
        if (!TextUtils.isEmpty(fistLinkFromComment)) {
            return LinkPreviewRetriever.getPreviewLink(getContext(), mApiService,
                    fistLinkFromComment).concatMap((Function<Response<LinkPreviewModel>,
                    ObservableSource<Response<Comment>>>) linkPreviewModelResponse -> {
                if (linkPreviewModelResponse.body() != null) {
                    newComment.setMetaPreview(Collections.singletonList(linkPreviewModelResponse.body()));
                }
                return mApiService.postComment(contentId, newComment.getContentType(), newComment);
            });
        } else {
            return mApiService.postComment(contentId, newComment.getContentType(), newComment);
        }
    }

    private String getFistLinkFromComment(String content) {
        return GenerateLinkPreviewUtil.getFirstUrlIndex(content);
    }

    public Observable<Response<Comment>> postSubComment(String mainCommentId, Comment comment) {
        Comment newComment = Comment.getNewComment(comment, true);
        String fistLinkFromComment = getFistLinkFromComment(comment.getContent());
        if (!TextUtils.isEmpty(fistLinkFromComment)) {
            return LinkPreviewRetriever.getPreviewLink(getContext(), mApiService,
                    fistLinkFromComment).concatMap((Function<Response<LinkPreviewModel>,
                    ObservableSource<Response<Comment>>>) linkPreviewModelResponse -> {
                if (linkPreviewModelResponse.body() != null) {
                    newComment.setMetaPreview(Collections.singletonList(linkPreviewModelResponse.body()));
                }
                return mApiService.postSubComment(mainCommentId, newComment);
            });
        } else {
            return mApiService.postSubComment(mainCommentId, newComment);
        }
    }

    public Observable<Response<Comment>> updateComment(String id, CommentBody comment) {
        String fistLinkFromComment = getFistLinkFromComment(comment.getMessage());
        if (!TextUtils.isEmpty(fistLinkFromComment)) {
            return LinkPreviewRetriever.getPreviewLink(getContext(), mApiService,
                    fistLinkFromComment).concatMap((Function<Response<LinkPreviewModel>,
                    ObservableSource<Response<Comment>>>) linkPreviewModelResponse -> {
                if (linkPreviewModelResponse.body() != null) {
                    comment.setMetaPreview(Collections.singletonList(linkPreviewModelResponse.body()));
                } else {
                    comment.setMetaPreview(new ArrayList<>());
                }
                return mApiService.updateComment(id, comment);
            });
        } else {
            comment.setMetaPreview(new ArrayList<>());
            return mApiService.updateComment(id, comment);
        }
    }

    public Observable<Response<Object>> deleteComment(String id) {
        return mApiService.deleteComment(id);
    }

    public Observable<Response<Object>> deleteSubComment(String subCommentId) {
        return mApiService.deleteSubComment(subCommentId);
    }

    public Observable<Response<List<Comment>>> getCommentList(String contentId, boolean isLoadMore) {
        if (!isLoadMore) {
            resetPagination();
        }

        return mApiService.getCommentByContentId(contentId, isLoadMore ? getCurrentPage() : "0", getPaginationSize())
                .concatMap(listResponse -> {
                    LoadMoreWrapper<Comment> result = listResponse.body();
                    PopUpCommentDataManager.this.checkPagination(result);
                    List<Comment> data = new ArrayList<>();
                    if (result != null && result.getData() != null) {
                        data.addAll(result.getData());
                    }
                    recordNewReadComment(data);
                    return Observable.just(Response.success(data));
                });
    }

    private void recordNewReadComment(List<Comment> commentList) {
        for (Comment comment : commentList) {
            if (comment.getContentStat() != null && !comment.getContentStat().getCommentRead()) {
                //This comment has never read before, so will added it into new read list comment list
                if (!mNewReadCommentCounterList.contains(comment.getId())) {
                    mNewReadCommentCounterList.add(comment.getId());
                }
                //Wil check its sub comment too
                if (comment.getReplyComment() != null) {
                    for (Comment subComment : comment.getReplyComment()) {
                        if (!mNewReadCommentCounterList.contains(subComment.getId())) {
                            mNewReadCommentCounterList.add(subComment.getId());
                        }
                    }
                }
            }
        }
    }

    public Observable<Response<List<Comment>>> getCommentListWithJumpSupport(boolean isLoadMore,
                                                                             String contentId,
                                                                             String loadDirection) {
        if (isInJumpCommentMode() && !TextUtils.isEmpty(mRedirectionNotificationData.getCommentId())) {
            //Handle for jump comment
            return mApiService.jumpForComment(getContentIdForJumpComment(),
                    getPaginationCommentDateForJump(loadDirection, mPreviousJumpCommentDate, mNextJumpCommentDate),
                    loadDirection,
                    getPaginationSize()).concatMap(jumpCommentResponseResponse -> {
                if (jumpCommentResponseResponse.body() != null) {
                    setJumpPagination(isLoadMore, loadDirection, jumpCommentResponseResponse.body());
                }
                return Observable.just(Response.success(jumpCommentResponseResponse.body().getCommentList()));
            });
        } else {
            return getCommentList(contentId, isLoadMore);
        }
    }

    public Observable<List<UserMentionable>> getUserMentionList(String userName) {
        return ConvertUserToMentionableUtil.getUserMentionList(mContext, mApiService, userName);
    }

    public Observable<Response<List<Comment>>> getSubCommentList(boolean isLoadMore,
                                                                 String rootCommentId,
                                                                 String loadDirection) {
        if (isInJumpCommentMode()) {
            //Handle for jump sub comment
            return mApiService.jumpForSubComment(mRedirectionNotificationData.getCommentId(),
                    getPaginationCommentDateForJump(loadDirection, mPreviousJumpCommentDate, mNextJumpCommentDate),
                    loadDirection,
                    getPaginationSize()).concatMap(jumpCommentResponseResponse -> {
                if (jumpCommentResponseResponse.body() != null) {
                    setJumpPagination(isLoadMore, loadDirection, jumpCommentResponseResponse.body());
                    mMainCommentOfJumpSubComment = jumpCommentResponseResponse.body().getMainComment();
                }
                return Observable.just(Response.success(jumpCommentResponseResponse.body().getCommentList()));
            });
        } else {
            if (!isLoadMore) {
                resetPagination();
            }
            return mApiService.getSubCommentList(rootCommentId,
                    getCurrentPage(),
                    getPaginationSize())
                    .concatMap((Response<LoadMoreWrapper<Comment>> listResponse) -> {
                        LoadMoreWrapper<Comment> result = listResponse.body();
                        PopUpCommentDataManager.this.checkPagination(result);
                        List<Comment> data = new ArrayList<>();
                        if (result != null && result.getData() != null) {
                            data.addAll(result.getData());
                        }
                        return Observable.just(Response.success(data));
                    });
        }
    }

    private void setJumpPagination(boolean isLoadMore, String loadDirection, JumpCommentResponse response) {
        /*
        Will check to update jump pagination base on the load more redirection.
         */
        if (!isLoadMore) {
            mPreviousJumpCommentDate = response.getPreviousRedirection();
            mNextJumpCommentDate = response.getNextRedirection();
        } else {
            if (TextUtils.equals(loadDirection, JumpCommentResponse.REDIRECTION_NEXT)) {
                mNextJumpCommentDate = response.getNextRedirection();
            } else {
                mPreviousJumpCommentDate = response.getPreviousRedirection();
            }
        }
    }

    private String getPaginationCommentDateForJump(String loadDirection, String previousCommentDate, String nextCommentDate) {
        String token = "";
        if (!TextUtils.isEmpty(loadDirection)) {
            if (isInJumpCommentMode()) {
                if (TextUtils.equals(loadDirection, JumpCommentResponse.REDIRECTION_PREVIOUS)) {
                    token = previousCommentDate;
                } else {
                    token = nextCommentDate;
                }
            } else {
                token = previousCommentDate;
            }
        } else {
            token = previousCommentDate;
        }

        if (TextUtils.isEmpty(token)) {
            //Assign the default one
            if (!TextUtils.isEmpty(mRedirectionNotificationData.getSubCommentId())) {
                token = mRedirectionNotificationData.getSubCommentId();
            } else {
                token = mRedirectionNotificationData.getCommentId();
            }
        }

        return token;
    }

    public String getContentIdForJumpComment() {
        if (!TextUtils.isEmpty(mRedirectionNotificationData.getMediaId())) {
            return mRedirectionNotificationData.getMediaId();
        } else {
            return mRedirectionNotificationData.getPostId();
        }
    }

    public Observable<Response<Object>> postLikeComment(String commentId, boolean isLike, String postId) {
        if (!isLike) {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
            return mApiService.likeContent(ContentType.COMMENT.getValue(), commentId, new LikeRequestBody(postId));
        } else {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
            return mApiService.unlikeContent(commentId);
        }
    }

    public Observable<Response<Object>> postLikeSubComment(String commentId, boolean isLike, String postId) {
        if (!isLike) {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
            return mApiService.likeContent(ContentType.SUB_COMMENT.getValue(), commentId, new LikeRequestBody(postId));
        } else {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
            return mApiService.unlikeContent(commentId);
        }
    }

    public Observable<Response<Comment>> updateSubComment(String subCommentId, CommentBody commentBody) {
        String fistLinkFromComment = getFistLinkFromComment(commentBody.getMessage());
        if (!TextUtils.isEmpty(fistLinkFromComment)) {
            return LinkPreviewRetriever.getPreviewLink(getContext(), mApiService,
                    fistLinkFromComment).concatMap((Function<Response<LinkPreviewModel>,
                    ObservableSource<Response<Comment>>>) linkPreviewModelResponse -> {
                if (linkPreviewModelResponse.body() != null) {
                    commentBody.setMetaPreview(Collections.singletonList(linkPreviewModelResponse.body()));
                } else {
                    commentBody.setMetaPreview(new ArrayList<>());
                }
                return mApiService.updateSubComment(subCommentId, commentBody);
            });
        } else {
            commentBody.setMetaPreview(new ArrayList<>());
            return mApiService.updateSubComment(subCommentId, commentBody);
        }
    }
}
