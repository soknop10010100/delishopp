package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.services.ApiService;

/**
 * Created by nuonveyo on 4/25/18.
 */

public class LoginDataManager extends AbsLoginDataManager {

    public LoginDataManager(Context context, ApiService publicApiService, ApiService privateApiService) {
        super(context, publicApiService, privateApiService);
    }
}
