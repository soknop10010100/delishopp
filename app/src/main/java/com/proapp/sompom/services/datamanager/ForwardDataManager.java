package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.model.DefaultForwardResult;
import com.proapp.sompom.model.request.LinkPreviewRequest;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by He Rotha on 9/13/18.
 */
public class ForwardDataManager extends AbsLoadMoreDataManager {

    public ForwardDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<DefaultForwardResult>> getSuggestion() {
        return mApiService.getDefaultOrSearch(null);
    }

    public Observable<Response<DefaultForwardResult>> searchPeople(String query) {
        return mApiService.getDefaultOrSearch(query);
    }

    public Observable<Response<LinkPreviewModel>> getPreviewLink(String url) {
        return LinkPreviewRetriever.getPreviewLink(getContext(), mApiService, url);
    }
}
