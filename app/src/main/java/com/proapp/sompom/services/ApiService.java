package com.proapp.sompom.services;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.proapp.sompom.licence.model.SynchroniseData;
import com.proapp.sompom.model.ActiveUser;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.DefaultForwardResult;
import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.GroupMediaSection;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.Price;
import com.proapp.sompom.model.SearchGeneralTypeResult;
import com.proapp.sompom.model.ShopDeliveryDate;
import com.proapp.sompom.model.ShopDeliveryTime;
import com.proapp.sompom.model.SupportCustomErrorResponse2;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.concierge.ConciergeBasket;
import com.proapp.sompom.model.concierge.ConciergeBasketMenuItem;
import com.proapp.sompom.model.concierge.ConciergeBrand;
import com.proapp.sompom.model.concierge.ConciergeCoupon;
import com.proapp.sompom.model.concierge.ConciergeGetDeliveryFeeRequest;
import com.proapp.sompom.model.concierge.ConciergeGetDeliveryFeeResponse;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeOnlinePaymentProvider;
import com.proapp.sompom.model.concierge.ConciergeOrderResponseWithABAPayWay;
import com.proapp.sompom.model.concierge.ConciergeRequestOrderMenuItem;
import com.proapp.sompom.model.concierge.ConciergeShop;
import com.proapp.sompom.model.concierge.ConciergeShopAnnouncementResponse;
import com.proapp.sompom.model.concierge.ConciergeShopCategory;
import com.proapp.sompom.model.concierge.ConciergeShopSetting;
import com.proapp.sompom.model.concierge.ConciergeSupplier;
import com.proapp.sompom.model.concierge.ConciergeUpdateBasketRequest;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.concierge.ConciergeVerifyPaymentWithABAResponse;
import com.proapp.sompom.model.concierge.LoadMoreConciergeBrandItemWrapper;
import com.proapp.sompom.model.concierge.LoadMoreConciergeCategoryItemWrapper;
import com.proapp.sompom.model.concierge.LoadMoreConciergeSubCategoryWrapper;
import com.proapp.sompom.model.concierge.WalletHistory;
import com.proapp.sompom.model.request.AddOrRemoveGroupOwnerBody;
import com.proapp.sompom.model.request.AddOrRemoveGroupParticipantBody;
import com.proapp.sompom.model.request.AddPlayerIdBody;
import com.proapp.sompom.model.request.ChangePasswordRequestBody;
import com.proapp.sompom.model.request.CheckEmailRequestBody;
import com.proapp.sompom.model.request.CheckPhoneExistRequestBody;
import com.proapp.sompom.model.request.CommentBody;
import com.proapp.sompom.model.request.FollowBody;
import com.proapp.sompom.model.request.ForgotPasswordRequestBody;
import com.proapp.sompom.model.request.JoinChannelStatusUpdateBody;
import com.proapp.sompom.model.request.JumpSearchRequestBody;
import com.proapp.sompom.model.request.JumpSearchResult;
import com.proapp.sompom.model.request.LikeRequestBody;
import com.proapp.sompom.model.request.LinkPreviewRequest;
import com.proapp.sompom.model.request.LoginBody;
import com.proapp.sompom.model.request.Mention;
import com.proapp.sompom.model.request.PhoneLoginRequest;
import com.proapp.sompom.model.request.QueryProduct;
import com.proapp.sompom.model.request.RefreshTokenRequest;
import com.proapp.sompom.model.request.ReportRequest;
import com.proapp.sompom.model.request.RequestBuyProductRequest;
import com.proapp.sompom.model.request.RequestGuestUserBody;
import com.proapp.sompom.model.request.ResetPasswordRequestBody;
import com.proapp.sompom.model.request.ResetPasswordRequestBody2;
import com.proapp.sompom.model.request.ResetPasswordViaPhoneRequestBody;
import com.proapp.sompom.model.request.SaveCustomerLocationRequestBody;
import com.proapp.sompom.model.request.SearchGroupMessageRequestBody;
import com.proapp.sompom.model.request.SignUpUserBody;
import com.proapp.sompom.model.request.SocialRequest;
import com.proapp.sompom.model.request.TelegramRedirectBody;
import com.proapp.sompom.model.request.VerifyPhoneRequestBody;
import com.proapp.sompom.model.result.AppFeature;
import com.proapp.sompom.model.result.AppSetting;
import com.proapp.sompom.model.result.AuthResponse;
import com.proapp.sompom.model.result.BaseSupportCustomErrorResponse;
import com.proapp.sompom.model.result.Category;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.CheckEmailResponse;
import com.proapp.sompom.model.result.CheckLatestMessageCountResponse;
import com.proapp.sompom.model.result.CheckPhoneExistResponse;
import com.proapp.sompom.model.result.CheckWorkingHourResponse;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.model.result.ConciergeCheckTrackingOrderResponse;
import com.proapp.sompom.model.result.ConciergeDriverLocationResponse;
import com.proapp.sompom.model.result.ConciergeGetWalletResponse;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.model.result.ConciergeOrderResponse;
import com.proapp.sompom.model.result.ConciergeShopFeature;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.CustomMessageResponse;
import com.proapp.sompom.model.result.GetAllConversationResponse;
import com.proapp.sompom.model.result.GuestUserAuthResponse;
import com.proapp.sompom.model.result.InBoardItemWrapper;
import com.proapp.sompom.model.result.JumpCommentResponse;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.model.result.MoreGame;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.Result;
import com.proapp.sompom.model.result.SearchGeneralUserWrapper;
import com.proapp.sompom.model.result.SignUpResponse;
import com.proapp.sompom.model.result.SignUpResponse2;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.model.result.TelegramChatRequest;
import com.proapp.sompom.model.result.TelegramRedirectUser;
import com.proapp.sompom.model.result.Token;
import com.proapp.sompom.model.result.UnreadCommentResponse;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.model.result.UserBadge;
import com.proapp.sompom.model.result.VerifyPhoneResponse;
import com.proapp.sompom.model.result.WallLoadMoreWrapper;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by he.rotha on 4/19/16.
 */

public interface ApiService {

    @GET("/wp-content/uploads/ads/{moregame-service}")
    Observable<List<MoreGame>> getMoreGame(@Path("moregame-service") String moregameService);

    @POST("v2/login/email")
    Observable<Response<AuthResponse>> loginViaEmail(@Body LoginBody body, @Query("_locale") String locale);

    @POST("auth/login/phone")
    Observable<Response<AuthResponse>> loginViaPhone(@Body LoginBody body);

    @POST("auth/firebase/verify")
    Observable<Response<VerifyPhoneResponse>> verifyPhoneNumberWithFirebase(@Body VerifyPhoneRequestBody body);

    @POST("v1/auth/check/email")
    Observable<Response<CheckEmailResponse>> checkIfEmailExist(@Body CheckEmailRequestBody body);

    @POST("v1/user/phone/exist")
    Observable<Response<CheckPhoneExistResponse>> checkIfPhoneExist(@Body CheckPhoneExistRequestBody body);

    @POST("v1/user/forgetpassword")
    Observable<Response<SupportCustomErrorResponse2>> forgotPassword2(@Body ForgotPasswordRequestBody body, @Query("_locale") String locale);

    @POST("users/forget-password")
    Observable<Response<CustomMessageResponse>> forgotPassword(@Body ForgotPasswordRequestBody body);

    @PUT("users/changepassword/{userId}")
    Observable<Response<SupportCustomErrorResponse2>> changePassword(@Path("userId") String userId, @Body ChangePasswordRequestBody body, @Query("_locale") String locale);

    @POST("users/reset-password")
    Observable<Response<SupportCustomErrorResponse2>> resetPassword(@Body ResetPasswordRequestBody body);

    @PUT("v1/user/resetpassword")
    Observable<Response<SupportCustomErrorResponse2>> resetPassword2(@Body ResetPasswordRequestBody2 body, @Query("_locale") String locale);

    @POST("v1/auth/phone/reset-password")
    Observable<Response<BaseSupportCustomErrorResponse>> resetPasswordViaPhone(@Body ResetPasswordViaPhoneRequestBody body,
                                                                               @Query("_locale") String locale);

    @POST("auth/register/email")
    Observable<Response<SignUpResponse2>> registerViaEmail(@Body SignUpUserBody body, @Query("_locale") String locale);

    @POST("auth/register/phone")
    Observable<Response<SignUpResponse>> registerViaPhone(@Body SignUpUserBody body);

    @PUT("v2/user/{id}")
    Observable<Response<User>> updateUser(@Path("id") String id, @Body JsonObject user, @Query("_locale") String locale);

    @PUT("v2/user/{id}")
    Observable<Response<User>> updateUser2(@Path("id") String id, @Body JsonObject user);

    @GET("v2/user/{id}")
    Observable<Response<User>> getUserById(@Path("id") String userId, @Query("_locale") String locale);

    @POST("auth/local/social")
    Observable<Response<Object>> checkFbExist(@Body SocialRequest request);

    @POST("auth/local/phoneexists/")
    Observable<Response<Object>> checkPhoneExist(@Body PhoneLoginRequest request);

    @POST("api/products")
    Observable<Response<Product>> postProduct(@Body Product product);

    @PUT("api/products/{id}")
    Observable<Response<Product>> updateProduct(@Path("id") String proId, @Body Product product);

    @GET("api/category/get/{language}")
    Observable<Response<List<Category>>> getCategoryList(@Path("language") String lang);


    @GET("api/products/list/seller/{sellerId}")
    Observable<Response<List<Product>>> getProductListByUserId(@Path("sellerId") String userID,
                                                               @Query("_page") int pageNumber,
                                                               @Query("status") int status);

    @GET("api/products/{id}")
    Observable<Response<Product>> getProductById(@Path("id") String id);

    @DELETE("api/products/{id}")
    Observable<Response<Object>> deleteProduct(@Path("id") String id);

    @GET("api/conversation/seller/{productId}/{next}/{value}")
    Observable<Response<LoadMoreWrapper<Conversation>>> getConversationProductHistory(@Path("productId") String porductId,
                                                                                      @Path("next") String next,
                                                                                      @Path("value") int value);

    @GET("v2/post/likeList/{contentId}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<User>>> getLikedUserContentList(@Path("contentId") String contentId,
                                                                        @Path("page") String page,
                                                                        @Path("limit") int limit,
                                                                        @Query("_locale") String locale);

    @POST("api/v2/wall")
    Observable<Response<LifeStream>> postWallStreet(@Body LifeStream lifeStream);

    @PUT("api/v2/post/{id}")
    Observable<Response<LifeStream>> updateWallStreet(@Path("id") String id,
                                                      @Body LifeStream lifeStream);

    @GET("api/v2/post/{id}")
    Observable<Response<LifeStream>> getWallStreetDetail(@Path("id") String id);

    @GET("api/v2/comment/{contentId}/{oldestCommentDate}/{limit}?device=android")
    Observable<Response<LoadMoreWrapper<Comment>>> getCommentByContentId(@Path("contentId") String contentId,
                                                                         @Path("oldestCommentDate") String oldestCommentDate,
                                                                         @Path("limit") int limit);

    @DELETE("api/v2/comment/{id}")
    Observable<Response<Object>> deleteComment(@Path("id") String id);

    @PUT("api/v2/comment/{id}")
    Observable<Response<Comment>> updateComment(@Path("id") String id,
                                                @Body CommentBody comment);

    @POST("api/v2/comment/{contentType}/{contentId}")
    Observable<Response<Comment>> postComment(@Path("contentId") String contentId,
                                              @Path("contentType") String contentType,
                                              @Body Comment comment);

    @POST("api/v2/subcomment/{commentId}")
    Observable<Response<Comment>> postSubComment(@Path("commentId") String commentId,
                                                 @Body Comment comment);

    @PUT("api/v2/subcomment/{subCommentId}")
    Observable<Response<Comment>> updateSubComment(@Path("subCommentId") String subCommentId,
                                                   @Body CommentBody comment);

    @DELETE("api/v2/subcomment/{subCommentId}")
    Observable<Response<Object>> deleteSubComment(@Path("subCommentId") String subCommentId);

    @GET("api/v2/subcomment/{commentId}/{oldestCommentDate}/{limit}")
    Observable<Response<LoadMoreWrapper<Comment>>> getSubCommentList(@Path("commentId") String commentId,
                                                                     @Path("oldestCommentDate") String oldestCommentDate,
                                                                     @Path("limit") int limit);

    @POST("api/v2/unlike/{contentId}")
    Observable<Response<Object>> unlikeContent(@Path("contentId") String contentId);

    @POST("api/v2/like/{contentType}/{contentId}")
    Observable<Response<Object>> likeContent(@Path("contentType") String contentType,
                                             @Path("contentId") String contentId,
                                             @Body LikeRequestBody body);


    @GET("v2/message/{conversationId}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<Chat>>> getChatList(@Path("conversationId") String conversationId,
                                                            @Path("page") String page,
                                                            @Path("limit") int limit);

    @GET("v1/message/count/latest/{conversationId}/{lastLocalMessageId}")
    Observable<Response<CheckLatestMessageCountResponse>> checkLatestMessageCount(@Path("conversationId") String conversationId,
                                                                                  @Path("lastLocalMessageId") String lastLocalMessageId);

    @GET("api/conversationbyconvid/{conversationId}")
    Observable<Response<Conversation>> getConversationDetail(@Path("conversationId") String conversationId);

    @GET("v2/message/latest/{conversationId}/{messageId}")
    Observable<Response<List<Chat>>> getLatestChatList(@Path("conversationId") String conversationId,
                                                       @Path("messageId") String latestMessageId);

    @GET("v1/conversation/all/{page}/{limit}")
    Observable<Response<GetAllConversationResponse>> getAllConversation(@Path("page") String page,
                                                                        @Path("limit") int limit);

    @GET("api/conversation/{type}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<Conversation>>> getConversationList(@Path("type") String type,
                                                                            @Path("page") String page,
                                                                            @Path("limit") int limit);

    @GET("api/pro/group/authorized/{userId}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<Conversation>>> getGroupConversationList(@Path("userId") String userId,
                                                                                 @Path("page") String page,
                                                                                 @Path("limit") int limit);

    @PUT("groups/{groupId}")
    Observable<Response<GroupDetail>> updateGroup(@Path("groupId") String groupId,
                                                  @Body GroupDetail groupDetail);

    @GET("api/suggestion/seller")
    Observable<Response<ActiveUser>> getSuggestionSeller();

    @GET("api/suggestion/myseller")
    Observable<Response<ActiveUser>> getSuggestionMySeller();

    @GET("api/currencies/{country_code}")
    Observable<Response<List<String>>> getCurrencyByCountryCode(@Path("country_code") String countryCode);

    @GET("api/v2/notification/badge")
    Observable<Response<UserBadge>> getUserBadge();

    @GET("api/products/related/{itemId}")
    Observable<Response<List<Product>>> getRelateProduct(@Path("itemId") String itemId);

    @POST("api/products/clone/{id}")
    Observable<Response<Product>> cloneProduct(@Path("id") String id);


    @POST("report/{reportType}/{contentId}")
    Observable<Response<Object>> report(@Path("reportType") String reportType,
                                        @Path("contentId") String contentId,
                                        @Body ReportRequest reportRequest);

    @POST("api/follow")
    Observable<Response<Object>> postFollow(@Body FollowBody request);

    @POST("api/unfollow")
    Observable<Response<Object>> unFollow(@Body FollowBody request);

    @POST("api/unfollowshop")
    Observable<Response<Object>> unFollowShop(@Body FollowBody request);

    @GET("v1/get/notification/list/{page}/{limit}")
    Observable<Response<JsonObject>> getNotification2(@Path("page") String page,
                                                      @Path("limit") int limit,
                                                      @Query("_locale") String locale);

    @GET("users/following/{userId}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<User>>> getFollowingByUserId(@Path("userId") String userId,
                                                                     @Path("page") String page,
                                                                     @Path("limit") int limit);

    @GET("users/followers/{userId}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<User>>> getFollowerByUserId(@Path("userId") String userId,
                                                                    @Path("page") String page,
                                                                    @Path("limit") int limit);

    @GET("v2/post/viewList/{contentId}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<User>>> getUserView(@Path("contentId") String contentId,
                                                            @Path("page") String page,
                                                            @Path("limit") int limit,
                                                            @Query("_locale") String locale);


    @DELETE("api/v2/post/{id}")
    Observable<Response<Object>> deleteLifeStream(@Path("id") String id);


    @GET("api/v2/wall/{next}/{limit}")
    Observable<Response<WallLoadMoreWrapper<Adaptive>>> getWallStreet(@Path("limit") int limit,
                                                                      @Path("next") String nextPost);

    @GET("api/userwall/{id}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<Adaptive>>> getWallStreetByUserId(@Path("id") String userId,
                                                                          @Path("page") String page,
                                                                          @Path("limit") int limit);

    @GET("api/search")
    Observable<Response<SearchGeneralTypeResult>> searchGeneralByType(@Query("keyword") String keyword,
                                                                      @Query("queryType") String queryType,
                                                                      @Query("nextProduct") String nextProduct,
                                                                      @Query("nextPeople") String nextPeople,
                                                                      @Query("nextBuying") String nextBuying,
                                                                      @Query("nextSelling") String nextSelling,
                                                                      @Query("nextMessage") String nextConversation,
                                                                      @Query("limit") int limit);

    @POST("api/product/search/{page}/{limit}")
    Observable<Response<WallLoadMoreWrapper<Adaptive>>> getProduct(@Body QueryProduct queryProduct,
                                                                   @Path("page") String page,
                                                                   @Path("limit") int limit);

    @GET("api/product/prices")
    Observable<Response<Price>> getMinMaxPrice(@Query("currency") String currency);

    @GET("api/suggested/")
    Observable<Response<List<User>>> getSuggestionList(@Query("type") String type,
                                                       @Query("lat") double lat,
                                                       @Query("long") double lng);

    @POST("api/post/mention/")
    Observable<Response<List<User>>> getUserMentionList(@Body Mention mention);

    @GET("api/pro/search/default")
    Observable<Response<SearchGeneralUserWrapper>> getSearchGeneralUser();

    @GET("api/share/suggested/")
    Observable<Response<DefaultForwardResult>> getDefaultOrSearch(@Query("keyword") String query);

    @GET("api/map")
    Observable<Response<List<User>>> getNearbyUser(@Query("lat") double lat,
                                                   @Query("long") double lng);

    /**
     * Use for request to buy a product.
     */
    @POST("private/buyingrequest.json")
    Observable<Result<Product>> requestBuyProduct(@Body RequestBuyProductRequest request);

    @GET("v2/subordination")
    Observable<Response<List<UserGroup>>> getUserContactList(@Query("_locale") String locale);

    @GET("prosettings")
    Observable<Response<List<AppSetting>>> getAppSetting();

    @POST("push/recordplayerid")
    Observable<Response<Object>> addPlayerId(@Body AddPlayerIdBody body);

    @HTTP(method = "DELETE", path = "/v1/push/remove/playerid", hasBody = true)
    Observable<Response<Object>> removePlayerId(@Body AddPlayerIdBody body);

    @POST("api/linkpreview")
    Observable<Response<LinkPreviewModel>> getPreviewLink(@Body LinkPreviewRequest body);

    @POST("public/refreshtoken/user_id.json")
    Call<Result<Token>> getRefreshToken(@Body RefreshTokenRequest refreshTokenRequest);

    @GET("inboardings")
    Observable<Response<List<InBoardItemWrapper>>> getInBoarding();

    @GET("/clientfeatures")
    Observable<Response<List<SynchroniseData>>> getClientFeatureData();

    @GET("/v1/appfeature")
    Observable<Response<AppFeature>> getAppFeature(@Query("orderType") int orderType,
                                                   @Query("userId") String userId);

    @GET("/v1/shop/havetracking")
    Observable<Response<ConciergeCheckTrackingOrderResponse>> checkPendingOrderStatus();

    @POST("activegroupcall/joinChannel")
    Observable<Response<JsonElement>> notifyJoinGroup(@Body JoinChannelStatusUpdateBody body);

    @POST("activegroupcall/exitChannel")
    Observable<Response<JsonElement>> notifyLeaveGroup(@Body JoinChannelStatusUpdateBody body);


    @GET("api/conversation/media/imagevideo/{groupId}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<GroupMediaSection<Media>>>> getGroupPhotoAndVideoDetail(@Path("groupId") String groupId,
                                                                                                @Path("page") String page,
                                                                                                @Path("limit") int limit);

    @GET("api/conversation/media/file/{groupId}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<GroupMediaSection<Media>>>> getGroupFileDetail(@Path("groupId") String groupId,
                                                                                       @Path("page") String page,
                                                                                       @Path("limit") int limit);

    @GET("api/conversation/media/audio/{groupId}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<GroupMediaSection<Media>>>> getGroupVoiceDetail(@Path("groupId") String groupId,
                                                                                        @Path("page") String page,
                                                                                        @Path("limit") int limit);

    @GET("api/conversation/media/link/{groupId}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<GroupMediaSection<LinkPreviewModel>>>> getGroupLinkDetail(@Path("groupId") String groupId,
                                                                                                  @Path("page") String page,
                                                                                                  @Path("limit") int limit);

    @GET("/groups/{groupId}")
    Observable<Response<GroupDetail>> getGroupDetail(@Path("groupId") String groupId);

    @POST("/groups/addmember")
    Observable<Response<GroupDetail>> addGroupParticipants(@Body AddOrRemoveGroupParticipantBody body);

    @POST("/groups/removemember")
    Observable<Response<GroupDetail>> removeGroupParticipants(@Body AddOrRemoveGroupParticipantBody body);

    @POST("/groups/addgroupowner")
    Observable<Response<GroupDetail>> addGroupOwners(@Body AddOrRemoveGroupOwnerBody body);

    @POST("/groups/removegroupowner")
    Observable<Response<GroupDetail>> removeGroupOwners(@Body AddOrRemoveGroupOwnerBody body);

    @GET("api/pro/group/users/authorized/{userId}")
    Observable<Response<List<User>>> getListOfUserForGroupAddingAction(@Path("userId") String userId);

    @GET("api/mygroups")
    Observable<Response<List<String>>> getActiveGroupList();

    @POST("v2/message/search/{page}/{limit}")
    Observable<Response<JumpSearchResult>> jumpSearchMessage(@Body JumpSearchRequestBody requestBody,
                                                             @Path("page") String page,
                                                             @Path("limit") int limit);

    @POST("api/group/search/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<Chat>>> searchGroupConversation(@Body SearchGroupMessageRequestBody requestBody,
                                                                        @Path("page") String page,
                                                                        @Path("limit") int limit);

    @GET("inworkinghour")
    Observable<Response<CheckWorkingHourResponse>> getCheckingWorkingHour();

    @GET("api/v2/jump/comment/{contentId}/{commentIdOrCommentDate}/{direction}/{limit}?device=android")
    Observable<Response<JumpCommentResponse>> jumpForComment(@Path("contentId") String contentId,
                                                             @Path("commentIdOrCommentDate") String commentIdOrCommentDate,
                                                             @Path("direction") String direction,
                                                             @Path("limit") int limit);

    @GET("api/v2/jump/subcomment/{commentId}/{subCommentIdOrCommentDate}/{direction}/{limit}")
    Observable<Response<JumpCommentResponse>> jumpForSubComment(@Path("commentId") String commentId,
                                                                @Path("subCommentIdOrCommentDate") String subCommentIdOrCommentDate,
                                                                @Path("direction") String direction,
                                                                @Path("limit") int limit);

    @GET("api/unreadcommentbadge/{contentId}")
    Observable<Response<UnreadCommentResponse>> getUnreadCommentCount(@Path("contentId") String contentId);

    @GET("v1/shop/{shopId}")
    Observable<Response<ConciergeShop>> getShopDetail(@Path("shopId") String shopId,
                                                      @Query("onlyDiscount") boolean onlyDiscount,
                                                      @Query("excludeMenu ") boolean excludeItem);

    @GET("v1/shopSetting/timeOptions")
    Observable<Response<List<ShopDeliveryTime>>> getDeliveryTime();

    @GET("v1/get/slot")
    Observable<Response<List<ShopDeliveryDate>>> getTimeSlot(@Query("orderType") int orderType);

    @GET("/v1/order/history/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<ConciergeOrder>>> getConciergeOrderHistory(@Path("page") String page,
                                                                                   @Path("limit") int limit,
                                                                                   @Query("_locale") String locale);

    @GET("/v1/order/driver/location/{orderId}")
    Observable<Response<ConciergeDriverLocationResponse>> getDriverLocation(@Path("orderId") String orderId);

    @GET("v3/order/tracking/{next}/{limit}")
    Observable<Response<LoadMoreWrapper<ConciergeOrder>>> getConciergeTrackingOrder(@Path("next") String next,
                                                                                    @Path("limit") int limit,
                                                                                    @Query("_locale") String locale);

    @GET("v1/address/{next}/{limit}")
    Observable<Response<LoadMoreWrapper<ConciergeUserAddress>>> getConciergeUserAddress(@Path("next") int page,
                                                                                        @Path("limit") int limit,
                                                                                        @Query("orderType") int type);

    @POST("v1/address")
    Observable<Response<ConciergeUserAddress>> createConciergeUserAddress(@Body ConciergeUserAddress newAddress,
                                                                          @Query("_locale") String locale,
                                                                          @Query("orderType") int type);

    @PUT("v1/address/{id}")
    Observable<Response<ConciergeUserAddress>> updateConciergeUserAddress(@Path("id") String addressId,
                                                                          @Body ConciergeUserAddress updatedAddress,
                                                                          @Query("_locale") String locale,
                                                                          @Query("orderType") int type);

    @GET("v1/address/primary")
    Observable<Response<ConciergeUserAddress>> getUserPrimaryAddress(@Query("orderType") int type);

    @POST("v1/get/deliveryfee")
    Observable<Response<ConciergeGetDeliveryFeeResponse>> getDeliveryFee(@Body ConciergeGetDeliveryFeeRequest request, @Query("_locale") String locale);

    @DELETE("v1/address/{id}")
    Observable<Response<SupportConciergeCustomErrorResponse>> deleteConciergeUserAddress(@Path("id") String addressId, @Query("_locale") String locale);

    @GET("v1/order/{id}")
    Observable<Response<ConciergeOrder>> getConciergeOrderHistoryDetail(@Path("id") String id, @Query("_locale") String locale);

    @PUT("v1/order/cancel/{id}")
    Observable<Response<ConciergeOrder>> cancelOrder(@Path("id") String id);

    @GET("v1/shop/feature/{next}/{limit}")
    Observable<Response<ConciergeShopFeature>> getShopFeature(@Path("next") int next,
                                                              @Path("limit") int limit,
                                                              @Query("_locale") String locale,
                                                              @Query("userId") String userId);

    @GET("v1/express/shop/feature/{next}/{limit}")
    Observable<Response<ConciergeShopFeature>> getExpressShopFeature(@Path("next") int next,
                                                                     @Path("limit") int limit,
                                                                     @Query("_locale") String locale);

    @GET("v1/get/couponCode")
    Observable<Response<ConciergeCoupon>> getCoupon(@Query("code") String couponCode,
                                                    @Query("_locale") String locale,
                                                    @Query("type") String tpye,
                                                    @Query("order_id") String orderId);

    @GET("v1/get/public/couponCode")
    Observable<Response<List<ConciergeCoupon>>> getPublicCoupon();

    @GET("v1/menuItem/{itemId}")
    Observable<Response<ConciergeMenuItem>> getConciergeItemDetail(@Path("itemId") String itemId,
                                                                   @Query("_locale") String locale,
                                                                   @Query("userId") String userId);

    @GET("v1/menuitem/search/{next}/{limit}")
    Observable<Response<LoadMoreWrapper<ConciergeMenuItem>>> searchGeneralProduct(@Path("next") int next,
                                                                                  @Path("limit") int limit,
                                                                                  @Query("search") String productName,
                                                                                  @Query("type") int type,
                                                                                  @Query("_locale") String locale,
                                                                                  @Query("userId") String userId);

    @GET("v1/express/search/supplier/item/{supplierId}/{next}/{limit}")
    Observable<Response<LoadMoreWrapper<ConciergeMenuItem>>> searchSupplierProduct(@Path("supplierId") String supplierId,
                                                                                   @Path("next") int next,
                                                                                   @Path("limit") int limit,
                                                                                   @Query("search") String productName,
                                                                                   @Query("sort") String sortBy,
                                                                                   @Query("_locale") String locale);

    @GET("v1/shopSetting")
    Observable<Response<ConciergeShopSetting>> getShopSetting(@Query("userId") String userId);

    @GET("v1/wallet/get/current/amount")
    Observable<Response<ConciergeGetWalletResponse>> getUserWallet();

    @POST("v1/order/menuItem")
    Observable<Response<ConciergeOrderResponse>> orderWithCash(@Body JsonObject request,
                                                               @Query("type") int type,
                                                               @Query("_locale") String locale);

    @POST("v1/order/payment/aba")
    Observable<Response<ConciergeOrderResponseWithABAPayWay>> orderWithABAPayWay(@Body JsonObject request, @Query("type") int type, @Query("_locale") String locale);

    @GET("v1/basket/verify/aba")
    Observable<Response<ConciergeVerifyPaymentWithABAResponse>> verifyOrderPaymentStatusWithABA(@Query("type") int type);

    @GET("v1/get/supplier/detail/{supplierId}")
    Observable<Response<ConciergeSupplier>> getSupplierDetail(@Path("supplierId") String supplierId, @Query("_locale") String locale);

    @GET("v1/basket")
    Observable<Response<ConciergeBasket>> getBasket(@Query("type") int type, @Query("_locale") String locale);

    @PUT("v1/basket")
    Observable<Response<ConciergeBasket>> updateBasket(@Body ConciergeUpdateBasketRequest body, @Query("type") int type, @Query("_locale") String locale);

    @POST("v1/basket/reorder/{orderId}")
    Observable<Response<JsonObject>> reorder(@Path("orderId") String orderId, @Query("_locale") String locale);

    @POST("v1/basketitem")
    Observable<Response<ConciergeBasketMenuItem>> addItemToBasket(@Body ConciergeRequestOrderMenuItem body, @Query("type") int type, @Query("_locale") String locale);

    @PUT("v1/basketitem/{itemId}")
    Observable<Response<ConciergeBasketMenuItem>> updateOrRemoveItemInBasket(@Path("itemId") String itemId,
                                                                             @Body ConciergeRequestOrderMenuItem body,
                                                                             @Query("type") int type,
                                                                             @Query("_locale") String locale);

    @DELETE("v1/basket/clear")
    Observable<Response<SupportConciergeCustomErrorResponse>> clearBasket(@Query("type") int type);

    @PUT("v1/basket/open")
    Observable<Response<SupportConciergeCustomErrorResponse>> enableBasket(@Query("type") int type);

    @GET("v1/shopsetting/paymentmethods")
    Observable<Response<List<ConciergeOnlinePaymentProvider>>> getOnlinePaymentProvider();

    @GET("v1/shopsetting/announcement")
    Observable<Response<ConciergeShopAnnouncementResponse>> getShopAnnouncement();

    @GET("v1/itemcategory/shop/{shopId}")
    Observable<Response<List<ConciergeShopCategory>>> getShopCategory(@Path("shopId") String shopId, @Query("_locale") String locale);

    @GET("v1/express/get/category/supplier/{supplierId}")
    Observable<Response<List<ConciergeShopCategory>>> getSupplierCategory(@Path("supplierId") String supplierId, @Query("_locale") String locale);

    @GET("v1/menuitem/shop/category/{shopId}/{categoryId}/{next}/{limit}")
    Observable<Response<LoadMoreConciergeCategoryItemWrapper>> getShopItemByCategoryAndShop(@Path("shopId") String shopId,
                                                                                            @Path("categoryId") String categoryId,
                                                                                            @Path("next") int page,
                                                                                            @Path("limit") int limit,
                                                                                            @Query("filter") String brandIds,
                                                                                            @Query("filterSupplier") String supplierIds,
                                                                                            @Query("oos") int showOutOfStockOption,
                                                                                            @Query("sort") String sortBy,
                                                                                            @Query("_locale") String locale,
                                                                                            @Query("userId") String userId);

    @GET("v1/express/get/supplier/extra/item/{supplierId}/{next}/{limit}")
    Observable<Response<LoadMoreWrapper<JsonObject>>> getExtraExpressSupplierItem(@Path("supplierId") String supplierId,
                                                                                  @Path("next") int page,
                                                                                  @Path("limit") int limit,
                                                                                  @Query("_locale") String locale);

    @GET("v1/get/more/menuitem/subcategory/{subCategoryId}/{next}/{limit}")
    Observable<Response<LoadMoreConciergeSubCategoryWrapper>> getSubCategoryItem(@Path("subCategoryId") String subCategoryId,
                                                                                 @Path("next") int page,
                                                                                 @Path("limit") int limit,
                                                                                 @Query("filter") String brandIds,
                                                                                 @Query("filterSupplier") String supplierIds,
                                                                                 @Query("oos") int showOutOfStockOption,
                                                                                 @Query("sort") String sortBy,
                                                                                 @Query("_locale") String locale,
                                                                                 @Query("userId") String userId);

    @GET("v1/get/item/brand/{brandId}/{next}/{limit}")
    Observable<Response<LoadMoreConciergeBrandItemWrapper>> getBrandItem(@Path("brandId") String subCategoryId,
                                                                         @Path("next") int page,
                                                                         @Path("limit") int limit,
                                                                         @Query("search") String searchText,
                                                                         @Query("sort") String sortBy,
                                                                         @Query("_locale") String locale,
                                                                         @Query("userId") String userId);

    @GET("v1/itemcategory/filter/{categoryId}/1")
    Observable<Response<List<ConciergeBrand>>> getCategoryFilter(@Path("categoryId") String subCategoryId,
                                                                 @Query("supplierId") String supplierId);

    @POST("v1/auth/visitor")
    Observable<Response<GuestUserAuthResponse>> getGuestUser(@Body RequestGuestUserBody body);


    // Telegram
    @GET("v1/telegramconnection")
    Observable<Response<List<TelegramChatRequest>>> getTelegramChatRequest();

    @PUT("v1/telegramconnection/accept/{id}")
    Observable<Response<Conversation>> acceptTelegramChatRequest(@Path("id") String id);

    @PUT("v1/telegramconnection/reject/{id}")
    Observable<Response<Object>> rejectTelegramChatRequest(@Path("id") String id);

    @GET("v1/telegramconnection/redirectusers/{telegramUserId}")
    Observable<Response<List<TelegramRedirectUser>>> getTelegramRedirectUsers(@Path("telegramUserId") String telegramUserId);

    @PUT("v1/telegramconnection/redirect")
    Observable<Response<Object>> redirectTelegramUser(@Body TelegramRedirectBody body);

    @GET("/v1/get/wallet/transaction/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<WalletHistory>>> getWalletHistory(@Path("page") String page,
                                                                          @Path("limit") int limit);

    @POST("/v1/customerlastlocation")
    Observable<Response<Object>> saveCustomerLocation(@Body SaveCustomerLocationRequestBody body);
}

