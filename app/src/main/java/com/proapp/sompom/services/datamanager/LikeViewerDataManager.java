package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.broadcast.SoundEffectService;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.model.request.FollowBody;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ResponseHandleErrorFunc;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class LikeViewerDataManager extends AbsLoadMoreDataManager {

    public LikeViewerDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<List<User>> getUserWhoLikeProduct(String contentID) {
        return mApiService.getLikedUserContentList(contentID,
                getCurrentPage(),
                getPaginationSize(),
                SharedPrefUtils.getLanguage(getContext()))
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(userResultList -> {
                    LoadMoreWrapper<User> loadMoreWrapper = userResultList.body();
                    if (loadMoreWrapper != null) {
                        checkPagination(loadMoreWrapper);
                        List<User> userList = loadMoreWrapper.getData();
                        List<String> authorizedUserId = UserHelper.getUserContactId(getContext());
                        if (userList != null) {
                            for (User user : userList) {
                                user.setIsAuthorizedUser(UserHelper.isInUserContact(authorizedUserId,
                                        user.getId()));
                            }
                            return Observable.just(userList);
                        }
                    }
                    return Observable.empty();
                });
    }

    public Observable<Response<Object>> postFollow(String id) {
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
        FollowBody followBody = new FollowBody(id);
        return mApiService.postFollow(followBody);
    }

    public Observable<Response<Object>> unFollow(String id) {
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        FollowBody followBody = new FollowBody(id);
        return mApiService.unFollow(followBody);
    }
}
