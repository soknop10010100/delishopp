package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.request.VerifyPhoneRequestBody;
import com.proapp.sompom.model.result.VerifyPhoneResponse;
import com.proapp.sompom.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Veasna Chhom on 9/4/21.
 */

public class ConfirmCodeDataManager extends AbsDataManager {

    private ExchangeAuthData mExchangeAuthData;
    private String mFirebaseToken;

    public ConfirmCodeDataManager(Context context, ApiService publicApiService, ExchangeAuthData exchangeAuthData) {
        super(context, publicApiService);
        mExchangeAuthData = exchangeAuthData;
    }

    public ConfirmCodeDataManager(Context context, ApiService publicApiService, ExchangeAuthData exchangeAuthData, String firebaseToken) {
        super(context, publicApiService);
        mExchangeAuthData = exchangeAuthData;
        mFirebaseToken = firebaseToken;
    }

    public ExchangeAuthData getExchangeAuthData() {
        return mExchangeAuthData;
    }

    public String getFirebaseToken() {
        return mFirebaseToken;
    }

    public Observable<Response<VerifyPhoneResponse>> getVerifyPhoneNumber(VerifyPhoneRequestBody body) {
        return mApiService.verifyPhoneNumberWithFirebase(body);
    }
}
