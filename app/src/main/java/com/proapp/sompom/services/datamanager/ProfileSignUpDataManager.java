package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.google.gson.JsonObject;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.licence.db.LicenseSynchronizationDb;
import com.proapp.sompom.licence.model.SynchroniseData;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.emun.AuthConfigurationType;
import com.proapp.sompom.model.request.SignUpUserBody;
import com.proapp.sompom.model.result.SignUpResponse;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Veasna Chhom on 9/4/21.
 */

public class ProfileSignUpDataManager extends AbsDataManager {

    private AuthConfigurationType mAuthConfigurationType;
    private ApiService mPrivateApiService;
    private ExchangeAuthData mExchangeAuthData;

    public ProfileSignUpDataManager(Context context,
                                    ApiService publicApiService,
                                    ApiService privateApiService,
                                    ExchangeAuthData data) {
        super(context, publicApiService);
        mPrivateApiService = privateApiService;
        mExchangeAuthData = data;
        mAuthConfigurationType = ApplicationHelper.getAuthConfigurationType(getContext());
    }

    public AuthConfigurationType getSignUpType() {
        return mAuthConfigurationType;
    }

    public ExchangeAuthData getExchangeAuthData() {
        return mExchangeAuthData;
    }

    public Observable<Response<User>> getUpdateUserProfileService(String fistName, String lastName) {
        JsonObject user = new JsonObject();
        user.addProperty(User.FIELD_FIRST_NAME, fistName);
        user.addProperty(User.FIELD_LAST_NAME, lastName);
        return mPrivateApiService.updateUser(mExchangeAuthData.getUserId(),
                user,
                SharedPrefUtils.getLanguage(getContext()));
    }

    public Observable<Response<JsonObject>> getSynUserService() {
        return mPrivateApiService.getAppSetting()
                .concatMap(listResponse -> {
                    if (listResponse.isSuccessful()) {
                        if (listResponse.body() != null && !listResponse.body().isEmpty())
                            SharedPrefUtils.setAppSetting(ProfileSignUpDataManager.this.getContext(), listResponse.body().get(0));
                        return Observable.just(Response.success(new JsonObject()));
                    } else {
                        return Observable.error(new Exception(listResponse.message()));
                    }
                }).concatMap(jsonObjectResponse -> mPrivateApiService.getShopSetting(ApplicationHelper.getUserId(mContext))
                        .concatMap(response -> {
                            if (response.isSuccessful() && response.body() != null) {
                                ConciergeHelper.setConciergeShopSetting(mContext, response.body());
                                return Observable.just(Response.success(new JsonObject()));
                            } else {
                                return Observable.error(new ErrorThrowable(response.code(), response.message()));
                            }
                        })).concatMap(jsonObjectResponse -> mPrivateApiService.getClientFeatureData()
                        .concatMap(listResponse -> {
                            if (listResponse.isSuccessful()) {
                                if (listResponse.body() != null) {
                                    SynchroniseData synchroniseData = listResponse.body().get(0);
                                    LicenseSynchronizationDb.saveSynchroniseData(getContext(), synchroniseData);
                                    if (synchroniseData.getMessage() != null) {
                                        SharedPrefUtils.setIsEnableEncryption(getContext(), synchroniseData.getMessage().isEnableEncryption());
                                    }
                                }
                                return Observable.just(Response.success(new JsonObject()));
                            } else {
                                return Observable.error(new Exception(listResponse.message()));
                            }
                        }))
                .concatMap(jsonObjectResponse -> mPrivateApiService.getAppFeature(ApplicationHelper.getRequestAPIMode(mContext),
                                ApplicationHelper.getUserId(mContext))
                        .concatMap(response -> {
                            if (response.isSuccessful() && response.body() != null) {
                                SharedPrefUtils.setAppFeature(mContext, response.body());

                                return Observable.just(Response.success(new JsonObject()));
                            } else {
                                return Observable.error(new Exception(response.message()));
                            }
                        }));
    }

    public Observable<Response<SignUpResponse>> getRegisterViaPhoneService(SignUpUserBody body) {
        User guestUser = SharedPrefUtils.getUser(mContext);
        body.setVisitorUserId(guestUser.getId());
        return mApiService.registerViaPhone(body);
    }
}

