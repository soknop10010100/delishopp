package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.model.result.Category;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.services.ApiService;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/20/18.
 */

public class FilterCategoryDataManager extends AbsDataManager {
    public FilterCategoryDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<List<Category>>> getCategoryList() {
        return mApiService.getCategoryList(SharedPrefUtils.getProperLanguage(getContext()));
    }
}
