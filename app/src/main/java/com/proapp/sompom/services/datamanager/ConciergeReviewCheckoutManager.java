package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.PreAPIRequestHelper;
import com.proapp.sompom.model.ConciergeCartModel;
import com.proapp.sompom.model.ConciergeOrderItemDisplayAdaptive;
import com.proapp.sompom.model.concierge.ConciergeBasketMenuItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeOrderResponseWithABAPayWay;
import com.proapp.sompom.model.concierge.ConciergeShop;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.result.ConciergeCheckoutData;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

public class ConciergeReviewCheckoutManager extends AbsLoadMoreDataManager {

    private ConciergeCheckoutData mConciergeCheckoutData;
    private boolean mNeedToResumeABAPayment;

    public ConciergeReviewCheckoutManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public boolean isNeedToResumeABAPayment() {
        return mNeedToResumeABAPayment;
    }

    private float getDeliveryFeeBaseOnShop(List<ConciergeMenuItem> conciergeMenuItemList) {
        /*
            How delivery will be generated:
            Will group the shop from all item orders and the sum delivery fee of each shop together.
         */
        List<ConciergeShop> shops = new ArrayList<>();
        for (ConciergeMenuItem conciergeMenuItem : conciergeMenuItemList) {
            if (conciergeMenuItem != null && !shops.contains(conciergeMenuItem.getShop())) {
                shops.add(conciergeMenuItem.getShop());
            }
        }

        float deliveryFee = 0;
        if (!shops.isEmpty()) {
            for (ConciergeShop shop : shops) {
                deliveryFee += shop.getDeliveryFee();
            }
        }

        return deliveryFee;
    }

    public Observable<Response<ConciergeCheckoutData>> getCartData(boolean isFirstLoadData) {
        mNeedToResumeABAPayment = false;
        if (isFirstLoadData) {
            return PreAPIRequestHelper.requestUserBasket(mContext,
                    mApiService,
                    true,
                    false).concatMap(conciergeBasketResponse -> {
                Timber.i("getTransactionId: " + conciergeBasketResponse.body().getTransactionId());
                if (conciergeBasketResponse.body().isLockedWithPayment()) {
                    return mApiService.getUserWallet().concatMap(conciergeGetWalletResponseResponse -> {
                        if (conciergeGetWalletResponseResponse.isSuccessful()) {
                            return ConciergeHelper.requestABAPaymentFormFromLocalBasket(mContext,
                                            conciergeGetWalletResponseResponse.body().getBalance(),
                                            mApiService).
                                    concatMap(withABAPayWayResponse -> {
                                        if (withABAPayWayResponse.isSuccessful() &&
                                                withABAPayWayResponse.body() != null &&
                                                !withABAPayWayResponse.body().isError() &&
                                                withABAPayWayResponse.body().getPaymentForm() != null) {
                                            ConciergeOrderResponseWithABAPayWay responseWithABAPayWay = withABAPayWayResponse.body();
                                            responseWithABAPayWay.setPaymentProvider(conciergeBasketResponse.body().getPaymentProvider());
                                            mConciergeCheckoutData = new ConciergeCheckoutData();
                                            mConciergeCheckoutData.setResumePaymentData(responseWithABAPayWay);
                                            return ConciergeCartHelper.getCart().concatMap(conciergeCartModel -> {
                                                setCheckoutData(conciergeCartModel);
                                                //Will resume payment screen directly.
                                                mNeedToResumeABAPayment = true;
                                                return Observable.just(Response.success(mConciergeCheckoutData));
                                            });
                                        } else {
                                            return ConciergeCartHelper.getCart().concatMap(conciergeCartModel -> {
                                                setCheckoutData(conciergeCartModel);
                                                return Observable.just(Response.success(mConciergeCheckoutData));
                                            });
                                        }
                                    });
                        } else {
                            return ConciergeCartHelper.getCart().concatMap(conciergeCartModel -> {
                                setCheckoutData(conciergeCartModel);
                                return Observable.just(Response.success(mConciergeCheckoutData));
                            });
                        }
                    });
                } else {
                    return ConciergeCartHelper.getCart().concatMap(conciergeCartModel -> {
                        setCheckoutData(conciergeCartModel);
                        return Observable.just(Response.success(mConciergeCheckoutData));
                    });
                }
            });
        } else {
            return ConciergeCartHelper.getCart().concatMap(conciergeCartModel -> {
                setCheckoutData(conciergeCartModel);
                return Observable.just(Response.success(mConciergeCheckoutData));
            });
        }
    }

    private void setCheckoutData(ConciergeCartModel conciergeCartModel) {
        if (mConciergeCheckoutData == null) {
            mConciergeCheckoutData = new ConciergeCheckoutData();
        }
        ConciergeHelper.loadUserInfo(mContext, mConciergeCheckoutData);
        mConciergeCheckoutData.setDeliveryCharge(getDeliveryFeeBaseOnShop(conciergeCartModel.getBasketItemList()));
        mConciergeCheckoutData.setContainerCharge((float) conciergeCartModel.getContainerFee());
        mConciergeCheckoutData.setCartModel(conciergeCartModel);
        mConciergeCheckoutData.setItemList(parseDisplayOrderItem(conciergeCartModel));
    }

    public Observable<Response<SupportConciergeCustomErrorResponse>> clearBasket() {
        return mApiService.clearBasket(ApplicationHelper.getRequestAPIMode(mContext)).concatMap(response -> {
            if (response.isSuccessful() && response.body() != null && !response.body().isError()) {
                return ConciergeCartHelper.clearCart(mContext).concatMap(o -> Observable.just(response));
            } else {
                return Observable.just(response);
            }
        });
    }

    private List<ConciergeOrderItemDisplayAdaptive> parseDisplayOrderItem(ConciergeCartModel cartModel) {
        List<ConciergeOrderItemDisplayAdaptive> list = new ArrayList<>();
        if (cartModel.getBasketItemList() != null && !cartModel.getBasketItemList().isEmpty()) {
            list.addAll(cartModel.getBasketItemList());
        }

        return list;
    }

    public Observable<Response<ConciergeBasketMenuItem>> removeItemInBasket(String basketITemId, ConciergeMenuItem menuItem) {
        ConciergeMenuItem requestItem = new ConciergeMenuItem();
        requestItem.setId(menuItem.getId());
        requestItem.setBasketItemId(menuItem.getBasketItemId());
        requestItem.setProductCount(0);
        return mApiService.updateOrRemoveItemInBasket(basketITemId,
                ConciergeHelper.createOrderMenuItem(requestItem),
                ApplicationHelper.getRequestAPIMode(mContext),
                LocaleManager.getAppLanguage(mContext));
    }

    public Observable<Response<ConciergeUserAddress>> getPrimaryAddressService() {
        return mApiService.getUserPrimaryAddress(ApplicationHelper.getRequestAPIMode(mContext));
    }

    public Observable<Response<ConciergeCartModel>> updateSelectTimeSlotToLocalBasket(ConciergeCartModel basket) {
        return ConciergeCartHelper.saveBasket(mContext, basket).concatMap(cartModel -> Observable.just(Response.success(cartModel)));
    }
}
