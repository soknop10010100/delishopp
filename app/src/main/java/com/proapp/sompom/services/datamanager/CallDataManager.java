package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.proapp.sompom.model.request.JoinChannelStatusUpdateBody;
import com.proapp.sompom.model.result.JoinOrLeaveGroupCallResponse;
import com.proapp.sompom.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

public class CallDataManager extends AbsDataManager {

    public CallDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<JoinOrLeaveGroupCallResponse>> getNotifyJoinOrLeaveGroupCall(String groupId,
                                                                                            boolean isVideo,
                                                                                            boolean isJoin,
                                                                                            boolean isUpdateToVideoType,
                                                                                            int callDuration,
                                                                                            boolean isCurrentUserCaller) {
        return getNotifyJoinOrLeaveGroupCallInternally(groupId,
                isVideo,
                isJoin,
                isUpdateToVideoType,
                callDuration,
                isCurrentUserCaller).concatMap(response -> {
            if (response.isSuccessful()) {
                JoinOrLeaveGroupCallResponse joinOrLeaveGroupCallResponse = new JoinOrLeaveGroupCallResponse();
                joinOrLeaveGroupCallResponse.setVideoCall(isVideo);
                if (response.body() instanceof JsonPrimitive) {
                    int asInt = response.body().getAsInt();
                    joinOrLeaveGroupCallResponse.setCallStillActive(asInt > 0);
                } else {
                    //There is still active user in call
                    joinOrLeaveGroupCallResponse.setCallStillActive(true);
                }
                return Observable.just(Response.success(joinOrLeaveGroupCallResponse));
            } else {
                return Observable.just(Response.error(response.code(), response.errorBody()));
            }
        });
    }

    public Observable<Response<JsonElement>> getNotifyJoinOrLeaveGroupCallInternally(String groupId,
                                                                                     boolean isVideo,
                                                                                     boolean isJoin,
                                                                                     boolean isUpdateToVideoType,
                                                                                     int callDuration,
                                                                                     boolean isCurrentUserCaller) {
        JoinChannelStatusUpdateBody body = new JoinChannelStatusUpdateBody(getMyUserId(), groupId, isVideo);
        if (isJoin) {
            body.setInitCall(isCurrentUserCaller);
            if (isVideo && isUpdateToVideoType) {
                body.setSendNotification(true);
            }
            return mApiService.notifyJoinGroup(body);
        } else {
            if (callDuration > 0) {
                body.setCallDuration(callDuration);
            }
            return mApiService.notifyLeaveGroup(body);
        }
    }
}
