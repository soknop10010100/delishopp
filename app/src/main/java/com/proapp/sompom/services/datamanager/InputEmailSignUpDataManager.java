package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.helper.NotificationServiceHelper;
import com.proapp.sompom.helper.PreAPIRequestHelper;
import com.proapp.sompom.helper.SentryHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.emun.NotificationProviderType;
import com.proapp.sompom.model.request.AddPlayerIdBody;
import com.proapp.sompom.model.request.SignUpUserBody;
import com.proapp.sompom.model.result.AuthResponse;
import com.proapp.sompom.model.result.SignUpResponse2;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.resourcemanager.helper.LocaleManager;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Response;

/**
 * Created by Veasna Chhom on 2/10/22.
 */

public class InputEmailSignUpDataManager extends AbsDataManager {

    private final String mEmail;
    private ApiService mPrivateApiService;

    public InputEmailSignUpDataManager(Context context,
                                       ApiService apiService,
                                       ApiService privateApiService,
                                       String email) {
        super(context, apiService);
        mEmail = email;
        mPrivateApiService = privateApiService;
    }

    public String getEmail() {
        return mEmail;
    }

    public Observable<Response<SignUpResponse2>> getRegisterViaEmailService(SignUpUserBody signUpUserBody) {
        User guestUser = SharedPrefUtils.getUser(mContext);
        signUpUserBody.setVisitorUserId(guestUser.getId());
        return mApiService.registerViaEmail(signUpUserBody, LocaleManager.getAppLanguage(mContext)).concatMap(signUpResponseResponse -> {
            if (signUpResponseResponse.isSuccessful() && !signUpResponseResponse.body().isError()) {
                SharedPrefUtils.setAccessToken(signUpResponseResponse.body().getAccessToken(), getContext());
                SharedPrefUtils.setUserValue(signUpResponseResponse.body().getUser(), getContext());
                SentryHelper.setUser(signUpResponseResponse.body().getUser());
                PreAPIRequestHelper.requestMandatoryAPIs(mContext, mPrivateApiService, mPrivateApiService, Observable.just(signUpResponseResponse));
            }

            return Observable.just(signUpResponseResponse);
        });
    }

    public Observable<Response<Object>> addPlayerId(AddPlayerIdBody body) {
        return mPrivateApiService.addPlayerId(body);
    }
}

