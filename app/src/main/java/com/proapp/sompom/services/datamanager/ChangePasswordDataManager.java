package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.model.SupportCustomErrorResponse2;
import com.proapp.sompom.model.emun.ChangePasswordActionType;
import com.proapp.sompom.model.request.ChangePasswordRequestBody;
import com.proapp.sompom.model.request.ResetPasswordRequestBody;
import com.proapp.sompom.model.request.ResetPasswordViaPhoneRequestBody;
import com.proapp.sompom.model.result.BaseSupportCustomErrorResponse;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.resourcemanager.helper.LocaleManager;

import io.reactivex.Observable;
import retrofit2.Response;

public class ChangePasswordDataManager extends AbsDataManager {

    private final ChangePasswordActionType mActionType;
    private final String mUserToken;
    private final String mPhoneNumber;
    private final ApiService mPublicAPIService;

    public ChangePasswordDataManager(Context context,
                                     ApiService apiService,
                                     ApiService publicAPIService,
                                     ChangePasswordActionType actionType,
                                     String userToken,
                                     String phoneNumber) {
        super(context, apiService);
        mPublicAPIService = publicAPIService;
        mActionType = actionType;
        mUserToken = userToken;
        mPhoneNumber = phoneNumber;
    }

    public ChangePasswordActionType getActionType() {
        return mActionType;
    }

    public Observable<Response<SupportCustomErrorResponse2>> getChangePasswordService(ChangePasswordRequestBody body) {
        return mApiService.changePassword(SharedPrefUtils.getUserId(mContext), body, LocaleManager.getAppLanguage(mContext));
    }

    public Observable<Response<SupportCustomErrorResponse2>> getResetPasswordViaEmailService(String password) {
        return mPublicAPIService.resetPassword(new ResetPasswordRequestBody(mUserToken, password, password));
    }

    public Observable<Response<BaseSupportCustomErrorResponse>> getResetPasswordViaPhoneService(String password) {
        return mPublicAPIService.resetPasswordViaPhone(new ResetPasswordViaPhoneRequestBody(mUserToken,
                        mPhoneNumber,
                        password),
                LocaleManager.getValidDeviceLangeCode());
    }
}
