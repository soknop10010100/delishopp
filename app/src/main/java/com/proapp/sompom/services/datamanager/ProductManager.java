package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.broadcast.SoundEffectService;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.request.ReportRequest;
import com.proapp.sompom.model.result.Category;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.observable.ResponseHandleErrorFunc;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by He Rotha on 10/9/17.
 */

public class ProductManager extends AbsDataManager {

    public ProductManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<Product>> getProductById(String productId, Locations locations) {
        Observable<Response<Product>> productReq;
        productReq = mApiService.getProductById(productId);
        productReq = productReq.concatMap(new ResponseHandleErrorFunc<>(mContext, false));
        return productReq;
    }

    public Observable<Response<List<Product>>> getRelatedProduct(String productId) {
        return mApiService.getRelateProduct(productId);
    }

    public Observable<Response<Product>> postOrUpdate(Product product) {
        if (TextUtils.isEmpty(product.getId())) {
            for (Media media : product.getMedia()) {
                media.setId(null);
            }
            return mApiService.postProduct(product);
        } else {
            return mApiService.updateProduct(product.getId(), product);
        }
    }

    public Observable<Response<List<Category>>> getCategories() {
        return mApiService.getCategoryList(SharedPrefUtils.getProperLanguage(getContext()));
    }

    public Observable<Response<Object>> reportProduct(String id, ReportRequest reportRequest) {
        return mApiService.report(ContentType.PRODUCT.getValue(), id, reportRequest);
    }

    public Observable<Response<Object>> postLikeProduct(String productId, boolean isLike) {
        if (isLike) {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
            return mApiService.likeContent(ContentType.PRODUCT.getValue(), productId, null);
        } else {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
            return mApiService.unlikeContent(productId);
        }
    }

}
