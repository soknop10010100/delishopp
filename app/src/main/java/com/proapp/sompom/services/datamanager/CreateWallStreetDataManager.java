package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.example.usermentionable.model.UserMentionable;
import com.proapp.sompom.helper.LinkPreviewRetriever;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.ConvertUserToMentionableUtil;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 11/29/18.
 */

public class CreateWallStreetDataManager extends StoreDataManager {

    private ApiService mApiServiceTest;

    public CreateWallStreetDataManager(Context context, ApiService apiService, ApiService apiServiceTest) {
        super(context, apiService);
        mApiServiceTest = apiServiceTest;
    }

    public Observable<List<UserMentionable>> getUserMentionList(String userName) {
        return ConvertUserToMentionableUtil.getUserMentionList(mContext, mApiService, userName);
    }

    public Observable<Response<LinkPreviewModel>> getPreviewLink(String url) {
        return LinkPreviewRetriever.getPreviewLink(getContext(), mApiService, url);
    }

    public ApiService getApiService() {
        return mApiService;
    }
}
