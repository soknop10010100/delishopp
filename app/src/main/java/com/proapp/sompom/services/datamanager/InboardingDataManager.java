package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.helper.InBoardingDataHelper;
import com.proapp.sompom.model.InBoardItem;
import com.proapp.sompom.model.result.InBoardItemWrapper;
import com.proapp.sompom.services.ApiService;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Response;
import timber.log.Timber;

public class InboardingDataManager extends AbsDataManager {

    public InboardingDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<List<InBoardItem>>> getInBoarding() {
        return mApiService.getInBoarding()
                .concatMap((Function<Response<List<InBoardItemWrapper>>, ObservableSource<Response<List<InBoardItem>>>>) jsonObjectResponse -> {
                    if (jsonObjectResponse.isSuccessful() && jsonObjectResponse.body() != null) {
                        List<InBoardItem> inBoardingData = InBoardingDataHelper.getInBoardingData(jsonObjectResponse.body());
                        if (!inBoardingData.isEmpty()) {
                            return Observable.just(Response.success(InBoardingDataHelper.getInBoardingData(jsonObjectResponse.body())));
                        } else {
                            return Observable.just(Response.success(getLocalData()));
                        }
                    } else {
                        return Observable.just(Response.success(getLocalData()));
                    }
                });
    }

    public List<InBoardItem> getLocalData() {
        Timber.i("Load local data instead.");
        List<InBoardItemWrapper> localInBoardingData = InBoardingDataHelper.getLocalInBoardingData(getContext());
        return InBoardingDataHelper.getInBoardingData(localInBoardingData);
    }
}
