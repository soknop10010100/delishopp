package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.SoundEffectService;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.PreAPIRequestHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.model.concierge.ConciergeShopSetting;
import com.proapp.sompom.model.emun.StoreStatus;
import com.proapp.sompom.model.request.CheckPhoneExistRequestBody;
import com.proapp.sompom.model.request.FollowBody;
import com.proapp.sompom.model.request.PhoneLoginRequest;
import com.proapp.sompom.model.result.AppSetting;
import com.proapp.sompom.model.result.CheckPhoneExistResponse;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.model.result.NotificationSettingModel;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ResponseHandleErrorFunc;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;


/**
 * Created by He Rotha on 10/10/17.
 */

public class StoreDataManager extends AbsLoadMoreDataManager {

    private static final String JOB = "job";
    private static final String LABEL = "label";

    private ApiService mPublicApiService;
    private double mUserWallet;

    public StoreDataManager(Context context, ApiService apiService) {
        super(context, apiService);
        int size = context.getResources().getInteger(R.integer.number_request_item);
        setPaginationSize(size);
    }

    public double getUserWallet() {
        return mUserWallet;
    }

    public void setUserWallet(double userWallet) {
        mUserWallet = userWallet;
    }

    public StoreDataManager(Context context, ApiService apiService, ApiService publicApiService) {
        super(context, apiService);
        mPublicApiService = publicApiService;
        int size = context.getResources().getInteger(R.integer.number_request_item);
        setPaginationSize(size);
    }

    public Observable<Response<Object>> postFollow(String id) {
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
        FollowBody body = new FollowBody(id);
        return mApiService.postFollow(body);
    }

    public Observable<Response<Object>> unFollow(String id) {
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        FollowBody body = new FollowBody(id);
        return mApiService.unFollow(body);
    }

    public Observable<Response<Object>> unFollowShop(String id) {
        FollowBody body = new FollowBody(id);
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        return mApiService.unFollowShop(body);
    }

    public Observable<Response<User>> getUserById(String userId) {
        return mApiService.getUserById(userId, SharedPrefUtils.getLanguage(mContext));
    }

    public Observable<Response<User>> getMyUserProfile2() {
        return mApiService.getUserWallet().concatMap(response -> {
            if (response.isSuccessful() && response.body() != null) {
                mUserWallet = response.body().getBalance();
            }
            return mApiService.getUserById(getMyUserId(), SharedPrefUtils.getLanguage(mContext));
        });
    }

    private Observable<Response<List<AppSetting>>> getAppSetting() {
        return mApiService.getAppSetting();
    }

    public Observable<Response<User>> updateMyProfile(User user) {
        return mApiService.updateUser(getMyUserId(),
                UserHelper.getUpdateUserObject(user),
                SharedPrefUtils.getLanguage(getContext()));
    }

    public Observable<List<User>> getFollower(String userId) {
        return mApiService.getFollowerByUserId(userId,
                        getCurrentPage(),
                        getPaginationSize())
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(userResultList -> {
                    LoadMoreWrapper<User> loadMoreWrapper = userResultList.body();
                    if (loadMoreWrapper != null) {
                        checkPagination(loadMoreWrapper);
                        List<User> conversationList = loadMoreWrapper.getData();
                        return Observable.just(conversationList);
                    }
                    return Observable.empty();
                });
    }

    public Observable<List<User>> getFollowing(String userId) {
        return mApiService.getFollowingByUserId(userId,
                        getCurrentPage(),
                        getPaginationSize())
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(userResultList -> {
                    LoadMoreWrapper<User> loadMoreWrapper = userResultList.body();
                    if (loadMoreWrapper != null) {
                        checkPagination(loadMoreWrapper);
                        List<User> conversationList = loadMoreWrapper.getData();
                        return Observable.just(conversationList);
                    }
                    return Observable.empty();
                });
    }

    public Observable<List<User>> getUserView(String contentId) {
        return mApiService.getUserView(contentId,
                        getCurrentPage(),
                        getPaginationSize(),
                        SharedPrefUtils.getLanguage(getContext()))
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(userResultList -> {
                    LoadMoreWrapper<User> loadMoreWrapper = userResultList.body();
                    if (loadMoreWrapper != null) {
                        checkPagination(loadMoreWrapper);
                        List<User> userList = loadMoreWrapper.getData();
                        injectAuthorizedUserStatus(userList);
                        return Observable.just(userList);
                    }
                    return Observable.empty();
                });
    }

    public Observable<List<User>> getUserLike(String userId) {
        return mApiService.getLikedUserContentList(userId,
                        getCurrentPage(),
                        getPaginationSize(),
                        SharedPrefUtils.getLanguage(getContext()))
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(userResultList -> {
                    LoadMoreWrapper<User> loadMoreWrapper = userResultList.body();
                    if (loadMoreWrapper != null) {
                        checkPagination(loadMoreWrapper);
                        List<User> userList = loadMoreWrapper.getData();
                        injectAuthorizedUserStatus(userList);
                        return Observable.just(userList);
                    }
                    return Observable.empty();
                });
    }

    private void injectAuthorizedUserStatus(List<User> userList) {
        List<String> authorizedUserId = UserHelper.getUserContactId(getContext());
        if (userList != null) {
            for (User user : userList) {
                user.setIsAuthorizedUser(UserHelper.isInUserContact(authorizedUserId,
                        user.getId()));
            }
        }
    }

    public Observable<Response<Object>> loginWithPhoneNumber(String phone, String token) {
        final PhoneLoginRequest phoneLoginRequest = new PhoneLoginRequest(phone, token);
        return mApiService.checkPhoneExist(phoneLoginRequest);
    }

    public Observable<Response<User>> changeMyStoreStatus(StoreStatus status) {
        User user = new User();
        user.setStatus(status);
        return mApiService.updateUser(getMyUserId(),
                UserHelper.getUpdateUserObject(user),
                SharedPrefUtils.getLanguage(getContext()));
    }

    public Observable<Response<User>> updateNotification(NotificationSettingModel data) {
        User user = new User();
        user.setNotificationSettingModel(data);
        return mApiService.updateUser(getMyUserId(),
                UserHelper.getUpdateUserObject(user),
                SharedPrefUtils.getLanguage(getContext()));
    }

    public Observable<Response<Object>> getGuestUser() {
        return PreAPIRequestHelper.requestGuestUser(getContext(), mPublicApiService);
    }

    public Observable<Response<CheckPhoneExistResponse>> checkIfPhoneNumberExisted(String countryCode, String phoneNumber) {
        return mApiService.checkIfPhoneExist(new CheckPhoneExistRequestBody(countryCode, phoneNumber));
    }

    public Observable<Response<ConciergeShopSetting>> getShopSettingService() {
        return mApiService.getShopSetting(null).concatMap(response -> {
            if (response.isSuccessful() && response.body() != null) {
                ConciergeHelper.setConciergeShopSetting(mContext, response.body());
                return Observable.just(response);
            }

            return Observable.just(response);
        });
    }
}
