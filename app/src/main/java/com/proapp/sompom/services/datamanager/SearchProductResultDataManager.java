package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Or Vitovongsak on 25/1/22.
 */

public class SearchProductResultDataManager extends AbsConciergeProductDataManager {

    public SearchProductResultDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<List<ConciergeMenuItem>> searchProduct(String toSearch, boolean isLoadMore) {
        if (!isLoadMore) {
            resetPagination();
        }

        return mApiService.searchGeneralProduct(Integer.parseInt(getCurrentPage()),
                        getPaginationSize(),
                        toSearch,
                        ApplicationHelper.getRequestAPIMode(mContext),
                        LocaleManager.getAppLanguage(mContext),
                        ApplicationHelper.getUserId(mContext))
                .concatMap(loadMoreWrapperResponse -> {
                    if (loadMoreWrapperResponse.isSuccessful()) {
                        LoadMoreWrapper<ConciergeMenuItem> wrapper = loadMoreWrapperResponse.body();
                        checkPagination(wrapper);
                        return Observable.just(wrapper.getData());
                    } else {
                        return Observable.error(new ErrorThrowable(loadMoreWrapperResponse.code(),
                                mContext.getString(R.string.error_general_description)));
                    }
                });
    }
}
