package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.model.request.SignUpUserBody;
import com.proapp.sompom.model.result.SignUpResponse;
import com.proapp.sompom.model.result.SignUpResponse2;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.resourcemanager.helper.LocaleManager;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Veasna Chhom on 9/4/21.
 */

public class EmailSignUpDataManager extends AbsDataManager {

    public EmailSignUpDataManager(Context context, ApiService publicApiService) {
        super(context, publicApiService);
    }

    public Observable<Response<SignUpResponse2>> getRegisterViaEmailService(String email, String password) {
        SignUpUserBody signUpUserBody = new SignUpUserBody(email, password);
        User guestUser = SharedPrefUtils.getUser(mContext);
        signUpUserBody.setVisitorUserId(guestUser.getId());
        return mApiService.registerViaEmail(signUpUserBody, LocaleManager.getAppLanguage(mContext));
    }
}
