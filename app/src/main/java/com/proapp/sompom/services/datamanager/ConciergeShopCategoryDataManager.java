package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.PreAPIRequestHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.AbsConciergeModel;
import com.proapp.sompom.model.concierge.ConciergeExtraItemTitle;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeShopCategory;
import com.proapp.sompom.model.concierge.ConciergeShopSetting;
import com.proapp.sompom.model.concierge.ConciergeSubCategory;
import com.proapp.sompom.model.concierge.ConciergeSupplier;
import com.proapp.sompom.model.concierge.ConciergeSupportItemAndSubCategoryAdaptive;
import com.proapp.sompom.model.emun.ConciergeViewItemByType;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

public class ConciergeShopCategoryDataManager extends AbsConciergeProductDataManager {

    private String mShopId;
    private List<ConciergeShopCategory> mRequestedConciergeShopCategories;
    private final ConciergeShopCategory mDefaultShopCategory; //ALL Category
    private final Map<OnCallbackListener<List<ConciergeSupportItemAndSubCategoryAdaptive>>, String> mRequestItemByCategoryMap = new HashMap<>();
    private ConciergeSupplier mSupplier;
    private boolean mIsReachedLoadExtraItemState;
    private boolean mIsExtraItemTitleAdded;
    private ConciergeShopCategory mConciergeShopCategory;
    private String mCategoryId;

    public ConciergeShopCategoryDataManager(Context context, ApiService apiService, ConciergeSupplier supplier) {
        super(context, apiService);
        setPaginationSize(20);
        mSupplier = supplier;
        mDefaultShopCategory = ConciergeShopCategory.newAllCategory(context.getString(R.string.shop_category_all_label));
        ConciergeShopSetting conciergeShopSetting = ConciergeHelper.getConciergeShopSetting(mContext);
        if (conciergeShopSetting != null) {
            mShopId = conciergeShopSetting.getPrimaryShopId();
        }
    }

    public ConciergeShopCategoryDataManager(Context context, ApiService apiService, String categoryId) {
        super(context, apiService);
        mSupplier = null;
        mCategoryId = categoryId;
        setPaginationSize(20);
        mDefaultShopCategory = ConciergeShopCategory.newAllCategory(context.getString(R.string.shop_category_all_label));
        ConciergeShopSetting conciergeShopSetting = ConciergeHelper.getConciergeShopSetting(mContext);
        if (conciergeShopSetting != null) {
            mShopId = conciergeShopSetting.getPrimaryShopId();
        }
    }

    public void onReachedLoadExtraItemState() {
        mIsReachedLoadExtraItemState = true;
        resetPagination();
    }

    public String getCategoryTitle() {
        if (mConciergeShopCategory != null) {
            return mConciergeShopCategory.getTitle();
        }

        return "";
    }

    public String getCategoryId() {
        if (mConciergeShopCategory != null) {
            return mConciergeShopCategory.getId();
        }

        return "";
    }

    public boolean isReachedLoadExtraItemState() {
        return mIsReachedLoadExtraItemState;
    }

    public ConciergeSupplier getSupplier() {
        return mSupplier;
    }

    public ConciergeShopCategory getDefaultShopCategory() {
        return mDefaultShopCategory;
    }

    public List<ConciergeShopCategory> getRequestedConciergeShopCategories() {
        return mRequestedConciergeShopCategories;
    }

    private Observable<Response<ConciergeShopSetting>> getShopSettingService() {
        return mApiService.getShopSetting(ApplicationHelper.getUserId(mContext)).concatMap(conciergeShopSettingResponse -> {
            if (conciergeShopSettingResponse.isSuccessful() && conciergeShopSettingResponse.body() != null) {
                ConciergeHelper.setConciergeShopSetting(mContext, conciergeShopSettingResponse.body());
            }
            return Observable.just(conciergeShopSettingResponse);
        });
    }

    public void addRequestItemByCategoryMap(OnCallbackListener<List<ConciergeSupportItemAndSubCategoryAdaptive>> callbackListener, String categoryId) {
        mRequestItemByCategoryMap.put(callbackListener, categoryId);
    }

    public void removeRequestItemByCategoryMap(OnCallbackListener<List<ConciergeSupportItemAndSubCategoryAdaptive>> callbackListener) {
        mRequestItemByCategoryMap.remove(callbackListener);
    }

    public String getRequestCategoryIdByCallback(OnCallbackListener<List<ConciergeSupportItemAndSubCategoryAdaptive>> callbackListener) {
        return mRequestItemByCategoryMap.get(callbackListener);
    }

    public void resetReachExtraItemSection() {
        mIsExtraItemTitleAdded = false;
        mIsReachedLoadExtraItemState = false;
    }

    public Observable<Response<Object>> getBatchRequestOfShopCategoryService() {
        return getCategoryListService().concatMap(categoryResponse -> {
            if (categoryResponse.isSuccessful()) {
                mRequestedConciergeShopCategories = categoryResponse.body();
                if (mSupplier != null) {
                    return getSupplierDetailService(mSupplier.getId()).concatMap(conciergeSupplierResponse -> {
                        if (conciergeSupplierResponse.isSuccessful()) {
                            if (conciergeSupplierResponse.body() != null) {
                                mSupplier = conciergeSupplierResponse.body();
                            }
                            return Observable.just(Response.success(new Object()));
                        } else {
                            return Observable.error(new Throwable(conciergeSupplierResponse.errorBody().string()));
                        }
                    });
                } else {
                    return Observable.just(Response.success(new Object()));
                }
            } else {
                return Observable.error(new Throwable(categoryResponse.errorBody().string()));
            }
        });
    }

    public Observable<Response<ConciergeSupplier>> getSupplierDetailService(String supplierId) {
        return mApiService.getSupplierDetail(supplierId, LocaleManager.getAppLanguage(getContext()));
    }

    public Observable<Response<List<ConciergeShopCategory>>> getCategoryListService() {
        if (mSupplier != null) {
            //Express mode
            return mApiService.getSupplierCategory(mSupplier.getId(), LocaleManager.getAppLanguage(mContext));
        } else {
            return mApiService.getShopCategory(mShopId, LocaleManager.getAppLanguage(mContext));
        }
    }

    public Observable<List<ConciergeSupportItemAndSubCategoryAdaptive>> getShopItemByCategoryReferenceId(boolean isLoadMore,
                                                                                                         List<String> brandId) {
        return getShopItemByCategory(mCategoryId, isLoadMore, brandId);
    }

    @Override
    public int getPaginationSize() {
        ConciergeViewItemByType selectedViewItemByOption = ConciergeHelper.getSelectedViewItemByOption(mContext);
        if (selectedViewItemByOption == ConciergeViewItemByType.THREE_COLUMNS) {
            return 21;
        } else {
            return 20;
        }
    }

    public Observable<List<ConciergeSupportItemAndSubCategoryAdaptive>> getShopItemByCategory(String categoryId,
                                                                                              boolean isLoadMore,
                                                                                              List<String> brandId) {

        if (mIsReachedLoadExtraItemState) {
            return getExtraItemOfExpressSupplier();
        } else {
            if (!isLoadMore) {
                resetPagination();
            }

            String brandIdFilter = PreAPIRequestHelper.getArrayStringIdQuery(brandId);
            String supplierIdFilter = PreAPIRequestHelper.getArrayStringIdQuery(mSupplier != null ? Collections.singletonList(mSupplier.getId()) : new ArrayList<>());
            Timber.i("brandIdFilter: " + brandIdFilter + ", supplierIdFilter: " + supplierIdFilter);

            return mApiService.getShopItemByCategoryAndShop(mShopId,
                            categoryId,
                            Integer.valueOf(getCurrentPage()),
                            getPaginationSize(),
                            brandIdFilter,
                            supplierIdFilter,
                            ConciergeHelper.getShowInAndOutOfStockItem(),
                            ConciergeHelper.getSortOption(),
                            LocaleManager.getAppLanguage(mContext),
                            ApplicationHelper.getUserId(mContext))
                    .concatMap(loadMoreWrapperResponse -> {
                        if (loadMoreWrapperResponse.isSuccessful() &&
                                loadMoreWrapperResponse.body() != null &&
                                loadMoreWrapperResponse.body().getData() != null) {
                            if (mConciergeShopCategory == null) {
                                mConciergeShopCategory = loadMoreWrapperResponse.body().getConciergeCategory();
                            }

                            List<ConciergeSupportItemAndSubCategoryAdaptive> adaptiveList = new ArrayList<>();
                            Gson gson = new Gson();
                            for (int i = 0; i < loadMoreWrapperResponse.body().getData().size(); i++) {
                                JsonObject jsonObject = loadMoreWrapperResponse.body().getData().get(i);
                                boolean subCategoryItem = ConciergeShopCategoryDataManager.this.isSubCategoryItem(jsonObject);
                                if (subCategoryItem) {
                                    ConciergeSubCategory conciergeSubCategory = gson.fromJson(jsonObject.toString(), ConciergeSubCategory.class);
                                    //Set auto expand sub category section for first item.
                                    conciergeSubCategory.setSectionExpended(i == 0 && !isLoadMore);
                                    adaptiveList.add(conciergeSubCategory);
                                } else {
                                    ConciergeMenuItem conciergeMenuItem = gson.fromJson(jsonObject.toString(), ConciergeMenuItem.class);
                                    adaptiveList.add(conciergeMenuItem);
                                }
                            }

                            ConciergeShopCategoryDataManager.this.checkPagination(loadMoreWrapperResponse.body());
                            return Observable.just(new ArrayList<>(adaptiveList));
                        } else {
                            return Observable.error(new Throwable(loadMoreWrapperResponse.errorBody().string()));
                        }
                    });
        }
    }

    public Observable<List<ConciergeSupportItemAndSubCategoryAdaptive>> getExtraItemOfExpressSupplier() {
        return mApiService.getExtraExpressSupplierItem(mSupplier.getId(),
                Integer.valueOf(getCurrentPage()),
                getPaginationSize(),
                LocaleManager.getAppLanguage(mContext)).concatMap(loadMoreWrapperResponse -> {
            if (loadMoreWrapperResponse.isSuccessful() &&
                    loadMoreWrapperResponse.body() != null &&
                    loadMoreWrapperResponse.body().getData() != null) {
                List<ConciergeSupportItemAndSubCategoryAdaptive> adaptiveList = new ArrayList<>();
                Gson gson = new Gson();
                for (int i = 0; i < loadMoreWrapperResponse.body().getData().size(); i++) {
                    JsonObject jsonObject = loadMoreWrapperResponse.body().getData().get(i);
                    ConciergeMenuItem conciergeMenuItem = gson.fromJson(jsonObject.toString(), ConciergeMenuItem.class);
                    adaptiveList.add(conciergeMenuItem);
                }

                ConciergeShopCategoryDataManager.this.checkPagination(loadMoreWrapperResponse.body());
                if (!mIsExtraItemTitleAdded && !adaptiveList.isEmpty()) {
                    mIsExtraItemTitleAdded = true;
                    adaptiveList.add(0, new ConciergeExtraItemTitle());
                }
                return Observable.just(new ArrayList<>(adaptiveList));
            } else {
                if (loadMoreWrapperResponse.errorBody() != null) {
                    return Observable.error(new Throwable(loadMoreWrapperResponse.errorBody().string()));
                } else {
                    return Observable.error(new Throwable(ErrorThrowable.newGeneralErrorInstance(mContext)));
                }
            }
        });
    }

    private boolean isSubCategoryItem(JsonObject jsonObject) {
        if (jsonObject.has(AbsConciergeModel.FIELD_TYPE)) {
            String asString = jsonObject.get(AbsConciergeModel.FIELD_TYPE).getAsString();
            return TextUtils.equals(asString, ConciergeSubCategory.SUB_CATEGORY_TYPE);

        }

        return false;
    }

    public Observable<Response<ConciergeSubCategory>> loadMoreSubCategoryItem(ConciergeSubCategory existingItem, List<String> brandId) {
        String brandIdFilter = PreAPIRequestHelper.getArrayStringIdQuery(brandId);
        String supplierIdFilter = PreAPIRequestHelper.getArrayStringIdQuery(mSupplier != null ? Collections.singletonList(mSupplier.getId()) : new ArrayList<>());
        Timber.i("brandIdFilter: " + brandIdFilter + ", supplierIdFilter: " + supplierIdFilter);

        return mApiService.getSubCategoryItem(existingItem.getId(),
                        Integer.valueOf(existingItem.getNextPage()),
                        getPaginationSize(),
                        brandIdFilter,
                        supplierIdFilter,
                        ConciergeHelper.getShowInAndOutOfStockItem(),
                        ConciergeHelper.getSortOption(),
                        LocaleManager.getAppLanguage(mContext), ApplicationHelper.getUserId(mContext))
                .concatMap(loadMoreWrapperResponse -> {
                    if (loadMoreWrapperResponse.isSuccessful()) {
                /*
                    Will create new ConciergeSubCategory for response. It will contain only new loaded item and
                    the information of next pagination.
              */
                        ConciergeSubCategory subCategory = new ConciergeSubCategory();
                        //Must set id for later usage.
                        subCategory.setId(existingItem.getId());
                        subCategory.setMenuItems(loadMoreWrapperResponse.body().getData());
                        subCategory.setNextPage(loadMoreWrapperResponse.body().getNextPage());
                        return Observable.just(Response.success(subCategory));
                    } else {
                        return Observable.error(ErrorThrowable.newGeneralErrorInstance(mContext));
                    }
                });
    }
}
