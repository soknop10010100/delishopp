package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.broadcast.SoundEffectService;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.R;
import com.proapp.sompom.services.ApiService;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;


/**
 * Created by He Rotha on 10/5/17.
 */

public class ProductStoreDataManager extends AbsLoadMoreDataManager {
    private String mCurrentUserId;
    private int mStatus;

    public ProductStoreDataManager(Context context, ApiService apiService) {
        super(context, apiService);
        int size = context.getResources().getInteger(R.integer.number_request_item);
        setPaginationSize(size);
    }

    public Observable<Response<List<Product>>> getProductByUserId(User user, int status) {
        mStatus = status;
        mCurrentUserId = user.getId();
        resetPagination();
        return mApiService.getProductListByUserId(mCurrentUserId, 1, mStatus);
    }

    public Observable<Response<Product>> soldProduct(String productId) {
        Product product = new Product();
        return mApiService.updateProduct(productId, product);
    }

    public Observable<Response<List<Product>>> loadMore() {
        if (TextUtils.isEmpty(mCurrentUserId)) {
            return Observable.error(new Throwable(mContext.getString(R.string.error_general_description)));
        }
        return mApiService.getProductListByUserId(mCurrentUserId, 1, mStatus);

    }

    public Observable<Response<Object>> delete(Product product) {
        return mApiService.deleteProduct(product.getId());
    }

    public Observable<Response<Product>> closeProduct(String productId) {
        return mApiService.cloneProduct(productId);
    }

    public Observable<Response<Object>> postLikeProduct(String productId, boolean isLike) {
        if (isLike) {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
            return mApiService.likeContent(ContentType.PRODUCT.getValue(), productId, null);
        } else {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
            return mApiService.unlikeContent(productId);
        }
    }
}
