package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.proapp.sompom.model.concierge.ConciergeBrand;
import com.proapp.sompom.model.concierge.ConciergeSupplier;
import com.proapp.sompom.services.ApiService;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 16/11/22.
 */
public class ConciergeFilterProductCriteriaDataManager extends AbsLoadMoreDataManager {

    private String mCategoryId;
    private boolean mShouldShowOutOfStockItem;
    private List<String> mBrandIds;
    private ConciergeSupplier mSupplier;

    public ConciergeFilterProductCriteriaDataManager(Context context,
                                                     ApiService apiService,
                                                     String categoryId,
                                                     List<String> brandIds,
                                                     ConciergeSupplier supplier) {
        super(context, apiService);
        mCategoryId = categoryId;
        mBrandIds = brandIds;
        mSupplier = supplier;
        Timber.i("mBrandIds: " + new Gson().toJson(mBrandIds));
    }

    public Observable<Response<List<ConciergeBrand>>> getConciergeBrand() {
        return mApiService.getCategoryFilter(mCategoryId, mSupplier != null ? mSupplier.getId() : null).concatMap(listResponse -> {
            if (listResponse.isSuccessful() && !listResponse.body().isEmpty() && mBrandIds != null && !mBrandIds.isEmpty()) {
                for (ConciergeBrand brand : listResponse.body()) {
                    for (String brandId : mBrandIds) {
                        if (!TextUtils.isEmpty(brandId) &&
                                !TextUtils.isEmpty(brand.getId()) &&
                                TextUtils.equals(brandId, brand.getId())) {
                            brand.setSelected(true);
                            break;
                        }
                    }
                }
            }

            return Observable.just(listResponse);
        });
    }
}
