package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeSubCategory;
import com.proapp.sompom.model.concierge.LoadMoreConciergeSubCategoryWrapper;
import com.proapp.sompom.model.emun.ConciergeViewItemByType;
import com.proapp.sompom.services.ApiService;
import com.resourcemanager.helper.LocaleManager;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Response;

/**
 * Created by Chhom Veasna on 16/11/22.
 */
public class ConciergeSubCategoryDetailDataManager extends AbsConciergeProductDataManager {

    private ConciergeSubCategory mSubCategory;
    private String mSubcategoryId;

    public ConciergeSubCategoryDetailDataManager(Context context, ApiService apiService, String subcategoryId) {
        super(context, apiService);
        mSubcategoryId = subcategoryId;
    }

    public ConciergeSubCategory getSubCategory() {
        return mSubCategory;
    }

    @Override
    public int getPaginationSize() {
        ConciergeViewItemByType selectedViewItemByOption = ConciergeHelper.getSelectedViewItemByOption(mContext);
        if (selectedViewItemByOption == ConciergeViewItemByType.THREE_COLUMNS) {
            return 21;
        } else {
            return 20;
        }
    }

    public Observable<Response<List<ConciergeMenuItem>>> getSubcategoryItem(boolean isLoadMore) {
        if (!isLoadMore) {
            resetPagination();
        }

        return mApiService.getSubCategoryItem(mSubcategoryId,
                        Integer.valueOf(getCurrentPage()),
                        getPaginationSize(),
                        null,
                        null,
                        ConciergeHelper.getShowInAndOutOfStockItem(),
                        ConciergeHelper.getSortOption(),
                        LocaleManager.getAppLanguage(mContext), ApplicationHelper.getUserId(mContext))
                .concatMap((Function<Response<LoadMoreConciergeSubCategoryWrapper>,
                        ObservableSource<Response<List<ConciergeMenuItem>>>>) response -> {
                    if (response.isSuccessful()) {
                        mSubCategory = response.body().getConciergeSubCategory();
                        checkPagination(response.body());

                        return Observable.just(Response.success(response.body().getData()));
                    } else {
                        return Observable.just(Response.error(response.code(), response.errorBody()));
                    }
                });
    }
}
