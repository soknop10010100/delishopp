package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.services.ApiService;

/**
 * Created by He Rotha on 10/10/17.
 */

public class AbsDataManager {

    public Context mContext;
    public ApiService mApiService;
    private String mMyUserId;

    public AbsDataManager(Context context, ApiService apiService) {
        mContext = context;
        mApiService = apiService;
    }


    public String getMyUserId() {
        if (TextUtils.isEmpty(mMyUserId)) {
            mMyUserId = SharedPrefUtils.getUserId(mContext);
        }
        return mMyUserId;
    }

    public ApiService getApiService() {
        return mApiService;
    }

    public Context getContext() {
        return mContext;
    }
}
