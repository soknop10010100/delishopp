package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.R;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.model.result.Pagination;
import com.proapp.sompom.model.result.ResultList;
import com.proapp.sompom.services.ApiService;

import javax.annotation.Nullable;

import timber.log.Timber;

/**
 * Created by He Rotha on 11/22/17.
 */

public abstract class AbsLoadMoreDataManager extends AbsDataManager {

    private int mSize;
    protected boolean mCanLoadMore = false;
    private Pagination mPagination;
    protected String mCurrentPage = "0";
    private boolean mIsNetworkAvailable;

    public AbsLoadMoreDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public boolean isNetworkAvailable() {
        return mIsNetworkAvailable;
    }

    public void setNetworkAvailable(boolean networkAvailable) {
        mIsNetworkAvailable = networkAvailable;
    }

    public int getPaginationSize() {
        if (mSize == 0) {
            mSize = mContext.getResources().getInteger(R.integer.number_request_item);
        }
        return mSize;
    }

    public void setPaginationSize(int size) {
        mSize = size;
    }


    public boolean isCanLoadMore() {
        return mCanLoadMore;
    }

    public void setCanLoadMore(boolean canLoadMore) {
        mCanLoadMore = canLoadMore;
    }

    public void checkPagination(ResultList<?> list) {
        mCanLoadMore = Pagination.isHasLoadMore(list.getData(), getPaginationSize(), list.getPagination());
        mPagination = list.getPagination();
    }

    @Nullable
    public Pagination getPagination() {
        return mPagination;
    }

    public String getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(String currentPage) {
        mCurrentPage = currentPage;
    }

    public void resetPagination() {
        mCanLoadMore = false;
        setCurrentPage("0");
    }

    public void checkPagination(LoadMoreWrapper<?> list) {
        if (list != null) {
            if (!TextUtils.isEmpty(list.getNextPage()) && list.getData().size() >= getPaginationSize()) {
                mCurrentPage = list.getNextPage();
                mCanLoadMore = true;
                Timber.e("canLoadMore: true");
            } else {
                mCanLoadMore = false;
                Timber.e("canLoadMore: false");
            }
        } else {
            resetPagination();
        }
    }
}
