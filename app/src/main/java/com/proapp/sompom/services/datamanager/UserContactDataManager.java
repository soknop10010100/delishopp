package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.listener.UserListAdaptive;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.functions.Function;
import retrofit2.Response;

public class UserContactDataManager extends AbsLoadMoreDataManager {

    public UserContactDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<List<UserListAdaptive>> getUserGroupListService() {
        return mApiService.getUserContactList(SharedPrefUtils.getLanguage(mContext))
                .concatMap((Function<Response<List<UserGroup>>, ObservableSource<Response<LoadMoreWrapper<UserGroup>>>>) listResponse -> {
                    if (listResponse.isSuccessful()) {
                        //TODO: Need to remove after server implemented pagination.
                        LoadMoreWrapper<UserGroup> loadMoreWrapper = new LoadMoreWrapper<>();
                        loadMoreWrapper.setData(listResponse.body());
                        return Observable.just(Response.success(loadMoreWrapper));
                    } else {
                        return Observable.error(new ErrorThrowable(listResponse.code(),
                                mContext.getString(R.string.error_general_description)));
                    }
                })
                .concatMap((Function<Response<LoadMoreWrapper<UserGroup>>,
                        ObservableSource<List<UserListAdaptive>>>) loadMoreWrapperResponse -> {
                    if (loadMoreWrapperResponse.isSuccessful()) {
                        checkPagination(loadMoreWrapperResponse.body());
                        return new Observable<List<UserListAdaptive>>() {
                            @Override
                            protected void subscribeActual(Observer<? super List<UserListAdaptive>> observer) {
                                if (loadMoreWrapperResponse.isSuccessful()) {
                                    List<UserListAdaptive> newData = new ArrayList<>();
                                    if (loadMoreWrapperResponse.body() != null) {
                                        UserHelper.saveAuthorizedUserListFromResponse(getContext(),
                                                loadMoreWrapperResponse.body().getData());
                                        for (UserGroup userGroup : loadMoreWrapperResponse.body().getData()) {
                                            newData.add(userGroup);
                                            if (userGroup.getParticipants() != null) {
                                                for (User participant : userGroup.getParticipants()) {
                                                    participant.setIsAuthorizedUser(true);
                                                }
                                                newData.addAll(userGroup.getParticipants());
                                            }
                                        }
                                    }
                                    observer.onNext(newData);
                                    observer.onComplete();
                                } else {
                                    observer.onError(new ErrorThrowable(loadMoreWrapperResponse.code(),
                                            mContext.getString(R.string.error_general_description)));
                                }
                            }
                        };
                    } else {
                        return Observable.error(new ErrorThrowable(loadMoreWrapperResponse.code(),
                                mContext.getString(R.string.error_general_description)));
                    }
                });
    }
}
