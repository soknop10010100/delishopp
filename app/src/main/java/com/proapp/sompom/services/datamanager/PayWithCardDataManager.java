package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.model.concierge.ConciergeVerifyPaymentWithABAResponse;
import com.proapp.sompom.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Veasna Chhom on 3/17/22.
 */

public class PayWithCardDataManager extends AbsDataManager {

    private String mBasketId;

    public PayWithCardDataManager(Context context, ApiService apiService, String basketId) {
        super(context, apiService);
        mBasketId = basketId;
    }

    public Observable<Response<ConciergeVerifyPaymentWithABAResponse>> verifyOrderPaymentStatus() {
        return mApiService.verifyOrderPaymentStatusWithABA(ApplicationHelper.getRequestAPIMode(mContext));
    }

    public String getBasketId() {
        return mBasketId;
    }
}
