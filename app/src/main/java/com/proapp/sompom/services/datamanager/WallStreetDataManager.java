package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.SoundEffectService;
import com.proapp.sompom.database.MediaDb;
import com.proapp.sompom.helper.OfflinePostDataHelper;
import com.proapp.sompom.injection.game.MoreGameAPIQualifier;
import com.proapp.sompom.injection.productserialize.ProductSerializeQualifier;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.request.LikeRequestBody;
import com.proapp.sompom.model.request.ReportRequest;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.model.result.MoreGame;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.WallLoadMoreWrapper;
import com.proapp.sompom.observable.ResponseHandleErrorFunc;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.AdsLoaderUtil;
import com.proapp.sompom.utils.DateUtils;
import com.proapp.sompom.utils.NetworkStateUtil;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import retrofit2.Response;
import timber.log.Timber;

//import com.google.android.gms.ads.AdView;

/**
 * Created by He Rotha on 7/10/18.
 */
public class WallStreetDataManager extends StoreDataManager {

    private static final int REQUEST_LIMIT_PER_PAGE = 20;

    private final ApiService mMoreGameService;
    private final ApiService mSerialApiService;
    private String mServerPaginationToken = "0";
    private String mLocalPaginationToken;

    public WallStreetDataManager(Context context,
                                 ApiService apiService,
                                 @ProductSerializeQualifier ApiService serialApiService,
                                 @MoreGameAPIQualifier ApiService moreGame) {
        super(context, apiService);
        setPaginationSize(REQUEST_LIMIT_PER_PAGE);
        mSerialApiService = serialApiService;
        mMoreGameService = moreGame;
    }

    public Observable<WallLoadMoreWrapper<Adaptive>> getData(boolean isLoadMore) {
        if (NetworkStateUtil.isNetworkAvailable(getContext())) {
            return mSerialApiService.getWallStreet(getPaginationSize(), mServerPaginationToken)
                    .concatMap(new ResponseHandleErrorFunc<>(getContext(),
                            false))
                    .concatMap(adaptiveResultList -> {
                        //Will dispatch the error.
                        if (adaptiveResultList.body() == null) {
                            return null;
                        }

                        validTheItem(adaptiveResultList.body().getData(), isLoadMore);
                        MediaDb.backupLocalDownloadedMFileMediaPath(getContext(), MediaDb.getMediaList(adaptiveResultList.body().getData()));
                        checkPage(adaptiveResultList.body(), false);
                        return Observable.just(adaptiveResultList.body());
                    }).concatMap(adaptiveLoadMoreWrapper -> AdsLoaderUtil.loadStreetWall(mContext,
                            mApiService,
                            adaptiveLoadMoreWrapper));
        } else {
            return Observable.create((ObservableEmitter<WallLoadMoreWrapper<Adaptive>> emitter) -> {
                OfflinePostDataHelper.readPostsWithPagination(getContext(),
                        isLoadMore ? mLocalPaginationToken : null,
                        REQUEST_LIMIT_PER_PAGE,
                        new OfflinePostDataHelper.OfflinePostDataHelperListener() {
                            @Override
                            public void onReadFinish(List<LifeStream> lifeStreamList, Throwable e) {
                                Timber.i("onReadFinish: size: " + lifeStreamList.size() + ", Error: " + e);
                                onLocalDataLoadFinished(emitter, lifeStreamList);
                            }
                        });
            });
        }
    }

    private void onLocalDataLoadFinished(ObservableEmitter<WallLoadMoreWrapper<Adaptive>> emitter,
                                         List<LifeStream> lifeStreamList) {
        WallLoadMoreWrapper<Adaptive> wallLoadMoreWrapper = new WallLoadMoreWrapper<>();
        wallLoadMoreWrapper.setData(getAdaptiveList(lifeStreamList));
        if (lifeStreamList.size() >= REQUEST_LIMIT_PER_PAGE) {
            LifeStream lastPost = lifeStreamList.get(lifeStreamList.size() - 1);
            wallLoadMoreWrapper.setLocalPaginationToken(lastPost.getPostId());
            if (lastPost.getCreateDate() != null) {
                wallLoadMoreWrapper.setNextPage(DateUtils.getUTCDateTimeFormat(lastPost.getCreateDate()));
            }

        }
        Timber.i("Local pagination token: " + wallLoadMoreWrapper.getLocalPaginationToken());
        Timber.i("Server pagination token: " + wallLoadMoreWrapper.getNextPage());
        MediaDb.backupLocalDownloadedMFileMediaPath(getContext(), MediaDb.getMediaList(lifeStreamList));
        checkPage(wallLoadMoreWrapper, true);
        emitter.onNext(wallLoadMoreWrapper);
        emitter.onComplete();
    }

    private List<Adaptive> getAdaptiveList(List<LifeStream> lifeStreamList) {
        return new ArrayList<>(lifeStreamList);
    }

    private void validTheItem(List<Adaptive> adaptiveList, boolean isLoadMore) {
        //Remove null or empty post
        if (adaptiveList != null) {
            for (int size = adaptiveList.size() - 1; size >= 0; size--) {
                if (adaptiveList.get(size) == null) {
                    adaptiveList.remove(size);
                }
            }
        }


        //Save post to local
        List<LifeStream> lifeStreams = new ArrayList<>();
        for (Adaptive adaptive : adaptiveList) {
            if (adaptive instanceof LifeStream) {
                lifeStreams.add((LifeStream) adaptive);
            }
        }
        if (!lifeStreams.isEmpty()) {
            OfflinePostDataHelper.writePosts(getContext(),
                    isLoadMore,
                    lifeStreams,
                    new OfflinePostDataHelper.OfflinePostDataHelperListener() {
                        @Override
                        public void onWriteFinish(Throwable e) {
                            Timber.i("Write post: " + e);
                        }
                    });
        }
    }

    public Observable<LoadMoreWrapper<Adaptive>> getMyWallStreet(String userId) {
        return mSerialApiService.getWallStreetByUserId(userId, getCurrentPage(), getPaginationSize())
                .concatMap(adaptiveResultList -> {
                    if (adaptiveResultList.body() != null) {
                        validTheItem(adaptiveResultList.body().getData(), false);
                        checkPagination(adaptiveResultList.body());
                        return Observable.just(adaptiveResultList.body());
                    }

                    return null;
                });
    }

    private void checkPage(WallLoadMoreWrapper<?> data, boolean isLocalData) {
        if (!isLocalData) {
            setCanLoadMore(!data.getData().isEmpty() &&
                    !TextUtils.isEmpty(data.getNextPage()) &&
                    data.getData().size() >= getPaginationSize());
        } else {
            setCanLoadMore(!TextUtils.isEmpty(data.getLocalPaginationToken()));
        }
        if (isCanLoadMore()) {
            mServerPaginationToken = data.getNextPage();
            mLocalPaginationToken = data.getLocalPaginationToken();
        }
    }

    @Override
    public void resetPagination() {
        super.resetPagination();
        resetPaginationToken();
    }

    private void resetPaginationToken() {
        mServerPaginationToken = "0";
        mLocalPaginationToken = "";
    }

    public Observable<LoadMoreWrapper<Adaptive>> loadMoreMyWallStreet(String userId) {
        if (isCanLoadMore()) {
            return getMyWallStreet(userId);
        }
        return Observable.error(new Throwable(mContext.getString(R.string.error_general_description)));
    }

    public Observable<WallLoadMoreWrapper<Adaptive>> loadMoreData() {
        return getData(true);
    }

    public Observable<List<MoreGame>> getMoreGame() {
        return mMoreGameService.getMoreGame(getContext().getString(R.string.more_game_url));
    }

    public Observable<Response<Object>> deletePost(String id) {
        return mApiService.deleteLifeStream(id);
    }

    public Observable<Response<Object>> deleteProduct(Product product) {
        return mApiService.deleteProduct(product.getId());
    }

    public Observable<Response<Object>> likePost(String postId, String contentId, ContentType contentType, boolean isLike) {
        if (isLike) {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
            return mApiService.likeContent(contentType.getValue(), contentId, new LikeRequestBody(postId));
        } else {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
            return mApiService.unlikeContent(contentId);
        }
    }

    public Observable<Response<Object>> reportProduct(String id, ReportRequest reportRequest) {
        return mApiService.report(ContentType.POST.getValue(), id, reportRequest);
    }

    public Observable<Response<Object>> reportPost(String id, ReportRequest reportRequest) {
        return mApiService.report(ContentType.PRODUCT.getValue(), id, reportRequest);
    }
}
