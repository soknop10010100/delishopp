package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.SoundEffectService;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.PreAPIRequestHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.injection.game.MoreGameAPIQualifier;
import com.proapp.sompom.injection.productserialize.ProductSerializeQualifier;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.emun.Range;
import com.proapp.sompom.model.request.FollowBody;
import com.proapp.sompom.model.request.QueryProduct;
import com.proapp.sompom.model.request.ReportRequest;
import com.proapp.sompom.model.result.Currency;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.model.result.UserBadge;
import com.proapp.sompom.model.result.WallLoadMoreWrapper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseHandleErrorFunc;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.AdsLoaderUtil;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;


/**
 * Created by He Rotha on 10/5/17.
 */

public class ProductListDataManager extends AbsLoadMoreDataManager {
    private final ApiService mMoreGameService;
    private final ApiService mProductService;
    private ApiService mPublicApiService;
    private Locations mLocations;

    public ProductListDataManager(Context context,
                                  @MoreGameAPIQualifier ApiService moreGame,
                                  @ProductSerializeQualifier ApiService product,
                                  ApiService apiService) {
        super(context, apiService);
        mMoreGameService = moreGame;
        mProductService = product;
    }

    public ProductListDataManager(Context context,
                                  @MoreGameAPIQualifier ApiService moreGame,
                                  @ProductSerializeQualifier ApiService product,
                                  ApiService apiService,
                                  ApiService publicApiService) {
        super(context, apiService);
        mMoreGameService = moreGame;
        mProductService = product;
        mPublicApiService = publicApiService;
    }

    public Observable<WallLoadMoreWrapper<Adaptive>> getProductList(final Locations locations) {
        mLocations = locations;
        QueryProduct query = SharedPrefUtils.getQueryProduct(getContext());
        query.initCurrency(getContext());
        query.setLatitude(locations.getLatitude());
        query.setLongitude(locations.getLongitude());
        if (query.getMaxPrice() == 0) {
            query.setMaxPrice(Range.RANGE_10.getValue());
        }
        return mProductService.getProduct(query, getCurrentPage(), getPaginationSize())
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(adaptiveResultList -> {
                    checkPagination(adaptiveResultList.body());
                    return Observable.just(adaptiveResultList.body());
                }).concatMap(adaptiveLoadMoreWrapper -> AdsLoaderUtil.loadStreetWall(mContext, mApiService, adaptiveLoadMoreWrapper))
                .onErrorResumeNext(throwable -> {
                    Timber.e(throwable);
                    WallLoadMoreWrapper<Adaptive> data = new WallLoadMoreWrapper<>();
                    data.setData(Collections.emptyList());
                    setCanLoadMore(false);
                    return Observable.just(data);
                });
    }

    public Observable<Response<Object>> postFollow(String id) {
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
        FollowBody body = new FollowBody(id);
        return mApiService.postFollow(body);
    }

    public Observable<Response<Object>> unFollow(String id) {
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        FollowBody body = new FollowBody(id);
        return mApiService.unFollow(body);
    }

    public Observable<Response<Object>> unFollowShop(String id) {
        FollowBody body = new FollowBody(id);
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        return mApiService.unFollowShop(body);
    }

    public Observable<Response<Object>> reportPost(String id, ReportRequest reportRequest) {
        return mApiService.report(ContentType.PRODUCT.getValue(), id, reportRequest);
    }

    public Observable<WallLoadMoreWrapper<Adaptive>> loadMore() {
        if (!isCanLoadMore()) {
            return Observable.error(new Throwable(mContext.getString(R.string.error_general_description)));
        }
        return getProductList(mLocations);
    }

    public Observable<Response<User>> updateUserLocation(Locations location) {
        if (SharedPrefUtils.isLogin(getContext()) && !location.isEmpty()) {
            User user = new User();
            user.setCountry(location.getCountry());
            user.setLatitude(location.getLatitude());
            user.setLongitude(location.getLongitude());
            return mApiService.updateUser(getMyUserId(),
                    UserHelper.getUpdateUserObject(user),
                    SharedPrefUtils.getLanguage(getContext()));
        }
        return Observable.error(new ErrorThrowable(-1, mContext.getString(R.string.error_no_data)));
    }

    public Observable<List<Currency>> insertOrUpdateCurrency(Locations location) {
        if (!TextUtils.isEmpty(location.getCountry())) {
            return mPublicApiService.getCurrencyByCountryCode(location.getCountry())
                    .concatMap(new ResponseHandleErrorFunc<>(mContext, false))
                    .concatMap(listResponse -> {
                        List<String> currencyString = listResponse.body();
                        List<Currency> currencies = new ArrayList<>();
                        if (currencyString != null && !currencyString.isEmpty()) {
                            for (String s : currencyString) {
                                currencies.add(new Currency(s));
                            }
                        }
                        return Observable.just(currencies);
                    });
        } else {
            return Observable.error(new ErrorThrowable(-1, mContext.getString(R.string.error_no_data)));
        }
    }

    public Observable<Response<Object>> postLikeProduct(String productId, boolean isLike) {
        if (isLike) {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
            return mApiService.likeContent(ContentType.PRODUCT.getValue(), productId, null);
        } else {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
            return mApiService.unlikeContent(productId);
        }
    }

    public Observable<Response<UserBadge>> getUerBadge() {
        return mApiService.getUserBadge();
    }

    public Observable<Response<Object>> getGuestUser() {
        return PreAPIRequestHelper.requestGuestUser(getContext(), mPublicApiService);
    }

    public Observable<Response<Object>> getRequestAppFeatureService() {
        return mApiService.getAppFeature(ApplicationHelper.getRequestAPIMode(mContext),
                ApplicationHelper.getUserId(mContext)).concatMap(listResponse -> {
            if (listResponse.isSuccessful() && listResponse.body() != null) {
                SharedPrefUtils.setAppFeature(getContext(), listResponse.body());
            }
            return Observable.just(Response.success(new Object()));
        });
    }
}
