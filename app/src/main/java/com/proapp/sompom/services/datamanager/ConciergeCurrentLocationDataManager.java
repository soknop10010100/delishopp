package com.proapp.sompom.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.licence.utils.FileUtils;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.model.searchaddress.google.AutoCompleteResponse;
import com.proapp.sompom.model.searchaddress.google.Prediction;
import com.proapp.sompom.model.searchaddress.mapbox.ContextItem;
import com.proapp.sompom.model.searchaddress.mapbox.Feature;
import com.proapp.sompom.services.ApiGoogle;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.LocationStateUtil;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Or Vitovongsak on 16/11/21.
 */
public class ConciergeCurrentLocationDataManager extends AbsLoadMoreDataManager {

    private static final String COUNTRY = "country";
    private static final String LOCALITY = "locality";
    private ApiGoogle mApiGoogle;
    private String mGoogleApiKey;
    private String mMapBoxToken;
    private SelectAddressDataManager.Strategy mStrategy;

    public ConciergeCurrentLocationDataManager(Context context,
                                               ApiService apiService,
                                               ApiGoogle apiGoogle,
                                               SelectAddressDataManager.Strategy strategy) {
        super(context, apiService);
        mApiGoogle = apiGoogle;
        mStrategy = strategy;
    }

    public Observable<List<ConciergeUserAddress>> getUserAddresses(boolean isRefresh, boolean isLoadMore) {
        if (!isLoadMore) {
            resetPagination();
        }

        return mApiService.getConciergeUserAddress(Integer.parseInt(getCurrentPage()),
                        getPaginationSize(),
                        ApplicationHelper.getRequestAPIMode(mContext))
                .concatMap(loadMoreWrapperResponse -> {
                    if (loadMoreWrapperResponse.isSuccessful()) {
                        LoadMoreWrapper<ConciergeUserAddress> wrapper = loadMoreWrapperResponse.body();
                        checkPagination(wrapper);
                        return Observable.just(wrapper.getData());
                    } else {
                        return Observable.just(new ArrayList<>());
                    }
                });
    }

    private LoadMoreWrapper<ConciergeUserAddress> loadMockupData() {
        String data = FileUtils.readRawTextFile(mContext, R.raw.user_address);
        List<ConciergeUserAddress> orderList = new Gson().fromJson(data, new TypeToken<List<ConciergeUserAddress>>() {
        }.getType());

        LoadMoreWrapper<ConciergeUserAddress> wrapper = new LoadMoreWrapper<>();
        wrapper.setData(orderList);

        return wrapper;
    }

    public Observable<List<SearchAddressResult>> searchAddress(String query) {
        if (mStrategy == SelectAddressDataManager.Strategy.Google) {
            return mApiGoogle.searchGoogleAddress(getGoogleApiKey(), query)
                    .concatMap(autoCompleteResponseResponse -> {
                        List<SearchAddressResult> searchAddressResults = new ArrayList<>();
                        AutoCompleteResponse autoCompleteResponse = autoCompleteResponseResponse.body();
                        if (autoCompleteResponse != null &&
                                autoCompleteResponse.getPredictions() != null) {
                            for (Prediction prediction : autoCompleteResponse.getPredictions()) {
                                SearchAddressResult searchAddressResult = new SearchAddressResult();
                                searchAddressResult.setId(prediction.getPlaceId());
                                if (prediction.getTerms() != null && !prediction.getTerms().isEmpty()) {
                                    //Place title is at the first index.
                                    searchAddressResult.setPlaceTitle(prediction.getTerms().get(0).getValue());
                                }
                                searchAddressResult.setAddress(LocationStateUtil.getOnlyAddressFromFullPlace(prediction.getDescription()));
                                searchAddressResult.setFullAddress(prediction.getDescription());
                                searchAddressResults.add(searchAddressResult);
                            }
                        }
                        return Observable.just(searchAddressResults);
                    });
        } else {
            return mApiGoogle.searchMapBoxAddress(query, getMapBoxToken())
                    .concatMap(mapBoxDataResponse -> {
                        List<SearchAddressResult> searchAddressResults = new ArrayList<>();
                        if (mapBoxDataResponse.body() != null && mapBoxDataResponse.body().getFeatures() != null) {
                            for (Feature feature : mapBoxDataResponse.body().getFeatures()) {
                                searchAddressResults.add(parseMapBoxPlaceData(feature));
                            }
                        }
                        return Observable.just(searchAddressResults);
                    });
        }
    }

    private SearchAddressResult parseMapBoxPlaceData(Feature feature) {
        SearchAddressResult searchAddressResult = new SearchAddressResult();
        searchAddressResult.setId(feature.getId());
        searchAddressResult.setPlaceTitle(feature.getText());
        searchAddressResult.setAddress(LocationStateUtil.getOnlyAddressFromFullPlace(feature.getPlaceName()));
        searchAddressResult.setFullAddress(feature.getPlaceName());

        //Since the MapBox response does not response specific city and country of a result place,
        //so we have to parse the data from response context property
        String city = null;
        String country = null;
        if (feature.getContextItemList() != null) {
            for (ContextItem contextItem : feature.getContextItemList()) {
                if (contextItem.isRegionOrCity()) {
                    city = contextItem.getText();
                }
                if (contextItem.isCountry()) {
                    country = contextItem.getText();
                }
            }
        }
        searchAddressResult.setCity(city);
        searchAddressResult.setCountry(country);

        //Parse location
        Locations locations = new Locations();
        locations.setLatitude(feature.getCenter().get(1));
        locations.setLongitude(feature.getCenter().get(0));
        searchAddressResult.setLocations(locations);

        return searchAddressResult;
    }

    private String getGoogleApiKey() {
        if (TextUtils.isEmpty(mGoogleApiKey)) {
            mGoogleApiKey = mContext.getString(R.string.google_place_key);
        }
        return mGoogleApiKey;
    }

    private String getMapBoxToken() {
        if (TextUtils.isEmpty(mMapBoxToken)) {
            mMapBoxToken = mContext.getString(R.string.mapbox_token);
        }
        return mMapBoxToken;
    }
}
