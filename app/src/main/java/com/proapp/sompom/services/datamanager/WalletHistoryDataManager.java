package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.R;
import com.proapp.sompom.model.concierge.WalletHistory;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Chhom Veasna on 8/23/22.
 */

public class WalletHistoryDataManager extends AbsLoadMoreDataManager {

    public WalletHistoryDataManager(Context context, ApiService apiService) {
        super(context, apiService);
        setPaginationSize(20);
    }

    public Observable<List<WalletHistory>> getWalletHistoryListService(boolean isLoadMore) {
        if (!isLoadMore) {
            resetPagination();
        }

        return mApiService.getWalletHistory(getCurrentPage(), getPaginationSize()).concatMap(loadMoreWrapperResponse -> {
            if (loadMoreWrapperResponse.isSuccessful()) {
                checkPagination(loadMoreWrapperResponse.body());
                return Observable.just(loadMoreWrapperResponse.body().getData());
            } else {
                return Observable.error(new ErrorThrowable(loadMoreWrapperResponse.code(),
                        mContext.getString(R.string.error_general_description)));
            }
        });
    }
}
