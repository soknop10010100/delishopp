package com.proapp.sompom.services.datamanager;

import android.content.Context;

import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.notification.RedirectionNotificationData;
import com.proapp.sompom.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Veasna Chhom on 5/25/21.
 */

public class TimelinePreviewDataManager extends TimelineDetailDataManager {

    private RedirectionNotificationData mRedirectionNotificationData;

    public TimelinePreviewDataManager(Context context, ApiService apiService, RedirectionNotificationData data) {
        super(context, apiService, data);
        mRedirectionNotificationData = data;
    }

    public String getPostId() {
        if (mRedirectionNotificationData != null) {
            return mRedirectionNotificationData.getPostId();
        }

        return null;
    }

    public Observable<Response<LifeStream>> getWallStreetDetail() {
        return super.getWallStreetDetail(getPostId());
    }
}
