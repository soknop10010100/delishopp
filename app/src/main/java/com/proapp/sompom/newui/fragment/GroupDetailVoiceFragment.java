package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.GroupVoiceAdapter;
import com.proapp.sompom.databinding.FragmentGroupDetailMediaBinding;
import com.proapp.sompom.decorataor.GroupMediaSectionItemDecorator;
import com.proapp.sompom.model.GroupMediaSection;
import com.proapp.sompom.model.emun.GroupMediaDetailType;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.GroupDetailMediaDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.AbsGroupDetailMediaViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.GroupDetailVoiceViewModel;
import com.proapp.sompom.widget.LoaderMoreLayoutManager;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Chhom Veasna on 7/24/2020.
 */
public class GroupDetailVoiceFragment extends AbsGroupDetailMediaFragment<FragmentGroupDetailMediaBinding> implements
        AbsGroupDetailMediaViewModel.GroupDetailMediaViewModelListener<Chat>,
        LoaderMoreLayoutManager.OnLoadMoreCallback {

    @Inject
    public ApiService mApiService;
    private GroupDetailVoiceViewModel mViewModel;
    private LoaderMoreLayoutManager mLoaderMoreLayoutManager;
    private GroupVoiceAdapter mAdapter;

    public static GroupDetailVoiceFragment newInstance(String groupId, GroupMediaDetailType type) {

        Bundle args = new Bundle();
        args.putString(GROUP_ID, groupId);
        args.putString(TYPE, type.getValue());

        GroupDetailVoiceFragment fragment = new GroupDetailVoiceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        if (getArguments() != null) {
            mViewModel = new GroupDetailVoiceViewModel(requireContext(),
                    new GroupDetailMediaDataManager(requireContext(),
                            mApiService,
                            getGroupId(),
                            GroupMediaDetailType.fromValue(getArguments().getString(TYPE))),
                    this);
            setVariable(BR.viewModel, mViewModel);
        }
    }

    @Override
    protected void onReceivedFirstNavigation() {
        new Handler().postDelayed(() -> mViewModel.getMediaDetail(false), 100);
    }

    @Override
    public void onLoadDataSuccess(List<GroupMediaSection<Chat>> data,
                                  boolean canLoadMore,
                                  boolean isLoadMoreResult,
                                  GroupMediaDetailType type) {
        if (mAdapter == null) {
            mLoaderMoreLayoutManager = new LoaderMoreLayoutManager(requireContext());
            mAdapter = new GroupVoiceAdapter(requireActivity(), data);
            mAdapter.setCanLoadMore(canLoadMore);
            if (getBinding().recyclerView.getItemDecorationCount() <= 0) {
                getBinding().recyclerView.addItemDecoration(new GroupMediaSectionItemDecorator(getResources()
                        .getDimensionPixelSize(R.dimen.space_large),
                        getResources().getDimensionPixelSize(R.dimen.space_xlarge)));
            }
            getBinding().recyclerView.setLayoutManager(mLoaderMoreLayoutManager);
            getBinding().recyclerView.setAdapter(mAdapter);
            if (canLoadMore && !data.isEmpty()) {
                mLoaderMoreLayoutManager.setOnLoadMoreListener(this);
            }
        } else {
            mAdapter.setCanLoadMore(canLoadMore);
            if (!canLoadMore || data.isEmpty()) {
                mLoaderMoreLayoutManager.setOnLoadMoreListener(null);
            } else {
                mLoaderMoreLayoutManager.setOnLoadMoreListener(this);
            }
            if (isLoadMoreResult) {
                mLoaderMoreLayoutManager.loadingFinished();
                mAdapter.addLoadMoreData(data);
            } else {
                //Refresh or reset
                mAdapter.setDatas(data);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onLoadFailed(ErrorThrowable ex, boolean isLoadMore, GroupMediaDetailType type) {
        if (isLoadMore) {
            mAdapter.setCanLoadMore(false);
            mLoaderMoreLayoutManager.loadingFinished();
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoadMoreFromBottom() {
        mViewModel.getMediaDetail(true);
    }
}
