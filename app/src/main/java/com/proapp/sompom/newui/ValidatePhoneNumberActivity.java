package com.proapp.sompom.newui;

import android.os.Bundle;

import com.proapp.sompom.intent.ValidatePhoneNumberIntent;
import com.proapp.sompom.newui.fragment.ValidatePhoneNumberFragment;

import androidx.annotation.Nullable;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class ValidatePhoneNumberActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ValidatePhoneNumberIntent validatePhoneNumberIntent = new ValidatePhoneNumberIntent(getIntent());
        setFragment(ValidatePhoneNumberFragment.newInstance(validatePhoneNumberIntent.getAuthType(),
                validatePhoneNumberIntent.isIgnoreFirebaseTokenValidation()),
                ValidatePhoneNumberFragment.class.getSimpleName());
    }
}
