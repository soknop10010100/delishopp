package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.NotificationAdapter;
import com.proapp.sompom.databinding.FragmentHomeNotificationBinding;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.injection.notificationserialize.NotificationSerializeQualifier;
import com.proapp.sompom.intent.ConciergeOrderHistoryDetailIntent;
import com.proapp.sompom.intent.newintent.ConciergeOrderDeliveryTrackingIntent;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.model.notification.ConciergeNotification;
import com.proapp.sompom.model.notification.NotificationAdaptive;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.NotificationDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.HomeNotificationFragmentViewModel;
import com.proapp.sompom.widget.LoadMoreLinearLayoutManager;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by nuonveyo on 7/4/18.
 */

public class NotificationFragment extends AbsBindingFragment<FragmentHomeNotificationBinding> {
    @NotificationSerializeQualifier
    @Inject
    public ApiService mApiService;
    private HomeNotificationFragmentViewModel mViewModel;
    private LoadMoreLinearLayoutManager mLayoutManager;
    private NotificationAdapter mNotificationAdapter;
    private LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener mOnLoadMoreCallback;

    public static NotificationFragment newInstance() {
        return new NotificationFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_home_notification;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_NOTIFICATION_LIST;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        UserHelper.resetNotificationBadge(requireContext());
        getControllerComponent().inject(this);
        getBinding().layoutToolbar.toolbarTitle.setText(getString(R.string.notification_title));
        setToolbar(getBinding().layoutToolbar.toolbar, true);
        initViewModel();
    }

    private void initViewModel() {
        NotificationDataManager dataManager = new NotificationDataManager(getActivity(), mApiService);
        mViewModel = new HomeNotificationFragmentViewModel(dataManager,
                this::iniRecyclerView);
        setVariable(BR.viewModel, mViewModel);
        mViewModel.getData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            requireActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void iniRecyclerView(List<NotificationAdaptive> baseNotificationModels, boolean canLoadMore) {
        mLayoutManager = new LoadMoreLinearLayoutManager(getActivity(), getBinding().recyclerView, false);
        mOnLoadMoreCallback = new LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener() {
            @Override
            public void onReachedLoadMoreBottom() {
                performLoadMore();
            }

            @Override
            public void onReachedLoadMoreByDefault() {
                performLoadMore();
            }
        };

        mLayoutManager.setLoadMoreLinearLayoutManagerListener(canLoadMore ? mOnLoadMoreCallback : null);
        getBinding().recyclerView.setLayoutManager(mLayoutManager);

        mNotificationAdapter = new NotificationAdapter(baseNotificationModels, (data, position) -> {
            if (data instanceof ConciergeNotification && ((ConciergeNotification) data).getOrder() != null) {
                ConciergeNotification notification = (ConciergeNotification) data;
                if (ConciergeHelper.checkShouldShowReOrderButton(notification.getOrder().getOrderStatus())) {
                    ConciergeOrderHistoryDetailIntent intent = new ConciergeOrderHistoryDetailIntent(requireContext(),
                            notification.getOrder().getId(),
                            notification.getOrder().getOrderNumber());
                    startActivity(intent);
                } else {
                    ConciergeOrderDeliveryTrackingIntent intent = new ConciergeOrderDeliveryTrackingIntent(requireContext(),
                            notification.getOrder().getId());
                    startActivity(intent);
                }
            }
        });
        mNotificationAdapter.setCanLoadMore(canLoadMore);
        getBinding().recyclerView.setAdapter(mNotificationAdapter);
        if (canLoadMore) {
            getBinding().recyclerView.postDelayed(() -> mLayoutManager.checkLToInvokeReachLoadMoreByDefaultIfNecessary(),
                    100);
        }
    }

    private void performLoadMore() {
        Timber.i("Load more");
        NotificationFragment.this.getBinding().recyclerView.stopScroll();
        mViewModel.loadMore(new OnCallbackListListener<List<NotificationAdaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mNotificationAdapter.setCanLoadMore(false);
                mNotificationAdapter.notifyDataSetChanged();
                mLayoutManager.loadingFinished();
                mLayoutManager.setLoadMoreLinearLayoutManagerListener(null);
            }

            @Override
            public void onComplete(List<NotificationAdaptive> result, boolean canLoadMore1) {
                mNotificationAdapter.addLoadMoreData(result);
                mNotificationAdapter.setCanLoadMore(canLoadMore1);
                mLayoutManager.loadingFinished();
                mLayoutManager.setLoadMoreLinearLayoutManagerListener(canLoadMore1 ? mOnLoadMoreCallback : null);
            }
        });
    }
}
