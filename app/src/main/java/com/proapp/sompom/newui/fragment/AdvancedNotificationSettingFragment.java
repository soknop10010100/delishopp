package com.proapp.sompom.newui.fragment;


import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.StoreDataManager;
import com.proapp.sompom.viewmodel.AdvanceNotificationFragmentViewModel;
import com.proapp.sompom.databinding.FragmentAdvancedNotificationSettingBinding;

import javax.inject.Inject;


public class AdvancedNotificationSettingFragment extends AbsBindingFragment<FragmentAdvancedNotificationSettingBinding> {
    public static final String TAG = AdvancedNotificationSettingFragment.class.getName();
    @Inject
    public ApiService mApiService;

    public static AdvancedNotificationSettingFragment newInstance() {
        return new AdvancedNotificationSettingFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_advanced_notification_setting;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        setToolbar(getBinding().toolbarInclude.toolbar, true);
        getBinding().toolbarInclude.toolbarTitle.setText(R.string.advance_notification_setting_title);

        StoreDataManager manager = new StoreDataManager(getActivity(), mApiService);
        AdvanceNotificationFragmentViewModel viewModel = new AdvanceNotificationFragmentViewModel(manager);
        setVariable(BR.viewModel, viewModel);
    }

}
