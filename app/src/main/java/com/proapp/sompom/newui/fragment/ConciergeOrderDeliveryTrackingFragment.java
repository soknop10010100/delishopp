package com.proapp.sompom.newui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ConciergeDeliveryTrackingAdapter;
import com.proapp.sompom.databinding.FragmentConciergeOrderDeliveryTrackingBinding;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.intent.newintent.ConciergeOrderDeliveryTrackingIntent;
import com.proapp.sompom.model.emun.ConciergeOrderStatus;
import com.proapp.sompom.model.result.ConciergeDriverLocationResponse;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.model.result.ConciergeOrderTrackingProgress;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeOrderDeliveryTrackingDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeOrderDeliveryTrackingFragmentViewModel;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 16/12/21.
 */
public class ConciergeOrderDeliveryTrackingFragment
        extends AbsMapFragment<FragmentConciergeOrderDeliveryTrackingBinding>
        implements ConciergeOrderDeliveryTrackingFragmentViewModel.ConciergeDeliveryTrackingListener {

    public static final String REFRESH_LIST_EVENT = "REFRESH_LIST_EVENT";

    @Inject
    public ApiService mApiService;
    private ConciergeOrderDeliveryTrackingFragmentViewModel mViewModel;
    private ConciergeDeliveryTrackingAdapter mAdapter;
    private BroadcastReceiver mOrderStatusUpdateReceiver;

    public static ConciergeOrderDeliveryTrackingFragment newInstance(String orderId, ConciergeOrder order) {
        Bundle args = new Bundle();
        args.putParcelable(ConciergeOrderDeliveryTrackingIntent.ORDER, order);
        args.putString(ConciergeOrderDeliveryTrackingIntent.ORDER_ID, orderId);

        ConciergeOrderDeliveryTrackingFragment fragment = new ConciergeOrderDeliveryTrackingFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_order_delivery_tracking;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerOrderStatusUpdateReceiver();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        super.onMapReady(googleMap);
        //Disable marker click event
        googleMap.setOnMarkerClickListener(marker -> true);
    }

    @Override
    protected void onViewAndMapCreated() {
        getControllerComponent().inject(this);
        getBinding().layoutToolbar.toolbarTitle.setText(getString(R.string.shop_tracking_order_screen_title));
        setToolbar(getBinding().layoutToolbar.toolbar, true);
        initViewModel();
    }

    private void registerOrderStatusUpdateReceiver() {
        mOrderStatusUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ConciergeOrder updatedOrder = intent.getParcelableExtra(ConciergeHelper.UPDATE_ORDER_STATUS_EVENT_DATA);
                if (updatedOrder != null && TextUtils.equals(updatedOrder.getId(), mViewModel.getOrder().getId())) {
                    if (mViewModel.getOrder().getOrderStatus() != updatedOrder.getOrderStatus()) {
                        Timber.i("onReceive orderStatusUpdateEvent of " + updatedOrder.getOrderStatus());
                        mViewModel.checkToUpdateOrderStatus(updatedOrder.getOrderStatus());
                        mAdapter.updateConciergeOrderTrackingProgress(updatedOrder.getOrderStatus());
                    }
                }
            }
        };
        requireContext().registerReceiver(mOrderStatusUpdateReceiver,
                new IntentFilter(ConciergeHelper.UPDATE_ORDER_STATUS_EVENT));
    }

    public void setupBottomSheet() {
        BottomSheetBehavior<FrameLayout> bottomSheetBehavior = BottomSheetBehavior.from(getBinding().bottomSheetView);

        // Programmatically expand the bottom sheet to fully extended state
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        float screenHeight = getBinding().getRoot().getHeight();

        // Move camera up by about 30% of screen height, enough so bottom sheet won't expand over
        // the marker
        getGoogleMap().moveCamera(CameraUpdateFactory.scrollBy(0, (float) (screenHeight * 0.3)));

        // Set the bottom sheet height to be 65%% of parent
        float bottomSheetHeight = Math.round(screenHeight * 0.65);
        getBinding().bottomSheetContent.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, (int) bottomSheetHeight));

        getBinding().bottomSheetView.setOnClickListener(v -> {
            // Expand bottom sheet if it's at collapsed state, meaning at bottom of screen, and user
            // tap on the bottom sheet view
            if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
    }

    public void initViewModel() {
        ConciergeOrder order = getArguments().getParcelable(ConciergeOrderDeliveryTrackingIntent.ORDER);
//        Timber.i("order: " + new Gson().toJson(order));
        mViewModel = new ConciergeOrderDeliveryTrackingFragmentViewModel(requireContext(),
                getGoogleMap(),
                new ConciergeOrderDeliveryTrackingDataManager(requireContext(),
                        mApiService,
                        getArguments().getString(ConciergeOrderDeliveryTrackingIntent.ORDER_ID),
                        order),
                this);
        setVariable(BR.viewModel, mViewModel);
        mViewModel.requestNecessaryData();
    }

    public void initAdapter(List<ConciergeOrderTrackingProgress> progressList,
                            ConciergeOrderStatus currentStatus) {
        getBinding().statusRecyclerView.setLayoutManager(new LinearLayoutManager(mViewModel.getContext()));
        mAdapter = new ConciergeDeliveryTrackingAdapter(requireContext(),
                progressList,
                currentStatus,
                new ConciergeDeliveryTrackingAdapter.ConciergeDeliveryTrackingAdapterListener() {
                    @Override
                    public void onCancelOrderClicked() {
                        mViewModel.onCancelOrderClicked();
                    }

                    @Override
                    public void onOrderTrackingStatusUpdated(int position, ConciergeOrderStatus orderStatus, boolean needToBroadcastUpdateEvent) {
                        if (needToBroadcastUpdateEvent) {
                            mViewModel.updateOrderStatus(orderStatus);
                            Intent intent = new Intent(REFRESH_LIST_EVENT);
                            SendBroadCastHelper.verifyAndSendBroadCast(requireContext(), intent);
                        }
                        getBinding().statusRecyclerView.smoothScrollToPosition(position);
                    }
                });
        getBinding().statusRecyclerView.setAdapter(mAdapter);
        int currentOrderStatus = mAdapter.findCurrentOrderStatus();
        if (currentOrderStatus >= 0) {
            getBinding().statusRecyclerView.post(() -> getBinding().statusRecyclerView.smoothScrollToPosition(currentOrderStatus));
        }
    }

    @Override
    public void onDataSuccess(List<ConciergeOrderTrackingProgress> progressList, ConciergeOrderStatus currentStatus) {
        initAdapter(progressList, currentStatus);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mViewModel.checkToClearRequestDriverLocationTimer();
        requireContext().unregisterReceiver(mOrderStatusUpdateReceiver);
    }

    @Override
    public void onShouldSetupBottomSheet() {
        setupBottomSheet();
    }

    @Override
    public void onRequestDriverLocationSuccess(ConciergeDriverLocationResponse response) {
        if (mAdapter != null && response.getCurrentOrderStatus() != null) {
            mAdapter.updateConciergeOrderTrackingProgress(response.getCurrentOrderStatus());
        }
    }
}
