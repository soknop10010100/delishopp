package com.proapp.sompom.newui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.proapp.sompom.BR;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentLogInBinding;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.model.result.AuthResponse;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.LoginDataManager;
import com.proapp.sompom.utils.SnackBarUtil;
import com.proapp.sompom.viewmodel.AbsLoginViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.LoginViewModel;

import javax.inject.Inject;

import androidx.annotation.Nullable;

/**
 * Created by He Rotha on 6/4/18.
 */
public class LoginFragment extends AbsLoginFragment<FragmentLogInBinding, LoginViewModel> implements
        AbsLoginViewModel.AbsLoginViewModelListener {

    @Inject
    @PublicQualifier
    public ApiService mPublicApiService;
    @Inject
    public ApiService mPrivateApiService;

    public static LoginFragment newInstance() {

        Bundle args = new Bundle();

        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected boolean forceRebuildControllerComponent() {
        return true;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_log_in;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        LoginDataManager loginDataManager = new LoginDataManager(getActivity(), mPublicApiService, mPrivateApiService);
        mViewModel = new LoginViewModel(loginDataManager, this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onLoginError(String message) {
        mViewModel.hideLoading();
        SnackBarUtil.showSnackBar(getBinding().getRoot(), message, true);
    }

    @Override
    public void onLoginSuccess() {
        handleOnLoginSuccess();
    }

    @Override
    public void onChangePasswordRequire(AuthResponse authResponse) {
        handleChangePasswordRequire(authResponse);
    }

    @Override
    public void rebuildApplicationComponent() {
        ((MainApplication) requireActivity().getApplication()).rebuildApplicationComponents();
        getControllerComponent().inject(this);
        startActivity(Intent.makeRestartActivityTask(requireActivity().getIntent().getComponent()));
    }
}
