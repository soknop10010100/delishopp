package com.proapp.sompom.newui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.ConciergeShopProductAdapter;
import com.proapp.sompom.databinding.FragmentConciergeShopDetailBinding;
import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.ScrimColorAppbarListener;
import com.proapp.sompom.intent.ConciergeShopDetailIntent;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;
import com.proapp.sompom.model.concierge.ConciergeMenu;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeMenuSection;
import com.proapp.sompom.model.concierge.ConciergeShop;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeShopDetailDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeShopDetailFragmentViewModel;
import com.proapp.sompom.widget.tablayout.CustomTabIndicatorLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 9/9/21.
 */

public class ConciergeShopDetailFragment extends AbsSupportShopCheckOutFragment<FragmentConciergeShopDetailBinding> implements
        ConciergeShopDetailFragmentViewModel.ConciergeShopDetailFragmentViewModelListener,
        ConciergeShopProductAdapter.ShopProductAdapterCallback {

    private ConciergeShopDetailFragmentViewModel mViewModel;
    @Inject
    public ApiService mApiService;
    private ConciergeShopProductAdapter mAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private final List<String> tabItems = new ArrayList<>();
    private BroadcastReceiver mShopStatusUpdatedReceiver;

    public static ConciergeShopDetailFragment newInstance(String id,
                                                          boolean isDiscountOnly) {
        Bundle args = new Bundle();
        args.putString(ConciergeShopDetailIntent.ID, id);
        args.putBoolean(ConciergeShopDetailIntent.IS_DISCOUNT_ONLY, isDiscountOnly);

        ConciergeShopDetailFragment fragment = new ConciergeShopDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_shop_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        initShopStatusUpdatedEvent();
        String storeId = null;
        boolean isDiscountOnly = false;
        if (getArguments() != null) {
            storeId = getArguments().getString(ConciergeShopDetailIntent.ID);
            isDiscountOnly = getArguments().getBoolean(ConciergeShopDetailIntent.IS_DISCOUNT_ONLY, false);
        }
        mViewModel = new ConciergeShopDetailFragmentViewModel(requireContext(),
                new ConciergeShopDetailDataManager(requireContext(),
                        mApiService,
                        storeId,
                        isDiscountOnly),
                getCheckOutViewModelInstance(),
                this);
        mViewModel.getStoreDetail();
        setVariable(BR.viewModel, mViewModel);
        getBinding().appBar.addOnOffsetChangedListener(new ScrimColorAppbarListener(getBinding().collapse) {
            @Override
            public void onStateChange(State state) {
                boolean shouldFadeIn = state == State.Collapse;
                int duration = shouldFadeIn ?
                        getResources().getInteger(R.integer.shop_detail_app_bar_fade_duration) * 3 :
                        getResources().getInteger(R.integer.shop_detail_app_bar_fade_duration);
                int startDelay = shouldFadeIn ?
                        getResources().getInteger(R.integer.shop_detail_app_bar_fade_duration) : 0;
                AnimationHelper.fadeInOrOut(getBinding().title,
                        shouldFadeIn,
                        duration,
                        startDelay,
                        shouldFadeIn ? new AccelerateInterpolator() : null,
                        null);
                getBinding().categoryTabContainer.setVisibility(state == State.Collapse ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    protected void onRefreshItemCountInList() {
        if (mAdapter != null) {
            mAdapter.refreshProductInCardCounter();
        }
    }

    private void initShopStatusUpdatedEvent() {
        mShopStatusUpdatedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive shopStatusUpdatedEvent");
                if (mViewModel != null) {
                    mViewModel.getCartViewModel().updateCheckoutButtonStatus(intent.getBooleanExtra(ConciergeHelper.FIELD_IS_SHOP_CLOSED,
                            false));
                }
            }
        };
        requireContext().registerReceiver(mShopStatusUpdatedReceiver,
                new IntentFilter(ConciergeHelper.SHOP_STATUS_UPDATED_EVENT));
    }

    @Override
    protected void onReceivedRefreshProductOrderCounterEvent(List<ConciergeMenuItem> items) {
        if (items != null && !items.isEmpty()) {
            reloadBottomSheetData();
            if (mAdapter != null) {
                for (ConciergeMenuItem item : items) {
                    mAdapter.updateProductSelectionCounter(item);
                }
            }
        }
    }

    @Override
    protected ApiService getPrivateAPIService() {
        return mApiService;
    }

    @Override
    protected void showLoading() {
        mViewModel.showLoading(true);
    }

    @Override
    protected void hideLoading() {
        mViewModel.hideLoading();
    }

    @Override
    public void onLoadStoreDetailSuccess(ConciergeShop shopDetail, boolean isShopClosed) {
        mViewModel.getCartViewModel().updateCheckoutButtonStatus(isShopClosed);
//        Timber.i("onLoadStoreDetailSuccess: " + new Gson().toJson(store));
        if (shopDetail.getConciergeMenus() != null && !shopDetail.getConciergeMenus().isEmpty()) {
            addProductCategoryTab(shopDetail);

            Timber.i("Shop detail loaded: " + new Gson().toJson(shopDetail));

            mAdapter = new ConciergeShopProductAdapter(requireContext(),
                    shopDetail,
                    getShopItemAdaptive(shopDetail),
                    true,
                    this);
            mLinearLayoutManager = new LinearLayoutManager(requireContext());
            getBinding().recyclerView.setLayoutManager(mLinearLayoutManager);
            getBinding().recyclerView.setAdapter(mAdapter);
            getBinding().recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    final int topItemIndex = mLinearLayoutManager.findFirstVisibleItemPosition();
                    final int secondItemIndex = topItemIndex + 1;
                    if (secondItemIndex < mAdapter.getDatas().size()) {
                        final ConciergeShopDetailDisplayAdaptive firstAdaptive = mAdapter.getDatas().get(topItemIndex);
                        final ConciergeShopDetailDisplayAdaptive secondAdaptive = mAdapter.getDatas().get(secondItemIndex);

                        if (firstAdaptive.isProductCategory()) {
                            for (int index = 0; index < tabItems.size(); index++) {
                                final String tabId = tabItems.get(index);
                                if (tabId.equals(firstAdaptive.getId()) && getBinding().tabs.getSelectedTabPosition() != index) {
                                    final TabLayout.Tab tab = getBinding().tabs.getTabAt(index);
                                    getBinding().tabs.scrollSelectTab(tab, true);
                                    break;
                                }
                            }
                        } else if (!firstAdaptive.isProductCategory() && secondAdaptive.isProductCategory()) {
                            for (int index = 0; index < tabItems.size(); index++) {
                                final String tabId = tabItems.get(index);

                                if (tabId.equals(secondAdaptive.getId())) {
                                    final TabLayout.Tab tab = getBinding().tabs.getTabAt(index == 0 ? 0 : index - 1);
                                    getBinding().tabs.scrollSelectTab(tab, true);
                                    break;
                                }
                            }
                        }
                    }
                }
            });
        }

        notifyShouldShowCartBottomSheet();
    }

    private void addProductCategoryTab(ConciergeShop shop) {
        ConciergeMenu menu = shop.getConciergeMenus().get(0);
        if (menu != null) {
            getBinding().tabs.addOnTabSelectedListener(new CustomTabIndicatorLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab, boolean isScrollSelected) {
                    Timber.i("onTabSelected: Tag: " + tab.getTag() + ", Position: " + tab.getPosition());
                    if (!isScrollSelected) {
                        if (mAdapter != null) {
                            Object tag = tab.getTag();
                            if (tag != null) {
                                int itemPositionById = mAdapter.findItemPositionById(tab.getTag().toString());
                                if (itemPositionById >= 0) {
                                    mLinearLayoutManager.scrollToPositionWithOffset(itemPositionById, 0);
                                }
                            }
                        }
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
            tabItems.clear();
            if (menu.getConciergeMenuSections() != null && !menu.getConciergeMenuSections().isEmpty()) {
                for (ConciergeMenuSection section : menu.getConciergeMenuSections()) {
                    TabLayout.Tab tab = getBinding().tabs.newTab();
                    tab.setText(section.getName());
                    tab.setTag(section.getId());
                    tabItems.add(section.getId());
                    getBinding().tabs.addTab(tab);
                }
            }
        }
    }

    private List<ConciergeShopDetailDisplayAdaptive> getShopItemAdaptive(ConciergeShop shop) {
        List<ConciergeShopDetailDisplayAdaptive> newList = new ArrayList<>();
        ConciergeMenu menu = shop.getConciergeMenus().get(0);
        if (menu != null) {
            if (menu.getConciergeMenuSections() != null && !menu.getConciergeMenuSections().isEmpty()) {
                for (ConciergeMenuSection section : menu.getConciergeMenuSections()) {
                    newList.add(section);
                    if (section != null && !section.getConciergeMenuItems().isEmpty()) {
                        newList.addAll(section.getConciergeMenuItems());
                    }
                }
            }
        }

        return newList;
    }

    @Override
    public void onProductAddedFromDetail() {
        notifyShouldShowCartBottomSheet();
    }

    @Override
    protected void onCheckOutViewVisibilityChanged(boolean isVisible) {
        int paddingBottom = isVisible ? getResources().getDimensionPixelSize(R.dimen.concierge_checkout_bottom_sheet_height) : 0;
        getBinding().recyclerView.setPadding(0, 0, 0, paddingBottom);
    }

    @Override
    public void onItemAddedToCart(ConciergeMenuItem conciergeMenuItem) {
        if (mAdapter != null) {
            mAdapter.onItemUpdatedInCart(conciergeMenuItem);
        }
    }

    @Override
    protected void onItemRemovedFromCart(ConciergeMenuItem conciergeMenuItem) {
        if (mAdapter != null) {
            mAdapter.onItemUpdatedInCart(conciergeMenuItem);
        }
    }

    @Override
    protected void onCartItemCleared() {
        if (mAdapter != null) {
            mAdapter.onCartCleared();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requireContext().unregisterReceiver(mShopStatusUpdatedReceiver);
    }
}
