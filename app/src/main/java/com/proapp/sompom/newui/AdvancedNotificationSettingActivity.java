package com.proapp.sompom.newui;

import android.os.Bundle;

import com.proapp.sompom.newui.fragment.AdvancedNotificationSettingFragment;

public class AdvancedNotificationSettingActivity extends AbsDefaultActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(AdvancedNotificationSettingFragment.newInstance(), AdvancedNotificationSettingFragment.TAG);
    }

}
