package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.intent.InputEmailSignUpDataIntent;
import com.proapp.sompom.newui.fragment.InputEmailSignUpDataFragment;

/**
 * Created by Veasna Chhom on 2/10/22.
 */
public class InputEmailSignUpDataActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(InputEmailSignUpDataFragment.newInstance(new InputEmailSignUpDataIntent(getIntent()).getEmail()),
                InputEmailSignUpDataFragment.class.getSimpleName());
    }
}
