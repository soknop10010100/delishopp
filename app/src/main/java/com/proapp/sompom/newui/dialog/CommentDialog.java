package com.proapp.sompom.newui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.DialogCommentBinding;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.listener.OnCommentDialogClickListener;
import com.proapp.sompom.listener.OnCompleteListener;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.newui.fragment.AbsBindingFragment;
import com.proapp.sompom.newui.fragment.PopUpCommentFragment;
import com.proapp.sompom.newui.fragment.ReplyCommentFragment;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class CommentDialog extends AbsBindingFragmentDialog<DialogCommentBinding> implements OnCommentDialogClickListener {

    private PopUpCommentFragment mPopUpCommentFragment;
    private ReplyCommentFragment mReplyCommentFragment;
    private int mPosition;
    private CommentType mCommentType;
    private OnCloseReplyCommentFragmentListener mOnNotifyCommentItemChangeListener;
    private OnClickListener mOnDismissDialogListener;
    private boolean mAutoDisplayKeyboard = true;
    private String mPostId;
    private ContentType mContentType;

    public static CommentDialog newInstanceForComment(String itemID) {
        Bundle args = new Bundle();
        args.putString(SharedPrefUtils.ID, itemID);
        CommentDialog fragment = new CommentDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setAutoDisplayKeyboard(boolean autoDisplayKeyboard) {
        mAutoDisplayKeyboard = autoDisplayKeyboard;
    }

    public void setPostId(String postId) {
        mPostId = postId;
    }

    public void setContentType(ContentType contentType) {
        mContentType = contentType;
    }

    public static CommentDialog newInstanceForSubComment(String itemID, Comment comment) {
        Bundle args = new Bundle();
        args.putInt(SharedPrefUtils.STATUS, CommentType.REPLY.getId());
        args.putParcelable(SharedPrefUtils.DATA, comment);
        args.putString(SharedPrefUtils.ID, itemID);

        CommentDialog fragment = new CommentDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_comment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getActivity() == null) {
            return super.onCreateDialog(savedInstanceState);
        }

        return new Dialog(getActivity(), getTheme()) {
            @Override
            public void onBackPressed() {
                Fragment fragment = getFragment(ReplyCommentFragment.TAG);
                if (fragment instanceof ReplyCommentFragment && fragment.isVisible()) {
                    if (!((ReplyCommentFragment) fragment).isCloseSearchGifView()) {
                        if (mCommentType == CommentType.REPLY) {
                            super.onBackPressed();
                        } else {
                            backToCommentFragment();
                        }
                    }
                } else {
                    PopUpCommentFragment popUpCommentFragment = (PopUpCommentFragment) getFragment(PopUpCommentFragment.TAG);
                    boolean isCloseGifView = (popUpCommentFragment).isCloseSearchGifView();
                    if (!isCloseGifView) {
                        super.onBackPressed();
                    }
                }
            }
        };
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            String itemId = getArguments().getString(SharedPrefUtils.ID);
            mCommentType = CommentType.getType(getArguments().getInt(SharedPrefUtils.STATUS, CommentType.NORMAL.getId()));
            if (mCommentType == CommentType.REPLY) {
                Comment comment = getArguments().getParcelable(SharedPrefUtils.DATA);
                mReplyCommentFragment = (ReplyCommentFragment) getFragment(comment,
                        itemId,
                        false,
                        true,
                        true);
                mReplyCommentFragment.setOnCommentDialogClickListener(this);
                setFragment(R.id.containerCommentView,
                        mReplyCommentFragment,
                        ReplyCommentFragment.TAG);
                mReplyCommentFragment.setContentType(mContentType);
                mReplyCommentFragment.setPostId(mPostId);

            } else {
                mReplyCommentFragment = (ReplyCommentFragment) getFragment(null,
                        itemId,
                        false,
                        false,
                        true);
                mReplyCommentFragment.setContentType(mContentType);
                mReplyCommentFragment.setPostId(mPostId);
                mReplyCommentFragment.setOnCommentDialogClickListener(this);


                mPopUpCommentFragment = (PopUpCommentFragment) getFragment(null,
                        itemId,
                        mAutoDisplayKeyboard,
                        false,
                        false);
                mPopUpCommentFragment.setCommentDialogClickListener(this);
                mPopUpCommentFragment.setContentType(mContentType);
                mPopUpCommentFragment.setPostId(mPostId);

                getChildFragmentManager().beginTransaction()
                        .add(R.id.containerCommentView, mReplyCommentFragment, ReplyCommentFragment.TAG)
                        .hide(mReplyCommentFragment)
                        .add(R.id.containerCommentView, mPopUpCommentFragment, PopUpCommentFragment.TAG)
                        .commit();
            }
        }
    }

    private AbsBindingFragment getFragment(Comment comment,
                                           String itemId,
                                           boolean autoDisplayKeyboard,
                                           boolean isInSubCommentMode,
                                           boolean isBuildSubCommentFragment) {
        if (isBuildSubCommentFragment) {
            if (isInSubCommentMode) {
                return ReplyCommentFragment.newInstance(itemId, comment);
            } else {
                return ReplyCommentFragment.newInstance(itemId);
            }
        } else {
            return PopUpCommentFragment.newInstance(itemId, autoDisplayKeyboard);
        }
    }

    @Override
    public void onRemoveSubCommentSuccess() {
        if (mPopUpCommentFragment != null) {
            mPopUpCommentFragment.addRemoveCommentCount(1);
        }
    }

    @Override
    public void onPostSubCommentSuccess() {
        if (mPopUpCommentFragment != null) {
            mPopUpCommentFragment.addCommentCount(1);
        }
    }

    @Override
    public void onReplaceCommentFragment(Comment comment, int position) {
        mPosition = position;
        final FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right,
                R.anim.exit_to_left);
        showFragmentWithAnimation(fragmentTransaction, mReplyCommentFragment, mPopUpCommentFragment);
        mReplyCommentFragment.setData(comment, position);
    }

    @Override
    public void onDismissDialog(boolean isDismiss) {
        if (isDismiss) {
            dismiss();
        } else {
            if (mCommentType == CommentType.REPLY) {
                dismiss();
            } else {
                backToCommentFragment();
            }
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissDialogListener != null) {
            mOnDismissDialogListener.onClick();
        }
    }

    @Override
    public void onMainCommentItemRemoved(int position, Comment comment) {
        /*
        We will count both both main comment and its sub comment as total removed comment count.
         */
        int totalRemoveCommentCount = 1;
        if (comment.getTotalSubComment() > 0) {
            totalRemoveCommentCount += comment.getTotalSubComment();
        }

        if (mPopUpCommentFragment != null) {
            mPopUpCommentFragment.addRemoveCommentCount(totalRemoveCommentCount);
            backToCommentFragment();
            mPopUpCommentFragment.notifyItemRemove(position);
        } else {
            dismiss();
            if (mOnNotifyCommentItemChangeListener != null) {
                mOnNotifyCommentItemChangeListener.onMainCommentItemRemoved(totalRemoveCommentCount);
            }
        }
    }

    @Override
    public void onNotifyCommentItemChange(Comment comment) {
        if (mPopUpCommentFragment != null) {
            mPopUpCommentFragment.notifyItemChanged(mPosition, comment);
        }

        if (mOnNotifyCommentItemChangeListener != null) {
            mOnNotifyCommentItemChangeListener.onComplete(comment);
        }
    }

    private void backToCommentFragment() {
        final FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left,
                R.anim.exit_to_right, 0, 0);
        showFragmentWithAnimation(fragmentTransaction,
                mPopUpCommentFragment,
                mReplyCommentFragment);
    }

    public void setOnNotifyCommentItemChangeListener(OnCloseReplyCommentFragmentListener onNotifyCommentItemChangeListener) {
        mOnNotifyCommentItemChangeListener = onNotifyCommentItemChangeListener;
    }

    public void setOnDismissDialogListener(OnClickListener onDismissDialogListener) {
        mOnDismissDialogListener = onDismissDialogListener;
    }

    public int getNumberOfAddComment() {
        if (mPopUpCommentFragment != null) {
            return mPopUpCommentFragment.getNumberOfAddComment();
        } else if (mReplyCommentFragment != null) {
            return mReplyCommentFragment.getAddSubCommentCounter();
        }

        return 0;
    }

    public enum CommentType {
        NORMAL(1),
        REPLY(2);

        private final int mId;

        CommentType(int id) {
            this.mId = id;
        }

        public static CommentType getType(int id) {
            for (CommentType commentType : CommentType.values()) {
                if (commentType.getId() == id) {
                    return commentType;
                }
            }
            return NORMAL;
        }

        public int getId() {
            return mId;
        }
    }

    public interface OnCloseReplyCommentFragmentListener extends OnCompleteListener<Comment> {
        void onMainCommentItemRemoved(int totalRemoveCommentCount);

        void onUpdateMainCommentCount(long totalComment);
    }
}
