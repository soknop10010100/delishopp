package com.proapp.sompom.newui.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.UiThread;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.DialogBaseBinding;
import com.proapp.sompom.injection.controller.ControllerComponent;
import com.proapp.sompom.injection.controller.ControllerModule;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.utils.SnackBarUtil;

/**
 * Created by He Rotha on 10/25/17.
 */

public abstract class AbsBaseFragmentDialog extends DialogFragment {
    public static final String TAG = AbsBaseFragmentDialog.class.getName();
    private ControllerComponent mControllerComponent;

    @UiThread
    public ControllerComponent getControllerComponent() {
        if (mControllerComponent == null) {
            mControllerComponent = ((MainApplication) getActivity().getApplication())
                    .getApplicationComponent()
                    .newControllerComponent(new ControllerModule(getActivity()));
        }
        return mControllerComponent;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogStyle);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog().getWindow() != null) {
            getDialog()
                    .getWindow()
                    .setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.MATCH_PARENT);

        }
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogBaseBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_base, container, false);
        binding.layoutContainer.removeAllViews();
        binding.layoutContainer.addView(onCreateView(inflater, container));
        binding.root.setOnClickListener(v -> {
            if (isCancelable()) {
                dismiss();
            }
        });
        return binding.getRoot();
    }

    public abstract View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container);

    protected void showKeyboard(EditText editText) {
        editText.setFocusable(true);
        editText.setClickable(true);
        editText.requestFocus();
        KeyboardUtil.showKeyboard(getActivity(), editText);
    }

    public void showSnackBar(String text, boolean isError) {
        SnackBarUtil.showSnackBar(getView(), text, isError);
    }

    public void showSnackBar(@StringRes int message, boolean isError) {
        SnackBarUtil.showSnackBar(getView(), message, isError);
    }

    public void setFragment(@IdRes int id, Fragment fragment, String tag) {
        if (!isVisible() && !isFragmentAlreadyReplaced(tag)) {
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(id, fragment, tag)
                    .commit();
        }
    }

    private boolean isFragmentAlreadyReplaced(String tag) {
        Fragment fragment = getChildFragmentManager().findFragmentByTag(tag);
        return fragment != null && fragment.isVisible();
    }

    public Fragment getFragment(String tag) {
        return getChildFragmentManager().findFragmentByTag(tag);
    }

    public void showFragmentWithAnimation(FragmentTransaction fragmentTransaction,
                                          Fragment showFragment,
                                          Fragment hideFragment) {
        if (hideFragment == null || showFragment == null || hideFragment == showFragment) {
            return;
        }
        fragmentTransaction
                .hide(hideFragment)
                .show(showFragment)
                .commit();
        //make sure fragment know fragment is paused, or resumed
        hideFragment.onPause();
        showFragment.onResume();
    }
}
