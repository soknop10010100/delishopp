package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.newui.fragment.EmailSignUpFragment;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class EmailSignUpActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(EmailSignUpFragment.newInstance(), EmailSignUpFragment.class.getSimpleName());
    }
}
