package com.proapp.sompom.newui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.proapp.sompom.intent.PayWithCardIntent;
import com.proapp.sompom.newui.fragment.AbsSupportABAPaymentFragment;
import com.proapp.sompom.newui.fragment.PayWithCardFragment;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 3/17/22.
 */
public class PayWithCardActivity extends AbsDefaultActivity implements PayWithCardFragment.PayWithCardFragmentListener {

    private String mBasketId;
    private String mMessage;
    private boolean mIsPaymentSuccess;
    private boolean mIsAllowBackPress = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PayWithCardIntent payWithCardIntent = new PayWithCardIntent(getIntent());
        setFragment(PayWithCardFragment.newInstance(payWithCardIntent.getCheckoutUrl(),
                        payWithCardIntent.getContinueSuccessUrl(),
                        payWithCardIntent.getBasketId()),
                PayWithCardFragment.class.getName());
    }

    @Override
    public void onReceiveABAPaymentPushbackEvent(String basketId, boolean isPaymentSuccess, String message) {
        Timber.i("onReceiveABAPaymentPushbackEvent: basketId: " + basketId + ", isPaymentSuccess: " + isPaymentSuccess);
        mBasketId = basketId;
        mIsPaymentSuccess = isPaymentSuccess;
        mMessage = message;
    }

    @Override
    public void updateAllowBackPress(boolean isAllowed) {
        mIsAllowBackPress = isAllowed;
    }

    @Override
    public void finish() {
        if (!TextUtils.isEmpty(mBasketId)) {
            Intent intent = new Intent();
            intent.putExtra(AbsSupportABAPaymentFragment.IS_ABA_PAYMENT_SUCCESS, mIsPaymentSuccess);
            intent.putExtra(AbsSupportABAPaymentFragment.ABA_PAYMENT_PUSHBACK_MESSAGE, mMessage);
            setResult(AppCompatActivity.RESULT_OK, intent);
        } else {
            setResult(AppCompatActivity.RESULT_CANCELED);
        }
        super.finish();
    }

    @Override
    public void onBackPressed() {
        if (mIsAllowBackPress) {
            super.onBackPressed();
        }
    }
}
