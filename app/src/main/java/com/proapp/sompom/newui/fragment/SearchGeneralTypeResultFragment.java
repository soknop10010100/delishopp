package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.library.baseAdapters.BR;

import com.desmond.squarecamera.utils.Keys;
import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.SearchGeneralTypeResultAdapter;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.databinding.FragmentSearchMessageResultBinding;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnLikeItemListener;
import com.proapp.sompom.model.Search;
import com.proapp.sompom.model.emun.GeneralSearchResultType;
import com.proapp.sompom.model.emun.OpenChatScreenType;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.SearchGeneralTypeResultDataManager;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.viewmodel.newviewmodel.SearchMessageResultFragmentViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.SearchResultScreenViewModel;
import com.proapp.sompom.widget.LoaderMoreLayoutManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class SearchGeneralTypeResultFragment extends AbsBindingFragment<FragmentSearchMessageResultBinding>
        implements OnLikeItemListener,
        SearchGeneralTypeResultAdapter.SearchGeneralTypeResultAdapterListener,
        LoaderMoreLayoutManager.OnLoadMoreCallback {

    @Inject
    public ApiService mApiService;
    private SearchMessageResultFragmentViewModel mViewModel;
    private SearchGeneralTypeResultAdapter mAdapter;
    private LoaderMoreLayoutManager mLoaderMoreLayoutManager;
    private SearchResultScreenViewModel mResultScreenViewModel;
    private GeneralSearchResultType mGeneralSearchResultType;

    public static SearchGeneralTypeResultFragment newInstance(GeneralSearchResultType type, ArrayList<Search> searches) {
        Bundle args = new Bundle();
        args.putInt(Keys.SOURCE_TYPE, type.getValue());
        args.putParcelableArrayList(Keys.DATA, searches);
        SearchGeneralTypeResultFragment fragment = new SearchGeneralTypeResultFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search_message_result;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mGeneralSearchResultType = GeneralSearchResultType.getFromValue(getArguments().getInt(Keys.SOURCE_TYPE));
        List<Search> data = null;
        if (getArguments() != null && getArguments().containsKey(Keys.DATA)) {
            data = getArguments().getParcelableArrayList(Keys.DATA);
        }

        if (data == null) {
            data = Collections.emptyList();
        }

        mLoaderMoreLayoutManager = new LoaderMoreLayoutManager(getActivity(), getBinding().recyclerView);
        getBinding().recyclerView.setLayoutManager(mLoaderMoreLayoutManager);
        getBinding().recyclerView.setOnTouchListener((view1, motionEvent) -> {
            KeyboardUtil.hideKeyboard(getActivity());
            view1.performClick();
            return false;
        });

        mAdapter = new SearchGeneralTypeResultAdapter(getActivity(),
                data,
                SearchGeneralTypeResultFragment.this,
                this);
        mAdapter.setCanLoadMore(false);
        getBinding().recyclerView.setAdapter(mAdapter);

        mViewModel = new SearchMessageResultFragmentViewModel(requireContext());

        mResultScreenViewModel = new SearchResultScreenViewModel(requireContext());
        setVariable(BR.viewModel, mResultScreenViewModel);
    }

    @Override
    public void onLoadMoreFromBottom() {
        Timber.i("onLoadMoreFromBottom");
        mViewModel.loadMore(mGeneralSearchResultType,
                new OnCallbackListListener<List<Search>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        Timber.e("onFail: " + ex.getMessage());
                        mLoaderMoreLayoutManager.setOnLoadMoreListener(null);
                        mAdapter.setCanLoadMore(false);
                        mAdapter.notifyDataSetChanged();
                        mLoaderMoreLayoutManager.loadingFinished();
                    }

                    @Override
                    public void onComplete(List<Search> result, boolean canLoadMore) {
                        Timber.i("onComplete: canLoadMore" + canLoadMore);
                        if (!canLoadMore) {
                            mLoaderMoreLayoutManager.setOnLoadMoreListener(null);
                        }
                        mAdapter.addLoadMoreData(result);
                        mAdapter.setCanLoadMore(canLoadMore);
                        mLoaderMoreLayoutManager.loadingFinished();
                    }
                });
    }

    @Override
    public void onFollowClick(User user, boolean isFollow) {
        mViewModel.onFollowClick(user, isFollow);
    }

    @Override
    public void onNotifyItem() {
        if (mAdapter != null) {
            mAdapter.notifyData();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null) {
            mAdapter.unRegisterReceiver();
        }
    }

    private boolean hasPreviousEmptyResult() {
        if (getBinding().recyclerView.getAdapter() instanceof SearchGeneralTypeResultAdapter) {
            return getBinding().recyclerView.getAdapter().getItemCount() <= 0;
        }

        return false;
    }

    public void setSearchKey(String searchKey) {
        mViewModel.setSearchText(searchKey);
    }

    public void bindData(List<Search> searches,
                         boolean isLocalData,
                         boolean isCanLoadMore,
                         SearchGeneralTypeResultDataManager dataManager) {
        mViewModel.setDataManager(dataManager);
        mLoaderMoreLayoutManager.setOnLoadMoreListener(isCanLoadMore ? this : null);
//        Timber.i("bindData: " + searches.size() + ", isLocalData: " + isLocalData + ", isCanLoadMore: " + isCanLoadMore);
        if (mAdapter != null) {
            mAdapter.setCanLoadMore(isCanLoadMore);
            mResultScreenViewModel.hideLoading();
            //Since we use the error support RecylcerView, we must check the error type.
            if (!(getBinding().recyclerView.getAdapter() instanceof SearchGeneralTypeResultAdapter)) {
                getBinding().recyclerView.setAdapter(mAdapter);
            }

            if (isLocalData) {
                mAdapter.setData(searches);
            } else {
                if (!searches.isEmpty()) {
                    mAdapter.setData(searches);
                } else if (hasPreviousEmptyResult()) {
                    mResultScreenViewModel.showNoDataScreen();
                }
            }
        }
    }

    @Override
    public String getSearchKeyWord() {
        return mViewModel.getSearchText();
    }

    @Override
    public void noParentConversationOfChatAttached(Chat chat) {
        Conversation conversationById = ConversationDb.getConversationById(requireContext(), chat.getChannelId());
//        Timber.i("noParentConversationOfChatAttached: Local conversation: " + new Gson().toJson(conversationById));
        if (conversationById != null && conversationById.isDeleted()) {
//            Timber.i("Ignore the open search chat conversation since the conversation is removed.");
            return;
        }

        if (conversationById != null) {
            chat.setParentConversation(conversationById);
            startActivity(new ChatIntent(requireContext(),
                    chat.getParentConversation(),
                    chat,
                    mViewModel.getSearchText(),
                    OpenChatScreenType.GENERAL_SEARCH));
        } else {
            mViewModel.requestGroupDetail(chat.getChannelId(),
                    mViewModel,
                    conversation1 -> {
                        chat.setParentConversation(conversation1);
                        startActivity(new ChatIntent(requireContext(),
                                chat.getParentConversation(),
                                chat,
                                mViewModel.getSearchText(),
                                OpenChatScreenType.GENERAL_SEARCH));
                    });
        }
    }
}
