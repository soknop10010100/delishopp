package com.proapp.sompom.newui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.proapp.sompom.R;

/**
 * Created by He Rotha on 2019-10-01.
 */
public class CallBalanceSynchronizeDialog extends Dialog {

    TextView mTextViewTitle;
    TextView mTextViewDescription;
    TextView mTextViewButton;
    String mTitle;
    String mDescription;

    public CallBalanceSynchronizeDialog(@NonNull Context context, String title, String desc) {
        super(context);
        this.mTitle = title;
        this.mDescription = desc;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_call_balance_check);
        mTextViewDescription = findViewById(R.id.textview_balance_call_message);
        mTextViewTitle = findViewById(R.id.textview_balance_call_title);
        mTextViewButton = findViewById(R.id.text_button_balance_call_view);
        mTextViewTitle.setText(mTitle);
        mTextViewButton.setText("Ok");
        mTextViewDescription.setText(mDescription);
        mTextViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }
}
