package com.proapp.sompom.newui.fragment;

import android.Manifest;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.GroupPhotoAndVideoAdapter;
import com.proapp.sompom.databinding.FragmentGroupDetailMediaBinding;
import com.proapp.sompom.decorataor.GroupMediaSectionItemDecorator;
import com.proapp.sompom.model.GroupMediaSection;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.GroupMediaDetailType;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.FullScreenMediaDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.GroupDetailMediaDataManager;
import com.proapp.sompom.utils.ToastUtil;
import com.proapp.sompom.viewmodel.newviewmodel.GroupDetailPhotoAndVideoViewModel;
import com.proapp.sompom.widget.LoaderMoreLayoutManager;
import com.sompom.baseactivity.PermissionCheckHelper;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Chhom Veasna on 7/24/2020.
 */
public class GroupDetailPhotoAndVideoFragment extends AbsGroupDetailMediaFragment<FragmentGroupDetailMediaBinding> implements
        GroupDetailPhotoAndVideoViewModel.GroupDetailMediaViewModelListener<Media>,
        LoaderMoreLayoutManager.OnLoadMoreCallback,
        GroupPhotoAndVideoAdapter.onItemClickListener<Media> {

    @Inject
    public ApiService mApiService;
    private GroupDetailPhotoAndVideoViewModel mViewModel;
    private LoaderMoreLayoutManager mLoaderMoreLayoutManager;
    private GroupPhotoAndVideoAdapter mAdapter;

    public static GroupDetailPhotoAndVideoFragment newInstance(String groupId, GroupMediaDetailType type) {

        Bundle args = new Bundle();
        args.putString(GROUP_ID, groupId);
        args.putString(TYPE, type.getValue());

        GroupDetailPhotoAndVideoFragment fragment = new GroupDetailPhotoAndVideoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        if (getArguments() != null) {
            mViewModel = new GroupDetailPhotoAndVideoViewModel(requireContext(),
                    new GroupDetailMediaDataManager(requireContext(),
                            mApiService,
                            getGroupId(),
                            GroupMediaDetailType.fromValue(getArguments().getString(TYPE))),
                    this);
            setVariable(BR.viewModel, mViewModel);
        }
    }

    @Override
    protected void onReceivedFirstNavigation() {
        new Handler().postDelayed(() -> mViewModel.getMediaDetail(false), 100);
    }

    @Override
    public void onLoadDataSuccess(List<GroupMediaSection<Media>> data,
                                  boolean canLoadMore,
                                  boolean isLoadMoreResult,
                                  GroupMediaDetailType type) {
        if (mAdapter == null) {
            mLoaderMoreLayoutManager = new LoaderMoreLayoutManager(requireContext());
            mAdapter = new GroupPhotoAndVideoAdapter(data);
            mAdapter.setListener(this);
            mAdapter.setCanLoadMore(canLoadMore);
            if (getBinding().recyclerView.getItemDecorationCount() <= 0) {
                getBinding().recyclerView.addItemDecoration(new GroupMediaSectionItemDecorator(getResources()
                        .getDimensionPixelSize(R.dimen.space_large),
                        getResources().getDimensionPixelSize(R.dimen.space_xlarge)));
            }
            getBinding().recyclerView.setLayoutManager(mLoaderMoreLayoutManager);
            getBinding().recyclerView.setAdapter(mAdapter);
            if (canLoadMore && !data.isEmpty()) {
                mLoaderMoreLayoutManager.setOnLoadMoreListener(this);
            }
        } else {
            mAdapter.setCanLoadMore(canLoadMore);
            if (!canLoadMore || data.isEmpty()) {
                mLoaderMoreLayoutManager.setOnLoadMoreListener(null);
            } else {
                mLoaderMoreLayoutManager.setOnLoadMoreListener(this);
            }
            if (isLoadMoreResult) {
                mLoaderMoreLayoutManager.loadingFinished();
                mAdapter.addLoadMoreData(data);
            } else {
                //Refresh or reset
                mAdapter.setDatas(data);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onLoadFailed(ErrorThrowable ex, boolean isLoadMore, GroupMediaDetailType type) {
        if (isLoadMore) {
            mAdapter.setCanLoadMore(false);
            mLoaderMoreLayoutManager.loadingFinished();
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoadMoreFromBottom() {
        mViewModel.getMediaDetail(true);
    }

    @Override
    public void onItemClicked(Media media) {
        FullScreenMediaDialog dialog = FullScreenMediaDialog.newInstance(Collections.singletonList(media),
                0,
                true);
        dialog.setChatImageFullScreenDialogListener(media1 -> {
            AbsBaseActivity absBaseActivity = ((AbsBaseActivity) getContext());
            if (absBaseActivity != null) {
                absBaseActivity.checkPermission(new PermissionCheckHelper.OnPermissionCallback() {
                    @Override
                    public void onPermissionGranted() {
                        dialog.saveMedia(media1);
                    }

                    @Override
                    public void onPermissionDenied(String[] grantedPermission,
                                                   String[] deniedPermission,
                                                   boolean isUserPressNeverAskAgain) {
                        ToastUtil.showToast(getContext(), R.string.permission_not_granted, true);
                    }
                }, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
        });
        dialog.show(requireActivity().getSupportFragmentManager(), null);
    }
}
