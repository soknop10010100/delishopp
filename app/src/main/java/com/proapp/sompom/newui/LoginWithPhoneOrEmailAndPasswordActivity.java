package com.proapp.sompom.newui;

import android.os.Bundle;

import com.proapp.sompom.newui.fragment.LoginWithPhoneOrEmailAndPasswordFragment;

import androidx.annotation.Nullable;

public class LoginWithPhoneOrEmailAndPasswordActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(LoginWithPhoneOrEmailAndPasswordFragment.newInstance(),
                LoginWithPhoneOrEmailAndPasswordFragment.TAG);
    }
}
