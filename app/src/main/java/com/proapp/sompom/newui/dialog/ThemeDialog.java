package com.proapp.sompom.newui.dialog;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.desmond.squarecamera.model.AppTheme;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.services.ApiService;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by He Rotha on 6/19/18.
 */
public class ThemeDialog extends MessageDialog {

    private OnThemeChooseListener mOnThemeChooseListener;
    private AppTheme mCurrentTheme;
    @Inject
    public ApiService mApiService;

    public void setOnThemeChooseListener(OnThemeChooseListener onThemeChooseListener) {
        mOnThemeChooseListener = onThemeChooseListener;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        getControllerComponent().inject(this);
        setTitle(getString(R.string.profile_user_section_theme_title));
        setMessage(getString(R.string.setting_change_theme_dialog_description));
        List<AppTheme> availableThemes = UserHelper.getAvailableThemeResources(requireContext());
        mCurrentTheme = UserHelper.getSelectedThemeResource(requireContext());
        final String[] currencies = new String[availableThemes.size()];
        int selectPosition = 0;
        for (int i = 0; i < currencies.length; i++) {
            if (getContext() == null) {
                break;
            }
            currencies[i] = getContext().getString(availableThemes.get(i).getStringRes());
            if (mCurrentTheme == availableThemes.get(i)) {
                selectPosition = i;
            }
        }
        setSingleChoiceItems(currencies, selectPosition, (dialog, which) -> {
            mCurrentTheme = availableThemes.get(which);
        });
        setLeftText(getString(R.string.setting_change_theme_dialog_ok_button), view1 -> {
            UserHelper.setUserThemeId(requireContext(), mApiService, mCurrentTheme.getId());
            mOnThemeChooseListener.onSelect(mCurrentTheme);
        });
        setRightText(getString(R.string.setting_change_theme_dialog_cancel_button), null);

        super.onViewCreated(view, savedInstanceState);
    }

    public interface OnThemeChooseListener {
        void onSelect(AppTheme currency);
    }
}
