package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.EditMediaFileAdapter;
import com.proapp.sompom.databinding.FragmentEditMediaFileBinding;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.newui.EditMediaFileActivity;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.EditMediaFileViewModel;

import timber.log.Timber;

/**
 * Created by nuonveyo on 5/7/18.
 */

public class EditMediaFileFragment extends AbsBindingFragment<FragmentEditMediaFileBinding> {
    public static final String TAG = EditMediaFileFragment.class.getName();
    private LifeStream mLifeStream;
    private int mClickPosition;
    private EditMediaFileAdapter mDetailAdapter;
    private boolean mIsEdit;

    public static EditMediaFileFragment newInstance(LifeStream lifeStream,
                                                    int position,
                                                    boolean isEdit) {

        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.CATEGORY, lifeStream);
        args.putInt(SharedPrefUtils.USER_ID, position);
        args.putBoolean(SharedPrefUtils.STATUS, isEdit);

        EditMediaFileFragment fragment = new EditMediaFileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_edit_media_file;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hideKeyboard();
        if (getArguments() != null) {
            mLifeStream = getArguments().getParcelable(SharedPrefUtils.CATEGORY);
            mClickPosition = getArguments().getInt(SharedPrefUtils.USER_ID);
            mIsEdit = getArguments().getBoolean(SharedPrefUtils.STATUS);
        }

        Timber.i("Edit: " + mIsEdit);

        setToolbar(getBinding().layoutToolbar.toolbar, true);
        getBinding().layoutToolbar.textViewTitle.setText(R.string.create_wall_street_edit_media_title);

        EditMediaFileViewModel viewModel = new EditMediaFileViewModel(getActivity(), this::closeScreen);
        setVariable(com.proapp.sompom.BR.viewModel, viewModel);
        initData();
    }

    public LifeStream getLifeStream() {
        return mLifeStream;
    }

    public void initData() {
        mDetailAdapter = new EditMediaFileAdapter(mLifeStream,
                mIsEdit,
                position -> {
                    mLifeStream.getMedia().remove(position);
                    mDetailAdapter.notifyItemRemoved(position);
                    if (mLifeStream.getMedia().isEmpty()) {
                        closeScreen();
                    }
                });

        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getBinding().recyclerView.smoothScrollToPosition(mClickPosition);
        getBinding().recyclerView.setAdapter(mDetailAdapter);
    }

    private void closeScreen() {
        if (getActivity() instanceof EditMediaFileActivity) {
            ((EditMediaFileActivity) getActivity()).onSetResult(getLifeStream());
            getActivity().finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getBinding().recyclerView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().recyclerView.onPause();

    }
}
