package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.intent.newintent.GroupDetailIntent;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.newui.fragment.GroupDetailFragment;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by Or Vitovongsak on 4/28/20.
 */
public class GroupDetailActivity extends AbsDefaultActivity{

    private Conversation mConversation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mConversation = getIntent().getParcelableExtra(SharedPrefUtils.CONVERSATION);
        setFragment(GroupDetailFragment.newInstance(mConversation), GroupDetailFragment.TAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
