package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.CommonViewPagerAdapter;
import com.proapp.sompom.databinding.FragmentInBoardingBinding;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.intent.newintent.HomeIntent;
import com.proapp.sompom.model.InBoardItem;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.InboardingDataManager;
import com.proapp.sompom.viewmodel.InBoardingViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class InBoardingFragment extends AbsBindingFragment<FragmentInBoardingBinding> implements
        InBoardingViewModel.InBoardingViewModelListener {

    @Inject
    @PublicQualifier
    public ApiService mApiService;

    public static InBoardingFragment newInstance() {

        Bundle args = new Bundle();

        InBoardingFragment fragment = new InBoardingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_in_boarding;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        InBoardingViewModel viewModel = new InBoardingViewModel(new InboardingDataManager(requireContext(),
                mApiService),
                this);
        setVariable(BR.viewModel, viewModel);
        viewModel.requestData();
    }

    @Override
    public void onActionDone() {
        checkToNavigateToNextScreen();
    }

    @Override
    public void onNextPage() {
        if (getBinding().viewPager.getAdapter() != null) {
            if (getBinding().viewPager.getCurrentItem() < (getBinding().viewPager.getAdapter().getCount() - 1)) {
                getBinding().viewPager.setCurrentItem(getBinding().viewPager.getCurrentItem() + 1,
                        true);
            } else {
                checkToNavigateToNextScreen();
            }
        }
    }

    @Override
    public void onDataLoadSuccess(List<InBoardItem> data) {
        List<AbsBaseFragment> pages = new ArrayList<>();
        for (InBoardItem datum : data) {
            pages.add(InBoardingItemFragment.newInstance(datum));
        }
        CommonViewPagerAdapter adapter = new CommonViewPagerAdapter(getChildFragmentManager(), pages);
        getBinding().viewPager.setAdapter(adapter);
        getBinding().viewPager.setOffscreenPageLimit(data.size());
        if (data.size() > 1) {
            getBinding().indicator.setViewPager(getBinding().viewPager);
        }
    }

    private void checkToNavigateToNextScreen() {
        if (ApplicationHelper.isAllowToUseWithoutLogin(requireContext())) {
            startActivity(new HomeIntent(requireContext()));
            requireActivity().finish();
            requireActivity().overridePendingTransition(0, R.anim.activity_fade_out);
        } else {
            startActivity(ApplicationHelper.getLoginIntent(requireContext(), true));
            requireActivity().finish();
        }
    }
}
