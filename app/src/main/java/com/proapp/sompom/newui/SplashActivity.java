package com.proapp.sompom.newui;

import android.os.Bundle;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.intent.DeeplinkIntent;
import com.proapp.sompom.intent.newintent.TimelineDetailIntent;
import com.proapp.sompom.model.emun.DeeplinkType;
import com.proapp.sompom.newui.fragment.SplashFragment;
import com.proapp.sompom.services.ServerUrl;

public class SplashActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Currently we always reset the application mode type normal when start up.
        ApplicationHelper.resetApplicationMode(this);
        ConciergeHelper.setShowOutOfStockProduct(false);
        ConciergeHelper.setSortItemOption(null);
        ServerUrl.switchToDevEnvironment(getApplicationContext());
        ((MainApplication) getApplication()).clearAllOpenConversations();

        //Move from Home activity
        DeeplinkIntent intent = new DeeplinkIntent(getIntent());
        DeeplinkType deepLink = intent.getDeeplinkType();
        if (deepLink != DeeplinkType.None) {
            String id = intent.getId();
            if (deepLink == DeeplinkType.Feed) {
                startActivity(new TimelineDetailIntent(getThis(), id));
            }
        }

        setFragment(SplashFragment.newInstance(), SplashFragment.class.getSimpleName());
    }
}
