package com.proapp.sompom.newui;

import com.proapp.sompom.newui.fragment.LoginFragment;

import androidx.fragment.app.Fragment;

public class LoginActivity extends SupportResetPasswordDeepLinkActivity {

    @Override
    protected Fragment getFragment() {
        return LoginFragment.newInstance();
    }

    @Override
    protected String getFragmentTag() {
        return LoginFragment.TAG;
    }
}
