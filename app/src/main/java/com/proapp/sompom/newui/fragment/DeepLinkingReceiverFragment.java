package com.proapp.sompom.newui.fragment;

import android.os.Bundle;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentDeepLinkngReceiverBinding;

public class DeepLinkingReceiverFragment extends AbsBindingFragment<FragmentDeepLinkngReceiverBinding> {

    public static DeepLinkingReceiverFragment newInstance() {

        Bundle args = new Bundle();

        DeepLinkingReceiverFragment fragment = new DeepLinkingReceiverFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_deep_linkng_receiver;
    }
}
