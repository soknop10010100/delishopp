package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.intent.newintent.ConciergeOrderDeliveryTrackingIntent;
import com.proapp.sompom.newui.fragment.ConciergeOrderDeliveryTrackingFragment;

/**
 * Created by Or Vitovongsak on 16/12/21.
 */
public class ConciergeOrderDeliveryTrackingActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConciergeOrderDeliveryTrackingIntent intent = new ConciergeOrderDeliveryTrackingIntent(getIntent());
        setFragment(ConciergeOrderDeliveryTrackingFragment.newInstance(intent.getOrderId(),
                        intent.getOrder()),
                ConciergeOrderDeliveryTrackingFragment.class.getName());
    }
}
