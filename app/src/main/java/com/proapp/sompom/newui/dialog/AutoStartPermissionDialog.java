package com.proapp.sompom.newui.dialog;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.judemanutd.autostarter.AutoStartPermissionHelper;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.PartialAutoStartPermissionDialogBinding;
import com.proapp.sompom.helper.AutoStartPermissionDialogHelper;

/**
 * Created by Veasna Chhom on 10/22/20.
 */
public class AutoStartPermissionDialog extends MessageDialog {

    private boolean mIsCompletedOrDontAskAgain;

    public static AutoStartPermissionDialog newInstance() {

        Bundle args = new Bundle();

        AutoStartPermissionDialog fragment = new AutoStartPermissionDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        PartialAutoStartPermissionDialogBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.partial_auto_start_permission_dialog,
                null,
                true);
        getBinding().textviewTitle.setTypeface(null, Typeface.BOLD);
        setCancelable(false);
        setTitle(getString(R.string.popup_device_notification_setting_title));
        setMessage(getString(R.string.popup_device_notification_setting_description,
                getResources().getString(R.string.app_name)));
        setLeftText(getString(R.string.popup_device_notification_setting_proceed_button), view1 -> {
            AutoStartPermissionDialogHelper.setLastUserResponseState(requireContext(),
                    true);
            AutoStartPermissionHelper.getInstance().getAutoStartPermission(requireContext());
        });
        setRightText(getString(R.string.popup_no_button), view12 ->
                AutoStartPermissionDialogHelper.setLastUserResponseState(requireContext(),
                        false));

        binding.checkboxNotAskAgain.setOnCheckedChangeListener((compoundButton, b) -> {
            mIsCompletedOrDontAskAgain = b;
        });
        getBinding().view.addView(binding.getRoot());
        super.onViewCreated(view, savedInstanceState);
        new Handler().post(() -> AutoStartPermissionDialogHelper.setLastShownAlertDate(requireContext(), System.currentTimeMillis()));
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mIsCompletedOrDontAskAgain) {
            AutoStartPermissionDialogHelper.setCheckingProcessCompleted(requireContext(), mIsCompletedOrDontAskAgain);
        }
    }
}
