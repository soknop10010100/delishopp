package com.proapp.sompom.newui;

import static com.proapp.sompom.newui.ChatActivity.REFRESH_NOTIFICATION_BADGE_ACTION;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.newui.fragment.ConciergeOrderHistoryFragment;
import com.sompom.pushy.service.SendBroadCastHelper;

/**
 * Created by Chhom Veasna on 6/27/2019.
 */
public class ConciergeOrderHistoryActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(ConciergeOrderHistoryFragment.newInstance(false), ConciergeOrderHistoryFragment.class.getName());
        SendBroadCastHelper.verifyAndSendBroadCast(this, new Intent(REFRESH_NOTIFICATION_BADGE_ACTION));
    }
}
