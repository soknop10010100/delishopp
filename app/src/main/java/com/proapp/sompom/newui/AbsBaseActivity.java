package com.proapp.sompom.newui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.ViewGroup;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.NetworkBroadcastReceiver;
import com.proapp.sompom.broadcast.upload.FloatingVideoService;
import com.proapp.sompom.chat.AbsChatBinder;
import com.proapp.sompom.chat.call.CallingService;
import com.proapp.sompom.chat.call.IncomingCallDataHolder;
import com.proapp.sompom.chat.listener.ServiceStateListener;
import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.helper.SingleRequestLocation;
import com.proapp.sompom.injection.controller.ControllerComponent;
import com.proapp.sompom.injection.controller.ControllerModule;
import com.proapp.sompom.intent.newintent.TimelineDetailIntent;
import com.proapp.sompom.listener.OnCompleteListener;
import com.proapp.sompom.listener.OnFloatingVideoRemoveListener;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.emun.TimelineDetailRedirectionType;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.dialog.SessionDialog;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.resourcemanager.helper.LocaleManager;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.baseactivity.ResultPermissionActivity;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public abstract class AbsBaseActivity extends ResultPermissionActivity implements ServiceConnection {

    public static final String MODE_CHANGE_EVENT = "MODE_CHANGE_EVENT";

    private final List<OnServiceListener> mOnServiceListener = new ArrayList<>();
    private AbsChatBinder mChatBinder;
    private SingleRequestLocation mSingleRequestLocation;
    private ControllerComponent mControllerComponent;
    private FloatingVideoService.FloatingVideoBinder mBinder;
    private OnFloatingVideoRemoveListener mOnFloatingVideoRemoveListener;
    private List<ActivityLifeCycleListener> mLifeCycleListeners = new ArrayList<>();
    private CallingService.CallServiceBinder mCallServiceBinder;
    private boolean mReceivedCommandToShowSessionDialogError;

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private final ServiceConnection mFloatingVideoServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to FloatingVideoService, cast the IBinder and get FloatingVideoService instance
            mBinder = (FloatingVideoService.FloatingVideoBinder) service;
            mBinder.setListener(new FloatingVideoService.OnVideoClickListener() {
                @Override
                public void onVideoClick(int position, LifeStream lifeStream) {
                    TimelineDetailIntent intent = new TimelineDetailIntent(AbsBaseActivity.this,
                            lifeStream,
                            position,
                            TimelineDetailRedirectionType.FLOATING_VIDEO);
                    startActivity(intent);
                }

                @Override
                public void onFloatingVideoPlayingFinish() {
                    if (mOnFloatingVideoRemoveListener != null) {
                        mOnFloatingVideoRemoveListener.onRemove();
                    }
                }
            });
            onFloatingVideoServiceConnected();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            // default implementation ignored
        }
    };
    private List<NetworkBroadcastReceiver.NetworkListener> mNetworkListeners;
    private NetworkBroadcastReceiver mNetworkBroadcastReceiver;
    private NetworkBroadcastReceiver.NetworkState mNetworkState = NetworkBroadcastReceiver.NetworkState.Nothing;
    private String mCurrentLocalize;
    private AppTheme mCurrentAppTheme;

    public FloatingVideoService.FloatingVideoBinder getFloatingVideo() {
        return mBinder;
    }

    public void setOnFloatingVideoRemoveListener(OnFloatingVideoRemoveListener onFloatingVideoRemoveListener) {
        mOnFloatingVideoRemoveListener = onFloatingVideoRemoveListener;
    }

    public AbsBaseActivity getThis() {
        return this;
    }

    public void setFragment(@IdRes int id, Fragment fragment, String tag) {
        if (!isFinishing() && !isFragmentAlreadyReplaced(tag)) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(id, fragment, tag)
                    .commitAllowingStateLoss();
        }
    }

    public SingleRequestLocation getSingleRequestLocation() {
        return mSingleRequestLocation;
    }

    public void addActivityLifeCycleListener(ActivityLifeCycleListener listener) {
        mLifeCycleListeners.add(listener);
    }

    public void removeActivityLifeCycleListener(ActivityLifeCycleListener listener) {
        mLifeCycleListeners.remove(listener);
    }

    private boolean isFragmentAlreadyReplaced(String tag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        return fragment != null && fragment.isVisible();
    }

    public Fragment getFragment(String tag) {
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((MainApplication) getApplication()).setForceGroundActivity(this);
        for (ActivityLifeCycleListener lifeCycleListener : mLifeCycleListeners) {
            lifeCycleListener.onResume();
        }
    }

    protected boolean isThereAppConfigurationChanged() {
        return !mCurrentLocalize.equalsIgnoreCase(SharedPrefUtils.getLanguage(this));
    }

    protected void completelyRecreateActivity() {
        /*
        Need to remove all fragments attached to activity first to make sure all the
        fragment state and data will be recreate.
         */
        removeAllFragmentInsideActivity();
        recreate();
    }

    private void removeAllFragmentInsideActivity() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        List<Fragment> fragmentList = fragmentManager.getFragments();
        for (Fragment fragment : fragmentList) {
            fragmentTransaction.remove(fragment);
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void requestLocation(boolean enableShowErrorPopup, SingleRequestLocation.Callback callback) {
        if (mSingleRequestLocation == null) {
            mSingleRequestLocation = new SingleRequestLocation(this,
                    getString(R.string.popup_request_location_permission_description));
        }
        mSingleRequestLocation.execute(enableShowErrorPopup, callback);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mSingleRequestLocation != null) {
            mSingleRequestLocation.onActivityResult(requestCode, resultCode, data);
        }

        /*
          Pass {@link #onActivityResult} to every activity's fragment which are
          added and visible.
         */
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment != null && fragment.isAdded() && fragment.isVisible()) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mSingleRequestLocation != null) {
            mSingleRequestLocation.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        /*
          Pass {@link #onActivityResult} to every activity's fragment which are
          added and visible.
         */
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment != null && fragment.isAdded() && fragment.isVisible()) {
                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        for (ActivityLifeCycleListener lifeCycleListener : mLifeCycleListeners) {
            lifeCycleListener.onCreate();
        }

        mCurrentAppTheme = ThemeManager.getAppTheme(this);
        mCurrentLocalize = SharedPrefUtils.getLanguage(this);

        setTheme(mCurrentAppTheme.getThemeRes());
//        getApplicationContext().bindService(new Intent(this, SocketService.class), this,
//                BIND_AUTO_CREATE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        for (ActivityLifeCycleListener lifeCycleListener : mLifeCycleListeners) {
            lifeCycleListener.onStart();
        }

        // Bind to FloatingVideoService
        Intent intent = new Intent(this, FloatingVideoService.class);
        bindService(intent, mFloatingVideoServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        for (ActivityLifeCycleListener lifeCycleListener : mLifeCycleListeners) {
            lifeCycleListener.onStop();
        }
        unbindService(mFloatingVideoServiceConnection);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((MainApplication) getApplication()).checkToClearForceGroundActivity(this);
        for (ActivityLifeCycleListener lifeCycleListener : mLifeCycleListeners) {
            lifeCycleListener.onDestroy();
        }
    }

    public void broadcastModeChangeReceiver() {
        Intent intent = new Intent(MODE_CHANGE_EVENT);
        SendBroadCastHelper.verifyAndSendBroadCast(this, intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        for (ActivityLifeCycleListener lifeCycleListener : mLifeCycleListeners) {
            lifeCycleListener.onPause();
        }
    }

    @UiThread
    public ControllerComponent getControllerComponent() {
        if (mControllerComponent == null) {
            mControllerComponent = ((MainApplication) getApplication())
                    .getApplicationComponent()
                    .newControllerComponent(new ControllerModule(this));
        }
        return mControllerComponent;
    }

    public void startLoginWizardActivity(LoginActivityResultCallback callback) {
        if (SharedPrefUtils.isLogin(this)) {
            callback.onActivityResultSuccess(true);
        } else {
            startActivityForResult(ApplicationHelper.getLoginIntent(this, true),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            if (SharedPrefUtils.isLogin(AbsBaseActivity.this)) {
                                callback.onActivityResultSuccess(false);
                            } else {
                                callback.onActivityResultFail();
                            }
                        }

                        @Override
                        public void onActivityResultFail() {
                            callback.onActivityResultFail();
                        }
                    });
        }
    }

    /**
     * Sub-Class override to perform remove floating video this is playing
     */
    public void onFloatingVideoServiceConnected() {
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (SocketService.class.getName().equals(componentName.getClassName())) {
            mChatBinder = (AbsChatBinder) iBinder;
            mChatBinder.startService(new ServiceStateListener() {
                @Override
                public void onServiceStart() {
                    Timber.i("Chat onServiceStart");
                    onChatSocketServiceReady();
                }

                @Override
                public void onServiceFail(Exception ex) {
                    Timber.e("Chat onServiceFail %s", ex.toString());

                }
            });

            if (!mOnServiceListener.isEmpty()) {
                for (int i = mOnServiceListener.size() - 1; i >= 0; i--) {
                    mOnServiceListener.get(i).onServiceReady(mChatBinder);
                    mOnServiceListener.remove(i);
                }
            }
        }
    }

    public void startSubscribeAllGroupChannel() {
        if (mChatBinder != null) {
            mChatBinder.startSubscribeAllGroupChannel();
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    public void addOnServiceListener(OnServiceListener onServiceListener) {
        if (!mOnServiceListener.contains(onServiceListener)) {
            mOnServiceListener.add(onServiceListener);
        }
        if (mChatBinder != null) {
            for (int i = mOnServiceListener.size() - 1; i >= 0; i--) {
                mOnServiceListener.get(i).onServiceReady(mChatBinder);
                mOnServiceListener.remove(i);
            }
        }
    }

    public void removeServiceListener(OnServiceListener onServiceListener) {
        mOnServiceListener.remove(onServiceListener);
    }

    public void addNetworkStateChangeListener(NetworkBroadcastReceiver.NetworkListener networkListener) {
        if (mNetworkState == NetworkBroadcastReceiver.NetworkState.Connected
                || mNetworkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
            networkListener.onNetworkState(mNetworkState);
        }

        if (mNetworkListeners == null) {
            mNetworkListeners = new ArrayList<>();
        }

        mNetworkListeners.add(networkListener);
        if (mNetworkBroadcastReceiver == null) {
            mNetworkBroadcastReceiver = new NetworkBroadcastReceiver();
            registerReceiver(mNetworkBroadcastReceiver, mNetworkBroadcastReceiver.getIntentFilter());
            mNetworkBroadcastReceiver.setListener(networkState -> {
                mNetworkState = networkState;
                for (NetworkBroadcastReceiver.NetworkListener listener : mNetworkListeners) {
                    listener.onNetworkState(mNetworkState);
                }
            });
        }
    }

    public void removeStateChangeListener(NetworkBroadcastReceiver.NetworkListener networkListener) {
        if (mNetworkListeners == null) {
            mNetworkListeners = new ArrayList<>();
        }
        mNetworkListeners.remove(networkListener);
        if (mNetworkListeners.isEmpty() && mNetworkBroadcastReceiver != null) {
            unregisterReceiver(mNetworkBroadcastReceiver);
            mNetworkBroadcastReceiver = null;
        }
    }

    public CallingService.CallServiceBinder getCallServiceBinder() {
        return mCallServiceBinder;
    }

    public void startIncomingCall(ViewGroup rootViewOfActivity,
                                  IncomingCallDataHolder incomingCallData,
                                  boolean isIncomingCallFromNotification, CallingService.CallStateListener listener) {
        if (mCallServiceBinder != null) {
            mCallServiceBinder.startIncomingCall(
                    rootViewOfActivity,
                    incomingCallData,
                    isIncomingCallFromNotification,
                    listener);
        }
    }

    public void startOutGoingCall(ViewGroup rootViewOfActivity,
                                  AbsCallService.CallType callType,
                                  User callTo,
                                  UserGroup group,
                                  CallingService.CallStateListener listener) {
        if (mCallServiceBinder != null) {
            mCallServiceBinder.startOutGoingCall(
                    rootViewOfActivity,
                    callType,
                    callTo,
                    group,
                    listener);
        }
    }

    public void startJoinGroupCall(ViewGroup rootViewOfActivity,
                                   AbsCallService.CallType callType,
                                   UserGroup group,
                                   CallingService.CallStateListener listener) {
        if (mCallServiceBinder != null) {
            mCallServiceBinder.startJoinGroupCall(
                    rootViewOfActivity,
                    callType,
                    group,
                    listener);
        }
    }

    public void bindCall(OnCompleteListener<CallingService.CallServiceBinder> listener, boolean startServiceImmediately) {
        getApplicationContext()
                .bindService(new Intent(this, CallingService.class), new ServiceConnection() {
                    @Override
                    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                        Timber.i("onServiceConnected video call");
                        CallingService.CallServiceBinder binder = (CallingService.CallServiceBinder) iBinder;
                        binder.setChatBinder(mChatBinder);
                        mCallServiceBinder = binder;
                        if (startServiceImmediately) {
                            binder.start(SharedPrefUtils.getUserId(getApplicationContext()),
                                    null);
                        }
                        if (listener != null) {
                            listener.onComplete(binder);
                        }
                    }

                    @Override
                    public void onServiceDisconnected(ComponentName componentName) {
                        Timber.i("onServiceDisconnected video call");
                    }
                }, BIND_AUTO_CREATE);
    }

    public void showSessionDialog() {
        //Ignore checking at Splash screen
        if (!isIgnoreShowingSessionDialogScreen()) {
            mReceivedCommandToShowSessionDialogError = true;
            ((MainApplication) getApplication()).setReceivedCommandToShowSessionDialogError(true);
            runOnUiThread(() -> {
                Timber.i("Show showSessionDialog on " + getClass().getSimpleName());
                Fragment fr = getSupportFragmentManager().findFragmentByTag(SessionDialog.class.getSimpleName());
                if (fr != null) {
                    return;
                }
                Timber.i("Show session expire dialog.");
                SessionDialog dialog = SessionDialog.newInstance();
                dialog.setListener(() -> {
                    mReceivedCommandToShowSessionDialogError = false;
                    ((MainApplication) getApplication()).setReceivedCommandToShowSessionDialogError(false);
                });
                dialog.show(getSupportFragmentManager(), SessionDialog.class.getSimpleName());
            });
        }
    }

    public boolean isReceivedCommandToShowSessionDialogError() {
        return mReceivedCommandToShowSessionDialogError;
    }

    public boolean isSessionTimeOutDialogDisplaying() {
        Fragment fr = getSupportFragmentManager().findFragmentByTag(SessionDialog.class.getSimpleName());
        return fr != null && fr.isVisible();
    }

    private boolean isIgnoreShowingSessionDialogScreen() {
        return (this instanceof SplashActivity && !SharedPrefUtils.isLogin(this)) ||
                this instanceof LoginActivity ||
                this instanceof EmailSignUpActivity ||
                this instanceof ProfileSignUpActivity;
    }

    public void broadcastOneToOneCallEventMessage(AbsCallService.CallType callType,
                                                  User recipient,
                                                  boolean isMissedCall,
                                                  int calledDuration) {
        if (mChatBinder != null) {
            Timber.i("broadcastOneToOneCallEventMessage");
            mChatBinder.sendMessage(recipient,
                    ChatHelper.buildOneToOneCallEventMessage(this,
                            callType,
                            recipient,
                            isMissedCall,
                            calledDuration));
        }
    }

    public void onSwitchModeChange(boolean inInExpressMode) {

    }

    public AbsChatBinder getChatBinder() {
        return mChatBinder;
    }

    public boolean isCallInProgressNow() {
        return ((MainApplication) getApplication()).isCallInProgressNow();
    }

    public void stopAllServices() {
        if (!isCallInProgressNow()) {
            Timber.i("Will stopAllServices");
            if (mChatBinder != null) {
                mChatBinder.stopService();
            }
            if (mCallServiceBinder != null) {
                mCallServiceBinder.stop();
            }
        }
    }

    protected void onChatSocketServiceReady() {
        //Empty Impl...
    }

    public interface OnServiceListener {
        void onServiceReady(AbsChatBinder binder);
    }

    public static class ActivityLifeCycleListener {

        protected void onCreate() {
            //Empty Impl...
        }

        protected void onStart() {
            //Empty Impl...
        }

        protected void onStop() {
            //Empty Impl...
        }

        protected void onResume() {
            //Empty Impl...
        }

        protected void onPause() {
            //Empty Impl...
        }

        protected void onDestroy() {
            //Empty Impl...
        }
    }
}
