package com.proapp.sompom.newui.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ConciergeFilterProductCriteriaBottomSheetBinding;
import com.proapp.sompom.databinding.ListConciergeFilterProductCriteriaItemBinding;
import com.proapp.sompom.intent.ConciergeSupplierDetailIntent;
import com.proapp.sompom.model.concierge.ConciergeBrand;
import com.proapp.sompom.model.concierge.ConciergeSupplier;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeFilterProductCriteriaDataManager;
import com.proapp.sompom.viewmodel.ConciergeFilterProductCriteriaBottomSheetDialogViewModel;
import com.proapp.sompom.viewmodel.ConciergeFilterProductCriteriaOptionItemViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Chhom Veasna on 5/6/22.
 */

public class ConciergeFilterProductCriteriaBottomSheetDialog
        extends AbsBindingBottomSheetDialog<ConciergeFilterProductCriteriaBottomSheetBinding>
        implements ConciergeFilterProductCriteriaBottomSheetDialogViewModel.ConciergeFilterProductCriteriaBottomSheetDialogViewModelCallBack {

    public static final String CATEGORY_ID = "CATEGORY_ID";
    public static final String BRAND_IDS = "BRAND_IDS";

    private List<ConciergeFilterProductCriteriaOptionItemViewModel> mBrandCriteriaViewModel = new ArrayList<>();
    private ConciergeFilterProductCriteriaBottomSheetDialogListener mListener;

    @Inject
    public ApiService mApiService;
    private ConciergeFilterProductCriteriaBottomSheetDialogViewModel mViewModel;

    public static ConciergeFilterProductCriteriaBottomSheetDialog newInstance(String categoryId,
                                                                              ArrayList<String> brandIds,
                                                                              ConciergeSupplier supplier) {
        Bundle args = new Bundle();
        args.putString(CATEGORY_ID, categoryId);
        args.putSerializable(BRAND_IDS, brandIds);
        args.putParcelable(ConciergeSupplierDetailIntent.SUPPLIER, supplier);

        ConciergeFilterProductCriteriaBottomSheetDialog fragment = new ConciergeFilterProductCriteriaBottomSheetDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setListener(ConciergeFilterProductCriteriaBottomSheetDialogListener listener) {
        mListener = listener;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.concierge_filter_product_criteria_bottom_sheet;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().getAttributes().windowAnimations = R.style.AnimatedPopupStyle;
        }
        mViewModel = new ConciergeFilterProductCriteriaBottomSheetDialogViewModel(getContext(),
                new ConciergeFilterProductCriteriaDataManager(requireContext(),
                        mApiService,
                        getArguments().getString(CATEGORY_ID),
                        (List<String>) getArguments().getSerializable(BRAND_IDS),
                        getArguments().getParcelable(ConciergeSupplierDetailIntent.SUPPLIER)),
                this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onClearFilterCriteria() {
        if (mListener != null) {
            mListener.onClearFilterCriteria();
        }
        dismiss();
    }

    @Override
    public void onSaveFilterCriteria() {
        if (mListener != null) {
            mListener.onApplyFilterCriteria(mViewModel.getIsShowOutOfStock().get(), getSelectedBrandIds());
        }
        dismiss();
    }

    @Override
    public void onLoadCriteriaSuccess(List<ConciergeBrand> data) {
        bindCriteria(data);
    }

    private void bindCriteria(List<ConciergeBrand> data) {
        if (data != null && !data.isEmpty()) {
            for (ConciergeBrand brand : data) {
                ListConciergeFilterProductCriteriaItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(requireContext()),
                        R.layout.list_concierge_filter_product_criteria_item,
                        getBinding().flowLayout,
                        true);
                ConciergeFilterProductCriteriaOptionItemViewModel viewModel = new ConciergeFilterProductCriteriaOptionItemViewModel(requireContext(), brand);
                mBrandCriteriaViewModel.add(viewModel);
                binding.setVariable(BR.viewModel, viewModel);
            }
        }
    }

    private List<String> getSelectedBrandIds() {
        List<String> ids = new ArrayList<>();
        for (ConciergeFilterProductCriteriaOptionItemViewModel viewModel : mBrandCriteriaViewModel) {
            if (viewModel.getIsSelected().get()) {
                ids.add(viewModel.getConciergeBrand().getId());
            }
        }

        return ids;
    }

    public interface ConciergeFilterProductCriteriaBottomSheetDialogListener {
        void onClearFilterCriteria();

        void onApplyFilterCriteria(boolean isShowOutOfStockProduct, List<String> ids);
    }
}
