package com.proapp.sompom.newui.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.DialogBaseBinding;

/**
 * Created by He Rotha on 10/25/17.
 */

public abstract class AbsBasePlayVideFullScreenFragmentDialog extends DialogFragment {
    public static final String TAG = AbsBasePlayVideFullScreenFragmentDialog.class.getName();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (requireDialog().getWindow() != null) {
            requireDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (requireDialog().getWindow() != null) {
            requireDialog()
                    .getWindow()
                    .setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.MATCH_PARENT);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        requireDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        DialogBaseBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_base, container, false);
        binding.layoutContainer.removeAllViews();
        binding.layoutContainer.addView(onCreateView(inflater, container));
        binding.root.setOnClickListener(v -> {
            if (isCancelable()) {
                dismiss();
            }
        });
        return binding.getRoot();
    }

    public abstract View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container);
}
