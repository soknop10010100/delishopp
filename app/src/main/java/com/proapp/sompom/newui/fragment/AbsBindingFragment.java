package com.proapp.sompom.newui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.newui.AbsBindingActivity;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import timber.log.Timber;

/**
 * Created by He Rotha on 10/20/17.
 */

public abstract class AbsBindingFragment<T extends ViewDataBinding> extends AbsBaseFragment
        implements AbsBindingActivity.GroupUpdateListener {

    private T mBinding;
    private AbsBaseViewModel mViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
        return getBinding().getRoot();
    }

    public T getBinding() {
        return mBinding;
    }

    @LayoutRes
    public abstract int getLayoutRes();

    public void setVariable(int id, Object value) {
        if (value instanceof AbsBaseViewModel) {
            mViewModel = (AbsBaseViewModel) value;
            mViewModel.setAbsBaseFragment(this);
        }
        if (mViewModel instanceof AbsLoadingViewModel) {
            ((AbsLoadingViewModel) mViewModel).mIsLoading.setOnValueSetListener(isSet -> {
                if (isSet) {
                    hideKeyboard();
                }
            });
        }
        getBinding().setVariable(id, value);
    }

    public boolean isAddedToActivity() {
        return isAdded();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isAdded()) {
            if (mViewModel != null) {
                mViewModel.onResume();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mViewModel != null) {
            mViewModel.onPause();
        }
    }

    @Override
    public void onDestroy() {
        if (mViewModel != null) {
            mViewModel.onDestroy();
        }
        super.onDestroy();

        //Remove group update callback.
        if (enableGroupUpdateCallback() && requireActivity() instanceof AbsBindingActivity) {
            ((AbsBindingActivity) requireActivity()).removeGroupUpdateListener(this);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mViewModel != null) {
            mViewModel.onCreate();
        }

        //Register group update callback
        if (enableGroupUpdateCallback() && requireActivity() instanceof AbsBindingActivity) {
            ((AbsBindingActivity) requireActivity()).addGroupUpdateListener(this);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mViewModel != null) {
            mViewModel.onActivityResult(requestCode, resultCode, data);
        }
    }

    //Will be overwritten by it sub classes.
    protected boolean enableGroupUpdateCallback() {
        return false;
    }

    //Will be overwritten by sub classes.
    @Override
    public void onGroupUpdated(String conversationId, Conversation conversation, boolean isRemovedMe) {
        //Do nothing...
    }
}
