package com.proapp.sompom.newui.dialog;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.DialogInputPasswordBinding;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.viewmodel.newviewmodel.InputPasswordDialogViewModel;

import timber.log.Timber;

public class InputPasswordTextDialog extends MessageDialog implements InputPasswordDialogViewModel.InputPasswordDialogListener {
    private EditText mEditText;
    private DialogInputPasswordBinding mBinding;
    private InputPasswordDialogViewModel mViewModel;
    private InputPasswordDialogListener mListener;
    private String mWarningText;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setDismissWhenClickOnButtonLeft(false);

        final LayoutInflater inflater = LayoutInflater.from(getContext());
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.dialog_input_password,
                null,
                true);
        mEditText = mBinding.inputText;

        setDismissWhenClickOnButtonLeft(false);
        mViewModel = new InputPasswordDialogViewModel(getContext(), this);

        mViewModel.setWarningText(mWarningText);

        mBinding.setVariable(BR.viewModel, mViewModel);

        getBinding().textviewTitle.setTypeface(null, Typeface.BOLD);
        getBinding().view.addView(mBinding.getRoot());
//        mBinding.clearInput.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mViewModel.clearInput();
//            }
//        });

        super.onViewCreated(view, savedInstanceState);
        view.postDelayed(() -> {
            mBinding.inputText.requestFocus();
            KeyboardUtil.showKeyboard(requireActivity(), mBinding.inputText);
        }, 100);
    }

    public void getPassword() {
        if (mViewModel.isReadyToProceed()) {
            Timber.e("isReadyToProceed");
            mViewModel.requestPassword();
        } else {
            Timber.e("notReady");
        }
    }

    public void setWarningText(String warning) {
        mWarningText = warning;
    }

    public void showError(String warningText) {
        mViewModel.setWarningText(warningText);
        getBinding().getRoot().postDelayed(() -> {
            mBinding.inputText.requestFocus();
            KeyboardUtil.showKeyboard(requireActivity(), mBinding.inputText);
        }, 100);
    }

    @Override
    public void onPasswordEnterSuccess(String password) {
        if (mListener != null) {
            mListener.onPasswordEnterSuccess(password);
        }
    }

    public void setListener(InputPasswordDialogListener listener) {
        mListener = listener;
    }

    public void setIsLoading(boolean isLoading) {
        if (isLoading) {
            KeyboardUtil.hideKeyboard(requireActivity());
            mViewModel.showLoading(false);
        } else {
            mViewModel.hideLoading();
        }
    }

    public interface InputPasswordDialogListener {
        void onPasswordEnterSuccess(String password);
    }
}
