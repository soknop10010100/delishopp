package com.proapp.sompom.newui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.newui.fragment.AbsLoginFragment;
import com.proapp.sompom.utils.SharedPrefUtils;

import timber.log.Timber;

public abstract class SupportResetPasswordDeepLinkActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Timber.i("onCreate");
        //If there is deep linking invoke and user already logged in, we just then ignore it.
        if (SharedPrefUtils.isLogin(this) && !ApplicationHelper.isInVisitorMode(this)) {
            finish();
        }
        super.onCreate(savedInstanceState);
        setFragment(getFragment(), getFragmentTag());
        new Handler().postDelayed(() -> parseDeepLinkingData(getIntent()), 100);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Timber.i("onNewIntent");
        parseDeepLinkingData(intent);
    }

    private void parseDeepLinkingData(Intent appLinkIntent) {
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
//        Timber.i("appLinkAction: " + appLinkAction + ", appLinkData: " + (appLinkData != null ? appLinkData.toString() : null));
//        if (appLinkData != null) {
//            Timber.i("Paths: " + new Gson().toJson(appLinkData.getPathSegments()) + ", Last: " + appLinkData.getLastPathSegment());
//        }
        if (!TextUtils.isEmpty(appLinkAction) && appLinkData != null && !TextUtils.isEmpty(appLinkData.getLastPathSegment())) {
            String token = appLinkData.getLastPathSegment();
            Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(getFragmentTag());
//            Timber.i("Token: " + token + ", fragmentByTag: " + fragmentByTag);
            if (fragmentByTag instanceof AbsLoginFragment) {
                ((AbsLoginFragment) fragmentByTag).handleResetPasswordDeepLinking(token);
            }
        }
    }

    protected abstract Fragment getFragment();

    protected abstract String getFragmentTag();
}
