package com.proapp.sompom.newui.fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.TimelineDetailAdapter;
import com.proapp.sompom.broadcast.upload.FloatingVideoService;
import com.proapp.sompom.databinding.FragmentTimelineDetailBinding;
import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.helper.OpenCommentDialogHelper;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.intent.CreateTimelineIntent;
import com.proapp.sompom.intent.CreateTimelineIntentResult;
import com.proapp.sompom.intent.newintent.TimelineDetailIntent;
import com.proapp.sompom.licence.db.LicenseSynchronizationDb;
import com.proapp.sompom.licence.model.SynchroniseData;
import com.proapp.sompom.listener.LoadMoreCommentDirectionCallback;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnCommentItemClickListener;
import com.proapp.sompom.listener.OnDetailShareItemCallback;
import com.proapp.sompom.listener.OnEditTextCommentListener;
import com.proapp.sompom.listener.OnFloatingVideoClickedListener;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.LoadCommentDirection;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.SharedProduct;
import com.proapp.sompom.model.SharedTimeline;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.AskFloatingWindowPermissionForType;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.emun.CreateWallStreetScreenType;
import com.proapp.sompom.model.emun.FollowItemType;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.emun.PostContextMenuItemType;
import com.proapp.sompom.model.emun.TimelineDetailRedirectionType;
import com.proapp.sompom.model.emun.TimelinePostItem;
import com.proapp.sompom.model.notification.NotificationListAndRedirectionHelper;
import com.proapp.sompom.model.notification.RedirectionNotificationData;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.model.result.ContentStat;
import com.proapp.sompom.model.result.JumpCommentResponse;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.TimelineDetailActivity;
import com.proapp.sompom.newui.dialog.AskFloatingWindowPermissionDialog;
import com.proapp.sompom.newui.dialog.BigPlayerFragment;
import com.proapp.sompom.newui.dialog.CommentDialog;
import com.proapp.sompom.newui.dialog.ListFollowDialog;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.newui.dialog.PostContextMenuDialog;
import com.proapp.sompom.newui.dialog.ShareLinkMenuDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.TimelineDetailDataManager;
import com.proapp.sompom.utils.CopyTextUtil;
import com.proapp.sompom.utils.NetworkStateUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.TimelinePostItemUtil;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemTimelineHeaderViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.TimelineDetailViewModel;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.videomanager.widget.MediaRecyclerView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 7/12/18.
 */

public class TimelineDetailFragment extends AbsBindingFragment<FragmentTimelineDetailBinding>
        implements OnFloatingVideoClickedListener,
        BigPlayerFragment.Callback,
        OnEditTextCommentListener,
        OnCommentItemClickListener,
        OnTimelineItemButtonClickListener,
        TimelineDetailAdapter.TimelineDetailAdapterListener {

    private static final String FLURRY_EVENT = "post_detail";

    @Inject
    public ApiService mApiService;
    private WallStreetAdaptive mWallStreetAdaptive;
    private TimelineDetailViewModel mViewModel;
    private TimelineDetailAdapter mAdapter;

    private int mMediaPosition;
    private boolean mIsRemoveItem;
    private boolean mIsGrantPermissionFloatingVideoFirstTime;
    private boolean mIsShowCommentForSingleItemPost; //Will be true if a post contain only single item which can be text or media
    private OpenCommentDialogHelper mCommentDialog;
    private boolean mCheckLikeComment;
    private String mPostId;
    private RedirectionNotificationData mRedirectionNotificationData;

    public static TimelineDetailFragment newInstance(WallStreetAdaptive adaptive,
                                                     TimelineDetailRedirectionType redirectionType,
                                                     int clickPosition,
                                                     String wallStreetId) {
        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.DATA, adaptive);
        args.putInt(SharedPrefUtils.ID, clickPosition);
        args.putString(SharedPrefUtils.STATUS, wallStreetId);
        args.putString(TimelineDetailIntent.REDIRECTION_TYPE, redirectionType.getValue());
        TimelineDetailFragment fragment = new TimelineDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static TimelineDetailFragment newInstanceForNotificationRedirection(RedirectionNotificationData data) {
        Bundle args = new Bundle();
        args.putParcelable(NotificationListAndRedirectionHelper.NOTIFICATION_EXCHANGE_DATA, data);
        if (data != null) {
            args.putString(SharedPrefUtils.STATUS, data.getPostId());
        }
        TimelineDetailFragment fragment = new TimelineDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_timeline_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        SynchroniseData synchroniseData = LicenseSynchronizationDb.readSynchroniseData(getContext());
        boolean checkLikeComment = false;
        if (synchroniseData != null && synchroniseData.getNewsFeed() != null) {
            checkLikeComment = synchroniseData.getNewsFeed().isEnableLikeComment();
        }
        mCheckLikeComment = checkLikeComment;
        if (getArguments() != null) {
            mRedirectionNotificationData = getArguments().getParcelable(NotificationListAndRedirectionHelper.NOTIFICATION_EXCHANGE_DATA);
            mWallStreetAdaptive = getArguments().getParcelable(SharedPrefUtils.DATA);
            mMediaPosition = getArguments().getInt(SharedPrefUtils.ID);
            mPostId = getArguments().getString(SharedPrefUtils.STATUS);
        }

        TimelineDetailDataManager dataManager = new TimelineDetailDataManager(getActivity(),
                mApiService,
                mRedirectionNotificationData);
        mViewModel = new TimelineDetailViewModel((AbsBaseActivity) getActivity(),
                mCheckLikeComment,
                mPostId,
                dataManager,
                mWallStreetAdaptive,
                new TimelineDetailViewModel.Callback() {
                    @Override
                    public void onFollowButtonClick(String id, boolean isFollowing) {
                        checkUserFollow(isFollowing, false);
                        mViewModel.checkToPostFollow(id, isFollowing);
                    }

                    @Override
                    public void onShowPostContextMenuClick(Adaptive adaptive, User user, int position) {
                        showPostContextMenuClick(adaptive, user);
                    }

                    @Override
                    public void onCommentClick(Adaptive adaptive, boolean autoDisplayKeyboard) {
                        Timber.i("onCommentClick: " + adaptive.getClass().getSimpleName());
                    }

                    @Override
                    public MediaRecyclerView getRecyclerView() {
                        return getBinding().recyclerView;
                    }

                    @Override
                    public void onShowAdapter(List<Adaptive> adaptiveList, boolean isShowEditText) {
                        mIsShowCommentForSingleItemPost = isShowEditText;
                        if (mIsShowCommentForSingleItemPost) {
                            bindGifView();
                        }
                        initAdapter(adaptiveList);
                    }

                    @Override
                    public void onLoadCommentSuccess(List<Comment> commentList,
                                                     boolean isInJumpMode,
                                                     boolean isCanLoadPrevious,
                                                     boolean isCanLoadNext,
                                                     boolean isFromResetJumpMode) {
                        Timber.i("onLoadCommentSuccess: commentList: " + commentList.size()
                                + ", isInJumpMode: " + isInJumpMode
                                + ", isCanLoadPrevious: " + isCanLoadPrevious
                                + ", isCanLoadNext: " + isCanLoadNext
                                + ", isFromResetJumpMode: " + isFromResetJumpMode);
                        List<Adaptive> adaptiveList = new ArrayList<>();
                        mAdapter.setCanLoadNext(isCanLoadNext);
                        if (isCanLoadPrevious) {
                            adaptiveList.add(new LoadCommentDirection(JumpCommentResponse.REDIRECTION_PREVIOUS));
                        }
                        adaptiveList.addAll(commentList);
                        if (isCanLoadNext) {
                            adaptiveList.add(new LoadCommentDirection(JumpCommentResponse.REDIRECTION_NEXT));
                        }
                        mAdapter.addCommentList(adaptiveList, isFromResetJumpMode);
                        if (isInJumpMode) {
                            checkToMakeBouncingAnimationForRedirectionComment();
                        }
                        //Request to update unread comment counter
                        WallStreetHelper.requestToUpdateUnreadCommentCounter(requireContext(),
                                mApiService,
                                mAdapter,
                                mAdapter.getDatas().get(0));
                    }

                    @Override
                    public void onGetPostDetailSuccess(WallStreetAdaptive adaptive) {
                        mWallStreetAdaptive = adaptive;
                        if (mWallStreetAdaptive instanceof LifeStream) {
                            ((LifeStream) mWallStreetAdaptive).checkUpdateBackupOfMappingFields();
                        }
                    }

                    @Override
                    public void onRemoveCommentSuccess(int removeCount) {
                        checkToUpdateCommentCounterForSingleItemPost(false, removeCount);
                    }
                },
                this);
        setVariable(BR.viewModel, mViewModel);
    }


    private void checkToMakeBouncingAnimationForRedirectionComment() {
        if (mViewModel.getDataManager().isInJumpCommentMode() &&
                !TextUtils.isEmpty(mViewModel.getDataManager().getRedirectionNotificationData().getCommentId()) &&
                getBinding().recyclerView.getLayoutManager() != null &&
                mAdapter != null &&
                mAdapter.getItemCount() > 0) {
            int itemPosition = mAdapter.findItemPosition(mViewModel.getDataManager().getRedirectionNotificationData().getCommentId());
            new Handler().postDelayed(() -> {
                if (itemPosition >= 0) {
                    getBinding().recyclerView.smoothScrollToPosition(itemPosition);
                    new Handler().postDelayed(() -> {
                                View viewByPosition = getBinding().recyclerView.getLayoutManager().findViewByPosition(itemPosition);
                                if (viewByPosition != null) {
                                    AnimationHelper.animateBounce(viewByPosition, null);
                                }
                            },
                            NotificationListAndRedirectionHelper.ANIMATION_DELAY);
                } else {
                    NotificationListAndRedirectionHelper.showRedirectionNotificationContentNotFound((AppCompatActivity) requireActivity());
                }
            }, NotificationListAndRedirectionHelper.SCROLL_DELAY);
        }
    }

    private String getValidPostId() {
        if (mWallStreetAdaptive != null) {
            return mWallStreetAdaptive.getId();
        }

        return mPostId;
    }

    @Override
    public void onPauseMediaView() {
        getBinding().recyclerView.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        getBinding().recyclerView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().recyclerView.onPause();
        getBinding().includeComment.messageInput.removeListPopupWindow();
    }

    private boolean isFloatingVideoPlaying() {
        if (getActivity() instanceof AbsBaseActivity) {
            FloatingVideoService.FloatingVideoBinder floatingVideoService = ((AbsBaseActivity) getActivity()).getFloatingVideo();
            return floatingVideoService != null && floatingVideoService.isFloating();
        }
        return false;
    }

    private void setOnFloatingVideoRemoveListener() {
        if (getActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getActivity()).setOnFloatingVideoRemoveListener(this::resumeVideo);
        }
    }

    private void resumeVideo() {
        getBinding().recyclerView.setAutoPlay(true);
        getBinding().recyclerView.resumeAfterRecycle();
    }

    private void checkToResumeAudioPlayVideo() {
        boolean isPlaying = isFloatingVideoPlaying();
        if (isPlaying) {
            getBinding().recyclerView.setAutoPlay(false);
            setOnFloatingVideoRemoveListener();
        } else {
            resumeVideo();
        }
    }

    private void initAdapter(List<Adaptive> adaptiveList) {
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getBinding().recyclerView.requestAudioFocus();
        mAdapter = new TimelineDetailAdapter((AbsBaseActivity) requireActivity(),
                mWallStreetAdaptive,
                mCheckLikeComment,
                mApiService,
                adaptiveList,
                this,
                this,
                this,
                new OnDetailShareItemCallback() {
                    @Override
                    public void onMediaClick(WallStreetAdaptive adaptive, int position) {
                        if (getActivity() != null) {
                            if (adaptive instanceof LifeStream) {
                                ((AbsBaseActivity) getActivity()).startActivityForResult(new TimelineDetailIntent(getContext(),
                                                adaptive,
                                                position,
                                                TimelineDetailRedirectionType.FROM_WALL),
                                        new ResultCallback() {
                                            @Override
                                            public void onActivityResultSuccess(int resultCode, Intent data) {
                                                checkToResumeAudioPlayVideo();
                                            }
                                        });
                            }
                        }
                    }

                    @Override
                    public void onClick() {
                        //Click like from item
                    }
                },
                (loadPreviousComment, position) -> mViewModel.loadMoreCommentList(loadPreviousComment.getRedirection(),
                        new LoadMoreCommentDirectionCallback() {
                            @Override
                            public void onFail(ErrorThrowable ex, boolean isInJumpMode, boolean isLoadPrevious, boolean isCanLoadNext) {
                                OpenCommentDialogHelper.onLoadMoreCommentFinish(new ArrayList<>(),
                                        isInJumpMode,
                                        isLoadPrevious,
                                        isCanLoadNext,
                                        loadPreviousComment,
                                        position,
                                        mAdapter);
                            }

                            @Override
                            public void onLoadCommentDirectionSuccess(List<Comment> listComment,
                                                                      boolean isInJumpMode,
                                                                      boolean isLoadPrevious,
                                                                      boolean isCanLoadNext) {
                                OpenCommentDialogHelper.onLoadMoreCommentFinish(listComment,
                                        isInJumpMode,
                                        isLoadPrevious,
                                        isCanLoadNext,
                                        loadPreviousComment,
                                        position,
                                        mAdapter);
                            }
                        }),
                this,
                mRedirectionNotificationData != null,
                mIsShowCommentForSingleItemPost);
        getBinding().recyclerView.setAdapter(mAdapter);
        getBinding().recyclerView.scrollToPosition(getScrollToMediaPosition());
        getBinding().recyclerView.setOnTouchListener((v, e) -> {
            if (getContext() != null) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            v.performClick();
            return false;
        });
    }

    private int getScrollToMediaPosition() {
        /*
          {@link mMediaPosition != - 1} true need to scroll to media position
         */
        Timber.i("mMediaPosition: " + mMediaPosition);
        if (mMediaPosition != ListItemTimelineHeaderViewModel.CLICK_ON_TEXT_POSITION) {
            if (mWallStreetAdaptive != null && !TextUtils.isEmpty(mWallStreetAdaptive.getDescription())) {
                //Since post's text is also considered as a post item in adaptive, so the clicked media
                //position need to increase one more to make scroll properly in this detail post.
                return mMediaPosition + 1;
            } else {
                return mMediaPosition;
            }
        } else {
            return 0;
        }
    }

    public WallStreetAdaptive getWallStreetAdaptive() {
        return mWallStreetAdaptive;
    }

    public boolean isRemoveItem() {
        return mIsRemoveItem;
    }

    @Override
    public void onFloatingVideo(int position, long time) {
        mMediaPosition = position;
        mWallStreetAdaptive.getMedia().get(position).setTime(time);
        requestPermission();
    }

    @Override
    public void onFullScreenClicked(int position, long time) {
        getBinding().recyclerView.pause();
        mWallStreetAdaptive.getMedia().get(position).setTime(time);
        BigPlayerFragment playerFragment =
                BigPlayerFragment.newInstance(position,
                        mWallStreetAdaptive.getMedia().get(position),
                        false);
        playerFragment.show(getChildFragmentManager(), BigPlayerFragment.TAG);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AskFloatingWindowPermissionDialog.OVERLAY_PERMISSION_REQUEST_CODE && Settings.canDrawOverlays(getActivity())) {
            //Start floating video
            mIsGrantPermissionFloatingVideoFirstTime = true;
            popupVideo();
        }
    }

    private void requestPermission() {
        // SYSTEM_ALERT_WINDOW permission need to request run-time with android Marshmallow and above
        if (getActivity() != null
                && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && !Settings.canDrawOverlays(getActivity())) {
            AskFloatingWindowPermissionDialog dialog = AskFloatingWindowPermissionDialog.newInstance(AskFloatingWindowPermissionForType.VIDEO);
            dialog.show(requireActivity().getSupportFragmentManager());
        } else {
            popupVideo();
        }
    }

    private void popupVideo() {
        FloatingVideoService.startFloating(getActivity(), (LifeStream) mWallStreetAdaptive, mMediaPosition);
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    @Override
    public void onBigPlayerDestroyed(int position, boolean isAutoPlay, Media media) {
        if (isAutoPlay) {
            getBinding().recyclerView.resume();
        }
    }

    public boolean isGrantPermissionFloatingVideoFirstTime() {
        return mIsGrantPermissionFloatingVideoFirstTime;
    }

    @Override
    public void onSendText(Comment comment) {
        if (NetworkStateUtil.isNetworkAvailable(requireContext())) {
            if (mAdapter != null) {
                hideKeyboard();
                mAdapter.addLatestComment(comment);
                getBinding().recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
            }
        }
    }

    @Override
    public void onPostCommentFail() {
        if (mAdapter != null) {
            mAdapter.removeInstanceComment();
        }
    }

    @Override
    public void onPostCommentSuccess(Comment comment) {
        Timber.i("onPostCommentSuccess");
        mAdapter.notifyItemChanged(mAdapter.getItemCount() - 1, comment);
        checkToUpdateCommentCounterForSingleItemPost(true, 1);
    }

    private void checkToUpdateCommentCounterForSingleItemPost(boolean isAdded, int number) {
        /*
        Will update the single post item comment counter and the post's item should be on the top
        of the adapter.
         */
        if (mViewModel.isSinglePostItem() && (mAdapter.getDatas().get(0) instanceof LifeStream ||
                mAdapter.getDatas().get(0) instanceof Media)) {
            WallStreetAdaptive adaptive = (WallStreetAdaptive) mAdapter.getDatas().get(0);
            if (adaptive.getContentStat() != null) {
                long newValue = adaptive.getContentStat().getTotalComments() + (isAdded ? number : (-number));
                Timber.i("checkToUpdateCommentCounterForSingleItemPost: isAdded: " + isAdded +
                        ", value: " + number +
                        ", Old total: " + adaptive.getContentStat().getTotalComments() +
                        ", Update total: " + newValue);
                if (newValue < 0) {
                    newValue = 0;
                }
                adaptive.getContentStat().setTotalComments(newValue);
                mAdapter.updateCommentCounter((Adaptive) adaptive);
                WallStreetHelper.requestToUpdateUnreadCommentCounter(requireContext(),
                        mApiService,
                        mAdapter,
                        (Adaptive) adaptive);
            }
        }
    }

    private void checkToUpdateCommentCounterForSingleItemPost(long updateTotalCommentCount) {
        if (mViewModel.isSinglePostItem() && (mAdapter.getDatas().get(0) instanceof LifeStream ||
                mAdapter.getDatas().get(0) instanceof Media)) {
            WallStreetAdaptive adaptive = (WallStreetAdaptive) mAdapter.getDatas().get(0);
            if (adaptive.getContentStat() != null) {
                adaptive.getContentStat().setTotalComments(updateTotalCommentCount);
                mAdapter.updateCommentCounter((Adaptive) adaptive);
            }
        }
    }

    @Override
    public void onUpdateCommentSuccess(Comment comment, int position) {
        hideKeyboard();
        mAdapter.notifyItemChanged(position, comment);
    }

    @Override
    public void onReplyButtonClick(Comment comment, int position) {
        openReplayCommentScreen(comment, position, true);
    }

    private void openReplayCommentScreen(Comment comment, int position, boolean isAutoShowKeyboard) {
        if (mCommentDialog == null) {
            mCommentDialog = new OpenCommentDialogHelper(getActivity());
        }
        //Normally the first item is either directly post or media that acts as post when click from
        //notification
        mCommentDialog.showReplyComment(requireContext(),
                mApiService,
                mWallStreetAdaptive.getId(),
                (WallStreetAdaptive) mAdapter.getDatas().get(0),
                mAdapter.getDatas().get(0),
                comment,
                isAutoShowKeyboard,
                mAdapter,
                new CommentDialog.OnCloseReplyCommentFragmentListener() {
                    @Override
                    public void onMainCommentItemRemoved(int totalRemoveCommentCount) {
                        mAdapter.notifyItemRemove(position);
                        checkToUpdateCommentCounterForSingleItemPost(false, totalRemoveCommentCount);
                    }

                    @Override
                    public void onComplete(Comment result) {
                        mAdapter.notifyItemChanged(position, result);
                    }

                    @Override
                    public void onUpdateMainCommentCount(long totalComment) {
                        checkToUpdateCommentCounterForSingleItemPost(totalComment);
                    }
                });
    }

    @Override
    public void onLikeButtonClick(Comment comment, int position, OnCallbackListener<Response<Object>> listener) {
        mViewModel.checkToLikeComment(comment, listener);
    }

    @Override
    public void onLongPress(Comment comment, int position) {
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        boolean isOwner = comment.getUser() != null && SharedPrefUtils.checkIsMe(getActivity(), comment.getUser().getId());
        dialog.addItem(PostContextMenuItemType.getCommentPopupMenuItem(comment, isOwner));
        dialog.setOnItemClickListener(result -> {
            dialog.dismiss();
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.COPY_TEXT_CLICK) {
                CopyTextUtil.copyTextToClipboard(getActivity(), comment.getContent());
            } else if (itemType == PostContextMenuItemType.EDIT_POST_CLICK) {
                getBinding().includeComment.messageInput.post(() -> showKeyboard(getBinding().includeComment.messageInput.getMentionsEditText()));
                getBinding().includeComment.messageInput.setText(comment.getContent(), false);
                mViewModel.setUpdateComment(comment, position);

            } else {
                MessageDialog messageDialog = new MessageDialog();
                messageDialog.setTitle(getString(R.string.comment_popup_confirm_delete_title));
                messageDialog.setMessage(getString(R.string.comment_popup_confirm_delete_description));
                messageDialog.setRightText(getString(R.string.setting_change_language_dialog_cancel_button), null);
                messageDialog.setLeftText(getString(R.string.seller_store_button_delete),
                        v -> mViewModel.onDeleteComment(comment, () -> mAdapter.notifyItemRemove(position)));
                messageDialog.show(getChildFragmentManager());
            }
        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    private void checkUserFollow(boolean isFollowing, boolean isSubWallStreet) {
        if (mWallStreetAdaptive instanceof SharedTimeline || mWallStreetAdaptive instanceof SharedProduct) {
            if (isSubWallStreet) {
                if (mWallStreetAdaptive instanceof SharedTimeline) {
                    ((SharedTimeline) mWallStreetAdaptive).getLifeStream().getUser().setFollow(isFollowing);
                } else {
                    ((SharedProduct) mWallStreetAdaptive).getProduct().getUser().setFollow(isFollowing);
                }
            } else {
                mWallStreetAdaptive.getUser().setFollow(isFollowing);
            }
        } else {
            ((LifeStream) mWallStreetAdaptive).getStoreUser().setFollow(isFollowing);
        }
    }

    private void bindGifView() {
        getBinding().includeComment.containerSearchGif.onBackPressClickListener(v -> {
            getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
            getBinding().includeComment.messageInput.requestFocus();
        });

        getBinding().includeComment.gifImageView.setOnClickListener(v -> {
            getBinding().includeComment.sendLayout.setVisibility(View.GONE);
            getBinding().includeComment.containerSearchGif.loadTrendingGifList(baseGifModel -> {
                getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
                getBinding().includeComment.messageInput.requestFocus();
                if (getActivity() != null) {
                    ((AbsBaseActivity) getActivity()).startLoginWizardActivity(new LoginActivityResultCallback() {
                        @Override
                        public void onActivityResultSuccess(boolean isAlreadyLogin) {
                            Media media = new Media();
                            media.setWidth(baseGifModel.getGifWidth());
                            media.setHeight(baseGifModel.getGifHeight());
                            media.setUrl(baseGifModel.getGifUrl());
                            media.setType(MediaType.TENOR_GIF);
                            Comment comment = new Comment();
                            comment.setContent("gif");
                            comment.setUser(SharedPrefUtils.getUser(getContext()));
                            comment.setDate(new Date());
                            comment.setPostId(mWallStreetAdaptive.getId());
                            comment.setContentType(ContentType.POST.getValue());
                            comment.setMedia(media);
                            mViewModel.postComment(comment);

                            hideKeyboard();
                            mAdapter.addLatestComment(comment);
                            getBinding().recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);

                        }
                    });
                }
            });
        });
    }

    public boolean isCloseSearchGifView() {
        if (getBinding().includeComment.containerSearchGif.getVisibility() == View.VISIBLE) {
            getBinding().includeComment.containerSearchGif.clearData();
            getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
            getBinding().includeComment.messageInput.requestFocus();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onFollowButtonClick(String id, boolean isFollowing) {
        //Follow in Shared Timeline Item
        checkUserFollow(isFollowing, true);
        mViewModel.checkToPostFollow(id, isFollowing);
    }

    @Override
    public void onShowPostContextMenuClick(Adaptive adaptive, User user, int position) {
        //Context Men in Shared Timeline Item
        showPostContextMenuClick(adaptive, user);
    }

    @Override
    public void onCommentClick(Adaptive adaptive, boolean autoDisplayKeyboard) {
        Timber.i("onCommentClick: " + adaptive.getClass().getSimpleName());
        if (mIsShowCommentForSingleItemPost) {
            showKeyboard(getBinding().includeComment.messageInput.getMentionsEditText());
        } else {
            openCommentDialog(adaptive, autoDisplayKeyboard);
        }
    }

    private void openCommentDialog(Adaptive adaptive, boolean autoDisplayKeyboard) {
        if (mCommentDialog == null) {
            mCommentDialog = new OpenCommentDialogHelper(getActivity());
        }
        mCommentDialog.show(requireContext(),
                mApiService,
                getValidPostId(),
                adaptive,
                mAdapter,
                autoDisplayKeyboard);
    }

    @Override
    public MediaRecyclerView getRecyclerView() {
        return getBinding().recyclerView;
    }

    @Override
    public void onCommentButtonClick(Adaptive adaptive, int position) {
        if (mIsShowCommentForSingleItemPost) {
            showKeyboard(getBinding().includeComment.messageInput.getMentionsEditText());
        } else {
            if (mCommentDialog == null) {
                mCommentDialog = new OpenCommentDialogHelper(getActivity());
            }
            openCommentDialog(adaptive, true);
        }
    }

    @Override
    public void onShareButtonClick(Adaptive adaptive, int position) {
        ShareLinkMenuDialog dialog = ShareLinkMenuDialog.newInstance(mWallStreetAdaptive);
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onUserViewMediaClick(Adaptive adaptive) {
        ListFollowDialog dialog = ListFollowDialog.newInstance(adaptive, FollowItemType.USER_ACTION);
        dialog.setPostContainOnlyItem(mViewModel.isSinglePostItem());
        dialog.setPostId(getValidPostId());
        dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
    }

    @Override
    public void onLikeViewerClick(Adaptive adaptive) {
        ListFollowDialog dialog = ListFollowDialog.newInstance(adaptive, FollowItemType.LIKE_VIEWER);
        dialog.setPostContainOnlyItem(mViewModel.isSinglePostItem());
        dialog.setPostId(getValidPostId());
        dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
    }

    @Override
    public void onShowSnackBar(boolean isError, String message) {

    }

    private void showPostContextMenuClick(Adaptive adaptive, User user) {
        if (user == null) {
            return;
        }

        TimelinePostItem timelinePostItem = TimelinePostItemUtil.getPostItem(adaptive);
        mWallStreetAdaptive = (WallStreetAdaptive) adaptive;
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        dialog.addItem(PostContextMenuItemType.getPostMenuItems(requireContext(),
                SharedPrefUtils.checkIsMe(requireContext(), user.getId()), timelinePostItem));
        dialog.setOnItemClickListener(result -> {
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.EDIT_POST_CLICK) {
                if (mWallStreetAdaptive != null
                        && getActivity() != null
                        && getActivity() instanceof AbsBaseActivity) {
                    CreateTimelineIntent intent = new CreateTimelineIntent(getActivity(),
                            mWallStreetAdaptive,
                            CreateWallStreetScreenType.EDIT_POST_FROM_POST_DETAIL);
                    ((AbsBaseActivity) getActivity()).startActivityForResult(intent, new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            if (!mViewModel.getDataManager().isInJumpCommentMode()) {
                                CreateTimelineIntentResult result1 = new CreateTimelineIntentResult(data);
                                if (result1.getLifeStream() != null) {
                                    //Need to backup like and comment count of current post
                                    ContentStat contentStat = mWallStreetAdaptive.getContentStat();
                                    mWallStreetAdaptive = result1.getLifeStream();
                                    mWallStreetAdaptive.setContentStat(contentStat);
                                    mViewModel.setPostData((LifeStream) mWallStreetAdaptive);
                                }
                            }
                        }
                    });
                }
            } else if (itemType == PostContextMenuItemType.UNFOLLOW_CLICK) {
                mViewModel.checkToPostFollow(user.getId(), false);
                if (adaptive != null && ((WallStreetAdaptive) adaptive).getUser() != null) {
                    ((WallStreetAdaptive) adaptive).getUser().setFollow(false);
                    mAdapter.update(adaptive);
                }
            } else if (itemType == PostContextMenuItemType.EDIT_PRIVACY_CLICK) {
                showEditPrivacyDialog(result1 -> {
                    //TODO: implement api edit privacy
                });
            } else if (itemType == PostContextMenuItemType.TURNOFF_NOTIFICATION_CLICK) {
                mViewModel.unFollowShop(adaptive.getId());
            } else if (itemType == PostContextMenuItemType.DELETE_CLICK) {
                if (mWallStreetAdaptive != null) {
                    showMessageDialog(R.string.post_menu_delete_post_title,
                            R.string.post_popup_confirm_delete_description,
                            R.string.seller_store_button_delete,
                            R.string.setting_change_language_dialog_cancel_button, dialog1 -> {
                                if (getActivity() instanceof TimelineDetailActivity) {
                                    mViewModel.deletePost(mWallStreetAdaptive);
                                    mIsRemoveItem = true;
                                    getActivity().finish();
                                }
                            });
                }
            }
            dialog.dismiss();

        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null) {
            mAdapter.checkToReleaseVideoPlayerInMediaLayout();
        }
    }
}

