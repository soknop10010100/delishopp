package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.intent.ConciergeSubCategoryDetailIntent;
import com.proapp.sompom.newui.fragment.ConciergeSubCategoryDetailFragment;

/**
 * Created by Chhom Veasna on 5/4/22.
 */
public class ConciergeSubCategoryDetailActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(ConciergeSubCategoryDetailFragment.newInstance(new ConciergeSubCategoryDetailIntent(getIntent()).getId()),
                ConciergeSubCategoryDetailFragment.class.getName());
    }
}
