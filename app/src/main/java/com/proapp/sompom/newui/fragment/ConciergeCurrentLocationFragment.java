package com.proapp.sompom.newui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.ConciergeCurrentLocationAdapter;
import com.proapp.sompom.databinding.FragmentConciergeCurrentLocationBinding;
import com.proapp.sompom.injection.google.GoogleQualifier;
import com.proapp.sompom.model.ConciergeCurrentLocationDisplayAdaptive;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.newui.dialog.UserLoginRegisterBottomSheetDialog;
import com.proapp.sompom.services.ApiGoogle;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeCurrentLocationDataManager;
import com.proapp.sompom.services.datamanager.SelectAddressDataManager;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeCurrentLocationFragmentViewModel;
import com.proapp.sompom.widget.LoadMoreLinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Or Vitovongsak on 16/11/21.
 */
public class ConciergeCurrentLocationFragment extends AbsBindingFragment<FragmentConciergeCurrentLocationBinding>
        implements ConciergeCurrentLocationFragmentViewModel.ConciergeCurrentLocationFragmentViewModelListener {

    @Inject
    public ApiService mApiService;
    @Inject
    @GoogleQualifier
    public ApiGoogle mApiGoogle;

    private Context mContext;
    private ConciergeCurrentLocationFragmentViewModel mViewModel;
    private ConciergeCurrentLocationAdapter mAdapter;
    private LoadMoreLinearLayoutManager mLoadMoreLayoutManager;

    public static ConciergeCurrentLocationFragment newInstance() {
        return new ConciergeCurrentLocationFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_current_location;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mContext = requireContext();

        SelectAddressDataManager.Strategy strategy = SelectAddressDataManager.Strategy.Mapbox;
        ConciergeCurrentLocationDataManager dataManager =
                new ConciergeCurrentLocationDataManager(mContext,
                        mApiService,
                        mApiGoogle,
                        strategy);

        mViewModel = new ConciergeCurrentLocationFragmentViewModel(mContext,
                dataManager,
                this);

        mAdapter = new ConciergeCurrentLocationAdapter(mContext,
                new ArrayList<>(),
                adaptive -> {
                    if (mViewModel != null) {
                        mViewModel.onAdapterItemClicked(adaptive);
                    }
                });
        mLoadMoreLayoutManager =
                new LoadMoreLinearLayoutManager(requireActivity(), getBinding().conciergeCurrentLocationRecyclerView);

        getBinding().conciergeCurrentLocationRecyclerView.setLayoutManager(mLoadMoreLayoutManager);
        getBinding().conciergeCurrentLocationRecyclerView.setAdapter(mAdapter);
        getBinding().conciergeCurrentLocationRecyclerView.setOnTouchListener((v, event) -> {
            KeyboardUtil.hideKeyboard(getActivity());
            v.performClick();
            return false;
        });

        getBinding().layoutToolbar.toolbarTitle.setText(getString(R.string.shop_current_location_screen_title));
        setToolbar(getBinding().layoutToolbar.toolbar, true);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onLocationSelected(ConciergeUserAddress address) {
        if (requireActivity() instanceof ConciergeCurrentLocationCallback) {
            ((ConciergeCurrentLocationCallback) requireActivity()).onLocationSelected(address);
        }
    }

    @Override
    public void onLoadSavedAddress(List<ConciergeUserAddress> userAddresses) {
        if (mAdapter != null) {
            List<ConciergeCurrentLocationDisplayAdaptive> adaptive = new ArrayList<>(userAddresses);
            mAdapter.updateAddressList(adaptive);
        }
    }

    @Override
    public void onLoadSearchAddressResult(List<SearchAddressResult> searchAddressResults) {
        if (mAdapter != null) {
            List<ConciergeCurrentLocationDisplayAdaptive> adaptive = new ArrayList<>(searchAddressResults);
            mAdapter.updateAddressList(adaptive);
        }
    }

    @Override
    public void onNewAddressAdded(ConciergeUserAddress newAddress) {
        if (mAdapter != null) {
            mAdapter.onNewAddressAdded(newAddress);
        }
    }

    @Override
    public void onRequireLogin() {
        UserLoginRegisterBottomSheetDialog dialog = UserLoginRegisterBottomSheetDialog.newInstance();
        dialog.show(getChildFragmentManager(), dialog.getTag());
        getChildFragmentManager().setFragmentResultListener(UserLoginRegisterBottomSheetDialog.REQUEST_LOGIN_OR_SIGNUP_KEY,
                getViewLifecycleOwner(),
                (resultRequestKey, result) -> {
                    if (resultRequestKey.equals(UserLoginRegisterBottomSheetDialog.REQUEST_LOGIN_OR_SIGNUP_KEY)) {
                        if (result.getInt(UserLoginRegisterBottomSheetDialog.LOGIN_RESULT_KEY) == AppCompatActivity.RESULT_OK) {
                            mViewModel.openAddNewAddressScreen();
                        }
                    }
                });
    }

    public interface ConciergeCurrentLocationCallback {
        void onLocationSelected(ConciergeUserAddress address);
    }
}
