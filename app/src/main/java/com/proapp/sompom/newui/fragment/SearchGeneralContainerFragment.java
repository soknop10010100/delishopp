package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentSearchGeneralContainerBinding;
import com.proapp.sompom.intent.newintent.SearchMessageIntent;
import com.proapp.sompom.viewmodel.newviewmodel.SearchGeneralContainerFragmentViewModel;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class SearchGeneralContainerFragment extends AbsBindingFragment<FragmentSearchGeneralContainerBinding> {

    private SearchGeneralContainerResultFragment mResultFragment;
    private SearchProductResultFragment mProductResultFragment;

    private SearchMessageIntent.SearchType mSearchType;

    public static SearchGeneralContainerFragment newInstance(String query,
                                                             String supplierId,
                                                             SearchMessageIntent.SearchType searchType) {
        Bundle args = new Bundle();
        args.putString(SearchMessageIntent.QUERY, query);
        args.putString(SearchMessageIntent.SUPPLIER_ID, supplierId);
        args.putSerializable(SearchMessageIntent.SEARCH_TYPE, searchType);

        SearchGeneralContainerFragment fragment = new SearchGeneralContainerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search_general_container;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbar(getBinding().toolbar, false);
        String query = null;
        if (getArguments() != null) {
            query = getArguments().getString(SearchMessageIntent.QUERY);
            mSearchType = (SearchMessageIntent.SearchType) getArguments().getSerializable(SearchMessageIntent.SEARCH_TYPE);
        }

        if (mSearchType == null) {
            // Default search type to normal search, which is search user and conversation
            mSearchType = SearchMessageIntent.SearchType.Normal;
        }

        switch (mSearchType) {
            case Normal:
                //Check if there was pre-query to perform
                if (TextUtils.isEmpty(query)) {
                    replaceFragment(SearchGeneralFragment.newInstance(), TAG);
                }
                break;
            case Product:
                // Add view here if we want to display a default list of product search result.
                // Currently we don't have any, so just leave empty.
                break;
        }

        SearchGeneralContainerFragmentViewModel viewModel = new SearchGeneralContainerFragmentViewModel(getActivity(),
                query,
                mSearchType,
                new SearchGeneralContainerFragmentViewModel.Callback() {
                    @Override
                    public void onReplaceFragment() {
                        switch (mSearchType) {
                            case Normal:
                                mResultFragment = SearchGeneralContainerResultFragment.newInstance();
                                getChildFragmentManager()
                                        .beginTransaction()
                                        .add(R.id.containerLayout,
                                                mResultFragment,
                                                SearchGeneralContainerResultFragment.TAG)
                                        .addToBackStack(SearchGeneralContainerResultFragment.TAG)
                                        .commit();
                                break;
                            case Product:
                                mProductResultFragment = SearchProductResultFragment.newInstance(getArguments().getString(SearchMessageIntent.SUPPLIER_ID));
                                getChildFragmentManager()
                                        .beginTransaction()
                                        .add(R.id.containerLayout,
                                                mProductResultFragment,
                                                SearchProductResultFragment.TAG)
                                        .addToBackStack(SearchProductResultFragment.TAG)
                                        .commit();
                                break;
                        }
                    }

                    @Override
                    public void onRemoveFragment() {
                        switch (mSearchType) {
                            case Normal:
                                removeFragment(getFragment(SearchGeneralContainerResultFragment.TAG));
                                break;
                            case Product:
                                removeFragment(getFragment(SearchProductResultFragment.TAG));
                                break;
                        }
                    }

                    @Override
                    public void onSearch(String text) {
                        switch (mSearchType) {
                            case Normal:
                                if (mResultFragment != null && mResultFragment.isAdded()) {
                                    mResultFragment.onSearch(text, false);
                                }
                                break;
                            case Product:
                                if (mProductResultFragment != null && mProductResultFragment.isAdded()) {
                                    mProductResultFragment.onSearch(text);
                                }
                                break;
                        }
                    }

                    @Override
                    public void onPerformLocalSearch(String text) {
                        switch (mSearchType) {
                            case Normal:
                                if (mResultFragment != null) {
                                    new Handler().postDelayed(() -> mResultFragment.onSearch(text,
                                            true),
                                            10);
                                }
                                break;
                            case Product:
                                break;
                        }
                    }

                    @Override
                    public void showLoading() {
                        switch (mSearchType) {
                            case Normal:
                                if (mResultFragment != null) {
                                    new Handler().postDelayed(() -> mResultFragment.showLoading(),
                                            10);
                                }
                                break;
                            case Product:
                                if (mProductResultFragment != null) {
                                    new Handler().postDelayed(() -> mProductResultFragment.showScreenLoading(),
                                            10);
                                }
                                break;
                        }
                    }
                });
        setVariable(BR.viewModel, viewModel);
        if (TextUtils.isEmpty(query)) {
            getBinding().editText.post(() -> getBinding().editText.requestFocus());
        }
    }

    private void replaceFragment(AbsBaseFragment fragment, String tag) {
        setFragment(R.id.containerLayout,
                fragment,
                tag);
    }
}
