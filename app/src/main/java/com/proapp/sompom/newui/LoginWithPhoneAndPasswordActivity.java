package com.proapp.sompom.newui;

import android.os.Bundle;

import com.proapp.sompom.intent.InputLoginPasswordIntent;
import com.proapp.sompom.newui.fragment.InputLoginPasswordFragment;
import com.proapp.sompom.newui.fragment.LoginWithPhoneAndPasswordFragment;

import androidx.annotation.Nullable;

public class LoginWithPhoneAndPasswordActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(LoginWithPhoneAndPasswordFragment.newInstance(),
                LoginWithPhoneAndPasswordFragment.TAG);
    }
}
