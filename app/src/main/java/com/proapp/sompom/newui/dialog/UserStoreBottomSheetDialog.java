package com.proapp.sompom.newui.dialog;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.proapp.sompom.adapter.newadapter.UserStoreDialogAdapter;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.widget.LoaderMoreLayoutManager;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.DialogUserStoreBottomSheetBinding;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ProductStoreDataManager;
import com.proapp.sompom.services.datamanager.StoreDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.DialogUserStoreBottomSheetViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 10/24/18.
 */

public class UserStoreBottomSheetDialog extends AbsBindingBottomSheetDialog<DialogUserStoreBottomSheetBinding>
        implements OnCallbackListListener<List<Product>> {
    @Inject
    public ApiService mApiService;
    private LoaderMoreLayoutManager mLoaderMoreLayoutManager;
    private DialogUserStoreBottomSheetViewModel mViewModel;
    private UserStoreDialogAdapter mUserStoreDialogAdapter;

    public static UserStoreBottomSheetDialog newInstance(User user) {

        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.DATA, user);

        UserStoreBottomSheetDialog fragment = new UserStoreBottomSheetDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_user_store_bottom_sheet;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        User user = getArguments().getParcelable(SharedPrefUtils.DATA);
        StoreDataManager dataManager = new StoreDataManager(getActivity(), mApiService);
        ProductStoreDataManager storeDataManager = new ProductStoreDataManager(getActivity(), mApiService);
        mViewModel = new DialogUserStoreBottomSheetViewModel(dataManager,
                storeDataManager,
                user,
                this);
        setVariable(BR.viewModel, mViewModel);
    }

    private void iniRecyclerView(List<Product> productList, boolean canLoadMore) {
        mLoaderMoreLayoutManager = new LoaderMoreLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mLoaderMoreLayoutManager.setOnLoadMoreListener(() -> mViewModel.loadMore(new OnCallbackListListener<List<Product>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mUserStoreDialogAdapter.setCanLoadMore(false);
                mUserStoreDialogAdapter.notifyDataSetChanged();
                mLoaderMoreLayoutManager.loadingFinished();
                mLoaderMoreLayoutManager.setOnLoadMoreListener(null);
            }

            @Override
            public void onComplete(List<Product> result, boolean canLoadMore) {
                mUserStoreDialogAdapter.addLoadMoreData(new ArrayList<>(result));
                mUserStoreDialogAdapter.setCanLoadMore(canLoadMore);
                mLoaderMoreLayoutManager.loadingFinished();
            }
        }));
        getBinding().recyclerView.setLayoutManager(mLoaderMoreLayoutManager);
        mUserStoreDialogAdapter = new UserStoreDialogAdapter(productList, result -> {
        });
        mUserStoreDialogAdapter.setCanLoadMore(canLoadMore);
        getBinding().recyclerView.setAdapter(mUserStoreDialogAdapter);
    }

    @Override
    public void onComplete(List<Product> result, boolean canLoadMore) {
        iniRecyclerView(result, canLoadMore);
    }

    @Override
    public void onFail(ErrorThrowable ex) {

    }
}
