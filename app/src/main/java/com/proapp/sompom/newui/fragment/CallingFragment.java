package com.proapp.sompom.newui.fragment;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.NetworkBroadcastReceiver;
import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.chat.call.CallNotificationHelper;
import com.proapp.sompom.chat.call.CallTimerHelper;
import com.proapp.sompom.chat.call.CallingService;
import com.proapp.sompom.chat.call.IncomingCallDataHolder;
import com.proapp.sompom.databinding.FragmentIncomingCallBinding;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.intent.newintent.CallIntent;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.request.JoinChannelStatusUpdateBody;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.CallingActivity;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.CallDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.CallingViewModel;
import com.proapp.sompom.widget.call.CallViewContainer;
import com.resourcemanager.utils.ToastUtil;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.Map;

import javax.inject.Inject;

import timber.log.Timber;

public class CallingFragment extends AbsBindingFragment<FragmentIncomingCallBinding>
        implements CallingService.CallingListener, CallingViewModel.CallingViewModelListener {

    public static final String SHOULD_BROADCAST_CALL_EVENT = "SHOULD_BROADCAST_CALL_EVENT";
    public static final String CALL_EVENT = "CALL_EVENT";
    public static final String CALL_TYPE = "CALL_TYPE";
    public static final String IS_MISSED_CALL = "IS_MISSED_CALL";
    public static final String RECIPIENT = "RECIPIENT";
    public static final String CALLED_DURATION = "CALLED_DURATION";
    public static final String IN_ACTION_CALL_EVENT = "IN_ACTION_CALL_EVENT";
    public static final String IS_OPEN = "IS_OPEN";
    public static final String NEW_MESSAGE = "NEW_MESSAGE";
    public static final String CALL_MESSAGE_EVEN = "CALL_MESSAGE_EVEN";

    private CallingViewModel mCallingViewModel;
    private boolean mCallEstablish = false;
    private CallingIntent.StartFromType mStartFromType;
    private String conversationId;
    @Inject
    public ApiService mApiService;
    private boolean mIsAlreadyBroadcastEndGroupCallMessage;
    private boolean mIsAlreadyBroadcastStartGroupCallMessage;
    private boolean mIsAlreadyBroadcastIndividualCallMessage;
    private BroadcastReceiver mJoinGroupCallStatusReceiver;
    private BroadcastReceiver mBluetoothConnectionStateChangeReceiver;
    private NetworkBroadcastReceiver.NetworkListener mNetworkListener;

    public static CallingFragment newIncomingCallInstance(IncomingCallDataHolder incomingCallData,
                                                          CallingIntent.StartFromType startFromType,
                                                          boolean isIncomingCallFromNotification) {
        Timber.i("Incoming call data: " + new Gson().toJson(incomingCallData));
        Bundle args = new Bundle();
        CallingFragment fragment = new CallingFragment();
        args.putParcelable(SharedPrefUtils.STATUS, incomingCallData);
        args.putInt(CallingIntent.START_FROM_TYPE, startFromType.getType());
        args.putString(CallIntent.CALL_ACTION_TYPE, AbsCallService.CallActionType.INCOMING.getValue());
        args.putString(CallHelper.TYPE, incomingCallData.getCallType().getValue());
        args.putBoolean(CallNotificationHelper.IS_INCOMING_CALL_FROM_NOTIFICATION, isIncomingCallFromNotification);

        fragment.setArguments(args);
        return fragment;
    }

    public static CallingFragment newCallInstance(String callType,
                                                  User callTo,
                                                  UserGroup group,
                                                  CallingIntent.StartFromType startFromType) {
        Bundle args = new Bundle();
        CallingFragment fragment = new CallingFragment();
        args.putString(CallHelper.TYPE, callType);
        args.putParcelable(SharedPrefUtils.ID, callTo);
        args.putInt(CallingIntent.START_FROM_TYPE, startFromType.getType());
        args.putString(CallIntent.CALL_ACTION_TYPE, AbsCallService.CallActionType.CALL.getValue());
        args.putParcelable(CallHelper.GROUP, group);

        fragment.setArguments(args);
        return fragment;
    }

    public static CallingFragment newJoinCallInstance(String callType,
                                                      CallingIntent.StartFromType startFromType,
                                                      UserGroup group) {
        Bundle args = new Bundle();
        CallingFragment fragment = new CallingFragment();
        args.putParcelable(CallHelper.GROUP, group);
        args.putInt(CallingIntent.START_FROM_TYPE, startFromType.getType());
        args.putString(CallHelper.TYPE, callType);
        args.putString(CallIntent.CALL_ACTION_TYPE, AbsCallService.CallActionType.JOINED.getValue());


        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_incoming_call;
    }


    public boolean isAlreadyGotCallBusyStatus() {
        return mCallingViewModel.isAlreadyGotCallBusyStatus();
    }

    public boolean isShowingIncomingCall() {
        return mCallingViewModel.isShowingIncomingCall();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerNetworkStatusChange();
        getControllerComponent().inject(this);
        registerBluetoothConnectionStateChangeReceiver();
        registerUpdateJoinGroupCallStatusReceiver();
        broadcastInActionCallEvent(true);
        if (getArguments() == null) {
            return;
        }
        IncomingCallDataHolder callVo = getArguments().getParcelable(SharedPrefUtils.STATUS);
        User recipientId = getArguments().getParcelable(SharedPrefUtils.ID);
        Product product = getArguments().getParcelable(SharedPrefUtils.PRODUCT);
        mStartFromType = CallingIntent.StartFromType.from(getArguments().getInt(CallingIntent.START_FROM_TYPE,
                0));


        CallingService.CallServiceBinder binder = null;

        if (getActivity() instanceof CallingActivity) {
            binder = ((CallingActivity) getActivity()).getBinder();
        }

        if (binder == null) {
            return;
        }

        AbsCallService.CallActionType callActionType = AbsCallService.CallActionType
                .getTypeFromValue(getArguments().getString(CallIntent.CALL_ACTION_TYPE));
        mCallingViewModel = new CallingViewModel(requireContext(),
                new CallDataManager(requireContext(), mApiService),
                AbsCallService.CallType.getFromValue(getArguments().getString(CallHelper.TYPE)),
                callActionType,
                callVo,
                recipientId,
                getArguments().getParcelable(CallHelper.GROUP),
                getArguments().getBoolean(CallNotificationHelper.IS_INCOMING_CALL_FROM_NOTIFICATION, false),
                product,
                binder,
                this,
                this);

        setVariable(BR.viewModel, mCallingViewModel);
    }

    private void registerNetworkStatusChange() {
        if (requireActivity() instanceof AbsBaseActivity) {
            mNetworkListener = new NetworkBroadcastReceiver.NetworkListener() {
                @Override
                public void onNetworkState(NetworkBroadcastReceiver.NetworkState networkState) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        ToastUtil.showToast(requireContext(),
                                getString(R.string.error_internet_connection_description),
                                true);
                        new Handler().postDelayed(() ->
                                        onShowIndefiniteToast(getString(R.string.call_screen_connect_label),
                                                true),
                                2000);
                    } else {
                        hideIndefiniteToast();
                    }
                }
            };
            ((AbsBaseActivity) requireActivity()).addNetworkStateChangeListener(mNetworkListener);
        }
    }

    public void checkToStopIncomingToneWhenDeviceButtonPressed() {
        mCallingViewModel.checkToStopIncomingToneWhenDeviceButtonPressed();
    }

    private void registerUpdateJoinGroupCallStatusReceiver() {
        mJoinGroupCallStatusReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                JoinChannelStatusUpdateBody data = intent.getParcelableExtra(SharedPrefUtils.DATA);
                if (data != null) {
                    Timber.i("onReceive: JoinGroupCallStatusReceiver: " + new Gson().toJson(data));
//                    mCallingViewModel.checkToEndGroupInComingCallIfNecessary(data.getConversationId(), data.getChannelOpen());
                    mCallingViewModel.checkToUpdateToVideoCallTypeIfNecessary(data.getConversationId(),
                            data.getChannelOpen(),
                            data.getVideo());
                }
            }
        };
        requireActivity().registerReceiver(mJoinGroupCallStatusReceiver,
                new IntentFilter(ChatHelper.JOIN_GROUP_CALL_STATUS_UPDATE));
    }

    private void registerBluetoothConnectionStateChangeReceiver() {
        mBluetoothConnectionStateChangeReceiver =
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String action = intent.getAction();
                        Timber.i("onReceive: mBluetoothConnectionStateChangeReceiver: action: " + action);
                        if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                            Timber.i("BluetoothDevice connected");
                            mCallingViewModel.onUpdateBluetoothConnectionStatus(true);
                        } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                            Timber.i("BluetoothDevice disconnected");
                            mCallingViewModel.onUpdateBluetoothConnectionStatus(false);
                        }
                    }
                };
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        requireActivity().registerReceiver(mBluetoothConnectionStateChangeReceiver, filter);
    }

    /**
     * This method must be called in onViewCreated of this fragment to inform all subscribers that
     * the screen of call action has been opened.
     */
    private void broadcastInActionCallEvent(boolean isOpen) {
        Intent intent = new Intent(IN_ACTION_CALL_EVENT);
        intent.putExtra(IS_OPEN, isOpen);
        SendBroadCastHelper.verifyAndSendBroadCast(requireContext(), intent);
    }

    private void setCallEventResult(AbsCallService.CallType callType,
                                    User recipient,
                                    boolean isMissedCall,
                                    int calledDuration) {
        Intent intent = new Intent(CALL_EVENT);
        intent.putExtra(CALL_TYPE, callType.getValue());
        intent.putExtra(SHOULD_BROADCAST_CALL_EVENT, true);
        intent.putExtra(RECIPIENT, recipient);
        intent.putExtra(IS_MISSED_CALL, isMissedCall);
        intent.putExtra(CALLED_DURATION, calledDuration);
        requireActivity().setResult(AppCompatActivity.RESULT_OK, intent);
    }

    @Override
    public void shouldDispatchStartGroupCall(AbsCallService.CallType callType) {
        Timber.i("shouldDispatchStartGroupCall: " + callType);
        if (!mIsAlreadyBroadcastStartGroupCallMessage) {
            dispatchStartOrEndGroupCall(callType,
                    SharedPrefUtils.getCurrentUser(requireContext()),
                    true);
        }
    }

    @Override
    public void onCallEnd(AbsCallService.CallType callType,
                          String channelId,
                          boolean isGroup,
                          User recipient,
                          boolean isMissedCall,
                          int calledDuration,
                          boolean isEndedByClickAction,
                          boolean isCauseByTimeOut,
                          boolean isCausedByParticipantOffline) {
        Timber.i("onCallEnd: recipient: " + (recipient != null ? recipient.getId() : null)
                + ", callType: " + callType
                + ", isMissedCall: " + isMissedCall
                + ", calledDuration: " + calledDuration
                + ", isEndedByClickAction: " + isEndedByClickAction);
        mCallingViewModel.playCallOnOrOffTone(true);
        boolean isGroupCall = mCallingViewModel.isGroupCall();
        if (!isGroupCall) {
            if (mCallingViewModel.isCurrentUserCaller() && !mIsAlreadyBroadcastIndividualCallMessage) {
                mIsAlreadyBroadcastIndividualCallMessage = true;
                if (isCallStartedInChatScreen()) {
                    Timber.i("mStartFromType CHAT_SCREEN");
                    //Will delegate the call even handling to chat screen since the call was made inside it.
                    //And please make sure that only chat screen will subscribe this broadcast.
                    setCallEventResult(callType, recipient, isMissedCall, calledDuration);
                } else {
                    if (requireActivity() instanceof AbsBaseActivity) {
                        ((AbsBaseActivity) requireActivity()).broadcastOneToOneCallEventMessage(callType,
                                recipient,
                                isMissedCall,
                                calledDuration);
                    }
                }
            }
        }

        if (isAdded() && !requireActivity().isFinishing()) {
            requireActivity().finish();
        }
    }

    private void dispatchStartOrEndGroupCall(AbsCallService.CallType callType,
                                             User sender,
                                             boolean isStartCall) {
        UserGroup group = mCallingViewModel.getCallService().getCallGroup();
        if (group != null) {
            if (isStartCall && !mCallingViewModel.isCurrentUserCaller()) {
                //Only caller of a group call that will dispatch the start group call messages.
                return;
            }
            if (isStartCall) {
                mIsAlreadyBroadcastStartGroupCallMessage = true;
            } else {
                mIsAlreadyBroadcastEndGroupCallMessage = true;
            }
            Chat groupChat = ChatHelper.buildStartOrEndGroupCallEventMessages(requireContext(),
                    callType,
                    sender,
                    group,
                    isStartCall);
            groupChat.setSendTo(group.getId());
            if (requireActivity() instanceof AbsBaseActivity) {
                ((AbsBaseActivity) requireActivity()).getChatBinder().sendMessage(null, groupChat);
                if (isCallStartedInChatScreen()) {
                    /*
                    Can take any message to be displayed in chat list for all the messages were sent
                    by current user.
                     */
                    broadcastToAddDisplayOfNewMessageAutomatically(groupChat);
                }
            }
        }
    }

    private void broadcastToAddDisplayOfNewMessageAutomatically(Chat newMessage) {
        /*
        Will broadcast new message to be added chat screen automatically.
         */
        Intent intent = new Intent(CALL_MESSAGE_EVEN);
        intent.putExtra(NEW_MESSAGE, newMessage);
        SendBroadCastHelper.verifyAndSendBroadCast(requireContext(), intent);
    }

    private boolean isCallStartedInChatScreen() {
        return mStartFromType == CallingIntent.StartFromType.CHAT_SCREEN;
    }

    @Override
    public void onIncomingCall(AbsCallService.CallType calType, Map<String, Object> data1, Map<String, Object> data2) {

    }

    @Override
    public void onCallEstablished(AbsCallService.CallType callType, String channelId, boolean isGroup) {
        mCallEstablish = true;
    }

    @Override
    public void onCallInfoUpdated(AbsCallService.CallType callType, String info, boolean isCallProfileVisible, boolean showCallControls) {

    }

    @Override
    public void onCheckShowingRequestVideoCamera(boolean shouldShow,
                                                 AbsCallService.CallType callType,
                                                 User requester,
                                                 boolean isGroupCall) {

    }

    @Override
    public void onCallTimerUp(AbsCallService.CallType callType,
                              CallTimerHelper.TimerType timerType,
                              CallTimerHelper.StarterTimerType starterTimerType) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requireContext().unregisterReceiver(mBluetoothConnectionStateChangeReceiver);
        requireContext().unregisterReceiver(mJoinGroupCallStatusReceiver);
        broadcastInActionCallEvent(false);
        getBinding().callViewContainer.resetCallViewAndDataState();
        CallNotificationHelper.cancelInComingCallNotification(requireContext());
        ((AbsBaseActivity) requireActivity()).removeStateChangeListener(mNetworkListener);
    }

    @Override
    public CallViewContainer getCallViewContainer() {
        return getBinding().callViewContainer;
    }

    @Override
    public View getTopCallOptionContainerView() {
        return getBinding().topCallOptionContainerView;
    }

    @Override
    public View getBottomCallOptionContainerView() {
        return getBinding().bottomCallOptionContainerView;
    }

    @Override
    public void onCallChannelIdIdGenerated(String channelId) {
        if (requireActivity() instanceof CallingFragmentListener) {
            ((CallingFragmentListener) requireActivity()).onCallChannelIdIdGenerated(channelId);
        }
    }

    private void onShowIndefiniteToast(String message, boolean isError) {
        Toast toast = ToastUtil.buildToast(requireContext(), message, isError);
        getBinding().toastRootView.addView(toast.getView());
    }

    private void hideIndefiniteToast() {
        getBinding().toastRootView.removeAllViews();
    }

    public void handleNotificationAction(AbsCallService.CallNotificationActionType type, String groupId) {
        mCallingViewModel.handleNotificationAction(type, groupId);
    }

    public interface CallingFragmentListener {
        void onCallChannelIdIdGenerated(String channelId);
    }
}
