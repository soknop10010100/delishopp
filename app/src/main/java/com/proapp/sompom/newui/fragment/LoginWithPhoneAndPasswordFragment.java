package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentLoginWithPhoneAndPasswordBinding;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.model.result.AuthResponse;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.LoginDataManager;
import com.proapp.sompom.viewmodel.LoginWithPhoneAndPasswordViewModel;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import javax.inject.Inject;

import androidx.annotation.Nullable;

/**
 * Created by Veasna Chhom on 4/21/21.
 */
public class LoginWithPhoneAndPasswordFragment extends AbsLoginFragment<FragmentLoginWithPhoneAndPasswordBinding, LoginWithPhoneAndPasswordViewModel> implements
        LoginWithPhoneAndPasswordViewModel.LoginWithPhoneAndPasswordViewModelListener {

    @PublicQualifier
    @Inject
    public ApiService mApiService;
    @Inject
    public ApiService mPrivateApiService;

    public static LoginWithPhoneAndPasswordFragment newInstance() {

        Bundle args = new Bundle();

        LoginWithPhoneAndPasswordFragment fragment = new LoginWithPhoneAndPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_login_with_phone_and_password;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mViewModel = new LoginWithPhoneAndPasswordViewModel(requireContext(),
                new LoginDataManager(requireContext(),
                        mApiService,
                        mPrivateApiService),
                this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onLoginError(String message) {
        handleLoginError(message);
    }

    @Override
    public void onLoginSuccess() {
        handleOnLoginSuccess();
    }

    @Override
    public void onChangePasswordRequire(AuthResponse authResponse) {
        handleChangePasswordRequire(authResponse);
    }

    @Override
    public CountryCodePicker getCountryCodePicker() {
        return getBinding().ccp;
    }
}
