package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.usermentionable.model.UserMentionable;
import com.example.usermentionable.tokenization.QueryToken;
import com.example.usermentionable.tokenization.interfaces.QueryTokenReceiver;
import com.google.android.gms.maps.GoogleMap;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentCreateTimelineBinding;
import com.proapp.sompom.helper.PostTextGenerateStyleHelper;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.intent.CreateTimelineIntent;
import com.proapp.sompom.listener.AbsLinkPreviewCallback;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.CreateWallStreetScreenType;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.CreateWallStreetDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.CreateTimelineFragmentViewModel;
import com.proapp.sompom.widget.PlacePreviewLayout;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by nuonveyo on 5/4/18.
 */

public class CreateTimelineFragment extends AbsBindingFragment<FragmentCreateTimelineBinding>
        implements QueryTokenReceiver, CreateTimelineFragmentViewModel.CreateTimelineFragmentListener {
    public static final String TAG = CreateTimelineFragment.class.getName();

    @Inject
    public ApiService mApiService;
    @Inject
    @PublicQualifier
    public ApiService mApiServiceTest;
    private CreateTimelineFragmentViewModel mViewModel;
    private PostTextGenerateStyleHelper mPostTextGenerateStyleHelper;
    private final AbsLinkPreviewCallback mLinkPreviewCallback = new AbsLinkPreviewCallback() {

        @Override
        public void onSetEditText(String text, boolean isFromEditPost) {
            getBinding().richEditorView.postDelayed(() -> {
                if (isFromEditPost) {
                    /*
                    We need to render previous mention or hash tag of post in edit mode.
                     */
                    getBinding().richEditorView.setText(text, true);
                } else {
                    getBinding().richEditorView.getMentionsEditText().setText(text);
                }
            }, 50);
            onClearKeyboardFocus();
        }

        @Override
        public void onRemoveListener() {
            getBinding().linkPreviewLayout.hideLayout();
        }

        @Override
        public void onStartDownloadLinkPreview(String link) {
            onHidePlacePreviewLayout();
            getBinding().linkPreviewLayout.showLayout();
        }

        @Override
        public void onShowGifLinkFormKeyboard(String link) {
            getBinding().linkPreviewLayout.showGifLinkFromKeyboard(link);
        }

        @Override
        public void shouldHidePreviewCloseButton() {
            getBinding().linkPreviewLayout.hideCloseButton();
        }
    };

    public static CreateTimelineFragment newInstance(WallStreetAdaptive adaptive,
                                                     int screenTypeId,
                                                     boolean isFromOtherAppContent) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SharedPrefUtils.DATA, adaptive);
        bundle.putInt(SharedPrefUtils.ID, screenTypeId);
        bundle.putBoolean(CreateTimelineIntent.IS_FROM_OTHER_APP_CONTENT, isFromOtherAppContent);
        CreateTimelineFragment fragment = new CreateTimelineFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_create_timeline;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        WallStreetAdaptive wallStreetAdaptive = null;
        CreateWallStreetScreenType screenType = CreateWallStreetScreenType.CREATE_POST;
        if (getArguments() != null) {
            wallStreetAdaptive = getArguments().getParcelable(SharedPrefUtils.DATA);
            int id = getArguments().getInt(SharedPrefUtils.ID, CreateWallStreetScreenType.CREATE_POST.getId());
            screenType = CreateWallStreetScreenType.getType(id);
        }

        setToolbar(getBinding().layoutToolbar.toolbar, true);
        if (getActivity() != null) {
            if (screenType == CreateWallStreetScreenType.EDIT_POST) {
                getBinding().layoutToolbar.textViewTitle.setText(getActivity().getString(R.string.create_wall_street_edit_title));
            } else {
                getBinding().layoutToolbar.textViewTitle.setText(getActivity().getString(R.string.create_wall_street_title));
            }
        }
        mPostTextGenerateStyleHelper = new PostTextGenerateStyleHelper(getBinding().richEditorView.getMentionsEditText(),
                null,
                getResources().getDimensionPixelSize(R.dimen.text_medium),
                getResources().getDimensionPixelSize(R.dimen.text_xxlarge),
                R.font.sf_pro_regular,
                R.font.sf_pro_light,
                true,
                null);

        CreateWallStreetDataManager dataManager = new CreateWallStreetDataManager(getActivity(), mApiService, mApiServiceTest);
        mViewModel = new CreateTimelineFragmentViewModel((AbsBaseActivity) getActivity(),
                dataManager,
                wallStreetAdaptive,
                screenType,
                getArguments().getBoolean(CreateTimelineIntent.IS_FROM_OTHER_APP_CONTENT, false),
                mLinkPreviewCallback,
                mPostTextGenerateStyleHelper,
                this);
        setVariable(BR.viewModel, mViewModel);
        getBinding().linkPreviewLayout.setOnCloseLinkPreviewListener(() -> {
            mViewModel.onLinkPreviewClosed();
            mViewModel.setShowedLinkPreview(false);
        });

        getBinding().richEditorView.setQueryTokenReceiver(this);
        getBinding().richEditorView.addOnTextChangeListener(mViewModel.onTextChanged());
        getBinding().richEditorView.setOnKeyboardInputCallback(mViewModel);
        getBinding().placePreview.onCreate(savedInstanceState, true);
        getBinding().placePreview.setListener(new PlacePreviewLayout.PlacePreviewLayoutListener() {
            @Override
            public void onMapReady(GoogleMap map) {
                Timber.i("onMapReady");
                mViewModel.checkToRenderPreviousEditPostPlacePreview();
            }

            @Override
            public void onPreviewClosed() {
                mViewModel.onMapPreviewClosed();
            }
        });
        mViewModel.checkToDisplayEditPostPreviewLink();
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().linkPreviewLayout.onPause();
        getBinding().richEditorView.removeListPopupWindow();
        getBinding().placePreview.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getBinding().linkPreviewLayout.onDestroy();
        getBinding().placePreview.onDestroy();
        getBinding().richEditorView.removeListPopupWindow();
    }

    @Override
    public void onResume() {
        super.onResume();
        getBinding().linkPreviewLayout.onResume();
        getBinding().placePreview.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        getBinding().placePreview.onStop();
    }

    @Override
    public void onQueryReceived(@NonNull QueryToken queryToken) {
        if (queryToken.getExplicitChar() == '@') {
            mViewModel.getMentionUserList(queryToken.getKeywords(), new OnCallbackListener<List<UserMentionable>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    getBinding().richEditorView.hideListPopupWindow();
                }

                @Override
                public void onComplete(List<UserMentionable> result) {
                    if (result != null && !result.isEmpty()) {
                        getBinding().richEditorView.showListPopupWindow(result);
                    } else {
                        getBinding().richEditorView.hideListPopupWindow();
                    }
                }
            });
        } else if (queryToken.getExplicitChar() == '#') {
            getBinding().richEditorView.checkToRenderHashTag(queryToken.getTokenString());
        } else {
            getBinding().richEditorView.hideListPopupWindow();
        }
    }

    @Override
    public void invalidToken() {
        getBinding().richEditorView.removeListPopupWindow();
    }

    @Override
    public void onHidePlacePreviewLayout() {
        getBinding().placePreview.hideLayout();
    }

    @Override
    public void onHideLinkPreviewLayout() {
        getBinding().linkPreviewLayout.hideLayout();
    }

    @Override
    public void onRenderPlacePreview(PlacePreviewLayout.PlacePreviewData data) {
        getBinding().placePreview.showLayout();
        getBinding().placePreview.resetPreviousMarker();
        getBinding().placePreview.bindPlace(data);
    }

    @Override
    public Editable getEditable() {
        return getBinding().richEditorView.getMentionsEditText().getEditableText();
    }

    @Override
    public void onClearKeyboardFocus() {
        getBinding().inputContainerView.requestFocus();
    }

    @Override
    public void onRequestLinkPreviewFinished(LinkPreviewModel linkPreviewModel) {
        Timber.i("onRequestLinkPreviewFinished: " + linkPreviewModel);
        if (linkPreviewModel == null) {
            showSnackBar(R.string.failed_to_previewlink, true);
            getBinding().linkPreviewLayout.hideLayout();
        } else {
            onHidePlacePreviewLayout();
            getBinding().linkPreviewLayout.showLayout();
            mViewModel.setShowedLinkPreview(true);
            getBinding().linkPreviewLayout.setLinkPreviewModel(linkPreviewModel);
        }
    }
}
