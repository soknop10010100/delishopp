package com.proapp.sompom.newui;

import android.content.Intent;
import android.os.Bundle;

import com.proapp.sompom.intent.ConciergeCheckoutIntent;
import com.proapp.sompom.intent.newintent.HomeIntent;
import com.proapp.sompom.newui.fragment.ConciergeCheckoutFragment;

/**
 * Created by Or Vitovongsak on 16/9/21.
 */

public class ConciergeCheckoutActivity extends AbsDefaultActivity {

    private boolean mIsOpenFromReOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConciergeCheckoutIntent intent = new ConciergeCheckoutIntent(getIntent());
        mIsOpenFromReOrder = intent.getIsOpenFromReorder();
        setFragment(ConciergeCheckoutFragment.newInstance(intent.getPrimaryUserAddress(),
                        intent.getSelectedTimeSlotId(),
                        intent.getIsSlotProvince(),
                        intent.getResumeABAPaymentData(),
                        intent.getDeliveryInstruction()),
                ConciergeCheckoutFragment.TAG);
    }

    @Override
    public void onBackPressed() {
        checkShouldReturnToHome();
        super.onBackPressed();
    }

    @Override
    public void finish() {
        checkShouldReturnToHome();
        super.finish();
    }

    // Intercept onBackPressed() and finish() of activity to check whether we should open the
    // concierge home screen based on if the checkout screen was opened from reorder
    private void checkShouldReturnToHome() {
        if (mIsOpenFromReOrder) {
            HomeIntent homeIntent = new HomeIntent(getApplicationContext(), HomeIntent.Redirection.Concierge);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
        }
    }
}
