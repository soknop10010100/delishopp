package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.PrivacyAdapter;
import com.proapp.sompom.databinding.FragmentChoosePrivacyBinding;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.intent.newintent.ChoosePrivacyIntent;
import com.proapp.sompom.model.ItemPrivacy;
import com.proapp.sompom.model.emun.PublishItem;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

public class ChoosePrivacyFragment extends AbsBindingFragment<FragmentChoosePrivacyBinding> {

    private PrivacyAdapter mAdapter;

    public static ChoosePrivacyFragment newInstance(int selectedId) {

        Bundle args = new Bundle();
        args.putInt(ChoosePrivacyIntent.SELECTED_ID, selectedId);

        ChoosePrivacyFragment fragment = new ChoosePrivacyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_choose_privacy;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbar(getBinding().layoutToolbar.toolbar, true);
        getBinding().layoutToolbar.toolbarTitle.setText(getString(R.string.privacy_header_title));
        bindData();
    }

    private void bindData() {
        mAdapter = new PrivacyAdapter(getData(getArguments().getInt(ChoosePrivacyIntent.SELECTED_ID,
                PublishItem.Follower.getId())));
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        getBinding().recyclerView.setAdapter(mAdapter);
    }

    private List<ItemPrivacy> getData(int selectionId) {
        List<ItemPrivacy> itemPrivacyList = UserHelper.getCreatePostPrivacy(requireContext());
        for (ItemPrivacy itemPrivacy : itemPrivacyList) {
            if (itemPrivacy.getPrivacy().getId() == selectionId) {
                itemPrivacy.setSelected(true);
            }
        }

        return itemPrivacyList;
    }

    public ItemPrivacy getSelectionPrivacy() {
        return mAdapter.getSelectedPrivacy();
    }
}
