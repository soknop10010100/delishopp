package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.intent.newintent.HomeIntent;
import com.proapp.sompom.intent.newintent.ReplyCommentShellIntent;
import com.proapp.sompom.listener.OnCommentDialogClickListener;
import com.proapp.sompom.newui.fragment.ReplyCommentFragment;

/**
 * Created by Chhom Veasna on 5/28/2021.
 */
public class ReplyCommentShellActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ReplyCommentFragment replyCommentFragment = ReplyCommentFragment.newInstanceForSubCommentRedirection(new ReplyCommentShellIntent(getIntent()).
                getRedirectionNotificationData());
        replyCommentFragment.setOnCommentDialogClickListener(new OnCommentDialogClickListener() {
            @Override
            public void onDismissDialog(boolean isDismiss) {
                onBackPressed();
            }
        });
        setFragment(replyCommentFragment, ReplyCommentFragment.TAG);
    }

    @Override
    public void finish() {
        if (!MainApplication.isIsHomeScreenOpened()) {
            super.finish();
            ((MainApplication) getApplication()).startFreshHomeScreen(HomeIntent.Redirection.None);
        } else {
            super.finish();
        }
    }
}
