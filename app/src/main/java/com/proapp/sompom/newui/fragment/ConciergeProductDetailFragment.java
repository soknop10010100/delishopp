package com.proapp.sompom.newui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.ConciergeComboDetailAdapter;
import com.proapp.sompom.adapter.ConciergeProductDetailAdapter;
import com.proapp.sompom.databinding.FragmentConciergeProductDetailBinding;
import com.proapp.sompom.databinding.ListItemConciergeOptionBinding;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.helper.ScrimColorAppbarListener;
import com.proapp.sompom.intent.ConciergeProductDetailIntent;
import com.proapp.sompom.model.concierge.ConciergeComboChoiceSection;
import com.proapp.sompom.model.concierge.ConciergeComboItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.emun.ConciergeMenuItemType;
import com.proapp.sompom.model.result.ConciergeMenuItemOption;
import com.proapp.sompom.model.result.ConciergeMenuItemOptionSection;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.newui.dialog.UserLoginRegisterBottomSheetDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeProductDetailFragmentDataManager;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.viewmodel.ConciergeSupportAddItemToCardViewModel;
import com.proapp.sompom.viewmodel.ListItemConciergeOptionViewModel;
import com.proapp.sompom.viewmodel.ListShopConciergeSectionTitleViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeProductDetailFragmentViewModel;
import com.proapp.sompom.widget.LoadMoreLinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 13/9/21.
 */

public class ConciergeProductDetailFragment
        extends AbsHandleConciergeBasketErrorFragment<FragmentConciergeProductDetailBinding>
        implements ConciergeProductDetailFragmentViewModel.ConciergeProductDetailViewModelListener {

    @Inject
    public ApiService mApiService;
    private ConciergeProductDetailFragmentDataManager mDataManager;
    private ConciergeProductDetailFragmentViewModel mViewModel;
    private Context mContext;
    private ConciergeMenuItem mEditOderItem;
    private final List<ConciergeComboDetailAdapter> mConciergeComboDetailAdapterList = new ArrayList<>();
    private final List<ConciergeProductDetailAdapter> mConciergeProductDetailAdapterList = new ArrayList<>();

    public static ConciergeProductDetailFragment newInstance(String menuItemId,
                                                             ConciergeComboItem comboItem,
                                                             boolean isOpenFromCombo) {

        Bundle args = new Bundle();
        args.putString(ConciergeProductDetailIntent.MENU_ITEM_ID, menuItemId);
        args.putParcelable(ConciergeProductDetailIntent.COMBO_ITEM, comboItem);
        args.putBoolean(ConciergeProductDetailIntent.IS_OPEN_FROM_COMBO, isOpenFromCombo);

        ConciergeProductDetailFragment fragment = new ConciergeProductDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ConciergeProductDetailFragment newEditOrderItemInstance(ConciergeMenuItem item,
                                                                          int positionInBasket) {

        Bundle args = new Bundle();
        args.putString(ConciergeProductDetailIntent.MENU_ITEM_ID, item.getId());
        args.putParcelable(ConciergeProductDetailIntent.EDIT_MENU_ITEM, item);
        args.putInt(ConciergeProductDetailIntent.POSITION_IN_BASKET, positionInBasket);

        ConciergeProductDetailFragment fragment = new ConciergeProductDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_PRODUCT_DETAIL;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_product_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getControllerComponent().inject(this);
        mContext = getActivity();
        String menuItemId = getArguments().getString(ConciergeProductDetailIntent.MENU_ITEM_ID);
        boolean isOpenFromCombo = getArguments().getBoolean(ConciergeProductDetailIntent.IS_OPEN_FROM_COMBO);
        mEditOderItem = getArguments().getParcelable(ConciergeProductDetailIntent.EDIT_MENU_ITEM);
        Timber.i("mEditOderItem: " + new Gson().toJson(mEditOderItem));

        mDataManager = new ConciergeProductDetailFragmentDataManager(mContext, mApiService);
        mViewModel = new ConciergeProductDetailFragmentViewModel(requireContext(),
                isOpenFromCombo,
                menuItemId,
                mEditOderItem,
                getArguments().getInt(ConciergeProductDetailIntent.POSITION_IN_BASKET, -1),
                mDataManager,
                this);

        setVariable(BR.viewModel, mViewModel);

        if (isOpenFromCombo) {
            mViewModel.initComboData(getArguments().getParcelable(ConciergeProductDetailIntent.COMBO_ITEM));
        } else {
            mViewModel.getProductDetail(mViewModel.getMenuId());
        }
    }

    @Override
    public void onItemAddedToCart(ConciergeMenuItem item) {
        // Broadcast item being added to cart so any previously opened screen in backstack that are
        // displaying this item is updated with the correct count

        ArrayList<ConciergeMenuItem> broadcastItemList = new ArrayList<>();
        ConciergeMenuItem broadcastItem = new ConciergeMenuItem();
        broadcastItem.setId(item.getId());
        broadcastItem.setProductCount(item.getProductCount());
        broadcastItemList.add(broadcastItem);
        ConciergeHelper.broadcastProductOrderCounterUpdateEvent2(requireContext(), broadcastItemList);

        if (mContext instanceof AbsBaseActivity) {
            Intent intent = new Intent();
            intent.putExtra(ConciergeProductDetailIntent.MENU_ITEM, item);
            ((AbsBaseActivity) mContext).setResult(AppCompatActivity.RESULT_OK, intent);
            ((AbsBaseActivity) mContext).finish();
        }
    }

    private ConciergeProductDetailAdapter getConciergeProductDetailAdapter(String itemId) {
        for (ConciergeProductDetailAdapter adapter : mConciergeProductDetailAdapterList) {
            if (adapter.getDatas() != null && !adapter.getDatas().isEmpty()) {
                for (ConciergeMenuItemOption conciergeMenuItemOption : adapter.getDatas()) {
                    if (TextUtils.equals(conciergeMenuItemOption.getId(), itemId)) {
                        return adapter;
                    }
                }
            }
        }

        return null;
    }

    private ConciergeComboDetailAdapter getConciergeComboDetailAdapterList(String itemId) {
        for (ConciergeComboDetailAdapter adapter : mConciergeComboDetailAdapterList) {
            if (adapter.getDatas() != null && !adapter.getDatas().isEmpty()) {
                for (ConciergeComboItem conciergeMenuItemOption : adapter.getDatas()) {
                    if (TextUtils.equals(conciergeMenuItemOption.getId(), itemId)) {
                        return adapter;
                    }
                }
            }
        }

        return null;
    }

    private void checkToBindEditOrderItem(ConciergeMenuItem conciergeMenuItem) {
        if (mEditOderItem != null) {
            if (mEditOderItem.getShop() == null) {
                mEditOderItem.setShop(conciergeMenuItem.getShop());
            }

            Timber.i("Edit order item.");
            if (mEditOderItem.getOptions() != null &&
                    !mEditOderItem.getOptions().isEmpty()) {
                new Handler().postDelayed(() -> {
                    for (ConciergeMenuItemOptionSection optionSection : mEditOderItem.getOptions()) {
                        if (optionSection.getOptionItems() != null && !optionSection.getOptionItems().isEmpty()) {
                            for (ConciergeMenuItemOption optionItem : optionSection.getOptionItems()) {
                                ConciergeProductDetailAdapter adapter = getConciergeProductDetailAdapter(optionItem.getId());
                                if (adapter != null) {
                                    adapter.makeItemSelection(optionItem.getId());
                                }
                            }
                        }
                    }
                }, 100);
            }

            if (mEditOderItem.getChoices() != null &&
                    !mEditOderItem.getChoices().isEmpty()) {
                new Handler().postDelayed(() -> {
                    for (ConciergeComboChoiceSection optionSection : mEditOderItem.getChoices()) {
                        if (optionSection.getList() != null && !optionSection.getList().isEmpty()) {
                            for (ConciergeComboItem comboItem : optionSection.getList()) {
                                ConciergeComboDetailAdapter adapter = getConciergeComboDetailAdapterList(comboItem.getId());
                                if (adapter != null) {
                                    String preselectionOptionId = null;
                                    if (comboItem.getOptions() != null) {
                                        for (ConciergeMenuItemOptionSection option : comboItem.getOptions()) {
                                            if (!TextUtils.isEmpty(preselectionOptionId)) {
                                                break;
                                            }

                                            if (option.getOptionItems() != null) {
                                                for (ConciergeMenuItemOption optionItem : option.getOptionItems()) {
                                                    if (optionItem.isSelected()) {
                                                        preselectionOptionId = optionItem.getId();
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    adapter.makeItemSelection(comboItem, preselectionOptionId);
                                }
                            }
                        }
                    }
                }, 100);
            }
        }
    }

    @Override
    public void onLoadDetailSuccess(ConciergeMenuItem item) {
        getBinding().productTitle.setViewModel(new ListShopConciergeSectionTitleViewModel(mViewModel.getProductName().get()));
        getBinding().productTitle.titleTextView.setTextColor(AttributeConverter.convertAttrToColor(getContext(), R.attr.shop_item_detail_form_title));
        getBinding().appBar.addOnOffsetChangedListener(new ScrimColorAppbarListener(getBinding().collapse) {
            @Override
            public void onStateChange(State state) {
                mViewModel.setIsToolbarCollapsed(state == State.Collapse);
            }
        });

        if (item.getType() == ConciergeMenuItemType.ITEM) {
            initItemOptions(item);
        } else if (item.getType() == ConciergeMenuItemType.COMBO) {
            initComboChoices(item);
        }
        checkToBindEditOrderItem(item);
    }

    @Override
    public void onRequiredItemNotSelected() {
        if (mViewModel.getMenuItemType() == ConciergeMenuItemType.ITEM) {
            mConciergeProductDetailAdapterList.forEach(ConciergeProductDetailAdapter::checkShouldShowError);
        } else if (mViewModel.getMenuItemType() == ConciergeMenuItemType.COMBO) {
            mConciergeComboDetailAdapterList.forEach(ConciergeComboDetailAdapter::checkShouldShowError);
        }
    }

    @Override
    protected boolean shouldHandleReceivingBasketModificationErrorFromAsynchronousProcess() {
        //Will handle in this screen directly
        return false;
    }

    @Override
    public void onHandlePaymentInProgressError(ConciergeSupportAddItemToCardViewModel viewModel,
                                               String errorMessage,
                                               ConciergeMenuItem menuItem,
                                               boolean isUpdateItemProcess) {
        handleOnPaymentInProcessError(errorMessage, new handleOnPaymentInProcessErrorListener() {
            @Override
            public void onEnableBasketSuccess() {
                //Resume add or update basket item action.
                if (isUpdateItemProcess) {
                    mViewModel.requestToUpdateOrRemoveItemInBasket(menuItem, false);
                } else {
                    mViewModel.requestAddItemToBasket(menuItem, false);
                }
            }

            @Override
            public void onEnableBasketFailed(ErrorThrowable ex) {
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onRequestBasketFailed(ErrorThrowable ex) {
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void makePaymentWithABAFailed(String errorMessage) {
                showSnackBar(errorMessage, true);
            }

            @Override
            public void makePaymentWithABASuccess() {
                Timber.i("makePaymentWithABASuccess");
                requireActivity().finish();
            }
        });
    }

    @Override
    protected ApiService getPrivateAPIService() {
        return mApiService;
    }

    @Override
    protected void showLoading() {
        mViewModel.showLoading(true);
    }

    @Override
    protected void hideLoading() {
        mViewModel.hideLoading();
    }

    public void initItemOptions(ConciergeMenuItem item) {
        List<ConciergeMenuItemOptionSection> optionList = item.getOptions();
        if (optionList != null && !optionList.isEmpty()) {
            for (ConciergeMenuItemOptionSection itemOption : optionList) {
                ListItemConciergeOptionBinding binding =
                        DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.list_item_concierge_option,
                                null,
                                false);
                final ConciergeProductDetailAdapter adapter = new ConciergeProductDetailAdapter(itemOption, item.getPrice());
                ListItemConciergeOptionViewModel viewModel = new ListItemConciergeOptionViewModel(mContext,
                        itemOption,
                        new ListItemConciergeOptionViewModel.ListItemConciergeVariationListener() {
                            @Override
                            public void onItemOptionUpdated(ConciergeMenuItemOptionSection itemOption) {
                                mViewModel.onOptionItemSelected(itemOption);
                                adapter.updateItemOptionData(itemOption.getOptionItems());
                            }

                            @Override
                            public void onComboItemUpdated(ConciergeComboChoiceSection comboSection) {

                            }
                        });

                mConciergeProductDetailAdapterList.add(adapter);
                adapter.setListener(viewModel::onItemOptionClick);

                LoadMoreLinearLayoutManager loadMoreLinearLayoutManager =
                        new LoadMoreLinearLayoutManager(requireActivity(), binding.variationRecycler);
                binding.variationRecycler.setLayoutManager(loadMoreLinearLayoutManager);
                binding.variationRecycler.setAdapter(adapter);
                binding.setViewModel(viewModel);

                getBinding().optionList.addView(binding.getRoot());
                if (getBinding().optionList.getChildCount() > 0) {
                    getBinding().optionList.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void initComboChoices(ConciergeMenuItem combo) {
        List<ConciergeComboChoiceSection> comboSections = combo.getChoices();
        if (comboSections != null && !comboSections.isEmpty()) {
            for (ConciergeComboChoiceSection comboSection : comboSections) {
                ListItemConciergeOptionBinding binding =
                        DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                                R.layout.list_item_concierge_option,
                                null,
                                false);

                final ConciergeComboDetailAdapter adapter = new ConciergeComboDetailAdapter(comboSection, combo.getPrice());
                ListItemConciergeOptionViewModel viewModel = new ListItemConciergeOptionViewModel(mContext,
                        comboSection,
                        new ListItemConciergeOptionViewModel.ListItemConciergeVariationListener() {
                            @Override
                            public void onItemOptionUpdated(ConciergeMenuItemOptionSection itemOption) {
                            }

                            @Override
                            public void onComboItemUpdated(ConciergeComboChoiceSection comboSection) {
                                Timber.i("onComboItemUpdated: " + new Gson().toJson(comboSection));
                                mViewModel.onComboItemSelected(comboSection);
                                adapter.updateComboData(comboSection.getList());
                            }
                        });

                mConciergeComboDetailAdapterList.add(adapter);
                adapter.setListener(viewModel::onComboChoiceClick);

                LoadMoreLinearLayoutManager loadMoreLinearLayoutManager =
                        new LoadMoreLinearLayoutManager(requireActivity(), binding.variationRecycler);
                binding.variationRecycler.setLayoutManager(loadMoreLinearLayoutManager);
                binding.variationRecycler.setAdapter(adapter);
                binding.setViewModel(viewModel);

                getBinding().optionList.addView(binding.getRoot());
                if (getBinding().optionList.getChildCount() > 0) {
                    getBinding().optionList.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onRequireLogin() {
        UserLoginRegisterBottomSheetDialog dialog = UserLoginRegisterBottomSheetDialog.newInstance();
        dialog.show(getChildFragmentManager(), dialog.getTag());
    }

    public void onBackPressOfActivity(AppCompatActivity appCompatActivity) {
        if (mViewModel != null && mViewModel.isHasModifiedProductCount()) {
            MessageDialog messageDialog = MessageDialog.newInstance();
            messageDialog.setTitle(appCompatActivity.getString(R.string.shop_checkout_alert_error));
            messageDialog.setMessage(appCompatActivity.getString(R.string.shop_item_counter_update_alert));
            messageDialog.setLeftText(appCompatActivity.getString(R.string.popup_ok_button), view -> {
                mViewModel.onAddToCartClicked();
            });
            messageDialog.setRightText(appCompatActivity.getString(R.string.popup_cancel_button), view -> appCompatActivity.finish());
            messageDialog.show(appCompatActivity.getSupportFragmentManager());
        } else {
            appCompatActivity.finish();
        }
    }
}
