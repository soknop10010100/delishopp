package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.SearchGeneralAdapter;
import com.proapp.sompom.databinding.FragmentSearchGeneralBinding;
import com.proapp.sompom.helper.NavigateSellerStoreHelper;
import com.proapp.sompom.helper.UserSearchDataHolder;
import com.proapp.sompom.listener.OnMessageItemClick;
import com.proapp.sompom.model.ActiveUser;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.SearchGeneralDataManager;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.viewmodel.SearchGeneralFragmentViewModel;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 8/2/18.
 */

public class SearchGeneralFragment extends AbsBindingFragment<FragmentSearchGeneralBinding> implements OnMessageItemClick {

    @Inject
    public ApiService mApiService;
    private SearchGeneralAdapter mAdapter;

    public static SearchGeneralFragment newInstance() {
        return new SearchGeneralFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search_general;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        UserSearchDataHolder.getInstance().loadUserContact(requireContext());
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getBinding().recyclerView.setOnTouchListener((v, event) -> {
            KeyboardUtil.hideKeyboard(getActivity());
            v.performClick();
            return false;
        });
        SearchGeneralDataManager dataManager = new SearchGeneralDataManager(getActivity(), mApiService);
        SearchGeneralFragmentViewModel viewModel = new SearchGeneralFragmentViewModel(dataManager,
                result -> setData(result));
        setVariable(com.proapp.sompom.BR.viewModel, viewModel);
    }

    @Override
    public void onConversationUserClick(User user) {
        NavigateSellerStoreHelper.openSellerStore((AppCompatActivity) Objects.requireNonNull(getContext()),
                user.getId());
    }

    private void setData(List<ActiveUser> result) {
        if (mAdapter == null) {
            mAdapter = new SearchGeneralAdapter(result, SearchGeneralFragment.this);
            SearchGeneralFragment.this.getBinding().recyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.refreshData(result);
        }
    }
}
