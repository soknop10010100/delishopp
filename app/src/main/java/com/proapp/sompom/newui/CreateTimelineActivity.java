package com.proapp.sompom.newui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ActivityCreateTimelineBinding;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.intent.CreateTimelineIntent;
import com.proapp.sompom.intent.newintent.HomeIntent;
import com.proapp.sompom.newui.fragment.CreateTimelineFragment;
import com.proapp.sompom.utils.ToastUtil;

public class CreateTimelineActivity extends AbsBindingActivity<ActivityCreateTimelineBinding> {

    @Override
    public int getLayoutRes() {
        return R.layout.activity_create_timeline;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startLoginWizardActivity(new LoginActivityResultCallback() {
            @Override
            public void onActivityResultSuccess(boolean isAlreadyLogin) {
                if (UserHelper.isAbleToCreatePost(CreateTimelineActivity.this)) {
                    CreateTimelineIntent intent = new CreateTimelineIntent(CreateTimelineActivity.this,
                            getIntent());
                    setFragment(R.id.container_view,
                            CreateTimelineFragment.newInstance(intent.getLifeStream(),
                                    intent.getScreenTypeId(),
                                    intent.isFromOtherAppContent()),
                            CreateTimelineFragment.TAG);
                } else {
                    HomeIntent homeIntent = new HomeIntent(CreateTimelineActivity.this);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(homeIntent);
                    finish();
                    new Handler().postDelayed(() -> {
                        ToastUtil.showToast(CreateTimelineActivity.this,
                                R.string.create_wall_no_right_to_create_from_external_sharing, true);
                    }, 200);
                }
            }

            @Override
            public void onActivityResultFail() {
                finish();
            }
        });
    }
}
