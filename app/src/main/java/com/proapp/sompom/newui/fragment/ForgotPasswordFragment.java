package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentForgotPasswordBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ResetPasswordDataManager;
import com.proapp.sompom.viewmodel.ForgotPasswordViewModel;

import javax.inject.Inject;

/**
 * Created by Veasna Chhom on 5/17/22.
 */
public class ForgotPasswordFragment extends AbsBindingFragment<FragmentForgotPasswordBinding> {

    @Inject
    @PublicQualifier
    public ApiService mPublicAPIService;
    private ForgotPasswordViewModel mViewModel;

    public static ForgotPasswordFragment newInstance() {

        Bundle args = new Bundle();

        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_FORGOT_PASSWORD;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_forgot_password;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mViewModel = new ForgotPasswordViewModel(requireContext(),
                new ResetPasswordDataManager(requireContext(), mPublicAPIService));
        setVariable(BR.viewModel, mViewModel);
    }
}
