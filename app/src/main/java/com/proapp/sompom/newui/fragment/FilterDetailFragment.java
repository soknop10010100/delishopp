package com.proapp.sompom.newui.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.View;
import android.widget.RelativeLayout;

import com.proapp.sompom.adapter.FilterCategoryAdapter;
import com.proapp.sompom.intent.newintent.FilterCategoryIntent;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.widget.MyMultiSlider;
import com.sompom.baseactivity.ResultCallback;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentFilterDetailBinding;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.model.Price;
import com.proapp.sompom.model.result.Category;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.PriceDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.FilterDetailFragmentViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 7/5/18.
 */

public class FilterDetailFragment extends AbsBindingFragment<FragmentFilterDetailBinding> {
    @Inject
    @PublicQualifier
    public ApiService mApiService;

    private ArrayList<Category> mCategories = new ArrayList<>();
    private FilterCategoryAdapter mCategoryAdapter;
    private FilterDetailFragmentViewModel mViewModel;
    private int mDropdownImage1PaddingLeft;
    private int mDropdownImage2PaddingRight;

    public static FilterDetailFragment newInstance() {
        return new FilterDetailFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_filter_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        setMarginDropDownImage();

        PriceDataManager dataManager = new PriceDataManager(getContext(), mApiService);
        mViewModel = new FilterDetailFragmentViewModel(getActivity(), dataManager, queryProduct -> {
            if (queryProduct != null) {
                List<Category> categories = queryProduct.getCategories();
                if (categories != null && !categories.isEmpty()) {
                    mCategories.addAll(categories);
                }
            }
            initAdapter();
        }, this::initMultiSlider);
        setVariable(BR.viewModel, mViewModel);

        getBinding().containerCategory.setOnClickListener(v -> chooseCategory());
    }

    private void initAdapter() {
        mCategoryAdapter = new FilterCategoryAdapter(mCategories);
        mCategoryAdapter.setOnCategoryClickListener(category -> chooseCategory());
        mCategoryAdapter.setDisableMultiCheck(true);
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getBinding().recyclerView.setAdapter(mCategoryAdapter);
    }

    private void chooseCategory() {
        if (getActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getActivity()).startActivityForResult(new FilterCategoryIntent(getActivity(), mCategories, true),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            if (mCategories != null) {
                                mCategories.clear();
                            }
                            mCategories = data.getParcelableArrayListExtra(SharedPrefUtils.CATEGORY);
                            mCategoryAdapter.notifyDataSetChange(mCategories);
                            mViewModel.setCategories(mCategories);
                        }
                    });
        }
    }

    private void initMultiSlider(Price price) {
        getBinding().rangePriceSlider.setMinMax(price.getMinPrice(), price.getMaxPrice());
        getBinding().rangePriceSlider.setSlideChangeListener(new MyMultiSlider.SlideChangeListener() {
            @Override
            public void onMinChange(int scrollInt, double value) {
                changeMinPriceSelected(scrollInt, value);
            }

            @Override
            public void onMaxChange(int scrollInt, double value) {
                changeMaxPriceSelected(scrollInt, value);
            }
        });
    }

    private void changeMinPriceSelected(int value, double price) {
        mViewModel.setMinPrice(value, price);
        getBinding().dropDownImage1.post(() -> {
            if (mDropdownImage1PaddingLeft > 0) {
                mDropdownImage1PaddingLeft = 0;
                shouldAddRemovePaddingLeft(getBinding().dropDownImage1, mDropdownImage1PaddingLeft);
            }
            float min = getBinding().rangePriceSlider.getThumb(0).getThumb().getBounds().exactCenterX()
                    + getBinding().rangePriceSlider.getLeft()
                    - getBinding().dropDownImage1.getWidth() / 2;
            getBinding().dropDownImage1.setTranslationX(min);
        });
    }

    private void changeMaxPriceSelected(int value, double price) {
        mViewModel.setMaxPrice(value, price);
        getBinding().dropDownImage2.post(() -> {
            if (mDropdownImage2PaddingRight > 0) {
                mDropdownImage2PaddingRight = 0;
                shouldAddRemovePaddingLeft(getBinding().dropDownImage2, mDropdownImage2PaddingRight);
            }
            float max1 = getBinding().rangePriceSlider.getThumb(1).getThumb().getBounds().exactCenterX()
                    + getBinding().rangePriceSlider.getLeft()
                    - getBinding().dropDownImage2.getWidth() / 2;
            getBinding().dropDownImage2.setTranslationX(max1);
        });
    }

    private void setMarginDropDownImage() {
        getBinding().dropDownImage1.post(() -> {
            float margin = getBinding().rangePriceSlider.getThumb(0).getThumb().getBounds().exactCenterX()
                    + getBinding().rangePriceSlider.getLeft()
                    - getBinding().dropDownImage1.getWidth() / 2;
            mDropdownImage1PaddingLeft = (int) margin;
            shouldAddRemovePaddingLeft(getBinding().dropDownImage1, mDropdownImage1PaddingLeft);
        });

        getBinding().dropDownImage2.post(() -> {
            float margin = getBinding().rangePriceSlider.getThumb(1).getThumb().getBounds().exactCenterX()
                    + getBinding().rangePriceSlider.getLeft()
                    - getBinding().dropDownImage2.getWidth() / 2;
            mDropdownImage2PaddingRight = (int) margin;
            shouldAddRemovePaddingLeft(getBinding().dropDownImage2, mDropdownImage2PaddingRight);
        });

        float addContainerLinePriceMargin = getBinding().rangePriceSlider.getThumb(0).getThumbOffset();
        addContainerLinePriceMargin((int) addContainerLinePriceMargin);
    }

    private void shouldAddRemovePaddingLeft(View view, int padding) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(padding, 0, 0, 0);
        view.setLayoutParams(params);
    }

    private void addContainerLinePriceMargin(int margin) {
        getBinding().containerLinePrice.post(() -> {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(margin, 0, margin, 0);
            params.addRule(RelativeLayout.BELOW, getBinding().containerSeekBar.getId());
            getBinding().containerLinePrice.setLayoutParams(params);
        });
    }
}
