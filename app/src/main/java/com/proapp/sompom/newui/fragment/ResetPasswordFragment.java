package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentResetPasswordBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ResetPasswordDataManager;
import com.proapp.sompom.viewmodel.ResetPasswordViewModel;

import javax.inject.Inject;

/**
 * Created by Veasna Chhom on 5/18/22.
 */
public class ResetPasswordFragment extends AbsBindingFragment<FragmentResetPasswordBinding> {

    @Inject
    @PublicQualifier
    public ApiService mPublicAPIService;
    private ResetPasswordViewModel mViewModel;

    public static ResetPasswordFragment newInstance(String email) {

        Bundle args = new Bundle();
        args.putString(ResetPasswordViewModel.FIELD_EMAIL, email);

        ResetPasswordFragment fragment = new ResetPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_RESET_PASSWORD;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_reset_password;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mViewModel = new ResetPasswordViewModel(requireContext(),
                getArguments().getString(ResetPasswordViewModel.FIELD_EMAIL),
                new ResetPasswordDataManager(requireContext(), mPublicAPIService));
        setVariable(BR.viewModel, mViewModel);
    }
}
