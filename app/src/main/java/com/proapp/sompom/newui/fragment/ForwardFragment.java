package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.desmond.squarecamera.utils.Keys;
import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ForwardAdapter;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.databinding.FragmentForwardBinding;
import com.proapp.sompom.listener.ConversationDataAdaptive;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ForwardDataManager;
import com.proapp.sompom.utils.GenerateLinkPreviewUtil;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.viewmodel.newviewmodel.ForwardViewModel;
import com.proapp.sompom.widget.LoaderMoreLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by He Rotha on 9/13/18.
 */
public class ForwardFragment extends AbsBindingFragment<FragmentForwardBinding>
        implements ForwardViewModel.OnViewModelCallback {

    @Inject
    public ApiService mApiService;

    private ForwardAdapter mForwardAdapter;
    private ForwardViewModel mViewModel;

    public static ForwardFragment newInstance(Chat chat,
                                              List<Media> media,
                                              boolean isFromOutSideApp) {
        ArrayList<Media> media1 = (ArrayList<Media>) media;
        Bundle args = new Bundle();
        args.putParcelable(Keys.DATA, chat);
        args.putParcelableArrayList(Keys.PATH, media1);
        args.putBoolean(Keys.SOURCE_TYPE, isFromOutSideApp);
        ForwardFragment fragment = new ForwardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_forward;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        LoaderMoreLayoutManager loaderMoreLayoutManager = new LoaderMoreLayoutManager(getActivity());
        getBinding().recyclerView.setLayoutManager(loaderMoreLayoutManager);
        getBinding().recyclerView.setOnTouchListener((view1, motionEvent) -> {
            KeyboardUtil.hideKeyboard(getActivity());
            view1.performClick();
            return false;
        });
        SimpleItemAnimator simpleItemAnimator = (SimpleItemAnimator) getBinding().recyclerView.getItemAnimator();
        if (simpleItemAnimator != null) {
            simpleItemAnimator.setSupportsChangeAnimations(false);
        }

        mForwardAdapter = new ForwardAdapter();
        mForwardAdapter.setOnForwardItemClickListener(user -> {
            return mViewModel.onForwardClick(user);
        });
        getBinding().recyclerView.setAdapter(mForwardAdapter);

        if (getArguments() != null) {
            Chat chat = getArguments().getParcelable(Keys.DATA);
            ArrayList<Media> media = getArguments().getParcelableArrayList(Keys.PATH);
            Timber.i("media: " + new Gson().toJson(media));
            boolean isFromOutSideApp = getArguments().getBoolean(Keys.SOURCE_TYPE);
            String content = "";
            if (chat != null) {
                content = chat.getContent();
            }

            final ForwardDataManager manager = new ForwardDataManager(getActivity(), mApiService);
            mViewModel = new ForwardViewModel((AbsBaseActivity) getActivity(),
                    chat,
                    content,
                    media,
                    isFromOutSideApp,
                    manager,
                    this);

            // Download link from inside app
            if (chat != null && chat.getLinkPreviewModel() != null) {
                if (chat.getLinkPreviewModel().isLinkDownloaded()) {
                    mViewModel.setDisplayLink(chat.getContent(), chat.getLinkPreviewModel());
                } else {
                    startDownloadLinkPreview(chat.getLinkPreviewModel().getLink());
                }
            } else {
                // Download link from other app
                String previewLink = GenerateLinkPreviewUtil.getPreviewLink(content);
                if (chat != null &&
                        isFromOutSideApp
                        && !TextUtils.isEmpty(content)
                        && GenerateLinkPreviewUtil.isValidUrl(previewLink)) {
                    startDownloadLinkPreview(content);
                }
            }
            setVariable(BR.viewModel, mViewModel);
        }
    }

    private void startDownloadLinkPreview(String link) {
        mViewModel.requestLinkPreview(link);
    }

    @Override
    public void onLoadSuggestPeopleComplete(List<ConversationDataAdaptive> suggest) {
        mForwardAdapter.setCanLoadMore(true);
        mForwardAdapter.addSuggestion(suggest);
    }

    @Override
    public void onLoadPeopleComplete(List<ConversationDataAdaptive> people, boolean isLoadMore) {
        mForwardAdapter.setCanLoadMore(isLoadMore);
        mForwardAdapter.addPeople(people);
    }

    @Override
    public void onLoadGroupComplete(List<ConversationDataAdaptive> group, boolean isLoadMore) {
//        Timber.i("onLoadGroupComplete: " + new Gson().toJson(group));
        if (group != null && !group.isEmpty()) {
            mForwardAdapter.setCanLoadMore(isLoadMore);
            mForwardAdapter.addGroup(group);
        }
    }

    @Override
    public void onSearchSuccess(List<ConversationDataAdaptive> groupResult, List<ConversationDataAdaptive> peopleResult) {
        mForwardAdapter.updateSearchResult(groupResult, peopleResult);
    }

    @Override
    public void onForwardComplete(String messageId, MessageState messageState) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> mForwardAdapter.updatePeopleSearch(messageId, messageState));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
