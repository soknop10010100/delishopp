package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.ConciergeShopProductByCategoryAdapter;
import com.proapp.sompom.databinding.FragmentConciergeBrandDetailBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.intent.ConciergeBrandDetailIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.SupportConciergeCheckoutBottomSheetDisplay;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeSubCategoryLoadMoreDataModel;
import com.proapp.sompom.model.emun.ConciergeSortItemOption;
import com.proapp.sompom.model.emun.ConciergeViewItemByType;
import com.proapp.sompom.model.emun.OpenBrandDetailType;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeBrandDetailDataManager;
import com.proapp.sompom.viewmodel.ConciergeBrandDetailFragmentViewModel;
import com.proapp.sompom.viewmodel.ConciergeSupportAddItemToCardViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 5/4/22.
 */

public class ConciergeBrandDetailFragment extends AbsSupportConciergeViewItemByFragment<FragmentConciergeBrandDetailBinding>
        implements ConciergeBrandDetailFragmentViewModel.ConciergeBrandDetailFragmentViewModelListener,
        SupportConciergeCheckoutBottomSheetDisplay {

    @Inject
    public ApiService mApiService;
    private ConciergeBrandDetailFragmentViewModel mViewModel;

    public static ConciergeBrandDetailFragment newInstance(String passParam, String titleParam, OpenBrandDetailType openBrandDetailType) {
        Bundle args = new Bundle();
        args.putString(ConciergeBrandDetailIntent.TEXT_PARAM, passParam);
        args.putString(ConciergeBrandDetailIntent.TITLE_PARAM, titleParam);
        args.putInt(ConciergeBrandDetailIntent.OPEN_TYPE, openBrandDetailType.getValue());

        ConciergeBrandDetailFragment fragment = new ConciergeBrandDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_brand_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        OpenBrandDetailType openBrandDetailType = OpenBrandDetailType.fromValue(getArguments().getInt(ConciergeBrandDetailIntent.OPEN_TYPE));
        logFlurryEvent(openBrandDetailType);
        mViewModel = new ConciergeBrandDetailFragmentViewModel(requireContext(),
                new ConciergeBrandDetailDataManager(requireContext(),
                        mApiService,
                        getArguments().getString(ConciergeBrandDetailIntent.TEXT_PARAM),
                        getArguments().getString(ConciergeBrandDetailIntent.TITLE_PARAM),
                        openBrandDetailType),
                getCheckOutViewModelInstance(),
                this);
        setVariable(BR.viewModel, mViewModel);
    }

    private void logFlurryEvent(OpenBrandDetailType type) {
        if (type == OpenBrandDetailType.OPEN_AS_NORMAL_MODE) {
            FlurryHelper.logEvent(requireContext(), FlurryHelper.SCREEN_BRAND_DETAIL);
        } else if (type == OpenBrandDetailType.OPEN_AS_SEARCH_SUPPLIER_PRODUCT_MODE) {
            FlurryHelper.logEvent(requireContext(), FlurryHelper.SCREEN_SEARCH_SUPPLIER_ITEM_EXPRESS);
        } else if (type == OpenBrandDetailType.OPEN_AS_SEARCH_GENERAL_PRODUCT_MODE) {
            FlurryHelper.logEvent(requireContext(), FlurryHelper.SCREEN_SEARCH_GENERAL_PRODUCT);
        }
    }

    @Override
    protected RecyclerView getRecycleView() {
        return getBinding().recyclerView;
    }

    @Override
    protected AppCompatSpinner getSortSpinner() {
        return getBinding().sortSpinner;
    }

    @Override
    protected void onReachedLoadMoreBottom() {
        mViewModel.loadMoreItem();
    }

    @Override
    protected void onReachedLoadMoreByDefault() {
        mViewModel.loadMoreItem();
    }

    @Override
    public void onViewItemByOptionChanged(ConciergeViewItemByType selectedOption) {
        super.onViewItemByOptionChanged(selectedOption);
    }

    @Override
    protected void onRebuildItemListWhenViewItemByOptionChanged(List<ConciergeShopDetailDisplayAdaptive> existingData, boolean canLoadMore) {
        bindItemList(existingData, true, false, false, canLoadMore);
    }

    @Override
    public void onLoadItemSuccess(List<ConciergeMenuItem> itemList,
                                  boolean isRefreshMode,
                                  boolean isLoadMore,
                                  boolean isCanLoadMore) {
        bindItemList(new ArrayList<>(itemList), false, isRefreshMode, isLoadMore, isCanLoadMore);
    }

    @Override
    public void onLoadMoreItemFailed() {
        super.onLoadMoreItemFailed();
    }

    @Override
    protected void onItemSortOptionChanged(ConciergeSortItemOption option) {
        mViewModel.onApplySort(option);
    }

    private void bindItemList(List<ConciergeShopDetailDisplayAdaptive> itemList,
                              boolean isViewByOptionChanged,
                              boolean isRefreshMode,
                              boolean isLoadMore,
                              boolean isCanLoadMore) {
        Timber.i("itemList size: " + itemList.size()
                + ", isRefreshMode: " + isRefreshMode
                + ", isLoadMore: " + isLoadMore
                + ", isCanLoadMore: " + isCanLoadMore);
        if (mMainItemAdapter == null || isViewByOptionChanged) {
            clearPreviousLayoutManagerIfNecessary();
            mMainItemAdapter = new ConciergeShopProductByCategoryAdapter(requireContext(),
                    mConciergeViewItemByType,
                    null,
                    new ArrayList<>(itemList),
                    new ConciergeShopProductByCategoryAdapter.ShopProductAdapterCallback() {
                        @Override
                        public void onProductAddedFromDetail() {

                        }

                        @Override
                        public void onSubCategoryPerformLoadMoreItem(ConciergeSubCategoryLoadMoreDataModel loadMoreDataModel) {

                        }

                        @Override
                        public void onViewItemByOptionChanged(ConciergeViewItemByType selectedOption) {

                        }

                        @Override
                        public void onExpandSubCategory(int position) {

                        }

                        @Override
                        public void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {
                            ConciergeBrandDetailFragment.this.onAddFirstItemToCart(mViewModel, conciergeMenuItem);
                        }

                        @Override
                        public void showAddItemFromDifferentShopWarningPopup(ConciergeSupportAddItemToCardViewModel viewModel, OnCallbackListener<Response<SupportConciergeCustomErrorResponse>> clearBasketCallback) {
                            ConciergeBrandDetailFragment.this.showAddItemFromDifferentShopWarningPopup(viewModel, clearBasketCallback);
                        }
                    });
            mMainItemAdapter.setConciergeViewItemByType(mConciergeViewItemByType);
            mCurrentLayoutManager = getLayoutManager(isCanLoadMore);
            mCurrentLayoutManager.setShouldDetectLoadMore(isCanLoadMore);
            mMainItemAdapter.setCanLoadMore(isCanLoadMore);
            getBinding().recyclerView.setLayoutManager((RecyclerView.LayoutManager) mCurrentLayoutManager);
            checkToAddOrRemoveItemDecorator();
            getBinding().recyclerView.setAdapter(mMainItemAdapter);
        } else {
            mCurrentLayoutManager.setLoadMoreLinearLayoutManagerListener(isCanLoadMore ? mLayoutManagerListener : null);
            mMainItemAdapter.setCanLoadMore(isCanLoadMore);
            mCurrentLayoutManager.setShouldDetectLoadMore(isCanLoadMore);
            if (isRefreshMode) {
                mMainItemAdapter.setDatas(itemList);
                mMainItemAdapter.notifyDataSetChanged();
            } else {
                if (isLoadMore) {
                    mCurrentLayoutManager.loadingFinished();
                }
                mMainItemAdapter.addLoadMoreData(itemList);
            }
        }
    }

    @Override
    public void onItemAddedToCart(ConciergeMenuItem conciergeMenuItem) {
        Timber.i("onItemAddedToCart: " + conciergeMenuItem);
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onItemUpdatedInCart(conciergeMenuItem);
        }
        notifyShouldShowCartBottomSheet();
    }

    @Override
    protected void onItemRemovedFromCart(ConciergeMenuItem conciergeMenuItem) {
        Timber.i("onItemRemovedFromCart: " + conciergeMenuItem);
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onItemUpdatedInCart(conciergeMenuItem);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.i("OnResume");
        notifyShouldShowCartBottomSheet();
    }

    @Override
    protected void onCartItemCleared() {
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onCartCleared();
        }
    }
}
