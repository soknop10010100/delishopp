package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentSelectAddressBinding;
import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.SelectAddressFragmentViewModel;

/**
 * Created by nuonveyo on 5/10/18.
 */

public class SelectAddressFragment extends AbsMapFragment<FragmentSelectAddressBinding>
        implements OnMapReadyCallback {
    public static final String TAG = SelectAddressFragment.class.getName();

    private SearchAddressResult mSearchAddressResult;

    public static SelectAddressFragment newInstance(SearchAddressResult searchAddressResult) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SharedPrefUtils.DATA, searchAddressResult);
        SelectAddressFragment fragment = new SelectAddressFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_select_address;
    }

    @Override
    protected void onViewAndMapCreated() {
        if (getArguments() != null) {
            mSearchAddressResult = getArguments().getParcelable(SharedPrefUtils.DATA);
        }

        GoogleMap googleMap = getGoogleMap();

        AppTheme theme = ThemeManager.getAppTheme(requireContext());
        if (theme == AppTheme.Black) {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.google_map_dark));
        } else if (theme == AppTheme.White) {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.google_map_light));
        }

        int paddingTop = getResources().getInteger(R.integer.google_map_select_address_padding_top);
        googleMap.setPadding(0, paddingTop, 0, 0);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        if (isTherePreviousSelection()) {
            AnimationHelper.slideUp(getBinding().address);
        } else {
            AnimationHelper.slideDown(getBinding().address);
        }
        SelectAddressFragmentViewModel viewModel = new SelectAddressFragmentViewModel(getActivity(),
                googleMap,
                mSearchAddressResult,
                () -> AnimationHelper.slideUp(getBinding().address));
        setVariable(BR.viewModel, viewModel);
    }

    private boolean isTherePreviousSelection() {
        return mSearchAddressResult != null &&
                !TextUtils.isEmpty(mSearchAddressResult.getAddress()) &&
                mSearchAddressResult.getLocations() != null;
    }
}
