package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ActivityDefaultBinding;
import com.proapp.sompom.intent.newintent.GroupDetailMediaContainerIntent;
import com.proapp.sompom.newui.fragment.GroupDetailMediaContainerFragment;

/**
 * Created by Chhom Veasna on 7/24/2020.
 */
public class GroupDetailMediaContainerActivity extends AbsSoundControllerActivity<ActivityDefaultBinding> {

    @Override
    public int getLayoutRes() {
        return R.layout.activity_default;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GroupDetailMediaContainerIntent intent = new GroupDetailMediaContainerIntent(getIntent());
        setFragment(R.id.containerView,
                GroupDetailMediaContainerFragment.newInstance(intent.getGroupDetail(),
                        intent.getIndex()),
                GroupDetailMediaContainerFragment.class.getName());
    }
}
