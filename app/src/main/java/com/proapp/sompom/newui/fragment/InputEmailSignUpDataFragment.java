package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentInputEmailSignupDataBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.intent.InputEmailSignUpDataIntent;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.InputEmailSignUpDataManager;
import com.proapp.sompom.viewmodel.InputEmailSignUpDataViewModel;

import javax.inject.Inject;

/**
 * Created by Veasna Chhom on 2/10/22.
 */
public class InputEmailSignUpDataFragment extends AbsBindingFragment<FragmentInputEmailSignupDataBinding> {

    @Inject
    @PublicQualifier
    public ApiService mPubicApiService;
    @Inject
    public ApiService mPrivateApiService;
    private InputEmailSignUpDataViewModel mViewModel;

    public static InputEmailSignUpDataFragment newInstance(String email) {

        Bundle args = new Bundle();
        args.putString(InputEmailSignUpDataIntent.EMAIL, email);

        InputEmailSignUpDataFragment fragment = new InputEmailSignUpDataFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_SIGN_UP;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_input_email_signup_data;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mViewModel = new InputEmailSignUpDataViewModel(requireContext(),
                new InputEmailSignUpDataManager(requireContext(),
                        mPubicApiService,
                        mPrivateApiService,
                        getArguments().getString(InputEmailSignUpDataIntent.EMAIL)));
        setVariable(BR.viewModel, mViewModel);
    }
}
