package com.proapp.sompom.newui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.proapp.sompom.helper.ConversationHelper;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.utils.NotificationUtils;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.AbsBaseViewModel;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by He Rotha on 10/20/17.
 */

public abstract class AbsBindingActivity<T extends ViewDataBinding> extends AbsBaseActivity {

    private T mBinding;
    private List<AbsBaseViewModel> mViewModels = new ArrayList<>();
    private BroadcastReceiver mMGroupUpdateReceiver;
    private List<GroupUpdateListener> mGroupUpdateListeners = new ArrayList<>();
    private boolean mIsSetContentView = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mIsSetContentView) {
            int layout = getLayoutRes();
            mBinding = DataBindingUtil.setContentView(this, layout);
        }
        initGroupUpdateReceiver();
    }

    public View getRootView() {
        return mBinding.getRoot();
    }

    protected void setSetContentView(boolean setContentView) {
        mIsSetContentView = setContentView;
    }

    private void initGroupUpdateReceiver() {
        mMGroupUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isRemovedMe = intent.getBooleanExtra(ConversationHelper.IS_REMOVED_ME,
                        false);
                Timber.i("onReceive group updated. isRemovedMe: " + isRemovedMe);
                if (isRemovedMe) {
                    String conId = intent.getStringExtra(ConversationHelper.GROUP_CONVERSATION_ID);
                    NotificationUtils.cancelNotificationByLocalId(AbsBindingActivity.this, conId);
                    if (getChatBinder() != null) {
                        getChatBinder().checkToSubscribeOrUnsubscribeGroupConversation(conId, true);
                    }
                    for (GroupUpdateListener groupUpdateListener : mGroupUpdateListeners) {
                        groupUpdateListener.onGroupUpdated(conId,
                                null,
                                true);
                    }
                } else {
                    Conversation conversation = intent.getParcelableExtra(SharedPrefUtils.DATA);
                    if (conversation != null) {
                        conversation.setFromGroupNotification(true);
                        for (GroupUpdateListener groupUpdateListener : mGroupUpdateListeners) {
                            groupUpdateListener.onGroupUpdated(conversation.getId(),
                                    conversation,
                                    false);
                        }
                    }
                }
            }
        };

        registerReceiver(mMGroupUpdateReceiver,
                new IntentFilter(ConversationHelper.GROUP_UPDATE_ACTION));
    }

    public void addGroupUpdateListener(GroupUpdateListener listener) {
        if (!mGroupUpdateListeners.contains(listener)) {
            mGroupUpdateListeners.add(listener);
        }
    }

    public void removeGroupUpdateListener(GroupUpdateListener listener) {
        mGroupUpdateListeners.remove(listener);
    }

    @LayoutRes
    public abstract int getLayoutRes();

    public T getBinding() {
        return mBinding;
    }

    public void setVariable(int id, Object value) {
        if (value instanceof AbsBaseViewModel) {
            mViewModels.add((AbsBaseViewModel) value);
        }
        getBinding().setVariable(id, value);
    }

    @Override
    public void onResume() {
        super.onResume();
        for (AbsBaseViewModel viewModel : mViewModels) {
            viewModel.onResume();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        for (AbsBaseViewModel viewModel : mViewModels) {
            viewModel.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mMGroupUpdateReceiver);
        for (AbsBaseViewModel viewModel : mViewModels) {
            viewModel.onDestroy();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (AbsBaseViewModel viewModel : mViewModels) {
            viewModel.onActivityResult(requestCode, resultCode, data);
        }
    }

    public interface GroupUpdateListener {
        void onGroupUpdated(String conversationId, Conversation conversation, boolean isRemovedMe);
    }
}
