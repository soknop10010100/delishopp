package com.proapp.sompom.newui;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.proapp.sompom.newui.fragment.NotificationFragment;

/**
 * Created by Chhom Veasna on 6/27/2019.
 */
public class NotificationActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(NotificationFragment.newInstance(), NotificationFragment.class.getName());
    }
}
