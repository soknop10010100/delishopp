package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.WalletHistoryAdapter;
import com.proapp.sompom.databinding.FragmentWalletHistoryBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.model.concierge.WalletHistory;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.WalletHistoryDataManager;
import com.proapp.sompom.viewmodel.WalletHistoryFragmentViewModel;
import com.proapp.sompom.widget.LoadMoreLinearLayoutManager;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Chhom Veasna on 8/23/22.
 */

public class WalletHistoryFragment extends AbsBindingFragment<FragmentWalletHistoryBinding> implements
        WalletHistoryFragmentViewModel.WalletHistoryFragmentViewModelListener {

    @Inject
    public ApiService mApiService;
    private WalletHistoryFragmentViewModel mViewModel;
    private LoadMoreLinearLayoutManager mLayoutManager;
    private LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener mLayoutManagerListener;
    private WalletHistoryAdapter mAdapter;

    public static WalletHistoryFragment newInstance() {
        return new WalletHistoryFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_wallet_history;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_WALLET_HISTORY_LIST;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        getBinding().layoutToolbar.toolbarTitle.setText(getString(R.string.setting_wallet_history_screen_title));
        setToolbar(getBinding().layoutToolbar.toolbar, true);
        mViewModel = new WalletHistoryFragmentViewModel(requireContext(),
                new WalletHistoryDataManager(requireContext(), mApiService),
                this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onLoadDataSuccess(List<WalletHistory> data, boolean isRefreshed, boolean isFromLoadMore, boolean isCanLoadMore) {
        if (mAdapter == null) {
            mAdapter = new WalletHistoryAdapter(data);
            mAdapter.setCanLoadMore(isCanLoadMore);
            mLayoutManager = new LoadMoreLinearLayoutManager(requireContext(), getBinding().recyclerView);
            mLayoutManagerListener = new LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener() {
                @Override
                public void onReachedLoadMoreBottom() {
                    mViewModel.loadMore();
                }

                @Override
                public void onReachedLoadMoreByDefault() {
                    mViewModel.loadMore();
                }
            };
            mLayoutManager.setLoadMoreLinearLayoutManagerListener(isCanLoadMore ? mLayoutManagerListener : null);
            getBinding().recyclerView.setLayoutManager(mLayoutManager);
            getBinding().recyclerView.setAdapter(mAdapter);
            getBinding().recyclerView.post(() -> mLayoutManager.checkLToInvokeReachLoadMoreByDefaultIfNecessary());
        } else {
            if (isRefreshed) {
                mAdapter.setRefreshData(data, isCanLoadMore);
                getBinding().recyclerView.post(() -> mLayoutManager.checkLToInvokeReachLoadMoreByDefaultIfNecessary());
            } else {
                mLayoutManager.loadingFinished();
                mAdapter.setCanLoadMore(isCanLoadMore);
                mAdapter.addLoadMoreData(data);

            }
            mLayoutManager.setLoadMoreLinearLayoutManagerListener(isCanLoadMore ? mLayoutManagerListener : null);
        }
    }
}
