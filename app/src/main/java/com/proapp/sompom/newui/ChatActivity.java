package com.proapp.sompom.newui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.databinding.ActivityDefaultBinding;
import com.proapp.sompom.helper.ConversationHelper;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.intent.newintent.HomeIntent;
import com.proapp.sompom.model.emun.OpenChatScreenType;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.fragment.ChatFragment;
import com.sompom.pushy.service.SendBroadCastHelper;

import timber.log.Timber;

/**
 * Created by He Rotha on 12/18/18.
 */
public class ChatActivity extends AbsSoundControllerActivity<ActivityDefaultBinding> {

    public static final String REFRESH_NOTIFICATION_BADGE_ACTION = "REFRESH_NOTIFICATION_BADGE_ACTION";
    public static final String UPDATE_READ_CONVERSATION_STATUS_ACTION = "UPDATE_READ_CONVERSATION_STATUS_ACTION";
    public static final String CONVERSATION_ID = "CONVERSATION_ID";

    private boolean mShouldBroadcastActionOnFinish = true;
    private String mConversationId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        ChatIntent intent = new ChatIntent(getIntent());
        intent.init(getThis());
        Timber.i("onCreate");
        /*
            We have to reset savedInstanceState to null in onCreate method as following reason.
            When there any chat notification from the app and the app is completely killed by the
            system such as no using for long time or to gain more memory. At that time, if user click
            on that notification, the app will try to open the chat screen, but at some points
            the app will get crashed because it try to restore old state of chat activity from the app
            that normally was killed by the system already as previous described. So in this case, we
            have to reset savedInstanceStaten to null so as to avoid crash and let the chat activity
            start as new.
         */
        if (intent.isFromNotification() &&
                !MainApplication.isIsHomeScreenOpened() &&
                savedInstanceState != null) {
            savedInstanceState = null;
            Timber.i("Reset savedInstanceState to null on create of chat activity.");
        }

        final String conversationId = intent.getConversationId(this);
        mConversationId = conversationId;
        checkToBroadcastRefreshBadgeCount(intent);
        final Product product = intent.getProduct();
        final User user = intent.getRecipient();
        Timber.i("onCreate: " + conversationId);
        /*
            We manage to save temporary open conversation ID to prevent duplicate conversation screen
            open and the chat screen is not open from group search message result.
         */
        if (((MainApplication) getApplication()).isConversationScreenAlreadyOpen(conversationId) &&
                intent.getOpenChatScreenType() != OpenChatScreenType.GROUP_SEARCH) {
            Timber.i("Conversation screen of ID: " + conversationId + ", is already open.");
            setSetContentView(false);
            super.onCreate(savedInstanceState);
            mShouldBroadcastActionOnFinish = false;
            finish();
        } else {
            super.onCreate(savedInstanceState);
            ((MainApplication) getApplication()).addOpenConversationScreen(conversationId);
            setFragment(R.id.containerView, ChatFragment.newInstance(intent.getConversation(),
                    user,
                    product,
                    conversationId,
                    intent.getChat(),
                    intent.getSearchKeyword(),
                    intent.getOpenChatScreenType()),
                    ChatFragment.TAG);
        }
    }

    @Override
    protected void onChatSocketServiceReady() {
        if (!MainApplication.isIsHomeScreenOpened()) {
            startSubscribeAllGroupChannel();
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_default;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ChatIntent chatIntent = new ChatIntent(intent);
        chatIntent.init(getThis());
        Timber.i("onNewIntent isGroup: " + chatIntent.isGroup());
        final String conversationId = chatIntent.getConversationId(this);
        if (chatIntent.isGroup() && !ConversationDb.isConversationExisted(this, conversationId)) {
            return;
        }

        checkToBroadcastRefreshBadgeCount(chatIntent);
        final Product product = chatIntent.getProduct();
        final User user = chatIntent.getRecipient();


        Fragment fr = getSupportFragmentManager().findFragmentByTag(ChatFragment.TAG);
        if (fr instanceof ChatFragment) {
            if (((ChatFragment) fr).getChannelId() != null
                    && !((ChatFragment) fr).getChannelId().equals(chatIntent.getConversationId(this))) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .remove(fr)
                        .replace(R.id.containerView, ChatFragment.newInstance(chatIntent.getConversation(),
                                user,
                                product,
                                conversationId,
                                chatIntent.getChat(),
                                chatIntent.getSearchKeyword(),
                                chatIntent.getOpenChatScreenType()),
                                ChatFragment.TAG)
                        .commit();
            }
        } else {
            setFragment(R.id.containerView, ChatFragment.newInstance(chatIntent.getConversation(),
                    user,
                    product,
                    conversationId,
                    chatIntent.getChat(),
                    chatIntent.getSearchKeyword(),
                    chatIntent.getOpenChatScreenType()),
                    ChatFragment.TAG);
        }
    }

    private void checkToBroadcastRefreshBadgeCount(ChatIntent chatIntent) {
        Timber.i("isFromNotification: " + chatIntent.isFromNotification());
        if (chatIntent.isFromNotification()) {
            SendBroadCastHelper.verifyAndSendBroadCast(this, new Intent(REFRESH_NOTIFICATION_BADGE_ACTION));
        }
    }

    @Override
    public void onBackPressed() {
        if (!TextUtils.isEmpty(mConversationId)) {
            ConversationHelper.saveLastLocalMessageId(getApplicationContext(), mConversationId, true);
        }
        Fragment fr = getSupportFragmentManager().findFragmentByTag(ChatFragment.TAG);
        if (fr instanceof ChatFragment && !((ChatFragment) fr).onBackPress()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
        Timber.i("finish: isIsHomeScreenOpened: " + MainApplication.isIsHomeScreenOpened());
        ((MainApplication) getApplication()).removeOpenConversationScreen(mConversationId);
        if (mShouldBroadcastActionOnFinish) {
            SendBroadCastHelper.verifyAndSendBroadCast(this, new Intent(REFRESH_NOTIFICATION_BADGE_ACTION));
            broadcastReadConversationActionIfNecessary();
        }
        if (!MainApplication.isIsHomeScreenOpened()) {
            ((MainApplication) getApplication()).startFreshHomeScreen(HomeIntent.Redirection.None);
        }

        /*
            Since the new conversation can be created from local such init a new conversation from
            contact user list screen. As local will check that if the conversation is not yet existed,
            we will create a new one. So this condition will make sure that only valid conversation will
            be kept in local after exit chat screen. Valid conversation is the conversation that at least
            have one message.
         */
        ((MainApplication) getApplication()).checkToRemoveEmptyConversationIfNecessary(mConversationId);
    }

    private void broadcastReadConversationActionIfNecessary() {
        Fragment chatFragment = getSupportFragmentManager().findFragmentByTag(ChatFragment.TAG);
        if (chatFragment instanceof ChatFragment) {
            if (!TextUtils.isEmpty(((ChatFragment) chatFragment).getConversationId())) {
                Intent intent = new Intent(UPDATE_READ_CONVERSATION_STATUS_ACTION);
                intent.putExtra(CONVERSATION_ID, ((ChatFragment) chatFragment).getConversationId());
                SendBroadCastHelper.verifyAndSendBroadCast(this, intent);
            }
        }
    }
}
