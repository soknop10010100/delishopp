package com.proapp.sompom.newui;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.WindowManager;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.chat.call.CallNotificationHelper;
import com.proapp.sompom.chat.call.CallingService;
import com.proapp.sompom.chat.call.IncomingCallDataHolder;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.intent.newintent.CallIntent;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.emun.AskFloatingWindowPermissionForType;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.dialog.AskFloatingWindowPermissionDialog;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.sompom.baseactivity.PermissionCheckHelper;

import timber.log.Timber;

public class CallingActivity extends AbsDefaultActivity implements CallingService.CallStateListener {

    private IncomingCallDataHolder mIncomingCallDataHolder;
    private User mRecipient;
    private CallingService.CallServiceBinder mBinder;
    private boolean mIsAnswerCallFromIncomingCallNotification;
    private ChatIntent mChatIntent;
    private boolean mIsCloseForMultipleScreenReason;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.i("onCreate: " + hashCode());
        /*
           Will prevent multiple calling activities present at the same time.
         */
        if (!MainApplication.isIsCallingScreenOpened()) {
            MainApplication.setIsCallingScreenOpened(true);
//            initCallEndBroadcastReceiver();
            enableScreenDisplayInLockScreen();
            initCallScreen(getIntent());
        } else {
            Timber.e("Will close the screen automatically since the calling screen is already present.");
            mIsCloseForMultipleScreenReason = true;
            finish();
        }
    }

    private void checkToAnswerIncomingCallAutomatically() {
        // Used with Android Q and notifications
        if (mIsAnswerCallFromIncomingCallNotification) {
            new Handler().postDelayed(() -> {
                Timber.i("Auto answer call from notification.");
                if (mBinder != null) {
                    mBinder.performAnswerCall();
                }
            }, 1000);
        }
    }

    private void enableScreenDisplayInLockScreen() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    public CallingService.CallServiceBinder getBinder() {
        return mBinder;
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.i("onResume: " + hashCode());
    }

    private boolean isNeedToResizeCallScreenToFull(Intent intent) {
        if (((MainApplication) getApplication()).isCallInProgressNow()) {
            Timber.i("((MainApplication) getApplication()).isCallInProgressNow() = true");
            return true;
        }
        return new CallingIntent(intent).isNeedToResizeCallScreenToFull();
    }

    private void initCallScreen(Intent intent) {
        CallIntent callingIntent = new CallIntent(intent);
        mIncomingCallDataHolder = callingIntent.getIncomingCallData();
        mRecipient = callingIntent.getRecipient();
        addOnServiceListener(binder -> checkPermission(new PermissionCheckHelper.OnPermissionCallback() {
            @Override
            public void onPermissionGranted() {
                bindCall(result -> {
                    mBinder = result;
                    if (isNeedToResizeCallScreenToFull(intent)) {
                        Timber.i("Will resize back to full window with activity");
                        runOnUiThread(() -> mBinder.resizeBackToFullScreen(getBinding().containerView,
                                CallingActivity.this));
                    } else {
                        Timber.i("Start new call action: " + hashCode());
                        Bundle extras = intent.getExtras();
                        boolean isIncomingCallFromNotification = false;
                        if (extras != null) {
                            mIsAnswerCallFromIncomingCallNotification = extras.getBoolean(CallNotificationHelper.CALL_ANSWERED);
                            isIncomingCallFromNotification = (mIsAnswerCallFromIncomingCallNotification ||
                                    extras.getBoolean(CallNotificationHelper.IS_INCOMING_CALL_FROM_NOTIFICATION, false));
                        }
                        AbsCallService.CallActionType callActionType = callingIntent.getCallActionType();
                        Timber.i("callActionType: " + callActionType + ", mIsAnswerCallFromIncomingCallNotification: " +
                                mIsAnswerCallFromIncomingCallNotification);
                        if (callActionType == AbsCallService.CallActionType.CALL) {
                            startOutGoingCall(
                                    getBinding().containerView,
                                    AbsCallService.CallType.getFromValue(callingIntent.getCallType()),
                                    mRecipient,
                                    callingIntent.getGroup(),
                                    CallingActivity.this);
                        } else if (callActionType == AbsCallService.CallActionType.INCOMING) {
                            startIncomingCall(
                                    getBinding().containerView,
                                    mIncomingCallDataHolder,
                                    isIncomingCallFromNotification,
                                    CallingActivity.this);
                            checkToAnswerIncomingCallAutomatically();
                        } else if (callActionType == AbsCallService.CallActionType.JOINED) {
                            startJoinGroupCall(
                                    getBinding().containerView,
                                    AbsCallService.CallType.getFromValue(callingIntent.getCallType()),
                                    callingIntent.getGroup(),
                                    CallingActivity.this);
                        }
                        //TODO: Need to check
//                    checkToAnswerIncomingCallAutomatically();
                    }
                }, !((MainApplication) getApplication()).isCallInProgressNow());
            }

            @Override
            public void onPermissionDenied(String[] grantedPermission, String[] deniedPermission,
                                           boolean isUserPressNeverAskAgain) {
                finish();
            }
        }, Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_PHONE_STATE));
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        Timber.i("dispatchKeyEvent: getKeyCode: " + event.getKeyCode());
        return super.dispatchKeyEvent(event);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Timber.i("onNewIntent: " + hashCode());
        /*
        When showing full incoming call screen intent on Android 10+ while screen is locked, user can
        still answer via that incoming call notification and in this case we have to manage auto answer
        too.
         */
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mIsAnswerCallFromIncomingCallNotification = extras.getBoolean(CallNotificationHelper.CALL_ANSWERED);
        }
        if (mIsAnswerCallFromIncomingCallNotification) {
            checkToAnswerIncomingCallAutomatically();
            return;
        }

        //Check if there is some call action from call notification.
        CallingIntent videoCallIntent = new CallingIntent(intent);
        boolean isShowingIncomingCall = false;

        if (mBinder != null) {
            isShowingIncomingCall = mBinder.isShowingIncomingCall();
            mBinder.handleNotificationAction(videoCallIntent.getNotificationActionType(),
                    videoCallIntent.getGroupId());

        }

        if (mBinder != null &&
                mBinder.getVoiceCallClientService() != null &&
                (mBinder.getVoiceCallClientService().isCallInProgress() || isShowingIncomingCall)) {
            CallIntent callingIntent = new CallIntent(intent);
            if (callingIntent.getIncomingCallData() != null &&
                    callingIntent.getIncomingCallData().getCallData() != null) {
                User caller = CallHelper.getCaller(callingIntent.getIncomingCallData().getCallData());
                if (caller != null) {
                    if (mBinder.getVoiceCallClientService().isGroupCall()) {
                        UserGroup group = CallHelper.getGroup(callingIntent.getIncomingCallData().getCallData());
                        if (group != null) {
                            if (TextUtils.equals(mBinder.getVoiceCallClientService().getChannelId(), group.getId()) &&
                                    !mBinder.getVoiceCallClientService().isUserEngagedInCall(caller.getId())) {
                                Timber.i("Ignore sending user busy.");
                            } else {
                                dispatchUserBusyMessage(callingIntent);
                            }
                        } else {
                            dispatchUserBusyMessage(callingIntent);
                        }
                    } else {
                        dispatchUserBusyMessage(callingIntent);
                    }
                }
            }
        }
    }

    private void dispatchUserBusyMessage(CallIntent callingIntent) {
        //Need to send busy status to the caller since current user is being engaged in another
        //call.
        Timber.i("Send user busy status to the caller.");
        mBinder.getVoiceCallClientService().sendUserBusyVOIPCallMessage(callingIntent.
                getIncomingCallData().getCallData());
    }

    @Override
    protected void onChatSocketServiceReady() {
        /*
        Receive incoming call when app close, so need to subscribe all group socket.
         */
        if (!MainApplication.isIsHomeScreenOpened()) {
            startSubscribeAllGroupChannel();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        Timber.i("onKeyDown: keyCode: " + keyCode);
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN ||
                keyCode == KeyEvent.KEYCODE_VOLUME_UP ||
                keyCode == KeyEvent.KEYCODE_POWER) {
            if (mBinder != null) {
                mBinder.checkToStopIncomingToneWhenDeviceButtonPressed();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void finish() {
        if (!mIsCloseForMultipleScreenReason) {
            String progressCallChannelId = getProgressCallChannelId();
            super.finish();
            Timber.i("finish: " + hashCode());
            if (mBinder != null) {
                mBinder.clearCallStateListener();
            }
            if (!MainApplication.isIsHomeScreenOpened() && !TextUtils.isEmpty(progressCallChannelId)) {
                Timber.i("finish: Will navigate to chat screen after call ended.");
                ((MainApplication) getApplication()).startChatScreen(progressCallChannelId);
            }
        } else {
            super.finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Timber.i("onDestroy: " + hashCode());
        if (!mIsCloseForMultipleScreenReason) {
            MainApplication.setIsCallingScreenOpened(false);
        }
    }

    private String getProgressCallChannelId() {
        if (mBinder != null) {
            return mBinder.getInProgressCallChannelId();
        }

        return null;
    }

    @Override
    public void onBackPressed() {
        Timber.i("onBackPressed: " + hashCode());
        Timber.i("isCallInProgressNow: " + ((MainApplication) getApplication()).isCallInProgressNow() +
                ", isCallViewInFloatingMode: " + ((MainApplication) getApplication()).isCallViewInFloatingMode());
        if (CallHelper.hasFloatingPermissionEnable(this)) {
            if (mBinder != null) {
                mBinder.enableFloatingCallWindow(this);
            }
            super.onBackPressed();
        } else if (shouldAskFloatingPermission()) {
            askFloatingWindowPermission();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onCallEnded() {
        Timber.e("onCallEnded: " + hashCode());
        finish();
    }

    @Override
    public void onMessageClicked(ChatIntent chatIntent) {
        Timber.i("onMessageClicked: " + hashCode());
        mChatIntent = chatIntent;
        if (CallHelper.hasFloatingPermissionEnable(CallingActivity.this)) {
            if (mBinder != null) {
                mBinder.enableFloatingCallWindow(CallingActivity.this);
            }
            startActivity(chatIntent);
            finish();
        } else if (shouldAskFloatingPermission()) {
            askFloatingWindowPermission();
        } else {
            startActivity(chatIntent);
            finish();
        }
    }

    private boolean shouldAskFloatingPermission() {
        return !SharedPrefUtils.getBoolean(this, SharedPrefUtils.DO_NOT_ASK_CALL_FLOATING_PERMISSION_AGAIN);
    }

    private void askFloatingWindowPermission() {
        AskFloatingWindowPermissionDialog dialog = AskFloatingWindowPermissionDialog.newInstance(AskFloatingWindowPermissionForType.CALL);
        dialog.show(getSupportFragmentManager());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Timber.i("onActivityResult: requestCode: " + requestCode);
        if (requestCode == AskFloatingWindowPermissionDialog.OVERLAY_PERMISSION_REQUEST_CODE) {
            //Check the state
            if (mChatIntent != null) {
                if (CallHelper.hasFloatingPermissionEnable(this)) {
                    if (mBinder != null) {
                        mBinder.enableFloatingCallWindow(CallingActivity.this);
                    }
                }
                startActivity(mChatIntent);
            } else {
                //Back press state
                if (CallHelper.hasFloatingPermissionEnable(this)) {
                    if (mBinder != null) {
                        mBinder.enableFloatingCallWindow(this);
                    }
                }
            }
            finish();
        }
    }
}
