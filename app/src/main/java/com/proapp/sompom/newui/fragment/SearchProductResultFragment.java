package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ConciergeFragmentAdapter;
import com.proapp.sompom.databinding.FragmentSearchProductResultBinding;
import com.proapp.sompom.decorataor.ConciergeOrderHistoryItemDecorator;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.intent.newintent.SearchMessageIntent;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.SearchProductResultDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.SearchProductResultFragmentViewModel;
import com.proapp.sompom.widget.LoadMoreLinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 25/1/22.
 */
public class SearchProductResultFragment extends AbsSupportShopCheckOutFragment<FragmentSearchProductResultBinding>
        implements SearchProductResultFragmentViewModel.SearchProductResultFragmentViewModelCallback {
    public static final String TAG = SearchProductResultFragment.class.getName();
    @Inject
    public ApiService mApiService;
    private SearchProductResultFragmentViewModel mViewModel;
    private ConciergeFragmentAdapter mAdapter;
    private LoadMoreLinearLayoutManager mLayoutManager;
    private LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener mLoadMoreLinearLayoutManagerListener;

    public static SearchProductResultFragment newInstance(String supplierId) {

        Bundle args = new Bundle();
        args.putString(SearchMessageIntent.SUPPLIER_ID, supplierId);

        SearchProductResultFragment fragment = new SearchProductResultFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_SEARCH_GENERAL_PRODUCT;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search_product_result;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        SearchProductResultDataManager dataManager = new SearchProductResultDataManager(requireContext(),
                mApiService);
        mViewModel = new SearchProductResultFragmentViewModel(dataManager, this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    protected void onReceivedRefreshProductOrderCounterEvent(List<ConciergeMenuItem> items) {
        if (items != null && !items.isEmpty()) {
            if (mAdapter != null) {
                ArrayList<String> ids = new ArrayList<>();
                for (ConciergeMenuItem item : items) {
                    ids.add(item.getId());
                }
                mAdapter.updateProductOrderCounter(ids);
            }
        }
    }

    @Override
    protected void onRefreshItemCountInList() {
        if (mAdapter != null) {
            mAdapter.refreshProductInCardCounter();
        }
    }

    @Override
    public void onItemAddedToCart(ConciergeMenuItem item) {
        Timber.i("onItemAddedToCart: " + item);
        if (mAdapter != null) {
            mAdapter.onItemUpdatedInCart(item);
        }
    }

    @Override
    protected void onCartItemCleared() {
        Timber.i("onCartItemCleared");
        if (mAdapter != null) {
            mAdapter.onCartCleared();
        }
    }

    @Override
    protected void onItemRemovedFromCart(ConciergeMenuItem shop) {
        if (mAdapter != null) {
            mAdapter.onItemUpdatedInCart(shop);
        }
    }

    @Override
    protected ApiService getPrivateAPIService() {
        return mApiService;
    }

    @Override
    protected void showLoading() {
        mViewModel.showLoading(true);
    }

    @Override
    protected void hideLoading() {
        mViewModel.hideLoading();
    }

    public void showScreenLoading() {
        if (mViewModel != null) {
            mViewModel.showLoading();
        }
    }

    public void onSearch(String searchText) {
        if (mViewModel != null) {
            mViewModel.searchProduct(searchText, true, false);
        }
    }

    @Override
    public void onSearchResult(List<ConciergeMenuItem> products,
                               boolean isRefresh,
                               boolean isFromLoadMore,
                               boolean isCanLoadMore) {
        updateAdapter(products, isRefresh, isFromLoadMore, isCanLoadMore);
    }

    public void updateAdapter(List<ConciergeMenuItem> products,
                              boolean isRefresh,
                              boolean isFromLoadMore,
                              boolean isCanLoadMore) {
        if (mAdapter == null) {
            mAdapter = new ConciergeFragmentAdapter(new ArrayList<>(products),
                    true,
                    ApplicationHelper.isInExpressMode(requireContext()),
                    getChildFragmentManager(),
                    getLifecycle(),
                    new ConciergeFragmentAdapter.ConciergeFragmentAdapterListener() {
                        @Override
                        public void onProductAddedFromDetail() {

                        }

                        @Override
                        public void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {
                            SearchProductResultFragment.this.onAddFirstItemToCart(mViewModel, conciergeMenuItem);
                        }
                    });
            mAdapter.setCanLoadMore(isCanLoadMore);
            mLayoutManager = new LoadMoreLinearLayoutManager(requireContext(), getBinding().recyclerView, false);
            mLoadMoreLinearLayoutManagerListener = new LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener() {
                @Override
                public void onReachedLoadMoreBottom() {
                    Timber.i("onReachedLoadMoreBottom");
                    mViewModel.loadMore();
                }

                @Override
                public void onReachedLoadMoreByDefault() {
                    Timber.i("onReachedLoadMoreByDefault");
                    mViewModel.loadMore();
                }
            };
            mLayoutManager.setLoadMoreLinearLayoutManagerListener(isCanLoadMore ? mLoadMoreLinearLayoutManagerListener : null);
            getBinding().recyclerView.setLayoutManager(mLayoutManager);
            getBinding().recyclerView.addItemDecoration(new ConciergeOrderHistoryItemDecorator(requireContext()));
            getBinding().recyclerView.setAdapter(mAdapter);
            getBinding().recyclerView.post(() -> mLayoutManager.checkLToInvokeReachLoadMoreByDefaultIfNecessary());
        } else {
            mLayoutManager.setLoadMoreLinearLayoutManagerListener(isCanLoadMore ? mLoadMoreLinearLayoutManagerListener : null);
            if (getBinding().recyclerView.getAdapter() == null) {
                getBinding().recyclerView.setLayoutManager(mLayoutManager);
                getBinding().recyclerView.setAdapter(mAdapter);
                getBinding().recyclerView.post(() -> mLayoutManager.checkLToInvokeReachLoadMoreByDefaultIfNecessary());
            }

            mAdapter.setCanLoadMore(isCanLoadMore);
            if (isRefresh) {
                mAdapter.setData(new ArrayList<>(products));
            } else {
                mAdapter.addLoadMoreData(new ArrayList<>(products));
            }
            mLayoutManager.loadingFinished();
        }
    }
}
