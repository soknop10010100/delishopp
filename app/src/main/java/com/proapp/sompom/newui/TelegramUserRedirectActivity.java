package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.intent.newintent.TelegramUserRedirectIntent;
import com.proapp.sompom.newui.fragment.TelegramUserRedirectFragment;

public class TelegramUserRedirectActivity extends AbsDefaultActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TelegramUserRedirectIntent intent = new TelegramUserRedirectIntent(getIntent());
        String telegramUserId = intent.getStringExtra(TelegramUserRedirectIntent.TELEGRAM_USER_ID);
        setFragment(TelegramUserRedirectFragment.newInstance(telegramUserId), TelegramUserRedirectFragment.TAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
