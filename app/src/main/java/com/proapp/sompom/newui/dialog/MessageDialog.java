package com.proapp.sompom.newui.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.SingleChoiceAdapter;
import com.proapp.sompom.databinding.DialogFragmentMessageBinding;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.newui.fragment.AbsDialogFragment;

/**
 * Created by he.rotha on 4/28/16.
 */
public class MessageDialog extends AbsDialogFragment {

    public static final String TAG = MessageDialog.class.getName();

    private DialogFragmentMessageBinding mBinding;
    private String mTitle;
    private String mMessage;
    private String mLeftText;
    private String mRightText;
    private View.OnClickListener mLeftClickListener;
    private View.OnClickListener mRightClickListener;
    private boolean mIsDismissWhenClickOnButtonRight = true;
    private boolean mIsDismissWhenClickOnButtonLeft = true;
    private boolean mIsDisplayDotIconForTitle;

    private String[] mTitles;
    private int mSelectedPosition = 0;
    private DialogInterface.OnClickListener mSingleChoiceListener;
    private OnClickListener mOnClickListener;

    public static MessageDialog newInstance() {

        Bundle args = new Bundle();

        MessageDialog fragment = new MessageDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        requireDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_message, container, false);
        return getBinding().getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (TextUtils.isEmpty(mTitle)) {
            getBinding().textviewTitle.setVisibility(View.GONE);
        } else {
            getBinding().textviewTitle.setText(mTitle);
        }

        if (TextUtils.isEmpty(mMessage)) {
            getBinding().textviewMessage.setVisibility(View.GONE);
        } else {
            getBinding().textviewMessage.setText(mMessage);
        }

        if (TextUtils.isEmpty(mLeftText)) {
            getBinding().buttonLeft.setVisibility(View.GONE);
        } else {
            getBinding().buttonLeft.setText(mLeftText);
            getBinding().buttonLeft.setOnClickListener(view1 -> {
                if (mLeftClickListener != null) {
                    mLeftClickListener.onClick(view1);
                }
                if (mIsDismissWhenClickOnButtonLeft) {
                    dismiss();
                }
            });
        }

        if (TextUtils.isEmpty(mRightText)) {
            getBinding().buttonRight.setVisibility(View.GONE);
        } else {
            getBinding().buttonRight.setText(mRightText);
            getBinding().buttonRight.setOnClickListener(view12 -> {
                if (mRightClickListener != null) {
                    mRightClickListener.onClick(view12);
                }
                if (mIsDismissWhenClickOnButtonRight) {
                    dismiss();
                }
            });
        }

        if (mTitles != null) {
            ListView listView = new ListView(getContext());
            listView.setVerticalScrollBarEnabled(false);
            listView.setOverScrollMode(View.OVER_SCROLL_NEVER);
            listView.setDivider(null);
            listView.setDividerHeight(0);

            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            getBinding().view.addView(listView, params);

            listView.setAdapter(new SingleChoiceAdapter(getContext(), mTitles, mSelectedPosition, mSingleChoiceListener));
        }

        if (mIsDisplayDotIconForTitle) {
            getBinding().dotViewForTitle.setVisibility(View.VISIBLE);
            getBinding().dotViewForDescription.setVisibility(View.INVISIBLE);
        } else {
            getBinding().dotViewForTitle.setVisibility(View.GONE);
            getBinding().dotViewForDescription.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnClickListener != null) {
            mOnClickListener.onClick();
        }
    }

    public void setDisplayDotIconForTitle(boolean displayDotIconForTitle) {
        mIsDisplayDotIconForTitle = displayDotIconForTitle;
    }

    public void showLoading() {
        getBinding().loadingView.setVisibility(View.VISIBLE);
    }

    public void hideLoading() {
        getBinding().loadingView.setVisibility(View.GONE);
    }

    public DialogFragmentMessageBinding getBinding() {
        return mBinding;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public void setLeftText(String leftText, View.OnClickListener listener) {
        mLeftText = leftText;
        mLeftClickListener = listener;
    }

    public void setRightText(String rightText, View.OnClickListener listener) {
        mRightText = rightText;
        mRightClickListener = listener;
    }

    public void show(FragmentManager manager) {
        super.show(manager, TAG);
    }

    public void setSingleChoiceItems(String[] titles,
                                     int selectedPosition,
                                     DialogInterface.OnClickListener listener) {
        mTitles = titles;
        mSelectedPosition = selectedPosition;
        mSingleChoiceListener = listener;
    }

    public void setDismissWhenClickOnButtonRight(boolean dismissWhenClickOnButtonRight) {
        mIsDismissWhenClickOnButtonRight = dismissWhenClickOnButtonRight;
    }

    public void setDismissWhenClickOnButtonLeft(boolean dismissWhenClickOnButtonLeft) {
        mIsDismissWhenClickOnButtonLeft = dismissWhenClickOnButtonLeft;
    }

    public void setOnDialogDismiss(OnClickListener onDialogDismiss) {
        mOnClickListener = onDialogDismiss;
    }
}
