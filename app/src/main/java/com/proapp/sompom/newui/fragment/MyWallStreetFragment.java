package com.proapp.sompom.newui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.desmond.squarecamera.utils.Keys;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.TimelineAdapter;
import com.proapp.sompom.databinding.FragmentMyWallStreetBinding;
import com.proapp.sompom.helper.OpenCommentDialogHelper;
import com.proapp.sompom.injection.mywallserialize.productserialize.MyWallSerializeQualifier;
import com.proapp.sompom.intent.CreateTimelineIntent;
import com.proapp.sompom.intent.CreateTimelineIntentResult;
import com.proapp.sompom.licence.db.LicenseSynchronizationDb;
import com.proapp.sompom.licence.model.SynchroniseData;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnTabSelectListener;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.CreateWallStreetScreenType;
import com.proapp.sompom.model.emun.FollowItemType;
import com.proapp.sompom.model.emun.PostContextMenuItemType;
import com.proapp.sompom.model.emun.TimelinePostItem;
import com.proapp.sompom.model.notification.NotificationListAndRedirectionHelper;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.ListFollowDialog;
import com.proapp.sompom.newui.dialog.PostContextMenuDialog;
import com.proapp.sompom.newui.dialog.ShareLinkMenuDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;
import com.proapp.sompom.utils.BetterSmoothScrollUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.TimelinePostItemUtil;
import com.proapp.sompom.viewmodel.newviewmodel.MyWallStreetFragmentViewModel;
import com.proapp.sompom.widget.LoaderMoreLayoutManager;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.videomanager.widget.MediaRecyclerView;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 8/1/18.
 */

public class MyWallStreetFragment extends AbsBindingFragment<FragmentMyWallStreetBinding>
        implements OnTimelineItemButtonClickListener, OnTabSelectListener {
    @MyWallSerializeQualifier
    @Inject
    public ApiService mSerialApiService;
    @Inject
    public ApiService mApiService;

    private MyWallStreetFragmentViewModel mViewModel;
    private LoaderMoreLayoutManager mLoadMoreLayoutManager;
    private TimelineAdapter mTimelineAdapter;
    private OpenCommentDialogHelper mCommentDialog;
    private boolean mIsTabSelected = true;

    public static MyWallStreetFragment newInstance(String userId) {
        MyWallStreetFragment fr = new MyWallStreetFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NotificationListAndRedirectionHelper.FIELD_ACTOR, userId);
        fr.setArguments(bundle);
        return fr;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_my_wall_street;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        String userId = getArguments().getString(NotificationListAndRedirectionHelper.FIELD_ACTOR);

        initLayout();

        WallStreetDataManager dataManager = new WallStreetDataManager(getActivity(), mApiService, mSerialApiService, null);
        mViewModel = new MyWallStreetFragmentViewModel(userId, dataManager, (result, canLoadMore) -> {
            SynchroniseData mSynchronizeData = LicenseSynchronizationDb.readSynchroniseData(getContext());
            boolean checkLikeComment = false;
            if (mSynchronizeData != null && mSynchronizeData.getNewsFeed() != null) {
                checkLikeComment = mSynchronizeData.getNewsFeed().isEnableLikeComment();
            }
            mTimelineAdapter = new TimelineAdapter((AbsBaseActivity) getActivity(),
                    mApiService,
                    result,
                    MyWallStreetFragment.this,
                    checkLikeComment);
//            mTimelineAdapter.setAdViews(dataManager.getAdViews());
            mTimelineAdapter.setCanLoadMore(canLoadMore);
            getBinding().recyclerView.setAdapter(mTimelineAdapter);
        });
        setVariable(BR.viewModel, mViewModel);

    }

    @Override
    public void onCommentButtonClick(Adaptive adaptive, int position) {
        onCommentClick(adaptive, true);
    }

    @Override
    public void onShareButtonClick(Adaptive adaptive, int position) {
        ShareLinkMenuDialog dialog = ShareLinkMenuDialog.newInstance((WallStreetAdaptive) adaptive);
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onUserViewMediaClick(Adaptive adaptive) {
        ListFollowDialog dialog = ListFollowDialog.newInstance(adaptive, FollowItemType.USER_ACTION);
        dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
    }

    @Override
    public void onLikeViewerClick(Adaptive adaptive) {
        ListFollowDialog dialog = ListFollowDialog.newInstance(adaptive, FollowItemType.LIKE_VIEWER);
        dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
    }

    @Override
    public void onFollowButtonClick(String id, boolean isFollowing) {
        mViewModel.checkToPostFollow(id, isFollowing);
    }

    @Override
    public void onShowSnackBar(boolean isError, String message) {

    }

    @Override
    public void onShowPostContextMenuClick(Adaptive adaptive, User user, int position) {
        if (user == null) {
            return;
        }

        TimelinePostItem timelinePostItem = TimelinePostItemUtil.getPostItem(adaptive);
        LifeStream lifeStream = TimelinePostItemUtil.getLifeStream(adaptive);
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        dialog.addItem(PostContextMenuItemType.getPostMenuItems(requireContext(),
                SharedPrefUtils.checkIsMe(requireActivity(), user.getId()), timelinePostItem));
        dialog.setOnItemClickListener(result -> {
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.EDIT_POST_CLICK) {
                startCreateTimelineActivity(adaptive, position);
            } else if (itemType == PostContextMenuItemType.UNFOLLOW_CLICK) {
                mViewModel.checkToPostFollow(user.getId(), false);
                if (adaptive instanceof WallStreetAdaptive && ((WallStreetAdaptive) adaptive).getUser() != null) {
                    ((WallStreetAdaptive) adaptive).getUser().setFollow(false);
                    mTimelineAdapter.update(adaptive);
                }
            } else if (itemType == PostContextMenuItemType.EDIT_PRIVACY_CLICK) {
                showEditPrivacyDialog(result1 -> {
                    //TODO: implement api edit privacy
                });
            } else if (itemType == PostContextMenuItemType.TURNOFF_NOTIFICATION_CLICK) {
                mViewModel.unFollowShop(adaptive.getId());
            } else if (itemType == PostContextMenuItemType.DELETE_CLICK) {
                if (lifeStream != null) {
                    showMessageDialog(R.string.post_popup_confirm_delete_title,
                            R.string.post_popup_confirm_delete_description,
                            R.string.seller_store_button_delete,
                            R.string.setting_change_language_dialog_cancel_button,
                            view -> {
                                mTimelineAdapter.removeData(position);
                                mViewModel.deletePost(lifeStream);
                            });
                }
            }
            dialog.dismiss();

        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onCommentClick(Adaptive adaptive, boolean autoDisplayKeyboard) {
        if (mCommentDialog == null) {
            mCommentDialog = new OpenCommentDialogHelper(getActivity());
        }
        mCommentDialog.show(requireContext(), mApiService, null, adaptive, mTimelineAdapter, true);
    }

    @Override
    public MediaRecyclerView getRecyclerView() {
        return getBinding().recyclerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mIsTabSelected) {
            getBinding().recyclerView.onResume();
            if (getActivity() instanceof AbsBaseActivity) {
                AbsBaseActivity base = (AbsBaseActivity) getActivity();
                if (base.getFloatingVideo() != null && base.getFloatingVideo().isFloating()) {
                    getBinding().recyclerView.setAutoPlay(false);
                    getBinding().recyclerView.setAutoResume(false);
                    base.setOnFloatingVideoRemoveListener(() -> {
                        getBinding().recyclerView.setAutoPlay(true);
                        getBinding().recyclerView.setAutoResume(true);
                        getBinding().recyclerView.resume();
                    });
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().recyclerView.onPause();
    }

    private void initLayout() {
        mLoadMoreLayoutManager = new LoaderMoreLayoutManager(getActivity());
        mLoadMoreLayoutManager.setOnLoadMoreListener(() -> {
            getBinding().recyclerView.stopScroll();
            mViewModel.loadMoreData(new OnCallbackListListener<LoadMoreWrapper<Adaptive>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    mTimelineAdapter.setCanLoadMore(false);
                    mTimelineAdapter.notifyDataSetChanged();
                    mLoadMoreLayoutManager.loadingFinished();
                    mLoadMoreLayoutManager.setOnLoadMoreListener(null);
                }

                @Override
                public void onComplete(LoadMoreWrapper<Adaptive> result, boolean canLoadMore) {
                    mTimelineAdapter.addLoadMoreData(result.getData());
                    mTimelineAdapter.setCanLoadMore(canLoadMore);
                    mLoadMoreLayoutManager.loadingFinished();
                }
            });
        });

        getBinding().recyclerView.setLayoutManager(mLoadMoreLayoutManager);
    }

    public void shouldScrollTop(boolean isPullToRefresh) {
        if (findFirstVisibleItemPosition() != 0) {
            if (isPullToRefresh) {
                mViewModel.onRefresh();
            }
            BetterSmoothScrollUtil.startScroll(getBinding().recyclerView,
                    0,
                    findFirstVisibleItemPosition());
        } else {
            mViewModel.onRefresh();
        }
    }

    public int findFirstVisibleItemPosition() {
        return mLoadMoreLayoutManager.findFirstVisibleItemPosition();
    }

    private void startCreateTimelineActivity(Adaptive adaptive, int position) {
        if (getActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getActivity()).startActivityForResult(new CreateTimelineIntent(getActivity(),
                            (WallStreetAdaptive) adaptive,
                            CreateWallStreetScreenType.EDIT_POST),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            CreateTimelineIntentResult result1 = new CreateTimelineIntentResult(data);
                            mTimelineAdapter.updateItem(result1.getLifeStream(), position);
                        }
                    });
        }
    }

    @Override
    public void onTabSelected(boolean isSelected) {
        mIsTabSelected = isSelected;
    }
}
