package com.proapp.sompom.newui.fragment;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ConciergeFragmentAdapter;
import com.proapp.sompom.databinding.FragmentConciergeBinding;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.SupportConciergeCheckoutBottomSheetDisplay;
import com.proapp.sompom.model.ConciergeItemAdaptive;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeShopAnnouncementResponse;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeFragmentDataManager;
import com.proapp.sompom.utils.BetterSmoothScrollUtil;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeFragmentViewModel;
import com.proapp.sompom.widget.LoadMoreLinearLayoutManager2;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 26/8/21.
 */
public class ConciergeFragment extends AbsSupportShopCheckOutFragment<FragmentConciergeBinding>
        implements ConciergeFragmentViewModel.ConciergeFragmentViewModelListener,
        SupportConciergeCheckoutBottomSheetDisplay {

    @Inject
    public ApiService mApiService;
    private ConciergeFragmentViewModel mViewModel;
    private ConciergeFragmentAdapter mConciergeScreenAdapter;
    private LoadMoreLinearLayoutManager2 mLoadMoreLayoutManager;
    private LoadMoreLinearLayoutManager2.LoadMoreLinearLayoutManagerListener mLoadMoreLinearLayoutManagerListener;
    private ConciergeFragmentDataManager mDataManager;

    private BroadcastReceiver mShowTrackingOrderReceiver;
    private BroadcastReceiver mAddressUpdatedReceiver;
    private BroadcastReceiver mAddressDeletedReceiver;
    private BroadcastReceiver mAddressSelectedReceiver;
    private BroadcastReceiver mShopStatusUpdatedReceiver;
    private boolean mIsInExpressMode;

    public static ConciergeFragment newInstance() {
        Bundle args = new Bundle();

        ConciergeFragment fragment = new ConciergeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mIsInExpressMode = ApplicationHelper.isInExpressMode(requireContext());
        initShowTrackingOrderReceiver();
        initAddressUpdatedEvent();
        initAddressDeletedEvent();
        initLoadMoreLinearLayoutManagerListener();
        initShopStatusUpdatedEvent();

        mLoadMoreLayoutManager = new LoadMoreLinearLayoutManager2(requireActivity(),
                getBinding().conciergeRecyclerView,
                false);
        getBinding().conciergeRecyclerView.setLayoutManager(mLoadMoreLayoutManager);
        mDataManager = new ConciergeFragmentDataManager(requireContext(), mApiService, mIsInExpressMode);
        mViewModel = new ConciergeFragmentViewModel(mDataManager,
                getCheckOutViewModelInstance(),
                this);
        mConciergeScreenAdapter = new ConciergeFragmentAdapter(new ArrayList<>(),
                false,
                mIsInExpressMode,
                getChildFragmentManager(),
                getLifecycle(),
                mViewModel);
        getBinding().conciergeRecyclerView.setAdapter(mConciergeScreenAdapter);
        notifyShouldShowCartBottomSheet();

        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    protected boolean shouldListenUserLoginSuccessEven() {
        return true;
    }

    @Override
    protected void onRequestPermissionLauncherCallback(Map<String, Boolean> permissionsResult) {
        boolean fineLocationEnabled = false;
        boolean coarseLocationEnabled = false;
        if (permissionsResult.containsKey(Manifest.permission.ACCESS_FINE_LOCATION)) {
            Boolean permission = permissionsResult.get(Manifest.permission.ACCESS_FINE_LOCATION);
            if (permission != null && permission) {
                fineLocationEnabled = true;
            }
        }

        if (permissionsResult.containsKey(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            Boolean permission = permissionsResult.get(Manifest.permission.ACCESS_COARSE_LOCATION);
            if (permission != null && permission) {
                coarseLocationEnabled = true;
            }
        }

        if (fineLocationEnabled || coarseLocationEnabled) {
            if (mViewModel.getCurrentLocationViewModel().get() != null) {
                mViewModel.getCurrentLocationViewModel().get().loadUserSelectedLocation();
            }
        }
    }

    @Override
    protected void onRefreshItemCountInList() {
        if (mConciergeScreenAdapter != null) {
            mConciergeScreenAdapter.refreshProductInCardCounter();
        }
    }

    @Override
    protected ApiService getPrivateAPIService() {
        return mApiService;
    }

    @Override
    protected void showLoading() {
        mViewModel.showLoading(true);
    }

    @Override
    protected void hideLoading() {
        mViewModel.hideLoading();
    }

    @Override
    protected void onReceivedRefreshProductOrderCounterEvent(List<ConciergeMenuItem> items) {
        if (items != null && !items.isEmpty()) {
            if (mConciergeScreenAdapter != null) {
                ArrayList<String> ids = new ArrayList<>();
                for (ConciergeMenuItem item : items) {
                    ids.add(item.getId());
                }
                mConciergeScreenAdapter.updateProductOrderCounter(ids);
            }
        }
    }

    private void initShowTrackingOrderReceiver() {
        mShowTrackingOrderReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean shouldShowTrackingButton =
                        intent.getBooleanExtra(ConciergeHelper.SHOW_TRACKING_ORDER_BUTTON_EVENT_DATA, false);
                Timber.i("onReceive showTrackingOrderEvent: " + shouldShowTrackingButton);
                if (mViewModel != null) {
                    mViewModel.setIsHasPending(shouldShowTrackingButton);
                }
            }
        };
        requireContext().registerReceiver(mShowTrackingOrderReceiver,
                new IntentFilter(ConciergeHelper.SHOW_TRACKING_ORDER_BUTTON_EVENT));
    }

    private void initAddressUpdatedEvent() {
        mAddressUpdatedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ConciergeUserAddress updatedAddress = intent.getParcelableExtra(ConciergeHelper.ADDRESS_EVENT_DATA);
                Timber.i("onReceive addressUpdatedEvent");
                if (mViewModel != null && updatedAddress != null) {
                    mViewModel.onUserAddressUpdated(updatedAddress);
                }
            }
        };
        requireContext().registerReceiver(mAddressUpdatedReceiver,
                new IntentFilter(ConciergeHelper.ADDRESS_UPDATED_EVENT));
    }

    private void initAddressDeletedEvent() {
        mAddressDeletedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ConciergeUserAddress deletedAddress = intent.getParcelableExtra(ConciergeHelper.ADDRESS_EVENT_DATA);
                Timber.i("onReceive addressDeletedEvent");
                if (mViewModel != null && deletedAddress != null) {
                    mViewModel.onUserAddressDeleted(deletedAddress);
                }
            }
        };
        requireContext().registerReceiver(mAddressDeletedReceiver,
                new IntentFilter(ConciergeHelper.ADDRESS_DELETED_EVENT));
    }

    private void initAddressSelectedEvent() {
        mAddressSelectedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ConciergeUserAddress selectedAddress = intent.getParcelableExtra(ConciergeHelper.ADDRESS_EVENT_DATA);
                boolean isCurrentLocation = intent.getBooleanExtra(ConciergeHelper.ADDRESS_IS_CURRENT_LOCATION_DATA, false);
                Timber.i("onReceive addressSelectedEvent");
                if (mViewModel != null) {
                    if (isCurrentLocation) {
                        mViewModel.loadUserCurrentLocation();
                    } else if (selectedAddress != null) {
                        mViewModel.onUserAddressSelected(selectedAddress);
                    }
                }
            }
        };
        requireContext().registerReceiver(mAddressSelectedReceiver,
                new IntentFilter(ConciergeHelper.ADDRESS_SELECTED_EVENT));
    }

    private void initShopStatusUpdatedEvent() {
        mShopStatusUpdatedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive shopStatusUpdatedEvent");
                if (mViewModel != null) {
                    mViewModel.makeSilentRequestOfShopAnnouncement();
                    mViewModel.onShopStatusUpdated(intent.getBooleanExtra(ConciergeHelper.FIELD_IS_SHOP_CLOSED,
                            false));
                }
            }
        };
        requireContext().registerReceiver(mShopStatusUpdatedReceiver,
                new IntentFilter(ConciergeHelper.SHOP_STATUS_UPDATED_EVENT));
    }

    public void scrollToTopAndRefresh(boolean shouldRefresh) {
        if (findFirstVisibleItemPosition() != 0) {
            if (shouldRefresh) {
                mViewModel.onRefresh();
            }
            BetterSmoothScrollUtil.startScroll(getBinding().conciergeRecyclerView,
                    0,
                    findFirstVisibleItemPosition());
        }
    }

    public int findFirstVisibleItemPosition() {
        return mLoadMoreLayoutManager.findFirstVisibleItemPosition();
    }

    @Override
    public void onLoadData(List<ConciergeItemAdaptive> data,
                           boolean isRefresh,
                           boolean isCanLoadMoreTrending) {
        if (!isFragmentStillInValidState()) {
            return;
        }

        if (isRefresh) {
            mConciergeScreenAdapter.refreshData(data);
            notifyShouldShowCartBottomSheet();
        } else {
            mConciergeScreenAdapter.setData(data);
        }
        mConciergeScreenAdapter.setCanLoadMore(isCanLoadMoreTrending);

        if (isCanLoadMoreTrending) {
            mLoadMoreLayoutManager.setLoadMoreLinearLayoutManagerListener(mLoadMoreLinearLayoutManagerListener);
        }
        notifyShouldShowCartBottomSheet();
    }

    @Override
    public void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {
        onAddFirstItemToCart(mViewModel, conciergeMenuItem);
    }

    @Override
    public void onItemAddedToCart(ConciergeMenuItem menuItem) {
        Timber.i("onItemAddedToCart: " + new Gson().toJson(menuItem));
        if (mConciergeScreenAdapter != null) {
            mConciergeScreenAdapter.onItemUpdatedInCart(menuItem);
        }
        notifyShouldShowCartBottomSheet();
    }

    @Override
    protected void onItemRemovedFromCart(ConciergeMenuItem menuItem) {
        Timber.i("onItemRemovedFromCart");
        if (mConciergeScreenAdapter != null) {
            mConciergeScreenAdapter.onItemUpdatedInCart(menuItem);
        }
    }

    @Override
    protected void onCartItemCleared() {
        Timber.i("onCartItemCleared");
        if (mConciergeScreenAdapter != null) {
            mConciergeScreenAdapter.onCartCleared();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Timber.i("OnPause");
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.i("OnResume");
        notifyShouldShowCartBottomSheet();
    }

    public void checkToGetUserCurrentLocation() {
        if (mViewModel != null) {
            mViewModel.getUserSelectedLocation();
        }
    }

    public void updateOrderBadgeCount(long newCount) {
        if (mViewModel != null) {
            mViewModel.updateOrderBadgeCount(newCount);
        }
    }

    public void performLoadMore() {
        mViewModel.loadMoreData(new OnCallbackListListener<List<ConciergeItemAdaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (!isFragmentStillInValidState()) {
                    return;
                }

                mConciergeScreenAdapter.setCanLoadMore(false);
                mConciergeScreenAdapter.notifyDataSetChanged();
                mLoadMoreLayoutManager.loadingFinished();
                mLoadMoreLayoutManager.setLoadMoreLinearLayoutManagerListener(null);
                showSnackBar(ex.getMessage(), true);
                mViewModel.isAlreadyLoadMore = false;
            }

            @Override
            public void onComplete(List<ConciergeItemAdaptive> result, boolean canLoadMore) {
                if (!isFragmentStillInValidState()) {
                    return;
                }

                mConciergeScreenAdapter.setCanLoadMore(canLoadMore);
                if (!result.isEmpty()) {
                    if (mIsInExpressMode) {
                        mConciergeScreenAdapter.addLoadMoreSupplierData(result);
                    } else {
                        mConciergeScreenAdapter.addLoadMoreTrendingData(result);
                    }
                } else {
                    Timber.i("Load more done with empty data");
                    mLoadMoreLayoutManager.setLoadMoreLinearLayoutManagerListener(null);
                    getBinding().conciergeRecyclerView.post(() -> {
                        //Need to refresh to hide load more view.
                        mConciergeScreenAdapter.setCanLoadMore(false);
                        mConciergeScreenAdapter.removeLoadMoreLayout();
                        mConciergeScreenAdapter.notifyDataSetChanged();
                    });
                }
                mLoadMoreLayoutManager.loadingFinished();
                mViewModel.isAlreadyLoadMore = false;
            }
        });
    }

    private void initLoadMoreLinearLayoutManagerListener() {
        mLoadMoreLinearLayoutManagerListener = new LoadMoreLinearLayoutManager2.LoadMoreLinearLayoutManagerListener() {
            @Override
            public void onReachedLoadMoreBottom() {
                performLoadMore();
            }

            @Override
            public void onReachedLoadMoreByDefault() {
                performLoadMore();
            }
        };
    }

    @Override
    public void onAppBarOffsetChange(int offset) {
        int appBarHeight = getResources().getDimensionPixelSize(R.dimen.home_toolbar_height);
        View bottomSheet = getBinding().getRoot().findViewById(R.id.conciergeBottomSheet);
        if (bottomSheet != null) {
            bottomSheet.setTranslationY(-appBarHeight - offset);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requireContext().unregisterReceiver(mShowTrackingOrderReceiver);
        requireContext().unregisterReceiver(mAddressUpdatedReceiver);
        requireContext().unregisterReceiver(mAddressDeletedReceiver);
//        requireContext().unregisterReceiver(mAddressSelectedReceiver);
        requireContext().unregisterReceiver(mShopStatusUpdatedReceiver);
    }

    @Override
    public void onMakeSilentRequestShopSettingFinished(ConciergeShopAnnouncementResponse shopAnnouncementResponse) {
        if (mConciergeScreenAdapter != null) {
            mConciergeScreenAdapter.checkToUpdateNoticeBannerVisibility(shopAnnouncementResponse);
        }
    }
}
