
package com.proapp.sompom.newui;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.onesignal.OSSubscriptionObserver;
import com.onesignal.OneSignal;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.upload.FloatingVideoService;
import com.proapp.sompom.databinding.ActivityDefaultBinding;
import com.proapp.sompom.helper.AutoStartPermissionDialogHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.helper.WallSeeMoreTextHelper;
import com.proapp.sompom.intent.newintent.HomeIntent;
import com.proapp.sompom.newui.fragment.HomeFragment;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.Locale;

import javax.inject.Inject;

import timber.log.Timber;

public class HomeActivity extends AbsSoundControllerActivity<ActivityDefaultBinding> implements HomeFragment.HomeFragmentListener {

    private static final int RC_VIDEO_APP_PERM = 0x012;

    @Inject
    public ApiService mApiService;
    private boolean mShouldReloadScreen;
    private OSSubscriptionObserver mOSSubscriptionObserver;
    private Task<AppUpdateInfo> appUpdateInfoTask;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_default;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addOneSignalSubscriptionObserver();
        ConciergeHelper.clearAllDisplayingUpdateItemErrorId();
        MainApplication.setIsHomeScreenOpened(true);
        getControllerComponent().inject(this);
        WallSeeMoreTextHelper.clearLineEndIndexForSeeMoreText();
        HomeFragment fragment = HomeFragment.newInstance(false);
        setFragment(R.id.containerView,
                fragment,
                HomeFragment.TAG);
        new Handler().postDelayed(() -> AutoStartPermissionDialogHelper.checkToShowDialog(HomeActivity.this),
                2000);
        new Handler().postDelayed(() -> checkToNavigateToConciergeTab(getIntent(),
                        2000),
                2000);

        // check version code
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(HomeActivity.this);
        appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

    }

    @Override
    protected void onChatSocketServiceReady() {
        startSubscribeAllGroupChannel();
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.i("onResume mShouldReloadScreen: " + mShouldReloadScreen);
        if (isThereAppConfigurationChanged() || mShouldReloadScreen) {
            mShouldReloadScreen = false;
            completelyRecreateActivity();
        }

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                popupMessageUpdateVersion(HomeActivity.this);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        HomeFragment fragment = (HomeFragment) getFragment(HomeFragment.TAG);
        if (fragment != null && fragment.onBackPress()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Timber.i("onDestroy");
        if (mOSSubscriptionObserver != null) {
            OneSignal.removeSubscriptionObserver(mOSSubscriptionObserver);
        }
        SharedPrefUtils.setUserSelectedLocation(getApplicationContext(), null);
        FloatingVideoService.stop(this);
        MainApplication.setIsHomeScreenOpened(false);
        SharedPrefUtils.clearPreloadedConversation(this);
        stopAllServices();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Timber.i("onNewIntent: " + intent);
        checkToNavigateToConciergeTab(intent, -1);
    }

    private void checkToNavigateToConciergeTab(Intent intent, long delayBeforeNavigation) {
        HomeFragment fragment = (HomeFragment) getFragment(HomeFragment.TAG);
        if (fragment != null) {
            HomeIntent homeIntent = new HomeIntent(intent);
            if (homeIntent.getRedirection() != null && homeIntent.getRedirection() == HomeIntent.Redirection.Concierge) {
                if (delayBeforeNavigation > 0) {
                    new Handler().postDelayed(() -> {
                        if (fragment.isAddedToActivity()) {
                            fragment.checkGoToConciergeTab();
                        }
                    }, delayBeforeNavigation);
                } else {
                    fragment.checkGoToConciergeTab();
                }
            }
        }
    }

    @Override
    public void onShouldReloadScreen() {
        mShouldReloadScreen = true;
    }


    private void addOneSignalSubscriptionObserver() {
        if (SharedPrefUtils.isLogin(HomeActivity.this)) {
            mOSSubscriptionObserver = osSubscriptionStateChanges -> {
                if (osSubscriptionStateChanges.getTo() != null) {
//                    Timber.i("OneSignal onOSSubscriptionChanged to player id: " + osSubscriptionStateChanges.getTo().getUserId());
                    if (!TextUtils.isEmpty(osSubscriptionStateChanges.getTo().getUserId())) {
//                        Timber.i("Will add new OneSignal player id of " + osSubscriptionStateChanges.getTo().getUserId());
                        UserHelper.addOneSignalPlayerId(HomeActivity.this, mApiService, osSubscriptionStateChanges.getTo().getUserId());
                    }
                }
            };
            OneSignal.addSubscriptionObserver(mOSSubscriptionObserver);
        }
    }

    private void popupMessageUpdateVersion(Activity activity) {
        if (!activity.isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.DialogStyle));
            builder.setTitle(activity.getString(R.string.new_version_available));
            builder.setMessage(activity.getString(R.string.there_is_a_new_version_for_download_please_update));
            builder.setPositiveButton(String.format(Locale.US, "%s", "Update"), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    final String appPackageName = activity.getPackageName();
                    try {
                        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException exp) {
                        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
            });
            builder.setCancelable(false);
            builder.show();
        }
    }
}
