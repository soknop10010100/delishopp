package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.ConciergeShopProductByCategoryAdapter;
import com.proapp.sompom.databinding.FragmentConciergeCategoryDetailBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.intent.ConciergeCategoryDetailIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.SupportConciergeCheckoutBottomSheetDisplay;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeSubCategory;
import com.proapp.sompom.model.concierge.ConciergeSubCategoryLoadMoreDataModel;
import com.proapp.sompom.model.concierge.ConciergeSupportItemAndSubCategoryAdaptive;
import com.proapp.sompom.model.emun.ConciergeSortItemOption;
import com.proapp.sompom.model.emun.ConciergeViewItemByType;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeShopCategoryDataManager;
import com.proapp.sompom.viewmodel.ConciergeCategoryDetailFragmentViewModel;
import com.proapp.sompom.viewmodel.ConciergeSupportAddItemToCardViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 6/8/22.
 */

public class ConciergeCategoryDetailFragment extends AbsSupportConciergeViewItemByFragment<FragmentConciergeCategoryDetailBinding>
        implements ConciergeCategoryDetailFragmentViewModel.ConciergeCategoryDetailFragmentViewModelListener,
        SupportConciergeCheckoutBottomSheetDisplay {

    @Inject
    public ApiService mApiService;
    private ConciergeCategoryDetailFragmentViewModel mViewModel;

    public static ConciergeCategoryDetailFragment newInstance(String id) {
        Bundle args = new Bundle();
        args.putString(ConciergeCategoryDetailIntent.ID, id);

        ConciergeCategoryDetailFragment fragment = new ConciergeCategoryDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_CATEGORY_DETAIL;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_category_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mViewModel = new ConciergeCategoryDetailFragmentViewModel(requireContext(),
                new ConciergeShopCategoryDataManager(requireContext(),
                        mApiService,
                        getArguments().getString((ConciergeCategoryDetailIntent.ID))),
                getCheckOutViewModelInstance(),
                this);
        setVariable(BR.viewModel, mViewModel);
        mViewModel.onGetMainData();
        getBinding().layoutToolbar.toolbarTitle.setText("");
        setToolbar(getBinding().layoutToolbar.toolbar, true);
    }

    @Override
    protected RecyclerView getRecycleView() {
        return getBinding().recyclerView;
    }

    @Override
    protected AppCompatSpinner getSortSpinner() {
        return getBinding().sortSpinner;
    }

    @Override
    protected void onReachedLoadMoreBottom() {
        mViewModel.loadMoreItem();
    }

    @Override
    protected void onReachedLoadMoreByDefault() {
        mViewModel.loadMoreItem();
    }

    @Override
    public void onViewItemByOptionChanged(ConciergeViewItemByType selectedOption) {
        super.onViewItemByOptionChanged(selectedOption);
    }

    @Override
    protected void onRebuildItemListWhenViewItemByOptionChanged(List<ConciergeShopDetailDisplayAdaptive> existingData, boolean canLoadMore) {
        bindItemBAndCategory(new ArrayList<>(existingData),
                true,
                false,
                false,
                canLoadMore);
    }

    @Override
    protected void onItemSortOptionChanged(ConciergeSortItemOption option) {
        Timber.i("onItemSortOptionChanged: " + option);
        mViewModel.onApplySort(option);
    }

    @Override
    public void onItemAddedToCart(ConciergeMenuItem conciergeMenuItem) {
        Timber.i("onItemAddedToCart: " + conciergeMenuItem);
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onItemUpdatedInCart(conciergeMenuItem);
        }
        notifyShouldShowCartBottomSheet();
    }

    @Override
    protected void onItemRemovedFromCart(ConciergeMenuItem conciergeMenuItem) {
        Timber.i("onItemRemovedFromCart: " + conciergeMenuItem);
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onItemUpdatedInCart(conciergeMenuItem);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.i("OnResume");
        notifyShouldShowCartBottomSheet();
    }

    @Override
    protected void onCartItemCleared() {
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onCartCleared();
        }
    }

    @Override
    public void onLoadItemSuccess(List<ConciergeSupportItemAndSubCategoryAdaptive> itemList,
                                  boolean isLoadMore,
                                  boolean isCanLoadMore,
                                  boolean isAppliedFilterResult) {
        bindItemBAndCategory(new ArrayList<>(itemList),
                false,
                isAppliedFilterResult,
                isLoadMore,
                isCanLoadMore);
    }

    @Override
    public void onMainDataLoadSuccess(String categoryTitle) {
        getBinding().layoutToolbar.toolbarTitle.setText(categoryTitle);
    }

    @Override
    public void onApplyFilterCriteria(boolean isShowOutOfStockProduct, List<String> ids) {
        mViewModel.onApplyFilter(isShowOutOfStockProduct, ids);
    }

    @Override
    public void onClearFilterCriteria() {
        mViewModel.onApplyFilter(false, new ArrayList<>());
    }

    @Override
    public void onLoadMoreItemFailed() {
        super.onLoadMoreItemFailed();
    }

    private void bindItemBAndCategory(List<ConciergeShopDetailDisplayAdaptive> itemList,
                                      boolean isViewItemByTypeChanged,
                                      boolean isAppliedFilterResult,
                                      boolean isLoadMore,
                                      boolean isCanLoadMore) {
        Timber.i("bindShopInfoAndItemByCategory: isCanLoadMore: " + isCanLoadMore +
                ", isLoadMore: " + isLoadMore +
                ", itemList: " + itemList.size());
        if (mMainItemAdapter == null ||
                isViewItemByTypeChanged ||
                isAppliedFilterResult) {

            clearPreviousLayoutManagerIfNecessary();
            mMainItemAdapter = new ConciergeShopProductByCategoryAdapter(requireContext(),
                    mConciergeViewItemByType,
                    null,
                    itemList,
                    new ConciergeShopProductByCategoryAdapter.ShopProductAdapterCallback() {
                        @Override
                        public void onProductAddedFromDetail() {
                            notifyShouldShowCartBottomSheet();
                        }

                        @Override
                        public void onSubCategoryPerformLoadMoreItem(ConciergeSubCategoryLoadMoreDataModel loadMoreDataModel) {
                            mViewModel.loadMoreSubCategoryItem(loadMoreDataModel.getConciergeSubCategory(), new OnCallbackListener<Response<ConciergeSubCategory>>() {
                                @Override
                                public void onFail(ErrorThrowable ex) {
                                    showSnackBar(ex.getMessage(), true);
                                }

                                @Override
                                public void onComplete(Response<ConciergeSubCategory> result) {
                                    if (result.body() != null) {
//                                        Timber.i("onComplete: " + new Gson().toJson(result.body()));
                                        //Will add new loaded items and next page status to sub category item
                                        if (result.body().getMenuItems() != null && !result.body().getMenuItems().isEmpty()) {
                                            loadMoreDataModel.getConciergeSubCategory().getMenuItems().addAll(result.body().getMenuItems());
                                            loadMoreDataModel.getConciergeSubCategory().setNextPage(result.body().getNextPage());
                                        }

                                        mMainItemAdapter.addSubCategoryLoadMoreItem(result.body());
                                    }
                                }
                            });
                        }

                        @Override
                        public void onViewItemByOptionChanged(ConciergeViewItemByType selectedOption) {
                            ConciergeCategoryDetailFragment.this.onViewItemByOptionChanged(selectedOption);
                        }

                        @Override
                        public void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {
                            ConciergeCategoryDetailFragment.this.onAddFirstItemToCart(mViewModel, conciergeMenuItem);
                        }

                        @Override
                        public void onExpandSubCategory(int position) {
                            Timber.i("onExpandSubCategory: position " + position);
                            getBinding().recyclerView.post(() -> {
                                if (mConciergeViewItemByType == ConciergeViewItemByType.NORMAL) {
                                    mNormalItemLayoutManager.scrollToPositionWithOffset(position, 20);
                                } else {
                                    mGridItemLayoutManager.scrollToPositionWithOffset(position, 20);
                                }
                            });
                        }

                        @Override
                        public void showAddItemFromDifferentShopWarningPopup(ConciergeSupportAddItemToCardViewModel viewModel,
                                                                             OnCallbackListener<Response<SupportConciergeCustomErrorResponse>> clearBasketCallback) {
                            ConciergeCategoryDetailFragment.this.showAddItemFromDifferentShopWarningPopup(viewModel, clearBasketCallback);
                        }
                    });

            mMainItemAdapter.setConciergeViewItemByType(mConciergeViewItemByType);
            mCurrentLayoutManager = getLayoutManager(isCanLoadMore);
            mCurrentLayoutManager.setShouldDetectLoadMore(isCanLoadMore);
            mMainItemAdapter.setCanLoadMore(isCanLoadMore);
            getBinding().recyclerView.setLayoutManager((RecyclerView.LayoutManager) mCurrentLayoutManager);
            checkToAddOrRemoveItemDecorator();
            getBinding().recyclerView.setAdapter(mMainItemAdapter);
        } else {
            mCurrentLayoutManager.setLoadMoreLinearLayoutManagerListener(isCanLoadMore ? mLayoutManagerListener : null);
            mMainItemAdapter.setCanLoadMore(isCanLoadMore);
            mCurrentLayoutManager.setShouldDetectLoadMore(isCanLoadMore);
            if (isLoadMore) {
                mCurrentLayoutManager.loadingFinished();
            }
            mMainItemAdapter.addLoadMoreData(itemList);
        }
    }
}
