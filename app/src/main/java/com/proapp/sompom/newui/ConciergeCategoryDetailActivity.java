package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.intent.ConciergeCategoryDetailIntent;
import com.proapp.sompom.newui.fragment.ConciergeCategoryDetailFragment;

/**
 * Created by Chhom Veasna on 6/8/22.
 */
public class ConciergeCategoryDetailActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(ConciergeCategoryDetailFragment.newInstance(new ConciergeCategoryDetailIntent(getIntent()).getId()),
                ConciergeCategoryDetailFragment.class.getName());
    }
}
