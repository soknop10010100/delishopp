package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ViewDataBinding;

import com.google.gson.Gson;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.intent.ConciergeReviewCheckoutIntent;
import com.proapp.sompom.listener.BasketModificationListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeMenuItemManipulationInfo;
import com.proapp.sompom.model.concierge.ConciergeVerifyPaymentWithABAResponse;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.viewmodel.ConciergeSupportAddItemToCardViewModel;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

public abstract class AbsHandleConciergeBasketErrorFragment<T extends ViewDataBinding> extends AbsBindingFragment<T> implements BasketModificationListener {

    private static boolean sIsPaymentInProcessErrorConfirmationDialogDisplaying;

    private boolean mIsRequestedOpenABAAppOnDeviceForABAPay;
    private boolean mIsAlreadyReceivedABAPushbackResultForABAPay;
    private String mBasketId;
    private handleOnPaymentInProcessErrorListener mHandleOnPaymentInProcessErrorListener;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBasketId = ConciergeHelper.getBasketId(requireContext());
        ((MainApplication) requireActivity().getApplication()).addBasketModificationListener(this);
    }

    @Override
    public void onBasketModificationError(SupportConciergeCustomErrorResponse response,
                                          String errorMessage,
                                          ConciergeMenuItemManipulationInfo manipulationInfo) {
        Timber.i("onBasketModificationError: errorMessage: " + errorMessage +
                ", : class: " + getClass().getSimpleName() +
                ", isAdded: " + isAdded() +
                ", isVisible: " + isVisible() +
                ", isResumed: " + isResumed() +
                ", isRemoving(): " + isRemoving() +
                ", isFinishing(): " + requireActivity().isFinishing() +
                ", sIsPaymentInProcessErrorConfirmationDialogDisplaying: " + sIsPaymentInProcessErrorConfirmationDialogDisplaying
                + ", response: " + new Gson().toJson(response));
        //Will make sure only one error popup will be displaying
        if (isResumed() &&
                !isRemoving() &&
                !requireActivity().isFinishing() &&
                !sIsPaymentInProcessErrorConfirmationDialogDisplaying) {
            if (shouldHandleReceivingBasketModificationErrorFromAsynchronousProcess()) {
                hideLoading();
                if (response != null &&
                        response.getDataType() == SupportConciergeCustomErrorResponse.ResponseDataType.PAYMENT_IN_PROCESS) {
                    handleOnPaymentInProcessError(errorMessage, new handleOnPaymentInProcessErrorListener() {
                        @Override
                        public void onEnableBasketSuccess() {
                            Timber.i("onEnableBasketSuccess");
                            //Call to add or update item basket on server again.
                            if (manipulationInfo != null) {
                                ConciergeHelper.manipulateItemToRemoteBasketInBackground(requireContext(),
                                        getPrivateAPIService(),
                                        manipulationInfo);
                            }
                        }

                        @Override
                        public void onEnableBasketFailed(ErrorThrowable ex) {
                            showSnackBar(ex.getMessage(), true);
                        }

                        @Override
                        public void onRequestBasketFailed(ErrorThrowable ex) {
                            showSnackBar(ex.getMessage(), true);
                        }

                        @Override
                        public void makePaymentWithABAFailed(String errorMessage) {
                            showSnackBar(errorMessage, true);
                        }

                        @Override
                        public void makePaymentWithABASuccess() {
                            Timber.i("makePaymentWithABASuccess");
                            makePaymentWithABASuccess();
                        }
                    });
                } else {
                    if (manipulationInfo.getMenuItem() != null) {
                        if (ConciergeHelper.shouldDisplayUpdateItemError(manipulationInfo.getMenuItem().getId())) {
                            ConciergeHelper.showUpdateItemInBasketErrorPopup((AppCompatActivity) requireActivity(),
                                    getString(R.string.shop_request_update_item_to_basket_error,
                                            manipulationInfo.getMenuItem().getName()),
                                    manipulationInfo.getMenuItem().getId());
                        }
                    }
                }
            }
        }
    }

    protected boolean shouldHandleReceivingBasketModificationErrorFromAsynchronousProcess() {
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.i("onResume: mIsRequestOpenABAAppOnDeviceForABAPay: " + mIsRequestedOpenABAAppOnDeviceForABAPay +
                ", mIsAlreadyReceivedABAPushbackResultForABAPay: " + mIsAlreadyReceivedABAPushbackResultForABAPay);
        if (mIsRequestedOpenABAAppOnDeviceForABAPay && !mIsAlreadyReceivedABAPushbackResultForABAPay) {
            mIsRequestedOpenABAAppOnDeviceForABAPay = false;
            verifyOrderPaymentStatus(true, mHandleOnPaymentInProcessErrorListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((MainApplication) requireActivity().getApplication()).removeBasketModificationListener(this);
    }

    protected void handleOnPaymentInProcessError(String message, handleOnPaymentInProcessErrorListener listener) {
        mHandleOnPaymentInProcessErrorListener = listener;
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setCancelable(false);
        messageDialog.setTitle(getString(R.string.popup_error_title));
        messageDialog.setMessage(message);
        messageDialog.setRightText(getString(R.string.shop_popup_cancel_payment_button), view -> {
            sIsPaymentInProcessErrorConfirmationDialogDisplaying = false;
            showLoading();
            ConciergeHelper.requestToEnableBasket(requireContext(),
                    getPrivateAPIService(),
                    new OnCallbackListener<Response<SupportConciergeCustomErrorResponse>>() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                            hideLoading();
                            if (listener != null) {
                                listener.onEnableBasketFailed(ex);
                            }
                        }

                        @Override
                        public void onComplete(Response<SupportConciergeCustomErrorResponse> result) {
                            hideLoading();
                            if (listener != null) {
                                listener.onEnableBasketSuccess();
                            }
                        }
                    });
        });
        messageDialog.setLeftText(getString(R.string.shop_popup_continue_payment_button), view -> {
            //Resume payment will be handled in review checkout screen
            requireContext().startActivity(new ConciergeReviewCheckoutIntent(requireContext()));
            if (this instanceof ConciergeReviewCheckoutFragment || this instanceof ConciergeProductDetailFragment) {
                requireActivity().finish();
            }
        });
        Timber.i("Show messageDialog in " + getClass().getSimpleName());
        messageDialog.show(getChildFragmentManager());
        sIsPaymentInProcessErrorConfirmationDialogDisplaying = true;
    }

    private void verifyOrderPaymentStatus(boolean isABAPayOption, handleOnPaymentInProcessErrorListener listener) {
        Timber.i("verifyOrderPaymentStatus for " + (isABAPayOption ? "ABA Pay" : "ABA Card"));
        showLoading();
        Observable<Response<ConciergeVerifyPaymentWithABAResponse>> observable = ConciergeHelper.verifyOrderPaymentStatus(requireContext(), getPrivateAPIService());
        ResponseObserverHelper<Response<ConciergeVerifyPaymentWithABAResponse>> helper = new ResponseObserverHelper<>(getContext(), observable);
        helper.execute(new OnCallbackListener<Response<ConciergeVerifyPaymentWithABAResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
            }

            @Override
            public void onComplete(Response<ConciergeVerifyPaymentWithABAResponse> result) {
                if (result.body() != null && result.body().isPaid()) {
                    //Mean the the payment is success.
                    ConciergeHelper.clearLocalBasket(requireContext(),
                            new OnCallbackListener<Response<Object>>() {
                                @Override
                                public void onFail(ErrorThrowable ex) {
                                    hideLoading();
                                    onMakePaymentWithABASuccess(listener);
                                }

                                @Override
                                public void onComplete(Response<Object> result) {
                                    hideLoading();
                                    onMakePaymentWithABASuccess(listener);
                                }
                            });
                } else {
                    hideLoading();
                }
            }
        });
    }

    protected void onMakePaymentWithABASuccess(handleOnPaymentInProcessErrorListener listener) {
        if (listener != null) {
            listener.makePaymentWithABASuccess();
        }
    }

    public void showOrderErrorPopup(String errorMessage, View.OnClickListener okClickListener) {
        ConciergeHelper.showCheckoutErrorPopup((AppCompatActivity) requireActivity(), errorMessage, okClickListener);
    }

    public void showAddItemFromDifferentShopWarningPopup(ConciergeSupportAddItemToCardViewModel viewModel,
                                                         OnCallbackListener<Response<SupportConciergeCustomErrorResponse>> clearBasketCallback) {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setCancelable(false);
        messageDialog.setTitle(getString(R.string.shop_add_item_to_cart_different_shop_error_title));
        messageDialog.setMessage(getString(R.string.shop_add_item_to_cart_different_shop_error));
        messageDialog.setRightText(getString(R.string.shop_add_item_to_cart_different_shop_error_cancel), null);
        messageDialog.setLeftText(getString(R.string.shop_add_item_to_cart_different_shop_error_remove), view -> {
            viewModel.requestToClearBasket(clearBasketCallback);
        });
        messageDialog.show(requireActivity().getSupportFragmentManager());
    }

    protected ApiService getPrivateAPIService() {
        //Client implement...
        return null;
    }

    protected void showLoading() {
        //Client implement...
    }

    protected void hideLoading() {
        //Client implement...
    }

    public interface handleOnPaymentInProcessErrorListener {

        void onEnableBasketSuccess();

        void onEnableBasketFailed(ErrorThrowable ex);

        void onRequestBasketFailed(ErrorThrowable ex);

        void makePaymentWithABAFailed(String errorMessage);

        void makePaymentWithABASuccess();
    }
}
