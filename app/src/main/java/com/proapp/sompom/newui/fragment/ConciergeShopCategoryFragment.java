package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.ConciergeShopProductByCategoryAdapter;
import com.proapp.sompom.adapter.newadapter.ConciergeShopCategoryAdapter;
import com.proapp.sompom.databinding.FragmentConciergeShopCategoryBinding;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.SupportConciergeCheckoutBottomSheetDisplay;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeShop;
import com.proapp.sompom.model.concierge.ConciergeShopCategory;
import com.proapp.sompom.model.concierge.ConciergeSubCategory;
import com.proapp.sompom.model.concierge.ConciergeSubCategoryLoadMoreDataModel;
import com.proapp.sompom.model.concierge.ConciergeSupplier;
import com.proapp.sompom.model.concierge.ConciergeSupportItemAndSubCategoryAdaptive;
import com.proapp.sompom.model.emun.ConciergeSortItemOption;
import com.proapp.sompom.model.emun.ConciergeViewItemByType;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeShopCategoryDataManager;
import com.proapp.sompom.viewmodel.ConciergeSupportAddItemToCardViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeShopCategoryFragmentViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Veasna Chhom on 26/1/22.
 */
public class ConciergeShopCategoryFragment extends AbsSupportConciergeViewItemByFragment<FragmentConciergeShopCategoryBinding> implements
        ConciergeShopCategoryFragmentViewModel.ConciergeShopCategoryFragmentViewModelListener,
        ConciergeShopCategoryAdapter.ConciergeShopCategoryAdapterListener,
        SupportConciergeCheckoutBottomSheetDisplay {

    @Inject
    public ApiService mApiService;
    private ConciergeShopCategoryFragmentViewModel mViewModel;
    private ConciergeShopCategoryAdapter mShopCategoryAdapter;

    public static ConciergeShopCategoryFragment newInstance() {

        Bundle args = new Bundle();

        ConciergeShopCategoryFragment fragment = new ConciergeShopCategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_shop_category;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mConciergeViewItemByType = ConciergeHelper.getSelectedViewItemByOption(requireContext());
        getControllerComponent().inject(this);
        mViewModel = new ConciergeShopCategoryFragmentViewModel(new ConciergeShopCategoryDataManager(requireContext(), mApiService, (ConciergeSupplier) null),
                false,
                getCheckOutViewModelInstance(),
                this);
        notifyShouldShowCartBottomSheet();
        setVariable(BR.viewModel, mViewModel);
        mViewModel.requestBatchShopCategoryData(false);
        //Must call this code below to show bottom checkout view counter
        mViewModel.getCartViewModel().updateCheckoutButtonStatus(false);
    }

    @Override
    protected RecyclerView getRecycleView() {
        return getBinding().itemRecyclerView;
    }

    @Override
    protected AppCompatSpinner getSortSpinner() {
        return null;
    }

    @Override
    protected void onReachedLoadMoreBottom() {
        if (mViewModel.getSelectedCategory() != null) {
            mViewModel.requestMoreItemOfSelectedCategory(mViewModel.getSelectedCategory());
        }
    }

    @Override
    protected void onReachedLoadMoreByDefault() {
        if (mViewModel.getSelectedCategory() != null) {
            mViewModel.requestMoreItemOfSelectedCategory(mViewModel.getSelectedCategory());
        }
    }

    @Override
    protected void onItemSortOptionChanged(ConciergeSortItemOption option) {
        //Do nothing
    }

    @Override
    protected void onRebuildItemListWhenViewItemByOptionChanged(List<ConciergeShopDetailDisplayAdaptive> existingData, boolean canLoadMore) {
        if (!isFragmentStillInValidState()) {
            return;
        }

        bindItemBAndCategory(new ArrayList<>(existingData),
                true,
                false,
                false,
                false,
                false,
                canLoadMore);
    }

    @Override
    public void onLoadBatchDataSuccess(List<ConciergeShopCategory> categories, boolean isRefreshWholeScreen) {
        if (!isFragmentStillInValidState()) {
            return;
        }

        Timber.i("onLoadBatchDataSuccess: isRefreshWholeScreen: " + isRefreshWholeScreen);
        bindCategory(categories, isRefreshWholeScreen);
    }

    private List<ConciergeShopDetailDisplayAdaptive> returnList(ConciergeShopDetailDisplayAdaptive adaptive) {
        List<ConciergeShopDetailDisplayAdaptive> list = new ArrayList<>();
        list.add(adaptive);

        return list;
    }

    @Override
    protected void onRefreshItemCountInList() {
        if (mMainItemAdapter != null) {
            mMainItemAdapter.refreshProductInCardCounter();
        }
    }

    @Override
    protected void onReceivedRefreshProductOrderCounterEvent(List<ConciergeMenuItem> items) {
        if (items != null && !items.isEmpty()) {
            if (mMainItemAdapter != null) {
                ArrayList<String> ids = new ArrayList<>();
                for (ConciergeMenuItem item : items) {
                    ids.add(item.getId());
                }
                mMainItemAdapter.updateProductOrderCounter(ids);
            }
        }
    }

    @Override
    protected ApiService getPrivateAPIService() {
        return mApiService;
    }

    @Override
    protected void showLoading() {
        mViewModel.showLoading(true);
    }

    @Override
    protected void hideLoading() {
        mViewModel.hideLoading();
    }

    @Override
    public void onLoadItemByCategorySuccess(List<ConciergeSupportItemAndSubCategoryAdaptive> itemList,
                                            boolean isLoadMore,
                                            boolean isCanLoadMore,
                                            boolean isAppliedFilterResult) {
        if (!isFragmentStillInValidState()) {
            return;
        }

        Timber.i("onLoadItemByCategorySuccess: isLoadMore: " + isLoadMore +
                ", isCanLoadMore: " + isCanLoadMore);
        ArrayList<ConciergeShopDetailDisplayAdaptive> data = new ArrayList<>(itemList);
        if (isAppliedFilterResult) {
            data.add(0, mViewModel.getSelectedCategory());
        }
        bindItemBAndCategory(data,
                false,
                isAppliedFilterResult,
                false,
                true,
                isLoadMore,
                isCanLoadMore);
    }

    private void bindCategory(List<ConciergeShopCategory> categories, boolean isRefresh) {
        if (mShopCategoryAdapter == null || isRefresh) {
            mShopCategoryAdapter = new ConciergeShopCategoryAdapter(categories, this);
            getBinding().categoryRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
            getBinding().categoryRecyclerView.setAdapter(mShopCategoryAdapter);
        }
    }

    @Override
    public void onCategoryClicked(ConciergeShopCategory category) {
        Timber.i("onCategoryClicked: " + category);
        if (!isFragmentStillInValidState()) {
            return;
        }

        if (!mViewModel.isCategorySelected(category)) {
            resetFilterAndSortCriteria();
            bindItemBAndCategory(returnList(category),
                    false,
                    false,
                    true,
                    false,
                    false,
                    false);
            mViewModel.onCategorySelected(category);
            //Reset item in list
            resetItemAdapter();
            /*
                Since we manage the default category, ALL, outside other category's list, we have to
                unselect it manually if another category is selected. And if default category is selected,
                we will have to reset the one other selected category too.
            */
            if (!category.isAllCategoryType()) {
                mViewModel.checkToUnselectDefaultCategory();
            } else if (mShopCategoryAdapter != null) {
                mShopCategoryAdapter.resetAllSelection();
            }
            mViewModel.requestItemOnCategorySelected(category);
        }
    }

    private void resetFilterAndSortCriteria() {
        //Reset all sort and filter criteria
        if (mViewModel != null) {
            mViewModel.setSortBy(null);
            mViewModel.setConciergeSortItemOption(ConciergeSortItemOption.NAME_A_Z);
            mViewModel.updateFilterSelectedCriteria(false, new ArrayList<>());
        }
    }

    private void resetItemAdapter() {
        if (mMainItemAdapter != null) {
            mMainItemAdapter.setCanLoadMore(false);
        }
        if (mCurrentLayoutManager != null) {
            mCurrentLayoutManager.setShouldDetectLoadMore(false);
            mCurrentLayoutManager.setLoadMoreLinearLayoutManagerListener(null);
        }
    }

    @Override
    public ConciergeShopCategoryAdapter.ConciergeShopCategoryAdapterListener getCategoryListener() {
        return this;
    }

    @Override
    public void onItemAddedToCart(ConciergeMenuItem conciergeMenuItem) {
        Timber.i("onItemAddedToCart: " + conciergeMenuItem);
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onItemUpdatedInCart(conciergeMenuItem);
        }
        notifyShouldShowCartBottomSheet();
    }

    @Override
    protected void onItemRemovedFromCart(ConciergeMenuItem conciergeMenuItem) {
        Timber.i("onItemRemovedFromCart: " + conciergeMenuItem);
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onItemUpdatedInCart(conciergeMenuItem);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.i("OnResume");
        notifyShouldShowCartBottomSheet();
    }

    @Override
    protected void onCartItemCleared() {
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onCartCleared();
        }
    }

    @Override
    public void onLoadMoreItemFailed() {
        if (!isFragmentStillInValidState()) {
            return;
        }

        mMainItemAdapter.setCanLoadMore(false);
        if (mCurrentLayoutManager != null) {
            mCurrentLayoutManager.loadingFinished();
            mCurrentLayoutManager.setShouldDetectLoadMore(false);
        }
        mMainItemAdapter.removeLoadMoreLayout();
    }

    private void bindItemBAndCategory(List<ConciergeShopDetailDisplayAdaptive> itemList,
                                      boolean isViewItemByTypeChanged,
                                      boolean isAppliedFilterResult,
                                      boolean isSelectedCategoryItem,
                                      boolean isBindItemData,
                                      boolean isLoadMore,
                                      boolean isCanLoadMore) {
        Timber.i("bindShopInfoAndItemByCategory: isSelectedCategoryItem: " + isSelectedCategoryItem +
                ", isCanLoadMore: " + isCanLoadMore +
                ", isLoadMore: " + isLoadMore +
                ", isBindItemData: " + isBindItemData +
                ", itemList: " + itemList.size());
        if (mMainItemAdapter == null ||
                isViewItemByTypeChanged ||
                isAppliedFilterResult ||
                isSelectedCategoryItem) {
            Timber.i("Bind new: isSelectedCategoryItem: " + isSelectedCategoryItem);

            clearPreviousLayoutManagerIfNecessary();
            mMainItemAdapter = new ConciergeShopProductByCategoryAdapter(requireContext(),
                    mConciergeViewItemByType,
                    null,
                    itemList,
                    new ConciergeShopProductByCategoryAdapter.ShopProductAdapterCallback() {
                        @Override
                        public void onProductAddedFromDetail() {
                            notifyShouldShowCartBottomSheet();
                        }

                        @Override
                        public void onSubCategoryPerformLoadMoreItem(ConciergeSubCategoryLoadMoreDataModel loadMoreDataModel) {
                            mViewModel.loadMoreSubCategoryItem(loadMoreDataModel.getConciergeSubCategory(), new OnCallbackListener<Response<ConciergeSubCategory>>() {
                                @Override
                                public void onFail(ErrorThrowable ex) {
                                    showSnackBar(ex.getMessage(), true);
                                }

                                @Override
                                public void onComplete(Response<ConciergeSubCategory> result) {
                                    if (result.body() != null) {
//                                        Timber.i("onComplete: " + new Gson().toJson(result.body()));
                                        //Will add new loaded items and next page status to sub category item
                                        if (result.body().getMenuItems() != null && !result.body().getMenuItems().isEmpty()) {
                                            loadMoreDataModel.getConciergeSubCategory().getMenuItems().addAll(result.body().getMenuItems());
                                            loadMoreDataModel.getConciergeSubCategory().setNextPage(result.body().getNextPage());
                                        }

                                        mMainItemAdapter.addSubCategoryLoadMoreItem(result.body());
                                    }
                                }
                            });
                        }

                        @Override
                        public void onViewItemByOptionChanged(ConciergeViewItemByType selectedOption) {
                            ConciergeShopCategoryFragment.this.onViewItemByOptionChanged(selectedOption);
                        }

                        @Override
                        public void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {
                            ConciergeShopCategoryFragment.this.onAddFirstItemToCart(mViewModel, conciergeMenuItem);
                        }

                        @Override
                        public void onExpandSubCategory(int position) {
                            Timber.i("onExpandSubCategory: position " + position);
                            getBinding().itemRecyclerView.post(() -> {
                                if (mConciergeViewItemByType == ConciergeViewItemByType.NORMAL) {
                                    mNormalItemLayoutManager.scrollToPositionWithOffset(position, 20);
                                } else {
                                    mGridItemLayoutManager.scrollToPositionWithOffset(position, 20);
                                }
                            });
                        }

                        @Override
                        public void onApplyFilterCriteria(boolean isShowOutOfStockProduct, List<String> ids) {
                            if (mViewModel != null && mViewModel.getSelectedCategory() != null) {
                                mViewModel.onApplyFilter(isShowOutOfStockProduct, ids);
                            }
                        }

                        @Override
                        public void onClearFilterCriteria() {
                            if (mViewModel != null && mViewModel.getSelectedCategory() != null) {
                                mViewModel.onApplyFilter(false, new ArrayList<>());
                            }
                        }

                        @Override
                        public boolean shouldShowOutOfStockItem() {
                            if (mViewModel != null) {
                                return mViewModel.isDisplayOutOfStockItem();
                            }

                            return false;
                        }

                        @Override
                        public List<String> getBranIds() {
                            if (mViewModel != null) {
                                return mViewModel.getFilterBrandId();
                            }

                            return new ArrayList<>();
                        }

                        @Override
                        public void onItemSortOptionChanged(ConciergeSortItemOption option) {
                            Timber.i("onItemSortOptionChanged: " + option);
                            if (mMainItemAdapter != null &&
                                    mViewModel != null &&
                                    mViewModel.getSelectedCategory() != null) {
                                mMainItemAdapter.getCategoryViewModel().getSortCriteriaSelected().set(true);
                                mViewModel.onApplySort(option);
                            }
                        }

                        @Override
                        public void showAddItemFromDifferentShopWarningPopup(ConciergeSupportAddItemToCardViewModel viewModel, OnCallbackListener<Response<SupportConciergeCustomErrorResponse>> clearBasketCallback) {
                            ConciergeShopCategoryFragment.this.showAddItemFromDifferentShopWarningPopup(viewModel, clearBasketCallback);
                        }
                    });

            mMainItemAdapter.setConciergeViewItemByType(mConciergeViewItemByType);
            mCurrentLayoutManager = getLayoutManager(isCanLoadMore);
            mCurrentLayoutManager.setShouldDetectLoadMore(isCanLoadMore);
            mMainItemAdapter.setCanLoadMore(isCanLoadMore);
            getBinding().itemRecyclerView.setLayoutManager((RecyclerView.LayoutManager) mCurrentLayoutManager);
            checkToAddOrRemoveItemDecorator();
            getBinding().itemRecyclerView.setAdapter(mMainItemAdapter);
        } else {
            mCurrentLayoutManager.setLoadMoreLinearLayoutManagerListener(isCanLoadMore ? mLayoutManagerListener : null);
            mMainItemAdapter.setCanLoadMore(isCanLoadMore);
            mCurrentLayoutManager.setShouldDetectLoadMore(isCanLoadMore);
            if (isLoadMore) {
                mCurrentLayoutManager.loadingFinished();
            }
            mMainItemAdapter.addLoadMoreData(itemList);
        }
    }
}
