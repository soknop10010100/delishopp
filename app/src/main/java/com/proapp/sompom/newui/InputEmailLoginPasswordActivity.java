package com.proapp.sompom.newui;

import androidx.fragment.app.Fragment;

import com.proapp.sompom.intent.InputEmailLoginPasswordIntent;
import com.proapp.sompom.newui.fragment.InputEmailLoginPasswordFragment;

public class InputEmailLoginPasswordActivity extends SupportResetPasswordDeepLinkActivity {

    @Override
    protected Fragment getFragment() {
        return InputEmailLoginPasswordFragment.newInstance(new InputEmailLoginPasswordIntent(getIntent()).getPassingData());
    }

    @Override
    protected String getFragmentTag() {
        return InputEmailLoginPasswordFragment.TAG;
    }
}

