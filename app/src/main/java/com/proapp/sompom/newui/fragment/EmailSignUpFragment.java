package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentEmailSignUpBinding;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.EmailSignUpDataManager;
import com.proapp.sompom.viewmodel.EmailSignUpViewModel;

import javax.inject.Inject;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class EmailSignUpFragment extends AbsBindingFragment<FragmentEmailSignUpBinding> {

    @Inject
    @PublicQualifier
    public ApiService mPublicApiService;
    private EmailSignUpViewModel mViewModel;

    public static EmailSignUpFragment newInstance() {

        Bundle args = new Bundle();

        EmailSignUpFragment fragment = new EmailSignUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_email_sign_up;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mViewModel = new EmailSignUpViewModel(new EmailSignUpDataManager(requireContext(), mPublicApiService));
        setVariable(BR.viewModel, mViewModel);
    }
}
