package com.proapp.sompom.newui.fragment;

import androidx.annotation.UiThread;
import androidx.fragment.app.DialogFragment;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.injection.controller.ControllerComponent;
import com.proapp.sompom.injection.controller.ControllerModule;

import java.util.Objects;

public abstract class AbsDialogFragment extends DialogFragment {

    private ControllerComponent mControllerComponent;

    @UiThread
    public ControllerComponent getControllerComponent() {
        if (mControllerComponent == null) {
            mControllerComponent = ((MainApplication) Objects.requireNonNull(requireActivity()).getApplication())
                    .getApplicationComponent()
                    .newControllerComponent(new ControllerModule(getActivity()));
        }
        return mControllerComponent;
    }
}
