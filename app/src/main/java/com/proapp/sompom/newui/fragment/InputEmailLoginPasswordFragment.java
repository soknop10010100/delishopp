package com.proapp.sompom.newui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentInputEmailLoginPasswordBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.intent.ConfirmCodeIntent;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.result.AuthResponse;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.InputLoginPasswordDataManager;
import com.proapp.sompom.viewmodel.AbsLoginViewModel;
import com.proapp.sompom.viewmodel.InputEmailLoginPasswordViewModel;
import com.proapp.sompom.viewmodel.ResetPasswordViewModel;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 4/10/22.
 */
public class InputEmailLoginPasswordFragment extends AbsLoginFragment<FragmentInputEmailLoginPasswordBinding,
        InputEmailLoginPasswordViewModel> implements
        AbsLoginViewModel.AbsLoginViewModelListener {

    @PublicQualifier
    @Inject
    public ApiService mApiService;
    @Inject
    public ApiService mPrivateApiService;
    private BroadcastReceiver mResetPasswordSuccessEventReceiver;

    public static InputEmailLoginPasswordFragment newInstance(ExchangeAuthData data) {

        Bundle args = new Bundle();
        args.putParcelable(ConfirmCodeIntent.DATA, data);

        InputEmailLoginPasswordFragment fragment = new InputEmailLoginPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_LOGIN;
    }

    @Override
    protected boolean shouldRequestAllPermissionAtStartUp() {
        return false;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_input_email_login_password;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        registerResetPasswordSuccessEventReceiver();
        mViewModel = new InputEmailLoginPasswordViewModel(requireContext(),
                new InputLoginPasswordDataManager(requireContext(),
                        mApiService,
                        mPrivateApiService,
                        getArguments().getParcelable(ConfirmCodeIntent.DATA)),
                this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onLoginError(String message) {
        handleLoginError(message);
    }

    @Override
    public void onLoginSuccess() {
        handleOnLoginSuccess();
    }

    @Override
    public void onChangePasswordRequire(AuthResponse authResponse) {
        handleChangePasswordRequire(authResponse);
    }

    private void registerResetPasswordSuccessEventReceiver() {
        mResetPasswordSuccessEventReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String email = intent.getStringExtra(ResetPasswordViewModel.FIELD_EMAIL);
                Timber.i("Received ResetPasswordSuccessEventReceiver: email: " + email);
                mViewModel.onReceivedResetPasswordSuccessEvent(email);
            }
        };
        requireContext().registerReceiver(mResetPasswordSuccessEventReceiver, new IntentFilter(ResetPasswordViewModel.RESET_PASSWORD_SUCCESS_EVENT));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requireContext().unregisterReceiver(mResetPasswordSuccessEventReceiver);
    }
}
