package com.proapp.sompom.newui.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.LayoutEditUserPopupBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.newui.fragment.AbsDialogFragment;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.UpdateUserProfileDataManager;
import com.proapp.sompom.utils.SnackBarUtil;
import com.proapp.sompom.viewmodel.EditUserProfilePopupDataViewModel;

import javax.inject.Inject;

/**
 * Created by Chhom Veasna on 6/17/22.
 */
public class EditUserInfoDialog extends AbsDialogFragment implements EditUserProfilePopupDataViewModel.EditUserProfilePopupDataViewModelListener {

    @Inject
    public ApiService mApiService;
    private LayoutEditUserPopupBinding mBinding;
    private EditUserProfilePopupDataViewModel mViewModel;
    private EditUserInfoDialogListener mListener;

    public static EditUserInfoDialog newInstance(EditUserInfoDialogListener listener) {

        Bundle args = new Bundle();

        EditUserInfoDialog fragment = new EditUserInfoDialog();
        fragment.setCancelable(false);
        fragment.setListener(listener);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        requireDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.layout_edit_user_popup,
                container,
                false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mViewModel = new EditUserProfilePopupDataViewModel(requireContext(),
                new UpdateUserProfileDataManager(requireContext(),
                        mApiService),
                this);
        mBinding.setVariable(BR.viewModel, mViewModel);
        FlurryHelper.logEvent(requireContext(), FlurryHelper.SCREEN_EDIT_PROFILE_POPUP);
    }

    private void setListener(EditUserInfoDialogListener listener) {
        mListener = listener;
    }

    @Override
    public void onUpdateProfileSuccess() {
        if (mListener != null) {
            mListener.onUpdateSuccess();
        }
        dismiss();
    }

    @Override
    public void onCancelButtonClicked() {
        dismiss();
    }

    @Override
    public void onShowError(String error) {
        SnackBarUtil.showSnackBar(mBinding.getRoot(), error, true);
    }

    public interface EditUserInfoDialogListener {
        void onUpdateSuccess();
    }
}
