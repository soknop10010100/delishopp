package com.proapp.sompom.newui.dialog;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.view.View;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.DialogProductDetailActionButtonBinding;
import com.proapp.sompom.viewmodel.newviewmodel.ProductDetailActionButtonDialogViewModel;

/**
 * Created by nuonveyo on 8/1/18.
 */

public class ProductDetailActionButtonDialog extends AbsBindingBottomSheetDialog<DialogProductDetailActionButtonBinding> {
    public static ProductDetailActionButtonDialog newInstance() {
        return new ProductDetailActionButtonDialog();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_product_detail_action_button;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBinding().closeButton.setOnClickListener(v -> dismiss());
        ProductDetailActionButtonDialogViewModel viewModel = new ProductDetailActionButtonDialogViewModel();
        setVariable(BR.viewModel, viewModel);
    }
}
