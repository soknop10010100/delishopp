package com.proapp.sompom.newui.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.LayoutInputBinding;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.utils.SpecialTextRenderUtils;

/**
 * Created by He Rotha on 11/30/18.
 */
public class InputMessageDialog extends MessageDialog {

    private String mHint;
    private String mEditDescription;
    private EditText mEditText;
    private Validation mValidation;
    private InputMessageDialogListener mListener;

    public static InputMessageDialog newInstance() {

        Bundle args = new Bundle();

        InputMessageDialog fragment = new InputMessageDialog();
        fragment.setArguments(args);
        fragment.setDisplayDotIconForTitle(true);
        return fragment;
    }

    public void setInputLayout(String defaultValue, String editDescription, Validation validation) {
        mHint = defaultValue;
        mEditDescription = editDescription;
        mValidation = validation;
    }

    public void setListener(InputMessageDialogListener listener) {
        mListener = listener;
    }

    public String getCurrentInputText() {
        if (mEditText == null || TextUtils.isEmpty(mEditText.getText())) {
            return null;
        }
        return mEditText.getText().toString().trim();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setDismissWhenClickOnButtonLeft(false);

        final LayoutInflater inflater = LayoutInflater.from(getContext());
        LayoutInputBinding binding = DataBindingUtil.inflate(inflater, R.layout.layout_input, null, true);
        mEditText = binding.editText;
//        if (mValidation == Validation.Email) {
//            mEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
//            mEditText.setFilters(new InputFilter[]{});
//        }

//        if (mValidation == Validation.Store) {
//            TextViewBindingUtil.setStoreNameText(binding.titleTextView, mHint);
//        } else {
//            binding.titleTextView.setText(mHint);
//        }
        getBinding().view.addView(binding.getRoot());
        binding.titleTextView.setText(mEditDescription);
        binding.oldValueTextView.setText(mHint);

        mEditText.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                new Handler().post(() -> KeyboardUtil.showKeyboard(getActivity(), mEditText));
                mEditText.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

    }

    @Override
    public void setLeftText(String leftText, View.OnClickListener listener) {
        super.setLeftText(leftText, view -> {
            if (mEditText != null) {
                if (TextUtils.isEmpty(mEditText.getText())) {
                    mEditText.setError(getString(R.string.login_error_field_required));
                } else if (mValidation == Validation.Email
                        && !SpecialTextRenderUtils.isEmailAddress(mEditText.getText().toString())) {
                    mEditText.setError(getString(R.string.edit_profile_invalid_email_title));
                } else {
                    if (mListener != null) {
                        showLoading();
                        mListener.onPerformUpdating(InputMessageDialog.this, getCurrentInputText());
                    }
                }
            }
        });
    }

    public interface InputMessageDialogListener {
        void onPerformUpdating(InputMessageDialog dialog, String inputText);
    }

    public enum Validation {
        Required, Email, Store
    }
}
