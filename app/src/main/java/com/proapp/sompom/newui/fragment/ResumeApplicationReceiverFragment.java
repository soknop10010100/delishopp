package com.proapp.sompom.newui.fragment;

import android.os.Bundle;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentDeepLinkngReceiverBinding;

public class ResumeApplicationReceiverFragment extends AbsBindingFragment<FragmentDeepLinkngReceiverBinding> {

    public static ResumeApplicationReceiverFragment newInstance() {

        Bundle args = new Bundle();

        ResumeApplicationReceiverFragment fragment = new ResumeApplicationReceiverFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_resume_application_receiver;
    }
}
