package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.UserListAdapter;
import com.proapp.sompom.databinding.FragmentUserListBinding;
import com.proapp.sompom.listener.OnHomeMenuClick;
import com.proapp.sompom.listener.UserListAdaptive;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.UserContactDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.UserListViewModel;
import com.proapp.sompom.widget.LoaderMoreLayoutManager;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

public class UserListFragment extends AbsBindingFragment<FragmentUserListBinding>
        implements UserListViewModel.UserListViewModelListener, OnHomeMenuClick, LoaderMoreLayoutManager.OnLoadMoreCallback {

    private UserListViewModel mViewModel;
    @Inject
    public ApiService mApiService;
    private boolean mIsShow;
    private UserListAdapter<UserListAdaptive> mAdapter;
    private boolean mHasScrolledDown;
    private LoaderMoreLayoutManager mLinearLayoutManager;

    public static UserListFragment newInstance() {

        Bundle args = new Bundle();

        UserListFragment fragment = new UserListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_user_list;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        getControllerComponent().inject(this);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onLoadSuccess(List<UserListAdaptive> userGroups,
                              boolean isRefresh,
                              boolean isLoadMore,
                              boolean isCanLoadMore) {
        Timber.i("onLoadSuccess: " + userGroups.size() +
                ", isRefresh: " + isRefresh +
                ", isLoadMore: " + isLoadMore +
                ", isCanLoadMore: " + isCanLoadMore);
        if (mAdapter == null) {
            mAdapter = new UserListAdapter<>(userGroups, getContext());
            mAdapter.setListener(new UserListAdapter.UserListAdapterListener() {
                @Override
                public void onAttemptedCallWhenNoNetworkConnectedOrDeviceIsInAnotherCallProcess(boolean isInAnotherCall) {
                    showSnackBar(isInAnotherCall ? R.string.call_toast_already_in_call_description : R.string.call_toast_no_internet, true);
                }

                @Override
                public void onMakeCallError(String error) {
                    showSnackBar(error, true);
                }
            });
            mAdapter.setCanLoadMore(isCanLoadMore);
            mLinearLayoutManager = new LoaderMoreLayoutManager(requireContext());
            if (isCanLoadMore) {
                mLinearLayoutManager.setOnLoadMoreListener(this);
            }
            getBinding().recyclerView.setLayoutManager(mLinearLayoutManager);
            getBinding().recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    mHasScrolledDown = dy > 0;
                    super.onScrolled(recyclerView, dx, dy);
                }
            });
            getBinding().recyclerView.setAdapter(mAdapter);
        } else {
            if (!isCanLoadMore) {
                mAdapter.removeLoadMoreLayoutIfAny();
            }
            mAdapter.setCanLoadMore(isCanLoadMore);
            mLinearLayoutManager.setOnLoadMoreListener(isCanLoadMore ? this : null);
            if (isLoadMore) {
                mLinearLayoutManager.loadingFinished();
                mAdapter.addLoadMoreData(userGroups);
            } else {
                mAdapter.refreshData(userGroups);
            }
        }
    }

    @Override
    public void onTabMenuClick() {
        if (mIsShow || !isAdded()) {
            return;
        }
        mIsShow = true;
        initViewModel();
    }


    private boolean isListScrolled() {
        if (mLinearLayoutManager != null) {
            if (mLinearLayoutManager.findFirstVisibleItemPosition() != 0) {
                return true;
            } else {
                return mHasScrolledDown;
            }
        }

        return false;
    }

    private void initViewModel() {
        mViewModel = new UserListViewModel(requireContext(),
                new UserContactDataManager(requireContext(), mApiService),
                this);
        setVariable(BR.viewModel, mViewModel);
        mViewModel.initializeViewModel();
    }

    public void scrollToTopAndRefreshIfNecessary() {
        if (isListScrolled()) {
            getBinding().recyclerView.smoothScrollToPosition(0);
            mViewModel.onRefresh();
        }
    }

    @Override
    public void onLoadMoreFromBottom() {
        Timber.i("onLoadMoreFromBottom");
        //We don't have load more feature yet
    }

    @Override
    public boolean hasItemInList() {
        return mAdapter != null && mAdapter.getItemCount() > 0;
    }
}
