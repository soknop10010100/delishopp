package com.proapp.sompom.newui;

import android.os.Bundle;

import com.proapp.sompom.newui.fragment.LoginWithPhoneOnlyFragment;

import androidx.annotation.Nullable;

public class LoginWithPhoneOnlyActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(LoginWithPhoneOnlyFragment.newInstance(), LoginWithPhoneOnlyFragment.TAG);
    }
}
