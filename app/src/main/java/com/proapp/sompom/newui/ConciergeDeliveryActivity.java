package com.proapp.sompom.newui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.proapp.sompom.intent.ConciergeDeliveryIntent;
import com.proapp.sompom.model.ShopDeliveryTimeSelection;
import com.proapp.sompom.newui.fragment.ConciergeDeliveryFragment;

import timber.log.Timber;

public class ConciergeDeliveryActivity extends AbsDefaultActivity
        implements ConciergeDeliveryFragment.DeliveryFragmentCallback {

    private Context mContext;
    private ShopDeliveryTimeSelection mShopDeliveryTimeSelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getThis();
        ConciergeDeliveryIntent intent = new ConciergeDeliveryIntent(getIntent());
        setFragment(ConciergeDeliveryFragment.newInstance(intent.getSelection(), intent.isViewOnlyMode()),
                ConciergeDeliveryFragment.TAG);
    }

    @Override
    public void onDeliveryOptionSelected(ShopDeliveryTimeSelection shopDeliveryTimeSelection) {
        Timber.i("On new delivery option selected: \nDate: %s, \nStart: %s, \nEnd: %s",
                shopDeliveryTimeSelection.getSelectedDate(),
                shopDeliveryTimeSelection.getStartTime(),
                shopDeliveryTimeSelection.getEndTime());
        mShopDeliveryTimeSelection = shopDeliveryTimeSelection;
    }

    @Override
    public void onConfirmationButtonClicked() {
        if (mShopDeliveryTimeSelection != null) {
            if (mContext instanceof AbsBaseActivity) {
                Intent intent = new Intent();
                intent.putExtra(ConciergeDeliveryIntent.DATA, mShopDeliveryTimeSelection);
                ((AbsBaseActivity) mContext).setResult(AppCompatActivity.RESULT_OK, intent);
                ((AbsBaseActivity) mContext).finish();
            }
        }
    }
}
