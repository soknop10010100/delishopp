package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.ConciergeShopProductByCategoryAdapter;
import com.proapp.sompom.adapter.SortItemOptionArrayAdapter;
import com.proapp.sompom.decorataor.ConciergeExpressListItemSpacingDecorator;
import com.proapp.sompom.decorataor.ConciergeExpressMixedGridSpacingAndFullSpanItemDecoration;
import com.proapp.sompom.decorataor.ConciergeMixedGridSpacingAndFullSpanItemDecoration;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.listener.SupportConciergeCheckoutBottomSheetDisplay;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.emun.ConciergeSortItemOption;
import com.proapp.sompom.model.emun.ConciergeViewItemByType;
import com.proapp.sompom.widget.AbsLayoutManager;
import com.proapp.sompom.widget.ConciergeLoadMoreGridLayoutManager;
import com.proapp.sompom.widget.ConciergeLoadMoreLinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 5/5/22.
 */
public abstract class AbsSupportConciergeViewItemByFragment<T extends ViewDataBinding> extends AbsSupportShopCheckOutFragment<T> implements
        SupportConciergeCheckoutBottomSheetDisplay {

    protected ConciergeShopProductByCategoryAdapter mMainItemAdapter;
    protected ConciergeLoadMoreLinearLayoutManager mNormalItemLayoutManager;
    protected ConciergeLoadMoreGridLayoutManager mGridItemLayoutManager;
    protected AbsLayoutManager.AbsLayoutManagerListener mLayoutManagerListener;
    protected AbsLayoutManager mCurrentLayoutManager;
    protected ConciergeViewItemByType mConciergeViewItemByType;
    protected ConciergeSortItemOption mConciergeSortItemOption;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mConciergeViewItemByType = ConciergeHelper.getSelectedViewItemByOption(requireContext());
        bindSortOptionList();
        //Must init checkout view model instance first.
        getCheckOutViewModelInstance();
        notifyShouldShowCartBottomSheet();
        getCheckOutViewModelInstance().updateCheckoutButtonStatus(false);
    }

    protected abstract RecyclerView getRecycleView();

    protected abstract AppCompatSpinner getSortSpinner();

    protected abstract void onReachedLoadMoreBottom();

    protected abstract void onReachedLoadMoreByDefault();

    protected abstract void onItemSortOptionChanged(ConciergeSortItemOption option);

    protected abstract void onRebuildItemListWhenViewItemByOptionChanged(List<ConciergeShopDetailDisplayAdaptive> existingData, boolean canLoadMore);

    public void onViewItemByOptionChanged(ConciergeViewItemByType selectedOption) {
        mConciergeViewItemByType = selectedOption;
        rebuildProductItemDisplayOnViewItemByChanged();
    }

    private void rebuildProductItemDisplayOnViewItemByChanged() {
        List<ConciergeShopDetailDisplayAdaptive> existingData = mMainItemAdapter.getDatas();
        boolean canLoadMore = mMainItemAdapter.canLoadMore();
        onRebuildItemListWhenViewItemByOptionChanged(existingData, canLoadMore);
    }

    @Override
    protected void onRefreshItemCountInList() {
        if (mMainItemAdapter != null) {
            mMainItemAdapter.refreshProductInCardCounter();
        }
    }

    @Override
    protected void onReceivedRefreshProductOrderCounterEvent(List<ConciergeMenuItem> items) {
        if (items != null && !items.isEmpty()) {
            if (mMainItemAdapter != null) {
                ArrayList<String> ids = new ArrayList<>();
                for (ConciergeMenuItem item : items) {
                    ids.add(item.getId());
                }
                mMainItemAdapter.updateProductOrderCounter(ids);
            }
        }
    }

    private void resetItemAdapter() {
        if (mMainItemAdapter != null) {
            mMainItemAdapter.setCanLoadMore(false);
        }
        if (mCurrentLayoutManager != null) {
            mCurrentLayoutManager.setShouldDetectLoadMore(false);
            mCurrentLayoutManager.setLoadMoreLinearLayoutManagerListener(null);
        }
    }

    @Override
    public void onAppBarOffsetChange(int offset) {
        int appBarHeight = getResources().getDimensionPixelSize(R.dimen.home_toolbar_height);
        View bottomSheet = getBinding().getRoot().findViewById(R.id.conciergeBottomSheet);
        if (bottomSheet != null) {
            bottomSheet.setTranslationY(-appBarHeight - offset);
        }
    }

    @Override
    public void onItemAddedToCart(ConciergeMenuItem conciergeMenuItem) {
        Timber.i("onItemAddedToCart: " + conciergeMenuItem);
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onItemUpdatedInCart(conciergeMenuItem);
        }
    }

    @Override
    protected void onItemRemovedFromCart(ConciergeMenuItem conciergeMenuItem) {
        Timber.i("onItemRemovedFromCart: " + conciergeMenuItem);
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onItemUpdatedInCart(conciergeMenuItem);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.i("OnResume");
        notifyShouldShowCartBottomSheet();
    }

    @Override
    protected void onCartItemCleared() {
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onCartCleared();
        }
    }

    protected boolean isInExpressSupplierScreen() {
        return false;
    }

    protected void clearPreviousLayoutManagerIfNecessary() {
        if (getRecycleView().getLayoutManager() != null) {
            getRecycleView().setLayoutManager(null);
        }
        if (mCurrentLayoutManager != null) {
            mCurrentLayoutManager.setLoadMoreLinearLayoutManagerListener(null);
            mCurrentLayoutManager = null;
        }
    }

    protected void checkToAddOrRemoveItemDecorator() {
        mMainItemAdapter.removeItemDecoratorFromRecycleView(getRecycleView());
        if (mConciergeViewItemByType == ConciergeViewItemByType.TWO_COLUMNS || mConciergeViewItemByType == ConciergeViewItemByType.THREE_COLUMNS) {
            Timber.i("Add item decorator gird view: mConciergeViewItemByType: " + mConciergeViewItemByType + ", isInExpressSupplierScreen: " + isInExpressSupplierScreen());
            RecyclerView.ItemDecoration itemDecoration;
            if (!isInExpressSupplierScreen()) {
                itemDecoration = new ConciergeMixedGridSpacingAndFullSpanItemDecoration(mMainItemAdapter,
                        mConciergeViewItemByType.getSpanCount(),
                        mMainItemAdapter.getGridSpacingForItemDecorator(),
                        requireContext().getResources().getDimensionPixelSize(R.dimen.space_xlarge),
                        false);
            } else {
                itemDecoration = new ConciergeExpressMixedGridSpacingAndFullSpanItemDecoration(requireContext(),
                        mMainItemAdapter,
                        getResources().getDimensionPixelSize(R.dimen.space_large),
                        mConciergeViewItemByType.getSpanCount());
            }
            getRecycleView().addItemDecoration(itemDecoration);
        } else if (isInExpressSupplierScreen()) {
            getRecycleView().addItemDecoration(new ConciergeExpressListItemSpacingDecorator(requireContext()));
        }
    }

    protected AbsLayoutManager getLayoutManager(boolean isCanLoadMore) {
        AbsLayoutManager absLayoutManager;

        if (mConciergeViewItemByType == ConciergeViewItemByType.NORMAL) {
            mNormalItemLayoutManager = new ConciergeLoadMoreLinearLayoutManager(requireContext(),
                    getRecycleView(),
                    false);
            mNormalItemLayoutManager.setShouldDetectLoadMore(isCanLoadMore);
            absLayoutManager = mNormalItemLayoutManager;
        } else {
            mGridItemLayoutManager = new ConciergeLoadMoreGridLayoutManager(requireContext(),
                    getRecycleView(),
                    mConciergeViewItemByType.getSpanCount());
            mGridItemLayoutManager.setShouldDetectLoadMore(isCanLoadMore);
            mGridItemLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return mMainItemAdapter.getSpanCountOfItemBaseOnItsModel(position);
                }
            });
            absLayoutManager = mGridItemLayoutManager;
        }

        if (mLayoutManagerListener == null) {
            mLayoutManagerListener = new AbsLayoutManager.AbsLayoutManagerListener() {
                @Override
                public void onReachedLoadMoreBottom() {
                    Timber.i("onReachedLoadMoreBottom");
                    AbsSupportConciergeViewItemByFragment.this.onReachedLoadMoreBottom();
                }

                @Override
                public void onReachedLoadMoreByDefault() {
                    Timber.i("onReachedLoadMoreByDefault");
                    AbsSupportConciergeViewItemByFragment.this.onReachedLoadMoreByDefault();
                }
            };
        }

        Timber.i("getLayoutManager: isCanLoadMore: " + isCanLoadMore);
        absLayoutManager.setLoadMoreLinearLayoutManagerListener(isCanLoadMore ? mLayoutManagerListener : null);

        return absLayoutManager;
    }

    private void bindSortOptionList() {
        if (getSortSpinner() != null) {
            mConciergeSortItemOption = ConciergeHelper.getSortItemOption();
            SortItemOptionArrayAdapter adapter = new SortItemOptionArrayAdapter(requireContext());
            getSortSpinner().setAdapter(adapter);
            ConciergeSortItemOption sortItemOption = ConciergeHelper.getSortItemOption();
            if (sortItemOption != null) {
                int foundPosition = 0;
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (adapter.getItem(i) == sortItemOption) {
                        foundPosition = i;
                        break;
                    }
                }
                getSortSpinner().setSelection(foundPosition);
            }
            getSortSpinner().postDelayed(() -> getSortSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                    ConciergeSortItemOption sortItemOption = adapter.getItem(position);
                    if (mConciergeSortItemOption != sortItemOption) {
                        Timber.i("onItemSelected: " + position);
                        mConciergeSortItemOption = sortItemOption;
                        onItemSortOptionChanged(mConciergeSortItemOption);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            }), 100);
        }
    }

    public void onLoadMoreItemFailed() {
        mMainItemAdapter.setCanLoadMore(false);
        if (mCurrentLayoutManager != null) {
            mCurrentLayoutManager.loadingFinished();
            mCurrentLayoutManager.setShouldDetectLoadMore(false);
        }
        mMainItemAdapter.removeLoadMoreLayout();
    }
}
