package com.proapp.sompom.newui;

import android.os.Bundle;

import com.proapp.sompom.intent.newintent.SearchMessageIntent;
import com.proapp.sompom.newui.fragment.SearchGeneralContainerFragment;

public class SearchMessageActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SearchMessageIntent intent = new SearchMessageIntent(getIntent());
        setFragment(SearchGeneralContainerFragment.newInstance(intent.getQuery(),
                intent.getSupplierId(),
                intent.getSearchType()),
                SearchGeneralContainerFragment.TAG);
    }
}
