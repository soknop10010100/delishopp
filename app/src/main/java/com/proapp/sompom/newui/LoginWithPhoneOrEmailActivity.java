package com.proapp.sompom.newui;

import com.proapp.sompom.newui.fragment.LoginWithPhoneOrEmailFragment;

import androidx.fragment.app.Fragment;

public class LoginWithPhoneOrEmailActivity extends SupportResetPasswordDeepLinkActivity {

    @Override
    protected Fragment getFragment() {
        return LoginWithPhoneOrEmailFragment.newInstance();
    }

    @Override
    protected String getFragmentTag() {
        return LoginWithPhoneOrEmailFragment.TAG;
    }
}
