package com.proapp.sompom.newui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.ConciergeOrderItemAdapter;
import com.proapp.sompom.databinding.FragmentConceirgeReviewCheckoutBinding;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.intent.ConciergeProductDetailIntent;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.result.ConciergeCheckoutData;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeReviewCheckoutManager;
import com.proapp.sompom.viewmodel.ConciergeReviewCheckoutFragmentViewModel;
import com.sompom.baseactivity.ResultCallback;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 5/4/22.
 */
public class ConciergeReviewCheckoutFragment extends AbsSupportABAPaymentFragment<FragmentConceirgeReviewCheckoutBinding>
        implements ConciergeReviewCheckoutFragmentViewModel.ConciergeReviewCheckoutFragmentViewModelCallback,
        ConciergeOrderItemAdapter.ConciergeOrderItemAdapterCallback {

    private ConciergeReviewCheckoutFragmentViewModel mViewModel;
    @Inject
    public ApiService mApiService;
    private ConciergeOrderItemAdapter mAdapter;
    private BroadcastReceiver mRefreshLocalBasketDisplayEventReceiver;

    public static ConciergeReviewCheckoutFragment newInstance() {
        return new ConciergeReviewCheckoutFragment();
    }

    @Override
    protected boolean shouldListenUserLoginSuccessEven() {
        return true;
    }

    @Override
    protected boolean shouldRegisterOrderSuccessEvent() {
        return true;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_REVIEW_CHECKOUT;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_conceirge_review_checkout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        registerRefreshLocalBasketDisplayEventReceiver();
        mViewModel = new ConciergeReviewCheckoutFragmentViewModel(requireContext(),
                new ConciergeReviewCheckoutManager(requireContext(), mApiService),
                this);
        setVariable(BR.viewModel, mViewModel);
        mViewModel.requestData(true, true, false);
    }

    @Override
    protected void onReceiveABAPaymentPushbackEvent(String basketId, boolean isPaymentSuccess, String message) {
        if (isPaymentSuccess) {
            requireActivity().finish();
        }
    }

    @Override
    public double getTotalPrice() {
        if (mAdapter != null) {
            return mAdapter.getTotalPrice();
        }

        return 0;
    }

    @Override
    public void needToScrollToSelectDeliverySlotButton() {
        getBinding().scrollContainer.post(() -> {
            final Rect rect = new Rect(0,
                    0,
                    getBinding().selectTimeSlotButton.getWidth(),
                    getBinding().selectTimeSlotButton.getHeight() * 2);
            getBinding().selectTimeSlotButton.requestRectangleOnScreen(rect, false);
        });
    }

    @Override
    protected void onReceivedOrderSuccessEvent() {
        requireActivity().finish();
    }

    @Override
    public void onRequestDataSuccess(ConciergeCheckoutData data) {
        mAdapter = new ConciergeOrderItemAdapter(data.getItemList(), this);
        mAdapter.setApiService(mApiService);
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        getBinding().recyclerView.setAdapter(mAdapter);
    }

    private void registerRefreshLocalBasketDisplayEventReceiver() {
        mRefreshLocalBasketDisplayEventReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                boolean isFromRollbackProductCount = intent.getBooleanExtra(ConciergeHelper.IS_FROM_ROLLBACK_PRODUCT_COUNT, false);
//                Timber.i("OnReceive registerRefreshLocalBasketDisplayEventReceiver, isFromRollbackProductCount: " + isFromRollbackProductCount);
                if (isFromRollbackProductCount) {
                    mViewModel.reloadData();
                }
            }
        };

        requireContext().registerReceiver(mRefreshLocalBasketDisplayEventReceiver,
                new IntentFilter(AbsSupportShopCheckOutFragment.REFRESH_LOCAL_BASKET_DISPLAY_EVENT));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requireContext().unregisterReceiver(mRefreshLocalBasketDisplayEventReceiver);
    }

    @Override
    public void onRefreshData(ConciergeCheckoutData data) {
        if (mAdapter != null) {
            mAdapter.onUpdateData(data.getItemList());
        }
    }

    @Override
    public void onItemEmpty() {
        onAllItemRemovedFromCart();
    }

    @Override
    public void onOrderSuccess() {
        requireActivity().setResult(AppCompatActivity.RESULT_OK);
        requireActivity().finish();
    }

    @Override
    protected void onMakePaymentWithABASuccess(handleOnPaymentInProcessErrorListener listener) {
        onOrderSuccess();
    }

    @Override
    public void onAllItemRemovedFromCart() {
        requireActivity().setResult(AppCompatActivity.RESULT_OK);
        requireActivity().finish();
    }

    @Override
    public void onItemRemoved(ConciergeMenuItem product) {
        if (mAdapter != null) {
            mAdapter.removeItem(product);
        }
    }

    @Override
    public void onItemClicked(ConciergeMenuItem product, int position) {
        Timber.i("onItemClicked: " + new Gson().toJson(product));
        if (requireActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) requireActivity()).startActivityForResult(new ConciergeProductDetailIntent(requireContext(),
                            product,
                            position),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            if (resultCode == AppCompatActivity.RESULT_OK) {
                                mViewModel.reloadData();
                            }
                        }
                    });
        }
    }

    @Override
    public void onRemoveWholeItemClicked(ConciergeMenuItem product) {
        mViewModel.requestToRemoveItemInBasket(product);
    }

    @Override
    public void onBasketModificationError(SupportConciergeCustomErrorResponse response, String errorMessage) {
        super.onBasketModificationError(response, errorMessage, null);
    }

    @Override
    protected ApiService getPrivateAPIService() {
        return mApiService;
    }

    @Override
    protected void showLoading() {
        mViewModel.showLoading(true);
    }

    @Override
    protected void hideLoading() {
        mViewModel.hideLoading();
    }

    @Override
    public void onRequireMinBasketValue(double minBasketPrice) {
        String minPrice = ConciergeHelper.getDisplayPrice(requireContext(), minBasketPrice);
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setCancelable(false);
        messageDialog.setTitle(getString(R.string.shop_checkout_alert_error));
        messageDialog.setMessage(getString(R.string.shop_checkout_alert_minimum_cart_amount_error, minPrice));
        messageDialog.setLeftText(getString(R.string.popup_ok_button), null);
        messageDialog.show(requireActivity().getSupportFragmentManager());
    }
}
