package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.ConciergeShopProductByCategoryAdapter;
import com.proapp.sompom.databinding.FragmentConciergeSubCategoryDetailBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.intent.ConciergeSubCategoryDetailIntent;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.SupportConciergeCheckoutBottomSheetDisplay;
import com.proapp.sompom.model.ConciergeShopDetailDisplayAdaptive;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeSubCategoryLoadMoreDataModel;
import com.proapp.sompom.model.emun.ConciergeSortItemOption;
import com.proapp.sompom.model.emun.ConciergeViewItemByType;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeSubCategoryDetailDataManager;
import com.proapp.sompom.viewmodel.ConciergeSubCategoryDetailFragmentViewModel;
import com.proapp.sompom.viewmodel.ConciergeSupportAddItemToCardViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 5/4/22.
 */

public class ConciergeSubCategoryDetailFragment extends AbsSupportConciergeViewItemByFragment<FragmentConciergeSubCategoryDetailBinding>
        implements ConciergeSubCategoryDetailFragmentViewModel.ConciergeSubCategoryDetailFragmentViewModelListener,
        SupportConciergeCheckoutBottomSheetDisplay {

    @Inject
    public ApiService mApiService;
    private ConciergeSubCategoryDetailFragmentViewModel mViewModel;

    public static ConciergeSubCategoryDetailFragment newInstance(String id) {
        Bundle args = new Bundle();
        args.putString(ConciergeSubCategoryDetailIntent.ID, id);

        ConciergeSubCategoryDetailFragment fragment = new ConciergeSubCategoryDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_SUB_CATEGORY_DETAIL;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_sub_category_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mViewModel = new ConciergeSubCategoryDetailFragmentViewModel(requireContext(),
                new ConciergeSubCategoryDetailDataManager(requireContext(),
                        mApiService,
                        getArguments().getString(ConciergeSubCategoryDetailIntent.ID)),
                getCheckOutViewModelInstance(),
                this);
        setVariable(BR.viewModel, mViewModel);
        mViewModel.onGetMainData();
        getBinding().layoutToolbar.toolbarTitle.setText("");
        setToolbar(getBinding().layoutToolbar.toolbar, true);
    }

    @Override
    protected RecyclerView getRecycleView() {
        return getBinding().recyclerView;
    }

    @Override
    protected AppCompatSpinner getSortSpinner() {
        return getBinding().sortSpinner;
    }

    @Override
    protected void onReachedLoadMoreBottom() {
        mViewModel.loadMoreItem();
    }

    @Override
    protected void onReachedLoadMoreByDefault() {
        mViewModel.loadMoreItem();
    }

    @Override
    public void onViewItemByOptionChanged(ConciergeViewItemByType selectedOption) {
        super.onViewItemByOptionChanged(selectedOption);
    }

    @Override
    protected void onRebuildItemListWhenViewItemByOptionChanged(List<ConciergeShopDetailDisplayAdaptive> existingData, boolean canLoadMore) {
        bindItemList(existingData, true, false, false, canLoadMore);
    }

    @Override
    public void onLoadItemSuccess(List<ConciergeMenuItem> itemList,
                                  boolean isRefreshMode,
                                  boolean isLoadMore,
                                  boolean isCanLoadMore) {
        bindItemList(new ArrayList<>(itemList), false, isRefreshMode, isLoadMore, isCanLoadMore);
    }

    @Override
    public void onLoadMoreItemFailed() {
        super.onLoadMoreItemFailed();
    }

    @Override
    public void onMainDataLoadSuccess(String subcategoryTitle) {
        getBinding().layoutToolbar.toolbarTitle.setText(subcategoryTitle);
    }

    @Override
    protected void onItemSortOptionChanged(ConciergeSortItemOption option) {
        mViewModel.onApplySort(option);
    }

    private void bindItemList(List<ConciergeShopDetailDisplayAdaptive> itemList,
                              boolean isViewByOptionChanged,
                              boolean isRefreshMode,
                              boolean isLoadMore,
                              boolean isCanLoadMore) {
        Timber.i("itemList size: " + itemList.size()
                + ", isRefreshMode: " + isRefreshMode
                + ", isLoadMore: " + isLoadMore
                + ", isCanLoadMore: " + isCanLoadMore);
        if (mMainItemAdapter == null || isViewByOptionChanged) {
            clearPreviousLayoutManagerIfNecessary();
            mMainItemAdapter = new ConciergeShopProductByCategoryAdapter(requireContext(),
                    mConciergeViewItemByType,
                    null,
                    new ArrayList<>(itemList),
                    new ConciergeShopProductByCategoryAdapter.ShopProductAdapterCallback() {
                        @Override
                        public void onProductAddedFromDetail() {

                        }

                        @Override
                        public void onSubCategoryPerformLoadMoreItem(ConciergeSubCategoryLoadMoreDataModel loadMoreDataModel) {

                        }

                        @Override
                        public void onViewItemByOptionChanged(ConciergeViewItemByType selectedOption) {

                        }

                        @Override
                        public void onExpandSubCategory(int position) {

                        }

                        @Override
                        public void onAddFirstItemToCart(ConciergeMenuItem conciergeMenuItem) {
                            ConciergeSubCategoryDetailFragment.this.onAddFirstItemToCart(mViewModel, conciergeMenuItem);
                        }

                        @Override
                        public void showAddItemFromDifferentShopWarningPopup(ConciergeSupportAddItemToCardViewModel viewModel, OnCallbackListener<Response<SupportConciergeCustomErrorResponse>> clearBasketCallback) {
                            ConciergeSubCategoryDetailFragment.this.showAddItemFromDifferentShopWarningPopup(viewModel, clearBasketCallback);
                        }
                    });
            mMainItemAdapter.setConciergeViewItemByType(mConciergeViewItemByType);
            mCurrentLayoutManager = getLayoutManager(isCanLoadMore);
            mCurrentLayoutManager.setShouldDetectLoadMore(isCanLoadMore);
            mMainItemAdapter.setCanLoadMore(isCanLoadMore);
            getBinding().recyclerView.setLayoutManager((RecyclerView.LayoutManager) mCurrentLayoutManager);
            checkToAddOrRemoveItemDecorator();
            getBinding().recyclerView.setAdapter(mMainItemAdapter);
        } else {
            mCurrentLayoutManager.setLoadMoreLinearLayoutManagerListener(isCanLoadMore ? mLayoutManagerListener : null);
            mMainItemAdapter.setCanLoadMore(isCanLoadMore);
            mCurrentLayoutManager.setShouldDetectLoadMore(isCanLoadMore);
            if (isRefreshMode) {
                mMainItemAdapter.setDatas(itemList);
                mMainItemAdapter.notifyDataSetChanged();
            } else {
                if (isLoadMore) {
                    mCurrentLayoutManager.loadingFinished();
                }
                mMainItemAdapter.addLoadMoreData(itemList);
            }
        }
    }

    @Override
    public void onItemAddedToCart(ConciergeMenuItem conciergeMenuItem) {
        Timber.i("onItemAddedToCart: " + conciergeMenuItem);
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onItemUpdatedInCart(conciergeMenuItem);
        }
        notifyShouldShowCartBottomSheet();
    }

    @Override
    protected void onItemRemovedFromCart(ConciergeMenuItem conciergeMenuItem) {
        Timber.i("onItemRemovedFromCart: " + conciergeMenuItem);
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onItemUpdatedInCart(conciergeMenuItem);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.i("OnResume");
        notifyShouldShowCartBottomSheet();
    }

    @Override
    protected void onCartItemCleared() {
        if (mMainItemAdapter != null) {
            mMainItemAdapter.onCartCleared();
        }
    }
}
