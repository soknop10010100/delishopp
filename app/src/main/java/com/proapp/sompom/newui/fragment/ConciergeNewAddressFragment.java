package com.proapp.sompom.newui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.CitySelectionItemOptionArrayAdapter;
import com.proapp.sompom.adapter.newadapter.ConciergeNewAddressLabelAdapter;
import com.proapp.sompom.databinding.FragmentConciergeNewAddressBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.intent.newintent.ConciergeNewAddressIntent;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.emun.CitySelectionType;
import com.proapp.sompom.model.emun.ConciergeUserAddressLabel;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeNewAddressDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeNewAddressFragmentViewModel;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 22/11/21.
 */
public class ConciergeNewAddressFragment extends AbsBindingFragment<FragmentConciergeNewAddressBinding>
        implements ConciergeNewAddressFragmentViewModel.ConciergeNewAddressFragmentViewModelListener {

    @Inject
    public ApiService mApiService;

    private Context mContext;
    private ConciergeNewAddressDataManager mDataManager;
    private ConciergeNewAddressFragmentViewModel mViewModel;
    private ConciergeNewAddressLabelAdapter mLabelAdapter;

    public static ConciergeNewAddressFragment newInstance(boolean isEditMode,
                                                          ConciergeUserAddress addressToEdit,
                                                          boolean shouldDisplayIsPrimaryOption) {
        Bundle args = new Bundle();
        args.putBoolean(ConciergeNewAddressIntent.IS_EDIT_ADDRESS_MODE, isEditMode);
        args.putParcelable(ConciergeNewAddressIntent.ADDRESS_DATA, addressToEdit);
        args.putBoolean(ConciergeNewAddressIntent.SHOULD_DISPLAY_PRIMARY_ADDRESS_FIELD, shouldDisplayIsPrimaryOption);

        ConciergeNewAddressFragment fragment = new ConciergeNewAddressFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_new_address;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mContext = requireActivity();

        boolean isEditMode = false;
        ConciergeUserAddress editAddress = null;

        if (getArguments() != null) {
            isEditMode = getArguments().getBoolean(ConciergeNewAddressIntent.IS_EDIT_ADDRESS_MODE);
            editAddress = getArguments().getParcelable(ConciergeNewAddressIntent.ADDRESS_DATA);
        }

        if (isEditMode) {
            getBinding().layoutToolbar.toolbarTitle.setText(R.string.shop_edit_address_screen_title);
        } else {
            getBinding().layoutToolbar.toolbarTitle.setText(R.string.shop_new_address_screen_title);
        }
        if (isEditMode) {
            FlurryHelper.logEvent(requireContext(), FlurryHelper.SCREEN_EDIT_DELIVERY_ADDRESS);
        } else {
            FlurryHelper.logEvent(requireContext(), FlurryHelper.SCREEN_ADD_NEW_DELIVERY_ADDRESS);
        }

        setToolbar(getBinding().layoutToolbar.toolbar, true);

        mDataManager = new ConciergeNewAddressDataManager(mContext, mApiService);
        mLabelAdapter = ConciergeNewAddressLabelAdapter.getDefaultAdapter(label -> {
            if (mViewModel != null) {
                mViewModel.onLabelSelected(label);
            }
        });
//        getBinding().labelRecyclerView.setAdapter(mLabelAdapter);
//        getBinding().labelRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        if (isEditMode && editAddress != null) {
            mViewModel = new ConciergeNewAddressFragmentViewModel(mContext,
                    true,
                    editAddress,
                    mDataManager,
                    getArguments().getBoolean(ConciergeNewAddressIntent.SHOULD_DISPLAY_PRIMARY_ADDRESS_FIELD, false),
                    this);
        } else {
            mViewModel = new ConciergeNewAddressFragmentViewModel(mContext,
                    mDataManager,
                    getArguments().getBoolean(ConciergeNewAddressIntent.SHOULD_DISPLAY_PRIMARY_ADDRESS_FIELD, false),
                    this);
        }

        setVariable(BR.viewModel, mViewModel);
        mViewModel.initData();
        bindCitySelectionList();
    }

    @Override
    public void onAddNewAddress(ConciergeUserAddress newAddress) {
        if (mContext instanceof AbsBaseActivity) {
            Intent intent = new Intent();
            intent.putExtra(ConciergeNewAddressIntent.ADDRESS_DATA, newAddress);
            ((AbsBaseActivity) mContext).setResult(AppCompatActivity.RESULT_OK, intent);
            ((AbsBaseActivity) mContext).finish();
        }
    }

    @Override
    public void onAddressUpdated(ConciergeUserAddress updatedAddress) {
        if (mContext instanceof AbsBaseActivity) {
            Intent intent = new Intent();
            intent.putExtra(ConciergeNewAddressIntent.ADDRESS_DATA, updatedAddress);
            ((AbsBaseActivity) mContext).setResult(AppCompatActivity.RESULT_OK, intent);
            ((AbsBaseActivity) mContext).finish();
        }
    }

    @Override
    public void onUserAddressDeleted(ConciergeUserAddress deletedAddress) {
        if (mContext instanceof AbsBaseActivity) {
            Intent intent = new Intent();
            intent.putExtra(ConciergeNewAddressIntent.ADDRESS_DATA, deletedAddress);
            intent.putExtra(ConciergeNewAddressIntent.IS_ADDRESS_DELETED, true);
            ((AbsBaseActivity) mContext).setResult(AppCompatActivity.RESULT_OK, intent);
            ((AbsBaseActivity) mContext).finish();
        }
    }

    @Override
    public void selectLabel(ConciergeUserAddressLabel label) {
        if (mLabelAdapter != null) {
            mLabelAdapter.updateItemSelection(label);
        }
    }

    @Override
    public CountryCodePicker getCountryCodePicker() {
        return getBinding().ccp;
    }

    @Override
    public ConciergeUserAddressLabel getUserSelectedLabel() {
        if (mLabelAdapter != null) {
            return mLabelAdapter.getSelectedLabel();
        }
        return null;
    }

    @Override
    public CitySelectionType getSelectedCity() {
        return (CitySelectionType) getBinding().citySpinner.getSelectedItem();
    }

    private void bindCitySelectionList() {
        CitySelectionItemOptionArrayAdapter adapter = new CitySelectionItemOptionArrayAdapter(getBinding().citySpinner.getContext());
        getBinding().citySpinner.setAdapter(adapter);
        CitySelectionType selectedCity = mViewModel.getPreviousSelectedCity();
        if (selectedCity != null) {
            int foundPosition = 0;
            for (int i = 0; i < adapter.getCount(); i++) {
                if (adapter.getItem(i) == selectedCity) {
                    foundPosition = i;
                    break;
                }
            }
            getBinding().citySpinner.setSelection(foundPosition);
        }
    }
}
