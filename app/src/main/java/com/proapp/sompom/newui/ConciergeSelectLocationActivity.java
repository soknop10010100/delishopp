package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.intent.newintent.ConciergeSelectLocationIntent;
import com.proapp.sompom.newui.fragment.ConciergeSelectLocationFragment;

/**
 * Created by Or Vitovongsak on 25/11/21.
 */
public class ConciergeSelectLocationActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(ConciergeSelectLocationFragment.newInstance(new ConciergeSelectLocationIntent(getIntent()).getSearchAddressResult()),
                ConciergeSelectLocationFragment.class.getName());
    }
}
