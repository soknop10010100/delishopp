package com.proapp.sompom.newui.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentConciergePromotionItemBinding;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.intent.ConciergeBrandDetailIntent;
import com.proapp.sompom.intent.ConciergeCategoryDetailIntent;
import com.proapp.sompom.intent.ConciergeSubCategoryDetailIntent;
import com.proapp.sompom.intent.ConciergeSupplierDetailIntent;
import com.proapp.sompom.model.concierge.ConciergeCarouselSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeSupplier;
import com.proapp.sompom.model.emun.OpenBrandDetailType;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergePromotionItemFragmentViewModel;

import java.util.List;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 26/8/21.
 */

public class ConciergePromotionItemFragment extends AbsBindingFragment<FragmentConciergePromotionItemBinding> {

    private static final String DATA = "DATA";
    private static final String DELISHOP_HOST = "delishop.asia";
    private static final String SEARCH_PATH = "search";
    private static final String BRAND_PATH = "brand";
    private static final String EXPRESS_PATH = "express";
    private static final String EXPRESS_STORE_PATH = "store";
    private static final String ALLOW_IP = "128.199.227.153";
    private static final int ALLOW_PORT = 7596;

    public static ConciergePromotionItemFragment newInstance(ConciergeCarouselSectionResponse.Data promotion) {

        Bundle args = new Bundle();
        args.putParcelable(DATA, promotion);

        ConciergePromotionItemFragment fragment = new ConciergePromotionItemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_promotion_item;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            ConciergeCarouselSectionResponse.Data promotion = getArguments().getParcelable(DATA);
            if (promotion != null) {
                ConciergePromotionItemFragmentViewModel viewModel = new ConciergePromotionItemFragmentViewModel(promotion,
                        data -> {
                            if (!TextUtils.isEmpty(data.getRedirectionUrl())) {
                                handleRedirection(data.getRedirectionUrl());
                            }
                        });
                setVariable(BR.viewModel, viewModel);
            }
        }
    }

    private void handleRedirection(String redirectionUrl) {
        Uri parseUrl = Uri.parse(redirectionUrl);
        if (parseUrl != null) {
            String host = parseUrl.getHost();
            if (host != null) {
                List<String> pathSegments = parseUrl.getPathSegments();
                boolean isMatchDefinedIP = host.contains(ALLOW_IP) && parseUrl.getPort() == ALLOW_PORT;
                if (host.contains(DELISHOP_HOST) || isMatchDefinedIP) {
                    if (pathSegments.size() == 3) {
                        String firstPath = pathSegments.get(0);
                        String secondPath = pathSegments.get(1);
                        String thirdPath = pathSegments.get(2);
                        if (TextUtils.equals(firstPath, EXPRESS_PATH)) {
                            if (TextUtils.equals(secondPath, EXPRESS_STORE_PATH)) {
                                ConciergeSupplier supplier = new ConciergeSupplier();
                                supplier.setId(thirdPath);
                                requireActivity().startActivity(new ConciergeSupplierDetailIntent(requireContext(), supplier));
                            } else if (TextUtils.equals(secondPath, SEARCH_PATH)) {
                                requireActivity().startActivity(new ConciergeBrandDetailIntent(requireContext(),
                                        thirdPath,
                                        OpenBrandDetailType.OPEN_AS_SEARCH_GENERAL_PRODUCT_MODE));
                            } else {
                                WallStreetHelper.openCustomTab(requireContext(), redirectionUrl);
                            }
                        } else {
                            WallStreetHelper.openCustomTab(requireContext(), redirectionUrl);
                        }
                    } else if (pathSegments.size() == 2) {
                        String firstPath = pathSegments.get(0);
                        String lastPath = pathSegments.get(1);
                        if (TextUtils.equals(firstPath, SEARCH_PATH)) {
                            requireActivity().startActivity(new ConciergeBrandDetailIntent(requireContext(),
                                    lastPath,
                                    OpenBrandDetailType.OPEN_AS_SEARCH_GENERAL_PRODUCT_MODE));
                        } else if (TextUtils.equals(firstPath, BRAND_PATH)) {
                            requireActivity().startActivity(new ConciergeBrandDetailIntent(requireContext(), lastPath));
                        } else {
                            requireActivity().startActivity(new ConciergeSubCategoryDetailIntent(requireContext(), lastPath));
                        }
                    } else if (pathSegments.size() == 1) {
                        String lastPath = pathSegments.get(0);
                        if (TextUtils.equals(lastPath, EXPRESS_PATH)) {
                            //Invalid express redirection url
                            WallStreetHelper.openCustomTab(requireContext(), redirectionUrl);
                        } else {
                            requireActivity().startActivity(new ConciergeCategoryDetailIntent(requireContext(), lastPath));
                        }
                    } else {
                        WallStreetHelper.openCustomTab(requireContext(), redirectionUrl);
                    }
                } else {
                    WallStreetHelper.openCustomTab(requireContext(), redirectionUrl);
                }
            }
        }
    }
}
