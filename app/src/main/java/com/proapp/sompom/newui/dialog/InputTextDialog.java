package com.proapp.sompom.newui.dialog;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.DialogInputTextBinding;
import com.proapp.sompom.utils.KeyboardUtil;

public class InputTextDialog extends MessageDialog {
    private EditText mEditText;
    private int mInputType;
    DialogInputTextBinding mBinding;

    public String getCurrentInputText() {
        if (mEditText == null || TextUtils.isEmpty(mEditText.getText())) {
            return null;
        }
        return mEditText.getText().toString().trim();
    }

    public InputTextDialog(int inputType) {
        mInputType = inputType;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setDismissWhenClickOnButtonLeft(false);

        final LayoutInflater inflater = LayoutInflater.from(getContext());
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.dialog_input_text,
                null,
                true);
        mEditText = mBinding.inputText;
        mEditText.setInputType(mInputType);
        getBinding().textviewTitle.setTypeface(null, Typeface.BOLD);
        getBinding().view.addView(mBinding.getRoot());
        mBinding.clearInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditText.setText("");
            }
        });

        view.postDelayed(() -> {
            mBinding.inputText.requestFocus();
            KeyboardUtil.showKeyboard(requireActivity(), mBinding.inputText);
        }, 100);
        super.onViewCreated(view, savedInstanceState);
    }
}
