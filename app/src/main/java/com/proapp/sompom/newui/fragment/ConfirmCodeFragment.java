package com.proapp.sompom.newui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.MySMSBroadcastReceiver;
import com.proapp.sompom.databinding.FragmentConfirmCodeBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.intent.ConfirmCodeIntent;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConfirmCodeDataManager;
import com.proapp.sompom.viewmodel.ConfirmCodeViewModel;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class ConfirmCodeFragment extends AbsBindingFragment<FragmentConfirmCodeBinding> {

    private ConfirmCodeViewModel mViewModel;
    @PublicQualifier
    @Inject
    public ApiService mApiService;
    private BroadcastReceiver mOTPSMSBroadcastReceiver;

    public static ConfirmCodeFragment newInstance(ExchangeAuthData data) {

        Bundle args = new Bundle();
        args.putParcelable(ConfirmCodeIntent.DATA, data);

        ConfirmCodeFragment fragment = new ConfirmCodeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ConfirmCodeFragment newInstance(ExchangeAuthData data, String firebaseToken) {

        Bundle args = new Bundle();
        args.putParcelable(ConfirmCodeIntent.DATA, data);
        args.putString(ConfirmCodeIntent.FIREBASE_TOKEN, firebaseToken);

        ConfirmCodeFragment fragment = new ConfirmCodeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_VERIFY_PHONE_CODE;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_confirm_code;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        registerOTPSMSReceiver();

        mViewModel = new ConfirmCodeViewModel((AppCompatActivity) requireActivity(),
                new ConfirmCodeDataManager(requireContext(),
                        mApiService,
                        getArguments().getParcelable(ConfirmCodeIntent.DATA),
                        getArguments().getString(ConfirmCodeIntent.FIREBASE_TOKEN)));
        setVariable(BR.viewModel, mViewModel);
    }

    private void registerOTPSMSReceiver() {
        mOTPSMSBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String otp = intent.getStringExtra(MySMSBroadcastReceiver.RECEIVED_OTP);
                Timber.i("onReceive OTP SMS: " + otp);
                if (!TextUtils.isEmpty(otp)) {
                    mViewModel.onAutoConfirmCode(otp);
                }
            }
        };
        requireContext().registerReceiver(mOTPSMSBroadcastReceiver, new IntentFilter(MySMSBroadcastReceiver.RECEIVED_OTP_ACTION));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requireContext().unregisterReceiver(mOTPSMSBroadcastReceiver);
    }
}
