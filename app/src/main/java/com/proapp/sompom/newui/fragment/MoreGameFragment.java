package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;

import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentMoreGameBinding;
import com.proapp.sompom.model.result.MoreGame;
import com.proapp.sompom.viewmodel.MoreGameFragmentViewModel;

/**
 * Created by he.rotha on 4/29/16.
 */
public class MoreGameFragment extends AbsBindingFragment<FragmentMoreGameBinding> {

    public static MoreGameFragment getInstance(MoreGame moreGame) {
        MoreGameFragment fragment = new MoreGameFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(SharedPrefUtils.PATH, moreGame);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_more_game;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MoreGame moreGame = null;
        if (getArguments() != null) {
            moreGame = getArguments().getParcelable(SharedPrefUtils.PATH);
        }
        MoreGameFragmentViewModel viewModel = new MoreGameFragmentViewModel(getActivity(), moreGame);
        setVariable(BR.viewModel, viewModel);
    }

}
