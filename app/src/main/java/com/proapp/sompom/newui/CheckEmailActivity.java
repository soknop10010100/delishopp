package com.proapp.sompom.newui;

import androidx.fragment.app.Fragment;

import com.proapp.sompom.newui.fragment.CheckEmailFragment;

public class CheckEmailActivity extends SupportResetPasswordDeepLinkActivity {

    @Override
    protected Fragment getFragment() {
        return CheckEmailFragment.newInstance();
    }

    @Override
    protected String getFragmentTag() {
        return CheckEmailFragment.TAG;
    }
}
