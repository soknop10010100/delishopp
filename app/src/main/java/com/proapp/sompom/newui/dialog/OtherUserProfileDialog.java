package com.proapp.sompom.newui.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.DialogOtherUserProfileBinding;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.OtherUserProfileDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.OtherProfileViewModel;

import javax.inject.Inject;

public class OtherUserProfileDialog extends AbsBindingFragmentDialog<DialogOtherUserProfileBinding>
        implements OtherProfileViewModel.OnCallback {

    private static final String USER_ID = "USER_ID";

    private OtherProfileViewModel mViewModel;
    @Inject
    public ApiService mApiService;

    public static OtherUserProfileDialog newInstance(String userId) {

        Bundle args = new Bundle();
        args.putString(USER_ID, userId);

        OtherUserProfileDialog fragment = new OtherUserProfileDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_other_user_profile;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return super.onCreateView(inflater, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        if (getArguments() != null) {
            String userId = getArguments().getString(USER_ID);
            mViewModel = new OtherProfileViewModel(new OtherUserProfileDataManager(requireContext(),
                    mApiService),
                    userId,
                    this);
            setVariable(BR.viewModel, mViewModel);
        }

    }

    @Override
    public void onCallClicked(User user) {
        if (getContext() != null) {
            requireActivity().startActivity(new CallingIntent(requireContext(),
                    AbsCallService.CallType.VOICE, user,
                    null));
        }
        dismiss();

//        if (getContext() != null) {
//            requireActivity().startActivity(new VideoCallIntent(requireContext(),
//                    AbsCallService.CallType.VOICE,
//                    user,
//                    null));
//        }
//        dismiss();
    }

    @Override
    public void onVideoCallClicked(User user) {
        if (getContext() != null) {
            requireActivity().startActivity(new CallingIntent(requireContext(),
                    AbsCallService.CallType.VIDEO, user,
                    null));
        }
        dismiss();
    }

    @Override
    public void onMessageClicked(User user) {
        requireActivity().startActivity(new ChatIntent(requireContext(), user));
        dismiss();
    }

    @Override
    public void onCloseClicked() {
        dismiss();
    }

    @Override
    public void onRootClicked() {
        if (isCancelable()) {
            dismiss();
        }
    }
}
