package com.proapp.sompom.newui.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.DialogConfirmationBinding;
import com.proapp.sompom.viewmodel.newviewmodel.ConfirmationDialogViewModel;

/**
 * Created by he.rotha on 4/28/16.
 */
public class ConfirmationDialog extends AbsBindingFragmentDialog<DialogConfirmationBinding> {
    public static final String TAG = ConfirmationDialog.class.getName();
    private ConfirmationDialogViewModel.DialogModel mModel;

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_confirmation;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container) {
        requireDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        return super.onCreateView(inflater, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBinding().setViewModel(new ConfirmationDialogViewModel(this, mModel));
    }

    public static class Builder {
        ConfirmationDialog mConfirmationDialog = new ConfirmationDialog();

        public ConfirmationDialog build(ConfirmationDialogViewModel.DialogModel model) {
            mConfirmationDialog.mModel = model;
            return mConfirmationDialog;
        }
    }

}
