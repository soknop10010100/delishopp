package com.proapp.sompom.newui.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.intent.ConciergeSupplierDetailIntent;
import com.proapp.sompom.newui.AbsDefaultActivity;

/**
 * Created by Chhom Veasna on 5/4/22.
 */
public class ConciergeSupplierDetailActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConciergeSupplierDetailIntent intent = new ConciergeSupplierDetailIntent(getIntent());
        setFragment(ConciergeSupplierDetailFragment.newInstance(intent.getSupplier()),
                ConciergeSupplierDetailFragment.class.getName());
    }
}
