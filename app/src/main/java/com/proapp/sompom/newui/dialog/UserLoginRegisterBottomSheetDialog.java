package com.proapp.sompom.newui.dialog;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.DialogUserLoginRegisterBottomSheetBinding;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.viewmodel.newviewmodel.UserLoginRegisterBottomSheetDialogViewModel;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 26/1/22.
 */

public class UserLoginRegisterBottomSheetDialog
        extends AbsBindingBottomSheetDialog<DialogUserLoginRegisterBottomSheetBinding>
        implements UserLoginRegisterBottomSheetDialogViewModel.DialogUserLoginRegisterBottomSheetViewModelCallback {

    public static final String REQUEST_LOGIN_OR_SIGNUP_KEY = "REQUEST_LOGIN_OR_SIGNUP_KEY";

    public static final String LOGIN_RESULT_KEY = "LOGIN_RESULT_KEY";

    private UserLoginRegisterBottomSheetDialogViewModel mViewModel;

    public static UserLoginRegisterBottomSheetDialog newInstance() {
        return new UserLoginRegisterBottomSheetDialog();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_user_login_register_bottom_sheet;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().getAttributes().windowAnimations = R.style.AnimatedPopupStyle;
        }

        mViewModel = new UserLoginRegisterBottomSheetDialogViewModel(getContext(), this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onLoginSignUpSuccess() {
        Timber.i("onLoginSignUpSuccess");
        ApplicationHelper.broadcastRefreshScreenEvent(requireContext(), true);
        Bundle bundle = new Bundle();
        bundle.putInt(LOGIN_RESULT_KEY, AppCompatActivity.RESULT_OK);
        // Use fragment result API to parse data between dialog fragment and parent
        getParentFragmentManager().setFragmentResult(REQUEST_LOGIN_OR_SIGNUP_KEY, bundle);
        onCloseButtonClick();
    }

    @Override
    public void onCloseButtonClick() {
        this.dismiss();
    }
}
