package com.proapp.sompom.newui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ConciergeFeaturedStoreGridItemAdapter;
import com.proapp.sompom.databinding.FragmentConciergeFeaturedStoreGridBinding;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.intent.ConciergeShopDetailIntent;
import com.proapp.sompom.model.concierge.ConciergeFeatureStoreSectionResponse;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeFeaturedStoreGridFragmentViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Or Vitovongsak on 27/8/21.
 */

public class ConciergeFeaturedStoreGridFragment
        extends AbsBindingFragment<FragmentConciergeFeaturedStoreGridBinding> {

    private ConciergeFeaturedStoreGridFragmentViewModel mViewModel;
    private Context mContext;

    private static final String DATA = "DATA";

    public static ConciergeFeaturedStoreGridFragment newInstance(List<ConciergeFeatureStoreSectionResponse.Data> stores) {

        Bundle args = new Bundle();

        ArrayList<ConciergeFeatureStoreSectionResponse.Data> promotions = new ArrayList<>(stores);
        args.putParcelableArrayList(DATA, promotions);

        ConciergeFeaturedStoreGridFragment fragment = new ConciergeFeaturedStoreGridFragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_featured_store_grid;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getContext();

        List<ConciergeFeatureStoreSectionResponse.Data> data = new ArrayList<>();
        if (getArguments() != null) {
            data = getArguments().getParcelableArrayList(DATA);
        }

        mViewModel = new ConciergeFeaturedStoreGridFragmentViewModel(data);

        initFeaturedGrid();
    }

    public void initFeaturedGrid() {
        ConciergeFeaturedStoreGridItemAdapter adapter =
                new ConciergeFeaturedStoreGridItemAdapter(mViewModel.getData(),
                        featured -> {
                            startActivity(new ConciergeShopDetailIntent(requireActivity(),
                                    featured.getShopId()));
                        });

        int spanCount = ConciergeHelper.CONCIERGE_FEATURED_STORE_ITEM / 2;
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, spanCount);
        getBinding().featuredRecyclerView.setLayoutManager(layoutManager);
        getBinding().featuredRecyclerView.setAdapter(adapter);
    }
}
