package com.proapp.sompom.newui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.ConciergeDeliveryDateAdapter;
import com.proapp.sompom.adapter.ConciergeDeliveryTimeAdapter;
import com.proapp.sompom.databinding.FragmentConciergeDeliveryBinding;
import com.proapp.sompom.decorataor.GridLeftAndEightEdgeSpaceItemDecorator;
import com.proapp.sompom.intent.ConciergeDeliveryIntent;
import com.proapp.sompom.model.ShopDeliveryDate;
import com.proapp.sompom.model.ShopDeliveryTime;
import com.proapp.sompom.model.ShopDeliveryTimeSelection;
import com.proapp.sompom.model.emun.CitySelectionType;
import com.proapp.sompom.model.result.ShopDeliveryData;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.DeliveryDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeDeliveryFragmentViewModel;
import com.proapp.sompom.widget.LoaderMoreLayoutManager;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 9/9/21.
 */

public class ConciergeDeliveryFragment extends AbsBindingFragment<FragmentConciergeDeliveryBinding>
        implements ConciergeDeliveryFragmentViewModel.ConciergeDeliveryFragmentViewModelListener,
        ConciergeDeliveryDateAdapter.ConciergeDeliveryDateAdapterCallback,
        ConciergeDeliveryTimeAdapter.ConciergeDeliveryTimeAdapterCallback {

    private Context mContext;
    @Inject
    public ApiService mApiService;
    private ShopDeliveryDate mShopDeliveryDate;
    private ShopDeliveryTime mShopDeliveryTime;
    private ShopDeliveryTimeSelection mShopDeliveryTimeSelection;
    private ConciergeDeliveryFragmentViewModel mViewModel;

    private ShopDeliveryTimeSelection mPreviousSelection;

    private ConciergeDeliveryTimeAdapter mTimeAdapter;
    private ConciergeDeliveryDateAdapter mDateAdapter;
    private LoaderMoreLayoutManager mTimeLayoutManager;
    private LinearLayoutManager mDateLayoutManager;
    private boolean mIsASAP = false;
    private boolean mIsViewOnlyMode;

    public static ConciergeDeliveryFragment newInstance(ShopDeliveryTimeSelection selection, boolean isViewOnlyMode) {
        Bundle args = new Bundle();
        args.putParcelable(ConciergeDeliveryIntent.DATA, selection);
        args.putBoolean(ConciergeDeliveryIntent.IS_VIEW_ONLY, isViewOnlyMode);

        ConciergeDeliveryFragment fragment = new ConciergeDeliveryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_delivery;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mContext = requireContext();

        if (getArguments() != null) {
            mPreviousSelection = getArguments().getParcelable(ConciergeDeliveryIntent.DATA);
            Timber.i("mPreviousSelection: " + mPreviousSelection);
        }

        mIsViewOnlyMode = getArguments().getBoolean(ConciergeDeliveryIntent.IS_VIEW_ONLY);
        mViewModel = new ConciergeDeliveryFragmentViewModel(mContext, mIsViewOnlyMode, new DeliveryDataManager(mContext, mApiService), this);
        if (mPreviousSelection != null) {
            mViewModel.setPreviousSelection(mPreviousSelection);
            // Determine if previous selection is an ASAP type
            mIsASAP = mPreviousSelection.getSelectedDate().toLocalDate().equals(LocalDate.now()) && mPreviousSelection.getStartTime() == null && mPreviousSelection.getEndTime() == null;
        }
        setVariable(BR.viewModel, mViewModel);
        getBinding().layoutToolbar.toolbarTitle.setText(getString(R.string.shop_delivery_slot_screen_title));
        setToolbar(getBinding().layoutToolbar.toolbar, true);
        mViewModel.requestData();
    }

    @Override
    public void onLoadDataSuccess(ShopDeliveryData deliveryTimeResponse) {
        if (deliveryTimeResponse != null) {
            if (deliveryTimeResponse.getShopDeliveryDates() != null && !deliveryTimeResponse.getShopDeliveryDates().isEmpty()) {
                buildDateList(deliveryTimeResponse.getShopDeliveryDates());
            }
            if (deliveryTimeResponse.getShopDeliveryTimes() != null && !deliveryTimeResponse.getShopDeliveryTimes().isEmpty()) {
                buildTimeList(deliveryTimeResponse.getShopDeliveryTimes(), true);
            }
        }
        mViewModel.hideLoading();
    }

    private void buildDateList(List<ShopDeliveryDate> dates) {
        mDateAdapter = new ConciergeDeliveryDateAdapter(mContext, dates, this);
        if (mPreviousSelection != null) {
            if (mPreviousSelection.getSelectedDate() != null) {
                mDateAdapter.selectPreviousDate(mPreviousSelection.getSelectedDate());
            }
        }
        mDateLayoutManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        getBinding().recyclerViewDate.setLayoutManager(mDateLayoutManager);
        getBinding().recyclerViewDate.addItemDecoration(new GridLeftAndEightEdgeSpaceItemDecorator(mContext.getResources().getDimensionPixelSize(R.dimen.space_xxlarge)));
        getBinding().recyclerViewDate.setAdapter(mDateAdapter);

        if (mPreviousSelection != null && mPreviousSelection.getSelectedDate() != null) {
            scrollToSelectedDate();
        }
    }

    private void buildTimeList(List<ShopDeliveryTime> timeList, boolean isFirstLoad) {
        if (mTimeAdapter == null) {
            mTimeAdapter = new ConciergeDeliveryTimeAdapter(timeList, mIsViewOnlyMode, this);
            mTimeLayoutManager = new LoaderMoreLayoutManager(mContext, getBinding().recyclerViewTime);

            if (mPreviousSelection != null && !TextUtils.isEmpty(mPreviousSelection.getTimeSlotId())) {
                // Preserve previous selection when user select a different date
                mTimeAdapter.selectPreviousTime(mPreviousSelection.getTimeSlotId());
            } else if (mShopDeliveryTimeSelection != null) {
                if (mShopDeliveryTimeSelection.isProvince()) {
                    mTimeAdapter.selectFirstTimeForProvinceSlot();
                }
            }

            getBinding().recyclerViewTime.setLayoutManager(mTimeLayoutManager);
            getBinding().recyclerViewTime.setAdapter(mTimeAdapter);

            if (mPreviousSelection != null) {
                scrollToSelectedTime();
            }
        } else {
            mTimeAdapter.resetData(timeList, true);
            if (mShopDeliveryTimeSelection != null) {
                if (mShopDeliveryTimeSelection.isProvince()) {
                    mTimeAdapter.selectFirstTimeForProvinceSlot();
                }
            }
        }
    }

    private void scrollToSelectedTime() {
        if (mTimeLayoutManager != null && mTimeAdapter != null) {
            int recyclerViewHeight = getBinding().recyclerViewTime.getHeight() / 2;
            mTimeLayoutManager.scrollToPositionWithOffset(mTimeAdapter.getCurrentSelectedIndex(), recyclerViewHeight);
        }
    }

    private void scrollToSelectedDate() {
        if (mDateLayoutManager != null && mDateAdapter != null) {
            getBinding().recyclerViewDate.postDelayed(() -> getBinding().recyclerViewDate.smoothScrollToPosition(mDateAdapter.getCurrentSelectedIndex()), 100);
        }
    }

    @Override
    public void onDateItemSelected(ShopDeliveryDate deliveryDate, boolean shouldResetTimeSelection) {
        Timber.i("onDateItemSelected: " + deliveryDate.toString() + ", shouldResetTimeSelection: " + shouldResetTimeSelection);
        if (mShopDeliveryDate == null) {
            mShopDeliveryDate = new ShopDeliveryDate(deliveryDate.getDate(), deliveryDate.isSelected());
        } else {
            mShopDeliveryDate.setDate(deliveryDate.getDate());
            mShopDeliveryDate.setSelected(deliveryDate.isSelected());
        }
        mShopDeliveryTime = null;
        mViewModel.setConfirmationButtonVisibility(false);
        mViewModel.setErrorEmptyTimeSlotDisplay(deliveryDate.getShopDeliveryTimes() == null || deliveryDate.getShopDeliveryTimes().isEmpty());
        buildTimeList(deliveryDate.getShopDeliveryTimes(), false);
        buildDeliveryTimeSelection();

//        if (mShopDeliveryDate == null) {
//            mShopDeliveryDate = new ShopDeliveryDate(deliveryDate.getDate(), deliveryDate.isSelected());
//        } else {
//            mShopDeliveryDate.setDate(deliveryDate.getDate());
//            mShopDeliveryDate.setSelected(deliveryDate.isSelected());
//        }
//
//        // Here, the user's unselected the date, so we have to use user's current time if they exit
//        // while also preserve time selection if they select another day
//        if (!deliveryDate.isSelected()) {
//            //Will set current date the selected one.
//            mShopDeliveryDate.setDate(LocalDateTime.now());
//            mIsASAP = true;
//            if (mTimeAdapter != null) {
//                mTimeAdapter.hidePassedHour();
//            }
//        } else {
//            if (deliveryDate.getDate().toLocalDate().isEqual(LocalDate.now())) {
//                if (mTimeAdapter != null) {
//                    mTimeAdapter.hidePassedHour();
//                }
//                if (mShopDeliveryTime != null) {
//                    if (mShopDeliveryTime.getStartTime().isBefore(LocalTime.now())) {
//                        if (mTimeAdapter != null) {
//                            mTimeAdapter.selectFirstAvailableHour();
//                        }
//                    }
//                }
//            } else {
//                if (mTimeAdapter != null) {
//                    mTimeAdapter.showFullHour();
//                    if (mTimeAdapter.isTimeSelected()) {
//                        mShopDeliveryTime = mTimeAdapter.getDatas().get(mTimeAdapter.getCurrentSelectedIndex());
//                    }
//                }
//            }
//        }
//        scrollToSelectedTime();
//        buildDeliveryTimeSelection();
    }

    /**
     * Handle when date adapter notify that there's no province slots
     */
    @Override
    public void onEmptyProvinceSlots() {
        mViewModel.onEmptyProvinceSlots();
    }

    @Override
    public void onTimeSelected(ShopDeliveryTime time) {
        Timber.i("onTimeSelected: " + time.toString());
        mShopDeliveryTime = time;
        if (!time.isSelected()) {
            mShopDeliveryTime = null;
        }
        buildDeliveryTimeSelection();
    }

    public void buildDeliveryTimeSelection() {
        if (mShopDeliveryDate != null) {
            if (mShopDeliveryTimeSelection == null) {
                mShopDeliveryTimeSelection = new ShopDeliveryTimeSelection();
            }
            if (mShopDeliveryTime != null) {
                mShopDeliveryTimeSelection.setTimeSlotId(mShopDeliveryTime.getId());
            }

            LocalDateTime selectedDate = mShopDeliveryDate.getDate();
            if (mShopDeliveryTime != null) {
                LocalDateTime startTime = selectedDate.withHour(mShopDeliveryTime.getStartTime().getHour()).withMinute(mShopDeliveryTime.getStartTime().getMinute());
                LocalDateTime endTime = selectedDate.withHour(mShopDeliveryTime.getEndTime().getHour()).withMinute(mShopDeliveryTime.getEndTime().getMinute());

                mShopDeliveryTimeSelection.setStartTime(startTime);
                mShopDeliveryTimeSelection.setEndTime(endTime);
                if (mViewModel != null) {
                    mViewModel.setConfirmationButtonVisibility(true);
                }
            } else {
                mShopDeliveryTimeSelection.setStartTime(null);
                mShopDeliveryTimeSelection.setEndTime(null);
            }

            mShopDeliveryTimeSelection.setSelectedDate(selectedDate);
            mShopDeliveryTimeSelection.setIsASAP(mIsASAP);

            if (requireActivity() instanceof DeliveryFragmentCallback) {
                ((DeliveryFragmentCallback) requireActivity()).onDeliveryOptionSelected(mShopDeliveryTimeSelection);
            }
        }
    }

    @Override
    public void onDeliveryTabOptionSelected(CitySelectionType type, boolean shouldResetSelection) {
        if (mShopDeliveryTimeSelection == null) {
            mShopDeliveryTimeSelection = new ShopDeliveryTimeSelection();
        }
        mShopDeliveryTimeSelection.setIsProvince(type == CitySelectionType.PROVINCE);
        Timber.i("On slot tab selected, " + type + ", shouldreset: " + shouldResetSelection);
        if (mDateAdapter != null) {
            mDateAdapter.shouldOnlyShowProvinceSlot(mShopDeliveryTimeSelection.isProvince(), shouldResetSelection);
        }
        if (mContext instanceof DeliveryFragmentCallback) {
            ((DeliveryFragmentCallback) mContext).onDeliveryOptionSelected(mShopDeliveryTimeSelection);
        }
    }

    @Override
    public void onConfirmationButtonSelected() {
        if (mContext instanceof DeliveryFragmentCallback) {
            ((DeliveryFragmentCallback) mContext).onConfirmationButtonClicked();
        }
    }

    public interface DeliveryFragmentCallback {
        void onDeliveryOptionSelected(ShopDeliveryTimeSelection shopDeliveryTimeSelection);

        void onConfirmationButtonClicked();
    }
}
