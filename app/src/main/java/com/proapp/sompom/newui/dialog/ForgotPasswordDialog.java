package com.proapp.sompom.newui.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.PartialForgotLayoutBinding;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ForgotPasswordDataManager;
import com.proapp.sompom.utils.SnackBarUtil;
import com.proapp.sompom.viewmodel.newviewmodel.ForgotPasswordViewModel;

import javax.inject.Inject;

public class ForgotPasswordDialog extends MessageDialog implements ForgotPasswordViewModel.ForgotPasswordViewModelListener {

    private ForgotPasswordViewModel mViewModel;
    @Inject
    public ApiService mApiService;
    private View mRoot;
    private ForgotPasswordDialogListener mListener;

    public static ForgotPasswordDialog newInstance() {

        Bundle args = new Bundle();

        ForgotPasswordDialog fragment = new ForgotPasswordDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mRoot = view;
        getControllerComponent().inject(this);
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        PartialForgotLayoutBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.partial_forgot_layout,
                null,
                true);

        setDismissWhenClickOnButtonLeft(false);
        mViewModel = new ForgotPasswordViewModel(requireContext(),
                new ForgotPasswordDataManager(requireContext(), mApiService),
                this);

        binding.setVariable(BR.viewModel, mViewModel);

        setTitle(getString(R.string.forget_password_title));
        setMessage(getString(R.string.forget_password_description));
        setLeftText(getString(R.string.forget_password_confirm_button), dialog -> {
            if (mViewModel.isAllFieldValid()) {
                showLoading();
                mViewModel.requestForgotPassword();
            }
        });
        setRightText(getString(R.string.popup_cancel_button), v -> mViewModel.clearDisposable());
        getBinding().view.addView(binding.getRoot());
        super.onViewCreated(view, savedInstanceState);
    }

    public void setListener(ForgotPasswordDialogListener listener) {
        mListener = listener;
    }

    @Override
    public void onFailed(String error) {
        hideLoading();
        SnackBarUtil.showSnackBar(mRoot, error, true);
    }

    @Override
    public void onSuccess() {
        hideLoading();
        dismiss();
        new AlertDialog.Builder(requireContext())
                .setTitle(getString(R.string.change_password_reset_title))
                .setMessage(getString(R.string.forget_password_send_code_description))
                .setPositiveButton(getString(R.string.popup_ok_button), null)
                .show();
    }

    public interface ForgotPasswordDialogListener {
        void onSuccess();
    }
}
