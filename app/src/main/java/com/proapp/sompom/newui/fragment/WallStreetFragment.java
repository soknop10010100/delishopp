package com.proapp.sompom.newui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.HomeTimelineAdapter;
import com.proapp.sompom.broadcast.upload.LifeStreamUploadService;
import com.proapp.sompom.databinding.FragmentWallStreetBinding;
import com.proapp.sompom.decorataor.WallStreetLinearItemDecorator;
import com.proapp.sompom.helper.OfflinePostDataHelper;
import com.proapp.sompom.helper.OpenCommentDialogHelper;
import com.proapp.sompom.injection.game.MoreGameAPIQualifier;
import com.proapp.sompom.injection.productserialize.ProductSerializeQualifier;
import com.proapp.sompom.intent.CreateTimelineIntent;
import com.proapp.sompom.intent.newintent.MyCameraIntent;
import com.proapp.sompom.licence.db.LicenseSynchronizationDb;
import com.proapp.sompom.licence.model.SynchroniseData;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnTabSelectListener;
import com.proapp.sompom.listener.OnTimelineActiveUserItemClick;
import com.proapp.sompom.listener.OnTimelineItemButtonClickListener;
import com.proapp.sompom.model.ActiveUser;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.MoreGameItem;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.CreateWallStreetScreenType;
import com.proapp.sompom.model.emun.FollowItemType;
import com.proapp.sompom.model.emun.PostContextMenuItemType;
import com.proapp.sompom.model.emun.TimelinePostItem;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.TimelineDetailActivity;
import com.proapp.sompom.newui.dialog.BigPlayerFragment;
import com.proapp.sompom.newui.dialog.ListFollowDialog;
import com.proapp.sompom.newui.dialog.PostContextMenuDialog;
import com.proapp.sompom.newui.dialog.ShareLinkMenuDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.WallStreetDataManager;
import com.proapp.sompom.utils.BetterSmoothScrollUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.TimelinePostItemUtil;
import com.proapp.sompom.viewmodel.newviewmodel.WallStreetViewModel;
import com.proapp.sompom.widget.LoadMoreLinearLayoutManager;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.videomanager.widget.MediaRecyclerView;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/22/18.
 */

public class WallStreetFragment extends AbsBindingFragment<FragmentWallStreetBinding>
        implements WallStreetViewModel.OnCallback,
        OnTimelineActiveUserItemClick,
        OnTimelineItemButtonClickListener, OnTabSelectListener {

    @ProductSerializeQualifier
    @Inject
    public ApiService mSerialApiService;
    @MoreGameAPIQualifier
    @Inject
    public ApiService mMoreGameApiService;
    @Inject
    public ApiService mApiService;
    private HomeTimelineAdapter mTimelineAdapter;
    private WallStreetViewModel mViewModel;
    private LoadMoreLinearLayoutManager mLoadMoreLayoutManager;
    private WallStreetDataManager mDataManager;
    private OpenCommentDialogHelper mCommentDialog;
    private boolean mIsCurrentTabDisplay = true;
    private LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener mLoadMoreLinearLayoutManagerListener;
    private BroadcastReceiver mPostSuccessReceiver;
    private BroadcastReceiver mRefreshWallPostContentStateReceiver;

    public static WallStreetFragment newInstance() {
        Bundle args = new Bundle();
        WallStreetFragment fragment = new WallStreetFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_wall_street;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        initOnPostSuccessReceiver();
        registerRefreshPostContentState();
        initLoadMoreLinearLayoutManagerListener();
        mLoadMoreLayoutManager = new LoadMoreLinearLayoutManager(requireActivity(), getBinding().recyclerView);
        getBinding().recyclerView.setLayoutManager(mLoadMoreLayoutManager);
        ((SimpleItemAnimator) getBinding().recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        SynchroniseData mSynchronizeData = LicenseSynchronizationDb.readSynchroniseData(getContext());
        boolean checkLikeComment = false;
        if (mSynchronizeData != null && mSynchronizeData.getNewsFeed() != null) {
            checkLikeComment = mSynchronizeData.getNewsFeed().isEnableLikeComment();
        }
        mTimelineAdapter = new HomeTimelineAdapter((AbsBaseActivity) getActivity(),
                mApiService,
                this,
                this,
                checkLikeComment);
        mTimelineAdapter.setCanLoadMore(false);
        if (getBinding().recyclerView.getItemDecorationCount() <= 0) {
            getBinding().recyclerView.addItemDecoration(new WallStreetLinearItemDecorator(getResources()
                    .getDimensionPixelSize(R.dimen.space_medium),
                    getResources().getDimensionPixelSize(R.dimen.space_medium),
                    true,
                    false,
                    false));
        }
        mTimelineAdapter.bindRecyclerViewListener(getBinding().recyclerView);
        getBinding().recyclerView.setAdapter(mTimelineAdapter);

        mDataManager = new WallStreetDataManager(getActivity(),
                mApiService,
                mSerialApiService,
                mMoreGameApiService);
        mViewModel = new WallStreetViewModel(getActivity(), mDataManager, this);
        getBinding().setVariable(BR.viewModel, mViewModel);
    }

    private void initLoadMoreLinearLayoutManagerListener() {
        mLoadMoreLinearLayoutManagerListener = new LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener() {
            @Override
            public void onReachedLoadMoreBottom() {
                Timber.i("onReachedLoadMoreBottom");
                performLoadMore();
            }

            @Override
            public void onReachedLoadMoreByDefault() {
                Timber.i("onReachedLoadMoreByDefault");
                performLoadMore();
            }
        };
    }

    private void performLoadMore() {
        getBinding().recyclerView.stopScroll();
        mViewModel.loadMoreData(new OnCallbackListListener<LoadMoreWrapper<Adaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mTimelineAdapter.setCanLoadMore(false);
                mTimelineAdapter.notifyDataSetChanged();
                mLoadMoreLayoutManager.loadingFinished();
                mLoadMoreLayoutManager.setLoadMoreLinearLayoutManagerListener(null);
                showSnackBar(R.string.error_general_description, true);
            }

            @Override
            public void onComplete(LoadMoreWrapper<Adaptive> result, boolean canLoadMore) {
                mTimelineAdapter.setCanLoadMore(canLoadMore);
                if (!result.getData().isEmpty()) {
                    mTimelineAdapter.addLoadMoreData(result.getData());
                } else {
                    mLoadMoreLayoutManager.setLoadMoreLinearLayoutManagerListener(null);
                    getBinding().recyclerView.post(() -> {
                        //Need to refresh to hide load more view.
                        mTimelineAdapter.notifyDataSetChanged();
                    });
                }
                mLoadMoreLayoutManager.loadingFinished();
            }
        });
    }

    private void initOnPostSuccessReceiver() {
        mPostSuccessReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mViewModel.onRefresh();
            }
        };
        requireActivity().registerReceiver(mPostSuccessReceiver,
                new IntentFilter(LifeStreamUploadService.POST_SUCCESS_ACTION));
    }

    private void registerRefreshPostContentState() {
        mRefreshWallPostContentStateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                Timber.i("registerRefreshPostContentState");
                WallStreetAdaptive adaptive = intent.getParcelableExtra(TimelineDetailActivity.DATA);
                if (adaptive != null && mTimelineAdapter != null) {
                    mTimelineAdapter.updatePostContentState(adaptive);
                }
            }
        };
        requireActivity().registerReceiver(mRefreshWallPostContentStateReceiver,
                new IntentFilter(TimelineDetailActivity.REFRESH_POST_CONTENT_STATE_EVEN));
    }

    public void setEnableQueryOption(boolean isEnable) {
        if (mDataManager != null) {
            mDataManager.resetPagination();
        }
        if (mViewModel != null) {
            mViewModel.onRefresh();
        }
    }

    @Override
    public void onVideoFullScreenClicked(Media media, int position, long time) {
        getBinding().recyclerView.pause();
        BigPlayerFragment playerFragment =
                BigPlayerFragment.newInstance(position,
                        media,
                        false);
        playerFragment.show(getChildFragmentManager(), BigPlayerFragment.TAG);
    }

    @Override
    public void onGetMoreGameSuccess(MoreGameItem moreGames) {
    }

    @Override
    public void onGetActiveUserSuccess(ActiveUser activeUser) {
    }

    @Override
    public void onGetTimelineSuccess(List<Adaptive> list, boolean isRefresh, boolean isCanLoadMore) {
        Timber.i("onGetTimelineSuccess: list: " + list.size() +
                ", isCanLoadMore: " + isCanLoadMore + ", isRefresh: " + isRefresh);
        if (isAddedToActivity()) {
            requireActivity().runOnUiThread(() -> {
                mTimelineAdapter.checkToAddWelcomePost(list);
                mTimelineAdapter.setCanLoadMore(isCanLoadMore);
                if (isRefresh) {
                    mTimelineAdapter.setRefreshData(list);
                } else {
                    mTimelineAdapter.addLoadMoreData(list);
                }
                if (isCanLoadMore) {
                    mLoadMoreLayoutManager.setLoadMoreLinearLayoutManagerListener(mLoadMoreLinearLayoutManagerListener);
                }
            });
        }
    }

    @Override
    public void onLiveClick() {
        Intent cameraIntent = MyCameraIntent.newLiveInstance(getContext());
        if (getActivity() != null) {
            ((AbsBaseActivity) getActivity()).startActivityForResult(cameraIntent, new ResultCallback() {
                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    //TODO: handle live click
                }
            });
        }
    }

    @Override
    public void onCommentButtonClick(Adaptive adaptive, int position) {
        onCommentClick(adaptive, true);
    }

    @Override
    public void onShareButtonClick(Adaptive adaptive, int position) {
        ShareLinkMenuDialog dialog = ShareLinkMenuDialog.newInstance((WallStreetAdaptive) adaptive);
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onUserViewMediaClick(Adaptive adaptive) {
        ListFollowDialog dialog = ListFollowDialog.newInstance(adaptive, FollowItemType.USER_ACTION);
        dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
    }

    @Override
    public void onLikeViewerClick(Adaptive adaptive) {
        ListFollowDialog dialog = ListFollowDialog.newInstance(adaptive, FollowItemType.LIKE_VIEWER);
        dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
    }

    @Override
    public void onFollowButtonClick(String id, boolean isFollowing) {
        mViewModel.checkTopPostFollow(id, isFollowing);
    }

    @Override
    public void onShowSnackBar(boolean isError, String message) {
        showSnackBar(message, isError);
    }

    @Override
    public void onShowPostContextMenuClick(Adaptive adaptive, User user, int position) {
        if (getActivity() == null || user == null) {
            return;
        }

        TimelinePostItem timelinePostItem = TimelinePostItemUtil.getPostItem(adaptive);
        LifeStream lifeStream = TimelinePostItemUtil.getLifeStream(adaptive);
        Product product = TimelinePostItemUtil.getProduct(adaptive);
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        dialog.addItem(PostContextMenuItemType.getPostMenuItems(requireContext(),
                SharedPrefUtils.checkIsMe(requireActivity(), user.getId()),
                timelinePostItem));
        dialog.setOnItemClickListener(result -> {
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.EDIT_POST_CLICK) {
                if (getActivity() != null) {
                    ((LifeStream) adaptive).checkUpdateBackupOfMappingFields();
                    CreateTimelineIntent intent = new CreateTimelineIntent(getActivity(),
                            (WallStreetAdaptive) adaptive,
                            CreateWallStreetScreenType.EDIT_POST);
                    ((AbsBaseActivity) getActivity()).startActivityForResult(intent, new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            LifeStream updateLifeStream = data.getParcelableExtra(CreateTimelineIntent.DATA);
                            if (updateLifeStream != null) {
                                mTimelineAdapter.beginPostUpdate(updateLifeStream);
                            }
                        }
                    });
                }
            } else if (itemType == PostContextMenuItemType.UNFOLLOW_CLICK) {
                mViewModel.checkTopPostFollow(user.getId(), false);
                if (adaptive instanceof WallStreetAdaptive && ((WallStreetAdaptive) adaptive).getUser() != null) {
                    ((WallStreetAdaptive) adaptive).getUser().setFollow(false);
                    mTimelineAdapter.update(adaptive);
                }
            } else if (itemType == PostContextMenuItemType.EDIT_PRIVACY_CLICK) {
                showEditPrivacyDialog(result1 -> {
                    //TODO: implement api edit privacy
                });
            } else if (itemType == PostContextMenuItemType.TURNOFF_NOTIFICATION_CLICK) {
                mViewModel.unFollowShop(adaptive.getId());
            } else if (itemType == PostContextMenuItemType.DELETE_CLICK) {
                showMessageDialog(R.string.post_menu_delete_post_title,
                        R.string.post_popup_confirm_delete_description,
                        R.string.seller_store_button_delete,
                        R.string.setting_change_language_dialog_cancel_button,
                        dialog12 -> {
                            mViewModel.showLoading(true);
                            mViewModel.deletePost(lifeStream, new OnCallbackListener<Response<Object>>() {
                                @Override
                                public void onFail(ErrorThrowable ex) {
                                    mViewModel.hideLoading();
                                    showSnackBar(ex.getMessage(), true);
                                }

                                @Override
                                public void onComplete(Response<Object> result) {
                                    OfflinePostDataHelper.removeLocalPost(requireContext(), adaptive.getId());
                                    mTimelineAdapter.removeItem(adaptive);
                                    mViewModel.hideLoading();
                                }
                            });
                        });
            }
            dialog.dismiss();

        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onCommentClick(Adaptive adaptive, boolean autoDisplayKeyboard) {
        if (mCommentDialog == null) {
            mCommentDialog = new OpenCommentDialogHelper(getActivity());
        }
        mCommentDialog.show(requireContext(), mApiService, adaptive.getId(), adaptive, mTimelineAdapter, autoDisplayKeyboard);
    }

    @Override
    public MediaRecyclerView getRecyclerView() {
        return getBinding().recyclerView;
    }

    @Override
    public void onConversationUserClick(User user) {
        //TODO: handle story user click
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mIsCurrentTabDisplay && getBinding() != null) {
            getBinding().recyclerView.onResume();
            if (getActivity() instanceof AbsBaseActivity) {
                AbsBaseActivity base = (AbsBaseActivity) getActivity();
                if (base.getFloatingVideo() != null && base.getFloatingVideo().isFloating()) {
                    getBinding().recyclerView.setAutoPlay(false);
                    getBinding().recyclerView.setAutoResume(false);
                    base.setOnFloatingVideoRemoveListener(() -> {
                        getBinding().recyclerView.setAutoPlay(true);
                        getBinding().recyclerView.setAutoResume(true);
                        getBinding().recyclerView.resume();
                    });
                }
            }
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (getBinding() != null) {
            getBinding().recyclerView.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTimelineAdapter != null) {
            mTimelineAdapter.unbindPostServiceConnection();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        requireActivity().unregisterReceiver(mPostSuccessReceiver);
        requireActivity().unregisterReceiver(mRefreshWallPostContentStateReceiver);
        if (getActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getActivity()).removeStateChangeListener(mViewModel.getNetworkStateListener());
        }
    }

    public void shouldScrollTop(boolean isPullToRefresh) {
        if (findFirstVisibleItemPosition() != 0) {
            if (isPullToRefresh) {
                mViewModel.onRefresh();
            }
            BetterSmoothScrollUtil.startScroll(getBinding().recyclerView,
                    0,
                    findFirstVisibleItemPosition());
        }
    }

    public int findFirstVisibleItemPosition() {
        return mLoadMoreLayoutManager.findFirstVisibleItemPosition();
    }

    @Override
    public void onTabSelected(boolean isSelected) {
        mIsCurrentTabDisplay = isSelected;
    }

    @Override
    public void onRefreshFailed() {
        showSnackBar(R.string.error_general_description, true);
    }
}
