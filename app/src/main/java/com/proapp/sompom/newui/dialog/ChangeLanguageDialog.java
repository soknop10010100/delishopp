package com.proapp.sompom.newui.dialog;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.View;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.FlurryHelper;

/**
 * Created by He Rotha on 6/19/18.
 */
public class ChangeLanguageDialog extends MessageDialog {
    private OnLanguageChooseListener mOnLanguageChooseListener;
    private String[] mLanguages;
    private int mPosition;

    public void setLanguages(String[] languages) {
        mLanguages = languages;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    public void setOnLanguageChooseListener(OnLanguageChooseListener onLanguageChooseListener) {
        mOnLanguageChooseListener = onLanguageChooseListener;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setTitle(getString(R.string.profile_user_section_localization_title));
        setMessage(getString(R.string.setting_change_language_dialog_description));
        setSingleChoiceItems(mLanguages, mPosition, (dialog, which) -> mPosition = which);
        setLeftText(getString(R.string.setting_change_language_dialog_ok_button), dialog -> mOnLanguageChooseListener.onSelect(mPosition));
        setRightText(getString(R.string.setting_change_language_dialog_cancel_button), null);
        super.onViewCreated(view, savedInstanceState);
        FlurryHelper.logEvent(requireContext(), FlurryHelper.SCREEN_CHANGE_LANGUAGE_POPUP);
    }

    public interface OnLanguageChooseListener {
        void onSelect(int position);
    }
}
