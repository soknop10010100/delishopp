package com.proapp.sompom.newui;

import androidx.fragment.app.Fragment;

import com.proapp.sompom.newui.fragment.ForgotPasswordFragment;

public class ForgotPasswordActivity extends SupportResetPasswordDeepLinkActivity {

    @Override
    protected Fragment getFragment() {
        return ForgotPasswordFragment.newInstance();
    }

    @Override
    protected String getFragmentTag() {
        return ForgotPasswordFragment.TAG;
    }
}
