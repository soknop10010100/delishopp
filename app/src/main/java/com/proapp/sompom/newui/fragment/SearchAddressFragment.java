package com.proapp.sompom.newui.fragment;


import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.SearchAddressAdapter;
import com.proapp.sompom.databinding.FragmentSearchAddressBinding;
import com.proapp.sompom.injection.google.GoogleQualifier;
import com.proapp.sompom.intent.SelectAddressIntentResult;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.ApiGoogle;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.SelectAddressDataManager;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.newviewmodel.SearchAddressFragmentViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by nuonveyo on 6/11/18.
 */

public class SearchAddressFragment extends AbsBindingFragment<FragmentSearchAddressBinding> {
    public static final String TAG = SearchAddressFragment.class.getName();
    @Inject
    public ApiService mApiService;
    @Inject
    @GoogleQualifier
    public ApiGoogle mApiGoogle;
    private SearchAddressAdapter mAdapter;
    private SearchAddressResult mSearchAddressResult;
    private SearchAddressFragmentViewModel mViewModel;

    public static SearchAddressFragment newInstance(SearchAddressResult searchAddressResult) {

        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.DATA, searchAddressResult);
        SearchAddressFragment fragment = new SearchAddressFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search_address;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        if (getArguments() != null) {
            mSearchAddressResult = getArguments().getParcelable(SharedPrefUtils.DATA);
        }

        getBinding().recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        getBinding().recyclerview.setOnTouchListener((v, event) -> {
            KeyboardUtil.hideKeyboard(getActivity());
            v.performClick();
            return false;
        });
        mAdapter = new SearchAddressAdapter(new ArrayList<>(),
                searchAddressResult -> {
                    if (searchAddressResult.getLocations() == null) {
                        mViewModel.searchDetailAddress(searchAddressResult, this::setSearchAddressResult);
                    } else {
                        setSearchAddressResult(searchAddressResult);
                    }
                });
        getBinding().recyclerview.setAdapter(mAdapter);
        //TODO: Need to check the priory of method
        SelectAddressDataManager.Strategy strategy = SelectAddressDataManager.Strategy.Mapbox;
        SelectAddressDataManager selectAddressDataManager = new SelectAddressDataManager(getActivity(),
                mApiService,
                mApiGoogle,
                strategy);
        mViewModel = new SearchAddressFragmentViewModel(selectAddressDataManager,
                mSearchAddressResult,
                list -> mAdapter.setData(list));
        setVariable(BR.viewModel, mViewModel);
    }

    private void setSearchAddressResult(SearchAddressResult addressResult) {
        if (getActivity() instanceof AbsBaseActivity) {
            if (addressResult.getLocations() != null && TextUtils.isEmpty(addressResult.getLocations().getCountry())) {
                Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                List<Address> addresses;
                try {
                    addresses = geocoder.getFromLocation(addressResult.getLocations().getLatitude(), addressResult.getLocations().getLongitude(), 1);

                    if (addresses != null && !addresses.isEmpty()) {
                        String countryCode = addresses.get(0).getCountryCode();
                        addressResult.getLocations().setCountry(countryCode);
                    }

                } catch (Exception ignored) {
                    Timber.e(ignored.toString());
                }


            }

            SelectAddressIntentResult intentResult = new SelectAddressIntentResult(addressResult);
            getActivity().setResult(Activity.RESULT_OK, intentResult);
            getActivity().finish();
        }
    }
}
