package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentProfileSignUpBinding;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.intent.ProfileSignUpIntent;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ProfileSignUpDataManager;
import com.proapp.sompom.viewmodel.ProfileSignUpViewModel;

import javax.inject.Inject;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class ProfileSignUpFragment extends AbsBindingFragment<FragmentProfileSignUpBinding> {

    @Inject
    @PublicQualifier
    public ApiService mPublicApiService;
    @Inject
    public ApiService mPrivateApiService;
    private ProfileSignUpViewModel mViewModel;

    public static ProfileSignUpFragment newInstance(ExchangeAuthData data) {

        Bundle args = new Bundle();
        args.putParcelable(ProfileSignUpIntent.SIGN_UP_DATA, data);

        ProfileSignUpFragment fragment = new ProfileSignUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_profile_sign_up;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        ExchangeAuthData data = getArguments().getParcelable(ProfileSignUpIntent.SIGN_UP_DATA);
        mViewModel = new ProfileSignUpViewModel(requireContext(),
                new ProfileSignUpDataManager(requireContext(),
                        mPublicApiService,
                        mPrivateApiService,
                        data));
        mViewModel.setCallback(() -> {
            if (isAddedToActivity()) {
                startActivity(ApplicationHelper.getFreshHomeIntent(getContext()));
            }
        });
        setVariable(BR.viewModel, mViewModel);
    }
}
