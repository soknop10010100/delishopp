package com.proapp.sompom.newui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.proapp.sompom.BR;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.AbsChatBinder;
import com.proapp.sompom.chat.call.CallingService;
import com.proapp.sompom.chat.service.ChatSocket;
import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.databinding.FragmentEditProfile2Binding;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.helper.ScrimColorAppbarListener;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.intent.AdvancedNotificationSettingIntent;
import com.proapp.sompom.licence.db.LicenseSynchronizationDb;
import com.proapp.sompom.licence.model.SynchroniseData;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnEditProfileListener;
import com.proapp.sompom.listener.OnSpannableClickListener;
import com.proapp.sompom.model.emun.StoreStatus;
import com.proapp.sompom.model.result.AppFeature;
import com.proapp.sompom.model.result.NotificationSettingModel;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.StoreDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.EditProfileFragmentViewModel;

import javax.inject.Inject;

import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/13/18.
 */

public class EditProfileFragment extends AbsBindingFragment<FragmentEditProfile2Binding>
        implements OnEditProfileListener, OnSpannableClickListener, AbsBaseActivity.OnServiceListener {

    @Inject
    public ApiService mApiService;
    @Inject
    @PublicQualifier
    public ApiService mPublicApiService;

    private EditProfileFragmentViewModel mViewModel;
    private MessageDialog mDisableNotificationDialog;
    private MessageDialog mActivateDialog;
    private CompoundButton.OnCheckedChangeListener mCheckListener;
    private StoreStatus mStatus;
    private boolean mIsChangeValue = false;
    private boolean mCheckPasswordVisibility;
    private boolean mCheckThemeVisibility;
    private boolean mIsEnableShop = false;
    private SocketService.SocketBinder mSocketBinder;
    private BroadcastReceiver mUpdateUserDisplayDataReceiver;

    public static EditProfileFragment newInstance() {
        return new EditProfileFragment();
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_PROFILE_SETTING;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_edit_profile2;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Timber.i("onViewCreated");
        getControllerComponent().inject(this);
        if (requireActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) requireActivity()).addOnServiceListener(this);
        }
        initUpdateUserDisplayDataReceiver();
        checkSynchronizeUi();

        StoreDataManager storeDataManager = new StoreDataManager(getActivity(), mApiService, mPublicApiService);
        getBinding().layoutToolbar.title.setTextColor(Color.WHITE);
        mViewModel = new EditProfileFragmentViewModel(storeDataManager,
                getChildFragmentManager(),
                EditProfileFragment.this,
                EditProfileFragment.this,
                mCheckPasswordVisibility,
                mCheckThemeVisibility,
                mIsEnableShop);
        setVariable(BR.viewModel, mViewModel);
        mCheckListener = (buttonView, isChecked) -> {
            if (buttonView.getId() == R.id.switch_disable_notification) {
                if (isChecked) {
                    getBinding().switchDisableNotification.setChecked(false);
                    mDisableNotificationDialog.show(getChildFragmentManager(), "");
                } else {
                    mViewModel.pushNotificationSetting(false);
                }
            } else if (buttonView.getId() == R.id.switch_activate) {
                getBinding().switchActivate.setChecked(!isChecked);
                mActivateDialog.show(getChildFragmentManager(), "");
            }
        };

        if (getActivity() != null && ThemeManager.getAppTheme(getActivity()) == AppTheme.White ||
                ThemeManager.getAppTheme(getActivity()) == AppTheme.ExpressWhite) {
            getBinding().appBar.addOnOffsetChangedListener(new ScrimColorAppbarListener(getBinding().collapse) {
                @Override
                public void onColorChange(int color) {
                    getBinding().layoutToolbar.title.setTextColor(color);
                }
            });
        }
    }

    private void initUpdateUserDisplayDataReceiver() {
        mUpdateUserDisplayDataReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive: update user action: ");
                mViewModel.reloadUserProfilePhoto();
            }
        };
        requireContext().registerReceiver(mUpdateUserDisplayDataReceiver, new IntentFilter(ChatSocket.UPDATE_USER_EVENT));
    }

    @Override
    public void onLogoutClick() {
        showMessageDialog(R.string.profile_user_popup_log_out_title,
                R.string.profile_user_popup_log_out_description,
                R.string.popup_yes_button,
                R.string.popup_no_button,
                dialog -> {
                    try {
                        if (((MainApplication) requireActivity().getApplication()).isCallInProgressNow()) {
                            ((MainApplication) requireActivity().getApplication()).endProgressCall();
                        }
                        mViewModel.showLoading(true);
                        //Stop chat and call services
                        if (requireActivity() instanceof AbsBaseActivity) {
                            ((AbsBaseActivity) requireActivity()).bindCall(CallingService.CallServiceBinder::logout,
                                    false);

                            if (((AbsBaseActivity) requireActivity()).getChatBinder() != null) {
                                ((AbsBaseActivity) requireActivity()).getChatBinder().stopService();
                            }
                        }

                        //Clear user player id
                        UserHelper.clearUserPlayerId(requireContext(), mApiService, new UserHelper.UserHelperListener() {
                            @Override
                            protected void onClearUserPlayerIdFinish(String error) {
                                Timber.i("onClearUserPlayerIdFinish: " + error);
                                mViewModel.checkShopSetting(() -> ConciergeHelper.clearLocalBasket(requireContext().getApplicationContext(),
                                        new OnCallbackListener<Response<Object>>() {
                                            @Override
                                            public void onFail(ErrorThrowable ex) {
                                                SharedPrefUtils.clearValue(requireContext());
                                                SharedPrefUtils.setAppFeature(requireContext(), null);
                                                if (!TextUtils.isEmpty(error)) {
                                                    showSnackBar(error, true);
                                                    new Handler().postDelayed(EditProfileFragment.this::checkToNavigateAfterLogOut, 2000);
                                                } else {
                                                    checkToNavigateAfterLogOut();
                                                }
                                            }

                                            @Override
                                            public void onComplete(Response<Object> result) {
                                                SharedPrefUtils.clearValue(requireContext());
                                                SharedPrefUtils.setAppFeature(requireContext(), null);
                                                if (!TextUtils.isEmpty(error)) {
                                                    showSnackBar(error, true);
                                                    new Handler().postDelayed(EditProfileFragment.this::checkToNavigateAfterLogOut, 2000);
                                                } else {
                                                    checkToNavigateAfterLogOut();
                                                }
                                            }
                                        }));
                            }
                        });
                    } catch (Exception ex) {
                        Timber.e("Log out failed: " + ex);
                        mViewModel.checkShopSetting(() -> ConciergeHelper.clearLocalBasket(requireContext().getApplicationContext(),
                                new OnCallbackListener<Response<Object>>() {
                                    @Override
                                    public void onFail(ErrorThrowable ex1) {
                                        SharedPrefUtils.clearValue(requireContext());
                                        SharedPrefUtils.setAppFeature(requireContext(), null);
                                        checkToNavigateAfterLogOut();
                                    }

                                    @Override
                                    public void onComplete(Response<Object> result) {
                                        SharedPrefUtils.clearValue(requireContext());
                                        SharedPrefUtils.setAppFeature(requireContext(), null);
                                        checkToNavigateAfterLogOut();
                                    }
                                }));
                    }
                });
    }

    private void checkToNavigateAfterLogOut() {
        if (ApplicationHelper.isAllowToUseWithoutLogin(requireActivity())) {
            ApplicationHelper.broadcastRefreshScreenEvent(requireContext(), false);
            mViewModel.hideLoading();
        } else {
            mViewModel.hideLoading();
            requireActivity().startActivity(ApplicationHelper.getFreshLoginIntent(requireContext()));
        }
        requireActivity().finish();
    }

    @Override
    public void onComplete(NotificationSettingModel result) {
        initDisableNotificationDialog();
        initNotificationSetting(result);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requireContext().unregisterReceiver(mUpdateUserDisplayDataReceiver);
    }

    private void initDisableNotificationDialog() {
        mDisableNotificationDialog = new MessageDialog();
        mDisableNotificationDialog.setMessage(getString(R.string.setting_popup_confirm_before_disable_notification_description));
        mDisableNotificationDialog.setTitle(getString(R.string.setting_popup_confirm_before_disable_notification_title));
        mDisableNotificationDialog.setLeftText(getString(R.string.popup_yes_button),
                dialog -> {
                    getBinding().switchDisableNotification.setOnCheckedChangeListener(null);
                    getBinding().switchDisableNotification.setChecked(true);
                    getBinding().switchDisableNotification.setOnCheckedChangeListener(mCheckListener);
                    mViewModel.pushNotificationSetting(true);
                });
        mDisableNotificationDialog.setRightText(getString(R.string.popup_no_button), null);
    }

    private void initNotificationSetting(NotificationSettingModel value) {
        SharedPrefUtils.setNotificationSetting(getActivity(), value);
        if (value != null && getBinding().switchDisableNotification != null) {
            getBinding().switchDisableNotification.setChecked(value.isDisableAllNotification());
        }

        if (getBinding().switchDisableNotification != null) {
            getBinding().switchDisableNotification.setOnCheckedChangeListener(mCheckListener);
        }
    }

    public void checkSynchronizeUi() {
        SynchroniseData mSynchronizeData = LicenseSynchronizationDb.readSynchroniseData(getContext());
        if (mSynchronizeData != null
                && mSynchronizeData.getProfile() != null) {
            mCheckPasswordVisibility = mSynchronizeData.getProfile().isEnableChangePassword();
            mCheckThemeVisibility = mSynchronizeData.getProfile().isEnableChangeThemes();
            if (!mSynchronizeData.getProfile().isEnableChangePassword()) {
                getBinding().textPassword.setVisibility(View.GONE);
            } else {
                getBinding().textPassword.setVisibility(View.VISIBLE);
            }
            if (!mSynchronizeData.getProfile().isEnableChangeThemes()) {
                getBinding().textTheme.setVisibility(View.GONE);
            } else {
                getBinding().textTheme.setVisibility(View.VISIBLE);
            }
        }

        // TODO: This doesn't feel like how this should be handled.

        // Check if shop is enabled by looking though AppFeature to see if one of the menu is specific
        // to the shop feature
        AppFeature appFeature = ApplicationHelper.getAppFeature(getContext());
        if (appFeature != null && appFeature.getNavigationBarMenu() != null) {
            boolean isShopEnabled = false;
            for (AppFeature.NavigationBarMenu navigationBarMenu : appFeature.getNavigationBarMenu()) {
                if (navigationBarMenu != null &&
                        navigationBarMenu.isEnabled() &&
                        navigationBarMenu.getNavMenuScreen() != null &&
                        navigationBarMenu.getNavMenuButton() != null) {
                    if (navigationBarMenu.getNavMenuScreen() == AppFeature.NavMenuScreen.Shop ||
                            navigationBarMenu.getNavMenuScreen() == AppFeature.NavMenuScreen.ShopDetail) {
                        isShopEnabled = true;
                        break;
                    }
                }
            }
            mIsEnableShop = isShopEnabled;
        }
    }

    public boolean isChangeValue() {
        return mIsChangeValue;
    }

    @Override
    public void onClick() {
        if (getBinding().switchDisableNotification.isChecked()) {
            showSnackBar(R.string.edit_profile_advanced_notification_setting_click_warning_title, true);
        } else {
            Intent intent = new AdvancedNotificationSettingIntent(getActivity());
            startActivity(intent);
        }
    }

    @Override
    public void onServiceReady(AbsChatBinder binder) {
        if (binder instanceof SocketService.SocketBinder) {
            mSocketBinder = (SocketService.SocketBinder) binder;
        }
    }

    private void addSwitchModeChangeListener() {
        getBinding().switchActivate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            }
        });
    }

    public void updateUserWallet(double wallet) {
        mViewModel.updateUserWallet(wallet);
    }
}
