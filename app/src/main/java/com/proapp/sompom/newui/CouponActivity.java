package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.intent.CouponIntent;
import com.proapp.sompom.newui.fragment.CouponFragment;

/**
 * Created by Chhom Veasna on 7/21/22.
 */
public class CouponActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(CouponFragment.newInstance(new CouponIntent(getIntent()).getSelectedCouponId()),
                CouponFragment.class.getName());
    }
}
