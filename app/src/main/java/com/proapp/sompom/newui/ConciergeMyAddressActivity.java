package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.intent.newintent.ConciergeMyAddressIntent;
import com.proapp.sompom.newui.fragment.ConciergeMyAddressFragment;

/**
 * Created by Or Vitovongsak on 22/11/21.
 */
public class ConciergeMyAddressActivity extends AbsDefaultActivity {

    private ConciergeMyAddressActivityListener mListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConciergeMyAddressIntent intent = new ConciergeMyAddressIntent(getIntent());
        setFragment(ConciergeMyAddressFragment.newInstance(intent.isFromCheckout(),
                        intent.getAddressTypeToShow(),
                        intent.getPreviousSelectedAddressId()),
                ConciergeMyAddressFragment.class.getName());
    }

    public void setListener(ConciergeMyAddressActivityListener listener) {
        mListener = listener;
    }

    @Override
    public void onBackPressed() {
        if (mListener != null) {
            mListener.onBackPressed();
        }
    }

    public interface ConciergeMyAddressActivityListener {
        void onBackPressed();
    }
}
