package com.proapp.sompom.newui;

import android.os.Bundle;

import com.proapp.sompom.intent.ConfirmCodeIntent;
import com.proapp.sompom.newui.fragment.ConfirmCodeFragment;

import androidx.annotation.Nullable;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class ConfirmCodeActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConfirmCodeIntent intent = new ConfirmCodeIntent(getIntent());
        setFragment(ConfirmCodeFragment.newInstance(intent.getPassingData(), intent.getPassingToken()),
                ConfirmCodeFragment.class.getSimpleName());
    }
}
