package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.TelegramUserRedirectAdapter;
import com.proapp.sompom.databinding.FragmentTelegramUserRedirectBinding;
import com.proapp.sompom.intent.newintent.TelegramUserRedirectIntent;
import com.proapp.sompom.model.result.TelegramRedirectUser;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemTelegramUserRedirectViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.TelegramUserRedirectFragmentViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class TelegramUserRedirectFragment extends AbsBindingFragment<FragmentTelegramUserRedirectBinding> implements
        TelegramUserRedirectFragmentViewModel.Listener,
        ListItemTelegramUserRedirectViewModel.Listener {

    @Inject
    public ApiService mApiService;
    private TelegramUserRedirectFragmentViewModel mViewModel;
    private TelegramUserRedirectAdapter mTelegramUserRedirectAdapter;
    private String mTelegramUserId;

    public static TelegramUserRedirectFragment newInstance(String telegramUserId) {
        TelegramUserRedirectFragment fragment = new TelegramUserRedirectFragment();
        Bundle args = new Bundle();
        args.putString(TelegramUserRedirectIntent.TELEGRAM_USER_ID, telegramUserId);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_telegram_user_redirect;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        mTelegramUserId = getArguments().getString(TelegramUserRedirectIntent.TELEGRAM_USER_ID);
        mViewModel = new TelegramUserRedirectFragmentViewModel(getContext(), mApiService, mTelegramUserId, this);
        setVariable(BR.viewModel, mViewModel);
        getBinding().backPressButton.setOnClickListener((View) -> {
            requireActivity().finish();
        });
    }

    private void setAdapter(List<TelegramRedirectUser> users) {
        if (users == null) users = Collections.emptyList();

        if (mTelegramUserRedirectAdapter == null) {
            //First setup adapter list
            mTelegramUserRedirectAdapter = new TelegramUserRedirectAdapter(new ArrayList<>(users), this);
            getBinding().userRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            getBinding().userRecyclerView.setAdapter(mTelegramUserRedirectAdapter);
        } else {
            mTelegramUserRedirectAdapter.refreshData(new ArrayList<>(users));
        }
    }

    @Override
    public void onGetSuccess(List<TelegramRedirectUser> users) {
        setAdapter(users);
    }

    @Override
    public void onClick(TelegramRedirectUser appgoraUser) {
    }
}
