package com.proapp.sompom.newui;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;

import com.proapp.sompom.intent.newintent.ChoosePrivacyIntent;
import com.proapp.sompom.newui.fragment.ChoosePrivacyFragment;

public class ChoosePrivacyActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(ChoosePrivacyFragment.newInstance(new ChoosePrivacyIntent(getIntent()).getSelectionId()),
                ChoosePrivacyFragment.class.getSimpleName());
    }

    @Override
    public void finish() {
        Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(ChoosePrivacyFragment.class.getSimpleName());
        if (fragmentByTag instanceof ChoosePrivacyFragment) {
            Intent intent = new Intent();
            intent.putExtra(ChoosePrivacyIntent.DATA, ((ChoosePrivacyFragment) fragmentByTag).getSelectionPrivacy());
            setResult(AppCompatActivity.RESULT_OK, intent);
        }
        super.finish();
    }
}
