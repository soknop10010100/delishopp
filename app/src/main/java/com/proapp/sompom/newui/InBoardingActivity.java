package com.proapp.sompom.newui;

import android.os.Bundle;

import com.proapp.sompom.newui.fragment.InBoardingFragment;
import com.proapp.sompom.utils.SharedPrefUtils;

public class InBoardingActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(InBoardingFragment.newInstance(), InBoardingFragment.class.getSimpleName());
    }

    @Override
    public void finish() {
        super.finish();
        SharedPrefUtils.setBoolean(this, SharedPrefUtils.ALREADY_SHOW_IN_BOARDING, true);
    }
}
