package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentSplashBinding;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.OfflinePostDataHelper;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.intent.newintent.HomeIntent;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.SplashScreenDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.SplashViewModel;

import javax.inject.Inject;

public class SplashFragment extends AbsBindingFragment<FragmentSplashBinding> implements SplashViewModel.SplashViewModelListener {

    private static final long DELAY = 1000;

    @Inject
    public ApiService mApiService;
    @Inject
    @PublicQualifier
    public ApiService mPublicApiService;

    public static SplashFragment newInstance() {

        Bundle args = new Bundle();

        SplashFragment fragment = new SplashFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        SplashViewModel viewModel = new SplashViewModel(requireContext(),
                new SplashScreenDataManager(requireContext(), mApiService, mPublicApiService),
                this);
        setVariable(BR.viewModel, viewModel);
        if (SharedPrefUtils.isLogin(requireContext()) && !ApplicationHelper.isInVisitorMode(requireContext())) {
//            viewModel.saveCustomerLocation();
            viewModel.getMyUserProfile();
        } else {
            if (ApplicationHelper.isAllowToUseWithoutLogin(requireContext())) {
                viewModel.requestNecessaryDataForNoneLoginUser(() -> {
                    startActivity(new HomeIntent(requireContext()));
                    requireActivity().finish();
                    requireActivity().overridePendingTransition(0, R.anim.activity_fade_out);
                });
            } else {
                new Handler().postDelayed(() -> {
                    startActivity(ApplicationHelper.getLoginIntent(requireContext(), true));
                    requireActivity().finish();
                }, DELAY);
            }
        }
    }

    @Override
    public void onSynchronizeUserProfileFinished() {
        OfflinePostDataHelper.checkMaxCountOfOfflinePost(requireContext(),
                new OfflinePostDataHelper.OfflinePostDataHelperListener() {
                    @Override
                    public void onValidDataFinish() {
                        startActivity(new HomeIntent(requireContext()));
                        requireActivity().finish();
                        requireActivity().overridePendingTransition(0, R.anim.activity_fade_out);
                    }
                });
    }
}
