package com.proapp.sompom.newui;

import android.os.Bundle;

import com.proapp.sompom.intent.newintent.SearchConversationIntent;
import com.proapp.sompom.newui.fragment.SearchConversationFragment;

public class SearchConversationActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SearchConversationIntent intent = new SearchConversationIntent(getIntent());
        setFragment(SearchConversationFragment.newInstance(intent.getConversation(), intent.getGroupDetail()),
                SearchConversationFragment.TAG);
    }
}
