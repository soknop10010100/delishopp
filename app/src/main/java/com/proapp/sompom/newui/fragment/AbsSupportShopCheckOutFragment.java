package com.proapp.sompom.newui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;

import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeCartHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.newui.dialog.UserLoginRegisterBottomSheetDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.viewmodel.ConciergeSupportAddItemToCardViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeCartBottomSheetViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Veasna Chhom on 9/14/21
 */

public abstract class AbsSupportShopCheckOutFragment<T extends ViewDataBinding> extends AbsHandleConciergeBasketErrorFragment<T> {

    public static final String TAG = AbsSupportShopCheckOutFragment.class.getName();

    public static final String CART_UPDATED_EVENT = "CART_UPDATED_EVENT";
    public static final String CART_ITEM_ADDED_EVENT = "CART_ADD_ITEM_EVENT";
    public static final String CART_ITEM_REMOVED_EVENT = "CART_REMOVE_ITEM_EVENT";
    public static final String CART_CLEARED_EVENT = "CART_CLEARED_EVENT";
    public static final String CART_EVENT_DATA = "CART_EVENT_DATA";
    public static final String CART_IS_SILENT_UPDATE = "CART_IS_SILENT_UPDATE";
    public static final String REFRESH_LOCAL_BASKET_DISPLAY_EVENT = "REFRESH_LOCAL_BASKET_DISPLAY_EVENT";

    protected ConciergeCartBottomSheetViewModel mCartBottomSheetViewModel;
    private BroadcastReceiver mCartUpdateEventReceiver;
    private BroadcastReceiver mCartItemAddedEventReceiver;
    private BroadcastReceiver mCartItemRemovedEventReceiver;
    private BroadcastReceiver mCartClearedEventReceiver;
    private BroadcastReceiver mRefreshLocalBasketDisplayEventReceiver;
    private BroadcastReceiver mRefreshProductOrderCounterReceiver;
    private boolean mIsInModeExpress;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mIsInModeExpress = ApplicationHelper.isInExpressMode(requireContext());
        registerCartUpdateEventReceiver();
        registerCartItemAddedEventReceiver();
        registerCartItemRemovedEventReceiver();
        registerCartClearedEventReceiver();
        initRefreshProductOrderCounterReceiver();
        registerRefreshLocalBasketDisplayEventReceiver();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Timber.i("onDestroyView of " + getClass().getSimpleName());
        requireContext().unregisterReceiver(mCartUpdateEventReceiver);
        requireContext().unregisterReceiver(mCartItemAddedEventReceiver);
        requireContext().unregisterReceiver(mCartItemRemovedEventReceiver);
        requireContext().unregisterReceiver(mCartClearedEventReceiver);
        requireContext().unregisterReceiver(mRefreshLocalBasketDisplayEventReceiver);
        requireContext().unregisterReceiver(mRefreshProductOrderCounterReceiver);
    }

    private void registerCartUpdateEventReceiver() {
        mCartUpdateEventReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Timber.i("OnReceive cart updated event");
                notifyShouldShowCartBottomSheet();
            }
        };

        requireContext().registerReceiver(mCartUpdateEventReceiver, new IntentFilter(CART_UPDATED_EVENT));
    }

    private void registerCartItemAddedEventReceiver() {
        mCartItemAddedEventReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Timber.i("OnReceive cart item added event");
                ConciergeMenuItem item = intent.getParcelableExtra(CART_EVENT_DATA);
                onItemAddedToCart(item);
                notifyShouldShowCartBottomSheet();
            }
        };

        requireContext().registerReceiver(mCartItemAddedEventReceiver, new IntentFilter(CART_ITEM_ADDED_EVENT));
    }

    private void registerCartItemRemovedEventReceiver() {
        mCartItemRemovedEventReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Timber.i("OnReceive cart item removed event");
                ConciergeMenuItem item = intent.getParcelableExtra(CART_EVENT_DATA);
                onItemRemovedFromCart(item);
                notifyShouldShowCartBottomSheet();
            }
        };

        requireContext().registerReceiver(mCartItemRemovedEventReceiver, new IntentFilter(CART_ITEM_REMOVED_EVENT));
    }

    private void registerCartClearedEventReceiver() {
        mCartClearedEventReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Timber.i("OnReceive cleared event");
                onCartItemCleared();
                notifyShouldShowCartBottomSheet();
            }
        };

        requireContext().registerReceiver(mCartClearedEventReceiver, new IntentFilter(CART_CLEARED_EVENT));
    }

    private void initRefreshProductOrderCounterReceiver() {
        mRefreshProductOrderCounterReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive mRefreshProductOrderCounterReceiver");
                ArrayList<ConciergeMenuItem> items = intent.getParcelableArrayListExtra(ConciergeHelper.REFRESH_PRODUCT_COUNTER_ITEM);
                if (items != null && !items.isEmpty()) {
                    onReceivedRefreshProductOrderCounterEvent(items);
                }
            }
        };

        requireContext().registerReceiver(mRefreshProductOrderCounterReceiver,
                new IntentFilter(ConciergeHelper.REFRESH_PRODUCT_COUNTER_EVENT));
    }

    private void registerRefreshLocalBasketDisplayEventReceiver() {
        mRefreshLocalBasketDisplayEventReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("OnReceive registerRefreshLocalBasketDisplayEventReceiver");
                onRefreshItemCountInList();
                notifyShouldShowCartBottomSheet();
            }
        };

        requireContext().registerReceiver(mRefreshLocalBasketDisplayEventReceiver,
                new IntentFilter(REFRESH_LOCAL_BASKET_DISPLAY_EVENT));
    }

    public void onHandlePaymentInProgressError(ConciergeSupportAddItemToCardViewModel viewModel,
                                               String errorMessage,
                                               ConciergeMenuItem menuItem,
                                               boolean isUpdateItemProcess) {
        performHandleOnPaymentInProcessError(viewModel, errorMessage, menuItem);
    }

    public void onAddFirstItemToCart(ConciergeSupportAddItemToCardViewModel viewModel, ConciergeMenuItem conciergeMenuItem) {
        if (ApplicationHelper.isInVisitorMode(getContext())) {
            UserLoginRegisterBottomSheetDialog dialog = UserLoginRegisterBottomSheetDialog.newInstance();
            dialog.show(getChildFragmentManager(), dialog.getTag());
            return;
        }

        if (mIsInModeExpress) {
            if (ConciergeCartHelper.isItemFromTheSameSupplierAsItemInBasket(conciergeMenuItem.getSupplier())) {
                viewModel.requestAddItemToBasket(conciergeMenuItem, AbsSupportShopCheckOutFragment.this);
            } else {
                showAddItemFromDifferentShopWarningPopup(viewModel, new OnCallbackListener<Response<SupportConciergeCustomErrorResponse>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {

                    }

                    @Override
                    public void onComplete(Response<SupportConciergeCustomErrorResponse> result) {
                        viewModel.requestAddItemToBasket(conciergeMenuItem, AbsSupportShopCheckOutFragment.this);
                    }
                });
            }
        } else {
            viewModel.requestAddItemToBasket(conciergeMenuItem, AbsSupportShopCheckOutFragment.this);
        }
    }

    public void notifyShouldShowCartBottomSheet() {
        if (mCartBottomSheetViewModel != null) {
            mCartBottomSheetViewModel.getCartData();
        }
    }

    public ConciergeCartBottomSheetViewModel getCheckOutViewModelInstance() {
        if (mCartBottomSheetViewModel == null) {
            mCartBottomSheetViewModel = new ConciergeCartBottomSheetViewModel(getContext(),
                    new ConciergeCartBottomSheetViewModel.ConciergeBottomSheetListener() {
                        @Override
                        public void onDismiss() {
                        }

                        @Override
                        public void onCartUpdate() {

                        }

                        @Override
                        public void onCartCleared() {
                            onCartItemCleared();
                        }

                        @Override
                        public void onItemAdded(ConciergeMenuItem shop) {
                            onItemAddedToCart(shop);
                        }

                        @Override
                        public void onItemRemoved(ConciergeMenuItem shop) {
                            onItemRemovedFromCart(shop);
                        }

                        @Override
                        public void onCheckOutViewVisibilityChanged(boolean isVisible) {
                            AbsSupportShopCheckOutFragment.this.onCheckOutViewVisibilityChanged(isVisible);
                        }
                    });
        }

        return mCartBottomSheetViewModel;
    }

    protected void performHandleOnPaymentInProcessError(ConciergeSupportAddItemToCardViewModel viewModel,
                                                        String errorMessage,
                                                        ConciergeMenuItem menuItem) {
        handleOnPaymentInProcessError(errorMessage, new handleOnPaymentInProcessErrorListener() {
            @Override
            public void onEnableBasketSuccess() {
                viewModel.requestAddItemToBasket(menuItem, AbsSupportShopCheckOutFragment.this);
            }

            @Override
            public void onEnableBasketFailed(ErrorThrowable ex) {
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void onRequestBasketFailed(ErrorThrowable ex) {
                showSnackBar(ex.getMessage(), true);
            }

            @Override
            public void makePaymentWithABAFailed(String errorMessage) {
                showSnackBar(errorMessage, true);
            }

            @Override
            public void makePaymentWithABASuccess() {
                Timber.i("makePaymentWithABASuccess");
            }
        });
    }

    public void reloadBottomSheetData() {
        mCartBottomSheetViewModel.getCartData();
    }

    protected void onCheckOutViewVisibilityChanged(boolean isVisible) {
        //Will be overwritten by sub class
    }

    public void onItemAddedToCart(ConciergeMenuItem item) {
        //Will be overwritten by sub class
    }

    protected void onItemRemovedFromCart(ConciergeMenuItem item) {
        //Will be overwritten by sub class
    }

    protected void onCartItemCleared() {
        //Will be overwritten by sub class
    }

    protected void onRefreshItemCountInList() {
        //Will be overwritten by sub class
    }

    protected void onReceivedRefreshProductOrderCounterEvent(List<ConciergeMenuItem> itemList) {
        //Will be overwritten by sub class
    }
}
