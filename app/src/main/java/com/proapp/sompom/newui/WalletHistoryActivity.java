package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.newui.fragment.WalletHistoryFragment;

/**
 * Created by Chhom Veasna on 8/23/22.
 */
public class WalletHistoryActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(WalletHistoryFragment.newInstance(), WalletHistoryFragment.class.getName());
    }
}
