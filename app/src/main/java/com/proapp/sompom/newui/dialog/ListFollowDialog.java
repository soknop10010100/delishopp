package com.proapp.sompom.newui.dialog;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.UserListAdapter;
import com.proapp.sompom.databinding.DialogListFollowBinding;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.emun.FollowItemType;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.StoreDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.newviewmodel.ListFollowDialogViewModel;
import com.proapp.sompom.widget.LoaderMoreLayoutManager;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by nuonveyo on 7/10/18.
 */

public class ListFollowDialog extends AbsBindingFragmentDialog<DialogListFollowBinding>
        implements ListFollowDialogViewModel.ListFollowDialogViewModelListener {

    @Inject
    public ApiService mApiService;
    private ListFollowDialogViewModel mListFollowDialogViewModel;
    private UserListAdapter<User> mViewerAdapter;
    private OnCallbackListener mOnCallbackListener;
    private FollowItemType mFollowItemType;
    private boolean mIsPostContainOnlyItem;
    private String mPostId;

    public static ListFollowDialog newInstance(User user, FollowItemType type) {
        Bundle args = new Bundle();
        args.putInt(SharedPrefUtils.ID, type.ordinal());
        args.putParcelable(SharedPrefUtils.DATA, user);
        ListFollowDialog fragment = new ListFollowDialog();
        fragment.setPostContainOnlyItem(false);
        fragment.setPostId(null);
        fragment.setArguments(args);
        return fragment;
    }

    public static ListFollowDialog newInstance(Adaptive adaptive, FollowItemType type) {
        Bundle args = new Bundle();
        args.putInt(SharedPrefUtils.ID, type.ordinal());
        args.putParcelable(SharedPrefUtils.PRODUCT, (Parcelable) adaptive);
        ListFollowDialog fragment = new ListFollowDialog();
        fragment.setPostContainOnlyItem(false);
        fragment.setPostId(null);
        fragment.setArguments(args);
        return fragment;
    }

    public void setPostContainOnlyItem(boolean postContainOnlyItem) {
        mIsPostContainOnlyItem = postContainOnlyItem;
    }

    public void setPostId(String postId) {
        mPostId = postId;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_list_follow;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        if (getArguments() != null) {
            User user = getArguments().getParcelable(SharedPrefUtils.DATA);
            Adaptive adaptive = getArguments().getParcelable(SharedPrefUtils.PRODUCT);
            mFollowItemType = FollowItemType.values()[getArguments().getInt(SharedPrefUtils.ID)];

            StoreDataManager dataManager = new StoreDataManager(getActivity(), mApiService);
            mListFollowDialogViewModel = new ListFollowDialogViewModel(dataManager,
                    adaptive,
                    user,
                    mIsPostContainOnlyItem,
                    mPostId,
                    mFollowItemType,
                    this);
            setVariable(BR.viewModel, mListFollowDialogViewModel);
        }
        getBinding().buttonClose.setOnClickListener(v -> dismiss());
        mListFollowDialogViewModel.getListFollow(false, false);
    }

    @Override
    public void onFail(ErrorThrowable ex) {
        //Do nothing...
    }

    @Override
    public void onLoadSuccess(List<User> userList,
                              boolean isRefresh,
                              boolean isLoadMore,
                              boolean isCanLoadMore) {
        Timber.i("onLoadSuccess: " + userList.size());
        if (mViewerAdapter == null) {
            mViewerAdapter = new UserListAdapter<>(userList, requireContext());
            mViewerAdapter.setListener(new UserListAdapter.UserListAdapterListener() {
                @Override
                public void onAttemptedCallWhenNoNetworkConnectedOrDeviceIsInAnotherCallProcess(boolean isInAnotherCall) {
                    showSnackBar(R.string.call_toast_no_internet, true);
                }

                @Override
                public void onMakeCallError(String error) {
                    showSnackBar(error, true);
                }
            });
            mViewerAdapter.setCanLoadMore(isCanLoadMore);
            getBinding().recyclerview.setLayoutManager(getLayoutManager(isCanLoadMore));
            getBinding().recyclerview.setAdapter(mViewerAdapter);
        } else if (isRefresh) {
            mViewerAdapter.refreshData(userList);
        }
    }

    private LoaderMoreLayoutManager getLayoutManager(boolean isDetectLoadMore) {
        LoaderMoreLayoutManager loaderMoreLayoutManager = new LoaderMoreLayoutManager(getActivity());
        if (isDetectLoadMore) {
            loaderMoreLayoutManager.setOnLoadMoreListener(() ->
                    mListFollowDialogViewModel.loadMore(new ListFollowDialogViewModel.ListFollowDialogViewModelListener() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                            mViewerAdapter.setCanLoadMore(false);
                            loaderMoreLayoutManager.setOnLoadMoreListener(null);
                            loaderMoreLayoutManager.loadingFinished();
                        }

                        @Override
                        public void onBackButtonClicked() {

                        }

                        @Override
                        public void onLoadSuccess(List<User> userList,
                                                  boolean isRefresh,
                                                  boolean isLoadMore,
                                                  boolean isCanLoadMore) {
                            mViewerAdapter.setCanLoadMore(isCanLoadMore);
                            mViewerAdapter.addLoadMoreData(userList);
                            loaderMoreLayoutManager.loadingFinished();
                            if (!isCanLoadMore) {
                                loaderMoreLayoutManager.setOnLoadMoreListener(null);
                            }
                        }
                    }));
        }

        return loaderMoreLayoutManager;
    }


    public void setOnCallbackListener(OnCallbackListener onCallbackListener) {
        mOnCallbackListener = onCallbackListener;
    }

    @Override
    public void onBackButtonClicked() {
        dismiss();
    }

    public interface OnCallbackListener {
        void onUpdateFollowing(boolean isFollowing);
    }
}
