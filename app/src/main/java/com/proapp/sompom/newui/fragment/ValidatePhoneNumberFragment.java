package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentValidatePhoneNumberBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.intent.ValidatePhoneNumberIntent;
import com.proapp.sompom.model.emun.AuthType;
import com.proapp.sompom.viewmodel.ValidatePhoneNumberViewModel;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class ValidatePhoneNumberFragment extends AbsBindingFragment<FragmentValidatePhoneNumberBinding> implements ValidatePhoneNumberViewModel.ValidatePhoneNumberViewModelListener {

    private ValidatePhoneNumberViewModel mViewModel;

    public static ValidatePhoneNumberFragment newInstance(AuthType authType, boolean isIgnoreFirebaseTokenValidation) {

        Bundle args = new Bundle();
        args.putString(ValidatePhoneNumberIntent.AUTH_TYPE, authType.getValue());
        args.putBoolean(ValidatePhoneNumberIntent.IS_IGNORE_FIREBASE_TOKEN_VALIDATION, isIgnoreFirebaseTokenValidation);

        ValidatePhoneNumberFragment fragment = new ValidatePhoneNumberFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_VERIFY_PHONE_CODE;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_validate_phone_number;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel = new ValidatePhoneNumberViewModel((AppCompatActivity) requireActivity(),
                AuthType.fromValue(getArguments().getString(ValidatePhoneNumberIntent.AUTH_TYPE)),
                getArguments().getBoolean(ValidatePhoneNumberIntent.IS_IGNORE_FIREBASE_TOKEN_VALIDATION, false),
                this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public CountryCodePicker getCountryCodePicker() {
        return getBinding().ccp;
    }
}
