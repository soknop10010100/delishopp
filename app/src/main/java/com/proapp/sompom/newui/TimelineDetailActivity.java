package com.proapp.sompom.newui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.intent.newintent.HomeIntent;
import com.proapp.sompom.intent.newintent.TimelineDetailIntent;
import com.proapp.sompom.intent.newintent.TimelineDetailIntentResult;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.TimelineDetailRedirectionType;
import com.proapp.sompom.model.notification.RedirectionNotificationData;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.newui.fragment.TimelineDetailFragment;
import com.sompom.pushy.service.SendBroadCastHelper;

import timber.log.Timber;

public class TimelineDetailActivity extends AbsDefaultActivity {

    public static final String REFRESH_POST_CONTENT_STATE_EVEN = "REFRESH_POST_CONTENT_STATE_EVEN";
    public static final String DATA = "DATA";

    private TimelineDetailRedirectionType mRedirectionType;
    private String mPostId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WallStreetAdaptive lifeStream = null;
        String id;
        int position = 0;
        Comment commentTemp = null;
        mRedirectionType = TimelineDetailRedirectionType.UNKNOWN;
        Timber.i("mRedirectionType: " + mRedirectionType);
        RedirectionNotificationData redirectionNotificationData = null;

        //From deep linking redirection
        if (getIntent().getData() != null) {
            id = getIntent().getData().getLastPathSegment();
        } else {
            //Normal Intent redirection
            TimelineDetailIntent intent = new TimelineDetailIntent(getIntent());
            lifeStream = intent.getLifeStream();
            position = intent.getPosition();
            id = intent.getWallStreetId();
            mRedirectionType = intent.getRedirectionType();
            redirectionNotificationData = intent.getExchangeData();
        }
        if (redirectionNotificationData != null) {
            mPostId = redirectionNotificationData.getPostId();
            setFragment(TimelineDetailFragment.newInstanceForNotificationRedirection(redirectionNotificationData),
                    TimelineDetailFragment.TAG);
        } else {
            mPostId = lifeStream.getId();
            setFragment(TimelineDetailFragment.newInstance(lifeStream, mRedirectionType, position, id),
                    TimelineDetailFragment.TAG);
        }
    }

    @Override
    public void onBackPressed() {
        TimelineDetailFragment fragment = (TimelineDetailFragment) getFragment(TimelineDetailFragment.TAG);
        if (fragment != null) {
            if (!fragment.isCloseSearchGifView()) {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void finish() {
        checkToBroadCastRefreshPostContentStateEvenIfNecessary();
        if (!MainApplication.isIsHomeScreenOpened()) {
            super.finish();
            ((MainApplication) getApplication()).startFreshHomeScreen(HomeIntent.Redirection.None);
        } else {
            setResult();
            super.finish();
        }
    }

    private void setResult() {
        TimelineDetailFragment fragment = (TimelineDetailFragment) getFragment(TimelineDetailFragment.TAG);
        if (fragment != null) {
            fragment.getBinding().recyclerView.pause();
            TimelineDetailIntentResult intentResult = new TimelineDetailIntentResult(fragment.getWallStreetAdaptive(),
                    fragment.isRemoveItem(),
                    fragment.isGrantPermissionFloatingVideoFirstTime());
            setResult(RESULT_OK, intentResult);
        }
    }

    private void checkToBroadCastRefreshPostContentStateEvenIfNecessary() {
        if (mRedirectionType == TimelineDetailRedirectionType.COMMENT ||
                mRedirectionType == TimelineDetailRedirectionType.LIKE) {
            TimelineDetailFragment fragment = (TimelineDetailFragment) getFragment(TimelineDetailFragment.TAG);
            if (fragment != null) {
                Intent intent = new Intent(REFRESH_POST_CONTENT_STATE_EVEN);
                intent.putExtra(DATA, fragment.getWallStreetAdaptive());
                SendBroadCastHelper.verifyAndSendBroadCast(this, intent);
                Timber.i("send REFRESH_POST_CONTENT_STATE_EVEN");
            }
        }
    }

    @Override
    public void onFloatingVideoServiceConnected() {
        //Will remove the previous floating video of the same post only
        if (getFloatingVideo() != null
                && getFloatingVideo().isFloating() &&
                TextUtils.equals(mPostId, getFloatingVideo().getPostId())) {
            getFloatingVideo().removeVideo();
        }
    }
}
