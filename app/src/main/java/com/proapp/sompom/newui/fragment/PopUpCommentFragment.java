package com.proapp.sompom.newui.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.ObservableBoolean;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.CommentAdapter;
import com.proapp.sompom.databinding.LayoutPopUpCommentBinding;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnCommentDialogClickListener;
import com.proapp.sompom.listener.OnCommentItemClickListener;
import com.proapp.sompom.listener.OnLoadPreviousCommentClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.LoadCommentDirection;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.emun.PostContextMenuItemType;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.newui.dialog.PostContextMenuDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.PopUpCommentDataManager;
import com.proapp.sompom.utils.CopyTextUtil;
import com.proapp.sompom.utils.NetworkStateUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.newviewmodel.PopUpCommentLayoutViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Created by he.rotha on 4/6/16.
 */
public class PopUpCommentFragment extends AbsBindingFragment<LayoutPopUpCommentBinding>
        implements EditText.OnEditorActionListener,
        OnCommentItemClickListener {

    private static String AUTO_SHOW_KEYBOARD = "AUTO_SHOW_KEYBOARD";

    @Inject
    public ApiService mApiService;
    private Activity mContext;
    private CommentAdapter mAdapter;
    private PopUpCommentLayoutViewModel mViewModel;
    private OnCommentDialogClickListener mCommentDialogClickListener;
    private boolean mIsCanLoadMore;
    private ContentType mContentType;
    private String mPostId;

    public static PopUpCommentFragment newInstance(String itemID, boolean isShowKeyboard) {
        Bundle args = new Bundle();
        args.putString(SharedPrefUtils.ID, itemID);
        args.putBoolean(AUTO_SHOW_KEYBOARD, isShowKeyboard);
        PopUpCommentFragment fragment = new PopUpCommentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setCommentDialogClickListener(OnCommentDialogClickListener commentDialogClickListener) {
        mCommentDialogClickListener = commentDialogClickListener;
    }

    public void setPostId(String postId) {
        mPostId = postId;
    }

    public void setContentType(ContentType contentType) {
        mContentType = contentType;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        bindGifView();

        String itemId = "";
        if (getArguments() != null) {
            itemId = getArguments().getString(SharedPrefUtils.ID);
        }

        mContext = getActivity();
        getBinding().textviewClose.setOnClickListener(v -> {
            if (mCommentDialogClickListener != null) {
                mCommentDialogClickListener.onDismissDialog(true);
            }
        });

        PopUpCommentDataManager dataManager = new PopUpCommentDataManager(mContext, mApiService);
        mViewModel = new PopUpCommentLayoutViewModel(dataManager,
                mPostId,
                itemId,
                mContentType,
                new PopUpCommentLayoutViewModel.OnCallback() {
                    @Override
                    public void onLoadCommentSuccess(List<Comment> listComment, boolean isCanLoadMore) {
                        initAdapter(listComment, isCanLoadMore);
                    }

                    @Override
                    public void onLoadCommentFail() {
                        initAdapter(new ArrayList<>(), false);
                    }

                    @Override
                    public void onPullAndRefresh(ObservableBoolean refresh) {
                        if (mAdapter != null && mAdapter.canInvokeLoadPreviousComment()) {
                            mAdapter.invokeLoadPreviousComment();
                        } else {
                            refresh.set(false);
                        }
                    }

                    @Override
                    public void onSendText(Comment comment) {
                        if (NetworkStateUtil.isNetworkAvailable(requireContext())) {
                            //Will add the instance comment in list only if there is internet connection.
                            mViewModel.showOrHideEmptyCommentLabel(false);
                            if (mAdapter != null) {
                                mAdapter.addLatestComment(comment);
                                getBinding().recyclerview.scrollToPosition(mAdapter.getItemCount() - 1);
                            }
                        }
                    }

                    @Override
                    public void onPostCommentFail() {
                        if (mAdapter != null) {
                            mAdapter.removeInstanceComment();
                            if (mAdapter.getItemCount() <= 0) {
                                mViewModel.showOrHideEmptyCommentLabel(true);
                            }
                        }
                    }

                    @Override
                    public void onPostCommentSuccess(Comment comment) {
                        if (mAdapter != null) {
                            notifyItemChanged(mAdapter.getItemCount() - 1, comment);
                        }
                    }

                    @Override
                    public void onUpdateCommentSuccess(Comment comment, int position) {
                        hideSoftKeyboard();
                        mAdapter.notifyItemChanged(position, comment);
                    }
                });
        setVariable(BR.viewModel, mViewModel);
        if (getArguments() != null && getArguments().getBoolean(AUTO_SHOW_KEYBOARD, false)) {
            getBinding().includeComment.messageInput.getMentionsEditText().post(() ->
                    showKeyboard(getBinding().includeComment.messageInput.getMentionsEditText()));
        }
    }

    public int getNumberOfAddComment() {
        if (mViewModel != null) {
            return mViewModel.getNumberOfCommentAdd();
        }

        return 0;
    }

    public void addRemoveCommentCount(int removeCount) {
        int numberOfAddComment = getNumberOfAddComment();
        mViewModel.setNumberOfCommentAdd(numberOfAddComment - removeCount);
    }

    public void addCommentCount(int addCount) {
        int numberOfAddComment = getNumberOfAddComment();
        mViewModel.setNumberOfCommentAdd(numberOfAddComment + addCount);
    }

    private void initAdapter(List<Comment> commentList, boolean isCanLoadMore) {
        mIsCanLoadMore = isCanLoadMore;
        getBinding().recyclerview.setOnTouchListener((v, e) -> {
            hideSoftKeyboard();
            v.performClick();
            return false;
        });

        List<Adaptive> adaptiveList = new ArrayList<>();
        if (mIsCanLoadMore) {
            adaptiveList.add(new LoadCommentDirection());
        }
        adaptiveList.addAll(commentList);

        getBinding().recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new CommentAdapter(adaptiveList,
                PopUpCommentFragment.this,
                new OnLoadPreviousCommentClickListener() {
                    @Override
                    public void onClick(LoadCommentDirection loadPreviousComment, int position) {
                        mViewModel.loadMore(new OnCallbackListListener<Response<List<Comment>>>() {
                            @Override
                            public void onFail(ErrorThrowable ex) {
                                mAdapter.checkToRemoveLoadPreviousItem(mIsCanLoadMore, loadPreviousComment, position);
                            }

                            @Override
                            public void onComplete(Response<List<Comment>> result, boolean canLoadMore) {
                                mIsCanLoadMore = canLoadMore;
                                mAdapter.checkToAddPreviousLoadedComment(mIsCanLoadMore, loadPreviousComment, position, result.body());
                            }
                        });
                    }
                });
        mAdapter.setCanLoadMore(false);
        getBinding().recyclerview.setAdapter(mAdapter);
        getBinding().recyclerview.post(() -> {
            if (!adaptiveList.isEmpty()) {
                getBinding().recyclerview.scrollToPosition(adaptiveList.size() - 1);
            }
        });
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (textView.getId() == R.id.message_input && i == EditorInfo.IME_ACTION_SEND) {
            getBinding().includeComment.textViewSend.performClick();
        }
        return false;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.layout_pop_up_comment;
    }

    @Override
    public void onReplyButtonClick(Comment comment, int position) {
        if (mCommentDialogClickListener != null) {
            mCommentDialogClickListener.onReplaceCommentFragment(comment, position);
        }
    }

    @Override
    public void onLikeButtonClick(Comment comment, int position, OnCallbackListener<Response<Object>> listener) {
        mViewModel.checkToLikeComment(comment, listener);
    }

    @Override
    public void onLongPress(Comment comment, int position) {
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        boolean isOwner = comment.getUser() != null && SharedPrefUtils.checkIsMe(getActivity(), comment.getUser().getId());
        dialog.addItem(PostContextMenuItemType.getCommentPopupMenuItem(comment, isOwner));
        dialog.setOnItemClickListener(result -> {
            dialog.dismiss();
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.COPY_TEXT_CLICK) {
                CopyTextUtil.copyTextToClipboard(getActivity(), comment.getContent());

            } else if (itemType == PostContextMenuItemType.EDIT_POST_CLICK) {
                getBinding().includeComment.messageInput.post(() -> showKeyboard(getBinding().includeComment.messageInput.getMentionsEditText()));
                getBinding().includeComment.messageInput.setText(comment.getContent(), false);
                mViewModel.setUpdateComment(comment, position);

            } else {
                MessageDialog messageDialog = new MessageDialog();
                messageDialog.setTitle(getString(R.string.comment_popup_confirm_delete_title));
                messageDialog.setMessage(getString(R.string.comment_popup_confirm_delete_description));
                messageDialog.setRightText(getString(R.string.setting_change_language_dialog_cancel_button), null);
                messageDialog.setLeftText(getString(R.string.seller_store_button_delete),
                        v -> mViewModel.onDeleteComment(comment, () -> {
                            notifyItemRemove(position);
                        }));
                messageDialog.show(getChildFragmentManager());
            }
        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().includeComment.messageInput.removeListPopupWindow();
    }

    public void notifyItemChanged(int position, Comment comment) {
        mAdapter.notifyItemChanged(position, comment);
    }

    public void notifyItemRemove(int position) {
        mAdapter.notifyItemRemove(position);
        if (mAdapter.getItemCount() == 0) {
            mViewModel.showEmptyData();
        }
    }

    private void bindGifView() {
        getBinding().includeComment.containerSearchGif.onBackPressClickListener(v -> {
            getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
            getBinding().includeComment.messageInput.requestFocus();
        });

        getBinding().includeComment.gifImageView.setOnClickListener(v -> {
            getBinding().includeComment.sendLayout.setVisibility(View.GONE);
            getBinding().includeComment.containerSearchGif.loadTrendingGifList(baseGifModel -> {
                getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
                getBinding().includeComment.messageInput.requestFocus();

                ((AbsBaseActivity) mContext).startLoginWizardActivity(new LoginActivityResultCallback() {
                    @Override
                    public void onActivityResultSuccess(boolean isAlreadyLogin) {
                        Media media = new Media();
                        media.setWidth(baseGifModel.getGifWidth());
                        media.setHeight(baseGifModel.getGifHeight());
                        media.setUrl(baseGifModel.getGifUrl());
                        media.setType(MediaType.TENOR_GIF);

                        Comment comment = new Comment();
                        comment.setContent("gif");
                        comment.setUser(SharedPrefUtils.getUser(mContext));
                        comment.setDate(new Date());
                        comment.setMedia(media);
                        comment.setContentType(mContentType.getValue());
                        comment.setPostId(mPostId);
                        mViewModel.postComment(comment);
                        if (mAdapter != null) {
                            mAdapter.addLatestComment(comment);
                            getBinding().recyclerview.scrollToPosition(mAdapter.getItemCount() - 1);
                        }
                    }
                });
            });
        });
    }

    public boolean isCloseSearchGifView() {
        if (getBinding().includeComment.containerSearchGif.getVisibility() == View.VISIBLE) {
            getBinding().includeComment.containerSearchGif.clearData();
            getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
            getBinding().includeComment.messageInput.requestFocus();
            return true;
        } else {
            return false;
        }
    }

    private void hideSoftKeyboard() {
        if (getContext() != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.hideSoftInputFromWindow(getBinding().includeComment.messageInput.getWindowToken(), 0);
        }
    }
}
