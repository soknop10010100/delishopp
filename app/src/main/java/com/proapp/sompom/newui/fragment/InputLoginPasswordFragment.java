package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentInputLoginPasswordBinding;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.intent.ConfirmCodeIntent;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.result.AuthResponse;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.InputLoginPasswordDataManager;
import com.proapp.sompom.viewmodel.AbsLoginViewModel;
import com.proapp.sompom.viewmodel.InputLoginPasswordViewModel;

import javax.inject.Inject;

import androidx.annotation.Nullable;

/**
 * Created by Veasna Chhom on 4/21/21.
 */
public class InputLoginPasswordFragment extends AbsLoginFragment<FragmentInputLoginPasswordBinding, InputLoginPasswordViewModel> implements
        AbsLoginViewModel.AbsLoginViewModelListener {

    @PublicQualifier
    @Inject
    public ApiService mApiService;
    @Inject
    public ApiService mPrivateApiService;

    public static InputLoginPasswordFragment newInstance(ExchangeAuthData data) {

        Bundle args = new Bundle();
        args.putParcelable(ConfirmCodeIntent.DATA, data);

        InputLoginPasswordFragment fragment = new InputLoginPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected boolean shouldRequestAllPermissionAtStartUp() {
        return false;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_input_login_password;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mViewModel = new InputLoginPasswordViewModel(requireContext(),
                new InputLoginPasswordDataManager(requireContext(),
                        mApiService,
                        mPrivateApiService,
                        getArguments().getParcelable(ConfirmCodeIntent.DATA)),
                this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onLoginError(String message) {
        handleLoginError(message);
    }

    @Override
    public void onLoginSuccess() {
        handleOnLoginSuccess();
    }

    @Override
    public void onChangePasswordRequire(AuthResponse authResponse) {
        handleChangePasswordRequire(authResponse);
    }
}
