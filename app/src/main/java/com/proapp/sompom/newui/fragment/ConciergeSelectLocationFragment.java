package com.proapp.sompom.newui.fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentConciergeSelectLocationBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.intent.newintent.ConciergeSelectLocationIntent;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeSelectLocationFragmentViewModel;

/**
 * Created by Or Vitovongsak on 25/11/21.
 */
public class ConciergeSelectLocationFragment extends AbsMapFragment<FragmentConciergeSelectLocationBinding> {

    private Context mContext;
    private SearchAddressResult mSearchAddressResult;

    public static ConciergeSelectLocationFragment newInstance(SearchAddressResult result) {

        Bundle args = new Bundle();
        args.putParcelable(ConciergeSelectLocationIntent.ADDRESS_DATA, result);

        ConciergeSelectLocationFragment fragment = new ConciergeSelectLocationFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_SELECT_DELIVERY_ADDRESS_ON_MAP;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_select_location;
    }

    @Override
    protected void onViewAndMapCreated() {
        mContext = getContext();

        if (getArguments() != null) {
            mSearchAddressResult = getArguments().getParcelable(ConciergeSelectLocationIntent.ADDRESS_DATA);
        }

        getBinding().layoutToolbar.toolbarTitle.setText(mContext.getString(R.string.shop_select_location_screen_title));
        setToolbar(getBinding().layoutToolbar.toolbar, true);

        ConciergeSelectLocationFragmentViewModel viewModel = new ConciergeSelectLocationFragmentViewModel(mContext,
                getGoogleMap(),
                mSearchAddressResult,
                () -> {
                }
        );

        setVariable(BR.viewModel, viewModel);
    }
}
