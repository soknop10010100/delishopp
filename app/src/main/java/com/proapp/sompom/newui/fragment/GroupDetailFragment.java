package com.proapp.sompom.newui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.UserListAdapter;
import com.proapp.sompom.broadcast.SoundEffectService;
import com.proapp.sompom.chat.AbsChatBinder;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.databinding.FragmentGroupDetailBinding;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.listener.UserListAdaptive;
import com.proapp.sompom.model.AddGroupParticipantHeader;
import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.AddGroupParticipantDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.GroupDetailDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.newviewmodel.GroupDetailFragmentViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 4/29/20.
 */
public class GroupDetailFragment extends AbsBindingFragment<FragmentGroupDetailBinding> implements AbsBaseActivity.OnServiceListener {

    @Inject
    public ApiService mApiService;
    private GroupDetailFragmentViewModel mViewModel;
    private Conversation mConversation;
    private User mRecipient;
    private SocketService.SocketBinder mSocketBinder;
    private String mConversationID;
    private UserListAdapter<UserListAdaptive> mMemberAdapter;
    private UserListAdapter<UserListAdaptive> mOwnerAdapter;
    private GroupDetail mGroupDetail;

    public static GroupDetailFragment newInstance(Conversation conversation) {
        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.CONVERSATION, conversation);
        GroupDetailFragment fragment = new GroupDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_group_detail;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        if (requireActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) requireActivity()).addOnServiceListener(this);
        }
    }

    @Override
    public void onServiceReady(AbsChatBinder binder) {
        Timber.i("onServiceReady");
        //Prevent any further action if the screen was already destroyed.
        if (!isAddedToActivity()) {
            return;
        }

        getArgumentsData();
        mSocketBinder = (SocketService.SocketBinder) binder;
        mViewModel = new GroupDetailFragmentViewModel(requireActivity(),
                getChildFragmentManager(),
                mConversation,
                mRecipient,
                new GroupDetailDataManager(requireContext(), mApiService, mConversationID),
                mSocketBinder,
                new GroupDetailFragmentViewModel.OnCallback() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                    }

                    @Override
                    public void onComplete(Conversation result, boolean canLoadMore) {
                    }

                    @Override
                    public void onLoadGroupDetailSuccess(GroupDetail groupDetail) {
                        if (isAddedToActivity()) {
                            mGroupDetail = groupDetail;
                            mSocketBinder.onResume(mConversationID);
                            initAdapter();
                        }
                    }

                    @Override
                    public void onReceivedCallEvent(AbsCallService.CallType callType,
                                                    User recipient,
                                                    boolean isMissedCall,
                                                    int calledDuration) {
                        Timber.i("onReceivedCallEvent: recipient: " + recipient);
                        requireActivity().runOnUiThread(() -> sendCallEventMessage(ChatHelper
                                .buildOneToOneCallEventMessage(requireContext(),
                                        callType,
                                        recipient,
                                        isMissedCall,
                                        calledDuration)));
                    }

                    @Override
                    public void onRemoveParticipantSuccess(boolean isRemoveGroupOwner, int position) {
                        if (isRemoveGroupOwner) {
                            mOwnerAdapter.removeUser(position);
                            mViewModel.onGroupOwnerRemoved();
                        } else {
                            mMemberAdapter.removeUser(position);
                            mViewModel.onGroupParticipantRemoved();
                        }
                    }

                    @Override
                    public void onNameChangeSuccess(Conversation conversation) {
                        mConversation = conversation;
                        setActivityResultSuccess();
                    }

                    @Override
                    public void onPhotoChangeSuccess(Conversation conversation) {
                        mConversation = conversation;
                        setActivityResultSuccess();
                    }
                }, (callType, recipient, isMissedCall, calledDuration) -> {
            Timber.i("onReceivedCallEvent: " + calledDuration + ", isMissedCall: " + isMissedCall);
            requireActivity().runOnUiThread(() -> sendCallEventMessage(ChatHelper
                    .buildOneToOneCallEventMessage(requireContext(),
                            callType,
                            recipient,
                            isMissedCall,
                            calledDuration)));
        });
        setVariable(BR.viewModel, mViewModel);
    }

    private void setActivityResultSuccess() {
        Intent intent = new Intent();
        intent.putExtra(SharedPrefUtils.CONVERSATION, mConversation);
        requireActivity().setResult(Activity.RESULT_OK, intent);
    }

    private void sendCallEventMessage(Chat comment) {
        Timber.i("sendCallEventMessage: " + new Gson().toJson(comment));
        comment.setStatus(MessageState.SENDING);
        SoundEffectService.playSound(requireActivity(), SoundEffectService.SoundFile.SEND_MESSAGE);
        mSocketBinder.sendMessage(mRecipient, comment);
    }

    private void getArgumentsData() {
        if (getArguments() != null) {
            mConversation = getArguments().getParcelable(SharedPrefUtils.CONVERSATION);
        }

        if (mConversation != null && mConversation.isGroup()) {
            mConversationID = mConversation.getGroupId();
            mRecipient = mConversation.getOneToOneRecipient(getContext());
        }
    }

    private void initAdapter() {
        if (mMemberAdapter == null) {
            mMemberAdapter = new UserListAdapter<>(getUserList(AddGroupParticipantDialog.AddingType.PARTICIPANT,
                    mGroupDetail.getParticipant()),
                    requireContext(),
                    false,
                    true,
                    isAllowToAddOrRemoveMember());
            mMemberAdapter.setListener(new UserListAdapter.UserListAdapterListener() {
                @Override
                public void onAddGroupParticipantClicked(AddGroupParticipantDialog.AddingType addingType) {
                    showAddParticipantDialog(AddGroupParticipantDialog.AddingType.PARTICIPANT);
                }

                @Override
                public void onDeleteClicked(int position, User user) {
                    mViewModel.removeParticipant(false, user.getId(), position);
                }

                @Override
                public void onAttemptedCallWhenNoNetworkConnectedOrDeviceIsInAnotherCallProcess(boolean isInAnotherCall) {
                    showSnackBar(isInAnotherCall ? R.string.call_toast_already_in_call_description : R.string.call_toast_no_internet, true);
                }

                @Override
                public void onMakeCallError(String error) {
                    showSnackBar(error, true);
                }
            });
            LinearLayoutManager manager = new LinearLayoutManager(getContext());

            manager.setOrientation(RecyclerView.VERTICAL);
            getBinding().memberView.setLayoutManager(manager);
            getBinding().memberView.setAdapter(mMemberAdapter);
        }

        if (mOwnerAdapter == null) {
            mOwnerAdapter = new UserListAdapter<>(getUserList(AddGroupParticipantDialog.AddingType.OWNER,
                    mGroupDetail.getGroupOwnerList()),
                    requireContext(),
                    false,
                    true,
                    isAllowToAddOrRemoveOwner());
            mOwnerAdapter.setListener(new UserListAdapter.UserListAdapterListener() {
                @Override
                public void onAddGroupParticipantClicked(AddGroupParticipantDialog.AddingType addingType) {
                    showAddParticipantDialog(AddGroupParticipantDialog.AddingType.OWNER);
                }

                @Override
                public void onDeleteClicked(int position, User user) {
                    mViewModel.removeParticipant(true, user.getId(), position);
                }

                @Override
                public void onAttemptedCallWhenNoNetworkConnectedOrDeviceIsInAnotherCallProcess(boolean isInAnotherCall) {
                    showSnackBar(isInAnotherCall ? R.string.call_toast_already_in_call_description : R.string.call_toast_no_internet, true);
                }

                @Override
                public void onMakeCallError(String error) {
                    showSnackBar(error, true);
                }
            });
            LinearLayoutManager manager = new LinearLayoutManager(getContext());
            manager.setOrientation(RecyclerView.VERTICAL);
            getBinding().ownerView.setLayoutManager(manager);
            getBinding().ownerView.setAdapter(mOwnerAdapter);
        }
    }

    private void showAddParticipantDialog(AddGroupParticipantDialog.AddingType addingType) {
        AddGroupParticipantDialog dialog = AddGroupParticipantDialog.newInstance(mConversationID,
                addingType);
        dialog.setListener((type, groupDetail) -> {
            if (type == AddGroupParticipantDialog.AddingType.OWNER) {
                //Maintain the add item
                List<UserListAdaptive> userListAdaptiveList = new ArrayList<>(groupDetail.getGroupOwnerList());
                userListAdaptiveList.add(0, mOwnerAdapter.getDatas().get(0));
                mGroupDetail.setGroupOwnerList(groupDetail.getGroupOwnerList());
                mOwnerAdapter.refreshData(userListAdaptiveList);
            }
            /*
                This also manage to update group member too since the group owner will also be in group
                member too.
            */
            updateGroupMember(groupDetail.getParticipant());
        });
        dialog.show(getChildFragmentManager(), null);
    }

    private void updateGroupMember(List<User> groupMembers) {
        List<UserListAdaptive> userListAdaptiveList = new ArrayList<>(groupMembers);
        //Maintain the add item
        userListAdaptiveList.add(0, mMemberAdapter.getDatas().get(0));
        mGroupDetail.setParticipant(groupMembers);
        mMemberAdapter.refreshData(userListAdaptiveList);
        mViewModel.bindGroupProfile();
        mViewModel.bindGroupOwnerCounter(mGroupDetail.getGroupOwnerList().size());
        mViewModel.bindGroupParticipantCounter(mGroupDetail.getParticipant().size());
    }

    private boolean isAllowToAddOrRemoveMember() {
//        return mViewModel.isCurrentUserIsGroupCreator() || mViewModel.isCurrentUserIsGroupOwner();
        //This feature is not enable in this project
        return false;
    }

    private boolean isAllowToAddOrRemoveOwner() {
//        return mViewModel.isCurrentUserIsGroupCreator();
        //This feature is not enable in this project
        return false;
    }

    private List<UserListAdaptive> getUserList(AddGroupParticipantDialog.AddingType addingType, List<User> users) {
        List<UserListAdaptive> participantList = new ArrayList<>(users);
        if (addingType == AddGroupParticipantDialog.AddingType.OWNER && isAllowToAddOrRemoveOwner()) {
            participantList.add(0, new AddGroupParticipantHeader(getString(R.string.add_owner), addingType));
        } else if (addingType == AddGroupParticipantDialog.AddingType.PARTICIPANT && isAllowToAddOrRemoveMember()) {
            participantList.add(0, new AddGroupParticipantHeader(getString(R.string.add_member), addingType));
        }
        return participantList;
    }
}
