package com.proapp.sompom.newui.fragment;

import android.os.Bundle;

import androidx.databinding.ViewDataBinding;

import com.proapp.sompom.R;

/**
 * Created by Chhom Veasna on 7/24/2020.
 */
public class AbsGroupDetailMediaFragment<T extends ViewDataBinding> extends AbsBindingFragment<T> {

    public static final String GROUP_ID = "GROUP_ID";
    public static final String TYPE = "TYPE";

    private boolean mIsReceivedFistNavigation;

    public static AbsGroupDetailMediaFragment newInstance() {

        Bundle args = new Bundle();

        AbsGroupDetailMediaFragment fragment = new AbsGroupDetailMediaFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_group_detail_media;
    }

    protected String getGroupId() {
        if (getArguments() != null) {
            return getArguments().getString(GROUP_ID);
        }

        return null;
    }

    public void onSelected() {
        if (!mIsReceivedFistNavigation) {
            mIsReceivedFistNavigation = true;
            onReceivedFirstNavigation();
        }
    }

    protected void onReceivedFirstNavigation() {
        //Empty Impl...
    }
}
