package com.proapp.sompom.newui.dialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.PartialAskFloatingWindowPermissionDialogBindingImpl;
import com.proapp.sompom.databinding.PartialAutoStartPermissionDialogBinding;
import com.proapp.sompom.helper.AutoStartPermissionDialogHelper;
import com.proapp.sompom.model.emun.AskFloatingWindowPermissionForType;
import com.proapp.sompom.utils.SharedPrefUtils;

/**
 * Created by Veasna Chhom on 10/22/20.
 */
public class AskFloatingWindowPermissionDialog extends MessageDialog {

    private static final String REQUEST_TYPE = "REQUEST_TYPE";
    public static final int OVERLAY_PERMISSION_REQUEST_CODE = 0X0022;

    private boolean mIsDontAskAgain;
    private AskFloatingWindowPermissionForType mType;

    public static AskFloatingWindowPermissionDialog newInstance(AskFloatingWindowPermissionForType type) {

        Bundle args = new Bundle();
        args.putString(REQUEST_TYPE, type.getValue());

        AskFloatingWindowPermissionDialog fragment = new AskFloatingWindowPermissionDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        mType = AskFloatingWindowPermissionForType.fromValue(getArguments().getString(REQUEST_TYPE));
        PartialAskFloatingWindowPermissionDialogBindingImpl binding = DataBindingUtil.inflate(inflater,
                R.layout.partial_ask_floating_window_permission_dialog,
                null,
                true);
        getBinding().textviewTitle.setTypeface(null, Typeface.BOLD);
        setCancelable(false);
        setTitle(getString(R.string.popup_ask_floating_window_permission_title));
        setMessage(getDescription());
        setLeftText(getString(R.string.popup_ask_floating_window_permission_proceed_button), view1 -> {
            requestPermission();
        });
        setRightText(getString(R.string.popup_no_button), null);
        binding.checkboxNotAskAgain.setOnCheckedChangeListener((compoundButton, b) -> {
            mIsDontAskAgain = b;
        });
        getBinding().view.addView(binding.getRoot());
        super.onViewCreated(view, savedInstanceState);
        new Handler().post(() -> AutoStartPermissionDialogHelper.setLastShownAlertDate(requireContext(), System.currentTimeMillis()));
        if (mType == AskFloatingWindowPermissionForType.VIDEO) {
            View checkBoxDonAskAgain = getBinding().getRoot().findViewById(R.id.checkboxNotAskAgain);
            if (checkBoxDonAskAgain != null) {
                checkBoxDonAskAgain.setVisibility(View.GONE);
            }
        }
    }

    private String getDescription() {
        if (mType == AskFloatingWindowPermissionForType.CALL) {
            return getString(R.string.popup_ask_floating_window_permission_for_call_description);
        }

        return getString(R.string.popup_ask_floating_window_permission_for_video_description);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mType == AskFloatingWindowPermissionForType.CALL && mIsDontAskAgain) {
            SharedPrefUtils.setBoolean(requireContext(),
                    SharedPrefUtils.DO_NOT_ASK_CALL_FLOATING_PERMISSION_AGAIN,
                    true);
        }
    }

    private void requestPermission() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + getActivity().getPackageName()));
        requireActivity().startActivityForResult(intent, OVERLAY_PERMISSION_REQUEST_CODE);
    }
}
