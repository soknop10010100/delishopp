package com.proapp.sompom.newui;

import static com.proapp.sompom.newui.ChatActivity.REFRESH_NOTIFICATION_BADGE_ACTION;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.intent.newintent.ConciergeOrderHistoryContainerIntent;
import com.proapp.sompom.newui.fragment.ConciergeOrderHistoryContainerFragment;
import com.sompom.pushy.service.SendBroadCastHelper;

/**
 * Created by Veasna Chhom on 4/25/22.
 */
public class ConciergeOrderHistoryContainerActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(ConciergeOrderHistoryContainerFragment.newInstance(new ConciergeOrderHistoryContainerIntent(getIntent()).isFromProfileScreen()),
                ConciergeOrderHistoryContainerFragment.class.getName());
        SendBroadCastHelper.verifyAndSendBroadCast(this, new Intent(REFRESH_NOTIFICATION_BADGE_ACTION));
    }
}
