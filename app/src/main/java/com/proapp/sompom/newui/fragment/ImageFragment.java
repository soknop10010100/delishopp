package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.GlideApp;
import com.proapp.sompom.utils.GlideLoadUtil;
import com.proapp.sompom.utils.SharedPrefUtils;


/**
 * Created by he.rotha on 3/9/16.
 */
public class ImageFragment extends Fragment {

    public static ImageFragment getInstance(String product) {
        final ImageFragment fragment = new ImageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SharedPrefUtils.PRODUCT, product);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_image, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String imageUrl = "";
        if (getArguments() != null && getArguments().containsKey(SharedPrefUtils.PRODUCT)) {
            imageUrl = getArguments().getString(SharedPrefUtils.PRODUCT);
        }

        final ImageView imageView = view.findViewById(R.id.imageview);
        if (!GlideLoadUtil.isValidContext(requireContext())) {
            return;
        }
        GlideApp.with(ImageFragment.this)
                .load(imageUrl)
                .placeholder(R.drawable.ic_photo_placeholder_400dp)
                .into(imageView);
    }
}
