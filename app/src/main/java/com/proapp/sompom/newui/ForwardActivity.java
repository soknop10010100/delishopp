package com.proapp.sompom.newui;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.proapp.sompom.helper.CheckPermissionCallbackHelper;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.intent.ForwardIntent;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.newui.fragment.ForwardFragment;

import java.util.ArrayList;

/**
 * Created by He Rotha on 9/13/18.
 */
public class ForwardActivity extends AbsDefaultActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPermission(new CheckPermissionCallbackHelper(this,
                CheckPermissionCallbackHelper.Type.STORAGE) {

            @Override
            public void onPermissionGranted() {
                ForwardIntent intent = new ForwardIntent(getThis(), getIntent());
                ArrayList<Media> medias = intent.getMedia();
                if (medias != null && medias.isEmpty() && intent.getChat() == null) {
                    finish();
                    return;
                }

                startLoginWizardActivity(new LoginActivityResultCallback() {
                    @Override
                    public void onActivityResultSuccess(boolean isAlreadyLogin) {
                        final Fragment fr = ForwardFragment.newInstance(intent.getChat(),
                                medias,
                                intent.isFromOutSideApplication());
                        setFragment(fr, ForwardFragment.TAG);
                    }

                    @Override
                    public void onActivityResultFail() {
                        finish();
                    }
                });
            }

            @Override
            public void onCancelClick() {
                finish();
            }
        }, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }
}
