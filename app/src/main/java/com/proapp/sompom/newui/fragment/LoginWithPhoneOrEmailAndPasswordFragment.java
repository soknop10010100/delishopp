package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentLoginWithPhoneOrEmailAndPasswordBinding;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.model.result.AuthResponse;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.LoginDataManager;
import com.proapp.sompom.viewmodel.AbsLoginViewModel;
import com.proapp.sompom.viewmodel.LoginWithPhoneOrEmailAndPasswordViewModel;

import javax.inject.Inject;

import androidx.annotation.Nullable;

/**
 * Created by Veasna Chhom on 4/21/21.
 */
public class LoginWithPhoneOrEmailAndPasswordFragment extends AbsLoginFragment<FragmentLoginWithPhoneOrEmailAndPasswordBinding, LoginWithPhoneOrEmailAndPasswordViewModel> implements
        AbsLoginViewModel.AbsLoginViewModelListener {

    @PublicQualifier
    @Inject
    public ApiService mApiService;
    @Inject
    public ApiService mPrivateApiService;

    public static LoginWithPhoneOrEmailAndPasswordFragment newInstance() {

        Bundle args = new Bundle();

        LoginWithPhoneOrEmailAndPasswordFragment fragment = new LoginWithPhoneOrEmailAndPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected boolean forceRebuildControllerComponent() {
        return true;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_login_with_phone_or_email_and_password;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mViewModel = new LoginWithPhoneOrEmailAndPasswordViewModel(requireContext(),
                new LoginDataManager(requireContext(),
                        mApiService,
                        mPrivateApiService),
                this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onLoginError(String message) {
        handleLoginError(message);
    }

    @Override
    public void onLoginSuccess() {
        handleOnLoginSuccess();
    }

    @Override
    public void onChangePasswordRequire(AuthResponse authResponse) {
        handleChangePasswordRequire(authResponse);
    }
}
