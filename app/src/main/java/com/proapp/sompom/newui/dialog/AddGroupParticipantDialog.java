package com.proapp.sompom.newui.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.AddGroupParticipantAdapter;
import com.proapp.sompom.databinding.DialogAddGroupMemberBinding;
import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.AddGroupMemberDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.AddGroupMemberViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class AddGroupParticipantDialog extends AbsBindingFragmentDialog<DialogAddGroupMemberBinding>
        implements AddGroupMemberViewModel.OnCallback {

    private static final String GROUP_ID = "GROUP_ID";
    private static final String ADDING_TYPE = "ADDING_TYPE";

    private AddGroupMemberViewModel mViewModel;
    @Inject
    public ApiService mApiService;
    private AddGroupParticipantDialogListener mListener;
    private AddGroupParticipantAdapter mAdapter;

    public static AddGroupParticipantDialog newInstance(String groupId, AddingType addingType) {

        Bundle args = new Bundle();
        args.putString(GROUP_ID, groupId);
        args.putInt(ADDING_TYPE, addingType.getValue());

        AddGroupParticipantDialog fragment = new AddGroupParticipantDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setListener(AddGroupParticipantDialogListener listener) {
        mListener = listener;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_add_group_member;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return super.onCreateView(inflater, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        if (getArguments() != null) {
            String groupId = getArguments().getString(GROUP_ID);
            AddingType type = AddingType.getFromValue(getArguments().getInt(ADDING_TYPE));
            mViewModel = new AddGroupMemberViewModel(new AddGroupMemberDataManager(requireContext(),
                    mApiService,
                    groupId,
                    type),
                    this);
            setVariable(BR.viewModel, mViewModel);
            mViewModel.requestAuthorizedUserList();
        }
    }

    @Override
    public void onRootClicked() {
        if (isCancelable()) {
            dismiss();
        }
    }

    @Override
    public void onCancelClicked() {
        dismiss();
    }

    @Override
    public void onAddSuccess(AddingType type, GroupDetail groupDetail) {
        showSnackBar(getString(R.string.group_info_update_success), false);
        new Handler().postDelayed(() -> {
            if (mListener != null) {
                mListener.onAddSuccess(type, groupDetail);
            }
            dismiss();
        }, 500);
    }

    @Override
    public List<String> getSelectedIds() {
        if (mAdapter != null) {
            return mAdapter.getSelectedUserIdList();
        }

        return new ArrayList<>();
    }

    @Override
    public void onLoadAuthorizedUserListSuccess(List<User> userList) {
        mAdapter = new AddGroupParticipantAdapter(userList);
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        getBinding().recyclerView.setAdapter(mAdapter);
    }

    public interface AddGroupParticipantDialogListener {
        void onAddSuccess(AddingType type, GroupDetail groupDetail);
    }

    public enum AddingType {
        OWNER(1), PARTICIPANT(2);

        private int mValue;

        AddingType(int value) {
            mValue = value;
        }

        public int getValue() {
            return mValue;
        }

        public static AddingType getFromValue(int value) {
            for (AddingType addingType : AddingType.values()) {
                if (addingType.getValue() == value) {
                    return addingType;
                }
            }

            return PARTICIPANT;
        }
    }
}
