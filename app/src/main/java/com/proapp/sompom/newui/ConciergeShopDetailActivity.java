package com.proapp.sompom.newui;

import android.os.Bundle;

import com.proapp.sompom.intent.ConciergeShopDetailIntent;
import com.proapp.sompom.newui.fragment.ConciergeShopDetailFragment;

public class ConciergeShopDetailActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConciergeShopDetailIntent intent = new ConciergeShopDetailIntent(getIntent());
        setFragment(ConciergeShopDetailFragment.newInstance(intent.getId(), intent.getIsDiscountOnly()),
                ConciergeShopDetailFragment.TAG);
    }
}
