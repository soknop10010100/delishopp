package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ViewPagerAdapter;
import com.proapp.sompom.databinding.FragmentSearchGeneralContainerResultBinding;
import com.proapp.sompom.model.Search;
import com.proapp.sompom.model.SearchGeneralTypeResult;
import com.proapp.sompom.model.emun.GeneralSearchResultType;
import com.proapp.sompom.model.emun.SearchGeneralResultType;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.SearchGeneralTypeResultDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.SearchMessageResultFragmentViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 8/2/18.
 */

public class SearchGeneralContainerResultFragment extends AbsBindingFragment<FragmentSearchGeneralContainerResultBinding> {
    public static final String TAG = SearchGeneralContainerResultFragment.class.getName();
    @Inject
    public ApiService mApiService;
    private ViewPagerAdapter mViewPagerAdapter;
    private String mSearchText;
    private SearchMessageResultFragmentViewModel mViewModel;

    public static SearchGeneralContainerResultFragment newInstance() {
        return new SearchGeneralContainerResultFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search_general_container_result;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        initializeTabs();

        SearchGeneralTypeResultDataManager dataManager = new SearchGeneralTypeResultDataManager(getActivity(), mApiService);
        mViewModel = new SearchMessageResultFragmentViewModel(dataManager,
                new SearchMessageResultFragmentViewModel.SearchMessageResultFragmentViewModelListener() {
                    @Override
                    public void onFail(ErrorThrowable ex, SearchGeneralTypeResult defaultData) {
                        bindTabData(defaultData, false);
                    }

                    @Override
                    public void onComplete(SearchGeneralTypeResult result,
                                           boolean isLocalData,
                                           boolean canLoadMore) {
                        bindTabData(result, isLocalData);
                    }
                });
        setVariable(BR.viewModel, mViewModel);
    }

    public void showLoading() {
        if (mViewModel == null) {
            return;
        }

        mViewModel.showLoading();
    }

    public void onSearch(String searchText, boolean isLocalSearch) {
        if (!isLocalSearch) {
            mSearchText = searchText;
            if (mViewModel != null) {
                mViewModel.getData(searchText);
            }
        } else {
            if (mViewModel != null) {
                mViewModel.performLocalUserContactFilter(searchText);
            }
        }
        passSearchKeyToEachResultSection(searchText);
    }

    private void bindTabData(SearchGeneralTypeResult result, boolean isLocalData) {
        if (mViewModel == null) {
            return;
        }

        /*
            Since we filter first local user contact to pre display the search result. There are following
            rule to hide the progression since we are managing both local and online search.
            1. If there is any local contact search result, then hide the progression
            2, If there is no local data found, and it is the response of server, we also hide
            the progression.

         */
        if ((result.getUser() != null &&
                result.getUser().getData() != null &&
                !result.getUser().getData().isEmpty()) ||
                !isLocalData) {
            mViewModel.hideLoading();
        }

        if (getActivity() == null || mViewPagerAdapter == null) {
            return;
        }
        ArrayList<Search> data;

        data = new ArrayList<>();
        data.addAll(result.getUser().getData());
        data.addAll(result.getChatList().getData());
        ((SearchGeneralTypeResultFragment) mViewPagerAdapter.getFr(0)).bindData(data,
                isLocalData,
                mViewModel.isCanLoadMore(GeneralSearchResultType.ALL),
                mViewModel.getDataManager());
        ((SearchGeneralTypeResultFragment) mViewPagerAdapter.getFr(1)).bindData(new ArrayList<>(result.getUser().getData()),
                isLocalData,
                mViewModel.isCanLoadMore(GeneralSearchResultType.PEOPLE),
                mViewModel.getDataManager());
        ((SearchGeneralTypeResultFragment) mViewPagerAdapter.getFr(2)).bindData(new ArrayList<>(result.getChatList().getData()),
                isLocalData,
                mViewModel.isCanLoadMore(GeneralSearchResultType.MESSAGE),
                mViewModel.getDataManager());
    }

    private void passSearchKeyToEachResultSection(String searchText) {
        //There is only 3 sections.
        if (mViewPagerAdapter != null && mViewPagerAdapter.getCount() == 3) {
            ((SearchGeneralTypeResultFragment) mViewPagerAdapter.getFr(0)).setSearchKey(searchText);
            ((SearchGeneralTypeResultFragment) mViewPagerAdapter.getFr(1)).setSearchKey(searchText);
            ((SearchGeneralTypeResultFragment) mViewPagerAdapter.getFr(2)).setSearchKey(searchText);
        }
    }

    private void initializeTabs() {
        /*
        Init tab with default empty data
         */
        if (mViewPagerAdapter == null) {
            mViewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
            //All Section
            mViewPagerAdapter.addFragment(SearchGeneralTypeResultFragment.newInstance(GeneralSearchResultType.ALL,
                    new ArrayList<>()),
                    getActivity().getString(SearchGeneralResultType.ALL.getTitle()));
            //People Section
            mViewPagerAdapter.addFragment(SearchGeneralTypeResultFragment.newInstance(GeneralSearchResultType.PEOPLE,
                    new ArrayList<>()),
                    getActivity().getString(SearchGeneralResultType.PEOPLE.getTitle()));
            //Message Section
            mViewPagerAdapter.addFragment(SearchGeneralTypeResultFragment.newInstance(GeneralSearchResultType.MESSAGE,
                    new ArrayList<>()),
                    getActivity().getString(SearchGeneralResultType.CONVERSATION.getTitle()));

            getBinding().viewPager.setAdapter(mViewPagerAdapter);
            getBinding().tabs.setupWithViewPager(getBinding().viewPager);

            //For this custom implementation, we must set tabIndicatorHeight and tabIndicator in layout file as normal
            //Then we set the width here
            //Do update with a better solution if necessary
            getBinding().tabs.setIndicatorWidth((int) getResources().getDimension(R.dimen.tab_indicator_width));
            getBinding().viewPager.setOffscreenPageLimit(5);
        } else {
            for (int i = 0; i < mViewPagerAdapter.getCount(); i++) {
                ((SearchGeneralTypeResultFragment) mViewPagerAdapter.getFr(i)).bindData(new ArrayList<>(),
                        false,
                        false,
                        mViewModel.getDataManager());
            }
        }
        //Select first tab by default.
        getBinding().getRoot().postDelayed(() -> getBinding().tabs.setSelectionTab(0),
                100);
    }
}
