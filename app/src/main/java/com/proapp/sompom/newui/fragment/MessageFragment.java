package com.proapp.sompom.newui.fragment;

import static com.proapp.sompom.newui.ChatActivity.REFRESH_NOTIFICATION_BADGE_ACTION;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentMessageBinding;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.listener.OnHomeMenuClick;
import com.proapp.sompom.model.emun.SegmentedControlItem;
import com.proapp.sompom.model.request.JoinChannelStatusUpdateBody;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.newviewmodel.MessageFragmentViewModel;
import com.sompom.pushy.service.SendBroadCastHelper;

import timber.log.Timber;

/**
 * Created by nuonveyo on 6/25/18.
 */

public class MessageFragment extends AbsBindingFragment<FragmentMessageBinding>
        implements OnHomeMenuClick {

    private AllMessageFragment mAllMessageFragment;
    private AllMessageFragment mMessageFragment;
    private AllMessageFragment mBuyingFragment;
    private AllMessageFragment mSellingFragment;
    private TelegramChatRequestFragment mTelegramFragment;
    private Fragment mCurrentFragment;
    private boolean mIsShow;
    private BroadcastReceiver mJoinGroupCallStatusReceiver;

    public static MessageFragment newInstance(boolean isLoadData) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(SharedPrefUtils.STATUS, isLoadData);
        MessageFragment fragment = new MessageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_message;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerUpdateJoinGroupCallStatusReceiver();
        initFragment();
        if (getArguments() != null) {
            boolean isLoadData = getArguments().getBoolean(SharedPrefUtils.STATUS, false);
            if (isLoadData) {
                onTabMenuClick();
            }
        }
    }

    private void registerUpdateJoinGroupCallStatusReceiver() {
        mJoinGroupCallStatusReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                JoinChannelStatusUpdateBody data = intent.getParcelableExtra(SharedPrefUtils.DATA);
                if (data != null) {
                    Timber.i("onReceive: JoinGroupCallStatusReceiver: " + new Gson().toJson(data));
                    //We have to request to update the badge of conversation too.
                    SendBroadCastHelper.verifyAndSendBroadCast(requireContext(),
                            new Intent(REFRESH_NOTIFICATION_BADGE_ACTION));

                    if (mAllMessageFragment != null) {
                        mAllMessageFragment.updateJoinGroupCallStatus(data.getConversationId(),
                                data.getChannelOpen(),
                                data.getVideo());
                    }
                    if (mMessageFragment != null) {
                        mMessageFragment.updateJoinGroupCallStatus(data.getConversationId(),
                                data.getChannelOpen(),
                                data.getVideo());
                    }
                    if (mBuyingFragment != null) {
                        mBuyingFragment.updateJoinGroupCallStatus(data.getConversationId(),
                                data.getChannelOpen(),
                                data.getVideo());
                    }
                }
            }
        };
        requireContext().registerReceiver(mJoinGroupCallStatusReceiver,
                new IntentFilter(ChatHelper.JOIN_GROUP_CALL_STATUS_UPDATE));
    }

    public Fragment getAllMessageFragment() {
        if (mAllMessageFragment == null) {
            mAllMessageFragment = AllMessageFragment.newInstance(SegmentedControlItem.All);
        }
        return mAllMessageFragment;
    }

    private Fragment getMessageFragment() {
        if (mMessageFragment == null) {
            mMessageFragment = AllMessageFragment.newInstance(SegmentedControlItem.Message);
        }
        return mMessageFragment;
    }

    private Fragment getBuyingFragment() {
        if (mBuyingFragment == null) {
            mBuyingFragment = AllMessageFragment.newInstance(SegmentedControlItem.Buying);
        }
        return mBuyingFragment;
    }

    private Fragment getTelegramFragment() {
        if (mTelegramFragment == null) {
            mTelegramFragment = TelegramChatRequestFragment.newInstance(conversation -> {
                mMessageFragment.onChannelCreateOrUpdate(conversation);
                mAllMessageFragment.onChannelCreateOrUpdate(conversation);
            });
        }
        return mTelegramFragment;
    }

    @Override
    public void onTabMenuClick() {
        if (mIsShow || !isAdded()) {
            return;
        }
        mIsShow = true;
        MessageFragmentViewModel viewModel = new MessageFragmentViewModel(getActivity(), item -> {
            if (item.getId() == SegmentedControlItem.All.getId()) {
                showFragment(getAllMessageFragment(), mCurrentFragment, false);
                mCurrentFragment = getAllMessageFragment();
            } else if (item.getId() == SegmentedControlItem.Message.getId()) {
                showFragment(getMessageFragment(), mCurrentFragment, mCurrentFragment.equals(mAllMessageFragment));
                mCurrentFragment = getMessageFragment();
            } else if (item.getId() == SegmentedControlItem.Buying.getId()) {
                showFragment(getBuyingFragment(), mCurrentFragment, true);
                mCurrentFragment = getBuyingFragment();
            } else if (item.getId() == SegmentedControlItem.Telegram.getId()) {
                showFragment(getTelegramFragment(), mCurrentFragment, true);
                mCurrentFragment = getTelegramFragment();
            }

            if (mCurrentFragment instanceof OnHomeMenuClick) {
                ((OnHomeMenuClick) mCurrentFragment).onTabMenuClick();
            }
        });
        setVariable(BR.viewModel, viewModel);
    }

    private void initFragment() {
        mCurrentFragment = getAllMessageFragment();
        getChildFragmentManager().beginTransaction()
                .add(R.id.containerLayout, getMessageFragment())
                .hide(getMessageFragment())
                .add(R.id.containerLayout, getBuyingFragment())
                .hide(getBuyingFragment())
//                Hide this feature for now
//                .add(R.id.containerLayout, getSellingFragment(), AllMessageFragment.TAG)
//                .hide(getSellingFragment())
                .add(R.id.containerLayout, getTelegramFragment())
                .hide(getTelegramFragment())
                .add(R.id.containerLayout, mCurrentFragment)
                .commit();
    }

    public void checkToRefreshMessageSectionIfNecessary() {
        if (mCurrentFragment instanceof AllMessageFragment && ((AllMessageFragment) mCurrentFragment).isListScrolled()) {
            ((AllMessageFragment) mCurrentFragment).scrollToTopAndRefresh();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requireContext().unregisterReceiver(mJoinGroupCallStatusReceiver);
    }
}
