package com.proapp.sompom.newui;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.proapp.sompom.intent.EditMediaFileIntent;
import com.proapp.sompom.newui.fragment.EditMediaFileFragment;
import com.proapp.sompom.model.LifeStream;

import timber.log.Timber;

public class EditMediaFileActivity extends AbsDefaultActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EditMediaFileIntent intent = new EditMediaFileIntent(getIntent());
        setFragment(EditMediaFileFragment.newInstance(intent.getLifeStream(),
                intent.getPosition(),
                intent.isEdit()),
                EditMediaFileFragment.TAG);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getFragment(EditMediaFileFragment.TAG);
        if (fragment != null && fragment instanceof EditMediaFileFragment) {
            onSetResult(((EditMediaFileFragment) fragment).getLifeStream());
        }
        super.onBackPressed();
    }

    public void onSetResult(LifeStream lifeStream) {
        Timber.e("onSetResult: %s", new Gson().toJson(lifeStream));
        EditMediaFileIntent intent = new EditMediaFileIntent(lifeStream);
        setResult(RESULT_OK, intent);
    }
}
