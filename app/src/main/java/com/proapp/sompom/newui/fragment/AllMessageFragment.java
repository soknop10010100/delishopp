package com.proapp.sompom.newui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.AllMessageAdapter;
import com.proapp.sompom.broadcast.NetworkBroadcastReceiver;
import com.proapp.sompom.chat.AbsChatBinder;
import com.proapp.sompom.chat.listener.ChannelListener;
import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.databinding.FragmentAllMessageBinding;
import com.proapp.sompom.helper.ConversationHelper;
import com.proapp.sompom.helper.TaskDebouncer;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.listener.ConversationDataAdaptive;
import com.proapp.sompom.listener.OnCallbackListListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnHomeMenuClick;
import com.proapp.sompom.listener.OnMessageItemClick;
import com.proapp.sompom.model.emun.PostContextMenuItemType;
import com.proapp.sompom.model.emun.SegmentedControlItem;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.ChatActivity;
import com.proapp.sompom.newui.dialog.PostContextMenuDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.AllMessageDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.newviewmodel.AllMessageFragmentViewModel;
import com.proapp.sompom.widget.LoadMoreLinearLayoutManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class AllMessageFragment extends AbsBindingFragment<FragmentAllMessageBinding>
        implements OnMessageItemClick,
        AllMessageFragmentViewModel.OnCallback,
        AbsBaseActivity.OnServiceListener,
        ChannelListener,
        OnHomeMenuClick,
        NetworkBroadcastReceiver.NetworkListener {

    private static final int REQUEST_UNREAD_MESSAGE_COUNT_DELAY = 5000;
    private static final int SILENT_REQUEST_REFRESH_CONVERSATION_DELAY = 5000;

    @Inject
    public ApiService mApiService;
    private AllMessageAdapter mAllMessageAdapter;

    private SocketService.SocketBinder mSocketBinder;
    private LoadMoreLinearLayoutManager mLayoutManager;
    private AllMessageFragmentViewModel mViewModel;
    private SegmentedControlItem mItemType = SegmentedControlItem.All;
    private boolean mIsShow;
    private boolean mHasScrolledDown;
    private BroadcastReceiver mUpdateReadConversationReceiver;
    private BroadcastReceiver mRefreshConversationListReceiver;
    private BroadcastReceiver mConversationBadgeUpdateToZeroReceiver;
    private String mCurrentUserId;
    private boolean mIsDisconnectedBefore = false;
    private TaskDebouncer<Conversation> mRequestConversationDetailTaskDebouncer;
    private TaskDebouncer<Conversation> mUpdateConversationToReadTaskDebouncer;

    public static AllMessageFragment newInstance(SegmentedControlItem itemType) {
        Bundle bundle = new Bundle();
        bundle.putInt(SharedPrefUtils.ID, itemType.getId());
        AllMessageFragment fragment = new AllMessageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_all_message;
    }

    @Override
    protected boolean enableGroupUpdateCallback() {
        return true;
    }

    @Override
    public void onGroupUpdated(String conversationId, Conversation conversation, boolean isRemovedMe) {
        Timber.i("onGroupUpdated. isRemovedMe: " + isRemovedMe);
        if (isRemovedMe) {
            if (mAllMessageAdapter != null) {
                mAllMessageAdapter.removeConversationById(conversationId);
            }
        } else {
            if (mAllMessageAdapter != null) {
                mAllMessageAdapter.updateOrCreateConversation(conversation);
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        registerConversationBadgeUpdateToZeroReceiver();
        registerUpdateReadConversationReceiver();
        registerRefreshConversationListReceiver();
        registerNetworkChangeStatusCallback();
        mCurrentUserId = SharedPrefUtils.getUserId(requireContext());

        if (getArguments() != null) {
            int id = getArguments().getInt(SharedPrefUtils.ID);
            mItemType = SegmentedControlItem.getSegmentedControlItem(id);
        }
        if (mItemType == SegmentedControlItem.All) {
            initView();
        }
    }


    private void registerNetworkChangeStatusCallback() {
        if (requireActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) requireActivity()).addNetworkStateChangeListener(this);
        }
    }

    @Override
    public void onNetworkState(NetworkBroadcastReceiver.NetworkState networkState) {
        if (mViewModel != null) {
            if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                if (mIsDisconnectedBefore) {
                    mIsDisconnectedBefore = false;
                    // Silent refresh messages when user reconnected to the internet
                    new Handler().postDelayed(() -> mViewModel.silentRefreshMessage(),
                            SILENT_REQUEST_REFRESH_CONVERSATION_DELAY);
                }
            } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                mIsDisconnectedBefore = true;
            }
        }
    }

    public void updateJoinGroupCallStatus(String conversationId, boolean isJoinAvailable, boolean isVideo) {
        if (mAllMessageAdapter != null) {
            Timber.i("onReceive: JoinGroupCallStatusReceiver: conversationId: " + conversationId +
                    ", isJoinAvailable: " + isJoinAvailable
                    + ", isVideo: " + isVideo);
            mAllMessageAdapter.updateJoinGroupCallStatus(conversationId,
                    isJoinAvailable,
                    isVideo);
        }
    }

    private void registerUpdateReadConversationReceiver() {
        mUpdateReadConversationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive UpdateReadConversationReceiver");
                String conversationId = intent.getStringExtra(ChatActivity.CONVERSATION_ID);
                if (!TextUtils.isEmpty(conversationId) &&
                        mAllMessageAdapter != null) {
                    if (mUpdateConversationToReadTaskDebouncer == null) {
                        mUpdateConversationToReadTaskDebouncer = new TaskDebouncer<>(2000, items -> {
                            if (isFragmentStillInValidState()) {
                                for (Map.Entry<String, Conversation> entry : items.entrySet()) {
                                    if (entry.getValue() != null) {
                                        Timber.i("Will update conversation : " + entry.getValue().getId() + " to read.");
                                        mAllMessageAdapter.updateReadConversation(entry.getValue().getId());
                                    }
                                }
                            }
                        });
                    }
                    mUpdateConversationToReadTaskDebouncer.debounce(conversationId, mAllMessageAdapter.getConversationById(conversationId));
                }
            }
        };

        requireActivity().registerReceiver(mUpdateReadConversationReceiver,
                new IntentFilter(ChatActivity.UPDATE_READ_CONVERSATION_STATUS_ACTION));
    }

    private void registerConversationBadgeUpdateToZeroReceiver() {
        mConversationBadgeUpdateToZeroReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                Timber.i("onReceive registerConversationBadgeUpdateToZeroReceiver");
                if (mAllMessageAdapter != null) {
                    mAllMessageAdapter.checkToRemoveUnreadConversation();
                }
            }
        };

        requireActivity().registerReceiver(mConversationBadgeUpdateToZeroReceiver,
                new IntentFilter(HomeFragment.ON_CONVERSATION_BADGE_UPDATE_TO_ZERO_EVENT));
    }

    private void registerRefreshConversationListReceiver() {
        mRefreshConversationListReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive mRefreshConversationListReceiver");
                if (mViewModel != null) {
                    mViewModel.onRefresh();
                }
            }
        };

        requireActivity().registerReceiver(mRefreshConversationListReceiver,
                new IntentFilter(ConversationHelper.REFRESH_CONVERSATION_LIST_ACTION));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAllMessageAdapter != null) {
            mAllMessageAdapter.unRegisterReceiver();
        }
        requireActivity().unregisterReceiver(mUpdateReadConversationReceiver);
        requireActivity().unregisterReceiver(mRefreshConversationListReceiver);
        requireActivity().unregisterReceiver(mConversationBadgeUpdateToZeroReceiver);
        if (requireActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) requireActivity()).removeStateChangeListener(this);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mSocketBinder != null) {
            mSocketBinder.removeChannelListener(this);
        }
    }

    @Override
    public void onConversationClick(Conversation conversation, int newBadgeCount) {
//        Timber.i("onConversationClick: newBadgeCount: " + newBadgeCount + ", data: " + new Gson().toJson(conversation));
       /*
        Manage to minis conversation badge count at run time when click to open conversation that
        has some new messages.
        */
        if (newBadgeCount > 0) {
            mSocketBinder.getChatSocket().invokeIncreaseMessageBadge(-newBadgeCount);
        }
        User recipient = conversation.getOneToOneRecipient(requireContext());

        if ((recipient != null && !TextUtils.isEmpty(recipient.getId())) || conversation.isGroup()) {
            ChatIntent chatIntent = new ChatIntent(requireActivity(), conversation);
            chatIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(chatIntent);
        }
    }

    @Override
    public void onConversationLongClick(Conversation conversation) {
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        dialog.addItem(PostContextMenuItemType.getConversationPopupMenuItem(requireContext()));
        dialog.setOnItemClickListener(result -> {
            dialog.dismiss();
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.ARCHIVE_CONVERSATION) {
                mSocketBinder.archivedConversation(conversation.getId());
            }

        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onConversationUserClick(User user) {
        ChatIntent chatIntent = new ChatIntent(requireActivity(), user);
        startActivity(chatIntent);
    }

    @Override
    public void getGetRecentMessageSuccess(List<Conversation> chatMessages, boolean isCanLoadMore) {
//        Timber.e("getGetRecentMessageSuccess: " + mItemType +
//                ", isVisible(): " + isVisible() +
//                ", !requireActivity().isFinishing(): " + !requireActivity().isFinishing() +
//                ", chatMessages: " + chatMessages.size() +
//                ", isCanLoadMore: " + isCanLoadMore);
        if (isFragmentStillInValidState()) {
            setAdapter(chatMessages, isCanLoadMore);
        }
    }

    @Override
    public boolean isHasData() {
        return mAllMessageAdapter != null && mAllMessageAdapter.getItemCount() > 0;
    }

    @Override
    public void onServiceReady(AbsChatBinder binder) {
        mSocketBinder = (SocketService.SocketBinder) binder;
        mSocketBinder.addChannelListener(this);
        Timber.i("Socket ready");
    }

    @Override
    public void onChannelCreateOrUpdate(Conversation conversation) {
        if (!isFragmentStillInValidState() || mAllMessageAdapter == null) {
            return;
        }

        final boolean shouldRequestUnreadMessageCounter = shouldRequestUnreadMessageCounter(conversation);

        /*
            Check to update the unread counter of unread counter manually on local when conversation
            update to new last message before requesting the update unread counter from server and update.
         */
        if (shouldRequestUnreadMessageCounter &&
                conversation.getLastMessage() != null &&
                !TextUtils.equals(conversation.getLastMessage().getSenderId(), mCurrentUserId) &&
                mAllMessageAdapter != null) {
            int existUnreadMessageCounter = mAllMessageAdapter.getExistUnreadMessageCounter(conversation.getId());
            existUnreadMessageCounter++;
            conversation.setUnreadCount(existUnreadMessageCounter);
        }

        Timber.i("onChannelCreateOrUpdate: " + conversation.getContent() +
                ", getUnreadCount: " + conversation.getUnreadCount() +
                ", shouldRequestUnreadMessageCounter: " + shouldRequestUnreadMessageCounter);

        requireActivity().runOnUiThread(() -> {
            if (mAllMessageAdapter == null) {
                if (mViewModel.mIsLoading.get()) {
                    return;
                } else if (mViewModel.mIsError.get()) {
                    setAdapter(new ArrayList<>(), false);
                }
            }

//            Timber.e("Updating chat: " + new Gson().toJson(channel));
            mAllMessageAdapter.updateOrCreateConversation(conversation);
            int currentVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
            if (currentVisibleItem > -1 && currentVisibleItem < mAllMessageAdapter.getItemCount()) {
                getBinding().recyclerViewRecentMessage.scrollToPosition(0);
            }

            if (shouldRequestUnreadMessageCounter) {
                if (mRequestConversationDetailTaskDebouncer == null) {
                    mRequestConversationDetailTaskDebouncer = new TaskDebouncer<>(REQUEST_UNREAD_MESSAGE_COUNT_DELAY, items -> {
                        Timber.i("onDebounce: " + items.size());
                        new Handler().postDelayed(() -> {
                            if (mViewModel != null && isFragmentStillInValidState()) {
                                for (Map.Entry<String, Conversation> entry : items.entrySet()) {
                                    if (entry.getValue() != null) {
                                        Timber.i("Will request conversation detail of " + entry.getValue().getId());
                                        mViewModel.getConversationDetail(entry.getValue().getId(),
                                                new OnCallbackListener<Response<Conversation>>() {
                                                    @Override
                                                    public void onFail(ErrorThrowable ex) {
                                                        Timber.e(ex);
                                                    }

                                                    @Override
                                                    public void onComplete(Response<Conversation> result) {
                                                        if (result.isSuccessful() && result.body() != null &&
                                                                mAllMessageAdapter != null &&
                                                                isFragmentStillInValidState()) {
                                                            mAllMessageAdapter.updateUnreadCount(result.body());
                                                        }
                                                    }
                                                });
                                    }
                                }
                            }
                        }, 2000);
                    });
                }
                mRequestConversationDetailTaskDebouncer.debounce(conversation.getId(), conversation);
            }
        });
    }

    private boolean shouldRequestUnreadMessageCounter(Conversation conversation) {
        boolean shouldRequestUnreadMessageCounter = false;
        List<ConversationDataAdaptive> dd = mAllMessageAdapter.getDatas();
        for (int i = 0; i < dd.size(); i++) {
            if (dd.get(i) instanceof Conversation) {
                Conversation existConversation = (Conversation) dd.get(i);
                if (existConversation == null) {
                    continue;
                }
                if (TextUtils.equals(existConversation.getId(), conversation.getId())) {
                    if (existConversation.getLastMessage() != null && conversation.getLastMessage() != null) {
                        if (existConversation.getLastMessage().getId() != null
                                && conversation.getLastMessage().getId() != null
                                && !existConversation.getLastMessage().getId().equals(conversation.getLastMessage().getId())) {
                            shouldRequestUnreadMessageCounter = true;
                        }
                    }
                    break;
                }
            }
        }

        return shouldRequestUnreadMessageCounter;
    }

    @Override
    public void onChannelRemoved(String channelId) {
        requireActivity().runOnUiThread(() -> {
            if (mAllMessageAdapter == null) {
                return;
            }
            mAllMessageAdapter.removeConversation(channelId);
        });
    }

    @Override
    public SegmentedControlItem getCurrentSegmentedControlItem() {
        return mItemType;
    }

    @Override
    public void onTabMenuClick() {
        if (mIsShow) {
            return;
        }
        initView();
    }

    private void setAdapter(List<Conversation> chatMessages, boolean isCanLoadMore) {
        Timber.i("setAdapter size " + chatMessages.size() + " isCanLoadMore " + isCanLoadMore);
        if (mAllMessageAdapter != null &&
                getBinding().recyclerViewRecentMessage.getAdapter() != null &&
                getBinding().recyclerViewRecentMessage.getAdapter() instanceof AllMessageAdapter) {
            Timber.i("Pull and refresh.");
            if (mLayoutManager != null) {
                mLayoutManager.loadingFinished();
                mLayoutManager.setShouldDetectLoadMore(isCanLoadMore);
                if (isCanLoadMore) {
                    mLayoutManager.setLoadMoreLinearLayoutManagerListener(instantiateLoadMoreListener());
                } else {
                    mLayoutManager.setLoadMoreLinearLayoutManagerListener(null);
                }
            } else {
                Timber.e("LayoutManager is null");
            }
            mAllMessageAdapter.setCanLoadMore(isCanLoadMore);
            mAllMessageAdapter.refreshData(getBinding().recyclerViewRecentMessage,
                    new ArrayList<>(chatMessages),
                    (mItemType == SegmentedControlItem.Buying));
            if (isCanLoadMore) {
                getBinding().recyclerViewRecentMessage.post(() ->
                        mLayoutManager.checkLToInvokeReachLoadMoreByDefaultIfNecessary());
            }

            return;
        }

        //First setup adapter list
        mAllMessageAdapter = new AllMessageAdapter(requireActivity(),
                new ArrayList<>(chatMessages),
                AllMessageFragment.this,
                mItemType,
                () -> mViewModel.getDefaultRecentConversationDisplaySize());
        mAllMessageAdapter.setCanLoadMore(isCanLoadMore);
        mLayoutManager = new LoadMoreLinearLayoutManager(requireActivity(),
                getBinding().recyclerViewRecentMessage,
                false);
        if (isCanLoadMore) {
            getBinding().recyclerViewRecentMessage.postDelayed(() -> mLayoutManager.checkLToInvokeReachLoadMoreByDefaultIfNecessary(), 100);
        }
        getBinding().recyclerViewRecentMessage.setLayoutManager(setupLayoutManager(isCanLoadMore));
        getBinding().recyclerViewRecentMessage.setAdapter(mAllMessageAdapter);
    }

    private LoadMoreLinearLayoutManager setupLayoutManager(boolean isCanLoadMore) {
        mLayoutManager = new LoadMoreLinearLayoutManager(requireActivity(),
                getBinding().recyclerViewRecentMessage);
        mLayoutManager.setShouldDetectLoadMore(isCanLoadMore);
        mLayoutManager.setLoadMoreLinearLayoutManagerListener(instantiateLoadMoreListener());
        return mLayoutManager;
    }

    private LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener instantiateLoadMoreListener() {
        return new LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener() {
            @Override
            public void onReachedLoadMoreBottom() {
                Timber.i("onReachedLoadMoreBottom");
                performLoadMoreItem(true);
            }

            @Override
            public void onReachedLoadMoreByDefault() {
                Timber.i("onReachedLoadMoreByDefault");
                performLoadMoreItem(false);
            }
        };
    }

    private void performLoadMoreItem(boolean isReachLoadMoreByDefault) {
        getBinding().recyclerViewRecentMessage.stopScroll();
        mViewModel.loadMore(new OnCallbackListListener<List<Conversation>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (mAllMessageAdapter == null) {
                    return;
                }
                mAllMessageAdapter.setCanLoadMore(false);
                mAllMessageAdapter.performNotifyDataSetChanged(getBinding().recyclerViewRecentMessage);
                mLayoutManager.loadingFinished();
                mLayoutManager.setLoadMoreLinearLayoutManagerListener(null);
            }

            @Override
            public void onComplete(List<Conversation> result, boolean canLoadMore) {
                if (mAllMessageAdapter == null) {
                    return;
                }
                boolean canLoadMoreBefore = mAllMessageAdapter.canLoadMore();
                mAllMessageAdapter.addLoadMoreData(new ArrayList<>(result));
                mAllMessageAdapter.setCanLoadMore(canLoadMore);
                mLayoutManager.loadingFinished();
                if (!canLoadMore) {
                    mLayoutManager.setLoadMoreLinearLayoutManagerListener(null);
                }

                //Check to remove load more layout if it still exist when there is no data to load more.
                if (!canLoadMore && result.isEmpty() && canLoadMoreBefore) {
                    Timber.i("Remove previous load more layout.");
                    mAllMessageAdapter.removeLoadMoreLayout();
                }

                if (isReachLoadMoreByDefault && canLoadMore) {
                    getBinding().recyclerViewRecentMessage.post(() ->
                            mLayoutManager.checkLToInvokeReachLoadMoreByDefaultIfNecessary());
                }
            }
        });
    }

    private void initView() {
        mIsShow = true;

        if (mSocketBinder == null) {
            if (requireActivity() instanceof AbsBaseActivity) {
                ((AbsBaseActivity) requireActivity()).addOnServiceListener(this);
            }
        }

        if (getBinding().recyclerViewRecentMessage.getItemAnimator() != null) {
            ((SimpleItemAnimator) getBinding().recyclerViewRecentMessage.getItemAnimator()).setSupportsChangeAnimations(false);
        }

        AllMessageDataManager dataManager = new AllMessageDataManager(requireActivity(),
                mApiService,
                mItemType,
                channelIds -> {
                    if (mSocketBinder != null) {
                        mSocketBinder.startSubscribeAllGroupChannel(channelIds);
                    }
                });
        mViewModel = new AllMessageFragmentViewModel(dataManager, this);
        setVariable(BR.viewModel, mViewModel);

        getBinding().recyclerViewRecentMessage.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                mHasScrolledDown = dy > 0;
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        //Add delay duration to make sure the transition screen finish before start requesting data
        new Handler().postDelayed(() -> mViewModel.getRecentlyMessageFromDb(),
                requireActivity().getResources().getInteger(R.integer.transition_anim_duration));
    }

    @Override
    public int existItemCount() {
        if (mAllMessageAdapter != null) {
            return mAllMessageAdapter.getExistConversationCount();
        }

        return 0;
    }

    public boolean isListScrolled() {
        if (mLayoutManager != null) {
            if (mLayoutManager.findFirstVisibleItemPosition() != 0) {
                return true;
            } else {
                return mHasScrolledDown;
            }
        }

        return false;
    }

    public void reloadRecentlyMessage() {
        getBinding().recyclerViewRecentMessage.smoothScrollToPosition(0);
        mViewModel.reloadRecentlyMessage();
    }

    public void scrollToTopAndRefresh() {
        getBinding().recyclerViewRecentMessage.smoothScrollToPosition(0);
        mViewModel.onRefresh();
    }
}
