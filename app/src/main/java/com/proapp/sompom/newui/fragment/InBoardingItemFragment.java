package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentInBoardingItemBinding;
import com.proapp.sompom.model.InBoardItem;
import com.proapp.sompom.viewmodel.InBoardingItemViewModel;

public class InBoardingItemFragment extends AbsBindingFragment<FragmentInBoardingItemBinding> {

    private static final String DATA = "DATA";

    public static InBoardingItemFragment newInstance(InBoardItem item) {

        Bundle args = new Bundle();
        args.putParcelable(DATA, item);

        InBoardingItemFragment fragment = new InBoardingItemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_in_boarding_item;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            InBoardItem item = getArguments().getParcelable(DATA);
            if (item != null) {
                InBoardingItemViewModel viewModel = new InBoardingItemViewModel(item);
                setVariable(BR.viewModel, viewModel);
            }
        }
    }
}
