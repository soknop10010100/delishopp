package com.proapp.sompom.newui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.ConciergeMyAddressAdapter;
import com.proapp.sompom.databinding.FragmentConciergeMyAddressBinding;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.intent.newintent.ConciergeCurrentLocationIntent;
import com.proapp.sompom.intent.newintent.ConciergeMyAddressIntent;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.emun.CitySelectionType;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.ConciergeMyAddressActivity;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeMyAddressDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeMyAddressFragmentViewModel;
import com.proapp.sompom.widget.LoadMoreLinearLayoutManager;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 22/11/21.
 */
public class ConciergeMyAddressFragment extends AbsBindingFragment<FragmentConciergeMyAddressBinding>
        implements ConciergeMyAddressFragmentViewModel.ConciergeMyAddressFragmentViewModelListener {

    @Inject
    public ApiService mApiService;

    private Context mContext;
    private ConciergeMyAddressAdapter mAdapter;
    private ConciergeMyAddressDataManager mDataManager;
    private ConciergeMyAddressFragmentViewModel mViewModel;
    private LoadMoreLinearLayoutManager mLayoutManager;
    private boolean mIsFromCheckout;
    private String mPreviousSelectedAddressId;
    private ConciergeUserAddress mUpdatePreviousSelectedAddress;
    private LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener mLoadMoreLinearLayoutManagerListener;
    private boolean mIsPreviousAddressDeleted;
    private CitySelectionType mAddressTypeToShow;

    public static ConciergeMyAddressFragment newInstance(boolean isFromCheckOut,
                                                         CitySelectionType addressType,
                                                         String previousSelectedAddressId) {

        Bundle args = new Bundle();
        args.putBoolean(ConciergeMyAddressIntent.IS_FROM_CHECKOUT_MODE, isFromCheckOut);
        args.putSerializable(ConciergeMyAddressIntent.ADDRESS_TYPE_TO_SHOW, addressType);
        args.putString(ConciergeMyAddressIntent.PREVIOUS_SELECTED_ADDRESS_ID, previousSelectedAddressId);

        ConciergeMyAddressFragment fragment = new ConciergeMyAddressFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_DELIVERY_ADDRESS_LIST;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_my_address;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mUpdatePreviousSelectedAddress = null;
        mContext = requireActivity();
        ((ConciergeMyAddressActivity) requireActivity()).setListener(() -> {
            if (mIsPreviousAddressDeleted) {
                ConciergeUserAddress currentPrimaryAddress = mAdapter.findCurrentPrimaryAddress();
                if (currentPrimaryAddress != null) {
                    performOnItemClicked(currentPrimaryAddress);
                } else {
                    requireActivity().finish();
                }
            } else if (mUpdatePreviousSelectedAddress != null) {
                performOnItemClicked(mUpdatePreviousSelectedAddress);
            } else {
                requireActivity().finish();
            }
        });

        getBinding().layoutToolbar.toolbarTitle.setText(R.string.shop_address_list_screen_title);
        setToolbar(getBinding().layoutToolbar.toolbar, true);
        mIsFromCheckout = getArguments().getBoolean(ConciergeMyAddressIntent.IS_FROM_CHECKOUT_MODE, false);
        mPreviousSelectedAddressId = getArguments().getString(ConciergeMyAddressIntent.PREVIOUS_SELECTED_ADDRESS_ID);
        mAddressTypeToShow = (CitySelectionType) getArguments().getSerializable(ConciergeMyAddressIntent.ADDRESS_TYPE_TO_SHOW);

        mDataManager = new ConciergeMyAddressDataManager(mContext, mApiService);
        mViewModel = new ConciergeMyAddressFragmentViewModel(mContext,
                mAddressTypeToShow,
                mDataManager,
                this);


        setVariable(BR.viewModel, mViewModel);

        mViewModel.loadUserAddresses(false, false);
    }

    @Override
    public void onLoadUserAddress(List<ConciergeUserAddress> addresses, boolean isFromLoadMore, boolean isCanLoadMore) {
        setData(addresses, isFromLoadMore, isCanLoadMore);
    }

    private void setData(List<ConciergeUserAddress> addresses, boolean isFromLoadMore, boolean isCanLoadMore) {
//        Timber.i("setData: " + addresses.size() + ", isFromLoadMore: " + isFromLoadMore + ", isCanLoadMore: " + isCanLoadMore);
        if (mAdapter == null) {
            mAdapter = new ConciergeMyAddressAdapter(addresses,
                    new ConciergeMyAddressAdapter.ConciergeMyAddressListener() {
                        @Override
                        public void onAllItemRemoved() {
                            if (mViewModel != null) {
                                mViewModel.showEmptyView(true);
                            }
                        }

                        @Override
                        public void onItemClicked(ConciergeUserAddress address) {
                            if (mIsFromCheckout && MainApplication.isInExpressMode()) {
                                if (ConciergeHelper.isValidAddressForExpressOrder(address)) {
                                    performOnItemClicked(address);
                                } else {
                                    ConciergeHelper.showCheckoutErrorPopup((AppCompatActivity) requireActivity(),
                                            getString(R.string.shop_checkout_alert_selected_address_out_of_range),
                                            null);
                                }
                            } else {
                                performOnItemClicked(address);
                            }
                        }

                        @Override
                        public void onAddressUpdated(ConciergeUserAddress address) {
                            if (TextUtils.equals(mPreviousSelectedAddressId, address.getId())) {
                                mUpdatePreviousSelectedAddress = address;
                            }
                        }

                        @Override
                        public void onAddressDeleted(ConciergeUserAddress address) {
                            if (TextUtils.equals(mPreviousSelectedAddressId, address.getId())) {
                                mIsPreviousAddressDeleted = true;
                            }
                        }
                    });
            mLoadMoreLinearLayoutManagerListener = new LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener() {
                @Override
                public void onReachedLoadMoreBottom() {
                    Timber.i("onReachedLoadMoreBottom");
                    mViewModel.loadMore();
                }

                @Override
                public void onReachedLoadMoreByDefault() {
                    Timber.i("onReachedLoadMoreByDefault");
                    mViewModel.loadMore();
                }
            };
            mAdapter.setCanLoadMore(isCanLoadMore);
            mLayoutManager = new LoadMoreLinearLayoutManager(requireActivity(), getBinding().recyclerView);
            mLayoutManager.setLoadMoreLinearLayoutManagerListener(isCanLoadMore ? mLoadMoreLinearLayoutManagerListener : null);
            getBinding().recyclerView.setLayoutManager(mLayoutManager);
            getBinding().recyclerView.setAdapter(mAdapter);
            getBinding().recyclerView.post(() -> mLayoutManager.checkLToInvokeReachLoadMoreByDefaultIfNecessary());
        } else {
            mLayoutManager.setLoadMoreLinearLayoutManagerListener(isCanLoadMore ? mLoadMoreLinearLayoutManagerListener : null);
            if (getBinding().recyclerView.getAdapter() == null) {
                getBinding().recyclerView.setLayoutManager(mLayoutManager);
                getBinding().recyclerView.setAdapter(mAdapter);
                getBinding().recyclerView.post(() -> mLayoutManager.checkLToInvokeReachLoadMoreByDefaultIfNecessary());
            }

            mAdapter.setCanLoadMore(isCanLoadMore);
            if (!isFromLoadMore) {
                mAdapter.setData(addresses);
                getBinding().recyclerView.post(() -> mLayoutManager.checkLToInvokeReachLoadMoreByDefaultIfNecessary());
            } else {
                mAdapter.addLoadMoreData(addresses);
            }
            mLayoutManager.loadingFinished();
        }
    }

    @Override
    public void onNewAddressAdded(ConciergeUserAddress newAddress) {
        if (mAdapter != null) {
            mAdapter.onNewAddressAdded(newAddress);
        }
    }

    @Override
    public int getAddressCount() {
        if (mAdapter != null) {
            return mAdapter.getItemCount();
        }

        return 0;
    }

    private void performOnItemClicked(ConciergeUserAddress address) {
        if (mIsFromCheckout) {
            Intent intent = new Intent();
            address.setHasUpdateProperty(mUpdatePreviousSelectedAddress != null);
            intent.putExtra(ConciergeCurrentLocationIntent.USER_CURRENT_ADDRESS, address);
            ((AbsBaseActivity) mContext).setResult(AppCompatActivity.RESULT_OK, intent);
            ((AbsBaseActivity) mContext).finish();
        }
    }
}
