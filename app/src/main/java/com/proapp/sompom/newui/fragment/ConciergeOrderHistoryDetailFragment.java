package com.proapp.sompom.newui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ConciergeOrderHistoryDetailItemAdapter;
import com.proapp.sompom.databinding.FragmentConciergeOrderHistoryDetailBinding;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.intent.ConciergeOrderHistoryDetailIntent;
import com.proapp.sompom.intent.ConciergeReviewCheckoutIntent;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeOrderHistoryDetailManager;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeOrderHistoryDetailFragmentViewModel;
import com.sompom.baseactivity.ResultCallback;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 22/9/21.
 */
public class ConciergeOrderHistoryDetailFragment extends AbsHandleConciergeBasketErrorFragment<FragmentConciergeOrderHistoryDetailBinding>
        implements ConciergeOrderHistoryDetailFragmentViewModel.ConciergeOrderHistoryDetailFragmentViewModelListener {

    private ConciergeOrderHistoryDetailFragmentViewModel mViewModel;
    @Inject
    public ApiService mApiService;
    private BroadcastReceiver mOrderStatusUpdateReceiver;

    public static ConciergeOrderHistoryDetailFragment newInstance(String orderId,
                                                                  String orderNumber) {

        Bundle args = new Bundle();
        args.putString(ConciergeOrderHistoryDetailIntent.ORDER_ID, orderId);
        args.putString(ConciergeOrderHistoryDetailIntent.ORDER_NUMBER, orderNumber);

        ConciergeOrderHistoryDetailFragment fragment = new ConciergeOrderHistoryDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_order_history_detail;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requireContext().unregisterReceiver(mOrderStatusUpdateReceiver);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Timber.i("onViewCreated");
        getControllerComponent().inject(this);
        initOrderStatusUpdateReceiver();
        setToolbar(getBinding().layoutToolbar.toolbar, true);
        String orderNumber = getArguments().getString(ConciergeOrderHistoryDetailIntent.ORDER_NUMBER);
        if (getArguments() != null &&
                !TextUtils.isEmpty(orderNumber)) {
            getBinding().layoutToolbar.toolbarTitle.setText(getString(R.string.shop_order_detail_screen_title,
                    "#" + getArguments().getString(ConciergeOrderHistoryDetailIntent.ORDER_NUMBER)));
        } else {
            getBinding().layoutToolbar.toolbarTitle.setText("");
        }
        initViewModel(false);
    }

    private void initViewModel(boolean justShowLoading) {
        mViewModel = new ConciergeOrderHistoryDetailFragmentViewModel(requireContext(),
                new ConciergeOrderHistoryDetailManager(requireContext(),
                        mApiService,
                        getArguments().getString(ConciergeOrderHistoryDetailIntent.ORDER_ID)),
                this);
        setVariable(BR.viewModel, mViewModel);
        if (justShowLoading) {
            mViewModel.showLoading();
        } else {
            mViewModel.requestData();
        }
    }

    private void initOrderStatusUpdateReceiver() {
        mOrderStatusUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive orderStatusUpdateEvent");
                ConciergeOrder updatedOrder = intent.getParcelableExtra(ConciergeHelper.UPDATE_ORDER_STATUS_EVENT_DATA);

                if (mViewModel != null) {
                    mViewModel.updateOrderStatus(updatedOrder);
                }
            }
        };
        requireContext().registerReceiver(mOrderStatusUpdateReceiver,
                new IntentFilter(ConciergeHelper.UPDATE_ORDER_STATUS_EVENT));
    }

    @Override
    public void onLoadDataSuccess(ConciergeOrder data) {
        getBinding().layoutToolbar.toolbarTitle.setText(getString(R.string.shop_order_detail_screen_title,
                "#" + data.getOrderNumber()));
        ConciergeOrderHistoryDetailItemAdapter adapter = new ConciergeOrderHistoryDetailItemAdapter(data.getOrderItems());
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        getBinding().recyclerView.setAdapter(adapter);
        mViewModel.hideLoading();
    }

    @Override
    public void onReorderSuccess() {
        if (requireActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) requireContext()).startActivityForResult(new ConciergeReviewCheckoutIntent(requireContext(),
                            true),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                        }
                    });
        }
    }

    @Override
    public void showOrderErrorPopup(String errorMessage, View.OnClickListener okClickListener) {
        super.showOrderErrorPopup(errorMessage, okClickListener);
    }
}
