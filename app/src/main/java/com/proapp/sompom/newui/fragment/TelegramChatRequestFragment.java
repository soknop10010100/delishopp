package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.TelegramChatRequestAdapter;
import com.proapp.sompom.databinding.FragmentTelegramChatRequsetBinding;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.TelegramChatRequest;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemTelegramChatRequestViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.TelegramChatRequestFragmentViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class TelegramChatRequestFragment extends AbsBindingFragment<FragmentTelegramChatRequsetBinding>
        implements ListItemTelegramChatRequestViewModel.Listener,
        TelegramChatRequestFragmentViewModel.Callback {

    @Inject
    public ApiService mApiService;
    private TelegramChatRequestAdapter mTelegramChatRequestAdapter;
    private TelegramChatRequestFragmentViewModel mViewModel;
    private Callback mTelegramChatRequestCallback;


    public static TelegramChatRequestFragment newInstance(Callback telegramChatRequestCallback) {
        TelegramChatRequestFragment fragment = new TelegramChatRequestFragment();
        fragment.mTelegramChatRequestCallback = telegramChatRequestCallback;
        return fragment;
    }


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_telegram_chat_requset;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mViewModel = new TelegramChatRequestFragmentViewModel(requireActivity(), mApiService, this);
        setVariable(BR.viewModel, mViewModel);
    }

    private void setAdapter(List<TelegramChatRequest> telegramChatRequest) {
        if (mTelegramChatRequestAdapter == null) {
            mTelegramChatRequestAdapter = new TelegramChatRequestAdapter(requireActivity(), new ArrayList<>(telegramChatRequest), this);
            getBinding().recyclerViewChatRequest.setLayoutManager(new LinearLayoutManager(getContext()));
            getBinding().recyclerViewChatRequest.setAdapter(mTelegramChatRequestAdapter);
        } else {
            mTelegramChatRequestAdapter.refreshData(new ArrayList<>(telegramChatRequest));
        }
    }

    @Override
    public void onGetSuccess(List<TelegramChatRequest> result, boolean isRefresh) {
        setAdapter(result);
    }

    @Override
    public void onAcceptRequestSuccess(Conversation conversation) {
        mTelegramChatRequestCallback.onAcceptRequestSuccess(conversation);
    }

    @Override
    public void onAcceptClick(String chatRequestId) {
    }

    @Override
    public void onRejectClick(String chatRequestId) {
    }

    public interface Callback {
        void onAcceptRequestSuccess(Conversation conversation);
    }
}
