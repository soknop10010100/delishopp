package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ViewPagerAdapter;
import com.proapp.sompom.databinding.FragmentGroupDetailMediaContainerBinding;
import com.proapp.sompom.intent.newintent.GroupDetailMediaContainerIntent;
import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.emun.GroupMediaDetailType;

/**
 * Created by Chhom Veasna on 7/24/2020.
 */
public class GroupDetailMediaContainerFragment extends AbsBindingFragment<FragmentGroupDetailMediaContainerBinding> {


    private ViewPagerAdapter mViewPagerAdapter;
    private GroupDetail mGroupDetail;

    public static GroupDetailMediaContainerFragment newInstance(GroupDetail groupDetail, int preSelectedIndex) {

        Bundle args = new Bundle();
        args.putParcelable(GroupDetailMediaContainerIntent.GROUP_DETAIL, groupDetail);
        args.putInt(GroupDetailMediaContainerIntent.INDEX, preSelectedIndex);

        GroupDetailMediaContainerFragment fragment = new GroupDetailMediaContainerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_group_detail_media_container;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            mGroupDetail = getArguments().getParcelable(GroupDetailMediaContainerIntent.GROUP_DETAIL);
            if (mGroupDetail != null) {
                getBinding().tabs.setIndicatorWidth((int) getResources().getDimension(R.dimen.tab_indicator_width));
                setToolbar(getBinding().layoutToolbar.toolbar, true);
                getBinding().layoutToolbar.toolbarTitle.setText(mGroupDetail.getName());
                initPagerAdapter(getArguments().getInt(GroupDetailMediaContainerIntent.INDEX, 0));
            }
        }
    }

    private void initPagerAdapter(final int preSelectedIndex) {
        if (mGroupDetail != null) {
            mViewPagerAdapter = new ViewPagerAdapter(requireActivity().getSupportFragmentManager());
            mViewPagerAdapter.addFragment(GroupDetailPhotoAndVideoFragment.newInstance(mGroupDetail.getId(),
                    GroupMediaDetailType.PHOTO_VIDEO),
                    getString(R.string.group_menu_photo));
            mViewPagerAdapter.addFragment(GroupDetailFileFragment.newInstance(mGroupDetail.getId(),
                    GroupMediaDetailType.FILE),
                    getString(R.string.group_menu_file));
            mViewPagerAdapter.addFragment(GroupDetailLinkFragment.newInstance(mGroupDetail.getId(),
                    GroupMediaDetailType.LINK),
                    getString(R.string.group_menu_link));
            mViewPagerAdapter.addFragment(GroupDetailVoiceFragment.newInstance(mGroupDetail.getId(),
                    GroupMediaDetailType.VOICE),
                    getString(R.string.group_menu_voice));
            getBinding().tabs.setupWithViewPager(getBinding().viewPager);
            getBinding().viewPager.setOffscreenPageLimit(mViewPagerAdapter.getCount());
            getBinding().viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    Fragment item = mViewPagerAdapter.getItem(position);
                    if (item instanceof AbsGroupDetailMediaFragment) {
                        ((AbsGroupDetailMediaFragment) item).onSelected();
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            getBinding().viewPager.setAdapter(mViewPagerAdapter);
            getBinding().viewPager.postDelayed(() -> {
                if (preSelectedIndex >= 0 && preSelectedIndex < mViewPagerAdapter.getCount()) {
                    getBinding().viewPager.setCurrentItem(preSelectedIndex, false);
                    //Force execute first default index
                    if (preSelectedIndex == 0) {
                        Fragment item = mViewPagerAdapter.getItem(0);
                        if (item instanceof AbsGroupDetailMediaFragment) {
                            ((AbsGroupDetailMediaFragment) item).onSelected();
                        }
                    }
                }
            }, 100);
        }
    }
}
