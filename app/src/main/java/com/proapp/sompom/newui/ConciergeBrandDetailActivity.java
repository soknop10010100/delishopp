package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.intent.ConciergeBrandDetailIntent;
import com.proapp.sompom.newui.fragment.ConciergeBrandDetailFragment;

/**
 * Created by Chhom Veasna on 5/4/22.
 */
public class ConciergeBrandDetailActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConciergeBrandDetailIntent intent = new ConciergeBrandDetailIntent(getIntent());
        setFragment(ConciergeBrandDetailFragment.newInstance(intent.getBrandId(), intent.getTitleParam(), intent.getOpenType()),
                ConciergeBrandDetailFragment.class.getName());
    }
}
