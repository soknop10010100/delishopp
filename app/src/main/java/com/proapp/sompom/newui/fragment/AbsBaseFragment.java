package com.proapp.sompom.newui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.UiThread;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.desmond.squarecamera.ui.dialog.EditPostPrivacyDialog;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.helper.PermissionHelper;
import com.proapp.sompom.injection.controller.ControllerComponent;
import com.proapp.sompom.injection.controller.ControllerModule;
import com.proapp.sompom.listener.OnCompleteListener;
import com.proapp.sompom.model.PostContextMenuItem;
import com.proapp.sompom.model.emun.PostContextMenuItemType;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.newui.dialog.PostContextMenuDialog;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.utils.SnackBarUtil;
import com.resourcemanager.helper.FontHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by He Rotha on 10/20/17.
 */

public abstract class AbsBaseFragment extends Fragment {

    public static final String TAG = AbsBaseFragment.class.getName();
    private ControllerComponent mControllerComponent;
    private BroadcastReceiver mUserLoginSuccessEventReceiver;

    // See this link regarding permission request:
    // https://developer.android.com/training/permissions/requesting#allow-system-manage-request-code

    // The ActivityResultLauncher *MUST* be initialized during the fragment or activity initialization.
    protected ActivityResultLauncher<String[]> mRequestPermissionsLauncher = registerForActivityResult(
            new ActivityResultContracts.RequestMultiplePermissions(), this::onRequestPermissionLauncherCallback);

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final String flurryLogEvent = getFlurryLogEventKey();
        if (!TextUtils.isEmpty(flurryLogEvent)) {
            FlurryHelper.logEvent(requireContext(), flurryLogEvent);
        }
        Timber.i("shouldListenUserLoginSuccessEven: " + shouldListenUserLoginSuccessEven());
        if (shouldListenUserLoginSuccessEven()) {
            initUserLoginSuccessEventReceiver();
        }
    }

    /**
     * Call this function to request package specific permissions for the current build/clone
     */
    protected void requestPackageSpecificPermissions() {
        PermissionHelper.requestPackageSpecificPermissions((AppCompatActivity) requireActivity(),
                mRequestPermissionsLauncher,
                new PermissionHelper.PermissionHelperCallback() {
                    @Override
                    public void onInitialPermissionDialogDenied() {
                        onRequestPermissionLauncherCallback(Collections.emptyMap());
                    }

                    @Override
                    public void onNoPermissionToRequest() {
                        onRequestPermissionLauncherCallback(Collections.emptyMap());
                    }
                });
    }

    /**
     * Call this function to request all possible permissions that the app requires
     */
    protected void requestAllAppRequirePermissions() {
        PermissionHelper.requestAllAppRequirePermissions((AppCompatActivity) requireActivity(),
                mRequestPermissionsLauncher,
                new PermissionHelper.PermissionHelperCallback() {
                    @Override
                    public void onInitialPermissionDialogDenied() {
                        onRequestPermissionLauncherCallback(Collections.emptyMap());
                    }

                    @Override
                    public void onNoPermissionToRequest() {
                        onRequestPermissionLauncherCallback(Collections.emptyMap());
                    }
                });
    }

    /**
     * Child class can override this if they wish to handle on permission request result
     */
    protected void onRequestPermissionLauncherCallback(Map<String, Boolean> permissionsResult) {
    }

    @UiThread
    public ControllerComponent getControllerComponent() {
        if (mControllerComponent == null || forceRebuildControllerComponent()) {
            mControllerComponent = ((MainApplication) requireActivity().getApplication())
                    .getApplicationComponent()
                    .newControllerComponent(new ControllerModule(getActivity()));
        }

        return mControllerComponent;
    }

    protected boolean shouldListenUserLoginSuccessEven() {
        return false;
    }

    public boolean isFragmentStillInValidState() {
        return isAdded() && !requireActivity().isFinishing();
    }

    protected boolean forceRebuildControllerComponent() {
        return false;
    }

    private void initUserLoginSuccessEventReceiver() {
        mUserLoginSuccessEventReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isFromSuccessfulAuthentication = intent.getBooleanExtra(ApplicationHelper.IS_FROM_AUTHENTICATION_SUCCESS, false);
                onRefreshScreen(isFromSuccessfulAuthentication);
            }
        };
        requireContext().registerReceiver(mUserLoginSuccessEventReceiver,
                new IntentFilter(ApplicationHelper.REFRESH_SCREEN_EVENT));
    }

    protected void onRefreshScreen(boolean isFromSuccessfulAuthentication) {
        //Client impl...
    }

    protected void showKeyboard(EditText editText) {
        editText.setFocusable(true);
        editText.setClickable(true);
        editText.requestFocus();
        KeyboardUtil.showKeyboard(getActivity(), editText);
    }

    protected void hideKeyboard() {
        KeyboardUtil.hideKeyboard(getActivity());
    }

    public void setToolbar(Toolbar toolbar, boolean isShowButtonBack) {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);
        if (isShowButtonBack) {
//            ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setDisplayHomeAsUpEnabled(true);
//                actionBar.setDisplayShowHomeEnabled(true);
//            }
//            toolbar.setNavigationIcon(AttributeConverter.convertAttrToDrawable(getActivity(), R.attr.iconBackPressSmall));
            View backButton = toolbar.findViewById(R.id.backButton);
            if (backButton != null) {
                backButton.setVisibility(View.VISIBLE);
                backButton.setOnClickListener(v -> requireActivity().onBackPressed());
            }
        }
    }

    public void showSnackBar(String text, boolean isError) {
        SnackBarUtil.showSnackBar(getView(), text, isError);
    }

    public void showSnackBar(@StringRes int message, boolean isError) {
        SnackBarUtil.showSnackBar(getView(), message, isError);
    }

    /**
     * for flurry log of screen
     *
     * @return if null, there is no log. otherwise, log text that super class overwrite
     */
    @Nullable
    protected String getFlurryLogEventKey() {
        return null;
    }

    public void showFragment(Fragment showFragment, Fragment hideFragment) {
        if (hideFragment == null || showFragment == null || hideFragment == showFragment) {
            return;
        }
        getChildFragmentManager()
                .beginTransaction()
                .hide(hideFragment)
                .show(showFragment)
                .commit();
        //make sure fragment know fragment is paused, or resumed
        hideFragment.onPause();
        showFragment.onResume();
    }

    public void showFragment(Fragment showFragment, Fragment hideFragment, boolean isBackNavigation) {
        if (hideFragment == null || showFragment == null || hideFragment == showFragment) {
            return;
        }
        FragmentTransaction fragmentTransaction = getChildFragmentManager()
                .beginTransaction();
        if (!isBackNavigation) {
            //Forward navigation
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_left_anim, R.anim.exit_to_right_anim);
        } else {
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right_anim, R.anim.exit_to_left_anim);
        }
        fragmentTransaction.hide(hideFragment)
                .show(showFragment)
                .commit();
        //make sure fragment know fragment is paused, or resumed
        hideFragment.onPause();
        showFragment.onResume();
    }

    public Fragment getFragment(String tag) {
        return getChildFragmentManager().findFragmentByTag(tag);
    }

    public void removeFragment(Fragment fragment) {
        if (fragment != null && fragment.isAdded()) {
            getChildFragmentManager().beginTransaction().remove(fragment).commit();
        }
    }

    public void setFragment(@IdRes int id, Fragment fragment, String tag) {
        if (!getActivity().isFinishing() && !isFragmentAlreadyReplaced(tag)) {
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(id, fragment, tag)
                    .commit();
        }
    }

    private boolean isFragmentAlreadyReplaced(String tag) {
        Fragment fragment = getChildFragmentManager().findFragmentByTag(tag);
        return fragment != null && fragment.isVisible();
    }

    public void showMessageDialog(@StringRes int title,
                                  @StringRes int description,
                                  @StringRes int ok,
                                  @StringRes int cancel,
                                  View.OnClickListener listener) {
        MessageDialog dialog = new MessageDialog();
        dialog.setTitle(getString(title));
        dialog.setMessage(getString(description));
        dialog.setLeftText(getString(ok), listener);
        dialog.setRightText(getString(cancel), null);
        dialog.show(getChildFragmentManager(), MessageDialog.TAG);
    }

    public void showMessageDialog(String title,
                                  String description,
                                  String ok,
                                  String cancel,
                                  View.OnClickListener listener) {
        MessageDialog dialog = new MessageDialog();
        dialog.setTitle(title);
        dialog.setMessage(description);
        dialog.setLeftText(ok, listener);
        dialog.setRightText(cancel, null);
        dialog.show(getChildFragmentManager(), MessageDialog.TAG);
    }

    public void showEditPrivacyDialog(OnCompleteListener<Integer> onClickListener) {
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        List<PostContextMenuItem> items = new ArrayList<>();
        items.add(new PostContextMenuItem(PostContextMenuItemType.FOLLOWER_CLICK.getId(),
                R.string.code_follower,
                R.string.post_menu_followers_title,
                -1,
                FontHelper.getFlatIconFontResource()));
        items.add(new PostContextMenuItem(PostContextMenuItemType.EVERYONE_CLICK.getId(),
                R.string.code_earth,
                R.string.post_menu_everyone_title,
                -1,
                FontHelper.getFlatIconFontResource()));
        dialog.addItem(items);
        dialog.setOnItemClickListener(result -> {
            dialog.dismiss();
            onClickListener.onComplete(result);
        });
        dialog.show(getChildFragmentManager(), EditPostPrivacyDialog.TAG);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (shouldListenUserLoginSuccessEven() && mUserLoginSuccessEventReceiver != null) {
            requireContext().unregisterReceiver(mUserLoginSuccessEventReceiver);
        }
    }
}
