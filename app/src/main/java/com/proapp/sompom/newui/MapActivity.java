package com.proapp.sompom.newui;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.widget.TextView;

import com.proapp.sompom.intent.MapIntent;
import com.proapp.sompom.newui.fragment.MapFragment;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ActivityMapBinding;

public class MapActivity extends AbsBindingActivity<ActivityMapBinding> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MapIntent intent = new MapIntent(getIntent());

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_view, MapFragment.getInstance(intent.getProduct()),
                            MapFragment.TAG).commit();
        }
        //Init toolbar
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        final TextView title = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        title.setText(R.string.search_address_map_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_map;
    }
}
