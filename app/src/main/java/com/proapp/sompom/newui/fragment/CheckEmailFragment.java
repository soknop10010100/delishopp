package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentCheckEmailBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.CheckEmailDataManager;
import com.proapp.sompom.viewmodel.AbsLoginViewModel;
import com.proapp.sompom.viewmodel.CheckEmailViewModel;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 2/10/22.
 */
public class CheckEmailFragment extends AbsLoginFragment<FragmentCheckEmailBinding, CheckEmailViewModel> implements
        AbsLoginViewModel.AbsLoginViewModelListener {

    @Inject
    @PublicQualifier
    public ApiService mPublicAPIService;

    public static CheckEmailFragment newInstance() {

        Bundle args = new Bundle();

        CheckEmailFragment fragment = new CheckEmailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_INPUT_EMAIL;
    }

    @Override
    protected boolean shouldListenUserLoginSuccessEven() {
        return true;
    }

    @Override
    protected void onRefreshScreen(boolean isFromSuccessfulAuthentication) {
        Timber.i("onRefreshScreen");
        if (isFromSuccessfulAuthentication) {
            requireActivity().finish();
        }
    }

    @Override
    protected boolean forceRebuildControllerComponent() {
        return true;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_check_email;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mViewModel = new CheckEmailViewModel(requireContext(),
                new CheckEmailDataManager(requireContext(), mPublicAPIService, null),
                this);
        setVariable(BR.viewModel, mViewModel);
    }
}
