package com.proapp.sompom.newui.dialog;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.annotation.UiThread;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.fragment.app.DialogFragment;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.injection.controller.ControllerComponent;
import com.proapp.sompom.injection.controller.ControllerModule;

/**
 * Created by nuonveyo on 2/21/18.
 */

public class AbsBaseBottomSheetDialog extends BottomSheetDialogFragment {
    public static final String TAG = AbsBaseBottomSheetDialog.class.getName();

    private ControllerComponent mControllerComponent;

    @UiThread
    public ControllerComponent getControllerComponent() {
        if (mControllerComponent == null) {
            mControllerComponent = ((MainApplication) getActivity().getApplication())
                    .getApplicationComponent()
                    .newControllerComponent(new ControllerModule(getActivity()));
        }
        return mControllerComponent;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, getStyle());
    }

    protected @StyleRes
    int getStyle() {
        return R.style.PopupStyle;
    }
}
