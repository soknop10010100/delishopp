package com.proapp.sompom.newui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.newui.fragment.DeepLinkingReceiverFragment;

import timber.log.Timber;

public class DeepLinkingReceiverActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Timber.i("onCreate");
        super.onCreate(savedInstanceState);
        setFragment(DeepLinkingReceiverFragment.newInstance(), DeepLinkingReceiverFragment.class.getSimpleName());
        new Handler().postDelayed(() -> parseDeepLinkingData(getIntent()), 100);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Timber.i("onNewIntent");
        parseDeepLinkingData(intent);
    }

    private void parseDeepLinkingData(Intent appLinkIntent) {
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
//        Timber.i("appLinkAction: " + appLinkAction + ", appLinkData: " + (appLinkData != null ? appLinkData.toString() : null));
        /*
            If there is any deep linking received within this activity, it will pass to corresponding
            activity and close its self. Currently we handle only deep linking for reset password, later
            on if we have more deep linking to handle, we add the implement below.
         */
        if (!TextUtils.isEmpty(appLinkAction) && appLinkData != null && !TextUtils.isEmpty(appLinkData.getLastPathSegment())) {
            Intent intent = ApplicationHelper.getSupportResetPasswordDeepLinkingIntent((MainApplication) getApplicationContext());
            if (intent != null) {
                intent.setAction(appLinkAction);
                intent.setData(appLinkData);
                startActivity(intent);
            }
        }
        finish();
    }
}
