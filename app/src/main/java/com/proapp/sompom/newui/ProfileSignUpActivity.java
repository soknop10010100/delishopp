package com.proapp.sompom.newui;

import android.os.Bundle;

import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.intent.ProfileSignUpIntent;
import com.proapp.sompom.model.emun.AuthConfigurationType;
import com.proapp.sompom.newui.fragment.ProfileSignUpFragment;

import androidx.annotation.Nullable;

/**
 * Created by Veasna Chhom on 9/4/21.
 */
public class ProfileSignUpActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(ProfileSignUpFragment.newInstance(new ProfileSignUpIntent(getIntent()).getSignUpData()),
                ProfileSignUpFragment.class.getSimpleName());
    }

    @Override
    public void onBackPressed() {
        //Do not allow to navigate back screen.
        AuthConfigurationType authConfigurationType =  ApplicationHelper.getAuthConfigurationType(this);
        if (authConfigurationType != AuthConfigurationType.EMAIL) {
            super.onBackPressed();
        }
    }
}
