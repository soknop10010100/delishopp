package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.library.baseAdapters.BR;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ItemMessageAdapter;
import com.proapp.sompom.databinding.FragmentSearchConversationBinding;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.intent.newintent.SearchConversationIntent;
import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.emun.OpenChatScreenType;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.viewmodel.newviewmodel.SearchConversationViewModel;
import com.proapp.sompom.widget.LoaderMoreLayoutManager;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 8/6/2020.
 */

public class SearchConversationFragment extends AbsBindingFragment<FragmentSearchConversationBinding> implements
        SearchConversationViewModel.SearchConversationViewModelListener, LoaderMoreLayoutManager.OnLoadMoreCallback {

    @Inject
    public ApiService mApiService;

    private SearchConversationViewModel mViewModel;
    private Conversation mConversation;
    private ItemMessageAdapter mAdapter;
    private LoaderMoreLayoutManager mMoreLayoutManager;

    public static SearchConversationFragment newInstance(Conversation conversation,
                                                         GroupDetail groupDetail) {
        Bundle args = new Bundle();
        args.putParcelable(SearchConversationIntent.CONVERSATION, conversation);
        args.putParcelable(SearchConversationIntent.GROUP_DETAIL, groupDetail);

        SearchConversationFragment fragment = new SearchConversationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search_conversation;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        setToolbar(getBinding().toolbar, false);
        if (getArguments() != null) {
            mConversation = getArguments().getParcelable(SearchConversationIntent.CONVERSATION);
            mViewModel = new SearchConversationViewModel(requireContext(),
                    mApiService,
                    getArguments().getParcelable(SearchConversationIntent.GROUP_DETAIL),
                    this);
            setVariable(BR.viewModel, mViewModel);
        }
    }

    @Override
    public void onQuerySuccess(List<Chat> data, boolean isLoadMore, boolean isCanLoadMore) {
        Timber.i("onQuerySuccess: size: " + data.size() +
                ", isLoadMore: " + isLoadMore +
                ", isCanLoadMore: " + isCanLoadMore);
        if (mAdapter == null) {
            mAdapter = new ItemMessageAdapter(data,
                    mConversation,
                    mViewModel.getQuery(),
                    chat -> {
                        Timber.i("Selected chat: " + new Gson().toJson(chat));
                        requireActivity().startActivity(new ChatIntent(requireContext(),
                                mConversation,
                                chat,
                                mViewModel.getQuery(),
                                OpenChatScreenType.GROUP_SEARCH));
                    });
            mAdapter.setCanLoadMore(isCanLoadMore);
            mMoreLayoutManager = new LoaderMoreLayoutManager(requireContext());
            if (isCanLoadMore) {
                mMoreLayoutManager.setOnLoadMoreListener(this);
            }
            getBinding().recyclerView.setLayoutManager(mMoreLayoutManager);
            getBinding().recyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setSearchKeyWord(mViewModel.getQuery());
            mAdapter.setCanLoadMore(isCanLoadMore);
            if (!isLoadMore) {
                mAdapter.setDatas(data);
                mAdapter.notifyDataSetChanged();
            } else {
                mMoreLayoutManager.loadingFinished();
                mAdapter.addLoadMoreData(data);
            }
            //Check to register or remove load more callback base on the result of query
            mMoreLayoutManager.setOnLoadMoreListener(isCanLoadMore ? this : null);
        }
    }

    @Override
    public void onQueryFailed(boolean isLoadMore, Throwable e) {
        Timber.i("onQueryFailed: isLoadMore: " + isLoadMore + ", error: " + e.getMessage());
        if (isLoadMore) {
            mMoreLayoutManager.loadingFinished();
            mMoreLayoutManager.setOnLoadMoreListener(null);
            showSnackBar(e.getMessage(), true);
        }
    }

    @Override
    public void onShouldClearResultList() {
        if (mAdapter != null && mAdapter.getItemCount() > 0) {
            mAdapter.resetData();
        }
    }

    @Override
    public void onLoadMoreFromBottom() {
        Timber.i("onLoadMoreFromBottom");
        mViewModel.loadMore();
    }
}
