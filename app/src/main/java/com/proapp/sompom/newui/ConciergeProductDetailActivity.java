package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.proapp.sompom.intent.ConciergeProductDetailIntent;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.newui.fragment.ConciergeProductDetailFragment;

/**
 * Created by Or Vitovongsak on 13/9/21.
 */

public class ConciergeProductDetailActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConciergeProductDetailIntent intent = new ConciergeProductDetailIntent(getIntent());
        ConciergeMenuItem editOderItem = intent.getEditOderItem();
        if (editOderItem != null) {
            setFragment(ConciergeProductDetailFragment.newEditOrderItemInstance(editOderItem, intent.getPositionInBasket()),
                    ConciergeProductDetailFragment.TAG);
        } else {
            setFragment(ConciergeProductDetailFragment.newInstance(intent.getMenuItemId(),
                            intent.getComboItem(),
                            intent.getIsOpenFromCombo()),
                    ConciergeProductDetailFragment.TAG);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(ConciergeProductDetailFragment.TAG);
        if (fragmentByTag instanceof ConciergeProductDetailFragment) {
            ((ConciergeProductDetailFragment) fragmentByTag).onBackPressOfActivity(this);
        }
    }
}
