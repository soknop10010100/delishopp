package com.proapp.sompom.newui;

import android.os.Bundle;

import com.proapp.sompom.intent.ConciergeReviewCheckoutIntent;
import com.proapp.sompom.newui.fragment.ConciergeReviewCheckoutFragment;

/**
 * Created by Veasna Chhom on 5/4/22.
 */

public class ConciergeReviewCheckoutActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(ConciergeReviewCheckoutFragment.newInstance(), ConciergeReviewCheckoutFragment.TAG);
    }
}
