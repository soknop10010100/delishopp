package com.proapp.sompom.newui.fragment;

import static com.proapp.sompom.newui.ChatActivity.REFRESH_NOTIFICATION_BADGE_ACTION;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ViewDataBinding;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.sompom.pushy.service.SendBroadCastHelper;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 3/18/22.
 */

public abstract class AbsSupportABAPaymentFragment<T extends ViewDataBinding> extends AbsHandleConciergeBasketErrorFragment<T> {

    public static final String ORDER_SUCCESS_EVENT = "ORDER_SUCCESS_EVENT";
    public static final String RECEIVE_ABA_PAYMENT_PUSHBACK_EVENT = "RECEIVE_ABA_PAYMENT_PUSHBACK_EVENT";
    public static final String IS_ABA_PAYMENT_SUCCESS = "IS_ABA_PAYMENT_SUCCESS";
    public static final String ABA_PAYMENT_PUSHBACK_MESSAGE = "ABA_PAYMENT_MESSAGE";
    public static final String BASKET_ID = "BASKET_ID";

    private BroadcastReceiver mABAPaymentSuccessBroadcastReceiver;
    private BroadcastReceiver mOrderWithCashSuccessEventBroadcastReceiver;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerABAPaymentSuccessPushbackEvenReceiver();
        if (shouldRegisterOrderSuccessEvent()) {
            registerOrderWithCashSuccessEventBroadcastReceiver();
        }
    }

    private void registerABAPaymentSuccessPushbackEvenReceiver() {
        mABAPaymentSuccessBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String basketId = intent.getStringExtra(BASKET_ID);
                String message = intent.getStringExtra(ABA_PAYMENT_PUSHBACK_MESSAGE);
                boolean isPaymentSuccess = intent.getBooleanExtra(IS_ABA_PAYMENT_SUCCESS, false);
                Timber.i("onReceive: RECEIVE_ABA_PAYMENT_SUCCESS_PUSHBACK_EVENT: basketId: " + basketId);
                onReceiveABAPaymentPushbackEvent(basketId, isPaymentSuccess, message);
            }
        };
        requireContext().registerReceiver(mABAPaymentSuccessBroadcastReceiver, new IntentFilter(RECEIVE_ABA_PAYMENT_PUSHBACK_EVENT));
    }

    private void registerOrderWithCashSuccessEventBroadcastReceiver() {
        mOrderWithCashSuccessEventBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                onReceivedOrderSuccessEvent();
            }
        };
        requireContext().registerReceiver(mOrderWithCashSuccessEventBroadcastReceiver,
                new IntentFilter(ORDER_SUCCESS_EVENT));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requireContext().unregisterReceiver(mABAPaymentSuccessBroadcastReceiver);
        if (mOrderWithCashSuccessEventBroadcastReceiver != null) {
            requireContext().unregisterReceiver(mOrderWithCashSuccessEventBroadcastReceiver);
        }
    }

    protected void onReceiveABAPaymentPushbackEvent(String basketId, boolean isPaymentSuccess, String message) {
        //Client implementation...
    }

    protected boolean shouldRegisterOrderSuccessEvent() {
        return false;
    }

    protected void onReceivedOrderSuccessEvent() {
        //Client implementation...
    }

    protected void broadcastOrderSuccessEvent() {
        Intent intent = new Intent(ORDER_SUCCESS_EVENT);
        SendBroadCastHelper.verifyAndSendBroadCast(requireContext(), intent);
    }

    protected void showMakeOrderSuccessPopup() {
        if (isAddedToActivity()) {
            ConciergeHelper.broadcastShowTrackingOrderButtonEvent(requireContext(), true);
            SendBroadCastHelper.verifyAndSendBroadCast(requireContext(), new Intent(REFRESH_NOTIFICATION_BADGE_ACTION));
            MessageDialog messageDialog = MessageDialog.newInstance();
            messageDialog.setCancelable(false);
            messageDialog.setTitle(getString(R.string.popup_success_title));
            messageDialog.setMessage(getString(R.string.shop_checkout_alert_success_order));
            messageDialog.setLeftText(getString(R.string.popup_ok_button), view -> {
                requireActivity().setResult(AppCompatActivity.RESULT_OK);
                requireActivity().finish();
            });
            messageDialog.show(requireActivity().getSupportFragmentManager());
            FlurryHelper.logEvent(requireContext(), FlurryHelper.ORDER_SUCCESS);
        }
    }
}
