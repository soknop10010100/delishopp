package com.proapp.sompom.newui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.AbsConciergeOrderHistoryAdapter;
import com.proapp.sompom.adapter.ConciergeOrderTrackingAdapter;
import com.proapp.sompom.databinding.FragmentConciergeOrderTrackingBinding;
import com.proapp.sompom.decorataor.ConciergeOrderHistoryItemDecorator;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.model.ItemConciergeOrderHistoryAdaptive;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeOrderTrackingDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeOrderTrackingFragmentViewModel;
import com.proapp.sompom.widget.LoadMoreLinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 4/11/21.
 */
public class ConciergeOrderTrackingFragment
        extends AbsBindingFragment<FragmentConciergeOrderTrackingBinding>
        implements ConciergeOrderTrackingFragmentViewModel.ConciergeOrderTrackingFragmentViewModelListener {

    @Inject
    public ApiService mApiService;
    private Context mContext;
    private ConciergeOrderTrackingFragmentViewModel mViewModel;
    private ConciergeOrderTrackingAdapter mAdapter;
    private LoadMoreLinearLayoutManager mLayoutManager;
    private LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener mLoadMoreLinearLayoutManagerListener;

    private BroadcastReceiver mOrderStatusUpdateReceiver;

    public static ConciergeOrderTrackingFragment newInstance() {
        return new ConciergeOrderTrackingFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_order_tracking;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requireContext().unregisterReceiver(mOrderStatusUpdateReceiver);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mContext = requireContext();
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        initLoadMoreListener();
        initOrderStatusUpdateReceiver();
        initViewModel();
    }

    private void initOrderStatusUpdateReceiver() {
        mOrderStatusUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive orderStatusUpdateEvent");
                ConciergeOrder updatedOrder = intent.getParcelableExtra(ConciergeHelper.UPDATE_ORDER_STATUS_EVENT_DATA);

                if (mAdapter != null) {
                    mAdapter.updateOrderStatus(updatedOrder);
                }
            }
        };
        requireContext().registerReceiver(mOrderStatusUpdateReceiver,
                new IntentFilter(ConciergeHelper.UPDATE_ORDER_STATUS_EVENT));
    }

    private void initViewModel() {
        mViewModel = new ConciergeOrderTrackingFragmentViewModel(mContext,
                new ConciergeOrderTrackingDataManager(mContext, mApiService),
                this);
        setVariable(BR.viewModel, mViewModel);
        mViewModel.requestData(false, false);
    }

    private void initAdapter() {
        mAdapter = new ConciergeOrderTrackingAdapter(mContext,
                new ArrayList<>(),
                new AbsConciergeOrderHistoryAdapter.AbsConciergeOrderHistoryAdapterListener() {
                    @Override
                    public void onMessageClicked(ItemConciergeOrderHistoryAdaptive adaptive) {
                        UserHelper.openCrispChatSupportScreen(requireContext());
                    }

                    @Override
                    public void onCallClicked() {

                    }

                    @Override
                    public void onEmptyTrackingOrder() {
                        mViewModel.showEmptyDataView();
                        onEmptyData();
                    }
                });
        mLayoutManager = new LoadMoreLinearLayoutManager(mContext, getBinding().recyclerView);
        getBinding().recyclerView.setLayoutManager(mLayoutManager);
        getBinding().recyclerView.addItemDecoration(new ConciergeOrderHistoryItemDecorator(mContext));
        getBinding().recyclerView.setAdapter(mAdapter);
    }

    public boolean isHasDataFromServer() {
        return mViewModel != null && mViewModel.isHasDataFromServer();
    }

    @Override
    public void onLoadDataSuccess(List<ConciergeOrder> data,
                                  boolean isRefreshed,
                                  boolean isFromLoadMore,
                                  boolean isCanLoadMore) {
        Timber.i("onLoadDataSuccess: " + (data != null ? data.size() : null) +
                ", isRefreshed: " + isRefreshed +
                ", isFromLoadMore: " + isFromLoadMore +
                ", isCanLoadMore: " + isCanLoadMore);
        setData(data, isRefreshed, isFromLoadMore, isCanLoadMore);
    }

    @Override
    public void onEmptyData() {
        // This callback is for removing the padding decoration we added when creating the adapter
        // for the order tracking list. We should also clear the current adapter if there are empty
        // data, since the recycler view's adapter would be the empty data adapter.
        int decorationCount = getBinding().recyclerView.getItemDecorationCount();
        for (int index = 0; index < decorationCount; index++) {
            getBinding().recyclerView.removeItemDecorationAt(index);
        }
        mAdapter = null;
    }

    public void initLoadMoreListener() {
        mLoadMoreLinearLayoutManagerListener = new LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener() {
            @Override
            public void onReachedLoadMoreBottom() {
                Timber.i("onReachedLoadMoreBottom");
                if (mContext instanceof AbsBaseActivity) {
                    new Handler().post(() -> performLoadMore());
                }
            }

            @Override
            public void onReachedLoadMoreByDefault() {
                Timber.i("onReachedLoadMoreByDefault");
                if (mContext instanceof AbsBaseActivity) {
                    new Handler().post(() -> performLoadMore());
                }
            }
        };
    }

    public void performLoadMore() {
        mViewModel.loadMore();
    }

    public void setData(List<ConciergeOrder> data,
                        boolean isRefreshed,
                        boolean isFromLoadMore,
                        boolean isCanLoadMore) {
        // If Adapter is null and
        if (mAdapter == null) {
            initAdapter();
        }

        if (isFromLoadMore) {
            mAdapter.setCanLoadMore(isCanLoadMore);
            if (!data.isEmpty()) {
                mAdapter.addLoadMoreData(parseAdaptiveModel(data));
            }

            if (!isCanLoadMore) {
                mLayoutManager.setLoadMoreLinearLayoutManagerListener(null);
                getBinding().recyclerView.post(() -> {
                    mAdapter.setCanLoadMore(false);
                    mAdapter.removeLoadMoreLayout();
//                    mAdapter.addOrRemoveEndOfHistoryItem(true);
                });
            }
            mLayoutManager.loadingFinished();
            mViewModel.setIsAlreadyLoadMore(false);
        } else {
            if (isRefreshed) {
                mAdapter.setRefreshData(parseAdaptiveModel(data), isCanLoadMore);
            } else {
                mAdapter.setData(parseAdaptiveModel(data));
            }
            mAdapter.setCanLoadMore(isCanLoadMore);
            if (isCanLoadMore) {
                mLayoutManager.setLoadMoreLinearLayoutManagerListener(mLoadMoreLinearLayoutManagerListener);
            }
//            else {
//                mAdapter.addOrRemoveEndOfHistoryItem(true);
//            }
        }
    }

    private List<ItemConciergeOrderHistoryAdaptive> parseAdaptiveModel(List<ConciergeOrder> data) {
        return new ArrayList<>(data);
    }
}
