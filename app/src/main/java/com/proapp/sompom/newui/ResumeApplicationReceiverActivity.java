package com.proapp.sompom.newui;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.newui.fragment.ResumeApplicationReceiverFragment;

public class ResumeApplicationReceiverActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(ResumeApplicationReceiverFragment.newInstance(), ResumeApplicationReceiverFragment.class.getSimpleName());
        handleActivity();
    }

    private void handleActivity() {
        /*
            If there is any activity running, we just resume it. Otherwise, we will start the app from
            splash screen.
         */
        if (isTaskRoot()) {
            Intent splashIntent = new Intent(this, SplashActivity.class);
            splashIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(splashIntent);
        }
        finish();
    }
}
