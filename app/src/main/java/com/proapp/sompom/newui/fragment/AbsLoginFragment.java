package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentActivity;

import com.proapp.sompom.R;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.model.emun.ChangePasswordActionType;
import com.proapp.sompom.model.result.AuthResponse;
import com.proapp.sompom.newui.dialog.ChangePasswordDialog;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.SnackBarUtil;
import com.proapp.sompom.viewmodel.AbsLoadingViewModel;

import java.util.Map;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 4/23/21.
 */
public abstract class AbsLoginFragment<T extends ViewDataBinding, VM extends AbsLoadingViewModel> extends AbsBindingFragment<T> {

    protected VM mViewModel;

    // See this link regarding permission request:
    // https://developer.android.com/training/permissions/requesting#allow-system-manage-request-code

    // The ActivityResultLauncher *MUST* be initialized during the fragment or activity initialization.
    protected ActivityResultLauncher<String[]> mRequestPermissionsLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), this::onRequestPermissionCallback);

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    // Delegate the callback so we can customize how to handle the permission result.
    protected void onRequestPermissionCallback(Map<String, Boolean> permissionsResult) {
        // The "permissionResults" here will be a Map<String, Boolean> representing a
        // permission and whether it was granted or denied by the user

        // Based on previous implementation, the login screen doesn't particularly care about the
        // permission result, so just invoke the on permission result function
    }

    protected boolean shouldRequestAllPermissionAtStartUp() {
        return true;
    }

    protected void handleLoginError(String message) {
        SnackBarUtil.showSnackBar(getBinding().getRoot(), message, true);
    }

    protected void handleOnLoginSuccess() {
        navigateToHomeScreen(requireActivity());
    }

    private void navigateToHomeScreen(FragmentActivity activity) {
        if (SharedPrefUtils.getShouldOpenHomeScreenAfterLogin(activity)) {
            ApplicationHelper.broadcastRefreshScreenEvent(activity, true);
            Timber.i("getShouldOpenHomeScreenAfterLogin");
            activity.startActivity(ApplicationHelper.getFreshHomeIntent(activity));
            activity.finish();
            activity.overridePendingTransition(0, R.anim.activity_fade_out);
        } else {
            Timber.i("onPermissionResultAfterLoginSuccess pass result");
            activity.setResult(AppCompatActivity.RESULT_OK);
            activity.finish();
        }
    }

    protected void handleChangePasswordRequire(AuthResponse authResponse) {
        ChangePasswordActionType type = ChangePasswordActionType.CHANGE_PASSWORD;
        if (!authResponse.getUser().getPasswordSetup()) {
            type = ChangePasswordActionType.CHANGE_PASSWORD_WITHOUT_OLD_PASSWORD;
        }
        ChangePasswordDialog dialog = ChangePasswordDialog.newInstance(type, null);
        dialog.setListener(new ChangePasswordDialog.ChangePasswordDialogListener() {
            @Override
            public void onChangePasswordSuccess(ChangePasswordActionType type) {
                AbsLoginFragment.this.showSnackBar(type == ChangePasswordActionType.RESET_PASSWORD ?
                                R.string.change_password_popup_success_reset_password_description : R.string.change_password_success_message,
                        false);
                mViewModel.hideLoading();
                handleOnLoginSuccess();
            }

            @Override
            public void onDismissed() {
                mViewModel.hideLoading();
            }
        });
        dialog.show(getParentFragmentManager());
    }

    public void handleResetPasswordDeepLinking(String userToken) {
        Timber.i("handleResetPasswordDeepLinking: " + userToken);
        ChangePasswordDialog dialog = ChangePasswordDialog.newInstance(ChangePasswordActionType.RESET_PASSWORD,
                userToken);
        dialog.setListener(new ChangePasswordDialog.ChangePasswordDialogListener() {
            @Override
            public void onChangePasswordSuccess(ChangePasswordActionType type) {
                mViewModel.hideLoading();
                new AlertDialog.Builder(AbsLoginFragment.this.requireContext())
                        .setTitle(AbsLoginFragment.this.getString(R.string.change_password_reset_title))
                        .setMessage(type == ChangePasswordActionType.RESET_PASSWORD ?
                                R.string.change_password_popup_success_reset_password_description : R.string.change_password_success_message)
                        .setPositiveButton(AbsLoginFragment.this.getString(R.string.popup_ok_button), null)
                        .show();
            }

            @Override
            public void onDismissed() {
                mViewModel.hideLoading();
            }
        });
        dialog.show(getParentFragmentManager());
    }
}
