package com.proapp.sompom.newui.dialog;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.DialogChangeGroupNameBinding;
import com.proapp.sompom.model.GroupDetail;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ChatDataManager;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.viewmodel.newviewmodel.ChangeGroupNameDialogViewModel;

import javax.inject.Inject;

/**
 * Created by Or Vitovongsak on 7/5/20.
 */
public class ChangeGroupNameDialog extends MessageDialog implements ChangeGroupNameDialogViewModel.ChangeGroupNameViewModelListener {

    private ChangeGroupNameDialogViewModel mViewModel;
    private Conversation mConversation;
    private View mRoot;
    private ChangeGroupNameDialogListener mListener;

    public ChangeGroupNameDialog(Conversation conversation) {
        mConversation = conversation;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mRoot = view;
        getControllerComponent().inject(this);
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        DialogChangeGroupNameBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.dialog_change_group_name,
                null,
                true);
        mViewModel = new ChangeGroupNameDialogViewModel(mRoot,
                mConversation,
                this);

        binding.setVariable(BR.viewModel, mViewModel);
        getBinding().textviewTitle.setTypeface(null, Typeface.BOLD);
        setTitle(getString(R.string.group_menu_dialog_change_name_title));

        setLeftText(getString(R.string.group_menu_dialog_change_name_button_ok), dialog -> mViewModel.updateGroupName());
        setRightText(getString(R.string.group_menu_dialog_change_name_button_cancel), null);
        getBinding().view.addView(binding.getRoot());
        getBinding().buttonRight.setAllCaps(false);
        super.onViewCreated(view, savedInstanceState);
        view.postDelayed(() -> {
            binding.inputText.requestFocus();
            KeyboardUtil.showKeyboard(requireActivity(), binding.inputText);
            binding.inputText.setSelection(binding.inputText.getText().length());
        }, 100);
    }

    public void setmListener(ChangeGroupNameDialogListener mListener) {
        this.mListener = mListener;
    }

    @Override
    public void onChangeNameClick(GroupDetail groupDetail) {
        dismiss();
        if (mListener != null)
            mListener.onChangeNameClick(groupDetail);
    }

    public interface ChangeGroupNameDialogListener {
        void onChangeNameClick(GroupDetail groupDetail);
    }
}
