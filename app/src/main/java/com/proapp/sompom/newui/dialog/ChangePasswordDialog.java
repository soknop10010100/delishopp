package com.proapp.sompom.newui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.PartialChangePasswordLayoutBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.model.emun.ChangePasswordActionType;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ChangePasswordDataManager;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.utils.SnackBarUtil;
import com.proapp.sompom.viewmodel.newviewmodel.ChangePasswordViewModel;

import javax.inject.Inject;

public class ChangePasswordDialog extends MessageDialog implements ChangePasswordViewModel.ChangePasswordViewModelListener {

    private static final String ACTION_TYPE = "ACTION_TYPE";
    private static final String USER_TOKEN = "USER_TOKEN";
    private static final String USER_PHONE_NUMBER = "USER_PHONE_NUMBER";

    private ChangePasswordViewModel mViewModel;
    @Inject
    @PublicQualifier
    public ApiService mPublicApiService;
    @Inject
    public ApiService mApiService;
    private View mRoot;
    private ChangePasswordDialogListener mListener;

    public static ChangePasswordDialog newInstance(ChangePasswordActionType type, String userToken) {
        Bundle args = new Bundle();
        args.putInt(ACTION_TYPE, type.getValue());
        args.putString(USER_TOKEN, userToken);

        ChangePasswordDialog fragment = new ChangePasswordDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static ChangePasswordDialog newInstance(ChangePasswordActionType type, String userToken, String phoneNumber) {
        Bundle args = new Bundle();
        args.putInt(ACTION_TYPE, type.getValue());
        args.putString(USER_TOKEN, userToken);
        args.putString(USER_PHONE_NUMBER, phoneNumber);

        ChangePasswordDialog fragment = new ChangePasswordDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mRoot = view;
        getControllerComponent().inject(this);
        FlurryHelper.logEvent(requireContext(), FlurryHelper.SCREEN_CHANGE_PASSWORD_POPUP);
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        PartialChangePasswordLayoutBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.partial_change_password_layout,
                null,
                true);

        ChangePasswordActionType actionType = getActionType();
        if (actionType != ChangePasswordActionType.CHANGE_PASSWORD) {
            setCancelable(false);
        }
        setDismissWhenClickOnButtonLeft(false);
        mViewModel = new ChangePasswordViewModel(requireContext(),
                new ChangePasswordDataManager(requireContext(),
                        mApiService,
                        mPublicApiService,
                        actionType,
                        getUserToken(),
                        getUserPhoneNumber()),
                this);

        binding.setVariable(BR.viewModel, mViewModel);
        setTitle(getString((actionType == ChangePasswordActionType.RESET_PASSWORD ||
                actionType == ChangePasswordActionType.RESET_PASSWORD_VIA_PHONE) ? R.string.change_password_reset_title : R.string.change_password_title));
        String message = getString(R.string.change_password_description);
        if (getActionType() == ChangePasswordActionType.CHANGE_PASSWORD_WITHOUT_OLD_PASSWORD) {
            message = getString(R.string.change_password_first_time_info);
        } else if (getActionType() == ChangePasswordActionType.RESET_PASSWORD) {
            message = getString(R.string.change_password_reset_description);
        }
        setMessage(message);
        setLeftText(getString(R.string.change_password_change_button), dialog -> {
            if (mViewModel.isReadyToChangePassword()) {
                showLoading();
                mViewModel.requestChangePassword();
            }
        });
        setRightText(getString(R.string.change_password_cancel_button), null);
        getBinding().view.addView(binding.getRoot());
        super.onViewCreated(view, savedInstanceState);
        view.postDelayed(() -> {
            binding.editTextCurrentPassword.requestFocus();
            KeyboardUtil.showKeyboard(requireActivity(), binding.editTextPassword);
        }, 100);
    }

    private String getUserToken() {
        if (getArguments() != null) {
            return getArguments().getString(USER_TOKEN);
        }

        return null;
    }

    private String getUserPhoneNumber() {
        if (getArguments() != null) {
            return getArguments().getString(USER_PHONE_NUMBER);
        }

        return null;
    }

    private ChangePasswordActionType getActionType() {
        if (getArguments() != null) {
            return ChangePasswordActionType.newInstanceFromValue(getArguments().getInt(ACTION_TYPE,
                    0));
        }

        return ChangePasswordActionType.CHANGE_PASSWORD;
    }

    public void setListener(ChangePasswordDialogListener listener) {
        mListener = listener;
    }

    @Override
    public void onChangePasswordFailed(String error) {
        hideLoading();
        SnackBarUtil.showSnackBar(mRoot, error, true);
    }

    @Override
    public void onChangePasswordSuccess(ChangePasswordActionType type) {
        hideLoading();
        dismiss();
        if (mListener != null) {
            mListener.onChangePasswordSuccess(type);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mListener != null) {
            mListener.onDismissed();
        }
    }

    public interface ChangePasswordDialogListener {

        void onChangePasswordSuccess(ChangePasswordActionType type);

        default void onDismissed() {
        }
    }
}
