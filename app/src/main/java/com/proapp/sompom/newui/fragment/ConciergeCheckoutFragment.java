package com.proapp.sompom.newui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ConciergeOnlinePaymentOptionAdapter;
import com.proapp.sompom.databinding.FragmentConciergeCheckoutBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.intent.ConciergeCheckoutIntent;
import com.proapp.sompom.model.concierge.ConciergeOrderResponseWithABAPayWay;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.result.ConciergeCheckoutData;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.newui.dialog.UserLoginRegisterBottomSheetDialog;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeCheckoutManager;
import com.proapp.sompom.utils.BitmapConverter;
import com.proapp.sompom.viewmodel.AbsMapViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeCheckoutFragmentViewModel;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 16/9/21.
 */
public class ConciergeCheckoutFragment extends AbsSupportABAPaymentFragment<FragmentConciergeCheckoutBinding>
        implements ConciergeCheckoutFragmentViewModel.ConciergeCheckoutFragmentViewModelCallback,
        OnMapReadyCallback {

    private ConciergeCheckoutFragmentViewModel mViewModel;
    @Inject
    public ApiService mApiService;
    private ConciergeOnlinePaymentOptionAdapter mPaymentOptionAdapter;
    private BroadcastReceiver mRefreshLocalBasketDisplayEventReceiver;
    private GoogleMap mGoogleMap;
    private MapView mMapView;
    private ConciergeUserAddress mConciergeUserAddress;

    public static ConciergeCheckoutFragment newInstance(ConciergeUserAddress primaryAddress,
                                                        String selectedSlotId,
                                                        boolean isSlotProvince,
                                                        ConciergeOrderResponseWithABAPayWay resumePaymentData,
                                                        String deliveryInstruction) {

        Bundle args = new Bundle();
        args.putParcelable(ConciergeCheckoutIntent.FIELD_USER_PRIMARY_ADDRESS, primaryAddress);
        args.putString(ConciergeCheckoutIntent.FIELD_SELECTED_TIMESLOT_ID, selectedSlotId);
        args.putBoolean(ConciergeCheckoutIntent.FIELD_IS_SLOT_PROVINCE, isSlotProvince);
        args.putParcelable(ConciergeCheckoutIntent.FIELD_RESUME_ABA_PAYMENT_DATA, resumePaymentData);
        args.putString(ConciergeCheckoutIntent.FIELD_DELIVERY_INSTRUCTION, deliveryInstruction);

        ConciergeCheckoutFragment fragment = new ConciergeCheckoutFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_PLACE_ORDER;
    }

    @Override
    protected boolean shouldListenUserLoginSuccessEven() {
        return true;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_checkout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(
                this);
        initGoogleMap(savedInstanceState);
        registerRefreshLocalBasketDisplayEventReceiver();
        mConciergeUserAddress = getArguments().getParcelable(ConciergeCheckoutIntent.FIELD_USER_PRIMARY_ADDRESS);
        mViewModel = new ConciergeCheckoutFragmentViewModel(requireContext(),
                mConciergeUserAddress,
                getArguments().getString(ConciergeCheckoutIntent.FIELD_SELECTED_TIMESLOT_ID),
                getArguments().getBoolean(ConciergeCheckoutIntent.FIELD_IS_SLOT_PROVINCE),
                getArguments().getParcelable(ConciergeCheckoutIntent.FIELD_RESUME_ABA_PAYMENT_DATA),
                getArguments().getString(ConciergeCheckoutIntent.FIELD_DELIVERY_INSTRUCTION),
                new ConciergeCheckoutManager(requireContext(), mApiService),
                this);
        setVariable(BR.viewModel, mViewModel);
        mViewModel.requestData(true, true, false);
    }

    private void initGoogleMap(@Nullable Bundle savedInstanceState) {
        mMapView = getBinding().mapView;
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        mMapView.getMapAsync(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mMapView != null) {
            mMapView.onSaveInstanceState(outState);
        }
    }

    @Override
    protected void onReceiveABAPaymentPushbackEvent(String basketId, boolean isPaymentSuccess, String message) {
        Timber.i("onReceiveABAPaymentPushbackEvent: isPaymentSuccess: " + isPaymentSuccess);
        mViewModel.onReceiveABAPaymentPushbackEvent(basketId, isPaymentSuccess, message);
    }

    @Override
    public void onRequestDataSuccess(ConciergeCheckoutData data) {
        initOnlinePaymentOptionList();
    }

    private void registerRefreshLocalBasketDisplayEventReceiver() {
        mRefreshLocalBasketDisplayEventReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Timber.i("OnReceive registerRefreshLocalBasketDisplayEventReceiver");
                mViewModel.reloadData(false);
            }
        };

        requireContext().registerReceiver(mRefreshLocalBasketDisplayEventReceiver,
                new IntentFilter(AbsSupportShopCheckOutFragment.REFRESH_LOCAL_BASKET_DISPLAY_EVENT));
    }

    private void initOnlinePaymentOptionList() {
        if (mViewModel.getPaymentOptions() != null && !mViewModel.getPaymentOptions().isEmpty()) {
            if (mPaymentOptionAdapter == null) {
                // Default to first index of payment
                int selectedPaymentIndex = 0;
//                Timber.i("mViewModel.getSelectedOnlinePaymentAccount(): " + new Gson().toJson(mViewModel.getSelectedOnlinePaymentAccount()) +
//                        ", mViewModel.getPaymentOptions(): " + new Gson().toJson(mViewModel.getPaymentOptions()));
                if (mViewModel.getSelectedOnlinePaymentAccount() != null) {
                    // Check if there's already a selected payment option when first init adapter
                    // and select it
                    for (int index = 0; index < mViewModel.getPaymentOptions().size(); index++) {
                        String provider = mViewModel.getPaymentOptions().get(index).getProvider();
//                        Timber.i("provider: " + provider);
                        if (TextUtils.equals(provider, mViewModel.getSelectedOnlinePaymentAccount().getProvider())) {
                            selectedPaymentIndex = index;
                            break;
                        }
                    }

//                    Timber.i("selectedPaymentIndex: " + selectedPaymentIndex);
                }

                mPaymentOptionAdapter = new ConciergeOnlinePaymentOptionAdapter(mViewModel.getPaymentOptions(),
                        selectedPaymentIndex,
                        conciergePaymentOption -> mViewModel.updateOnlinePaymentSelectionAccount(conciergePaymentOption));
                getBinding().recyclerViewOnlinePayment.setLayoutManager(new LinearLayoutManager(requireContext()));
                getBinding().recyclerViewOnlinePayment.setAdapter(mPaymentOptionAdapter);

                if (mViewModel.getSelectedOnlinePaymentAccount() == null) {
                    //Make the default selection of online payment option
                    mViewModel.updateOnlinePaymentSelectionAccount(mViewModel.getPaymentOptions().get(selectedPaymentIndex));
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requireContext().unregisterReceiver(mRefreshLocalBasketDisplayEventReceiver);
        if (mMapView != null) {
            mMapView.onDestroy();
        }
    }

    @Override
    public void onRefreshData(ConciergeCheckoutData data) {
    }

    @Override
    public void onOrderSuccess() {
        broadcastOrderSuccessEvent();
        showMakeOrderSuccessPopup();
    }

    @Override
    protected void onMakePaymentWithABASuccess(handleOnPaymentInProcessErrorListener listener) {
        onOrderSuccess();
    }

    @Override
    public void onAllItemRemovedFromCart() {
        requireActivity().setResult(AppCompatActivity.RESULT_OK);
        requireActivity().finish();
    }

    @Override
    public void onRequireLogin() {
        UserLoginRegisterBottomSheetDialog dialog = UserLoginRegisterBottomSheetDialog.newInstance();
        dialog.show(getChildFragmentManager(), dialog.getTag());
        getChildFragmentManager().setFragmentResultListener(UserLoginRegisterBottomSheetDialog.REQUEST_LOGIN_OR_SIGNUP_KEY,
                getViewLifecycleOwner(),
                (resultRequestKey, result) -> {
                    if (resultRequestKey.equals(UserLoginRegisterBottomSheetDialog.REQUEST_LOGIN_OR_SIGNUP_KEY)) {
                        if (result.getInt(UserLoginRegisterBottomSheetDialog.LOGIN_RESULT_KEY) == AppCompatActivity.RESULT_OK) {
                            mViewModel.reloadUserAndCompanyInfo();
                        }
                    }
                });
    }

    @Override
    public void onBasketModificationError(SupportConciergeCustomErrorResponse response, String errorMessage) {
        super.onBasketModificationError(response, errorMessage, null);
    }

    @Override
    protected ApiService getPrivateAPIService() {
        return mApiService;
    }

    @Override
    protected void showLoading() {
        mViewModel.showLoading(true);
    }

    @Override
    protected void hideLoading() {
        mViewModel.hideLoading();
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        Timber.i("onMapReady: " + new Gson().toJson(mConciergeUserAddress));
        if (!isAdded()) {
            return;
        }

        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        AppTheme theme = ThemeManager.getAppTheme(requireContext());
        if (theme == AppTheme.Black) {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.google_map_dark));
        } else if (theme == AppTheme.White) {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.google_map_light));
        }
        mGoogleMap = googleMap;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMapView != null) {
            mMapView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mMapView != null) {
            mMapView.onPause();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mMapView != null) {
            mMapView.onLowMemory();
        }
    }

    @Override
    public void addMarker(double lat, double lng) {
        if (mGoogleMap != null) {
            MarkerOptions options = new MarkerOptions();
            options.icon(BitmapConverter.bitmapDescriptorFromVector(requireContext(), R.drawable.ic_home_icon));
            options.position(new LatLng(lat, lng));
            mGoogleMap.clear();
            mGoogleMap.addMarker(options);
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,
                            lng),
                    AbsMapViewModel.ZOOM_LEVEL));
        }
    }

    @Override
    public void onABAPaymentIsPaidOnVerificationChecking() {
        broadcastOrderSuccessEvent();
        showMakeOrderSuccessPopup();
    }

    @Override
    public void showOrderErrorPopup(String errorMessage, View.OnClickListener okClickListener) {
        super.showOrderErrorPopup(errorMessage, okClickListener);
    }
}
