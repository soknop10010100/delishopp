package com.proapp.sompom.newui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.proapp.sompom.intent.newintent.ConciergeNewAddressIntent;
import com.proapp.sompom.newui.fragment.ConciergeNewAddressFragment;

/**
 * Created by Or Vitovongsak on 22/11/21.
 */
public class ConciergeNewAddressActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConciergeNewAddressIntent intent = new ConciergeNewAddressIntent(getIntent());
        setFragment(ConciergeNewAddressFragment.newInstance(intent.getIsEditAddressMode(),
                intent.getAddressData(),
                intent.shouldDisplayPrimaryAddressField()),
                ConciergeNewAddressFragment.class.getName());
    }
}
