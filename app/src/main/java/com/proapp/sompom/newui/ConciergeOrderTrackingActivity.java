package com.proapp.sompom.newui;

import static com.proapp.sompom.newui.ChatActivity.REFRESH_NOTIFICATION_BADGE_ACTION;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.proapp.sompom.intent.newintent.ConciergeOrderTrackingIntent;
import com.proapp.sompom.newui.fragment.ConciergeOrderTrackingFragment;
import com.sompom.pushy.service.SendBroadCastHelper;

/**
 * Created by Or Vitovongsak on 4/11/21.
 */
public class ConciergeOrderTrackingActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(ConciergeOrderTrackingFragment.newInstance(), ConciergeOrderTrackingFragment.class.getName());
        SendBroadCastHelper.verifyAndSendBroadCast(this, new Intent(REFRESH_NOTIFICATION_BADGE_ACTION));
    }

    @Override
    public void finish() {
        Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(ConciergeOrderTrackingFragment.class.getName());
        if (fragmentByTag instanceof ConciergeOrderTrackingFragment) {
            Intent data = new Intent();
            data.putExtra(ConciergeOrderTrackingIntent.HAS_DATA_FROM_SERVER, ((ConciergeOrderTrackingFragment) fragmentByTag).isHasDataFromServer());
            setResult(AppCompatActivity.RESULT_OK, data);
        }
        super.finish();
    }
}
