package com.proapp.sompom.newui;

import static com.proapp.sompom.newui.ChatActivity.REFRESH_NOTIFICATION_BADGE_ACTION;

import android.content.Intent;
import android.os.Bundle;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.intent.ConciergeOrderHistoryDetailIntent;
import com.proapp.sompom.intent.newintent.HomeIntent;
import com.proapp.sompom.newui.fragment.ConciergeOrderHistoryDetailFragment;
import com.sompom.pushy.service.SendBroadCastHelper;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 22/9/21.
 */

public class ConciergeOrderHistoryDetailActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.i("onCreate");
        ConciergeOrderHistoryDetailIntent intent = new ConciergeOrderHistoryDetailIntent(getIntent());
        setFragment(ConciergeOrderHistoryDetailFragment.newInstance(intent.getOrderId(),
                        intent.getOrderNumber()),
                ConciergeOrderHistoryDetailFragment.TAG);
        SendBroadCastHelper.verifyAndSendBroadCast(this, new Intent(REFRESH_NOTIFICATION_BADGE_ACTION));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Timber.i("onNewIntent");
    }

    @Override
    public void finish() {
        super.finish();
        boolean homeActivityRunning = ((MainApplication) getApplication()).isHomeActivityRunning();
        Timber.i("finish homeActivityRunning: " + homeActivityRunning);
        if (!homeActivityRunning) {
            ((MainApplication) getApplication()).startFreshHomeScreen(HomeIntent.Redirection.None);
        }
    }
}
