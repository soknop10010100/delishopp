package com.proapp.sompom.newui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.AbsConciergeOrderHistoryAdapter;
import com.proapp.sompom.adapter.ConciergeOrderHistoryAdapter;
import com.proapp.sompom.databinding.FragmentConciergeOrderHistoryBinding;
import com.proapp.sompom.decorataor.ConciergeOrderHistoryItemDecorator;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.model.ItemConciergeOrderHistoryAdaptive;
import com.proapp.sompom.model.emun.ConciergeOrderStatus;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ConciergeOrderHistoryDataManager;
import com.proapp.sompom.viewmodel.newviewmodel.ConciergeOrderHistoryFragmentViewModel;
import com.proapp.sompom.widget.LoadMoreLinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by nuonveyo on 7/4/18.
 */

public class ConciergeOrderHistoryFragment extends AbsBindingFragment<FragmentConciergeOrderHistoryBinding>
        implements ConciergeOrderHistoryFragmentViewModel.ConciergeOrderHistoryFragmentViewModelListener {

    public static final String IS_LOAD_CURRENT_ORDER_MODE = "IS_LOAD_CURRENT_ORDER_MODE";

    @Inject
    public ApiService mApiService;
    private ConciergeOrderHistoryFragmentViewModel mViewModel;
    private ConciergeOrderHistoryAdapter mAdapter;
    private LoadMoreLinearLayoutManager mLayoutManager;
    private LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener mLoadMoreLinearLayoutManagerListener;
    private boolean mShouldDisplayTrackingButton;
    private BroadcastReceiver mOrderStatusUpdateReceiver;
    private BroadcastReceiver mRefreshListReceiver;

    public static ConciergeOrderHistoryFragment newInstance(boolean isLoadCurrentOrderMode) {

        Bundle args = new Bundle();
        args.putBoolean(IS_LOAD_CURRENT_ORDER_MODE, isLoadCurrentOrderMode);

        ConciergeOrderHistoryFragment fragment = new ConciergeOrderHistoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_order_history;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requireContext().unregisterReceiver(mOrderStatusUpdateReceiver);
        requireContext().unregisterReceiver(mRefreshListReceiver);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        initOrderStatusUpdateReceiver();
        registerRefreshListEventReceiver();
        initViewModel();
    }

    private void initOrderStatusUpdateReceiver() {
        mOrderStatusUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive orderStatusUpdateEvent");
                ConciergeOrder updatedOrder = intent.getParcelableExtra(ConciergeHelper.UPDATE_ORDER_STATUS_EVENT_DATA);
                if (mAdapter != null) {
                    if (updatedOrder.getOrderStatus() == ConciergeOrderStatus.DELIVERED ||
                            updatedOrder.getOrderStatus() == ConciergeOrderStatus.CANCELED) {
                        mViewModel.makeSilentRefresh();
                    } else {
                        mAdapter.updateOrderStatus(updatedOrder);
                    }
                }
            }
        };
        requireContext().registerReceiver(mOrderStatusUpdateReceiver,
                new IntentFilter(ConciergeHelper.UPDATE_ORDER_STATUS_EVENT));
    }

    private void registerRefreshListEventReceiver() {
        mRefreshListReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive registerRefreshListEventReceiver");
                mViewModel.makeSilentRefresh();
            }
        };
        requireContext().registerReceiver(mRefreshListReceiver,
                new IntentFilter(ConciergeOrderDeliveryTrackingFragment.REFRESH_LIST_EVENT));
    }

    private void initViewModel() {
        mShouldDisplayTrackingButton = getArguments().getBoolean(IS_LOAD_CURRENT_ORDER_MODE, false);
        mViewModel = new ConciergeOrderHistoryFragmentViewModel(requireContext(),
                new ConciergeOrderHistoryDataManager(requireContext(),
                        mApiService,
                        mShouldDisplayTrackingButton),
                this);
        setVariable(BR.viewModel, mViewModel);
        mViewModel.requestData(false, false);
    }

    @Override
    public void onLoadDataSuccess(List<ConciergeOrder> data, boolean isRefreshed, boolean isFromLoadMore, boolean isCanLoadMore) {
        Timber.i("onLoadDataSuccess: " + (data != null ? data.size() : null) +
                ", isRefreshed: " + isRefreshed +
                ", isFromLoadMore: " + isFromLoadMore +
                ", isCanLoadMore: " + isCanLoadMore);
        setData(data, isRefreshed, isFromLoadMore, isCanLoadMore);
    }

    @Override
    public void onLoadMoreError() {
        mLayoutManager.setLoadMoreLinearLayoutManagerListener(null);
        mAdapter.setCanLoadMore(false);
        mLayoutManager.loadingFinished();
        mAdapter.removeLoadMoreLayout();
    }

    public void setData(List<ConciergeOrder> data, boolean isRefreshed, boolean isFromLoadMore, boolean isCanLoadMore) {
        if (mAdapter == null) {
            mAdapter = new ConciergeOrderHistoryAdapter(parseAdaptiveModel(data),
                    mShouldDisplayTrackingButton,
                    new AbsConciergeOrderHistoryAdapter.AbsConciergeOrderHistoryAdapterListener() {
                        @Override
                        public void onMessageClicked(ItemConciergeOrderHistoryAdaptive adaptive) {
                            UserHelper.openCrispChatSupportScreen(requireContext());
                        }

                        @Override
                        public void onCallClicked() {

                        }

                        @Override
                        public void onEmptyTrackingOrder() {

                        }
                    });
            mAdapter.setCanLoadMore(isCanLoadMore);
            mLayoutManager = new LoadMoreLinearLayoutManager(requireContext(), getBinding().recyclerView);
            mLoadMoreLinearLayoutManagerListener = new LoadMoreLinearLayoutManager.LoadMoreLinearLayoutManagerListener() {
                @Override
                public void onReachedLoadMoreBottom() {
                    Timber.i("onReachedLoadMoreBottom");
                    mViewModel.loadMore();
                }

                @Override
                public void onReachedLoadMoreByDefault() {
                    Timber.i("onReachedLoadMoreByDefault");
                    mViewModel.loadMore();
                }
            };
            mLayoutManager.setLoadMoreLinearLayoutManagerListener(isCanLoadMore ? mLoadMoreLinearLayoutManagerListener : null);
            getBinding().recyclerView.setLayoutManager(mLayoutManager);
            getBinding().recyclerView.addItemDecoration(new ConciergeOrderHistoryItemDecorator(requireContext()));
            getBinding().recyclerView.setAdapter(mAdapter);
            getBinding().recyclerView.post(() -> mLayoutManager.checkLToInvokeReachLoadMoreByDefaultIfNecessary());
        } else {
            mLayoutManager.setLoadMoreLinearLayoutManagerListener(isCanLoadMore ? mLoadMoreLinearLayoutManagerListener : null);
            if (isRefreshed) {
                mAdapter.setRefreshData(parseAdaptiveModel(data), isCanLoadMore);
            } else {
                mAdapter.setCanLoadMore(isCanLoadMore);
                if (!data.isEmpty()) {
                    mAdapter.addLoadMoreData(parseAdaptiveModel(data));
                } else if (!isCanLoadMore) {
                    mAdapter.removeLoadMoreLayout();
                }
                mLayoutManager.loadingFinished();
            }
        }

        getBinding().recyclerView.post(() -> {
            if (!mAdapter.canLoadMore()) {
                //Show end history item
                mAdapter.addOrRemoveEndOfHistoryItem(true);
            }
        });
    }

    private List<ItemConciergeOrderHistoryAdaptive> parseAdaptiveModel(List<ConciergeOrder> data) {
        return new ArrayList<>(data);
    }
}
