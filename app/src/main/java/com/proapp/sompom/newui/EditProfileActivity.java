package com.proapp.sompom.newui;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.proapp.sompom.intent.newintent.EditProfileIntent;
import com.proapp.sompom.intent.newintent.EditProfileIntentResult;
import com.proapp.sompom.newui.fragment.EditProfileFragment;
import com.proapp.sompom.utils.ConciergeNotificationUtil;
import com.resourcemanager.helper.LocaleManager;

import timber.log.Timber;

public class EditProfileActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(EditProfileFragment.newInstance(), EditProfileFragment.TAG);
    }

    public void recreateFragment(String local) {
        LocaleManager.applyChangeLanguage(this, local);
        startActivity(new EditProfileIntent(this));
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isThereAppConfigurationChanged()) {
            completelyRecreateActivity();
        }
    }

    @Override
    public void finish() {
        sendResult();
        super.finish();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Double wallet = (Double) intent.getSerializableExtra(ConciergeNotificationUtil.WALLET);
        if (wallet != null) {
            Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(EditProfileFragment.TAG);
            if (fragmentByTag instanceof EditProfileFragment) {
                ((EditProfileFragment) fragmentByTag).updateUserWallet(wallet);
            }
        }
    }

    private void sendResult() {
        boolean isChange = false;
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(EditProfileFragment.TAG);
        if (fragment instanceof EditProfileFragment) {
            isChange = ((EditProfileFragment) fragment).isChangeValue();
        }

        EditProfileIntentResult intentResult = new EditProfileIntentResult(isChange);
        setResult(RESULT_OK, intentResult);
    }
}
