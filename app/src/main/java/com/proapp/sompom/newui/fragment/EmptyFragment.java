package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentEmptyBinding;
import com.proapp.sompom.listener.OnHomeMenuClick;
import com.proapp.sompom.viewmodel.newviewmodel.EmptyFragmentViewModel;

/**
 * Created by Or Vitovongsak on 12/1/22.
 */
public class EmptyFragment extends AbsBindingFragment<FragmentEmptyBinding>
        implements OnHomeMenuClick {

    public static final String FIELD_TITLE = "FIELD_TITLE";
    public static final String FIELD_MESSAGE = "FIELD_MESSAGE";

    private EmptyFragmentViewModel mViewModel;

    public static EmptyFragment newInstance() {
        return new EmptyFragment();
    }

    public static EmptyFragment newInstance(String title,
                                            String message) {
        Bundle args = new Bundle();
        args.putString(FIELD_TITLE, title);
        args.putString(FIELD_MESSAGE, message);

        EmptyFragment emptyFragment = new EmptyFragment();
        emptyFragment.setArguments(args);

        return emptyFragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_empty;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            mViewModel = new EmptyFragmentViewModel(getContext(),
                    getArguments().getString(FIELD_TITLE),
                    getArguments().getString(FIELD_MESSAGE));
        } else {
            mViewModel = new EmptyFragmentViewModel(getContext());
        }
        setVariable(BR.viewModel, mViewModel);
        mViewModel.showEmptyMessage();
    }

    @Override
    public void onTabMenuClick() {

    }
}
