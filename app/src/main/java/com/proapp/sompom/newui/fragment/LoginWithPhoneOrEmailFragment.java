package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentLoginWithPhoneOrEmailBinding;
import com.proapp.sompom.viewmodel.AbsLoginViewModel;
import com.proapp.sompom.viewmodel.LoginWithPhoneOrEmailViewModel;

/**
 * Created by Veasna Chhom on 4/21/21.
 */
public class LoginWithPhoneOrEmailFragment extends AbsLoginFragment<FragmentLoginWithPhoneOrEmailBinding, LoginWithPhoneOrEmailViewModel> implements
        AbsLoginViewModel.AbsLoginViewModelListener {

    public static LoginWithPhoneOrEmailFragment newInstance() {

        Bundle args = new Bundle();

        LoginWithPhoneOrEmailFragment fragment = new LoginWithPhoneOrEmailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected boolean forceRebuildControllerComponent() {
        return true;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_login_with_phone_or_email;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel = new LoginWithPhoneOrEmailViewModel(requireContext(), this);
        setVariable(BR.viewModel, mViewModel);
    }
}
