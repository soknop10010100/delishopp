package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentLoginWithPhoneOnlyBinding;
import com.proapp.sompom.viewmodel.LoginWithPhoneOnlyViewModel;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

/**
 * Created by Veasna Chhom on 4/21/21.
 */
public class LoginWithPhoneOnlyFragment extends AbsLoginFragment<FragmentLoginWithPhoneOnlyBinding, LoginWithPhoneOnlyViewModel> implements
        LoginWithPhoneOnlyViewModel.LoginWithPhoneOnlyViewModelListener {

    public static LoginWithPhoneOnlyFragment newInstance() {

        Bundle args = new Bundle();

        LoginWithPhoneOnlyFragment fragment = new LoginWithPhoneOnlyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected boolean forceRebuildControllerComponent() {
        return true;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_login_with_phone_only;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel = new LoginWithPhoneOnlyViewModel(requireContext(), this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public CountryCodePicker getCountryCodePicker() {
        return getBinding().ccp;
    }
}
