package com.proapp.sompom.newui;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.proapp.sompom.intent.SelectAddressIntent;
import com.proapp.sompom.newui.fragment.SearchAddressFragment;
import com.proapp.sompom.newui.fragment.SelectAddressFragment;
import com.proapp.sompom.utils.KeyboardUtil;
import com.proapp.sompom.R;
import com.proapp.sompom.model.SearchAddressResult;

public class SelectAddressActivity extends AbsDefaultActivity {

    private SearchAddressResult mSearchAddressResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SelectAddressIntent intent = new SelectAddressIntent(getIntent());
        mSearchAddressResult = intent.getSearchAddressResult();
        setFragment(R.id.containerView,
                SearchAddressFragment.newInstance(mSearchAddressResult),
                SearchAddressFragment.TAG);
    }

    public void navigateToSetLocationOnMap() {
        KeyboardUtil.hideKeyboard(this);
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right,
                R.anim.exit_to_left);
        fragmentTransaction.replace(R.id.containerView,
                SelectAddressFragment.newInstance(mSearchAddressResult),
                SelectAddressFragment.TAG);
        fragmentTransaction.commit();
    }

    public void backToSelectAddress() {
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left,
                R.anim.exit_to_right, 0, 0);
        fragmentTransaction.replace(R.id.containerView,
                SearchAddressFragment.newInstance(mSearchAddressResult),
                SearchAddressFragment.TAG);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getFragment(SelectAddressFragment.TAG);
        if (fragment instanceof SelectAddressFragment) {
            backToSelectAddress();
        } else {
            super.onBackPressed();
        }
    }
}
