package com.proapp.sompom.newui.dialog;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ChatImageFullScreenPagerAdapter;
import com.proapp.sompom.databinding.DialogFullMediaScreenBinding;
import com.proapp.sompom.helper.SaveImageHelper;
import com.proapp.sompom.helper.SaveVideoHelper;
import com.proapp.sompom.intent.ForwardIntent;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.PostContextMenuItem;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.emun.PostContextMenuItemType;
import com.proapp.sompom.utils.IntentUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.ToastUtil;
import com.proapp.sompom.viewmodel.newviewmodel.ChatImageFullScreeDialogViewModel;
import com.resourcemanager.helper.FontHelper;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by nuonveyo on 8/30/18.
 */

public class FullScreenMediaDialog extends AbsBindingFragmentDialog<DialogFullMediaScreenBinding>
        implements ChatImageFullScreenPagerAdapter.ChatImageFullScreenPagerAdapterListener {

    private static final String ENABLE_FORWARD_OPTION = "ENABLE_FORWARD_OPTION";

    private SaveImageHelper mSaveImageHelper;
    private SaveVideoHelper mSaveVideoHelper;
    private List<Media> mProductMedias;
    private ChatImageFullScreenPagerAdapter mAdapter;
    private ChatImageFullScreenDialogListener mChatImageFullScreenDialogListener;

    public static FullScreenMediaDialog newInstance(List<Media> list,
                                                    int position,
                                                    boolean enableForwardOption) {
        Bundle args = new Bundle();
        ArrayList<Media> media = new ArrayList<>(list);
        args.putBoolean(ENABLE_FORWARD_OPTION, enableForwardOption);
        args.putParcelableArrayList(SharedPrefUtils.DATA, media);
        args.putInt(SharedPrefUtils.ID, position);
        FullScreenMediaDialog fragment = new FullScreenMediaDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_ChatImageFullScreenDialogStyle);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_full_media_screen;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Timber.i("onViewCreated");
        mSaveImageHelper = new SaveImageHelper(getActivity());
        mSaveVideoHelper = new SaveVideoHelper(getActivity());
        if (getArguments() != null) {
            mProductMedias = getArguments().getParcelableArrayList(SharedPrefUtils.DATA);
            int position = getArguments().getInt(SharedPrefUtils.ID);
            mAdapter = new ChatImageFullScreenPagerAdapter(getActivity(), mProductMedias, this);
            mAdapter.setOnClickListener(this::checkToHideToolbar);
            getBinding().viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {
                    Timber.e("onPageScrolled " + i + " i1 " + i1 + " v " + v);
                }

                @Override
                public void onPageSelected(int i) {
                    mAdapter.resetOtherVideoPlayerPlayPosition(i);
                    mAdapter.checkToPlayUrlMP4Gif(i);
                    if (getBinding().backContainer.getVisibility() == View.GONE) {
                        checkToHideToolbar();
                    }
                }

                @Override
                public void onPageScrollStateChanged(int i) {
                    Timber.e("onPageScrollStateChanged " + i);

                }
            });
            getBinding().viewPager.setAdapter(mAdapter);
            getBinding().viewPager.setCurrentItem(position);
            getBinding().viewPager.postDelayed(() -> mAdapter.playVideoDirectly(position), 100);
        }

        ChatImageFullScreeDialogViewModel viewModel = new ChatImageFullScreeDialogViewModel(getActivity(),
                new ChatImageFullScreeDialogViewModel.Callback() {
                    @Override
                    public void onMoreButtonClick() {
                        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
                        dialog.setPopupStyle(R.style.FullScreenPopupStyle);
                        List<PostContextMenuItem> items = new ArrayList<>();
                        items.add(new PostContextMenuItem(PostContextMenuItemType.SAVE_IMAGE_CLICK.getId(), R.string.code_bookmark,
                                R.string.media_preview_screen_menu_save_title,
                                -1,
                                FontHelper.getMagicIconFontResource()));
                        items.add(new PostContextMenuItem(PostContextMenuItemType.SHARE_CLICK.getId(),
                                R.string.code_jump_right_up,
                                R.string.media_preview_screen_menu_share_title,
                                -1,
                                FontHelper.getMagicIconFontResource()));
                        if (getArguments() != null && getArguments().getBoolean(ENABLE_FORWARD_OPTION, false)) {
                            items.add(new PostContextMenuItem(PostContextMenuItemType.FORWARD_CLICK.getId(),
                                    R.string.code_share, R.string.chat_popup_forward_title,
                                    -1,
                                    FontHelper.getFlatIconFontResource()));
                        }
                        dialog.addItem(items);
                        dialog.setOnItemClickListener(result -> {
                            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
                            if (itemType == PostContextMenuItemType.SAVE_IMAGE_CLICK) {
                                if (mChatImageFullScreenDialogListener != null) {
                                    mChatImageFullScreenDialogListener.onSaveClicked(mProductMedias.get(getCurrentItem()));
                                }
                                dialog.dismiss();
                            } else if (itemType == PostContextMenuItemType.SHARE_CLICK) {
                                if (mProductMedias.get(getCurrentItem()).getType() == MediaType.VIDEO) {
                                    IntentUtil.shareVideoIntent(getActivity(), mProductMedias.get(getCurrentItem()).getUrl());
                                } else {
                                    mSaveImageHelper.download(mProductMedias.get(getCurrentItem()), new SaveImageHelper.OnCallback() {
                                        @Override
                                        public void onFail() {
                                            Timber.e("share image was fail");
                                        }

                                        @Override
                                        public void onSuccess(@NonNull Bitmap resource) {

                                            IntentUtil.shareIntent(getActivity(), resource);
                                        }
                                    });
                                }
                                dialog.dismiss();

                            } else if (itemType == PostContextMenuItemType.FORWARD_CLICK) {
                                startActivity(new ForwardIntent(getActivity(),
                                        mProductMedias.get(getBinding().viewPager.getCurrentItem())));
                            }
                        });
                        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
                    }

                    @Override
                    public void onClick() {
                        dismiss();
                    }
                });
        setVariable(BR.viewModel, viewModel);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        Timber.i("onDismiss");
        if (mAdapter != null) {
            mAdapter.releaseVideoPlayer();
            mAdapter.disableAutoOrientation();
        }
        super.onDismiss(dialog);
        setScreenToPortrait();
    }

    public void saveMedia(Media media) {
        if (media.getType() == MediaType.VIDEO ||
                media.getType() == MediaType.GIF) {
            mSaveVideoHelper.download(media, new SaveVideoHelper.OnVideoSaveCallBack() {
                @Override
                public void onFail(String message) {
                    ToastUtil.showToast(getContext(), R.string.video_saved_fail, true);

                }

                @Override
                public void onSuccess() {
                    ToastUtil.showToast(getContext(), R.string.video_saved, false);
                }
            });
        } else {
            mSaveImageHelper.startSave(media, new SaveImageHelper.OnCallback() {
                @Override
                public void onFail() {
                    if (getContext() instanceof AppCompatActivity) {
                        ((AppCompatActivity) getContext()).runOnUiThread(() -> ToastUtil.showToast(getContext(),
                                R.string.toast_save_file_fail,
                                true));
                    }
                }

                @Override
                public void onSuccess(@NonNull Bitmap resource) {
                    if (getContext() instanceof AppCompatActivity) {
                        ((AppCompatActivity) getContext()).runOnUiThread(() -> ToastUtil.showToast(getContext(),
                                R.string.toast_file_save,
                                false));
                    }
                }
            });
        }
    }

    private void setScreenToPortrait() {
        if (!isDetached()) {
            requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    private int getCurrentItem() {
        return getBinding().viewPager.getCurrentItem();
    }

    private void checkToHideToolbar() {
        if (getBinding().backContainer.getVisibility() == View.GONE) {
            getBinding().backContainer.setVisibility(View.VISIBLE);
            getBinding().more.setVisibility(View.VISIBLE);
        } else {
            getBinding().backContainer.setVisibility(View.GONE);
            getBinding().more.setVisibility(View.GONE);
        }
    }


    @Override
    public void onVideoPlayStateChang(boolean isPlay) {
        if (isPlay && getBinding().backContainer.getVisibility() == View.VISIBLE) {
            checkToHideToolbar();
        } else if (!isPlay && getBinding().backContainer.getVisibility() == View.GONE) {
            checkToHideToolbar();
        }
    }

    public void setChatImageFullScreenDialogListener(ChatImageFullScreenDialogListener chatImageFullScreenDialogListener) {
        mChatImageFullScreenDialogListener = chatImageFullScreenDialogListener;
    }

    public interface ChatImageFullScreenDialogListener {
        void onSaveClicked(Media media);
    }
}
