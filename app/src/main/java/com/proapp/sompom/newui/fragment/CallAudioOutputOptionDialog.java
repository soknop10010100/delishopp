package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.DialogFragment;

import com.proapp.sompom.R;
import com.proapp.sompom.model.emun.CallAudioOutputType;
import com.proapp.sompom.viewmodel.newviewmodel.CallAudioOutputDialogViewModel;

public class CallAudioOutputOptionDialog extends DialogFragment implements CallAudioOutputDialogViewModel
        .CallAudioOutputDialogViewModelListener {

    private static final String SELECTED_TYPE = "SELECTED_TYPE";

    private CallAudioOutputOptionDialogListener mListener;

    public static CallAudioOutputOptionDialog newInstance(CallAudioOutputType type) {

        Bundle args = new Bundle();
        args.putInt(SELECTED_TYPE, type.getId());

        CallAudioOutputOptionDialog fragment = new CallAudioOutputOptionDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setListener(CallAudioOutputOptionDialogListener listener) {
        mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(inflater,
                R.layout.dialog_fragment_call_audio_output_option,
                container,
                true);
        if (getArguments() != null) {
            viewDataBinding.setVariable(BR.viewModel,
                    new CallAudioOutputDialogViewModel(CallAudioOutputType.getType(getArguments().getInt(SELECTED_TYPE, 0)),
                            this));
        }
        return viewDataBinding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomWidthDialogStyle);
    }

    @Override
    public void onItemSelected(CallAudioOutputType type) {
        if (mListener != null) {
            mListener.onItemSelected(type);
        }
        dismiss();
    }

    public interface CallAudioOutputOptionDialogListener {

        void onItemSelected(CallAudioOutputType type);
    }
}
