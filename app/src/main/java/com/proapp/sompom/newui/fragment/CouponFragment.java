package com.proapp.sompom.newui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ConciergeCouponAdapter;
import com.proapp.sompom.databinding.FragmentCouponBinding;
import com.proapp.sompom.intent.CouponIntent;
import com.proapp.sompom.model.concierge.ConciergeCoupon;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.CouponDataManager;
import com.proapp.sompom.viewmodel.CouponFragmentViewModel;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Chhom Veasna on 7/21/22.
 */

public class CouponFragment extends AbsBindingFragment<FragmentCouponBinding> implements CouponFragmentViewModel.CouponFragmentViewModelListener {

    public static final String SELECTED_COUPON = "SELECTED_COUPON";

    @Inject
    public ApiService mApiService;
    private CouponFragmentViewModel mViewModel;
    private ConciergeCouponAdapter mAdapter;

    public static CouponFragment newInstance(String selectedCouponId) {

        Bundle args = new Bundle();
        args.putString(CouponIntent.SELECTED_COUPON_ID, selectedCouponId);

        CouponFragment fragment = new CouponFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_coupon;
    }

    @Nullable
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        getBinding().layoutToolbar.toolbarTitle.setText(getString(R.string.shop_checkout_popup_coupon_title_label));
        setToolbar(getBinding().layoutToolbar.toolbar, true);
        mViewModel = new CouponFragmentViewModel(requireContext(), new CouponDataManager(requireContext(),
                mApiService,
                getArguments().getString(CouponIntent.SELECTED_COUPON_ID)),
                this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onDataLoadSuccess(List<ConciergeCoupon> data) {
        mAdapter = new ConciergeCouponAdapter(data, new ConciergeCouponAdapter.ConciergeCouponAdapterListener() {
            @Override
            public void onItemSelected() {
                mViewModel.checkToEnableApplyButton(true);
            }
        });
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        getBinding().recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onApplyClicked() {
        ConciergeCoupon selection = mAdapter.getSelection();
        if (selection != null) {
            Intent data = new Intent();
            data.putExtra(SELECTED_COUPON, selection);
            requireActivity().setResult(AppCompatActivity.RESULT_OK, data);
            requireActivity().finish();
        }
    }
}
