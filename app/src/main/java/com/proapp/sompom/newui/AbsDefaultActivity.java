package com.proapp.sompom.newui;

import androidx.fragment.app.Fragment;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.ActivityDefaultBinding;

/**
 * Created by He Rotha on 10/20/17.
 */

public abstract class AbsDefaultActivity extends AbsBindingActivity<ActivityDefaultBinding> {

    @Override
    public int getLayoutRes() {
        return R.layout.activity_default;
    }

    public void setFragment(Fragment fragment, String tag) {
        setFragment(R.id.containerView, fragment, tag);
    }
}
