package com.proapp.sompom.newui.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.call.CallingService;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.utils.SharedPrefUtils;

import retrofit2.Response;

/**
 * Created by He Rotha on 3/18/19.
 */
public class SessionDialog extends MessageDialog {

    private SessionDialogListener mListener;

    public static SessionDialog newInstance() {
        return new SessionDialog();
    }

    public void setListener(SessionDialogListener listener) {
        mListener = listener;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setTitle(getString(R.string.popup_session_expired_title));
        setCancelable(false);
        setMessage(getString(R.string.popup_session_expired_description));
        if (((MainApplication) requireActivity().getApplication()).isCallInProgressNow()) {
            ((MainApplication) requireActivity().getApplication()).endProgressCall();
        }
        setLeftText(getString(R.string.setting_change_language_dialog_ok_button), view1 -> {
            if (mListener != null) {
                mListener.onOkButtonClicked();
            }
            Intent loginIntent = ApplicationHelper.getLoginIntent(requireContext(),
                    true);
            loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            requireActivity().startActivity(loginIntent);
        });

        //Will clear all user data anc chat socket and call services.
        if (SharedPrefUtils.isLogin(requireContext())) {
            ConciergeHelper.clearLocalBasket(requireContext().getApplicationContext(),
                    new OnCallbackListener<Response<Object>>() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                            clearAllRemainingDataAndServices();
                        }

                        @Override
                        public void onComplete(Response<Object> result) {
                            clearAllRemainingDataAndServices();
                        }
                    });
        }
        super.onViewCreated(view, savedInstanceState);
    }

    private void clearAllRemainingDataAndServices() {
        SharedPrefUtils.clearValue(requireContext());
        SharedPrefUtils.setAppFeature(requireContext(), null);
        //Stop chat and call services
        if (requireActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) requireActivity()).bindCall(CallingService.CallServiceBinder::logout,
                    false);
            if (((AbsBaseActivity) requireActivity()).getChatBinder() != null) {
                ((AbsBaseActivity) requireActivity()).getChatBinder().stopService();
            }
        }
    }

    public interface SessionDialogListener {
        void onOkButtonClicked();
    }
}
