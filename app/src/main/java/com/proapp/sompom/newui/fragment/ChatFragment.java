
package com.proapp.sompom.newui.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.ChatAdapter;
import com.proapp.sompom.broadcast.NetworkBroadcastReceiver;
import com.proapp.sompom.broadcast.SoundEffectService;
import com.proapp.sompom.broadcast.chat.PlayAudioService;
import com.proapp.sompom.chat.AbsChatBinder;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.chat.call.IncomingCallDataHolder;
import com.proapp.sompom.chat.call.agora.VOIPCallMessageListener;
import com.proapp.sompom.chat.listener.ChatListener;
import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.database.LinkPreviewDb;
import com.proapp.sompom.database.MessageDb;
import com.proapp.sompom.databinding.DialogFragmentChatBinding;
import com.proapp.sompom.databinding.ItemSeenNameTextviewBinding;
import com.proapp.sompom.databinding.ListItemNewGroupWelcomeBinding;
import com.proapp.sompom.helper.AbsCallService;
import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.helper.ChatHelper;
import com.proapp.sompom.helper.CheckPermissionCallbackHelper;
import com.proapp.sompom.helper.ConversationHelper;
import com.proapp.sompom.helper.FileDownloadHelper;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.helper.MessageStatusHelper;
import com.proapp.sompom.helper.MyHandler;
import com.proapp.sompom.helper.ReceivingTypingHelper;
import com.proapp.sompom.helper.RecordAudioHelper;
import com.proapp.sompom.helper.UserHelper;
import com.proapp.sompom.helper.WallStreetHelper;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.intent.ChatIntent;
import com.proapp.sompom.intent.ForwardIntent;
import com.proapp.sompom.licence.db.LicenseSynchronizationDb;
import com.proapp.sompom.licence.model.SynchroniseData;
import com.proapp.sompom.listener.ChatMediaUploadListener;
import com.proapp.sompom.listener.OnChatItemListener;
import com.proapp.sompom.model.BaseChatModel;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.emun.OpenChatScreenType;
import com.proapp.sompom.model.emun.PostContextMenuItemType;
import com.proapp.sompom.model.request.JoinChannelStatusUpdateBody;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.model.result.Product;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.AbsSoundControllerActivity;
import com.proapp.sompom.newui.dialog.FullScreenMediaDialog;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.newui.dialog.PostContextMenuDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ChatDataManager;
import com.proapp.sompom.utils.CopyTextUtil;
import com.proapp.sompom.utils.DownloaderUtils;
import com.proapp.sompom.utils.MediaUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.utils.ToastUtil;
import com.proapp.sompom.viewmodel.ChatMediaViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ChatDialogViewModel;
import com.proapp.sompom.viewmodel.newviewmodel.ListItemNewGroupWelcomeViewModel;
import com.proapp.sompom.widget.MyItemAnimator;
import com.proapp.sompom.widget.ChatLayoutManager;
import com.proapp.sompom.widget.call.CallScreenView;
import com.sompom.baseactivity.PermissionCheckHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by nuonveyo on 6/25/18.
 */

public class ChatFragment extends AbsBindingFragment<DialogFragmentChatBinding>
        implements ChatListener, AbsBaseActivity.OnServiceListener, VOIPCallMessageListener, NetworkBroadcastReceiver.NetworkListener {

    private static final int SCROLL_DOWN_BUTTON_VISIBLE_TARGET = -1000;

    private final RecyclerView.SimpleOnItemTouchListener mDisableTouchrecyclerViewListener = new RecyclerView.SimpleOnItemTouchListener() {
        @Override
        public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
            return true;
        }
    };
    @Inject
    public ApiService mApiService;
    private ChatDialogViewModel mViewModel;
    private ChatAdapter mAdapter;
    private final MyHandler mScrollToBottomHandler = new MyHandler(200, () -> {
        getBinding().recyclerView.scrollBy(0, getBinding().recyclerView.getContext()
                .getResources().getDimensionPixelSize(R.dimen.chat_radius));
        getBinding().recyclerView.smoothScrollToPosition(mAdapter.getItemCount());
    });
    private ChatLayoutManager.OnLoadMoreCallback mLoadMoreListener;
    private ChatLayoutManager mLayoutManager;
    private SocketService.SocketBinder mSocketBinder;
    private User mRecipient;
    private ReceivingTypingHelper mReceivingTypingHelper;
    private User mMyUser;
    private String mConversationID;
    private RecordAudioHelper mRecordAudioHelper;
    private Product mProduct;
    private boolean mIsInBg = false;
    private PlayAudioService mPlayAudioService;
    private Conversation mConversation;
    public boolean checkAudioChat;
    private BroadcastReceiver mCallingScreenOpenActionReceiver;
    private boolean mIsCallingOpen;
    private ChatMediaUploadListener mChatMediaUploadListener;
    private BroadcastReceiver mJoinGroupCallStatusReceiver;
    private BroadcastReceiver mCallMessageEvenReceiver;
    private boolean mIsRemoving;
    private int mAnimateViewItemPosition = -1;
    private boolean mHasScrolled;
    private Chat mQueueMessage;
    private String mSearchKeyword;
    private boolean mHasSetChatToAdapter;
    private MessageStatusHelper mMessageStatusHelper;
    private OnChatItemListener mOnChatItemListener;

    public static ChatFragment newInstance(Conversation conversation,
                                           User recipient,
                                           Product product,
                                           String conversationID,
                                           Chat selectedChat,
                                           String searchKeyWord,
                                           OpenChatScreenType openChatScreenType) {
        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.CONVERSATION, conversation);
        args.putParcelable(SharedPrefUtils.USER_ID, recipient);
        args.putParcelable(SharedPrefUtils.PRODUCT, product);
        args.putString(SharedPrefUtils.ID, conversationID);
        args.putParcelable(ChatIntent.CHAT, selectedChat);
        args.putString(ChatIntent.SEARCH_KEYWORD, searchKeyWord);
        args.putInt(ChatIntent.OPEN_CHAT_TYPE, openChatScreenType.getValue());
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_fragment_chat;
    }

    @Override
    protected boolean enableGroupUpdateCallback() {
        return true;
    }

    @Override
    public void onGroupUpdated(String conversationId, Conversation conversation, boolean isRemovedMe) {
        if (isRemovedMe) {
            if (mConversation != null && mConversation.getId().matches(conversationId)) {
                /*
                Current user has been removed from current opening conversation. So will close the screen
                immediately.
                 */
                requireActivity().finish();
            }
        } else {
            //Update the current conversation if the update notification match this group.
            if (mConversation != null && mConversation.getId().matches(conversation.getId())) {
                mConversation = conversation;
                if (mAdapter != null) {
                    mAdapter.updateConversation(conversation);
                }
                if (mViewModel != null) {
                    mViewModel.updateGroupConversation(conversation);
                }
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mMessageStatusHelper = new MessageStatusHelper();
        getControllerComponent().inject(this);
        initReceivingTypingHelper();
        initChatMediaUploadListener();
        registerNetworkChangeStatusCallback();
        registerUpdateJoinGroupCallStatusReceiver();
        initCallMessageEvenReceiver();
        registerCallingScreenOpenReceiver();
        bindGifView();

        if (getActivity() instanceof AbsBaseActivity) {
            mPlayAudioService = ((AbsSoundControllerActivity) getActivity()).getPlayAudioService();
            ((AbsBaseActivity) getActivity()).addOnServiceListener(this);
        }

        mRecordAudioHelper = new RecordAudioHelper(getActivity(),
                getBinding().voiceRippleView,
                getBinding().duration,
                getBinding().textViewSlide,
                new RecordAudioHelper.OnCallback() {
                    @Override
                    public void onStopRecord(File recordFile) {
                        if (!mViewModel.isAbleToSendMessage()) {
                            showSnackBar(R.string.chat_popup_no_recipient, true);
                            return;
                        }

                        getBinding().recyclerView.removeOnItemTouchListener(mDisableTouchrecyclerViewListener);
                        mViewModel.mIsRecording.set(false);

                        if (recordFile == null) {
                            return;
                        }

                        if (getContext() instanceof AbsBaseActivity) {
                            ((AbsBaseActivity) getContext()).startLoginWizardActivity(new LoginActivityResultCallback() {
                                @Override
                                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                                    Media productMedia = new Media();
                                    productMedia.setUrl(recordFile.getAbsolutePath());
                                    productMedia.setType(MediaType.AUDIO);
                                    productMedia.setDuration(MediaUtil.getAudioDuration(recordFile.getAbsolutePath()));

                                    if (isGroupChat()) {
                                        sendMessage(generateGroupChat(null, Collections.singletonList(productMedia)));
                                    } else {
                                        Chat chat = Chat.getNewChat(getContext(), mRecipient, mProduct);
                                        chat.setChannelId(mConversationID);
                                        chat.setMediaList(Collections.singletonList(productMedia));

                                        sendMessage(chat);
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onStartRecord() {
                        getBinding().recyclerView.addOnItemTouchListener(mDisableTouchrecyclerViewListener);
                        mViewModel.mIsRecording.set(true);
                    }

                    @Override
                    public void onClick() {
                        mViewModel.onSendButtonClick();
                    }
                });
        getBinding().backPressButton.setOnClickListener(v -> requireActivity().finish());
        mLoadMoreListener = new ChatLayoutManager.OnLoadMoreCallback() {
            @Override
            public void onLoadMoreFromTop() {
                Timber.i("onLoadMoreFromTop");
                mViewModel.loadMoreData(new ChatDialogViewModel.OnLoadMoreCallback() {
                    @Override
                    public Chat getOldestChat() {
                        return mAdapter.getOldestChatInList();
                    }

                    @Override
                    public void onFail(ErrorThrowable ex) {
//                        Timber.i("onLoadMoreFromTop onFail");
                        stopLoadMore(true);
                        checkToAddWelcomeSupportConversationItem();
                    }

                    @Override
                    public void onComplete(List<BaseChatModel> result, boolean canLoadMore) {
//                        Timber.i("onLoadMoreFromTop success: " + result.size() + ", canLoadMore: " + canLoadMore + ", data: " + new Gson().toJson(result));
                        mAdapter.setCanLoadMore(canLoadMore);
                        if (!canLoadMore) {
                            stopLoadMore(true);
                        }
                        if (!result.isEmpty()) {
                            mAdapter.addByLoadMore(result, canLoadMore);
                        }
                        if (result.isEmpty() || !canLoadMore) {
                            checkToAddWelcomeSupportConversationItem();
                        }
                        mLayoutManager.loadingFinished();
                    }
                });
            }

            @Override
            public void onLoadMoreFromBottom() {
                Timber.i("onLoadMoreFromBottom");
                if (mViewModel.isJumpSearch() && mAdapter.isCanLoadMoreFromBottom()) {
                    mViewModel.jumpSearchMessage(true);
                } else {
                    //Consider as loading finished. Must call this method to allow another load more
                    //can operate their work.
                    mLayoutManager.loadingFinished();
                }
            }

            @Override
            public void onReachedLoadMoreFromTopByDefault() {
                Timber.i("onReachedLoadMoreFromTopByDefault");
                onLoadMoreFromTop();
            }

            @Override
            public void onReachedLoadMoreFromBottomByDefault() {
                Timber.i("onReachedLoadMoreFromBottomByDefault");
                onLoadMoreFromBottom();
            }
        };

        getBinding().voiceRippleView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (getActivity() == null) {
                    return;
                }
                int space = getActivity().getResources().getDimensionPixelSize(R.dimen.chat_edit_text_margin_end);

                int sendLayoutHeight = getBinding().sendLayout.getHeight();

                final int height = getBinding().voiceRippleView.getHeight();

                float spaceX = getBinding().voiceRippleView.getTranslationX() + (height - space) / 2;
                float spaceY = getBinding().voiceRippleView.getTranslationY() + (height - sendLayoutHeight) / 2;

                getBinding().voiceRippleView.setTranslationX(spaceX);
                getBinding().voiceRippleView.setTranslationY(spaceY);
                mRecordAudioHelper.setMinScroll(getBinding().sendLayout.getWidth() / 2 - space);
                getBinding().voiceRippleView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        getBinding().recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            private int mLastScrollUp;

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                mLastScrollUp += dy;
                mHasScrolled = true;
                if (mLastScrollUp > SCROLL_DOWN_BUTTON_VISIBLE_TARGET &&
                        mViewModel.getScrollDownButtonVisibility().get() != View.GONE) {
                    mViewModel.setScrollDownButtonVisibility(View.GONE);
                }
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                Timber.i("onScrollStateChanged: " + newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    mHasScrolled = false;
                    //Check to animate scroll to position if any
                    if (mAnimateViewItemPosition >= 0 && getBinding().recyclerView.getLayoutManager() != null) {
                        int positionHolder = mAnimateViewItemPosition;
                        //Reset the value immediately
                        mAnimateViewItemPosition = -1;
                        animateItemViewWhenScrollTo(positionHolder);
                    }

                    if (mLastScrollUp <= SCROLL_DOWN_BUTTON_VISIBLE_TARGET &&
                            mViewModel.getScrollDownButtonVisibility().get() != View.VISIBLE) {
                        mViewModel.setScrollDownButtonVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    private void initReceivingTypingHelper() {
        mReceivingTypingHelper = new ReceivingTypingHelper(new ReceivingTypingHelper.ReceivingTypingHelperListener() {
            @Override
            public void onAddReceivingTyping(User sender) {
                mAdapter.checkToDisplayReceivingTypingIndicator(sender, true);
                scrollToBottom(true);
            }

            @Override
            public void onDisplayReceivingTypingTimeOutOrRemoved(User sender) {
                mAdapter.checkToDisplayReceivingTypingIndicator(sender, false);
            }
        });
    }

    private void registerNetworkChangeStatusCallback() {
        if (requireActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) requireActivity()).addNetworkStateChangeListener(this);
        }
    }

    private void stopLoadMore(boolean isStopFromTop) {
        mLayoutManager.setOnLoadMoreListener(null);
        mLayoutManager.loadingFinished();
        if (isStopFromTop) {
            mAdapter.setCanLoadMore(false);
            mAdapter.notifyItemRemoved(0);
        } else {
            //Stop from bottom
            int lastItemCount = mAdapter.getItemCount();
            mAdapter.setCanLoadMoreFromBottom(false);
            mAdapter.notifyItemRemoved(lastItemCount);
        }
    }

    private void animateItemViewWhenScrollTo(int position) {
        View viewByPosition = getBinding().recyclerView.getLayoutManager().findViewByPosition(position);
//        Timber.i("position: " + position + ", viewByPosition: " + viewByPosition);
        if (viewByPosition != null) {
            View viewById = viewByPosition.findViewById(R.id.message);
            if (viewById != null) {
                AnimationHelper.animateBounce(viewById, null);
            }
        }
    }

    private void registerUpdateJoinGroupCallStatusReceiver() {
        mJoinGroupCallStatusReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                JoinChannelStatusUpdateBody data = intent.getParcelableExtra(SharedPrefUtils.DATA);
                if (data != null) {
                    Timber.i("onReceive: JoinGroupCallStatusReceiver: " + new Gson().toJson(data));
                    mViewModel.updateJoinGroupCallStatus(data.getChannelOpen(), data.getVideo());
                }
            }
        };
        requireActivity().registerReceiver(mJoinGroupCallStatusReceiver,
                new IntentFilter(ChatHelper.JOIN_GROUP_CALL_STATUS_UPDATE));
    }

    private void initCallMessageEvenReceiver() {
        mCallMessageEvenReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Chat newMessage = intent.getParcelableExtra(CallingFragment.NEW_MESSAGE);
                if (newMessage != null) {
                    newMessage.setStatus(MessageState.SENDING);
                    Timber.i("onReceive: CallMessageEvenReceiver: " + newMessage.getChatType());
                    if (mAdapter.addLatestChat(newMessage)) {
                        scrollToBottom(false);
                        getBinding().messageInput.requestFocus();
                    }
                    SoundEffectService.playSound(requireActivity(), SoundEffectService.SoundFile.SEND_MESSAGE);
                }
            }
        };
        requireContext().registerReceiver(mCallMessageEvenReceiver,
                new IntentFilter(CallingFragment.CALL_MESSAGE_EVEN));
    }

    public void checkSynchronizeUi() {
        SynchroniseData mSynchronizeData = LicenseSynchronizationDb.readSynchroniseData(getContext());
        if (mSynchronizeData != null) {
            if (mSynchronizeData.getMessage() != null) {
                if (!mSynchronizeData.getMessage().isEnableVideoChat() || !mSynchronizeData.getMessage().isEnableImageChat()) {
                    mViewModel.setCameraVisibility(false);
                    mViewModel.setPictureVisibility(false);
                } else {
                    mViewModel.setCameraVisibility(true);
                    mViewModel.setPictureVisibility(true);
                }
                if (!mSynchronizeData.getMessage().isEnableFileChat()) {
                    mViewModel.setFileVisibility(false);
                } else {
                    mViewModel.setFileVisibility(true);
                }
                if (!mSynchronizeData.getMessage().isEnableGifChat()) {
                    mViewModel.setGifVisibility(false);
                } else {
                    mViewModel.setGifVisibility(true);
                }
                if (!mSynchronizeData.getMessage().isEnableAudioChat()) {
                    mRecordAudioHelper.setIcon(RecordAudioHelper.Type.SEND);
                    checkAudioChat = false;
                } else {
                    checkAudioChat = true;
                }
            }
        }
    }

    private void initChatMediaUploadListener() {
        mChatMediaUploadListener = new ChatMediaUploadListener() {
            @Override
            public void onUploadSuccess(String requestId,
                                        Chat chat,
                                        String mediaId,
                                        String imageUrl,
                                        String thumb) {
                if (mAdapter != null) {
                    mAdapter.onUploadOrDownloadSuccess(true, requestId, chat, mediaId, imageUrl, thumb);
                }
            }

            @Override
            public void onUploadFailed(String requestId,
                                       Chat chat,
                                       String mediaId,
                                       Exception ex) {
                showSnackBar(R.string.toast_upload_file_failed, true);
                if (mAdapter != null) {
                    mAdapter.onUploadOrDownloadFailed(true,
                            requestId, chat, mediaId, ex);
                }
            }

            @Override
            public void onUploadProgressing(String requestId,
                                            Chat chat,
                                            String mediaId,
                                            int percent,
                                            long percentBytes,
                                            long totalBytes) {
                if (mAdapter != null) {
                    mAdapter.onUploadOrDownloadProgressing(true,
                            requestId,
                            chat,
                            mediaId,
                            percent,
                            percentBytes,
                            totalBytes);
                }
            }

            @Override
            public void onCancel(String requestId, Chat chat, String mediaId) {
                //Do nothing.
            }
        };
    }

    private void registerCallingScreenOpenReceiver() {
        mCallingScreenOpenActionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isOpen = intent.getBooleanExtra(CallScreenView.IS_OPEN, false);
                Timber.i("onReceive of mCallingScreenOpenActionReceiver: " + isOpen);
                mIsCallingOpen = isOpen;
            }
        };

        requireActivity().registerReceiver(mCallingScreenOpenActionReceiver,
                new IntentFilter(CallScreenView.IN_ACTION_CALL_EVENT));
    }

    private void extractPassingData() {
        if (getArguments() != null) {
            mSearchKeyword = getArguments().getString(ChatIntent.SEARCH_KEYWORD);
            Timber.i("mSearchKeyword: " + mSearchKeyword);
            mConversation = getArguments().getParcelable(SharedPrefUtils.CONVERSATION);
            mProduct = getArguments().getParcelable(SharedPrefUtils.PRODUCT);
            mRecipient = getArguments().getParcelable(SharedPrefUtils.USER_ID);
            if (isGroupChat()) {
                mConversationID = mConversation.getGroupId();
            } else {
                mConversationID = getArguments().getString(SharedPrefUtils.ID);
                Timber.i("mConversationID: " + mConversationID);
            }
        }
        checkConversation();
        initAdapter();
    }

    public String getConversationId() {
        return mConversationID;
    }

    private void checkConversation() {
        if (mConversation == null) {
            Timber.e("Getting conversation from db");
            mConversation = ConversationDb.getConversationById(requireContext(), mConversationID);
            if (mConversation == null) {
                Timber.e("creating new conversation");
                /*
                  In a specific case, the new conversation has started and there is no data has previously
                  saved in local, that case is when we start a conversation screen without passing conversation
                  object and it is being used in open conversation from user list or user dialog screen which is
                  possible for now only for individual chatting and we have to recheck this condition
                  if there will be the possibility in the future to start a group chat without passing
                  a conversation object to the conversation screen.
                 */
                mConversation = new Conversation();
                mConversation.setId(mConversationID);
                List<User> userList = new ArrayList<>();
                if (mRecipient != null) {
                    userList.add(mRecipient);
                }
                userList.add(SharedPrefUtils.getUser(requireContext()));
                mConversation.setParticipants(userList);
                ConversationDb.save(requireContext(), mConversation, "checkConversation");
            }
        } else {
            Timber.e("checkConversation(): mConversation notNull");
        }
    }

    private void downloadFile(Chat chat, Media selectedMedia) {
        if (requireContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) requireContext()).checkPermission(new CheckPermissionCallbackHelper((Activity) getContext(),
                    CheckPermissionCallbackHelper.Type.STORAGE) {
                @Override
                public void onPermissionGranted() {
                    Timber.i("Download media: " + new Gson().toJson(selectedMedia));
                    new FileDownloadHelper(chat.getMediaList().get(0).getUrl(),
                            chat.getMediaList().get(0).getFileName(),
                            selectedMedia.getId(),
                            DownloaderUtils.getRootDirPath(requireContext()),
                            FileDownloadHelper.CHAT_DIR,
                            selectedMedia.getSize(),
                            new FileDownloadHelper.FileDownloadListener() {

                                @Override
                                public void onDownloadGetStarted(String requestId, String downloadId) {
                                    if (mAdapter != null) {
                                        mAdapter.onDownloadGetStarted(requestId,
                                                chat,
                                                selectedMedia.getId());
                                    }
                                }

                                @Override
                                public void onDownloadFailed(String requestId, String downloadId, Throwable ex) {
                                    Timber.e("onDownloadFailed: " + ex.getMessage());
                                    showSnackBar(R.string.toast_download_file_failed, true);
                                    if (mAdapter != null) {
                                        mAdapter.onUploadOrDownloadFailed(false,
                                                requestId,
                                                chat,
                                                selectedMedia.getId(),
                                                ex);
                                    }
                                }

                                @Override
                                public void onDownloadInProgress(String requestId, String downloadId, long downloadedBytes, long totalBytes) {
                                    Timber.i("onDownloadInProgress: downloadedBytes: " + downloadedBytes + ", totalBytes: " + totalBytes);
                                    if (mAdapter != null) {
                                        mAdapter.onUploadOrDownloadProgressing(false,
                                                requestId,
                                                chat,
                                                selectedMedia.getId(),
                                                (int) ((downloadedBytes * 100) / totalBytes),
                                                downloadedBytes,
                                                totalBytes);
                                    }
                                }

                                @Override
                                public void onDownloadSuccess(String requestId, String downloadId, File file) {
                                    Timber.i("onDownloadSuccess: " + file.getAbsolutePath());
                                    if (mAdapter != null) {
                                        mAdapter.onUploadOrDownloadSuccess(false,
                                                requestId,
                                                chat,
                                                selectedMedia.getId(),
                                                file.getAbsolutePath(),
                                                null);
                                    }
                                }
                            }).performDownload();
                }
            }, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    private void initChatListener() {
        mOnChatItemListener = new OnChatItemListener() {
            @Override
            public void onFileClick(Chat chat, Media selectedMedia) {
                downloadFile(chat, selectedMedia);
            }

            @Override
            public void onImageClick(List<Media> list, int productMediaPosition) {
                FullScreenMediaDialog dialog =
                        FullScreenMediaDialog.newInstance(list, productMediaPosition, true);

                if (!dialog.isAdded()) {
                    dialog.setChatImageFullScreenDialogListener(media -> {
                        AbsBaseActivity absBaseActivity = ((AbsBaseActivity) getContext());
                        if (absBaseActivity != null) {
                            absBaseActivity.checkPermission(new PermissionCheckHelper.OnPermissionCallback() {
                                @Override
                                public void onPermissionGranted() {
                                    dialog.saveMedia(media);
                                }

                                @Override
                                public void onPermissionDenied(String[] grantedPermission,
                                                               String[] deniedPermission,
                                                               boolean isUserPressNeverAskAgain) {
                                    ToastUtil.showToast(getContext(), R.string.permission_not_granted, true);
                                }
                            }, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                        }
                    });
                    dialog.show(getChildFragmentManager(), FullScreenMediaDialog.TAG);
                }
            }

            @Override
            public void onChatItemLongPressClick(Chat chat, @Nullable Media media) {
                boolean isAllowOnlyDeleteOption = false;
                if (TextUtils.equals(chat.getSenderId(), mMyUser.getId())) {
                    if (chat.getStatus() == MessageState.SENDING) {
                        //Do not to hav any option for sending message
                        return;
                    } else if (chat.getStatus() == MessageState.FAIL) {
                        isAllowOnlyDeleteOption = true;
                    }
                }
                PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
                if (isAllowOnlyDeleteOption) {
                    dialog.addItem(PostContextMenuItemType.getChatOnlyRemoveMenuItem());
                } else {
                    boolean isMultiImage = chat.getMediaList() != null && chat.getMediaList().size() > 1;
                    final boolean shouldShowDelete = TextUtils.equals(chat.getSenderId(), mMyUser.getId());
                    //Allow to edit only text content chat
                    final boolean shouldShowEdit = shouldShowDelete && chat.getType() == Chat.Type.TEXT && !TextUtils.isEmpty(chat.getContent());
                    dialog.addItem(PostContextMenuItemType.getChatMenuItems(isMultiImage, shouldShowDelete, shouldShowEdit, chat.getType()));
                }
                dialog.setOnItemClickListener(result -> {
                    checkPostContextMenuItemClick(chat, media, result);
                    dialog.dismiss();
                });
                dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
            }

            @Override
            public void onForwardClick(Chat chat) {
                injectIsGroupProperty(chat);
                startForwardIntent(chat, null);
            }

            @Override
            public void onMessageRetryClick(Chat chat) {
                sendMessage(chat);
            }

            @Override
            public User getUser(String id) {
                if (TextUtils.equals(mMyUser.getId(), id)) {
                    return mMyUser;
                } else {
                    return mRecipient;
                }
            }

            @Override
            public void onCallAgainOrBack(Chat chat, AbsCallService.CallType callType) {
                if (isGroupChat()) {
                    if (mViewModel.getIsJoinAvailable().get()) {
                        if (getBinding().joinButtonView.getVisibility() == View.VISIBLE) {
                            getBinding().joinButtonView.performClick();
                        } else {
                            showSnackBar(R.string.call_toast_no_call_feature, true);
                        }
                    } else {
                        performCall(callType);
                    }
                } else {
                    performCall(callType);
                }
            }

            @Override
            public void onCalculateSeenDisplayNameHeight(String chatId,
                                                         String seenText,
                                                         boolean isRecipientSide,
                                                         ChatMediaViewModel.CalculateSeenNameDisplayCallback callback) {
                if (mIsInBg) {
//                    Timber.i("App in background, so will add the request of onCalculateSeenDisplayNameHeight to backup list.");
                    mMessageStatusHelper.addBackupRequestSeenNameViewHeightData(new MessageStatusHelper.RequestSeenNameViewHeightData(chatId,
                            seenText,
                            isRecipientSide,
                            callback));
                    return;
                }

                        /*
                            This method purpose is to calculate the seen avatar name text view height which
                            will be shown and animate when user click on chat item. So we have to pre-calculate
                            its height base on generated seen name text and the way to archive this by adding
                            a dump text view having the same property as text view displaying seen name in chat
                            item into main chat screen and set the exact same text and then get the height from
                            that dump view and pass back to chat item and the dump view will be removed from
                            the main chat screen too. And we also have the logic to re-use the calculated
                            value one.
                         */
                int calculatedSeenDisplayViewHeight = mMessageStatusHelper.getCalculatedSeenDisplayViewHeight(chatId, seenText);
                if (calculatedSeenDisplayViewHeight > 0) {
//                    Timber.i("Pass the existing calculatedSeenDisplayViewHeight of " + chatId + ", height: " + calculatedSeenDisplayViewHeight);
                    if (callback != null) {
                        callback.onCalculationFinished(calculatedSeenDisplayViewHeight);
                    }
                } else {
                    requireActivity().runOnUiThread(() -> {
                        ItemSeenNameTextviewBinding binding = DataBindingUtil.inflate(LayoutInflater.from(requireContext()),
                                R.layout.item_seen_name_textview,
                                null,
                                false);
                        final TextView textViewStatus = binding.textViewStatus;
                        textViewStatus.setText(seenText);
                        textViewStatus.setVisibility(View.INVISIBLE);
                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);
                        params.leftMargin = getResources().getDimensionPixelSize(R.dimen.seen_avatar_margin_lef);
                        params.rightMargin = getResources().getDimensionPixelSize(R.dimen.seen_avatar_margin_right);
                        textViewStatus.setGravity(isRecipientSide ? Gravity.LEFT : Gravity.RIGHT);
                        getBinding().rootView.addView(textViewStatus, params);

                        textViewStatus.postDelayed(() -> {
                            mMessageStatusHelper.addSeenMessageAvatarData(new MessageStatusHelper.SeenMessageAvatarData(chatId,
                                    seenText,
                                    textViewStatus.getHeight()));
//                            Timber.i("Record new calculatedSeenDisplayViewHeight of " + chatId +
//                                    ", height: " + textViewStatus.getHeight() +
//                                    ", text: " + textViewStatus.getText().toString());
                            if (callback != null) {
                                callback.onCalculationFinished(textViewStatus.getHeight());
                            }
                            getBinding().rootView.removeView(textViewStatus);
                        }, 500);
                    });
                }
            }

            @Override
            public void onSwipeToReply(Chat replyingChat) {
//                Timber.i("onSwipeToReply");
                onReplyChat(replyingChat);
            }

            @Override
            public void onRepliedChatClicked(Chat repliedChat, boolean isInInputReplyMode) {
//                        Timber.i("onRepliedChatClicked: isInInputReplyMode: " + isInInputReplyMode + ", chat: " + new Gson().toJson(repliedChat));
                int chatPosition = mAdapter.findChatPosition(repliedChat.getId());
                if (chatPosition >= 0) {
//                            Timber.i("Post: " + getBinding().recyclerView.getLayoutManager().findViewByPosition(chatPosition));
                    mAnimateViewItemPosition = chatPosition;
                    getBinding().recyclerView.smoothScrollToPosition(chatPosition);
                    getBinding().recyclerView.postDelayed(() -> {
//                                Timber.i("mHasScrolled: " + mHasScrolled);
                                /*
                                Need to recheck to force animate item view for sometime the scroll to
                                position is already in position which will not invoke scroll change
                                listener that we handle for animate view.
                                 */
                        if (!mHasScrolled) {
                            animateItemViewWhenScrollTo(mAnimateViewItemPosition);
                            mAnimateViewItemPosition = -1;
                        }
                    }, 50);
                } else if (mAdapter.getItemCount() > 0) {
                            /*
                            Will check to query to selected reference chat and add into list if it existed in local db.
                             */
                    Chat referenceChat = MessageDb.queryById(requireContext(), repliedChat.getId());
//                            Timber.i("Local referenced chat: " + new Gson().toJson(referenceChat));
                    if (referenceChat != null) {
                        Chat mostEarlierMessage = mAdapter.getMostEarlierMessage();
                        if (mostEarlierMessage != null) {
//                                    Timber.i("mostEarlierMessage: " + new Gson().toJson(mostEarlierMessage));
                            //Will query more all from local db.
                            List<Chat> chatList = MessageDb.queryFromNewerMessageToOldMessage(requireContext(),
                                    mostEarlierMessage,
                                    referenceChat);
//                                    Timber.i("Local referenced chatList: " + (chatList != null ? chatList.size() : null));
                            if (chatList != null && !chatList.isEmpty()) {
//                                        Timber.i("Index 0: " + new Gson().toJson(chatList.get(0)));
//                                        Timber.i("Index Last: " + new Gson().toJson(chatList.get(chatList.size() - 1)));
                                //Maintain the previous load more state
                                mAdapter.addByLoadMore(new ArrayList<>(chatList), mAdapter.canLoadMore());
                                mLayoutManager.loadingFinished();
                                getBinding().recyclerView.postDelayed(() -> {
                                    //The selected referenced chat will be added in list and we have
                                    //to find its position again and scroll to.
                                    int chatPosition2 = mAdapter.findChatPosition(repliedChat.getId());
                                    mAnimateViewItemPosition = chatPosition2;
                                    if (chatPosition2 >= 0) {
                                        getBinding().recyclerView.smoothScrollToPosition(chatPosition2);
                                    }
                                }, 100);
                            }
                        }
                    }
                }
            }
        };
    }

    private void initAdapter() {
        if (mAdapter != null) {
            //Ignore the initialization if the adapter was previously initialized.
            return;
        }

        // find the ListView in the popup layout
        mLayoutManager = new ChatLayoutManager(getActivity(), getBinding().recyclerView);
        mLayoutManager.setStackFromEnd(true);
        mLayoutManager.setItemPrefetchEnabled(true);
        getBinding().recyclerView.setLayoutManager(mLayoutManager);
        getBinding().recyclerView.setItemAnimator(new MyItemAnimator());
        initChatListener();
        mMyUser = SharedPrefUtils.getUser(getContext());
        mAdapter = new ChatAdapter(getActivity(),
                mApiService,
                mConversation,
                SharedPrefUtils.getUser(getActivity()),
                new ArrayList<>(),
                mSearchKeyword,
                mOnChatItemListener,
                new ChatAdapter.ChatAdapterCallback() {
                    @Override
                    public void onAddNewMessage(Chat chat) {
                        mViewModel.setShowNewGroupWelcomeMessage(false);
                    }

                    @Override
                    public void onAddWelcomeSupportGroupMessageItem() {
                        mViewModel.setWelcomeSupportGroupMessageVisible(false);
                    }

                    @Override
                    public boolean isScreenInBackground() {
                        return mIsInBg;
                    }
                });
        getBinding().recyclerView.setHasFixedSize(true);
        getBinding().recyclerView.setItemViewCacheSize(50);
        mAdapter.setShowEmpty(false);
        mAdapter.setCanLoadMore(false);
        getBinding().recyclerView.setAdapter(mAdapter);
    }

    private void performCall(AbsCallService.CallType callType) {
        if (callType == AbsCallService.CallType.VIDEO) {
            if (getBinding().callVideoButtonView.getVisibility() == View.VISIBLE) {
                getBinding().callVideoButtonView.performClick();
            } else {
                showSnackBar(R.string.call_toast_no_call_feature, true);
            }
        } else {
            if (getBinding().callAudioButtonView.getVisibility() == View.VISIBLE) {
                getBinding().callAudioButtonView.performClick();
            } else {
                showSnackBar(R.string.call_toast_no_call_feature, true);
            }
        }
    }

    private void checkPostContextMenuItemClick(Chat chat, Media media, int result) {
        PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
        if (itemType == PostContextMenuItemType.COPY_TEXT_CLICK) {
            CopyTextUtil.copyTextToClipboard(getActivity(), mAdapter.getDisplayChatContent(chat.getId()));
        } else if (itemType == PostContextMenuItemType.SAVE_IMAGE_CLICK) {
            checkPermissionBeforeSaveImgVideo(chat.getMediaList());
        } else if (itemType == PostContextMenuItemType.FORWARD_CLICK) {
            startForwardIntent(chat, media);
        } else if (itemType == PostContextMenuItemType.DELETE_CLICK) {
            if (!isGroupChat() && mRecipient != null) {
                chat.setSendTo(mRecipient.getId());
            }
            onRemoveMessage(chat);
        } else if (itemType == PostContextMenuItemType.REPLY_CLICK) {
            onReplyChat(chat);
        } else if (itemType == PostContextMenuItemType.EDIT_POST_CLICK) {
            onEditChat(chat);
        }
    }

    private void onRemoveMessage(Chat chat) {
        MessageDialog dialog = new MessageDialog();
        dialog.setTitle(getString(R.string.chat_popup_confirm_delete_title));
        dialog.setMessage(getString(R.string.chat_popup_confirm_delete_description));
        dialog.setLeftText(getString(R.string.popup_yes_button), view -> {
            mAdapter.deleteItem(chat);
            if (mSocketBinder != null) {
                mSocketBinder.removeMessage(chat);
            }
        });
        dialog.setRightText(getString(R.string.popup_no_button), null);
        dialog.show(requireActivity().getSupportFragmentManager(), MessageDialog.TAG);
    }

    private void onReplyChat(Chat replyingChat) {
        if (!isChatAlreadyReplying(replyingChat)) {
            mViewModel.updateEditOrReplyMessageMode(true);
            if (mViewModel.isInEditMessageInput()) {
                mViewModel.resetFromEditModeToNormalMode();
            }
            mViewModel.setInputReplyVisibility(true);
            getBinding().inputReplyLayout.buildReplayView(replyingChat,
                    null,
                    true,
                    true,
                    null);
            new Handler().postDelayed(() -> showKeyboard(getBinding().messageInput.getMentionsEditText()), 100);
        }
    }

    private void onEditChat(Chat editChat) {
        if (!isChatAlreadyEditing(editChat)) {
            mViewModel.updateEditOrReplyMessageMode(false);
            mViewModel.setInputEditVisibility(true);
            mViewModel.switchToEditMessageMode();
            getBinding().editHeaderLayout.buildView(editChat);
            getBinding().messageInput.setText(editChat.getContent(), false);
            new Handler().postDelayed(() -> {
                getBinding().messageInput.getMentionsEditText().setSelection(Objects.requireNonNull(getBinding().messageInput.getMentionsEditText().getText()).length());
                showKeyboard(getBinding().messageInput.getMentionsEditText());
            }, 100);
        }
    }

    private boolean isChatAlreadyReplying(Chat replyingChat) {
        return mViewModel.isInRepliedInput() && TextUtils.equals(getBinding().inputReplyLayout.getReplyingChatId(), replyingChat.getId());
    }

    private boolean isChatAlreadyEditing(Chat chat) {
        return mViewModel.isInEditMessageInput() && TextUtils.equals(getBinding().editHeaderLayout.getChat().getId(), chat.getId());
    }

    private void checkPermissionBeforeSaveImgVideo(List<Media> mediaList) {
        ((AbsBaseActivity) getContext()).checkPermission(new PermissionCheckHelper.OnPermissionCallback() {
            @Override
            public void onPermissionGranted() {
                mViewModel.saveImageVideoIntoLocal(mediaList);
            }

            @Override
            public void onPermissionDenied(String[] grantedPermission, String[] deniedPermission, boolean isUserPressNeverAskAgain) {
                ToastUtil.showToast(getContext(), R.string.permission_not_granted, true);
            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    private void setFreshData(List<BaseChatModel> value,
                              boolean isResetData,
                              boolean isCanLoadMoreFromTop,
                              boolean isCanLoadMoreFromBottom,
                              boolean isRequestChatHistory) {
        if (value == null) {
            return;
        }
        mHasSetChatToAdapter = true;
        Timber.i("value: " + value.size() +
                ", isResetData: " + isResetData +
                ", isCanLoadMoreFromTop: " + isCanLoadMoreFromTop +
                ", isCanLoadMoreFromBottom: " + isCanLoadMoreFromBottom +
                " , isRequestChatHistory: " + isRequestChatHistory +
                ", Adapter: " + getBinding().recyclerView.getAdapter());
        //Only bind load more listener only if there are more data to be loaded more.
        if (isCanLoadMoreFromTop || isCanLoadMoreFromBottom) {
            mLayoutManager.setOnLoadMoreListener(mLoadMoreListener);
        } else {
            mLayoutManager.setOnLoadMoreListener(null);
        }

        if (value.isEmpty()) {
            if (mConversation.isGroup() && mConversation.getDate() != null) {
                showNewGroupWelcomeMessage();
            } else {
                if (mViewModel != null) {
                    mViewModel.setShowNewGroupWelcomeMessage(false);
                }
                if (mAdapter != null) {
                    mAdapter.setShowEmpty(value.isEmpty());
                }
            }
        } else {
            mViewModel.setShowNewGroupWelcomeMessage(false);
            mAdapter.setShowEmpty(false);
        }

        mAdapter.setCanLoadMore(isCanLoadMoreFromTop);
        mAdapter.setCanLoadMoreFromBottom(isCanLoadMoreFromBottom);

        if (isResetData || isRequestChatHistory) {
            mAdapter.resetData(value);
        } else {
            mAdapter.setData(value);
        }
    }

    private void checkToInvokeLoadMoreByDefault() {
        if (mViewModel != null && mAdapter.getItemCount() > 0) {
            getBinding().recyclerView.postDelayed(() -> {
                mLayoutManager.checkLToInvokeReachLoadMoreFromTopByDefaultIfNecessary();
                mLayoutManager.checkLToInvokeReachLoadMoreFromBottomByDefaultIfNecessary();
            }, 1000);
        }
    }

    private void checkToAnimatedSelectedChat() {
        if (mAdapter != null && mAdapter.getItemCount() > 0 && mViewModel != null) {
            final int selectedChatIndex = mViewModel.getSelectedChatIndex(mAdapter.getData());
            if (selectedChatIndex >= 0 && selectedChatIndex < mAdapter.getItemCount()) {
                Timber.i("selectedChatIndex: " + selectedChatIndex);
                getBinding().recyclerView.scrollToPosition(selectedChatIndex);
                getBinding().recyclerView.postDelayed(() -> animateItemViewWhenScrollTo(selectedChatIndex),
                        300);
            }
        }
    }

    private void addLoadMoreFromBottomData(List<BaseChatModel> value, boolean isCanLoadMoreFromBottom) {
        if (value.isEmpty()) {
            stopLoadMore(false);
        } else {
            mAdapter.addByLoadMoreFromBottom(value, isCanLoadMoreFromBottom);
            mLayoutManager.loadingFinished();
        }
    }

    private void checkResetSearchKeywordHighLightIfNecessary(boolean removeFromViewItem) {
        if (!TextUtils.isEmpty(mSearchKeyword)) {
            mSearchKeyword = null;
            mAdapter.resetHighLightSearchKeyword(removeFromViewItem);
        }
    }

    private void sendMessage(Chat chat) {
        Timber.i("sendMessage: " + chat);
        if (chat == null) {
            return;
        }

        if (mViewModel.isJumpSearch()) {
            checkResetSearchKeywordHighLightIfNecessary(false);
            mQueueMessage = chat;
            mViewModel.resetToNormalMode();
            mAdapter.removeLoadMoreFromBottom();
            //Will send message only after the message screen is refreshed with normal message
        } else {
            checkResetSearchKeywordHighLightIfNecessary(true);
            //Will send message immediately
            if (mViewModel.isInRepliedInput()) {
                chat.setChatReferenceWrapper(getBinding().inputReplyLayout.getRepliedChat());
            }
            injectIsGroupProperty(chat);
            chat.setStatus(MessageState.SENDING);
            if (mAdapter.addLatestChat(chat)) {
                scrollToBottom(false);
                getBinding().messageInput.requestFocus();
            }
            SoundEffectService.playSound(requireActivity(), SoundEffectService.SoundFile.SEND_MESSAGE);
            //when send message, need to reset new typing handler
            String productId = null;
            if (mProduct != null) {
                productId = mProduct.getId();
            }
            mSocketBinder.stopTyping(mRecipient.getId(), productId, mConversationID);

            //Need to intercept to check add meta preview here.
            if (!chat.hasMetaPreview() && WallStreetHelper.isContainLink(chat.getContent())) {
                mViewModel.requestLinkPreview(chat, previewModel -> {
                    if (previewModel != null) {
                        chat.setMetaPreview(Collections.singletonList(previewModel));
                    }
                    mSocketBinder.sendMessage(mRecipient, chat);
                });
            } else {
                mSocketBinder.sendMessage(mRecipient, chat);
            }

            //Need to close the reply input view if it was previously displayed
            mViewModel.setInputReplyVisibility(false);
            if (mViewModel.isInEditMessageInput()) {
                mViewModel.resetFromEditModeToNormalMode();
            }
        }
    }

    private void sendCallEventMessage(Chat comment) {
        Timber.i("sendCallEventMessage: " + new Gson().toJson(comment));
        comment.setStatus(MessageState.SENDING);
        if (mAdapter.addLatestChat(comment)) {
            scrollToBottom(false);
            getBinding().messageInput.requestFocus();
        }
        SoundEffectService.playSound(requireActivity(), SoundEffectService.SoundFile.SEND_MESSAGE);
        mSocketBinder.sendMessage(mRecipient, comment);
    }

    private Chat generateGroupChat(String content, List<Media> mediaList) {
        Chat baseChat = Chat.getNewChat(requireContext(), mConversation.getParticipants().get(0), null);
        baseChat.setContent(content);
        baseChat.setChannelId(mConversation.getGroupId());
        baseChat.setMediaList(mediaList);
        baseChat.setGroup(true);
        baseChat.setGroupName(mConversation.getGroupName());
        baseChat.setSendTo(baseChat.getChannelId());

        return baseChat;
    }

    private List<String> getAllParticipantIds() {
        List<String> idList = new ArrayList<>();
        for (User participant : mConversation.getParticipants()) {
            idList.add(participant.getId());
        }

        return idList;
    }

    private boolean isGroupChat() {
        return mConversation != null && mConversation.isGroup();
    }

    private void sendGroupMessage(Chat message, Conversation conversation) {
        if (mViewModel.isInRepliedInput()) {
            message.setChatReferenceWrapper(getBinding().inputReplyLayout.getRepliedChat());
        }
        //Add only first chat in the list
        Chat firstChat = message;
        firstChat.setStatus(MessageState.SENDING);
        if (mAdapter.addLatestChat(firstChat)) {
            scrollToBottom(false);
            getBinding().messageInput.requestFocus();
        }
        SoundEffectService.playSound(requireContext(), SoundEffectService.SoundFile.SEND_MESSAGE);
        mSocketBinder.stopTyping(null, null, null);

        //Need to intercept to check add meta preview here.
        if (!firstChat.hasMetaPreview() && WallStreetHelper.isContainLink(firstChat.getContent())) {
            mViewModel.requestLinkPreview(firstChat, new ChatDialogViewModel.LinkPreviewCallback() {
                @Override
                public void onGetLinkPreviewFinished(LinkPreviewModel previewModel) {
                    if (previewModel != null) {
                        List<LinkPreviewModel> metaPreview = Collections.singletonList(previewModel);
                        message.setMetaPreview(metaPreview);
                    }
                    mSocketBinder.sendGroupChat(message, conversation);
                }
            });
        } else {
            mSocketBinder.sendGroupChat(message, conversation);
        }
        mViewModel.setInputReplyVisibility(false);
    }

    private void scrollToBottom(boolean forceScroll) {
        if (forceScroll) {
            Timber.i("forceScroll");
            final int last = mLayoutManager.findLastVisibleItemPosition();
            final int count = mAdapter.getItemCount() - 1;
            if (count - last <= 2) {
                mScrollToBottomHandler.startDelay();
            }
        } else {
            mScrollToBottomHandler.startDelay();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mIsInBg = false;
        checkToResumeRequestSeenNameViewHeightIfNecessary();
        if (mSocketBinder != null) {
            mSocketBinder.onResume(mConversationID);
        }
        Timber.i("onResume");
        if (mAdapter != null) {
            mAdapter.checkToPlayOrStopWritingMessageIndicatorSound(false);
        }
    }

    private void checkToResumeRequestSeenNameViewHeightIfNecessary() {
        if (mMessageStatusHelper != null && mOnChatItemListener != null) {
            List<MessageStatusHelper.RequestSeenNameViewHeightData> backup = mMessageStatusHelper.getBackupRequestSeenNameViewHeightData();
            if (!backup.isEmpty()) {
                for (MessageStatusHelper.RequestSeenNameViewHeightData request : backup) {
//                    Timber.i("Will resume onCalculateSeenDisplayNameHeight of " + request.getChatId());
                    mOnChatItemListener.onCalculateSeenDisplayNameHeight(request.getChatId(),
                            request.getSeenDisplay(),
                            request.isRecipientSide(),
                            request.getCallback());
                    mMessageStatusHelper.removeBackupRequestSeenNameViewHeightData(request.getChatId());
                }
            }
        }
    }

    @Override
    public void onReceiveUpdateSeenUserIdListUpdate(String conversationId, List<Chat> chats) {
        if (mAdapter != null) {
            mAdapter.onReceiveUpdateSeenUserIdListUpdate(chats);
        }
    }

    @Override
    public void onReceiveMessage(Chat message) {
        Timber.i("onReceiveMessage: " + new Gson().toJson(message));
        if (getActivity() == null) {
            return;
        }
        boolean validMessageToAdd = mAdapter.isValidMessageToAdd(message);
        Timber.i("validMessageToAdd: " + validMessageToAdd + ", content: " + message.getContent() + ", id: " + message.getId());
        if (!validMessageToAdd) {
            return;
        }


        int chatIndex = mAdapter.findChatIndex(message);
        if (mViewModel.isJumpSearch() && chatIndex < 0) {
//            Timber.i("Ignore adding new receiving message in jump to search mode.");
            return;
        }

        getActivity().runOnUiThread(() -> {
            if (TextUtils.equals(message.getChannelId(), mConversationID)) {
                if (mViewModel.isJumpSearch() &&
                        message.getSendingType() == Chat.SendingType.READ_STATUS &&
                        chatIndex >= 0) {
//                    Timber.i("Wil update read status in jump message mode.");
                    //Will mark previous message as seen too.
                    for (int i = 0; i < mAdapter.getItemCount(); i++) {
                        if (i < chatIndex && mAdapter.getData().get(i) instanceof Chat) {
                            Chat chat = (Chat) mAdapter.getData().get(i);
                            if (chat.getStatus() == MessageState.DELIVERED ||
                                    chat.getStatus() == MessageState.SENT) {
                                chat.setStatus(MessageState.SEEN);
                                chat.setIgnoreSeenStatus(true);
                                mAdapter.addLatestChat(chat);
                            }
                        }
                    }

                    Chat existing = ((Chat) mAdapter.getData().get(chatIndex)).cloneNewChat();
                    existing.setStatus(MessageState.SEEN);
                    existing.setSender(message.getSender());
                    existing.setSenderId(message.getSenderId());
                    mAdapter.addLatestChat(existing);
                    return;
                }

                //Add message as normal case
                boolean isAdd = mAdapter.addLatestChat(message);
                if (isAdd) {
                    //scroll to button
                    scrollToBottom(true);
                    if (message.getSender() != null) {
                        mReceivingTypingHelper.removeTyping(message.getSender());
                    }
                }
            }
        });
    }

    @Override
    public void onRecipientTyping(Chat chat) {
        requireActivity().runOnUiThread(() -> {
            if (TextUtils.equals(chat.getChannelId(), mConversationID)) {
                Timber.i("onRecipientTyping: " + new Gson().toJson(chat));
                if (chat.getSender() != null) {
                    mReceivingTypingHelper.addTyping(chat.getSender());
                }
            }
        });
    }

    @Override
    public boolean isInBackground() {
        /*
           If the calling screen is open above Chat screen, then the chat screen should not been considered
           as in background anymore even if the real status of its lifecycle was to properly manage the
           update of missed call message type in chat screen.
         */
        return !mIsCallingOpen && mIsInBg;
    }

    @Override
    public boolean isChatScreen() {
        return true;
    }

    @Override
    public String getChannelId() {
        return mConversationID;
    }

    @Override
    public void onServiceReady(AbsChatBinder binder) {
        Timber.i("onServiceReady");
        extractPassingData();
        if (binder instanceof SocketService.SocketBinder) {
            mSocketBinder = (SocketService.SocketBinder) binder;
            mSocketBinder.getChatSocket().addUploadMediaListener(mChatMediaUploadListener);
            mSocketBinder.addChatListener(this);
            if (!MainApplication.isIsHomeScreenOpened()) {
                mSocketBinder.addCallVOIPMessageListeners(this);
            }
            mAdapter.setSocketBinder(mSocketBinder);

            ChatDataManager dataManager = new ChatDataManager(requireContext(),
                    mApiService,
                    new ChatDataManager.ChatDataManagerCommunicator() {
                        @Override
                        public Chat getLastChatInList() {
                            if (mAdapter != null) {
                                return mAdapter.getLatestChatInList();
                            }

                            return null;
                        }

                        @Override
                        public boolean isAllDisplayMessageAreIgnoreDisplaySeenAvatarType() {
                            if (mAdapter != null) {
                                return mAdapter.isAllDisplayMessageAreIgnoreDisplaySeenAvatarType();
                            }

                            return false;
                        }
                    },
                    mConversation,
                    getArguments().getParcelable(ChatIntent.CHAT),
                    OpenChatScreenType.getFromValue(getArguments().getInt(ChatIntent.OPEN_CHAT_TYPE)));
            mViewModel = new ChatDialogViewModel(dataManager,
                    mSocketBinder,
                    mConversation,
                    mConversationID,
                    mRecipient,
                    mProduct,
                    new ChatDialogViewModel.OnCallback() {
                        @Override
                        public void onSendButtonClick(Chat chat) {
                            //One to one chatting
                            mRecordAudioHelper.setIcon(RecordAudioHelper.Type.RECORD);
                            sendMessage(chat);
                        }

                        @Override
                        public void onSendButtonClick(String content, Conversation conversation) {
                            //Group Chatting
                            sendGroupMessage(generateGroupChat(content, null), conversation);
                        }

                        @Override
                        public void onKeyboardMediaSelected(Chat message, boolean isGroup) {
                            if (!isGroup) {
                                sendMessage(message);
                            } else {
                                sendGroupMessage(message, mConversation);
                            }
                        }

                        @Override
                        public void onNeedResetFocus() {
                            getBinding().messageInput.clearFocus();
                            getBinding().messageInput.requestFocus();
                        }

                        @Override
                        public void onStartTyping(int start, int count) {
//                            Timber.i("onStartTyping out " + start);
                            if (mAdapter != null && mAdapter.isUnreadMessageTitleExist(mAdapter.getData())) {
                                if (!mIsRemoving) {
                                    if (!mAdapter.getData().isEmpty()) {
                                        mIsRemoving = true;
                                        removeUnreadMessageItem(mAdapter.getUnreadMessagePosition());
                                    }
                                }
                            }
                            // Prevent sending typing message to socket if it's a support conversation
                            if (mSocketBinder != null && !mViewModel.getIsSupportConversation().get()) {
                                boolean groupChat = isGroupChat();
                                mSocketBinder.startTyping(groupChat,
                                        groupChat ? mConversationID : mRecipient.getId(),
                                        null,
                                        mConversationID);
                            }
                        }

                        @Override
                        public void onFail(ErrorThrowable ex) {
                            //No implementation
                        }

                        @Override
                        public void onComplete(List<BaseChatModel> result, boolean canLoadMore) {
                            //No implementation
                        }

                        @Override
                        public void onReplaceRecordIcon() {
                            if (checkAudioChat) {
                                mRecordAudioHelper.setIcon(RecordAudioHelper.Type.RECORD);
                            }
                        }

                        @Override
                        public void onReplaceSendIcon() {
                            mRecordAudioHelper.setIcon(RecordAudioHelper.Type.SEND);
                        }

                        @Override
                        public void onScrollDownToLastItem() {
                            if (getBinding().recyclerView.getAdapter() != null && getBinding().recyclerView.getAdapter().getItemCount() > 0) {
                                getBinding().recyclerView.smoothScrollToPosition(getBinding().recyclerView.getAdapter().getItemCount());
                            }
                        }

                        @Override
                        public void onLocalLoadSuccess(List<BaseChatModel> result, boolean isResetData, boolean canLoadMore) {
                            ChatHelper.logMessages("onLocalLoadSuccess", ChatHelper.getChatList(result));
                            if (isAddedToActivity()) {
                                setFreshData(result, isResetData, canLoadMore, false, false);
                                checkToAnimatedSelectedChat();
                                checkToInvokeLoadMoreByDefault();
                            }
                        }

                        @Override
                        public void onLocalLoadFailed(ErrorThrowable ex, boolean isResetData) {
                            if (getActivity() != null && ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                                mAdapter.setEmptyMessage(getActivity().getString(R.string.error_internet_connection_description));
                            } else {
                                mAdapter.setEmptyMessage(ex.getMessage());
                            }
                            setFreshData(new ArrayList<>(), isResetData, false, false, false);
                        }

                        @Override
                        public void onChatFromServerSuccess(List<BaseChatModel> result,
                                                            boolean isResetData,
                                                            boolean canLoadMore,
                                                            boolean isRequestChatHistory) {
                            if (isAddedToActivity()) {
                                Timber.i("onChatFromServerSuccess: " + result.size() +
                                        ", canLoadMore: " + canLoadMore +
                                        ", isResetData: " + isResetData +
                                        ", isRequestChatHistory: " + isRequestChatHistory);
                                ChatHelper.logMessages("onChatFromServerSuccess", ChatHelper.getChatList(result));
                                if (!result.isEmpty()) {
                                    if (mAdapter != null) {
                                        if (mAdapter.getItemCount() <= 0) {
                                            setFreshData(result,
                                                    isResetData,
                                                    canLoadMore,
                                                    false,
                                                    isRequestChatHistory);
                                            checkToInvokeLoadMoreByDefault();
                                            if (isResetData) {
                                                sendMessage(mQueueMessage);
                                            }
                                            checkToAddWelcomeSupportConversationItem();
                                        } else {
                                            /*
                                                In case the result from server is isRequestChatHistory,
                                                we will overwrite the display with this list directly in chat screen.
                                             */
                                            if (isRequestChatHistory) {
                                                setFreshData(result,
                                                        isResetData,
                                                        canLoadMore,
                                                        false,
                                                        true);
                                            } else {
                                                mAdapter.onGotMessageFromServer(result);
                                            }
                                            getBinding().recyclerView.post(() -> {
                                                if (getBinding().recyclerView.getAdapter() != null) {
                                                    /*
                                                        We will try to enable load more from top for following use case.
                                                        1. There some messages in local, and then user open
                                                        the chat screen without network connection. In this case,
                                                        user will see the local message and they also be able to
                                                        to paginate older message in local until it reach last
                                                        message in local and then there is no more chat to load more.
                                                        After that user connect back to network at the
                                                        same time they are in that chat screen, so after receiving network
                                                        get connected status, we will then try to load some conversation message
                                                        history from server and we also enable the load more from top too so as to allow
                                                        user loading more data from server.
                                                     */
                                                    if (!mAdapter.canLoadMore()) {
                                                        mLayoutManager.loadingFinished();
                                                        mLayoutManager.setOnLoadMoreListener(mLoadMoreListener);
                                                        mAdapter.addLoadMoreView();
                                                    }

                                                    if (isResetData) {
                                                        sendMessage(mQueueMessage);
                                                    }

                                                    if (getBinding().recyclerView.getAdapter().getItemCount() > 0) {
                                                        getBinding().recyclerView.smoothScrollToPosition(getBinding().recyclerView.getAdapter().getItemCount());
                                                    }
                                                }
                                            });
                                        }
                                    }
                                } else {
                                    if (isResetData) {
                                        sendMessage(mQueueMessage);
                                    } else if (!mHasSetChatToAdapter) {
                                        /*
                                        Will display empty data when both local and server chat are empty.
                                         */
                                        setFreshData(new ArrayList<>(),
                                                isResetData,
                                                false,
                                                false,
                                                false);
                                        /*
                                            There two use cases to show welcome support group message item for
                                            support conversation.
                                            1. If there is no any chat in conversation yet, we will show
                                            welcome item at the top of chat screen which will be handled in
                                            chat screen directly
                                            2. If there is any chat in chat, we will handle add welcome
                                            message within chat list screen directly.
                                         */
                                        if (mViewModel.getIsSupportConversation().get()) {
                                            mViewModel.setWelcomeSupportGroupMessageVisible(true);
                                        }
                                    }
                                }
                                mSocketBinder.checkToSendReadStatusFromCurrentUser(mConversationID);
                            }
                        }

                        @Override
                        public void onLoadChatFromServerFailed(ErrorThrowable ex, boolean isResetData) {
                            Timber.i("onLoadChatFromServerFailed: isResetData: " + isResetData);
                            if (isResetData) {
                                sendMessage(mQueueMessage);
                            } else {
                                //Check if there was previous local data
                                if (mAdapter != null && mAdapter.getItemCount() > 0) {
                                    //There is previous local chat in list. So just display error of server as pop-up.
                                    showSnackBar(ex.getMessage(), true);
                                } else {
                                    if (getActivity() != null && ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                                        mAdapter.setEmptyMessage(getActivity().getString(R.string.error_internet_connection_description));
                                    } else {
                                        mAdapter.setEmptyMessage(ex.getMessage());
                                    }
                                    setFreshData(new ArrayList<>(),
                                            isResetData,
                                            false,
                                            false, false);
                                }
                            }
                            mSocketBinder.checkToSendReadStatusFromCurrentUser(mConversationID);
                        }

                        @Override
                        public void onJumpSearchLoadSuccess(List<BaseChatModel> chatModelList,
                                                            boolean isCanLoadMoreFromTop,
                                                            boolean isCanLoadMoreFromBottom,
                                                            boolean isLoadMoreFromBottom) {
                            Timber.i("onJumpSearchLoadSuccess: " + chatModelList.size() +
                                    ", isCanLoadMoreFromTop: " + isCanLoadMoreFromTop +
                                    ", isCanLoadMoreFromBottom: " + isCanLoadMoreFromBottom +
                                    ", isLoadMoreFromBottom: " + isLoadMoreFromBottom);
                            if (!isLoadMoreFromBottom) {
                                setFreshData(chatModelList, false, isCanLoadMoreFromTop, isCanLoadMoreFromBottom, false);
                                shouldSendReadStatusForLatestJumpMessage((Chat) chatModelList.get(chatModelList.size() - 1));
                                checkToAnimatedSelectedChat();
                                checkToInvokeLoadMoreByDefault();
                            } else {
                                addLoadMoreFromBottomData(chatModelList, isCanLoadMoreFromBottom);
                            }
                        }

                        @Override
                        public void onJumpSearchLoadFailed(ErrorThrowable ex, boolean isLoadMoreFromBottom) {
                            Timber.e("onJumpSearchLoadFailed: isLoadMoreFromBottom: " + isLoadMoreFromBottom);
                            if (isLoadMoreFromBottom) {
                                stopLoadMore(false);
                                showSnackBar(ex.getMessage(), true);
                            } else {
                                if (getActivity() != null && ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                                    mAdapter.setEmptyMessage(getActivity().getString(R.string.error_internet_connection_description));
                                } else {
                                    mAdapter.setEmptyMessage(ex.getMessage());
                                }
                                mAdapter.setShowEmpty(true);
                                mAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onEditedMessageSendClicked(String editedContent) {
                            Timber.i("onEditedMessageSendClicked: " + editedContent);
                            Chat oldChat = getBinding().editHeaderLayout.getChat();
                            //Must clone new chat before perform sending
                            oldChat = oldChat.cloneNewChat();
                            if (!isGroupChat() && mRecipient != null) {
                                oldChat.setSendTo(mRecipient.getId());
                            }
                            oldChat.setSendingType(Chat.SendingType.UPDATE);
                            oldChat.setContent(editedContent);
                            oldChat.setEdited(true);
                            oldChat.setLastEditedDate(new Date());//Current date
                            oldChat.setSender(UserHelper.getNecessaryUserInfo(requireContext()));
                            //Remove cache old link preview from db.
                            if (oldChat.getLinkPreviewModel() != null) {
                                LinkPreviewDb.remove(requireContext(), oldChat.getId());
                            }
                            /*
                            Must reset all link preview and let it request again if the edited content
                             has link when send message.
                             */
                            oldChat.setMetaPreview(new ArrayList<>());
                            sendMessage(oldChat);
                        }
                    }, (callType, recipient, isMissedCall, calledDuration) -> {
                Timber.i("onReceivedCallEvent: recipient: " + recipient);
                requireActivity().runOnUiThread(() -> sendCallEventMessage(ChatHelper
                        .buildOneToOneCallEventMessage(requireContext(),
                                callType,
                                recipient,
                                isMissedCall,
                                calledDuration)));
            });
            if (mRecordAudioHelper != null) {
                mRecordAudioHelper.setAbleToSendVoice(mViewModel.isAbleToSendMessage());
            }
            checkSynchronizeUi();
            setVariable(BR.viewModel, mViewModel);
        }
    }

    private void checkToAddWelcomeSupportConversationItem() {
        if (mAdapter != null) {
            mViewModel.setWelcomeSupportGroupMessageVisible(false);
            mAdapter.checkToAddWelcomeSupportConversationItem();
        }
    }

    private void shouldSendReadStatusForLatestJumpMessage(Chat latestJumpMessage) {
        //Will also check to send read status with new messages that appear with jump result
        Chat lastMessageOfConversation = MessageDb.getLastMessageOfConversation(requireContext(), mConversationID);
        if (!TextUtils.equals(SharedPrefUtils.getUserId(requireContext()), latestJumpMessage.getSenderId()) &&
                (lastMessageOfConversation == null || lastMessageOfConversation.getDate().compareTo(latestJumpMessage.getDate()) < 0)) {
            Timber.i("Will send read status of jump message result.");
            mSocketBinder.sendReadStatusWithoutSavingToLocal(latestJumpMessage, mConversationID);
        }
    }

    private void removeUnreadMessageItem(int position) {
        new Handler().postDelayed(() -> {
            mAdapter.removeUnreadMessage(position);
        }, 200);
    }

    private void injectIsGroupProperty(Chat chat) {
        chat.setGroup(isGroupChat());
        if (mConversation != null) {
            chat.setGroupName(mConversation.getGroupName());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMessageStatusHelper.clearAllData();
        requireContext().unregisterReceiver(mCallMessageEvenReceiver);
        requireContext().unregisterReceiver(mJoinGroupCallStatusReceiver);
        requireActivity().unregisterReceiver(mCallingScreenOpenActionReceiver);
        if (mSocketBinder != null && mSocketBinder.getChatSocket() != null) {
            mSocketBinder.getChatSocket().removeUploadMediaListener(mChatMediaUploadListener);
            mSocketBinder.removeCallVOIPMessageListeners(this);
        }
        if (mAdapter != null) {
            mAdapter.releaseWritingMessageIndicatorSound();
        }
        if (requireActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) requireActivity()).removeStateChangeListener(this);
        }
        if (mAdapter != null) {
            mAdapter.releaseAllGifVideoPlayer();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPlayAudioService != null) {
            mPlayAudioService.stop();
        }

        if (mSocketBinder != null) {
            mSocketBinder.removeChatListener(this);
        }

        if (getActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getActivity()).removeServiceListener(this);
        }

        mRecordAudioHelper.destroy();
        mScrollToBottomHandler.destroy();
        mReceivingTypingHelper.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        mIsInBg = true;
        Timber.i("onPause");
        if (mAdapter != null) {
            mAdapter.checkToPlayOrStopWritingMessageIndicatorSound(true);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mRecordAudioHelper.checkToCancelRecord();
    }

    private void startForwardIntent(Chat chat, Media productMedia) {
        if (mPlayAudioService != null
                && (mPlayAudioService.isPlaying() || mPlayAudioService.isPause())) {
            mPlayAudioService.stop();
        }
        new Handler().post(() -> {
            if (productMedia != null) {
                startActivity(new ForwardIntent(getActivity(), productMedia));
            } else {
                startActivity(new ForwardIntent(getActivity(), chat));
            }
        });
    }

    private void bindGifView() {
        getBinding().containerSearchGif.onBackPressClickListener(v -> {
            getBinding().containerEditText.setVisibility(View.VISIBLE);
            getBinding().messageInput.requestFocus();
            getBinding().voiceRippleView.setVisibility(View.VISIBLE);
        });

        getBinding().gifImageView.setOnClickListener(v -> {
            getBinding().containerEditText.setVisibility(View.GONE);
            getBinding().voiceRippleView.setVisibility(View.GONE);
            getBinding().containerSearchGif.loadTrendingGifList(baseGifModel -> {
                getBinding().containerEditText.setVisibility(View.VISIBLE);
                getBinding().messageInput.requestFocus();
                getBinding().voiceRippleView.setVisibility(View.VISIBLE);

                ArrayList<Media> productMedias = new ArrayList<>();
                Media media = new Media();
                media.setWidth(baseGifModel.getGifWidth());
                media.setHeight(baseGifModel.getGifHeight());
                media.setUrl(baseGifModel.getGifUrl());
                media.setType(MediaType.TENOR_GIF);
                productMedias.add(media);

                if (isGroupChat()) {
                    sendMessage(generateGroupChat(null, productMedias));
                } else {
                    Chat chat = Chat.getNewChat(getContext(), mRecipient, mProduct);
                    chat.setChannelId(mConversationID);
                    chat.setMediaList(productMedias);

                    sendMessage(chat);
                }
            });
        });
    }

    public boolean onBackPress() {
        if (getBinding().containerSearchGif.getVisibility() == View.VISIBLE) {
            getBinding().containerSearchGif.clearData();
            getBinding().containerEditText.setVisibility(View.VISIBLE);
            getBinding().messageInput.requestFocus();
            getBinding().voiceRippleView.setVisibility(View.VISIBLE);
            return false;
        }
        return true;
    }

    @Override
    public void onReceiveVOIPCallMessage(Chat message) {
        if (!MainApplication.isIsHomeScreenOpened()) {
            Timber.i("onReceiveVOIPMessage: " + new Gson().toJson(message));
            if (CallHelper.isInComingVOIPCallMessageType(message) &&
                    CallHelper.shouldShowIncomingCallScreenOnReceiveIncomingInsideScreen()) {
                CallHelper.clearRejectedCallList();
                IncomingCallDataHolder incomingCallDataHolder = new IncomingCallDataHolder();
                incomingCallDataHolder.setCallType(message.getSendingTypeAsString());
                incomingCallDataHolder.setCallData(message.getCallData());
                Timber.i("Incoming call data: " + new Gson().toJson(incomingCallDataHolder));
                requireActivity().startActivity(new CallingIntent(getContext(), incomingCallDataHolder));
            }
        }
    }

    @Override
    public void onNetworkState(NetworkBroadcastReceiver.NetworkState networkState) {
        Timber.i("onNetworkState: " + networkState);
        if (mViewModel != null) {
            if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                mViewModel.checkToReloadChatHistoryFromServer(mAdapter != null ? mAdapter.getOldestChatInList() : null);
            } else {
                ConversationHelper.saveLastLocalMessageId(requireContext(), mConversationID, false);
            }
        }
    }

    private void showNewGroupWelcomeMessage() {
        if (!mViewModel.getIsShowNewGroupWelcomeMessage().get() && !mViewModel.getIsSupportConversation().get()) {
            //Will only show if that group does not have any message yet.
            ListItemNewGroupWelcomeBinding binding = DataBindingUtil.inflate(LayoutInflater.from(requireContext()),
                    R.layout.list_item_new_group_welcome,
                    getBinding().newGroupWelcomeMessageContainer,
                    true);
            ListItemNewGroupWelcomeViewModel viewModel = new ListItemNewGroupWelcomeViewModel(requireContext(), mConversation);
            binding.setVariable(BR.viewModel, viewModel);
            mViewModel.setShowNewGroupWelcomeMessage(true);
        }
    }
}

