package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ViewPagerAdapter;
import com.proapp.sompom.databinding.FragmentConciergeOrderHistoryContainerBinding;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.intent.newintent.ConciergeOrderHistoryContainerIntent;

/**
 * Created by Veasna Chhom on 4/25/22.
 */

public class ConciergeOrderHistoryContainerFragment extends AbsBindingFragment<FragmentConciergeOrderHistoryContainerBinding> {
    public static final String TAG = ConciergeOrderHistoryContainerFragment.class.getName();

    public static ConciergeOrderHistoryContainerFragment newInstance(boolean isFromProfileScreen) {

        Bundle args = new Bundle();
        args.putBoolean(ConciergeOrderHistoryContainerIntent.IS_FROM_PROFILE_SCREEN, isFromProfileScreen);

        ConciergeOrderHistoryContainerFragment fragment = new ConciergeOrderHistoryContainerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_concierge_order_history_container;
    }

    @Nullable
    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_ORDER_TRACKING;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments().getBoolean(ConciergeOrderHistoryContainerIntent.IS_FROM_PROFILE_SCREEN)) {
            getBinding().layoutToolbar.toolbarTitle.setText(getString(R.string.shop_order_list_screen_title));
        } else {
            getBinding().layoutToolbar.toolbarTitle.setText(getString(R.string.shop_order_tracking_list_screen_title));
        }
        setToolbar(getBinding().layoutToolbar.toolbar, true);
        initializeTabs();
    }

    private void initializeTabs() {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFragment(ConciergeOrderHistoryFragment.newInstance(true),
                requireActivity().getString(R.string.shop_order_list_screen_current_order_tab));
        viewPagerAdapter.addFragment(ConciergeOrderHistoryFragment.newInstance(false),
                requireActivity().getString(R.string.shop_order_list_screen_past_order_tab));

        getBinding().viewPager.setAdapter(viewPagerAdapter);
        getBinding().tabs.setupWithViewPager(getBinding().viewPager);

        getBinding().tabs.setIndicatorWidth((int) getResources().getDimension(R.dimen.tab_indicator_width));
        getBinding().viewPager.setOffscreenPageLimit(2);

        //Select first tab by default.
        getBinding().getRoot().postDelayed(() -> getBinding().tabs.setSelectionTab(0),
                100);
    }
}
