package com.proapp.sompom.newui;

import androidx.fragment.app.Fragment;

import com.proapp.sompom.intent.ResetPasswordIntent;
import com.proapp.sompom.newui.fragment.ForgotPasswordFragment;
import com.proapp.sompom.newui.fragment.ResetPasswordFragment;

public class ResetPasswordActivity extends SupportResetPasswordDeepLinkActivity {

    @Override
    protected Fragment getFragment() {
        return ResetPasswordFragment.newInstance(new ResetPasswordIntent(getIntent()).getEmail());
    }

    @Override
    protected String getFragmentTag() {
        return ResetPasswordFragment.TAG;
    }
}
