package com.proapp.sompom.newui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.library.baseAdapters.BR;

import com.proapp.sompom.R;
import com.proapp.sompom.databinding.FragmentPayWithCardBinding;
import com.proapp.sompom.intent.PayWithCardIntent;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.PayWithCardDataManager;
import com.proapp.sompom.viewmodel.PayWithCardFragmentViewModel;
import com.sompom.abapayway.widget.ABACheckOutCardWebView;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by Veasna Chhom on 3/17/22.
 */

public class PayWithCardFragment extends AbsSupportABAPaymentFragment<FragmentPayWithCardBinding> {

    private PayWithCardFragmentViewModel mViewModel;
    @Inject
    public ApiService mApiService;

    public static PayWithCardFragment newInstance(String checkoutUrl, String continueSuccessUrl, String basketId) {

        Bundle args = new Bundle();
        args.putString(PayWithCardIntent.CHECKOUT_URL, checkoutUrl);
        args.putString(PayWithCardIntent.CONTINUE_SUCCESS_URL, continueSuccessUrl);
        args.putString(PayWithCardIntent.BASKET_ID, basketId);

        PayWithCardFragment fragment = new PayWithCardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_pay_with_card;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        getBinding().layoutToolbar.toolbarTitle.setText("");
        setToolbar(getBinding().layoutToolbar.toolbar, true);
        mViewModel = new PayWithCardFragmentViewModel(requireContext(),
                new PayWithCardDataManager(requireContext(),
                        mApiService,
                        getArguments().getString(PayWithCardIntent.BASKET_ID)));
        setVariable(BR.viewModel, mViewModel);
        String checkoutUrl = getArguments().getString(PayWithCardIntent.CHECKOUT_URL);
        String continueSuccessUrl = getArguments().getString(PayWithCardIntent.CONTINUE_SUCCESS_URL);
        loadCheckout(checkoutUrl, continueSuccessUrl);
    }

    @Override
    protected void onReceiveABAPaymentPushbackEvent(String basketId, boolean isPaymentSuccess, String message) {
        Timber.i("onReceiveABAPaymentPushbackEvent: basketId: " + basketId + ", isPaymentSuccess: " + isPaymentSuccess);
        if (requireActivity() instanceof PayWithCardFragmentListener) {
            ((PayWithCardFragmentListener) requireActivity()).updateAllowBackPress(false);
            ((PayWithCardFragmentListener) requireActivity()).onReceiveABAPaymentPushbackEvent(basketId, isPaymentSuccess, message);
        }
        mViewModel.showLoading(true);
        new Handler().postDelayed(() -> {
            mViewModel.hideLoading();
            showMakeOrderSuccessPopup();
            ((PayWithCardFragmentListener) requireActivity()).updateAllowBackPress(true);
        }, 2000);
    }

    private void loadCheckout(String checkoutUrl, String continueSuccessUrl) {
        getBinding().webView.setListener(new ABACheckOutCardWebView.ABACheckOutCardWebViewListener() {
            @Override
            public void onWebViewClosed(ABACheckOutCardWebView.ABACheckOutCardWebViewCloseByEvent event) {

            }

            @Override
            public void onPageFinished() {
                mViewModel.hideLoading();
            }
        });
        mViewModel.showLoading();
        getBinding().webView.loadCheckout(checkoutUrl, continueSuccessUrl);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mViewModel.cancelTimer();
    }

    public interface PayWithCardFragmentListener {
        void onReceiveABAPaymentPushbackEvent(String basketId, boolean isPaymentSuccess, String message);

        void updateAllowBackPress(boolean isAllowed);
    }
}
