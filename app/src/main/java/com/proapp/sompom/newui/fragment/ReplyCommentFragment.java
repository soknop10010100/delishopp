package com.proapp.sompom.newui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ObservableBoolean;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.ReplyCommentAdapter;
import com.proapp.sompom.databinding.FragmentReplyCommentBinding;
import com.proapp.sompom.helper.AnimationHelper;
import com.proapp.sompom.helper.LoginActivityResultCallback;
import com.proapp.sompom.helper.OpenCommentDialogHelper;
import com.proapp.sompom.listener.LoadMoreCommentDirectionCallback;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnCommentDialogClickListener;
import com.proapp.sompom.listener.OnCommentItemClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.LoadCommentDirection;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.emun.MediaType;
import com.proapp.sompom.model.emun.PostContextMenuItemType;
import com.proapp.sompom.model.notification.NotificationListAndRedirectionHelper;
import com.proapp.sompom.model.notification.RedirectionNotificationData;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.model.result.JumpCommentResponse;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.newui.dialog.PostContextMenuDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.PopUpCommentDataManager;
import com.proapp.sompom.utils.CopyTextUtil;
import com.proapp.sompom.utils.NetworkStateUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.newviewmodel.ReplyCommentFragmentViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class ReplyCommentFragment extends AbsBindingFragment<FragmentReplyCommentBinding>
        implements ReplyCommentFragmentViewModel.OnCallback,
        OnCommentItemClickListener {
    public static final String TAG = ReplyCommentFragment.class.getName();

    @Inject
    public ApiService mApiService;
    private OnCommentDialogClickListener mOnCommentDialogClickListener;
    private ReplyCommentFragmentViewModel mViewModel;
    private ReplyCommentAdapter mReplyCommentAdapter;
    private Comment mComment;
    private int mPosition;
    private String mContentId;
    private ContentType mContentType;
    private String mPostId;
    private int mAddSubCommentCounter;

    public static ReplyCommentFragment newInstance(String itemID) {
        Bundle args = new Bundle();
        args.putString(SharedPrefUtils.ID, itemID);
        ReplyCommentFragment fragment = new ReplyCommentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ReplyCommentFragment newInstance(String itemID, Comment comment) {
        Bundle args = new Bundle();
        args.putString(SharedPrefUtils.ID, itemID);
        args.putParcelable(SharedPrefUtils.DATA, comment);
        ReplyCommentFragment fragment = new ReplyCommentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ReplyCommentFragment newInstanceForSubCommentRedirection(RedirectionNotificationData data) {
        Bundle args = new Bundle();
        args.putParcelable(NotificationListAndRedirectionHelper.NOTIFICATION_EXCHANGE_DATA, data);
        ReplyCommentFragment fragment = new ReplyCommentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public int getAddSubCommentCounter() {
        return mAddSubCommentCounter;
    }

    public void setContentType(ContentType contentType) {
        mContentType = contentType;
    }

    public void setPostId(String postId) {
        mPostId = postId;
    }

    public void setOnCommentDialogClickListener(OnCommentDialogClickListener onCommentDialogClickListener) {
        mOnCommentDialogClickListener = onCommentDialogClickListener;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_reply_comment;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (enter && nextAnim > 0) {
            Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    bindViewModel(null);
                }
            });
            return anim;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        bindGifView();
        if (getArguments() != null) {
            mContentId = getArguments().getString(SharedPrefUtils.ID);
            mComment = getArguments().getParcelable(SharedPrefUtils.DATA);
            RedirectionNotificationData redirectionNotificationData = getArguments().getParcelable(NotificationListAndRedirectionHelper.NOTIFICATION_EXCHANGE_DATA);
            if (redirectionNotificationData != null) {
                bindViewModel(redirectionNotificationData);
            } else if (mComment != null) {
                bindViewModel(null);
            }
        }

        getBinding().buttonClose.setOnClickListener(v -> {
            if (mOnCommentDialogClickListener != null) {
                mOnCommentDialogClickListener.onDismissDialog(false);
            }
        });
    }

    public void setData(Comment comment, int position) {
        if (mViewModel != null) {
            //If the screen was already created and then we redirect to it again.
            //So will show loading screen first and then try to request new data.
            mViewModel.showLoading();
        }
        mPosition = position;
        mComment = comment;
    }

    private void bindViewModel(RedirectionNotificationData data) {
        PopUpCommentDataManager dataManager = new PopUpCommentDataManager(getActivity(),
                mApiService,
                data
        );
        mViewModel = new ReplyCommentFragmentViewModel(mPostId,
                mContentId,
                mContentType,
                mComment,
                dataManager,
                ReplyCommentFragment.this,
                (viewModel, newComment) -> {
                    if (mViewModel.getDataManager().isInJumpCommentMode()) {
                        mViewModel.getDataManager().resetToNormalTimeLineDetailMode();
                        mViewModel.getData(new OnCallbackListener<Response<List<Comment>>>() {
                            @Override
                            public void onFail(ErrorThrowable ex) {
                                viewModel.proceedSendNewComment(newComment);
                            }

                            @Override
                            public void onComplete(Response<List<Comment>> result) {
                                new Handler().postDelayed(() ->
                                        viewModel.proceedSendNewComment(newComment), 500);

                            }
                        }, true);
                    } else {
                        viewModel.proceedSendNewComment(newComment);
                    }
                });
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onReplyButtonClick(Comment comment, int position) {
        requestShowKeyboard();
    }

    @Override
    public void onLikeButtonClick(Comment comment, final int position, OnCallbackListener<Response<Object>> listener) {
        mViewModel.checkToLikeComment(comment, position, new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (listener != null) {
                    listener.onFail(ex);
                }
            }

            @Override
            public void onComplete(Response<Object> result) {
                if (position == 0) {
                    mComment.setContentStat(mComment.getContentStat());
                    mComment.setLike(comment.isLike());
                    if (mOnCommentDialogClickListener != null) {
                        mOnCommentDialogClickListener.onNotifyCommentItemChange(mComment);
                    }
                }
                if (listener != null) {
                    listener.onComplete(result);
                }
            }
        });
    }

    @Override
    public void onLongPress(Comment comment, int position) {
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        boolean isOwner = comment.getUser() != null && SharedPrefUtils.checkIsMe(getActivity(), comment.getUser().getId());
        dialog.addItem(PostContextMenuItemType.getCommentPopupMenuItem(comment, isOwner));
        dialog.setOnItemClickListener(result -> {
            dialog.dismiss();
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.COPY_TEXT_CLICK) {
                CopyTextUtil.copyTextToClipboard(getActivity(), comment.getContent());
            } else if (itemType == PostContextMenuItemType.EDIT_POST_CLICK) {
                requestShowKeyboard();
                getBinding().includeComment.messageInput.setText(comment.getContent(), false);
                mViewModel.setUpdateComment(comment, position);

            } else {
                MessageDialog messageDialog = new MessageDialog();
                messageDialog.setTitle(getString(R.string.comment_popup_confirm_delete_title));
                messageDialog.setMessage(getString(R.string.comment_popup_confirm_delete_description));
                messageDialog.setRightText(getString(R.string.setting_change_language_dialog_cancel_button), null);
                messageDialog.setLeftText(getString(R.string.comment_popup_menu_delete),
                        v -> mViewModel.onDeleteComment(comment, position, () -> {
                            if (position == 0) {
                                if (mOnCommentDialogClickListener != null) {
                                    mOnCommentDialogClickListener.onMainCommentItemRemoved(mPosition, comment);
                                }
                            } else {
                                removeSubCommentFrommList(comment.getId());
                                mReplyCommentAdapter.notifyItemRemove(position);
                                int totalSubComment = mComment.getTotalSubComment() - 1;
                                mComment.setTotalSubComment(totalSubComment);
                                if (mOnCommentDialogClickListener != null) {
                                    mOnCommentDialogClickListener.onNotifyCommentItemChange(mComment);
                                }
                            }
                        }));
                messageDialog.show(getChildFragmentManager());
            }
        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    private void removeSubCommentFrommList(String subCommentId) {
        if (mComment.getReplyComment() != null && !mComment.getReplyComment().isEmpty()) {
            for (int i = mComment.getReplyComment().size() - 1; i >= 0; i--) {
                if (TextUtils.equals(subCommentId, mComment.getReplyComment().get(i).getId())) {
                    mComment.getReplyComment().remove(i);
                    Timber.i("Removed sub comment from list: " + subCommentId);
                    break;
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().includeComment.messageInput.removeListPopupWindow();
    }

    private void bindGifView() {
        getBinding().includeComment.containerSearchGif.onBackPressClickListener(v -> {
            getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
            getBinding().includeComment.messageInput.requestFocus();
        });

        getBinding().includeComment.gifImageView.setOnClickListener(v -> {
            getBinding().includeComment.sendLayout.setVisibility(View.GONE);
            getBinding().includeComment.containerSearchGif.loadTrendingGifList(baseGifModel -> {
                getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
                getBinding().includeComment.messageInput.requestFocus();

                AbsBaseActivity absBaseActivity = (AbsBaseActivity) getActivity();
                if (absBaseActivity != null) {
                    absBaseActivity.startLoginWizardActivity(new LoginActivityResultCallback() {
                        @Override
                        public void onActivityResultSuccess(boolean isAlreadyLogin) {
                            Media media = new Media();
                            media.setWidth(baseGifModel.getGifWidth());
                            media.setHeight(baseGifModel.getGifHeight());
                            media.setUrl(baseGifModel.getGifUrl());
                            media.setType(MediaType.TENOR_GIF);

                            Comment comment = new Comment();
                            comment.setContent("gif");
                            comment.setUser(SharedPrefUtils.getUser(getActivity()));
                            comment.setDate(new Date());
                            comment.setMedia(media);
                            if (mContentType != null) {
                                comment.setContentType(mContentType.getValue());
                            }
                            comment.setPostId(mPostId);
                            onSendText(comment);
                            mViewModel.postComment(comment);

                        }
                    });
                }
            });
        });
    }

    public boolean isCloseSearchGifView() {
        if (getBinding().includeComment.containerSearchGif.getVisibility() == View.VISIBLE) {
            getBinding().includeComment.containerSearchGif.clearData();
            getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
            getBinding().includeComment.messageInput.requestFocus();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onSendText(Comment comment) {
        if (NetworkStateUtil.isNetworkAvailable(requireContext())) {
            //Will add the instance comment in list only if there is internet connection.
            if (mReplyCommentAdapter != null) {
                mReplyCommentAdapter.addLatestComment(comment);
                getBinding().recyclerView.scrollToPosition(mReplyCommentAdapter.getItemCount() - 1);
            }
        }
    }

    @Override
    public void onPostCommentFail() {
        if (mReplyCommentAdapter != null) {
            mReplyCommentAdapter.removeInstanceComment();
        }
    }

    @Override
    public void onPostCommentSuccess(Comment comment) {

    }


    @Override
    public void onPostSubCommentSuccess(Comment comment) {
        mAddSubCommentCounter++;
        mReplyCommentAdapter.removeInstanceComment();
        mReplyCommentAdapter.addNewComment(Collections.singletonList(comment));
        updateTotalCommentCount(comment);
        mOnCommentDialogClickListener.onPostSubCommentSuccess();
    }

    @Override
    public void onRemoveSubCommentSuccess() {
        mAddSubCommentCounter--;
        mOnCommentDialogClickListener.onRemoveSubCommentSuccess();
    }

    private void updateTotalCommentCount(Comment comment) {
        List<Comment> commentList = mComment.getReplyComment();
        if (commentList == null) {
            commentList = new ArrayList<>();
        }
        commentList.add(comment);
        mComment.setReplyComment(commentList);
        mComment.setTotalSubComment(mComment.getTotalSubComment() + 1);
        mOnCommentDialogClickListener.onNotifyCommentItemChange(mComment);
    }

    @Override
    public void onSubCommentUpdated(Comment comment, int position) {
        Timber.i("onSubCommentUpdated: position: " + position + ", comment: " + comment);
        mReplyCommentAdapter.updateComment(position, comment);
    }

    @Override
    public void onPullAndRefresh(ObservableBoolean refresh) {
        if (mReplyCommentAdapter.canInvokeLoadPreviousComment()) {
            mReplyCommentAdapter.invokeLoadPreviousComment();
        } else {
            refresh.set(false);
        }
    }

    @Override
    public void onUpdateCommentSuccess(Comment comment, int position) {
        Timber.i("onUpdateCommentSuccess: position: " + position + ", comment: " + new Gson().toJson(comment));
        hideSoftKeyboard();
        mReplyCommentAdapter.notifyItemChanged(position, comment);
        if (position == 0) {
            mComment.setContent(comment.getContent());
            mOnCommentDialogClickListener.onNotifyCommentItemChange(mComment);
        }
    }

    @Override
    public void onLoadCommentSuccess(Comment mainComment,
                                     List<Comment> listComment,
                                     boolean isInJumpMode,
                                     boolean isCanLoadPrevious,
                                     boolean isCanLoadNext) {
        Timber.i("onLoadCommentSuccess: " + listComment.size() +
                ", mainComment: " + mainComment +
                ", isInJumpMode: " + isInJumpMode +
                ", isCanLoadPrevious: " + isCanLoadPrevious +
                ", isCanLoadNext: " + isCanLoadNext);
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getBinding().recyclerView.setOnTouchListener((v, e) -> {
            hideSoftKeyboard();
            v.performClick();
            return false;
        });

        List<Adaptive> adaptiveList = new ArrayList<>();
        if (mainComment != null) {
            mComment = mainComment;
        }
        adaptiveList.add(mComment);
        if (isCanLoadPrevious) {
            adaptiveList.add(new LoadCommentDirection(JumpCommentResponse.REDIRECTION_PREVIOUS));
        }
        if (listComment != null) {
            adaptiveList.addAll(listComment);
        }
        if (isCanLoadNext) {
            adaptiveList.add(new LoadCommentDirection(JumpCommentResponse.REDIRECTION_NEXT));
        }
        mReplyCommentAdapter = new ReplyCommentAdapter(adaptiveList,
                this,
                (loadCommentDirection, position) ->
                        mViewModel.loadMoreCommentFromDirection(loadCommentDirection.getRedirection(), new LoadMoreCommentDirectionCallback() {
                            @Override
                            public void onFail(ErrorThrowable ex, boolean isInJumpMode1, boolean isLoadPrevious1, boolean isCanLoadNext1) {
                                OpenCommentDialogHelper.onLoadMoreCommentFinish(new ArrayList<>(),
                                        isInJumpMode1,
                                        isLoadPrevious1,
                                        isCanLoadNext1,
                                        loadCommentDirection,
                                        position,
                                        mReplyCommentAdapter);
                            }

                            @Override
                            public void onLoadCommentDirectionSuccess(List<Comment> listComment1,
                                                                      boolean isInJumpMode1,
                                                                      boolean isLoadPrevious1,
                                                                      boolean isCanLoadNext1) {
                                OpenCommentDialogHelper.onLoadMoreCommentFinish(listComment1,
                                        isInJumpMode1,
                                        isLoadPrevious1,
                                        isCanLoadNext1,
                                        loadCommentDirection,
                                        position,
                                        mReplyCommentAdapter);
                            }
                        }));
        mReplyCommentAdapter.setCanLoadNext(isCanLoadNext);
        getBinding().recyclerView.scrollToPosition(adaptiveList.size() - 1);
        getBinding().recyclerView.setAdapter(mReplyCommentAdapter);
        if (isInJumpMode) {
            checkToMakeBouncingAnimationForRedirectionComment();
        }
    }

    private void hideSoftKeyboard() {
        if (getContext() != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(getBinding().includeComment.messageInput.getWindowToken(), 0);
            }
        }
    }

    private void requestShowKeyboard() {
        getBinding().includeComment.messageInput.getMentionsEditText().post(() ->
                showKeyboard(getBinding().includeComment.messageInput.getMentionsEditText()));
    }

    private void checkToMakeBouncingAnimationForRedirectionComment() {
        if (mViewModel.getDataManager().isInJumpCommentMode() &&
                getBinding().recyclerView.getLayoutManager() != null &&
                mReplyCommentAdapter != null &&
                mReplyCommentAdapter.getItemCount() > 0) {
            int itemPosition = mReplyCommentAdapter.findItemPosition(mViewModel.getDataManager().getRedirectionNotificationData().getSubCommentId());
            new Handler().postDelayed(() -> {
                if (itemPosition >= 0) {
                    getBinding().recyclerView.smoothScrollToPosition(itemPosition);
                    new Handler().postDelayed(() -> {
                                View viewByPosition = getBinding().recyclerView.getLayoutManager().findViewByPosition(itemPosition);
                                if (viewByPosition != null) {
                                    AnimationHelper.animateBounce(viewByPosition, null);
                                }
                            },
                            NotificationListAndRedirectionHelper.ANIMATION_DELAY);
                } else {
                    NotificationListAndRedirectionHelper.showRedirectionNotificationContentNotFound((AppCompatActivity) requireActivity());
                }
            }, NotificationListAndRedirectionHelper.SCROLL_DELAY);
        }
    }
}
