package com.proapp.sompom.newui.fragment;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.proapp.sompom.BR;
import com.proapp.sompom.R;
import com.proapp.sompom.chat.AbsChatBinder;
import com.proapp.sompom.chat.call.CallHelper;
import com.proapp.sompom.chat.call.IncomingCallDataHolder;
import com.proapp.sompom.chat.call.agora.VOIPCallMessageListener;
import com.proapp.sompom.chat.service.ChatSocket;
import com.proapp.sompom.chat.service.SocketService;
import com.proapp.sompom.databinding.FragmentHomeBinding;
import com.proapp.sompom.helper.ApplicationHelper;
import com.proapp.sompom.helper.ConciergeHelper;
import com.proapp.sompom.helper.FlurryHelper;
import com.proapp.sompom.injection.controller.PublicQualifier;
import com.proapp.sompom.intent.CallingIntent;
import com.proapp.sompom.listener.OnBadgeUpdateListener;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.OnCompleteListener;
import com.proapp.sompom.listener.OnHomeMenuClick;
import com.proapp.sompom.model.concierge.ConciergeShopSetting;
import com.proapp.sompom.model.result.AppFeature;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.ChatActivity;
import com.proapp.sompom.newui.dialog.UserLoginRegisterBottomSheetDialog;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.ProductListDataManager;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.newviewmodel.HomeFragmentViewModel;
import com.proapp.sompom.widget.HomeTabLayout;

import java.util.Map;

import javax.inject.Inject;

import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/22/18.
 */

public class HomeFragment extends AbsBindingFragment<FragmentHomeBinding> implements AbsBaseActivity.OnServiceListener,
        OnBadgeUpdateListener, VOIPCallMessageListener, HomeFragmentViewModel.HomeFragmentViewModelLister {

    public static final String ON_CONVERSATION_BADGE_UPDATE_TO_ZERO_EVENT = "ON_CONVERSATION_BADGE_UPDATE_TO_ZERO_EVENT";

    @Inject
    public ApiService mApiService;
    @Inject
    @PublicQualifier
    public ApiService mPublicApiService;
    private boolean mIsLoadMessageData;
    private SocketService.SocketBinder mSocketBinder;
    private BroadcastReceiver mRefreshNotificationBadgeReceiver;
    private HomeFragmentViewModel mViewModel;
    private BroadcastReceiver mUpdateUserDisplayDataReceiver;

    private AppBarLayout.OnOffsetChangedListener mAppBarOffsetListener;

    public static HomeFragment newInstance(boolean isLoadMessageData) {
        Bundle args = new Bundle();
        args.putBoolean(SharedPrefUtils.STATUS, isLoadMessageData);
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected String getFlurryLogEventKey() {
        return FlurryHelper.SCREEN_SHOP_HOME;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_home;
    }

    @Override
    protected boolean shouldListenUserLoginSuccessEven() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        if (requireActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) requireActivity()).addOnServiceListener(this);
        }
        initUpdateUserDisplayDataReceiver();
        initNotificationBadgeReceiver();
        setToolbar(getBinding().toolbar, false);
        if (getArguments() != null) {
            mIsLoadMessageData = getArguments().getBoolean(SharedPrefUtils.STATUS, false);
        }
        ProductListDataManager manager = new ProductListDataManager(getActivity(),
                mApiService,
                mApiService,
                mApiService,
                mPublicApiService);
        mViewModel = new HomeFragmentViewModel(manager,
                isEnable -> {
                    AbsBaseFragment fr = HomeFragment.this.getBinding().viewpager.getFragmentAt(0);
                    if (fr instanceof WallStreetFragment) {
                        ((WallStreetFragment) fr).setEnableQueryOption(isEnable);
                    }
                },
                this);
        mViewModel.setOnBadgeUpdateListener(userBadge -> {
            if (userBadge != null && userBadge.getConciergeOrderBadge() != null) {
                updateConciergeTabBadge(userBadge.getConciergeOrderBadge());
            }
        });

        setVariable(BR.viewModel, mViewModel);
        mViewModel.checkToRequestMandatoryData();
    }

    @SuppressLint("NewApi")
    @Override
    protected void onRequestPermissionLauncherCallback(Map<String, Boolean> permissionsResult) {
        getBinding().viewpager
                .getAllFragments()
                .forEach(fragment -> fragment.onRequestPermissionLauncherCallback(permissionsResult));
    }

    @Override
    protected void onRefreshScreen(boolean isFromSuccessfulAuthentication) {
        Timber.i("onRefreshScreen");
        requireActivity().recreate();
    }

    private void initUpdateUserDisplayDataReceiver() {
        mUpdateUserDisplayDataReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive: update user action: ");
                mViewModel.reloadUserProfile();
            }
        };
        requireContext().registerReceiver(mUpdateUserDisplayDataReceiver, new IntentFilter(ChatSocket.UPDATE_USER_EVENT));
    }

    private void initNotificationBadgeReceiver() {
        mRefreshNotificationBadgeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.i("onReceive refresh notification badge event.");
                mViewModel.getUserBadge(HomeFragment.this);
            }
        };

        requireActivity().registerReceiver(mRefreshNotificationBadgeReceiver,
                new IntentFilter(ChatActivity.REFRESH_NOTIFICATION_BADGE_ACTION));
    }

    public void checkGoToConversationTab() {
        if (!(getBinding().viewpager.getCurrentFragment() instanceof MessageFragment)) {
            getBinding().viewpager.setCurrentItem(1, true);
        }
    }

    public void checkGoToConciergeTab() {
        if (!(getBinding().viewpager.getCurrentFragment() instanceof ConciergeFragment)) {
            // Locate the Concierge fragment dynamically and then set viewpager to the correct index
            for (int index = 0; index < getBinding().viewpager.getAllFragments().size(); index++) {
                if (getBinding().viewpager.getFragmentAt(index) instanceof ConciergeFragment) {
                    getBinding().viewpager.setCurrentItem(index, true);
                    break;
                }
            }
        }
    }

    public void updateConciergeTabBadge(long newCount) {
        for (int index = 0; index < getBinding().viewpager.getAllFragments().size(); index++) {
            if (getBinding().viewpager.getFragmentAt(index) instanceof ConciergeFragment) {
                ((ConciergeFragment) getBinding().viewpager.getFragmentAt(index)).updateOrderBadgeCount(newCount);
                break;
            }
        }
    }

    private void initTab() {
//        getBinding().viewpager.setPageTransformer(false, new ZoomOutPageTransformer());
        getBinding().viewpager.setupAdapter(getActivity(),
                getChildFragmentManager(),
                () -> getBinding().appBar.setExpanded(false),
                mIsLoadMessageData,
                getBinding().tabs);
        mViewModel.loadApplicationModeIcon();
        mAppBarOffsetListener = (appBarLayout, verticalOffset) -> {
            getBinding().viewpager.onAppBarOffsetChange(verticalOffset);
        };
        getBinding().appBar.addOnOffsetChangedListener(mAppBarOffsetListener);

        getBinding().tabs.setTabLayoutChangeListener(new HomeTabLayout.OnTabLayoutChangeListener() {
            @Override
            public void onPageReselect(AbsBaseFragment fragment, int position) {
                if (fragment instanceof WallStreetFragment) {
                    WallStreetFragment wallStreetFr = (WallStreetFragment) fragment;
                    shouldScroll(wallStreetFr, true);
                } else if (fragment instanceof MessageFragment) {
                    ((MessageFragment) fragment).checkToRefreshMessageSectionIfNecessary();
                } else if (fragment instanceof UserListFragment) {
                    ((UserListFragment) fragment).scrollToTopAndRefreshIfNecessary();
                } else if (fragment instanceof ConciergeFragment) {
                    ((ConciergeFragment) fragment).scrollToTopAndRefresh(true);
                }
                getBinding().appBar.setExpanded(!(fragment instanceof NearbyMapFragment), true);
            }

            @Override
            public void onPageChanged(AbsBaseFragment fragment, int position) {
                if (fragment instanceof OnHomeMenuClick) {
                    ((OnHomeMenuClick) fragment).onTabMenuClick();
                } else if (fragment instanceof ConciergeFragment) {
                    ((ConciergeFragment) fragment).checkToGetUserCurrentLocation();
                }
                getBinding().appBar.setExpanded(!(fragment instanceof NearbyMapFragment), true);

                AppFeature.NavigationBarMenu menu = getBinding().tabs.getTabViewModels().get(position).getNavBarMenu();
                mViewModel.onNavMenuChange(menu);

                //No need to show post floating button
//                checkToHideFloatingButton(fragment);
            }

            @Override
            public void onPageClick(AppFeature.NavigationBarMenu navigationBarMenu) {
                ConciergeShopSetting shopSetting = ConciergeHelper.getConciergeShopSetting(requireContext());
                switch (navigationBarMenu.getNavMenuScreen()) {
                    case Wall:
                    case Conversation:
                    case Contact:
                    case Shop:
                        break;
                    case ShopDetail:
                        if (shopSetting != null && !TextUtils.isEmpty(shopSetting.getPrimaryShopId())) {
                            if (navigationBarMenu.getNavMenuButton() == AppFeature.NavMenuButton.ShopCategory) {
                                // Open primary shop detail screen
                                mViewModel.onShopDetailClicked(shopSetting.getPrimaryShopId(), false);
                            } else if (navigationBarMenu.getNavMenuButton() == AppFeature.NavMenuButton.ShopDiscount) {
                                // Open discount shop detail screen
                                mViewModel.onShopDetailClicked(shopSetting.getPrimaryShopId(), true);
                            }
                        }
                        break;
                    case Chat:
                        if (navigationBarMenu.getNavMenuButton() == AppFeature.NavMenuButton.Support) {
                            if (ApplicationHelper.isInVisitorMode(getContext())) {
                                mViewModel.requestUserAuthentication(result ->
                                        mViewModel.onSupportConversationClicked(null));
                            } else {
                                mViewModel.onSupportConversationClicked(null);
                            }
                        }
                        break;
                }
            }
        });
        getBinding().tabs.setupWithViewPager(getBinding().viewpager);
    }

    @Override
    public void onSwitchApplicationMode(boolean isSwitchToExpressMode) {
        mViewModel.showOrHideLoadingScreen(true);
        ApplicationHelper.setInExpressMode(requireContext(), isSwitchToExpressMode);
        ConciergeHelper.clearLocalBasket(requireContext(),
                new OnCallbackListener<Response<Object>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        SharedPrefUtils.setAppFeature(requireContext(), null);
                        mViewModel.showOrHideLoadingScreen(false);
                        requireActivity().startActivity(ApplicationHelper.getFreshHomeIntent(getContext()));
                    }

                    @Override
                    public void onComplete(Response<Object> result) {
                        SharedPrefUtils.setAppFeature(requireContext(), null);
                        mViewModel.showOrHideLoadingScreen(false);
                        requireActivity().startActivity(ApplicationHelper.getFreshHomeIntent(getContext()));
                    }
                });
    }

    private void shouldScroll(WallStreetFragment wallStreetFr, boolean isPullToRefresh) {
        //Old code
//        if (wallStreetFr.findFirstVisibleItemPosition() > 0 && !getBinding().fab.isShown()) {
//            AnimationHelper.showFabButton(getBinding().fab, new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationStart(Animator animation) {
//                    getBinding().fab.show();
//                }
//            });
//        }
//        wallStreetFr.shouldScrollTop(isPullToRefresh);=

        wallStreetFr.shouldScrollTop(isPullToRefresh);
    }

    /**
     * @return true: mean can back press, otherwise, cannot
     */
    public boolean onBackPress() {
        //Allow to make back press if the home screen is displaying error.
        if (mViewModel != null && mViewModel.isDisplayingError()) {
            return true;
        }

        /*
            In case there is failure with load app toolbar, normally we show the error and we will
            allow user to click back to exit the app too.
         */
        if (getBinding().viewpager.isLoadAppToolbarFailed()) {
            return true;
        }

        //Logic of handle that contain wall toolbar
        if (getBinding().viewpager.getFragmentAt(0) instanceof WallStreetFragment) {
            if (getBinding().viewpager.getCurrentFragment() instanceof WallStreetFragment) {
                WallStreetFragment wallStreetFr = (WallStreetFragment) getBinding().viewpager.getCurrentFragment();
                if (wallStreetFr.findFirstVisibleItemPosition() > 0) {
                    //No need to show post floating button
                    shouldScroll(wallStreetFr, true);
                    return false;
                }
            } else {
                getBinding().viewpager.setCurrentItem(0, true);
                return false;
            }

            return true;
        } else {
            /*
                If the first toolbar menu is not yet navigate too, we will navigate to it and then
                allow the back press to exit the app.
             */
            AbsBaseFragment firstMenu = getBinding().viewpager.getFragmentAt(0);
            if (getBinding().viewpager.getCurrentFragment().equals(firstMenu)) {
                return true;
            } else {
                getBinding().viewpager.setCurrentItem(0, true);
                return false;
            }
        }
    }

    @Override
    public void onServiceReady(AbsChatBinder binder) {
        mSocketBinder = (SocketService.SocketBinder) binder;
        mSocketBinder.addBadgeListener(this);
        mSocketBinder.addCallVOIPMessageListeners(this);
    }

    @Override
    public void onReceiveVOIPCallMessage(Chat message) {
        Timber.i("onReceiveVOIPMessage: " + new Gson().toJson(message));
        if (CallHelper.isInComingVOIPCallMessageType(message) &&
                CallHelper.shouldShowIncomingCallScreenOnReceiveIncomingInsideScreen()) {
            CallHelper.clearRejectedCallList();
            IncomingCallDataHolder incomingCallDataHolder = new IncomingCallDataHolder();
            incomingCallDataHolder.setCallType(message.getSendingTypeAsString());
            incomingCallDataHolder.setCallData(message.getCallData());
            Timber.i("Incoming call data: " + new Gson().toJson(incomingCallDataHolder));
            requireActivity().startActivity(new CallingIntent(getContext(), incomingCallDataHolder));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requireActivity().unregisterReceiver(mRefreshNotificationBadgeReceiver);
        requireActivity().unregisterReceiver(mUpdateUserDisplayDataReceiver);
        if (mAppBarOffsetListener != null) {
            getBinding().appBar.removeOnOffsetChangedListener(mAppBarOffsetListener);
        }
        if (mSocketBinder != null) {
            mSocketBinder.removeBadgeListener(this);
            mSocketBinder.removeCallVOIPMessageListeners(this);
        }
    }

    @Override
    public void onConversationBadgeUpdate(int value) {
        getBinding().tabs.addConversationBadge(value);
    }

    @Override
    public void onShouldInitTab() {
        initTab();
    }

    @Override
    public void showLoginSignupBottomSheet(OnCompleteListener<Object> listener) {
        UserLoginRegisterBottomSheetDialog dialog = UserLoginRegisterBottomSheetDialog.newInstance();
        dialog.show(getChildFragmentManager(), UserLoginRegisterBottomSheetDialog.TAG);
        getChildFragmentManager().setFragmentResultListener(UserLoginRegisterBottomSheetDialog.REQUEST_LOGIN_OR_SIGNUP_KEY,
                getViewLifecycleOwner(),
                (resultRequestKey, result) -> {
                    if (resultRequestKey.equals(UserLoginRegisterBottomSheetDialog.REQUEST_LOGIN_OR_SIGNUP_KEY)) {
                        if (result.getInt(UserLoginRegisterBottomSheetDialog.LOGIN_RESULT_KEY) == AppCompatActivity.RESULT_OK) {
                            listener.onComplete(new Object());
                        }
                    }
                });
    }

    public interface HomeFragmentListener {
        void onShouldReloadScreen();
    }
}
