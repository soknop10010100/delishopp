package com.proapp.sompom.newui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.proapp.sompom.intent.newintent.ConciergeCurrentLocationIntent;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.newui.fragment.ConciergeCurrentLocationFragment;

/**
 * Created by Or Vitovongsak on 16/11/21.
 */
public class ConciergeCurrentLocationActivity extends AbsDefaultActivity
        implements ConciergeCurrentLocationFragment.ConciergeCurrentLocationCallback {

    private Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getThis();
        setFragment(ConciergeCurrentLocationFragment.newInstance(), ConciergeCurrentLocationFragment.TAG);
    }

    @Override
    public void onLocationSelected(ConciergeUserAddress address) {
        if (mContext instanceof AbsBaseActivity) {
            Intent intent = new Intent();
            intent.putExtra(ConciergeCurrentLocationIntent.USER_CURRENT_ADDRESS, address);
            ((AbsBaseActivity) mContext).setResult(AppCompatActivity.RESULT_OK, intent);
            ((AbsBaseActivity) mContext).finish();
        }
    }
}
