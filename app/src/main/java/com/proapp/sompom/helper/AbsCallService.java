package com.proapp.sompom.helper;

import android.text.TextUtils;

import com.proapp.sompom.broadcast.AbsService;
import com.proapp.sompom.chat.call.CallTimerHelper;
import com.proapp.sompom.model.call.CallData;
import com.proapp.sompom.model.result.User;

import java.util.List;
import java.util.Map;

/**
 * Created by Chhom Veasna on 12/2/2019.
 */
public abstract class AbsCallService extends AbsService {

    public enum CallActionType {

        CALL("call"),
        INCOMING("incoming"),
        DECLINE("decline"),
        CANCEL("cancel"),
        PROGRESS("progress"),
        BUSY("busy"),
        JOINED("joined"),
        CANCEL_REQUEST_VIDEO("cancelRequestVideo"),
        RINGING("ringing"),
        SPEAKING("speaking"),
        NONE("none");

        private String mValue;

        CallActionType(String value) {
            mValue = value;
        }

        public String getValue() {
            return mValue;
        }

        public static CallActionType getTypeFromValue(String value) {
            for (CallActionType callActionType : CallActionType.values()) {
                if (TextUtils.equals(callActionType.getValue(), value)) {
                    return callActionType;
                }
            }

            return NONE;
        }
    }

    public enum CallNotificationActionType {

        ACCEPT(1, 0x111),
        DECLINE(2, 0x112),
        JOIN(3, 0x113),
        END(4, 0x114),
        MESSAGE(5, 0x115),
        UNDEFINED(-1, 0x110);

        private int mAction;
        private int mRequestCode;

        CallNotificationActionType(int action, int requestCode) {
            mAction = action;
            mRequestCode = requestCode;
        }

        public int getRequestCode() {
            return mRequestCode;
        }

        public int getAction() {
            return mAction;
        }

        public static CallNotificationActionType getFromValue(int value) {
            for (CallNotificationActionType callActionType : CallNotificationActionType.values()) {
                if (callActionType.mAction == value) {
                    return callActionType;
                }
            }

            return UNDEFINED;
        }
    }

    public enum CallType {

        VOICE("callAudio"),
        VIDEO("callVideo"),
        GROUP_VOICE("callGroupAudio"),
        GROUP_VIDEO("callGroupVideo"),
        REJECT_CALL("rejectCall"),
        RINGING("ringing"),
        BUSY("callBusy"),
        REQUEST_VIDEO("requestVideo"),
        ACCEPTED_VIDEO("acceptVideo"),
        CALL_ACCEPTED("callAccepted"),
        REJECT_VIDEO("rejectVideo"),
        UNKNOWN("unknown");

        private String mValue;

        CallType(String value) {
            mValue = value;
        }

        public String getValue() {
            return mValue;
        }

        public static CallType getFromValue(String value) {
            for (CallType callType : CallType.values()) {
                if (TextUtils.equals(callType.mValue, value)) {
                    return callType;
                }
            }

            return UNKNOWN;
        }
    }

    public interface CallingListener {

        void onCallEnd(CallType callType,
                       String channelId,
                       boolean isGroup,
                       User recipient,
                       boolean isMissedCall,
                       int calledDuration,
                       boolean isEndedByClickAction,
                       boolean isCauseByTimeOut,
                       boolean isCausedByParticipantOffline);

        default void onIncomingCall(CallType calType, Map<String, Object> data1, Map<String, Object> data2) {
        }

        default void onCallEstablished(CallType callType, String channelId, boolean isGroup) {
        }

        default void onCallInfoUpdated(CallType callType,
                                       String info,
                                       boolean isCallProfileVisible,
                                       boolean showCallControls) {
        }

        default void onCheckShowingRequestVideoCamera(boolean shouldShow,
                                                      CallType callType,
                                                      User requester,
                                                      boolean isGroupCall) {
        }

        default void onCallTimerUp(CallType callType,
                                   CallTimerHelper.TimerType timerType,
                                   CallTimerHelper.StarterTimerType starterTimerType) {
        }

        default void addCallView(CallType callType,
                                 boolean isGroupCall,
                                 List<CallData> callDataList) {
        }

        default void removeCallView(String userId) {
        }

        default void onVideoCameraStateChanged(String userId,
                                               boolean isOff,
                                               boolean isCurrentUser) {
        }

        default void onRequestingCameraStateChanged(CallData callData,
                                                    boolean isRequestingCamera) {
        }

        default void onReceivingCameraRequestStateChanged(CallData participantData) {
        }

        default void onOtherParticipantAcceptVideoRequest(CallData participantData) {
        }

        default void onAudioStateChanged(String userId, boolean isMute) {
        }

        default void onCurrentUserJoinOrLeaveGroupCall(CallType callType,
                                                       String channelId,
                                                       boolean isLeft,
                                                       int callDuration) {
        }

        default void onGroupMemberJoinedCall(User user) {
        }

        default void onGroupMemberLeftGroupCall(User user) {
        }
    }
}
