package com.proapp.sompom.helper;

import android.os.CountDownTimer;

import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

public class TaskDebouncer<T> {

    private CountDownTimer mCountDownTimer;
    private Map<String, T> mItem = new HashMap<>();
    private int mDebounceInterval;
    private APIRequestDebouncerListener<T> mListener;
    private boolean mIsRunning;

    public TaskDebouncer(int debounceInterval, APIRequestDebouncerListener<T> listener) {
        mDebounceInterval = debounceInterval;
        mListener = listener;
        init();
    }

    private void init() {
        mCountDownTimer = new CountDownTimer(mDebounceInterval, 1000L) {
            @Override
            public void onTick(long millisUntilFinished) {
                Timber.i("onTick: millisUntilFinished: " + millisUntilFinished);
            }

            @Override
            public void onFinish() {
                Timber.i("onFinish: " + mItem.size());
                Map<String, T> result = new HashMap<>(mItem);
                mItem.clear();
                mIsRunning = false;
                if (mListener != null) {
                    mListener.onDebounce(result);
                }
            }
        };
    }

    public void debounce(String key, T item) {
        mItem.put(key, item);
        if (!mIsRunning) {
            mIsRunning = true;
            mCountDownTimer.start();
        }
    }

    public interface APIRequestDebouncerListener<T> {
        void onDebounce(Map<String, T> items);
    }
}
