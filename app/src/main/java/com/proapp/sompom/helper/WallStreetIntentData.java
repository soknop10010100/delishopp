package com.proapp.sompom.helper;

import com.proapp.sompom.adapter.newadapter.TimelineAdapter;
import com.proapp.sompom.listener.OnTimelineHeaderItemClickListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.newui.AbsBaseActivity;

import timber.log.Timber;

/**
 * Created by He Rotha on 2/1/19.
 */
public class WallStreetIntentData implements IntentData {
    private final AbsBaseActivity mContext;
    private final OnTimelineHeaderItemClickListener mClickListener;
    private int mMediaPosition;

    public WallStreetIntentData(AbsBaseActivity context,
                                OnTimelineHeaderItemClickListener clickListener) {
        mContext = context;
        mClickListener = clickListener;
    }

    public void setMediaPosition(int mediaPosition) {
        mMediaPosition = mediaPosition;
    }

    @Override
    public int getMediaClickPosition() {
        return mMediaPosition;
    }

    @Override
    public AbsBaseActivity getActivity() {
        return mContext;
    }

    @Override
    public void onDataResultCallBack(Adaptive adaptive, boolean isRemove) {
        Timber.i("onDataResultCallBack: " + adaptive.getClass().getSimpleName() + ", isRemove: " + isRemove);
        final boolean isTimelineAdapter = mClickListener.getRecyclerView().getAdapter() instanceof TimelineAdapter;
        if (!isTimelineAdapter) {
            return;
        }
        TimelineAdapter adapter = (TimelineAdapter) mClickListener.getRecyclerView().getAdapter();
        if (adaptive != null) {
            if (isRemove) {
                adapter.remove(adaptive);
            } else {
                adapter.update(adaptive);
            }
        }

        if (!isFloatingVideoPlaying()) {
            mClickListener.getRecyclerView().resumeAfterRecycle();
        }

    }

    private boolean isFloatingVideoPlaying() {
        return getActivity().getFloatingVideo() != null && getActivity().getFloatingVideo().isFloating();
    }
}
