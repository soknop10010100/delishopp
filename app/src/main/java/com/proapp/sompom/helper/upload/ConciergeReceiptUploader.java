package com.proapp.sompom.helper.upload;

import android.content.Context;
import android.graphics.Bitmap;

import com.desmond.squarecamera.utils.media.ImageUtil;
import com.proapp.sompom.R;
import com.proapp.sompom.utils.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;

import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by Veasna Chhom on 4/1/22.
 */
public class ConciergeReceiptUploader extends AbsUploader {

    private static final int MAX_WIDTH = 720;
    private static final int MAX_HEIGHT = 1280;

    private final Callback mCallback;

    public ConciergeReceiptUploader(Context context,
                                    String imagePath,
                                    UploadType uploadType,
                                    Callback callback) {
        super(context, imagePath, uploadType);
        mCallback = callback;
    }

    @Override
    public String getUploadFolder() {
        return super.getUploadFolder() + "shop/" + mContext.getString(R.string.upload_receipt_key);
    }

    public static Observable<UploadResult> processUpload(Context context, String path) {
        Observable<UploadResult> ob = Observable.create(e -> {
            byte[] bytes = getUploadDataForProfilePhoto(context, path);
            ConciergeReceiptUploader uploader = new ConciergeReceiptUploader(context,
                    path,
                    UploadType.Receipt,
                    new Callback() {
                        @Override
                        public void onUploaded(UploadResult result) {
                            e.onNext(result);
                            e.onComplete();
                        }

                        @Override
                        public void onUploadFail(Exception ex) {
                            e.onError(ex);
                            e.onComplete();
                        }
                    });
            uploader.doUpload(bytes);
        });
        ob = ob.onErrorResumeNext(throwable -> {
            if (throwable instanceof FileNotFoundException) {
                return Observable.just(new UploadResult());
            }
            return Observable.error(throwable);
        });
        return ob;
    }

    private static byte[] getUploadDataForProfilePhoto(Context context, String uri) {
        File file = new File(uri);
        if (file.exists()) {
            Bitmap bmp = ImageUtil.getBitmapFromFilePath(uri);
            byte[] bytesArray = null;
            if (bmp != null) {
                if (bmp.getWidth() > MAX_WIDTH || bmp.getHeight() > MAX_HEIGHT) {
                    Timber.i("Will resize photo resolution to max width " + MAX_WIDTH + " and max height " + MAX_HEIGHT);
                    Bitmap resizeBitmap = ImageUtil.resizeBitmap(bmp, MAX_WIDTH, MAX_HEIGHT);
                    if (resizeBitmap != null) {
                        bytesArray = convertByteArrayFromBitmap(resizeBitmap);
                    }
                } else {
                    bytesArray = convertByteArrayFromBitmap(bmp);
                }
            }

            if (bytesArray == null || bytesArray.length <= 0) {
                return FileUtils.convertFileToByArray(context, uri);
            } else {
                return bytesArray;
            }
        }

        return FileUtils.convertFileToByArray(context, uri);
    }

    private static byte[] convertByteArrayFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        bitmap.recycle();

        return byteArray;
    }

    @Override
    public void onUploadSucceeded(String imageUrl, String thumb) {
        if (mCallback != null) {
            mCallback.onUploaded(new UploadResult(imageUrl, thumb));
        }
    }

    @Override
    public void onUploadFail(String id, String ex) {
        if (mCallback != null) {
            mCallback.onUploadFail(new Exception(ex));
        }
    }

    public interface Callback {
        void onUploaded(UploadResult uploadResult);

        void onUploadFail(Exception ex);
    }

    public static class UploadResult {
        private String mUrl;
        private String mThumbnail;

        public UploadResult() {
        }

        public UploadResult(String url, String thumbnail) {
            mUrl = url;
            mThumbnail = thumbnail;
        }

        public String getUrl() {
            return mUrl;
        }


        public String getThumbnail() {
            return mThumbnail;
        }
    }
}
