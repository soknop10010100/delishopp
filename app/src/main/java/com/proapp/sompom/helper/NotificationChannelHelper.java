package com.proapp.sompom.helper;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.RequiresApi;

import java.util.HashMap;
import java.util.Map;

public class NotificationChannelHelper {

    private static final String CHANNEL_NAME_SEPARATOR = "_";

    //Based notification channel IDs
    public static final String CHANNEL_TYPE_FEED = "Feed";
    public static final String CHANNEL_TYPE_CHAT = "Chat";
    public static final String CHANNEL_TYPE_FEATURE_SYNCHRONIZATION = "Feature Synchronization";
    public static final String CHANNEL_TYPE_CALL_INFORMATION = "Call Information";
    public static final String CHANNEL_TYPE_INCOMING_CALL = "Incoming Call";
    public static final String CHANNEL_TYPE_SILENT = "Silent";
    public static final String CHANNEL_TYPE_VIBRATION = "Vibrate";
    public static final String CHANNEL_TYPE_ORDER_STATUS_UPDATE = "Order Status Update";

    /*
        Notification channel version. Any update regarding to channel, please increase this number
        version, so the update notification channel will be created accordingly.

        The full channel name will be created base on the {Based notification channel IDs} combine with
         channel version number.
        Ex: If channel version number is 2, so we have following channel:
           1. "Chat"
           2. "Chat_2"

        Note:
        1. We start naming the full notification channel by default version number 1.
        So if the version number is one, we will not have the name suffix, we will use the prefix name
        directly, for example: "Chat".
        2. Normally there will only one latest channel of notification created with different with suffix
        version number. And whenever the new channel of a specific group is created, all previous channels
        within the same group will deleted. By doing this we can always update notification channel sound
        without requiring to uninstall the app.
        3. The default version must be 1 and the default base notification channel id must be used.
     */
    private static final int CHANNEL_VERSION_TYPE_FEED = 1;
    private static final int CHANNEL_VERSION_TYPE_CHAT = 2;
    private static final int CHANNEL_VERSION_TYPE_FEATURE_SYNCHRONIZATION = 1;
    private static final int CHANNEL_VERSION_TYPE_CALL_INFORMATION = 1;
    private static final int CHANNEL_VERSION_TYPE_INCOMING_CALL = 1;
    private static final int CHANNEL_VERSION_TYPE_SILENT = 1;
    private static final int CHANNEL_VERSION_TYPE_VIBRATION = 1;
    private static final int CHANNEL_VERSION_TYPE_ORDER_STATUS_UPDATE = 3;

    private static final Map<String, Integer> sChannelIdMap = new HashMap<>();

    static {
        //New channel type must be added here.
        sChannelIdMap.put(CHANNEL_TYPE_FEED, CHANNEL_VERSION_TYPE_FEED);
        sChannelIdMap.put(CHANNEL_TYPE_CHAT, CHANNEL_VERSION_TYPE_CHAT);
        sChannelIdMap.put(CHANNEL_TYPE_FEATURE_SYNCHRONIZATION, CHANNEL_VERSION_TYPE_FEATURE_SYNCHRONIZATION);
        sChannelIdMap.put(CHANNEL_TYPE_CALL_INFORMATION, CHANNEL_VERSION_TYPE_CALL_INFORMATION);
        sChannelIdMap.put(CHANNEL_TYPE_INCOMING_CALL, CHANNEL_VERSION_TYPE_INCOMING_CALL);
        sChannelIdMap.put(CHANNEL_TYPE_SILENT, CHANNEL_VERSION_TYPE_SILENT);
        sChannelIdMap.put(CHANNEL_TYPE_VIBRATION, CHANNEL_VERSION_TYPE_VIBRATION);
        sChannelIdMap.put(CHANNEL_TYPE_ORDER_STATUS_UPDATE, CHANNEL_VERSION_TYPE_ORDER_STATUS_UPDATE);
    }

    private static String createNotificationChannelNameWithVersion(String prefix, int version) {
        return prefix + CHANNEL_NAME_SEPARATOR + version;
    }

    public static String getLatestChannelId(String channelType) {
        for (Map.Entry<String, Integer> entry : sChannelIdMap.entrySet()) {
            if (TextUtils.equals(entry.getKey(), channelType)) {
                Integer version = entry.getValue();
                if (version == null || version <= 1) {
                    return entry.getKey();
                } else {
                    return createNotificationChannelNameWithVersion(entry.getKey(), version);
                }
            }
        }

        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void deleteAllPreviousChatChannelVersion(Context context, String channelType) {
        for (Map.Entry<String, Integer> entry : sChannelIdMap.entrySet()) {
            if (TextUtils.equals(entry.getKey(), channelType)) {
                Integer version = entry.getValue();
                if (version != null) {
                    NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
                    for (int i = 0; i < version - 1; i++) {
                        String channelId;
                        if ((i + 1) == 1) {
                            channelId = entry.getKey();
                        } else {
                            channelId = createNotificationChannelNameWithVersion(entry.getKey(), (i + 1));
                        }

                        notificationManager.deleteNotificationChannel(channelId);
//                        Timber.i("Delete notification channel of " + channelId);
                    }
                }

                break;
            }
        }
    }
}
