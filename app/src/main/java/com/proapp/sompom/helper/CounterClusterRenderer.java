package com.proapp.sompom.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.proapp.sompom.model.SupportingResource;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.GlideLoadUtil;
import com.proapp.sompom.widget.OverlapImageContainer;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by He Rotha on 10/30/17.
 */

public class CounterClusterRenderer extends DefaultClusterRenderer<User> {

    private static final int sMaxDownloadImage = OverlapImageContainer.MAX_DISPLAY_COUNT;

    private Context mContext;
    private MyIconGenerator mIconGenerator;

    public CounterClusterRenderer(Context context, GoogleMap map, ClusterManager<User> clusterManager) {
        super(context, map, clusterManager);
        mContext = context;
        mIconGenerator = new MyIconGenerator(context);
    }

    @Override
    protected void onClusterRendered(Cluster<User> cluster, Marker marker) {

        List<SupportingResource> bitmaps = new ArrayList<>();
        final int imageSizeToLoad = getDownloadImageSize(cluster);

        int i = 0;
        for (User user : cluster.getItems()) {
            if (TextUtils.isEmpty(user.getUserProfileThumbnail())) {
                bitmaps.add(new SupportingResource(mContext, user.getFirstName(), user.getLastName()));
                if (bitmaps.size() >= imageSizeToLoad) {
                    try {
                        Bitmap bitmap = mIconGenerator.makeIcons(bitmaps, cluster.getItems().size());
                        marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap));
                    } catch (Exception ignore) {
                        Timber.e(ignore.toString());
                    }
                }
            } else {
                if (!GlideLoadUtil.isValidContext(mContext)) {
                    break;
                }

                GlideApp.with(mContext)
                        .asBitmap()
                        .apply(new RequestOptions()
                                .centerCrop()
                                .dontAnimate()
                                .circleCrop()
                                .override(50, 50))
                        .load(user.getUserProfileThumbnail())
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                bitmaps.add(new SupportingResource(resource));
                                if (bitmaps.size() >= imageSizeToLoad) {
                                    try {
                                        Bitmap bitmap = mIconGenerator.makeIcons(bitmaps, cluster.getItems().size());
                                        marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap));
                                    } catch (Exception ignore) {
                                        Timber.e(ignore.toString());
                                    }
                                }
                            }
                        });
            }

            i++;
            if (i >= imageSizeToLoad) {
                break;
            }
        }
    }

    private int getDownloadImageSize(Cluster<User> cluster) {
        final int imageSizeToLoad;
        if (cluster.getItems().size() > sMaxDownloadImage) {
            imageSizeToLoad = sMaxDownloadImage;
        } else {
            imageSizeToLoad = cluster.getItems().size();
        }
        return imageSizeToLoad;
    }

    @Override
    protected void onClusterItemRendered(User clusterItem, Marker marker) {
        if (TextUtils.isEmpty(clusterItem.getUserProfileThumbnail())) {
            SupportingResource resource = new SupportingResource(mContext, clusterItem.getFirstName(), clusterItem.getLastName());
            marker.setIcon(BitmapDescriptorFactory.fromBitmap(mIconGenerator.makeIcon(resource)));
        } else {
            if (!GlideLoadUtil.isValidContext(mContext)) {
                return;
            }

            GlideApp.with(mContext)
                    .asBitmap()
                    .load(clusterItem.getUserProfileThumbnail())
                    .apply(new RequestOptions()
                            .centerCrop()
                            .dontAnimate()
                            .circleCrop()
                            .override(50, 50))
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            try {
                                SupportingResource image = new SupportingResource(resource);
                                marker.setIcon(BitmapDescriptorFactory.fromBitmap(mIconGenerator.makeIcon(image)));
                            } catch (Exception ignore) {
                                Timber.e(ignore.toString());
                            }
                        }
                    });
        }
    }

    @Override
    protected void onBeforeClusterItemRendered(User item,
                                               MarkerOptions markerOptions) {
        SupportingResource resource = new SupportingResource(mContext, item.getFirstName(), item.getLastName());
        Bitmap bitmap = mIconGenerator.makeIcon(resource);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<User> cluster, MarkerOptions markerOptions) {

        List<SupportingResource> bitmaps = new ArrayList<>();
        final int imageSizeToLoad = getDownloadImageSize(cluster);

        int i = 0;
        for (User user : cluster.getItems()) {
            bitmaps.add(new SupportingResource(mContext, user.getFirstName(), user.getLastName()));
            if (bitmaps.size() >= imageSizeToLoad) {
                try {
                    Bitmap bitmap = mIconGenerator.makeIcons(bitmaps, cluster.getItems().size());
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
                } catch (Exception ignore) {
                    Timber.e(ignore.toString());
                }
            }

            i++;
            if (i > imageSizeToLoad) {
                break;
            }
        }
    }


}
