package com.proapp.sompom.helper;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.proapp.sompom.model.notification.Notification;
import com.proapp.sompom.model.notification.NotificationAdaptive;
import com.proapp.sompom.model.notification.NotificationListAndRedirectionHelper;
import com.proapp.sompom.model.result.LoadMoreWrapper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 8/30/17.
 */

public class NotificationAdapterDeserializer implements JsonDeserializer<LoadMoreWrapper<NotificationAdaptive>> {
    @Override
    public LoadMoreWrapper<NotificationAdaptive> deserialize(JsonElement json, Type typeOfT,
                                                             JsonDeserializationContext context) throws JsonParseException {
        LoadMoreWrapper<NotificationAdaptive> loadMoreWrapper = new LoadMoreWrapper<>();
        JsonObject jsonObject = (JsonObject) json;

        //Deserialize Next
        JsonElement obj = jsonObject.get(LoadMoreWrapper.NEXT);
        if (obj != null && !obj.isJsonNull()) {
            loadMoreWrapper.setNextPage(obj.getAsString());
        }

        //Deserialize List
        obj = jsonObject.get(LoadMoreWrapper.LIST);
        List<NotificationAdaptive> list = new ArrayList<>();
        for (JsonElement jsonElement : obj.getAsJsonArray()) {
            if (!NotificationListAndRedirectionHelper.isValidNotificationData((JsonObject) jsonElement)) {
                //Ignore invalid notification data.
                continue;
            }
            NotificationAdaptive adaptive = context.deserialize(jsonElement, Notification.class);
            if (adaptive != null) {
                list.add(adaptive);
            }
        }
        loadMoreWrapper.setData(list);

        return loadMoreWrapper;
    }
}
