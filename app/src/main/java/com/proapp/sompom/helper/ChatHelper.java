package com.proapp.sompom.helper;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.proapp.sompom.R;
import com.proapp.sompom.chat.MessageState;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.model.BaseChatModel;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.request.JoinChannelStatusUpdateBody;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.LoadMoreWrapper;
import com.proapp.sompom.model.result.ReferencedChat;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.utils.ConversationUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 12/9/2019.
 */
public class ChatHelper {

    public static final String JOIN_GROUP_CALL_STATUS_UPDATE = "JOIN_GROUP_CALL_STATUS_UPDATE";
    private static final int CONSIDER_AS_LOW_NAME_COUNT = 8;
    private static final int CONSIDER_AS_MEDIUM_NAME_COUNT = 12;
    private static final int CONSIDER_AS_LONG_NAME_COUNT = 20;

    private static final double FIRST_SIZE_FACTOR = 1000L;

    private ChatHelper() {
    }

    public static boolean isModifyChatType(Chat chat) {
        return chat.isDeleted() || chat.getSendingType() == Chat.SendingType.UPDATE;
    }

    public static boolean isGroupConversation(Context context, String conversationId) {
        return ConversationDb.isGroupConversation(context, conversationId);
    }

    public static int getDesireGiftMediaWidthOfChatItemBaseOnReferenceChat(Context context, ReferencedChat referencedChat) {
        String senderName = referencedChat.getSender().getFullName();
        if (referencedChat.getType() == Chat.Type.IMAGE ||
                referencedChat.getType() == Chat.Type.VIDEO ||
                referencedChat.getType() == Chat.Type.TENOR_GIF) {
            if (senderName.length() > CONSIDER_AS_MEDIUM_NAME_COUNT) {
                return context.getResources().getDimensionPixelSize(R.dimen.chat_content_width);
            }
        } else if (senderName.length() >= CONSIDER_AS_LONG_NAME_COUNT) {
            return context.getResources().getDimensionPixelSize(R.dimen.chat_content_width);
        } else if (senderName.length() <= CONSIDER_AS_LOW_NAME_COUNT) {
            return -1;
        }

        return context.getResources().getDimensionPixelSize(R.dimen.chat_media_width);
    }

    public static int getDesireLinkPreviewWidthOfChatItemBaseOnReferenceChat(Context context, ReferencedChat referencedChat) {
        String senderName = referencedChat.getSender().getFullName();
        if (referencedChat.getType() == Chat.Type.IMAGE ||
                referencedChat.getType() == Chat.Type.VIDEO ||
                referencedChat.getType() == Chat.Type.TENOR_GIF) {
            if (senderName.length() > CONSIDER_AS_MEDIUM_NAME_COUNT) {
                return context.getResources().getDimensionPixelSize(R.dimen.chat_content_width);
            }
        }

        return -1;
    }


    public static void injectSomeNecessaryStatusToChatFromServer(Context context,
                                                                 String conversationId,
                                                                 List<Chat> chatList) {
        if (!TextUtils.isEmpty(conversationId) && chatList != null && !chatList.isEmpty()) {
            boolean isGroup = isGroupConversation(context, conversationId);
            for (Chat chat : chatList) {
                /*
                1. Check to update isGroup status base on conversation since the chat model from
                 server does not contain this info.
                 */
                chat.setGroup(isGroup);

                /*
                2. Message status from server must be sent or seen. Here we will add some protection
                to make sure the message will have these types before using in local.
                 */
                if (!isMessageSent(chat)) {
                    chat.setStatus(MessageState.SENT);
                }
            }
        }
    }

    public static boolean isMessageSent(Chat chat) {
        //Check if the message is sent on server.
        return chat.getStatus() == MessageState.SENT ||
                chat.getStatus() == MessageState.DELIVERED ||
                chat.getStatus() == MessageState.SEEN;
    }

    public static Observable<Response<List<Chat>>> concatLoadChatResultFromServer(Context context,
                                                                                  String conversationId,
                                                                                  Observable<Response<List<Chat>>> observable) {
        return observable.concatMap(listResponse -> {
            if (listResponse.body() != null && !listResponse.body().isEmpty()) {
                injectSomeNecessaryStatusToChatFromServer(context, conversationId, listResponse.body());
            }
            return Observable.just(listResponse);
        });
    }

    public static Observable<Response<LoadMoreWrapper<Chat>>> concatLoadMoreAbleChatResultFromServer(Context context,
                                                                                                     String conversationId,
                                                                                                     Observable<Response<LoadMoreWrapper<Chat>>> observable) {
        return observable.concatMap(listResponse -> {
            if (listResponse.body() != null &&
                    listResponse.body().getData() != null &&
                    !listResponse.body().getData().isEmpty()) {
                injectSomeNecessaryStatusToChatFromServer(context, conversationId, listResponse.body().getData());
            }
            return Observable.just(listResponse);
        });
    }

    public static void broadcastGroupJoinStatusOnGroupCallEngaged(String tag,
                                                                  Context context,
                                                                  String groupId,
                                                                  boolean isVideo) {
//        Timber.i("broadcastGroupJoinStatusOnGroupCallEngaged: " + tag);
        JoinChannelStatusUpdateBody data = new JoinChannelStatusUpdateBody();
        data.setChannelOpen(true);
        data.setConversationId(groupId);
        data.setVideo(isVideo);
        broadcastGroupJoinStatusEven(context, data);
    }

    public static void broadcastGroupJoinStatusEven(Context context, JoinChannelStatusUpdateBody data) {
        Intent dataIntent = new Intent(JOIN_GROUP_CALL_STATUS_UPDATE);
        dataIntent.putExtra(SharedPrefUtils.DATA, data);
        SendBroadCastHelper.verifyAndSendBroadCast(context, dataIntent);
    }

    public static Chat buildOneToOneCallEventMessage(Context context,
                                                     AbsCallService.CallType callType,
                                                     User recipient,
                                                     boolean isMissedCall,
                                                     int calledDuration) {
        Timber.i("callType: " + callType + ", isMissedCall: " + isMissedCall);
        Chat chat;
        if (callType == AbsCallService.CallType.VOICE) {
            chat = Chat.getNewCallEventChat(context,
                    ConversationUtil.getConversationId(recipient.getId(),
                            SharedPrefUtils.getUserId(context)),
                    recipient,
                    isMissedCall ? Chat.ChatType.MISSED_CALL : Chat.ChatType.CALLED,
                    false);
        } else {
            //Video
            chat = Chat.getNewCallEventChat(context,
                    ConversationUtil.getConversationId(recipient.getId(),
                            SharedPrefUtils.getUserId(context)),
                    recipient,
                    isMissedCall ? Chat.ChatType.MISSED_VIDEO_CALL : Chat.ChatType.VIDEO_CALLED,
                    false);
        }

        if (calledDuration > 0) {
            //Call duration is only for called
            chat.setCallDuration(calledDuration);
        }

        return chat;
    }

    public static Chat buildStartOrEndGroupCallEventMessages(Context context,
                                                             AbsCallService.CallType callType,
                                                             User sender,
                                                             UserGroup userGroup,
                                                             boolean isStartCall) {
        Chat.ChatType chatType;
        if (isStartCall) {
            chatType = (callType == AbsCallService.CallType.VIDEO || callType == AbsCallService.CallType.GROUP_VIDEO) ? Chat.ChatType.START_GROUP_VIDEO_CALL
                    : Chat.ChatType.START_GROUP_CALL;
        } else {
            chatType = (callType == AbsCallService.CallType.VIDEO || callType == AbsCallService.CallType.GROUP_VIDEO) ? Chat.ChatType.END_GROUP_VIDEO_CALL
                    : Chat.ChatType.END_GROUP_CALL;
        }
        Chat baseCallEventChat = Chat.getNewCallEventChat(context,
                userGroup.getId(),
                userGroup.getParticipants().get(0),
                chatType,
                true);
        if (sender != null) {
            baseCallEventChat.setSender(sender);
            baseCallEventChat.setSenderId(sender.getId());
        }
        baseCallEventChat.setSendTo(userGroup.getId());

        return baseCallEventChat;
    }

    public static String getDisplaySizeWithFormat(long sizeBytes) {
        double kiloBytes = sizeBytes / FIRST_SIZE_FACTOR;
        if (((int) kiloBytes) <= 0) {
            return sizeBytes + " B";
        } else {
            double megaBytes = sizeBytes / Math.pow(FIRST_SIZE_FACTOR, 2);
            if (((int) megaBytes) <= 0) {
                return formatSizeValue(kiloBytes) + " KB";
            } else {
                return formatSizeValue(megaBytes) + " MB";
            }
        }
    }

    /*
      Will show only one floating point number if it exists.
     */
    private static String formatSizeValue(double d) {
        if (d == (long) d) {
            return String.format("%d", (long) d);
        } else {
            return getFileSizeFormatter().format(d);
        }
    }

    private static DecimalFormat getFileSizeFormatter() {
        DecimalFormat decimalFormat = new DecimalFormat("##.##");
        decimalFormat.setRoundingMode(RoundingMode.FLOOR);
        return decimalFormat;
    }

    public static List<String> queryAllGroupConversationId(Context context) {
        List<Conversation> conversationList = ConversationDb.getIndividualOrGroupConversation(context,
                true);
        List<String> groupIdList = new ArrayList<>();
        if (conversationList != null) {
            for (Conversation conversation : conversationList) {
                groupIdList.add(conversation.getId());
            }
        }

        return groupIdList;
    }

    public static void logMessages(String tag, List<Chat> chatList) {
//        if (chatList != null) {
//            Timber.i("Start log message of " + tag + ", size: " + chatList.size());
//            for (Chat chat : chatList) {
//                Timber.i("addLatestChat: getChatType: " + chat.getChatType() +
//                        ", status: " + chat.getStatus() +
//                        ", id: " + chat.getId() +
//                        ", getSendingType: " + chat.getSendingType() +
//                        ", content: " + chat.getContent() +
//                        ", date: " + getChatDateUTC(chat.getDate()) +
//                        ", isDeleted: " + chat.isDeleted());
//            }
//            Timber.i("End log message of " + tag);
//        }
    }

    public static String getChatDateUTC(Date date) {
//        if (date != null) {
//            return DateUtils.getUTCDateTimeFormat(date);
//        }
//
//        return null;

        return "";
    }

    public static List<Chat> getChatList(List<BaseChatModel> chatList) {
        List<Chat> chats = new ArrayList<>();
        for (BaseChatModel baseChatModel : chatList) {
            if (baseChatModel instanceof Chat) {
                chats.add((Chat) baseChatModel);
            }
        }

        return chats;
    }

    public static boolean isIgnoreDisplaySeenAvatarMessage(Chat seenMessage) {
        return isCallEventTypes(seenMessage) || ConversationHelper.isGroupModificationMessageType(seenMessage);
    }

    public static boolean isCallEventTypes(Chat chat) {
        Chat.ChatType type = Chat.ChatType.from(chat.getChatType());
        return type == Chat.ChatType.MISSED_CALL ||
                type == Chat.ChatType.CALLED ||
                type == Chat.ChatType.MISSED_VIDEO_CALL ||
                type == Chat.ChatType.VIDEO_CALLED ||
                type == Chat.ChatType.START_GROUP_CALL ||
                type == Chat.ChatType.START_GROUP_VIDEO_CALL ||
                type == Chat.ChatType.END_GROUP_CALL ||
                type == Chat.ChatType.END_GROUP_VIDEO_CALL;
    }
}
