package com.proapp.sompom.helper;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.proapp.sompom.R;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import timber.log.Timber;

/**
 * Created by He Rotha on 9/29/17.
 */

public class HeaderInterceptor implements Interceptor {

    public static final String HTTP_UNAUTHORIZED_EVEN = "HTTP_UNAUTHORIZED_EVEN";
    private static final int MAKE_ORDER_REQUEST_TIME_OUT = 5;

    private static final String AUTHENTICATION = "Authorization";
    private static final String BEARER = "Bearer ";
    private final Context mContext;

    public HeaderInterceptor(Context context) {
        this.mContext = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain
                .request()
                .newBuilder()
                .addHeader("Device", mContext.getString(R.string.device))
                .addHeader("Accept-Language", SharedPrefUtils.getLanguage(mContext));

        String token = SharedPrefUtils.getAccessToken(mContext);
//        Timber.i("Ur: " + chain.request().url() + "\nToken: " + token);
        if (!TextUtils.isEmpty(token)) {
            builder.addHeader(AUTHENTICATION, BEARER + token);
        }

        Request request = builder.build();
        Response response;
        if (isMakeOrderAPIRequest(chain)) {
//            Timber.i("Wil set time out of make order request to " + MAKE_ORDER_REQUEST_TIME_OUT + " minutes.");
            response = chain.withConnectTimeout(MAKE_ORDER_REQUEST_TIME_OUT, TimeUnit.MINUTES)
                    .withReadTimeout(MAKE_ORDER_REQUEST_TIME_OUT, TimeUnit.MINUTES)
                    .proceed(request);
        } else {
            response = chain.proceed(request);
        }

//        Timber.i("Request url: " + request.url() + ", response code: " + response.code());

        if (response.code() == HttpURLConnection.HTTP_UNAUTHORIZED &&
                !ApplicationHelper.isInVisitorMode(mContext)) {
            //Will broadcast authorization even only if user is already logged in.
            SendBroadCastHelper.verifyAndSendBroadCast(mContext, new Intent(HTTP_UNAUTHORIZED_EVEN));
            return null;
        } else {
            return response;
        }
    }

    private boolean isMakeOrderAPIRequest(Chain chain) {
        if (chain != null) {
            String url = chain.request().url().toString();
            return (url.contains("v1/order/payment/aba") ||
                    url.contains("v1/order/menuItem")) &&
                    chain.request().method().equals("POST");
        }

        return false;
    }
}
