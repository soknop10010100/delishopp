package com.proapp.sompom.helper;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.proapp.sompom.BuildConfig;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.intent.InputEmailLoginPasswordIntent;
import com.proapp.sompom.intent.LoginWithPhoneOnlyIntent;
import com.proapp.sompom.intent.newintent.CheckEmailIntent;
import com.proapp.sompom.intent.newintent.HomeIntent;
import com.proapp.sompom.intent.newintent.LoginIntent;
import com.proapp.sompom.intent.newintent.LoginWithPhoneAndPasswordIntent;
import com.proapp.sompom.intent.newintent.LoginWithPhoneOrEmailAndPasswordIntent;
import com.proapp.sompom.intent.newintent.LoginWithPhoneOrEmailIntent;
import com.proapp.sompom.licence.utils.FileUtils;
import com.proapp.sompom.model.concierge.ConciergeShopSetting;
import com.proapp.sompom.model.emun.ApplicationMode;
import com.proapp.sompom.model.emun.AuthConfigurationType;
import com.proapp.sompom.model.result.AppFeature;
import com.proapp.sompom.newui.InputEmailLoginPasswordActivity;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.List;

import timber.log.Timber;

public class ApplicationHelper {

    private static final String BUILD_TYPE_DEBUG = "debug";
    private static final String BUILD_TYPE_RELEASE = "release";

    private static final String SIGN_UP_METHOD = "SIGN_UP_METHOD";
    public static final String REFRESH_SCREEN_EVENT = "REFRESH_SCREEN_EVENT";
    public static final String IS_FROM_AUTHENTICATION_SUCCESS = "IS_FROM_AUTHENTICATION_SUCCESS";

    public static boolean isReleaseBuildType() {
        return TextUtils.equals(BuildConfig.BUILD_TYPE, BUILD_TYPE_RELEASE);
    }

    public static AuthConfigurationType getAuthConfigurationType(Context context) {
        return AuthConfigurationType.fromValue(context.getString(R.string.register_type));
    }

    public static void setSignUpMethodToLocal(Context context, AuthConfigurationType authConfigurationType) {
        SharedPrefUtils.setString(context, SIGN_UP_METHOD, authConfigurationType.getValue());
    }

    public static Intent getLoginIntent(Context context, boolean shouldGoToHomeScreenAfterLoginSuccess) {
        // Set in shared pref if login should go to home screen after success
        SharedPrefUtils.setShouldOpenHomeScreenAfterLogin(context, shouldGoToHomeScreenAfterLoginSuccess);
        AuthConfigurationType authConfigurationType = ApplicationHelper.getAuthConfigurationType(context);
        if (authConfigurationType == AuthConfigurationType.PHONE) {
            return new LoginWithPhoneOnlyIntent(context);
        } else if (authConfigurationType == AuthConfigurationType.PHONE_AND_EMAIL) {
            return new LoginWithPhoneOrEmailIntent(context);
        } else if (authConfigurationType == AuthConfigurationType.PHONE_AND_PASSWORD) {
            return new LoginWithPhoneAndPasswordIntent(context);
        } else if (authConfigurationType == AuthConfigurationType.PHONE_AND_EMAIL_AND_PASSWORD) {
            return new LoginWithPhoneOrEmailAndPasswordIntent(context);
        } else if (authConfigurationType == AuthConfigurationType.EMAIL_AND_PHONE) {
            return new CheckEmailIntent(context);
        } else {
            return new LoginIntent(context);
        }
    }

    public static Intent getSupportResetPasswordDeepLinkingIntent(MainApplication application) {
        AuthConfigurationType authConfigurationType = ApplicationHelper.getAuthConfigurationType(application);
        if (authConfigurationType == AuthConfigurationType.AGENCY ||
                authConfigurationType == AuthConfigurationType.EMAIL) {
            return new LoginIntent(application);
        } else if (authConfigurationType == AuthConfigurationType.PHONE_AND_EMAIL) {
            return new LoginWithPhoneOrEmailIntent(application);
        } else if (authConfigurationType == AuthConfigurationType.PHONE_AND_PASSWORD) {
            return new LoginWithPhoneAndPasswordIntent(application);
        } else if (authConfigurationType == AuthConfigurationType.PHONE_AND_EMAIL_AND_PASSWORD) {
            return new LoginWithPhoneOrEmailAndPasswordIntent(application);
        } else if (authConfigurationType == AuthConfigurationType.EMAIL_AND_PHONE) {
            if (application.isActivityRunning(InputEmailLoginPasswordActivity.class.getSimpleName())) {
                return new InputEmailLoginPasswordIntent(application, null);
            } else {
                return new CheckEmailIntent(application);
            }
        }

        return null;
    }

    public static boolean isEmailLoginOnly(Context context) {
        AuthConfigurationType authConfigurationType = ApplicationHelper.getAuthConfigurationType(context);
        return authConfigurationType == AuthConfigurationType.EMAIL ||
                authConfigurationType == AuthConfigurationType.AGENCY ||
                authConfigurationType == AuthConfigurationType.EMAIL_AND_PHONE;
    }

    public static boolean isPhoneLoginOnly(Context context) {
        AuthConfigurationType authConfigurationType = ApplicationHelper.getAuthConfigurationType(context);
        return authConfigurationType == AuthConfigurationType.PHONE ||
                authConfigurationType == AuthConfigurationType.PHONE_AND_PASSWORD;
    }

    public static HomeIntent getFreshHomeIntent(Context context) {
        HomeIntent homeIntent = new HomeIntent(context);
        //will clear all previous activity
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        return homeIntent;
    }

    public static Intent getFreshLoginIntent(Context context) {
        Intent intent = ApplicationHelper.getLoginIntent(context, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        return intent;
    }

    public static boolean isAllowToUseWithoutLogin(Context context) {
        return context.getResources().getBoolean(R.bool.allow_public);
    }

    public static boolean isInVisitorMode(Context context) {
        if (SharedPrefUtils.isLogin(context) && !UserHelper.isGuestUserExist(context)) {
            //User has logged in with their account
            return false;
        } else {
            //In guest/visitor mode
            return isAllowToUseWithoutLogin(context);
        }
    }

    public static boolean shouldRequestVisitorUserData(Context context) {
        return context.getResources().getBoolean(R.bool.has_visitor);
    }

    public static List<AppFeature.NavigationBarMenu> getTestNavBar(Context context) {
        String data = FileUtils.readRawTextFile(context, R.raw.test_nav_menu);
        return new Gson().fromJson(data, new TypeToken<List<AppFeature.NavigationBarMenu>>() {
        }.getType());
    }

    public static void broadcastRefreshScreenEvent(Context context, boolean isFromSuccessfulAuthentication) {
        Intent intent = new Intent(REFRESH_SCREEN_EVENT);
        intent.putExtra(IS_FROM_AUTHENTICATION_SUCCESS, isFromSuccessfulAuthentication);
        SendBroadCastHelper.verifyAndSendBroadCast(context, intent);
    }

    public static AppFeature getAppFeature(Context context) {
        return new Gson().fromJson(SharedPrefUtils.getString(context, SharedPrefUtils.APP_FEATURE), AppFeature.class);
    }

    public static void setInExpressMode(Context context, boolean isInExpressMode) {
        SharedPrefUtils.setBoolean(context, SharedPrefUtils.IS_IN_EXPRESS_MODE, isInExpressMode);
        MainApplication.setInExpressMode(isInExpressMode);
        ThemeManager.setAppTheme(context, isInExpressMode ? AppTheme.ExpressWhite : AppTheme.White);
    }

    public static int getRequestAPIMode(Context context) {
        boolean isExpressMode = isInExpressMode(context);
        return isExpressMode ? ApplicationMode.EXPRESS_MODE.getType() : ApplicationMode.NORMAL_MODE.getType();
    }

    public static boolean isInExpressMode(Context context) {
        boolean inExpressMode = SharedPrefUtils.getBoolean(context, SharedPrefUtils.IS_IN_EXPRESS_MODE);
        if (inExpressMode) {
            /*
                Use case:
                Need to reset in express mode to false if the express mode is not enable in shop setting.
                In case can happen if we manage to save switch app mode option in local and after that
                user open app again with express mode, but the shop setting has disable the express mode,
                so we will just reset it to normal mode.
             */
            ConciergeShopSetting conciergeShopSetting = ConciergeHelper.getConciergeShopSetting(context);
            if (conciergeShopSetting != null) {
                if (!conciergeShopSetting.isEnableExpress()) {
                    setInExpressMode(context, false);
                    inExpressMode = false;
                }
            }

            return inExpressMode;
        } else {
            return false;
        }
    }

    public static void injectUpdateHomeExpressIcon(AppFeature appFeature) {
        if (appFeature != null &&
                appFeature.getNavigationBarMenu() != null &&
                !appFeature.getNavigationBarMenu().isEmpty()) {
            for (AppFeature.NavigationBarMenu navigationBarMenu : appFeature.getNavigationBarMenu()) {
                if (navigationBarMenu.getNavMenuButton() == AppFeature.NavMenuButton.NormalMode) {
                    //In express mode now. Will find home shop nav button to update icon.
                    for (AppFeature.NavigationBarMenu navigationBarMenu2 : appFeature.getNavigationBarMenu()) {
                        if (navigationBarMenu2.getNavMenuButton() == AppFeature.NavMenuButton.SingleShop) {
                            navigationBarMenu2.setNavButton(AppFeature.NavMenuButton.ExpressShop.getKey());
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }

    public static void checkToResetToNormalModeIfNecessary(Context context, ConciergeShopSetting shopSetting) {
        if (shopSetting != null) {
            boolean inExpressMode = SharedPrefUtils.getBoolean(context, SharedPrefUtils.IS_IN_EXPRESS_MODE);
            if (inExpressMode && !shopSetting.isEnableExpress()) {
                Timber.i("Reset to normal mode when set app feature.");
                setInExpressMode(context, false);
            }
        }
    }

    public static void resetApplicationMode(Context context) {
        ApplicationHelper.setInExpressMode(context, false);
        SharedPrefUtils.clearAppFeature(context);
    }

    public static String getUserId(Context context) {
        return SharedPrefUtils.getUserId(context);
    }
}
