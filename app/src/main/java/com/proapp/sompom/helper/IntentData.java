package com.proapp.sompom.helper;

import androidx.annotation.Nullable;

import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;

/**
 * Created by He Rotha on 1/31/19.
 */
public interface IntentData {
    AbsBaseActivity getActivity();

    default int getMediaClickPosition() {
        return -1;
    }

    @Nullable
    default User getUser() {
        return null;
    }

    @Nullable
    default Action getAction() {
        return Action.NOTHING;
    }

    default void onDataResultCallBack(Adaptive adaptive, boolean isRemove) {
        //do nothing
    }

    enum Action {
        NOTHING,

        CREATE_TIMELINE_ITEM,
        CREATE_TIMELINE_CAMERA,

        CLICK_HEADER
    }
}
