package com.proapp.sompom.helper;

import android.view.View;

import androidx.annotation.NonNull;

public interface DelayClickListener {

    public static final int DELAY_DURATION = 500;

    void onDelayClick(@NonNull View view);
}
