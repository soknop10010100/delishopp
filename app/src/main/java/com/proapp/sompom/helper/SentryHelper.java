package com.proapp.sompom.helper;

import android.content.Context;

import com.proapp.sompom.model.result.User;

public class SentryHelper {

    public static void setUser(User user) {
        com.sompom.proapp.core.helper.SentryHelper.setUser(user.getId(), user.getEmail(), user.getFullName());
    }

    public static void logSentryError(Exception exception) {
        com.sompom.proapp.core.helper.SentryHelper.logSentryError(exception);
    }
}
