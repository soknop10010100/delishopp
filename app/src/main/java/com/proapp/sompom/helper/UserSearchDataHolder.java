package com.proapp.sompom.helper;

import android.content.Context;
import android.text.TextUtils;

import com.proapp.sompom.model.result.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 5/7/2020.
 */
public class UserSearchDataHolder {

    private static UserSearchDataHolder sUserSearchDataHolder;

    private List<User> mUserList = new ArrayList<>();

    private UserSearchDataHolder() {
    }

    public static UserSearchDataHolder getInstance() {
        if (sUserSearchDataHolder == null) {
            sUserSearchDataHolder = new UserSearchDataHolder();
        }

        return sUserSearchDataHolder;
    }

    /*
      Load user contact and store temporary in memory.
     */
    public void loadUserContact(Context context) {
        clearUserContact();
        mUserList = UserHelper.getUserContact(context);
    }

    public void clearUserContact() {
        if (mUserList != null) {
            mUserList.clear();
            mUserList = null;
        }
    }

    public void filerUserInList(String query, UserSearchDataHolderListener listener) {
        Observable.create((ObservableOnSubscribe<List<User>>) emitter -> {
            List<User> result = new ArrayList<>();
            if (!TextUtils.isEmpty(query) && mUserList != null) {
                String newQuery = query.toLowerCase();
                for (User user : mUserList) {
                    if (isMatchWith(user.getFullName(), newQuery)) {
                        result.add(user);
                    }
                }
            }
            sortUserResult(result);
            emitter.onNext(result);
            emitter.onComplete();
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<User>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<User> userList) {
                        if (listener != null) {
                            listener.onSearchSuccess(userList);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public static void sortUserResult(List<User> result) {
        //Sort via ASC
        if (!result.isEmpty()) {
            Collections.sort(result, (o1, o2) -> o1.getFullName().compareTo(o2.getFullName()));
        }
    }

    private boolean isMatchWith(String data, String token) {
        return !TextUtils.isEmpty(data) && data.toLowerCase().contains(token);
    }

    public interface UserSearchDataHolderListener {
        void onSearchSuccess(List<User> result);
    }
}
