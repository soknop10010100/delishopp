package com.proapp.sompom.helper.upload;

/**
 * Created by He Rotha on 6/20/18.
 */
public enum Orientation {
    Portrait, Landscape
}
