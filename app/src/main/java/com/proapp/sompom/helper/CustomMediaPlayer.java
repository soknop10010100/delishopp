package com.proapp.sompom.helper;

import android.media.MediaPlayer;

import java.io.IOException;

/**
 * Created by Or Vitovongsak on 4/2/22.
 */

public class CustomMediaPlayer extends MediaPlayer {

    private boolean mIsPlayerPrepared;
    private boolean mIsPlayerPlaying;

    public CustomMediaPlayer() {
    }

    public boolean isPlayerPrepared() {
        return mIsPlayerPrepared;
    }

    public boolean isPlayerPlaying() {
        return mIsPlayerPlaying;
    }

    @Override
    public void setOnPreparedListener(OnPreparedListener listener) {
        super.setOnPreparedListener(mp -> {
            mIsPlayerPrepared = true;
            if (listener != null) {
                listener.onPrepared(mp);
            }
        });
    }

    @Override
    public void stop() throws IllegalStateException {
        super.stop();
        mIsPlayerPlaying = false;
    }

    @Override
    public void prepare() throws IOException, IllegalStateException {
        super.prepare();
        mIsPlayerPrepared = true;
    }

    @Override
    public void start() throws IllegalStateException {
        super.start();
        mIsPlayerPlaying = true;
    }

    @Override
    public void pause() throws IllegalStateException {
        super.pause();
        mIsPlayerPlaying = false;
    }

    @Override
    public void release() {
        super.release();
        // Reset all status once player is released
        mIsPlayerPrepared = false;
        mIsPlayerPlaying = false;
    }

    @Override
    public void setOnCompletionListener(OnCompletionListener listener) {
        super.setOnCompletionListener(mp -> {
            // Update playing status once the player completes playing
            mIsPlayerPlaying = false;
            if (listener != null) {
                listener.onCompletion(mp);
            }
        });
    }

    @Override
    public boolean isPlaying() throws IllegalStateException {
        // Should we ignore super.isPlaying() ? We can sort of confidently handle the playing status
        // here, but it's nice to know an error is thrown just to be sure
        return mIsPlayerPrepared && mIsPlayerPlaying;
//        return mIsPlayerPlaying || super.isPlaying();
    }
}
