package com.proapp.sompom.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView;

import androidx.browser.customtabs.CustomTabsIntent;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.adapter.newadapter.TimelineAdapter;
import com.proapp.sompom.adapter.newadapter.TimelineDetailAdapter;
import com.proapp.sompom.database.MediaDb;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.CommonWallAdapter;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.result.AppSetting;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.model.result.Theme;
import com.proapp.sompom.model.result.UnreadCommentResponse;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.DownloaderUtils;
import com.proapp.sompom.utils.GenerateLinkPreviewUtil;
import com.proapp.sompom.utils.GlideLoadUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.proapp.sompom.viewmodel.newviewmodel.AbsItemFileViewModel;

import java.io.File;
import java.util.Date;

import io.reactivex.Observable;
import io.reactivex.Observer;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 10/9/2019.
 */
public final class WallStreetHelper {

    public static final String WELCOME_POST_ID = "539a3ece-3e63-454f-b5c5-d5e17dff4027";

    public static boolean isContainLink(String content) {
        return !TextUtils.isEmpty(content) && !TextUtils.isEmpty(GenerateLinkPreviewUtil.getPreviewLink(content));
    }

    public static LifeStream createWelcomePost(Context context) {
        AppSetting appSetting = SharedPrefUtils.getAppSetting(context);

        LifeStream lifeStream = null;
        if (appSetting != null && !TextUtils.isEmpty(appSetting.getWallPlaceholderMessage())) {
            lifeStream = new LifeStream();
            lifeStream.setWelcomePost(true);
            lifeStream.setId(WELCOME_POST_ID);
            lifeStream.setTitle(appSetting.getWallPlaceholderMessage());
            lifeStream.setCreateDate(new Date());
            User user = new User();
            user.setFirstName(appSetting.getOrganizationName());
            Theme theme = UserHelper.getSelectedThemeFromAppSetting(context);
            if (theme != null && !TextUtils.isEmpty(theme.getCompanyAvatar())) {
                user.setUserProfileThumbnail(theme.getCompanyAvatar());
            }
            lifeStream.setStoreUser(user);
        }

        return lifeStream;
    }

    public static boolean isPostContainOnlyText(Adaptive adaptive) {
        return !TextUtils.isEmpty(adaptive.getTitle()) &&
                isEmptyMedia(adaptive) &&
                !shouldRenderLinkPreview(adaptive) &&
                !shouldRenderPlacePreview(adaptive);
    }

    public static void openCustomTab(Context context, String url) {
        if (!TextUtils.isEmpty(url)) {
            if (!url.startsWith("http")) {
                url = "http://" + url;
            }
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(context, Uri.parse(url));
        }
    }

    public static boolean shouldRenderLinkPreview(Adaptive adaptive) {
        return adaptive instanceof LifeStream &&
                (adaptive.getMedia() == null || adaptive.getMedia().isEmpty()) &&
                ((LifeStream) adaptive).getLinkPreviewModel() != null &&
                adaptive.shouldShowLinkPreview();
    }

    public static boolean shouldRenderLinkPreview(WallStreetAdaptive adaptive) {
        return adaptive instanceof LifeStream &&
                (adaptive.getMedia() == null || adaptive.getMedia().isEmpty()) &&
                (((LifeStream) adaptive).getMedia() == null || ((LifeStream) adaptive).getMedia().isEmpty()) &&
                (WallStreetHelper.isContainLink(((LifeStream) adaptive).getTitle()) ||
                        !TextUtils.isEmpty(((LifeStream) adaptive).getShareUrl())) &&
                adaptive.shouldShowLinkPreview();
    }

    public static boolean isThereRenderLinkPreviewData(WallStreetAdaptive adaptive) {
        return adaptive instanceof LifeStream &&
                (adaptive.getMedia() == null || adaptive.getMedia().isEmpty()) &&
                (((LifeStream) adaptive).getMedia() == null || ((LifeStream) adaptive).getMedia().isEmpty()) &&
                (WallStreetHelper.isContainLink(((LifeStream) adaptive).getTitle()) ||
                        !TextUtils.isEmpty(((LifeStream) adaptive).getShareUrl()));
    }

    public static boolean shouldRenderPlacePreview(Adaptive adaptive) {
        return adaptive instanceof LifeStream &&
                (adaptive.getMedia() == null || adaptive.getMedia().isEmpty()) &&
                !TextUtils.isEmpty(((LifeStream) adaptive).getAddress()) &&
                !TextUtils.isEmpty(((LifeStream) adaptive).getLocationName()) &&
                !TextUtils.isEmpty(((LifeStream) adaptive).getLocationCountry()) &&
                adaptive.shouldShowPlacePreview();
    }

    public static boolean shouldRenderCheckedInLabel(Adaptive adaptive) {
        return adaptive instanceof LifeStream &&
                !TextUtils.isEmpty(((LifeStream) adaptive).getAddress()) &&
                !TextUtils.isEmpty(((LifeStream) adaptive).getLocationName()) &&
                !TextUtils.isEmpty(((LifeStream) adaptive).getLocationCountry());
    }

    public static boolean shouldRenderPlacePreview(WallStreetAdaptive adaptive) {
        return adaptive instanceof LifeStream &&
                (adaptive.getMedia() == null || adaptive.getMedia().isEmpty()) &&
                !TextUtils.isEmpty(((LifeStream) adaptive).getAddress()) &&
                !TextUtils.isEmpty(((LifeStream) adaptive).getLocationName()) &&
                !TextUtils.isEmpty(((LifeStream) adaptive).getLocationCountry()) &&
                adaptive.shouldShowPlacePreview();
    }

    public static boolean isEmptyMedia(Adaptive adaptive) {
        return adaptive instanceof LifeStream &&
                (((LifeStream) adaptive).getMedia() == null || ((LifeStream) adaptive).getMedia().isEmpty());
    }

    public static void calculatePostHeight(Point mediaPoint, CalculateDisplayPhotoSizeListener listener) {
        /*
          mediaPoint contains media width and height.
         */
        int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        int screenHeight;
        if (mediaPoint != null) {
            if (mediaPoint.x == mediaPoint.y) {
                screenHeight = screenWidth;
            } else {
                screenHeight = mediaPoint.y * screenWidth / mediaPoint.x;
            }
        } else {
            screenHeight = screenWidth;
        }

        int calculatedHeight = screenHeight;

        ImageView.ScaleType scaleType = ImageView.ScaleType.FIT_CENTER;

        int maxHeight = (int) (screenWidth * 2);
        if (screenHeight > maxHeight) {
            screenHeight = maxHeight;
            scaleType = ImageView.ScaleType.CENTER_CROP;
        }

        Timber.i("screenWidth: " + screenWidth +
                ", maxHeight: " + maxHeight +
                ", screenHeight: " + screenHeight +
                ", calculatedHeight: " + calculatedHeight +
                ", mediaPoint: " + mediaPoint +
                ", scaleType: " + scaleType);

        if (listener != null) {
            listener.onCalculateFinished(screenWidth, screenHeight, scaleType);
        }
    }

    public static void testUpdateMediaSize(Media media) {
        if (media.getId().matches("5d9d9a0a6abd01048e00b4b8")) {
            Timber.i("Change photo size.");
            media.setUrl("https://www.mrcutout.com/images/cutouts/teenager-walking-0026-watermarked.jpg");
            media.setWidth(270);
            media.setHeight(799);
        }
    }

    public interface CalculateDisplayPhotoSizeListener {
        void onCalculateFinished(int desireWidth, int desiredHeight, ImageView.ScaleType scaleType);
    }

    public static void downloadFile(Context context,
                                    Media selectedMedia,
                                    AbsItemFileViewModel fileViewModel,
                                    FileDownloadHelper.FileDownloadListener listener) {
        if (context instanceof AbsBaseActivity) {
            ((AbsBaseActivity) context).checkPermission(new CheckPermissionCallbackHelper((Activity) context,
                    CheckPermissionCallbackHelper.Type.STORAGE) {
                @Override
                public void onPermissionGranted() {
                    Timber.i("Download media: " + new Gson().toJson(selectedMedia));
                    new FileDownloadHelper(selectedMedia.getUrl(),
                            null,
                            selectedMedia.getId(),
                            DownloaderUtils.getRootDirPath(context),
                            FileDownloadHelper.WALLSTREET_DIR,
                            selectedMedia.getSize(),
                            new FileDownloadHelper.FileDownloadListener() {

                                @Override
                                public void onDownloadGetStarted(String requestId, String downloadId) {
                                    fileViewModel.setFileStatus(Media.FileStatusType.DOWNLOADING);
                                    if (listener != null) {
                                        listener.onDownloadGetStarted(requestId, downloadId);
                                    }
                                }

                                @Override
                                public void onDownloadFailed(String requestId, String downloadId,
                                                             Throwable ex) {
                                    fileViewModel.setFileStatus(Media.FileStatusType.IDLE);
                                    if (listener != null) {
                                        listener.onDownloadFailed(requestId,
                                                downloadId,
                                                new Exception(context.getString(R.string.toast_download_file_failed)));
                                    }
                                }

                                @Override
                                public void onDownloadInProgress(String requestId, String downloadId,
                                                                 long downloadedBytes,
                                                                 long totalBytes) {
                                    Timber.i("onDownloadInProgress: downloadedBytes: "
                                            + downloadedBytes + ", totalBytes: " + totalBytes);
                                    fileViewModel.setDownloadProgression((int) ((downloadedBytes * 100) / totalBytes));
                                    if (listener != null) {
                                        listener.onDownloadInProgress(requestId, downloadId, downloadedBytes, totalBytes);
                                    }
                                }

                                @Override
                                public void onDownloadSuccess(String requestId, String downloadId, File file) {
                                    Timber.i("onDownloadSuccess: " + file.getAbsolutePath());
                                    selectedMedia.setFileStatusType(Media.FileStatusType.DOWNLOADED);
                                    selectedMedia.setDownloadedPath(file.getAbsolutePath());
                                    fileViewModel.setDownloadProgression(100);
                                    fileViewModel.setFileStatus(Media.FileStatusType.DOWNLOADED);
                                    MediaDb.saveDownloadedMedia(context, selectedMedia);
                                    if (listener != null) {
                                        listener.onDownloadSuccess(requestId, downloadId, file);
                                    }
                                }
                            }).performDownload();
                }
            }, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    public static Observable<Response<LifeStream>> getCreateOrUpdatePostObservable(Context context,
                                                                                   ApiService apiService,
                                                                                   LifeStream lifeStream,
                                                                                   String postId,
                                                                                   boolean isNewPost) {
        //Validate if the link preview and downloaded
        String previewLink = LinkPreviewRetriever.getPreviewLinkFromPost(lifeStream);
        Timber.i("previewLink: " + previewLink);
        if (!TextUtils.isEmpty(previewLink) && lifeStream.getMetaPreviewModelList().isEmpty()) {
            //Need to re-download preview link again.
            Timber.i("Need to re-download preview link again: " + new Gson().toJson(lifeStream));
            return LinkPreviewRetriever.getPreviewLink(context, apiService, previewLink).
                    onErrorResumeNext(new Observable<Response<LinkPreviewModel>>() {
                        @Override
                        protected void subscribeActual(Observer<? super Response<LinkPreviewModel>> observer) {
                            observer.onNext(Response.success(new LinkPreviewModel()));
                        }
                    }).concatMap(linkPreviewModelResponse -> {
                Timber.i("linkPreviewModelResponse: " + new Gson().toJson(linkPreviewModelResponse.body()));
                if (linkPreviewModelResponse.body() != null && linkPreviewModelResponse.body().isValidPreviewData()) {
                    //For re-download case only
                    linkPreviewModelResponse.body().setShouldPreview(true);
                    lifeStream.setRetrievedLinkPreviewResult(linkPreviewModelResponse.body());
                }

                if (!isNewPost) {
                    LifeStream body = LifeStream.getPostInstance(lifeStream, true);
                    return apiService.updateWallStreet(postId, body);
                } else {
                    return apiService.postWallStreet(LifeStream.getPostInstance(lifeStream, false));
                }
            });
        }

        if (!isNewPost) {
            LifeStream body = LifeStream.getPostInstance(lifeStream, true);
            return apiService.updateWallStreet(postId, body);
        } else {
            return apiService.postWallStreet(LifeStream.getPostInstance(lifeStream, false));
        }
    }

    public static void requestToUpdateUnreadCommentCounter(Context context,
                                                           ApiService apiService,
                                                           CommonWallAdapter adapter,
                                                           Adaptive item) {
        Observable<Response<UnreadCommentResponse>> checkingWorkingHour = apiService.getUnreadCommentCount(item.getId());
        ResponseObserverHelper<Response<UnreadCommentResponse>> helper = new ResponseObserverHelper<>(context,
                checkingWorkingHour);
        helper.execute(new OnCallbackListener<Response<UnreadCommentResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
            }

            @Override
            public void onComplete(Response<UnreadCommentResponse> result) {
                if (result.body() != null && GlideLoadUtil.isValidContext(context)) {
                    item.setUnreadCommentCounter(result.body().getUnreadCommentCounter());
                    if (adapter instanceof TimelineAdapter) {
                        ((TimelineAdapter) adapter).update(item);
                    } else if (adapter instanceof TimelineDetailAdapter) {
                        ((TimelineDetailAdapter) adapter).updateUnreadCommentCounter(item);
                    }
                }
            }
        });
    }
}
