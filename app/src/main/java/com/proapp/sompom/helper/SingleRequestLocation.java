package com.proapp.sompom.helper;

import androidx.appcompat.app.AppCompatActivity;

import com.proapp.sompom.model.Locations;
import com.proapp.sompom.utils.GeocoderUtil;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by imac on 6/15/17.
 */

public class SingleRequestLocation extends RequestLocationManager
        implements RequestLocationManager.OnRequestLocationCallback {

    private static Locations sLocations;
    private List<Callback> mCallback = new ArrayList<>();
    private boolean mIsLocationPermissionEnabled;
    private boolean mIsLocationEnabled;

    public SingleRequestLocation(AppCompatActivity appCompatActivity, String locationRequestMessage) {
        super(appCompatActivity, locationRequestMessage);
        setOnRequestLocationCallback(this);
    }

    public static void reset() {
        if (sLocations != null) {
            sLocations.reset();
            sLocations = null;
        }
    }

    public static Locations getLocation() {
        return sLocations;
    }

    public void execute(boolean enableShowErrorPopup, Callback callback) {
        if (sLocations != null && !sLocations.isEmpty()) {
            Timber.i("Current user location was already requested: " +
                    ", country: " + sLocations.getCountry() +
                    ", latitude: " + sLocations.getLatitude() +
                    ", longitudes: " + sLocations.getLongitude());
            mIsLocationPermissionEnabled = RequestLocationManager.isLocationPermissionGranted(getActivity());
            callback.onComplete(sLocations, false, mIsLocationPermissionEnabled, mIsLocationEnabled);
        } else {
            forceExecute(enableShowErrorPopup, callback);
        }
    }

    public void forceExecute(boolean enableShowErrorPopup, Callback callback) {
        mCallback.add(callback);
        execute(enableShowErrorPopup);
    }

    @Override
    public void onLocationSuccess(final double latitude, final double longtitude) {
        mIsLocationPermissionEnabled = true;
        mIsLocationEnabled = true;
        getCountry(latitude, longtitude, false);
    }

    private void getCountry(final double latitude, final double longtitude, boolean isNeverAskAgain) {
        GeocoderUtil.getCountry(getActivity(), latitude, longtitude, countryCode -> {
            Locations location = new Locations();
            location.setLatitude(latitude);
            location.setLongitude(longtitude);
            location.setCountry(countryCode);
            sLocations = location;
            Timber.e("country: " + sLocations.getCountry() +
                    ", latitude: " + latitude +
                    ", longtitude: " + longtitude);

            for (Callback callback : mCallback) {
                callback.onComplete(sLocations, isNeverAskAgain, mIsLocationPermissionEnabled, mIsLocationEnabled);
            }
            for (int i = mCallback.size() - 1; i >= 0; i--) {
                mCallback.remove(i);
            }
        });
    }

    @Override
    public void onLocationDisable() {
        mIsLocationEnabled = false;
        getCountry(0, 0, false);
    }

    @Override
    public void onLocationPermissionDeny(boolean isNeverAskAgain) {
        mIsLocationPermissionEnabled = false;
        getCountry(0, 0, isNeverAskAgain);
    }

    public interface Callback {

        void onComplete(Locations location, boolean isNeverAskAgain, boolean isPermissionEnabled, boolean isLocationEnabled);
    }
}
