package com.proapp.sompom.helper.upload;

import android.content.Context;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.policy.UploadPolicy;
import com.proapp.sompom.R;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.Map;

import timber.log.Timber;

/**
 * Created by imac on 4/1/16.
 */
public class AbsChatUploader extends AbsMyUploader {

    private String mImagePath;
    private String mFileName;
    private final String mFileTitle;
    private final String mPreset;
    private final String mTag;
    private final FileType mFileType;

    /**
     * Constructor.
     *
     * @param context   :
     * @param imagePath : String the path of the image file in phone, where user want to us to upload.
     */
    public AbsChatUploader(final Context context,
                           FileType fileType,
                           final String imagePath,
                           final String fileTitle) {
        super(context);
        mImagePath = imagePath;
        mFileTitle = fileTitle;
        mPreset = context.getString(R.string.cloudinary_preset);
        mTag = context.getString(R.string.chat_tag);
        if (fileType == FileType.FILE) {
            mFileName = "chat_" + System.currentTimeMillis() + "_" + SharedPrefUtils.getUserId(context) + "_" + mFileTitle;
        } else {
            mFileName = "chat_" + SharedPrefUtils.getUserId(context) + "_" + System.currentTimeMillis();
        }

        mFileType = fileType;
        Timber.e(mPreset + ", " + mTag + ", " + mFileName + ", " + mImagePath);
    }

    @Override
    public void cancelUpload(String requestId) {
        MediaManager.get().cancelRequest(requestId);
    }

    /**
     * Call for Process upload image.
     */
    public String doUpload() {
        return TransformationProvider.generateDefaultUploadRequest(
                mFileType,
                mImagePath,
                mFileName,
                mPreset,
                mTag,
                getUploadFolder() + "chat",
                new UploadPolicy.Builder().maxRetries(0).build(),
                this)
                .dispatch();
    }

    @Override
    public void onSuccess(String requestId, Map resultData) {
        handleOnSuccess(requestId, resultData);
    }
}
