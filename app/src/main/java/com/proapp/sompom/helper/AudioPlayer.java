package com.proapp.sompom.helper;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.RawRes;

import com.proapp.sompom.R;

import java.io.FileInputStream;

import timber.log.Timber;

public class AudioPlayer {

    static final String LOG_TAG = AudioPlayer.class.getSimpleName();

    private final static int SAMPLE_RATE = 16000;
    private final Context mContext;
    private CustomMediaPlayer mIncomingTonePlayer;
    private CustomMediaPlayer mContactingTonePlayer;
    private CustomMediaPlayer mCallOnOrOffTonePlayer;
    private AudioTrack mRingingTone; //As a caller, it is ringing status sound
    private MediaPlayer mWritingMessagePlayer;

    public AudioPlayer(Context context) {
        this.mContext = context.getApplicationContext();
    }

    private static AudioTrack createProgressTone(Context context, @RawRes int toneId) throws Exception {
        AssetFileDescriptor fd = context.getResources().openRawResourceFd(toneId);
        int length = (int) fd.getLength();

        AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_VOICE_CALL, SAMPLE_RATE,
                AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, length, AudioTrack.MODE_STATIC);

        byte[] data = new byte[length];
        readFileToBytes(fd, data);

        audioTrack.write(data, 0, data.length);
        audioTrack.setLoopPoints(0, data.length / 2, 30);

        return audioTrack;
    }

    private static void readFileToBytes(AssetFileDescriptor fd, byte[] data) throws Exception {
        FileInputStream inputStream = fd.createInputStream();

        int bytesRead = 0;
        while (bytesRead < data.length) {
            int res = inputStream.read(data, bytesRead, (data.length - bytesRead));
            if (res == -1) {
                break;
            }
            bytesRead += res;
        }
    }

    public void playIncomingCallTone(boolean isDelayed) {
        // Best practice to always stop and release a mediaPlayer when no longer needed
        if (mIncomingTonePlayer != null) {
            stopIncomingTone(true);
        }
        // Honour silent mode
        mIncomingTonePlayer = new CustomMediaPlayer();
        mIncomingTonePlayer.setAudioStreamType(AudioManager.STREAM_RING);
        try {
            mIncomingTonePlayer.setDataSource(mContext,
                    Uri.parse("android.resource://" + mContext.getPackageName() + "/" + R.raw.phone_loud1));
            mIncomingTonePlayer.prepare();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Could not setup media player for playIncomingCallTone");
            mIncomingTonePlayer = null;
            SentryHelper.logSentryError(e);
            return;
        }
        mIncomingTonePlayer.setLooping(true);
        if (isDelayed) {
            new Handler().post(() -> {
                if (mIncomingTonePlayer != null &&
                        mIncomingTonePlayer.isPlayerPrepared() &&
                        !mIncomingTonePlayer.isPlayerPlaying()) {
                    // Only play ringing and vibrate once everything is setup and the MediaPlayer
                    // was not canceled
                    DeviceHelper.performVibration(mContext);
                    mIncomingTonePlayer.start();
                }
            });
        } else {
            mIncomingTonePlayer.start();
        }
    }

    public void stopContactingOrIncomingTone() {
        stopIncomingTone(true);
        stopContactingTone();
    }

    public void stopIncomingTone(boolean stopVibration) {
        if (stopVibration) {
            DeviceHelper.stopVibration(mContext);
        }
        try {
            stopMediaPlayer(mIncomingTonePlayer);
            releaseMediaPlayer(mIncomingTonePlayer);
            mIncomingTonePlayer = null;
        } catch (Exception ex) {
            Timber.e("Stop stopContactingOrIncomingTone error: " + ex.getMessage());
            SentryHelper.logSentryError(ex);
        }
    }

    private void stopMediaPlayer(MediaPlayer mediaPlayer) {
        if (mediaPlayer != null) {
            if (mediaPlayer instanceof CustomMediaPlayer) {
                if (((CustomMediaPlayer) mediaPlayer).isPlaying()) {
                    ((CustomMediaPlayer) mediaPlayer).stop();
                }
            } else {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
            }
        }
    }

    private void releaseMediaPlayer(MediaPlayer mediaPlayer) {
        if (mediaPlayer != null) {
            if (mediaPlayer instanceof CustomMediaPlayer) {
                ((CustomMediaPlayer) mediaPlayer).release();
            } else {
                mediaPlayer.release();
            }
        }
    }

    public void stopContactingTone() {
        try {
            Timber.i("stopContactingTone");
            stopMediaPlayer(mContactingTonePlayer);
            releaseMediaPlayer(mContactingTonePlayer);
            mContactingTonePlayer = null;
        } catch (Exception ex) {
            Timber.e("Stop stopContactingTone error: " + ex.getMessage());
            SentryHelper.logSentryError(ex);
        }
    }

    public void stopAllTones() {
        stopContactingOrIncomingTone();
        stopRingingTone();
    }

    public void playRingingTone() {
        if (!isRingingTonePlaying()) {
            stopContactingOrIncomingTone();
            try {
                mRingingTone = createProgressTone(mContext, R.raw.progress_tone);
                mRingingTone.play();
            } catch (Exception e) {
                Log.e(LOG_TAG, "Could not play RingingTone.", e);
                SentryHelper.logSentryError(e);
            }
        }
    }

    private boolean isRingingTonePlaying() {
        return mRingingTone != null && mRingingTone.getPlayState() == AudioTrack.PLAYSTATE_PLAYING;
    }

    public void playContactingTone() {
        // Best practice to always stop and release a mediaPlayer when no longer needed
        if (mContactingTonePlayer != null) {
            stopContactingTone();
        }

        mContactingTonePlayer = new CustomMediaPlayer();
        mContactingTonePlayer.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
        try {
            Timber.i("playContactingTone");
            mContactingTonePlayer.setDataSource(mContext,
                    Uri.parse("android.resource://" + mContext.getPackageName() + "/" + R.raw.contacting_tone));
            mContactingTonePlayer.setOnPreparedListener(preparedPlayer -> {
                if (mContactingTonePlayer != null) {
                    mContactingTonePlayer.setLooping(true);
                    mContactingTonePlayer.start();
                }
            });
            mContactingTonePlayer.prepareAsync();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Could not setup media player for contactingTone");
            mContactingTonePlayer = null;
            SentryHelper.logSentryError(e);
        }
    }

    public void playCallOnOrOffTone(boolean isCallOff) {
        CustomMediaPlayer callOnOrOffTonePlayer = new CustomMediaPlayer();
        if (isCallOff) {
            callOnOrOffTonePlayer.setAudioStreamType(AudioManager.STREAM_SYSTEM);
        } else {
            callOnOrOffTonePlayer.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
        }
        try {
            callOnOrOffTonePlayer.setDataSource(mContext,
                    Uri.parse("android.resource://" + mContext.getPackageName() + "/" + (isCallOff ? R.raw.call_off : R.raw.call_on)));
            callOnOrOffTonePlayer.setOnPreparedListener(preparedPlayer -> {
                // Since callOn or callOff is a one shot mediaPlayer, make sense to always release
                // the media player once it finished playing
                callOnOrOffTonePlayer.setOnCompletionListener(completedPlayer -> {
                    try {
                        callOnOrOffTonePlayer.stop();
                    } catch (Exception exception) {
                        Log.e(LOG_TAG, "Could not setup media player for callOnOrOffTone");
                        SentryHelper.logSentryError(exception);
                    }
                    callOnOrOffTonePlayer.release();
                });
                callOnOrOffTonePlayer.start();
            });
            callOnOrOffTonePlayer.prepareAsync();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Could not setup media player for callOnOrOffTone");
            SentryHelper.logSentryError(e);
        }
    }

    private boolean isContactingOrIncomingTonePlaying() {
        try {
            return (mIncomingTonePlayer != null && mIncomingTonePlayer.isPlaying()) ||
                    (mContactingTonePlayer != null && mContactingTonePlayer.isPlaying());
        } catch (Exception exception) {
            SentryHelper.logSentryError(exception);
            return false;
        }
    }

    private boolean isIncomingTonePlaying() {
        try {
            return (mIncomingTonePlayer != null && mIncomingTonePlayer.isPlaying());
        } catch (Exception exception) {
            SentryHelper.logSentryError(exception);
            return false;
        }
    }

    public void stopRingingTone() {
        if (mRingingTone != null) {
            try {
                if (mRingingTone.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
                    mRingingTone.stop();
                }
                mRingingTone.release();
                mRingingTone = null;
            } catch (Exception ex) {
                Timber.e("stopRingingTone error: " + ex.getMessage());
                SentryHelper.logSentryError(ex);
            }
        }
    }

    public void checkToStopIncomingToneWhenDeviceButtonPressed() {
        if (isIncomingTonePlaying()) {
            stopIncomingTone(true);
        }
    }

    public void checkToStopContactingOrIncomingTone() {
        if (isContactingOrIncomingTonePlaying()) {
            stopContactingOrIncomingTone();
        }
    }

    public void playWritingMessageIndicatorSound() {
        if (mWritingMessagePlayer == null) {
            mWritingMessagePlayer = new MediaPlayer();
            try {
                mWritingMessagePlayer.setAudioStreamType(AudioManager.STREAM_SYSTEM);
                mWritingMessagePlayer.setDataSource(mContext,
                        Uri.parse("android.resource://" + mContext.getPackageName() + "/" + R.raw.writing_message));
                mWritingMessagePlayer.setOnPreparedListener(preparedPlayer -> {
                    if (mWritingMessagePlayer != null) {
                        mWritingMessagePlayer.setLooping(true);
                        mWritingMessagePlayer.start();
                    }
                });
                mWritingMessagePlayer.prepare();
            } catch (Exception e) {
                Timber.e("Setup playWritingMessageIndicatorSound error: " + e);
                SentryHelper.logSentryError(e);
            }
        } else if (!mWritingMessagePlayer.isPlaying()) {
            mWritingMessagePlayer.seekTo(0);
            mWritingMessagePlayer.start();
        }
    }

    public boolean isPlayingWritingMessageIndicatorSound() {
        return mWritingMessagePlayer != null && mWritingMessagePlayer.isPlaying();
    }

    public void releaseWritingMessageIndicatorSound() {
        try {
            stopMediaPlayer(mWritingMessagePlayer);
            releaseMediaPlayer(mWritingMessagePlayer);
            mWritingMessagePlayer = null;
        } catch (Exception ex) {
            Timber.e("releaseWritingMessageIndicatorSound error: " + ex.getMessage());
            SentryHelper.logSentryError(ex);
        }
    }

    public void stopWritingMessageIndicatorSound() {
        try {
            if (isPlayingWritingMessageIndicatorSound()) {
                mWritingMessagePlayer.pause();
            }
        } catch (Exception ex) {
            Timber.e("stopWritingMessageIndicatorSound error: " + ex.getMessage());
            SentryHelper.logSentryError(ex);
        }
    }
}
