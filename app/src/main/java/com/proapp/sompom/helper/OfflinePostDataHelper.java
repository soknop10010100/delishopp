package com.proapp.sompom.helper;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.utils.FileUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 5/5/2020.
 */
public class OfflinePostDataHelper {

    private static final String NEW_FEED_FILE_NAME = "new_feed.json";
    private static final int MAX_OFFLINE_POST_COUNT = 300;

    private OfflinePostDataHelper() {
    }

    public static void writePosts(Context context,
                                  boolean isLoadMore,
                                  List<LifeStream> newPosts,
                                  OfflinePostDataHelperListener listener) {
        readPosts(context, new OfflinePostDataHelperListener() {
            @Override
            public void onReadFinish(List<LifeStream> lifeStreams, Throwable e) {
                if (lifeStreams != null && !lifeStreams.isEmpty()) {
                    List<LifeStream> newList = new ArrayList<>();
                    for (LifeStream newPost : newPosts) {
                        int postIndex = findPostIndexByIdOrActivityId(lifeStreams, newPost.getId());
                        if (postIndex >= 0) {
                            //Update the exist data
                            lifeStreams.set(postIndex, newPost);
                        } else {
                            //New
                            newList.add(newPost);
                        }
                    }

                    if (isLoadMore) {
                        //Append last
                        lifeStreams.addAll(newList);
                    } else {
                        //Append top
                        lifeStreams.addAll(0, newList);
                    }
                } else {
                    lifeStreams = new ArrayList<>(newPosts);
                }

                //Start writing data
                final List<LifeStream> dataToWrite = new ArrayList<>(lifeStreams);
//                Timber.i("New list size: " + dataToWrite.size());
                writeFileInternally(context, dataToWrite, listener);
            }
        });
    }

    private static void writeFileInternally(Context context,
                                            List<LifeStream> dataToWrite,
                                            OfflinePostDataHelperListener listener) {
        Observable.create((ObservableOnSubscribe<Boolean>) e1 -> {
            FileUtils.writeToFile(context, NEW_FEED_FILE_NAME, new Gson().toJson(dataToWrite));
            e1.onNext(true);
            e1.onComplete();
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(getWriteSubscriber(listener));
    }

    private static int findPostIndexByIdOrActivityId(List<LifeStream> lifeStreamList, String checkingId) {
        for (int i = 0; i < lifeStreamList.size(); i++) {
            if (TextUtils.equals(checkingId, lifeStreamList.get(i).getId())) {
                return i;
            }
        }

        return -1;
    }

    public static void resetLocalData(Context context) {
        FileUtils.writeToFile(context, NEW_FEED_FILE_NAME, "");
    }

    public static void readPostsWithPagination(final Context context,
                                               String lastPostActivityId,
                                               int limit,
                                               final OfflinePostDataHelperListener listener) {
        Timber.i("readPostsWithPagination: lastPostActivityId: " + lastPostActivityId + ", limit: " + limit);
        Observable.create((ObservableOnSubscribe<List<LifeStream>>) e -> {
            String data = FileUtils.readFromFile(context, NEW_FEED_FILE_NAME);
            List<LifeStream> newData = new ArrayList<>();
            if (!TextUtils.isEmpty(data)) {
                List<LifeStream> lifeStreams = new Gson().fromJson(data, new TypeToken<List<LifeStream>>() {
                }.getType());
                Timber.i("Local post: " + lifeStreams.size());
                if (!lifeStreams.isEmpty()) {
                    if (!TextUtils.isEmpty(lastPostActivityId)) {
                        int lastDisplayIndex = findPostIndexByIdOrActivityId(lifeStreams, lastPostActivityId);
                        Timber.i("lastDisplayIndex: " + lastDisplayIndex);
                        if (lastDisplayIndex >= 0) {
                            //Last display post exist
                            if (lifeStreams.size() - (lastDisplayIndex + 1) >= limit) {
                                for (int i = lastDisplayIndex + 1; i < lifeStreams.size(); i++) {
                                    if (newData.size() == limit) {
                                        break;
                                    }
                                    newData.add(lifeStreams.get(i));
                                }
                            } else {
                                for (int i = lastDisplayIndex + 1; i < lifeStreams.size(); i++) {
                                    newData.add(lifeStreams.get(i));
                                }
                            }
                        } else if (limit <= lifeStreams.size()) {
                            //No last display post
                            for (int i = 0; i < limit; i++) {
                                newData.add(lifeStreams.get(i));
                            }
                        } else {
                            //No last display post
                            newData.addAll(lifeStreams);
                        }
                    } else {
                        //No last display post
                        if (lifeStreams.size() >= limit) {
                            for (int i = 0; i < limit; i++) {
                                newData.add(lifeStreams.get(i));
                            }
                        } else {
                            newData.addAll(lifeStreams);
                        }
                    }
                }
            }
            e.onNext(newData);
            e.onComplete();
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(getReadSubscriber(listener));
    }

    public static void readPosts(final Context context,
                                 final OfflinePostDataHelperListener listener) {
        Observable.create((ObservableOnSubscribe<List<LifeStream>>) e -> {
            String data = FileUtils.readFromFile(context, NEW_FEED_FILE_NAME);
            if (!TextUtils.isEmpty(data)) {
                e.onNext(new Gson().fromJson(data, new TypeToken<List<LifeStream>>() {
                }.getType()));
            } else {
                e.onNext(new ArrayList<>());
            }
            e.onComplete();
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(getReadSubscriber(listener));
    }

    public static void findPostByIdOrActivityId(final Context context,
                                                String postIdOrActivityId,
                                                final OfflinePostDataHelperListener listener) {
        readPosts(context, new OfflinePostDataHelperListener() {
            @Override
            public void onReadFinish(List<LifeStream> lifeStreamList, Throwable e) {
                int index = findPostIndexByIdOrActivityId(lifeStreamList, postIdOrActivityId);
                if (index >= 0) {
                    if (listener != null) {
                        listener.onFindPostSuccess(lifeStreamList.get(index));
                    }
                } else {
                    if (listener != null) {
                        listener.onFindPostSuccess(null);
                    }
                }
            }
        });
    }

    public static void removeLocalPost(Context context, String postId) {
        readPosts(context, new OfflinePostDataHelperListener() {
            @Override
            public void onReadFinish(List<LifeStream> lifeStreamList, Throwable e) {
                if (lifeStreamList != null && !lifeStreamList.isEmpty()) {
                    Timber.i("removeLocalPost: 1: " + lifeStreamList.size());
                    boolean found = false;
                    for (int size = lifeStreamList.size() - 1; size >= 0; size--) {
                        if (TextUtils.equals(lifeStreamList.get(size).getId(), postId)) {
                            lifeStreamList.remove(size);
                            found = true;
                            break;
                        }
                    }
                    if (found) {
                        Timber.i("removeLocalPost: 2: " + lifeStreamList.size());
                        writeFileInternally(context, lifeStreamList, null);
                    }
                }
            }
        });
    }

    private static Observer<List<LifeStream>> getReadSubscriber(final OfflinePostDataHelperListener listener) {
        return new Observer<List<LifeStream>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<LifeStream> lifeStreamList) {
                if (listener != null) {
                    listener.onReadFinish(lifeStreamList, null);
                }
            }

            @Override
            public void onError(Throwable e) {
                Timber.e(e);
                if (listener != null) {
                    listener.onReadFinish(new ArrayList<>(), e);
                }
            }

            @Override
            public void onComplete() {

            }
        };
    }

    private static Observer<Boolean> getWriteSubscriber(final OfflinePostDataHelperListener listener) {
        return new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Boolean lifeStreamList) {
                if (listener != null) {
                    listener.onWriteFinish(null);
                }
            }

            @Override
            public void onError(Throwable e) {
                Timber.e(e);
                if (listener != null) {
                    listener.onWriteFinish(e);
                }
            }

            @Override
            public void onComplete() {

            }
        };
    }

    public static void checkMaxCountOfOfflinePost(final Context context,
                                                  final OfflinePostDataHelperListener listener) {
        readPosts(context, new OfflinePostDataHelperListener() {
            @Override
            public void onReadFinish(List<LifeStream> lifeStreamList, Throwable e) {
//                Timber.i("onReadFinish: size " + lifeStreamList.size());
                if (e == null) {
                    if (lifeStreamList.size() > MAX_OFFLINE_POST_COUNT) {
//                        Timber.i("Offline post size is over " + MAX_OFFLINE_POST_COUNT +
//                                ", so need to remove the exceeded items.");
                        for (int index = lifeStreamList.size() - 1; index >= MAX_OFFLINE_POST_COUNT; index--) {
                            lifeStreamList.remove(index);
                        }
//                        Timber.i("Valid size after remove: " + lifeStreamList.size());
                        writeFileInternally(context, lifeStreamList, new OfflinePostDataHelperListener() {
                            @Override
                            public void onWriteFinish(Throwable e) {
                                if (listener != null) {
                                    listener.onValidDataFinish();
                                }
                            }
                        });
                    } else {
                        if (listener != null) {
                            listener.onValidDataFinish();
                        }
                    }
                } else {
                    if (listener != null) {
                        listener.onValidDataFinish();
                    }
                }
            }
        });
    }

    public static class OfflinePostDataHelperListener {

        public void onWriteFinish(Throwable e) {
            //Empty Impl...
        }

        public void onReadFinish(List<LifeStream> lifeStreamList, Throwable e) {
            //Empty Impl...
        }

        public void onFindPostSuccess(LifeStream lifeStream) {
            //Empty Impl...
        }

        public void onValidDataFinish() {
            //Empty Impl...
        }
    }
}
