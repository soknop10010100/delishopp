package com.proapp.sompom.helper;

import android.os.Handler;
import android.text.style.ClickableSpan;
import android.view.View;

import androidx.annotation.NonNull;

public class DelayClickableSpan extends ClickableSpan implements DelayClickListener {

    private boolean mIsClickable = true;

    @Override
    public final void onClick(@NonNull View widget) {
        if (mIsClickable) {
            mIsClickable = false;
            onDelayClick(widget);
            new Handler().postDelayed(() -> mIsClickable = true, DELAY_DURATION);
        }
    }

    @Override
    public void onDelayClick(@NonNull View widget) {
        //Empty Impl....
    }
}
