package com.proapp.sompom.helper.upload;

import android.content.Context;
import android.graphics.Bitmap;

import com.desmond.squarecamera.utils.media.ImageUtil;
import com.proapp.sompom.utils.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;

import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 25/11/21.
 */
public class ConciergeAddressUploader extends AbsUploader {

    // TODO: Confirm address photo resolution
    private static final int CONCIERGE_ADDRESS_PHOTO_RESIZE = 512;

    private Callback mCallback;

    public ConciergeAddressUploader(Context context,
                                    String imagePath,
                                    UploadType uploadType,
                                    Callback callback) {
        super(context, imagePath, uploadType);
        mCallback = callback;
    }

    @Override
    public String getUploadFolder() {
        return super.getUploadFolder() + "address";
    }

    public static Observable<UploadResult> uploadConciergeAddressPhoto(Context context, String path) {
        Observable<UploadResult> ob = Observable.create(e -> {
            byte[] bytes = getUploadDataForProfilePhoto(context, path);
            ConciergeAddressUploader uploader = new ConciergeAddressUploader(context, path, UploadType.Address, new Callback() {
                @Override
                public void onUploaded(UploadResult result) {
                    e.onNext(result);
                    e.onComplete();
                }

                @Override
                public void onUploadFail(Exception ex) {
                    e.onError(ex);
                    e.onComplete();
                }
            });
            uploader.doUpload(bytes);
        });
        ob = ob.onErrorResumeNext(throwable -> {
            if (throwable instanceof FileNotFoundException) {
                return Observable.just(new UploadResult());
            }
            return Observable.error(throwable);
        });
        return ob;
    }

    private static byte[] getUploadDataForProfilePhoto(Context context, String uri) {
        File file = new File(uri);
        if (file.exists()) {
            Bitmap bmp = ImageUtil.getBitmapFromFilePath(uri);
            byte[] bytesArray = null;
            if (bmp != null) {
                if (bmp.getWidth() > CONCIERGE_ADDRESS_PHOTO_RESIZE) {
                    Timber.i("Will resize concierge address photo to only " + CONCIERGE_ADDRESS_PHOTO_RESIZE);
                    Bitmap resizeBitmap = ImageUtil.resizeBitmap(bmp, CONCIERGE_ADDRESS_PHOTO_RESIZE, CONCIERGE_ADDRESS_PHOTO_RESIZE);
                    if (resizeBitmap != null) {
                        bytesArray = convertByteArrayFromBitmap(resizeBitmap);
                    }
                } else {
                    bytesArray = convertByteArrayFromBitmap(bmp);
                }
            }

            if (bytesArray == null || bytesArray.length <= 0) {
                return FileUtils.convertFileToByArray(context, uri);
            } else {
                return bytesArray;
            }
        }

        return FileUtils.convertFileToByArray(context, uri);
    }

    private static byte[] convertByteArrayFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        bitmap.recycle();

        return byteArray;
    }

    @Override
    public void onUploadSucceeded(String imageUrl, String thumb) {
        if (mCallback != null) {
            mCallback.onUploaded(new UploadResult(imageUrl, thumb));
        }
    }

    @Override
    public void onUploadFail(String id, String ex) {
        if (mCallback != null) {
            mCallback.onUploadFail(new Exception(ex));
        }
    }

    public interface Callback {
        void onUploaded(UploadResult uploadResult);

        void onUploadFail(Exception ex);
    }

    public static class UploadResult {
        private String mUrl;
        private String mThumbnail;

        public UploadResult() {
        }

        public UploadResult(String url, String thumbnail) {
            mUrl = url;
            mThumbnail = thumbnail;
        }

        public String getUrl() {
            return mUrl;
        }


        public String getThumbnail() {
            return mThumbnail;
        }
    }
}
