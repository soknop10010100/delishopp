package com.proapp.sompom.helper.upload;

import android.net.Uri;
import android.text.TextUtils;

import com.cloudinary.Transformation;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.UploadRequest;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.policy.UploadPolicy;
import com.proapp.sompom.utils.FileUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 9/2/2019.
 */
public final class TransformationProvider {

    private static final List<String> AUDIO_SUPPORT_EXTENSIONS = Arrays.asList("aac", "adts", "ac3", "aif", "aiff", "aifc", "caf", "mp3", "mp4", "m4a", "snd", "au", "sd2", "wav");
    private static final String GIF_FORMAT = "gif";
    private static final String MP4_FORMAT = "mp4";

    private static final String RESOURCE_TYPE = "resource_type";
    private static final String FORMAT = "format";
    private static final String PUBLIC_ID = "public_id";
    private static final String RAW = "raw";
    private static final String AUTO = "auto";
    private static final String SECURE_URL = "secure_url";
    private static final String IMAGE = "image";
    private static final String VIDEO = "video";
    private static final String WIDTH = "width";
    private static final String HEIGHT = "height";
    private static final String PRESET = "upload_preset";
    private static final String TAGS = "tags";
    private static final String FOLDER = "folder";
    private static final String IS_AUDIO = "is_audio";
    private static final int SCALE_FACTOR = 480;
    private static final int SQUARE_SCALE_FACTOR = 200;

    public static UploadRequest generateDefaultUploadRequest(FileType fileType,
                                                             String uri,
                                                             String fileName,
                                                             String preset,
                                                             String tags,
                                                             String folder,
                                                             UploadPolicy uploadPolicy,
                                                             UploadCallback callback) {
        String resourceType = getUploadResourceType(fileType, fileName);
        String uploadPublicId = getUploadPublicId(fileName, resourceType);
        Timber.i("fileType: " + fileType +
                ", fileName: " + fileName +
                ", folder: " + folder +
                ", uri: " + uri +
                ", resourceType: " + resourceType +
                ", uploadPublicId: " + uploadPublicId);
        UploadRequest uploadRequest;
        if (FileUtils.isFileExisted(uri)) {
            uploadRequest = MediaManager.get().upload(uri);
        } else {
            uploadRequest = MediaManager.get().upload(Uri.parse(uri));
        }
        return uploadRequest
                .option(RESOURCE_TYPE, resourceType)
                .option(PUBLIC_ID, uploadPublicId)
                .option(PRESET, preset)
                .option(TAGS, tags)
                .option(FOLDER, folder)
                .policy(uploadPolicy)
                .callback(callback);
    }

    public static UploadRequest generateDefaultUploadRequest(FileType fileType,
                                                             byte[] bytes,
                                                             String fileName,
                                                             String preset,
                                                             String tags,
                                                             String folder,
                                                             UploadPolicy uploadPolicy,
                                                             UploadCallback callback) {
        String resourceType = getUploadResourceType(fileType, fileName);
        String uploadPublicId = getUploadPublicId(fileName, resourceType);
        Timber.i("fileType: " + fileType +
                ", fileName: " + fileName +
                ", folder: " + folder +
                ", resourceType: " + resourceType +
                ", uploadPublicId: " + uploadPublicId);
        return MediaManager.get().upload(bytes)
                .option(RESOURCE_TYPE, resourceType)
                .option(PUBLIC_ID, uploadPublicId)
                .option(PRESET, preset)
                .option(TAGS, tags)
                .option(FOLDER, folder)
                .policy(uploadPolicy)
                .callback(callback);
    }

    private static String getUploadResourceType(FileType fileType, String fileName) {
        if (fileType.equals(FileType.IMAGE) ||
                fileType.equals(FileType.GIF) ||
                fileType.equals(FileType.VIDEO) ||
                fileType.equals(FileType.Audio) ||
                TransformationProvider.isInAutoSupportFormat(fileName)) {
            return AUTO;
        } else {
            return RAW;
        }
    }

    private static String getUploadPublicId(String fileName, String uploadResourceType) {
        if (uploadResourceType.equalsIgnoreCase(AUTO)) {
            return getPublicIdWithoutExtension(fileName);
        } else {
            //RAW type
            return getPublicIdWithExtension(fileName, fileName);
        }
    }

    public static String getDefaultOptimizeTransformation(Map resultData) {
        if (isAudio(resultData)) {
            Timber.i("isAudio");
            return MediaManager.get()
                    .url()
                    .resourceType(getResourceType(resultData))
                    .transformation(new Transformation()
                            .fetchFormat("mp3")
                            .quality("auto")
                            .videoCodec("auto"))
                    .generate(getPublicId(resultData) + ".mp3");
        } else if (isImageType(resultData)) {
            Timber.i("isImageType");
            return MediaManager
                    .get()
                    .url()
                    .resourceType(getResourceType(resultData))
//                    .transformation(new Transformation()
//                            .width(getCorrectTransformWidth(resultData))
//                            .quality("auto")
//                            .crop("scale")
//                    )
                    .generate(getPublicId(resultData) + "." + getExtensionFromResultUrl(resultData));
        } else if (isVideoType(resultData)) {
            Timber.i("isVideoType");
            return MediaManager
                    .get()
                    .url()
                    .resourceType(getResourceType(resultData))
                    .transformation(new Transformation()
                            .fetchFormat("mp4")
//                            .width(getCorrectTransformWidth(resultData))
//                            .quality("auto")
//                            .crop("scale")
                            .videoCodec("auto"))
                    .generate(getPublicId(resultData) + ".mp4");
        } else {
            //Return the default one.
            return getDefaultUrl(resultData);
        }
    }

    public static String getProfileOptimizeTransformation(Map resultData) {
        if (isImageType(resultData)) {
            int fileHeight = getFileHeight(resultData);
            int fileWidth = getFileWidth(resultData);
            Timber.i("fileHeight: " + fileHeight + ", fileWidth: " + fileWidth);
            Transformation transformation = new Transformation()
                    .crop("scale")
                    .quality("auto");
            if (fileHeight > SQUARE_SCALE_FACTOR || fileWidth > SQUARE_SCALE_FACTOR) {
                transformation = transformation.crop("fill")
                        .width(SQUARE_SCALE_FACTOR)
                        .height(SQUARE_SCALE_FACTOR);
            }
            return MediaManager.get()
                    .url()
                    .resourceType(getResourceType(resultData))
                    .transformation(transformation)
                    .generate(getPublicId(resultData) + "." + getExtensionFromResultUrl(resultData));
        } else {
            //Return the default one.
            return getDefaultUrl(resultData);
        }
    }

    public static String getThumbnailOptimizeTransformation(Map resultData) {
        if (isVideoType(resultData) || isImageType(resultData)) {
            String fullName = isVideoType(resultData) ? getPublicId(resultData) + ".jpg"
                    : getPublicId(resultData) + "." + getExtensionFromResultUrl(resultData);
            int correctTransformWidth = getCorrectTransformWidth(resultData);
            Transformation transformation = new Transformation()
                    .crop("scale")
                    .fetchFormat("auto")
                    .flags("lossy")
                    .quality("auto");
            if (correctTransformWidth >= SCALE_FACTOR) {
                transformation = transformation.width(correctTransformWidth);
            }
            return MediaManager.get()
                    .url()
                    .resourceType(getResourceType(resultData))
                    .transformation(transformation)
                    .generate(fullName);
        }

        //Return the default one.
        return getDefaultUrl(resultData);
    }

    public static boolean shouldOptimizeResource(Map resultData) {
        return isAudio(resultData) || isImageType(resultData) || isVideoType(resultData);
    }

    public static boolean shouldGenerateThumbnail(Map resultData) {
        return isImageType(resultData) || isVideoType(resultData);
    }

    private static boolean isAudio(Map resultData) {
        //First checking
        Boolean isAudio = (Boolean) resultData.get(IS_AUDIO);
        if (isAudio != null) {
            return isAudio;
        }

        //Second checking
        String resourceType = getResourceType(resultData);
        if (resourceType.equalsIgnoreCase(RAW)) {
            String extension = getExtensionFromResultUrl(resultData);
            Timber.i("extension: " + resourceType);
            for (String audioSupportExtension : AUDIO_SUPPORT_EXTENSIONS) {
                if (!TextUtils.isEmpty(extension) && audioSupportExtension.equalsIgnoreCase(extension)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static boolean isInAudioExtension(String extension) {
        for (String audioSupportExtension : AUDIO_SUPPORT_EXTENSIONS) {
            if (!TextUtils.isEmpty(extension) && audioSupportExtension.equalsIgnoreCase(extension)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isInAutoSupportFormat(String filePath) {
        return isInAudioExtension(FileUtils.getFileExtensionFromFilePath(filePath));
    }

    public static String getPublicIdWithoutExtension(String fileName) {
        String extension = FileUtils.getFileExtensionFromFilePath(fileName);
        if (!TextUtils.isEmpty(extension)) {
            return fileName.replace("." + extension, "");
        }

        return fileName;
    }

    public static String getPublicIdWithExtension(String filePath, String fileName) {
        String extension = FileUtils.getFileExtensionFromFilePath(fileName);
        if (!TextUtils.isEmpty(extension)) {
            //Already has extension
            return fileName;
        }

        String extension1 = FileUtils.getFileExtensionFromFilePath(filePath);
        if (!TextUtils.isEmpty(extension1)) {
            return fileName + "." + extension1;
        }

        return fileName;
    }

    private static boolean isImageType(Map resultData) {
        String resourceType = getResourceType(resultData);
        return resourceType.equalsIgnoreCase(IMAGE);
    }

    public static boolean isGifFormat(Map resultData) {
        String format = (String) resultData.get(FORMAT);
        return !TextUtils.isEmpty(format) && TextUtils.equals(format, GIF_FORMAT);
    }

    public static String getMP4UrlFromGifFile(Map resultData) {
        String defaultUrl = getDefaultUrl(resultData);
        String gifExtension = "." + GIF_FORMAT;
        String mp4Extension = "." + MP4_FORMAT;
        return defaultUrl.replace(gifExtension, mp4Extension);
    }

    public static String getGifMP4ThumbnailUrl(Map resultData) {
        String fullName = getPublicId(resultData) + ".mp4";
        int correctTransformWidth = getCorrectTransformWidth(resultData);
        Transformation transformation = new Transformation()
                .crop("scale")
                .fetchFormat("mp4")
                .videoCodec("auto")
                .quality("auto");
        if (correctTransformWidth >= SCALE_FACTOR) {
            transformation = transformation.width(correctTransformWidth);
        }

        return MediaManager.get()
                .url()
                .resourceType(getResourceType(resultData))
                .transformation(transformation)
                .generate(fullName);
    }

    private static boolean isVideoType(Map resultData) {
        String resourceType = getResourceType(resultData);
        return resourceType.equalsIgnoreCase(VIDEO);
    }

    private static String getExtensionFromResultUrl(Map resultData) {
        return FileUtils.getFileExtensionFromFilePath(getDefaultUrl(resultData));
    }

    public static String getDefaultUrl(Map resultData) {
        return (String) resultData.get(SECURE_URL);
    }

    private static String getResourceType(Map resultData) {
        String resourceType = (String) resultData.get(RESOURCE_TYPE);
        Timber.i("resourceType: " + resourceType);
        return resourceType;
    }

    private static String getPublicId(Map resultData) {
        return (String) resultData.get(PUBLIC_ID);
    }

    private static int getFileWidth(Map resultData) {
        if (resultData.containsKey(WIDTH)) {
            return (int) resultData.get(WIDTH);
        }

        return 0;
    }

    private static int getFileHeight(Map resultData) {
        if (resultData.containsKey(HEIGHT)) {
            return (int) resultData.get(HEIGHT);
        }

        return 0;
    }

    private static int getCorrectTransformWidth(Map resultData) {
        int fileWidth = getFileWidth(resultData);
        if (fileWidth > SCALE_FACTOR) {
            return SCALE_FACTOR;
        }

        return fileWidth;
    }
}
