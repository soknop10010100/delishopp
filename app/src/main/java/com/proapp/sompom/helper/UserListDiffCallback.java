package com.proapp.sompom.helper;

import android.text.TextUtils;

import androidx.recyclerview.widget.DiffUtil;

import com.proapp.sompom.listener.UserListAdaptive;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.result.User;

import java.util.List;

public class UserListDiffCallback extends DiffUtil.Callback {

    private final List<UserListAdaptive> mOldList;
    private final List<UserListAdaptive> mNewList;

    public UserListDiffCallback(List<UserListAdaptive> oldBaseChatModelList,
                                List<UserListAdaptive> newBaseChatModelList) {
        this.mOldList = oldBaseChatModelList;
        this.mNewList = newBaseChatModelList;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldPos, int newPos) {
        //Check for group header
        if ((mOldList.get(oldPos) instanceof UserGroup && mNewList.get(newPos) instanceof UserGroup)) {
            return true;
        } else if ((mOldList.get(oldPos) instanceof User && mNewList.get(newPos) instanceof User)) {
            return (((User) mOldList.get(oldPos)).getId().matches(((User) mNewList.get(newPos)).getId()));
        }

        return true;
    }

    @Override
    public boolean areContentsTheSame(int oldPos, int newPos) {
        if ((mOldList.get(oldPos) instanceof UserGroup && mNewList.get(newPos) instanceof UserGroup) &&
                (!TextUtils.isEmpty(((UserGroup) mOldList.get(oldPos)).getName()) &&
                        !TextUtils.isEmpty(((UserGroup) mNewList.get(newPos)).getName())) &&
                TextUtils.equals(((UserGroup) mOldList.get(oldPos)).getName(), ((UserGroup) mNewList.get(newPos)).getName())) {
            return true;
        } else if ((mOldList.get(oldPos) instanceof User && mNewList.get(newPos) instanceof User)) {
            return (((User) mOldList.get(oldPos)).areContentsTheSame((User) mNewList.get(newPos)));
        }

        return false;
    }
}
