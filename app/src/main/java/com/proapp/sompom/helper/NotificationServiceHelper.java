package com.proapp.sompom.helper;

import android.content.Context;
import android.text.TextUtils;

import com.onesignal.OneSignal;
import com.proapp.sompom.R;
import com.proapp.sompom.model.emun.NotificationProviderType;
import com.proapp.sompom.model.request.AddPlayerIdBody;
import com.sompom.pushy.service.PushyService;

import io.reactivex.Observable;
import timber.log.Timber;

public class NotificationServiceHelper {

    public static AddPlayerIdBody getAddPlayerIdRequestBody(String playerId, NotificationProviderType type) {
        AddPlayerIdBody body = new AddPlayerIdBody();
        if (type == NotificationProviderType.ONESIGNAL) {
            body.setOneSignalPlayerId(playerId);
        } else {
            body.setPushyPlayerId(playerId);
        }

        return body;
    }

    public static void loadPlayerId(Context context, NotificationServiceHelperListener listener) {
        NotificationProviderType type = NotificationProviderType.fromValue(context.getString(R.string.notification_provider));
        if (type == NotificationProviderType.ONESIGNAL) {
            if (listener != null) {
                listener.onLoadPlayerIdFinished(getPlayerIdFromOneSignal(), NotificationProviderType.ONESIGNAL);
                listener.onLoadPlayerIdFinishedInBackground(getPlayerIdFromOneSignal(), NotificationProviderType.ONESIGNAL);
            }
        } else {
            loadPlayerIdFromPushy(context, listener);
        }
    }

    public static Observable<PlayerIdData> loadPlayerId(Context context) {
        Timber.i("loadPlayerId");
        return Observable.create(emitter -> loadPlayerId(context, new NotificationServiceHelperListener() {
            @Override
            public void onLoadPlayerIdFinished(String playerId, NotificationProviderType type) {

            }

            @Override
            public void onLoadPlayerIdFinishedInBackground(String playerId, NotificationProviderType type) {
                emitter.onNext(new PlayerIdData(type, playerId));
                emitter.onComplete();
            }
        }));
    }

    public static boolean isPushyProviderEnabled(Context context) {
        return NotificationProviderType.fromValue(context.getString(R.string.notification_provider)) == NotificationProviderType.PUSHY;
    }

    private static String getPlayerIdFromOneSignal() {
        String playerId = null;
        if (OneSignal.getDeviceState() != null) {
            playerId = OneSignal.getDeviceState().getUserId();
        }

        Timber.i("OneSignal.getDeviceState(): " + OneSignal.getDeviceState() + ", playerId: " + playerId);

        return playerId;
    }

    private static void loadPlayerIdFromPushy(Context context, NotificationServiceHelperListener listener) {
        String registeredDeviceId = PushyService.getRegisteredDeviceId(context);
        if (!TextUtils.isEmpty(registeredDeviceId)) {
            if (listener != null) {
                listener.onLoadPlayerIdFinished(registeredDeviceId, NotificationProviderType.PUSHY);
                listener.onLoadPlayerIdFinishedInBackground(registeredDeviceId, NotificationProviderType.PUSHY);
            }
        } else {
            new PushyService(context, new PushyService.PushyServiceListener() {
                @Override
                public void onRegisterFinished(String token, Exception error) {
                    if (listener != null) {
                        listener.onLoadPlayerIdFinished(token, NotificationProviderType.PUSHY);
                    }
                    if (error != null) {
                        SentryHelper.logSentryError(error);
                    }
                }

                @Override
                public void onRegisterFinishedInBackground(String token, Exception error) {
                    if (listener != null) {
                        listener.onLoadPlayerIdFinishedInBackground(token, NotificationProviderType.PUSHY);
                    }
                    if (error != null) {
                        SentryHelper.logSentryError(error);
                    }

                }
            }).checkToRegister();
        }
    }

    public interface NotificationServiceHelperListener {
        void onLoadPlayerIdFinished(String playerId, NotificationProviderType type);

        default void onLoadPlayerIdFinishedInBackground(String playerId, NotificationProviderType type) {
        }
    }

    public static class PlayerIdData {

        private NotificationProviderType mType;
        private String mPlayerId;

        public PlayerIdData(NotificationProviderType type, String playerId) {
            mType = type;
            mPlayerId = playerId;
        }

        public NotificationProviderType getType() {
            return mType;
        }

        public String getPlayerId() {
            return mPlayerId;
        }
    }
}
