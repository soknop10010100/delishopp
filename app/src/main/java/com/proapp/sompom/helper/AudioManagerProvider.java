package com.proapp.sompom.helper;

import android.content.Context;
import android.media.AudioManager;

public class AudioManagerProvider {

    private static AudioManagerProvider sAudioManagerProvider;
    private AudioManager mAudioManager;

    public static AudioManagerProvider getInstance(Context context) {
        if (sAudioManagerProvider == null) {
            sAudioManagerProvider = new AudioManagerProvider(context);
        }

        return sAudioManagerProvider;
    }

    private AudioManagerProvider(Context context) {
        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
    }

    public AudioManager getAudioManager() {
        return mAudioManager;
    }
}
