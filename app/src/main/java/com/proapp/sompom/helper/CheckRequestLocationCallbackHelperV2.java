package com.proapp.sompom.helper;

import android.app.Activity;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import com.proapp.sompom.model.Locations;

import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 02/12/2021.
 */

public class CheckRequestLocationCallbackHelperV2 implements SingleRequestLocation.Callback {
    private final Activity mContext;
    private final Callback mCallback;

    public CheckRequestLocationCallbackHelperV2(Activity activity, Callback callback) {
        mContext = activity;
        mCallback = callback;
    }

    @Override
    public void onComplete(Locations location, boolean isNeverAskAgain, boolean isPermissionEnabled, boolean isLocationEnabled) {
        Timber.i("onComplete: isNeverAskAgain" + isNeverAskAgain +
                ", isPermissionEnabled: " + isPermissionEnabled +
                ", isLocationEnabled: " + isLocationEnabled);
        if (isNeverAskAgain) {
            CheckRequestLocationCallbackHelper.showAskingToEnableLocationAccessFromAppSettingPopup((AppCompatActivity) mContext, view -> {
                if (mCallback != null) {
                    mCallback.onNeverAskAgainDialogClosed();
                }
            });
        } else {
            mCallback.onComplete(location, isPermissionEnabled, isLocationEnabled);
        }
    }

    private String getString(@StringRes int id) {
        return mContext.getString(id);
    }

    public interface Callback {
        void onComplete(Locations locations, boolean isLocationPermissionEnabled, boolean isLocationEnabled);

        void onNeverAskAgainDialogClosed();
    }
}
