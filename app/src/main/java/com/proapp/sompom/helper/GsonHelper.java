package com.proapp.sompom.helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.proapp.sompom.adapter.GSonGMTDateAdapter;
import com.proapp.sompom.adapter.newadapter.GSonReferenceMessageAdapter;
import com.proapp.sompom.adapter.newadapter.GSonUserAdapter;
import com.proapp.sompom.model.result.ReferencedChat;
import com.proapp.sompom.model.result.User;

import java.util.Date;

public class GsonHelper {

    public static Gson getGsonForAPICommunication() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(User.class, new GSonUserAdapter());
        gsonBuilder.registerTypeAdapter(ReferencedChat.class, new GSonReferenceMessageAdapter());
        return gsonBuilder.create();
    }

    public static Gson getGsonForSocketCommunication() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(User.class, new GSonUserAdapter());
        gsonBuilder.registerTypeAdapter(Date.class, new GSonGMTDateAdapter());
        gsonBuilder.registerTypeAdapter(ReferencedChat.class, new GSonReferenceMessageAdapter());
        return gsonBuilder.create();
    }

    public static Gson getSupportedGSonGMTDate() {
        //Will always convert Date to zero time zone format: "2021-10-13T10:50:28.640Z"
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new GSonGMTDateAdapter());
        return gsonBuilder.create();
    }
}
