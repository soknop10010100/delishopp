package com.proapp.sompom.helper;

import android.content.Context;
import android.text.TextUtils;

import androidx.recyclerview.widget.DiffUtil;

import com.proapp.sompom.listener.ConversationDataAdaptive;
import com.proapp.sompom.model.result.Conversation;

import java.util.List;

public class ConversationDiffCallback extends DiffUtil.Callback {

    private final List<ConversationDataAdaptive> mOldList;
    private final List<ConversationDataAdaptive> mNewList;
    private Context mContext;

    public ConversationDiffCallback(Context context,
                                    List<ConversationDataAdaptive> oldBaseChatModelList,
                                    List<ConversationDataAdaptive> newBaseChatModelList) {
        this.mContext = context;
        this.mOldList = oldBaseChatModelList;
        this.mNewList = newBaseChatModelList;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldPos, int newPos) {
        if (mOldList.get(oldPos) instanceof Conversation && ((Conversation) mOldList.get(oldPos)).isHeaderSection() &&
                mNewList.get(newPos) instanceof Conversation && ((Conversation) mNewList.get(newPos)).isHeaderSection()) {
            return true;
        }

        return !TextUtils.isEmpty(mOldList.get(oldPos).getId()) &&
                !TextUtils.isEmpty(mNewList.get(newPos).getId()) &&
                mOldList.get(oldPos).getId().matches(mNewList.get(newPos).getId());
    }

    @Override
    public boolean areContentsTheSame(int oldPos, int newPos) {
        if (mOldList.get(oldPos) instanceof Conversation && mNewList.get(newPos) instanceof Conversation) {
            return ((Conversation) mOldList.get(oldPos)).hasSameContent(mContext, (Conversation) mNewList.get(newPos));
        }

        return false;
    }
}
