package com.proapp.sompom.helper.upload;

import android.content.Context;
import android.text.TextUtils;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.google.gson.Gson;
import com.proapp.sompom.helper.SentryHelper;

import java.util.Map;

import timber.log.Timber;

/**
 * Created by He Rotha on 6/20/18.
 */
public abstract class AbsMyUploader extends SharedUploader implements UploadCallback {
    UploadListener mUploadListener;

    public AbsMyUploader(Context context) {
        super(context);
    }

    public void setListener(UploadListener listener) {
        mUploadListener = listener;
    }

    public abstract String doUpload();

    public void cancelUpload(String requestId) {
        if (!TextUtils.isEmpty(requestId)) {
            MediaManager.get().cancelRequest(requestId);
        }
    }

    @Override
    public void onStart(String requestId) {
        Timber.v("onServiceStart: " + requestId + "  " + MediaManager.get().hashCode());
    }

    @Override
    public void onProgress(String requestId, long bytes, long totalBytes) {

        long percent = bytes * 100 / totalBytes;
        Timber.v("onProgress: " + requestId + "  " + percent);
        if (percent % 5 == 0) {
            mUploadListener.onUploadProgressing(requestId, (int) percent, bytes, totalBytes);
        }
    }

    @Override
    public void onError(String requestId, ErrorInfo error) {
        Timber.e("onError: " + requestId + ", error code: " + error.getCode() + ", error: " + error.getDescription());
        if (error.getCode() != ErrorInfo.REQUEST_CANCELLED) {
            mUploadListener.onUploadFail(requestId, new Exception(error.getDescription()));
        } else {
            mUploadListener.onCancel(requestId);
        }
    }

    @Override
    public void onReschedule(String requestId, ErrorInfo error) {
        MediaManager.get().cancelRequest(requestId);
        mUploadListener.onUploadFail(requestId, new Exception(error.getDescription()));
        Timber.e("onReschedule: " + requestId + ", error:" + error);
    }

    protected void handleOnSuccess(String requestId, Map resultData) {
        try {
            Timber.i("onSuccess: %s", new Gson().toJson(resultData));
            if (TransformationProvider.isGifFormat(resultData)) {
                String mp4UrlFromGifFile = TransformationProvider.getMP4UrlFromGifFile(resultData);
                String gifMP4ThumbnailUrl = TransformationProvider.getGifMP4ThumbnailUrl(resultData);
                Timber.i("mp4UrlFromGifFile: " + mp4UrlFromGifFile + ", gifMP4ThumbnailUrl: " + gifMP4ThumbnailUrl);
                mUploadListener.onUploadSucceeded(requestId, mp4UrlFromGifFile, gifMP4ThumbnailUrl);
            } else {
                if (TransformationProvider.shouldOptimizeResource(resultData)) {
                    String thumbnail = null;
                    String mainOptimizedUrl = TransformationProvider.getDefaultOptimizeTransformation(resultData);
                    if (TransformationProvider.shouldGenerateThumbnail(resultData)) {
                        thumbnail = TransformationProvider.getThumbnailOptimizeTransformation(resultData);
                    }
                    Timber.i("mainOptimizedUrl: " + mainOptimizedUrl + ",\nthumbnailOptimizedUrl: " + thumbnail);
                    mUploadListener.onUploadSucceeded(requestId, mainOptimizedUrl, thumbnail);
                } else {
                    String url = TransformationProvider.getDefaultUrl(resultData);
                    Timber.i("Original uploaded url: " + url);
                    mUploadListener.onUploadSucceeded(requestId, url, null);
                }
            }
        } catch (Exception ex) {
            mUploadListener.onUploadFail(requestId, ex);
            SentryHelper.logSentryError(ex);
        }
    }
}
