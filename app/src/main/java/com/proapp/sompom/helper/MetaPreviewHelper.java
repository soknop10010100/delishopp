package com.proapp.sompom.helper;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.proapp.sompom.model.BasePreviewModel;
import com.proapp.sompom.model.result.LinkPreviewModel;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 4/10/2020.
 */
public class MetaPreviewHelper {

    private MetaPreviewHelper() {
    }

    public static boolean shouldDisplayPreviewByType(BasePreviewModel.PreviewType previewType,
                                                     JsonElement metaPreviewList) {
        /*
        Meta preview default value is empty is not empty array. So we have to make some condition check.
         */
        if (metaPreviewList instanceof JsonPrimitive) {
            return false;
        } else if (metaPreviewList instanceof JsonArray) {
            return shouldDisplayMetaPreviewItem(previewType,
                    getPreviewByType(previewType, metaPreviewList.getAsJsonArray()));
        } else {
            return false;
        }
    }

    private static boolean shouldDisplayMetaPreviewItem(BasePreviewModel.PreviewType previewType,
                                                        JsonObject previewItem) {
        if (previewItem == null) {
            return false;
        }

        if (previewType == BasePreviewModel.PreviewType.LINK) {
            if (previewItem.has(BasePreviewModel.SHOULD_PREVIEW)) {
                return previewItem.get(BasePreviewModel.SHOULD_PREVIEW).getAsBoolean();
            } else {
                /*
                  To maintain the previous link preview state of old version of application that is not yet implemented
                  this feature.
                 */
                return true;
            }
        }

        return previewItem.has(BasePreviewModel.SHOULD_PREVIEW) &&
                previewItem.get(BasePreviewModel.SHOULD_PREVIEW).getAsBoolean();
    }

    private static JsonObject getPreviewByType(BasePreviewModel.PreviewType previewType,
                                               JsonElement metaPreviewList) {
        if (metaPreviewList instanceof JsonArray) {
            for (JsonElement jsonElement : metaPreviewList.getAsJsonArray()) {
                if (isMetaPreviewItemMatchType(jsonElement.getAsJsonObject(), previewType)) {
                    return jsonElement.getAsJsonObject();
                }
            }
        }

        return null;
    }

    public static JsonElement addMetaPreviewItemState(JsonElement metaPreviewList,
                                                      JsonElement previewItem) {
        JsonElement updateMetaPreview = metaPreviewList;
        if (metaPreviewList instanceof JsonArray) {
            ((JsonArray) metaPreviewList).add(previewItem);
        } else {
            JsonArray jsonElements = new JsonArray();
            jsonElements.add(previewItem);
            updateMetaPreview = jsonElements;
        }

        return updateMetaPreview;
    }

    public static JsonElement addOrUpdateLinkMetaPreviewItemState(JsonElement metaPreviewList,
                                                                  LinkPreviewModel previewItem) {
        Timber.i("addOrUpdateLinkMetaPreviewItemState: getShouldPreview: " + previewItem.getShouldPreview());
        removeMetaPreviewStateItem(metaPreviewList,
                BasePreviewModel.PreviewType.LINK,
                BasePreviewModel.PreviewType.FILE);
        Gson gson = new Gson();
        JsonObject jsonObject = gson.fromJson(gson.toJson(previewItem),
                JsonObject.class);
        Timber.i("addOrUpdateLinkMetaPreviewItemState: LinkPreviewModel Gson: " + jsonObject.toString());
        return addMetaPreviewItemState(metaPreviewList, jsonObject);
    }

    public static JsonElement addCommonMetaPreviewItemState(JsonElement metaPreviewList,
                                                            BasePreviewModel.PreviewType previewType,
                                                            boolean shouldDisplay) {
        removeMetaPreviewStateItem(metaPreviewList, previewType);
        BasePreviewModel basePreviewModel = new BasePreviewModel(previewType.getType(),
                shouldDisplay);
        Gson gson = new Gson();
        return addMetaPreviewItemState(metaPreviewList, gson.fromJson(gson.toJson(basePreviewModel),
                JsonObject.class));
    }

    public static JsonElement addOrUpdateMapMetaPreviewState(JsonElement metaPreviewList,
                                                             boolean shouldDisplay) {
        JsonElement updateMetaPreview;
        removeMetaPreviewStateItem(metaPreviewList, BasePreviewModel.PreviewType.MAP);
        updateMetaPreview = addCommonMetaPreviewItemState(metaPreviewList,
                BasePreviewModel.PreviewType.MAP,
                shouldDisplay);

        return updateMetaPreview;
    }

    public static JsonElement updateDisplayMetaPreviewState(JsonElement metaPreviewList,
                                                            BasePreviewModel.PreviewType previewType,
                                                            boolean shouldDisplay) {
        if (metaPreviewList instanceof JsonArray) {
            for (JsonElement jsonElement : ((JsonArray) metaPreviewList)) {
                if (isMetaPreviewItemMatchType(jsonElement.getAsJsonObject(), previewType)) {
                    //Update should display property status
                    jsonElement.getAsJsonObject().remove(BasePreviewModel.TYPE);
                    jsonElement.getAsJsonObject().addProperty(BasePreviewModel.TYPE, shouldDisplay);
                }
            }
        }

        return metaPreviewList;
    }

    public static JsonElement removeMetaPreviewStateItem(JsonElement metaPreviewList,
                                                         BasePreviewModel.PreviewType... previewType) {
        if (metaPreviewList instanceof JsonArray) {
            JsonArray asJsonArray = metaPreviewList.getAsJsonArray();
            for (int size = asJsonArray.size() - 1; size >= 0; size--) {
                JsonElement jsonElement = asJsonArray.get(size);
                for (BasePreviewModel.PreviewType type : previewType) {
                    if (isMetaPreviewItemMatchType(jsonElement.getAsJsonObject(), type)) {
                        asJsonArray.remove(size);
                        break;
                    }
                }
            }
        }

        return metaPreviewList;
    }

    private static boolean isMetaPreviewItemMatchType(JsonElement metaPreviewItem,
                                                      BasePreviewModel.PreviewType previewType) {
        if (metaPreviewItem instanceof JsonObject && metaPreviewItem.getAsJsonObject().has(BasePreviewModel.TYPE)) {
            BasePreviewModel.PreviewType type = BasePreviewModel.PreviewType
                    .getFromValue(metaPreviewItem.getAsJsonObject().get(BasePreviewModel.TYPE).getAsString());
            return type == previewType;
        }


        return false;
    }

    public static List<LinkPreviewModel> getMetaLinkPreview(JsonElement metaPreviewList) {
        List<LinkPreviewModel> linkPreviewModels = new ArrayList<>();
        if (metaPreviewList instanceof JsonArray) {
            Gson gson = new Gson();
            for (JsonElement jsonElement : metaPreviewList.getAsJsonArray()) {
                if (isMetaPreviewItemMatchType(jsonElement.getAsJsonObject(),
                        BasePreviewModel.PreviewType.LINK) ||
                        isMetaPreviewItemMatchType(jsonElement.getAsJsonObject(),
                                BasePreviewModel.PreviewType.FILE)) {
                    linkPreviewModels.add(gson.fromJson(jsonElement.toString(), LinkPreviewModel.class));
                }
            }
        }

        return linkPreviewModels;
    }

    public static String getStringBackupOfMetaPreviewList(JsonElement metaPreviewList) {
        if (metaPreviewList instanceof JsonArray) {
            return new Gson().toJson(metaPreviewList, JsonArray.class);
        }

        return "";
    }

    public static JsonElement getMetaPreviewListFromStringBackup(String metaPreviewList) {
        if (!TextUtils.isEmpty(metaPreviewList)) {
            return new Gson().fromJson(metaPreviewList, JsonArray.class);
        } else {
            return new JsonPrimitive("");
        }
    }
}
