package com.proapp.sompom.helper;

import android.content.Context;
import android.text.TextUtils;

import com.flurry.android.FlurryAgent;
import com.proapp.sompom.BuildConfig;

import java.util.HashMap;
import java.util.Map;

public class FlurryHelper {

    public static final String SCREEN_INPUT_EMAIL = "SCREEN_INPUT_EMAIL";
    public static final String SCREEN_LOGIN = "SCREEN_LOGIN";
    public static final String SCREEN_SIGN_UP = "SCREEN_SIGN_UP";
    public static final String SCREEN_FORGOT_PASSWORD = "SCREEN_FORGOT_PASSWORD";
    public static final String SCREEN_RESET_PASSWORD = "SCREEN_RESET_PASSWORD";
    public static final String SCREEN_VERIFY_PHONE_CODE = "SCREEN_VERIFY_PHONE_CODE";
    public static final String SCREEN_SHOP_HOME = "SCREEN_SHOP_HOME";
    public static final String SCREEN_CATEGORY = "SCREEN_CATEGORY";
    public static final String SCREEN_SUPPLIER_DETAIL_EXPRESS = "SCREEN_SUPPLIER_DETAIL_EXPRESS";
    public static final String SCREEN_SEARCH_SUPPLIER_ITEM_EXPRESS = "SCREEN_SEARCH_SUPPLIER_ITEM_EXPRESS";
    public static final String SCREEN_CHAT_SUPPORT = "SCREEN_CHAT_SUPPORT";
    public static final String SCREEN_ORDER_TRACKING = "SCREEN_ORDER_TRACKING";
    public static final String SCREEN_CATEGORY_DETAIL = "SCREEN_CATEGORY_DETAIL";
    public static final String SCREEN_SUB_CATEGORY_DETAIL = "SCREEN_SUB_CATEGORY_DETAIL";
    public static final String SCREEN_BRAND_DETAIL = "SCREEN_BRAND_DETAIL";
    public static final String SCREEN_SEARCH_GENERAL_PRODUCT = "SCREEN_SEARCH_GENERAL_PRODUCT";
    public static final String SCREEN_PRODUCT_DETAIL = "SCREEN_PRODUCT_DETAIL";
    public static final String SCREEN_REVIEW_CHECKOUT = "SCREEN_REVIEW_CHECKOUT";
    public static final String SCREEN_SELECT_DELIVERY_SLOT = "SCREEN_SELECT_DELIVERY_SLOT";
    public static final String SCREEN_PLACE_ORDER = "SCREEN_PLACE_ORDER";
    public static final String SCREEN_DELIVERY_ADDRESS_LIST = "SCREEN_DELIVERY_ADDRESS_LIST";
    public static final String SCREEN_ADD_NEW_DELIVERY_ADDRESS = "SCREEN_ADD_NEW_DELIVERY_ADDRESS";
    public static final String SCREEN_EDIT_DELIVERY_ADDRESS = "SCREEN_EDIT_DELIVERY_ADDRESS";
    public static final String SCREEN_SELECT_DELIVERY_ADDRESS_ON_MAP = "SCREEN_SELECT_DELIVERY_ADDRESS_ON_MAP";
    public static final String SCREEN_NOTIFICATION_LIST = "SCREEN_NOTIFICATION_LIST";
    public static final String SCREEN_WALLET_HISTORY_LIST = "SCREEN_WALLET_HISTORY_LIST";
    public static final String SCREEN_PROFILE_SETTING = "SCREEN_PROFILE_SETTING";
    public static final String SCREEN_EDIT_PROFILE_POPUP = "SCREEN_EDIT_PROFILE_POPUP";
    public static final String SCREEN_CHANGE_PASSWORD_POPUP = "SCREEN_CHANGE_PASSWORD_POPUP";
    public static final String SCREEN_CHANGE_LANGUAGE_POPUP = "SCREEN_CHANGE_LANGUAGE_POPUP";
    public static final String ADD_PRODUCT_TO_CHECKOUT_CARD = "ADD_PRODUCT_TO_CHECKOUT_CARD";
    public static final String UPDATE_PRODUCT_IN_CHECKOUT_CARD = "UPDATE_PRODUCT_IN_CHECKOUT_CARD";
    public static final String REMOVE_PRODUCT_FROM_CHECKOUT_CARD = "REMOVE_PRODUCT_FROM_CHECKOUT_CARD";
    public static final String CLEAR_CHECKOUT_CARD = "CLEAR_CHECKOUT_CARD";
    public static final String APPLY_COUPON = "APPLY_COUPON";
    public static final String APPLY_WALLET = "APPLY_WALLET";
    public static final String PAY_WITH_CASH = "PAY_WITH_CASH";
    public static final String PAY_WITH_ABA_PAY = "PAY_WITH_ABA_PAY";
    public static final String PAY_WITH_CARD = "PAY_WITH_CARD";
    public static final String PAY_WITH_KHQR = "PAY_WITH_KHQR";
    public static final String ORDER_SUCCESS = "ORDER_SUCCESS";
    public static final String ORDER_FAILED = "ORDER_FAILED";

    private static final Map<String, EventMode> sEventMap = new HashMap<>();

    static {
        sEventMap.put(SCREEN_INPUT_EMAIL, new EventMode("SCREEN_INPUT_EMAIL", "SCREEN_INPUT_EMAIL_EXPRESS"));
        sEventMap.put(SCREEN_LOGIN, new EventMode("SCREEN_LOGIN", "SCREEN_LOGIN_EXPRESS"));
        sEventMap.put(SCREEN_SIGN_UP, new EventMode("SCREEN_SIGN_UP", "SCREEN_SIGN_UP_EXPRESS"));
        sEventMap.put(SCREEN_FORGOT_PASSWORD, new EventMode("SCREEN_FORGOT_PASSWORD", "SCREEN_FORGOT_PASSWORD_EXPRESS"));
        sEventMap.put(SCREEN_RESET_PASSWORD, new EventMode("SCREEN_RESET_PASSWORD", "SCREEN_RESET_PASSWORD_EXPRESS"));
        sEventMap.put(SCREEN_VERIFY_PHONE_CODE, new EventMode("SCREEN_VERIFY_PHONE_CODE", "SCREEN_VERIFY_PHONE_CODE_EXPRESS"));
        sEventMap.put(SCREEN_SHOP_HOME, new EventMode("SCREEN_SHOP_HOME", "SCREEN_SHOP_HOME_EXPRESS"));
        sEventMap.put(SCREEN_CATEGORY, new EventMode("SCREEN_CATEGORY", null));
        sEventMap.put(SCREEN_SUPPLIER_DETAIL_EXPRESS, new EventMode(null, "SCREEN_SUPPLIER_DETAIL_EXPRESS"));
        sEventMap.put(SCREEN_SEARCH_SUPPLIER_ITEM_EXPRESS, new EventMode(null, "SCREEN_SEARCH_SUPPLIER_ITEM_EXPRESS"));
        sEventMap.put(SCREEN_CHAT_SUPPORT, new EventMode("SCREEN_CHAT_SUPPORT", "SCREEN_CHAT_SUPPORT_EXPRESS"));
        sEventMap.put(SCREEN_ORDER_TRACKING, new EventMode("SCREEN_ORDER_TRACKING", "SCREEN_ORDER_TRACKING_EXPRESS"));
        sEventMap.put(SCREEN_CATEGORY_DETAIL, new EventMode("SCREEN_CATEGORY_DETAIL", "SCREEN_CATEGORY_DETAIL_EXPRESS"));
        sEventMap.put(SCREEN_SUB_CATEGORY_DETAIL, new EventMode("SCREEN_SUB_CATEGORY_DETAIL", "SCREEN_SUB_CATEGORY_DETAIL_EXPRESS"));
        sEventMap.put(SCREEN_BRAND_DETAIL, new EventMode("SCREEN_BRAND_DETAIL", "SCREEN_BRAND_DETAIL_EXPRESS"));
        sEventMap.put(SCREEN_SEARCH_GENERAL_PRODUCT, new EventMode("SCREEN_SEARCH_GENERAL_PRODUCT", "SCREEN_SEARCH_GENERAL_PRODUCT_EXPRESS"));
        sEventMap.put(SCREEN_PRODUCT_DETAIL, new EventMode("SCREEN_PRODUCT_DETAIL", "SCREEN_PRODUCT_DETAIL_EXPRESS"));
        sEventMap.put(SCREEN_REVIEW_CHECKOUT, new EventMode("SCREEN_REVIEW_CHECKOUT", "SCREEN_REVIEW_CHECKOUT_EXPRESS"));
        sEventMap.put(SCREEN_SELECT_DELIVERY_SLOT, new EventMode("SCREEN_SELECT_DELIVERY_SLOT", "SCREEN_SELECT_DELIVERY_SLOT_EXPRESS"));
        sEventMap.put(SCREEN_PLACE_ORDER, new EventMode("SCREEN_PLACE_ORDER", "SCREEN_PLACE_ORDER_EXPRESS"));
        sEventMap.put(SCREEN_DELIVERY_ADDRESS_LIST, new EventMode("SCREEN_DELIVERY_ADDRESS_LIST", "SCREEN_DELIVERY_ADDRESS_LIST_EXPRESS"));
        sEventMap.put(SCREEN_ADD_NEW_DELIVERY_ADDRESS, new EventMode("SCREEN_ADD_NEW_DELIVERY_ADDRESS", "SCREEN_ADD_NEW_DELIVERY_ADDRESS_EXPRESS"));
        sEventMap.put(SCREEN_EDIT_DELIVERY_ADDRESS, new EventMode("SCREEN_EDIT_DELIVERY_ADDRESS", "SCREEN_EDIT_DELIVERY_ADDRESS_EXPRESS"));
        sEventMap.put(SCREEN_SELECT_DELIVERY_ADDRESS_ON_MAP, new EventMode("SCREEN_SELECT_DELIVERY_ADDRESS_ON_MAP", "SCREEN_SELECT_DELIVERY_ADDRESS_ON_MAP_EXPRESS"));
        sEventMap.put(SCREEN_NOTIFICATION_LIST, new EventMode("SCREEN_NOTIFICATION_LIST", "SCREEN_NOTIFICATION_LIST_EXPRESS"));
        sEventMap.put(SCREEN_PROFILE_SETTING, new EventMode("SCREEN_PROFILE_SETTING", "SCREEN_PROFILE_SETTING_EXPRESS"));
        sEventMap.put(SCREEN_EDIT_PROFILE_POPUP, new EventMode("SCREEN_EDIT_PROFILE_POPUP", "SCREEN_EDIT_PROFILE_POPUP_EXPRESS"));
        sEventMap.put(SCREEN_CHANGE_PASSWORD_POPUP, new EventMode("SCREEN_CHANGE_PASSWORD_POPUP", "SCREEN_CHANGE_PASSWORD_POPUP_EXPRESS"));
        sEventMap.put(SCREEN_CHANGE_LANGUAGE_POPUP, new EventMode("SCREEN_CHANGE_LANGUAGE_POPUP", "SCREEN_CHANGE_LANGUAGE_POPUP_EXPRESS"));
        sEventMap.put(ADD_PRODUCT_TO_CHECKOUT_CARD, new EventMode("ADD_PRODUCT_TO_CHECKOUT_CARD", "ADD_PRODUCT_TO_CHECKOUT_CARD_EXPRESS"));
        sEventMap.put(UPDATE_PRODUCT_IN_CHECKOUT_CARD, new EventMode("UPDATE_PRODUCT_IN_CHECKOUT_CARD", "UPDATE_PRODUCT_IN_CHECKOUT_CARD_EXPRESS"));
        sEventMap.put(REMOVE_PRODUCT_FROM_CHECKOUT_CARD, new EventMode("REMOVE_PRODUCT_FROM_CHECKOUT_CARD", "REMOVE_PRODUCT_FROM_CHECKOUT_CARD_EXPRESS"));
        sEventMap.put(CLEAR_CHECKOUT_CARD, new EventMode("CLEAR_CHECKOUT_CARD", "CLEAR_CHECKOUT_CARD_EXPRESS"));
        sEventMap.put(APPLY_COUPON, new EventMode("APPLY_COUPON", "APPLY_COUPON_EXPRESS"));
        sEventMap.put(APPLY_WALLET, new EventMode("APPLY_WALLET", "APPLY_WALLET_EXPRESS"));
        sEventMap.put(PAY_WITH_CASH, new EventMode("PAY_WITH_CASH", "PAY_WITH_CASH_EXPRESS"));
        sEventMap.put(PAY_WITH_ABA_PAY, new EventMode("PAY_WITH_ABA_PAY", "PAY_WITH_ABA_PAY_EXPRESS"));
        sEventMap.put(PAY_WITH_CARD, new EventMode("PAY_WITH_CARD", "PAY_WITH_CARD_EXPRESS"));
        sEventMap.put(PAY_WITH_KHQR, new EventMode("PAY_WITH_KHQR", "PAY_WITH_KHQR_EXPRESS"));
        sEventMap.put(ORDER_SUCCESS, new EventMode("ORDER_SUCCESS", "ORDER_SUCCESS_EXPRESS"));
        sEventMap.put(ORDER_FAILED, new EventMode("ORDER_FAILED", "ORDER_FAILED_EXPRESS"));
        sEventMap.put(SCREEN_WALLET_HISTORY_LIST, new EventMode("SCREEN_WALLET_HISTORY_LIST", "SCREEN_WALLET_HISTORY_LIST_EXPRESS"));
    }

    public static void logEvent(Context context, String evenKey) {
        //Will log only event in release build.
        if (TextUtils.equals(BuildConfig.BUILD_TYPE, "release") && !TextUtils.isEmpty(evenKey)) {
            String eventName = getEventNameFromEventKey(context, evenKey);
            if (!TextUtils.isEmpty(eventName)) {
                FlurryAgent.logEvent(eventName);
            }
        }
    }

    private static String getEventNameFromEventKey(Context context, String evenKey) {
        EventMode eventMode = sEventMap.get(evenKey);
        if (eventMode != null) {
            boolean isExpressMode = ApplicationHelper.isInExpressMode(context);
            if (isExpressMode) {
                return eventMode.getExpressModeEventName();
            } else {
                return eventMode.getNormalModeEventName();
            }
        }

        return "";
    }

    public static class EventMode {

        private String mNormalModeEventName;
        private String mExpressModeEventName;

        public EventMode(String normalModeEventName, String expressModeEventName) {
            mNormalModeEventName = normalModeEventName;
            mExpressModeEventName = expressModeEventName;
        }

        public String getExpressModeEventName() {
            return mExpressModeEventName;
        }

        public String getNormalModeEventName() {
            return mNormalModeEventName;
        }
    }
}
