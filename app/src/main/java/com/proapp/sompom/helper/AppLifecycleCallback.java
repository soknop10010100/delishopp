package com.proapp.sompom.helper;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.proapp.sompom.chat.call.CallingService;
import com.proapp.sompom.newui.CallingActivity;
import com.proapp.sompom.newui.HomeActivity;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;


/**
 * Created by He Rotha on 9/27/17.
 */

public class AppLifecycleCallback implements Application.ActivityLifecycleCallbacks {

    private int mNumRunningActivities = 0;
    private final List<String> mRunningActivityList = new ArrayList<>();
    private int mHomeActivityCount;

    @Override
    public void onActivityStarted(Activity activity) {
        mNumRunningActivities++;
    }

    @Override
    public void onActivityStopped(Activity activity) {
        mNumRunningActivities--;
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        if (activity instanceof HomeActivity) {
            mHomeActivityCount--;
        }

        mRunningActivityList.remove(activity.getClass().getSimpleName());
        Timber.i("Running activity size: " + mRunningActivityList.size());
        Timber.i("onActivityDestroyed " + activity + " " + mNumRunningActivities);
        if (mNumRunningActivities == 0) {
            // if the app is in background
            //(Calling service) stop receive call on foreground but not the notification
            // let the FirebaseMsgService handle the notification in the background again
            if (activity instanceof CallingActivity) {
                Timber.i("onActivityDestroyed 1" + activity + " " + mNumRunningActivities);
                CallingService.CallServiceBinder binder = ((CallingActivity) activity).getBinder();
                if (binder != null && !binder.isCallInProgressNow()) {
                    binder.stop();
                }
            }
        }
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        if (activity instanceof HomeActivity) {
            mHomeActivityCount++;
        }
        if (!mRunningActivityList.contains(activity.getClass().getSimpleName())) {
            mRunningActivityList.add(activity.getClass().getSimpleName());
        }
        Timber.i("Running activity size: " + mRunningActivityList.size());
    }

    public boolean isActivityRunning(String activitySimpleName) {
        return mRunningActivityList.contains(activitySimpleName);
    }

    public boolean isHomeActivityRunning() {
        return mHomeActivityCount > 0;
    }

    public int getHomeActivityCount() {
        return mHomeActivityCount;
    }

    public int getNumRunningActivityCount() {
        return mNumRunningActivities;
    }
}
