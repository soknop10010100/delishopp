package com.proapp.sompom.helper;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.licence.db.LicenseSynchronizationDb;
import com.proapp.sompom.licence.model.SynchroniseData;
import com.proapp.sompom.model.ConciergeCartModel;
import com.proapp.sompom.model.concierge.ConciergeBasket;
import com.proapp.sompom.model.concierge.ConciergeBasketMenuItem;
import com.proapp.sompom.model.concierge.ConciergeRequestOrderMenuItem;
import com.proapp.sompom.model.emun.NotificationProviderType;
import com.proapp.sompom.model.request.RequestGuestUserBody;
import com.proapp.sompom.model.result.AppFeature;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.model.result.UserBadge;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseHandleErrorFunc;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.HTTPResponse;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.resourcemanager.helper.LocaleManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 5/7/2020.
 */
public class PreAPIRequestHelper {

    private PreAPIRequestHelper() {
    }

    public static <T> Function<Response<T>, ObservableSource<? extends Response<T>>> getAppSetting(Context context,
                                                                                                   ApiService apiService) {
        return userResponse -> apiService.getAppSetting()
                .onErrorResumeNext(throwable -> {
                    return Observable.just(Response.success(new ArrayList<>()));
                }).concatMap(listResponse -> {
                    if (listResponse.body() != null && !listResponse.body().isEmpty()) {
                        Timber.i("getAppSetting success");
                        SharedPrefUtils.setAppSetting(context, listResponse.body().get(0));
                    }
                    return Observable.just(userResponse);
                });
    }

    public static <T> Function<Response<T>, ObservableSource<? extends Response<T>>> getUserBadge(Context context, ApiService apiService) {
        return userResponse -> apiService.getUserBadge()
                .onErrorResumeNext((Throwable throwable) ->
                        Observable.just(Response.success(new UserBadge()))).concatMap((Response<UserBadge> response) -> {
                    if (response.body() != null) {
                        Timber.i("getUserBadge success");
                        UserHelper.saveUserBadge(context, response.body());
                    }
                    return Observable.just(userResponse);
                });
    }

    public static <T> Function<Response<T>, ObservableSource<? extends Response<T>>> getClientFeature(Context context, ApiService apiService) {
        return userResponse -> apiService.getClientFeatureData()
                .concatMap(new ResponseHandleErrorFunc<>(context, false))
                .concatMap(listResponse -> {
                    Timber.i("getClientFeature: isSuccessful: " + listResponse.isSuccessful());
                    if (listResponse.isSuccessful() && listResponse.body() != null && !listResponse.body().isEmpty()) {
                        SynchroniseData synchroniseData = listResponse.body().get(0);
                        LicenseSynchronizationDb.saveSynchroniseData(context, synchroniseData);
                        if (synchroniseData.getMessage() != null) {
                            SharedPrefUtils.setIsEnableEncryption(context, synchroniseData.getMessage().isEnableEncryption());
                        }
                    }
                    return Observable.just(userResponse);
                });
    }

    public static <T> Function<Response<T>, ObservableSource<? extends Response<T>>> getAppFeature(Context context, ApiService apiService) {
        return userResponse -> apiService.getAppFeature(ApplicationHelper.getRequestAPIMode(context),
                        ApplicationHelper.getUserId(context))
                .concatMap(new ResponseHandleErrorFunc<>(context, false))
                .concatMap(listResponse -> {
                    Timber.i("getAppFeature: isSuccessful " + listResponse.isSuccessful());
                    if (listResponse.isSuccessful() && listResponse.body() != null) {
                        SharedPrefUtils.setAppFeature(context, listResponse.body());
                    }
                    return Observable.just(userResponse);
                });
    }

    public static <T> Function<Response<T>, ObservableSource<? extends Response<T>>> getShopSetting(Context context, ApiService apiService) {
        return userResponse -> apiService.getShopSetting(ApplicationHelper.getUserId(context))
                .concatMap(new ResponseHandleErrorFunc<>(context, false))
                .concatMap(shopSettingResponse -> {
                    Timber.i("getShopSetting: isSuccessful " + shopSettingResponse.isSuccessful());
                    if (shopSettingResponse.isSuccessful() && shopSettingResponse.body() != null) {
                        ConciergeHelper.setConciergeShopSetting(context, shopSettingResponse.body());
                    }
                    return Observable.just(userResponse);
                });
    }

    public static Observable<Response<AppFeature>> requestAppFeature(Context context, ApiService apiService) {
        return apiService.getAppFeature(ApplicationHelper.getRequestAPIMode(context),
                ApplicationHelper.getUserId(context)).concatMap(appFeatureResponse -> {
            Timber.i("getAppFeature: isSuccessful " + appFeatureResponse.isSuccessful());
            if (appFeatureResponse.isSuccessful() && appFeatureResponse.body() != null) {
                SharedPrefUtils.setAppFeature(context, appFeatureResponse.body());
            }
            return Observable.just(appFeatureResponse);
        });
    }

    public static Observable<Response<Object>> requestGuestUser(Context context, ApiService publicApiService) {
        return NotificationServiceHelper.loadPlayerId(context).concatMap(playerIdData -> {
            RequestGuestUserBody body = new RequestGuestUserBody();
            if (!TextUtils.isEmpty(playerIdData.getPlayerId())) {
                if (playerIdData.getType() == NotificationProviderType.PUSHY) {
                    body.setPushyPlayerId(playerIdData.getPlayerId());
                } else {
                    body.setPlayerId(playerIdData.getPlayerId());
                }
            }
            return publicApiService.getGuestUser(body).concatMap(guestUserAuthResponseResponse -> {
                if (guestUserAuthResponseResponse.isSuccessful()) {
                    User guest = guestUserAuthResponseResponse.body().getUser();
                    guest.setGuestUser(true);
                    guest.setAccessToken(guestUserAuthResponseResponse.body().getAccessToken());
                    SharedPrefUtils.setUserValue(guest, context);
                    return Observable.just(Response.success(new Object()));
                } else {
                    return Observable.error(new ErrorThrowable(ErrorThrowable.GENERAL_ERROR,
                            context.getString(R.string.error_general_description)));
                }
            });
        });
    }

    public static Observable<Response<ConciergeBasket>> requestUserBasket(Context context,
                                                                          ApiService privatAPIService,
                                                                          boolean isIgnoreWhenError,
                                                                          boolean shouldBroadcastRefreshBasketEvent) {
        if (ApplicationHelper.isInVisitorMode(context)) {
            /*
                We Will not make request basket if user is not yet logged in. We just prevent that
                the request is success and return with empty basket.
             */
            return Observable.just(Response.success(new ConciergeBasket()));
        }

        Observable<Response<ConciergeBasket>> responseObservable = privatAPIService.getBasket(ApplicationHelper.getRequestAPIMode(context),
                        LocaleManager.getAppLanguage(context))
                .concatMap(basketResponse -> {
                    if (basketResponse.isSuccessful() &&
                            basketResponse.body() != null &&
                            (basketResponse.body().getCode() == HTTPResponse.NOT_FOUND ||
                                    basketResponse.body().isBasketEmpty())) {
                        /*
                            Will clear current local basket if there is any because the basket is empty on the
                            server.
                         */
                        Timber.i("Will clear card data.");
                        return ConciergeCartHelper.clearCart(context).concatMap(o -> {
                            if (shouldBroadcastRefreshBasketEvent) {
                                ConciergeHelper.broadcastRefreshLocalBasketEvent(context);
                            }
                            return Observable.just(basketResponse);
                        });
                    } else {
                        if (basketResponse.isSuccessful() &&
                                basketResponse.body() != null &&
                                !basketResponse.body().isBasketEmpty()) {
                            Timber.i("Will update local basket with basket response of server: " + new Gson().toJson(basketResponse.body()));
                            return ConciergeCartHelper.getCart().concatMap(localBasket -> {
                                ConciergeBasket basketData = basketResponse.body();
                                ConciergeCartModel conciergeCartModel = ConciergeHelper.transformRemoteBasketIntoLocalBasket(basketData);
                                return ConciergeCartHelper.saveBasket(context, conciergeCartModel).concatMap(conciergeCartModel1 -> {
                                    if (shouldBroadcastRefreshBasketEvent) {
                                        ConciergeHelper.broadcastRefreshLocalBasketEvent(context);
                                    }
                                    return Observable.just(Response.success(basketData));
                                });
                            });
                        } else {
                            return Observable.just(basketResponse);
                        }
                    }
                });

        if (!isIgnoreWhenError) {
            return responseObservable;
        } else {
            return responseObservable.onErrorResumeNext(throwable -> {
                    /*
                        We will ignore any error when request basket data and the local basket data
                        still being kept.
                     */
                Timber.e("onErrorResumeNext of requestUserBasket");
                return Observable.just(Response.success(new ConciergeBasket()));
            });
        }
    }

    public static Observable<Response<ConciergeBasketMenuItem>> requestUpdateOrRemoveItemToBasket(Context context,
                                                                                                  ApiService privatAPIService,
                                                                                                  String itemId,
                                                                                                  ConciergeRequestOrderMenuItem body) {
        return privatAPIService.updateOrRemoveItemInBasket(itemId,
                body,
                ApplicationHelper.getRequestAPIMode(context),
                LocaleManager.getAppLanguage(context));
    }

    public static String getArrayStringIdQuery(List<String> ids) {
        if (ids == null || ids.isEmpty()) {
            return null;
        }

        StringBuilder filter = new StringBuilder();
        for (String id : ids) {
            if (!TextUtils.isEmpty(filter)) {
                filter.append(",");
            }
            filter.append(id);
        }


        return "[" + filter + "]";
    }

    public static <T> void requestMandatoryAPIs(Context context,
                                                ApiService apiService,
                                                ApiService privateAPIService,
                                                Observable<Response<T>> ob) {
        Observable.defer(() -> {
                    return ob;
                })
                .concatMap(userResponse -> {
                    List<Function> ff = new ArrayList<>();
                    ff.add(PreAPIRequestHelper.getClientFeature(context, apiService));
                    ff.add(PreAPIRequestHelper.getShopSetting(context, apiService));
                    ff.add(PreAPIRequestHelper.getAppFeature(context, apiService));
                    ff.add(PreAPIRequestHelper.getAppSetting(context, apiService));
                    ff.add(PreAPIRequestHelper.getUserBadge(context, privateAPIService));
                    return Observable.just(ff);
                })
                .concatMapIterable(functions -> functions)
                .concatMapEager(function -> {
                    return ob.concatMap(function).onErrorResumeNext(ob);
                })
                .toList()
                .toObservable()
                .subscribeOn(Schedulers.io())
                .subscribe(userResponse -> {
                });
    }
}
