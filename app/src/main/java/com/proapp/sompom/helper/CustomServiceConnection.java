package com.proapp.sompom.helper;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

public class CustomServiceConnection implements ServiceConnection {

    private boolean mConnected;


    public boolean isConnected() {
        return mConnected;
    }

    @Override
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        mConnected = true;
        onCustomServiceConnected(componentName, iBinder);
    }

    @Override
    public final void onServiceDisconnected(ComponentName componentName) {
        mConnected = false;
        onCustomServiceDisconnected(componentName);
    }

    public void onCustomServiceConnected(ComponentName componentName, IBinder iBinder) {
        //Client must implement this method
    }

    public void onCustomServiceDisconnected(ComponentName componentName) {
        //Client must implement this method
    }
}
