package com.proapp.sompom.helper;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.proapp.sompom.R;

import java.util.Map;

/**
 * Created by Chhom Veasna on 12/18/2019.
 */
public class MappingHelper {

    private MappingHelper() {

    }

    public static String getStringFromJson(JsonObject jsonObject, String key) {
        if (jsonObject.has(key)) {
            return jsonObject.get(key).toString();
        }

        return null;
    }

    public static String getStringPropertyFromJsonElement(Context context, JsonElement element, String key) {
        if (element != null) {
            if (element.isJsonPrimitive()) {
                return element.getAsString();
            } else {
                return getStringFromJson(element.getAsJsonObject(), key);
            }
        }

        return context.getString(R.string.profile_user_default_role);
    }

    public static JsonObject removeJavaModelFromPostBody(JsonObject jsonObject, String... removeKeys) {
        if (jsonObject != null && removeKeys.length > 0) {
            for (Map.Entry<String, JsonElement> jsonElementEntry : jsonObject.entrySet()) {
                for (String removeKey : removeKeys) {
                    if (TextUtils.equals(removeKey, jsonElementEntry.getKey())) {
                        jsonObject.remove(jsonElementEntry.getKey());
                    }
                }
            }
        }

        return jsonObject;
    }
}
