package com.proapp.sompom.helper;

import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.Transformation;

import androidx.interpolator.view.animation.FastOutSlowInInterpolator;

import com.proapp.sompom.R;

/**
 * Created by nuonveyo on 12/5/17.
 */

public final class AnimationHelper {
    private static final Interpolator INTERPOLATOR = new FastOutSlowInInterpolator();

    private AnimationHelper() {

    }

    public static void animateBounce(View view, AnimationHelperListener listener) {
        if (view == null) {
            //Consider animation ended automatically
            if (listener != null) {
                listener.onAnimationEnded();
            }
            return;
        }

        Animation animation = AnimationUtils.loadAnimation(view.getContext(), R.anim.bounce);
        animation.setDuration(600L);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (listener != null) {
                    listener.onAnimationEnded();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.25, 10.0);
        animation.setInterpolator(interpolator);
        view.startAnimation(animation);
    }

    public static void animateBounceFollowButton(View view, long duration, float amplitude, float frequency) {
        Animation animation = AnimationUtils.loadAnimation(view.getContext(), R.anim.bounce);
        animation.setDuration(duration);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        MyBounceInterpolator interpolator = new MyBounceInterpolator(amplitude, frequency);
        animation.setInterpolator(interpolator);

        view.startAnimation(animation);
    }

    public static void slideDown(View view) {
        Animation animation = AnimationUtils.loadAnimation(view.getContext(), R.anim.bottom_down);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
    }

    public static void slideUp(View view) {
        Animation animation = AnimationUtils.loadAnimation(view.getContext(), R.anim.bottom_up);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
    }

    /**
     * Show the quick return view.
     * <p>
     * Animates showing the view, with the view sliding up from the bottom of the screen.
     * After the view has reappeared, its visibility will change to VISIBLE.
     *
     * @param view The quick return view
     */
    public static void showFabButton(final View view, AnimatorListenerAdapter listenerAdapter) {
        view.animate().cancel();

        view.animate()
                .translationY(0f)
                .setInterpolator(INTERPOLATOR)
                .setDuration(200)
                .setListener(listenerAdapter);
    }

    /**
     * Hide the quick return view.
     * <p>
     * Animates hiding the view, with the view sliding down and out of the screen.
     * After the view has disappeared, its visibility will change to GONE.
     *
     * @param view The quick return view
     */
    public static void hideFabButton(final View view, AnimatorListenerAdapter listenerAdapter) {
        view.animate().cancel();

        view.animate()
                .translationY(view.getHeight())
                .setInterpolator(INTERPOLATOR)
                .setDuration(200)
                .setListener(listenerAdapter);
    }

    public static void translationFromX(final View view, AnimatorListenerAdapter listenerAdapter) {
        view.animate().cancel();

        view.animate()
                .translationX(0f)
                .setInterpolator(INTERPOLATOR)
                .setDuration(100)
                .setListener(listenerAdapter);
    }

    public static void translationToX(final View view, AnimatorListenerAdapter listenerAdapter) {
        view.animate().cancel();

        view.animate()
                .translationX(view.getWidth())
                .setInterpolator(INTERPOLATOR)
                .setDuration(100)
                .setListener(listenerAdapter);
    }

    public static void fadeInOrOut(final View view,
                                   boolean isFadeIn,
                                   int duration,
                                   int startDelay,
                                   Interpolator interpolator,
                                   AnimatorListenerAdapter listener) {
        view.animate().cancel();
        view.animate()
                .setStartDelay(startDelay)
                .alpha(isFadeIn ? 1f : 0f)
                .setInterpolator(interpolator != null ? interpolator : INTERPOLATOR)
                .setDuration(duration)
                .setListener(listener);
    }

    public static void expand(final View v) {
        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public interface AnimationHelperListener {
        void onAnimationEnded();
    }
}
