package com.proapp.sompom.helper;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.BasePreviewModel;
import com.proapp.sompom.model.request.LinkPreviewRequest;
import com.proapp.sompom.model.result.LinkPreviewModel;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.GenerateLinkPreviewUtil;
import com.proapp.sompom.utils.GlideLoadUtil;
import com.proapp.sompom.utils.MediaUtil;

import java.net.ConnectException;
import java.util.Arrays;
import java.util.regex.Pattern;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 12/4/2019.
 */
public class LinkPreviewRetriever {

    public static final String IMAGE_PATTERN = "(.+?)\\.(jpg|png|gif|bmp)$";
    public static final String VIDEO_PATTERN = "(.+?)\\.(mp4|3gp|webm|mkv)$";
    public static final String FILE_PATTERN = "(.*/)*.+\\\\.[a-zA-Z0-9]+$";

    private LinkPreviewRetriever() {
    }

    public static String getPreviewLinkFromPost(Adaptive adaptive) {
        if (adaptive == null) {
            return null;
        } else {
            if (!TextUtils.isEmpty(adaptive.getShareUrl())) {
                return adaptive.getShareUrl();
            } else {
                return GenerateLinkPreviewUtil.getFirstUrlIndex(adaptive.getTitle());
            }
        }
    }

    public static String getLinkPreviewSource(Adaptive adaptive) {
        if (adaptive == null) {
            return null;
        } else {
            if (!TextUtils.isEmpty(adaptive.getShareUrl())) {
                return adaptive.getShareUrl();
            } else {
                return adaptive.getTitle();
            }
        }
    }

    public static boolean isImageType(String preViewUrl) {
        return !TextUtils.isEmpty(preViewUrl) && preViewUrl.matches(IMAGE_PATTERN);
    }

    public static boolean isVideoType(String preViewUrl) {
        return !TextUtils.isEmpty(preViewUrl) && preViewUrl.matches(VIDEO_PATTERN);
    }

    public static boolean isFileType(String preViewUrl) {
        Pattern tagPatter = Pattern.compile(FILE_PATTERN);
        return !TextUtils.isEmpty(preViewUrl) && tagPatter.matcher(preViewUrl).matches();
    }

    public static boolean isFilePreviewLink(String preViewUrl) {
        String extension = getExtensionFromResultUrl(preViewUrl);
        if (!TextUtils.isEmpty(extension) &&
                !TextUtils.equals(extension.toLowerCase(), "com") &&
                !TextUtils.equals(extension.toLowerCase(), "net")) {
            return true;
        }

        return false;
    }

    public static String getExtensionFromResultUrl(String url) {
        return MimeTypeMap.getFileExtensionFromUrl(url);
    }

    public static void preCachePreviewImage(Context context, LinkPreviewModel linkPreviewModel) {
        BasePreviewModel.PreviewType previewType = BasePreviewModel.PreviewType.getFromValue(linkPreviewModel.getType());
        if (previewType == BasePreviewModel.PreviewType.LINK && !TextUtils.isEmpty(linkPreviewModel.getImage())) {
            GlideLoadUtil.preCacheImage(context, linkPreviewModel.getImage(), null);
        }
    }

    public static String getPreviewTitle(LinkPreviewModel previewModel) {
        if (!TextUtils.isEmpty(previewModel.getTitle())) {
            return previewModel.getTitle();
        } else {
            return getHostFromUrl(previewModel.getLink());
        }
    }

    public static String getPreviewDescription(LinkPreviewModel previewModel) {
        if (!TextUtils.isEmpty(previewModel.getDescription())) {
            return previewModel.getDescription();
        } else {
            return previewModel.getLink();
        }
    }

    public static LinkPreviewModel generateDefaultLinkPreview(String url) {
        LinkPreviewModel linkPreviewModel = new LinkPreviewModel();
        String hostFromUrl = getHostFromUrl(url);
        linkPreviewModel.setLink(url);
        linkPreviewModel.setTitle(hostFromUrl);
        linkPreviewModel.setDescription(url);
        linkPreviewModel.setType(BasePreviewModel.PreviewType.LINK.getType());
        linkPreviewModel.setShouldPreview(true);

        return linkPreviewModel;
    }

    public static void injectMissingDefaultLinkPreview(LinkPreviewModel linkPreviewModel) {
        if (linkPreviewModel != null) {
            /*
            Will add default title and description of link preview if it does not yet exist.
             */
            if (TextUtils.isEmpty(linkPreviewModel.getTitle())) {
                linkPreviewModel.setTitle(getHostFromUrl(linkPreviewModel.getLink()));
            }
            if (TextUtils.isEmpty(linkPreviewModel.getDescription())) {
                linkPreviewModel.setDescription(linkPreviewModel.getLink());
            }
        }
    }

    public static Observable<Response<LinkPreviewModel>> getPreviewLink(Context context,
                                                                        ApiService apiService,
                                                                        String url) {
        return apiService.getPreviewLink(new LinkPreviewRequest(url))
                .concatMap((Function<Response<LinkPreviewModel>,
                        ObservableSource<Response<LinkPreviewModel>>>) linkPreviewModelResponse -> {
                    Timber.i("isSuccessful: " + linkPreviewModelResponse.isSuccessful() + ", code: " + linkPreviewModelResponse.code());
                    if (linkPreviewModelResponse.isSuccessful()) {
                        LinkPreviewModel preview = linkPreviewModelResponse.body();
                        if (preview != null) {
                            preview.setShouldPreview(true);
                            if (TextUtils.isEmpty(preview.getTitle()) &&
                                    TextUtils.isEmpty(preview.getDescription()) &&
                                    TextUtils.isEmpty(preview.getImage()) &&
                                    TextUtils.isEmpty(preview.getLogo()) &&
                                    isLinkHasFileExtension(url)) {
                           /*
                            Consider as File preview type and will use link extension to render the
                            preview view.
                            */
                                getFilePreviewDataType(context, url, preview);
                            } else {
                                preview.setType(BasePreviewModel.PreviewType.LINK.getType());
                            }

                            injectMissingDefaultLinkPreview(preview);
                            return Observable.just(linkPreviewModelResponse);
                        }
                    }
                    return Observable.just(Response.success(LinkPreviewRetriever.generateDefaultLinkPreview(url)));
                }).onErrorResumeNext(new Function<Throwable, ObservableSource<? extends Response<LinkPreviewModel>>>() {
                    @Override
                    public ObservableSource<? extends Response<LinkPreviewModel>> apply(@NonNull Throwable throwable) throws Exception {
                        /*
                        Will ignore the default link preview generation the error was due to connection such as
                        no network connection.
                         */
                        if (throwable instanceof ConnectException) {
                            return Observable.error(throwable);
                        }
                        return Observable.just(Response.success(LinkPreviewRetriever.generateDefaultLinkPreview(url)));
                    }
                });
    }

    private static boolean isLinkHasFileExtension(String url) {
        return !TextUtils.isEmpty(MimeTypeMap.getFileExtensionFromUrl(url));
    }

    private static LinkPreviewModel getFilePreviewDataType(Context context,
                                                           String url,
                                                           LinkPreviewModel linkPreviewModel) {
        linkPreviewModel.setId(null);
        linkPreviewModel.setLink(url);
        linkPreviewModel.setType(BasePreviewModel.PreviewType.FILE.getType());
        if (isImageType(url)) {
            setImageOrVideoPreviewProperties(context, url, true, linkPreviewModel);
        } else if (isVideoType(url)) {
            setImageOrVideoPreviewProperties(context, url, false, linkPreviewModel);
        } else {
            linkPreviewModel.setFileType(BasePreviewModel.FilePreviewType.FILE.getType());
            linkPreviewModel.setFileName(getFileNameFromUrl(url));
            linkPreviewModel.setFileExtension(getExtensionFromResultUrl(url));
        }

        return linkPreviewModel;
    }

    private static void setImageOrVideoPreviewProperties(Context context,
                                                         String url,
                                                         boolean isImagePreview,
                                                         LinkPreviewModel existModel) {
        existModel.setFileType(isImagePreview ? BasePreviewModel.FilePreviewType.IMAGE.getType() :
                BasePreviewModel.FilePreviewType.VIDEO.getType());
        int[] resolution;
        if (isImagePreview) {
            resolution = GlideLoadUtil.getRemoteImageResolution(context, url);
        } else {
            resolution = MediaUtil.getRemoteVideoResolution(url);
        }
        Timber.i("Resolution: " + Arrays.toString(resolution));
        if (resolution[0] > 0 && resolution[1] > 0) {
            existModel.setWidth(resolution[0]);
            existModel.setHeight(resolution[1]);
        }
    }

    public static String getHostFromUrl(String url) {
        try {
            Uri parse = Uri.parse(url);
            return parse.getHost();
        } catch (Exception ex) {
            Timber.e(ex);
        }

        return url;
    }

    private static String getFileNameFromUrl(String url) {
        Uri parse = Uri.parse(url);
        return parse.getLastPathSegment();
    }
}
