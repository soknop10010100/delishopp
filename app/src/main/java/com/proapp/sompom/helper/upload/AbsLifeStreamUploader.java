package com.proapp.sompom.helper.upload;

import android.content.Context;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.policy.UploadPolicy;
import com.proapp.sompom.R;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.Map;

import timber.log.Timber;

/**
 * Created by imac on 4/1/16.
 */
public class AbsLifeStreamUploader extends AbsMyUploader {

    private static final String POST = "post";

    private Context mContext;
    private String mImagePath;
    private String mFileName;
    private String mPreset;
    private String mTag;
    private FileType mFileType;
    private String mFileTitle;

    /**
     * Constructor.
     *
     * @param context   :
     * @param imagePath : String the path of the image file in phone, where user want to us to upload.
     */
    public AbsLifeStreamUploader(final Context context,
                                 final String imagePath,
                                 final FileType fileType,
                                 final String fileTitle,
                                 final Orientation orientation) {
        super(context);
        mContext = context;
        mImagePath = imagePath;
        mFileType = fileType;
        mFileTitle = fileTitle;
        if (fileType == FileType.VIDEO) {
            if (orientation == Orientation.Portrait) {
                mPreset = mContext.getString(R.string.cloudinary_preset);
            } else {
                mPreset = mContext.getString(R.string.cloudinary_preset);
            }
        } else {
            mPreset = mContext.getString(R.string.cloudinary_preset);
        }
        mTag = mContext.getString(R.string.life_stream_tag);

        if (fileType == FileType.FILE) {
            //TODO: Android currently does not have feature to upload file type to timeline
            // check back here
            mFileName = POST + "_" + System.currentTimeMillis() + "_" + SharedPrefUtils.getUserId(context) + "_" + mFileTitle;
        } else {
            mFileName = POST + "_" + SharedPrefUtils.getUserId(context) + "_" + System.currentTimeMillis();
        }
        Timber.e(mPreset + ", " + mTag + ", " + mFileName + ", " + mImagePath);
    }

    @Override
    public void cancelUpload(String requestId) {
        MediaManager.get().cancelRequest(requestId);
    }

    /**
     * Call for Process upload image.
     */
    @Override
    public String doUpload() {
        return TransformationProvider.generateDefaultUploadRequest(
                mFileType,
                mImagePath,
                mFileName,
                mPreset,
                mTag,
                getUploadFolder() + POST,
                new UploadPolicy.Builder().maxRetries(0).build(),
                this)
                .dispatch();
    }

    @Override
    public void onSuccess(String requestId, Map resultData) {
        handleOnSuccess(requestId, resultData);
    }

}
