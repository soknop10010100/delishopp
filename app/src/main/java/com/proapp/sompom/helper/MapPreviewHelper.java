package com.proapp.sompom.helper;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.LifeStream;

import java.util.Random;
import java.util.UUID;

/**
 * Created by Chhom Veasna on 5/19/2020.
 */
public class MapPreviewHelper {

    private static LifeStream sPost;
    private static String sRAW = "{\n" +
            "  \"item_type\": \"POST\",\n" +
            "  \"text\": \"\",\n" +
            "  \"_id\": \"5ec392377454185d6f3445e7\",\n" +
            "  \"createdAt\": \"2020-05-19T08:00:55.900Z\",\n" +
            "  \"media\": [],\n" +
            "  \"activityId\": \"de288a60-99a6-11ea-b6b1-0639629ed316\",\n" +
            "  \"metaPreview\": [\n" +
            "    {\n" +
            "      \"shouldPreview\": true,\n" +
            "      \"type\": \"map\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"longitude\": 104.94483411312103,\n" +
            "  \"latitude\": 11.461022227178592,\n" +
            "  \"address\": \"Street Nº 115,  Krong Ta Khmau,  Cambodia\",\n" +
            "  \"locationName\": \"TAKMAO GYM CLUB\",\n" +
            "  \"locationCountry\": \"Cambodia\",\n" +
            "  \"city\": \"Krong Ta Khmau\",\n" +
            "  \"shareUrl\": \"\",\n" +
            "  \"visibility\": 1,\n" +
            "  \"userstore\": {\n" +
            "    \"firstName\": \"Veasna\",\n" +
            "    \"lastName\": \"Chhom\",\n" +
            "    \"profileUrl\": \"http://res.cloudinary.com/sompom/image/upload/c_fill,h_200,q_auto,w_200/v1/proapp/profile/profile_5d916ef174b3367ba7f4f37c_1577093391661.jpg\",\n" +
            "    \"address\": \"Street Nº 115,  Krong Ta Khmau,  Cambodia\",\n" +
            "    \"_id\": \"5d916ef174b3367ba7f4f37c\",\n" +
            "    \"isLike\": false,\n" +
            "    \"isFollow\": false,\n" +
            "    \"contentStat\": {\n" +
            "      \"_id\": \"5e461dedee8f0445bc1b21f6\",\n" +
            "      \"totalLikes\": 0,\n" +
            "      \"likers\": [],\n" +
            "      \"totalComments\": 0,\n" +
            "      \"commenters\": [],\n" +
            "      \"totalViews\": 0,\n" +
            "      \"viewers\": [],\n" +
            "      \"totalFollowers\": 1,\n" +
            "      \"totalFollowings\": 0,\n" +
            "      \"followings\": [],\n" +
            "      \"contentType\": \"user\",\n" +
            "      \"contentId\": \"5d916ef174b3367ba7f4f37c\",\n" +
            "      \"totalShares\": null,\n" +
            "      \"sharers\": \"\",\n" +
            "      \"createdAt\": \"2020-02-14T04:11:25.491Z\",\n" +
            "      \"updatedAt\": \"2020-02-14T04:11:25.491Z\",\n" +
            "      \"__v\": 0,\n" +
            "      \"isFollow\": false,\n" +
            "      \"isLike\": false,\n" +
            "      \"fullLiker\": null,\n" +
            "      \"fullCommenter\": null,\n" +
            "      \"fullViewer\": null\n" +
            "    },\n" +
            "    \"isOnline\": true,\n" +
            "    \"lastActiveDate\": \"2020-05-19T09:34:17.000Z\"\n" +
            "  },\n" +
            "  \"isLike\": false,\n" +
            "  \"isFollow\": false,\n" +
            "  \"contentStat\": {\n" +
            "    \"_id\": \"5ec393297454185d6f3445e9\",\n" +
            "    \"totalLikes\": 0,\n" +
            "    \"totalComments\": 2,\n" +
            "    \"totalViews\": 0,\n" +
            "    \"totalFollowers\": 0,\n" +
            "    \"totalFollowings\": 0,\n" +
            "    \"followings\": [],\n" +
            "    \"contentType\": \"post\",\n" +
            "    \"contentId\": \"5ec392377454185d6f3445e7\",\n" +
            "    \"totalShares\": null,\n" +
            "    \"sharers\": \"\",\n" +
            "    \"createdAt\": \"2020-05-19T08:04:57.617Z\",\n" +
            "    \"updatedAt\": \"2020-05-19T08:04:57.617Z\",\n" +
            "    \"__v\": 0,\n" +
            "    \"isFollow\": false,\n" +
            "    \"isLike\": false,\n" +
            "    \"fullLiker\": null,\n" +
            "    \"fullCommenter\": \"5d889a85d73814259eb47656\",\n" +
            "    \"fullViewer\": null\n" +
            "  },\n" +
            "  \"userView\": [\n" +
            "    {\n" +
            "      \"_id\": \"5d889a85d73814259eb47656\",\n" +
            "      \"confirmed\": true,\n" +
            "      \"blocked\": false,\n" +
            "      \"country\": \"\",\n" +
            "      \"city\": \"\",\n" +
            "      \"location\": null,\n" +
            "      \"status\": null,\n" +
            "      \"accountStatus\": null,\n" +
            "      \"firstName\": \"솜폼\",\n" +
            "      \"lastName\": \"관리자\",\n" +
            "      \"playerId\": \"\",\n" +
            "      \"proType\": \"\",\n" +
            "      \"isDeleted\": \"\",\n" +
            "      \"provider\": \"local\",\n" +
            "      \"__v\": 0,\n" +
            "      \"id\": \"5d889a85d73814259eb47656\",\n" +
            "      \"role\": \"5b287726f023d71fe4fc8fd2\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"_id\": \"5d916eba74b3367ba7f4f37a\",\n" +
            "      \"confirmed\": true,\n" +
            "      \"blocked\": false,\n" +
            "      \"country\": \"KH\",\n" +
            "      \"city\": \"Cambodia\",\n" +
            "      \"location\": null,\n" +
            "      \"status\": 1,\n" +
            "      \"accountStatus\": null,\n" +
            "      \"playerId\": [\n" +
            "        \"23ddec34-106f-48c9-ab5b-440d2c4c1a9f\",\n" +
            "        \"e78d748f-732c-4298-9ef8-a96d5884c40a\",\n" +
            "        \"d0b63f5c-bbbb-4cf2-9a93-42fb876ffc59\",\n" +
            "        \"33b23688-60fe-4c6a-b38a-1d0810a14c50\",\n" +
            "        \"a68558b7-a31e-4cdc-b299-193ba4efc6c9\",\n" +
            "        \"9d476549-3ff7-4308-9d58-24bc09001a0d\",\n" +
            "        \"52fa1304-f005-41be-96d5-9529aaca4614\",\n" +
            "        \"88f0dec2-6507-4799-8db3-d7016b64df3d\",\n" +
            "        \"376989db-1c1d-4e78-b46e-3684fde7b459\"\n" +
            "      ],\n" +
            "      \"proType\": \"staff\",\n" +
            "      \"isDeleted\": \"\",\n" +
            "      \"firstName\": \"ルディ\",\n" +
            "      \"lastName\": \"ロザモント\",\n" +
            "      \"provider\": \"local\",\n" +
            "      \"__v\": 0,\n" +
            "      \"job\": \"5d4aa6b09b78d737c59bdb83\",\n" +
            "      \"role\": \"5b287726f023d71fe4fc8fd3\",\n" +
            "      \"mIsSeenMessage\": true,\n" +
            "      \"currency\": \"USD\",\n" +
            "      \"hasPhone\": true,\n" +
            "      \"hasSocial\": false,\n" +
            "      \"isFollow\": false,\n" +
            "      \"oldSell\": 0,\n" +
            "      \"profileUrl\": \"http://res.cloudinary.com/sompom/image/upload/c_fill,h_200,q_auto,w_200/v1/proapp/profile/profile_5d916eba74b3367ba7f4f37a_1589267930865.jpg\",\n" +
            "      \"storeItems\": 0,\n" +
            "      \"id\": \"5d916eba74b3367ba7f4f37a\",\n" +
            "      \"lastPostPrivacy\": 2,\n" +
            "      \"contentStat\": {\n" +
            "        \"contentType\": \"user\",\n" +
            "        \"createdAt\": \"2020-02-06T09:09:03.508Z\",\n" +
            "        \"_id\": \"5e3bd7afee8f0445bc1b1f04\",\n" +
            "        \"totalComments\": 0,\n" +
            "        \"totalFollowers\": 2,\n" +
            "        \"totalFollowings\": 0,\n" +
            "        \"totalLikes\": 0,\n" +
            "        \"totalViews\": 0,\n" +
            "        \"updatedAt\": \"2020-02-06T09:09:03.508Z\",\n" +
            "        \"viewers\": []\n" +
            "      }\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    static {
        sPost = new Gson().fromJson(sRAW, LifeStream.class);
    }

    private static class NamedLocation {

        public final String name;
        public final LatLng location;

        NamedLocation(String name, LatLng location) {
            this.name = name;
            this.location = location;
        }

        public LatLng getLocation() {
            return location;
        }
    }

    /**
     * A list of locations to show in this ListView.
     */
    private static final NamedLocation[] LIST_LOCATIONS = new NamedLocation[]{
            new NamedLocation("Cape Town", new LatLng(-33.920455, 18.466941)),
            new NamedLocation("Beijing", new LatLng(39.937795, 116.387224)),
            new NamedLocation("Bern", new LatLng(46.948020, 7.448206)),
            new NamedLocation("Breda", new LatLng(51.589256, 4.774396)),
            new NamedLocation("Brussels", new LatLng(50.854509, 4.376678)),
            new NamedLocation("Copenhagen", new LatLng(55.679423, 12.577114)),
            new NamedLocation("Hannover", new LatLng(52.372026, 9.735672)),
            new NamedLocation("Helsinki", new LatLng(60.169653, 24.939480)),
            new NamedLocation("Hong Kong", new LatLng(22.325862, 114.165532)),
            new NamedLocation("Istanbul", new LatLng(41.034435, 28.977556)),
            new NamedLocation("Johannesburg", new LatLng(-26.202886, 28.039753)),
            new NamedLocation("Lisbon", new LatLng(38.707163, -9.135517)),
            new NamedLocation("London", new LatLng(51.500208, -0.126729)),
            new NamedLocation("Madrid", new LatLng(40.420006, -3.709924)),
            new NamedLocation("Mexico City", new LatLng(19.427050, -99.127571)),
            new NamedLocation("Moscow", new LatLng(55.750449, 37.621136)),
            new NamedLocation("New York", new LatLng(40.750580, -73.993584)),
            new NamedLocation("Oslo", new LatLng(59.910761, 10.749092)),
            new NamedLocation("Paris", new LatLng(48.859972, 2.340260)),
            new NamedLocation("Prague", new LatLng(50.087811, 14.420460)),
            new NamedLocation("Rio de Janeiro", new LatLng(-22.90187, -43.232437)),
            new NamedLocation("Rome", new LatLng(41.889998, 12.500162)),
            new NamedLocation("Sao Paolo", new LatLng(-22.863878, -43.244097)),
            new NamedLocation("Seoul", new LatLng(37.560908, 126.987705)),
            new NamedLocation("Stockholm", new LatLng(59.330650, 18.067360)),
            new NamedLocation("Sydney", new LatLng(-33.873651, 151.2068896)),
            new NamedLocation("Taipei", new LatLng(25.022112, 121.478019)),
            new NamedLocation("Tokyo", new LatLng(35.670267, 139.769955)),
            new NamedLocation("Tulsa Oklahoma", new LatLng(36.149777, -95.993398)),
            new NamedLocation("Vaduz", new LatLng(47.141076, 9.521482)),
            new NamedLocation("Vienna", new LatLng(48.209206, 16.372778)),
            new NamedLocation("Warsaw", new LatLng(52.235474, 21.004057)),
            new NamedLocation("Wellington", new LatLng(-41.286480, 174.776217)),
            new NamedLocation("Winnipeg", new LatLng(49.875832, -97.150726))
    };

    public static Adaptive getRandomMapPreviewPost() {
        LifeStream lifeStream = new LifeStream(sPost);
        NamedLocation location = LIST_LOCATIONS[new Random().nextInt(LIST_LOCATIONS.length)];
        lifeStream.setLatitude(location.getLocation().latitude);
        lifeStream.setLongitude(location.getLocation().longitude);
        lifeStream.setId(UUID.randomUUID().toString());

        return lifeStream;
    }
}
