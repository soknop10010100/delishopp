package com.proapp.sompom.helper;

import androidx.recyclerview.widget.DiffUtil;

import com.proapp.sompom.model.ConciergeItemAdaptive;
import com.proapp.sompom.model.concierge.ConciergeBrandSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeCarouselSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeFeatureStoreSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeNewArrivalProductSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeOurServiceSectionResponse;
import com.proapp.sompom.model.concierge.ConciergePushItemSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeShopAnnouncement;
import com.proapp.sompom.model.concierge.ConciergeSupplierSectionResponse;
import com.proapp.sompom.model.concierge.ConciergeTrendingSectionResponse;

import java.util.List;

/**
 * Created by Or Vitovongsak on 1/9/21.
 */
public class ConciergeShopListDiffCallback extends DiffUtil.Callback {

    private final List<ConciergeItemAdaptive> mOldList;
    private final List<ConciergeItemAdaptive> mNewList;

    public ConciergeShopListDiffCallback(List<ConciergeItemAdaptive> oldBaseChatModelList,
                                         List<ConciergeItemAdaptive> newBaseChatModelList) {
        this.mOldList = oldBaseChatModelList;
        this.mNewList = newBaseChatModelList;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        if ((mOldList.get(oldItemPosition) instanceof ConciergeShopAnnouncement && mNewList.get(newItemPosition) instanceof ConciergeShopAnnouncement)) {
            return ((ConciergeShopAnnouncement) mOldList.get(oldItemPosition)).areItemsTheSame((ConciergeShopAnnouncement) mNewList.get(newItemPosition));
        } else if ((mOldList.get(oldItemPosition) instanceof ConciergeOurServiceSectionResponse && mNewList.get(newItemPosition) instanceof ConciergeOurServiceSectionResponse)) {
            return ((ConciergeOurServiceSectionResponse) mOldList.get(oldItemPosition)).areItemsTheSame((ConciergeOurServiceSectionResponse) mNewList.get(newItemPosition));
        } else if ((mOldList.get(oldItemPosition) instanceof ConciergeCarouselSectionResponse && mNewList.get(newItemPosition) instanceof ConciergeCarouselSectionResponse)) {
            return ((ConciergeCarouselSectionResponse) mOldList.get(oldItemPosition)).areItemsTheSame((ConciergeCarouselSectionResponse) mNewList.get(newItemPosition));
        } else if (mOldList.get(oldItemPosition) instanceof ConciergePushItemSectionResponse && mNewList.get(newItemPosition) instanceof ConciergePushItemSectionResponse) {
            return ((ConciergePushItemSectionResponse) mOldList.get(oldItemPosition)).areItemsTheSame((ConciergePushItemSectionResponse) mNewList.get(newItemPosition));
        } else if ((mOldList.get(oldItemPosition) instanceof ConciergeFeatureStoreSectionResponse && mNewList.get(newItemPosition) instanceof ConciergeFeatureStoreSectionResponse)) {
            return ((ConciergeFeatureStoreSectionResponse) mOldList.get(oldItemPosition)).areItemsTheSame((ConciergeFeatureStoreSectionResponse) mNewList.get(newItemPosition));
        } else if ((mOldList.get(oldItemPosition) instanceof ConciergeTrendingSectionResponse && mNewList.get(newItemPosition) instanceof ConciergeTrendingSectionResponse)) {
            return ((ConciergeTrendingSectionResponse) mOldList.get(oldItemPosition)).areItemsTheSame((ConciergeTrendingSectionResponse) mNewList.get(newItemPosition));
        } else if ((mOldList.get(oldItemPosition) instanceof ConciergeMenuItem && mNewList.get(newItemPosition) instanceof ConciergeMenuItem)) {
            return ((ConciergeMenuItem) mOldList.get(oldItemPosition)).areItemsTheSame((ConciergeMenuItem) mNewList.get(newItemPosition));
        } else if (mOldList.get(oldItemPosition) instanceof ConciergeNewArrivalProductSectionResponse && mNewList.get(newItemPosition) instanceof ConciergeNewArrivalProductSectionResponse) {
            return ((ConciergeNewArrivalProductSectionResponse) mOldList.get(oldItemPosition)).areItemsTheSame((ConciergeNewArrivalProductSectionResponse) mNewList.get(newItemPosition));
        } else if (mOldList.get(oldItemPosition) instanceof ConciergeBrandSectionResponse && mNewList.get(newItemPosition) instanceof ConciergeBrandSectionResponse) {
            return ((ConciergeBrandSectionResponse) mOldList.get(oldItemPosition)).areItemsTheSame((ConciergeBrandSectionResponse) mNewList.get(newItemPosition));
        } else if (mOldList.get(oldItemPosition) instanceof ConciergeSupplierSectionResponse && mNewList.get(newItemPosition) instanceof ConciergeSupplierSectionResponse) {
            return ((ConciergeSupplierSectionResponse) mOldList.get(oldItemPosition)).areItemsTheSame((ConciergeSupplierSectionResponse) mNewList.get(newItemPosition));
        }

        return false;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        if ((mOldList.get(oldItemPosition) instanceof ConciergeShopAnnouncement && mNewList.get(newItemPosition) instanceof ConciergeShopAnnouncement)) {
            return ((ConciergeShopAnnouncement) mOldList.get(oldItemPosition)).areContentsTheSame((ConciergeShopAnnouncement) mNewList.get(newItemPosition));
        } else if ((mOldList.get(oldItemPosition) instanceof ConciergeOurServiceSectionResponse && mNewList.get(newItemPosition) instanceof ConciergeOurServiceSectionResponse)) {
            return ((ConciergeOurServiceSectionResponse) mOldList.get(oldItemPosition)).areContentsTheSame((ConciergeOurServiceSectionResponse) mNewList.get(newItemPosition));
        } else if ((mOldList.get(oldItemPosition) instanceof ConciergeCarouselSectionResponse && mNewList.get(newItemPosition) instanceof ConciergeCarouselSectionResponse)) {
            return ((ConciergeCarouselSectionResponse) mOldList.get(oldItemPosition)).areContentsTheSame((ConciergeCarouselSectionResponse) mNewList.get(newItemPosition));
        } else if (mOldList.get(oldItemPosition) instanceof ConciergePushItemSectionResponse && mNewList.get(newItemPosition) instanceof ConciergePushItemSectionResponse) {
            return ((ConciergePushItemSectionResponse) mOldList.get(oldItemPosition)).areContentsTheSame((ConciergePushItemSectionResponse) mNewList.get(newItemPosition));
        } else if ((mOldList.get(oldItemPosition) instanceof ConciergeFeatureStoreSectionResponse && mNewList.get(newItemPosition) instanceof ConciergeFeatureStoreSectionResponse)) {
            return ((ConciergeFeatureStoreSectionResponse) mOldList.get(oldItemPosition)).areContentsTheSame((ConciergeFeatureStoreSectionResponse) mNewList.get(newItemPosition));
        } else if ((mOldList.get(oldItemPosition) instanceof ConciergeTrendingSectionResponse && mNewList.get(newItemPosition) instanceof ConciergeTrendingSectionResponse)) {
            return ((ConciergeTrendingSectionResponse) mOldList.get(oldItemPosition)).areContentsTheSame((ConciergeTrendingSectionResponse) mNewList.get(newItemPosition));
        } else if ((mOldList.get(oldItemPosition) instanceof ConciergeMenuItem && mNewList.get(newItemPosition) instanceof ConciergeMenuItem)) {
            return ((ConciergeMenuItem) mOldList.get(oldItemPosition)).areContentsTheSame((ConciergeMenuItem) mNewList.get(newItemPosition));
        } else if ((mOldList.get(oldItemPosition) instanceof ConciergeNewArrivalProductSectionResponse && mNewList.get(newItemPosition) instanceof ConciergeNewArrivalProductSectionResponse)) {
            return ((ConciergeNewArrivalProductSectionResponse) mOldList.get(oldItemPosition)).areContentsTheSame((ConciergeNewArrivalProductSectionResponse) mNewList.get(newItemPosition));
        } else if ((mOldList.get(oldItemPosition) instanceof ConciergeBrandSectionResponse && mNewList.get(newItemPosition) instanceof ConciergeBrandSectionResponse)) {
            return ((ConciergeBrandSectionResponse) mOldList.get(oldItemPosition)).areContentsTheSame((ConciergeBrandSectionResponse) mNewList.get(newItemPosition));
        } else if ((mOldList.get(oldItemPosition) instanceof ConciergeSupplierSectionResponse && mNewList.get(newItemPosition) instanceof ConciergeSupplierSectionResponse)) {
            return ((ConciergeSupplierSectionResponse) mOldList.get(oldItemPosition)).areContentsTheSame((ConciergeSupplierSectionResponse) mNewList.get(newItemPosition));
        }

        return false;
    }
}
