package com.proapp.sompom.helper;

import android.content.Context;
import android.text.TextUtils;

import androidx.recyclerview.widget.DiffUtil;

import com.proapp.sompom.model.BaseChatModel;
import com.proapp.sompom.model.TypingChatItem;
import com.proapp.sompom.model.UnreadChatModel;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;

import java.util.List;

public class ChatDiffCallback extends DiffUtil.Callback {
    private final List<BaseChatModel> mOldList;
    private final List<BaseChatModel> mNewList;
    private Context mContext;
    private Conversation mConversation;

    public ChatDiffCallback(Context context,
                            Conversation conversation,
                            List<BaseChatModel> oldBaseChatModelList,
                            List<BaseChatModel> newBaseChatModelList) {
        this.mContext = context;
        this.mConversation = conversation;
        this.mOldList = oldBaseChatModelList;
        this.mNewList = newBaseChatModelList;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldPos, int newPos) {
        if (mOldList.get(oldPos) instanceof TypingChatItem && mNewList.get(newPos) instanceof TypingChatItem) {
            return true;
        } else if (mOldList.get(oldPos) instanceof UnreadChatModel && mNewList.get(newPos) instanceof UnreadChatModel) {
            return true;
        } else {
            if (mOldList.get(oldPos) instanceof Chat && mNewList.get(newPos) instanceof Chat) {
                String oldId = ((Chat) mOldList.get(oldPos)).getId();
                String newId = ((Chat) mNewList.get(newPos)).getId();
                return TextUtils.equals(oldId, newId);
            }
        }
        return false;
    }

    @Override
    public boolean areContentsTheSame(int oldPos, int newPos) {
        if (mOldList.get(oldPos) instanceof TypingChatItem && mNewList.get(newPos) instanceof TypingChatItem) {
            return true;
        } else if (mOldList.get(oldPos) instanceof UnreadChatModel && mNewList.get(newPos) instanceof UnreadChatModel) {
            return true;
        } else {
            if (mOldList.get(oldPos) instanceof Chat && mNewList.get(newPos) instanceof Chat) {
                Chat oldChat = ((Chat) mOldList.get(oldPos));
                Chat newChat = ((Chat) mNewList.get(newPos));
                return TextUtils.equals(oldChat.getActualContent(mContext, mConversation, false),
                        newChat.getActualContent(mContext, mConversation, false)) &&
                        oldChat.getStatus() == newChat.getStatus() &&
                        oldChat.getDate().getTime() == newChat.getDate().getTime();
            }
        }
        return false;
    }
}
