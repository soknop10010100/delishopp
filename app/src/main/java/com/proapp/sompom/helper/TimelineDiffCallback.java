package com.proapp.sompom.helper;

import android.text.TextUtils;

import androidx.recyclerview.widget.DiffUtil;

import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.LifeStream;
import com.proapp.sompom.model.WelcomeItem;

import java.util.List;

public class TimelineDiffCallback extends DiffUtil.Callback {

    private final List<Adaptive> mOldList;
    private final List<Adaptive> mNewList;

    public TimelineDiffCallback(List<Adaptive> oldAdaptiveList,
                                List<Adaptive> newAdaptiveList) {
        this.mOldList = oldAdaptiveList;
        this.mNewList = newAdaptiveList;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldPos, int newPos) {
        if (mOldList.get(oldPos) instanceof LifeStream && mNewList.get(newPos) instanceof LifeStream) {
            return ((LifeStream) mOldList.get(oldPos)).areItemsTheSame((LifeStream) mNewList.get(newPos));
        } else if (mOldList.get(oldPos) instanceof WelcomeItem && mNewList.get(newPos) instanceof WelcomeItem) {
            return ((WelcomeItem) mOldList.get(oldPos)).areItemsTheSame((WelcomeItem) mNewList.get(newPos));
        } else {
            if (!TextUtils.isEmpty(mOldList.get(oldPos).getId()) && !TextUtils.isEmpty(mNewList.get(newPos).getId())) {
                return mOldList.get(oldPos).getId().matches(mNewList.get(newPos).getId());
            } else {
                return false;
            }
        }
    }

    @Override
    public boolean areContentsTheSame(int oldPos, int newPos) {
        if (mOldList.get(oldPos) instanceof LifeStream && mNewList.get(newPos) instanceof LifeStream) {
            return ((LifeStream) mOldList.get(oldPos)).areContentsTheSame((LifeStream) mNewList.get(newPos));
        } else if (mOldList.get(oldPos) instanceof WelcomeItem && mNewList.get(newPos) instanceof WelcomeItem) {
            return ((WelcomeItem) mOldList.get(oldPos)).areContentsTheSame((WelcomeItem) mNewList.get(newPos));
        }

        return false;
    }
}
