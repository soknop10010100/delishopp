package com.proapp.sompom.helper;

import android.app.Activity;

import androidx.annotation.StringRes;

import com.proapp.sompom.R;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.utils.IntentUtil;
import com.sompom.baseactivity.PermissionCheckHelper;

/**
 * Created by nuonveyo
 * on 1/31/19.
 */

public abstract class CheckPermissionCallbackHelper implements PermissionCheckHelper.OnPermissionCallback {

    private final Activity mContext;
    private final Type mType;

    protected CheckPermissionCallbackHelper(Activity context, Type type) {
        mContext = context;
        mType = type;
    }

    @Override
    public void onPermissionDenied(String[] grantedPermission, String[] deniedPermission, boolean isUserPressNeverAskAgain) {
        if (isUserPressNeverAskAgain) {
            MessageDialog dialog = new MessageDialog();
            dialog.setTitle(getString(R.string.popup_permission_request_title));
            dialog.setMessage(getDescription());
            dialog.setLeftText(getString(R.string.popup_permission_app_setting_title), view -> IntentUtil.openAppSetting(mContext));
            dialog.setRightText(getString(R.string.popup_no_button), null);
            dialog.setOnDialogDismiss(this::onCancelClick);
            dialog.show(((AbsBaseActivity) mContext).getSupportFragmentManager(), MessageDialog.TAG);
        }
    }

    public void onCancelClick() {

    }

    private String getString(@StringRes int id) {
        return mContext.getString(id);
    }

    private String getDescription() {
        if (mType == Type.CAMERA) {
            return mContext.getString(R.string.popup_permission_force_request_camera_description, getString(R.string.app_name));
        } else if (mType == Type.MICROPHONE) {
            return mContext.getString(R.string.popup_permission_force_request_microphone_description, getString(R.string.app_name));
        } else {
            return mContext.getString(R.string.popup_permission_force_request_storage_description, getString(R.string.app_name));
        }
    }

    public enum Type {
        CAMERA,
        MICROPHONE,
        STORAGE
    }
}