package com.proapp.sompom.helper;

import android.content.Context;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.proapp.sompom.model.AutoStartPermissionData;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.concurrent.TimeUnit;

import timber.log.Timber;

public class AutoStartPermissionDialogHelper {

    private static final int CANCEL_INTERVAL = 3;
    private static final int PROCEED_INTERVAL = 9;
    private static final String AUTO_START_PERMISSION_DATA = "AUTOSTARTPERMISSIONDATA";

    public static void checkToShowDialog(AppCompatActivity activity) {
        /*
            Normally this kind checking is necessary for the build with OneSingal push notification.
            If the Pushy push service is enabled for the build, we do not have to perform this checking.
         */
        //TODO: Need to enable back after implementation of receiving OneSignal push notification
//        if (!NotificationServiceHelper.isPushyProviderEnabled(activity) &&
//                AutoStartPermissionHelper.getInstance().isAutoStartPermissionAvailable(activity)) {
//            if (shouldShowAlert(activity)) {
//                AutoStartPermissionDialog dialog = AutoStartPermissionDialog.newInstance();
//                dialog.show(activity.getSupportFragmentManager());
//            }
//        }
    }

    private static AutoStartPermissionData getData(Context context) {
        String data = SharedPrefUtils.getString(context, AUTO_START_PERMISSION_DATA);
        if (!TextUtils.isEmpty(data)) {
            return new Gson().fromJson(data, AutoStartPermissionData.class);
        }

        return new AutoStartPermissionData();
    }

    private static void setData(Context context, AutoStartPermissionData data) {
        SharedPrefUtils.setString(context, AUTO_START_PERMISSION_DATA, new Gson().toJson(data));
    }

    public static void setLastUserResponseState(Context context, boolean isAcceptedToProceed) {
        AutoStartPermissionData data = getData(context);
        data.setLastResponseIsProceed(isAcceptedToProceed);
        setData(context, data);
    }

    public static void setLastShownAlertDate(Context context, long milli) {
        AutoStartPermissionData data = getData(context);
        data.setLastShownDate(milli);
        setData(context, data);
    }

    public static void setCheckingProcessCompleted(Context context, boolean isCompleted) {
        AutoStartPermissionData data = getData(context);
        data.setCompleted(isCompleted);
        setData(context, data);
    }

    private static boolean shouldShowAlert(Context context) {
        boolean shouldShow;
        AutoStartPermissionData data = getData(context);
        if (data.isCompleted()) {
            Timber.i("Checking Process Completed");
            shouldShow = false;
        } else if (!data.isShowFirstRequest()) {
            Timber.i("ShowFirstRequest");
            shouldShow = true;
            data.setShowFirstRequest(true);
        } else if (data.getLastShownDate() <= 0) {
            Timber.i("LastShownDate is less than 0.");
            shouldShow = true;
        } else if (data.getLastShownDate() > 0) {
            long diff = System.currentTimeMillis() - data.getLastShownDate();
            long days = getDayAmountFromMilliSeconds(diff);
            Timber.i("Diff days: " + days);
            /*
            Note:
            Response "Proceed" mean user agreed to open setting screen in mobile.
            Response "No" mean user refused to open setting screen in mobile.
            We currently don't have the possibility to know if the app has been added in protection
            by user. Here, we are just checking to show the alert and ask user to manually set thing up.
             */
            if (data.isLastResponseIsProceed()) {
                shouldShow = days >= PROCEED_INTERVAL;
            } else {
                shouldShow = days >= CANCEL_INTERVAL;
            }
        } else {
            Timber.i("Reach default for not showing alert.");
            shouldShow = false;
        }

        setData(context, data);
        return shouldShow;
    }

    private static long getDayAmountFromMilliSeconds(long milli) {
        return TimeUnit.MILLISECONDS.toDays(milli);
    }
}
