package com.proapp.sompom.helper;

import com.google.android.exoplayer2.ControlDispatcher;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;

/**
 * Created by He Rotha on 2/6/19.
 */
public abstract class PlayControlDispatcher implements ControlDispatcher {

    @Override
    public boolean dispatchPrepare(Player player) {
        return false;
    }

    @Override
    public boolean dispatchSetPlayWhenReady(Player player, boolean playWhenReady) {
        player.setPlayWhenReady(playWhenReady);
        onButtonPlayClick(playWhenReady);
        return true;
    }

    @Override
    public boolean dispatchSeekTo(Player player, int windowIndex, long positionMs) {
        player.seekTo(windowIndex, positionMs);
        return true;
    }

    @Override
    public boolean dispatchPrevious(Player player) {
        return false;
    }

    @Override
    public boolean dispatchNext(Player player) {
        return false;
    }

    @Override
    public boolean dispatchRewind(Player player) {
        return false;
    }

    @Override
    public boolean dispatchFastForward(Player player) {
        return false;
    }

    @Override
    public boolean dispatchSetRepeatMode(Player player, @Player.RepeatMode int repeatMode) {
        player.setRepeatMode(repeatMode);
        return true;
    }

    @Override
    public boolean dispatchSetShuffleModeEnabled(Player player, boolean shuffleModeEnabled) {
        player.setShuffleModeEnabled(shuffleModeEnabled);
        return true;
    }

    @Override
    public boolean dispatchStop(Player player, boolean reset) {
        player.stop(reset);
        return true;
    }

    @Override
    public boolean dispatchSetPlaybackParameters(Player player, PlaybackParameters playbackParameters) {
        return false;
    }

    @Override
    public boolean isRewindEnabled() {
        return false;
    }

    @Override
    public boolean isFastForwardEnabled() {
        return false;
    }

    public abstract void onButtonPlayClick(boolean isPlay);

}
