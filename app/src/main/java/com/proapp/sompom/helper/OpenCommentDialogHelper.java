package com.proapp.sompom.helper;

import android.app.Activity;
import android.content.Context;

import com.proapp.sompom.adapter.newadapter.CommentAdapter;
import com.proapp.sompom.adapter.newadapter.TimelineAdapter;
import com.proapp.sompom.adapter.newadapter.TimelineDetailAdapter;
import com.proapp.sompom.listener.OnCompleteListener;
import com.proapp.sompom.model.Adaptive;
import com.proapp.sompom.model.CommonWallAdapter;
import com.proapp.sompom.model.LoadCommentDirection;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.WallStreetAdaptive;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.result.Comment;
import com.proapp.sompom.model.result.ContentStat;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.CommentDialog;
import com.proapp.sompom.services.ApiService;

import java.util.List;

/**
 * Created by nuonveyo
 * on 2/18/19.
 */

public class OpenCommentDialogHelper {

    private final Activity mActivity;
    private boolean mIsShow;

    public OpenCommentDialogHelper(Activity activity) {
        mActivity = activity;
    }

    public void show(Context context,
                     ApiService apiService,
                     String postId,
                     Adaptive adaptive,
                     CommonWallAdapter adapter,
                     boolean autoDisplayKeyboard) {
        show(postId, adaptive, autoDisplayKeyboard, result -> {
            if (adapter instanceof TimelineAdapter) {
                ((TimelineAdapter) adapter).update(result);
            } else if (adapter instanceof TimelineDetailAdapter) {
                ((TimelineDetailAdapter) adapter).updateCommentCounter(result);
            }
            WallStreetHelper.requestToUpdateUnreadCommentCounter(context, apiService, adapter, adaptive);
        });
    }

    private void show(String postId,
                      Adaptive adaptive,
                      boolean autoDisplayKeyboard,
                      OnCompleteListener<Adaptive> listener) {
        if (!mIsShow && mActivity instanceof AbsBaseActivity) {
            mIsShow = true;
            CommentDialog commentDialog = CommentDialog.newInstanceForComment(adaptive.getId());
            commentDialog.setContentType(getContentType(adaptive));
            commentDialog.setPostId(postId);
            commentDialog.setAutoDisplayKeyboard(autoDisplayKeyboard);
            commentDialog.setOnDismissDialogListener(() -> {
                mIsShow = false;
                if (adaptive instanceof WallStreetAdaptive && ((WallStreetAdaptive) adaptive).getContentStat() != null) {
                    ContentStat stat = ((WallStreetAdaptive) adaptive).getContentStat();
                    if (stat != null) {
                        int addNum = commentDialog.getNumberOfAddComment();
                        long value = stat.getTotalComments() + addNum;
                        if (value < 0) {
                            value = 0;
                        }
                        stat.setTotalComments(value);
                        listener.onComplete(adaptive);
                    }
                }
            });
            commentDialog.show(((AbsBaseActivity) mActivity).getSupportFragmentManager(), CommentDialog.TAG);
        }
    }

    private static ContentType getContentType(Adaptive adaptive) {
        if (adaptive instanceof Media) {
            return ContentTypeHelper.getMediaContentType((Media) adaptive);
        } else {
            return ContentType.POST;
        }
    }

    private static ContentType getContentType(WallStreetAdaptive adaptive) {
        if (adaptive instanceof Media) {
            return ContentTypeHelper.getMediaContentType((Media) adaptive);
        } else {
            return ContentType.POST;
        }
    }

    public void showReplyComment(Context context,
                                 ApiService apiService,
                                 String postId,
                                 WallStreetAdaptive wallStreetAdaptive,
                                 Adaptive adaptive,
                                 Comment comment,
                                 boolean autoDisplayKeyboard,
                                 CommonWallAdapter adapter,
                                 CommentDialog.OnCloseReplyCommentFragmentListener listener) {
        if (!mIsShow && mActivity instanceof AbsBaseActivity) {
            mIsShow = true;
            CommentDialog commentDialog = CommentDialog.newInstanceForSubComment(wallStreetAdaptive.getId(), comment);
            commentDialog.setContentType(getContentType(wallStreetAdaptive));
            commentDialog.setAutoDisplayKeyboard(autoDisplayKeyboard);
            commentDialog.setPostId(postId);
            commentDialog.setOnNotifyCommentItemChangeListener(listener);
            commentDialog.setOnDismissDialogListener(() -> {
                mIsShow = false;
                if (wallStreetAdaptive.getContentStat() != null) {
                    ContentStat stat = wallStreetAdaptive.getContentStat();
                    int addNum = commentDialog.getNumberOfAddComment();
                    long value = stat.getTotalComments() + addNum;
                    if (value < 0) {
                        value = 0;
                    }
                    stat.setTotalComments(value);
                    if (listener != null) {
                        listener.onUpdateMainCommentCount(value);
                    }
                }
                WallStreetHelper.requestToUpdateUnreadCommentCounter(context, apiService, adapter, adaptive);
            });
            commentDialog.show(((AbsBaseActivity) mActivity).getSupportFragmentManager(), CommentDialog.TAG);
        }
    }

    public static void onLoadMoreCommentFinish(List<Comment> listComment,
                                               boolean isInJumpMode,
                                               boolean isLoadPrevious,
                                               boolean isCanLoadNext,
                                               LoadCommentDirection loadCommentDirection,
                                               int position,
                                               CommentAdapter commentAdapter) {
        if (!isInJumpMode) {
            //Normal mode
            commentAdapter.checkToAddPreviousLoadedComment(isLoadPrevious, loadCommentDirection, position, listComment);
        } else {
            //Load more with jump mode
            if (loadCommentDirection.isLoadPreviousCommentMode()) {
                commentAdapter.checkToAddPreviousLoadedComment(isLoadPrevious, loadCommentDirection, position, listComment);
            } else {
                commentAdapter.checkToAddNextLoadedComment(isCanLoadNext, loadCommentDirection, position, listComment);
            }
        }
    }
}
