package com.proapp.sompom.helper;

import com.proapp.sompom.model.Media;
import com.proapp.sompom.model.emun.ContentType;
import com.proapp.sompom.model.emun.MediaType;

public class ContentTypeHelper {

    public static ContentType getMediaContentType(Media media) {
        if (media.getType() == MediaType.VIDEO) {
            return ContentType.VIDEO;
        } else if (media.getType() == MediaType.IMAGE) {
            return ContentType.IMAGE;
        } else {
            return ContentType.MEDIA;
        }
    }
}
