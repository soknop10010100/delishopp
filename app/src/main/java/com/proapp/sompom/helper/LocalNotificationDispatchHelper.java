package com.proapp.sompom.helper;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.proapp.sompom.R;

public class LocalNotificationDispatchHelper {

    private static final long[] NORMAL_VIBRATION = new long[]{0, 500};
    public static final long[] NONE_VIBRATION = new long[]{0, 0};

    public static NotificationData getNotificationData(Context context, String baseChannelId) {
        if (TextUtils.equals(baseChannelId, NotificationChannelHelper.CHANNEL_TYPE_CHAT)) {
            return new NotificationData(NotificationCompat.PRIORITY_HIGH,
                    NotificationManagerCompat.IMPORTANCE_HIGH,
                    NORMAL_VIBRATION,
                    "android.resource://" + context.getPackageName() +
                            "/" + R.raw.notification_chat);
        } else if (TextUtils.equals(baseChannelId, NotificationChannelHelper.CHANNEL_TYPE_FEATURE_SYNCHRONIZATION)) {
            return new NotificationData(NotificationCompat.PRIORITY_DEFAULT,
                    NotificationManagerCompat.IMPORTANCE_DEFAULT,
                    NORMAL_VIBRATION,
                    "android.resource://" + context.getPackageName() +
                            "/" + R.raw.notification_chat);
        } else if (TextUtils.equals(baseChannelId, NotificationChannelHelper.CHANNEL_TYPE_CALL_INFORMATION)) {
            return new NotificationData(NotificationCompat.PRIORITY_DEFAULT,
                    NotificationManagerCompat.IMPORTANCE_DEFAULT,
                    NONE_VIBRATION,
                    null);
        } else if (TextUtils.equals(baseChannelId, NotificationChannelHelper.CHANNEL_TYPE_INCOMING_CALL)) {
            /*
            For Android version 10 up, we need to display incoming call notification as high priority mode for
            if the app is not open and receiving incoming via notification push service, on Android 10,
            we just display incoming call notification and not yet open any screen until user make the
            interaction on that incoming call notification first, so that notification must display directly
            to user.
             */
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                return new NotificationData(NotificationCompat.PRIORITY_HIGH,
                        NotificationManagerCompat.IMPORTANCE_HIGH,
                        NONE_VIBRATION,
                        null);
            } else {
                return new NotificationData(NotificationCompat.PRIORITY_DEFAULT,
                        NotificationManagerCompat.IMPORTANCE_DEFAULT,
                        NONE_VIBRATION,
                        null);
            }
        } else if (TextUtils.equals(baseChannelId, NotificationChannelHelper.CHANNEL_TYPE_SILENT)) {
            return new NotificationData(NotificationCompat.PRIORITY_LOW,
                    NotificationManagerCompat.IMPORTANCE_LOW,
                    NONE_VIBRATION,
                    null);
        } else if (TextUtils.equals(baseChannelId, NotificationChannelHelper.CHANNEL_TYPE_VIBRATION)) {
            return new NotificationData(NotificationCompat.PRIORITY_DEFAULT,
                    NotificationManagerCompat.IMPORTANCE_DEFAULT,
                    NORMAL_VIBRATION,
                    null);
        } else if (TextUtils.equals(baseChannelId, NotificationChannelHelper.CHANNEL_TYPE_ORDER_STATUS_UPDATE)) {
            return new NotificationData(NotificationCompat.PRIORITY_HIGH,
                    NotificationManagerCompat.IMPORTANCE_HIGH,
                    NORMAL_VIBRATION,
                    "android.resource://" + context.getPackageName() +
                            "/" + R.raw.order_status_update_notification);
        } else {
            return new NotificationData(NotificationCompat.PRIORITY_DEFAULT,
                    NotificationManagerCompat.IMPORTANCE_DEFAULT,
                    NORMAL_VIBRATION,
                    "android.resource://" + context.getPackageName() +
                            "/" + R.raw.feed_notification);
        }
    }

    public static class NotificationData {

        private final int mPriority; // Use with {setPriority} on Android version below O.
        private final int mImportance; // Use with create channel notification on Android version O and above¶¶
        private final long[] mVibrationPattern;
        private final String mSoundUri;

        public NotificationData(int priority, int importance, long[] vibrationPattern, String soundUri) {
            mPriority = priority;
            mImportance = importance;
            mVibrationPattern = vibrationPattern;
            mSoundUri = soundUri;
        }

        public int getPriority() {
            return mPriority;
        }

        public long[] getVibrationPattern() {
            return mVibrationPattern;
        }

        public int getImportance() {
            return mImportance;
        }

        public String getSoundUri() {
            return mSoundUri;
        }
    }
}
