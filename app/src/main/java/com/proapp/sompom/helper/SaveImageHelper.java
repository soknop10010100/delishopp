package com.proapp.sompom.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.PictureDrawable;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.desmond.squarecamera.utils.media.FileNameUtil;
import com.desmond.squarecamera.utils.media.ImageSaver;
import com.proapp.sompom.R;
import com.proapp.sompom.glide.SvgSoftwareLayerSetter;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.utils.GlideLoadUtil;
import com.proapp.sompom.utils.ToastUtil;

import java.util.List;

import timber.log.Timber;

/**
 * Created by nuonveyo on 9/17/18.
 */

public class SaveImageHelper {

    private Context mContext;
    private boolean mIsNotError;

    public SaveImageHelper(Context context) {
        mContext = context;
    }

    public void startSave(List<Media> medias) {
        for (int i = 0; i < medias.size(); i++) {
            int finalI = i;
            download(medias.get(finalI), new OnCallback() {
                @Override
                public void onFail() {
                    if (finalI == medias.size() - 1 && !mIsNotError) {
                        ToastUtil.showToast(mContext, mContext.getString(R.string.toast_save_file_fail), true);
                    }
                }

                @Override
                public void onSuccess(@NonNull Bitmap resource) {
                    mIsNotError = true;
                    new Handler(mContext.getMainLooper()).post(() -> {
                        ImageSaver.with(mContext)
                                .load(resource)
                                .into(FileNameUtil.getMessengerFile(medias.get(finalI).getUrl(), mContext).toString())
                                .resize(false)
                                .execute();
                    });
                    if (finalI == medias.size() - 1) {
                        ToastUtil.showToast(mContext, mContext.getString(R.string.toast_file_save), false);
                    }
                }
            });
        }
    }

    public void startSave(Media media, OnCallback myOnCallBack) {
        download(media, new OnCallback() {
            @Override
            public void onFail() {
                if (myOnCallBack != null) {
                    myOnCallBack.onFail();
                }
            }

            @Override
            public void onSuccess(@NonNull Bitmap resource) {
                Timber.i("onSuccess image: isRecycled: " + resource.isRecycled());
                new Handler(mContext.getMainLooper()).post(() -> ImageSaver.with(mContext)
                        .load(resource)
                        .into(FileNameUtil.getMessengerFile(media.getUrl(), mContext).toString())
                        .resize(false)
                        .execute());
                if (myOnCallBack != null) {
                    myOnCallBack.onSuccess(resource);
                }
            }
        });
    }

    public void download(Media media, OnCallback onCallback) {
        if (!GlideLoadUtil.isValidContext(mContext)) {
            return;
        }

        if (GlideLoadUtil.isSVGImageUrl(media.getUrl())) {
            GlideRequest<PictureDrawable> request = GlideLoadUtil.createLoadSVGRequest(mContext,
                    media.getUrl(),
                    null,
                    Target.SIZE_ORIGINAL,
                    Target.SIZE_ORIGINAL,
                    false);
            if (request == null) {
                return;
            }

            request = request.listener(new SvgSoftwareLayerSetter() {
                @Override
                public boolean onResourceReady(PictureDrawable resource,
                                               Object model,
                                               Target<PictureDrawable> target,
                                               DataSource dataSource,
                                               boolean isFirstResource) {
                    Timber.i("onResourceReady SVG");
                    if (onCallback != null) {
                        Bitmap bitmap = GlideLoadUtil.drawableToBitmap(resource);
                        Timber.i("bitmap: " + bitmap);
                        onCallback.onSuccess(bitmap);
                    }

                    return super.onResourceReady(resource, model, target, dataSource, isFirstResource);
                }

                @Override
                public boolean onLoadFailed(GlideException e,
                                            Object model,
                                            Target<PictureDrawable> target,
                                            boolean isFirstResource) {
                    Timber.e("Load SVG image failed: " + e.getMessage());
                    if (onCallback != null) {
                        onCallback.onFail();
                    }

                    return super.onLoadFailed(e, model, target, isFirstResource);
                }
            });
            request.submit();
        } else {
            GlideApp.with(mContext)
                    .asBitmap()
                    .load(media.getUrl())
                    .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .listener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e,
                                                    Object model, Target<Bitmap> target,
                                                    boolean isFirstResource) {
                            if (onCallback != null) {
                                onCallback.onFail();
                            }

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource,
                                                       Object model,
                                                       Target<Bitmap> target,
                                                       DataSource dataSource,
                                                       boolean isFirstResource) {
                            if (onCallback != null) {
                                onCallback.onSuccess(resource);
                            }

                            return false;
                        }
                    }).submit();
        }
    }

    public interface OnCallback {
        void onFail();

        void onSuccess(@NonNull Bitmap resource);
    }
}
