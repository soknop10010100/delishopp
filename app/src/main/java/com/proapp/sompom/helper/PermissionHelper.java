package com.proapp.sompom.helper;

import android.Manifest;
import android.content.pm.PackageManager;
import android.view.Window;

import androidx.activity.result.ActivityResultLauncher;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.proapp.sompom.R;
import com.proapp.sompom.licence.db.LicenseSynchronizationDb;
import com.proapp.sompom.licence.model.SynchroniseData;
import com.proapp.sompom.licence.utils.SynchroniseDataPermissionHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Chhom Veasna on 12/9/2019.
 */
public class PermissionHelper {

    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x9190;

    private PermissionHelper() {
    }

    public static void requestAllAppRequirePermissions(AppCompatActivity appCompatActivity,
                                                       ActivityResultLauncher<String[]> permissionRequestLauncher,
                                                       PermissionHelperCallback callback) {
        requestSpecifiedAppPermissions(appCompatActivity,
                getDefaultRequiredAppPermissions(),
                permissionRequestLauncher,
                callback);
    }

    private static List<String> getDefaultRequiredAppPermissions() {
        return Arrays.asList(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_PHONE_STATE);
    }

    /**
     * @param appCompatActivity
     * @param permissionRequestLauncher An {@link ActivityResultLauncher<>} that will launch a permission request popup dialog
     * @param callback
     */
    public static void requestPackageSpecificPermissions(AppCompatActivity appCompatActivity,
                                                         ActivityResultLauncher<String[]> permissionRequestLauncher,
                                                         PermissionHelperCallback callback) {
        // Read the saved synchronize data
        SynchroniseData data = LicenseSynchronizationDb.readSynchroniseData(appCompatActivity);
        if (data != null) {
            // Show the better experience popup before calling the permissionRequestLauncher to
            // request the various required permission from the user.
            requestSpecifiedAppPermissions(appCompatActivity,
                    // This method build a list of permissions based on the current package enabled
                    // features
                    SynchroniseDataPermissionHelper.getRequiredPermissionBasedOnFeature(data),
                    permissionRequestLauncher,
                    callback);
        } else {
            if (callback != null) {
                callback.onNoPermissionToRequest();
            }
        }
    }

    /**
     * @param appCompatActivity
     * @param specifiedPermissions
     * @param permissionRequestLauncher An {@link ActivityResultLauncher<>} that will launch a permission request popup dialog
     * @param callback
     */
    public static void requestSpecifiedAppPermissions(AppCompatActivity appCompatActivity,
                                                      List<String> specifiedPermissions,
                                                      ActivityResultLauncher<String[]> permissionRequestLauncher,
                                                      PermissionHelperCallback callback) {
        String[] requirePermissions =
                checkRequirePermission(appCompatActivity, specifiedPermissions.toArray(new String[0]));
        if (requirePermissions.length > 0) {
            AlertDialog alertDialog = new AlertDialog.Builder(appCompatActivity)
                    .setCancelable(false)
                    .setMessage(appCompatActivity.getString(R.string.popup_permission_require_all_permission_description,
                            appCompatActivity.getString(R.string.app_name)))
                    .setNegativeButton(R.string.popup_permission_do_not_allow_button, (dialog, which) -> {
                        if (callback != null) {
                            callback.onInitialPermissionDialogDenied();
                        }
                    })
                    .setPositiveButton(R.string.popup_ok_button, (dialog, which) ->
                            // Launch the permissionRequestLauncher to request the specified permission
                            permissionRequestLauncher.launch(requirePermissions))
                    .create();
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.show();
        } else {
            // Invoke callback to indicate that there are no permission to request from user
            if (callback != null) {
                callback.onNoPermissionToRequest();
            }
        }
    }

    private static String[] checkRequirePermission(AppCompatActivity context, String... permissions) {
        List<String> requirePermissions = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                requirePermissions.add(permission);
            }
        }

        return requirePermissions.toArray(new String[0]);
    }

    public interface PermissionHelperCallback {
        void onInitialPermissionDialogDenied();

        void onNoPermissionToRequest();
    }
}
