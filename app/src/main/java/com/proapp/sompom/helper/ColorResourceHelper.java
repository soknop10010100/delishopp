package com.proapp.sompom.helper;

import android.content.Context;
import android.view.ContextThemeWrapper;

import androidx.annotation.ColorInt;

import com.desmond.squarecamera.utils.ThemeManager;
import com.proapp.sompom.R;
import com.proapp.sompom.utils.AttributeConverter;

public class ColorResourceHelper {

    public static @ColorInt
    int getAccentColor(Context context) {
        /*
        Must create Context wrapper to get proper color base on theme.
         */
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, ThemeManager.getAppTheme(context).getThemeRes());
        return AttributeConverter.convertAttrToColor(contextThemeWrapper, R.attr.colorAccent);
    }
}
