package com.proapp.sompom.helper;

import android.text.TextUtils;

import com.proapp.sompom.viewmodel.ChatMediaViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageStatusHelper {

    private Map<String, SeenMessageAvatarData> mSeenNameDisplayMap = new HashMap<>();
    private Map<String, RequestSeenNameViewHeightData> mBackupRequestSeenNameViewHeightData = new HashMap<>();

    public int getCalculatedSeenDisplayViewHeight(String chatId, String seenDisplay) {
        SeenMessageAvatarData seenMessageAvatarData = mSeenNameDisplayMap.get(chatId);
        if (seenMessageAvatarData != null && TextUtils.equals(seenDisplay, seenMessageAvatarData.getSeenDisplay())) {
            return seenMessageAvatarData.getCalculatedSeenDisplayViewHeight();
        }

        return -1;
    }

    public void addSeenMessageAvatarData(SeenMessageAvatarData data) {
        mSeenNameDisplayMap.put(data.getChatId(), data);
    }

    public void clearAllData() {
        mSeenNameDisplayMap.clear();
        mBackupRequestSeenNameViewHeightData.clear();
    }

    public void addBackupRequestSeenNameViewHeightData(RequestSeenNameViewHeightData data) {
        mBackupRequestSeenNameViewHeightData.put(data.getChatId(), data);
    }

    public List<RequestSeenNameViewHeightData> getBackupRequestSeenNameViewHeightData() {
        List<RequestSeenNameViewHeightData> values = new ArrayList<>();
        for (RequestSeenNameViewHeightData value : mBackupRequestSeenNameViewHeightData.values()) {
            if (value != null) {
                values.add(value);
            }
        }

        return values;
    }

    public void removeBackupRequestSeenNameViewHeightData(String chatId) {
        mBackupRequestSeenNameViewHeightData.remove(chatId);
    }

    public static class SeenMessageAvatarData {

        private final String mSeenDisplay;
        private final String mChatId;
        private final int mCalculatedSeenDisplayViewHeight;

        public SeenMessageAvatarData(String chatId, String seenDisplay, int calculatedSeenDisplayViewHeight) {
            mSeenDisplay = seenDisplay;
            mChatId = chatId;
            mCalculatedSeenDisplayViewHeight = calculatedSeenDisplayViewHeight;
        }

        public String getSeenDisplay() {
            return mSeenDisplay;
        }

        public String getChatId() {
            return mChatId;
        }

        public int getCalculatedSeenDisplayViewHeight() {
            return mCalculatedSeenDisplayViewHeight;
        }
    }

    public static class RequestSeenNameViewHeightData {

        private final String mChatId;
        private final String mSeenDisplay;
        private final boolean mIsRecipientSide;
        private final ChatMediaViewModel.CalculateSeenNameDisplayCallback mCallback;

        public RequestSeenNameViewHeightData(String chatId,
                                             String seenDisplay,
                                             boolean isRecipientSide,
                                             ChatMediaViewModel.CalculateSeenNameDisplayCallback callback) {
            mChatId = chatId;
            mSeenDisplay = seenDisplay;
            mIsRecipientSide = isRecipientSide;
            mCallback = callback;
        }

        public String getChatId() {
            return mChatId;
        }

        public String getSeenDisplay() {
            return mSeenDisplay;
        }

        public boolean isRecipientSide() {
            return mIsRecipientSide;
        }

        public ChatMediaViewModel.CalculateSeenNameDisplayCallback getCallback() {
            return mCallback;
        }
    }
}
