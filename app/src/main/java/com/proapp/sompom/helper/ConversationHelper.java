package com.proapp.sompom.helper;

import static com.proapp.sompom.newui.ChatActivity.REFRESH_NOTIFICATION_BADGE_ACTION;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proapp.sompom.database.ConversationDb;
import com.proapp.sompom.database.LastMessageIdTempDb;
import com.proapp.sompom.database.MessageDb;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.MetaGroup;
import com.proapp.sompom.model.emun.ConversationType;
import com.proapp.sompom.model.result.AllConversationDisplayDataModel;
import com.proapp.sompom.model.result.Chat;
import com.proapp.sompom.model.result.Conversation;
import com.proapp.sompom.model.result.GetAllConversationResponse;
import com.proapp.sompom.model.result.Theme;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.ConversationUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

public class ConversationHelper {

    public static final String REFRESH_CONVERSATION_LIST_ACTION = "REFRESH_CONVERSATION_LIST_ACTION";
    public static final String GROUP_UPDATE_ACTION = "GROUP_UPDATE_ACTION";
    public static final String IS_REMOVED_ME = "IS_REMOVED_ME";
    public static final String GROUP_CONVERSATION_ID = "GROUP_CONVERSATION_ID";
    public static final String RECEIVE_NEW_SUPPORT_GROUP_EVENT = "RECEIVE_NEW_SUPPORT_GROUP_EVENT";
    public static final String NEW_SUPPORT_GROUP_ID = "NEW_SUPPORT_GROUP_ID";

    private ConversationHelper() {
    }

    public static void checkOrderConversation(Context context,
                                              String existConversationId,
                                              ApiService apiService,
                                              CheckOrderConversationListener listener) {

        if (!TextUtils.isEmpty(existConversationId)) {
            Conversation existConversation = ConversationDb.getConversationById(context, existConversationId);
            if (existConversation != null) {
                if (listener != null) {
                    listener.onCheckCompleted(existConversation);
                }
            } else {
                requestConversationOfUser(context, existConversationId,
                        apiService,
                        new OnCallbackListener<Response<Conversation>>() {
                            @Override
                            public void onFail(ErrorThrowable ex) {
                                if (listener != null) {
                                    listener.onCheckFailed(ex);
                                }
                            }

                            @Override
                            public void onComplete(Response<Conversation> result) {
                                if (listener != null) {
                                    listener.onCheckCompleted(result.body());
                                }
                            }
                        });
            }
        }
    }

    public static boolean checkToUpdateGroupData(Context context,
                                                 String conversationId,
                                                 MetaGroup group,
                                                 Chat chat,
                                                 String tag) {
        Chat.ChatType modificationType = Chat.ChatType.from(chat.getChatType());
        Conversation conversation = ConversationDb.getConversationById(context, conversationId);
        if (conversation != null) {
            Timber.i("checkToUpdateGroupData: " + tag);
            boolean isRemovedMe = false;
            if (modificationType == Chat.ChatType.CHANGE_GROUP_NAME) {
                conversation.setGroupName(group.getName());
            } else if (modificationType == Chat.ChatType.CHANGE_GROUP_PICTURE) {
                conversation.setGroupImageUrl(group.getProfilePhoto());
            } else if (modificationType == Chat.ChatType.ADD_GROUP_MEMBER) {
                if (conversation.getParticipants() == null) {
                    conversation.setParticipants(new ArrayList<>());
                }
                for (User participant : group.getParticipants()) {
                    boolean existed = false;
                    for (User user1 : conversation.getParticipants()) {
                        if (TextUtils.equals(user1.getId(), participant.getId())) {
                            existed = true;
                            break;
                        }
                    }
                    if (!existed) {
                        conversation.getParticipants().add(participant);
                    }
                }
            } else if (modificationType == Chat.ChatType.REMOVE_GROUP_MEMBER) {
                /*
                Check if current user was removed once before since the duplication message from either
                Socket and OneSignal.
                 */
                String userId = SharedPrefUtils.getUserId(context);
                boolean isContainCurrentUserInRemoveList = false;
                for (User participant : group.getParticipants()) {
                    if (TextUtils.equals(participant.getId(), userId)) {
                        isContainCurrentUserInRemoveList = true;
                        break;
                    }
                }

                if (!isConversationOfCurrentUser(context, conversation) && isContainCurrentUserInRemoveList) {
                    Timber.i("Current user was already removed once from this group before.");
                    isRemovedMe = true;
                } else {
                    if (conversation.getParticipants() == null) {
                        conversation.setParticipants(new ArrayList<>());
                    }
                    for (int size = conversation.getParticipants().size() - 1; size >= 0; size--) {
                        for (User participant : group.getParticipants()) {
                            if (TextUtils.equals(participant.getId(), conversation.getParticipants().get(size).getId())) {
                                if (TextUtils.equals(userId, participant.getId())) {
                                    isRemovedMe = true;
                                    conversation.setRemoved(true);
                                }
                                conversation.getParticipants().remove(size);
                                break;
                            }
                        }
                    }
                }
            }

            //Need to update content of conversation with last message update.
            conversation.setContent(chat.getActualContent(context, conversation, true));
            ConversationDb.save(context, conversation, "checkToUpdateGroupData");
            //Send broadcast to all client
            Timber.i("isRemovedMe: " + isRemovedMe + ", conversation.getRemoved(): " + conversation.getRemoved());
            maintainTheLastMessageOfConversation(conversation, chat);
            Intent intent1 = new Intent(GROUP_UPDATE_ACTION);
            intent1.putExtra(IS_REMOVED_ME, isRemovedMe);
            intent1.putExtra(SharedPrefUtils.DATA, conversation);
            intent1.putExtra(GROUP_CONVERSATION_ID, conversation.getId());
            SendBroadCastHelper.verifyAndSendBroadCast(context, intent1);

            /*
            If the current user was removed from any group, then we should check to update
            the conversation badge too.
             */
            if (isRemovedMe) {
                SendBroadCastHelper.verifyAndSendBroadCast(context, new Intent(REFRESH_NOTIFICATION_BADGE_ACTION));
            }

            return isRemovedMe;
        }

        return false;
    }

    public static void onGroupConversationRemoved(Context context, String conversationId) {
        Conversation conversation = ConversationDb.getConversationById(context, conversationId);
        if (conversation != null) {
            conversation.setRemoved(true);
            ConversationDb.save(context, conversation, "onGroupConversationRemoved");

            Intent intent1 = new Intent(GROUP_UPDATE_ACTION);
            intent1.putExtra(IS_REMOVED_ME, true);
            intent1.putExtra(SharedPrefUtils.DATA, conversation);
            intent1.putExtra(GROUP_CONVERSATION_ID, conversation.getId());
            SendBroadCastHelper.verifyAndSendBroadCast(context, intent1);
            SendBroadCastHelper.verifyAndSendBroadCast(context, new Intent(REFRESH_NOTIFICATION_BADGE_ACTION));
            Timber.i("onGroupConversationRemoved");
        }
    }

    public static void broadcastUpdateConversationListAction(Context context) {
        SendBroadCastHelper.verifyAndSendBroadCast(context, new Intent(REFRESH_CONVERSATION_LIST_ACTION));
    }

    public static boolean isAddMeToGroup(Context context, Chat chat, MetaGroup group) {
        Chat.ChatType modificationType = Chat.ChatType.from(chat.getChatType());
        if (modificationType == Chat.ChatType.ADD_GROUP_MEMBER) {
            String userId = SharedPrefUtils.getUserId(context);
            if (group.getParticipants() != null) {
                for (User participant : group.getParticipants()) {
                    if (TextUtils.equals(participant.getId(), userId)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /*
    Since the last message of conversation is not managed to save in local db. We have to maintain it
    manually.
     */
    public static void maintainTheLastMessageOfConversation(Conversation conversation, Chat lastMessage) {
        if (conversation != null) {
            //Will have to set value to both last message and its content fields.
            conversation.setLastMessage(lastMessage);
        }
    }

    public static boolean isGroupModificationMessageType(Chat chat) {
        Chat.ChatType chatType = Chat.ChatType.from((chat).getChatType());
        return chatType == Chat.ChatType.ADD_GROUP_MEMBER ||
                chatType == Chat.ChatType.REMOVE_GROUP_MEMBER ||
                chatType == Chat.ChatType.CHANGE_GROUP_PICTURE ||
                chatType == Chat.ChatType.CHANGE_GROUP_NAME;
    }

    public static void checkIfConversationExistedAndBelongToCurrentUser(Context context,
                                                                        String conversationId,
                                                                        ApiService apiService,
                                                                        ConversationHelperListener listener) {
        if (TextUtils.isEmpty(conversationId)) {
            //Ignore if there is conversation id
            return;
        }

        if (ConversationDb.isConversationExisted(context, conversationId)) {
            if (isConversationOfCurrentUser(context, conversationId)) {
                if (listener != null) {
                    listener.onConversationIsValid();
                }
            }
        } else {
            requestConversationOfUser(context,
                    conversationId,
                    apiService, new OnCallbackListener<Response<Conversation>>() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                        }

                        @Override
                        public void onComplete(Response<Conversation> result) {
                            if (result != null) {
                                if (listener != null) {
                                    listener.onConversationIsValid();
                                }
                            }
                        }
                    });
        }
    }

    public static void requestConversationOfUser(Context context,
                                                 String conversationId,
                                                 ApiService apiService,
                                                 OnCallbackListener<Response<Conversation>> callback) {
        Timber.i("Request conversation from server since it is not exist in local db.");
        Observable<Response<Conversation>> ob = apiService.getConversationDetail(conversationId);
        BaseObserverHelper<Response<Conversation>> helper = new BaseObserverHelper<>(context, ob);
        helper.execute(new OnCallbackListener<Response<Conversation>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e(ex);
                if (callback != null) {
                    callback.onFail(ex);
                }
            }

            @Override
            public void onComplete(Response<Conversation> result) {
                if (result.body() != null) {
                    if (result.body().isDeleted()) {
                        Timber.i("Deleted conversation");
                        if (callback != null) {
                            callback.onComplete(null);
                        }
                    } else {
                        if (isConversationOfCurrentUser(context, result.body())) {
                            Timber.i("isConversationOfCurrentUser");
                            if (ConversationType.fromValue(result.body().getType()) == ConversationType.SUPPORT) {
                                Timber.i("Will broadcast RECEIVE_NEW_SUPPORT_GROUP_EVENT");
                                Intent intent = new Intent(RECEIVE_NEW_SUPPORT_GROUP_EVENT);
                                intent.putExtra(NEW_SUPPORT_GROUP_ID, result.body().getId());
                                SendBroadCastHelper.verifyAndSendBroadCast(context, intent);
                            }
                            ConversationDb.save(context, result.body(),
                                    "Request group detail from server.");
                            if (callback != null) {
                                callback.onComplete(result);
                            }
                        } else {
                            if (callback != null) {
                                callback.onComplete(null);
                            }
                        }
                    }
                } else {
                    if (callback != null) {
                        callback.onComplete(null);
                    }
                }
            }
        });
    }

    public static boolean isConversationOfCurrentUser(Context context, Conversation conversation) {
        boolean isConversationOfCurrentUser = false;
        String userId = SharedPrefUtils.getUserId(context);
        if (conversation == null || conversation.isDeleted() || conversation.getRemoved()) {
            isConversationOfCurrentUser = false;
        }

        if (conversation.isGroup()) {
            if (conversation.getParticipants() != null) {
                for (User participant : conversation.getParticipants()) {
                    if (TextUtils.equals(participant.getId(), userId)) {
                        isConversationOfCurrentUser = true;
                        break;
                    }
                }
            }
        } else {
            isConversationOfCurrentUser = ConversationUtil.isOneToOneConversationOfUser(conversation.getId(), userId);
            Timber.i("isConversationOfCurrentUser of one to one conversation id: " + isConversationOfCurrentUser);
        }

        Timber.i("isConversationOfCurrentUser: " + isConversationOfCurrentUser + ", conversation: " + new Gson().toJson(conversation));
        return isConversationOfCurrentUser;
    }

    public static boolean isConversationOfCurrentUser(Context context, String conversationId) {
        return isConversationOfCurrentUser(context, ConversationDb.getConversationById(context, conversationId));
    }

    public static void saveUserGroupConversationId(Context context, List<String> idsList) {
        SharedPrefUtils.setString(context, SharedPrefUtils.GROUP_CONVERSATION_IDS, new Gson().toJson(idsList));
    }

    public static List<String> getUserGroupConversationId(Context context) {
        String data = SharedPrefUtils.getString(context, SharedPrefUtils.GROUP_CONVERSATION_IDS);
        List<String> ids = null;
        if (!TextUtils.isEmpty(data)) {
            ids = new Gson().fromJson(data, new TypeToken<List<String>>() {
            }.getType());
        }

        if (ids == null) {
            ids = new ArrayList<>();
        }

        //TODO: Currently we don't allow to chat with support without user login.
        //Check to add support conversation id if necessary
//        ConciergeShopSetting conciergeShopSetting = ConciergeHelper.getConciergeShopSetting(context);
//        if (conciergeShopSetting != null && !TextUtils.isEmpty(conciergeShopSetting.getSupportConversationId())) {
//            if (!ids.contains(conciergeShopSetting.getSupportConversationId())) {
//                ids.add(conciergeShopSetting.getSupportConversationId());
//            }
//        }

        return ids;
    }

    public static String getSupportConversationProfile(Context context) {
        Theme theme = UserHelper.getSelectedThemeFromAppSetting(context);
        if (theme != null && !TextUtils.isEmpty(theme.getCompanyAvatar())) {
            return theme.getCompanyAvatar();
        }

        return null;
    }

    public static void saveLastLocalMessageId(Context context, String conversationId, boolean willOverwrite) {
         /*
        Since the message that is received via notification will not be saved into local database.
        So in case, the app is not yet open and user receive message and try to make reply to
        that message or to make sure that when user enter screen all new message receive via notification will be loaded,
        we have to save last local message of that conversation before a reply will be managed
        to send over socket and save into local db. By doing this, when user open app again and go to that
        conversation, we will try to request all new messages from that last message of local we previously
        save and as result, we don't lost any message in case that user reply message via Notification directly.
        **Must be called before sending reply message.
         */
        Chat lastMessageOfConversation = MessageDb.getLastMessageOfConversationThatSent(context, conversationId);
        LastMessageIdTempDb.saveLastMessageId(context, lastMessageOfConversation, willOverwrite);
    }

    public static AllConversationDisplayDataModel parseAllConversationResponse(Response<GetAllConversationResponse> response,
                                                                               boolean isLoadMore) {
        if (response != null && response.body() != null) {
            AllConversationDisplayDataModel displayDataModel = new AllConversationDisplayDataModel();
            int recentLimit = getValidRecentConversationDisplayCount(response);
            if (recentLimit > 0 && !isLoadMore) {
                List<Conversation> recentConversations = new ArrayList<>();
                List<Conversation> moreConversations = new ArrayList<>();
                //Filter only recent conversations
                for (int i = 0; i < recentLimit; i++) {
                    recentConversations.add(response.body().getData().get(i));
                }
                //Filter only more conversations
                for (int i = recentLimit; i < response.body().getData().size(); i++) {
                    moreConversations.add(response.body().getData().get(i));
                }

                response.body().setData(moreConversations);
                displayDataModel.setMoreConversation(response.body());
                displayDataModel.setRecentConversation(recentConversations);
            } else {
                displayDataModel.setMoreConversation(response.body());
                //No recent message display
                displayDataModel.setRecentConversation(new ArrayList<>());
            }

            return displayDataModel;
        }

        return null;
    }

    public static int getValidRecentConversationDisplayCount(Response<GetAllConversationResponse> response) {
        if (response != null && response.body() != null) {
            return Math.min(response.body().getData().size(), response.body().getRecentLimit());
        }

        return 0;
    }

    public interface ConversationHelperListener {
        //Valid conversation is the conversation of current user and exist in local db.
        void onConversationIsValid();
    }

    public interface CheckOrderConversationListener {
        void onCheckCompleted(Conversation conversation);

        void onCheckFailed(ErrorThrowable ex);
    }
}
