package com.proapp.sompom.helper;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.desmond.squarecamera.utils.media.FileNameUtil;
import com.proapp.sompom.model.Media;

import java.io.File;

import timber.log.Timber;

/**
 * Created by nuonveyo on 9/17/18.
 */

public class SaveVideoHelper {
    private Context mContext;

    public SaveVideoHelper(Context context) {
        mContext = context;
    }


    public void download(Media media, OnVideoSaveCallBack onVideoSaveCallBack) {
        Timber.e("URL of video: " + media.getUrl());
        Timber.e("Path of video: " + FileNameUtil.getMessengerFile(media.getUrl(), mContext));
        new FileDownloadHelper(media.getUrl(),
                media.getId(),
                FileNameUtil.getMessengerFile(media.getUrl(), mContext),
                media.getSize(),
                new FileDownloadHelper.FileDownloadListener() {
                    @Override
                    public void onDownloadGetStarted(String requestId, String downloadId) {

                    }

                    @Override
                    public void onDownloadSuccess(String requestId, String downloadId, File file) {
                        Timber.e("Download successful");
                        Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                        mediaScannerIntent.setData(Uri.parse("file://" + file.getAbsolutePath()));
                        Timber.e("Scan directory: " + mediaScannerIntent.getData().getPath());
                        mContext.sendBroadcast(mediaScannerIntent);
                        onVideoSaveCallBack.onSuccess();
                    }

                    @Override
                    public void onDownloadFailed(String requestId, String downloadId, Throwable ex) {
                        onVideoSaveCallBack.onFail(ex.getMessage());
                    }

                    @Override
                    public void onDownloadInProgress(String requestId, String downloadId, long downloadedBytes, long totalBytes) {

                    }
                }).performDownload();
    }

    public interface OnVideoSaveCallBack {
        void onFail(String message);

        void onSuccess();
    }
}
