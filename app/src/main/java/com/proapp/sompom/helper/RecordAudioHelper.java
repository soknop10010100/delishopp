package com.proapp.sompom.helper;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.MediaRecorder;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.BlendModeColorFilterCompat;
import androidx.core.graphics.BlendModeCompat;

import com.desmond.squarecamera.utils.FormatTime;
import com.proapp.sompom.R;
import com.proapp.sompom.broadcast.SoundEffectService;
import com.proapp.sompom.broadcast.chat.PlayAudioService;
import com.proapp.sompom.listener.OnClickListener;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.AbsSoundControllerActivity;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.MediaUtil;
import com.proapp.sompom.utils.ToastUtil;
import com.proapp.sompom.widget.chat.CustomVoiceRippleView;

import java.io.File;

import info.kimjihyok.ripplelibrary.Rate;
import info.kimjihyok.ripplelibrary.renderer.CircleRippleRenderer;
import info.kimjihyok.ripplelibrary.renderer.Renderer;
import timber.log.Timber;

/**
 * Created by nuonveyo on 9/18/18.
 */

public class RecordAudioHelper {
    private static final int ONE_SECOND = 1000;
    private static final double RIPPLE_RATIO = 1.45;

    private final Activity mContext;
    private final CustomVoiceRippleView mVoiceRipple;
    private final OnCallback mOnClickListener;
    private final TextView mTextView;
    private final View mSlideView;
    private final AudioGestureListener mAudioGestureListener;

    private Type mType;
    private File mRecordFile;
    private boolean mIsActionRecorded;
    private boolean mIsAbleToSendVoice = true;

    public RecordAudioHelper(Activity context,
                             CustomVoiceRippleView voiceRippleView,
                             TextView textView,
                             View slideView,
                             OnCallback onClickListener) {
        mContext = context;
        mVoiceRipple = voiceRippleView;
        mTextView = textView;
        mSlideView = slideView;
        mOnClickListener = onClickListener;
        mVoiceRipple.setRecordingListener(new CustomVoiceRippleView.OnRecordingListener() {
            @Override
            public void onRecordingStopped(File fileRecorded) {
                if (MediaUtil.getAudioDuration(fileRecorded.getAbsolutePath()) < ONE_SECOND) {
                    Timber.e("delete file record: %s", fileRecorded.delete());
                    mOnClickListener.onStopRecord(null);
                } else {
                    mRecordFile = fileRecorded;
                    if (mOnClickListener != null) {
                        mOnClickListener.onStopRecord(mRecordFile);
                    }
                }
            }

            @Override
            public void onRecordingStarted() {
                if (!mIsAbleToSendVoice) {
                    return;
                }

                mOnClickListener.onStartRecord();
            }
        });

        // set view related settings for ripple view
        mVoiceRipple.setRippleSampleRate(Rate.MEDIUM);
        mVoiceRipple.setRippleDecayRate(Rate.HIGH);
        mVoiceRipple.setBackgroundRippleRatio(RIPPLE_RATIO);
        mVoiceRipple.setIconSize(mContext.getResources().getInteger(R.integer.chat_icon_size));
        setIcon(Type.RECORD);

        // set recorder related settings for ripple view
        mVoiceRipple.setMediaRecorder(new MediaRecorder());
        mVoiceRipple.setAudioSource(MediaRecorder.AudioSource.MIC);
        mVoiceRipple.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mVoiceRipple.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        setRenderer();

        mAudioGestureListener = new AudioGestureListener(mVoiceRipple.getContext()) {
            @Override
            public void onCancelRecord() {
                mVoiceRipple.reset(false);
                mVoiceRipple.deleteCurrentVoiceRecord();
                mOnClickListener.onStopRecord(null);
                super.onCancelRecord();
                SoundEffectService.playSound(mContext, SoundEffectService.SoundFile.WOOSH);
            }

            @Override
            public void onStopRecord() {
                if (mVoiceRipple.isRecording()) {
                    mVoiceRipple.reset(true);
                }
                super.onStopRecord();
            }

            @Override
            void onStartRecord() {
                onStartRecordClick();
            }

            @Override
            void onRecordingDuration(int second) {
                mTextView.setText(FormatTime.getFormattedDuration(second));
            }

            @Override
            void onClick() {
                if (mType == Type.RECORD && !mIsAbleToSendVoice) {
                    return;
                }

                if (mType == Type.RECORD) {
                    if (isRecordAudioPermissionGranted()) {
                        SoundEffectService.playSound(mContext, SoundEffectService.SoundFile.MATCH);
                        ToastUtil.showToast(mContext, R.string.chat_toast_voice_record_hint, false);
                    } else {
                        if (!mIsActionRecorded) {
                            mIsActionRecorded = true;
                            requestRecordAudioPermission();
                        }
                    }
                } else {
                    if (mOnClickListener != null) {
                        mOnClickListener.onClick();
                    }
                }
            }

            @Override
            void onTranslationXUpdate(float px) {
                mSlideView.setTranslationX(px);
            }

            @Override
            boolean isEnableTranslation() {
                return mType == Type.RECORD;
            }

            @Override
            void onTouch() {
                mIsActionRecorded = false;
            }
        };
        mVoiceRipple.setOnTouchListener(mAudioGestureListener);
    }

    public void setRenderer() {
        Renderer normalRenderer = new CircleRippleRenderer(getDefaultRipplePaint(),
                getDefaultRippleBackgroundPaint(false),
                getButtonPaint());
        Renderer recordingRenderer = new CircleRippleRenderer(getDefaultRipplePaint(),
                getDefaultRippleBackgroundPaint(true),
                getButtonPaint());
        mVoiceRipple.setNormalStateRenderer(normalRenderer);
        mVoiceRipple.setRecordingRenderer(recordingRenderer);
    }

    public void setAbleToSendVoice(boolean ableToSendVoice) {
        mIsAbleToSendVoice = ableToSendVoice;
    }

    private void onStartRecordClick() {
        if (mType == Type.RECORD && !mIsActionRecorded && mContext instanceof AbsBaseActivity) {
            mIsActionRecorded = true;
            if (isRecordAudioPermissionGranted()) {
                checkToStopAudioPlaying();
                mVoiceRipple.startRecording();
            } else {
                requestRecordAudioPermission();
            }
        }
    }

    private boolean isRecordAudioPermissionGranted() {
        return ContextCompat.checkSelfPermission(mContext, Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestRecordAudioPermission() {
        ((AbsBaseActivity) mContext).checkPermission(new CheckPermissionCallbackHelper(mContext,
                CheckPermissionCallbackHelper.Type.MICROPHONE) {
            @Override
            public void onPermissionGranted() {
                Timber.e("permission granted");
            }

        }, Manifest.permission.RECORD_AUDIO);
    }

    public void setMinScroll(int minScroll) {
        mAudioGestureListener.setMinScroll(minScroll);
    }

    private void checkToStopAudioPlaying() {
        PlayAudioService service = ((AbsSoundControllerActivity) mContext).getPlayAudioService();
        if (service != null && service.isPlaying()) service.stop();
    }

    public void destroy() {
        if (mRecordFile != null && mRecordFile.exists()) {
            Timber.e("delete record file: %s",
                    mRecordFile.delete());
        }

        try {
            if (mVoiceRipple != null) {
                mVoiceRipple.onStop();
                mVoiceRipple.onDestroy();
            }
        } catch (Exception e) {
            Timber.e("destroy error: %s", e.getMessage());
            SentryHelper.logSentryError(e);
        }
    }

    public void setIcon(Type type) {
        mType = type;
        Drawable icon = getDrawable(type.getIcon());
        if (icon != null) {
            int colorFilter;
            //Applied the color filter
            if (type == Type.RECORD) {
                colorFilter = AttributeConverter.convertAttrToColor(mContext, R.attr.chat_audio_button);
            } else {
                colorFilter = AttributeConverter.convertAttrToColor(mContext, R.attr.chat_send_button);
            }
            icon.setColorFilter(BlendModeColorFilterCompat.createBlendModeColorFilterCompat(colorFilter,
                    BlendModeCompat.SRC_ATOP));
        }
        mVoiceRipple.setRecordDrawable(icon, icon);
    }

    public void checkToCancelRecord() {
        if (mVoiceRipple != null && mVoiceRipple.isRecording()) {
            mAudioGestureListener.onCancelRecord();
        }
    }

    private Paint getDefaultRipplePaint() {
        Paint ripplePaint = new Paint();
        ripplePaint.setStyle(Paint.Style.FILL);
        ripplePaint.setColor(Color.TRANSPARENT);
        ripplePaint.setAntiAlias(true);
        return ripplePaint;
    }

    private Paint getDefaultRippleBackgroundPaint(boolean forRenderingMode) {
        Paint rippleBackgroundPaint = new Paint();
        rippleBackgroundPaint.setStyle(Paint.Style.FILL);
        rippleBackgroundPaint.setColor(forRenderingMode ? AttributeConverter.convertAttrToColor(mContext, R.attr.chat_record_wave_effect) : Color.TRANSPARENT);
        rippleBackgroundPaint.setAntiAlias(true);
        return rippleBackgroundPaint;
    }

    private Paint getButtonPaint() {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.TRANSPARENT);
        paint.setStyle(Paint.Style.FILL);
        return paint;
    }

    private Drawable getDrawable(@DrawableRes int drawable) {
        return ContextCompat.getDrawable(mContext, drawable);
    }

    public enum Type {
        RECORD(R.drawable.ic_mic_icon),
        SEND(R.drawable.ic_green_send);

        @DrawableRes
        private final int mIcon;

        Type(int icon) {
            mIcon = icon;
        }

        public int getIcon() {
            return mIcon;
        }
    }

    public interface OnCallback extends OnClickListener {
        void onStopRecord(File recordFile);

        void onStartRecord();
    }
}
