package com.proapp.sompom.helper.upload;

import android.content.Context;

import com.proapp.sompom.R;

/**
 * Created by Chhom Veasna on 9/2/2019.
 */
public class SharedUploader {

    Context mContext;

    public SharedUploader(Context context) {
        mContext = context;
    }

    public String getUploadFolder() {
        return mContext.getString(R.string.cloudinary_folder) + "/";
    }
}
