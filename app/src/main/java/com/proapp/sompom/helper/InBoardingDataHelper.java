package com.proapp.sompom.helper;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proapp.sompom.R;
import com.proapp.sompom.licence.utils.FileUtils;
import com.proapp.sompom.model.InBoardItem;
import com.proapp.sompom.model.result.InBoardItemWrapper;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Chhom Veasna on 5/29/2020.
 */
public final class InBoardingDataHelper {

    private static List<String> sResourceNameList = new ArrayList<>();

    static {
        sResourceNameList.add("inboard_01");
        sResourceNameList.add("inboard_02");
        sResourceNameList.add("inboard_03");
        sResourceNameList.add("inboard_04");
    }

    public static List<InBoardItem> getInBoardingData(List<InBoardItemWrapper> inBoardItemWrapperList) {
        String systemLanguageCode = com.resourcemanager.helper.LocaleManager.getValidDeviceLangeCode();
        Timber.i("systemLanguageCode: " + systemLanguageCode);
        for (InBoardItemWrapper inBoardItemWrapper : inBoardItemWrapperList) {
            if (TextUtils.equals(inBoardItemWrapper.getLanguage(), systemLanguageCode)) {
                embedLocalImageResource(inBoardItemWrapper.getInBoardItemList());
                return inBoardItemWrapper.getInBoardItemList();
            }
        }
        return new ArrayList<>();
    }

    public static List<InBoardItemWrapper> getLocalInBoardingData(Context context) {
        List<InBoardItemWrapper> inBoardItemWrapperList = new Gson().fromJson(FileUtils.readRawTextFile(context,
                R.raw.inboarding),
                new TypeToken<List<InBoardItemWrapper>>() {
                }.getType());
        if (inBoardItemWrapperList != null) {
            for (InBoardItemWrapper inBoardItemWrapper : inBoardItemWrapperList) {
                embedLocalImageResource(inBoardItemWrapper.getInBoardItemList());
            }
        }

        return inBoardItemWrapperList;
    }

    /*
    Inboarding use the local embed image resources.
     */
    public static void embedLocalImageResource(List<InBoardItem> inBoardItems) {
        if (inBoardItems != null) {
            for (int i = 0; i < inBoardItems.size(); i++) {
                if (i < sResourceNameList.size()) {
                    inBoardItems.get(i).setImage(sResourceNameList.get(i));
                }
            }
        }
    }
}
