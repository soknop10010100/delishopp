package com.proapp.sompom.helper;

import android.content.Context;

import com.proapp.sompom.model.result.AppSetting;
import com.proapp.sompom.model.result.DateList;
import com.proapp.sompom.model.result.TimeList;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.Calendar;
import java.util.Date;

public class WorkingTimeNotificationHelper {

    /*
        If the notification is in working time then will dispatch notification with sound and vibration
        base on device setting. Otherwise, will dispatch in silent notification mode only which mean
        no sound and vibration to avoid user annoying during their break time.
     */
    public static boolean isValidToDispatchLocalNotification(Context context) {
        //There is no other criteria to check for Delishop.
        return true;

//        AppSetting appSetting = SharedPrefUtils.getAppSetting(context);
//        if (appSetting == null || appSetting.getNotificationSetting() == null) {
//            return false;
//        } else {
//            /*
//            Will check if the current time is in allowing notification base on notification time
//            setting from server.
//            1. Define Day of Week response from server:
//            Sunday = 0
//            ......
//            Saturday = 6
//
//           2. And Day of week response from Android native framework is starting from 1 which is why
//           we have to always minus 1 to have the same value as server response.
//             */
//            int todayWeekDay = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
////            Timber.i("Today weekday: " + todayWeekDay);
//
//            // Loop through all dateList in notificationSetting
//            for (DateList dateList : appSetting.getNotificationSetting().getDateList()) {
//                if (!dateList.isActive() || (dateList.getTimeList() == null || dateList.getTimeList().isEmpty())) {
//                    continue;
//                }
//
//                // If it is today and is active status, start to check the time within
//                if (dateList.getWeekday() == todayWeekDay) {
//                    for (TimeList timeList : dateList.getTimeList()) {
//                        // We will take only hour and minute to compare.
//                        Date currentDate = keepOnlyTimeOfDate(new Date());
//                        Date startDate = keepOnlyTimeOfDate(timeList.getStartTime());
//                        Date endDate = keepOnlyTimeOfDate(timeList.getEndTime());
////                        Timber.i("currentDate: " + currentDate + ", startDate: " + startDate + ", endDate: " + endDate);
//                        if (currentDate.getTime() >= startDate.getTime() && currentDate.getTime() <= endDate.getTime()) {
//                            return true;
//                        }
//                    }
//                }
//            }
//        }
//
//        return false;
    }

    private static Date keepOnlyTimeOfDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.YEAR, 0);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_YEAR, 0);

        return calendar.getTime();
    }
}
