package com.proapp.sompom.helper;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.proapp.sompom.MainApplication;
import com.proapp.sompom.R;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.model.ConciergeCartModel;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.model.NotificationOrder;
import com.proapp.sompom.model.SearchAddressResult;
import com.proapp.sompom.model.ShopDeliveryTime;
import com.proapp.sompom.model.ShopDeliveryTimeSelection;
import com.proapp.sompom.model.concierge.BasketTimeSlot;
import com.proapp.sompom.model.concierge.ConciergeBasket;
import com.proapp.sompom.model.concierge.ConciergeBasketMenuItem;
import com.proapp.sompom.model.concierge.ConciergeComboChoiceSection;
import com.proapp.sompom.model.concierge.ConciergeComboItem;
import com.proapp.sompom.model.concierge.ConciergeDeliverySlot;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItemHistory;
import com.proapp.sompom.model.concierge.ConciergeMenuItemManipulationInfo;
import com.proapp.sompom.model.concierge.ConciergeOrderHistoryComboItem;
import com.proapp.sompom.model.concierge.ConciergeOrderRequest;
import com.proapp.sompom.model.concierge.ConciergeOrderRequestWithABAPayWay;
import com.proapp.sompom.model.concierge.ConciergeOrderResponseWithABAPayWay;
import com.proapp.sompom.model.concierge.ConciergePreferenceDelivery;
import com.proapp.sompom.model.concierge.ConciergePreferredSlot;
import com.proapp.sompom.model.concierge.ConciergeRequestOrderMenuItem;
import com.proapp.sompom.model.concierge.ConciergeRequestOrderMenuItemChoice;
import com.proapp.sompom.model.concierge.ConciergeShop;
import com.proapp.sompom.model.concierge.ConciergeShopSetting;
import com.proapp.sompom.model.concierge.ConciergeSupplier;
import com.proapp.sompom.model.concierge.ConciergeUpdateBasketRequest;
import com.proapp.sompom.model.concierge.ConciergeUserAddress;
import com.proapp.sompom.model.concierge.ConciergeVerifyPaymentWithABAResponse;
import com.proapp.sompom.model.emun.ConciergeMenuItemType;
import com.proapp.sompom.model.emun.ConciergeOrderStatus;
import com.proapp.sompom.model.emun.ConciergeSortItemOption;
import com.proapp.sompom.model.emun.ConciergeViewItemByType;
import com.proapp.sompom.model.emun.ShopDeliveryType;
import com.proapp.sompom.model.result.AppSetting;
import com.proapp.sompom.model.result.ConciergeCheckoutData;
import com.proapp.sompom.model.result.ConciergeMenuItemOption;
import com.proapp.sompom.model.result.ConciergeMenuItemOptionSection;
import com.proapp.sompom.model.result.ConciergeOrder;
import com.proapp.sompom.model.result.ConciergeOrderItem;
import com.proapp.sompom.model.result.SupportConciergeCustomErrorResponse;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.newui.AbsBaseActivity;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.newui.fragment.AbsSupportShopCheckOutFragment;
import com.proapp.sompom.observable.BaseObserverHelper;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.services.datamanager.DeliveryDataManager;
import com.proapp.sompom.utils.AttributeConverter;
import com.proapp.sompom.utils.DateUtils;
import com.proapp.sompom.utils.HTTPResponse;
import com.proapp.sompom.utils.LocationStateUtil;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.resourcemanager.helper.LocaleManager;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import timber.log.Timber;

public class ConciergeHelper {

    public static final String N_A = "N/A";
    public static final String CONCIERGE_SHOP_SETTING = "CONCIERGE_SHOP_SETTING";
    public static final int CONCIERGE_FEATURED_STORE_ITEM = 10;
    public static final String REFRESH_PRODUCT_COUNTER_EVENT = "REFRESH_PRODUCT_COUNTER_EVENT";
    public static final String REFRESH_PRODUCT_COUNTER_ITEM = "REFRESH_PRODUCT_COUNTER_ITEM";
    public static final String SHOW_TRACKING_ORDER_BUTTON_EVENT = "SHOW_TRACKING_ORDER_BUTTON_EVENT";
    public static final String SHOW_TRACKING_ORDER_BUTTON_EVENT_DATA = "SHOW_TRACKING_ORDER_BUTTON_EVENT_DATA";

    public static final String UPDATE_ORDER_STATUS_EVENT = "UPDATE_ORDER_STATUS_EVENT";
    public static final String UPDATE_ORDER_STATUS_EVENT_DATA = "UPDATE_ORDER_STATUS_EVENT_DATA";


    public static final String ADDRESS_CREATED_EVENT = "ADDRESS_CREATED_EVENT";
    public static final String ADDRESS_UPDATED_EVENT = "ADDRESS_UPDATED_EVENT";
    public static final String ADDRESS_DELETED_EVENT = "ADDRESS_DELETED_EVENT";
    public static final String ADDRESS_SELECTED_EVENT = "ADDRESS_SELECTED_EVENT";
    public static final String ADDRESS_EVENT_DATA = "ADDRESS_EVENT_DATA";
    public static final String ADDRESS_IS_CURRENT_LOCATION_DATA = "ADDRESS_IS_CURRENT_LOCATION_DATA";

    public static final String SHOP_STATUS_UPDATED_EVENT = "SHOP_STATUS_UPDATED_EVENT";
    public static final String FIELD_IS_SHOP_CLOSED = "isClosed";

    public static final String KEY_BASKET_ID = "KEY_BASKET_ID";
    public static final String IS_FROM_ROLLBACK_PRODUCT_COUNT = "IS_FROM_ROLLBACK_PRODUCT_COUNT";

    public static final int SHOP_NOTICE_BANNER_INDEX = 1;

    public static final int SHOW_IN_AND_OUT_OF_STOCK_ITEM = 0;
    public static final int SHOW_ONLY_IN_STOCK_ITEM = 1;

    private static final List<String> sDisplayingUpdateItemErrorIds = new ArrayList<>();
    private static boolean sShowOutOfStockProduct = false;
    private static ConciergeSortItemOption sSortItemOption;

    public static void addDisplayingUpdateItemErrorId(String itemId) {
        if (!sDisplayingUpdateItemErrorIds.contains(itemId)) {
            sDisplayingUpdateItemErrorIds.add(itemId);
        }
    }

    public static void removeDisplayingUpdateItemErrorId(String itemId) {
        sDisplayingUpdateItemErrorIds.remove(itemId);
    }

    public static void clearAllDisplayingUpdateItemErrorId() {
        sDisplayingUpdateItemErrorIds.clear();
    }

    public static void setShowOutOfStockProduct(boolean showOutOfStockProduct) {
        sShowOutOfStockProduct = showOutOfStockProduct;
    }

    public static boolean getShowOutOfStockProduct() {
        return sShowOutOfStockProduct;
    }

    public static int getShowInAndOutOfStockItem() {
        return sShowOutOfStockProduct ? ConciergeHelper.SHOW_IN_AND_OUT_OF_STOCK_ITEM : ConciergeHelper.SHOW_ONLY_IN_STOCK_ITEM;
    }

    public static void setSortItemOption(ConciergeSortItemOption option) {
        sSortItemOption = option;
    }

    public static ConciergeSortItemOption getSortItemOption() {
        return sSortItemOption;
    }

    public static String getSortOption() {
        if (sSortItemOption != null) {
            return sSortItemOption.getValue();
        }

        return null;
    }

    public static boolean shouldDisplayUpdateItemError(String itemId) {
        return ConciergeCartHelper.isThereCurrentBasketItem() && !sDisplayingUpdateItemErrorIds.contains(itemId);
    }

    public static String getCurrency(Context context) {
        ConciergeShopSetting conciergeShopSetting = getConciergeShopSetting(context);
        if (conciergeShopSetting != null) {
            return conciergeShopSetting.getCurrencyLabel();
        }

        return "";
    }

    public static String getDisplayPrice(Context context, double price) {
        double newPrice = ConciergeCartHelper.roundPriceValueAsDouble(price, 2);
        //Currently we manage with "$" currency format display only
        //Ex: $10.78
        return context.getString(R.string.concierge_price_display_format,
                getCurrency(context),
                String.format(Locale.ENGLISH,
                        "%.2f",
                        newPrice));
    }

    /**
     * Determine whether to display normal price "$20" or discounted price "$10 - $̶2̶0̶"
     *
     * @param context              {@link Context}
     * @param menuItem             The item to check to price of
     * @param priceColor           Text color of normal price
     * @param crossedOutPriceColor Text color of crossed out price
     * @param shouldShowDash       Whether to show a dash between the prices
     * @return A {@link Spannable} to display with {@link android.widget.TextView}
     */
    public static Spannable buildShopItemNormalOrDiscountPriceDisplay(Context context,
                                                                      ConciergeMenuItem menuItem,
                                                                      int priceColor,
                                                                      int crossedOutPriceColor,
                                                                      boolean shouldShowDash) {
        if (menuItem.getOriginalPrice() > 0) {
            return ConciergeHelper.buildShopItemDiscountPriceDisplay(context,
                    menuItem.getPrice(),
                    menuItem.getOriginalPrice(),
                    priceColor,
                    crossedOutPriceColor,
                    shouldShowDash);
        } else {
            String priceString = ConciergeHelper.getDisplayPrice(context, menuItem.getPrice());
            SpannableStringBuilder priceSpan = new SpannableStringBuilder(priceString);
            priceSpan.setSpan(
                    new ForegroundColorSpan(priceColor),
                    0,
                    priceSpan.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            return priceSpan;
        }
    }

    /**
     * This function will output "$10 - $̶2̶0̶", with the first price value being the discounted
     * price and second price value to the right is the original price
     *
     * @param context              {@link Context}
     * @param price                Item's original price
     * @param originalPrice        Item's discounted price
     * @param discountPriceColor   Text color for discount price
     * @param crossedOutPriceColor Text color for crossed out original price
     * @param shouldShowDash       Whether to show a dash between the prices
     * @return "$10 $̶2̶0̶" Span to display with textview
     */
    public static Spannable buildShopItemDiscountPriceDisplay(Context context,
                                                              double price,
                                                              double originalPrice,
                                                              int discountPriceColor,
                                                              int crossedOutPriceColor,
                                                              boolean shouldShowDash) {
        // Building the discounted price span "$10 - "
        String discountPrice = getDisplayPrice(context, price);
        SpannableStringBuilder discountSpan = new SpannableStringBuilder(discountPrice);
        discountSpan.append(shouldShowDash ? " - " : "  ");
        discountSpan.setSpan(
                new ForegroundColorSpan(discountPriceColor), 0, discountSpan.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Building the original price span "$̶2̶0̶"
        Spannable crossedOutNormalPriceSpan =
                getDisplayCrossedOutPrice(context, originalPrice, crossedOutPriceColor);

        // Combining the span
        SpannableStringBuilder spanToReturn = new SpannableStringBuilder();
        spanToReturn.append(discountSpan);
        spanToReturn.append(crossedOutNormalPriceSpan);

        return spanToReturn;
    }

    /**
     * Returns a {@link Spannable} that display "$̶2̶0̶"
     *
     * @param context        {@link Context}
     * @param price          The price to be rendered as "$̶2̶0̶"
     * @param priceTextColor The text color to set for the price
     * @return "$̶2̶0̶" Spannable
     */
    public static Spannable getDisplayCrossedOutPrice(Context context,
                                                      double price,
                                                      @Nullable
                                                              Integer priceTextColor) {
        // Building the crossed out price span "$̶2̶0̶"
        String normalPrice = getDisplayPrice(context, price);
        SpannableStringBuilder crossedOutPriceSpan = new SpannableStringBuilder(normalPrice);
        if (priceTextColor != null) {
            crossedOutPriceSpan.setSpan(
                    new ForegroundColorSpan(priceTextColor), 0, crossedOutPriceSpan.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        crossedOutPriceSpan.setSpan(
                new StrikethroughSpan(), 0, crossedOutPriceSpan.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return crossedOutPriceSpan;
    }

    public static String buildItemVariationDisplay(ConciergeMenuItem product, boolean isFromHistory) {
//        Timber.i("product: " + new Gson().toJson(product));
        StringBuilder text = new StringBuilder();

        if (product.getType() == ConciergeMenuItemType.COMBO) {
            if (product.getChoices() != null && !product.getChoices().isEmpty()) {
                for (ConciergeComboChoiceSection comboChoiceSection : product.getChoices()) {
                    if (comboChoiceSection.getList() != null && !comboChoiceSection.getList().isEmpty()) {
                        for (ConciergeComboItem comboItem : comboChoiceSection.getList()) {
                            boolean shouldAdd = isFromHistory || comboItem.isSelected();

                            if (!shouldAdd) {
                                continue;
                            }

                            StringBuilder selectedComboSection = new StringBuilder();
                            selectedComboSection.append(comboItem.getName());

                            StringBuilder selectedOptions = new StringBuilder();

                            for (ConciergeMenuItemOptionSection optionSection : comboItem.getOptions()) {
                                for (ConciergeMenuItemOption option : optionSection.getOptionItems()) {
                                    if (option.isSelected()) {
                                        if (!TextUtils.isEmpty(selectedOptions)) {
                                            selectedOptions.append(", ");
                                        }
                                        selectedOptions.append(option.getName());
                                    }
                                }
                            }

                            if (!TextUtils.isEmpty(selectedComboSection)) {
                                if (!TextUtils.isEmpty(text)) {
                                    text.append(", ");
                                }
                                text.append(selectedComboSection);
                                if (!TextUtils.isEmpty(selectedOptions)) {
                                    text.append(" (").append(selectedOptions).append(")");
                                }
                            }
                        }
                    }
                }
            }
        } else if (product.getType() == ConciergeMenuItemType.ITEM) {
            if (product.getOptions() != null && !product.getOptions().isEmpty()) {
                for (ConciergeMenuItemOptionSection variation : product.getOptions()) {
                    if (variation.getOptionItems() != null && !variation.getOptionItems().isEmpty()) {
                        //Check first if there is any option selected.
                        StringBuilder selectedOptions = new StringBuilder();
                        for (ConciergeMenuItemOption option : variation.getOptionItems()) {
                            if (option.isSelected()) {
                                selectedOptions.append(option.getName());
                            }
                        }

                        if (!TextUtils.isEmpty(selectedOptions)) {
                            if (!TextUtils.isEmpty(text)) {
                                text.append(", ");
                            }
                            text.append(selectedOptions);
                        }
                    }
                }
            }
        }

        return text.toString();
    }

    public static void setConciergeShopSetting(Context context, ConciergeShopSetting shopSetting) {
        SharedPrefUtils.setString(context, CONCIERGE_SHOP_SETTING, new Gson().toJson(shopSetting));
        ApplicationHelper.checkToResetToNormalModeIfNecessary(context, shopSetting);
    }

    public static ConciergeShopSetting getConciergeShopSetting(Context context) {
        String data = SharedPrefUtils.getString(context, CONCIERGE_SHOP_SETTING);
        if (!TextUtils.isEmpty(data)) {
            return new Gson().fromJson(data, ConciergeShopSetting.class);
        }

        return null;
    }

    public static ConciergeMenuItem parseFromConciergeMenuOrderHistoryItem(Gson gson, ConciergeOrderItem orderHistoryItem) {
        if (gson == null) {
            gson = new Gson();
        }
        ConciergeMenuItemHistory history = orderHistoryItem.getMenuItem();
        ConciergeMenuItem conciergeMenuItem = gson.fromJson(gson.toJson(history), ConciergeMenuItem.class);
        if (conciergeMenuItem != null) {
            conciergeMenuItem.setHash(orderHistoryItem.getItemHash());
            if (conciergeMenuItem.getShop() != null) {
                conciergeMenuItem.getShop().setHash(orderHistoryItem.getShopHash());
            }

            if (history.getChoices() != null) {
                List<ConciergeComboChoiceSection> choiceSectionList = new ArrayList<>();
                for (ConciergeOrderHistoryComboItem choice : history.getChoices()) {
                    ConciergeComboItem comboItem = new ConciergeComboItem();
                    comboItem.setDefaultPrice(choice.getDefaultPrice());
                    comboItem.setName(choice.getName());
                    comboItem.setPrice(choice.getPrice());
                    comboItem.setId(choice.getId());

                    comboItem.setIsSelected(true);
                    if (choice.getOptions() != null) {
                        for (ConciergeMenuItemOption option : choice.getOptions()) {
                            option.setIsSelected(true);
                        }
                        List<ConciergeMenuItemOptionSection> conciergeMenuItemOptionSections = new ArrayList<>();
                        ConciergeMenuItemOptionSection optionSection = new ConciergeMenuItemOptionSection();
                        optionSection.setOptionItems(choice.getOptions());
                        conciergeMenuItemOptionSections.add(optionSection);
                        comboItem.setOptions(conciergeMenuItemOptionSections);
                    }

                    ConciergeComboChoiceSection conciergeComboChoiceSection = new ConciergeComboChoiceSection();
                    List<ConciergeComboItem> conciergeComboItemList = new ArrayList<>();
                    conciergeComboItemList.add(comboItem);
                    conciergeComboChoiceSection.setList(conciergeComboItemList);
                    choiceSectionList.add(conciergeComboChoiceSection);
                }

                conciergeMenuItem.setChoices(choiceSectionList);
            }

            if (history.getOptions() != null) {
                List<ConciergeMenuItemOptionSection> options = new ArrayList<>();
                ConciergeMenuItemOptionSection optionSection = new ConciergeMenuItemOptionSection();
                for (ConciergeMenuItemOption option : history.getOptions()) {
                    //Must set this field to true so as to make it calculated.
                    option.setIsSelected(true);
                }
                optionSection.setOptionItems(history.getOptions());
                options.add(optionSection);
                conciergeMenuItem.setOptions(options);
            }

            //Need to set the default empty list value
            if (conciergeMenuItem.getChoices() == null) {
                conciergeMenuItem.setChoices(new ArrayList<>());
            }
            if (conciergeMenuItem.getOptions() == null) {
                conciergeMenuItem.setOptions(new ArrayList<>());
            }

            if (orderHistoryItem.getQuantity() >= 1) {
                conciergeMenuItem.setProductCount(orderHistoryItem.getQuantity());
            }
            conciergeMenuItem.setSpecialInstruction(orderHistoryItem.getInstruction());
        }

//        Timber.i("conciergeMenuItem: " + new Gson().toJson(conciergeMenuItem));

        return conciergeMenuItem;
    }

    public static void broadcastProductOrderCounterUpdateEvent2(Context context,
                                                                ArrayList<ConciergeMenuItem> menuItemList) {
        //Send broadcast even to update product counter on the displaying screen.
        Intent intent = new Intent(REFRESH_PRODUCT_COUNTER_EVENT);
        intent.putParcelableArrayListExtra(REFRESH_PRODUCT_COUNTER_ITEM, menuItemList);
        SendBroadCastHelper.verifyAndSendBroadCast(context, intent);
    }

    public static void broadcastShowTrackingOrderButtonEvent(Context context, boolean shouldShow) {
        Intent intent = new Intent(SHOW_TRACKING_ORDER_BUTTON_EVENT);
        intent.putExtra(SHOW_TRACKING_ORDER_BUTTON_EVENT_DATA, shouldShow);
        SendBroadCastHelper.verifyAndSendBroadCast(context, intent);
    }

    public static void loadUserInfo(Context context, ConciergeOrder data) {
        AppSetting appSetting = SharedPrefUtils.getAppSetting(context);
        if (appSetting != null) {
            data.setOrganizationName(appSetting.getOrganizationName());
//            data.setOrganizationAddress("9th Floor Room 124");
            User user = SharedPrefUtils.getUser(context);
            data.setUserName(user.getFullName());
        }
    }

    public static void loadUserInfo(Context context, ConciergeCheckoutData data) {
        AppSetting appSetting = SharedPrefUtils.getAppSetting(context);
        if (appSetting != null) {
            data.setCompanyName(appSetting.getOrganizationName());
//            data.setCompanyAddress("9th Floor Room 124");
            User user = SharedPrefUtils.getUser(context);
            data.setUserName(user.getFullName());
        }
    }

    public static boolean checkShouldShowReOrderButton(ConciergeOrderStatus orderStatus) {
//        Timber.i("Order status: " + orderToCheck.getOrderStatus().getValue());
        return orderStatus == ConciergeOrderStatus.DELIVERED ||
                orderStatus == ConciergeOrderStatus.CANCELED;
    }

    public static int getOrderStatusBackground(Context context, ConciergeOrder order) {
        if (order.getOrderStatus() == ConciergeOrderStatus.CANCELED) {
            return AttributeConverter.convertAttrToColor(context, R.attr.shop_order_list_order_status_canceled_background);
        } else {
            return AttributeConverter.convertAttrToColor(context, R.attr.shop_order_list_order_status_background);
        }
    }

    public static void broadcastOrderStatusUpdateEvent(Context context, ConciergeOrder orderToUpdate) {
        Intent intent = new Intent(UPDATE_ORDER_STATUS_EVENT);
        intent.putExtra(UPDATE_ORDER_STATUS_EVENT_DATA, orderToUpdate);
        SendBroadCastHelper.verifyAndSendBroadCast(context, intent);
    }

    public static void broadcastAddressCreatedEvent(Context context, ConciergeUserAddress newAddress) {
        Intent intent = new Intent(ADDRESS_CREATED_EVENT);
        intent.putExtra(ADDRESS_EVENT_DATA, newAddress);
        SendBroadCastHelper.verifyAndSendBroadCast(context, intent);
    }

    public static void broadcastAddressUpdatedEvent(Context context, ConciergeUserAddress updatedAddress) {
        Intent intent = new Intent(ADDRESS_UPDATED_EVENT);
        intent.putExtra(ADDRESS_EVENT_DATA, updatedAddress);
        SendBroadCastHelper.verifyAndSendBroadCast(context, intent);
    }

    public static void broadcastAddressDeletedEvent(Context context, ConciergeUserAddress deletedAddress) {
        Intent intent = new Intent(ADDRESS_DELETED_EVENT);
        intent.putExtra(ADDRESS_EVENT_DATA, deletedAddress);
        SendBroadCastHelper.verifyAndSendBroadCast(context, intent);
    }

    public static void broadcastAddressSelectedEvent(Context context, ConciergeUserAddress selectedAddress, boolean isCurrentLocation) {
        Intent intent = new Intent(ADDRESS_SELECTED_EVENT);
        intent.putExtra(ADDRESS_EVENT_DATA, selectedAddress);
        intent.putExtra(ADDRESS_IS_CURRENT_LOCATION_DATA, isCurrentLocation);
        SendBroadCastHelper.verifyAndSendBroadCast(context, intent);
    }

    public static void broadcastShopStatusUpdatedEvent(Context context, boolean isShopClosed) {
        Intent intent = new Intent(SHOP_STATUS_UPDATED_EVENT);
        intent.putExtra(FIELD_IS_SHOP_CLOSED, isShopClosed);
        SendBroadCastHelper.verifyAndSendBroadCast(context, intent);
    }

    /**
     * Convenience method to get {@link ConciergeUserAddress} from user's current position
     *
     * @param context       Context
     * @param userLatLng    User's location in LatLng
     * @param selectionName Optional selection name
     * @return User's location as {@link ConciergeUserAddress}
     */
    public static Observable<ConciergeUserAddress> getUserAddressFromLatLng(Context context, LatLng userLatLng, String selectionName) {
        final int MAX_RESULT = 1;

        AtomicReference<SearchAddressResult> addressResult = new AtomicReference<>(new SearchAddressResult());

        Timber.i("point: " + userLatLng.toString() + ", selectionName: " + selectionName);
        Observable<SearchAddressResult> observable = Observable.just(userLatLng).concatMap(latLng -> {
            Geocoder geocoder = new Geocoder(context);
            List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, MAX_RESULT);
            Timber.i("addressList: " + new Gson().toJson(addressList));
            if (addressList != null && !addressList.isEmpty()) {
//                Timber.i("Address 1: " + new Gson().toJson(addressList.get(0)));
                SearchAddressResult searchAddressResult = new SearchAddressResult();
                searchAddressResult.setAddress(LocationStateUtil.getOnlyAddressFromFullPlace(addressList.get(0).getAddressLine(0)));
                if (!TextUtils.isEmpty(selectionName)) {
                    //From POIs selection
                    searchAddressResult.setPlaceTitle(selectionName);
                    String fullAddress = selectionName;
                    if (!TextUtils.isEmpty(searchAddressResult.getAddress())) {
                        fullAddress += ", " + searchAddressResult.getAddress();
                    }
                    searchAddressResult.setFullAddress(fullAddress);
                } else {
                    searchAddressResult.setPlaceTitle(LocationStateUtil.getPlaceNameFromFullAddress(addressList.get(0).getAddressLine(0)));
                    searchAddressResult.setFullAddress(addressList.get(0).getAddressLine(0));
                }
                searchAddressResult.setCity(addressList.get(0).getLocality());
                searchAddressResult.setCountry(addressList.get(0).getCountryName());
                Locations locations = new Locations();
                locations.setLatitude(userLatLng.latitude);
                locations.setLongitude(userLatLng.longitude);
                locations.setCountry(addressList.get(0).getCountryCode());
                searchAddressResult.setLocations(locations);

                return Observable.just(searchAddressResult);
            }
            return Observable.error(new Throwable(context.getString(R.string.address_not_found)));
        });

        return observable
                .concatMap((Function<SearchAddressResult, ObservableSource<ConciergeUserAddress>>) data -> {
                    addressResult.set(data);
                    Timber.i("Search Address Result: " + new Gson().toJson(addressResult));
                    ConciergeUserAddress address = new ConciergeUserAddress();
                    address.setAddress(addressResult.get().getAddress());
                    address.setLatitude(userLatLng.latitude);
                    address.setLongitude(userLatLng.longitude);
                    return Observable.just(address);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static ConciergeCartModel transformRemoteBasketIntoLocalBasket(ConciergeBasket remoteBasket) {
        ConciergeCartModel localBasket = new ConciergeCartModel();
        localBasket.setBasketId(remoteBasket.getId());

        //Transform menu items
        if (remoteBasket.getMenuItems() != null && !remoteBasket.getMenuItems().isEmpty()) {
            List<ConciergeMenuItem> menuItemList = new ArrayList<>();
            Gson gson = new Gson();
            for (ConciergeBasketMenuItem menuItem : remoteBasket.getMenuItems()) {
                menuItemList.add(createMenuItemFromBasketMenuItem(gson, menuItem));
            }

            localBasket.setBasketItemList(menuItemList);
        }

        localBasket.setCouponId(remoteBasket.getCouponId());
        localBasket.setWalletApplied(remoteBasket.isAppliedWallet());
        localBasket.setConciergeCoupon(remoteBasket.getConciergeCoupon());
        ConciergeCartHelper.calculateCartTotalPrice(localBasket);
        localBasket.setPaymentMethod(remoteBasket.getPaymentMethod());
        localBasket.setDeliveryAddress(remoteBasket.getDeliveryAddress());
        localBasket.setDeliveryInstruction(remoteBasket.getDeliveryInstruction());
        localBasket.setPaymentProvider(remoteBasket.getPaymentProvider());
        localBasket.setType(remoteBasket.getType());
        /*
            Will update time slot from server to local basket only if the remote basket is locked.
            Otherwise, selected time slot will be saved and used for local only.
         */
        if (remoteBasket.isLockedWithPayment()) {
            localBasket.setBasketTimeSlot(remoteBasket.getBasketTimeSlot());
        }
        localBasket.setOOSOption(remoteBasket.getOOSOption());
        localBasket.setDeliveryFee(remoteBasket.getDeliveryFee());
        localBasket.setContainerFee(remoteBasket.getContainerFee());
        localBasket.setReceiptUrl(remoteBasket.getReceiptUrl());

        Timber.i("localBasket: " + new Gson().toJson(localBasket));
        return localBasket;
    }

    public static ConciergePreferenceDelivery buildOrderDeliveryFromTimeSelection(
            ShopDeliveryTimeSelection deliverySelection) {
        Timber.i("buildOrderDelivery: \n" + deliverySelection.toString());

        ConciergePreferenceDelivery preferenceDelivery = new ConciergePreferenceDelivery();
        preferenceDelivery.setType(deliverySelection.getDeliveryType().getValue());

        ConciergeDeliverySlot deliverySlot = new ConciergeDeliverySlot();
        if (deliverySelection.getSelectedDate() != null) {
            deliverySlot.setDate(Date.from(deliverySelection.getSelectedDate().atZone(ZoneId.systemDefault()).toInstant()));
        }

        if (deliverySelection.getStartTime() != null && deliverySelection.getEndTime() != null) {
            ConciergePreferredSlot preferredSlot = new ConciergePreferredSlot();

            String startTime = deliverySelection.getStartTime().format(DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_14));
            String endTime = deliverySelection.getEndTime().format(DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_14));

            preferredSlot.setStartTime(startTime);
            preferredSlot.setEndTime(endTime);

            deliverySlot.setDate(Date.from(deliverySelection.getStartTime().atZone(ZoneId.systemDefault()).toInstant()));
            deliverySlot.setPreferredSlot(Collections.singletonList(preferredSlot));
        }
        Timber.d("Delivery date: " + deliverySlot.getDate().toString());

        preferenceDelivery.setSlots(Collections.singletonList(deliverySlot));
        return preferenceDelivery;
    }

    public static ShopDeliveryTimeSelection convertPreferenceDeliveryToTimeSelection(
            ConciergePreferenceDelivery conciergePreferenceDelivery) {
        ShopDeliveryTimeSelection deliveryTimeSelection = new ShopDeliveryTimeSelection();
        // Set the delivery type
        deliveryTimeSelection.setDeliveryType(ShopDeliveryType.fromValue(conciergePreferenceDelivery.getType()));

        if (!conciergePreferenceDelivery.getSlots().isEmpty()) {
            // There's only ever one slot currently that we managed. This may change in the future,
            // but we'll maybe have to create a new model, say "ShopMultiDeliveryTimeSelection" to
            // handle multiple time selection and a separate function handle converting that
            ConciergeDeliverySlot slot = conciergePreferenceDelivery.getSlots().get(0);

            // The default system time zone conversion here could be a potential issue, but only
            // for clone that are used internationally. We can solve this for international clone if
            // there is an option to set a time zone for each clone, maybe on admin site, but realistically
            // maybe in database so time zone can't be easily changed, and server can expose an API
            // to get the time zone.
            deliveryTimeSelection.setSelectedDate(
                    LocalDateTime.ofInstant(slot.getDate().toInstant(), ZoneId.systemDefault()));

            LocalTime startTime = LocalTime.parse(slot.getPreferredSlot().get(0).getStartTime(),
                    DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_14));
            LocalTime endTime = LocalTime.parse(slot.getPreferredSlot().get(0).getEndTime(),
                    DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_14));

            LocalDateTime startDateTime = deliveryTimeSelection.getSelectedDate().with(startTime);
            LocalDateTime endDateTime = deliveryTimeSelection.getSelectedDate().with(endTime);

            deliveryTimeSelection.setStartTime(startDateTime);
            deliveryTimeSelection.setEndTime(endDateTime);

            boolean isAsap = false;
            // If selected time is today, and is between now and the next 40 minutes from now, we can
            // safely assume the selected time is ASAP
            if (startDateTime.toLocalDate().isEqual(LocalDate.now())) {
                if (startDateTime.toLocalTime().isAfter(LocalTime.now()) &&
                        startDateTime.toLocalTime().isBefore(LocalTime.now().plusMinutes(30))) {
                    isAsap = true;
                }
            }
            deliveryTimeSelection.setIsASAP(isAsap);
        }
        return deliveryTimeSelection;
    }

    public static ConciergeMenuItem createMenuItemFromBasketMenuItem(Gson gson, ConciergeBasketMenuItem history) {
        if (gson == null) {
            gson = new Gson();
        }

        ConciergeMenuItem conciergeMenuItem = gson.fromJson(gson.toJson(history), ConciergeMenuItem.class);
        if (conciergeMenuItem != null) {
            /*
                Must set item id to convert ConciergeMenuItem model because the "_id" from ConciergeBasketMenuItem is
                the id of basket menu item not menu item.
             */
            conciergeMenuItem.setId(history.getItemId());
            //Must set basket item id to MenuItem for using when update or remove the item from remote basket
            conciergeMenuItem.setBasketItemId(history.getId());
            
            /*
                Manage to set item and shop has. If there is no shop, we just create empty shop object with
                required property, "hash" for future usage with ConciergeMenuItem.
             */
            conciergeMenuItem.setHash(history.getItemHash());
            //Set default shop if it does not exist within the item
            if (conciergeMenuItem.getShop() == null) {
                conciergeMenuItem.setShop(new ConciergeShop());
            }
            conciergeMenuItem.getShop().setHash(history.getShopHash());

            if (history.getChoices() != null) {
                List<ConciergeComboChoiceSection> choiceSectionList = new ArrayList<>();
                for (ConciergeOrderHistoryComboItem choice : history.getChoices()) {
                    ConciergeComboItem comboItem = new ConciergeComboItem();
                    comboItem.setDefaultPrice(choice.getDefaultPrice());
                    comboItem.setName(choice.getName());
                    comboItem.setPrice(choice.getPrice());
                    comboItem.setId(choice.getId());

                    comboItem.setIsSelected(true);
                    if (choice.getOptions() != null) {
                        for (ConciergeMenuItemOption option : choice.getOptions()) {
                            option.setIsSelected(true);
                        }
                        List<ConciergeMenuItemOptionSection> conciergeMenuItemOptionSections = new ArrayList<>();
                        ConciergeMenuItemOptionSection optionSection = new ConciergeMenuItemOptionSection();
                        optionSection.setOptionItems(choice.getOptions());
                        conciergeMenuItemOptionSections.add(optionSection);
                        comboItem.setOptions(conciergeMenuItemOptionSections);
                    }

                    ConciergeComboChoiceSection conciergeComboChoiceSection = new ConciergeComboChoiceSection();
                    List<ConciergeComboItem> conciergeComboItemList = new ArrayList<>();
                    conciergeComboItemList.add(comboItem);
                    conciergeComboChoiceSection.setList(conciergeComboItemList);
                    choiceSectionList.add(conciergeComboChoiceSection);
                }

                conciergeMenuItem.setChoices(choiceSectionList);
            }

            if (history.getOptions() != null) {
                List<ConciergeMenuItemOptionSection> options = new ArrayList<>();
                ConciergeMenuItemOptionSection optionSection = new ConciergeMenuItemOptionSection();
                for (ConciergeMenuItemOption option : history.getOptions()) {
                    //Must set this field to true so as to make it calculated.
                    option.setIsSelected(true);
                }
                optionSection.setOptionItems(history.getOptions());
                options.add(optionSection);
                conciergeMenuItem.setOptions(options);
            }

            //Need to set the default empty list value
            if (conciergeMenuItem.getChoices() == null) {
                conciergeMenuItem.setChoices(new ArrayList<>());
            }
            if (conciergeMenuItem.getOptions() == null) {
                conciergeMenuItem.setOptions(new ArrayList<>());
            }

            if (history.getQuantity() >= 1) {
                conciergeMenuItem.setProductCount(history.getQuantity());
                conciergeMenuItem.setBackupProductCount(history.getQuantity());
            }
            conciergeMenuItem.setSpecialInstruction(history.getInstruction());
        }

        return conciergeMenuItem;
    }

    public static ConciergeRequestOrderMenuItem createOrderMenuItem(ConciergeMenuItem conciergeMenuItem) {
        ConciergeRequestOrderMenuItem menuItem = new ConciergeRequestOrderMenuItem();
        menuItem.setItemId(conciergeMenuItem.getId());
        menuItem.setBasketId(conciergeMenuItem.getBasketItemId());
        if (conciergeMenuItem.getShop() != null) {
            menuItem.setShopHash(conciergeMenuItem.getShop().getHash());
        }
        menuItem.setItemHash(conciergeMenuItem.getHash());
        menuItem.setInstruction(menuItem.getInstruction());
        menuItem.setQuantity(conciergeMenuItem.getProductCount());
        menuItem.setInstruction(conciergeMenuItem.getSpecialInstruction());
        menuItem.setPrice(conciergeMenuItem.getPrice());

        if (conciergeMenuItem.getType() == ConciergeMenuItemType.ITEM) {
            if (conciergeMenuItem.getOptions() != null && !conciergeMenuItem.getOptions().isEmpty()) {
                List<String> optionIds = new ArrayList<>();
                for (ConciergeMenuItemOptionSection menuItemOptionSection : conciergeMenuItem.getOptions()) {
                    if (menuItemOptionSection.getOptionItems() != null && !menuItemOptionSection.getOptionItems().isEmpty()) {
                        for (ConciergeMenuItemOption optionItem : menuItemOptionSection.getOptionItems()) {
                            if (optionItem.isSelected()) {
                                optionIds.add(optionItem.getId());
                            }
                        }
                    }
                }

                menuItem.setOptionId(optionIds);
            }
        } else if (conciergeMenuItem.getType() == ConciergeMenuItemType.COMBO) {
            if (conciergeMenuItem.getChoices() != null && !conciergeMenuItem.getChoices().isEmpty()) {
                List<ConciergeRequestOrderMenuItemChoice> menuItemChoices = new ArrayList<>();
                for (ConciergeComboChoiceSection comboChoiceSection : conciergeMenuItem.getChoices()) {
                    if (comboChoiceSection.getList() != null && !comboChoiceSection.getList().isEmpty()) {
                        for (ConciergeComboItem comboItem : comboChoiceSection.getList()) {
                            if (comboItem != null && comboItem.isSelected()) {
                                ConciergeRequestOrderMenuItemChoice choice = new ConciergeRequestOrderMenuItemChoice();
                                choice.setId(comboItem.getId());

                                List<String> optionIds = new ArrayList<>();
                                for (ConciergeMenuItemOptionSection menuItemOptionSection : comboItem.getOptions()) {
                                    if (menuItemOptionSection != null) {
                                        if (menuItemOptionSection.getOptionItems() != null && !menuItemOptionSection.getOptionItems().isEmpty()) {
                                            for (ConciergeMenuItemOption optionItem : menuItemOptionSection.getOptionItems()) {
                                                if (optionItem != null) {
                                                    if (optionItem.isSelected()) {
                                                        optionIds.add(optionItem.getId());
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                choice.setOptionIds(optionIds);
                                menuItemChoices.add(choice);
                            }
                        }
                    }
                }
                menuItem.setChoices(menuItemChoices);
            }
        }

        return menuItem;
    }

    public static void manipulateItemToRemoteBasketInBackground(Context context,
                                                                ApiService privatAPIService,
                                                                ConciergeMenuItemManipulationInfo manipulationInfo) {
//        Timber.i("manipulateItemToRemoteBasket: " + new Gson().toJson(manipulationInfo));
        FlurryHelper.logEvent(context, FlurryHelper.UPDATE_PRODUCT_IN_CHECKOUT_CARD);
        //Currently this asynchronous item modification will support only update or remove item from basket.
        Observable<Response<ConciergeBasketMenuItem>> observable = PreAPIRequestHelper.requestUpdateOrRemoveItemToBasket(context,
                privatAPIService,
                manipulationInfo.getMenuItem().getBasketItemId(),
                ConciergeHelper.createOrderMenuItem(manipulationInfo.getMenuItem()));

        ResponseObserverHelper<Response<ConciergeBasketMenuItem>> helper = new ResponseObserverHelper<>(context, observable);
        helper.execute(new OnCallbackListener<Response<ConciergeBasketMenuItem>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                //Will ignore handing in error if the session dialog is showing and normally user have to log in again.
                if (!(context instanceof AbsBaseActivity && ((MainApplication) ((AbsBaseActivity) context).getApplication()).getReceivedCommandToShowSessionDialogError())) {
                    if (ConciergeCartHelper.isProductInLocalBasket(manipulationInfo.getMenuItem())) {
                        rollbackToBackupProductCount(context, manipulationInfo.getMenuItem(), null);
                        if (context.getApplicationContext() instanceof MainApplication) {
                            ((MainApplication) context.getApplicationContext()).fireBasketModificationListener(null,
                                    ex.getMessage(),
                                    manipulationInfo);
                        }
                    }
                }
            }

            @Override
            public void onComplete(Response<ConciergeBasketMenuItem> result) {
                if (result.body().isError() && result.body().getCode() == HTTPResponse.NOT_FOUND) {
                    Timber.i("Product not found. So will ignore any future action.");
                    return;
                }

                if (ConciergeCartHelper.isProductInLocalBasket(manipulationInfo.getMenuItem())) {
                    if (result.body() != null && result.body().isError()) {
                        Timber.i("manipulateItemToRemoteBasketInBackground error: " + result.body().getErrorMessage());
                        if (result.body() != null && result.body().getDataType() != SupportConciergeCustomErrorResponse.ResponseDataType.PAYMENT_IN_PROCESS) {
                            rollbackToBackupProductCount(context, manipulationInfo.getMenuItem(), null);
                        }

                        if (context.getApplicationContext() instanceof MainApplication) {
                            ((MainApplication) context.getApplicationContext()).fireBasketModificationListener(result.body(),
                                    result.body().getErrorMessage(),
                                    manipulationInfo);
                        }
                    } else {
                        updateBackupProductCount(context, manipulationInfo.getMenuItem(), null);
                    }
                }
            }
        });
    }

    public static void clearLocalBasket(Context context, OnCallbackListener<Response<Object>> callbackListener) {
        Observable<Response<Object>> observable = getClearLocalBasketService(context);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(context, observable);
        helper.execute(callbackListener);
    }

    public static void updateBackupProductCount(Context context,
                                                ConciergeMenuItem menuItem,
                                                OnCallbackListener<Response<ConciergeCartModel>> callbackListener) {
        Observable<Response<ConciergeCartModel>> observable = ConciergeCartHelper.updateBackupProductCount(context, menuItem);
        ResponseObserverHelper<Response<ConciergeCartModel>> helper = new ResponseObserverHelper<>(context, observable);
        if (callbackListener != null) {
            helper.execute(callbackListener);
        } else {
            helper.execute();
        }
    }

    public static void rollbackToBackupProductCount(Context context,
                                                    ConciergeMenuItem menuItem,
                                                    OnCallbackListener<Response<ConciergeCartModel>> callbackListener) {
        Observable<Response<ConciergeCartModel>> observable = ConciergeCartHelper.rollbackToBackupProductCount(context, menuItem);
        ResponseObserverHelper<Response<ConciergeCartModel>> helper = new ResponseObserverHelper<>(context, observable);
        if (callbackListener != null) {
            helper.execute(callbackListener);
        } else {
            helper.execute();
        }
    }

    public static Observable<Response<Object>> getClearLocalBasketService(Context context) {
        return ConciergeCartHelper.clearCart(context).concatMap(o -> Observable.just(Response.success(new Object())));
    }

    public static void getLocalBasket(Context context, OnCallbackListener<Response<ConciergeCartModel>> callbackListener) {
        Observable<Response<ConciergeCartModel>> observable = getLocalBasketService();
        ResponseObserverHelper<Response<ConciergeCartModel>> helper = new ResponseObserverHelper<>(context, observable);
        helper.execute(callbackListener);
    }

    public static Observable<Response<ConciergeOrderResponseWithABAPayWay>> requestABAPaymentFormFromLocalBasket(Context context,
                                                                                                                 double walletAmount,
                                                                                                                 ApiService privateAPIService) {
        return getLocalBasketService().concatMap(conciergeCartModelResponse -> {
            //Build ABA request order
            ConciergeCartModel localBasket = conciergeCartModelResponse.body();
            if (localBasket != null) {
                ConciergeOrderRequest request = new ConciergeOrderRequest();
                request.setOOSOption(localBasket.getOOSOption());
                request.setCouponId(localBasket.getCouponId());
                request.setDeliveryFee((float) localBasket.getDeliveryFee());
                request.setContainerFee((float) localBasket.getContainerFee());
                request.setTotalDiscount(localBasket.getCouponPrice());
                request.setAppliedWallet(localBasket.isWalletApplied());
                request.setWalletAmount(walletAmount);
                if (localBasket.getPaymentProvider() != null) {
                    request.setPaymentOption(localBasket.getPaymentProvider().getProvider());
                }

                request.setDeliveryInstruction(localBasket.getDeliveryInstruction());
                if (localBasket.getDeliveryAddress() != null) {
                    request.setAddressId(localBasket.getDeliveryAddress().getId());
                }
                if (isSelectedBasketTimeSlotValid(localBasket.getBasketTimeSlot())) {
                    request.setTimeSlotId(localBasket.getBasketTimeSlot().getDeliveryTimes().get(0).getId());
                }
                if (localBasket.getBasketItemList() != null && !localBasket.getBasketItemList().isEmpty()) {
                    request.setMenuItems(parseOrderMenuItems(localBasket.getBasketItemList()));
                }

                Gson gson = GsonHelper.getSupportedGSonGMTDate();
                ConciergeOrderRequestWithABAPayWay orderRequestWithABAPayWay = new ConciergeOrderRequestWithABAPayWay(request);

                return privateAPIService.orderWithABAPayWay(gson.fromJson(gson.toJson(orderRequestWithABAPayWay), JsonObject.class),
                        ApplicationHelper.getRequestAPIMode(context),
                        LocaleManager.getAppLanguage(context));
            } else {
                return Observable.error(ErrorThrowable.newGeneralErrorInstance(context));
            }
        });
    }

    public static List<ConciergeRequestOrderMenuItem> parseOrderMenuItems(List<ConciergeMenuItem> conciergeMenuItemList) {
        List<ConciergeRequestOrderMenuItem> menuItemList = new ArrayList<>();
        for (ConciergeMenuItem conciergeMenuItem : conciergeMenuItemList) {
            if (conciergeMenuItem.getProductCount() <= 0) {
                //Ignore invalid value. By default it is should be 1.
                continue;
            }
            menuItemList.add(ConciergeHelper.createOrderMenuItem(conciergeMenuItem));
        }

        return menuItemList;
    }

    private static Observable<Response<ConciergeCartModel>> getLocalBasketService() {
        return ConciergeCartHelper.getCart().concatMap(cartModel -> Observable.just(Response.success(cartModel)));
    }

    public static Observable<Response<SupportConciergeCustomErrorResponse>> enableBasket(Context context, ApiService privateAPIService) {
        return privateAPIService.enableBasket(ApplicationHelper.getRequestAPIMode(context));
    }

    public static void requestToEnableBasket(Context context,
                                             ApiService privateAPIService,
                                             OnCallbackListener<Response<SupportConciergeCustomErrorResponse>> callbackListener) {
        Observable<Response<SupportConciergeCustomErrorResponse>> responseObservable = enableBasket(context, privateAPIService);
        BaseObserverHelper<Response<SupportConciergeCustomErrorResponse>> helper = new BaseObserverHelper<>(context, responseObservable);
        helper.execute(callbackListener);
    }

    public static void requestBasket(Context context,
                                     ApiService privateAPIService,
                                     OnCallbackListener<Response<ConciergeBasket>> callbackListener) {
        Observable<Response<ConciergeBasket>> responseObservable = PreAPIRequestHelper.requestUserBasket(context,
                privateAPIService,
                false,
                true);
        BaseObserverHelper<Response<ConciergeBasket>> helper = new BaseObserverHelper<>(context, responseObservable);
        helper.execute(callbackListener);
    }

    public static void updateOnlineBasket(Context context,
                                          ApiService apiService,
                                          ConciergeUpdateBasketRequest request,
                                          OnCallbackListener<Response<ConciergeBasket>> callbackListener) {
        Observable<Response<ConciergeBasket>> responseObservable = apiService.updateBasket(request, ApplicationHelper.getRequestAPIMode(context),
                LocaleManager.getAppLanguage(context));
        BaseObserverHelper<Response<ConciergeBasket>> helper = new BaseObserverHelper<>(context, responseObservable);
        helper.execute(callbackListener);
    }

    public static void saveBasketId(Context context, String basketId) {
        SharedPrefUtils.setString(context, KEY_BASKET_ID, basketId);
    }

    public static String getBasketId(Context context) {
        return SharedPrefUtils.getString(context, KEY_BASKET_ID);
    }

    public static Observable<Response<ConciergeVerifyPaymentWithABAResponse>> verifyOrderPaymentStatus(Context context, ApiService privatAPIService) {
        return privatAPIService.verifyOrderPaymentStatusWithABA(ApplicationHelper.getRequestAPIMode(context));
    }

    public static void broadcastRefreshLocalBasketEvent(Context context) {
        Intent intent = new Intent(AbsSupportShopCheckOutFragment.REFRESH_LOCAL_BASKET_DISPLAY_EVENT);
        SendBroadCastHelper.verifyAndSendBroadCast(context, intent);
    }

    public static void broadcastRefreshLocalBasketEventFromRollbackProductCount(Context context) {
        if (context != null) {
            Intent intent = new Intent(AbsSupportShopCheckOutFragment.REFRESH_LOCAL_BASKET_DISPLAY_EVENT);
            intent.putExtra(IS_FROM_ROLLBACK_PRODUCT_COUNT, true);
            SendBroadCastHelper.verifyAndSendBroadCast(context, intent);
        }
    }

    public static float roundValue(float value) {
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_EVEN);
        String StringValue = df.format(value);
        Timber.i("roundValue: " + StringValue);
        return Float.parseFloat(StringValue);
    }

    public static double roundValue(double value) {
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_EVEN);
        String StringValue = df.format(value);
        Timber.i("roundValue: " + StringValue);
        return Double.parseDouble(StringValue);
    }

    public static BasketTimeSlot buildBasketTimeSlotFromShopDeliveryTimeSelection(ShopDeliveryTimeSelection deliveryTimeSelection) {
        BasketTimeSlot basketTimeSlot = new BasketTimeSlot();
        List<ShopDeliveryTime> deliveryTimes = new ArrayList<>();

        LocalTime startLocalTime = deliveryTimeSelection.getStartTime().toLocalTime();
        LocalTime endLocalTime = deliveryTimeSelection.getEndTime().toLocalTime();
        LocalDateTime selectedDate = deliveryTimeSelection.getSelectedDate();
        String selectedDateString = selectedDate.format(DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_19));

        ShopDeliveryTime deliveryTime = new ShopDeliveryTime();
        deliveryTime.setId(deliveryTimeSelection.getTimeSlotId());
        deliveryTime.setStartTimeString(startLocalTime.toString());
        deliveryTime.setEndTimeString(endLocalTime.toString());
        deliveryTime.setSlotDateString(selectedDateString);

        deliveryTimes.add(deliveryTime);
        basketTimeSlot.setDeliveryTimes(deliveryTimes);

        return basketTimeSlot;
    }

    public static ShopDeliveryTimeSelection buildShopDeliveryTimeSelectionFromBasketTimeSlot(BasketTimeSlot basketTimeSlot) {
        List<ShopDeliveryTime> shopDeliveryTimes = DeliveryDataManager.convertTimeStringToLocalTime(basketTimeSlot.getDeliveryTimes());
        ShopDeliveryTime shopDeliveryTime = shopDeliveryTimes.get(0);

        Date dateFromStringDate = DateUtils.getDateFromStringDateWithSpecificFormat(shopDeliveryTime.getSlotDateString(), DateUtils.DATE_FORMAT_19);
        Calendar instance = Calendar.getInstance();
        instance.setTime(dateFromStringDate);
        LocalDateTime dateTime = LocalDateTime.of(instance.get(Calendar.YEAR),
                instance.get(Calendar.MONTH) + 1,
                instance.get(Calendar.DAY_OF_MONTH),
                0,
                0);

        ShopDeliveryTimeSelection shopDeliveryTimeSelection = new ShopDeliveryTimeSelection();
        LocalDateTime startTime = dateTime
                .withHour(shopDeliveryTime.getStartTime().getHour())
                .withMinute(shopDeliveryTime.getStartTime().getMinute());
        LocalDateTime endTime = dateTime
                .withHour(shopDeliveryTime.getEndTime().getHour())
                .withMinute(shopDeliveryTime.getEndTime().getMinute());
        shopDeliveryTimeSelection.setTimeSlotId(shopDeliveryTime.getId());
        shopDeliveryTimeSelection.setSelectedDate(dateTime);
        shopDeliveryTimeSelection.setStartTime(startTime);
        shopDeliveryTimeSelection.setEndTime(endTime);

        return shopDeliveryTimeSelection;
    }

    public static boolean isSelectedBasketTimeSlotValid(BasketTimeSlot basketTimeSlot) {
        return basketTimeSlot != null &&
                basketTimeSlot.getDeliveryTimes() != null &&
                !basketTimeSlot.getDeliveryTimes().isEmpty();
    }

    public static String buildTimeSlotDisplayFormat(Context context, LocalTime startTime, LocalTime endTime) {
        String startTimeString = null;
        String endTimeString = null;

        if (startTime != null) {
            startTimeString = startTime.format(DateTimeFormatter.ofPattern(DateUtils.CONCIERGE_START_TIME_SLOT_FORMAT));
        }
        if (endTime != null) {
            endTimeString = getTimeFormatDisplay(context, endTime, DateUtils.CONCIERGE_END_TIME_SLOT_FORMAT);
        }

        if (!TextUtils.isEmpty(startTimeString) && !TextUtils.isEmpty(endTimeString)) {
            return (startTimeString + "-" + endTimeString).toLowerCase();
        } else if (!TextUtils.isEmpty(startTimeString)) {
            return startTimeString;
        } else {
            return endTimeString;
        }
    }

    public static String buildTimeSlotDisplayFormat3(Context context, LocalTime startTime, LocalTime endTime) {
        String startTimeString = null;
        String endTimeString = null;

        if (startTime != null) {
            startTimeString = getTimeFormatDisplay(context, startTime, DateUtils.CONCIERGE_SUPPLIER_ESTIMATE_DELIVERY_TIME_FORMAT);
        }
        if (endTime != null) {
            endTimeString = getTimeFormatDisplay(context, endTime, DateUtils.CONCIERGE_SUPPLIER_ESTIMATE_DELIVERY_TIME_FORMAT);
        }

        if (!TextUtils.isEmpty(startTimeString) && !TextUtils.isEmpty(endTimeString)) {
            return (startTimeString + " - " + endTimeString).toLowerCase();
        } else if (!TextUtils.isEmpty(startTimeString)) {
            return startTimeString;
        } else {
            return endTimeString;
        }
    }

    public static String buildTimeSlotDisplayFormat4(Context context, LocalTime startTime, LocalTime endTime) {
        String startTimeString = null;
        String endTimeString = null;

        if (startTime != null) {
            startTimeString = getTimeFormatDisplay(context, startTime, DateUtils.CONCIERGE_END_TIME_SLOT_FORMAT);
        }
        if (endTime != null) {
            endTimeString = getTimeFormatDisplay(context, endTime, DateUtils.CONCIERGE_END_TIME_SLOT_FORMAT);
        }

        if (!TextUtils.isEmpty(startTimeString) && !TextUtils.isEmpty(endTimeString)) {
            return (startTimeString + "-" + endTimeString).toLowerCase();
        } else if (!TextUtils.isEmpty(startTimeString)) {
            return startTimeString;
        } else {
            return endTimeString;
        }
    }

    public static String getTimeFormatDisplay(Context context, LocalTime localTime, String timeFormat) {
        Calendar instance = Calendar.getInstance();
        instance.set(Calendar.HOUR_OF_DAY, localTime.getHour());
        instance.set(Calendar.MINUTE, localTime.getMinute());
        instance.set(Calendar.SECOND, localTime.getSecond());

        return DateUtils.getDateWithFormat(context,
                instance.getTimeInMillis(),
                timeFormat,
                LocaleManager.getLocale(context.getResources()));
    }

    public static String buildTimeSlotDisplayFormat2(Context context, LocalTime startTime, LocalTime endTime) {
        String startTimeString = null;
        String endTimeString = null;

        if (startTime != null) {
            startTimeString = getTimeFormatDisplay(context, startTime, DateUtils.CONCIERGE_SUPPLIER_ESTIMATE_DELIVERY_TIME_FORMAT);
        }
        if (endTime != null) {
            endTimeString = getTimeFormatDisplay(context, endTime, DateUtils.CONCIERGE_SUPPLIER_ESTIMATE_DELIVERY_TIME_FORMAT);
        }

        if (!TextUtils.isEmpty(startTimeString) && !TextUtils.isEmpty(endTimeString)) {
            return (startTimeString + "-" + endTimeString).toLowerCase();
        } else if (!TextUtils.isEmpty(startTimeString)) {
            return startTimeString;
        } else {
            return endTimeString;
        }
    }

    public static ConciergeViewItemByType getSelectedViewItemByOption(Context context) {
        int anInt = SharedPrefUtils.getInt(context, SharedPrefUtils.SELECTED_VIEW_ITEM_BY_OPTION);
        return ConciergeViewItemByType.from(anInt);
    }

    public static void saveSelectedViewItemByOption(Context context, ConciergeViewItemByType selectedOption) {
        SharedPrefUtils.setInt(context, SharedPrefUtils.SELECTED_VIEW_ITEM_BY_OPTION, selectedOption.getId());
    }

    public static void showCheckoutErrorPopup(AppCompatActivity appCompatActivity, String errorMessage, View.OnClickListener okClickListener) {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setCancelable(false);
        messageDialog.setTitle(appCompatActivity.getString(R.string.popup_error_title));
        messageDialog.setMessage(errorMessage);
        messageDialog.setLeftText(appCompatActivity.getString(R.string.popup_ok_button), view -> {
            if (okClickListener != null) {
                okClickListener.onClick(view);
            }
        });
        messageDialog.show(appCompatActivity.getSupportFragmentManager());
    }

    public static void showUpdateItemInBasketErrorPopup(AppCompatActivity appCompatActivity, String errorMessage, String itemId) {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setCancelable(false);
        messageDialog.setTitle(appCompatActivity.getString(R.string.popup_error_title));
        messageDialog.setMessage(errorMessage);
        messageDialog.setLeftText(appCompatActivity.getString(R.string.popup_ok_button), view -> {
            removeDisplayingUpdateItemErrorId(itemId);
        });
        addDisplayingUpdateItemErrorId(itemId);
        messageDialog.show(appCompatActivity.getSupportFragmentManager());
    }

    public static String getValidateProductCountForDisplay(int count) {
        return count > 99 ? "99+" : String.valueOf(count);
    }

    public static int getEstimateTimeDelivery(Context context, ConciergeSupplier supplier) {
        int est = -1;
        if (supplier != null) {
            est = supplier.getEstimateTimeDeliveryMinute();
        }

        if (est <= 0) {
            ConciergeShopSetting conciergeShopSetting = getConciergeShopSetting(context);
            if (conciergeShopSetting != null) {
                est = conciergeShopSetting.getDefaultEstimateTimeDeliveryMinute();
            }
        }

        return est;
    }

    public static ConciergeOrder fromNotificationOrder(Context context, NotificationOrder notificationOrder) {
        ConciergeOrder order = new ConciergeOrder();
        order.setId(notificationOrder.getId());
        order.setOrderNumber(notificationOrder.getOrderNumber());
        order.setOrderStatus(notificationOrder.getOrderStatus());
        order.setStatusString(notificationOrder.getStatusString(context));

        return order;
    }

    public static boolean isAddProuctButtonVisible(ConciergeMenuItem menuItem) {
        if (!menuItem.isInStock()) {
            return false;
        } else {
            if (MainApplication.isInExpressMode()) {
                return menuItem.isShopOpen();
            } else {
                return true;
            }
        }
    }

    public static boolean isValidAddressForExpressOrder(@Nonnull ConciergeUserAddress conciergeUserAddress) {
        return conciergeUserAddress.isInDeliveryRange();
    }
}
