package com.proapp.sompom.helper.upload;

import android.content.Context;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.policy.UploadPolicy;
import com.proapp.sompom.R;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.Map;

import timber.log.Timber;

/**
 * Created by imac on 4/1/16.
 */
public class AbsProductUploader extends AbsMyUploader {
    private Context mContext;
    private String mImagePath;
    private String mFileName;
    private String mPreset;
    private String mTag;

    /**
     * Constructor.
     *
     * @param context   :
     * @param imagePath : String the path of the image file in phone, where user want to us to upload.
     */
    public AbsProductUploader(final Context context,
                              final String imagePath,
                              final FileType fileType,
                              final Orientation orientation) {
        super(context);
        mContext = context;
        mImagePath = imagePath;
        mPreset = mContext.getString(R.string.cloudinary_preset);
        mTag = mContext.getString(R.string.product_tag);
        mFileName = "product_" + SharedPrefUtils.getUserId(context) + "_" + System.currentTimeMillis();
        Timber.e(mPreset + ", " + mTag + ", " + mFileName + ", " + mImagePath);
    }

    @Override
    public void cancelUpload(String requestId) {
        MediaManager.get().cancelRequest(requestId);
    }

    /**
     * Call for Process upload image.
     */
    public String doUpload() {
        return TransformationProvider.generateDefaultUploadRequest(
                FileType.IMAGE,
                mImagePath,
                mFileName,
                mPreset,
                mTag,
                getUploadFolder() + "post",
                new UploadPolicy.Builder().maxRetries(0).build(),
                this)
                .dispatch();
    }

    @Override
    public void onSuccess(String requestId, Map resultData) {
        handleOnSuccess(requestId, resultData);
    }
}
