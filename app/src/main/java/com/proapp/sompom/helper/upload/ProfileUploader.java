package com.proapp.sompom.helper.upload;

import android.content.Context;
import android.graphics.Bitmap;

import com.desmond.squarecamera.utils.media.ImageUtil;
import com.proapp.sompom.utils.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;

import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by nuonveyo on 4/27/18.
 */

public class ProfileUploader extends AbsUploader {

    private static final int PROFILE_PHOTO_RESIZE = 512;

    private Callback mCallback;

    public ProfileUploader(Context context, String imagePath, final UploadType uploadType, Callback callback) {
        super(context, imagePath, uploadType);
        mCallback = callback;
    }

    public static Observable<UploadResult> uploadProfileImage(Context context, String path) {
        Observable<UploadResult> ob = Observable.create(e -> {
            byte[] bytes = getUploadDataForProfilePhoto(context, path);
            ProfileUploader uploader = new ProfileUploader(context, path, UploadType.Profile, new Callback() {
                @Override
                public void onUploaded(UploadResult result) {
                    e.onNext(result);
                    e.onComplete();
                }

                @Override
                public void onUploadFail(Exception ex) {
                    e.onError(ex);
                    e.onComplete();
                }
            });
            uploader.doUpload(bytes);
        });
        ob = ob.onErrorResumeNext(throwable -> {
            if (throwable instanceof FileNotFoundException) {
                return Observable.just(new ProfileUploader.UploadResult());
            }
            return Observable.error(throwable);
        });
        return ob;
    }

    @Override
    public String getUploadFolder() {
        return super.getUploadFolder() + "profile";
    }

    public static Observable<UploadResult> uploadCoverImage(Context context, String path) {
        Observable<UploadResult> ob = Observable.create(e -> {
            byte[] bytes = getUploadDataForProfilePhoto(context, path);
            ProfileUploader uploader = new ProfileUploader(context, path, UploadType.Cover, new Callback() {
                @Override
                public void onUploaded(UploadResult uploadResult) {
                    e.onNext(uploadResult);
                    e.onComplete();
                }

                @Override
                public void onUploadFail(Exception ex) {
                    e.onError(ex);
                    e.onComplete();
                }
            });
            uploader.doUpload(bytes);
        });
        ob = ob.onErrorResumeNext(throwable -> {
            if (throwable instanceof FileNotFoundException) {
                return Observable.just(new ProfileUploader.UploadResult());
            }
            return Observable.error(throwable);
        });
        return ob;
    }

    @Override
    public void onUploadSucceeded(String imageUrl, String thumb) {
        if (mCallback != null) {
            mCallback.onUploaded(new UploadResult(imageUrl, thumb));
        }
    }

    @Override
    public void onUploadFail(String id, String ex) {
        if (mCallback != null) {
            mCallback.onUploadFail(new Exception(ex));
        }
    }

    private static byte[] getUploadDataForProfilePhoto(Context context, String uri) {
        File file = new File(uri);
        if (file.exists()) {
            Bitmap bmp = ImageUtil.getBitmapFromFilePath(uri);
            byte[] bytesArray = null;
            if (bmp != null) {
                if (bmp.getWidth() > PROFILE_PHOTO_RESIZE) {
                    Timber.i("Will resize profile photo to only " + PROFILE_PHOTO_RESIZE);
                    Bitmap resizeBitmap = ImageUtil.resizeBitmap(bmp, PROFILE_PHOTO_RESIZE, PROFILE_PHOTO_RESIZE);
                    if (resizeBitmap != null) {
                        bytesArray = convertByteArrayFromBitmap(resizeBitmap);
                    }
                } else {
                    bytesArray = convertByteArrayFromBitmap(bmp);
                }
            }

            if (bytesArray == null || bytesArray.length <= 0) {
                return FileUtils.convertFileToByArray(context, uri);
            } else {
                return bytesArray;
            }
        }

        return FileUtils.convertFileToByArray(context, uri);
    }

    private static byte[] convertByteArrayFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        bitmap.recycle();

        return byteArray;
    }

    public interface Callback {
        void onUploaded(UploadResult uploadResult);

        void onUploadFail(Exception ex);
    }

    public static class UploadResult {
        private String mUrl;
        private String mThumbnail;

        public UploadResult() {
        }

        public UploadResult(String url, String thumbnail) {
            mUrl = url;
            mThumbnail = thumbnail;
        }

        public String getUrl() {
            return mUrl;
        }


        public String getThumbnail() {
            return mThumbnail;
        }
    }
}
