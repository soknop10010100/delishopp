package com.proapp.sompom.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.pacoworks.rxpaper2.RxPaperBook;
import com.proapp.sompom.model.ConciergeCartModel;
import com.proapp.sompom.model.concierge.ConciergeComboChoiceSection;
import com.proapp.sompom.model.concierge.ConciergeComboItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItem;
import com.proapp.sompom.model.concierge.ConciergeMenuItemManipulationInfo;
import com.proapp.sompom.model.concierge.ConciergeSupplier;
import com.proapp.sompom.model.emun.ConciergeMenuItemOptionType;
import com.proapp.sompom.model.emun.ConciergeMenuItemType;
import com.proapp.sompom.model.result.ConciergeMenuItemOption;
import com.proapp.sompom.model.result.ConciergeMenuItemOptionSection;
import com.proapp.sompom.newui.fragment.AbsSupportShopCheckOutFragment;
import com.sompom.pushy.service.SendBroadCastHelper;

import java.math.BigDecimal;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Or Vitovongsak on 7/9/21.
 */

@SuppressLint("NewApi")
public class ConciergeCartHelper {

    private static final String CONCIERGE_BOOK_KEY = "CONCIERGE_BOOK_KEY";
    private static final String CONCIERGE_CART_KEY = "CONCIERGE_CART_KEY";

    // Main idea is that a reference of cart model is always kept in memory whenever an action is
    // made to the cart like add/remove/clear.
    private static ConciergeCartModel sCartModel;

    public enum CartModificationType {
        ADD,
        REMOVE,
        FORCE_REMOVE,
        MODIFY,
    }

    /**
     * Get the current cart model
     */
    public static Observable<ConciergeCartModel> getCart() {
        RxPaperBook cartBook = RxPaperBook.with(CONCIERGE_BOOK_KEY);
        // Retrieve an existing cart if existed, else return a new empty cart
        Single<ConciergeCartModel> readCartFromBook = cartBook.read(CONCIERGE_CART_KEY, new ConciergeCartModel());
        return readCartFromBook.toObservable().concatMap(cartModel -> {
            // Update reference of cart model whenever anything happens to cart
            sCartModel = cartModel;
            Timber.i("getCart: " + new Gson().toJson(sCartModel.getBasketItemList()));
            return Observable.just(cartModel);
        });
    }

    public static ConciergeCartModel getCartModel() {
        return sCartModel;
    }

    public static boolean isItemFromTheSameSupplierAsItemInBasket(ConciergeSupplier supplier) {
        if (sCartModel == null || sCartModel.getBasketItemList() == null || sCartModel.getBasketItemList().isEmpty()) {
            return true;
        }

        if (supplier == null) {
            return false;
        }

        ConciergeSupplier existedBasketSupplier = findSupplierInList(sCartModel.getBasketItemList(), supplier);
        boolean notSupportMultiExpressSupplierAlreadyExisted = isNotSupportMultiExpressSupplierAlreadyExisted(sCartModel.getBasketItemList());

        return existedBasketSupplier != null ||
                supplier.isAllowMultiBasket() ||
                !notSupportMultiExpressSupplierAlreadyExisted;
    }

    private static ConciergeSupplier findSupplierInList(List<ConciergeMenuItem> menuItemList, ConciergeSupplier supplier) {
        for (ConciergeMenuItem conciergeMenuItem : menuItemList) {
            if (conciergeMenuItem.getSupplier() != null && TextUtils.equals(conciergeMenuItem.getSupplier().getId(), supplier.getId())) {
                return conciergeMenuItem.getSupplier();
            }
        }

        return null;
    }

    private static boolean isNotSupportMultiExpressSupplierAlreadyExisted(List<ConciergeMenuItem> menuItemList) {
        for (ConciergeMenuItem conciergeMenuItem : menuItemList) {
            if (conciergeMenuItem.getSupplier() != null && !conciergeMenuItem.getSupplier().isAllowMultiBasket()) {
                return true;
            }
        }

        return false;
    }

    public static Observable<ConciergeCartModel> saveBasket(Context context, ConciergeCartModel basket) {
        Timber.i("saveBasket: " + new Gson().toJson(basket.getBasketItemList()));
        ConciergeHelper.saveBasketId(context, basket.getBasketId());
        RxPaperBook cartBook = RxPaperBook.with(CONCIERGE_BOOK_KEY);
        return cartBook.write(CONCIERGE_CART_KEY, basket)
                .andThen(new ObservableSource<ConciergeCartModel>() {
                    @Override
                    public void subscribe(Observer<? super ConciergeCartModel> observer) {
                        sCartModel = basket;
                        Timber.i("saveBasket success: " + new Gson().toJson(sCartModel.getBasketItemList()));
                        observer.onNext(basket);
                        observer.onComplete();
                    }
                });
    }

    public static Observable<Response<ConciergeCartModel>> rollbackToBackupProductCount(Context context, ConciergeMenuItem menuItem) {
//        Timber.i("rollbackToBackupProductCount: sCartModel: " + sCartModel + ", item: " + new Gson().toJson(menuItem));
        if (sCartModel != null) {
            return rollbackToBackupProductCountInternally(context, menuItem);
        } else {
            return getCart().concatMap(cartModel -> rollbackToBackupProductCountInternally(context, menuItem));
        }
    }

    private static Observable<Response<ConciergeCartModel>> rollbackToBackupProductCountInternally(Context context, ConciergeMenuItem menuItem) {
//        Timber.i("rollbackToBackupProductCountInternally: " + new Gson().toJson(menuItem));
        if (sCartModel.getBasketItemList() != null && !sCartModel.getBasketItemList().isEmpty()) {
            boolean shouldSave = false;
            ConciergeMenuItem foundLocalItem = null;
            for (ConciergeMenuItem localItem : sCartModel.getBasketItemList()) {
                if (TextUtils.equals(menuItem.getId(), localItem.getId())) {
//                    Timber.i("localItem: " + new Gson().toJson(localItem));
//                    Timber.i("rollbackToBackupProductCountInternally: localItem.getBackupProductCount(): " + localItem.getBackupProductCount() +
//                            ", localItem.getProductCount(): " + localItem.getProductCount() +
//                            ", Checking product count: " + menuItem.getProductCount() +
//                            ", Checking backup product count: " + menuItem.getBackupProductCount() +
//                            ", Product name: " + menuItem.getName());
                    if (localItem.getBackupProductCount() > 0) {
                        localItem.setProductCount(localItem.getBackupProductCount());
                        localItem.setSoftDeleted(false);
                        foundLocalItem = localItem;
                        shouldSave = true;
                        break;
                    }
                }
            }

//            Timber.i("Will save: " + shouldSave);
            ConciergeMenuItem finalFoundLocalItem = foundLocalItem;
            if (shouldSave) {
//                Timber.i("localItem: " + new Gson().toJson(foundLocalItem));
//                Timber.i("Before saving: " + new Gson().toJson(sCartModel.getBasketItemList()));
                calculateCartTotalPrice(sCartModel);
                return saveBasket(context, sCartModel).concatMap(cartModel -> {
                    ConciergeHelper.broadcastRefreshLocalBasketEventFromRollbackProductCount(context);
//                    Timber.i("saveBasket success: " + new Gson().toJson(cartModel.getBasketItemList()));
                    Intent intent = new Intent(AbsSupportShopCheckOutFragment.CART_ITEM_ADDED_EVENT);
                    intent.putExtra(AbsSupportShopCheckOutFragment.CART_EVENT_DATA, finalFoundLocalItem);
                    SendBroadCastHelper.verifyAndSendBroadCast(context, intent);

                    return Observable.just(Response.success(cartModel));
                });
            } else {
                return Observable.just(Response.success(sCartModel));
            }
        } else {
            return Observable.just(Response.success(sCartModel));
        }
    }

    public static Observable<Response<ConciergeCartModel>> updateBackupProductCount(Context context, ConciergeMenuItem menuItem) {
        if (sCartModel != null) {
            return updateBackupProductCountInternally(context, menuItem);
        } else {
            return getCart().concatMap(cartModel -> updateBackupProductCountInternally(context, menuItem));
        }
    }

    public static boolean isProductInLocalBasket(ConciergeMenuItem menuItem) {
        if (sCartModel.getBasketItemList() != null && !sCartModel.getBasketItemList().isEmpty()) {
            for (ConciergeMenuItem conciergeMenuItem : sCartModel.getBasketItemList()) {
                if (TextUtils.equals(conciergeMenuItem.getId(), menuItem.getId())) {
                    return true;
                }
            }
        }

        return false;
    }

    private static Observable<Response<ConciergeCartModel>> updateBackupProductCountInternally(Context context, ConciergeMenuItem menuItem) {
        if (sCartModel.getBasketItemList() != null && !sCartModel.getBasketItemList().isEmpty()) {
            boolean shouldSave = false;
            for (int size = sCartModel.getBasketItemList().size() - 1; size >= 0; size--) {
                ConciergeMenuItem localItem = sCartModel.getBasketItemList().get(size);
                if (TextUtils.equals(menuItem.getId(), localItem.getId())) {
                    if (menuItem.getProductCount() == 0) {
                        //Item is removed success from server basket.
                        sCartModel.getBasketItemList().remove(size);
                    } else {
                        localItem.setBackupProductCount(menuItem.getProductCount());
                    }
                    shouldSave = true;
                    break;
                }
            }

            if (shouldSave) {
                return saveBasket(context, sCartModel).concatMap(cartModel -> {
                    ConciergeHelper.broadcastRefreshLocalBasketEvent(context);
                    return Observable.just(Response.success(sCartModel));
                });
            } else {
                return Observable.just(Response.success(sCartModel));
            }
        } else {
            return Observable.just(Response.success(sCartModel));
        }
    }

    public static boolean isThereCurrentBasketItem() {
        return sCartModel != null && sCartModel.getBasketItemList() != null && !sCartModel.getBasketItemList().isEmpty();
    }

    public static Observable<ConciergeMenuItemManipulationInfo> addItemToCart(ConciergeMenuItem conciergeMenuItem,
                                                                              int addToPosition,
                                                                              boolean isPerformActionInBackground) {
//        Timber.i("addItemToCart: " + new Gson().toJson(shopToModify));
        return findItemAndApplyModification2(conciergeMenuItem, CartModificationType.ADD, false, addToPosition, isPerformActionInBackground);
    }

    public static Observable<Object> removeItem(ConciergeMenuItem conciergeMenuItem, boolean isFromEdit) {
        return findItemAndApplyModification(conciergeMenuItem, CartModificationType.REMOVE, isFromEdit);
    }

    public static Observable<Object> forceRemoveItem(ConciergeMenuItem conciergeMenuItem) {
        return findItemAndApplyModification(conciergeMenuItem, CartModificationType.FORCE_REMOVE, false);
    }

    public static Observable<ConciergeMenuItemManipulationInfo> modifyItemCount(ConciergeMenuItem itemToModify,
                                                                                int newItemCount,
                                                                                boolean isPerformActionInBackground) {
        itemToModify.setProductCount(newItemCount);
        return findItemAndApplyModification2(itemToModify, CartModificationType.MODIFY, false, -1, isPerformActionInBackground);
    }

    public static Observable<Object> clearCart(Context context) {
        ConciergeHelper.saveBasketId(context, null);
        RxPaperBook cartBook = RxPaperBook.with(CONCIERGE_BOOK_KEY);
        return cartBook.delete(CONCIERGE_CART_KEY)
                .andThen(Observable.just(new Object()))
                .concatMap(object -> {
                    Timber.i("Local basket cleared.");
                    sCartModel = null;
                    Intent intent = new Intent(AbsSupportShopCheckOutFragment.CART_CLEARED_EVENT);
                    SendBroadCastHelper.verifyAndSendBroadCast(context, intent);
                    return Observable.just(object);
                });
    }

    private static Observable<Object> findItemAndApplyModification(ConciergeMenuItem itemToAdd,
                                                                   CartModificationType modificationType,
                                                                   boolean isFromEdit) {

        // Remove any options that are not selected
        ConciergeMenuItem filteredMenuItem = ConciergeCartHelper.removeUnselectedOptions(itemToAdd);

        // Clear the item's shop menu to avoid issue with saving data
        if (itemToAdd.getShop() != null) {
            itemToAdd.getShop().setConciergeMenus(null);
        }

        if (modificationType == CartModificationType.ADD) {
            // Make sure that item added to cart is not 0 count
            if (itemToAdd.getProductCount() == 0) {
                itemToAdd.setProductCount(1);
            }
        }

        return getCart()
                .concatMap((Function<ConciergeCartModel, ObservableSource<ConciergeCartModel>>) cartModel -> {

                    // Get a copy of the list of item in cart currently to modify
                    List<ConciergeMenuItem> basketItemListCopy = cartModel.getBasketItemList();

                    // Check if the basket is empty
                    if (basketItemListCopy.isEmpty()) {
                        switch (modificationType) {
                            case ADD:
                                // If basket is empty, just add the item to cart
                                basketItemListCopy.add(itemToAdd);
                                break;
                            case MODIFY:
                            case REMOVE:
                                break;
                        }
                    } else {
                        // If the modification is force remove, only match item with their ID
                        // and then removed if found. Use case is for when removing item from
                        // "trending now" list since we have no control of what specific item with
                        // what combination of variation and addon in cart should be remove
                        if (modificationType == CartModificationType.FORCE_REMOVE) {
                            for (int index = 0; index < basketItemListCopy.size(); index++) {
                                // Check if item has the same ID
                                if (basketItemListCopy.get(index).areItemsTheSame(itemToAdd)) {
                                    // IF found, decrease count
                                    ConciergeMenuItem itemInCart = basketItemListCopy.get(index);
                                    itemInCart.decreaseProductCount();

                                    // If count is 0 after decrease, remove from cart, else save it
                                    // back to cart at it's correct index
                                    if (itemInCart.getProductCount() == 0) {
                                        basketItemListCopy.remove(index);
                                    } else {
                                        basketItemListCopy.set(index, itemInCart);
                                    }
                                    break;
                                }
                            }
                        } else {
                            ConciergeMenuItem itemInCart = null;
                            int itemInCartIndex = 0;
                            // Look for the same item with the same variation and addon in cart
                            for (int index = 0; index < basketItemListCopy.size(); index++) {
                                if (basketItemListCopy.get(index).areContentsTheSameForCartHelper(filteredMenuItem)) {
                                    itemInCartIndex = index;
                                    itemInCart = basketItemListCopy.get(index);
                                    break;
                                }
                            }

                            // There are some item in cart but there is no matching item, so add it
                            // cart
                            if (itemInCart == null) {
                                switch (modificationType) {
                                    case ADD:
                                        basketItemListCopy.add(itemToAdd);
                                        break;
                                    case REMOVE:
                                    case MODIFY:
                                        break;
                                }
                            } else {
                                switch (modificationType) {
                                    case ADD:
                                        // Get the item count in cart and sum it with the new amount
                                        // being added
                                        int currentCount = itemInCart.getProductCount();
                                        int additionalCount = itemToAdd.getProductCount();
                                        itemInCart.setProductCount(currentCount + additionalCount);
                                        basketItemListCopy.set(itemInCartIndex, itemInCart);
                                        break;
                                    case REMOVE:
                                        /*
                                           For edit order item mode, we will first remove the item from
                                           the card list and add them again.
                                         */
                                        if (isFromEdit) {
                                            itemInCart.setProductCount(0);
                                        } else {
                                            itemInCart.decreaseProductCount();
                                        }

                                        // If count is 0 after decrease, remove from cart, else save it
                                        // back to cart at it's correct index
                                        if (itemInCart.getProductCount() == 0) {
                                            basketItemListCopy.remove(itemInCartIndex);
                                        } else {
                                            basketItemListCopy.set(itemInCartIndex, itemInCart);
                                        }
                                        break;
                                    case MODIFY:
                                        // Directly modify the item count in cart. No use case for
                                        // now, maybe later we want like foodpanda where we tap the
                                        // item's current amount in cart to directly modify it
                                        // instead of incrementing one by one
                                        itemInCart.setProductCount(itemToAdd.getProductCount());
                                        if (itemInCart.getProductCount() == 0) {
                                            basketItemListCopy.remove(itemInCartIndex);
                                        } else {
                                            basketItemListCopy.set(itemInCartIndex, itemInCart);
                                        }
                                        break;
                                }
                            }
                        }
                    }

                    // Set the cart item with the newly updated list
                    cartModel.setBasketItemList(basketItemListCopy);
                    // Timber.i("After modification " + new Gson().toJson(cartModel));
                    return Observable.just(cartModel);
                })
                .concatMap(cartModel -> {
                    // Calculate cart's total price here
                    calculateCartTotalPrice(cartModel);
                    return Observable.just(cartModel);
                })
                .flatMap(cartModel -> {
                    // Write the cart model back into the book.
                    RxPaperBook cartBook = RxPaperBook.with(CONCIERGE_BOOK_KEY);
                    return cartBook.write(CONCIERGE_CART_KEY, cartModel)
                            .andThen(Observable.just(cartModel));
                }).concatMap(cartModel -> {
                    sCartModel = cartModel;
                    return Observable.just(cartModel);
                });
    }

    private static Observable<ConciergeMenuItemManipulationInfo> findItemAndApplyModification2(ConciergeMenuItem itemToModify,
                                                                                               CartModificationType modificationType,
                                                                                               boolean isFromEdit,
                                                                                               int addToPosition,
                                                                                               boolean isPerformActionInBackground) {
//        Timber.i("findItemAndApplyModification2: modificationType: " + modificationType + ", item: " + new Gson().toJson(itemToModify));

        /*
            Note:
            If isPerformActionInBackground is true means that we will update the local basket count directly without
            waiting the answer from server when user update product counter in basket.
         */

        // Remove any options that are not selected
        ConciergeMenuItem filteredMenuItem = ConciergeCartHelper.removeUnselectedOptions(itemToModify);
        ConciergeMenuItemManipulationInfo manipulationInfo = new ConciergeMenuItemManipulationInfo();
        manipulationInfo.setCartModificationType(modificationType);
        manipulationInfo.setMenuItem(itemToModify);

        // Clear the item's shop menu to avoid issue with saving data
        if (itemToModify.getShop() != null) {
            itemToModify.getShop().setConciergeMenus(null);
        }

        if (modificationType == CartModificationType.ADD) {
            // Make sure that item added to cart is not 0 count
            if (itemToModify.getProductCount() == 0) {
                itemToModify.setProductCount(1);
            }
        }

        return getCart()
                .concatMap((Function<ConciergeCartModel, ObservableSource<ConciergeCartModel>>) cartModel -> {
                    // Get a copy of the list of item in cart currently to modify
                    List<ConciergeMenuItem> basketItemListCopy = cartModel.getBasketItemList();

                    // Check if the basket is empty
                    if (basketItemListCopy.isEmpty()) {
                        switch (modificationType) {
                            case ADD:
                                // If basket is empty, just add the item to cart
                                basketItemListCopy.add(itemToModify);
                                break;
                            case MODIFY:
                            case REMOVE:
                                break;
                        }
                    } else {
                        // If the modification is force remove, only match item with their ID
                        // and then removed if found. Use case is for when removing item from
                        // "trending now" list since we have no control of what specific item with
                        // what combination of variation and addon in cart should be remove
                        if (modificationType == CartModificationType.FORCE_REMOVE) {
                            for (int index = 0; index < basketItemListCopy.size(); index++) {
                                // Check if item has the same ID
                                if (basketItemListCopy.get(index).areItemsTheSame(itemToModify)) {
                                    // IF found, decrease count
                                    ConciergeMenuItem itemInCart = basketItemListCopy.get(index);
                                    itemInCart.decreaseProductCount();

                                    // If count is 0 after decrease, remove from cart, else save it
                                    // back to cart at it's correct index
                                    if (itemInCart.getProductCount() == 0) {
                                        basketItemListCopy.remove(index);
                                    } else {
                                        basketItemListCopy.set(index, itemInCart);
                                    }
                                    break;
                                }
                            }
                        } else {
                            ConciergeMenuItem itemInCart = null;
                            int itemInCartIndex = 0;
                            // Look for the same item with the same variation and addon in cart
                            for (int index = 0; index < basketItemListCopy.size(); index++) {
                                if (modificationType == CartModificationType.MODIFY) {
                                    //Will check only item id
                                    if (TextUtils.equals(basketItemListCopy.get(index).getId(), itemToModify.getId())) {
                                        itemInCartIndex = index;
                                        itemInCart = basketItemListCopy.get(index);
                                        break;
                                    }
                                } else {
                                    if (basketItemListCopy.get(index).areContentsTheSameForCartHelper(filteredMenuItem)) {
                                        itemInCartIndex = index;
                                        itemInCart = basketItemListCopy.get(index);
                                        break;
                                    }
                                }
                            }

                            // There are some item in cart but there is no matching item, so add it
                            // cart
                            if (itemInCart == null) {
                                switch (modificationType) {
                                    case ADD:
                                        if (addToPosition >= 0 && addToPosition < basketItemListCopy.size()) {
                                            basketItemListCopy.add(addToPosition, itemToModify);
                                        } else {
                                            basketItemListCopy.add(itemToModify);
                                        }
                                        break;
                                    case REMOVE:
                                    case MODIFY:
                                        break;
                                }
                            } else {
//                                Timber.i("Item in card: " + new Gson().toJson(itemInCart) + ", findItemAndApplyModification2 of " + modificationType);
                                manipulationInfo.setMenuItem(itemInCart);
                                switch (modificationType) {
                                    case ADD:
                                        // Get the item count in cart and sum it with the new amount
                                        // being added
                                        manipulationInfo.setUpdated(true);
                                        int currentCount = itemInCart.getProductCount();
                                        int additionalCount = itemToModify.getProductCount();
                                        itemInCart.setProductCount(currentCount + additionalCount);
                                        basketItemListCopy.set(itemInCartIndex, itemInCart);
                                        break;
                                    case REMOVE:
                                        /*
                                           For edit order item mode, we will first remove the item from
                                           the card list and add them again.

                                         */
                                        if (isFromEdit) {
                                            itemInCart.setProductCount(0);
                                        } else {
                                            itemInCart.decreaseProductCount();
                                        }

                                        // If count is 0 after decrease, remove from cart, else save it
                                        // back to cart at it's correct index
                                        if (itemInCart.getProductCount() == 0) {
                                            basketItemListCopy.remove(itemInCartIndex);
                                        } else {
                                            basketItemListCopy.set(itemInCartIndex, itemInCart);
                                        }
                                        break;
                                    case MODIFY:
//                                        Timber.i("oldCount: " + itemInCart.getProductCount());
                                        // Directly modify the item count in cart. No use case for
                                        // now, maybe later we want like foodpanda where we tap the
                                        // item's current amount in cart to directly modify it
                                        // instead of incrementing one by one
                                        itemInCart.setProductCount(itemToModify.getProductCount());
                                        if (itemInCart.getProductCount() == 0) {
                                            if (isPerformActionInBackground) {
                                                /*
                                                    Just mark soft as soft delete since we don't get the
                                                    answer from server yet. If the remove action success from
                                                    server, we will completely remove this item from local
                                                    basket.
                                                 */
                                                itemInCart.setSoftDeleted(true);
                                                basketItemListCopy.set(itemInCartIndex, itemInCart);
                                            } else {
                                                basketItemListCopy.remove(itemInCartIndex);
                                            }
                                        } else {
                                            basketItemListCopy.set(itemInCartIndex, itemInCart);
                                        }
                                        break;
                                }
                            }
                        }
                    }

                    // Set the cart item with the newly updated list
                    cartModel.setBasketItemList(basketItemListCopy);
                    // Timber.i("After modification " + new Gson().toJson(cartModel));
                    return Observable.just(cartModel);
                })
                .concatMap(cartModel -> {
                    // Calculate cart's total price here
                    calculateCartTotalPrice(cartModel);
                    return Observable.just(cartModel);
                })
                .flatMap(cartModel -> {
                    // Write the cart model back into the book.
                    RxPaperBook cartBook = RxPaperBook.with(CONCIERGE_BOOK_KEY);
                    return cartBook.write(CONCIERGE_CART_KEY, cartModel)
                            .andThen(Observable.just(cartModel));
                }).concatMap(cartModel -> {
                    sCartModel = cartModel;
                    return Observable.just(manipulationInfo);
                });
    }

    public static ConciergeMenuItem removeUnselectedOptions(ConciergeMenuItem toCheck) {
        if (toCheck.getType() == ConciergeMenuItemType.ITEM) {
            if (toCheck.getOptions() != null && !toCheck.getOptions().isEmpty()) {
                for (ConciergeMenuItemOptionSection section : toCheck.getOptions()) {
                    if (section != null) {
                        if (section.getOptionItems() != null && !section.getOptionItems().isEmpty()) {
                            // Remove any item's option that are not selected
                            section.getOptionItems().removeIf(filter -> !filter.isSelected());
                        }
                    }
                }
            }
        } else if (toCheck.getType() == ConciergeMenuItemType.COMBO) {
            if (toCheck.getChoices() != null && !toCheck.getChoices().isEmpty()) {
                for (ConciergeComboChoiceSection comboChoiceSection : toCheck.getChoices()) {
                    if (comboChoiceSection != null) {
                        if (comboChoiceSection.getList() != null && !comboChoiceSection.getList().isEmpty()) {
                            // Remove any combo section item that are not selected
                            comboChoiceSection.getList().removeIf(comboItem -> !comboItem.isSelected());
                            for (ConciergeComboItem comboItem : comboChoiceSection.getList()) {
                                if (comboItem != null) {
                                    for (ConciergeMenuItemOptionSection itemOptionSection : comboItem.getOptions()) {
                                        if (itemOptionSection != null) {
                                            if (itemOptionSection.getOptionItems() != null && !itemOptionSection.getOptionItems().isEmpty()) {
                                                // Remove combo item's option that are not selected
                                                itemOptionSection.getOptionItems().removeIf(option -> !option.isSelected());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return toCheck;
    }

    public static void calculateCartTotalPrice(ConciergeCartModel cartModel) {
        cartModel.setTotalPrice(0);
        cartModel.setTotalDiscountPrice(0);

        for (ConciergeMenuItem product : cartModel.getBasketItemList()) {
            //Ignore calculate price of soft deleted item
            if (product.isSoftDeleted()) {
                continue;
            }

            double productAccumulatedPrice;
            productAccumulatedPrice = product.getPrice();
            product.setAccumulatedPrice(productAccumulatedPrice);

            //Calculate discount price
            double totalOriginalPrice = multiplyPrice(product.getOriginalPrice(), product.getProductCount());
            double totalPrice = multiplyPrice(product.getPrice(), product.getProductCount());
            double discountPrice = minusPrice(totalOriginalPrice, totalPrice);

            if (discountPrice < 0) {
                discountPrice = 0;
            }
            cartModel.setTotalDiscountPrice(addPriceAsFloat(discountPrice, cartModel.getTotalDiscountPrice()));

            // Check if the product is a normal item or a combo item
            if (product.getType() == ConciergeMenuItemType.ITEM) {
                // We'll loop through each item's option
                if (product.getOptions() != null && !product.getOptions().isEmpty()) {
                    // Calculate the item's accumulated price based on the selected options using
                    // the function: calculateItemAccumulatedPrice()
                    double itemAccumulatedPrice = calculateItemAccumulatedPrice(product.getOptions());
                    productAccumulatedPrice = addPrice(productAccumulatedPrice, itemAccumulatedPrice);
                    // Get the product's total price by multiplying the accumulated price with the
                    // count of the product
                    double totalProductPrice = multiplyPrice(productAccumulatedPrice, product.getProductCount());
                    // Set the product's total price
                    product.setTotalPrice(totalProductPrice);
                    // Set the product's accumulated price
                    product.setAccumulatedPrice(productAccumulatedPrice);

                    cartModel.setTotalPrice(addPriceAsFloat(cartModel.getTotalPrice(), totalProductPrice));
                } else {
                    double totalProductPrice;
                    // Since there are no variation, just get the product base price times the
                    // product's count
                    totalProductPrice = multiplyPrice(product.getPrice(), product.getProductCount());

                    // Set the product's total price
                    product.setTotalPrice(totalProductPrice);
                    cartModel.setTotalPrice(addPriceAsFloat(cartModel.getTotalPrice(), totalProductPrice));
                }
            } else if (product.getType() == ConciergeMenuItemType.COMBO) {
                if (product.getChoices() != null && !product.getChoices().isEmpty()) {
                    double comboAccumulatedChoicePrice = 0;

                    // Checking each combo choice section
                    for (ConciergeComboChoiceSection comboChoiceSection : product.getChoices()) {
                        if (comboChoiceSection != null) {
                            if (comboChoiceSection.getList() != null && !comboChoiceSection.getList().isEmpty()) {
                                // Checking each combo choice section's comboItem
                                for (ConciergeComboItem comboItem : comboChoiceSection.getList()) {
                                    // If the combo item is selected
                                    if (comboItem != null && comboItem.isSelected()) {
                                        // Calculated accumulated priced based on what combo item was selected
                                        // and all it's selected variations
                                        double calculateItemAccumulatedPrice = calculateItemAccumulatedPrice(comboItem.getOptions());
                                        double addPrice1 = addPrice(calculateItemAccumulatedPrice, comboItem.getPrice());
                                        comboAccumulatedChoicePrice = addPrice(comboAccumulatedChoicePrice, addPrice1);
                                        productAccumulatedPrice = addPrice(productAccumulatedPrice, comboAccumulatedChoicePrice);
                                    }
                                }
                            }
                        }
                    }

                    // Get the product's total price by multiplying the accumulated price with the
                    // count of the product
                    double totalProductPrice = multiplyPrice(productAccumulatedPrice, product.getProductCount());
                    // Set the product's total price
                    product.setTotalPrice(totalProductPrice);
                    // Set the product's accumulated price
                    product.setAccumulatedPrice(productAccumulatedPrice);
                    cartModel.setTotalPrice(addPriceAsFloat(cartModel.getTotalPrice(), totalProductPrice));
                }
            }
        }

        //Must call to calculate coupon price again.
        cartModel.calculateCouponPrice(cartModel.getTotalPrice());
        Timber.i("Calculate cart total price: " + cartModel.getTotalPrice() + ", Coupon price: " + cartModel.getCouponPrice());
    }

    /**
     * Method for calculating accumulated price based on selected option
     *
     * @param optionSections The item's option
     * @return The calculated price
     */
    public static double calculateItemAccumulatedPrice(List<ConciergeMenuItemOptionSection> optionSections) {
        BigDecimal productAccumulatedPrice = new BigDecimal(0);

        for (ConciergeMenuItemOptionSection option : optionSections) {
            ConciergeMenuItemOptionType optionType = ConciergeMenuItemOptionType.from(option.getType());
            // We'll loop through each option's item
            if (option.getOptionItems() != null) {
                for (ConciergeMenuItemOption optionItem : option.getOptionItems()) {
                    // If that option item is selected, add the price to accumulated price
                    if (optionItem.isSelected()) {
                        productAccumulatedPrice = productAccumulatedPrice.add(BigDecimal.valueOf(optionItem.getPrice()));
                        // If it's a Variation type, break and check the next option
                        if (optionType == ConciergeMenuItemOptionType.VARIATION) {
                            break;
                        }
                    }
                }
            }
        }

        return productAccumulatedPrice.doubleValue();
    }

    public static double multiplyPrice(float value, int multiplier) {
        BigDecimal valueToMultiply = new BigDecimal(value);
        BigDecimal multiplierValue = new BigDecimal(multiplier);

        return valueToMultiply.multiply(multiplierValue).doubleValue();
    }

    public static double multiplyPrice(double value, int multiplier) {
        BigDecimal valueToMultiply = new BigDecimal(value);
        BigDecimal multiplierValue = new BigDecimal(multiplier);

        return valueToMultiply.multiply(multiplierValue).doubleValue();
    }

    public static double addPrice(double originalValue, float addValue) {
        BigDecimal value1 = new BigDecimal(originalValue);
        BigDecimal value2 = new BigDecimal(addValue);

        return value1.add(value2).doubleValue();
    }

    public static float addPriceAsFloat(double originalValue, float addValue) {
        BigDecimal value1 = new BigDecimal(originalValue);
        BigDecimal value2 = new BigDecimal(addValue);

        return value1.add(value2).floatValue();
    }

    public static float addPriceAsFloat(double originalValue, double addValue) {
        BigDecimal value1 = new BigDecimal(originalValue);
        BigDecimal value2 = new BigDecimal(addValue);

        return value1.add(value2).floatValue();
    }

    public static double minusPrice(float inputValue1, float inputValue2) {
        BigDecimal value1 = new BigDecimal(inputValue1);
        BigDecimal value2 = new BigDecimal(inputValue2);

        return value1.subtract(value2).doubleValue();
    }

    public static double minusPrice(double inputValue1, double inputValue2) {
        BigDecimal value1 = new BigDecimal(inputValue1);
        BigDecimal value2 = new BigDecimal(inputValue2);

        return value1.subtract(value2).doubleValue();
    }

    public static double addPrice(double originalValue, double addValue) {
        BigDecimal value1 = new BigDecimal(originalValue);
        BigDecimal value2 = new BigDecimal(addValue);

        return value1.add(value2).doubleValue();
    }

    public static float roundPriceValue(float value, int precious) {
        return new BigDecimal(value).setScale(precious, BigDecimal.ROUND_HALF_EVEN).floatValue();
    }

    public static float roundPriceValue(double value, int precious) {
        return new BigDecimal(value).setScale(precious, BigDecimal.ROUND_HALF_EVEN).floatValue();
    }

    public static double roundPriceValueAsDouble(double value, int precious) {
        return new BigDecimal(value).setScale(precious, BigDecimal.ROUND_HALF_EVEN).doubleValue();
    }

    public static float roundPriceDown(float value) {
        return new BigDecimal(value).setScale(2, BigDecimal.ROUND_DOWN).floatValue();
    }

    public static int getExistingProductCountInCart(ConciergeMenuItem itemToCheck) {
        if (sCartModel == null) {
            return 0;
        }

        if (itemToCheck == null || itemToCheck.getId() == null) {
            return 0;
        }

        List<ConciergeMenuItem> itemInCart = sCartModel.getBasketItemList();

        int count = 0;

        if (itemInCart != null && !itemInCart.isEmpty()) {
            for (ConciergeMenuItem item : itemInCart) {
                if (TextUtils.equals(item.getId(), itemToCheck.getId())) {
                    count += item.getProductCount();
                }
            }
        }

        return count;
    }

    public static ConciergeMenuItem getExistingProductInCart(ConciergeMenuItem itemToCheck) {
        if (sCartModel == null) {
            return null;
        }

        List<ConciergeMenuItem> itemInCart = sCartModel.getBasketItemList();

        if (itemInCart != null && !itemInCart.isEmpty()) {
            for (ConciergeMenuItem item : itemInCart) {
                if (TextUtils.equals(item.getId(), itemToCheck.getId())) {
                    return item;
                }
            }
        }

        return null;
    }
}
