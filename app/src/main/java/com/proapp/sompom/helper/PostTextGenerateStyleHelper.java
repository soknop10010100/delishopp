package com.proapp.sompom.helper;

import android.text.Editable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TextView;

import androidx.annotation.FontRes;
import androidx.core.content.res.ResourcesCompat;

import com.example.usermentionable.widget.MentionsEditText;
import com.proapp.sompom.R;
import com.proapp.sompom.model.Media;
import com.proapp.sompom.utils.SpecialTextRenderUtils;

import java.util.List;

/**
 * Created by Chhom Veasna on 1/13/2020.
 */
public class PostTextGenerateStyleHelper {

    private static final int QUOTE_GENERATE_CHARACTER_SIZE = 160;

    private TextView mTextView;
    private TextGenerateType mTextGenerateType = TextGenerateType.UNKNOWN;
    private int mNormalSize;
    private int mQuoteSize;
    private @FontRes
    int mNormalFontRes;
    private @FontRes
    int mQuoteFontRes;
    private boolean mIsEditMode;
    private String mOriginalText;
    private boolean mIgnoreTextChangeCheck;
    private List<Media> mPostMedia;
    private int mPaddingTop;
    private int mPaddingLeft;
    private int mPaddingRight;
    private int mPaddingBottom;
    private boolean mIsIgnoreQuoteStyle;
    private PostTextGenerateStyleHelperListener mListener;

    public PostTextGenerateStyleHelper(TextView textView,
                                       String originalText,
                                       int normalSize,
                                       int quoteSize,
                                       @FontRes int normalFontRes,
                                       @FontRes int quoteFontRes,
                                       boolean isEditMode,
                                       PostTextGenerateStyleHelperListener listener) {
        mTextView = textView;
        mNormalSize = normalSize;
        mQuoteSize = quoteSize;
        mNormalFontRes = normalFontRes;
        mQuoteFontRes = quoteFontRes;
        mOriginalText = originalText;
        mIsEditMode = isEditMode;
        mListener = listener;

        if (textView instanceof MentionsEditText) {
            ((MentionsEditText) textView).addMentionEditTextListener(this::validateTextStyle);
        }

        textView.post(() -> {
            mPaddingLeft = textView.getPaddingLeft();
            mPaddingRight = textView.getPaddingRight();
            mPaddingTop = textView.getPaddingTop();
            mPaddingBottom = textView.getPaddingBottom();

            textView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!mIgnoreTextChangeCheck && !isPostContainMedia()) {
                        validateTextStyle();
                    }
                }
            });
        });
    }

    private boolean isPostContainMedia() {
        return mPostMedia != null && !mPostMedia.isEmpty();
    }

    public void setPostMedia(List<Media> postMedia) {
        mPostMedia = postMedia;
        if (isPostContainMedia()) {
            resetToNormalStyle();
        }
    }

    public void setIgnoreTextChangeCheck(boolean ignoreTextChangeCheck) {
        mIgnoreTextChangeCheck = ignoreTextChangeCheck;
    }

    public void setOriginalText(String originalText) {
        mOriginalText = originalText;
    }

    public boolean shouldRenderAsQuoteType() {
        if (SpecialTextRenderUtils.isContainSpecialInput(mIsEditMode ? mTextView.getText() : mOriginalText, mIsEditMode)) {
            return false;
        }

        if (mIsIgnoreQuoteStyle) {
            return false;
        }

        if (mListener != null && !mListener.isSupportQuoteRendering()) {
            return false;
        }

        return mTextView.getText().length() <= QUOTE_GENERATE_CHARACTER_SIZE;
    }

    public void revalidateTextStyle(boolean isIgnoreQuoteStyle) {
        mIsIgnoreQuoteStyle = isIgnoreQuoteStyle;
        validateTextStyle();
    }

    public void setIgnoreQuoteStyle(boolean ignoreQuoteStyle) {
        mIsIgnoreQuoteStyle = ignoreQuoteStyle;
    }

    private void validateTextStyle() {
        if (shouldRenderAsQuoteType()) {
            //Render as Quote style
            if (mTextGenerateType != TextGenerateType.QUOTE) {
                int padding = mTextView.getContext().getResources().getDimensionPixelSize(R.dimen.space_xlarge);
                mTextView.setPadding(padding, padding, padding, padding);
                mTextGenerateType = TextGenerateType.QUOTE;
                mTextView.setGravity(Gravity.CENTER);
                mTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                        mQuoteSize);
                mTextView.setTypeface(ResourcesCompat.getFont(mTextView.getContext(), mQuoteFontRes));
                mTextView.invalidate();
            }
        } else {
            //Render as normal with see more text if necessary
            if (mTextGenerateType != TextGenerateType.NORMAL) {
                mTextView.setPadding(mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom);
                mTextGenerateType = TextGenerateType.NORMAL;
                mTextView.setGravity(Gravity.START);
                mTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                        mNormalSize);
                mTextView.setTypeface(ResourcesCompat.getFont(mTextView.getContext(), mNormalFontRes));
                mTextView.invalidate();
            }
        }
    }

    public void resetToNormalStyle() {
        if (mTextGenerateType != TextGenerateType.NORMAL) {
            mTextView.setPadding(mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom);
            mTextGenerateType = TextGenerateType.NORMAL;
            mTextView.setGravity(Gravity.START);
            mTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    mNormalSize);
            mTextView.setTypeface(ResourcesCompat.getFont(mTextView.getContext(), mNormalFontRes));
            mTextView.invalidate();
        }
    }

    public enum TextGenerateType {
        NORMAL(),
        QUOTE(),
        UNKNOWN()
    }

    public interface PostTextGenerateStyleHelperListener {
        boolean isSupportQuoteRendering();
    }
}
