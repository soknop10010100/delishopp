package com.proapp.sompom.helper;

import android.text.TextUtils;

import java.util.HashMap;

/**
 * Created by Chhom Veasna on 1/17/2020.
 */
public final class WallSeeMoreTextHelper {

    private static HashMap<String, SeeMorePost> sSeeMorePostHashMap = new HashMap<>();

    public static Integer getLineEndIndexForSeeMoreText(String postId, String text) {
        SeeMorePost seeMorePost = sSeeMorePostHashMap.get(postId);
        if (seeMorePost != null) {
            //If the text of post updated it will consider as new.
            if (TextUtils.equals(text, seeMorePost.getText())) {
                return seeMorePost.getEndLineIndex();
            } else {
                return null;
            }
        }

        return null;
    }

    public static void addLineEndIndexForSeeMoreText(String postId, String text, int lineEndIndex) {
        sSeeMorePostHashMap.put(postId, new SeeMorePost(postId, text, lineEndIndex));
    }

    public static void clearLineEndIndexForSeeMoreText() {
        sSeeMorePostHashMap.clear();
    }

    public static class SeeMorePost {

        private String mPostId;
        private String mText;

        /*
        The last index of text which display in two {@SEE_MORE_LINE} lines which this index will
        be the start index of generating see more text.
         */
        private int mEndLineIndex;

        public SeeMorePost(String postId, String text, int endLineIndex) {
            mPostId = postId;
            mText = text;
            mEndLineIndex = endLineIndex;
        }

        public String getPostId() {
            return mPostId;
        }

        public String getText() {
            return mText;
        }

        public int getEndLineIndex() {
            return mEndLineIndex;
        }
    }
}
