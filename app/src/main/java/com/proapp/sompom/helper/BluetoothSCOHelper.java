package com.proapp.sompom.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;

import com.proapp.sompom.chat.call.CallHelper;

import timber.log.Timber;

public class BluetoothSCOHelper {

    private Context mContext;
    private AudioManager mAudioManager;
    private BroadcastReceiver mBroadcastReceiver;

    public BluetoothSCOHelper(Context context) {
        mContext = context;
    }

    public void init() {
        mAudioManager = AudioManagerProvider.getInstance(mContext).getAudioManager();
        IntentFilter newIntent = new IntentFilter();
        newIntent.addAction(AudioManager.ACTION_SCO_AUDIO_STATE_UPDATED);
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int scoAudioState = intent.getIntExtra(AudioManager.EXTRA_SCO_AUDIO_STATE, -1);
                Timber.i("scoAudioState: " + scoAudioState);
            }
        };
        mContext.registerReceiver(mBroadcastReceiver, newIntent);
    }

    public void stopSco() {
        if (CallHelper.isBluetoothHeadsetDeviceConnected()) {
            Timber.i("stopSco");
            resetScoMode();
        }
    }

    public void startSco() {
        if (CallHelper.isBluetoothHeadsetDeviceConnected()) {
            Timber.i("startSco");
            mAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            mAudioManager.startBluetoothSco();
            mAudioManager.setBluetoothScoOn(true);
        }
    }

    public void resetScoMode() {
        Timber.i("resetScoMode");
        mAudioManager.setMode(AudioManager.MODE_NORMAL);
        mAudioManager.stopBluetoothSco();
        mAudioManager.setBluetoothScoOn(false);
    }

    public void unregister() {
        if (mContext != null) {
            mContext.unregisterReceiver(mBroadcastReceiver);
        }
    }
}
