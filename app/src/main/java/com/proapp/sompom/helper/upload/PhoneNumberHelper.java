package com.proapp.sompom.helper.upload;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.proapp.sompom.helper.SentryHelper;

import timber.log.Timber;

public class PhoneNumberHelper {

    public static final String DEFAULT_COUNTY = "KH";

    public static String getDefaultCountry(Context context) {
        String defaultCountry = detectSIMCountry(context);
        if (TextUtils.isEmpty(defaultCountry)) {
            defaultCountry = detectNetworkCountry(context);
        }
        if (TextUtils.isEmpty(defaultCountry)) {
            defaultCountry = detectLocaleCountry(context);
        }

        if (defaultCountry != null && !TextUtils.isEmpty(defaultCountry)) {
            defaultCountry = defaultCountry.toUpperCase();
        }

        Timber.i("defaultCountry: " + defaultCountry);
        if (TextUtils.isEmpty(defaultCountry)) {
            defaultCountry = DEFAULT_COUNTY;
        }

        return defaultCountry;
    }

    private static String detectSIMCountry(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            Timber.i("detectSIMCountry");
            return telephonyManager.getSimCountryIso();
        } catch (Exception e) {
            Timber.e(e.getMessage());
            SentryHelper.logSentryError(e);
        }

        return null;
    }

    private static String detectNetworkCountry(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            Timber.i("detectNetworkCountry");
            return telephonyManager.getNetworkCountryIso();
        } catch (Exception e) {
            Timber.e(e.getMessage());
            SentryHelper.logSentryError(e);
        }

        return null;
    }

    private static String detectLocaleCountry(Context context) {
        try {
            Timber.i("detectLocaleCountry");
            return context.getResources().getConfiguration().locale.getCountry();
        } catch (Exception e) {
            Timber.e(e.getMessage());
            SentryHelper.logSentryError(e);
        }

        return null;
    }
}
