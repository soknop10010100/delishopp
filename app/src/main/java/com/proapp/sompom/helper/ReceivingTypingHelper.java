package com.proapp.sompom.helper;

import com.proapp.sompom.chat.service.TypingIndicator;
import com.proapp.sompom.model.result.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

public class ReceivingTypingHelper {

    private static final long TYPING_INDICATOR_DURATION = TypingIndicator.RECEIVING_DURATION;

    //We create a map to store handler of each user to avoid many creation of handle instance when receiving typing message
    //and for clearing the handle state when the chat screen is closed.
    private Map<String, MyHandler> mTypingTimeOutHandlers = new HashMap<>();
    private List<String> mReceivingTypingSenderList = new ArrayList<>();
    private ReceivingTypingHelperListener mListener;

    public ReceivingTypingHelper(ReceivingTypingHelperListener listener) {
        mListener = listener;
    }

    public void addTyping(final User user) {
        if (!mReceivingTypingSenderList.contains(user.getId())) {
            if (mListener != null) {
                mListener.onAddReceivingTyping(user);
            }
            MyHandler typingTimeOutHandler = mTypingTimeOutHandlers.get(user.getId());
            if (typingTimeOutHandler == null) {
                typingTimeOutHandler = new MyHandler(TYPING_INDICATOR_DURATION, () -> {
                    removeTyping(user);
                });
                mTypingTimeOutHandlers.put(user.getId(), typingTimeOutHandler);
            }
            mReceivingTypingSenderList.add(user.getId());
            typingTimeOutHandler.startDelay();
        }
    }

    public void removeTyping(User user) {
        if (mReceivingTypingSenderList.contains(user.getId())) {
            mReceivingTypingSenderList.remove(user.getId());
            if (mListener != null) {
                mListener.onDisplayReceivingTypingTimeOutOrRemoved(user);
            }
        }
    }

    public void onDestroy() {
        for (MyHandler value : mTypingTimeOutHandlers.values()) {
            value.destroy();
            value = null;
        }
        mTypingTimeOutHandlers.clear();
        mReceivingTypingSenderList.clear();
    }

    public interface ReceivingTypingHelperListener {
        void onAddReceivingTyping(User sender);

        void onDisplayReceivingTypingTimeOutOrRemoved(User sender);
    }
}
