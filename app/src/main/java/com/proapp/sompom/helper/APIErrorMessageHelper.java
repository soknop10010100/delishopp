package com.proapp.sompom.helper;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.JsonElement;
import com.proapp.sompom.model.result.LocalizationMessage;
import com.proapp.sompom.utils.SharedPrefUtils;

public class APIErrorMessageHelper {

    public static String getError(Context context, LocalizationMessage message) {
        String languageCode = SharedPrefUtils.getLanguage(context);
        if (TextUtils.isEmpty(languageCode)) {
            languageCode = com.resourcemanager.helper.LocaleManager.getValidDeviceLangeCode();
        }
        JsonElement jsonElement = message.getAsJSON().get(languageCode);
        if (jsonElement != null) {
            return jsonElement.getAsString();
        }

        return message.getDefaultOne();
    }
}
