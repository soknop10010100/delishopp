package com.proapp.sompom.helper.upload;

import android.content.Context;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.policy.UploadPolicy;
import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.utils.SharedPrefUtils;

import java.util.Map;

import timber.log.Timber;

/**
 * Created by imac on 4/1/16.
 */
public abstract class AbsUploader extends SharedUploader {

    protected Context mContext;
    protected String mImagePath;
    protected String mFileName;
    protected String mPreset;
    protected String mTag;
    protected boolean mShouldOptimize = true;

    /*
        * Media name rule

        Image, Audio, Video:
        + chat: chat_userId_date
        + post: post_userId_date
        + profile: profile_userId_date

        File:
        + chat: chat_date_userId_filename
        + post: post_date_userId_filename
     */

    /**
     * Constructor.
     *
     * @param context   :
     * @param imagePath : String the path of the image file in phone, where user want to us to upload.
     */
    public AbsUploader(final Context context,
                       final String imagePath,
                       final UploadType uploadType) {
        super(context);
        mContext = context;
        mImagePath = imagePath;
        if (uploadType == UploadType.Cover) {
            mPreset = mContext.getString(R.string.cloudinary_preset);
            mTag = mContext.getString(R.string.profile_cover_tag);
            mFileName = context.getString(R.string.upload_coverUserName_key) + "_" + SharedPrefUtils.getUserId(context) + "_" + System.currentTimeMillis();
        } else if (uploadType == UploadType.Address) {
            mPreset = mContext.getString(R.string.cloudinary_preset);
            mTag = mContext.getString(R.string.address_tag);
            mFileName = context.getString(R.string.upload_address_key) + "_" + SharedPrefUtils.getUserId(context) + "_" + System.currentTimeMillis();
            mShouldOptimize = false;
        } else if (uploadType == UploadType.Receipt) {
            mPreset = mContext.getString(R.string.cloudinary_preset);
            mTag = mContext.getString(R.string.receipt_tag);
            mFileName = context.getString(R.string.upload_receipt_key) + "_" + SharedPrefUtils.getUserId(context) + "_" + System.currentTimeMillis();
            mShouldOptimize = false;
        } else {
            mPreset = mContext.getString(R.string.cloudinary_preset);
            mTag = mContext.getString(R.string.profile_tag);
            mFileName = context.getString(R.string.upload_profileUserName_key) + "_" + SharedPrefUtils.getUserId(context) + "_" + System.currentTimeMillis();
        }

        Timber.i(mPreset + ", " + mTag + ", " + mFileName + ", " + mImagePath);
    }

    public static void cancelUpload(String requestId) {
        MediaManager.get().cancelRequest(requestId);
    }

    public Context getContext() {
        return mContext;
    }

    public void setShouldOptimize(boolean shouldOptimize) {
        this.mShouldOptimize = shouldOptimize;
    }

    /**
     * Call for Process upload image.
     */
    public String doUpload(byte[] bytes) {
        UploadCallback callback = new UploadCallback() {
            @Override
            public void onStart(String requestId) {
                Timber.v("onServiceStart: " + requestId + "  " + MediaManager.get().hashCode());
            }

            @Override
            public void onProgress(String requestId, long bytes, long totalBytes) {
                Timber.v("onProgress: " + requestId + "  " + MediaManager.get().hashCode());
            }

            @Override
            public void onSuccess(String requestId, Map resultData) {
                Timber.i("Result: " + new Gson().toJson(resultData));
                if (mShouldOptimize && TransformationProvider.shouldOptimizeResource(resultData)) {
                    String originalUrl = TransformationProvider.getDefaultUrl(resultData);
                    String thumbnailUrl = TransformationProvider.getProfileOptimizeTransformation(resultData);
                    Timber.i("originalUrl: " + originalUrl + ", thumbnailUrl: " + thumbnailUrl);
                    //Use the same transform resource
                    onUploadSucceeded(originalUrl, thumbnailUrl);
                } else {
                    String url = TransformationProvider.getDefaultOptimizeTransformation(resultData);
                    onUploadSucceeded(url, url);
                }
            }

            @Override
            public void onError(String requestId, ErrorInfo error) {
                Timber.e("onError: " + requestId + ", error:" + error.getDescription());
                onUploadFail(requestId, error.getDescription());
            }

            @Override
            public void onReschedule(String requestId, ErrorInfo error) {
                MediaManager.get().cancelRequest(requestId);
                onUploadFail(requestId, error.getDescription());
                Timber.e("onReschedule: " + requestId + ", error:" + error);
            }
        };

        if (bytes != null && bytes.length > 0) {
            return TransformationProvider.generateDefaultUploadRequest(
                    FileType.IMAGE,
                    bytes,
                    mFileName,
                    mPreset,
                    mTag,
                    getUploadFolder(),
                    new UploadPolicy.Builder().maxRetries(0).build(),
                    callback)
                    .dispatch();
        } else {
            return TransformationProvider.generateDefaultUploadRequest(
                    FileType.IMAGE,
                    mImagePath,
                    mFileName,
                    mPreset,
                    mTag,
                    getUploadFolder(),
                    new UploadPolicy.Builder().maxRetries(0).build(),
                    callback)
                    .dispatch();
        }
    }


    public abstract void onUploadSucceeded(String imageUrl, String thumb);

    /**
     * Call when upload is failed.
     *
     * @param ex : Exception that occur during uploading.
     */
    public abstract void onUploadFail(String id, String ex);

    public enum UploadType {
        Cover, Profile, Address, Receipt
    }
}
