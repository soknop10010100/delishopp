package com.proapp.sompom.helper;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;
import com.proapp.sompom.R;
import com.sompom.baseactivity.PermissionCheckHelper;

import timber.log.Timber;


/**
 * Created by he.rotha on 6/10/16.
 */
public class RequestLocationManager implements PermissionCheckHelper.OnPermissionCallback {

    private static final int mRequestCode = 0X100;
    public static final String[] LOCATION_PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    private LocationSettingsRequest.Builder mBuilder;
    private AppCompatActivity mAppCompatActivity;
    private PermissionCheckHelper mLocationPermission;
    private LocationManager mLocationManager;
    private double mLongitude;
    private double mLatitude;
    private RequestLocationManager.OnRequestLocationCallback mListener;
    protected boolean mEnableShowErrorPopup;
    private String mLocationRequestMessage;
    private boolean mIsLocationRequiredPopUpDisplayed;

    public RequestLocationManager(AppCompatActivity appCompatActivity, String locationRequestMessage) {
        mAppCompatActivity = appCompatActivity;
        mLocationRequestMessage = locationRequestMessage;
    }

    public void execute(boolean enableShowErrorPopup) {
        if (isLocationPermissionGranted(mAppCompatActivity)) {
            executeRequestInternally(enableShowErrorPopup);
        } else {
            showLocationRequestInformation((dialog, which) -> {
                executeRequestInternally(enableShowErrorPopup);
            }, (dialog, which) -> onPermissionDenied(new String[]{},
                    LOCATION_PERMISSIONS,
                    false));
        }
    }

    private void executeRequestInternally(boolean enableShowErrorPopup) {
        mEnableShowErrorPopup = enableShowErrorPopup;
        if (mLocationPermission == null) {
            mLocationPermission = new PermissionCheckHelper(mAppCompatActivity,
                    mRequestCode);
            mLocationPermission.setPermissions(LOCATION_PERMISSIONS);
            mLocationPermission.setCallback(RequestLocationManager.this);
        }
        mLocationPermission.execute();
    }

    public static boolean isLocationPermissionGranted(Context context) {
        boolean isGranted = false;
        for (String locationPermission : LOCATION_PERMISSIONS) {
            boolean granted = ActivityCompat.checkSelfPermission(context, locationPermission) == PackageManager.PERMISSION_GRANTED;
            if (granted) {
                isGranted = true;
                break;
            }
        }

        return isGranted;
    }

    private void showLocationRequestInformation(DialogInterface.OnClickListener allowToRequestListener,
                                                DialogInterface.OnClickListener denyToRequestListener) {
        new AlertDialog.Builder(mAppCompatActivity)
                .setTitle(R.string.app_name)
                .setMessage(mLocationRequestMessage)
                .setNegativeButton(R.string.popup_cancel_button, denyToRequestListener)
                .setPositiveButton(R.string.popup_ok_button, allowToRequestListener)
                .setCancelable(false)
                .create()
                .show();
    }

    public AppCompatActivity getActivity() {
        return mAppCompatActivity;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onPermissionGranted() {
        Timber.i("onPermissionGranted");
        //Permission is being checked here.
        if (!isLocationPermissionGranted(mAppCompatActivity)) {
            mListener.onLocationPermissionDeny(false);
            return;
        }

        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient
                .getLastLocation()
                .addOnCompleteListener(getActivity(), task -> {
                    try {
                        Location location = task.getResult();
                        if (location != null) {
                            mLatitude = location.getLatitude();
                            mLongitude = location.getLongitude();
                            mListener.onLocationSuccess(mLatitude, mLongitude);
                        } else {
                            mListener.onLocationDisable();
                            if (mEnableShowErrorPopup && !mIsLocationRequiredPopUpDisplayed) {
                                showRequestLocationEnablePopup();
                            }
                        }
                    } catch (Exception ex) {
                        mListener.onLocationDisable();
                        if (mEnableShowErrorPopup && !mIsLocationRequiredPopUpDisplayed) {
                            showRequestLocationEnablePopup();
                        }
                    }
                });
    }

    @Override
    public void onPermissionDenied(String[] grantedPermission,
                                   String[] deniedPermission,
                                   boolean isUserPressNeverAskAgain) {
        mListener.onLocationPermissionDeny(isUserPressNeverAskAgain);
    }

    private void requestNewLocation() {
        Timber.e("requestNewLocation");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) mAppCompatActivity.getSystemService(Context.LOCATION_SERVICE);
        }

        final LocationListener locationListener = new LocationListener() {

            @Override
            public void onLocationChanged(Location location) {
                Timber.e("onLocationChanged");
                mIsLocationRequiredPopUpDisplayed = false;
                mLongitude = location.getLongitude();
                mLatitude = location.getLatitude();
                if (mAppCompatActivity != null) {
                    if (mLatitude == 0 && mLongitude == 0) {
                        onPermissionGranted();
                        return;
                    } else {
                        mListener.onLocationSuccess(mLatitude, mLongitude);
                    }

                    if (!isLocationPermissionGranted(mAppCompatActivity)) {
                        mListener.onLocationPermissionDeny(false);
                        return;
                    }
                    mLocationManager.removeUpdates(this);
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Timber.e("onStatusChanged");
            }

            @Override
            public void onProviderEnabled(String provider) {
                Timber.i("onProviderEnabled");
                mIsLocationRequiredPopUpDisplayed = false;
            }

            @Override
            public void onProviderDisabled(String provider) {
                Timber.e("onProviderDisabled");
                mIsLocationRequiredPopUpDisplayed = false;
            }
        };

        final long minTime = 1000;
        float minMeter = 0;

        final Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);

        String provider = mLocationManager.getBestProvider(criteria, false);
        Timber.e("provider: " + provider);
        mLocationManager.requestLocationUpdates(provider,
                minTime, minMeter, locationListener);

        if (TextUtils.isEmpty(provider)) {
            mListener.onLocationDisable();
        }
    }

    private void showRequestLocationEnablePopup() {
        mIsLocationRequiredPopUpDisplayed = true;
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(5000);
        mBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(getActivity()).checkLocationSettings(mBuilder.build());
        result.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response = task.getResult(ApiException.class);
                requestNewLocation();
                // All location settings are satisfied. The client can initialize location
                // requests here.
            } catch (ApiException exception) {
                mIsLocationRequiredPopUpDisplayed = false;
                switch (exception.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.
                        try {
                            // Cast to a resolvable exception.
                            ResolvableApiException resolvable = (ResolvableApiException) exception;
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(getActivity(), mRequestCode);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        } catch (ClassCastException e) {
                            // Ignore, should be an impossible error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                    default:
                        mListener.onLocationDisable();
                        break;
                }
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case mRequestCode:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        requestNewLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        mListener.onLocationDisable();
                        break;
                    default:
                        break;
                }
                break;
        }
    }


    public void setOnRequestLocationCallback(RequestLocationManager.OnRequestLocationCallback listener) {
        mListener = listener;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (mLocationPermission != null) {
            mLocationPermission.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public interface OnRequestLocationCallback {
        void onLocationSuccess(double latitude, double longtitude);

        void onLocationDisable();

        void onLocationPermissionDeny(boolean isUserPressNeverAskAgain);
    }
}
