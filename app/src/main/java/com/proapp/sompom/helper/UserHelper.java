package com.proapp.sompom.helper;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.example.usermentionable.utils.RenderTextAsMentionable;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.onesignal.OneSignal;
import com.proapp.sompom.R;
import com.proapp.sompom.licence.db.LicenseSynchronizationDb;
import com.proapp.sompom.licence.model.SynchroniseData;
import com.proapp.sompom.licence.utils.FileUtils;
import com.proapp.sompom.listener.OnCallbackListener;
import com.proapp.sompom.listener.UserListAdaptive;
import com.proapp.sompom.model.ExchangeAuthData;
import com.proapp.sompom.model.ItemPrivacy;
import com.proapp.sompom.model.UserFeatureSetting;
import com.proapp.sompom.model.UserGroup;
import com.proapp.sompom.model.WelcomeDataItem;
import com.proapp.sompom.model.WelcomeDataItemWrapper;
import com.proapp.sompom.model.WelcomeDataWrapper;
import com.proapp.sompom.model.WelcomeItem;
import com.proapp.sompom.model.emun.PublishItem;
import com.proapp.sompom.model.emun.UserType;
import com.proapp.sompom.model.request.AddPlayerIdBody;
import com.proapp.sompom.model.result.AppSetting;
import com.proapp.sompom.model.result.NotificationSettingModel;
import com.proapp.sompom.model.result.Theme;
import com.proapp.sompom.model.result.User;
import com.proapp.sompom.model.result.UserBadge;
import com.proapp.sompom.observable.ErrorThrowable;
import com.proapp.sompom.observable.ResponseObserverHelper;
import com.proapp.sompom.services.ApiService;
import com.proapp.sompom.utils.SharedPrefUtils;
import com.resourcemanager.helper.LocaleManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import im.crisp.client.ChatActivity;
import im.crisp.client.Crisp;
import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 7/19/2019.
 */
public final class UserHelper {
    /**
     * User type and there post accessibility
     * customer => (post) => public only
     * customer => (see) => public only
     * <p>
     * staff/administrative => (post) => (public / corporate)
     * <p>
     * staff/administrative => (see) => (public / corporate)
     * <p>
     * intervenant => (post) => public only
     * <p>
     * intervenant => (see) => public only
     */

    public static final int MAX_NOTIFICATION_BADE_DISPLAY = 99;

    private UserHelper() {
    }

    public static String getValidDisplayName(String checkingName) {
        return RenderTextAsMentionable.getValidDisplayName(checkingName);
    }

    /**
     * We have to check two level to determine if user can make post.
     * 1. Application level {@link SynchroniseData}
     * 2. At user level {@link UserFeatureSetting}
     */
    public static boolean isAbleToCreatePost(Context context) {
        SynchroniseData synchroniseData = LicenseSynchronizationDb.
                readSynchroniseData(context);
        return synchroniseData != null
                && synchroniseData.getNewsFeed() != null
                && synchroniseData.getNewsFeed().isEnablePost() &&
                isUserHasRightToPost(context);
    }

    public static boolean areUserMandatoryPropertyCompleted(Context context) {
        User user = SharedPrefUtils.getUser(context);
        return user != null && !TextUtils.isEmpty(user.getFirstName()) && !TextUtils.isEmpty(user.getLastName());
    }

    public static ExchangeAuthData getRequireDataForFillUpProfileMandatoryProperty(Context context) {
        User user = SharedPrefUtils.getUser(context);
        ExchangeAuthData exchangeAuthData = new ExchangeAuthData();
        exchangeAuthData.setUserId(user.getId());
        exchangeAuthData.setAccessToken(SharedPrefUtils.getAccessToken(context));
        exchangeAuthData.setFirstName(user.getFirstName());
        exchangeAuthData.setLastName(user.getLastName());
        exchangeAuthData.setEmail(user.getEmail());
        return exchangeAuthData;
    }

    public static JsonObject getUpdateUserObject(User user) {
        /*
         Remove some unused or conflicted properties when update to server.
       */
        user.setSeenMessage(null);
        user.setIsAuthorizedUser(null);

        Gson gson = new Gson();
        return gson.fromJson(gson.toJson(user), JsonObject.class);
    }

    public static void saveAuthorizedUserListFromResponse(Context context, List<UserGroup> userGroups) {

        // Now we save the whole list of UserGroup instead of just the users in order to allow quick
        // load of contact screen

        List<User> commonUserList = new ArrayList<>();
        for (UserGroup userGroup : userGroups) {
            if (userGroup.getParticipants() != null && !userGroup.getParticipants().isEmpty()) {
                commonUserList.addAll(userGroup.getParticipants());
                userGroup.setParticipants(getCommonUserList(commonUserList));
                commonUserList.clear();
            }
        }
        saveUserContact(context, userGroups);
    }

    public static void saveUserContact(Context context, List<UserGroup> userList) {
        if (userList != null && !userList.isEmpty()) {
            SharedPrefUtils.setString(context, SharedPrefUtils.AUTHORIZED_USER_LIST,
                    new Gson().toJson(userList));
        } else {
            SharedPrefUtils.setString(context, SharedPrefUtils.AUTHORIZED_USER_LIST, "");
        }
    }

    public static List<User> getUserContact(Context context) {
        String data = SharedPrefUtils.getString(context, SharedPrefUtils.AUTHORIZED_USER_LIST);
        List<UserGroup> userGroups = new Gson().fromJson(data, new TypeToken<List<UserGroup>>() {
        }.getType());

        List<User> users = new ArrayList<>();
        if (userGroups != null) {
            for (UserGroup userGroup : userGroups) {
                if (userGroup.getParticipants() != null && !userGroup.getParticipants().isEmpty()) {
                    users.addAll((userGroup.getParticipants()));
                }
            }
        }

        return users;
    }

    public static List<UserGroup> getFullUserContact(Context context) {
        String data = SharedPrefUtils.getString(context, SharedPrefUtils.AUTHORIZED_USER_LIST);
        return new Gson().fromJson(data, new TypeToken<List<UserGroup>>() {
        }.getType());
    }

    public static List<UserListAdaptive> getUserListForContactScreen(Context context) {
        String data = SharedPrefUtils.getString(context, SharedPrefUtils.AUTHORIZED_USER_LIST);
        List<UserGroup> userGroups = new Gson().fromJson(data, new TypeToken<List<UserGroup>>() {
        }.getType());
        List<UserListAdaptive> adaptiveList = new ArrayList<>();
        if (userGroups != null) {
            Timber.i("User contact section size: " + userGroups.size());
            for (UserGroup userGroup : userGroups) {
                if (userGroup.getParticipants() != null && !userGroup.getParticipants().isEmpty()) {
                    adaptiveList.add(userGroup);
                    for (User participant : userGroup.getParticipants()) {
                        participant.setIsAuthorizedUser(true);
                    }
                    adaptiveList.addAll(userGroup.getParticipants());
                }
            }
        }

        Timber.i("Final user contact section size: " + adaptiveList.size());
        return adaptiveList;
    }

    public static boolean isInUserContact(Context context, String userId) {
        List<User> userContact = getUserContact(context);
        if (userContact != null && !userContact.isEmpty()) {
            for (User user : userContact) {
                if (TextUtils.equals(userId, user.getId())) {
                    return true;
                }
            }
        }

        return false;
    }

    public static List<String> getUserContactId(Context context) {
        List<User> userContact = getUserContact(context);
        List<String> userIds = new ArrayList<>();
        if (userContact != null && !userContact.isEmpty()) {
            for (User user : userContact) {
                userIds.add(user.getId());
            }
        }

        return userIds;
    }

    public static boolean isInUserContact(List<String> idList, String checkId) {
        return idList != null && idList.contains(checkId);
    }

    public static void saveUserBadge(Context context, UserBadge userBadge) {
        SharedPrefUtils.setString(context, SharedPrefUtils.USER_NOTIFICATION, new Gson().toJson(userBadge));
    }

    public static UserBadge getUserBadge(Context context) {
        String string = SharedPrefUtils.getString(context, SharedPrefUtils.USER_NOTIFICATION);
        return new Gson().fromJson(string, UserBadge.class);
    }

    public static void resetNotificationBadge(Context context) {
        UserBadge userBadge = getUserBadge(context);
        if (userBadge != null) {
            userBadge.setUnreadNotification((long) 0);
            saveUserBadge(context, userBadge);
        }
    }

    public static void clearUserData(Context context) {
        saveUserContact(context, new ArrayList<>());
        saveUserBadge(context, null);
        saveSuggestedUser(context, new ArrayList<>());
        OfflinePostDataHelper.resetLocalData(context);
    }

    public static int getUserLastPostPrivacy(Context context) {
        PublishItem item = PublishItem.getItem(SharedPrefUtils.getInt(context, User.LAST_POST_PRIVACY));
        UserType userType = getUserType(context);
        if (userType == UserType.CUSTOMER ||
                userType == UserType.INTERVENANT) {
            //Those user types can post only as public {PublishItem.EveryOne}.
            if (item != PublishItem.EveryOne) {
                return PublishItem.EveryOne.getId();
            } else {
                return item.getId();
            }
        } else {
            return item.getId();
        }
    }

    public static void addOneSignalPlayerId(Context context, ApiService apiService, String newPlayerId) {
        Observable<Response<Object>> responseObservable = apiService.addPlayerId(new AddPlayerIdBody(newPlayerId));
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(context, responseObservable);
        helper.execute();
    }

    public static void setUserLastPostPrivacy(Context context, ApiService apiService, int value) {
        SharedPrefUtils.setInt(context, User.LAST_POST_PRIVACY, value);
        User user = new User();
        user.setLastPostPrivacy(value);
        Observable<Response<User>> responseObservable = apiService.updateUser(SharedPrefUtils.getUserId(context),
                getUpdateUserObject(user),
                SharedPrefUtils.getLanguage(context));
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(context,
                responseObservable);
        helper.execute();
    }

    public static void setUserThemeId(Context context, ApiService apiService, int value) {
        User user = new User();
        user.setThemeId(value);
        Observable<Response<User>> responseObservable = apiService.updateUser(SharedPrefUtils.getUserId(context),
                getUpdateUserObject(user),
                SharedPrefUtils.getLanguage(context));
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(context,
                responseObservable);
        helper.execute();
    }

    public static void setUserLanguage(Context context, ApiService apiService) {
        String language = SharedPrefUtils.getLanguage(context);
        if (!TextUtils.isEmpty(language)) {
            User user = new User();
            user.setLanguage(language);
            Observable<Response<User>> responseObservable = apiService.updateUser(SharedPrefUtils.getUserId(context),
                    getUpdateUserObject(user),
                    SharedPrefUtils.getLanguage(context));
            ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(context,
                    responseObservable);
            helper.execute();
        }
    }

    public static void setCrispUserProfile(User user) {
        if (user != null) {
            Crisp.setUserAvatar(user.getUserProfileThumbnail());
            Crisp.setUserNickname(user.getFullName());
            Crisp.setUserPhone(user.getPhone());
            Crisp.setUserEmail(user.getEmail());
        }
    }

    public static void openCrispChatSupportScreen(Context context) {
        Intent crispIntent = new Intent(context, ChatActivity.class);
        context.startActivity(crispIntent);
        FlurryHelper.logEvent(context, FlurryHelper.SCREEN_CHAT_SUPPORT);
    }

    public static void clearUserPlayerId(Context context,
                                         ApiService apiService,
                                         UserHelperListener listener) {
        NotificationServiceHelper.loadPlayerId(context, (playerId, type) -> {
            Timber.i("clearUserPlayerId: " + playerId);
            if (!TextUtils.isEmpty(playerId)) {
                AddPlayerIdBody removePlayerId = new AddPlayerIdBody();
                removePlayerId.setOneSignalPlayerId(playerId);
                Observable<Response<Object>> responseObservable = apiService.removePlayerId(removePlayerId);
                ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(context, responseObservable);
                helper.execute(new OnCallbackListener<Response<Object>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        Timber.i("onFail: " + ex.getCode());
                        if (listener != null) {
                            listener.onClearUserPlayerIdFinish(ex.getMessage());
                        }
                    }

                    @Override
                    public void onComplete(Response<Object> result) {
                        if (listener != null) {
                            listener.onClearUserPlayerIdFinish(null);
                        }
                    }
                });
            } else {
                if (listener != null) {
                    listener.onClearUserPlayerIdFinish(null);
                }
            }
        });
    }

    public static List<User> getCommonUserList(List<User> participant) {
        List<User> users = new ArrayList<>();
        if (participant != null) {
            for (User user : participant) {
                users.add(User.getCommonUserInstance(user));
            }
        }

        return users;
    }

    public static void saveSuggestedUser(Context context, List<User> users) {
        if (users != null) {
            SharedPrefUtils.setString(context, SharedPrefUtils.SUGGESTED_USERS,
                    new Gson().toJson(getCommonUserList(users)));
        }
    }

    public static List<User> getSuggestedUser(Context context) {
        String data = SharedPrefUtils.getString(context, SharedPrefUtils.SUGGESTED_USERS);
        return new Gson().fromJson(data, new TypeToken<List<User>>() {
        }.getType());
    }

    public static void updateContactUserInfo(Context context, List<User> contactUserInfo) {
        List<UserGroup> userGroups = getFullUserContact(context);
        if (userGroups != null) {
            boolean isAnyUpdated = false;
            for (UserGroup userGroup : userGroups) {
                List<User> userContact = userGroup.getParticipants();

                for (User user : userContact) {
                    for (User user1 : contactUserInfo) {
                        if (TextUtils.equals(user.getId(), user1.getId())) {
                            user.copyCommonUserProperties(user1);
                            isAnyUpdated = true;
                        }
                    }
                }
            }

            if (isAnyUpdated) {
                saveUserContact(context, userGroups);
            }
        }
    }

    public static WelcomeDataWrapper getLocalWelcomeData(Context context) {
        return new Gson().fromJson(FileUtils.readRawTextFile(context, R.raw.welcome), WelcomeDataWrapper.class);
    }

    public static void loadWelcomeData(Context context, UserHelperListener listener) {
        WelcomeDataItemWrapper welcomeData = getWelcomeData(context);
        if (welcomeData != null) {
            /*
               - Morning: 00:00 - 10:30h
               - Day: 10:301 - 17:30h
               - Night: 17:31h ->
             */
            Calendar currentTime = Calendar.getInstance();

            //Morning
            Calendar morningTime = Calendar.getInstance();
            morningTime.set(Calendar.HOUR_OF_DAY, 10);
            morningTime.set(Calendar.MINUTE, 30);
            if (currentTime.getTimeInMillis() <= morningTime.getTimeInMillis()) {
                if (welcomeData.getMorningData() != null && listener != null) {
                    listener.onLoadWelcomeDataSuccess(getWelcomeItem(context, welcomeData.getMorningData()));
                }
                return;
            }

            //Day and Night
            Calendar dayTime = Calendar.getInstance();
            dayTime.set(Calendar.HOUR_OF_DAY, 17);
            dayTime.set(Calendar.MINUTE, 30);
            if (currentTime.getTimeInMillis() <= dayTime.getTimeInMillis()) {
                //Day
                if (welcomeData.getDayData() != null && listener != null) {
                    listener.onLoadWelcomeDataSuccess(getWelcomeItem(context, welcomeData.getDayData()));
                }
            } else {
                //Night
                if (welcomeData.getNightData() != null && listener != null) {
                    listener.onLoadWelcomeDataSuccess(getWelcomeItem(context, welcomeData.getNightData()));
                }
            }
        } else {
            listener.onLoadWelcomeDataSuccess(null);
        }
    }

    private static WelcomeItem getWelcomeItem(Context context, WelcomeDataItem welcomeDataItem) {
        User user = SharedPrefUtils.getUser(context);
        WelcomeItem welcomeItem = new WelcomeItem();
        welcomeItem.setUserName(user.getFirstName());
        welcomeItem.setWelcomeTitle(welcomeDataItem.getTitle());
        if (welcomeDataItem.getDescriptions().length > 0) {
            welcomeItem.setWelcomeDescription(welcomeDataItem.getDescriptions()[new Random().nextInt(welcomeDataItem.getDescriptions().length)]);
        }

        return welcomeItem;
    }

    public static WelcomeDataItemWrapper getWelcomeData(Context context) {
        AppSetting appSetting = SharedPrefUtils.getAppSetting(context);
        if (appSetting != null && appSetting.getWelcomeData() != null) {
            String languageCode = SharedPrefUtils.getLanguage(context);
            if (TextUtils.equals(languageCode, LocaleManager.FRENCH_LANGUAGE_KEY)) {
                return appSetting.getWelcomeData().getFrenchData();
            } else if (TextUtils.equals(languageCode, LocaleManager.KHMER_LANGUAGE_KEY)) {
                return appSetting.getWelcomeData().getKhmerData();
            } else {
                //English as default
                return appSetting.getWelcomeData().getEnglishData();
            }
        }

        return null;
    }

    public static AppTheme getSelectedThemeResource(Context context) {
        AppTheme appTheme = ThemeManager.getAppTheme(context);
        List<AppTheme> availableThemes = getAvailableThemeResources(context);
        for (AppTheme availableTheme : availableThemes) {
            if (availableTheme.getId() == appTheme.getId()) {
                return appTheme;
            }
        }

        //Return the default one.
        return AppTheme.White;
    }

    public static List<AppTheme> getAvailableThemeResources(Context context) {
        List<AppTheme> appThemeList = new ArrayList<>();

        AppSetting appSetting = SharedPrefUtils.getAppSetting(context);
        if (appSetting != null) {
            if (appSetting.getThemes() != null) {
                for (Theme theme : appSetting.getThemes()) {
                    AppTheme appTheme = AppTheme.fromValue(theme.getId());
                    if (appTheme != AppTheme.UNKNOWN) {
                        appThemeList.add(appTheme);
                    }
                }
            }
        }

        return appThemeList;
    }

    public static Theme getSelectedThemeFromAppSetting(Context context) {
        AppTheme selectedThemeResource = getSelectedThemeResource(context);
        AppSetting appSetting = SharedPrefUtils.getAppSetting(context);
        if (appSetting != null && appSetting.getThemes() != null) {
            for (Theme theme : appSetting.getThemes()) {
                if (theme.getId() == selectedThemeResource.getId()) {
                    return theme;
                }
            }
        }

        return null;
    }

    public static String getTimezone(Context context) {
        NotificationSettingModel notificationSetting = SharedPrefUtils.getNotificationSetting(context);
        if (notificationSetting != null) {
            return notificationSetting.getTimeZone();
        }

        return null;
    }

    public static void checkToUpdateUserTimezone(Context context, ApiService apiService, UserHelperListener listener) {
        String timezoneId = TimeZone.getDefault().getID();
        Timber.i("timezoneId: " + timezoneId);
        NotificationSettingModel notificationSetting = SharedPrefUtils.getNotificationSetting(context);
        if (!TextUtils.equals(timezoneId, notificationSetting.getTimeZone())) {
            Timber.i("Will update user time zone to: " + timezoneId);
            notificationSetting.setTimeZone(timezoneId);
            SharedPrefUtils.setNotificationSetting(context, notificationSetting);
            User user = new User();
            user.setNotificationSettingModel(notificationSetting);
            Observable<Response<User>> responseObservable = apiService.updateUser(SharedPrefUtils.getUserId(context),
                    getUpdateUserObject(user),
                    SharedPrefUtils.getLanguage(context));
            ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(context,
                    responseObservable);
            helper.execute(new OnCallbackListener<Response<User>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    if (listener != null) {
                        listener.onUpdateUserDataFinished();
                    }
                }

                @Override
                public void onComplete(Response<User> result) {
                    if (listener != null) {
                        listener.onUpdateUserDataFinished();
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onUpdateUserDataFinished();
            }
        }
    }

    public static void checkUserCallFeatureSetting(Context context, UserHelperListener listener) {
        SynchroniseData data = LicenseSynchronizationDb.readSynchroniseData(context);
        UserFeatureSetting userFeatureSetting = UserHelper.getUserFeatureSetting(context);
        if (data != null && userFeatureSetting != null) {
            boolean isCallEnable = data.getCall() != null &&
                    data.getCall().isEnableCall() &&
                    userFeatureSetting.isEnableCall();
            boolean isVideoCallEnable = data.getCall() != null &&
                    data.getCall().isEnableVideoCall() &&
                    userFeatureSetting.isEnableVideoCall();
            if (listener != null) {
                listener.onCheckUserCallFeatureFinished(isCallEnable, isVideoCallEnable);
            }
        } else {
            if (listener != null) {
                listener.onCheckUserCallFeatureFinished(false, false);
            }
        }
    }

    public static String getUserIdFromOneSignal() {
        if (OneSignal.getDeviceState() != null) {
            return OneSignal.getDeviceState().getUserId();
        }

        return null;
    }

    public static User getNecessaryUserInfo(Context context) {
        User user = SharedPrefUtils.getUser(context);
        if (user != null) {
            User newUser = new User();
            newUser.setId(user.getId());
            newUser.setFirstName(user.getFirstName());
            newUser.setLastName(user.getLastName());
            newUser.setUserProfileThumbnail(user.getUserProfileThumbnail());
        }

        return user;
    }

    public static UserType getUserType(Context context) {
        return UserType.fromValue(SharedPrefUtils.getString(context, SharedPrefUtils.USER_TYPE));
    }

    public static List<ItemPrivacy> getCreatePostPrivacy(Context context) {
        List<ItemPrivacy> itemPrivacyList = new ArrayList<>();
        UserType userType = getUserType(context);
        if (userType == UserType.CUSTOMER ||
                userType == UserType.INTERVENANT) {
            itemPrivacyList.add(new ItemPrivacy(PublishItem.EveryOne, false));
        } else if (userType == UserType.STAFF || userType == UserType.ADMINISTRATIVE) {
            itemPrivacyList.add(new ItemPrivacy(PublishItem.EveryOne, false));
            itemPrivacyList.add(new ItemPrivacy(PublishItem.Follower, false));
        } else {
            //For other type we, will show all options
            itemPrivacyList.add(new ItemPrivacy(PublishItem.EveryOne, false));
            itemPrivacyList.add(new ItemPrivacy(PublishItem.Follower, false));
        }

        return itemPrivacyList;
    }

    public static boolean isGuestUserExist(Context context) {
        User user = SharedPrefUtils.getUser(context);
        return user.isGuestUser();
    }

    public static boolean isUserHasRightToPost(Context context) {
        UserFeatureSetting userFeatureSetting = getUserFeatureSetting(context);
        return userFeatureSetting.isEnablePost();
    }

    public static UserFeatureSetting getUserFeatureSetting(Context context) {
        return SharedPrefUtils.getUserFeatureSetting(context);
    }

    public static void testInvalidToken(Context context) {
        SharedPrefUtils.setAccessToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkOTE2ZWYxNzRiMzM2N2JhN2Y0ZjM3YyIsImlhdCI6MTY1ODE5ODgxMywiZXhwIjoxNjc0MDEwMDEzfQ.D_TtKtrm8T0J26rczD7od0W4N4H3hspnTPX_qgqCQ18", context);
    }

    public static class UserHelperListener {

        protected void onClearUserPlayerIdFinish(String error) {
            //Empty Impl...
        }

        protected void onLoadWelcomeDataSuccess(WelcomeItem welcomeItem) {
            //Empty Impl...
        }

        protected void onUpdateUserDataFinished() {
            //Empty Impl...
        }

        protected void onCheckUserCallFeatureFinished(boolean isCallEnable, boolean isVideoCallEnable) {
            //Empty Impl...
        }
    }
}
