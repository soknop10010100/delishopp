package com.proapp.sompom.helper;

import android.app.Activity;
import android.os.Build;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.proapp.sompom.R;
import com.proapp.sompom.model.Locations;
import com.proapp.sompom.newui.dialog.MessageDialog;
import com.proapp.sompom.utils.IntentUtil;

import timber.log.Timber;

/**
 * Created by nuonveyo
 * on 1/31/19.
 */

public class CheckRequestLocationCallbackHelper implements SingleRequestLocation.Callback {
    private final Activity mContext;
    private final Callback mCallback;

    public CheckRequestLocationCallbackHelper(Activity activity, Callback callback) {
        mContext = activity;
        mCallback = callback;
    }

    @Override
    public void onComplete(Locations location, boolean isNeverAskAgain, boolean isPermissionEnabled, boolean isLocationEnabled) {
        Timber.i("onComplete: isNeverAskAgain" + isNeverAskAgain +
                ", isPermissionEnabled: " + isPermissionEnabled +
                ", isLocationEnabled: " + isLocationEnabled +
                ", location: " + new Gson().toJson(location));
        if (isNeverAskAgain) {
            showAskingToEnableLocationAccessFromAppSettingPopup((AppCompatActivity) mContext, null);
        }
        mCallback.onComplete(location);
    }

    public interface Callback {
        void onComplete(Locations locations);
    }

    public static void showAskingToEnableLocationAccessFromAppSettingPopup(AppCompatActivity context,
                                                                           View.OnClickListener cancelListener) {
        String title = context.getString(R.string.popup_permission_request_title);
        String description = context.getString(R.string.popup_permission_force_request_location_description,
                context.getString(R.string.app_name));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            title = context.getString(R.string.popup_permission_request_enable_precise_location_title);
            description = context.getString(R.string.popup_permission_request_enable_precise_location_description);
        }

        MessageDialog dialog = new MessageDialog();
        dialog.setTitle(title);
        dialog.setCancelable(false);
        dialog.setMessage(description);
        dialog.setLeftText(context.getString(R.string.popup_permission_app_setting_title), view -> IntentUtil.openAppSetting(context));
        dialog.setRightText(context.getString(R.string.popup_no_button), cancelListener);
        dialog.show(context.getSupportFragmentManager(), MessageDialog.TAG);
    }
}
