package com.proapp.sompom.helper.upload;

/**
 * Created by He Rotha on 6/20/18.
 */
public interface UploadListener {

    void onUploadSucceeded(String requestId, String imageUrl, String thumb);

    void onUploadFail(String id, Exception ex);

    void onUploadProgressing(String id, int percent, long uploadedBytes, long totalBytes);

    void onCancel(String id);
}
