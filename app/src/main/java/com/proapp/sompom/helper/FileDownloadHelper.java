package com.proapp.sompom.helper;

import android.text.TextUtils;

import com.liulishuo.filedownloader.BaseDownloadTask;
import com.proapp.sompom.utils.FileUtils;

import java.io.File;

import timber.log.Timber;

public class FileDownloadHelper {

    public static final String CHAT_DIR = "chats";
    public static final String WALLSTREET_DIR = "walls";

    private String mDownloadId;
    private String mParentDir;
    private String mSubDir;
    private FileDownloadListener mListener;
    private long mFileSize;
    private String mUrl;
    private File mDownloadedFile;

    public FileDownloadHelper(String url,
                              String fileName,
                              String downloadId,
                              String parentDir,
                              String subDir,
                              long fileSize,
                              FileDownloadListener listener) {
        mUrl = url;
        mDownloadId = downloadId;
        mParentDir = parentDir;
        mSubDir = subDir;
        mListener = listener;
        mFileSize = fileSize;
        mDownloadedFile = generateDownloadFilePath(TextUtils.isEmpty(fileName) ?
                FileUtils.getFileNameFromUrl(mUrl) : fileName);
    }

    public FileDownloadHelper(String url,
                              String downloadId,
                              File path,
                              long fileSize,
                              FileDownloadListener listener) {
        mUrl = url;
        mDownloadId = downloadId;
        mDownloadedFile = path;
        mFileSize = fileSize;
        mListener = listener;
    }

    public void performDownload() {
        com.liulishuo.filedownloader
                .FileDownloader
                .getImpl()
                .create(mUrl)
                .addHeader("Accept-Encoding", "identity") //Must be add this header to avoid download failure with .txt, .json files...
                .setPath(mDownloadedFile.getAbsolutePath())
                .setForceReDownload(true)
                .setListener(new com.liulishuo.filedownloader.FileDownloadListener() {
                    @Override
                    protected void pending(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                        Timber.i("pending");
                    }

                    @Override
                    protected void progress(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                        Timber.i("progress: soFarBytes=> " + soFarBytes + ", of " + totalBytes);
                        if (mListener != null) {
                            mListener.onDownloadInProgress(String.valueOf(task.getId()),
                                    mDownloadId,
                                    soFarBytes,
                                    totalBytes > 0 ? totalBytes : mFileSize);
                        }
                    }

                    @Override
                    protected void completed(BaseDownloadTask task) {
                        Timber.i("completed");
                        if (mListener != null) {
                            mListener.onDownloadSuccess(String.valueOf(task.getId()),
                                    mDownloadId,
                                    mDownloadedFile);
                        }
                    }

                    @Override
                    protected void paused(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                        Timber.i("paused");
                    }

                    @Override
                    protected void error(BaseDownloadTask task, Throwable e) {
                        Timber.e("error: " + e.getMessage());
                        if (mListener != null) {
                            mListener.onDownloadFailed(String.valueOf(task.getId()), mDownloadId, e);
                        }
                    }

                    @Override
                    protected void warn(BaseDownloadTask task) {
                        Timber.i("warn");
                    }

                    @Override
                    protected void started(BaseDownloadTask task) {
                        Timber.i("started");
                        if (mListener != null) {
                            mListener.onDownloadGetStarted(String.valueOf(task.getId()), mDownloadId);
                        }
                    }

                    @Override
                    protected void connected(BaseDownloadTask task, String etag, boolean isContinue, int soFarBytes, int totalBytes) {
                        Timber.i("connected: totalBytes=> " + totalBytes + ", isContinue: " + isContinue);
                    }

                    @Override
                    protected void blockComplete(BaseDownloadTask task) {
                        Timber.i("blockComplete");
                    }

                    @Override
                    protected void retry(BaseDownloadTask task, Throwable ex, int retryingTimes, int soFarBytes) {
                        Timber.i("retry: ex: " + ex.getMessage() + ", retryingTimes: " + retryingTimes);
                    }
                }).start();
    }

    private File generateDownloadFilePath(String fileName) {
        if (!TextUtils.isEmpty(mSubDir)) {
            File parent = new File(mParentDir + File.separator + mSubDir);
            if (!parent.exists()) {
                parent.mkdir();
            }
            return new File(parent, fileName);
        }

        return new File(mParentDir, fileName);
    }

    public interface FileDownloadListener {

        default void onDownloadGetStarted(String requestId, String downloadId) {
        }

        default void onDownloadSuccess(String requestId, String downloadId, File file) {
        }

        default void onDownloadFailed(String requestId, String downloadId, Throwable ex) {
        }

        default void onDownloadInProgress(String requestId, String downloadId, long downloadedBytes, long totalBytes) {
        }
    }
}