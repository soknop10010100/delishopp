<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <import type="com.proapp.sompom.utils.AttributeConverter" />

        <import type="com.proapp.sompom.R" />

        <import type="com.proapp.sompom.model.emun.OpenBrandDetailType" />

        <variable
            name="viewModel"
            type="com.proapp.sompom.viewmodel.ConciergeBrandDetailFragmentViewModel" />
    </data>

    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="?screen_background">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:orientation="vertical">

            <androidx.appcompat.widget.Toolbar
                android:id="@+id/toolbar"
                android:layout_width="match_parent"
                android:layout_height="?actionBarSize"
                android:background="?screen_background"
                android:elevation="0dp"
                app:contentInsetLeft="0dp"
                app:contentInsetStart="0dp"
                app:layout_scrollFlags="scroll|enterAlways|snap"
                app:popupTheme="@style/AppTheme.PopupOverlay">

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:focusable="true"
                    android:focusableInTouchMode="true"
                    android:gravity="center_vertical"
                    android:orientation="horizontal">

                    <LinearLayout
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:onClick="@{() -> viewModel.onBackPress()}"
                        android:paddingStart="@dimen/space_large"
                        android:paddingEnd="@dimen/space_large">

                        <include layout="@layout/custom_back_btn" />

                    </LinearLayout>

                    <EditText
                        android:id="@+id/editText"
                        style="@style/SearchInputStyle"
                        android:layout_width="0dp"
                        android:layout_weight="1"
                        android:hint="@string/search_item_home_screen_input_hint"
                        android:minHeight="48dp"
                        android:padding="@dimen/space_medium"
                        android:text="@={viewModel.searchText}"
                        app:addTextChangeListener="@{viewModel.onSearchTextChange()}" />

                    <ImageView
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginEnd="@dimen/space_medium"
                        android:contentDescription="@string/app_name"
                        android:onClick="@{() -> viewModel.onClearText()}"
                        android:padding="@dimen/space_medium"
                        android:src="@drawable/ic_eraser"
                        android:visibility="@{viewModel.isShowClearIcon? View.VISIBLE: View.INVISIBLE}"
                        app:tint="?button_selected" />

                </LinearLayout>

            </androidx.appcompat.widget.Toolbar>

            <LinearLayout
                android:id="@+id/containerLayout"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:background="?screen_background"
                android:orientation="vertical"
                android:paddingStart="@dimen/space_large"
                android:paddingTop="@dimen/space_large"
                android:paddingEnd="@dimen/space_large">

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:gravity="center_vertical"
                    android:orientation="horizontal">

                    <TextView
                        style="@style/ShopTitleStyle"
                        android:layout_width="0dp"
                        android:layout_marginEnd="@dimen/normal_padding"
                        android:layout_weight="1"
                        android:ellipsize="end"
                        android:maxLines="2"
                        android:text="@{viewModel.title}"
                        android:textAllCaps="true"
                        android:textColor="?shop_category_section_title"
                        android:textSize="@dimen/text_large"
                        android:visibility="@{viewModel.openType == OpenBrandDetailType.OPEN_AS_SEARCH_GENERAL_PRODUCT_MODE? View.INVISIBLE: View.VISIBLE}"
                        tools:text="ALL"
                        tools:visibility="visible" />

                    <LinearLayout
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:gravity="center"
                        android:orientation="horizontal">

                        <RelativeLayout
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:layout_marginEnd="@dimen/space_large">

                            <LinearLayout
                                android:id="@+id/sortContainer"
                                android:layout_width="wrap_content"
                                android:layout_height="@dimen/concierge_view_item_by_icon_size"
                                android:layout_centerInParent="true"
                                android:background="@drawable/unselected_message_segment_background"
                                android:backgroundTint="@{!viewModel.sortCriteriaSelected? AttributeConverter.convertAttrToColor(context, R.attr.shop_category_view_item_by_background) : AttributeConverter.convertAttrToColor(context, R.attr.shop_item_sort_filter_selected_background)}"
                                android:gravity="center"
                                android:onClick="@{() -> viewModel.onSortByOptionClicked(sortSpinner)}"
                                android:orientation="horizontal"
                                android:paddingLeft="@dimen/space_medium"
                                android:paddingRight="@dimen/space_medium"
                                android:visibility="@{viewModel.openType == OpenBrandDetailType.OPEN_AS_SEARCH_GENERAL_PRODUCT_MODE? View.INVISIBLE: View.VISIBLE}"
                                tools:backgroundTint="?shop_category_view_item_by_background">

                                <TextView
                                    style="@style/ShopTitleStyle"
                                    android:text="@string/shop_item_sort_title"
                                    android:textColor="@{!viewModel.sortCriteriaSelected? AttributeConverter.convertAttrToColor(context, R.attr.shop_category_view_item_by_icon) : AttributeConverter.convertAttrToColor(context, R.attr.common_primary_color)}"
                                    android:textSize="@dimen/text_normal"
                                    tools:textColor="?shop_category_view_item_by_icon" />

                                <TextView
                                    style="@style/BaseSelectionUniconsFontStyle"
                                    android:layout_marginStart="@dimen/space_tiny"
                                    android:gravity="center"
                                    android:text="@string/fi_sr_caret_down"
                                    android:textColor="@{!viewModel.sortCriteriaSelected? AttributeConverter.convertAttrToColor(context, R.attr.shop_category_view_item_by_icon) : AttributeConverter.convertAttrToColor(context, R.attr.common_primary_color)}"
                                    android:textSize="@dimen/text_11sp"
                                    tools:textColor="?shop_category_view_item_by_icon" />

                            </LinearLayout>

                            <com.proapp.sompom.widget.CustomSpinner
                                android:id="@+id/sortSpinner"
                                android:layout_width="@dimen/concierge_view_item_by_icon_size"
                                android:layout_height="1dp"
                                android:layout_below="@+id/sortContainer"
                                android:visibility="invisible" />

                        </RelativeLayout>

                        <LinearLayout
                            android:layout_width="@dimen/concierge_view_item_by_icon_size"
                            android:layout_height="@dimen/concierge_view_item_by_icon_size"
                            android:background="@drawable/round_background"
                            android:backgroundTint="?shop_category_view_item_by_background"
                            android:gravity="center"
                            android:onClick="@{() -> viewModel.onViewByOptionClicked(context)}"
                            android:orientation="vertical">

                            <TextView
                                style="@style/ConciergeViewByOptionStyle"
                                android:text="@{viewModel.nextViewByIcon}"
                                tools:text="@string/fi_rr_grid" />

                        </LinearLayout>

                    </LinearLayout>

                </LinearLayout>

                <RelativeLayout
                    android:layout_width="match_parent"
                    android:layout_height="0dp"
                    android:layout_weight="1">

                    <androidx.recyclerview.widget.RecyclerView
                        android:id="@+id/recyclerView"
                        android:layout_width="match_parent"
                        android:layout_height="match_parent"
                        android:layout_marginTop="@dimen/space_xlarge" />

                    <RelativeLayout
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        tools:visibility="gone">

                        <include
                            layout="@layout/layout_base_loading"
                            app:viewModel="@{viewModel.appliedSearchCriteriaLoadingViewModel}"
                            tools:visibility="visible" />

                        <LinearLayout
                            android:layout_width="match_parent"
                            android:layout_height="match_parent"
                            app:setVisibility="@{viewModel.appliedSearchCriteriaLoadingViewModel.mIsError}">

                            <include
                                layout="@layout/layout_base_error"
                                app:viewModel="@{viewModel.appliedSearchCriteriaLoadingViewModel}"
                                tools:visibility="visible" />

                        </LinearLayout>

                    </RelativeLayout>

                </RelativeLayout>

                <androidx.cardview.widget.CardView
                    android:id="@+id/conciergeBottomSheet"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <include
                        layout="@layout/dialog_concierge_cart_bottom_sheet"
                        app:viewModel="@{viewModel.cartViewModel}" />

                </androidx.cardview.widget.CardView>

            </LinearLayout>

        </LinearLayout>

        <RelativeLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            tools:visibility="gone">

            <include
                layout="@layout/layout_base_loading"
                app:viewModel="@{viewModel}"
                tools:visibility="visible" />

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                app:setVisibility="@{viewModel.mIsError}">

                <include
                    layout="@layout/layout_base_error"
                    app:viewModel="@{viewModel}"
                    tools:visibility="visible" />

            </LinearLayout>

        </RelativeLayout>

    </RelativeLayout>

</layout>
