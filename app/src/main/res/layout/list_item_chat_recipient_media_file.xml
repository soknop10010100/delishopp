<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.text.TextUtils" />

        <import type="com.proapp.sompom.utils.AttributeConverter" />

        <import type="com.proapp.sompom.R" />

        <import type="android.view.View" />

        <import type="com.proapp.sompom.model.Media.FileStatusType" />

        <variable
            name="viewModel"
            type="com.proapp.sompom.viewmodel.newviewmodel.FileChatViewModel" />
    </data>

    <LinearLayout
        android:id="@+id/containerView"
        style="@style/ChatContainerStyle"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:chatGroupSeparator="@{viewModel.mChatBg}">

        <TextView
            android:id="@+id/textViewTime"
            style="@style/SeenDateTimeStyle"
            android:gravity="bottom"
            android:text="@{viewModel.getTime(context)}"
            android:visibility="@{viewModel.visibility}" />

        <com.chauthai.swipereveallayout.SwipeRevealLayout
            android:id="@+id/swipeRevealLayout"
            style="@style/ChatSwipeLayoutStyle"
            app:dragEdge="left"
            app:lockRevealLayout="@{viewModel.isLockSwipeReply}"
            app:mode="same_level"
            app:swipeListener="@{viewModel.swipeListener}"
            app:touchListener="@{viewModel.getTouchListener(swipeRevealLayout)}"
            app:willVibrateReplyingView="@{viewModel.willVibrateReplyingView}">

            <TextView
                style="@style/ChatReplyIconStyle.Recipient"
                app:setCenterParentVertical="@{swipeRevealLayout}"
                app:shouldMakeCenter="@{viewModel.shouldCenterSecondaryViewToItsParent}" />

            <LinearLayout style="@style/ChatSubContainerLayoutStyle">

                <RelativeLayout
                    android:id="@+id/layout_chat"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:clipChildren="false"
                    android:clipToPadding="false"
                    android:longClickable="true">

                    <androidx.constraintlayout.widget.ConstraintLayout
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_alignParentBottom="true"
                        android:layout_toEndOf="@+id/username"
                        android:clipChildren="false"
                        android:clipToPadding="false">

                        <TextView
                            style="@style/ChatShareIconStyle"
                            android:layout_centerVertical="true"
                            android:layout_marginEnd="@dimen/space_medium"
                            android:onClick="@{() -> viewModel.onForwardClick()}"
                            app:layout_constraintBottom_toBottomOf="parent"
                            app:layout_constraintBottom_toTopOf="@+id/message"
                            app:layout_constraintStart_toEndOf="@+id/message"
                            app:layout_constraintTop_toBottomOf="@+id/message" />

                        <com.proapp.sompom.widget.MaxWidthLinearLayout
                            android:id="@+id/message"
                            android:layout_width="@dimen/chat_content_width"
                            android:layout_height="wrap_content"
                            android:layout_marginEnd="@dimen/chat_height"
                            android:background="@{viewModel.getBackground(message, context)}"
                            android:backgroundTint="?chat_file_background"
                            android:orientation="vertical"
                            app:customMaxWidth="@dimen/chat_content_width"
                            app:layout_constraintBottom_toBottomOf="parent"
                            app:layout_constraintStart_toStartOf="parent"
                            app:layout_constraintWidth_default="spread"
                            app:layout_constraintWidth_max="@dimen/chat_content_width"
                            app:setLongClickListener="@{() -> viewModel.onLongClick(layoutChat)}"
                            tools:background="@drawable/layout_chat_you_single">

                            <com.proapp.sompom.widget.ReplyChatHeaderView
                                android:id="@+id/inputReplyLayout"
                                android:layout_width="wrap_content"
                                android:layout_height="wrap_content"
                                android:layout_marginStart="@dimen/space_12dp"
                                android:layout_marginTop="@dimen/space_medium"
                                android:layout_marginEnd="@dimen/space_12dp"
                                android:visibility="@{viewModel.replyHeaderVisibility}"
                                app:bindData="@{viewModel.repliedChat}"
                                app:isCurrentUserSender="@{false}"
                                app:mainChat="@{viewModel.chat}"
                                app:repliedListener="@{viewModel.getRepliedViewListener(message)}"
                                tools:visibility="gone" />

                            <LinearLayout
                                android:id="@+id/layout_file"
                                android:layout_width="match_parent"
                                android:layout_height="wrap_content"
                                android:orientation="horizontal"
                                android:padding="@dimen/space_12dp">

                                <LinearLayout
                                    android:layout_width="match_parent"
                                    android:layout_height="wrap_content"
                                    android:orientation="vertical">

                                    <TextView
                                        android:id="@+id/textViewUserName"
                                        style="@style/UserNameStyle"
                                        android:layout_marginEnd="@dimen/space_large"
                                        android:ellipsize="middle"
                                        android:includeFontPadding="false"
                                        android:maxWidth="@dimen/chat_content_width"
                                        android:paddingLeft="@dimen/space_medium"
                                        android:paddingRight="@dimen/space_medium"
                                        android:singleLine="true"
                                        android:text="@{viewModel.name}"
                                        android:textColor="?chat_file_title"
                                        android:textSize="@dimen/text_chat"
                                        tools:text="how-to-train-your-dragon.docx" />

                                    <TextView
                                        style="@style/label.Text"
                                        android:layout_marginTop="@dimen/space_small"
                                        android:ellipsize="end"
                                        android:includeFontPadding="false"
                                        android:maxLines="1"
                                        android:paddingLeft="@dimen/space_medium"
                                        android:paddingRight="@dimen/space_medium"
                                        android:text="@{viewModel.displaySize}"
                                        android:textColor="?chat_file_size"
                                        android:textSize="@dimen/text_small"
                                        tools:text="550 KB" />

                                    <RelativeLayout
                                        android:layout_width="match_parent"
                                        android:layout_height="wrap_content"
                                        android:layout_marginTop="@dimen/space_small">

                                        <TextView
                                            android:id="@+id/textViewExtension"
                                            style="@style/label.Text"
                                            android:layout_marginLeft="@dimen/space_medium"
                                            android:layout_marginTop="@dimen/space_small"
                                            android:layout_marginRight="@dimen/space_medium"
                                            android:background="@drawable/file_extension_background"
                                            android:ellipsize="end"
                                            android:gravity="center"
                                            android:includeFontPadding="false"
                                            android:maxLines="1"
                                            android:minWidth="@dimen/min_message_segment_width"
                                            android:paddingLeft="@dimen/space_small"
                                            android:paddingTop="@dimen/space_too_small"
                                            android:paddingRight="@dimen/space_small"
                                            android:paddingBottom="@dimen/space_too_small"
                                            android:text="@{viewModel.extension}"
                                            android:textAllCaps="true"
                                            android:textColor="?chat_file_extension_text"
                                            android:textSize="@dimen/text_small"
                                            android:visibility="invisible"
                                            app:setVisibility="@{viewModel.fileExtensionVisibility}"
                                            tools:text="mp3"
                                            tools:visibility="visible" />

                                        <ProgressBar
                                            android:id="@+id/progression"
                                            style="?android:attr/progressBarStyleHorizontal"
                                            android:layout_width="match_parent"
                                            android:layout_height="4dip"
                                            android:layout_below="@id/textViewExtension"
                                            android:layout_marginTop="@dimen/space_small"
                                            android:progress="@{viewModel.progression}"
                                            android:progressDrawable="@drawable/chat_file_download_progress_background"
                                            android:visibility="@{viewModel.fileStatus == FileStatusType.IDLE.ordinal() || viewModel.fileStatus == FileStatusType.DOWNLOADED.ordinal() || viewModel.fileStatus == FileStatusType.UPLOADED.ordinal() || viewModel.fileStatus == FileStatusType.RETRY_UPLOAD.ordinal()? View.GONE: View.VISIBLE}"
                                            tools:progress="10" />

                                        <LinearLayout
                                            android:layout_width="wrap_content"
                                            android:layout_height="wrap_content"
                                            android:layout_alignParentEnd="true"
                                            android:layout_toEndOf="@id/textViewExtension"
                                            android:gravity="end">

                                            <TextView
                                                style="@style/UserNameStyle"
                                                android:layout_width="wrap_content"
                                                android:layout_marginStart="@dimen/space_large"
                                                android:layout_marginTop="@dimen/space_medium"
                                                android:drawableEnd="@{viewModel.fileStatus == FileStatusType.DOWNLOADED.ordinal()? null: @drawable/ic_cloud_download}"
                                                android:drawablePadding="@dimen/space_small"
                                                android:ellipsize="end"
                                                android:gravity="center"
                                                android:includeFontPadding="false"
                                                android:maxLines="1"
                                                android:onClick="@{() -> viewModel.onFileClick()}"
                                                android:padding="@dimen/space_small"
                                                android:text="@{viewModel.fileStatus == FileStatusType.DOWNLOADED.ordinal()? @string/chat_message_file_open: @string/chat_message_file_download}"
                                                android:textAllCaps="true"
                                                android:textColor="@{viewModel.fileStatus == FileStatusType.DOWNLOADED.ordinal()? AttributeConverter.convertAttrToColor(context, R.attr.chat_file_download_text) : AttributeConverter.convertAttrToColor(context, R.attr.chat_file_open_text)}"
                                                android:textSize="@dimen/text_small"
                                                android:textStyle="bold"
                                                android:visibility="@{viewModel.fileStatus == FileStatusType.DOWNLOADING.ordinal() || viewModel.fileStatus == FileStatusType.UPLOADING.ordinal() || viewModel.fileStatus == FileStatusType.RETRY_UPLOAD.ordinal()? View.GONE: View.VISIBLE}"
                                                app:drawableTint="?chat_file_download_icon"
                                                tools:drawableEnd="@drawable/ic_cloud_download"
                                                tools:text="@string/chat_message_file_download" />

                                        </LinearLayout>

                                    </RelativeLayout>

                                </LinearLayout>

                            </LinearLayout>

                        </com.proapp.sompom.widget.MaxWidthLinearLayout>

                    </androidx.constraintlayout.widget.ConstraintLayout>


                    <com.proapp.sompom.widget.ImageProfileLayout
                        android:id="@+id/username"
                        style="@style/ChatProfileItem"
                        android:layout_alignParentBottom="true"
                        android:visibility="invisible"
                        app:setUserStatusImageProfile="@{viewModel.mSender}"
                        app:setVisibility="@{viewModel.senderAvatarVisibility}"
                        tools:visibility="visible" />

                    <!--Single seen avatar-->
                    <com.proapp.sompom.widget.ImageProfileLayout
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentEnd="true"
                        android:layout_alignParentBottom="true"
                        android:layout_marginStart="@dimen/space_small"
                        android:layout_marginEnd="@dimen/space_small"
                        android:onClick="@{() -> viewModel.onSeenAvatarClicked(containerView)}"
                        android:visibility="@{viewModel.singleSeenAvatarVisibility}"
                        app:singleSeenAvatar="@{viewModel.singleSeenAvatar}"
                        tools:visibility="gone" />

                </RelativeLayout>

                <TextView
                    android:id="@+id/textViewStatus"
                    style="@style/ChatMessageStatusTextStyle.Recipient"
                    android:text="@{viewModel.getStatus(context)}"
                    android:visibility="@{viewModel.statusVisibility}"
                    tools:text="Sending..." />

                <!--Multiple seen avatars-->
                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/space_small"
                    android:layout_marginEnd="@dimen/space_small"
                    android:onClick="@{() -> viewModel.onSeenAvatarClicked(containerView)}"
                    android:orientation="vertical"
                    android:visibility="@{viewModel.multipleSeenAvatarVisibility}">

                    <include
                        layout="@layout/list_item_chat_seen_avatar"
                        app:viewModel="@{viewModel}" />

                </LinearLayout>

            </LinearLayout>

        </com.chauthai.swipereveallayout.SwipeRevealLayout>

        <View
            style="@style/ChatItemBottomPaddingStyle"
            android:visibility="@{viewModel.bottomLineVisibility}" />

    </LinearLayout>

</layout>
