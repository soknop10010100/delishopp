Clone Proapp Stepping List in Build Level
I. Package Name
    1. Script to replace new application in build.gradle file
    2. Script to replace application name in String resource file
II. Assets
    1. Script to replace new application/notification icon in resource files
    2. Scrip to place content of inboarding which has JSON format
III. Configuration Files
    1. OneSignal: Script to replace new application id in build.gradle file
    2. Firebase: Script to replace the content of "google-services.json" with the new project configuration
    3. Script to replace key configuration in String resource file [the keys need to be consistent for all platforms]
        - Cloundinary key
        - Google Map Key
        - MapBox Keys
        - Virgil Key
        - Flurry Key
        - Argora Key
IV. Classes
   1. Script to replace API and Socket Urls in "ServerUrl" class