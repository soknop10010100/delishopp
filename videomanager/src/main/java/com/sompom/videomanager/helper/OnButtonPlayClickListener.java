package com.sompom.videomanager.helper;

/**
 * Created by He Rotha on 2/7/19.
 */
public interface OnButtonPlayClickListener {
    void onButtonPlayClick(boolean isPlay);

    void onButtonMuteClick(boolean isMute);
}
