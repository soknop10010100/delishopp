package com.sompom.videomanager.helper;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.sompom.videomanager.model.CustomExoPlayerEventListener;

/**
 * Created by He Rotha on 9/29/17.
 */

public class ExoPlayerBuilder {

    private static final String TAG = ExoPlayerBuilder.class.getSimpleName();

    private static final DefaultLoadControl sLoadControl = new DefaultLoadControl();
    private static final DefaultTrackSelector sTrackSelector = new DefaultTrackSelector();
    private static ExoData sExoData;

    private ExoPlayerBuilder() {
    }

    public static void initApp(Application application) {
        sExoData = new ExoData();
        sExoData.setRendererFactory(new DefaultRenderersFactory(application));
        sExoData.setSourceFactory(new CacheDataSourceFactory(application,
                500 * 1024 * 1024, 500 * 1024 * 1024));
    }

    private static void initContext(Context context) {
        initApp((Application) context.getApplicationContext());
    }

    public static SimpleExoPlayer build(Context context, String url, OnPlayerListener listener) {
        if (sExoData == null) {
            initContext(context);
        }

        sExoData.setExoPlayer(ExoPlayerFactory.newSimpleInstance(sExoData.getRendererFactory(),
                sTrackSelector,
                sLoadControl));
        final MediaSource videoSource = new ExtractorMediaSource.Factory(sExoData.getSourceFactory())
                .createMediaSource(Uri.parse(url));
        sExoData.getExoPlayer().prepare(videoSource);
        sExoData.getExoPlayer().addListener(new CustomExoPlayerEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == Player.STATE_ENDED) {
                    //restart video's position to 0 second if video is completed
                    sExoData.getExoPlayer().seekTo(0);
                    sExoData.getExoPlayer().setPlayWhenReady(false);
                } else if (playbackState == Player.STATE_READY && listener != null) {
                    listener.onPlayStatus(sExoData.getExoPlayer().getPlayWhenReady());
                }
            }

        });
        return sExoData.getExoPlayer();
    }

    public interface OnPlayerListener {
        /**
         * @param isPlaying: if isPlaying equal true means that video is playing, otherwise means pause
         */
        void onPlayStatus(boolean isPlaying);
    }

    private static class ExoData {

        private CacheDataSourceFactory mSourceFactory;
        private SimpleExoPlayer mExoPlayer;
        private DefaultRenderersFactory mRendererFactory;

        public void setSourceFactory(CacheDataSourceFactory sourceFactory) {
            mSourceFactory = sourceFactory;
        }

        public void setExoPlayer(SimpleExoPlayer exoPlayer) {
            Log.d(TAG, "Previous player: " + (mExoPlayer != null ? mExoPlayer.hashCode() : null));
            mExoPlayer = exoPlayer;
            Log.d(TAG, "New player: " + exoPlayer.hashCode());
        }

        public void setRendererFactory(DefaultRenderersFactory rendererFactory) {
            mRendererFactory = rendererFactory;
        }

        public SimpleExoPlayer getExoPlayer() {
            return mExoPlayer;
        }

        public CacheDataSourceFactory getSourceFactory() {
            return mSourceFactory;
        }

        public DefaultRenderersFactory getRendererFactory() {
            return mRendererFactory;
        }
    }

    public static boolean isPlayerStoppable(SimpleExoPlayer player) {
        return player != null
                && player.getPlaybackLooper() != null
                && player.getPlaybackLooper().getThread() != null
                && player.getPlaybackLooper().getThread().isAlive();
    }

    public static void stopAndReleaseMediaPlayer(SimpleExoPlayer player) {
        if (player.getPlaybackState() == Player.STATE_READY && player.getPlayWhenReady()) {
            Log.d(TAG, "stop media player.");
            player.stop();
        }
        player.release();
        Log.d(TAG, "stopAndReleaseMediaPlayer");
    }

    public static void stopMediaPlayer(SimpleExoPlayer player) {
        Log.d(TAG, "stop media player.");
        player.stop();
    }
}
