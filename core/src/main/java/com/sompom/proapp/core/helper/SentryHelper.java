package com.sompom.proapp.core.helper;


import android.content.Context;
import android.text.TextUtils;

import org.lucasr.twowayview.BuildConfig;

import io.sentry.Sentry;
import io.sentry.android.core.SentryAndroid;

public class SentryHelper {

    public static void setUser(String id, String email, String fullName) {
        io.sentry.protocol.User sentryUser = new io.sentry.protocol.User();
        sentryUser.setId(id);
        sentryUser.setEmail(email);
        sentryUser.setUsername(fullName);
        Sentry.setUser(sentryUser);
    }

    public static void logSentryError(Exception exception) {
        Sentry.captureException(exception);
    }

    public static void initSentry(Context context) {
        SentryAndroid.init(context, options -> options.setBeforeSend((event, hint) -> {
            //Ignore reporting error in other build type modes except for release mode.
            if (TextUtils.equals(BuildConfig.BUILD_TYPE, "release")) {
                return event;
            }

            return null;
        }));
    }
}
