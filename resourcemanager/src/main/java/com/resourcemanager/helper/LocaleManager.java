package com.resourcemanager.helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

import androidx.core.os.ConfigurationCompat;
import androidx.core.os.LocaleListCompat;

import com.resourcemanager.utils.SharedPrefUtils;
import com.sompom.resourcemanager.R;

import java.util.Locale;

public class LocaleManager {

    private static final String TAG = LocaleManager.class.getSimpleName();

    public static final String ENGLISH_LANGUAGE_KEY = "en";
    public static final String FRENCH_LANGUAGE_KEY = "fr";
    public static final String KHMER_LANGUAGE_KEY = "km";

    public static Context setLocale(Context c) {
        return updateResources(c, getAppLanguage(c));
    }

    public static Context setNewLocale(Context c, String language) {
        persistLanguage(c, language);
        return updateResources(c, language);
    }

    public static void applyChangeLanguage(Context context, String lang) {
        setNewLocale(context, lang);
        updateLanguage((Application) context.getApplicationContext(), lang);
    }

    @SuppressLint("ApplySharedPref")
    private static void persistLanguage(Context c, String language) {
        SharedPrefUtils.setLanguage(language, c);
    }

    private static Context updateResources(Context context, String language) {
        Log.d(TAG, "updateResources to: " + language);

        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.setLocale(locale);
        context = context.createConfigurationContext(config);
        return context;
    }

    public static Locale getLocale(Resources res) {
        Configuration config = res.getConfiguration();
        return Build.VERSION.SDK_INT >= 24 ? config.getLocales().get(0) : config.locale;
    }

    public static int selectLanguagePosition(Context context) {
        int languageSelectedIndex = 0;
        final String[] iso3SupportLanguage = context.getResources().getStringArray(R.array.support_lang_iso3);
        for (int i = 0; i < iso3SupportLanguage.length; i++) {
            if (iso3SupportLanguage[i].equals(SharedPrefUtils.getLanguage(context))) {
                languageSelectedIndex = i;
                break;
            }
        }
        return languageSelectedIndex;
    }

    public static void updateLanguage(Application context, String localString) {
        Configuration conf = context.getResources().getConfiguration();
        conf.locale = new Locale(localString);
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Activity.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        context.getResources().updateConfiguration(conf, metrics);
        Resources resources = new Resources(context.getAssets(), metrics, conf);
        resources.updateConfiguration(conf, metrics);
        context.getResources().updateConfiguration(conf, metrics);


        Configuration configuration = context.getBaseContext().getResources().getConfiguration();
        configuration.locale = conf.locale;
        context.getBaseContext().getResources()
                .updateConfiguration(configuration, context.getResources().getDisplayMetrics());

        Configuration systemConf = Resources.getSystem().getConfiguration();
        systemConf.locale = conf.locale;
        Resources.getSystem().updateConfiguration(systemConf, Resources.getSystem().getDisplayMetrics());

        Locale.setDefault(conf.locale);
    }

    public static String getAppLanguage(Context context) {
        String languageCode = SharedPrefUtils.getLanguage(context);
        Log.d(TAG, "languageCode: " + languageCode);
        if (TextUtils.isEmpty(languageCode)) {
            return getValidDeviceLangeCode();
        }

        return languageCode;
    }

    private static String getSystemLanguageCode() {
        LocaleListCompat LocaleListCompat = ConfigurationCompat.getLocales(Resources.getSystem().getConfiguration());
        if (!LocaleListCompat.isEmpty()) {
            return LocaleListCompat.get(0).getLanguage();
        }

        return "";
    }

    public static String getValidDeviceLangeCode() {
        String systemLanguageCode = getSystemLanguageCode();
        Log.d(TAG, "systemLanguageCode: " + systemLanguageCode);
        if (TextUtils.equals(systemLanguageCode, ENGLISH_LANGUAGE_KEY) ||
                TextUtils.equals(systemLanguageCode, KHMER_LANGUAGE_KEY)) {
            return systemLanguageCode;
        }

        return ENGLISH_LANGUAGE_KEY;
    }
}