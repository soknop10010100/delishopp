package com.resourcemanager.helper;

import android.content.Context;
import android.graphics.Typeface;

import androidx.annotation.FontRes;
import androidx.core.content.res.ResourcesCompat;

import com.sompom.resourcemanager.R;


/**
 * Created by Chhom Veasna on 12/12/2019.
 */
public class FontHelper {

    private FontHelper() {
    }

    public static Typeface getSFProFontFamily(Context context) {
        return ResourcesCompat.getFont(context, R.font.sf_pro_family);
    }

    public static Typeface getBoldFontStyle(Context context) {
        return ResourcesCompat.getFont(context, R.font.sf_pro_display_bold);
    }

    public static Typeface getSemiBoldFontStyle(Context context) {
        return ResourcesCompat.getFont(context, R.font.sf_pro_semibold);
    }

    public static Typeface getRegularFontStyle(Context context) {
        return ResourcesCompat.getFont(context, R.font.sf_pro_regular);
    }

    public static Typeface getLightFontStyle(Context context) {
        return ResourcesCompat.getFont(context, R.font.sf_pro_light);
    }

    public static @FontRes
    int getFlatIconFontResource() {
        return R.font.flaticon2;
    }

    public static @FontRes
    int getMagicIconFontResource() {
        return R.font.magicons;
    }
}
