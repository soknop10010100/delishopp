package com.resourcemanager.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;

/**
 * Created by He Rotha on 10/19/18.
 */
public final class AttributeConverter {

    private AttributeConverter() {
    }

    public static int convertAttrToColor(Context context, int attr) {
        TypedValue typedValue = new TypedValue();
        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[]{attr});
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }

    public static Drawable convertAttrToDrawable(Context context, int attr) {
        TypedValue typedValue = new TypedValue();
        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[]{attr});
        int attributeResourceId = a.getResourceId(0, 0);
        Drawable drawable = context.getResources().getDrawable(attributeResourceId);
        a.recycle();
        return drawable;
    }

    public static Drawable applyAttrColorToDrawable(Context context, int drawableID, int attr) {
        TypedValue typedValue = new TypedValue();
        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[]{attr});
        int color = a.getColor(0, 0);
        Drawable drawable = AppCompatResources.getDrawable(context, drawableID);
        if (drawable != null) {
            drawable.mutate();
            DrawableCompat.setTint(drawable, color);
//            drawable.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));
        }
        a.recycle();
        return drawable;
    }
}
