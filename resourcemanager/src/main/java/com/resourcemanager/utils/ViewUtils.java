package com.resourcemanager.utils;

import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.databinding.BindingAdapter;

public final class ViewUtils {
    private ViewUtils() {
    }

    public static Drawable setColorFilter(@NonNull Drawable drawable, @ColorInt int color) {
        Drawable newDrawable = drawable.mutate();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            newDrawable.setColorFilter(new BlendModeColorFilter(color, BlendMode.SRC_ATOP));
        } else {
            newDrawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        }

        return newDrawable;
    }

    @BindingAdapter("setCustomBackgroundTint")
    public static void setBackgroundTint(View view, int color) {
        if (view.getBackground() != null) {
            Drawable mutate = view.getBackground().mutate();
            DrawableCompat.setTint(mutate, color);
            view.setBackground(mutate);
        }
    }
}
