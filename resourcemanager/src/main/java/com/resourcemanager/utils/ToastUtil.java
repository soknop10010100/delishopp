package com.resourcemanager.utils;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sompom.resourcemanager.R;

import androidx.annotation.StringRes;

/**
 * Created by He Rotha on 9/27/17.
 */

public final class ToastUtil {
    private ToastUtil() {
    }

    public static void showToast(Context context, String message, boolean isError) {
        customizeToastColor(Toast.makeText(context, message, Toast.LENGTH_SHORT),
                isError ? AttributeConverter.convertAttrToColor(context, R.attr.toast_error_text) :
                        AttributeConverter.convertAttrToColor(context, R.attr.toast_normal_text),
                isError ? AttributeConverter.convertAttrToColor(context, R.attr.toast_error_background) :
                        AttributeConverter.convertAttrToColor(context, R.attr.toast_normal_background))
                .show();
    }

    public static Toast buildToast(Context context, String message, boolean isError) {
        return customizeToastColor(Toast.makeText(context, message, Toast.LENGTH_SHORT),
                isError ? AttributeConverter.convertAttrToColor(context, R.attr.toast_error_text) :
                        AttributeConverter.convertAttrToColor(context, R.attr.toast_normal_text),
                isError ? AttributeConverter.convertAttrToColor(context, R.attr.toast_error_background) :
                        AttributeConverter.convertAttrToColor(context, R.attr.toast_normal_background));
    }

    public static void showToast(Context context, @StringRes int message, boolean isError) {
        if (context == null) {
            return;
        }
        customizeToastColor(Toast.makeText(context, message, Toast.LENGTH_SHORT),
                isError ? AttributeConverter.convertAttrToColor(context, R.attr.toast_error_text) :
                        AttributeConverter.convertAttrToColor(context, R.attr.toast_normal_text),
                isError ? AttributeConverter.convertAttrToColor(context, R.attr.toast_error_background) :
                        AttributeConverter.convertAttrToColor(context, R.attr.toast_normal_background))
                .show();
    }

    public static void showToast(Context context, String message, int textColor, int backgroundColor) {
        if (context == null) {
            return;
        }
        customizeToastColor(Toast.makeText(context, message, Toast.LENGTH_SHORT),
                textColor,
                backgroundColor)
                .show();
    }

    public static Toast customizeToastColor(Toast toast, int textColor, int backgroundColor) {
        View toastView = toast.getView();
        if (toastView != null) {
            if (toastView.getBackground() != null) {
                toastView.getBackground().setTint(backgroundColor);
            }
            TextView toastMessage = toastView.findViewById(android.R.id.message);
            if (toastMessage != null) {
                toastMessage.setTextColor(textColor);
            }
        }

        return toast;
    }
}
