package com.resourcemanager.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Veasna Chhom on 4/2/21.
 */
public final class SharedPrefUtils {

    private static final String SHARE_PREF = "SHARE_PREF";
    private static final String LANGUAGE = "LANGUAGE";


    public static void setLanguage(String value, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(LANGUAGE, value).apply();
    }

    /**
     * it will return km, fr, en
     */
    public static String getLanguage(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(LANGUAGE, "");
    }
}
