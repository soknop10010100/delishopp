package com.desmond.squarecamera.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import com.desmond.squarecamera.R;

/**
 * Created by Fadi on 5/11/2014.
 */
public class FocusBoxView extends View {

    private Bitmap mTopLefConner;
    private Bitmap mTopRightConner;
    private Bitmap mBottomLeftConner;
    private Bitmap mBottomRightConner;

    public FocusBoxView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mTopLefConner = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.corner_up_left);
        mTopRightConner = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.corner_up_right);
        mBottomLeftConner = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.corner_down_left);
        mBottomRightConner = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.corner_down_right);
    }

    @Override
    public void onDraw(Canvas canvas) {
        int width = getWidth();
        int padding = getPadding();

        canvas.drawBitmap(mTopLefConner,
                padding,
                padding, null);

        canvas.drawBitmap(mTopRightConner,
                width - mTopRightConner.getWidth() - padding,
                padding,
                null);

        canvas.drawBitmap(mBottomLeftConner,
                padding,
                width - mBottomLeftConner.getHeight() - padding,
                null);

        canvas.drawBitmap(mBottomRightConner,
                width - mBottomRightConner.getWidth() - padding,
                width - mBottomRightConner.getHeight() - padding,
                null);
    }

    private int getPadding() {
        return getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
