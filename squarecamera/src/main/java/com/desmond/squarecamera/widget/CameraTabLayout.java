package com.desmond.squarecamera.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

import androidx.annotation.StringRes;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;

import com.desmond.squarecamera.R;
import com.desmond.squarecamera.databinding.LayoutCustomCameraTabBinding;
import com.desmond.squarecamera.helper.OnSwipeListener;
import com.desmond.squarecamera.intent.CameraIntentBuilder;
import com.desmond.squarecamera.intent.CameraType;
import com.desmond.squarecamera.intent.GalleryType;
import com.desmond.squarecamera.viewmodel.CustomCameraTabViewModel;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 8/9/18.
 */
public class CameraTabLayout extends TabLayout {

    private CameraType mCameraType;
    private int mCurrentPosition;
    private final OnSwipeListener mOnSwipeListener = new OnSwipeListener() {
        @Override
        public boolean onSwipe(Direction direction) {
            if (direction == Direction.left && mCurrentPosition + 1 <= getTabCount() - 1) {
                Tab tab = getTabAt(mCurrentPosition + 1);
                if (tab != null) {
                    tab.select();
                }
            } else if (direction == Direction.right && mCurrentPosition - 1 >= 0) {
                Tab tab = getTabAt(mCurrentPosition - 1);
                if (tab != null) {
                    tab.select();
                }
            }
            return super.onSwipe(direction);
        }
    };
    private OnTabChangeListener mOnTabChangeListener;
    private List<CustomCameraTabViewModel> mTabViewModels = new ArrayList<>();

    public CameraTabLayout(Context context) {
        super(context);
    }

    public CameraTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CameraTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public OnSwipeListener getOnSwipeListener() {
        return mOnSwipeListener;
    }

    public void setCameraIntentBuilder(CameraIntentBuilder cameraIntentBuilder, TabIndex selectedTabIndex) {
        mTabViewModels.clear();
        if (cameraIntentBuilder.getOpenType() instanceof CameraType) {
            mCameraType = (CameraType) cameraIntentBuilder.getOpenType();
        } else if (cameraIntentBuilder.getOpenType() instanceof GalleryType) {
            mCameraType = ((GalleryType) cameraIntentBuilder.getOpenType()).getCameraType();
        } else {
            mCameraType = null;
        }

        if (mCameraType != null) {
            for (int i = 0; i < mCameraType.getTabIndex().size(); i++) {
                Tab tab = newTab();
                //Bind custom tab layout
                LayoutCustomCameraTabBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                        R.layout.layout_custom_camera_tab,
                        this,
                        false);

                boolean isTabSelected;

                if (selectedTabIndex != null) {
                    isTabSelected = selectedTabIndex == mCameraType.getTabIndex().get(i);
                } else {
                    isTabSelected = i == mCameraType.getSelectIndex();
                }

                CustomCameraTabViewModel viewModel = new CustomCameraTabViewModel(getResources().getString(mCameraType.getTabIndex().get(i).getValue()),
                        isTabSelected);
                binding.setVariable(com.desmond.squarecamera.BR.viewModel, viewModel);
                tab.setCustomView(binding.getRoot());
                mTabViewModels.add(viewModel);
                addTab(tab);

                if (selectedTabIndex != null && selectedTabIndex == mCameraType.getTabIndex().get(i)) {
                    tab.select();
                    TabIndex currentTab = mCameraType.getTabIndex().get(i);
                    mOnTabChangeListener.onTabSelect(currentTab);
                    mCurrentPosition = i;
                } else if (i == mCameraType.getSelectIndex()) {
                    tab.select();
                    TabIndex currentTab = mCameraType.getTabIndex().get(i);
                    mOnTabChangeListener.onTabSelect(currentTab);
                    mCurrentPosition = i;
                }
            }
        }
    }

    public void setOnTabChangeListener(final OnTabChangeListener listener) {
        mOnTabChangeListener = listener;
        addOnTabSelectedListener(new OnTabSelectedListener() {
            @Override
            public void onTabSelected(Tab tab) {
                if (mCameraType == null || tab.getPosition() < 0) {
                    return;
                }
                mTabViewModels.get(tab.getPosition()).onSelected();
                TabIndex currentTab = mCameraType.getTabIndex().get(tab.getPosition());
                mOnTabChangeListener.onTabSelect(currentTab);
                mCurrentPosition = tab.getPosition();
            }

            @Override
            public void onTabUnselected(Tab tab) {
                mTabViewModels.get(tab.getPosition()).onUnselected();
            }

            @Override
            public void onTabReselected(Tab tab) {

            }
        });
    }

    public enum TabIndex {
        LIVE(R.string.camera_live_button),
        PHOTO(R.string.camera_screen_photo_title),
        VIDEO(R.string.camera_screen_video_title);

        @StringRes
        private final int mValue;

        TabIndex(int value) {
            mValue = value;
        }

        private int getValue() {
            return mValue;
        }
    }

    public interface OnTabChangeListener {
        void onTabSelect(TabIndex tabIndex);
    }

    @BindingAdapter("customIndicator")
    public static void customIndicator(final CameraTabLayout tabLayout, final View customIndicator) {
        tabLayout.addOnTabSelectedListener(new OnTabSelectedListener() {
            @Override
            public void onTabSelected(Tab tab) {
                performHorizontalTranslationAnimation(customIndicator,
                        findCentralXPositionOfSelectedTab(tab, customIndicator.getWidth()),
                        true);
            }

            @Override
            public void onTabUnselected(Tab tab) {

            }

            @Override
            public void onTabReselected(Tab tab) {

            }
        });

        //Redirect to the default selection tab
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                final Tab tabAt = tabLayout.getTabAt(tabLayout.getSelectedTabPosition());
                if (tabAt != null) {
                    tabAt.getCustomView().post(new Runnable() {
                        @Override
                        public void run() {
                            performHorizontalTranslationAnimation(customIndicator,
                                    findCentralXPositionOfSelectedTab(tabAt,
                                            customIndicator.getWidth()),
                                    false);
                        }
                    });
                }
            }
        });
    }

    private static float findCentralXPositionOfSelectedTab(Tab tab, int customIndicatorWidth) {
        View customView = tab.getCustomView();
        if (customView != null) {
            View customTabSegmentView = customView.findViewById(R.id.customTabSegmentView);
            float xPos = getRealXOrYPosition(customTabSegmentView, true);
            return xPos + ((customTabSegmentView.getWidth() - customIndicatorWidth) / 2);
        }

        return 0f;
    }

    private static float getRealXOrYPosition(View view, boolean isX) {
        int[] locations = new int[2];
        view.getLocationOnScreen(locations);

        return isX ? locations[0] : locations[1];
    }

    private static void performHorizontalTranslationAnimation(final View view,
                                                              float desX,
                                                              final boolean isAnimate) {
        float lastXPos = 0f;
        if (view.getTag() != null) {
            lastXPos = (float) view.getTag();
        }

        TranslateAnimation animation = new TranslateAnimation(Animation.ABSOLUTE,
                lastXPos,
                Animation.ABSOLUTE,
                desX,
                Animation.ABSOLUTE,
                0f,
                Animation.ABSOLUTE,
                0f);
        animation.setDuration(isAnimate ? 300 : 0);
        animation.setFillAfter(true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!isAnimate) {
                    //Default animate to first tab position
                    view.setAlpha(1f);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
        //Set the next last X pos
        view.setTag(desX);
    }
}
