package com.desmond.squarecamera.listeners;

import com.desmond.squarecamera.model.MediaFile;

/**
 * Created by He Rotha on 6/7/18.
 */
public interface OnGalleryItemClickListener {
    void onGalleryItemClick(int position, MediaFile galleryData);

    void onGalleryAddClick();
}
