package com.desmond.squarecamera.listeners;

import com.desmond.squarecamera.model.MediaFile;

import java.util.List;

/**
 * Created by He Rotha on 6/7/18.
 */
public interface GalleryLoaderListener {
    void onLoadComplete(List<MediaFile> galleryData);
}
