package com.desmond.squarecamera.listeners;

/**
 * Created by He Rotha on 2/26/18.
 */
public interface OnChooseImageGalleryListener extends MediaChooserFileListener {
    void onPermissionGranted();

    void onPermissionDeny();

}
