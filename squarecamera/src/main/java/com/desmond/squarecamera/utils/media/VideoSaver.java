package com.desmond.squarecamera.utils.media;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;

import com.sompom.proapp.core.helper.SentryHelper;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by He Rotha on 10/11/18.
 */
public final class VideoSaver {
    private Context mContext;
    private Bitmap mBitmap;
    private boolean mIsResize = false;
    private String mSavingPath;
    private boolean mIsBroadcast = false;

    private VideoSaver(Context context) {
        mContext = context;
    }

    public static VideoSaver with(Context context) {
        return new VideoSaver(context);
    }

    public VideoSaver load(Bitmap bitmap) {
        mBitmap = bitmap;
        return this;
    }

    public VideoSaver into(String path) {
        mSavingPath = path;
        mIsBroadcast = true;
        return this;
    }

    public VideoSaver setBroadcast(boolean broadcast) {
        mIsBroadcast = broadcast;
        return this;
    }

    public VideoSaver intoCacheDir() {
        into(FileNameUtil.getVideoCacheFile(mContext).toString());
        mIsBroadcast = false;
        return this;
    }

    public String execute() {

        if (TextUtils.isEmpty(mSavingPath)) {
            into(FileNameUtil.getVideoFile(mContext).toString());
        }

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            FileOutputStream stream = new FileOutputStream(mSavingPath);
            stream.write(out.toByteArray());
            stream.close();
            mBitmap.recycle();
        } catch (IOException exception) {
            exception.printStackTrace();
            SentryHelper.logSentryError(exception);
        }

        if (mIsBroadcast) {
            Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScannerIntent.setData(Uri.parse("file://" + mSavingPath));
            mContext.sendBroadcast(mediaScannerIntent);
        }

        return mSavingPath;
    }
}
