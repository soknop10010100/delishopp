package com.desmond.squarecamera.utils;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

/**
 * Created by nuonveyo on 8/15/18.
 */

public final class ScaleAnimationHelper {
    private ScaleAnimationHelper() {
    }

    public static void startScale(View view, float fromX, float toX, float fromY, float toY) {
        ScaleAnimation animation = new ScaleAnimation(fromX,
                toX,
                fromY,
                toY,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f);
        animation.setInterpolator(view.getContext(), android.R.interpolator.accelerate_decelerate);
        animation.setDuration(300);
        animation.setFillAfter(true);
        view.startAnimation(animation);
    }
}
