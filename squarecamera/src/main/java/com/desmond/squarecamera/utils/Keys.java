package com.desmond.squarecamera.utils;

/**
 * Created by He Rotha on 6/11/18.
 */
public final class Keys {

    public static final String SOURCE_TYPE = "SOURCE_TYPE";
    public static final String DATA = "DATA";
    public static final String PATH = "PATH";
    public static final String PARAM = "PARAM";
}
