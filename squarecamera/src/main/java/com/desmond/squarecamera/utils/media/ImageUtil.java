package com.desmond.squarecamera.utils.media;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import androidx.exifinterface.media.ExifInterface;

import com.sompom.proapp.core.helper.SentryHelper;

import java.io.File;
import java.io.IOException;

/**
 * Created by desmond on 24/10/14.
 */
public final class ImageUtil {

    private static String[] sImageExtensions = new String[]{"bmp", "gif", "jpg", "png", "webp", "heic", "heif"};

    private static final String TAG = ImageUtil.class.getName();

    private ImageUtil() {
    }

    public static Bitmap rotateImageWithoutCompress(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static boolean isImageFile(String extension) {
        for (String imageExtension : sImageExtensions) {
            if (TextUtils.equals(extension.toLowerCase(), imageExtension)) {
                return true;
            }
        }

        return false;
    }

    public static Bitmap resizeBitmap(final Bitmap temp, int maxWidth, int maxHeight) {
        int width = temp.getWidth();
        int height = temp.getHeight();
        if (width > height) {
            // landscape
            float ratio = (float) width / maxWidth;
            width = maxWidth;
            height = (int) (height / ratio);
        } else if (height > width) {
            // portrait
            float ratio = (float) height / maxHeight;
            height = maxHeight;
            width = (int) (width / ratio);
        } else {
            // square
            height = maxWidth;
            width = maxWidth;
        }
        return Bitmap.createScaledBitmap(temp, width, height, true);
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, proj,
                null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static Bitmap getBitmapFromFilePath(String path) {
        File file = new File(path);
        if (file.exists()) {
            Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
            ExifInterface exif = null;
            try {
                exif = new ExifInterface(file.getAbsolutePath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                Log.d(TAG, "Exif: " + orientation);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                }
                // rotating bitmap
                bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
                return bmp;
            } catch (IOException e) {
                e.printStackTrace();
                SentryHelper.logSentryError(e);
            }
        }

        return null;
    }
}
