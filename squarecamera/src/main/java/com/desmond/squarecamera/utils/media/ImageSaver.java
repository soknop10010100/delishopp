package com.desmond.squarecamera.utils.media;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.sompom.proapp.core.helper.SentryHelper;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by He Rotha on 10/11/18.
 */
public final class ImageSaver {

    private Context mContext;
    private Bitmap mBitmap;
    private boolean mIsResize = false;
    private String mSavingPath;
    private boolean mIsBroadcast = false;

    private ImageSaver(Context context) {
        mContext = context;
    }

    public static ImageSaver with(Context context) {
        return new ImageSaver(context);
    }

    public ImageSaver load(Bitmap bitmap) {
        mBitmap = bitmap;
        return this;
    }

    public ImageSaver into(String path) {
        mSavingPath = path;
        mIsBroadcast = true;
        return this;
    }

    public ImageSaver resize(boolean isResize) {
        mIsResize = isResize;
        return this;
    }

    public ImageSaver setBroadcast(boolean broadcast) {
        mIsBroadcast = broadcast;
        return this;
    }

    public ImageSaver intoCacheDir() {
        into(FileNameUtil.getPictureCacheFile(mContext).toString());
        mIsBroadcast = false;
        return this;
    }

    public String execute() {
        if (TextUtils.isEmpty(mSavingPath)) {
            into(FileNameUtil.getPictureFile(mContext).toString());
        }

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            FileOutputStream stream = new FileOutputStream(mSavingPath);
            stream.write(out.toByteArray());
            stream.close();
        } catch (IOException exception) {
            exception.printStackTrace();
            SentryHelper.logSentryError(exception);
        }
        if (mIsBroadcast) {
            Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScannerIntent.setData(Uri.parse("file://" + mSavingPath));
            Log.e("Path of saved file", "" + mediaScannerIntent.getData().getPath());
            mContext.sendBroadcast(mediaScannerIntent);
        }

        return mSavingPath;
    }
}
