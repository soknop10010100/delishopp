package com.desmond.squarecamera.intent;

import android.os.Parcel;

import com.desmond.squarecamera.widget.CameraTabLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 10/19/18.
 */
public class CameraType implements FirstOpen {
    private List<CameraTabLayout.TabIndex> mTabIndex = new ArrayList<>();
    private int mSelectIndex;
    private boolean mIsShowDescription;
    private GalleryType mGalleryType;

    public CameraType() {
    }

    public CameraType addTab(CameraTabLayout.TabIndex tab, boolean isSelect) {
        if (!mTabIndex.contains(tab)) {
            mTabIndex.add(tab);
        }
        if (isSelect || mTabIndex.size() == 1) {
            mSelectIndex = mTabIndex.size() - 1;
        }
        return this;
    }

    public CameraType showGallery(GalleryType galleryType) {
        mGalleryType = galleryType;
        return this;
    }

    public CameraType showDescription(boolean showDescription) {
        mIsShowDescription = showDescription;
        return this;
    }

    public List<CameraTabLayout.TabIndex> getTabIndex() {
        return mTabIndex;
    }

    public int getSelectIndex() {
        return mSelectIndex;
    }

    public boolean isShowDescription() {
        return mIsShowDescription;
    }

    public GalleryType getGalleryType() {
        return mGalleryType;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.mTabIndex);
        dest.writeInt(this.mSelectIndex);
        dest.writeByte(this.mIsShowDescription ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.mGalleryType, flags);
    }

    protected CameraType(Parcel in) {
        this.mTabIndex = new ArrayList<CameraTabLayout.TabIndex>();
        in.readList(this.mTabIndex, CameraTabLayout.TabIndex.class.getClassLoader());
        this.mSelectIndex = in.readInt();
        this.mIsShowDescription = in.readByte() != 0;
        this.mGalleryType = in.readParcelable(GalleryType.class.getClassLoader());
    }

    public static final Creator<CameraType> CREATOR = new Creator<CameraType>() {
        @Override
        public CameraType createFromParcel(Parcel source) {
            return new CameraType(source);
        }

        @Override
        public CameraType[] newArray(int size) {
            return new CameraType[size];
        }
    };
}
