package com.desmond.squarecamera.intent;

import android.os.Parcel;

import com.desmond.squarecamera.helper.GalleryLoader;
import com.desmond.squarecamera.model.MediaFile;

import java.util.List;

/**
 * Created by He Rotha on 10/19/18.
 */
public class GalleryType implements FirstOpen {
    private CameraType mCameraType;
    private boolean mIsShowDescription;
    private GalleryLoader.LoaderType mLoaderType;
    private int mSelectionNumbers;
    private List<MediaFile> mMediaFiles;
    private boolean mIsFirstIndexIsCover = false;

    public GalleryType( GalleryLoader.LoaderType cameraType) {
        mLoaderType = cameraType;
    }

    public GalleryType showCameraButton(CameraType cameraType) {
        mCameraType = cameraType;
        return this;
    }

    public GalleryType showDescription(boolean showDescription) {
        mIsShowDescription = showDescription;
        return this;
    }


    public GalleryType firstIndexIsCover(boolean firstIndexIsCover) {
        mIsFirstIndexIsCover = firstIndexIsCover;
        return this;
    }

    public GalleryType numberSelection(int selectionNumbers) {
        mSelectionNumbers = selectionNumbers;
        return this;
    }

    public GalleryType selectFiles(List<MediaFile> selectedFiles) {
        mMediaFiles = selectedFiles;
        return this;
    }

    public GalleryLoader.LoaderType getLoaderType() {
        return mLoaderType;
    }

    public CameraType getCameraType() {
        return mCameraType;
    }

    public boolean isShowDescription() {
        return mIsShowDescription;
    }

    public int getSelectionNumbers() {
        return mSelectionNumbers;
    }

    public List<MediaFile> getMediaFiles() {
        return mMediaFiles;
    }

    public boolean isFirstIndexIsCover() {
        return mIsFirstIndexIsCover;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mCameraType, flags);
        dest.writeByte(this.mIsShowDescription ? (byte) 1 : (byte) 0);
        dest.writeInt(this.mLoaderType == null ? -1 : this.mLoaderType.ordinal());
        dest.writeInt(this.mSelectionNumbers);
        dest.writeTypedList(this.mMediaFiles);
        dest.writeByte(this.mIsFirstIndexIsCover ? (byte) 1 : (byte) 0);
    }

    protected GalleryType(Parcel in) {
        this.mCameraType = in.readParcelable(CameraType.class.getClassLoader());
        this.mIsShowDescription = in.readByte() != 0;
        int tmpMLoaderType = in.readInt();
        this.mLoaderType = tmpMLoaderType == -1 ? null : GalleryLoader.LoaderType.values()[tmpMLoaderType];
        this.mSelectionNumbers = in.readInt();
        this.mMediaFiles = in.createTypedArrayList(MediaFile.CREATOR);
        this.mIsFirstIndexIsCover = in.readByte() != 0;
    }

    public static final Creator<GalleryType> CREATOR = new Creator<GalleryType>() {
        @Override
        public GalleryType createFromParcel(Parcel source) {
            return new GalleryType(source);
        }

        @Override
        public GalleryType[] newArray(int size) {
            return new GalleryType[size];
        }
    };
}
