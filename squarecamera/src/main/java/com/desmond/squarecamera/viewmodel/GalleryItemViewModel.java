package com.desmond.squarecamera.viewmodel;

import android.content.Context;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.desmond.squarecamera.R;
import com.desmond.squarecamera.intent.GalleryType;
import com.desmond.squarecamera.listeners.OnGalleryItemClickListener;
import com.desmond.squarecamera.model.MediaFile;

import java.util.Locale;

/**
 * Created by He Rotha on 6/7/18.
 */
public class GalleryItemViewModel {

    public ObservableField<String> mPath = new ObservableField<>();
    public ObservableBoolean mIsSelect = new ObservableBoolean();
    public ObservableBoolean mIsShowText = new ObservableBoolean();
    public ObservableField<String> mIndex = new ObservableField<>();
    public ObservableField<String> mDuration = new ObservableField<>();
    private MediaFile mMediaFile;
    private OnGalleryItemClickListener mListener;
    private int mPosition;

    public GalleryItemViewModel(Context context,
                                GalleryType sourceType,
                                MediaFile mediaFile,
                                int position,
                                OnGalleryItemClickListener listener) {
        mMediaFile = mediaFile;
        mListener = listener;
        mPath.set(mediaFile.getPath());
        mPosition = position;
        mIsSelect.set(mediaFile.isSelected());
        mDuration.set(getVideoDurationDisplayFormat(mediaFile.getVideoDuration()));

        if (sourceType.getSelectionNumbers() > 1) {
            mIsShowText.set(mediaFile.isSelected());
            if (mediaFile.isSelected()) {
                if (mediaFile.getPosition() == 0 && sourceType.isFirstIndexIsCover()) {
                    mIndex.set(context.getString(R.string.gallery_cover));
                } else {
                    mIndex.set(String.valueOf(mediaFile.getPosition() + 1));
                }
            }
        } else {
            mIsShowText.set(false);
            mIndex.set(null);
        }
    }

    public ObservableField<String> getDuration() {
        return mDuration;
    }

    public void onItemClick() {
        mListener.onGalleryItemClick(mPosition, mMediaFile);
    }

    private String getVideoDurationDisplayFormat(long duration) {
        int[] durations = splitToComponentTimes(duration);
        if (durations[0] > 0) {
            return String.format(Locale.getDefault(),
                    "%02d:%02d:%02d",
                    durations[0], durations[1], durations[2]);
        }

        return String.format(Locale.getDefault(),
                "%02d:%02d",
                durations[1], durations[2]);
    }

    private int[] splitToComponentTimes(long timeMilliSeconds) {
        /*
        Will return hh:mm:seconds
         */
        int hour = (int) timeMilliSeconds / 3600000;
        int remainingMilliFromHour = (int) timeMilliSeconds % 3600000;
        int minute = 0;
        int second = 0;
        if (remainingMilliFromHour > 0) {
            minute = remainingMilliFromHour / 60000;
            second = (remainingMilliFromHour % 60000) / 1000;
        }

        return new int[]{hour, minute, second};
    }
}
