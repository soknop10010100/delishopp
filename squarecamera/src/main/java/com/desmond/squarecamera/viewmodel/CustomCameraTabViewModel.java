package com.desmond.squarecamera.viewmodel;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

/**
 * Created by Chhom Veasna on 1/10/2020.
 */
public class CustomCameraTabViewModel {

    private ObservableBoolean mIsSelected = new ObservableBoolean();
    private ObservableField<String> mLabel = new ObservableField<>();

    public CustomCameraTabViewModel(String label, boolean isSelected) {
        mIsSelected.set(isSelected);
        mLabel.set(label);
    }

    public ObservableField<String> getLabel() {
        return mLabel;
    }

    public ObservableBoolean getIsSelected() {
        return mIsSelected;
    }

    public void onSelected() {
        mIsSelected.set(true);
    }

    public void onUnselected() {
        mIsSelected.set(false);
    }
}
