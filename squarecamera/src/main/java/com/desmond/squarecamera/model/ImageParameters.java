package com.desmond.squarecamera.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by desmond on 9/8/15.
 */
public class ImageParameters implements Parcelable {

    public boolean mIsPortrait;
    public int mDisplayOrientation;
    public int mLayoutOrientation;
    public int mRotation;
    public int mWidth;
    public int mHeight;

    public ImageParameters() {
    }

    public boolean isPortrait() {
        return mIsPortrait;
    }

    public ImageParameters createCopy() {
        ImageParameters imageParameters = new ImageParameters();

        imageParameters.mIsPortrait = mIsPortrait;
        imageParameters.mDisplayOrientation = mDisplayOrientation;
        imageParameters.mLayoutOrientation = mLayoutOrientation;
        imageParameters.mWidth = mWidth;
        imageParameters.mHeight = mHeight;
        imageParameters.mRotation = mRotation;

        return imageParameters;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.mIsPortrait ? (byte) 1 : (byte) 0);
        dest.writeInt(this.mDisplayOrientation);
        dest.writeInt(this.mLayoutOrientation);
        dest.writeInt(this.mRotation);
        dest.writeInt(this.mWidth);
        dest.writeInt(this.mHeight);
    }

    private ImageParameters(Parcel in) {
        this.mIsPortrait = in.readByte() != 0;
        this.mDisplayOrientation = in.readInt();
        this.mLayoutOrientation = in.readInt();
        this.mRotation = in.readInt();
        this.mWidth = in.readInt();
        this.mHeight = in.readInt();
    }

    public static final Creator<ImageParameters> CREATOR = new Creator<ImageParameters>() {
        @Override
        public ImageParameters createFromParcel(Parcel source) {
            return new ImageParameters(source);
        }

        @Override
        public ImageParameters[] newArray(int size) {
            return new ImageParameters[size];
        }
    };
}
