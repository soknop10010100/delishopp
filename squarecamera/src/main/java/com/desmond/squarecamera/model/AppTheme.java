package com.desmond.squarecamera.model;


import com.desmond.squarecamera.R;

/**
 * Created by He Rotha on 10/18/18.
 */
public enum AppTheme {

    White(1, R.style.AppTheme_NoActionBar_White, R.string.setting_theme_light_title),
    Black(2, R.style.AppTheme_NoActionBar_Black, R.string.setting_theme_dark_title),
    Purple(3, R.style.AppTheme_NoActionBar_Purple, R.string.setting_theme_purple_title),
    ExpressWhite(4, R.style.AppTheme_NoActionBar_ExpressWhite, R.string.setting_theme_light_title),
    UNKNOWN(-1, -1, -1);

    private int mThemeRes;
    private int mId;
    private int mStringRes;

    AppTheme(int id, int themeRes, int stringRes) {
        mId = id;
        mThemeRes = themeRes;
        mStringRes = stringRes;
    }

    public int getThemeRes() {
        return mThemeRes;
    }

    public int getId() {
        return mId;
    }

    public int getStringRes() {
        return mStringRes;
    }

    public static AppTheme fromValue(int id) {
        for (AppTheme appTheme : AppTheme.values()) {
            if (appTheme.getId() == id) {
                return appTheme;
            }
        }

        return UNKNOWN;
    }
}
