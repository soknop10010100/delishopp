package com.desmond.squarecamera.helper;

import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

import androidx.interpolator.view.animation.FastOutSlowInInterpolator;

/**
 * Created by Or Vitovongsak on 10/01/2022.
 *
 * Copied over from main app's module
 */

public final class AnimationHelper {
    private static final Interpolator INTERPOLATOR = new FastOutSlowInInterpolator();

    private AnimationHelper() {

    }

    public static void animateBounce(View view,
                                     AnimationHelperListener listener) {
        if (view == null) {
            //Consider animation ended automatically
            if (listener != null) {
                listener.onAnimationEnded();
            }
            return;
        }

        Animation animation = AnimationUtils.loadAnimation(view.getContext(), com.sompom.resourcemanager.R.anim.slight_bounce);
        animation.setDuration(600L);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (listener != null) {
                    listener.onAnimationEnded();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.25, 10.0);
        animation.setInterpolator(interpolator);
        view.startAnimation(animation);
    }

    public static void animateBounceFollowButton(View view, long duration, float amplitude, float frequency) {
        Animation animation = AnimationUtils.loadAnimation(view.getContext(), com.sompom.resourcemanager.R.anim.bounce);
        animation.setDuration(duration);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        MyBounceInterpolator interpolator = new MyBounceInterpolator(amplitude, frequency);
        animation.setInterpolator(interpolator);

        view.startAnimation(animation);
    }

    public static void slideDown(View view) {
        Animation animation = AnimationUtils.loadAnimation(view.getContext(), com.sompom.resourcemanager.R.anim.bottom_down);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
    }

    public static void slideUp(View view) {
        Animation animation = AnimationUtils.loadAnimation(view.getContext(), com.sompom.resourcemanager.R.anim.bottom_up);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
    }

    /**
     * Show the quick return view.
     * <p>
     * Animates showing the view, with the view sliding up from the bottom of the screen.
     * After the view has reappeared, its visibility will change to VISIBLE.
     *
     * @param view The quick return view
     */
    public static void showFabButton(final View view, AnimatorListenerAdapter listenerAdapter) {
        view.animate().cancel();

        view.animate()
                .translationY(0f)
                .setInterpolator(INTERPOLATOR)
                .setDuration(200)
                .setListener(listenerAdapter);
    }

    /**
     * Hide the quick return view.
     * <p>
     * Animates hiding the view, with the view sliding down and out of the screen.
     * After the view has disappeared, its visibility will change to GONE.
     *
     * @param view The quick return view
     */
    public static void hideFabButton(final View view, AnimatorListenerAdapter listenerAdapter) {
        view.animate().cancel();

        view.animate()
                .translationY(view.getHeight())
                .setInterpolator(INTERPOLATOR)
                .setDuration(200)
                .setListener(listenerAdapter);
    }

    public static void translationFromX(final View view, AnimatorListenerAdapter listenerAdapter) {
        view.animate().cancel();

        view.animate()
                .translationX(0f)
                .setInterpolator(INTERPOLATOR)
                .setDuration(100)
                .setListener(listenerAdapter);
    }

    public static void translationToX(final View view, AnimatorListenerAdapter listenerAdapter) {
        view.animate().cancel();

        view.animate()
                .translationX(view.getWidth())
                .setInterpolator(INTERPOLATOR)
                .setDuration(100)
                .setListener(listenerAdapter);
    }

    public static void fadeInOrOut(final View view,
                                   boolean isFadeIn,
                                   int duration,
                                   AnimatorListenerAdapter listener) {
        view.animate().cancel();
        view.animate()
                .alpha(isFadeIn ? 1f : 0f)
                .setInterpolator(INTERPOLATOR)
                .setDuration(duration)
                .setListener(listener);
    }

    public interface AnimationHelperListener {
        void onAnimationEnded();
    }

    public static class MyBounceInterpolator implements Interpolator {
        private double mAmplitude = 1;
        private double mFrequency = 10;

        public MyBounceInterpolator(double amplitude, double frequency) {
            mAmplitude = amplitude;
            mFrequency = frequency;
        }

        @Override
        public float getInterpolation(float time) {
            return (float) (-1 * Math.pow(Math.E, -time / mAmplitude) *
                    Math.cos(mFrequency * time) + 1);
        }
    }
}
