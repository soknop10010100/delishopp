package com.desmond.squarecamera.helper;

import android.graphics.SurfaceTexture;
import android.view.TextureView;

/**
 * Created by He Rotha on 2/8/19.
 */
public abstract class SurfaceListener implements TextureView.SurfaceTextureListener {
    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
        onSurfaceTextureAvailable(surfaceTexture);
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {
        //nothing to do with this stuff
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        //nothing to do with this stuff
    }

    public abstract void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture);
}
