package com.desmond.squarecamera.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.util.Log;

import com.sompom.proapp.core.helper.SentryHelper;

public class MediaHelper {

    private static final String TAG = MediaHelper.class.getSimpleName();

    public static int[] retrieveSomeMediaDataOnDevice(String mediaPath,
                                                      int originalWidth,
                                                      int originalHeight,
                                                      int originalOrientation,
                                                      boolean isVideo,
                                                      MediaMetadataRetriever retriever) {
        /*
        Will return with, height, and rotation
         */
        int width = originalWidth;
        int height = originalHeight;
        int rotation = originalOrientation;
        if (isVideo) {
            try {
                boolean shouldRelease = false;
                if (retriever == null) {
                    retriever = new MediaMetadataRetriever();
                    shouldRelease = true;
                }
                retriever.setDataSource(mediaPath);
                width = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
                height = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
                rotation = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));
                if (shouldRelease && retriever != null) {
                    retriever.release();
                }
            } catch (Exception ex) {
                Log.e(TAG, "Unsupported video type: " + ex.getMessage());
                SentryHelper.logSentryError(ex);
            }
        } else {
            if (originalWidth <= 0 || originalHeight <= 0) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                Bitmap bmp = BitmapFactory.decodeFile(mediaPath, options);
                width = options.outWidth;
                height = options.outHeight;
                if (bmp != null) {
                    bmp.recycle();
                }
            }
        }

        return new int[]{width, height, rotation};
    }
}
