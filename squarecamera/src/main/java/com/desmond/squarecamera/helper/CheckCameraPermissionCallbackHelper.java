package com.desmond.squarecamera.helper;

import android.app.Activity;

import androidx.annotation.StringRes;

import com.desmond.squarecamera.R;
import com.desmond.squarecamera.intent.IntentUtil;
import com.desmond.squarecamera.ui.CameraActivity;
import com.desmond.squarecamera.ui.dialog.MessageDialog;

/**
 * Created by nuonveyo
 * on 1/31/19.
 */

public abstract class CheckCameraPermissionCallbackHelper implements PermissionCheckHelper.OnPermissionCallback {
    private final Activity mActivity;

    protected CheckCameraPermissionCallbackHelper(Activity activity) {
        mActivity = activity;
    }

    @Override
    public void onPermissionDeny(boolean isUserPressNeverAskAgain) {
        if (isUserPressNeverAskAgain) {
            MessageDialog dialog = new MessageDialog();
            dialog.setTitle(getString(R.string.popup_permission_request_title));
            dialog.setMessage(mActivity.getString(R.string.popup_permission_force_request_camera_description,
                    mActivity.getString(R.string.app_name)));
            dialog.setLeftText(getString(R.string.popup_permission_app_setting_title),
                    view -> IntentUtil.openAppSetting(mActivity));
            dialog.setRightText(getString(R.string.popup_no_button), null);
            dialog.setCallback(this::onDismissDialog);
            dialog.show(((CameraActivity) mActivity).getSupportFragmentManager(), MessageDialog.TAG);
        }
    }

    public void onDismissDialog() {
    }

    private String getString(@StringRes int id) {
        return mActivity.getString(id);
    }
}
