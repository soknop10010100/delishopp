package com.desmond.squarecamera.helper;

import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import com.desmond.squarecamera.listeners.GalleryLoaderListener;
import com.desmond.squarecamera.model.MediaFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 6/7/18.
 */
public class GalleryLoader implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = GalleryLoader.class.getSimpleName();

    private static final int sLoaderId = 1;
    private FragmentActivity mContext;
    private LoaderType mLoaderType;
    private GalleryLoaderListener mGalleryLoaderListener;
    private ArrayList<MediaFile> mSelectMedia;
    private boolean mIsQueryOnlyLatestOne;

    /**
     * query all photo/video from phone
     */
    public GalleryLoader(FragmentActivity context,
                         List<MediaFile> selectMedia,
                         LoaderType loaderType,
                         GalleryLoaderListener galleryLoaderListener) {
        mContext = context;
        if (selectMedia != null) {
            mSelectMedia = new ArrayList<>(selectMedia);
        }
        mLoaderType = loaderType;
        mGalleryLoaderListener = galleryLoaderListener;
        mIsQueryOnlyLatestOne = false;
    }

    /**
     * query latest one photo/video from phone
     */
    public GalleryLoader(FragmentActivity context,
                         GalleryLoaderListener galleryLoaderListener) {
        mContext = context;
        mGalleryLoaderListener = galleryLoaderListener;
        mLoaderType = LoaderType.PhotoVideo;
        mIsQueryOnlyLatestOne = true;
    }

    public void execute() {
        getLoaderManager().initLoader(sLoaderId, null, this);
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        String[] projection = {
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.DATA,
                MediaStore.Files.FileColumns.MEDIA_TYPE,
                MediaStore.Files.FileColumns.MIME_TYPE,
                MediaStore.Files.FileColumns.DURATION,
                MediaStore.Files.FileColumns.WIDTH,
                MediaStore.Files.FileColumns.HEIGHT,
                MediaStore.Files.FileColumns.SIZE,
                MediaStore.Images.Media.ORIENTATION,
                MediaStore.Video.VideoColumns.DURATION,
                MediaStore.Video.VideoColumns.HEIGHT,
                MediaStore.Video.VideoColumns.WIDTH,

        };

        StringBuilder selection = new StringBuilder();
        if (mLoaderType == LoaderType.PhotoVideo) {
            selection.append("(");
            selection.append(MediaStore.Files.FileColumns.MEDIA_TYPE);
            selection.append("=");
            selection.append(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE);
            selection.append(" OR ");
            selection.append(MediaStore.Files.FileColumns.MEDIA_TYPE);
            selection.append("=");
            selection.append(MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO);
            selection.append(")");
        } else {
            selection.append("(");
            selection.append(MediaStore.Files.FileColumns.MEDIA_TYPE);
            selection.append("=");
            selection.append(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE);
            selection.append(")");
        }

        Uri queryUri = MediaStore.Files.getContentUri("external");

//        if (mIsQueryOnlyLatestOne) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//                return new CursorLoader(
//                        mContext,
//                        queryUri,
//                        projection,
//                        buildSelectionWithSupportFileTypes(selection),
//                        null, // Selection args (none).
//                        MediaStore.Files.FileColumns.DATE_MODIFIED + " DESC"  // Sort order.
//                );
//            } else {
//                return new CursorLoader(
//                        mContext,
//                        queryUri,
//                        projection,
//                        buildSelectionWithSupportFileTypes(selection),
//                        null, // Selection args (none).
//                        MediaStore.Files.FileColumns.DATE_MODIFIED + " DESC LIMIT 1"  // Sort order.
//                );
//            }
//        } else {
            return new CursorLoader(
                    mContext,
                    queryUri,
                    projection,
                    buildSelectionWithSupportFileTypes(selection),
                    null, // Selection args (none).
                    MediaStore.Files.FileColumns.DATE_MODIFIED + " DESC"  // Sort order.
            );
//        }
    }

    private String buildSelectionWithSupportFileTypes(StringBuilder selection) {
        return buildSupportFileTypes(selection,
                "mp4", "mkv", "3gp", "webm", //Video Support
                "bmp", "gif", "jpg", "png", "webp", "heic", "heif"); //Image Support
    }

    private String buildSupportFileTypes(StringBuilder selection, String... types) {
        StringBuilder builder = new StringBuilder(selection);
        builder.append(" AND ");
        builder.append(MediaStore.Files.FileColumns.MIME_TYPE);
        builder.append(" IN ");
        builder.append("(");
        for (int i = 0; i < types.length; i++) {
            builder.append("'");
            builder.append(MimeTypeMap.getSingleton().getMimeTypeFromExtension(types[i]));
            builder.append("'");
            if (i < (types.length - 1)) {
                builder.append(", ");
            }
        }
        builder.append(")");

        return builder.toString();
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
        int dataIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA);
        int mimeTypeIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.MIME_TYPE);
        int widthIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.WIDTH);
        int heightIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.HEIGHT);
        int orientationIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.ORIENTATION);
        int lengthIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.SIZE);
        int videoDurationIndex = cursor.getColumnIndexOrThrow(MediaStore.Video.VideoColumns.DURATION);

        List<MediaFile> galleryData = new ArrayList<>();
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();

        if (cursor.moveToFirst()) {
            do {
                String absolutePathOfImage = cursor.getString(dataIndex);
                String mineType = cursor.getString(mimeTypeIndex);
                int width = cursor.getInt(widthIndex);
                int height = cursor.getInt(heightIndex);
                int orientation = cursor.getInt(orientationIndex);
                int size = cursor.getInt(lengthIndex);
                int maxFileSize = size / (1024 * 1024);

                //We don't allow to upload gif as profile photo.
                boolean isIgnoreImage = false;
                if (mLoaderType == LoaderType.Profile) {
                    isIgnoreImage = !TextUtils.isEmpty(mineType) && TextUtils.equals(mineType, MediaFile.GIF_MIME_TYPE);
                }

                if (maxFileSize <= 10 && !isIgnoreImage) {
                    MediaFile mediaFile = new MediaFile(absolutePathOfImage, mineType);
                    mediaFile.setVideoDuration(cursor.getInt(videoDurationIndex));
                    mediaFile.setWidth(width);
                    mediaFile.setHeight(height);
                    mediaFile.setOrientation(orientation);
//                    Log.d(TAG, "Original mediaFile: width: " + width +
//                            ", height: " + height +
//                            " , orientation: " + orientation +
//                            ", type: " + mediaFile.getType() +
//                            ", File Path: " + mediaFile.getPath());
                    if (mSelectMedia != null) {
                        for (MediaFile file : mSelectMedia) {
                            if (mediaFile.getPath().equals(file.getPath())) {
                                mediaFile.setSelected(true);
                                mediaFile.setPosition(file.getPosition());
                                mSelectMedia.remove(file);
                                break;
                            }
                        }
                    }
                    galleryData.add(mediaFile);
                    if (mIsQueryOnlyLatestOne) {
                        break;
                    }
                }
            } while (cursor.moveToNext());
        }

        retriever.release();
        if (mSelectMedia != null) {
            for (MediaFile mediaFile : mSelectMedia) {
                mediaFile.setSelected(true);
            }
            galleryData.addAll(0, mSelectMedia);
        }
        getLoaderManager().destroyLoader(sLoaderId);
        mGalleryLoaderListener.onLoadComplete(galleryData);
    }

    private LoaderManager getLoaderManager() {
        return LoaderManager.getInstance(mContext);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
    }

    public enum LoaderType {
        PhotoOnly,
        PhotoVideo,
        Profile
    }
}