package com.desmond.squarecamera.ui.fragment;

import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.desmond.squarecamera.BR;
import com.desmond.squarecamera.R;
import com.desmond.squarecamera.adapter.GalleryAdapter;
import com.desmond.squarecamera.databinding.FragmentGalleryBinding;
import com.desmond.squarecamera.helper.MediaHelper;
import com.desmond.squarecamera.intent.CameraIntentBuilder;
import com.desmond.squarecamera.intent.GalleryType;
import com.desmond.squarecamera.model.MediaFile;
import com.desmond.squarecamera.model.MediaType;
import com.desmond.squarecamera.ui.CameraActivity;
import com.desmond.squarecamera.utils.Keys;
import com.desmond.squarecamera.viewmodel.GalleryFrViewModel;

import java.util.List;

/**
 * Created by He Rotha on 6/7/18.
 */
public class GalleryFragment extends Fragment
        implements GalleryFrViewModel.ViewModelListener {

    public static final String TAG = GalleryFragment.class.getName();
    public static final int ORIENTATION_ROTATE_270 = 270;
    public static final int ORIENTATION_ROTATE_90 = 90;

    public GalleryAdapter mAdapter;
    private FragmentGalleryBinding mBinding;
    private GalleryFrViewModel mViewModel;
    private CameraIntentBuilder mSourceType;

    public static GalleryFragment newInstance(CameraIntentBuilder sourceType) {
        Bundle args = new Bundle();
        args.putParcelable(Keys.DATA, sourceType);
        GalleryFragment fragment = new GalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = FragmentGalleryBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSourceType = getArguments().getParcelable(Keys.DATA);
        mViewModel = new GalleryFrViewModel(getActivity(), mSourceType, this);
        mBinding.setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onLoadComplete(List<MediaFile> mediaFiles) {
        mAdapter = new GalleryAdapter(mediaFiles, mSourceType, mViewModel);
        ((SimpleItemAnimator) mBinding.recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        mBinding.recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3), false);
        mBinding.recyclerView.setHasFixedSize(true);
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemUpdate(MediaFile MediaFile, boolean isselect) {
        mAdapter.notifyItem(MediaFile, isselect);
    }

    @Override
    public void onPhotoChoose(List<MediaFile> mediaFiles) {
        if (getActivity() instanceof CameraActivity) {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            for (MediaFile mediaFile : mediaFiles) {
                /*
                Need to re-validate media resolution for some following cases:
                1. If the passed media has zero resolution
                2. Some portrait video or image has the width bigger than height which happen on some
                higher devices whose resolution is height such as Samsung Note 8 with its taken photo or video.
                 */
                if (mediaFile.getWidth() > mediaFile.getHeight() || ((mediaFile.getWidth() == 0 || mediaFile.getHeight() == 0))) {
                    int[] resolution = MediaHelper.retrieveSomeMediaDataOnDevice(mediaFile.getPath(),
                            mediaFile.getWidth(),
                            mediaFile.getHeight(),
                            mediaFile.getOrientation(),
                            mediaFile.getType() == MediaType.VIDEO,
                            retriever);
                    Log.d(TAG, "Original Width: " + mediaFile.getWidth() +
                            ", Original Height: " + mediaFile.getHeight() +
                            ", Original Orientation: " + mediaFile.getOrientation() +
                            ", Update width: " + resolution[0] +
                            ", Update height: " + resolution[1] +
                            ", Update Orientation: " + resolution[2]);
                    mediaFile.setWidth(resolution[0]);
                    mediaFile.setHeight(resolution[1]);
                    mediaFile.setOrientation(resolution[2]);
                }

                if (shouldReverseTheResolution(mediaFile.getWidth(), mediaFile.getHeight(), mediaFile.getOrientation())) {
                    int oldWidth = mediaFile.getWidth();
                    int oldHeight = mediaFile.getHeight();
                    mediaFile.setWidth(oldHeight);
                    mediaFile.setHeight(oldWidth);
                    Log.d(TAG, "Reverse the resolution, new resolution: update width: " + mediaFile.getWidth() + ", update height: " + mediaFile.getHeight());
                }
            }
            retriever.release();
            ((CameraActivity) getActivity()).returnUri(mediaFiles, false);
        }
    }

    private boolean shouldReverseTheResolution(float width, float height, int orientation) {
//        Log.d(TAG, "Check reverse orientation : " + orientation);
//        Log.d(TAG, "shouldReverseTheResolution: width: " + width +
//                ", height: " + height + ", orientation: " + orientation);
        //Need to reverse the width/height for some media that captured with supporting
        // HD display such as aspect ratio 16:9 devices.
        //EX: 1920x1080 video should make reverse to 1080x1920 so as to keep the portrait display
        //Note: This logic is being managing for our application is currently support only
        //portrait display.
        return width > height &&
                (orientation == ORIENTATION_ROTATE_90 ||
                        orientation == ORIENTATION_ROTATE_270);
    }

    @Override
    public void onCameraClick() {
        //To make sure all previous selected media will be used after adding new capture of photo/video
        if (!mViewModel.getMediaFile().isEmpty() && requireActivity() instanceof CameraActivity) {
            ((CameraActivity) requireActivity()).setOriginalData(mViewModel.getMediaFile());
        }

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container,
                        CameraFragmentV2.newInstance(mSourceType),
                        CameraFragmentV2.TAG)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackClick() {
        if (mSourceType.getOpenType() instanceof GalleryType) {
            getActivity().finish();
        } else {
            if (getActivity() instanceof CameraActivity) {
                ((CameraActivity) getActivity()).onCancel(null);
            }
        }
    }

    @Override
    public void onGalleryItemClick(int position) {
        mBinding.recyclerView.setCurrentPlayPosition(position);
    }
}
