package com.desmond.squarecamera.ui.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.desmond.squarecamera.R;
import com.desmond.squarecamera.databinding.SquarecameraFragmentEditSavePhotoBinding;
import com.desmond.squarecamera.helper.AnimationHelper;
import com.desmond.squarecamera.helper.CheckCameraPermissionCallbackHelper;
import com.desmond.squarecamera.intent.CameraIntentBuilder;
import com.desmond.squarecamera.intent.CameraType;
import com.desmond.squarecamera.intent.GalleryType;
import com.desmond.squarecamera.model.ImageParameters;
import com.desmond.squarecamera.model.MediaFile;
import com.desmond.squarecamera.model.MediaType;
import com.desmond.squarecamera.ui.CameraActivity;
import com.desmond.squarecamera.utils.Keys;
import com.desmond.squarecamera.utils.ScreenSize;
import com.desmond.squarecamera.utils.media.CameraImageUtility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class EditSavePhotoFragmentV2 extends Fragment {

    public static final String TAG = EditSavePhotoFragmentV2.class.getSimpleName();

    private SquarecameraFragmentEditSavePhotoBinding mBinding;
    private String mPath;
    private ImageParameters mImageParameters;
    private boolean mIsPhotoSelected = false;

    public EditSavePhotoFragmentV2() {
    }

    public static Fragment newInstance(CameraIntentBuilder sourceType,
                                       String path,
                                       ImageParameters parameters) {
        Fragment fragment = new EditSavePhotoFragmentV2();
        Bundle args = new Bundle();
        args.putParcelable(Keys.SOURCE_TYPE, sourceType);
        args.putString(Keys.PATH, path);
        args.putParcelable(Keys.PARAM, parameters);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = SquarecameraFragmentEditSavePhotoBinding.inflate(inflater, container, false);
        if (getActivity() != null) {
            int navigationBarHeight = ScreenSize.getNavigationBarSize(getActivity());
            int margin = getActivity().getResources().getDimensionPixelOffset(R.dimen.navigation_bar_margin);
            navigationBarHeight += margin;
            if (navigationBarHeight > 0) {
                mBinding.layoutControl.setPadding(0, 0, 0, navigationBarHeight);
            }
        }
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() == null || getActivity() == null) {
            return;
        }
        CameraIntentBuilder sourceType = getArguments().getParcelable(Keys.SOURCE_TYPE);
        mImageParameters = getArguments().getParcelable(Keys.PARAM);

        if (sourceType == null) {
            return;
        }
        mPath = getArguments().getString(Keys.PATH);
        Glide.with(getActivity())
                .load(new File(mPath))
                .into(mBinding.imageView);

        mBinding.savePhoto.setOnClickListener(v -> {
            mBinding.savePhoto.setOnClickListener(null);
            mIsPhotoSelected = true;
            requestForPermission();
        });
        mBinding.retake.setOnClickListener(v -> {
            mBinding.retake.setOnClickListener(null);
            AnimationHelper.animateBounce(v, () -> {
                if (!mIsPhotoSelected) {
                    File file = new File(mPath);
                    file.delete();
                }
                if (getActivity() instanceof CameraActivity) {
                    ((CameraActivity) getActivity()).onCancel(null);
                }
            });
        });

        CameraType cameraType = null;
        if (sourceType.getOpenType() instanceof CameraType) {
            cameraType = (CameraType) sourceType.getOpenType();
        } else if (sourceType.getOpenType() instanceof GalleryType) {
            cameraType = ((GalleryType) sourceType.getOpenType()).getCameraType();
        }
        if (cameraType == null || !cameraType.isShowDescription()) {
            mBinding.textviewDescTop.setVisibility(View.GONE);
            mBinding.textviewDescBottom.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void requestForPermission() {
        if (getActivity() == null) {
            return;
        }
        ((CameraActivity) getActivity()).checkPermission(new CheckCameraPermissionCallbackHelper(getActivity()) {
            @Override
            public void onPermissionGranted() {
                MediaFile mediaFile = new MediaFile();
                mediaFile.setType(MediaType.IMAGE);
                mediaFile.setWidth(mImageParameters.mWidth);
                mediaFile.setHeight(mImageParameters.mHeight);
//                Log.d(TAG, "Width: " + mediaFile.getWidth() + ", height: " + mediaFile.getHeight() + ", mPath: " + mPath);

                if (TextUtils.isEmpty(mPath)) {
                    Bitmap bitmap = ((BitmapDrawable) mBinding.imageView.getDrawable()).getBitmap();
                    Uri photoUri = CameraImageUtility.savePicture(getActivity(), bitmap, false);
                    if (photoUri == null) {
                        return;
                    }
                    mediaFile.setWidth(bitmap.getWidth());
                    mediaFile.setHeight(bitmap.getHeight());
//                    Log.d(TAG, "bitmap Width: " + bitmap.getWidth() + ", bitmap: height: " + bitmap.getHeight() + ", photoUri.getPath(): " + photoUri.getPath());
                    mediaFile.setPath(photoUri.getPath());
                } else {
                    mediaFile.setPath(mPath);
                }

                List<MediaFile> mediaFiles = new ArrayList<>();
                mediaFiles.add(mediaFile);
                ((CameraActivity) getActivity()).returnUri(mediaFiles, true);
            }
        });
    }


}
