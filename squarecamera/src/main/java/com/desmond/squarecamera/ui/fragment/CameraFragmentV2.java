package com.desmond.squarecamera.ui.fragment;

import static com.desmond.squarecamera.utils.media.FileNameUtil.getPictureFile;
import static com.desmond.squarecamera.utils.media.FileNameUtil.getVideoFile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.core.AspectRatio;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.FocusMeteringAction;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.MeteringPoint;
import androidx.camera.core.MeteringPointFactory;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.video.FallbackStrategy;
import androidx.camera.video.FileOutputOptions;
import androidx.camera.video.MediaStoreOutputOptions;
import androidx.camera.video.OutputOptions;
import androidx.camera.video.PendingRecording;
import androidx.camera.video.Quality;
import androidx.camera.video.QualitySelector;
import androidx.camera.video.Recorder;
import androidx.camera.video.Recording;
import androidx.camera.video.VideoCapture;
import androidx.camera.video.VideoRecordEvent;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.desmond.squarecamera.BR;
import com.desmond.squarecamera.R;
import com.desmond.squarecamera.databinding.FragmentCameraV2Binding;
import com.desmond.squarecamera.intent.CameraIntentBuilder;
import com.desmond.squarecamera.listeners.OnCameraViewModelListener;
import com.desmond.squarecamera.model.FlashMode;
import com.desmond.squarecamera.model.ImageParameters;
import com.desmond.squarecamera.ui.CameraActivity;
import com.desmond.squarecamera.ui.dialog.EditPostPrivacyDialog;
import com.desmond.squarecamera.utils.CameraSettingPreferences;
import com.desmond.squarecamera.utils.FormatTime;
import com.desmond.squarecamera.utils.Keys;
import com.desmond.squarecamera.utils.ScreenSize;
import com.desmond.squarecamera.viewmodel.CameraViewModel;
import com.desmond.squarecamera.widget.CameraTabLayout;
import com.google.common.util.concurrent.ListenableFuture;
import com.sompom.proapp.core.helper.SentryHelper;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CameraFragmentV2 extends Fragment {

    public static final String TAG = CameraFragmentV2.class.getSimpleName();
    public static final int MAX_VIDEO_DURATION = 20; //20 seconds

    private static final double RATIO_4_3_VALUE = 4.0 / 3.0;
    private static final double RATIO_16_9_VALUE = 16.0 / 9.0;

    private FlashMode mFlashMode;
    private FragmentCameraV2Binding mBinding;
    private CameraIntentBuilder mCameraIntentBuilder;
    private int mCurrVideoRecTimer = 0;
    private Handler mTimerHandler = new Handler();

    private CameraViewModel mViewModel;
    private ExecutorService mCameraExecutor;
    private ImageCapture mImageCapture;
    private VideoCapture<Recorder> mVideoCapture;
    private Recording mRecording;
    private ProcessCameraProvider mCameraProvider;
    private Camera mCamera;
    private int mLensFacing = CameraSelector.LENS_FACING_BACK;
    private int mCurrentRotation;
    private OrientationEventListener mOrientationEventListener;
    private String mTempPhotoPath;
    private Runnable mMaxVideoDurationRunnable = new Runnable() {
        @Override
        public void run() {
            if (mCurrVideoRecTimer > MAX_VIDEO_DURATION) {
                captureVideo();
            } else {
                mBinding.textViewTime.setText(FormatTime.getFormattedDuration(mCurrVideoRecTimer++));
                mTimerHandler.postDelayed(this, 1000);
            }
        }
    };

    public static CameraFragmentV2 newInstance(CameraIntentBuilder sourceType) {
        Bundle args = new Bundle();
        args.putParcelable(Keys.DATA, sourceType);
        CameraFragmentV2 fragment = new CameraFragmentV2();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            if (savedInstanceState == null) {
                mFlashMode = FlashMode.fromValue(CameraSettingPreferences.getCameraFlashMode(getActivity()));
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_camera_v2, container, false);
        if (getActivity() != null) {
            int navigationBarHeight = ScreenSize.getNavigationBarSize(getActivity());

            if (navigationBarHeight > 0) {
                int margin = getActivity().getResources().getDimensionPixelOffset(R.dimen.navigation_bar_margin);
                Log.e(TAG, "navigationBarHeight: " + navigationBarHeight + ", marginSmall: " + margin);

                navigationBarHeight += margin;
                ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams) mBinding.tabs.getLayoutParams();
                param.setMargins(0, 0, 0, navigationBarHeight);
                ViewGroup.MarginLayoutParams param1 = (ViewGroup.MarginLayoutParams) mBinding.containerRecordButton.getLayoutParams();
                param1.setMargins(0, 0, 0, navigationBarHeight);
            }

            mCameraExecutor = Executors.newSingleThreadExecutor();
        }
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Implement an orientation listener to capture photo with correct orientation
        mOrientationEventListener = new OrientationEventListener(requireContext()) {
            @Override
            public void onOrientationChanged(int orientation) {
                int rotation;
                // Monitors orientation values to determine the target rotation value
                if (orientation >= 45 && orientation < 135) {
                    rotation = Surface.ROTATION_270;
                } else if (orientation >= 135 && orientation < 225) {
                    rotation = Surface.ROTATION_180;
                } else if (orientation >= 225 && orientation < 315) {
                    rotation = Surface.ROTATION_90;
                } else {
                    rotation = Surface.ROTATION_0;
                }
                // Keep track of current orientation of the device
                mCurrentRotation = rotation;
            }
        };
        // Enable the orientation listener to listen to orientation change event. Remember to call
        // disable on the orientation event listener after we are done using it
        mOrientationEventListener.enable();

        if (getArguments() != null) {
            mCameraIntentBuilder = getArguments().getParcelable(Keys.DATA);
        }

        mBinding.containerPrivacy.setOnClickListener(v -> {
            final EditPostPrivacyDialog dialog = EditPostPrivacyDialog.newInstance();
            dialog.setOnCallback(new EditPostPrivacyDialog.OnCallback() {
                @Override
                public void onFollowersClick() {
                    if (getActivity() == null) {
                        return;
                    }
                    dialog.dismiss();
                    mBinding.followerIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_followers));
                    mBinding.followerTextView.setText(R.string.post_menu_followers_title);
                }

                @Override
                public void onEveryoneClick() {
                    if (getActivity() == null) {
                        return;
                    }
                    dialog.dismiss();
                    mBinding.followerIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                            R.drawable.ic_earth_grid));
                    mBinding.followerTextView.setText(R.string.post_menu_everyone_title);
                }
            });
            dialog.show(getChildFragmentManager(), EditPostPrivacyDialog.TAG);
        });

        mViewModel = new CameraViewModel(getActivity(), mCameraIntentBuilder, mFlashMode, new OnCameraViewModelListener() {
            @Override
            public void onCaptureClick(CameraTabLayout.TabIndex index) {
                switch (index) {
                    case LIVE:
                        break;
                    case PHOTO:
                        takePhoto();
                        break;
                    case VIDEO:
                        captureVideo();
                        break;
                }
            }

            @Override
            public void onGalleryClick() {
                if (isAdded()) {
                    FragmentManager fragmentManager = getParentFragmentManager();
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.fragment_container,
                                    GalleryFragment.newInstance(mCameraIntentBuilder),
                                    GalleryFragment.TAG)
                            .addToBackStack(null)
                            .commit();
                }
            }

            @Override
            public void onFlashClick(FlashMode flashMode) {
                mFlashMode = flashMode;
                if (mImageCapture != null) {
                    switch (mFlashMode) {
                        case FlashOn:
                            mImageCapture.setFlashMode(ImageCapture.FLASH_MODE_ON);
                            break;
                        case FlashOff:
                            mImageCapture.setFlashMode(ImageCapture.FLASH_MODE_OFF);
                            break;
                        case FlashAuto:
                            mImageCapture.setFlashMode(ImageCapture.FLASH_MODE_AUTO);
                            break;
                    }
                }
            }
        });
        mBinding.setVariable(BR.viewModel, mViewModel);

        mBinding.tabs.setOnTabChangeListener(tab -> {
            if (getActivity() instanceof CameraActivity) {
                ((CameraActivity) getActivity()).setCurrentCameraTab(tab);
            }
            mViewModel.onTabSelect(tab);
            bindCameraUseCases(tab);
        });
        CameraTabLayout.TabIndex previousIndex = null;
        if (getActivity() instanceof CameraActivity) {
            previousIndex = ((CameraActivity) getActivity()).getCurrentCameraTab();
        }
        mBinding.tabs.setCameraIntentBuilder(mCameraIntentBuilder, previousIndex);
        // TODO: Implement swipe to switch camera functionality
//        mBinding.cameraPreviewView.setSwipeListener(mBinding.tabs.getOnSwipeListener());

        // Implement camera change functionality
        mBinding.changeCamera.setOnClickListener(v -> {
            if (mLensFacing == CameraSelector.LENS_FACING_FRONT) {
                mLensFacing = CameraSelector.LENS_FACING_BACK;
            } else {
                mLensFacing = CameraSelector.LENS_FACING_FRONT;
            }
            // Re-bind use cases to update selected camera
            bindCameraUseCases(mViewModel.mTabIndex.get());
        });

        mBinding.close.setOnClickListener(v -> {
            if (getActivity() == null) {
                return;
            }
            getActivity().finish();
        });

        // Let the preview finish being laid out first, then call function to setup the camera
        mBinding.cameraPreviewView.post(this::setupCamera);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Rebind the camera with the updated display metrics
        bindCameraUseCases(mViewModel.mTabIndex.get());
    }

    @Override
    public void onStop() {
        super.onStop();
        // Disable the orientation event listener when the camera fragment is stopped
        mOrientationEventListener.disable();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBinding = null;
        if (mCameraExecutor != null) {
            mCameraExecutor.shutdown();
        }
    }

    private void setupCamera() {
        ListenableFuture<ProcessCameraProvider> cameraProviderFuture =
                ProcessCameraProvider.getInstance(requireContext());

        cameraProviderFuture.addListener(() -> {
            try {
                // Used to bind the lifecycle of cameras to the lifecycle owner
                mCameraProvider = cameraProviderFuture.get();

                // Call to bind all required camera use case
                bindCameraUseCases(mViewModel.mTabIndex.get());
            } catch (Exception e) {
                Log.e(TAG, "Failed to get cameraProvider", e);
                SentryHelper.logSentryError(e);
            }
        }, ContextCompat.getMainExecutor(requireContext()));
    }

    private void bindCameraUseCases(CameraTabLayout.TabIndex index) {
        if (mCameraProvider == null) {
            return;
        }

//        Point sizeScreen = ScreenSize.getScreenSize(requireActivity());
//        int screenAspectRatio = aspectRatio(sizeScreen.x, sizeScreen.y);
        int screenAspectRatio = AspectRatio.RATIO_16_9;

        // Get rotation of the screen
        int rotation = mBinding.cameraPreviewView.getDisplay().getRotation();

        // Define Preview use case
        Preview preview = new Preview.Builder()
                .setTargetAspectRatio(screenAspectRatio)
                .setTargetRotation(rotation)
                .build();
        preview.setSurfaceProvider(mBinding.cameraPreviewView.getSurfaceProvider());

        switch (index) {
            case LIVE:
                // STUB
                break;
            case PHOTO:
                // Define Image capture use case
                mImageCapture = new ImageCapture.Builder()
                        .setCaptureMode(ImageCapture.CAPTURE_MODE_MAXIMIZE_QUALITY)
                        // We request aspect ratio but no resolution to match preview config, but letting
                        // CameraX optimize for whatever specific resolution best fits our use cases
                        .setTargetAspectRatio(screenAspectRatio)
                        // Set initial target rotation, we will have to call this again if rotation changes
                        // during the lifecycle of this use case
                        .setTargetRotation(rotation)
                        .build();
                break;
            case VIDEO:
                Recorder recorder = new Recorder.Builder()
                        // Target Full HD resolution for now, else for fallback use whatever the
                        // highest quality for the device that is not Full HD
                        .setQualitySelector(QualitySelector.from(Quality.FHD,
                                FallbackStrategy.higherQualityOrLowerThan(Quality.HIGHEST)))
                        .build();
                mVideoCapture = VideoCapture.withOutput(recorder);
                break;
        }

        // If we try to use the 'mFlashMode' to directly set flash mode for mImageCapture, it will
        // display an error. ie: mImageCapture.setFlashMode(mFlashMode.getCameraXFlashMode)
        switch (mFlashMode) {
            case FlashOn:
                mImageCapture.setFlashMode(ImageCapture.FLASH_MODE_ON);
                break;
            case FlashOff:
                mImageCapture.setFlashMode(ImageCapture.FLASH_MODE_OFF);
                break;
            case FlashAuto:
                mImageCapture.setFlashMode(ImageCapture.FLASH_MODE_AUTO);
                break;
        }

        // Determine which camera should be selected
        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(mLensFacing)
                .build();

        try {
            // Unbind use cases before rebinding
            mCameraProvider.unbindAll();

            // Bind use cases to camera
            mCamera = mCameraProvider.bindToLifecycle(this,
                    cameraSelector,
                    preview,
                    index == CameraTabLayout.TabIndex.PHOTO ? mImageCapture : mVideoCapture);

            // Implement tap to focus feature for camerax
            mBinding.cameraPreviewView.setOnTouchListener((view, motionEvent) -> {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    return true;
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    // Get the MeteringPointFactory from PreviewView
                    MeteringPointFactory factory = mBinding.cameraPreviewView.getMeteringPointFactory();

                    // Create a MeteringPoint from the tap coordinates
                    MeteringPoint point = factory.createPoint(motionEvent.getX(), motionEvent.getY());

                    // Create a MeteringAction from the MeteringPoint, you can configure it to specify the metering mode
                    FocusMeteringAction action = new FocusMeteringAction.Builder(point).build();

                    // Trigger the focus and metering. The method returns a ListenableFuture since the operation
                    // is asynchronous. You can use it get notified when the focus is successful or if it fails.
                    mCamera.getCameraControl().startFocusAndMetering(action);
                    return true;
                }
                return false;
            });
        } catch (Exception e) {
            Log.e(TAG, "Use case binding failed", e);
            SentryHelper.logSentryError(e);
        }
    }

    private int aspectRatio(int width, int height) {
        int previewRatio = Math.max(width, height) / Math.min(width, height);
        if (Math.abs(previewRatio - RATIO_4_3_VALUE) <= Math.abs(previewRatio - RATIO_16_9_VALUE)) {
            return AspectRatio.RATIO_4_3;
        }
        return AspectRatio.RATIO_16_9;
    }

    private void takePhoto() {
        if (mImageCapture == null) return;

        // Just before taking a photo, set the desired photo orientation based on device's current
        // rotation. This ensure photo is capture with the correct orientation
        mImageCapture.setTargetRotation(mCurrentRotation);

        // Approach 1: Take a photo, and retrieve an image object in memory.

//        mImageCapture.takePicture(mCameraExecutor, new ImageCapture.OnImageCapturedCallback() {
//            @Override
//            public void onCaptureSuccess(@NonNull ImageProxy image) {
//                image.close();
//            }
//
//            @Override
//            public void onError(@NonNull ImageCaptureException exception) {
//                super.onError(exception);
//            }
//        });

        // Approach 2: Define where the photo will be saved, then take a photo

        ImageCapture.OutputFileOptions outputOptions;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            // Just defined the output file normally for implementation pre scoped storage
            outputOptions = new ImageCapture.OutputFileOptions
                    .Builder(getPictureFile(requireContext()))
                    .build();
        } else {
            // To comply with scope storage, we must save image using MediaStore. Using the File
            // API here doesn't seem to work, but for recording video, it seems to be working ok.
            String name = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name);
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
            contentValues.put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/" + requireContext().getString(R.string.app_name));
            outputOptions = new ImageCapture.OutputFileOptions
                    .Builder(requireContext().getContentResolver(),
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    contentValues)
                    .build();

            // Hackish way to still be able to access the photo using the File API. This may be marked
            // as deprecated, but there are some conflicting source on whether it will actually be removed
            // or not, but currently it still seem to be working
            mTempPhotoPath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    requireContext().getString(R.string.app_name)).getPath() + File.separator + name + ".jpg";
        }

        // Take a photo, and then the photo will be saved to the file specified in outputOptions.
        mImageCapture.takePicture(
                outputOptions,
                mCameraExecutor,
                new ImageCapture.OnImageSavedCallback() {
                    @Override
                    public void onImageSaved(@NonNull ImageCapture.OutputFileResults output) {
                        Uri savedUri = output.getSavedUri();
                        Log.d(TAG, "Photo capture succeeded: " + savedUri);
                        if (savedUri == null) {
                            return;
                        }

                        // Implicit broadcasts will be ignored for devices running API level >= 24
                        // so if you only target API level 24+ you can remove this statement
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                            requireActivity().sendBroadcast(
                                    new Intent(android.hardware.Camera.ACTION_NEW_PICTURE, savedUri)
                            );
                        }
                        File outputFile = new File(savedUri.getPath());
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                            outputFile = new File(mTempPhotoPath);
                        }

                        // If the folder selected is an external media directory, this is
                        // unnecessary but otherwise other apps will not be able to access our
                        // images unless we scan them using [MediaScannerConnection]
//                        String mimeType = MimeTypeMap.getSingleton()
//                                .getMimeTypeFromExtension(outputFile.getAbsolutePath());
//                        MediaScannerConnection.scanFile(
//                                requireContext(),
//                                new String[]{outputFile.getAbsolutePath()},
//                                new String[]{mimeType},
//                                (s, uri) -> Log.d(TAG, "Image capture scanned into media store: " + uri));

//                        Log.d(TAG, "Where is the photo file: " + outputFile);
                        String filePath = outputFile.getAbsolutePath();
                        ImageParameters param = new ImageParameters();

                        int rotationDegree = mImageCapture.getResolutionInfo().getRotationDegrees();
                        Log.d(TAG, "what's the resolution: " + mImageCapture.getResolutionInfo().getResolution());
                        Log.d(TAG, "Image rotation: " + rotationDegree);

                        if (rotationDegree == 0 || rotationDegree == 180) {
                            param.mWidth = mImageCapture.getResolutionInfo().getResolution().getWidth();
                            param.mHeight = mImageCapture.getResolutionInfo().getResolution().getHeight();
                        } else {
                            param.mWidth = mImageCapture.getResolutionInfo().getResolution().getHeight();
                            param.mHeight = mImageCapture.getResolutionInfo().getResolution().getWidth();
                        }

                        if (isAdded()) {
                            getParentFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.fragment_container,
                                            EditSavePhotoFragmentV2.newInstance(mCameraIntentBuilder, filePath, param),
                                            EditSavePhotoFragmentV2.TAG)
                                    .addToBackStack(null)
                                    .commit();
                        }
                    }

                    @Override
                    public void onError(@NonNull ImageCaptureException exception) {
                        Log.e(TAG, "Photo capture failed: ${exc.message}", exception);
                    }
                }
        );
    }

    @SuppressLint("MissingPermission")
    private void captureVideo() {
        if (mVideoCapture == null) return;

        if (mRecording != null) {
            mRecording.stop();
            mRecording = null;
            resetVideoUiState();
        } else {
            // Using Media store to save image to be in compliance with scoped storage when app
            // is targeting Android 11
//            String name = "MV_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//            ContentValues contentValues = new ContentValues();
//            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name);
//            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "video/mp4");
//            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
//                contentValues.put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/" + requireContext().getString(R.string.app_name));
//            }
//            MediaStoreOutputOptions mediaStoreOutputOptions = new MediaStoreOutputOptions
//                    .Builder(requireContext().getContentResolver(), MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
//                    .setContentValues(contentValues)
//                    .build();

            // This seems to still work ok for both scoped storage and no scoped storage, for now
            // at that is.
            FileOutputOptions outputOptions =
                    new FileOutputOptions.Builder(getVideoFile(requireContext())).build();

            try {
                PendingRecording pendingRecording = mVideoCapture.getOutput()
                        .prepareRecording(requireContext(), outputOptions);
                if (PermissionChecker.checkSelfPermission(requireActivity(),
                        Manifest.permission.RECORD_AUDIO) == PermissionChecker.PERMISSION_GRANTED) {
                    pendingRecording.withAudioEnabled();
                }
                mRecording = pendingRecording.start(mCameraExecutor, videoRecordEvent -> {
                    if (videoRecordEvent instanceof VideoRecordEvent.Start) {
                        startRecordingTimer();
                    }

                    if (videoRecordEvent instanceof VideoRecordEvent.Finalize) {
                        // This will always be triggered when recording stop, either successfully
                        // or unsuccessfully.
                        resetVideoUiState();
                        VideoRecordEvent.Finalize finalizeEvent = (VideoRecordEvent.Finalize) videoRecordEvent;
                        OutputOptions options = finalizeEvent.getOutputOptions();

                        // Handle 
                        switch (finalizeEvent.getError()) {
                            case VideoRecordEvent.Finalize.ERROR_FILE_SIZE_LIMIT_REACHED:
                            case VideoRecordEvent.Finalize.ERROR_INSUFFICIENT_STORAGE:
                                if (options instanceof FileOutputOptions) {
                                    if (((FileOutputOptions) options).getFile().exists()) {
                                        // There were some error, but the file exist.
                                        if (isAdded()) {
                                            getParentFragmentManager()
                                                    .beginTransaction()
                                                    .replace(R.id.fragment_container,
                                                            EditSaveVideoFragment.newInstance(mCameraIntentBuilder,
                                                                    ((FileOutputOptions) options).getFile().toString()),
                                                            EditSaveVideoFragment.TAG)
                                                    .addToBackStack(null)
                                                    .commit();
                                        }
                                    }
                                } else if (options instanceof MediaStoreOutputOptions) {
                                    Uri uri = finalizeEvent.getOutputResults().getOutputUri();
                                    if (uri != Uri.EMPTY) {
                                        // Get the image file from Uri then parse the file to the
                                        // preview screen
                                        if (isAdded()) {
                                            getParentFragmentManager()
                                                    .beginTransaction()
                                                    .replace(R.id.fragment_container,
                                                            EditSaveVideoFragment.newInstance(mCameraIntentBuilder,
                                                                    new File(uri.getPath()).toString()),
                                                            EditSaveVideoFragment.TAG)
                                                    .addToBackStack(null)
                                                    .commit();
                                        }
                                    }
                                }
                                break;
                            case VideoRecordEvent.Finalize.ERROR_INVALID_OUTPUT_OPTIONS:
                                // No output are generated here, so reset recording and try again?
                                break;
                            case VideoRecordEvent.Finalize.ERROR_ENCODING_FAILED:
                            case VideoRecordEvent.Finalize.ERROR_NO_VALID_DATA:
                            case VideoRecordEvent.Finalize.ERROR_RECORDER_ERROR:
                            case VideoRecordEvent.Finalize.ERROR_SOURCE_INACTIVE:
                            case VideoRecordEvent.Finalize.ERROR_UNKNOWN:
                                if (options instanceof FileOutputOptions) {
                                    ((FileOutputOptions) options).getFile().delete();
                                } else if (options instanceof MediaStoreOutputOptions) {
                                    Uri uri = finalizeEvent.getOutputResults().getOutputUri();
                                    if (uri != Uri.EMPTY) {
                                        requireContext().getContentResolver().delete(uri, null, null);
                                    }
                                }
                                break;
                            case VideoRecordEvent.Finalize.ERROR_NONE:
                                // If the recording is finalized and there are no error, proceed to 
                                // EditSaveVideoFragment like normally
                                if (options instanceof FileOutputOptions) {
                                    if (isAdded()) {
                                        getParentFragmentManager()
                                                .beginTransaction()
                                                .replace(R.id.fragment_container,
                                                        EditSaveVideoFragment.newInstance(mCameraIntentBuilder,
                                                                ((FileOutputOptions) options).getFile().toString()),
                                                        EditSaveVideoFragment.TAG)
                                                .addToBackStack(null)
                                                .commit();
                                    }
                                } else if (options instanceof MediaStoreOutputOptions) {
                                    Uri uri = finalizeEvent.getOutputResults().getOutputUri();
                                    if (uri != Uri.EMPTY) {
                                        if (isAdded()) {
                                            getParentFragmentManager()
                                                    .beginTransaction()
                                                    .replace(R.id.fragment_container,
                                                            EditSaveVideoFragment.newInstance(mCameraIntentBuilder,
                                                                    new File(uri.getPath()).toString()),
                                                            EditSaveVideoFragment.TAG)
                                                    .addToBackStack(null)
                                                    .commit();
                                        }
                                    }
                                }
                                break;
                        }
                    }
//                    // Handle UI Update here when CameraCapture is recording a video
//                    long time =
//                            TimeUnit.NANOSECONDS.toSeconds(videoRecordEvent.getRecordingStats().getRecordedDurationNanos());
//                    if (mViewModel != null) {
//                        mViewModel.mRecordingDurationText.set(FormatTime.getFormattedDuration((int) time));
//                    }
//                    if (time > MAX_VIDEO_DURATION) {
//                        captureVideo();
//                    }
                });
            } catch (Exception e) {
                Log.e(TAG, "Error starting video capture: " + e);
                SentryHelper.logSentryError(e);
            }
        }
    }

    public void startRecordingTimer() {
        requireActivity().runOnUiThread(mMaxVideoDurationRunnable);
    }

    public void resetVideoUiState() {
        mTimerHandler.removeCallbacks(mMaxVideoDurationRunnable);
        mBinding.textViewTime.setVisibility(View.GONE);
        mBinding.textViewTime.setText(FormatTime.getFormattedDuration(0));
        mCurrVideoRecTimer = 0;
    }
}
