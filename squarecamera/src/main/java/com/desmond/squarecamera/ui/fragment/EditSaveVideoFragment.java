package com.desmond.squarecamera.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.desmond.squarecamera.R;
import com.desmond.squarecamera.databinding.SquarecameraFragmentEditSaveVideoBinding;
import com.desmond.squarecamera.helper.AnimationHelper;
import com.desmond.squarecamera.helper.CheckCameraPermissionCallbackHelper;
import com.desmond.squarecamera.intent.CameraIntentBuilder;
import com.desmond.squarecamera.intent.CameraType;
import com.desmond.squarecamera.intent.GalleryType;
import com.desmond.squarecamera.model.MediaFile;
import com.desmond.squarecamera.model.MediaType;
import com.desmond.squarecamera.ui.CameraActivity;
import com.desmond.squarecamera.utils.Keys;
import com.desmond.squarecamera.utils.ScreenSize;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;
import com.sompom.videomanager.helper.ExoPlayerBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class EditSaveVideoFragment extends Fragment {

    public static final String TAG = EditSaveVideoFragment.class.getSimpleName();

    private SquarecameraFragmentEditSaveVideoBinding mBinding;
    private String mVideoPath;
    private Activity mActivity;
    private SimpleExoPlayer mSimpleExoPlayer;
    private boolean isSaveVideoClicked = false;

    public EditSaveVideoFragment() {
    }

    public static Fragment newInstance(CameraIntentBuilder sourceType,
                                       String videoPath) {
        Fragment fragment = new EditSaveVideoFragment();
        Bundle args = new Bundle();
        args.putParcelable(Keys.SOURCE_TYPE, sourceType);
        args.putString(Keys.DATA, videoPath);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = SquarecameraFragmentEditSaveVideoBinding.inflate(inflater, container, false);
        if (getActivity() != null) {
            mActivity = getActivity();
            int navigationBarHeight = ScreenSize.getNavigationBarSize(getActivity());
            int margin = getActivity().getResources().getDimensionPixelOffset(R.dimen.navigation_bar_margin);
            navigationBarHeight += margin;
            if (navigationBarHeight > 0) {
                mBinding.layoutControl.setPadding(0, 0, 0, navigationBarHeight);
            }
        }
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() == null || getActivity() == null) {
            return;
        }
        CameraIntentBuilder sourceType = getArguments().getParcelable(Keys.SOURCE_TYPE);

        if (sourceType == null) {
            return;
        }
        mVideoPath = getArguments().getString(Keys.DATA);
        initExoPlayer(mActivity, mVideoPath, mBinding.exoPlayerView);
        mBinding.saveVideo.setOnClickListener(v -> {
            mBinding.saveVideo.setOnClickListener(null);
            isSaveVideoClicked = true;
            requestForPermission();
        });
        mBinding.retake.setOnClickListener(v -> {
            mBinding.retake.setOnClickListener(null);
            AnimationHelper.animateBounce(v, () -> {
                if (getActivity() instanceof CameraActivity) {
                    ((CameraActivity) getActivity()).onCancel(null);
                }
            });
        });

        CameraType cameraType = null;
        if (sourceType.getOpenType() instanceof CameraType) {
            cameraType = (CameraType) sourceType.getOpenType();
        } else if (sourceType.getOpenType() instanceof GalleryType) {
            cameraType = ((GalleryType) sourceType.getOpenType()).getCameraType();
        }
        if (cameraType == null || !cameraType.isShowDescription()) {
            mBinding.textviewDescTop.setVisibility(View.GONE);
            mBinding.textviewDescBottom.setVisibility(View.GONE);
        }
    }

    private void initExoPlayer(Context context, String videoPath, PlayerView playerView) {
        mSimpleExoPlayer = ExoPlayerBuilder.build(context,
                videoPath,
                isPlaying -> {
                    if (isPlaying) {
                        hideControl();
                    } else {
                        showControl();
                    }
                });
        mSimpleExoPlayer.setPlayWhenReady(false);
        playerView.setPlayer(mSimpleExoPlayer);
    }

    private void showControl() {
        mBinding.retake.setVisibility(View.VISIBLE);
        mBinding.saveVideo.setVisibility(View.VISIBLE);
    }

    private void hideControl() {
        mBinding.retake.setVisibility(View.INVISIBLE);
        mBinding.saveVideo.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        mSimpleExoPlayer.release();
        if (!isSaveVideoClicked) {
            File file = new File(mVideoPath);
            file.delete();
        }
        super.onDestroy();
    }

    private void requestForPermission() {
        if (getActivity() == null) {
            return;
        }
        ((CameraActivity) getActivity()).checkPermission(new CheckCameraPermissionCallbackHelper(getActivity()) {
            @Override
            public void onPermissionGranted() {
                //add video file into media store, to make sure MediaStore can query new video
                MediaScannerConnection.scanFile(getActivity(), new String[]{mVideoPath}, null, null);
                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                retriever.setDataSource(mVideoPath);
                String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                int width = Integer.parseInt(retriever.extractMetadata((MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH)));
                int height = Integer.parseInt(retriever.extractMetadata((MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT)));
                long duration = Long.parseLong(time);
                int orientation = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));
//                    Log.e("CameraFragment " , "video width : " + width + " height : " + height);
//                    Log.e("CameraFragment " , "orientation " + retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));

                if (orientation == GalleryFragment.ORIENTATION_ROTATE_90 || orientation == GalleryFragment.ORIENTATION_ROTATE_270) {
                    int tmp = height;
                    height = width;
                    width = tmp;
                }

                MediaFile mediaFile = new MediaFile(mVideoPath, MediaType.VIDEO, width, height);
                mediaFile.setVideoDuration(duration);
                List<MediaFile> mediaFiles = new ArrayList<>();
                mediaFiles.add(mediaFile);
                ((CameraActivity) getActivity()).returnUri(mediaFiles, true);
            }
        });
    }
}
