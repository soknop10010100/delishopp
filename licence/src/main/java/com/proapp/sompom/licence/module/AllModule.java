package com.proapp.sompom.licence.module;

import io.realm.annotations.RealmModule;

/**
 * Created by He Rotha on 2019-09-12.
 */

@RealmModule(library = true, allClasses = true)
public class AllModule {
}
