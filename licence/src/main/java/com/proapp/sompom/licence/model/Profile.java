package com.proapp.sompom.licence.model;


import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by He Rotha on 2019-09-10.
 */
public class Profile extends RealmObject {

    @SerializedName("enableChangeFirstName")
    private boolean mEnableChangeFirstName;
    @SerializedName("enableChangeLastName")
    private boolean mEnableChangeLastName;
    @SerializedName("enableChangePassword")
    private boolean mEnableChangePassword;
    @SerializedName("enableChangeThemes")
    private boolean mEnableChangeThemes;

    public boolean isEnableChangeFirstName() {
        return mEnableChangeFirstName;
    }

    public void setEnableChangeFirstName(boolean enableChangeFirstName) {
        this.mEnableChangeFirstName = enableChangeFirstName;
    }

    public boolean isEnableChangeLastName() {
        return mEnableChangeLastName;
    }

    public void setEnableChangeLastName(boolean enableChangeLastName) {
        this.mEnableChangeLastName = enableChangeLastName;
    }

    public boolean isEnableChangePassword() {
        return mEnableChangePassword;
    }

    public void setEnableChangePassword(boolean enableChangePassword) {
        this.mEnableChangePassword = enableChangePassword;
    }

    public boolean isEnableChangeThemes() {
        return mEnableChangeThemes;
    }

    public void setEnableChangeThemes(boolean mEnableChangeThemes) {
        this.mEnableChangeThemes = mEnableChangeThemes;
    }
}
