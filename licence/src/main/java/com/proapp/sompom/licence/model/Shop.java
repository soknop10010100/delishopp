package com.proapp.sompom.licence.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Or Vitovongsak on 19/10/21.
 */

public class Shop extends RealmObject{

    @SerializedName("enableShop")
    private boolean mEnableShop;

    public boolean isEnableShop() {
        return mEnableShop;
    }

    public void setEnableShop(boolean enableShop) {
        this.mEnableShop = enableShop;
    }
}
