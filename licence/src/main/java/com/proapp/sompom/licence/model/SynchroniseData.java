package com.proapp.sompom.licence.model;


import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


/**
 * Created by He Rotha on 2019-09-10.
 */
public class SynchroniseData extends RealmObject {

    @PrimaryKey
    @SerializedName("_id")
    private String mId;

    @SerializedName("profile")
    private Profile mProfile;

    @SerializedName("newsfeed")
    private NewsFeed mNewsFeed;

    @SerializedName("call")
    private VoiceCall mCall;

    @SerializedName("message")
    private Message mMessage;

    public String getId() {return mId;}

    public Profile getProfile() {
        return mProfile;
    }

    public NewsFeed getNewsFeed() {
        return mNewsFeed;
    }

    public VoiceCall getCall() {
        return mCall;
    }

    public Message getMessage() {
        return mMessage;
    }
}
