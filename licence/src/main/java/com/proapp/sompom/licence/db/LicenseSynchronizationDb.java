package com.proapp.sompom.licence.db;

import android.content.Context;

import com.proapp.sompom.licence.model.SynchroniseData;
import com.proapp.sompom.licence.module.AllModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmModel;
import io.realm.RealmResults;

/**
 * Created by He Rotha on 2019-09-12.
 */
public class LicenseSynchronizationDb {

    private LicenseSynchronizationDb() {
    }

    public static Realm getLibraryInstance(Context context) {
        Realm realm;
        try {

            RealmConfiguration licenceConfig = new RealmConfiguration.Builder()
                    .name("licence.realm")
                    .modules(new AllModule())
                    .schemaVersion(DBMigration.SCHEMA_VERSION)
                    .migration(new DBMigration())
                    .build();

            realm = Realm.getInstance(licenceConfig);

        } catch (IllegalStateException ex) {
            Realm.init(context.getApplicationContext());
            realm = Realm.getDefaultInstance();
        }

        return realm;
    }

    public static void clearAllData(Context context) {
        Realm realm = getLibraryInstance(context);
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
        realm.close();
    }

    public static void saveSynchroniseData(Context context, SynchroniseData mSynchroniseData) {
        Realm realm = LicenseSynchronizationDb.getLibraryInstance(context);
        realm.beginTransaction();
        realm.insertOrUpdate(mSynchroniseData);
        realm.commitTransaction();
        realm.close();
    }

    public static SynchroniseData readSynchroniseData(Context context) {
        Realm realm = LicenseSynchronizationDb.getLibraryInstance(context);
        refresh(realm);
        SynchroniseData synchroniseData = realm
                .where(SynchroniseData.class)
                .findFirst();
        if (synchroniseData == null) {
            return null;
        }
        synchroniseData = realm.copyFromRealm(synchroniseData);
        realm.close();
        return synchroniseData;
    }

    public static void refresh(Realm realm) {
        if (!realm.isInTransaction()) {
            realm.refresh();
        }
    }
}
