package com.proapp.sompom.licence.model;


import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by He Rotha on 2019-09-10.
 */
public class Message extends RealmObject {

    @SerializedName("enableGifChat")
    private boolean mEnableGifChat;
    @SerializedName("enableVideoChat")
    private boolean mEnableVideoChat;
    @SerializedName("enableImageChat")
    private boolean mEnableImageChat;
    @SerializedName("enableFileChat")
    private boolean mEnableFileChat;
    @SerializedName("enableAudioChat")
    private boolean mEnableAudioChat;
    @SerializedName("enableEncryption")
    private boolean mEnableEncryption;

    public boolean isEnableGifChat() {
        return mEnableGifChat;
    }

    public void setEnableGifChat(boolean enableGifChat) {
        this.mEnableGifChat = enableGifChat;
    }

    public boolean isEnableVideoChat() {
        return mEnableVideoChat;
    }

    public void setEnableVideoChat(boolean enableVideoChat) {
        this.mEnableVideoChat = enableVideoChat;
    }

    public boolean isEnableImageChat() {
        return mEnableImageChat;
    }

    public void setEnableImageChat(boolean enableImageChat) {
        this.mEnableImageChat = enableImageChat;
    }

    public boolean isEnableFileChat() {
        return mEnableFileChat;
    }

    public void setEnableFileChat(boolean enableFileChat) {
        this.mEnableFileChat = enableFileChat;
    }

    public boolean isEnableAudioChat() {
        return mEnableAudioChat;
    }

    public void setEnableAudioChat(boolean enableAudioChat) {
        this.mEnableAudioChat = enableAudioChat;
    }

    public boolean isEnableEncryption() {
        return mEnableEncryption;
    }

    public void setEnableEncryption(boolean mEnableEncryption) {
        this.mEnableEncryption = mEnableEncryption;
    }
}
