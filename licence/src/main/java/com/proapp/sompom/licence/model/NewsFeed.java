package com.proapp.sompom.licence.model;


import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by He Rotha on 2019-09-10.
 */
public class NewsFeed extends RealmObject {

    @SerializedName("enablePost")
    private boolean mEnablePost;
    @SerializedName("enableLikeComment")
    private boolean mEnableLikeComment;

    public boolean isEnablePost() {
        return mEnablePost;
    }

    public void setEnablePost(boolean enablePost) {
        this.mEnablePost = enablePost;
    }

    public boolean isEnableLikeComment() {
        return mEnableLikeComment;
    }

    public void setEnableLikeComment(boolean enableLikeComment) {
        this.mEnableLikeComment = enableLikeComment;
    }
}
