package com.proapp.sompom.licence.db;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import timber.log.Timber;

/**
 * Created by Chhom Veasna on 2/21/2020.
 */
public class DBMigration implements RealmMigration {

    public static final int SCHEMA_VERSION = 6; // This number must be increase whenever there is the change of DB schema

    @Override

    public void migrate(@NonNull DynamicRealm realm, long oldVersion, long newVersion) {
        Timber.i("migrate: oldVersion: " + oldVersion + ", newVersion: " + newVersion);
        if (oldVersion == 0) {
            RealmObjectSchema voiceCallTempSchema = realm.getSchema().get("VoiceCall");
            if (voiceCallTempSchema != null) {
                voiceCallTempSchema.addField("mEnableVideoCall", boolean.class);
            }
            oldVersion++;
        }

        if (oldVersion == 1) {
            RealmObjectSchema synchroniseDataSchema = realm.getSchema().get("SynchroniseData");
            RealmObjectSchema profileDataSchema = realm.getSchema().get("Profile");
            RealmObjectSchema messageSchema = realm.getSchema().get("Message");

            if (synchroniseDataSchema != null) {
                synchroniseDataSchema.removeField("mUserId");
                synchroniseDataSchema.removeField("mFeature");

                //Add list theme
                RealmObjectSchema listThemeSchema = realm.getSchema().get("ListTheme");
                if (listThemeSchema != null) {
                    synchroniseDataSchema.addRealmListField("mListThemes", listThemeSchema);
                }

                RealmObjectSchema newsfeedSchema = realm.getSchema().get("NewsFeed");
                if (newsfeedSchema != null) {
                    synchroniseDataSchema.addRealmObjectField("mNewsFeed", newsfeedSchema);
                }

                RealmObjectSchema voiceCallSchema = realm.getSchema().get("VoiceCall");
                if (voiceCallSchema != null) {
                    synchroniseDataSchema.addRealmObjectField("mCall", voiceCallSchema);
                }

                if (messageSchema != null) {
                    synchroniseDataSchema.addRealmObjectField("mMessage", messageSchema);
                }

                realm.delete(synchroniseDataSchema.getClassName());
            }

            if (profileDataSchema != null) {
                profileDataSchema.removeField("mTheme");
                profileDataSchema.removeField("mEnableCustomCover");

                profileDataSchema.addField("mEnableChangeThemes", boolean.class);
                realm.delete(profileDataSchema.getClassName());
            }

            if (messageSchema != null) {
                messageSchema.addField("mEnableEncryption", boolean.class);
            }

            oldVersion++;
        }

        if (oldVersion == 2 || oldVersion == 3) {
            RealmObjectSchema synchroniseDataSchema = realm.getSchema().get("SynchroniseData");
            if (synchroniseDataSchema != null && synchroniseDataSchema.hasField("mListThemes")) {
                synchroniseDataSchema.removeField("mListThemes");
            }

            oldVersion++;
        }

        if (oldVersion == 4) {
            RealmObjectSchema synchroniseDataSchema = realm.getSchema().get("SynchroniseData");
            if (synchroniseDataSchema != null) {
                synchroniseDataSchema.addField("mId", String.class);
                synchroniseDataSchema.addPrimaryKey("mId");
            }

            oldVersion++;
        }

        if (oldVersion == 5) {
            RealmObjectSchema synchroniseDataSchema = realm.getSchema().get("SynchroniseData");
            if (synchroniseDataSchema != null && synchroniseDataSchema.hasField("mShop")) {
                synchroniseDataSchema.removeField("mShop");
            }

            RealmObjectSchema shopSchema = realm.getSchema().get("Shop");
            if (shopSchema != null) {
                realm.delete(shopSchema.getClassName());
            }

            oldVersion++;
        }
    }

    @Override
    public int hashCode() {
        //Make sure all instance of this class will be considering as the same.
        return 0x001;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return hashCode() == obj.hashCode();
    }
}
