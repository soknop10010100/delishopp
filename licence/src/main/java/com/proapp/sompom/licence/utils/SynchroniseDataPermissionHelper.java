package com.proapp.sompom.licence.utils;

import android.Manifest;

import androidx.annotation.NonNull;

import com.proapp.sompom.licence.model.SynchroniseData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Or Vitovongsak on 9/2/22.
 */

public class SynchroniseDataPermissionHelper {

    /**
     * A helper function to build a list of permission that the app should request from user based
     * on the package's enabled features
     *
     * @param synchroniseData The client feature used for checking which {@link Manifest.permission} to enable
     * @return A list of {@link Manifest.permission} the app should ask the user to enable
     */
    public static List<String> getRequiredPermissionBasedOnFeature(@NonNull SynchroniseData synchroniseData) {
        List<String> permissionList = new ArrayList<>();

        // This is sort of a mandatory permission for all clone?
        permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        if (isEnableCameraPermission(synchroniseData)) {
            permissionList.add(Manifest.permission.CAMERA);
        }

        if (isEnableRecordAudioPermission(synchroniseData)) {
            permissionList.add(Manifest.permission.RECORD_AUDIO);
        }

        if (isEnableWriteExternalStoragePermission(synchroniseData)) {
            permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (isEnableReadPhoneStatePermission(synchroniseData)) {
            permissionList.add(Manifest.permission.READ_PHONE_STATE);
        }

        return permissionList;
    }

    public static boolean isEnableCameraPermission(@NonNull SynchroniseData synchroniseData) {
        return synchroniseData.getNewsFeed().isEnablePost() ||
                synchroniseData.getNewsFeed().isEnableLikeComment() ||
                synchroniseData.getCall().isEnableVideoCall() ||
                synchroniseData.getMessage().isEnableImageChat() ||
                synchroniseData.getMessage().isEnableVideoChat();
    }

    public static boolean isEnableRecordAudioPermission(@NonNull SynchroniseData synchroniseData) {
        return synchroniseData.getMessage().isEnableAudioChat() ||
                synchroniseData.getMessage().isEnableVideoChat() ||
                synchroniseData.getCall().isEnableCall() ||
                synchroniseData.getCall().isEnableVideoCall() ||
                synchroniseData.getNewsFeed().isEnableLikeComment() ||
                synchroniseData.getNewsFeed().isEnablePost();
    }

    public static boolean isEnableWriteExternalStoragePermission(@NonNull SynchroniseData synchroniseData) {
        return synchroniseData.getNewsFeed().isEnableLikeComment() ||
                synchroniseData.getNewsFeed().isEnablePost() ||
                synchroniseData.getMessage().isEnableImageChat() ||
                synchroniseData.getMessage().isEnableVideoChat() ||
                synchroniseData.getMessage().isEnableGifChat() ||
                synchroniseData.getMessage().isEnableFileChat();
    }

    public static boolean isEnableReadPhoneStatePermission(@NonNull SynchroniseData synchroniseData) {
        return synchroniseData.getCall().isEnableCall() ||
                synchroniseData.getCall().isEnableVideoCall();
    }
}
