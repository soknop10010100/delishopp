package com.proapp.sompom.licence.model;


import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by He Rotha on 2019-09-11.
 */
public class VoiceCall extends RealmObject {

    @SerializedName("enableCall")
    private boolean mEnableCall;
    @SerializedName("freeMinute")
    private int mFreeMinute;
    @SerializedName("amountSpent")
    private int mAmountSpent;
    @SerializedName("amountAvailable")
    private int mAmountAvailable;
    @SerializedName("enableVideoCall")
    private boolean mEnableVideoCall;

    public boolean isEnableCall() {
        return mEnableCall;
    }

    public void setEnableCall(boolean enableCall) {
        this.mEnableCall = enableCall;
    }

    public int getFreeMinute() {
        return mFreeMinute;
    }

    public void setFreeMinute(int freeMinute) {
        this.mFreeMinute = freeMinute;
    }

    public int getAmountSpent() {
        return mAmountSpent;
    }

    public void setAmountSpent(int amountSpent) {
        this.mAmountSpent = amountSpent;
    }

    public int getAmountAvailable() {
        return mAmountAvailable;
    }

    public void setAmountAvailable(int amountAvailable) {
        this.mAmountAvailable = amountAvailable;
    }

    public boolean isEnableVideoCall() {
        return mEnableVideoCall;
    }

    public void setEnableVideoCall(boolean enableVideoCall) {
        mEnableVideoCall = enableVideoCall;
    }
}

