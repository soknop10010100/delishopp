package com.sompom.pushy.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.sompom.pushy.service.SendBroadCastHelper;

public class PushReceiver extends BroadcastReceiver {

    public static final String RECEIVE_PUSHY_NOTIFICATION_EVENT = "com.sompom.pushy.broadcast.PushReceiver";
    public static final String FIELD_ADDITIONAL_DATA = "additionalData";
    private static final String PUSHY_ID_FIELD = "_pushyId";
    private static final String TAG = PushReceiver.class.getSimpleName();

    @Override
    public final void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive");
        String pushyId = intent.getStringExtra(PUSHY_ID_FIELD);
        Log.d(TAG, "pushyId: " + pushyId);
        if (TextUtils.isEmpty(pushyId)) {
            Log.e(TAG, "Received notification from unknown sender.");
            return;
        }

        Intent intentData = new Intent(RECEIVE_PUSHY_NOTIFICATION_EVENT);
        intentData.putExtra(FIELD_ADDITIONAL_DATA, intent.getStringExtra(FIELD_ADDITIONAL_DATA));
        SendBroadCastHelper.verifyAndSendBroadCast(context, intentData);
    }
}
