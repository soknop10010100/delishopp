package com.sompom.pushy.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class PushyNotificationReceiver extends BroadcastReceiver {

    private PushyNotificationReceiverListener mListener;

    public PushyNotificationReceiver(PushyNotificationReceiverListener listener) {
        mListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(getClass().getSimpleName(), "onReceive: PushyNotificationReceiver");
        JSONObject additionalData = null;
        String stringExtra = intent.getStringExtra(PushReceiver.FIELD_ADDITIONAL_DATA);
        try {
            if (stringExtra != null && !TextUtils.isEmpty(stringExtra)) {
                additionalData = new JSONObject(stringExtra);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(getClass().getSimpleName(), "additionalData: " + additionalData);
        if (mListener != null) {
            mListener.onReceivedNotification(additionalData);
        }
    }

    public interface PushyNotificationReceiverListener {
        void onReceivedNotification(JSONObject additionalData);
    }
}
