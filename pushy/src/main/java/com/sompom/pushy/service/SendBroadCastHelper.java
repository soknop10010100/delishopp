package com.sompom.pushy.service;

import android.content.Context;
import android.content.Intent;

public class SendBroadCastHelper {

    public static void verifyAndSendBroadCast(Context context, Intent intent) {
        /*
            Must set the package name for the broadcast intent. otherwise, it will broadcast to all
            cloned apps installed on the same device.
         */
        intent.setPackage(context.getPackageName());
        context.sendBroadcast(intent);
    }
}
