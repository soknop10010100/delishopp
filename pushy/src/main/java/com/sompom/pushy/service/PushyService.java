package com.sompom.pushy.service;

import android.content.Context;
import android.content.IntentFilter;
import android.util.Log;

import com.sompom.pushy.broadcast.PushReceiver;
import com.sompom.pushy.broadcast.PushyNotificationReceiver;

import org.json.JSONObject;

import me.pushy.sdk.Pushy;

public class PushyService {

    private PushyNotificationReceiver mReceiver;
    private Context mContext;
    private PushyServiceListener mListener;

    public PushyService(Context context, PushyServiceListener listener) {
        mContext = context;
        mListener = listener;
    }

    public void init() {
        // Enable FCM fallback delivery
        Pushy.toggleFCM(true, mContext);
        //Init Pushy
        Pushy.listen(mContext);
        checkToRegister();
        mReceiver = new PushyNotificationReceiver(additionalData -> {
            if (mListener != null) {
                mListener.onReceivedNotification(additionalData);
            }
        });
        mContext.registerReceiver(mReceiver, new IntentFilter(PushReceiver.RECEIVE_PUSHY_NOTIFICATION_EVENT));
    }

    public void checkToRegister() {
        if (!Pushy.isRegistered(mContext)) {
            new RegisterForPushNotificationsAsync(mContext, new RegisterForPushNotificationsAsync.RegisterForPushyListener() {
                @Override
                public void onRegisterFinished(String token, Exception error) {
                    if (mListener != null) {
                        mListener.onRegisterFinished(token, error);
                    }
                }

                @Override
                public void onRegisterFinishedInBackground(String token,
                                                           Exception error) {
                    if (mListener != null) {
                        mListener.onRegisterFinishedInBackground(token, error);
                    }
                }
            }).execute();
        } else {
            String token = Pushy.getDeviceCredentials(mContext).token;
            Log.d(PushyService.class.getSimpleName(), "Token: " + Pushy.getDeviceCredentials(mContext).token);
            if (mListener != null) {
                mListener.onRegisterFinished(token, null);
            }
        }
    }

    public void onDestroyService() {
        try {
            mContext.unregisterReceiver(mReceiver);
        } catch (Exception exception) {
            Log.e(getClass().getSimpleName(), exception.toString());
        }
    }

    public static boolean isDeviceRegistered(Context context) {
        return Pushy.isRegistered(context);
    }

    public static String getRegisteredDeviceId(Context context) {
        if (isDeviceRegistered(context)) {
            return Pushy.getDeviceCredentials(context).token;
        }

        return null;
    }

    public interface PushyServiceListener {
        void onRegisterFinished(String token, Exception error);

        default void onReceivedNotification(JSONObject additionalData) {
        }

        default void onRegisterFinishedInBackground(String token,
                                                    Exception error) {
        }
    }
}
