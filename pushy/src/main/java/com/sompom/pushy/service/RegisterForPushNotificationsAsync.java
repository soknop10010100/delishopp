package com.sompom.pushy.service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import me.pushy.sdk.Pushy;

public class RegisterForPushNotificationsAsync extends AsyncTask<Void, Void, String> {

    private static final String TAG = RegisterForPushNotificationsAsync.class.getSimpleName();

    private Context mContext;
    private RegisterForPushyListener mListener;
    private Exception mError;

    public RegisterForPushNotificationsAsync(Context context, RegisterForPushyListener listener) {
        Log.d(RegisterForPushNotificationsAsync.class.getSimpleName(), "RegisterForPushNotificationsAsync");
        this.mContext = context;
        this.mListener = listener;
    }

    protected String doInBackground(Void... params) {
        try {
            String id = Pushy.register(mContext.getApplicationContext());
            if (mListener != null) {
                mListener.onRegisterFinishedInBackground(id, null);
            }
            return id;
        } catch (Exception exc) {
            Log.e(TAG, exc.toString());
            mError = exc;
            if (mListener != null) {
                mListener.onRegisterFinishedInBackground(null, exc);
            }
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG, "Registered token: " + result);
        if (mListener != null) {
            mListener.onRegisterFinished(result, mError);
        }
    }

    public interface RegisterForPushyListener {
        void onRegisterFinished(String token, Exception error);

        default void onRegisterFinishedInBackground(String token, Exception error) {
        }
    }
}